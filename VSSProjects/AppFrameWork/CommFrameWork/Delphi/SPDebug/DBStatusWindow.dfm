object Form1: TForm1
  Left = 348
  Top = 381
  Width = 546
  Height = 311
  Caption = 'Database Status Window'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object lstLog: TListBox
    Left = 0
    Top = 0
    Width = 538
    Height = 284
    Align = alClient
    Color = clBlack
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 0
  end
  object tmCycle: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmCycleTimer
    Left = 2
    Top = 6
  end
  object RCV_MSG: TOracleQuery
    SQL.Strings = (
      'BEGIN'
      'APP_COMM.VIEW_TRACE(:pLOG);'
      'END;')
    Session = OracleSession1
    Variables.Data = {0300000001000000050000003A504C4F47050000000000000000000000}
    Left = 42
    Top = 8
  end
  object OracleSession1: TOracleSession
    AfterLogOn = OracleSession1AfterLogOn
    DesignConnection = True
    Left = 82
    Top = 14
  end
  object OracleLogon1: TOracleLogon
    Session = OracleSession1
    Caption = 'DB Status Trace Logon'
    Left = 158
    Top = 10
  end
  object oSMTPClient: TIdSMTP
    MaxLineAction = maException
    ReadTimeout = 0
    Host = 'mail.ibrid.co.jp'
    Port = 25
    AuthenticationType = atLogin
    Password = 'vicomm-db@ibrid.co.jp'
    Username = 'vicomm-db@ibrid.co.jp'
    Left = 278
    Top = 36
  end
  object oMailMsg: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meMIME
    Recipients = <>
    ReplyTo = <>
    Left = 264
    Top = 116
  end
end
