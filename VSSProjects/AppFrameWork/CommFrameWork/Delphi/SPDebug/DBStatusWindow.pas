
unit DBStatusWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, ExtCtrls,IniFiles, Oracle, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP,IdCoderHeader,
  IdMessage;

type
  TForm1 = class(TForm)
    lstLog: TListBox;
    tmCycle: TTimer;
	RCV_MSG: TOracleQuery;
    OracleSession1: TOracleSession;
    OracleLogon1: TOracleLogon;
    oSMTPClient: TIdSMTP;
	oMailMsg: TIdMessage;
    procedure tmCycleTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
	procedure OracleSession1AfterLogOn(Sender: TOracleSession);
private
		FbNotSendMail:Boolean;

		function	ConnectMailSrv:Boolean;
		function	SendMail(pDoc:String):Boolean;
		procedure 	DisconnectMailSrv;
  public
  end;

var
  Form1: TForm1;

implementation


{$R *.DFM}

procedure TForm1.tmCycleTimer(Sender: TObject);
var
	sLog:String;
begin
	inherited;

	tmCycle.Enabled :=False;

	while (True) do begin

		with RCV_MSG do begin
			Execute;
			sLog := GetVariable('PLOG');
		end;

		if (sLog <> '') then begin
			if ((Copy(sLog,16,7) <> 'ALERT-B') and
				(Copy(sLog,16,8) <> 'BootUpMG')) then begin
				lstLog.Items.Add(sLog);
				if (Copy(sLog,16,7) = '!FATAL!') and (FbNotSendMail = false) then begin
					if (ConnectMailSrv) then begin
						SendMail(sLog);
						DisconnectMailSrv;
					end;
				end;
				if (lstLog.Items.Count > 50000) then begin
					lstLog.Items.Clear;
				end;
			end;
		end else begin
			tmCycle.Enabled :=True;
			break;
		end;
	end;
end;


procedure TForm1.FormShow(Sender: TObject);
begin
	FbNotSendMail := False;

	if OracleSession1.Connected = False then begin
		if (ParamCount < 3) then begin
			OracleLogon1.Execute;
		end else begin
			OracleSession1.LogonUserName := ParamStr(1);
			OracleSession1.LogonPassword := ParamStr(2);
			OracleSession1.LogonDatabase := ParamStr(3);
			if (ParamStr(4) = '1') then begin
				FbNotSendMail := True;
			end;
		end;

		OracleSession1.LogOn;
		tmCycle.Enabled  := True;
	end;

end;

procedure TForm1.OracleSession1AfterLogOn(Sender: TOracleSession);
begin
	Form1.Caption := OracleSession1.LogonUsername + ' DB Status Trace';
	Application.Title :=Copy(OracleSession1.LogonUsername,8,20);
end;

function TForm1.ConnectMailSrv:Boolean;
begin
	Result := False;
	try
		oSMTPClient.Username	:= 'vicomm-db@ibrid.co.jp';
		oSMTPClient.Password	:= 'vicomm-db';
		oSMTPClient.Host		:= 'mail.ibrid.co.jp';
		oSMTPClient.Port		:= 587;
		if Not(oSMTPClient.Connected) then begin
			oSMTPClient.Connect(10000);
		end;
		Result := True;
	except
	end;
end;

function TForm1.SendMail(pDoc:String):Boolean;
begin
	Result := False;

	with oMailMsg do begin
		Clear;

		Body.Add(Encode2022JP(OracleSession1.LogonUsername));
		Body.Add(Encode2022JP(pDoc));

		From.Text					:= 'vicomm-db@ibrid.co.jp';
		ReplyTo.EMailAddresses		:= 'vicomm-db@ibrid.co.jp';
		Recipients.EMailAddresses 	:= 'system@ibrid.co.jp';
		Subject						:= Encode2022JP('MAQIA DB FATAL ERROR');
		CCList.EMailAddresses		:= '';
		BccList.EMailAddresses		:= '';
		ContentType					:= 'text/plain';
		CharSet						:= 'ISO-2022-JP';
	end;
	try
		oSMTPClient.Send(oMailMsg);
		Result := True;
	except
		On CommError : Exception Do Begin
			ShowMessage(CommError.Message + ' '  + oMailMsg.Recipients.EMailAddresses);
		end;
	end;
end;

procedure TForm1.DisconnectMailSrv;
begin
	try
		oSMTPClient.DisConnect;
	except
	end;
end;


end.
