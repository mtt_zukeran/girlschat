object Form1: TForm1
  Left = 235
  Top = 133
  Width = 429
  Height = 574
  Caption = 'Stored Procedure Trace Window'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object lstLog: TListBox
    Left = 0
    Top = 41
    Width = 421
    Height = 506
    Align = alClient
    Color = clBlack
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 421
    Height = 41
    Align = alTop
    TabOrder = 1
    object btnStart: TButton
      Left = 2
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = btnStartClick
    end
    object edtCycle: TEdit
      Left = 80
      Top = 4
      Width = 39
      Height = 20
      ImeMode = imClose
      TabOrder = 1
      Text = '2000'
    end
    object btnStop: TButton
      Left = 176
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 2
      OnClick = btnStopClick
    end
    object btnGet: TButton
      Left = 256
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Get'
      TabOrder = 3
      OnClick = btnGetClick
    end
    object btnClear: TButton
      Left = 334
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Clear'
      TabOrder = 4
      OnClick = btnClearClick
    end
  end
  object tmCycle: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmCycleTimer
    Left = 2
    Top = 28
  end
  object RCV_MSG: TOracleQuery
    SQL.Strings = (
      'BEGIN'
      'APP_COMM.VIEW_TRACE(:pLOG);'
      'END;')
    Session = AppData.DataBase
    Variables.Data = {0300000001000000050000003A504C4F47050000000000000000000000}
    Left = 54
    Top = 40
  end
end
