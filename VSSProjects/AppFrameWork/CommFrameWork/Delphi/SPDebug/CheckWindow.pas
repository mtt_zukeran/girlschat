
unit CheckWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, ExtCtrls,IniFiles, Oracle;

type
  TFrmMain = class(TForm)
    Panel1: TPanel;
    tmTimer: TTimer;
    Button1: TButton;
    procedure tmTimerTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
		procedure ConnectDataBase;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

uses AppDataBase;

{$R *.DFM}

{*----------------------------------*}
{*	Connect DataBase				*}
{*----------------------------------*}
procedure TFrmMain.ConnectDataBase;
begin
	try
		repeat
			Try
				if Not(AppData.DataBase.Connected) then AppData.DataBase.Connected := True;
			Except
				Sleep(3000);
			end;
		until (AppData.DataBase.Connected);


		if (AppData.DataBase.Connected = False) then begin
			Application.Terminate;
			Exit;
		end;

	except
		On IVPError : Exception Do Begin
			MessageDlg('ConnectDataBase ' + IVPError.Message, mtWarning,[mbOK], 0);
		end;
	end;
end;

procedure TFrmMain.tmTimerTimer(Sender: TObject);
begin
	tmTimer.Enabled := false;
	ConnectDataBase;
	Application.Terminate;
end;

procedure TFrmMain.Button1Click(Sender: TObject);
begin
	Application.Terminate;
end;

end.
