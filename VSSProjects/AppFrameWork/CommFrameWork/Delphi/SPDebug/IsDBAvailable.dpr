program IsDBAvailable;

uses
  Forms,
  CheckWindow in 'CheckWindow.pas' {FrmMain},
  AppDataBase in '..\Common\AppDataBase.pas' {AppData: TDataModule},
  IBRID_APP_CONST in '..\Common\IBRID_APP_CONST.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TAppData, AppData);
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
