unit SPDebugWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, ExtCtrls,IniFiles, Oracle;

type
  TForm1 = class(TForm)
    lstLog: TListBox;
    tmCycle: TTimer;
    RCV_MSG: TOracleQuery;
    Panel1: TPanel;
    btnStart: TButton;
    edtCycle: TEdit;
    btnStop: TButton;
    btnGet: TButton;
    btnClear: TButton;
    procedure tmCycleTimer(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure btnGetClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  Form1: TForm1;

implementation

uses AppDataBase;

{$R *.DFM}

procedure TForm1.tmCycleTimer(Sender: TObject);
var
	sLog:String;
begin
	inherited;

	tmCycle.Enabled :=False;

	while (True) do begin

		with RCV_MSG do begin
			Execute;
			sLog := GetVariable('PLOG');
		end;

		if (sLog <> '') then begin
			lstLog.Items.Add(sLog);
			if (lstLog.Items.Count > 5000) then begin
				lstLog.Items.Clear;
			end;
		end else begin
			tmCycle.Enabled :=True;
			break;
		end;
	end;
end;



procedure TForm1.btnStartClick(Sender: TObject);
begin
	tmCycle.Interval := StrToInt(edtCycle.Text);
	tmCycle.Enabled  := True;
	btnStart.Enabled := False;
	btnStop.Enabled  := True;
end;

procedure TForm1.btnStopClick(Sender: TObject);
begin
	tmCycle.Enabled  := False;
	btnStart.Enabled := True;
	btnStop.Enabled  := False;
end;

procedure TForm1.btnGetClick(Sender: TObject);
var
	sLog:String;
begin
	tmCycle.Enabled :=False;

	with RCV_MSG do begin
		Execute;
		sLog := GetVariable('PLOG');
	end;

	if (sLog <> '') then begin
		lstLog.Items.Add(sLog);
	end;

	tmCycle.Enabled :=True;
end;

procedure TForm1.btnClearClick(Sender: TObject);
begin
	lstLog.Items.Clear;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
	if AppData.DataBase.Connected = False then begin
		AppData.DataBase.LogOn;
	end;

end;

end.
