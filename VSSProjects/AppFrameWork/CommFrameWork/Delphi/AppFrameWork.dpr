program AppFrameWork;

uses
  Forms,
  BasicFrame in 'Common\BasicFrame.pas' {FrmBasic},
  IBRID_APP_CONST in 'Common\IBRID_APP_CONST.pas',
  ErrorMsgDlg in 'Common\ErrorMsgDlg.pas' {FrmErrorMsgDlg},
  AppDataBase in 'Common\AppDataBase.pas' {AppData: TDataModule},
  AppUtils in 'Common\AppUtils.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFrmBasic, FrmBasic);
  Application.CreateForm(TFrmErrorMsgDlg, FrmErrorMsgDlg);
  Application.CreateForm(TAppData, AppData);
  Application.Run;
end.
