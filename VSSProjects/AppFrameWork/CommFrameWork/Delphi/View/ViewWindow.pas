unit ViewWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, ExtCtrls,IniFiles, Oracle, OracleData, Grids,
  DBGrids;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    edtSQL: TEdit;
    Button1: TButton;
    DBGrid1: TDBGrid;
    OracleDataSet1: TOracleDataSet;
    DataSource1: TDataSource;
    procedure Button1Click(Sender: TObject);
  private
  public
  end;

var
  Form1: TForm1;

implementation

uses AppDataBase;

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
	if (AppData.DataBase.Connected = False) then AppData.DataBase.Connected := True;

	with OracleDataSet1 do begin
		Close;
		SQL.Clear;
		SQL.Add(edtSQL.Text);
		Open;
	end;

end;

end.
