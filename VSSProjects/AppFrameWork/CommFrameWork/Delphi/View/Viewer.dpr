program Viewer;

uses
  Forms,
  ViewWindow in 'ViewWindow.pas' {Form1},
  AppDataBase in '..\Common\AppDataBase.pas' {AppData: TDataModule},
  IBRID_APP_CONST in '..\Common\IBRID_APP_CONST.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TAppData, AppData);
  Application.Run;
end.
