object Form1: TForm1
  Left = 171
  Top = 358
  Width = 552
  Height = 235
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 18
    Top = 30
    Width = 21
    Height = 12
    Caption = 'Port'
  end
  object Label2: TLabel
    Left = 18
    Top = 10
    Width = 10
    Height = 12
    Caption = 'IP'
  end
  object edtPort: TEdit
    Left = 48
    Top = 26
    Width = 63
    Height = 20
    ImeMode = imClose
    TabOrder = 1
  end
  object Button1: TButton
    Left = 256
    Top = 22
    Width = 80
    Height = 25
    Caption = 'Open Socket'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 338
    Top = 22
    Width = 80
    Height = 25
    Caption = 'Close Socket'
    TabOrder = 5
    OnClick = Button2Click
  end
  object edtIP: TEdit
    Left = 48
    Top = 6
    Width = 181
    Height = 20
    ImeMode = imClose
    TabOrder = 0
  end
  object edtRemarks: TEdit
    Left = 18
    Top = 56
    Width = 455
    Height = 20
    ImeMode = imClose
    TabOrder = 6
  end
  object Button3: TButton
    Left = 18
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 7
    OnClick = Button3Click
  end
  object btnUDP: TRadioButton
    Left = 124
    Top = 28
    Width = 57
    Height = 17
    Caption = 'UDP'
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object btnTCP: TRadioButton
    Left = 188
    Top = 28
    Width = 57
    Height = 17
    Caption = 'TCP'
    TabOrder = 3
  end
  object memStatus: TMemo
    Left = 0
    Top = 119
    Width = 544
    Height = 89
    Align = alBottom
    ImeMode = imClose
    Lines.Strings = (
      'memStatus')
    ReadOnly = True
    TabOrder = 8
  end
  object sktUDP: TIdUDPClient
    Port = 0
    Left = 192
    Top = 58
  end
  object sktTCP: TKTcpClient
    OnError = sktTCPError
    RecvMode = ktrmText
    OnRcvReady = sktTCPRcvReady
    OnDisconnect = sktTCPDisconnect
    Active = False
    Port = 0
    OnConnect = sktTCPConnect
    Left = 68
    Top = 52
  end
end
