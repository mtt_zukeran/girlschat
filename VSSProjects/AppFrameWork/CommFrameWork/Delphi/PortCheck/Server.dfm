object Form1: TForm1
  Left = 351
  Top = 262
  Width = 444
  Height = 253
  Caption = 'Port Check Server'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 4
    Top = 12
    Width = 40
    Height = 12
    Caption = 'RX Port'
  end
  object edRxPort: TEdit
    Left = 50
    Top = 6
    Width = 63
    Height = 20
    ImeMode = imClose
    TabOrder = 0
    Text = '5060'
  end
  object lstLog: TIBridMemo
    Left = 0
    Top = 35
    Width = 436
    Height = 191
    Align = alBottom
    Lines.Strings = (
      'lstLog')
    TabOrder = 1
  end
  object Button1: TButton
    Left = 120
    Top = 6
    Width = 75
    Height = 25
    Caption = 'Open UDP '
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 200
    Top = 6
    Width = 75
    Height = 25
    Caption = 'Clsoe UDP '
    TabOrder = 3
    OnClick = Button2Click
  end
  object sktUDP: TIdUDPServer
    Bindings = <
      item
        IP = '0.0.0.0'
        Port = 7
      end>
    DefaultPort = 0
    OnUDPRead = sktUDPUDPRead
    Left = 232
    Top = 14
  end
end
