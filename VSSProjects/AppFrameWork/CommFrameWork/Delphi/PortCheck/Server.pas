unit Server;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent, IdUDPBase, IdUDPServer, StdCtrls,IdSocketHandle,
  IBridMemo, IdUDPClient;

type
  TForm1 = class(TForm)
    edRxPort: TEdit;
    Label1: TLabel;
    sktUDP: TIdUDPServer;
    lstLog: TIBridMemo;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure sktUDPUDPRead(Sender: TObject; AData: TStream;
      ABinding: TIdSocketHandle);
  private
    { Private �錾 }
	FcBuf 				:Array[0..2048] of Char;
  public
    { Public �錾 }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
	sckTmp :TIdSocketHandle;
	colTmp :TIdSocketHandles;
begin
	colTmp := TIdSocketHandles.Create(Self);
	sckTmp := TIdSocketHandle.Create(colTmp);

	sckTmp.IP 	:= '0.0.0.0';
	sckTmp.Port := StrToInt(edRxPort.Text);


	sktUDP.Bindings := colTmp;
	sktUDP.Active := True;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
	sktUDP.Active := False;
	sktUDP.Bindings.Clear;

end;

procedure TForm1.sktUDPUDPRead(Sender: TObject; AData: TStream;
  ABinding: TIdSocketHandle);
var
	sMsg:String;
begin
	AData.ReadBuffer(FcBuf,Adata.Size);
	FcBuf[Adata.Size] := Char(0);

	SetString(sMsg,FcBuf,Adata.Size);
	lstLog.Lines.add(sMsg);


end;

end.
