unit Client;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent, IdUDPBase, IdUDPServer, StdCtrls,IdSocketHandle,
  IBridMemo, IdUDPClient, KTcp;

type
  TForm1 = class(TForm)
    edtPort: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    sktUDP: TIdUDPClient;
    Label2: TLabel;
    edtIP: TEdit;
    edtRemarks: TEdit;
    Button3: TButton;
    btnUDP: TRadioButton;
    btnTCP: TRadioButton;
    sktTCP: TKTcpClient;
    memStatus: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure sktTCPConnect(Sender: TObject);
    procedure sktTCPDisconnect(Sender: TObject);
    procedure sktTCPError(Sender: TObject; E: TKError);
    procedure sktTCPRcvReady(Sender: TObject);
  private
    { Private �錾 }
	FcBuf 				:Array[0..512] of Char;
  public
    { Public �錾 }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
	if btnUDP.Checked then begin
		sktUDP.Host := edtIP.Text;
		sktUDP.Port := StrToInt(edtPort.Text);
		sktUDP.Active:= True;
	end else begin
		sktTCP.ServerName	:= edtIP.Text;
		sktTCP.Port 		:= StrToInt(edtPort.Text);
		sktTCP.Active		:= True;
	end;

	btnUDP.Enabled := False;
	btnTCP.Enabled := False;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
	if btnUDP.Checked then begin
		sktUDP.Active:= False;
	end else begin
		sktTCP.Active:= False;
	end;

	btnUDP.Enabled := True;
	btnTCP.Enabled := True;
end;


procedure TForm1.Button3Click(Sender: TObject);
begin
	if btnUDP.Checked then begin
		sktUDP.Send(edtRemarks.Text);
	end else begin
		sktTCP.SendString(edtRemarks.Text);
	end;
end;

procedure TForm1.sktTCPConnect(Sender: TObject);
begin
	memStatus.Lines.Add('TCP Connected');
end;

procedure TForm1.sktTCPDisconnect(Sender: TObject);
begin
	memStatus.Lines.Add('TCP Disconnected');
end;

procedure TForm1.sktTCPError(Sender: TObject; E: TKError);
begin
	memStatus.Lines.Add('TCP Error ' + E.Message);
end;

procedure TForm1.sktTCPRcvReady(Sender: TObject);
begin
	memStatus.Lines.Add(sktTCP.RecvString);
end;

end.
