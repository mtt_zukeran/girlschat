unit AppDataBase;

interface

uses
  Windows,SysUtils, Classes, Oracle,Registry, DB, OracleData,Dialogs;

type
  TAppData = class(TDataModule)
    DataBase: TOracleSession;
    qryError: TOracleQuery;
    qryGeneral: TOracleQuery;
    GET_TEXT_PROC_NO: TOracleQuery;
    qryOutputText: TOracleDataSet;
    qryInputFile: TOracleQuery;
    pkgFunction: TOraclePackage;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataBaseBeforeLogOn(Sender: TOracleSession);

	private

	protected

	public
		FoRegInfo			:TRegistry;
		FsLocalIP			:String;
		FsLocalMachine		:String;
		FsDefTextDir		:String;
		FsUserID			:String;
		FsPassword			:String;
		FsServer			:String;
		FsOracleHome		:String;
		FsLanguage			:String;


		procedure OutputText(pProcNo:double;pOutFileName:String);
		procedure InputText(var pProcNo:double;pInFileName:String);
  end;

var
  AppData: TAppData;

implementation

uses IBRID_APP_CONST;

{$R *.dfm}

procedure TAppData.DataModuleCreate(Sender: TObject);
begin
	FoRegInfo	:= TRegistry.Create;
	FoRegInfo.RootKey := HKEY_LOCAL_MACHINE;

	with FoRegInfo do begin
		if (OpenKey('\SOFTWARE\iBrid\AppFrameWork',False)) then begin
			FsUserID		:= ReadString('DBUser');
			FsPassword		:= ReadString('DBPassword');
			FsServer		:= ReadString('DBServer');
			FsDefTextDir	:= ReadString('DefTextDir');
			FsOracleHome	:= ReadString('OracleHome');
			FsLanguage		:= ReadString('Language');
		end;
		CloseKey;
	end;
end;

procedure TAppData.DataModuleDestroy(Sender: TObject);
begin
	if Assigned(FoRegInfo) then FoRegInfo.Free;

end;

procedure TAppData.DataBaseBeforeLogOn(Sender: TOracleSession);
begin
	try
		DataBase.LogonUsername	:= FsUserID;
		DataBase.LogonPassword	:= FsPassword;
		DataBase.LogonDatabase	:= FsServer;
	except
		On DBError : Exception Do Begin
			MessageDlg('AppDataBase.DataBaseBeforeLogOn ' + DBError.Message, mtWarning,[mbOK], 0);
		end;
	end;
end;

{*----------------------------------------------*}
{* Output Text									*}
{*----------------------------------------------*}
procedure TAppData.OutputText(pProcNo:double;pOutFileName:String);
var
 	TxtFile		: TextFile;
begin
	inherited;
	try
		AssignFile(TxtFile, pOutFileName);
		try
			Rewrite(TxtFile);
		except
			Exit;
		end;

		if qryOutputText.Active then qryOutputText.Close;

		with qryOutputText do begin
			SetVariable('wSEQ_NO',pProcNo);
			Open;

			While Not(EOF) do begin
				Writeln(TxtFile,FieldByName('TEXT_DTL').AsString);
				Next;
			end;

			CloseFile(TxtFile);
		end;
	except
	end;

	ChDir('C:\');
end;

{*----------------------------------------------*}
{* Input Text									*}
{*----------------------------------------------*}
procedure TAppData.InputText(var pProcNo:double;pInFileName:String);
var
 	InFile		: TextFile;
	sDtl		: String;
	iLine		: Integer;
begin
	inherited;
	try
		AssignFile(InFile, pInFileName);
		Reset(InFile);
	except
		Exit;
	end;

	try
		with GET_TEXT_PROC_NO do begin
			Execute;
			if GetVariable('pSTATUS') <> RETURN_NORMAL then begin
				Exit;
			end;
			pProcNo := GetVariable('PTEXT_PROC_NO');
		end;

		iLine := 0;

		While Not(Eoln(InFile)) do begin
			ReadLn(InFile,sDtl);
			iLine := iLine + 1;

			with qryInputFile do begin
				SetVariable('wSEQ_NO'		,pProcNo);
				SetVariable('wTEXT_LINE_NO'	,iLine);
				SetVariable('wTEXT_DTL'		,sDtl);
				Execute;
			end;
		end;

		qryInputFile.Close;
		DataBase.Commit;


		CloseFile(InFile);
	except
	end;

	ChDir('C:\');
end;

end.
