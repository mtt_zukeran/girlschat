{*************************************************************************/
--	System			: Appilcation Frame
--  Sub System Name	:
--	Title			: FTP Server Interface
--	Progaram ID		: FtpSrvInterface
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit FtpSrvInterface;

interface
uses
  Windows, Messages, SysUtils, Variants,Classes,INTERFACE_CONST,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdFTP,IniFiles;
type
	TFtpThread= class(TThread)

	private
		FhEventWait 	:THandle;
		FsMsg			:String;
		FoInterfaceInfo	:TFtpBasicInterface;

		procedure SetEnv;
		procedure SetResponse;
		procedure SetErrorMsg;
		procedure FreeObj;

	Protected
	    procedure Execute;override;

	Public
		constructor Create(pExecInfo:TFtpBasicInterface); virtual;
end;

implementation

uses  IBRID_APP_CONST, AppUtils;

constructor TFtpThread.Create(pExecInfo:TFtpBasicInterface);
begin
	FoInterfaceInfo	:=pExecInfo;

	FhEventWait := OpenEvent(EVENT_MODIFY_STATE+Windows.SYNCHRONIZE,FALSE,PChar('IbridFtpSrvSeession' + IntToStr(pExecInfo.EventSeq)));

 	inherited Create(False);

end;

procedure TFtpThread.Execute;
begin
	inherited;

	if Self.Terminated then begin
		Synchronize(FreeObj);
		Exit;
	end;

	with FoInterfaceInfo do begin

		Synchronize(SetEnv);

		try
			if Not(FTPClient.Connected) then begin
				FTPClient.Connect(True,10000);
				FTPClient.Noop;

				case Action of
					ACT_FTP_PUT:begin
						FTPClient.CustomPut(SrcPath+'\'+SrcFileName, DestPath+'\'+SrcFileName,False);
					end;

					ACT_FTP_GET:begin
						FTPClient.CustomGet(SrcPath+'\'+SrcFileName, DestPath+'\'+SrcFileName,True);
					end;

				end;

				FTPClient.Disconnect;
				Synchronize(SetResponse);
			end;
		except
			On ExistError : EIdFTPFileAlreadyExists Do begin
				Synchronize(SetResponse);
				try
					if (FTPClient.Connected) then begin
						FTPClient.Disconnect;
					end;
				except
				end;
			end;

			On CommError : Exception Do Begin
				FsMsg	:= CommError.Message;
				Synchronize(SetErrorMsg);
				try
					if (FTPClient.Connected) then begin
						FTPClient.Disconnect;
					end;
				except
				end;
			end;
		end;
	end;

	Synchronize(FreeObj);

	PostMessage(FoInterfaceInfo.WndHandle, FoInterfaceInfo.WndMsgID,0,Self.ThreadID);

	if (FhEventWait <> 0) then begin
		WaitForSingleObject(FhEventWait , 10000);
		CloseHandle(FhEventWait);
	end;
end;

procedure TFtpThread.SetEnv;
begin
	with FoInterfaceInfo do begin
		FTPClient.Host		:= IP;
		FTPClient.Username	:= UserID;
		FTPClient.Password	:= Password;
		StartTime			:= Now;
	end;
end;

procedure TFtpThread.SetErrorMsg;
begin
	FoInterfaceInfo.StatusMsg	:= FsMsg;
	FoInterfaceInfo.Status		:= RESULT_NG;
	FoInterfaceInfo.EndTime		:= Now;
end;

procedure TFtpThread.SetResponse;
begin
	FoInterfaceInfo.StatusMsg	:= '';
	FoInterfaceInfo.Status		:= RESULT_OK;
	FoInterfaceInfo.EndTime		:= Now;
end;

procedure TFtpThread.FreeObj;
begin
	with FoInterfaceInfo do begin
		if (Assigned(FTPClient)) then begin
			FTPClient.Free;
		end;
	end;
end;

end.
