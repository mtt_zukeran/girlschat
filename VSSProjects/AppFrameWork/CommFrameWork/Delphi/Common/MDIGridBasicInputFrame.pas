{*************************************************************************/
--	System			: iBrid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: シングルグリッド入力基本フレーム(MDI版)
--	Progaram ID		: TFrmMDIGridBasicInput
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit MDIGridBasicInputFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GridBasicInputFrame, ActnList, Grids, IBridGridBase, ITBGrid,
  ExtCtrls, Buttons, Oracle;

type
  TFrmMDIGridBasicInput = class(TFrmGridBasicInput)
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actPF1Execute(Sender: TObject);
    procedure actPF2Execute(Sender: TObject);
    procedure actPF1Update(Sender: TObject);
    procedure actPF2Update(Sender: TObject);
    procedure actPF6Execute(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  FrmMDIGridBasicInput: TFrmMDIGridBasicInput;

implementation

uses CustomDataModule;

{$R *.dfm}
procedure TFrmMDIGridBasicInput.FormCreate(Sender: TObject);
begin
	inherited;
 	Self.WindowState := wsMaximized;
end;

procedure TFrmMDIGridBasicInput.FormActivate(Sender: TObject);
begin
	inherited;
 	Self.WindowState := wsMaximized;
end;


procedure TFrmMDIGridBasicInput.actPF1Execute(Sender: TObject);
begin
	inherited;
	sgTable.Row := sgTable.FixedRows;
end;

procedure TFrmMDIGridBasicInput.actPF2Execute(Sender: TObject);
begin
	inherited;
	sgTable.Row := sgTable.RecCount
end;

procedure TFrmMDIGridBasicInput.actPF1Update(Sender: TObject);
begin
	actPF1.Enabled := (sgTable.Row <>  sgTable.FixedRows) and (sgTable.RecCount <> 0);
end;

procedure TFrmMDIGridBasicInput.actPF2Update(Sender: TObject);
begin
	actPF2.Enabled := (sgTable.Row <> sgTable.RowCount -1) and (sgTable.RecCount <> 0)
end;

procedure TFrmMDIGridBasicInput.actPF6Execute(Sender: TObject);
begin
	inherited;
	if (FbShowOnGetData = True) then begin
		ExecGetSP;
	end;
end;

end.
