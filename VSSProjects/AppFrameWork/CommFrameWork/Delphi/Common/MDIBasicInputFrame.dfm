inherited FrmMDIBaiscInputFrame: TFrmMDIBaiscInputFrame
  VertScrollBar.Range = 0
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'MDI BasicInput'
  FormStyle = fsMDIChild
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel3: TPanel
    inherited btnPF5: TSpeedButton
      Left = 327
    end
    inherited btnPF6: TSpeedButton
      Left = 406
    end
    inherited btnPF7: TSpeedButton
      Left = 485
    end
    inherited btnPF8: TSpeedButton
      Left = 563
    end
    inherited btnPF9: TSpeedButton
      Left = 648
    end
    inherited btnPF10: TSpeedButton
      Left = 726
    end
    inherited btnPF11: TSpeedButton
      Left = 804
    end
    inherited btnPF12: TSpeedButton
      Left = 882
    end
    inherited bvlSep1: TBevel
      Top = 6
    end
    inherited bvlSep2: TBevel
      Left = 643
      Top = 6
    end
  end
end
