{*************************************************************************/
--	System			: iBrid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: DB入力基本フレーム
--	Progaram ID		: TFrmDBBasicInput
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit BasicDBInputFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BasicInputFrame, ActnList, ExtCtrls, Buttons,DB, Oracle;

type
  TFrmDBBasicInput = class(TFrmBasicInput)
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actPF7Update(Sender: TObject);
    procedure actPF7Execute(Sender: TObject);
	private
	protected
		FbFindRec	:Boolean;
		FbDelRec	:Boolean;

		FiRevisionNo:Double;
		FsRowID		:String;

		FsSelSyntax		:String;
		FsOrderSyntax	:String;

		procedure CloseDataSet;
	public
 		procedure	InitWin;				override;

  end;

var
  FrmDBBasicInput: TFrmDBBasicInput;

implementation

{$R *.dfm}

{*--------------------------------------------------------------*}
{*	Window Action												*}
{*--------------------------------------------------------------*}
procedure TFrmDBBasicInput.FormShow(Sender: TObject);
begin
	CloseDataSet;
	inherited;
end;

procedure TFrmDBBasicInput.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	inherited;
end;

{*--------------------------------------------------------------*}
{*	Override Functions											*}
{*--------------------------------------------------------------*}
procedure TFrmDBBasicInput.InitWin();
begin
	inherited;
	FbFindRec	:= False;
	FbDelRec	:= False;
end;


{*--------------------------------------------------------------*}
{*	Private Functions											*}
{*--------------------------------------------------------------*}
procedure TFrmDBBasicInput.CloseDataSet;
var
	ctlTemp	:TComponent;
	i		:Integer;
begin
	with Self do begin
		for I := ComponentCount - 1 downto 0 do begin
            if Components[I] is TDataSet then begin
            	ctlTemp := Components[I];
				if TDataSet(ctlTemp).Active Then Begin
					TDataSet(ctlTemp).Cancel;
					TDataSet(ctlTemp).Close;
				end;
			end;
        end;
    end;
end;


procedure TFrmDBBasicInput.actPF7Update(Sender: TObject);
begin
	inherited;
	actPF7.Enabled := FbFindRec;
end;

procedure TFrmDBBasicInput.actPF7Execute(Sender: TObject);
begin
	inherited;
	if (MessageDlg(GetErrorMsg('C102'), mtConfirmation,[mbNo,mbYes], 0) = mrNo) then begin
		Exit;
	end;
	FbDelRec := True;

	UpdateControl;
end;

end.
