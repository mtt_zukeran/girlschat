{*************************************************************************/
--	System			: iBrid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: 基本フレーム
--	Progaram ID		: TFrmBasic
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit BasicFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls,DBCtrls,DBGrids,Dialogs, ActnList, ExtCtrls, Buttons,
  QryComboBox,IBridEdit,iBridNumEdit,iBridRadioButton,iBridCheckBox,ITBGrid,IBridComboBox;

type
  TFrmBasic = class(TForm)
    pnlTitleFarme: TPanel;
    pnlTitle: TPanel;
    Panel3: TPanel;
    btnPF1: TSpeedButton;
    btnPF2: TSpeedButton;
    btnPF3: TSpeedButton;
    btnPF4: TSpeedButton;
    btnPF5: TSpeedButton;
    btnPF6: TSpeedButton;
    btnPF7: TSpeedButton;
    btnPF8: TSpeedButton;
    btnPF9: TSpeedButton;
    btnPF10: TSpeedButton;
    btnPF11: TSpeedButton;
    btnPF12: TSpeedButton;
    bvlSep1: TBevel;
    bvlSep2: TBevel;
    alBase: TActionList;
    actPF1: TAction;
    actPF2: TAction;
    actPF3: TAction;
    actPF4: TAction;
    actPF5: TAction;
    actPF6: TAction;
    actPF7: TAction;
    actPF8: TAction;
    actPF9: TAction;
    actPF10: TAction;
    actPF11: TAction;
    actPF12: TAction;
    pnlMain: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure actPF12Execute(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure actPF11Execute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure actPF6Execute(Sender: TObject);
	private

	protected
		IBRID_APP_CONTROL_NEXT	:Integer;
		IBRID_APP_CONTROL_PREV	:Integer;
		IBRID_APP_CONTROL_ERROR	:Integer;

		FiColorError	:TColor;
		FiColorNormal	:TColor;
		FiColorWarning	:TColor;

		FbModified		:Boolean;

		FiProcNo		:Integer;
		FbPrinting		:Boolean;

		FbShiftSate		:Boolean;
		FbExistShiftMenu:Boolean;

		FsMenu			:Array[1..12] of String;
 		FsShiftMenu		:Array[1..12] of String;


		// Control Action
		function 	FindControl				(pForm:TForm;pName:String):TComponent;
		procedure	ClearColor				(pComp:TComponent);virtual;
		procedure 	ClearText				(pComp:TComponent);
		procedure	SetColor				(pControl:TComponent;pColor:TColor);
		procedure	SetReadOnly				(pCtl:TComponent;pLock:Boolean);
		procedure	SetReadOnlyStatByScreen	(pForm:TComponent;pLock:Boolean);

		function	GetCode(AText: String): String;

		procedure 	DisplayButton;virtual;

	public
		procedure	FreeIns;		virtual;abstract;
 		procedure	InitWin;		virtual;abstract;
		procedure	WndProc	(var Msg:TMessage);override;

		procedure DataAreaOnKeyPress(Sender: TObject; var Key: Char);
		procedure DataOnKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
end;

var
  FrmBasic: TFrmBasic;

implementation

uses IBRID_APP_CONST, CustomDataModule;

{$R *.dfm}
{*--------------------------------------------------------------*}
{*	Override Function											*}
{*--------------------------------------------------------------*}
procedure TFrmBasic.WndProc(var Msg:TMessage);
begin
	inherited;
end;



{*--------------------------------------------------------------*}
{*	Window Action												*}
{*--------------------------------------------------------------*}
procedure TFrmBasic.FormCreate(Sender: TObject);
begin
	FiColorError	:= clAqua;
	FiColorNormal	:= clWindow;
	FiColorWarning	:= clYellow;

	IBRID_APP_CONTROL_NEXT	:= RegisterWindowMessage(DEF_MSG_CONTROL_NEXT);
	IBRID_APP_CONTROL_PREV	:= RegisterWindowMessage(DEF_MSG_CONTROL_PREV);
	IBRID_APP_CONTROL_ERROR	:= RegisterWindowMessage(DEF_MSG_CONTROL_ERROR);

	if (CustomDB.FsLanguage = 'CHINESE') then begin
		Self.pnlMain.Font.Name := 'SimSun';
		Self.pnlMain.Font.Size	:= 9;
		Self.pnlMain.Font.Charset := 134;
	end;
end;

procedure TFrmBasic.FormShow(Sender: TObject);
var
	i:Integer;
begin
	ClearColor(Self);
	ClearText(Self);

	FsMenu[1]	:= actPF1.Caption;
	FsMenu[2]	:= actPF2.Caption;
	FsMenu[3]	:= actPF3.Caption;
	FsMenu[4]	:= actPF4.Caption;
	FsMenu[5]	:= actPF5.Caption;
	FsMenu[6]	:= actPF6.Caption;
	FsMenu[7]	:= actPF7.Caption;
	FsMenu[8]	:= actPF8.Caption;
	FsMenu[9]	:= actPF9.Caption;
	FsMenu[10]	:= actPF10.Caption;
	FsMenu[11]	:= actPF11.Caption;
	FsMenu[12]	:= actPF12.Caption;

	FbExistShiftMenu := False;

	for i:= 1 to 12 do begin
		if FsShiftMenu[i] <> '' then begin
			FbExistShiftMenu := True;
			break;
		end;
	end;
end;

procedure TFrmBasic.FormActivate(Sender: TObject);
begin
	FbShiftSate := False;
	DisplayButton;
end;

procedure TFrmBasic.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Screen.Cursor 	:= crDefault;
	Action			:= caFree;
end;

procedure TFrmBasic.FormDestroy(Sender: TObject);
begin
	FreeIns;

//	if Owner <> Nil then begin
//		TWinControl(Self.Owner).SetFocus;
//	end;
end;

procedure TFrmBasic.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key ='1' then begin
		FbShiftSate := False;
		DisplayButton;
	end;
end;

procedure TFrmBasic.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if Key =VK_SHIFT then begin
		FbShiftSate := False;
		DisplayButton;
	end;
end;

procedure TFrmBasic.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	case Key of

		VK_SHIFT:begin
			FbShiftSate := True;
			DisplayButton;
			Key := 0;
		end;
		VK_ESCAPE:begin
			actPF12.Execute;
			Key := 0;
		end;
		VK_F1:begin
			actPF1.Execute;
			Key := 0;
		end;
		VK_F2:begin
			actPF2.Execute;
			Key := 0;
		end;
		VK_F3:begin
			actPF3.Execute;
			Key := 0;
		end;
		VK_F4:begin
			actPF4.Execute;
		end;
		VK_F5:begin
			actPF5.Execute;
			Key := 0;
		end;
		VK_F6:begin
			actPF6.Execute;
			Key := 0;
		end;
		VK_F7:begin
			actPF7.Execute;
			Key := 0;
		end;
		VK_F8:begin
			actPF8.Execute;
			Key := 0;
		end;
		VK_F9:begin
			actPF9.Execute;
			Key := 0;
		end;
		VK_F10:begin
			actPF10.Execute;
			Key := 0;
		end;
		VK_F11:begin
			actPF11.Execute;
			Key := 0;
		end;
		VK_F12:begin
			actPF12.Execute;
			Key := 0;
		end;
	end;
end;


procedure TFrmBasic.actPF6Execute(Sender: TObject);
begin
	ClearColor(Self);
end;

procedure TFrmBasic.actPF11Execute(Sender: TObject);
begin
//	ClearColor(Self);
end;

procedure TFrmBasic.actPF12Execute(Sender: TObject);
begin
	Close;
end;


{*--------------------------------------------------------------*}
{*	Window Control Funtions										*}
{*--------------------------------------------------------------*}
Function TFrmBasic.FindControl(pForm:TForm;pName:String):TComponent;
var
	ctlTemp	:TComponent;
	i		:Integer;
begin

	Result := Nil;

	with pForm do begin
		for I := ComponentCount - 1 downto 0 do begin
			ctlTemp := Components[I];
			if UpperCase(ctlTemp.Name) = UpperCase(pName) Then begin
				Result := ctlTemp;
				exit;
			end;
		end;
	end;
end;

// Set Control Color Normal
procedure TFrmBasic.ClearColor(pComp:TComponent);
var
	ctlTemp	:TComponent;
	i		:Integer;
begin
	with pComp do begin
		for I := ComponentCount - 1 downto 0 do begin
			ctlTemp := Components[I];

            if ctlTemp is TEdit	then begin
				if Not TEdit(ctlTemp).ReadOnly			then TEdit(ctlTemp).Color := FiColorNormal else TEdit(ctlTemp).Color := LOCK_COLOR;
			end;

            if ctlTemp is TMemo then begin
				if Not TMemo(ctlTemp).ReadOnly			then TMemo(ctlTemp).Color := FiColorNormal else TMemo(ctlTemp).Color := LOCK_COLOR;
			end;

            if ctlTemp is TDBEdit then begin
				if Not TDBEdit(ctlTemp).ReadOnly		then TDBEdit(ctlTemp).Color := FiColorNormal else TDBEdit(ctlTemp).Color := LOCK_COLOR;
			end;

            if ctlTemp is TIBridComboBox then begin
				if Not TIBridComboBox(ctlTemp).ReadOnly	then TIBridComboBox(ctlTemp).Color := FiColorNormal else TComboBox(ctlTemp).Color := LOCK_COLOR;
			end;

            if ctlTemp is TDBComboBox then begin
				if TDBComboBox(ctlTemp).Enabled		then TDBComboBox(ctlTemp).Color := FiColorNormal else TDBComboBox(ctlTemp).Color := LOCK_COLOR;
			end;

            if ctlTemp is TIBridRadioButton			then TIBridRadioButton(ctlTemp).Color	:= clBtnFace;
            if ctlTemp is TIBridCheckBox			then TIBridCheckBox(ctlTemp).Color		:= clBtnFace;
		end;
	end;
end;

// Clear Control Text
procedure TFrmBasic.ClearText(pComp:TComponent);
var
	ctlTemp	:TComponent;
	i,x,y	:Integer;
begin
	FbModified := False;

	with pComp do begin
		for I := ComponentCount - 1 downto 0 do begin
			ctlTemp := Components[I];

            if ctlTemp is TEdit     		then TEdit(ctlTemp).Text			:= '';
			if ctlTemp is TDBEdit     		then TDBEdit(ctlTemp).Text			:= '';
            if ctlTemp is TComboBox			then TComboBox(ctlTemp).Text		:= '';
			if ctlTemp is TDBComboBox 		then TDBComboBox(ctlTemp).Text		:= '';
			if ctlTemp is TMemo     		then TMemo(ctlTemp).Text			:= '';
            if ctlTemp is TQryComboBox		then TQryComboBox(ctlTemp).Text		:= '';
            if ctlTemp is TIBridEdit		then TIBridEdit(ctlTemp).Text		:= '';
            if ctlTemp is TIBridCheckBox	then begin
				TIBridCheckBox(ctlTemp).Checked:= False;
			end;
            if ctlTemp is TIBridNumEdit		then TIbridNumEdit(ctlTemp).Text	:= '';

            if ctlTemp is TITBGrid			then begin
				with TITBGrid(ctlTemp) do begin
					for x:= 0 to ColCount - 1 do begin
						for y := 1 to RowCount - FixedRows do begin
							Cells[x,y] := '';
						end;
					end;
				end;
			end;
		end;
	end;
end;

// Set Control Color On Form
procedure TFrmBasic.SetColor(pControl:TComponent;pColor:TColor);
begin
	if pControl = Nil then Exit;

	if pControl is TEdit		then TEdit(pControl)		.Color	:= pColor;
	if pControl is TDBEdit		then TDBEdit(pControl)		.Color	:= pColor;
    if pControl is TComboBox	then TComboBox(pControl)	.Color	:= pColor;
	if pControl is TDBComboBox	then TDBComboBox(pControl)	.Color	:= pColor;
	if pControl is TMemo		then TMemo(pControl)		.Color	:= pColor;
	if pControl is TDBMemo		then TDBMemo(pControl)		.Color	:= pColor;
end;

// Set Read Only OR Not Read Only At Control
procedure TFrmBasic.SetReadOnly(pCtl:TComponent;pLock:Boolean);
var
	ctlColor	:TColor;
begin

	if pLock then begin
		ctlColor := LOCK_COLOR;
	end else begin
		ctlColor := FiColorNormal;
	end;

    if pCtl is TEdit     	then begin
    	TEdit(pCtl).ReadOnly		:= pLock;
		TEdit(pCtl).Color			:= ctlColor;
    end;

	if pCtl is TDBEdit     	then begin
		TDBEdit(pCtl).ReadOnly		:= pLock;
		TDBEdit(pCtl).Color			:= ctlColor;
	end;

    if pCtl is TComboBox 	then begin
		TComboBox(pCtl).Enabled		:= not(pLock);
		TComboBox(pCtl).Color		:= ctlColor;
	end;

    if pCtl is TCheckBox 	then begin
		TCheckBox(pCtl).Enabled		:= not(pLock);
//		TCheckBox(pCtl).Color		:= ctlColor;
	end;

    if pCtl is TDBComboBox 	then begin
		TDBComboBox(pCtl).ReadOnly	:= pLock;
		TDBComboBox(pCtl).Color		:= ctlColor;
	end;

    if pCtl is TMemo 	then begin
		TMemo(pCtl).ReadOnly		:= pLock;
		TMemo(pCtl).Color			:= ctlColor;
	end;

    if pCtl is TDBMemo 	then begin
		TDBMemo(pCtl).ReadOnly		:= pLock;
		TDBMemo(pCtl).Color			:= ctlColor;
	end;
end;

// Set Read Only Or Not Read Only On Form
procedure TFrmBasic.SetReadOnlyStatByScreen(pForm:TComponent;pLock:Boolean);
var
	ctlTemp	:TComponent;
	i			:Integer;
	ctlColor	:TColor;
begin

	if pLock then begin
		ctlColor := LOCK_COLOR;
	end else begin
		ctlColor := FiColorNormal;
	end;

	with pForm do begin
		for I := ComponentCount - 1 downto 0 do begin
			ctlTemp := Components[I];

            if ctlTemp is TEdit     	then begin
				TEdit(ctlTemp).ReadOnly			:= pLock;
				TEdit(ctlTemp).Color			:= ctlColor;
			end;

			if ctlTemp is TDBEdit     	then begin
				TDBEdit(ctlTemp).ReadOnly		:= pLock;
				TDBEdit(ctlTemp).Color			:= ctlColor;
			end;

            if ctlTemp is TComboBox 	then begin
				TComboBox(ctlTemp).Enabled		:= not(pLock);
				TComboBox(ctlTemp).Color		:= ctlColor;
			end;

			if ctlTemp is TDBComboBox 	then begin
				TDBComboBox(ctlTemp).ReadOnly	:= pLock;
				TDBComboBox(ctlTemp).Color		:= ctlColor;
			end;

            if ctlTemp is TMemo 	then begin
				TMemo(ctlTemp).ReadOnly			:= pLock;
				TMemo(ctlTemp).Color			:= ctlColor;
			end;

    		if ctlTemp is TDBMemo 	then begin
				TDBMemo(ctlTemp).ReadOnly		:= pLock;
				TDBMemo(ctlTemp).Color			:= ctlColor;
			end;

			if ctlTemp is TDBGrid 	then begin
				TDBGrid(ctlTemp).ReadOnly	:= pLock;
			end;

			if ctlTemp is TDBCheckBox 	then begin
				TDBCheckBox(ctlTemp).ReadOnly	:= pLock;
			end;

			if ctlTemp is TDBRadioGroup 	then begin
				TDBRadioGroup(ctlTemp).ReadOnly	:= pLock;
			end;

//			if ctlTemp is TGroupBox 	then begin
//				if pLock then begin
//					TGroupBox(ctlTemp).Enabled	:= False;
//				end else begin
//					TGroupBox(ctlTemp).Enabled	:= True;
//				end;
//			end;
		end;
	end;
end;

{*--------------------------------------------------------------*}
{* Focus Cotrol & Modify Detection								*}
{*--------------------------------------------------------------*}
procedure TFrmBasic.DataAreaOnKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then begin
		Key := #0;
	end;
	if Key <> #0 then inherited KeyPress(Key);
end;

procedure TFrmBasic.DataOnKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
	inherited;
	case (Key) of
		VK_RETURN:begin
			if (ssShift in Shift) then begin
				SelectNext(Self,False,True);
			end else begin
				SelectNext(Self,True,True);
			end;
			Key := 0;
		end;

		VK_SHIFT:begin
			FbShiftSate := True;
			DisplayButton;
			Key := 0;
		end;
	end;
end;


{*--------------------------------------------------------------*}
{* Query Combo Functions										*}
{*--------------------------------------------------------------*}
function TFrmBasic.GetCode(AText: String): String;
begin
	if Pos(' ', AText) = 0 then begin
		result := AText
	end else begin
		result := Copy(AText, 1, Pos(' ', AText)-1);
	end;
end;

{*--------------------------------------------------------------*}
{* Display														*}
{*--------------------------------------------------------------*}
procedure TFrmBasic.DisplayButton;
var
	i	:Integer;
	oAct:TAction;
begin
	if Not(FbExistShiftMenu) Then Exit;


	for i:= 1 to 12 do begin
		oAct := TAction(FindControl(self,Format('actPF%d',[i])));

		if (FbShiftSate = False) then begin
			oAct.Caption := FsMenu[i];
		end else begin
			oAct.Caption := FsShiftMenu[i];
		end;

	end;
end;



end.
