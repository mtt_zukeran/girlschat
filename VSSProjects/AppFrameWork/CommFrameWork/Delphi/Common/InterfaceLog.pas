{*************************************************************************/
--	System			: iBrid Appilcation Frame
--  Sub System Name	: ����
--	Title			: HTTP Server Interface Log
--	Progaram ID		: TFrmInterfaceLog
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit InterfaceLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

const
	MAX_LOG_LINES = 1000;

type
  TFrmInterfaceLog = class(TForm)
    lstWeb: TMemo;
    procedure FormCreate(Sender: TObject);
	private
	public
		procedure LogHTTP(pThreadID:Integer;pMsg:String);
	end;

var
  FrmInterfaceLog: TFrmInterfaceLog;

implementation

{$R *.dfm}

procedure TFrmInterfaceLog.FormCreate(Sender: TObject);
begin
	lstWeb.Lines.Clear;
end;

procedure TFrmInterfaceLog.LogHTTP(pThreadID:Integer;pMsg:String);
begin
	if (pMsg = '') Then Exit;


	if (lstWeb.Lines.Count >= MAX_LOG_LINES) then begin
		lstWeb.Clear;
	end;

	lstWeb.Lines.Add(Format('%0.7d ',[pThreadID]) + FormatDateTime('mm/dd hh:nn:ss',Now) + ' ' +  pMsg);
end;

end.
