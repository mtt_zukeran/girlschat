{*************************************************************************/
--	System			: Appilcation Frame
--  Sub System Name	: 
--	Title			: HTTP Server Interface
--	Progaram ID		: HttpSrvInterface
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit HttpSrvInterface;

interface
uses
  Windows, Messages, SysUtils, Variants,Classes,INTERFACE_CONST,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  IdIOHandler, IdIOHandlerSocket, IdSSLOpenSSL,IniFiles;
type
	THttpThread= class(TThread)

	private
		FhEventWait 		:THandle;

		FsResponse			:String;
		FsErrorMsg			:String;
		FrpInterfaceInfo	:THttpBasicInterface;

		FHttpQueryField		:THashedStringList;
		FHttpQueryValue		:THashedStringList;

		function SetQuery(pFHttpQuery:String):Boolean;

		procedure SetEnv;
		procedure SetResponse;
		procedure SetErrorMsg;
		procedure FreeObj;

	Protected
	    procedure Execute;override;

	Public
		constructor Create(pExecInfo:THttpBasicInterface); virtual;
end;

implementation

uses  IBRID_APP_CONST, AppUtils;

{*----------------------------------------------------------------------------------*}
{*	THttpThread�@Class																*}
{*----------------------------------------------------------------------------------*}
constructor THttpThread.Create(pExecInfo:THttpBasicInterface);
begin
	FrpInterfaceInfo:= pExecInfo;

	FHttpQueryField	:= THashedStringList.Create;
	FHttpQueryValue := THashedStringList.Create;

	FhEventWait := OpenEvent(EVENT_MODIFY_STATE+Windows.SYNCHRONIZE,FALSE,PChar('IbridFtpSrvSeession' + IntToStr(pExecInfo.EventSeq)));

 	inherited Create(True);
end;

procedure THttpThread.Execute;
var
	sQuery	:String;
	oQuery	:THashedStringList;
	i		:Integer;
begin
	inherited;

	if Self.Terminated then begin
		Synchronize(FreeObj);
		Exit;
	end;

	with FrpInterfaceInfo do begin
		oQuery	:= Nil;

		Synchronize(SetEnv);

		try
			if Not(HttpClient.Connected) then begin

				if Not(PostFlag) then begin
					sQuery		:=URI + Format('?%s',[TransQuery]);
					FsResponse	:=HttpClient.Get(sQuery);
				end else begin
					SetQuery(FrpInterfaceInfo.TransQuery);
					oQuery	:= THashedStringList.Create;
					for i:= 0 to FHttpQueryField.Count-1 do begin
						oQuery.Add(Format('%s=%s',[FHttpQueryField[i],FHttpQueryValue[i]]));
					end;
					FsResponse :=HttpClient.Post(URI,oQuery);
				end;

				HttpClient.Disconnect;
				Synchronize(SetResponse);
			end;

		except
			On DlgError : Exception Do Begin
				FsErrorMsg := DlgError.Message;
				Synchronize(SetErrorMsg);
				try
					if (FrpInterfaceInfo.HttpClient.Connected) then FrpInterfaceInfo.HttpClient.Disconnect;
				except
				end;
			end;
		end;

		Synchronize(FreeObj);

		if (PostFlag) then begin
			if oQuery <> Nil Then oQuery.Free;
		end;

		PostMessage(FrpInterfaceInfo.WndHandle, FrpInterfaceInfo.WndMsgID,0,Self.ThreadID);
	end;

	if (FhEventWait <> 0) then begin
		WaitForSingleObject(FhEventWait , 10000);
		CloseHandle(FhEventWait);
	end;
end;


procedure THttpThread.SetEnv;
begin
	with FrpInterfaceInfo do begin
		HTTPClient.ReadTimeout		:= FrpInterfaceInfo.TimeOut;
		HTTPClient.ProtocolVersion	:= pv1_1;

		if (SSLFlag) then begin
			SSLHandler.SSLOptions.Method:= sslvSSLv3;
			SSLHandler.SSLOptions.Mode	:= sslmUnassigned;
			HttpClient.IOHandler		:= SSLHandler;
		end;

		if Not(HttpClient.Connected) then begin
			TransDate	:= Now;
    	end;
	end;
end;

procedure THttpThread.SetResponse;
begin
	with FrpInterfaceInfo do begin
		Response	:= FsResponse;
		Status		:= RESULT_OK;
	end;
end;

procedure THttpThread.SetErrorMsg;
begin
	FrpInterfaceInfo.ErrorMsg	:= FsErrorMsg;
	FrpInterfaceInfo.Status		:= RESULT_NG;
end;

procedure THttpThread.FreeObj;
begin
	if (Assigned(FHttpQueryField)) then FHttpQueryField.Free;
	if (Assigned(FHttpQueryValue)) then FHttpQueryValue.Free;

	with FrpInterfaceInfo do begin
		if (Assigned(HttpClient)) then begin
			HttpClient.Free;
			if (SSLFlag) then begin
				if SSLHandler <> Nil Then SSLHandler.Free;
			end;
		end;
	end;
end;


function THttpThread.SetQuery(pFHttpQuery:String):Boolean;
begin
	Result := AppUtils.SetQuery(FHttpQueryField,FHttpQueryValue,pFHttpQuery);
end;

end.
