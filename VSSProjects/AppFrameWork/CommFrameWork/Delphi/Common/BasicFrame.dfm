object FrmBasic: TFrmBasic
  Left = 89
  Top = 131
  Width = 1020
  Height = 740
  Caption = 'Frame Basic'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object pnlTitleFarme: TPanel
    Left = 0
    Top = 0
    Width = 1012
    Height = 34
    Align = alTop
    BevelInner = bvLowered
    Caption = 'pnlTitleFarme'
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object pnlTitle: TPanel
      Left = 2
      Top = 2
      Width = 1008
      Height = 30
      Align = alClient
      Caption = 'Program Title'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clTeal
      Font.Height = -16
      Font.Name = #65325#65331' '#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 679
    Width = 1012
    Height = 34
    Align = alBottom
    BevelInner = bvLowered
    Caption = ' '
    TabOrder = 1
    object btnPF1: TSpeedButton
      Left = 8
      Top = 6
      Width = 78
      Height = 24
      Action = actPF1
    end
    object btnPF2: TSpeedButton
      Left = 86
      Top = 6
      Width = 78
      Height = 24
      Action = actPF2
    end
    object btnPF3: TSpeedButton
      Left = 164
      Top = 6
      Width = 78
      Height = 24
      Action = actPF3
    end
    object btnPF4: TSpeedButton
      Left = 242
      Top = 6
      Width = 78
      Height = 24
      Action = actPF4
    end
    object btnPF5: TSpeedButton
      Left = 326
      Top = 6
      Width = 78
      Height = 24
      Action = actPF5
    end
    object btnPF6: TSpeedButton
      Left = 404
      Top = 6
      Width = 78
      Height = 24
      Action = actPF6
    end
    object btnPF7: TSpeedButton
      Left = 482
      Top = 6
      Width = 78
      Height = 24
      Action = actPF7
    end
    object btnPF8: TSpeedButton
      Left = 560
      Top = 6
      Width = 78
      Height = 24
      Action = actPF8
    end
    object btnPF9: TSpeedButton
      Left = 644
      Top = 6
      Width = 78
      Height = 24
      Action = actPF9
    end
    object btnPF10: TSpeedButton
      Left = 722
      Top = 6
      Width = 78
      Height = 24
      Action = actPF10
    end
    object btnPF11: TSpeedButton
      Left = 800
      Top = 6
      Width = 78
      Height = 24
      Action = actPF11
    end
    object btnPF12: TSpeedButton
      Left = 878
      Top = 6
      Width = 78
      Height = 24
      Action = actPF12
    end
    object bvlSep1: TBevel
      Left = 322
      Top = 7
      Width = 2
      Height = 24
      Style = bsRaised
    end
    object bvlSep2: TBevel
      Left = 640
      Top = 7
      Width = 2
      Height = 24
      Style = bsRaised
    end
  end
  object pnlMain: TPanel
    Left = 0
    Top = 34
    Width = 1012
    Height = 645
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvNone
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object alBase: TActionList
    Left = 16
    Top = 65532
    object actPF1: TAction
      Tag = 1
      Category = 'Function'
      ShortCut = 112
    end
    object actPF2: TAction
      Tag = 2
      Category = 'Function'
      ShortCut = 113
    end
    object actPF3: TAction
      Tag = 3
      Category = 'Function'
      ShortCut = 114
    end
    object actPF4: TAction
      Tag = 4
      Category = 'Function'
      ShortCut = 115
    end
    object actPF5: TAction
      Tag = 5
      Category = 'Function'
      ShortCut = 116
    end
    object actPF6: TAction
      Tag = 6
      Category = 'Function'
      Caption = #12463#12522#12450
      ShortCut = 117
      OnExecute = actPF6Execute
    end
    object actPF7: TAction
      Tag = 7
      Category = 'Function'
      ShortCut = 118
    end
    object actPF8: TAction
      Tag = 8
      Category = 'Function'
      ShortCut = 119
    end
    object actPF9: TAction
      Tag = 9
      Category = 'Function'
      ShortCut = 120
    end
    object actPF10: TAction
      Tag = 10
      Category = 'Function'
      ShortCut = 121
    end
    object actPF11: TAction
      Tag = 11
      Category = 'Function'
      Caption = #12463#12522#12450
      ShortCut = 122
      OnExecute = actPF11Execute
    end
    object actPF12: TAction
      Tag = 12
      Category = 'Function'
      Caption = #25147#12427
      ShortCut = 123
      OnExecute = actPF12Execute
    end
  end
end
