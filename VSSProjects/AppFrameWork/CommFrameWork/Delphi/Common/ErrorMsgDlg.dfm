object FrmErrorMsgDlg: TFrmErrorMsgDlg
  Left = 153
  Top = 415
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Message'
  ClientHeight = 127
  ClientWidth = 840
  Color = clBtnFace
  ParentFont = True
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sgMsg: TStringGrid
    Left = 0
    Top = 0
    Width = 840
    Height = 127
    Align = alClient
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 100
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = #65325#65331' '#12468#12471#12483#12463
    Font.Style = []
    GridLineWidth = 0
    Options = [goFixedVertLine, goFixedHorzLine]
    ParentFont = False
    TabOrder = 0
    OnDblClick = sgMsgDblClick
    OnKeyDown = sgMsgKeyDown
    OnMouseDown = sgMsgMouseDown
    ColWidths = (
      220
      600
      0
      0
      0)
  end
end
