unit INTERFACE_CONST;


interface
uses
  Classes,IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,IdIOHandler, IdIOHandlerSocket,
 IdSSLOpenSSL,IdFTP,IdMessage,IdSMTP,SysUtils,IdResourceStrings,windows;

const
	DEF_HTTP_RESPONSE 		= 'iBridFrameWorkHTTPResponse';
	DEF_FTP_RESPONSE 		= 'iBridFrameWorkFTPResponse';
	DEF_SMTP_RESPONSE 		= 'iBridFrameWorkSMTPResponse';

	HTTP_CONNECTION_TIMEOUT	= 180000;
	RESULT_INTERFACE_ERROR	= '@';

 	MAX_FTP_SESSION			= 20;

	ACT_FTP_PUT		= 1;
	ACT_FTP_GET		= 2;

	// gmbDOC Interface
	ID_SEARCH_SIP_ROUTE		= 8;	// SIP ROUTE�����v��

type

	TCustomIdFTP = class(TIdFTP)
		public
			FDestStream: TFileStream;
			procedure CustomGet(const ASourceFile, ADestFile: string; const ACanOverwrite: boolean = false;AResume: Boolean = false);
			procedure CustomPut(const ASourceFile: string; const ADestFile: string = '';const AAppend: boolean = false);
	end;


	THttpBasicInterface = class(TObject)
		ThreadID		: Integer;
		SSLFlag			: Boolean;
		PostFlag		: Boolean;
		ResultProcFlag	: Boolean;
		ErrorRetry		: Boolean;
		TimeOut			: Integer;
		SrvID			: Integer;
		RequestID		: Integer;
		URI				: String;
		RcvQuery		: String;
		TransQuery		: String;
		TransDate		: TDateTime;
		ErrorMsg		: String;
		Response		: String;
		WndMsgID		: Cardinal;
		WndHandle		: THandle;
		Status			: Integer;
		SSLHandler		: TIdSSLIOHandlerSocket;
		HttpClient		: TIdHTTP;
		EventSeq		: Cardinal;
		EventWaitHandle	: THandle;
		public
			constructor Create(var pEventSeq:Cardinal);
			destructor  Destroy;override;
	end;

	PHttpBasicInterface	= ^THttpBasicInterface;


	TFtpBasicInterface = class(TObject)
		ThreadID		: Integer;
		ThreadHandle	: Cardinal;
		Action			: SmallInt;
		IP				: String;
		UserID			: String;
		Password		: String;
		SrcPath			: String;
		SrcFileName		: String;
		DestPath		: String;
		StatusMsg		: String;
		StartTime		: TDateTime;
		EndTime			: TDateTime;
		WndMsgID		: Cardinal;
		WndHandle		: THandle;
		Status			: Integer;
		ResultProcFlag	: Boolean;
		Key				: String;
		ObjectType		: String;
		FTPClient 		: TCustomIdFTP;
		EventSeq		: Cardinal;
		EventWaitHandle	: THandle;
		public
			constructor Create(var pEventSeq:Cardinal);
			destructor  Destroy;override;
	end;

	PFtpBasicInterface	= ^TFtpBasicInterface;

	TSMTPBasicInterface = class(TObject)
		ThreadID		: Integer;
		SenderName		: String;
		FromMailAddr	: String;
		ToMailAddr		: String;
		Subject			: String;
		CharSet			: String;
		Doc				: TStringList;
		AttachmentFrom	: String;
		AttachmentNm	: String;
		SmtpAuthType	: Integer;		// 0: None Auth 1:Simple Login
		SmtpUserID		: String;
		SmtpPassword	: String;
		SmtpServerName	: String;
		WndMsgID		: Cardinal;
		WndHandle		: THandle;
		StartTime		: TDateTime;
		EndTime			: TDateTime;
		Status			: Integer;
		StatusMsg		: String;
		ResultProcFlag	: Boolean;
		Key				: String;
		MsgSend			: TIdMessage;
		SMTPClient		: TIdSMTP;
		EventSeq		: Cardinal;
		EventWaitHandle	: THandle;
		public
			constructor Create(var pEventSeq:Cardinal);
			destructor  Destroy;override;
	end;

	PMailBasicInterface	= ^TSMTPBasicInterface;


implementation


{*------------------------------------------------------------------*}
{* 	FTP Interface Class												*}
{*------------------------------------------------------------------*}
constructor TFtpBasicInterface.Create(var pEventSeq:Cardinal);
begin
	if (pEventSeq <> 0) then begin
		EventSeq		:= pEventSeq;
		EventWaitHandle := CreateEvent(Nil,FALSE,FALSE,PChar('IbridFtpSrvSeession' + IntToStr(EventSeq)));
		pEventSeq		:= pEventSeq + 1;
		if (pEventSeq >=  4294967294) then begin
			pEventSeq := 1;
		end;
	end else begin
		EventWaitHandle := 0;
	end;
end;

destructor TFtpBasicInterface.Destroy;
begin
	if (EventWaitHandle <> 0)then begin
		CloseHandle(EventWaitHandle);
	end;
end;

procedure TCustomIdFTP.CustomGet(const ASourceFile, ADestFile: string; const ACanOverwrite: boolean = false;AResume: Boolean = false);
begin
	if FileExists(ADestFile) then begin
	    AResume := AResume and CanResume;
		if ACanOverwrite and (not AResume) then begin
			FDestStream := TFileStream.Create(ADestFile, fmCreate);
		end else begin
			if (not ACanOverwrite) and AResume then begin
				FDestStream := TFileStream.Create(ADestFile, fmOpenReadWrite or fmShareDenyWrite);
				FDestStream.Seek(0, soFromEnd);
			end else begin
				raise EIdFTPFileAlreadyExists.Create(RSDestinationFileAlreadyExists);
			end;
		end;
	end else begin
		FDestStream := TFileStream.Create(ADestFile, fmCreate);
	end;

	try
		Get(ASourceFile, FDestStream, AResume);
	finally
		FreeAndNil(FDestStream);
	end;
end;

procedure TCustomIdFTP.CustomPut(const ASourceFile: string; const ADestFile: string = '';const AAppend: boolean = false);
begin
	FDestStream := TFileStream.Create(ASourceFile, fmOpenRead or fmShareDenyNone);
	try
		Put(FDestStream, ADestFile, AAppend);
	finally
		FreeAndNil(FDestStream);
	end;
end;


{*------------------------------------------------------------------*}
{* 	HTTP Interface Class											*}
{*------------------------------------------------------------------*}
constructor THttpBasicInterface.Create(var pEventSeq:Cardinal);
begin
	if (pEventSeq <> 0) then begin
		EventSeq		:= pEventSeq;
		EventWaitHandle := CreateEvent(Nil,FALSE,FALSE,PChar('IbridHttpSrvSeession' + IntToStr(EventSeq)));
		pEventSeq		:= pEventSeq + 1;
		if (pEventSeq >=  4294967294) then begin
			pEventSeq := 1;
		end;
	end else begin
		EventWaitHandle := 0;
	end;
end;

destructor THttpBasicInterface.Destroy;
begin
	if (EventWaitHandle <> 0)then begin
		CloseHandle(EventWaitHandle);
	end;
 	inherited Destroy;
end;

{*------------------------------------------------------------------*}
{* 	SMTP Interface Class											*}
{*------------------------------------------------------------------*}
constructor TSmtpBasicInterface.Create(var pEventSeq:Cardinal);
begin
	if (pEventSeq <> 0) then begin
		EventSeq		:= pEventSeq;
		EventWaitHandle := CreateEvent(Nil,FALSE,FALSE,PChar('IbridHttpSrvSeession' + IntToStr(EventSeq)));
		pEventSeq		:= pEventSeq + 1;
		if (pEventSeq >= 4294967294) then begin
			pEventSeq := 1;
		end;
	end else begin
		EventWaitHandle := 0;
	end;
end;

destructor TSmtpBasicInterface.Destroy;
begin
	if (EventWaitHandle <> 0)then begin
		CloseHandle(EventWaitHandle);
	end;
 	inherited Destroy;
end;

end.
