{*************************************************************************/
--	System			: Appilcation Frame Work
--  Sub System Name	: 共通
--	Title			: マスタ変更履歴
--	Progaram ID		: TFrmMasterMainteLog
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit MasterMainteLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BasicDBInputFrame, ActnList, ExtCtrls, Buttons, DB, OracleData,
  Grids, DBGrids, StdCtrls, IBridCheckBox, IBridEdit, MemoDateEdit,Oracle,
  MDIDBBasicInputFrame;

type
  TFrmMasterMainteLog = class(TFrmMDIDBBasicInput)
    pnlKey: TPanel;
    Panel34: TPanel;
    Label1: TLabel;
    Panel35: TPanel;
    edtUpdateDateFrom: TMemoDateEdit;
    edtUpdateDateTo: TMemoDateEdit;
    Panel1: TPanel;
    grdLog: TDBGrid;
    dsMainteLog: TDataSource;
    qryMainteLog: TOracleDataSet;
    qryMainteLogSEQ_NO: TFloatField;
    qryMainteLogUPDATE_DATE: TDateTimeField;
    qryMainteLogMACHINE_NM: TStringField;
    qryMainteLogIP_ADDR: TStringField;
    qryMainteLogUPDATE_DTL: TStringField;
	private
		FsSelSyntax	:String;
	public
		procedure	FreeIns;				override;
 		procedure	InitWin;				override;
		function	ValidateInput:boolean;	override;
		function	ExecUpdate:boolean;		override;
	end;

var
  FrmMasterMainteLog: TFrmMasterMainteLog;

implementation
uses AppUtils, CustomDataModule;

{$R *.dfm}
{*--------------------------------------------------------------*}
{*	Override Funtions											*}
{*--------------------------------------------------------------*}
procedure TFrmMasterMainteLog.FreeIns;
begin
	inherited;
	FrmMasterMainteLog := nil;
end;

procedure TFrmMasterMainteLog.InitWin;
begin
	inherited;
	FsSelSyntax := '';

	edtUpdateDateTo.Text	:= FormatDateTime('yyyy/mm/dd',Now);
	edtUpdateDateFrom.Text	:= FormatDateTime('yyyy/mm/dd',Now - 30);

	FsUpdateMsgID := '';
	CloseDataSet;

	edtUpdateDateFrom.SetFocus;
end;

function TFrmMasterMainteLog.ValidateInput:Boolean;
begin
	Result := True;

	if (edtUpdateDateFrom.Text <>'') or (edtUpdateDateTo.Text <>'') then begin
		if edtUpdateDateFrom.Text	= '' then edtUpdateDateFrom.Text:= edtUpdateDateTo.Text;
		if edtUpdateDateTo.Text		= '' then edtUpdateDateTo.Text 	:= edtUpdateDateFrom.Text;
	end;

	if Not Self.IsNotNull(edtUpdateDateFrom)	then Result := False;
	if Not Self.IsNotNull(edtUpdateDateTo)		then Result := False;

	if (Result) then begin
		if Not RangeCheckDate(edtUpdateDateFrom,edtUpdateDateTo) then Result := False;
	end;
end;

function TFrmMasterMainteLog.ExecUpdate:Boolean;
begin

	Result 			:= True;
	Screen.Cursor   := crSQLWait;

	with qryMainteLog do begin
		Close;
		DeleteVariables;

		with SQL do begin
			Clear;
			FsSelSyntax := '';

			Add('SELECT * FROM T_MAINTE_LOG');


			if (edtUpdateDateFrom.Text <>'') and (edtUpdateDateTo.Text <> '') then begin
				AddSQLAndCondition(FsSelSyntax,'UPDATE_DATE >= :wUPDATE_DATE_FROM AND UPDATE_DATE <= :wUPDATE_DATE_TO');
				DeclareVariable('wUPDATE_DATE_FROM'	, otDate);
				DeclareVariable('wUPDATE_DATE_TO'	, otDate);
				SetVariable('wUPDATE_DATE_FROM'		, StrToDateTime(edtUpdateDateFrom.Text	+' 00:00:00'));
				SetVariable('wUPDATE_DATE_TO'		, StrToDateTime(edtUpdateDateTo.Text	+' 23:59:59'));
			end;

			if Length(FsSelSyntax) <> 0 then begin
				Add(FsSelSyntax);
			end;
			Add('ORDER BY SEQ_NO DESC');

		end;

		Open;

		Screen.Cursor   :=  crDefault;
		grdLog.SetFocus;
	end;
end;

end.

