{*************************************************************************/
--	System			: iBrid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: 定数定義
--	Progaram ID		:
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit IBRID_APP_CONST;

interface
uses
	Windows,Graphics,SysUtils;

type
	EInternalError	= class(Exception);

Const
 //	LOCK_COLOR 		= $00F0F0F0;
 	LOCK_COLOR 		= clSilver;


{*----------------------------------*}
{* General Define					*}
{*----------------------------------*}
	RESULT_PROGRESS			= 1;
	RESULT_OK				= 0;
	RESULT_NG				= -1;
	RESULT_TIMEOUT			= -2;
	RESULT_INTERNAL_ERROR	= -9;

	STATUS_CONTINUE		= 0;
	STATUS_END			= 1;

	RETURN_NORMAL     	= 'I000';
	RETURN_UPDATE		= 'W002';

{*----------------------------------*}
{* Register Messages				*}
{*----------------------------------*}
	DEF_MSG_CONTROL_ERROR	= 'iBridAppControlError';
	DEF_MSG_CONTROL_NEXT	= 'iBridAppControlNext';
	DEF_MSG_CONTROL_PREV	= 'iBridAppControlPrev';

{*----------------------------------*}
{* ERROR Message ID					*}
{*----------------------------------*}
	ERR_NULL_VAL			= 'E001';
	ERR_NUM					= 'E002';
	ERR_CMB_VALUE			= 'E003';
	ERR_DATE_RELATION		= 'E004';
	ERR_MAX_VALUE			= 'E005';
	ERR_MIN_VALUE			= 'E006';
	ERR_FROM_TO				= 'E007';
	ERR_TIME				= 'E008';

	ERR_LENGTH				= 'E009';
	ERR_CURRENCY			= 'E010';
	ERR_DATE				= 'E011';
	ERR_IP_ADDR				= 'E012';
	ERR_DUP_KEY				= 'E013';
	ERR_TEL					= 'E014';


implementation

end.
















