{*************************************************************************/
--	System			: Appilcation Frame
--  Sub System Name	:
--	Title			: Mail Server Interface
--	Progaram ID		: MailSrvInterface
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit SMTPSrvInterface;

interface
uses
  Windows, Messages, SysUtils, Variants,Classes,INTERFACE_CONST,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,idSMTP, IdMessage,IniFiles;
type
	TSmtpThread= class(TThread)

	private
		FsMsg			:String;
		FoInterfaceInfo	:TSMTPBasicInterface;

		procedure SetEnv;
		procedure SetResponse;
		procedure SetErrorMsg;
		procedure FreeObj;

	Protected
	    procedure Execute;override;

	Public
		constructor Create(pExecInfo:TSMTPBasicInterface); virtual;
end;

implementation

uses  IBRID_APP_CONST, AppUtils;

constructor TSmtpThread.Create(pExecInfo:TSMTPBasicInterface);
begin
	FoInterfaceInfo	:=pExecInfo;

 	inherited Create(False);

end;

procedure TSmtpThread.Execute;
begin
	inherited;

	if Self.Terminated then begin
		Synchronize(FreeObj);
		Exit;
	end;

	with FoInterfaceInfo do begin
		Synchronize(SetEnv);
		try
			if Not(SMTPClient.Connected) then begin
				SMTPClient.Connect(10000);
				SMTPClient.Send(MsgSend);

				SMTPClient.Disconnect;
				Synchronize(SetResponse);
			end;
		except
			On CommError : Exception Do Begin
				FsMsg	:= CommError.Message;
				Synchronize(SetErrorMsg);
				try
					if (SMTPClient.Connected) then begin
						SMTPClient.Disconnect;
					end;
				except
				end;
			end;
		end;
	end;

	Synchronize(FreeObj);

	PostMessage(FoInterfaceInfo.WndHandle, FoInterfaceInfo.WndMsgID,0,Self.ThreadID);
end;

procedure TSmtpThread.SetEnv;
var
	oAttach :TIdAttachment;
begin
	with FoInterfaceInfo do begin
		MsgSend.Body.Assign(Doc);
		MsgSend.From.Text					:= FromMailAddr;
		MsgSend.ReplyTo.EMailAddresses		:= FromMailAddr;
//		MsgSend.ReceiptRecipient.Text		:= FromMailAddr;
		MsgSend.Recipients.EMailAddresses 	:= ToMailAddr;
		MsgSend.Subject						:= Subject;
		MsgSend.Priority 					:= TIdMessagePriority(2);
		MsgSend.CCList.EMailAddresses		:= '';
		MsgSend.BccList.EMailAddresses		:= '';
		MsgSend.CharSet						:= CharSet;


		MsgSend.Headers.Add('X-Mailer:iBrid Fa-Mail System') ;


		if (AttachmentFrom <> '') then begin
			oAttach := TIdAttachment.Create(MsgSend.MessageParts, AttachmentFrom);
			oAttach.FileName := AttachmentNm
		end;

		{authentication settings}
		case SmtpAuthType of
			0: SMTPClient.AuthenticationType := atNone;
			1: SMTPClient.AuthenticationType := atLogin; {Simple Login}
		end;
		SMTPClient.Username := SmtpUserID;
		SMTPClient.Password := SmtpPassword;

		{General setup}
		SMTPClient.Host := SmtpServerName;
		SMTPClient.Port := 25;

		StartTime		:= Now;
	end;
end;

procedure TSmtpThread.SetErrorMsg;
begin
	FoInterfaceInfo.StatusMsg	:= FsMsg;
	FoInterfaceInfo.Status		:= RESULT_NG;
	FoInterfaceInfo.EndTime		:= Now;
end;

procedure TSmtpThread.SetResponse;
begin
	FoInterfaceInfo.StatusMsg	:= '';
	FoInterfaceInfo.Status		:= RESULT_OK;
	FoInterfaceInfo.EndTime		:= Now;
end;

procedure TSmtpThread.FreeObj;
begin
	with FoInterfaceInfo do begin
		if (Assigned(SMTPClient)) then begin

			if (Assigned(Doc)) then begin
				Doc.Free;
			end;

			if (Assigned(MsgSend)) then begin
				MsgSend.Free;
			end;

			SMTPClient.Free;
		end;
	end;
end;

end.
