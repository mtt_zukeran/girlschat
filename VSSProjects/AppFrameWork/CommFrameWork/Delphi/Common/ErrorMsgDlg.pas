{*************************************************************************/
--	System			: Brid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: エラー表示ダイアログ
--	Progaram ID		: TFrmErrorMsgDlg
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit ErrorMsgDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, Grids;

type
  TFrmErrorMsgDlg = class(TForm)
    sgMsg: TStringGrid;
    procedure sgMsgDblClick(Sender: TObject);
    procedure sgMsgMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgMsgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private 宣言 }
	FhOwner					: HWND;
	FiXMouse				: integer;
	FiYMouse				: integer;
	FiMsgCount				: integer;

	IBRID_APP_CONTROL_ERROR	:Integer;

	function	GetCol		: Integer;
	function	GetRow		: Integer;
	function	GetDetail	: String;
	function	GetIsError	: Boolean;

  public
    { Public 宣言 }
	procedure	Init(pTop,pLeft:Integer);

	procedure	SetMsg		(pErrorStr: TStrings);
	procedure	SetHandle	(pHnadle : HWND);

	property	Col		: Integer	read GetCol;
	property	Row		: Integer	read GetRow;
	property	Detail	: String	read GetDetail;
	property	IsError : Boolean	read GetIsError;
  end;

var
  FrmErrorMsgDlg: TFrmErrorMsgDlg;

implementation

uses IBRID_APP_CONST;

{$R *.dfm}

{*--------------------------------------------------------------*}
{*	Window Action												*}
{*--------------------------------------------------------------*}
procedure TFrmErrorMsgDlg.FormCreate(Sender: TObject);
begin
	IBRID_APP_CONTROL_ERROR	:= RegisterWindowMessage(DEF_MSG_CONTROL_ERROR);
	FiMsgCount				:= 0;
end;


procedure TFrmErrorMsgDlg.sgMsgDblClick(Sender: TObject);
var
	ACol, ARow	: integer;
begin
	sgMsg.MouseToCell(FiXMouse, FiYMouse, ACol, ARow);

	if (sgMsg.Row = ARow) then begin
		PostMessage(FhOwner,IBRID_APP_CONTROL_ERROR,ARow,0);
	end;

	if (FiMsgCount = 1) then begin
		Self.Close;
	end;
end;

procedure TFrmErrorMsgDlg.sgMsgMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	FiXMouse := X;
	FiYMouse := Y;
end;

{*--------------------------------------------------------------*}
{*	Public Functions											*}
{*--------------------------------------------------------------*}
procedure TFrmErrorMsgDlg.Init(pTop,pLeft:Integer);
var
	iCol, iRow	: integer;
begin

	for iRow := sgMsg.FixedRows to sgMsg.RowCount - 1 do begin
		for iCol := sgMsg.FixedCols to sgMsg.ColCount - 1 do begin
			sgMsg.Cells[iCol, iRow] := '';
		end;
	end;
end;

procedure TFrmErrorMsgDlg.SetMsg(pErrorStr: TStrings);
var
	i	:Integer;
	iCnt:Integer;
	oStr:TStringList;
begin
	oStr := TStringList.Create;

	for i:= 0 to pErrorStr.Count-1 do begin

		oStr.Clear;
		oStr.Delimiter := ',';
		oStr.CommaText := pErrorStr[i];

		iCnt:= oStr.Count;

		if (iCnt >= 1) then sgMsg.Cells[0, i+1]	:= oStr[0];	// Title
		if (iCnt >= 2) then sgMsg.Cells[1, i+1]	:= oStr[1];	// Msg
		if (iCnt >= 3) then sgMsg.Cells[2, i+1]	:= oStr[2];	// Control Name
		if (iCnt >= 4) then sgMsg.Cells[3, i+1]	:= oStr[3];	// Grid時 x
		if (iCnt >= 5) then sgMsg.Cells[4, i+1]	:= oStr[4];	// Grid時 y
	end;

	FiMsgCount := pErrorStr.Count;

	oStr.Free;

end;


function TFrmErrorMsgDlg.GetCol: Integer;
begin
	if sgMsg.Cells[0, sgMsg.Row] <> '' then begin
		Result := StrToInt(sgMsg.Cells[0, sgMsg.Row]) - 1;
	end else begin
		Result := -1;
	end;
end;

function TFrmErrorMsgDlg.GetRow: Integer;
begin
	if sgMsg.Cells[1, sgMsg.Row] <> '' then begin
		Result := StrToInt(sgMsg.Cells[1, sgMsg.Row]);
	end else begin
		Result := -1;
	end;
end;

function TFrmErrorMsgDlg.GetDetail: String;
begin
	Result := sgMsg.Cells[2, sgMsg.Row];
end;

function TFrmErrorMsgDlg.GetIsError: Boolean;
begin
	Result := sgMsg.Cells[2, 1] <> '';
end;

procedure TFrmErrorMsgDlg.SetHandle(pHnadle : HWND);
begin
	FhOwner := pHnadle;
end;


procedure TFrmErrorMsgDlg.FormShow(Sender: TObject);
begin
	sgMsg.Cells[0, 0]	:= '項目';
	sgMsg.Cells[1, 0]	:= 'エラー内容';
end;

procedure TFrmErrorMsgDlg.sgMsgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	inherited;
	case (Key) of
		VK_RETURN,
		VK_ESCAPE:begin
			if (sgMsg.Cells[2,sgMsg.Row] <> '') then begin
				PostMessage(FhOwner,IBRID_APP_CONTROL_ERROR,sgMsg.Row,0);
			end;
			Key := 0;
			Self.Close;
		end;
	end;
end;

end.
