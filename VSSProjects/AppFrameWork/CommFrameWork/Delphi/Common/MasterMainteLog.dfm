inherited FrmMasterMainteLog: TFrmMasterMainteLog
  Left = 405
  Top = 161
  Caption = #12510#12473#12479#22793#26356#23653#27508
  PixelsPerInch = 96
  TextHeight = 12
  inherited pnlTitleFarme: TPanel
    inherited pnlTitle: TPanel
      Caption = #12510#12473#12479#22793#26356#23653#27508
    end
  end
  inherited pnlMain: TPanel
    inherited pnlDtl: TPanel
      object pnlKey: TPanel
        Left = 1
        Top = 1
        Width = 1010
        Height = 24
        Align = alTop
        TabOrder = 0
        object Panel34: TPanel
          Left = 1
          Top = 1
          Width = 1008
          Height = 22
          Align = alClient
          Alignment = taLeftJustify
          Caption = ' '#26356#26032#26085
          TabOrder = 0
          object Label1: TLabel
            Left = 152
            Top = 5
            Width = 12
            Height = 12
            Caption = #65374
          end
          object Panel35: TPanel
            Left = 58
            Top = 3
            Width = 3
            Height = 16
            TabOrder = 0
          end
          object edtUpdateDateFrom: TMemoDateEdit
            Left = 66
            Top = 1
            Width = 80
            Height = 20
            ImeMode = imClose
            Lines.Strings = (
              'edtLastUse')
            MaxLength = 10
            TabOrder = 1
            WantReturns = False
            WordWrap = False
            Text = 'edtLastUse'
            EnterFocusMove = True
            Title = #26356#26032#26085'From'
          end
          object edtUpdateDateTo: TMemoDateEdit
            Left = 170
            Top = 1
            Width = 80
            Height = 20
            ImeMode = imClose
            Lines.Strings = (
              'edtLastUse')
            MaxLength = 10
            TabOrder = 2
            WantReturns = False
            WordWrap = False
            Text = 'edtLastUse'
            EnterFocusMove = True
            Title = #26356#26032#26085'To'
          end
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 25
        Width = 1010
        Height = 619
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        object grdLog: TDBGrid
          Left = 2
          Top = 2
          Width = 1006
          Height = 615
          Align = alClient
          DataSource = dsMainteLog
          ImeMode = imClose
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = SHIFTJIS_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #65325#65331' '#65328#12468#12471#12483#12463
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'UPDATE_DATE'
              Title.Alignment = taCenter
              Title.Caption = #26356#26032#26085#26178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP_ADDR'
              Title.Alignment = taCenter
              Title.Caption = #26356#26032#20803'IP'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MACHINE_NM'
              Title.Alignment = taCenter
              Title.Caption = #26356#26032#20803#65423#65404#65437#21517
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UPDATE_DTL'
              Title.Alignment = taCenter
              Title.Caption = #26356#26032#20869#23481
              Width = 2000
              Visible = True
            end>
        end
      end
    end
  end
  inherited alBase: TActionList
    inherited actPF5: TAction
      Caption = #29031#20250
      OnUpdate = nil
    end
    inherited actPF7: TAction
      Caption = ''
      OnExecute = nil
      OnUpdate = nil
    end
  end
  object dsMainteLog: TDataSource
    DataSet = qryMainteLog
    Left = 81
    Top = 257
  end
  object qryMainteLog: TOracleDataSet
    SQL.Strings = (
      'SELECT * FROM T_MAINTE_LOG')
    QBEDefinition.QBEFieldDefs = {
      0400000005000000060000005345515F4E4F0100000000000B00000055504441
      54455F444154450100000000000A0000004D414348494E455F4E4D0100000000
      000700000049505F414444520100000000000A0000005550444154455F44544C
      010000000000}
    Session = CustomDB.DataBase
    Left = 111
    Top = 259
    object qryMainteLogSEQ_NO: TFloatField
      FieldName = 'SEQ_NO'
      Required = True
    end
    object qryMainteLogUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      DisplayFormat = 'yyyy/mm/dd hh:nn:ss'
    end
    object qryMainteLogMACHINE_NM: TStringField
      FieldName = 'MACHINE_NM'
      Size = 64
    end
    object qryMainteLogIP_ADDR: TStringField
      FieldName = 'IP_ADDR'
      Size = 15
    end
    object qryMainteLogUPDATE_DTL: TStringField
      FieldName = 'UPDATE_DTL'
      Size = 512
    end
  end
end
