object AppData: TAppData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 536
  Top = 553
  Height = 235
  Width = 372
  object DataBase: TOracleSession
    BeforeLogOn = DataBaseBeforeLogOn
    BytesPerCharacter = bcAutoDetect
    Left = 48
    Top = 16
  end
  object qryError: TOracleQuery
    SQL.Strings = (
      'SELECT * FROM T_ERROR WHERE ERROR_CD = :wERROR_CD')
    Session = DataBase
    Optimize = False
    Variables.Data = {
      03000000010000000A0000003A574552524F525F434405000000000000000000
      0000}
    Left = 120
    Top = 12
  end
  object qryGeneral: TOracleQuery
    Session = DataBase
    Optimize = False
    Left = 184
    Top = 12
  end
  object GET_TEXT_PROC_NO: TOracleQuery
    SQL.Strings = (
      'BEGIN'
      '  GET_TEXT_PROC_NO(:PTEXT_PROC_NO,:PSTATUS);'
      'END;'
      ''
      '')
    Session = DataBase
    Optimize = False
    Variables.Data = {
      03000000020000000E0000003A50544558545F50524F435F4E4F040000000000
      000000000000080000003A50535441545553050000000000000000000000}
    Left = 46
    Top = 74
  end
  object qryOutputText: TOracleDataSet
    SQL.Strings = (
      'SELECT * FROM T_TEXT'
      'WHERE'
      ' SEQ_NO = :wSEQ_NO'
      'ORDER BY'
      ' SEQ_NO,TEXT_LINE_NO')
    Optimize = False
    Variables.Data = {
      0300000001000000080000003A575345515F4E4F040000000000000000000000}
    Session = DataBase
    Left = 52
    Top = 134
  end
  object qryInputFile: TOracleQuery
    SQL.Strings = (
      'INSERT INTO T_TEXT('
      ' SEQ_NO,'
      ' TEXT_LINE_NO ,'
      ' TEXT_DTL'
      ')VALUES('
      ' :wSEQ_NO ,'
      ' :wTEXT_LINE_NO   ,'
      ' :wTEXT_DTL'
      ')')
    Session = DataBase
    Optimize = False
    Variables.Data = {
      03000000030000000E0000003A57544558545F4C494E455F4E4F040000000000
      000000000000080000003A575345515F4E4F0400000000000000000000000A00
      00003A57544558545F44544C050000000000000000000000}
    Left = 138
    Top = 134
  end
  object pkgFunction: TOraclePackage
    Session = DataBase
    Optimize = False
    Left = 257
    Top = 11
  end
end
