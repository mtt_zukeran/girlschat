{*************************************************************************/
--	System			: iBrid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: 入力基本フレーム
--	Progaram ID		: TFrmBasicInput
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit BasicInputFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BasicFrame, ActnList, ExtCtrls, Buttons,ErrorMsgDlg,QryComboBox,IBridEdit,iBridNumEdit,
  iBridRadioButton,IBridCheckBox,Grids,IBridGridBase, ITBGrid,StdCtrls,DBCtrls,FileCtrl;

type
  TFrmBasicInput = class(TFrmBasic)
    pnlDtl: TPanel;
    procedure actPF5Execute(Sender: TObject);
    procedure actPF5Update(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
	procedure   DataOnChange(Sender: TObject);
    procedure actPF11Execute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actPF6Execute(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
	private
		FbAbortFlag		: Boolean;
		FoErrorStr		: TStrings;
		FoErrorDlg		: TFrmErrorMsgDlg;
		FbErrorFocusFlg	: Boolean;


		function	IsNotNullQryCombo	(pQueyCmb	: TQryComboBox	): Boolean;
		function	IsNotNullMemo		(pMemo		: TIBridEdit	): Boolean;
		function	IsNaturalNumber		(pNumber: String): boolean;


	protected
		FsUpdateMsgID	:String;

		Function 	CheckUpdateStatus:Word;
		procedure	CtlError				(pErrorCd:String; pWinControl:TWinControl);
		function	CtlErrorMain			(pErrorCd:String; pWinControl:TWinControl): Boolean;
		procedure	DispErrorMsg			(pErrorCd:String);
		function	DispWarningMsg			(pErrorCd:String): Boolean;
		function	DispConfirmationMsg		(pErrorCd:String): Boolean;
		function	GetErrorMsg				(pErrorCd:String): String;
		procedure 	SetErrorStr				(pControlNm,pTitle,pMsg:String;pX:Integer=-1;pY:Integer=-1);
		procedure	SetErrorMsgToDlg		(pErrorStr: TStrings);
		function 	SetErrorField			(pCtl: TWinControl;pErroMsg:String):Boolean;

		function	IsNotNull				(pWinControl: TWinControl	): Boolean;
		function 	ValidateNum				(pEdit		: TIBridEdit	): boolean;
		function	ValidateQueryCombo		(pQueryCmb	: TQryComboBox	): Boolean;
		function 	ValidateDay				(pEdit		: TIBridEdit	): boolean;
		function 	ValidateTime			(pEdit		: TIBridEdit	): boolean;
		function 	ValidateTel				(pEdit		: TIBridEdit	): boolean;
		function 	ValidateIPAddr			(pEdit		: TIBridEdit	): boolean;
		function 	ValidateGridData		(pGrid		: TITBGrid		): boolean;
		function	RangeCheckDate			(pFromDate,pToDate:TIBridEdit): boolean;
 		function	RangeCheckNum			(pEdit		: TIBridEdit;pMin,pMax:Integer): boolean;
 		function	RangeCheckFromTo		(pEditFrom,pEditTo: TIBridEdit): boolean;

		procedure 	SetComboCaption			(pQueryCmb: TQryComboBox);
		procedure 	SetDefaultPickList		(pGird:		TITBGrid;ACol: integer);

		Function 	UpdateControl:Boolean;
	public
		procedure	InitWin;								override;
		function	ValidateInput:boolean;					virtual;abstract;
		function	ExecUpdate:boolean;	virtual;abstract;
		procedure	ShowErrorMsgDlg;
		procedure	WndProc	(var Msg:TMessage);override;

  end;

var
  FrmBasicInput: TFrmBasicInput;

implementation

uses IBRID_APP_CONST, CustomDataModule, AppUtils, StrLib;

{$R *.dfm}

{*--------------------------------------------------------------*}
{*	Override Funtions											*}
{*--------------------------------------------------------------*}
procedure TFrmBasicInput.WndProc(var Msg:TMessage);
var
	sControlNm	:String;
	oTemp		:TComponent;
	i			:Integer;
	x,y			:Integer;
	sName		:String;
begin
	if (Msg.Msg = UINT(IBRID_APP_CONTROL_ERROR)) then begin

		sControlNm	:= FoErrorDlg.sgMsg.Cells[2,Msg.WParam];
		sName		:= Self.ActiveControl.Name;

		with Self do begin
			for i := ComponentCount - 1 downto 0 do begin
				oTemp := Components[I];
				if (oTemp is TWinControl) then begin
					if TWinControl(oTemp).Name = sControlNm Then begin

						if (oTemp is TITBGrid) then begin
							x:= StrToIntDef(FoErrorDlg.sgMsg.Cells[3,Msg.WParam],-1);
							y:= StrToIntDef(FoErrorDlg.sgMsg.Cells[4,Msg.WParam],-1);
							if (x <> -1) and (y <> -1) then begin
								TITBGrid(oTemp).HiLightCell(x, y);
							end;
						end;

						if (sName <> sControlNm) then begin
							TWinControl(oTemp).SetFocus;
							exit;
						end else begin
							windows.SetFocus(TWinControl(oTemp).Handle);
							exit;
						end;
					end;
				end;
			end;
		end;
		Exit;
	end;

	inherited;
end;


procedure TFrmBasicInput.InitWin;
begin
	inherited;
	Self.ClearColor(Self);
	Self.ClearText(Self);

	FbModified := False;

	if (FoErrorDlg <> nil) then begin
		FoErrorDlg.Close;
		FoErrorDlg := Nil;
	end;

	FsUpdateMsgID := 'C101';

end;

{*--------------------------------------------------------------*}
{*	Window Action												*}
{*--------------------------------------------------------------*}
procedure TFrmBasicInput.FormCreate(Sender: TObject);
begin
	inherited;
	FoErrorStr	:= TStringList.Create;
	FbModified	:= False;
end;

procedure TFrmBasicInput.FormDestroy(Sender: TObject);
begin
	FoErrorStr.Free;
	if Assigned(FoErrorDlg) then begin
		FoErrorDlg.Free;
	end;
	inherited;
end;

procedure TFrmBasicInput.FormShow(Sender: TObject);
var
	oTemp	:TComponent;
	i		:Integer;
begin
	inherited;

	InitWin;

	with Self do begin
		for i := ComponentCount - 1 downto 0 do begin
            if Components[I] is TQryComboBox then begin
            	oTemp := Components[i];
				TQryComboBox(oTemp).SetItem;
			end;
        end;
    end;
end;

procedure TFrmBasicInput.actPF5Execute(Sender: TObject);
begin
	inherited;
	if (FsUpdateMsgID <> '') then begin
		if (MessageDlg(GetErrorMsg(FsUpdateMsgID), mtConfirmation,[mbNo,mbYes], 0) = mrNo) then begin
			Exit;
		end;
	end;
	UpdateControl;
end;

procedure TFrmBasicInput.actPF5Update(Sender: TObject);
begin
	inherited;
	actPF5.Enabled := Self.FbModified;
end;


procedure TFrmBasicInput.actPF11Execute(Sender: TObject);
begin
//	inherited;
//  InitWin;
end;

procedure TFrmBasicInput.actPF6Execute(Sender: TObject);
begin
	inherited;
    InitWin;
end;

procedure TFrmBasicInput.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	inherited;
	CanClose := True;

	if CheckUpdateStatus = mrYES then begin
		if (UpdateControl = False) then begin
			CanClose := False;
		end
	end;
end;


{*--------------------------------------------------------------*}
{*	Private Functions											*}
{*--------------------------------------------------------------*}
Function TFrmBasicInput.CheckUpdateStatus:Word;
var
   wrdBtn      :Word;
begin
	wrdBtn := mrNO;

	if FbModified then begin
		wrdBtn := MessageDlg(GetErrorMsg('C104'), mtConfirmation,[mbNo,mbYes,mbCancel], 0);
	end;

	if wrdBtn = mrCancel then begin
		SysUtils.Abort;
	end;

	Result := wrdBtn;
end;

procedure TFrmBasicInput.DataOnChange(Sender: TObject);
begin
	if (False = FbModified)  then begin
		FbModified := True;
	end;
end;

Function TFrmBasicInput.UpdateControl:Boolean;
begin
	Result := False;

	FoErrorStr.Clear;

	Screen.Cursor := crHourGlass;

	if (ValidateInput) then begin
		if (FoErrorDlg <> nil) then begin
			FoErrorDlg.Close;
		end;

		ClearColor(Self);

		if (ExecUpdate) then begin
			Result		:= True;
			FbModified	:= False;
		end;
	end else begin
		ShowErrorMsgDlg;
	end;

	Screen.Cursor := crDefault;
end;

procedure TFrmBasicInput.ShowErrorMsgDlg;
begin
	Screen.Cursor := crDefault;

	if FoErrorStr.Count > 0 then begin
		if FoErrorDlg = nil then begin
			FoErrorDlg := TFrmErrorMsgDlg.Create(Self);
		end;
		FoErrorDlg.Init(500,10);
		SetErrorMsgToDlg(FoErrorStr);
		FoErrorDlg.SetHandle(Self.Handle);
		FoErrorDlg.Show;
	end;
end;


{*--------------------------------------------------------------*}
{*	Error Control Functions										*}
{*--------------------------------------------------------------*}
// Control Disp Error & Abort
procedure TFrmBasicInput.CtlError(pErrorCd:String; pWinControl:TWinControl);
begin
	CtlErrorMain(pErrorCd,pWinControl);
	Screen.Cursor := crDefault;
	if pErrorCd <> RETURN_NORMAL then begin
		if FbAbortFlag then SysUtils.Abort;
	end;
end;

// Control Error Base
function TFrmBasicInput.CtlErrorMain(pErrorCd:String; pWinControl:TWinControl): Boolean;
begin
	Result := True;

	ClearColor(Self);

	if pErrorCd = RETURN_NORMAL then begin
		Exit;
	end;

	if pWinControl <> Nil Then SetColor(pWinControl,FiColorError);

	if (Copy(pErrorCd,1,1) = 'E') then begin
		DispErrorMsg(pErrorCd);
	end else if (Copy(pErrorCd,1,1) = 'W') then begin
		Result := DispWarningMsg(pErrorCd);
	end else if (Copy(pErrorCd,1,1) = 'C') then begin
		Result := DispConfirmationMsg(pErrorCd);
	end;

	if pWinControl <> Nil Then begin
		pWinControl.SetFocus;
	end;
end;

// Diplay Error
procedure TFrmBasicInput.DispErrorMsg(pErrorCd:String);
begin
	// Get Error Msg
	FbAbortFlag := True;

	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		if Not(Eof) Then begin
			MessageDlg(Field('ERROR_DTL'),mtError	, [mbOK], 0);
		end else begin
			MessageDlg( 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd ,mtWarning	, [mbOK], 0);
		end;
	end;
end;

// Diplay Warning
function TFrmBasicInput.DispWarningMsg(pErrorCd:String): Boolean;
begin
	// Get Warning Msg
	Result := True;
	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		if Not(Eof) Then begin
			if MessageDlg(Field('ERROR_DTL'),mtWarning	, [mbOK], 0) = mrYes then begin
				Result := True;
				FbAbortFlag := False;
			end else begin
				Result := False;
				FbAbortFlag := True;
			end;
		end else begin
			MessageDlg( 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd ,mtWarning	, [mbOK], 0);
			FbAbortFlag := True;
		end;
	end;
end;

// Diplay Confirmation
function TFrmBasicInput.DispConfirmationMsg(pErrorCd: String): Boolean;
begin
	// Get Warning Msg
	Result := True;
	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		if Not(Eof) Then begin
			if MessageDlg(Field('ERROR_DTL'),mtConfirmation	, [mbOK,mbCancel], 0) = mrYes then begin
				Result 		:= True;
				FbAbortFlag := False;
			end else begin
				Result 		:= False;
				FbAbortFlag := True;
			end;
		end else begin
			MessageDlg( 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd ,mtWarning	, [mbOK], 0);
			FbAbortFlag := True;
		end;
	end;
end;

procedure TFrmBasicInput.SetErrorStr(pControlNm,pTitle,pMsg:String;pX:Integer=-1;pY:Integer=-1);
begin
	if (pX <> -1) and (pY <> -1) then begin
		FoErrorStr.Add('"'+ pTitle +'","' + pMsg +'",' + pControlNm + ',' + IntToStr(pX)+','+IntToStr(pY)+',');
	end else begin
		FoErrorStr.Add('"'+ pTitle +'","' + pMsg +'",' + pControlNm + ',' + ',,');
	end;
end;

function TFrmBasicInput.GetErrorMsg(pErrorCd: String): String;
begin
	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		IF Not(Eof) Then begin
			Result := Trim(Field('ERROR_DTL'));
		end else begin
			Result := 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd;
		end;
	end;
end;

procedure TFrmBasicInput.SetErrorMsgToDlg(pErrorStr: TStrings);
begin
	FoErrorDlg.SetMsg(pErrorStr);
end;


function TFrmBasicInput.SetErrorField(pCtl: TWinControl;pErroMsg:String):Boolean;
begin
	Result := False;


	if pCtl is TiBridEdit then begin
		SetErrorStr(pCtl.Name,TiBridEdit(pCtl).Title,pErroMsg);
		TiBridEdit(pCtl).Color		:= FiColorError;
	end;

	if pCtl is TiBridRadioButton then begin
		SetErrorStr(pCtl.Name,TiBridRadioButton(pCtl).Title,pErroMsg);
		TiBridRadioButton(pCtl).Color	:= FiColorError;
	end;

	if pCtl is TQryComboBox then begin
		SetErrorStr(pCtl.Name,TQryComboBox(pCtl).Title,pErroMsg);
		TQryComboBox(pCtl).Color	:= FiColorError;
	end;

	if pCtl is TIBridCheckBox then begin
		SetErrorStr(pCtl.Name,TiBridCheckBox(pCtl).Title,pErroMsg);
		TiBridCheckBox(pCtl).Color	:= FiColorError;
	end;

	if not FbErrorFocusFlg then begin
		FbErrorFocusFlg := true;
		pCtl.SetFocus;
	end;

end;

{*--------------------------------------------------------------*}
{*	Validate Input Values										*}
{*--------------------------------------------------------------*}
function TFrmBasicInput.IsNotNull(pWinControl: TWinControl): Boolean;
begin
	Result := False;

	if (pWinControl is TiBridEdit) then begin
		Result := IsNotNullMemo(TiBridEdit(pWinControl));
		Exit;
	end;

	if (pWinControl is TQryComboBox) then begin
		Result := IsNotNullQryCombo(TQryComboBox(pWinControl));
		Exit;
	end;

	MessageDlg( 'IsNotNull Not Define Type' ,mtWarning	, [mbOK], 0);
end;

function TFrmBasicInput.IsNotNullMemo(pMemo: TiBridEdit): Boolean;
begin
	Result := True;

	if (Trim(pMemo.Text) = '') then begin
		Result := False;
		SetErrorStr(pMemo.Name,pMemo.Title,GetErrorMsg(ERR_NULL_VAL));
		pMemo.Color := FiColorError;
		if not FbErrorFocusFlg then begin
			FbErrorFocusFlg := true;
			pMemo.SetFocus;
		end;
	end;
end;


function TFrmBasicInput.IsNotNullQryCombo(pQueyCmb: TQryComboBox): Boolean;
begin
	Result := True;
	if pQueyCmb.Text = '' then begin
		Result := false;
		SetErrorStr(pQueyCmb.Name,pQueyCmb.Title,GetErrorMsg(ERR_NULL_VAL));
		pQueyCmb.Color := FiColorError;
		if not FbErrorFocusFlg then begin
			FbErrorFocusFlg := true;
			pQueyCmb.SetFocus;
		end;
	end;
end;



function TFrmBasicInput.ValidateNum(pEdit: TIBridEdit): boolean;
begin
	Result := True;
	pEdit.Color := clWindow;
	try
		if False = IsNaturalNumber(pEdit.Text) then begin
			SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_NUM));
			raise Exception.Create(GetErrorMsg(ERR_NUM));
		end;
	except
		pEdit.Color := FiColorError;
		if not FbErrorFocusFlg then begin
			pEdit.SetFocus;
			FbErrorFocusFlg := True;
		end;
		Result := False;
	end;
end;


function TFrmBasicInput.IsNaturalNumber(pNumber: String): boolean;
var
	iNum: Integer;
begin
	result := True;
	try
		if pNumber <> '' then begin
			iNum := StrToInt(pNumber);
			if (0 > iNum) then begin
				Result := False;
			end;
		end;
	except
		Result := False;
    end;
end;

function TFrmBasicInput.ValidateQueryCombo(pQueryCmb	: TQryComboBox): Boolean;
begin
	Result := True;
	if not pQueryCmb.CheckKey then begin
		SetErrorStr(pQueryCmb.Name,pQueryCmb.Title,GetErrorMsg(ERR_CMB_VALUE));

		pQueryCmb.Color := FiColorError;
		Result := False;
	end;
end;

function TFrmBasicInput.ValidateTime(pEdit: TIBridEdit): Boolean;
var
	dDate :TDateTime;
	sDate :String;
begin
	Result	:= True;
	sDate	:= pEdit.Text;

	if (Length(sDate) <> 5) and (Length(sDate) <> 8) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_TIME));
		pEdit.Color := FiColorError;
		Result := False;
		Exit;
	end;

	if (Length(sDate) = 5) then begin
		sDate := sDate + ':00';
	end;
	dDate	:= StrToTimeDef(sDate,0);

	if (dDate = 0) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_TIME));

		pEdit.Color := FiColorError;
		Result := False;
	end;
end;

function TFrmBasicInput.ValidateDay(pEdit: TIBridEdit): Boolean;
var
	dDate :TDateTime;
	sDate :String;
begin
	Result	:= True;
	sDate	:= pEdit.Text;

	if (Length(sDate) <> 10) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_DATE));
		pEdit.Color := FiColorError;
		Result := False;
		Exit;
	end;

	dDate	:= StrToDateDef(sDate,0);

	if (dDate = 0) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_DATE));

		pEdit.Color := FiColorError;
		Result := False;
	end;
end;

function TFrmBasicInput.ValidateTel(pEdit: TIBridEdit): Boolean;
var
	sTel:String;
begin
	Result	:= True;
	sTel	:= pEdit.Text;
	if (sTel = '') Then exit;

	if (Copy(sTel,1,1) <> '0') then begin
		Result := False;
	end;

	if (Copy(sTel,1,3) = '090') or (Copy(sTel,1,3) = '080') or (Copy(sTel,1,3) = '070') or (Copy(sTel,1,3) = '050') then begin
		if (Length(sTel) <> 11) then begin
			Result := False;
		end;
	end else begin
		if (Length(sTel) <> 10) then begin
			Result := False;
		end;
	end;

	if (Result) then begin
		Result := StrIsNumOnly(sTel);
	end;


	if (Result = False) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_TEL));
		pEdit.Color := FiColorError;
		Result := False;
	end;
end;

function TFrmBasicInput.ValidateIPAddr(pEdit: TIBridEdit): Boolean;
var
	sIPList	: TStrings;
	i		: integer;
begin
	Result := True;

	if pEdit.Text = '' then Exit;

	sIPList	:= TStringList.Create;
	sIPList.Delimiter := '.';
	sIPList.DelimitedText := pEdit.Text;

	if sIPList.Count <> 4 then begin
		Result := False;
	end;

	if (Result = True) then begin
		for i := 0 To sIPList.Count - 1 do begin
			if not (StrToInt(sIPList[i]) >= 0) or not (StrToInt(sIPList[i]) <= 255) then begin
				Result := False;
			end;
		end;
	end;

	sIPList.Free;

	if (Result = False) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_IP_ADDR));
		pEdit.Color := FiColorError;
	end;
end;

function TFrmBasicInput.ValidateGridData(pGrid:TITBGrid): boolean;
var
	oGridErr	:TStrings;
	oStr		:TStrings;
	i			:Integer;
begin
	pGrid.CheckGridData;
	oGridErr := TStringList.Create;
	oGridErr.Assign(pGrid.GetErrorStrings);

	if (pGrid.GetErrorStrings.Count = 0) then begin
		Result := True;
	end else begin
		Result := False;

		oStr := TStringList.Create;
		for i := 0 to oGridErr.Count - 1 do begin
			oStr.Clear;
			oStr.Delimiter := ',';
			oStr.CommaText := oGridErr[i];

			SetErrorStr(
				pGrid.Name	,
				oStr[3]		,
				GetErrorMsg(oStr[2]),		// Msg
				StrToInt(oStr[0])	,		// x
				StrToInt(oStr[1])			// y
			);
		end;

		oStr.Free;
	end;
end;

function TFrmBasicInput.RangeCheckDate(pFromDate,pToDate:TIBridEdit): boolean;
var
	dFromDate	: TDateTime;
	dToDate		: TDateTime;
begin
	Result := True;
	try

		dFromDate	:= StrToDateTimeDef(pFromDate.Text,0);
		dToDate 	:= StrToDateTimeDef(pToDate.Text,0);

		if (dToDate < dFromDate) or ((dFromDate=0) or (dToDate=0)) then begin
			Result := False;
			SetErrorStr(pFromDate.Name,pFromDate.Title,GetErrorMsg(ERR_DATE_RELATION));

			pFromDate.Color	:= FiColorError;
			pToDate.Color	:= FiColorError;

			if not FbErrorFocusFlg then begin
				FbErrorFocusFlg := true;
				pFromDate.SetFocus;
			end;

		end;
	except
		Result := False;
    end;
end;

function TFrmBasicInput.RangeCheckNum(pEdit: TIBridEdit;pMin,pMax:Integer): boolean;
var
	iValue:Integer;
begin
	Result := True;
	iValue := StrToIntDef(pEdit.Text,0);
	if (iValue > pMax) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_MAX_VALUE) + Format(' %d以下にして下さい。',[pMax]));
		pEdit.Color := FiColorError;
		Result := False;
	end;

	if (iValue < pMin) then begin
		SetErrorStr(pEdit.Name,pEdit.Title,GetErrorMsg(ERR_MIN_VALUE) + Format(' %d以上にして下さい。',[pMin]));
		pEdit.Color := FiColorError;
		Result := False;
	end;
end;

function TFrmBasicInput.RangeCheckFromTo(pEditFrom,pEditTo: TIBridEdit): boolean;
var
	iFrom	:Integer;
	iTo		:Integer;
begin
	iFrom	:= StrToIntDef(pEditFrom.Text,0);
	iTo		:= StrToIntDef(pEditTo.Text,0);

	Result := True;

	if (iFrom > iTo) then begin
		SetErrorStr(pEditFrom.Name,pEditFrom.Title,GetErrorMsg(ERR_FROM_TO));
		pEditFrom.Color := FiColorError;
		Result := False;
	end;
end;




procedure TFrmBasicInput.SetComboCaption(pQueryCmb: TQryComboBox);
var
	list	: TStrings;
	iLoop	: integer;
begin
	try
		if (pQueryCmb.Text <> '') and (pQueryCmb.Items.Count > 0) then begin
			list := TStringList.Create;
			try
				for iLoop := 0 to pQueryCmb.Items.Count - 1 do begin
					list.Add(DelAfterSpace(pQueryCmb.Items[iLoop]));
				end;
				if list.IndexOf(DelAfterSpace(pQueryCmb.Text)) <> - 1 then begin
					pQueryCmb.ItemIndex	:= list.IndexOf(DelAfterSpace(pQueryCmb.Text));
					// ItemIndexの設定だけではABBRが表示されないことがある。
					pQueryCmb.Text    := pQueryCmb.Items[pQueryCmb.ItemIndex];
				end;
			finally
				list.Free;
			end;
		end;
	except
	end;
end;

procedure TFrmBasicInput.SetDefaultPickList(pGird:TITBGrid;ACol: integer);
var
	i	: integer;
begin
	with pGird do begin
		SetPickList(ACol, FixedRows);
		for i := FixedRows To RowCount - 1 do begin
			if Cells[ACol, i] <> '' then begin
				if TITBColParam(ColParams[ACol]).PickList.Count > 0 then begin
					if TITBColParam(ColParams[ACol]).CodeList.IndexOf(DelAfterSpace(Cells[ACol, i])) <> - 1 then begin
						Cells[ACol, i] := TITBColParam(ColParams[ACol]).PickList[TITBColParam(ColParams[ACol]).CodeList.IndexOf(DelAfterSpace(Cells[ACol, i]))];
					end;
				end;
			end;
		end;
	end;
end;


procedure TFrmBasicInput.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	case Key of
		VK_UP:begin
			if 	(Not(Activecontrol is TCustomGrid))			and
				(Not(Activecontrol is TCheckBox	 ))			and
				(Not(Activecontrol is TDBCheckBox))			and
				(Not(Activecontrol is TButtonControl))		and
				(Not(Activecontrol is TComboBox)  )			and
				(Not(Activecontrol is TDirectoryListBox))	and
				(Not(Activecontrol is TBitBtn)) 			and
				(Not(Activecontrol is TDBComboBox)) Then begin
				SelectNext(Activecontrol,False,True);
				Key := 0;
			end else begin
				Key := 0;
			end;
		end;

		VK_DOWN:begin
			if 	(Not(Activecontrol is TCustomGrid))			and
				(Not(Activecontrol is TCheckBox	 ))			and
				(Not(Activecontrol is TDBCheckBox))			and
				(Not(Activecontrol is TButtonControl))		and
				(Not(Activecontrol is TComboBox)  )			and
				(Not(Activecontrol is TDirectoryListBox))	and
				(Not(Activecontrol is TBitBtn)) 			and
				(Not(Activecontrol is TDBComboBox)) Then begin
				SelectNext(Activecontrol,True,True);
				Key := 0;
			end else begin
				Key := 0;
			end;
		end;

		else begin
			inherited;
		end;
   end;
end;

end.
