unit CLXDataBase;

interface

uses
  SysUtils, Classes, QTypes, DB,Oracle,Registry,Windows,Dialogs;

type
  TClxData = class(TDataModule)
    DataBase: TOracleSession;
    qryError: TOracleQuery;
    qryGeneral: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataBaseBeforeLogOn(Sender: TOracleSession);
    procedure DataBaseAfterLogOn(Sender: TOracleSession);
  private
  public
		FoRegInfo			:TRegistry;
		FsNLSLang			:String;
		FsLocalIP			:String;
		FsLocalMachine		:String;
		FsDefTextDir		:String;
		FsUserID			:String;
		FsPassword			:String;
		FsServer			:String;
  end;

var
  ClxData: TClxData;

implementation

{$R *.xfm}

procedure TClxData.DataModuleCreate(Sender: TObject);
begin
	FoRegInfo	:= TRegistry.Create;
	FoRegInfo.RootKey := HKEY_LOCAL_MACHINE;

	with FoRegInfo do begin
		if (OpenKey('\SOFTWARE\iBrid\AppFrameWork',False)) then begin
			FsUserID	:= ReadString('DBUser');
			FsPassword	:= ReadString('DBPassword');
			FsServer	:= ReadString('DBServer');
			FsDefTextDir:= ReadString('DefTextDir');
		end;
		CloseKey;
	end;
end;

procedure TClxData.DataModuleDestroy(Sender: TObject);
begin
	if Assigned(FoRegInfo) then FoRegInfo.Free;
end;

procedure TClxData.DataBaseBeforeLogOn(Sender: TOracleSession);
begin
	try
		with FoRegInfo do begin
			if (OpenKey('\SOFTWARE\ORACLE\KEY_OraDb10g_home1',False)) then begin
				FsNLSLang := ReadString('NLS_LANG');
				WriteString('NLS_LANG','JAPANESE_JAPAN.AL32UTF8');
			end;
			CloseKey;
		end;

		DataBase.LogonUsername	:= FsUserID;
		DataBase.LogonPassword	:= FsPassword;
		DataBase.LogonDatabase	:= FsServer;
	except
		On DBError : Exception Do Begin
			MessageDlg('AppDataBase.DataBaseBeforeLogOn ' + DBError.Message, mtWarning,[mbOK], 0);
		end;
	end;

end;



procedure TClxData.DataBaseAfterLogOn(Sender: TOracleSession);
begin
	if Assigned(FoRegInfo) then begin
		with FoRegInfo do begin
			if (OpenKey('\SOFTWARE\ORACLE\KEY_OraDb10g_home1',False)) then begin
				WriteString('NLS_LANG',FsNLSLang);
			end;
			CloseKey;
		end;
	end;
end;

end.
