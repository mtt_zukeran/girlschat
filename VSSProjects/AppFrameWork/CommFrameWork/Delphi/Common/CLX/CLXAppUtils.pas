{*************************************************************************/
--	System			: Internet & Telephone Brige
--  Sub System Name	: 共通
--	Title			: ITB用ユーティリティ
--	Progaram ID		: CLXAppUtils
--
--  Creation Date	: 2002.12.24
--  Creater			: i-Brid(M.Suzuki)
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}

unit CLXAppUtils;

interface

uses
	Messages, SysUtils, QControls, QDialogs, Variants, StrLibCLX, QStdCtrls,Classes;

	// Validate Input
	procedure	CheckFromTo(edtFrom,edtTo:TCustomEdit);
	procedure	CheckFromToNum(edtFrom,edtTo:TCustomEdit);
	procedure   CheckNumValid(Edit:TCustomEdit);
	procedure   CheckNum(Edit:TCustomEdit;strMin,strMax: WideString);
	procedure	CheckDate(Edit:TCustomEdit);
	procedure	CheckMonth(Edit:TCustomEdit);
	procedure   CheckTime(Edit:TCustomEdit);
	procedure   CheckTimeMin(Edit:TCustomEdit);

	// Edit SQL
	function	EditZeroSup(const Text: WideString;Size:integer):WideString;

	procedure 	AddSQLAndCondition(var SelSyntax:String;AddCondtion:String);


	function 	GetStringParam(Value:Variant):String;
	function	GetDateParam(Value:Variant):TDateTime;
	function 	GetNumParam(Value:Variant):Integer;

	// HTTP Request Set and Get
	function	SetQuery		(pFHttpQueryField,pFHttpQueryValue:TStringList;pFHttpQuery:WideString):Boolean;
	function	GetQueryValue	(pFHttpQueryField,pFHttpQueryValue:TStringList;pItem:WideString):WideString;

	// DBMessage
	procedure	DBErrorMessageBox(Value: WideString);

	// StringUtils
	function	DelAfterSpace(Value: WideString): WideString;

	function	IsDelimiterUTF8(const Delimiters:WideString ; S: WideString; Index: Integer): Boolean;

implementation

{*--------------------------------------*}
{* Check Field Value					*}
{*--------------------------------------*}

procedure  CheckFromTo(edtFrom,edtTo:TCustomEdit);
begin

	if ( Trim(edtFrom.Text) = '') and (Trim(edtTo.Text) = '' ) then Exit;

	if Trim(edtFrom.Text) = '' then edtFrom.Text := edtTo.Text;
	if Trim(edtTo.Text)   = '' then edtTO.Text   := edtFrom.Text;

	if Trim(edtFrom.Text) > Trim(edtTo.Text) then begin
		MessageDlg('The input of the size relation is not right', mtWarning,[mbOK], 0);
		edtFrom.SetFocus;
        abort;
	end;
end;

procedure  CheckFromToNum(edtFrom,edtTo:TCustomEdit);
begin

	if ( Trim(edtFrom.Text) = '') and (Trim(edtTo.Text) = '' ) then Exit;

	if Trim(edtFrom.Text) = '' then edtFrom.Text := edtTo.Text;
	if Trim(edtTo.Text)   = '' then edtTO.Text   := edtFrom.Text;

	if ( StrToInt(Trim(edtFrom.Text)) > StrToInt(Trim(edtTo.Text)) ) then begin
		MessageDlg('The input of the size relation is not right', mtWarning,[mbOK], 0);
		edtFrom.SetFocus;
        abort;
	end;
end;

procedure CheckNumValid(Edit:TCustomEdit);
begin
	try
		if Trim(Edit.Text) = '' then Edit.Text := '0';

		StrToFloat(StrUnSeparator(Edit.Text,','));

	except
		MessageDlg('The input of the number is not right', mtWarning,[mbOK], 0);
		Edit.SetFocus;
        abort;
	end;
end;

procedure CheckNum(Edit:TCustomEdit;strMin,strMax: WideString);
var
	strValue:Extended;
begin
	try
		CheckNumValid(Edit);
        strValue := StrToFloat(StrUnSeparator(Edit.Text,','));
		if (strValue < StrToFloat(strMin) ) or (strValue > StrToFloat(strMax) ) then begin
			MessageDlg('Please input between ' + strMin + ' to ' + strMax, mtWarning,[mbOK], 0);
			abort;
		end;
	except
		abort;
	end;
end;

// Validate Date
procedure CheckDate(Edit:TCustomEdit);
var
	strDate     :WideString;
	blnValid    :Boolean;
begin
	try
    
		if Trim(Edit.Text) = '' Then begin
			Exit;
		end;

		strDate := StrUnSeparator(Edit.Text,'/');

		case Length(strDate) of
			6:blnValid := StrIsYYMMDD(strDate);
		    8:blnValid := StrIsYYYYMMDD(strDate);
		else
			blnValid := False;
		end;

		if blnValid <> True then begin
			abort;
		end;

		Edit.Text := EditDateFull(strDate);

	except
		MessageDlg('The input of the date is not right', mtWarning,[mbOK], 0);
		Edit.SetFocus;
        abort;
	end;
end;


// Validate Month
procedure CheckMonth(Edit:TCustomEdit);
var
	strDate :WideString;
	blnValid:Boolean;
begin
	try

		if Trim(Edit.Text) = '' Then begin
			Exit;
		end;

		strDate := StrUnSeparator(Edit.Text,'/');

		case Length(strDate) of
			4:blnValid := StrIsYYMM(strDate);
        	6:blnValid := StrIsYYYYMM(strDate);
		else
			blnValid := False;
		end;

		if blnValid <> True then begin
			abort;
		end;

		Edit.Text := EditDateYearMonth(strDate);

	except
		MessageDlg('The input of the month is not right', mtWarning,[mbOK], 0);
		Edit.SetFocus;
		abort;
	end;
end;

// Validate Time Fromat (HH:MM:SS)
procedure CheckTime(Edit:TCustomEdit);
var
	strTime	:WideString;
begin
	try

		if Trim(Edit.Text) = '' Then begin
			Exit;
		end;

		strTime := StrUnSeparator(Edit.Text,':');

		if not(StrIsTime(strTime)) then begin;
			abort;
		end;

		Edit.Text := COPY(strTime,1,2) + ':'+ COPY(strTime,3,2)+ ':' + Copy(strTime,5,2);

	except
		MessageDlg('The input of the time is not right', mtWarning,[mbOK], 0);
		Edit.SetFocus;
		abort;
	end;
end;

// Validate Time Fromat (HH:MM)
procedure CheckTimeMin(Edit:TCustomEdit);
var
	strTime	:WideString;
begin
   try

		if Trim(Edit.Text) = '' Then begin
			Exit;
		end;

		strTime := StrUnSeparator(Edit.Text,':');

		if not(StrIsTimeMin(strTime)) then begin;
			abort;
		end;

		Edit.Text := COPY(strTime,1,2) + ':'+ COPY(strTime,3,2);

	except
		MessageDlg('The input of the time is not right', mtWarning,[mbOK], 0);
		Edit.SetFocus;
		abort;
	end;
end;

{*--------------------------------------*}
{* Input Error Control					*}
{*--------------------------------------*}
// Control Only Disp Error

function EditZeroSup(const Text: WideString;Size:integer):WideString;
var
	tmp	:WideString;
begin
	tmp := Text;
	if Trim(tmp) <> '' then begin
		if Length(Text) < Size then begin
			tmp := Str0Add(Text,Size);
		end;
	end;

	result := tmp;
end;

procedure AddSQLAndCondition(var SelSyntax:String;AddCondtion:String);
var
	Len :Integer;
begin
	Len :=  Length(SelSyntax);
	if  Len = 0 then begin
		SelSyntax := ' WHERE ';
	end else begin
		SelSynTax := SelSyntax + ' AND ';
	end;

	SelSyntax := SelSynTax + AddCondtion;
end;

function GetStringParam(Value:Variant):String;
begin
	if (VarIsNull(Value)) then begin
		Result := '';
	end else begin
		Result := Value;
	end;
end;

function GetDateParam(Value:Variant):TDateTime;
begin
	if (VarIsNull(Value)) then begin
		Result := 0;
	end else begin
		Result := Value;
	end;
end;

function GetNumParam(Value:Variant):Integer;
begin
	if (VarIsNull(Value)) then begin
		Result := 0;
	end else begin
		Result := Value;
	end;
end;

{*--------------------------------------*}
{* Other Function For TPNCP				*}
{*--------------------------------------*}
function SetQuery(pFHttpQueryField,pFHttpQueryValue:TStringList;pFHttpQuery:WideString):Boolean;
var
	iLength	:Integer;
	iLenPart:Integer;
	iPos	:Integer;
	sQuery	:WideString;
	sPart	:WideString;
begin
	pFHttpQueryField.Clear;
	pFHttpQueryValue.Clear;
	sQuery := pFHttpQuery;

	iLength := Length(sQuery);

	While(iLength > 0) do begin

		iPos := Pos('&',sQuery);
		if (iPos = 0) then begin
			iPos := iLength+1;
		end;

		sPart := Copy(sQuery,1,iPos-1);
		Delete(sQuery,1,iPos);
		iLength := Length(sQuery);

		iPos := Pos('=',sPart);
		if (iPos <= 1) then begin
			Result := False;
			Exit;
		end;

		iLenPart := Length(sPart);
		pFHttpQueryField.Add(Copy(sPart,1,iPos-1));
		pFHttpQueryValue.Add(Copy(sPart,iPos+1,iLenPart-iPos));
	end;

	Result := True;
end;

function GetQueryValue(pFHttpQueryField,pFHttpQueryValue:TStringList;pItem:WideString):WideString;
var
	iSize,i :Integer;
begin
	iSize := pFHttpQueryField.Count;
	Result:= '';

	for i:=0 to iSize -1 do begin
		if (pFHttpQueryField.Strings[i] = pItem) then begin
			Result := pFHttpQueryValue.Strings[i];
			Exit;
		end;
	end;

end;

// DBMessage

procedure DBErrorMessageBox(Value: WideString);
begin
	MessageDlg(Value, mtError, [mbOK], 0);
end;

function DelAfterSpace(Value: WideString): WideString;
var
	dstStr		: WideString;
	iSpacePos	: integer;
begin
	dstStr := Copy(Value, 1, Length(Value));
	iSpacePos := Pos(' ', dstStr);
	if iSpacePos <> 0 then begin
		dstStr := Copy(dstStr, 1, iSpacePos - 1);
	end;
	Result := dstStr;
end;

function IsDelimiterUTF8(const Delimiters:WideString; S: WideString; Index: Integer): Boolean;
var
	iLen		: Integer;
begin
	iLen		:= Length(s);
	Result 		:= False;

	if Index > iLen then begin
		Exit;
	end;

	if (WideSameText(S[Index],Delimiters)=True) then begin
		Result := True;
	end;
end;


end.
