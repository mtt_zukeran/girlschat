unit CLXErrorMsgDlg;

interface

uses
  QT,SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms,
  QDialogs, QStdCtrls, QGrids, QExtCtrls, QComCtrls;

type
  TFrmErrorMsgDlg = class(TForm)
    StatusBar1: TStatusBar;
    pnlBase: TPanel;
    sgMsg: TStringGrid;
    lstMsg: TListBox;
    Panel13: TPanel;
    procedure lstMsgClick(Sender: TObject);
    procedure sgMsgMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sgMsgSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgMsgDblClick(Sender: TObject);
  private
	FOwnerHandle	: QOBJECTH;
	xMouse			: integer;
	yMouse			: integer;

	function	GetCol: Integer;
	function	GetRow: Integer;
	function	GetDetail: WideString;
	function	GetIsError: Boolean;
  public
	procedure	Init;
	procedure	SetMsg(msg : WideString);
	function	GetMsg: WideString;

	procedure	SetError(sCol, sRow, sDetail : WideString);
	procedure	SetHandle(AHnadle : QObjectH);
	property	Col : Integer read GetCol;
	property	Row : Integer read GetRow;
	property	Detail : WideString read GetDetail;
	property	IsError : Boolean read GetIsError;

  end;

var
  FrmErrorMsgDlg: TFrmErrorMsgDlg;

implementation

uses ITB_CONST, CLX_IBRID_APP_CONST;

{$R *.xfm}

function TFrmErrorMsgDlg.GetCol: Integer;
begin
	if sgMsg.Cells[0, sgMsg.Row] <> '' then begin
		Result := StrToInt(sgMsg.Cells[0, sgMsg.Row]) - 1;
	end else begin
		Result := -1;
	end;
end;

function TFrmErrorMsgDlg.GetRow: Integer;
begin
	if sgMsg.Cells[1, sgMsg.Row] <> '' then begin
		Result := StrToInt(sgMsg.Cells[1, sgMsg.Row]);
	end else begin
		Result := -1;
	end;
end;



procedure TFrmErrorMsgDlg.sgMsgSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
	QApplication_postEvent(FOwnerHandle,QCustomEvent_create(QEvent_ErrorMsgWd));
end;

procedure TFrmErrorMsgDlg.SetError(sCol, sRow, sDetail: WideString);
begin
	if sgMsg.Cells[2, 1] <> '' then begin
		sgMsg.RowCount := sgMsg.RowCount + 1;
	end;
	if (sCol <> '') and (sRow <> '') then begin
		sgMsg.Cells[0, sgMsg.RowCount - 1] := IntToStr(StrToInt(sCol) + 1);
		sgMsg.Cells[1, sgMsg.RowCount - 1] := sRow;
		sgMsg.Cells[2, sgMsg.RowCount - 1] := sDetail;
	end else if (sCol = '') and (sRow = '') then begin
		sgMsg.Cells[2, sgMsg.RowCount - 1] := sDetail;
	end;
end;

procedure TFrmErrorMsgDlg.Init;
var
	iCol, iRow	: integer;
begin
	for iRow := sgMsg.FixedRows to sgMsg.RowCount - 1 do begin
		for iCol := sgMsg.FixedCols to sgMsg.ColCount - 1 do begin
			sgMsg.Cells[iCol, iRow] := '';
		end;
	end;
	sgMsg.RowCount := 2;
	lstMsg.Items.Clear;
end;

procedure TFrmErrorMsgDlg.sgMsgDblClick(Sender: TObject);
var
	ACol, ARow	: integer;
begin
	sgMsg.MouseToCell(xMouse, yMouse, ACol, ARow);
	if (sgMsg.Row = ARow) then begin
		QApplication_postEvent(FOwnerHandle,QCustomEvent_create(QEvent_ErrorMsgWd));
	end;
end;

procedure TFrmErrorMsgDlg.sgMsgMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
	xMouse := X;
	yMouse := Y;
end;

function TFrmErrorMsgDlg.GetDetail: WideString;
begin
	Result := sgMsg.Cells[2, sgMsg.Row];
end;

function TFrmErrorMsgDlg.GetIsError: Boolean;
begin
	Result := sgMsg.Cells[2, 1] <> '';
end;

procedure TFrmErrorMsgDlg.SetMsg(msg: WideString);
begin
	lstMsg.Items.Add(msg);
end;

function TFrmErrorMsgDlg.GetMsg: WideString;
begin
	with lstMsg do begin
		if ItemIndex <> - 1 then begin
			Result := Items[ItemIndex];
		end else begin
			Result := '';
		end;
	end;
end;

procedure TFrmErrorMsgDlg.lstMsgClick(Sender: TObject);
begin
	if lstMsg.ItemIndex <> - 1 then begin
		QApplication_postEvent(FOwnerHandle,QCustomEvent_create(QEvent_ErrorMsgWd));
	end;
end;

procedure TFrmErrorMsgDlg.SetHandle(AHnadle : QObjectH);
begin
	FOwnerHandle := AHnadle;
end;


end.

