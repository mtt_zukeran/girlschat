{*************************************************************************/
--	System			: Internet & Telephone Brige
--  Sub System Name	: 共通
--	Title			: 基本フレーム
--	Progaram ID		: TFrmBase
--
--  Creation Date	: 2002.12.02
--  Creater			: i-Brid(T.Itoi)
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit CLXBasicInputFrame;

interface

uses
  SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms,
  QDialogs, QStdCtrls, QButtons, QExtCtrls, QActnList,
	CLXErrorMsgDlg,QryComboBoxCLX,IBridEditCLX,QT,DB,QDBCtrls,QDBGrids,iBridNumEditCLX;

type
  TFrmBase = class(TForm)
    pnlTitleBase: TPanel;
    pnlTitle: TPanel;
    alBase: TActionList;
    actPF1: TAction;
    actPF2: TAction;
    actPF3: TAction;
    actPF4: TAction;
    actPF5: TAction;
    actPF6: TAction;
    actPF7: TAction;
    actPF8: TAction;
    actPF9: TAction;
    actPF10: TAction;
    actPF11: TAction;
    actPF12: TAction;
    pnlFuncBarBase: TPanel;
    bvlSep1: TBevel;
    bvlSep2: TBevel;
    btnPF1: TSpeedButton;
    btnPF2: TSpeedButton;
    btnPF3: TSpeedButton;
    btnPF4: TSpeedButton;
    btnPF5: TSpeedButton;
    btnPF6: TSpeedButton;
    btnPF7: TSpeedButton;
    btnPF8: TSpeedButton;
    btnPF9: TSpeedButton;
    btnPF10: TSpeedButton;
    btnPF11: TSpeedButton;
    btnPF12: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure actPF6Execute(Sender: TObject);
    procedure actPF12Execute(Sender: TObject);
    procedure pnlFuncBarBaseResize(Sender: TObject);
	procedure DataAreaOnKeyPress(Sender: TObject; var Key: Char);
	procedure DataOnKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure DataOnChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  	FAbortFlag		:Boolean;

  protected
	FColorError		:TColor;
    FColorNormal	:TColor;
    FColorLock		:TColor;
    FColorWarning	:TColor;
	FErrorStr		:TStrings;
	FErrorDlg		:TFrmErrorMsgDlg;
	FDataChangeFlg	: Boolean;
	FErrorFocusFlg	: Boolean;

    FProcNo			:Integer;

	FNowPrint		:Boolean;

	FModified		:Boolean;

    procedure	ExecUpdate(pStatus :SmallInt);	virtual;
    procedure	CloseDateSet;
    function	CheckUpdateStatus:Word;
	Function 	EventFilter(Sender:QObjectH;Event:QEventH):Boolean;override;

	// Control Action
	function 	FindControl(pForm:TForm;pName:String):TComponent;
	procedure	ClearColor(pComp:TComponent);
	procedure	SetColor(pControl:TComponent;pColor:TColor);
	procedure	SetReadOnly(pCtl:TComponent;pLock:Boolean);
	procedure	SetReadOnlyScreen(pForm:TComponent;pLock:Boolean);

	function	GetErrorMsg(pErrorCd: WideString): WideString;

	procedure	CtlError(pErrorCd:WideString; ctlWin:TWidgetControl);
	function	CtlErrorMain(pErrorCd:WideString; ctlWin:TWidgetControl): Boolean;
	procedure	DispErrorMsg(pErrorCd:WideString);
	function	DispWarningMsg(pErrorCd:WideString): Boolean;
	function	DispConfirmationMsg(pErrorCd:WideString): Boolean;


	function	CheckNotNullQryCombo(cmb: TQryComboBox): Boolean;
	function	CheckNotNullIBridEdit(edit: TIBridEdit): Boolean;

	function 	CheckNaturalNum(AIBridEdit: TIBridEdit): boolean;overload;

	function	NumberIsNatural(ANumber: WideString): boolean;
	function	GetCode(AText: WideString): WideString;

	procedure 	SetComboCaption(cmb: TQryComboBox);
	function	CheckQryCombo(cmb: TQryComboBox): Boolean;
	procedure	SetErrorMsgToDlg(ErrorStr: TStrings);
	function	RageCheckDate(FromDate,ToDate:TIBridEdit): boolean;
	procedure 	SetErrorMsg(edit: TIBridEdit;msg:String);

  public
  { Public 宣言 }
	procedure	FreeIns;		virtual;abstract;
	procedure	InitWin;		virtual;
  end;

var
  FrmBase: TFrmBase;

implementation

uses ITB_CONST, CLXAppUtils, IBRID_APP_CONST,
  CLX_IBRID_APP_CONST, CustomDataModule;

const
	btnleft = 6;
	btnSpace = 3;


{$R *.xfm}
Function TFrmBase.EventFilter(Sender:QObjectH;Event:QEventH):Boolean;
var
	sHint	:WideString;
	ctlTemp	:TComponent;
	i		:Integer;
	iID		:Integer;
begin
	iID		:= Integer(QEvent_type(Event));
	Result	:= True;

	case iId of
		QEvent_ErrorMsgWd:begin
			sHint	:= Copy(FErrorDlg.GetMsg,1,Pos(':',FErrorDlg.GetMsg)-1);

			with Self do begin
				for I := ComponentCount - 1 downto 0 do begin
					ctlTemp := Components[I];
					if (ctlTemp is TWidgetControl) then begin
						if TWidgetControl(ctlTemp).Hint = sHint Then begin
							TWidgetControl(ctlTemp).SetFocus;
							exit;
						end;
					end;
				end;
			end;

			Result := True;
		end;

		QEvent_NextControl:begin
			SelectNext(Self.ActiveControl,True,True);
			Result := True;
		end;

		QEvent_PrivControl:begin
			SelectNext(Self.ActiveControl,False,True);
			Result := True;
		end;

		QEvent_InitWin:begin
			InitWin;
		end;

		else begin
			Result :=inherited EventFilter(Sender,Event);
		end;
	end;
end;

procedure TFrmBase.FormShow(Sender: TObject);
begin
	CloseDateSet;
	ClearColor(Self);
end;

procedure TFrmBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Screen.Cursor := crDefault;
	if CheckUpdateStatus = mrYES then begin
		ExecUpdate(STATUS_END);
	end;
	CloseDateSet;
	Action	:= caFree;
end;

procedure TFrmBase.FormCreate(Sender: TObject);
begin
	FColorError		:= clAqua;
	FColorNormal	:= clWindow;
	FColorLock		:= clSilver;
	FColorWarning	:= clYellow;

	FErrorStr		:= TStringList.Create;
end;

procedure TFrmBase.FormDestroy(Sender: TObject);
begin
	FreeIns;

	FErrorStr.Free;
	if Assigned(FErrorDlg) then begin
		FErrorDlg.Free;
	end;

	if Owner <> Nil then begin
		TWinControl(Self.Owner).SetFocus;
	end;
end;

procedure TFrmBase.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	case Key of
		Key_Escape:begin
			actPF12.Execute;
			Key := 0;
		end;
		Key_f1:begin
			actPF1.Execute;
			Key := 0;
		end;
		Key_f2:begin
			actPF2.Execute;
			Key := 0;
		end;
		Key_f3:begin
			actPF3.Execute;
			Key := 0;
		end;
		Key_f4:begin
			actPF4.Execute;
		end;
		Key_f5:begin
			actPF5.Execute;
			Key := 0;
		end;
		Key_f6:begin
			actPF6.Execute;
			Key := 0;
		end;
		Key_f7:begin
			actPF7.Execute;
			Key := 0;
		end;
		Key_f8:begin
			actPF8.Execute;
			Key := 0;
		end;
		Key_f9:begin
			actPF9.Execute;
			Key := 0;
		end;
		Key_f10:begin
			actPF10.Execute;
			Key := 0;
		end;
		Key_f11:begin
			actPF11.Execute;
			Key := 0;
		end;
		Key_f12:begin
			actPF12.Execute;
			Key := 0;
		end;
	end;

end;

procedure TFrmBase.actPF6Execute(Sender: TObject);
begin
	ClearColor(Self);
end;

procedure TFrmBase.actPF12Execute(Sender: TObject);
begin
	Close;
end;

procedure TFrmBase.pnlFuncBarBaseResize(Sender: TObject);
var
	iBtnSize : Integer;
begin
	try
		iBtnSize := (pnlFuncBarBase.Width - (btnleft * 2) - (btnSpace * 4) - bvlSep1.Width - bvlSep2.Width) div 12;

		btnPF1.Width := iBtnSize;
		btnPF2.Width := iBtnSize;
		btnPF3.Width := iBtnSize;
		btnPF4.Width := iBtnSize;
		btnPF5.Width := iBtnSize;
		btnPF6.Width := iBtnSize;
		btnPF7.Width := iBtnSize;
		btnPF8.Width := iBtnSize;
		btnPF9.Width := iBtnSize;
		btnPF10.Width := iBtnSize;
		btnPF11.Width := iBtnSize;
		btnPF12.Width := iBtnSize;

		btnPF1.Left := btnleft;
		btnPF2.Left := btnPF1.Left + btnPF1.Width;
		btnPF3.Left := btnPF2.Left + btnPF2.Width;
		btnPF4.Left := btnPF3.Left + btnPF3.Width;
		bvlSep1.Left := btnPF4.Left + btnPF4.Width + btnSpace;

		btnPF5.Left := btnPF4.Left + btnPF4.Width + (btnSpace * 2) + bvlSep1.Width;
		btnPF6.Left := btnPF5.Left + btnPF5.Width;
		btnPF7.Left := btnPF6.Left + btnPF6.Width;
		btnPF8.Left := btnPF7.Left + btnPF7.Width;
		bvlSep2.Left := btnPF8.Left + btnPF8.Width + btnSpace;

		btnPF9.Left := btnPF8.Left + btnPF8.Width + (btnSpace * 2) + bvlSep2.Width;
		btnPF10.Left := btnPF9.Left + btnPF9.Width;
		btnPF11.Left := btnPF10.Left + btnPF10.Width;
		btnPF12.Left := btnPF11.Left + btnPF11.Width;
	except
	end;
end;

{*--------------------------------------*}
{* DataSet Control						*}
{*--------------------------------------*}

procedure TFrmBase.ExecUpdate(pStatus :SmallInt);
begin

end;

procedure TFrmBase.CloseDateSet;
var
	ctlTemp	:TComponent;
	i		:Integer;
begin
	with Self do begin
		for I := ComponentCount - 1 downto 0 do begin
            if Components[I] is TDataSet then begin
            	ctlTemp := Components[I];
				if TDataSet(ctlTemp).Active Then Begin
					TDataSet(ctlTemp).Cancel;
					TDataSet(ctlTemp).Close;
				end;
			end;
        end;
    end;
end;

Function TFrmBase.CheckUpdateStatus:Word;
var
   wrdBtn      :Word;
begin
	wrdBtn := mrNO;

	if FModified then begin
		wrdBtn := MessageDlg('ﾚｺｰﾄﾞが修正されています。更新しますか？', mtConfirmation,[mbNo,mbYes,mbCancel], 0);
	end;

	if wrdBtn = mrCancel then begin
		SysUtils.Abort;
	end;

	Result := wrdBtn;
end;

{*--------------------------------------*}
{* Window Control Action				*}
{*--------------------------------------*}
// Find Comtrol By Name
Function TFrmBase.FindControl(pForm:TForm;pName:String):TComponent;
var
	ctlTemp	:TComponent;
	i		:Integer;
begin

	Result := Nil;

	with pForm do begin
		for I := ComponentCount - 1 downto 0 do begin
			ctlTemp := Components[I];
			if UpperCase(ctlTemp.Name) = UpperCase(pName) Then begin
				Result := ctlTemp;
				exit;
			end;
		end;
	end;
end;

// Set Comtrol Color Normal
procedure TFrmBase.ClearColor(pComp:TComponent);
var
	ctlTemp	:TComponent;
	i		:Integer;
begin
	with pComp do begin
		for I := ComponentCount - 1 downto 0 do begin
			ctlTemp := Components[I];

            if ctlTemp is TEdit     	then begin
				if TEdit(ctlTemp).ReadOnly = False then TEdit(ctlTemp).Color				:= FColorNormal;
			end;

			if ctlTemp is TDBEdit     	then begin
				if TDBEdit(ctlTemp).ReadOnly = False then TDBEdit(ctlTemp).Color			:= FColorNormal;
			end;

            if ctlTemp is TComboBox		then begin
				if TComboBox(ctlTemp).Enabled = True then TComboBox(ctlTemp).Color			:= FColorNormal;
			end;

			if ctlTemp is TDBComboBox 	then begin
				if TDBComboBox(ctlTemp).ReadOnly = False then TDBComboBox(ctlTemp).Color	:= FColorNormal;
			end;

			if ctlTemp is TMemo     	then begin
				if TMemo(ctlTemp).ReadOnly = False then TMemo(ctlTemp).Color				:= FColorNormal;
			end;

            if ctlTemp is TQryComboBox     	then begin
				if TQryComboBox(ctlTemp).Enabled = True then TQryComboBox(ctlTemp).Color	:= FColorNormal;
			end;

            if ctlTemp is TIBridEdit     then begin
				if TIBridEdit(ctlTemp).ReadOnly = False then TIBridEdit(ctlTemp).Color		:= FColorNormal;
			end;

            if ctlTemp is TIBridNumEdit     then begin
				if TiBridNumEdit(ctlTemp).ReadOnly = False then TIbridNumEdit(ctlTemp).Color:= FColorNormal;
			end;

		end;
	end;
end;

// Set Comtrol Color On Form
procedure TFrmBase.SetColor(pControl:TComponent;pColor:TColor);
begin
	if pControl = Nil then Exit;

	if pControl is TEdit		then TEdit(pControl).Color			:= pColor;
	if pControl is TDBEdit		then TDBEdit(pControl).Color		:= pColor;
    if pControl is TComboBox	then TComboBox(pControl).Color		:= pColor;
	if pControl is TDBComboBox	then TDBComboBox(pControl).Color	:= pColor;
	if pControl is TMemo		then TMemo(pControl).Color			:= pColor;
	if pControl is TDBMemo		then TDBMemo(pControl).Color		:= pColor;
end;

// Set Read Only OR Not Read Only At Control
procedure TFrmBase.SetReadOnly(pCtl:TComponent;pLock:Boolean);
var
	ctlColor	:TColor;
begin

	if pLock then begin
		ctlColor := FColorLock;
	end else begin
		ctlColor := FColorNormal;
	end;

    if pCtl is TEdit     	then begin
    	TEdit(pCtl).ReadOnly		:= pLock;
		TEdit(pCtl).Color			:= ctlColor;
    end;

	if pCtl is TDBEdit     	then begin
		TDBEdit(pCtl).ReadOnly		:= pLock;
		TDBEdit(pCtl).Color			:= ctlColor;
	end;

    if pCtl is TComboBox 	then begin
		TComboBox(pCtl).Enabled		:= not(pLock);
		TComboBox(pCtl).Color		:= ctlColor;
	end;

    if pCtl is TDBComboBox 	then begin
		TDBComboBox(pCtl).ReadOnly	:= pLock;
		TDBComboBox(pCtl).Color		:= ctlColor;
	end;

    if pCtl is TMemo 	then begin
		TMemo(pCtl).ReadOnly		:= pLock;
		TMemo(pCtl).Color			:= ctlColor;
	end;

    if pCtl is TDBMemo 	then begin
		TDBMemo(pCtl).ReadOnly		:= pLock;
		TDBMemo(pCtl).Color			:= ctlColor;
	end;
end;

// Set Read Only Or Not Read Only On Form
procedure TFrmBase.SetReadOnlyScreen(pForm:TComponent;pLock:Boolean);
var
	ctlTemp	:TComponent;
	i			:Integer;
	ctlColor	:TColor;
begin

	if pLock then begin
		ctlColor := FColorLock;
	end else begin
		ctlColor := FColorNormal;
	end;

	with pForm do begin
		for I := ComponentCount - 1 downto 0 do begin
			ctlTemp := Components[I];

            if ctlTemp is TEdit     	then begin
				TEdit(ctlTemp).ReadOnly			:= pLock;
				TEdit(ctlTemp).Color			:= ctlColor;
			end;

			if ctlTemp is TDBEdit     	then begin
				TDBEdit(ctlTemp).ReadOnly		:= pLock;
				TDBEdit(ctlTemp).Color			:= ctlColor;
			end;

            if ctlTemp is TComboBox 	then begin
				TComboBox(ctlTemp).Enabled		:= not(pLock);
				TComboBox(ctlTemp).Color		:= ctlColor;
			end;

			if ctlTemp is TDBComboBox 	then begin
				TDBComboBox(ctlTemp).ReadOnly	:= pLock;
				TDBComboBox(ctlTemp).Color		:= ctlColor;
			end;

            if ctlTemp is TMemo 	then begin
				TMemo(ctlTemp).ReadOnly			:= pLock;
				TMemo(ctlTemp).Color			:= ctlColor;
			end;

    		if ctlTemp is TDBMemo 	then begin
				TDBMemo(ctlTemp).ReadOnly		:= pLock;
				TDBMemo(ctlTemp).Color			:= ctlColor;
			end;

			if ctlTemp is TDBGrid 	then begin
				TDBGrid(ctlTemp).ReadOnly	:= pLock;
			end;

			if ctlTemp is TDBCheckBox 	then begin
				TDBCheckBox(ctlTemp).ReadOnly	:= pLock;
			end;

			if ctlTemp is TDBRadioGroup 	then begin
				TDBRadioGroup(ctlTemp).ReadOnly	:= pLock;
			end;

			if ctlTemp is TGroupBox 	then begin
				if pLock then begin
					TGroupBox(ctlTemp).Enabled	:= False;
				end else begin
					TGroupBox(ctlTemp).Enabled	:= True;
				end;
			end;
		end;
	end;
end;

{*--------------------------------------*}
{* Error Control						*}
{*--------------------------------------*}

// Control Disp Error & Abort
procedure TFrmBase.CtlError(pErrorCd:WideString; ctlWin:TWidgetControl);
begin
	CtlErrorMain(pErrorCd,ctlWin);
	Screen.Cursor := crDefault;
	if pErrorCd <> RETURN_NORMAL then begin
		if FAbortFlag then SysUtils.Abort;
	end;
end;

// Control Error Base
function TFrmBase.CtlErrorMain(pErrorCd:WideString; ctlWin:TWidgetControl): Boolean;
begin
	Result := True;

	ClearColor(Self);

	if pErrorCd = RETURN_NORMAL then begin
		Exit;
	end;

	if CtlWin <> Nil Then SetColor(CtlWin,FColorError);

	if (Copy(pErrorCd,1,1) = 'E') then begin
		DispErrorMsg(pErrorCd);
	end else if (Copy(pErrorCd,1,1) = 'W') then begin
		Result := DispWarningMsg(pErrorCd);
	end else if (Copy(pErrorCd,1,1) = 'C') then begin
		Result := DispConfirmationMsg(pErrorCd);
	end;

	if CtlWin <> Nil Then begin
		CtlWin.SetFocus;
	end;
end;

// Diplay Error
procedure TFrmBase.DispErrorMsg(pErrorCd:WideString);
begin
	// Get Error Msg
	FAbortFlag := True;

	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		if Not(Eof) Then begin
			MessageDlg(UTF8Decode(Field('ERROR_DTL')),mtError	, [mbOK], 0);
		end else begin
			MessageDlg( 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd ,mtWarning	, [mbOK], 0);
		end;
	end;
end;

// Diplay Warning
function TFrmBase.DispWarningMsg(pErrorCd:WideString): Boolean;
begin
	// Get Warning Msg
	Result := True;
	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		if Not(Eof) Then begin
			if MessageDlg(UTF8Decode(Field('ERROR_DTL')),mtWarning	, [mbOK], 0) = mrYes then begin
				Result := True;
				FAbortFlag := False;
			end else begin
				Result := False;
				FAbortFlag := True;
			end;
		end else begin
			MessageDlg( 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd ,mtWarning	, [mbOK], 0);
			FAbortFlag := True;
		end;
	end;
end;

// Diplay Confirmation
function TFrmBase.DispConfirmationMsg(pErrorCd: WideString): Boolean;
begin
	// Get Warning Msg
	Result := True;
	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		if Not(Eof) Then begin
			if MessageDlg(UTF8Decode(Field('ERROR_DTL')),mtConfirmation	, [mbOK,mbCancel], 0) = mrYes then begin
				Result := True;
				FAbortFlag := False;
			end else begin
				Result := False;
				FAbortFlag := True;
			end;
		end else begin
			MessageDlg( 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd ,mtWarning	, [mbOK], 0);
			FAbortFlag := True;
		end;
	end;
end;

function TFrmBase.GetErrorMsg(pErrorCd: WideString): WideString;
begin
	with CustomDB.qryError do begin
		Close;
		SetVariable('WERROR_CD', pErrorCd);
		Execute;
		IF Not(Eof) Then begin
			Result := UTF8Decode(Field('ERROR_DTL'));
		end else begin
			Result := 'ｴﾗｰｺｰﾄﾞ未登録 PGM-ID = ' + Self.Name + ' ERROR CD = ' + pErrorCd;
		end;
	end;
end;

function TFrmBase.CheckNotNullQryCombo(cmb: TQryComboBox): Boolean;
begin
	Result := True;
	if cmb.Text = '' then begin
		Result := false;
		FErrorStr.Add(cmb.Hint + ':' + GetErrorMsg('E104'));
		cmb.Color := FColorError;
		if not FErrorFocusFlg then begin
			FErrorFocusFlg := true;
			cmb.SetFocus;
		end;
	end;
end;

function TFrmBase.CheckNotNullIBridEdit(edit: TIBridEdit): Boolean;
begin
	Result := True;
	if edit.Text = '' then begin
		Result := false;
		FErrorStr.Add(edit.Hint + ':' + GetErrorMsg('E104'));
		edit.Color := FColorError;
		if not FErrorFocusFlg then begin
			FErrorFocusFlg := true;
			edit.SetFocus;
		end;
	end;
end;

procedure TFrmBase.SetErrorMsg(edit: TIBridEdit;msg:String);
begin
	FErrorStr.Add(edit.Hint + ':' + GetErrorMsg(msg));
	edit.Color := FColorError;
	if not FErrorFocusFlg then begin
		FErrorFocusFlg := true;
		edit.SetFocus;
	end;
end;


function TFrmBase.CheckNaturalNum(AIBridEdit: TIBridEdit): boolean;
begin
	result := True;
	AIBridEdit.Color := clWindow;
	try
		if False = NumberIsNatural(AIBridEdit.Text) then begin
			FErrorStr.Add(AIBridEdit.Hint + ':' + GetErrorMsg('E107'));
			raise Exception.Create(GetErrorMsg('E107'));
		end;
	except
		AIBridEdit.Color := FColorError;
		if not FErrorFocusFlg then begin
			AIBridEdit.SetFocus;
			FErrorFocusFlg := True;
		end;
		result := False;
	end;
end;


function TFrmBase.NumberIsNatural(ANumber: WideString): boolean;
var
	WNum: Integer;
begin
	result := True;
	try
		if ANumber <> '' then begin
			WNum := StrToInt(ANumber);
			if (0 > WNum) then begin
				result := False;
			end;
		end;
	except
		result := False;
    end;
end;

function TFrmBase.GetCode(AText: WideString): WideString;
begin
	if Pos(' ', AText) = 0 then begin
		result := AText
	end else begin
		result := Copy(AText, 1, Pos(' ', AText)-1);
	end;
end;


procedure TFrmBase.DataAreaOnKeyPress(Sender: TObject; var Key: Char);
begin
  	try
  		if Key = #13 then begin
    		Key := #0;
  		end;
	    if Key <> #0 then inherited KeyPress(Key);
    except
    end;
end;


procedure TFrmBase.DataOnKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	inherited;
	try
		if (Key = Key_Return) or (Key = Key_Enter) then begin
			if (ssShift in Shift) then begin
				SelectNext(Self,False,True);
			end else begin
				SelectNext(Self,True,True);
			end;
			Key := 0;
		end;
	except
	end;
end;

procedure TFrmBase.DataOnChange(Sender: TObject);
begin
	inherited;
	try
		if False = FDataChangeFlg then begin
			FDataChangeFlg := True;
		end;
	except
	end;
end;

procedure TFrmBase.SetComboCaption(cmb: TQryComboBox);
var
	list	: TStrings;
	iLoop	: integer;
begin
	try
		if (cmb.Text <> '') and (cmb.Items.Count > 0) then begin
			list := TStringList.Create;
			try
				for iLoop := 0 to cmb.Items.Count - 1 do begin
					list.Add(DelAfterSpace(cmb.Items[iLoop]));
				end;
				if list.IndexOf(DelAfterSpace(cmb.UTF8Text)) <> - 1 then begin
					cmb.ItemIndex	:= list.IndexOf(DelAfterSpace(cmb.UTF8Text));
					// ItemIndexの設定だけではABBRが表示されないことがある。
					cmb.UTF8Text    := UTF8Encode(cmb.Items[cmb.ItemIndex]);
				end;
			finally
				list.Free;
			end;
		end;
	except
	end;
end;

function TFrmBase.CheckQryCombo(cmb: TQryComboBox): Boolean;
begin
	Result := True;
	if not cmb.CheckKey then begin
		FErrorStr.Add(cmb.Hint + ':' + GetErrorMsg('E101'));
		cmb.Color := FColorError;
		Result := False;
	end;
end;

procedure TFrmBase.SetErrorMsgToDlg(ErrorStr: TStrings);
var
	iCount	: integer;
begin
	for iCount := 0 to ErrorStr.Count - 1 do begin
		FErrorDlg.SetMsg(ErrorStr[iCount]);
	end;
end;



function TFrmBase.RageCheckDate(FromDate,ToDate:TIBridEdit): boolean;
var
	wFromDate	: TDateTime;
	wToDate		: TDateTime;
begin
	Result := True;
	try

		wFromDate	:= StrToDateTime(FromDate.Text);
		wToDate 	:= StrToDateTime(ToDate.Text);

		if wToDate < wFromDate then begin
			result := False;
			FErrorStr.Add(FromDate.Hint + ':' + GetErrorMsg('E136'));
			FromDate.Color	:= FColorError;
			ToDate.Color	:= FColorError;

			if not FErrorFocusFlg then begin
				FErrorFocusFlg := true;
				FromDate.SetFocus;
			end;

		end;
	except
		result := False;
    end;
end;

procedure TFrmBase.InitWin;
begin
end;


end.
