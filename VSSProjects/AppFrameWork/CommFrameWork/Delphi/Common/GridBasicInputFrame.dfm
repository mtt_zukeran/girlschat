inherited FrmGridBasicInput: TFrmGridBasicInput
  Left = 419
  Top = 150
  Caption = 'FrmGridBasicInput'
  PixelsPerInch = 96
  TextHeight = 12
  inherited pnlMain: TPanel
    BevelWidth = 2
    inherited pnlDtl: TPanel
      Left = 2
      Top = 2
      Width = 1008
      Height = 641
      BevelWidth = 2
      object sgTable: TITBGrid
        Left = 4
        Top = 4
        Width = 1000
        Height = 633
        Align = alClient
        ColCount = 3
        DefaultRowHeight = 19
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goTabs]
        TabOrder = 0
        ColParams = <>
        Modified = False
        ColWidths = (
          64
          64
          64)
      end
    end
  end
  inherited alBase: TActionList
    inherited actPF11: TAction
      OnExecute = nil
    end
  end
  object doaGet: TOracleQuery
    Session = CustomDB.DataBase
    Left = 76
    Top = 9
  end
  object doaMainte: TOracleQuery
    Session = CustomDB.DataBase
    Left = 108
    Top = 9
  end
end
