{*************************************************************************/
--	System			: Brid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: ユーティリティ
--	Progaram ID		: UAPPUtils
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit AppUtils;

interface

uses
	Windows,Messages, SysUtils, Controls, Dialogs, Variants, StrLib, StdCtrls,Classes,StrUtils,IniFiles,Math;

	// Edit SQL
	procedure	AddSQLOrCondition(var SelSyntax:String;AddCondtion:String);
	procedure	AddSQLAndCondition(var SelSyntax:String;AddCondtion:String);
	function 	GetStringParam(Value:Variant):String;
	function	GetDateParam(Value:Variant):TDateTime;
	function 	GetNumParam(Value:Variant):Integer;

	// HTTP Request Set and Get
	function	SetQuery			(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pFHttpQuery:String;pSeparator:String='&'):Boolean;
	function	GetQueryValue		(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pItem:String):String;
	function 	AddQueryItemValue	(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pItem,pValue:String):String;
	function	ReplaceQueryValue	(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pItem,pValue:String):String;
	function	HTTPEncode			(const AStr: String): String;

	// DBMessage
	procedure	DBErrorMessageBox(Value: string);

	// StringUtils
	function	DelAfterSpace(Value: string): string;
	function	IsNumOnly(S: string): boolean;

	Function	CalcSecond(FromTime,ToTime:TDateTime):Integer;

	// Purge Log
	procedure PurgeLog(pPurgeDays:Integer;pPurgeDir:String);

	function GrabStdOut(sCommandLine: string; pStdIn, pStdOut, pStdErr: TStream): DWORD;
implementation

uses IBRID_APP_CONST;

{*--------------------------------------------------------------*}
{*	SQL Functions												*}
{*--------------------------------------------------------------*}
procedure AddSQLOrCondition(var SelSyntax:String;AddCondtion:String);
var
	Len :Integer;
begin
	Len :=  Length(SelSyntax);
	if  Len = 0 then begin
		SelSyntax := 'AND (';
	end else begin
		Delete(SelSyntax,Len,1);
		SelSynTax := SelSyntax + ' OR ';
	end;

	SelSyntax := SelSynTax + AddCondtion + ')';
end;

procedure AddSQLAndCondition(var SelSyntax:String;AddCondtion:String);
var
	Len :Integer;
begin
	Len :=  Length(SelSyntax);
	if  Len = 0 then begin
		SelSyntax := ' WHERE ';
	end else begin
		SelSynTax := SelSyntax + ' AND ';
	end;

	SelSyntax := SelSynTax + AddCondtion;
end;

function GetStringParam(Value:Variant):String;
begin
	if (VarIsNull(Value)) then begin
		Result := '';
	end else begin
		Result := Value;
	end;
end;

function GetDateParam(Value:Variant):TDateTime;
begin
	if (VarIsNull(Value)) then begin
		Result := 0;
	end else begin
		Result := Value;
	end;
end;

function GetNumParam(Value:Variant):Integer;
begin
	if (VarIsNull(Value)) then begin
		Result := 0;
	end else begin
		Result := Value;
	end;
end;

{*--------------------------------------------------------------*}
{*	HTTP Query Set & Parase										*}
{*--------------------------------------------------------------*}
function SetQuery(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pFHttpQuery:String;pSeparator:String='&'):Boolean;
var
	iLength	:Integer;
	iLenPart:Integer;
	sQuery	:String;
	sPart	:String;
	bEnd	:Boolean;

	iPos,iPosEQ,iNewPos	:Integer;
begin
	pFHttpQueryField.Clear;
	pFHttpQueryValue.Clear;
	sQuery := pFHttpQuery;

	iLength := Length(sQuery);

	iPos := 1;
	bEnd := False;

	While (bEnd = False) do begin

		iNewPos := PosEx(pSeparator,sQuery,iPos);

		if (iNewPos = 0) then begin
			iNewPos := iLength  + 1;
			bEnd := True;
		end;

		sPart := Copy(sQuery,iPos,iNewPos - iPos);

		iPosEQ := Pos('=',sPart);

		if (iPosEQ <= 1) then begin
			Result := False;
			Exit;
		end;

		iLenPart := Length(sPart);

		pFHttpQueryField.Add(Copy(sPart,1,iPosEQ-1));
		pFHttpQueryValue.Add(Copy(sPart,iPosEQ+1,iLenPart-iPosEQ));

		iPos := iNewPos + 1;
	end;

	Result := True;
end;

function GetQueryValue(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pItem:String):String;
var
	i :Integer;
begin
	Result:= '';

	i:=  pFHttpQueryField.IndexOf(pItem);
	if (i <> -1) then begin
		Result := pFHttpQueryValue.Strings[i];
	end;
end;

function AddQueryItemValue(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pItem,pValue:String):String;
var
	iSize,i :Integer;
begin
	pFHttpQueryField.Add(pItem);
	pFHttpQueryValue.Add(pValue);

	iSize := pFHttpQueryField.Count;

	for i:=0 to iSize -1 do begin
		Result:= Result + pFHttpQueryField[i] + '=' + pFHttpQueryValue.Strings[i];
		if (i <> iSize -1 ) then  Result := Result + '&';
	end;
end;


function ReplaceQueryValue(pFHttpQueryField,pFHttpQueryValue:THashedStringList;pItem,pValue:String):String;
var
	iSize,i :Integer;
begin
	iSize := pFHttpQueryField.Count;
	Result:= '';

	for i:=0 to iSize -1 do begin
		if (pFHttpQueryField.Strings[i] = pItem) then begin
			pFHttpQueryValue.Strings[i] := pValue;
		end;
		Result:= Result + pFHttpQueryField[i] + '=' + pFHttpQueryValue.Strings[i];
		if (i <> iSize -1 ) then  Result := Result + '&';
	end;
end;


// DBMessage
procedure DBErrorMessageBox(Value: string);
begin
	MessageDlg(Value, mtError, [mbOK], 0);
end;

function DelAfterSpace(Value: string): string;
var
	dstStr		: string;
	iSpacePos	: integer;
begin
	dstStr := Copy(Value, 1, Length(Value));
	iSpacePos := Pos(' ', dstStr);
	if iSpacePos <> 0 then begin
		dstStr := Copy(dstStr, 1, iSpacePos - 1);
	end;
	Result := dstStr;
end;

function IsNumOnly(S: string): boolean;
var
  i, l : integer;
begin
	if (S <> '') Then begin
		Result := True;
	end else begin
		Result := False;
		Exit;
	end;
	l := Length(S);

	for i := 1 to l do begin
		if (ord(S[i]) < ord('0')) or (ord('9') < ord(S[i])) then begin
			if (ord(S[i]) = ord('-')) then begin
				if (i <> 1 ) then begin
					Result := False;
					Break;
				end;
			end else begin
				Result := False;
				Break;
			end;
		end;
	end
end;

Function CalcSecond(FromTime,ToTime:TDateTime):Integer;
var
	StartTime	:COMP;
	EndTime		:COMP;
begin
try
	StartTime	:= TimeStampToMSecs(DateTimeToTimeStamp(FromTime));
	EndTime		:= TimeStampToMSecs(DateTimeToTimeStamp(ToTime));
	Result		:= Trunc((EndTime - StartTime) / 1000);
except
	Result := 0;
end;
end;



{*--------------------------------------*}
{* Purge Log							*}
{*--------------------------------------*}
procedure PurgeLog(pPurgeDays:Integer;pPurgeDir:String);
var
	oSearchRec	:TSearchRec;
	iFind		:Integer;
	sFileNm		:String;
	sDelDay		:String;
	sFileDay	:String;
begin
	try
		sDelDay	:= FormatDateTime('yyyy/mm/dd',(Now - pPurgeDays));

		iFind	:= FindFirst(pPurgeDir+'\*.TXT', faAnyFile, oSearchRec);

		While (iFind = RESULT_OK ) do begin
			sFileNm		:= oSearchRec.Name;
			sFileDay	:= FormatDateTime('yyyy/mm/dd',FileDateToDateTime(oSearchRec.Time));

			if  ( sFileDay <= sDelDay) then begin
				try
					DeleteFile(pPurgeDir +'\' + sFileNm);
				Except
				end;
			end;

			iFind := FindNext(oSearchRec);
		end;
		FindClose(oSearchRec);
	Except
	end;
end;

function HTTPEncode(const AStr: String): String;
const
  NoConversion = ['A'..'Z','a'..'z','*','@','.','_','-'];
var
  Sp, Rp: PChar;
begin
  SetLength(Result, Length(AStr) * 3);
  Sp := PChar(AStr);
  Rp := PChar(Result);
  while Sp^ <> #0 do
  begin
    if Sp^ in NoConversion then
      Rp^ := Sp^
    else
      if Sp^ = ' ' then
        Rp^ := '+'
      else
      begin
        FormatBuf(Rp^, 3, '%%%.2x', 6, [Ord(Sp^)]);
        Inc(Rp,2);
      end;
    Inc(Rp);
    Inc(Sp);
  end;
  SetLength(Result, Rp - PChar(Result));
end;


type
  // CreateProcess() が失敗したときに投げられる例外
  // この関数を置くユニットの interface にでも書いて
  ELaunchFailed = class(Exception)
  end;

function GrabStdOut(sCommandLine: string; pStdIn, pStdOut, pStdErr: TStream): DWORD;
const
  BUFFER_SIZE = 8192;
  WAIT_FOR_READY = 1000;
  WAIT_FOR_RUN = 200;
var
	hReadPipe			:THandle;
	hWritePipe			:THandle;
	hStdInReadPipe		:THandle;
	hStdInWritePipe		:THandle;
	hStdInWritePipeDup	:THandle;
	hErrReadPipe		:THandle;
	hErrWritePipe		:THandle;

	oSecAttr			: TSecurityAttributes;

	StartupInfo			:TStartupInfo;
	ProcessInfo			:TProcessInformation;
	dwStdOut			:DWord;
	dwStdErr			:DWord;
	dwRet				:DWord;
	dwStreamBufferSize	:DWord;
	dwnWritten			:DWord;

	abyBuffer: array[0..BUFFER_SIZE] of Byte;

begin
	ZeroMemory(@oSecAttr, sizeof(TSecurityAttributes));

	with oSecAttr do begin
		nLength := sizeof(TSecurityAttributes);
		lpSecurityDescriptor := nil;
		bInheritHandle := true;
	end;

	Result			:= $ffffffff;
	hReadPipe		:= 0;
	hWritePipe		:= 0;
  	hErrReadPipe	:= 0;
	hErrWritePipe	:= 0;

	CreatePipe(hStdInReadPipe, hStdInWritePipe, @oSecAttr, BUFFER_SIZE);
	DuplicateHandle(GetCurrentProcess(), hStdInWritePipe, GetCurrentProcess(),@hStdInWritePipeDup, 0, false, DUPLICATE_SAME_ACCESS);
	CloseHandle(hStdInWritePipe);
	CreatePipe(hReadPipe, hWritePipe, @oSecAttr, BUFFER_SIZE);

	try
		CreatePipe(hErrReadPipe, hErrWritePipe, @oSecAttr, BUFFER_SIZE);
		try
			ZeroMemory(@StartupInfo, sizeof(TStartupInfo));
			with StartupInfo do begin
				cb		:= sizeof(TStartupInfo);
				dwFlags := STARTF_USESTDHANDLES;
				wShowWindow := SW_HIDE;

				// 標準 IO にパイプの端っこを指定してやる
        		hStdInput	:= hStdInReadPipe;
        		hStdOutput	:= hWritePipe;
        		hStdError	:= hErrWritePipe;
	      	end;

			if CreateProcess(nil, PChar(sCommandLine), @oSecAttr, nil, true, DETACHED_PROCESS,nil, nil, StartupInfo, ProcessInfo) then begin
				// 入力
				if (pStdIn <> Nil) then begin
					WaitForInputIdle(ProcessInfo.hProcess, WAIT_FOR_READY);
	        		dwStreamBufferSize := BUFFER_SIZE;
					while dwStreamBufferSize = BUFFER_SIZE do begin
						dwStreamBufferSize := pStdIn.Read(abyBuffer, BUFFER_SIZE);
						WriteFile(hStdInWritePipeDup, abyBuffer, dwStreamBufferSize, dwnWritten, nil);
					end;
					CloseHandle(hStdInWritePipeDup);
				end;

				try
          			repeat
						// Fixed 2006.08.26: バッファの最後を取りこぼすのを修正
						dwRet := WaitForSingleObject(ProcessInfo.hProcess, WAIT_FOR_RUN);

						if (pStdOut <> Nil) then begin
							PeekNamedPipe(hReadPipe, nil, 0, nil, @dwStdOut, nil);
							if (dwStdOut > 0) then begin
								ReadFile(hReadPipe, abyBuffer, dwStdOut, dwStdOut, nil);
								pStdOut.WriteBuffer(abyBuffer, dwStdOut);
							end;
						end;

						if (pStdErr <> Nil) then begin
							PeekNamedPipe(hErrReadPipe, nil, 0, nil, @dwStdErr, nil);
							if (dwStdErr > 0) then begin
								ReadFile(hErrReadPipe, abyBuffer, dwStdErr, dwStdErr, nil);
								pStdErr.WriteBuffer(abyBuffer, dwStdErr)
							end;
						end;

					until (dwRet = WAIT_OBJECT_0);

					GetExitCodeProcess(ProcessInfo.hProcess, Result);
				finally
					CloseHandle(ProcessInfo.hProcess);
					CloseHandle(ProcessInfo.hThread);
					CloseHandle(hStdInReadPipe);
				end;
			end else begin
		        raise ELaunchFailed.Create('CreateProcess() failed!');
			end;
		finally
			CloseHandle(hErrReadPipe);
			CloseHandle(hErrWritePipe);
		end;
	finally
		CloseHandle(hReadPipe);
		CloseHandle(hWritePipe);
	end;
end;


end.
