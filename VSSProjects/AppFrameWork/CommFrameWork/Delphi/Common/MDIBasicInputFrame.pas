unit MDIBasicInputFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BasicInputFrame, ActnList, ExtCtrls, Buttons;

type
  TFrmMDIBaiscInputFrame = class(TFrmBasicInput)
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private �錾 }
  public
    { Public �錾 }
  end;

var
  FrmMDIBaiscInputFrame: TFrmMDIBaiscInputFrame;

implementation

{$R *.dfm}

procedure TFrmMDIBaiscInputFrame.FormCreate(Sender: TObject);
begin
	inherited;
	Self.WindowState := wsMaximized;
end;

procedure TFrmMDIBaiscInputFrame.FormActivate(Sender: TObject);
begin
	inherited;
 	WindowState := wsMaximized
end;

end.
