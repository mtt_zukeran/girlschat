{*************************************************************************/
--	System			: iBrid Appilcation Frame
--  Sub System Name	: 共通
--	Title			: シングルグリッド入力基本フレーム
--	Progaram ID		: TFrmGridBasicInput
--
--  Creation Date	: 2005.06.01
--  Creater			: i-Brid
--
/*************************************************************************}

// [ Update History ]
{-------------------------------------------------------------------------/

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

/------------------------------------------------------------------------*}
unit GridBasicInputFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BasicInputFrame, ActnList, ExtCtrls, Buttons, Grids,
  IBridGridBase, ITBGrid, BasicDBInputFrame, Oracle;

type
  TFrmGridBasicInput = class(TFrmDBBasicInput)
    sgTable: TITBGrid;
    doaGet: TOracleQuery;
    doaMainte: TOracleQuery;
    procedure FormCreate(Sender: TObject);
    procedure actPF7Execute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actPF5Update(Sender: TObject);
    procedure actPF7Update(Sender: TObject);
    procedure actPF6Execute(Sender: TObject);

	private

	protected

		FbShowOnGetData	: Boolean;
		FvStatus		: Variant;

		function	ExecGetSP:Boolean;
		procedure	GetAfter				;virtual;

		procedure	SetDefaultPickList(ACol: integer);
		procedure	AddZero(iCol,iLen: integer);
		procedure	DelZero(iCol: integer);
	public
 		procedure	InitWin					;override;
		function	ExecUpdate:boolean		;override;
		procedure	MainteParams			;virtual;abstract;
		procedure	GetParams				;virtual;abstract;

	published
		property	ShowOnGetData	: Boolean read FbShowOnGetData write FbShowOnGetData Default False;
  end;

var
  FrmGridBasicInput: TFrmGridBasicInput;

implementation

{$R *.dfm}

uses AppUtils, CustomDataModule, IBRID_APP_CONST;
{*--------------------------------------------------------------*}
{*	Window Action												*}
{*--------------------------------------------------------------*}
procedure TFrmGridBasicInput.FormCreate(Sender: TObject);
begin
	inherited;
	sgTable.ErrorColor := FiColorError;
end;

procedure TFrmGridBasicInput.FormShow(Sender: TObject);
begin
	inherited;
	if (FbShowOnGetData = True) then begin
		ExecGetSP;
	end;
end;

procedure TFrmGridBasicInput.actPF5Update(Sender: TObject);
begin
	FbModified := sgTable.Modified;
	inherited;
end;

procedure TFrmGridBasicInput.actPF7Update(Sender: TObject);
begin
//
end;


procedure TFrmGridBasicInput.actPF6Execute(Sender: TObject);
begin
	sgTable.Modified := False;
	inherited;
end;


procedure TFrmGridBasicInput.actPF7Execute(Sender: TObject);
begin
	if MessageDlg(GetErrorMsg('C102'), mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
		sgTable.DelRecord(sgTable.Row);
	end;
end;
{*--------------------------------------------------------------*}
{*	Override Functions											*}
{*--------------------------------------------------------------*}
procedure TFrmGridBasicInput.InitWin();
begin
	inherited;
	sgTable.ClearList;
end;


procedure TFrmGridBasicInput.AddZero(iCol,iLen: integer);
var
	iRow	: integer;
begin
	for iRow := sgTable.FixedRows To sgTable.RowCount - 1 do begin
		if (Trim(sgTable.Cells[iCol, iRow]) <> '') then begin
			sgTable.Cells[iCol, iRow] := sgTable.AddZero(sgTable.Cells[iCol, iRow], iLen);
		end;
	end;
end;

procedure TFrmGridBasicInput.DelZero(iCol: integer);
var
	iRow	: integer;
begin
	for iRow := sgTable.FixedRows To sgTable.RowCount - 1 do begin
		if not (Length(sgTable.Cells[iCol, iRow]) = 1) then begin
			sgTable.Cells[iCol, iRow] := sgTable.DelZero(sgTable.Cells[iCol, iRow]);
		end;
	end;
end;

procedure TFrmGridBasicInput.SetDefaultPickList(ACol: integer);
var
	i	: integer;
begin
	with sgTable do begin
		SetPickList(ACol, FixedRows);
		for i := FixedRows To RowCount - 1 do begin
			if Cells[ACol, i] <> '' then begin
				if TITBColParam(ColParams[ACol]).PickList.Count > 0 then begin
					if TITBColParam(ColParams[ACol]).CodeList.IndexOf(DelAfterSpace(Cells[ACol, i])) <> - 1 then begin
						Cells[ACol, i] := TITBColParam(ColParams[ACol]).PickList[TITBColParam(ColParams[ACol]).CodeList.IndexOf(DelAfterSpace(Cells[ACol, i]))];
					end;
				end;
			end;
		end;
	end;
end;



function TFrmGridBasicInput.ExecGetSP:Boolean;
begin

	Result := False;

	doaGet.Execute;

	FvStatus := doaGet.GetVariable('PSTATUS');
	if (FvStatus = RETURN_NORMAL) then begin
		GetAfter;
		Result := True;
	end else begin
		CtlErrorMain(FvStatus, nil);
		FbModified := False;
		actPF12.Execute;
	end;
end;


procedure TFrmGridBasicInput.GetAfter;
begin
	if doaGet.GetVariable('PRECORD_COUNT') > 0 then begin
		GetParams;
	end else begin
		sgTable.RecCount := 0;
	end;
	sgTable.Modified := false;
	sgTable.Row := sgTable.FixedRows;
	sgTable.Col := sgTable.FixedCols;

	Self.FbFindRec := True;
end;

function TFrmGridBasicInput.ExecUpdate:Boolean;
begin
	MainteParams;
	doaMainte.Execute;
	FvStatus := doaMainte.GetVariable('PSTATUS');
	if FvStatus = RETURN_NORMAL then begin
		doaMainte.Close;
		doaGet.Execute;
		sgTable.ClearList;
		sgTable.ClearCells;
		GetAfter;

		Result := True;

	end else begin
		CtlErrorMain(FvStatus, nil);
		Result := False;
	end;
end;


end.
