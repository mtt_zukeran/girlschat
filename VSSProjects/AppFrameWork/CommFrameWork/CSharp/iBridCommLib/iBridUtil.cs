﻿using System;

namespace iBridCommLib {
	public class iBridUtil {

		public iBridUtil() {
		}

		public static string GetStringValue(object value) {
			if (value == null) {
				return "";
			} else {
				return value.ToString();
			}
		}

		public static string addZero(String value, int num) {
			if (value.Length < num) {
				for (int i = value.Length;i < num;i++) {
					value = "0" + value;
				}
			}

			return value;
		}


		public static bool IsTrue(Object pObj) {
			int i = 0;
			int.TryParse(pObj.ToString(), out i);
			if (i != 0) {
				return true;
			} else {
				return false;
			}
		}

		/// ------------------------------------------------------------------------
		/// <summary>
		///     指定した精度の数値に四捨五入します。</summary>
		/// <param name="dValue">
		///     丸め対象の倍精度浮動小数点数。</param>
		/// <param name="iDigits">
		///     戻り値の有効桁数の精度。</param>
		/// <returns>
		///     iDigits に等しい精度の数値に四捨五入された数値。</returns>
		/// ------------------------------------------------------------------------
		public static double RoundOff(double dValue,int iDigits) {
			double dCoef = System.Math.Pow(10,iDigits);

			return dValue > 0 ? System.Math.Floor((dValue * dCoef) + 0.5) / dCoef :
								System.Math.Ceiling((dValue * dCoef) - 0.5) / dCoef;
		}
	}
}
