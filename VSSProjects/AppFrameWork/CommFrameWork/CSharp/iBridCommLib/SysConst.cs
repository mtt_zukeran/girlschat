﻿using System;
using System.Text;

namespace iBridCommLib {

	public class SysConst {
		public const string DUMMY = "";
		public const int DB_MAX_ROWS = 2147483647;
#if (PREVIOUS)
		public const int MAX_HTML_BLOCKS = 10;
#else
		public const int MAX_HTML_BLOCKS = 20;
#endif
		public const int NOTHING = -1;
		
		public static readonly Encoding EncodingShiftJIS = Encoding.GetEncoding("Shift_JIS");

		/// <summary>
		/// システムで表示する最初の年 (運用開始年)
		/// </summary>
		public const int SYSTEM_FIRST_YEAR = 2009;
	}

	public enum RunMode {
		Reigst = 1,
		Update = 2,
		Delete = 3,
		Inquiry = 4,
		Execute = 5,
	}

	public enum SysColor {
		LOCK_COLOR = 0xe0e0e0,
		NORMAL_COLOR = 0xffffff,
	}

}
