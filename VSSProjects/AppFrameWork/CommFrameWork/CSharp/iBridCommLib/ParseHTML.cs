// Ver1.0の場合は以下のコメントをはずす。
//#define PREVIOUS
using System;
using System.Web;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace iBridCommLib {

	[Serializable]
	public abstract class ParseUser {
		public const int MAX_TABLE_IDXS = 99;

		public string pattern;
		public RegexOptions option;
		public ParseHTML parseContainer;
		public DataTable[] dataTable;
		public DataTable[] dataRowContainer;

		public bool[] isExistPrevRec;
		public bool[] isExistNextRec;
		public int postAction;
		public bool isCrawler;
		public string sessionId;
		public string domain;
		public string basePath;
		public int currentTableIdx;
		public bool desingDebug;
		public bool isPreviousVer;
		public Dictionary<int,string> datasetDictionary;
		public List<string> divList;
		public List<string> parseErrorList;
		public List<string> parseList;

#if (PREVIOUS)
		public Dictionary<int,int> tableToDatasetDictionary;
		public Dictionary<string,int> tableDictionary;
		public bool noneDisplay;
#else
		public bool checkNewVer = true;
#endif
		public ParseUser() {
		}

		public ParseUser(string pPattern,bool pIsCrawler,string pSessionId,string pDomain,string pBasePath,RegexOptions pOption) {
			pattern = pPattern;
			option = pOption;
			postAction = 0;
			isCrawler = pIsCrawler;
			sessionId = pSessionId;
			domain = pDomain;
			basePath = pBasePath;
			dataTable = new DataTable[MAX_TABLE_IDXS];
			dataRowContainer = new DataTable[MAX_TABLE_IDXS];
			isExistPrevRec = new bool[MAX_TABLE_IDXS];
			isExistNextRec = new bool[MAX_TABLE_IDXS];
			currentTableIdx = -1;
			desingDebug = false;
			isPreviousVer = false;
			datasetDictionary = new Dictionary<int,string>();
			divList = new List<string>();
			parseErrorList = new List<string>();
			parseList = new List<string>();
#if (PREVIOUS)
			tableToDatasetDictionary = new Dictionary<int,int>();
			tableDictionary = new Dictionary<string,int>();
			noneDisplay = false;
#endif
		}

		public void SetDataRow(int pTblIndex,DataRow pDataRow) {
			if (pDataRow != null) {
				this.dataRowContainer[pTblIndex] = pDataRow.Table.Clone();
				this.dataRowContainer[pTblIndex].Rows.Clear();
				this.dataRowContainer[pTblIndex].ImportRow(pDataRow);
			} else {
				if (this.dataRowContainer[pTblIndex] != null) {
					this.dataRowContainer[pTblIndex].Rows.Clear();
				}
			}
		}

		public DataRow GetDataRow(int pTblIndex) {
			DataRow oDataRow = null;
			if (this.dataRowContainer[pTblIndex] != null) {
				if (this.dataRowContainer[pTblIndex].Rows.Count > 0) {
					oDataRow = this.dataRowContainer[pTblIndex].Rows[0];
				}
			}
			return oDataRow;
		}

		public abstract byte[] Perser(string pTag,string pArgument);
		public virtual string AfterParse(string pParsedValue) {
			return pParsedValue;
		}
	}
	[Serializable]
	public class ParseHTML {
		public const int MAX_TABLE_IDXS = 99;

		public int[] loopCount;
		public int[] currentRecPos;
		public bool noTrans;
		public bool IsPcDesign = false;
		public bool ignoreParseEmoji = false;

		public ParseUser parseUser;
		public Regex rgxTag;
		public Regex rgxArgumentNext;
		public Regex rgxArgumentExt;

		public ParseHTML() {
		}

		public ParseHTML(ParseUser pParseUser) {
			parseUser = pParseUser;
			loopCount = new int[MAX_TABLE_IDXS];
			currentRecPos = new int[MAX_TABLE_IDXS];
			noTrans = false;
			rgxTag = new Regex(@"(\$[^(]*)",RegexOptions.Compiled);
			rgxArgumentNext = new Regex(@"((\$\w{1,})(\[[^\]]+\]);)",RegexOptions.Compiled);
			rgxArgumentExt = new Regex(@"(<[^>]*)@(checked|selected)@([^>]*)>",RegexOptions.Compiled | RegexOptions.IgnoreCase);
		}

		public byte[] DataSetLoop(int pTblIdx,string pHtmlSource,int pStartPos,ref int pCopiedPos) {
			string sSrc = pHtmlSource.Substring(pStartPos,pHtmlSource.Length - pStartPos);
			string sParse = "";

			Regex rgx;
			rgx = new Regex(@"\$DATASET_LOOP_END" + pTblIdx.ToString());

			if (parseUser.isPreviousVer == false) {
				pTblIdx = parseUser.currentTableIdx;
			}

			Match rgxMatch = rgx.Match(sSrc);
			if (rgxMatch.Success) {
				string sLoopBlock = sSrc.Substring(0,rgxMatch.Index);
				pCopiedPos = pStartPos + rgxMatch.Index + rgxMatch.Length;

				for (currentRecPos[pTblIdx] = 0;currentRecPos[pTblIdx] < loopCount[pTblIdx];currentRecPos[pTblIdx]++) {
					parseUser.SetDataRow(pTblIdx,parseUser.dataTable[pTblIdx].Rows[currentRecPos[pTblIdx]]);
					//parseUser.dataRow[pTblIdx] = parseUser.dataTable[pTblIdx].Rows[currentRecPos[pTblIdx]];
					sParse = sParse + Parse(sLoopBlock);
				}
			}

			Encoding encSjis = Encoding.GetEncoding(932); // shift-jis
			byte[] byteArray = encSjis.GetBytes(sParse);
			return byteArray;
		}
		public string ParseDirect(string pHtmlSource,DataSet pDataSet,int pTableIndex) {
			string sValue = string.Empty;
			if (pDataSet == null || pDataSet.Tables.Count == 0)
				return sValue;

			int prevTableIndex = parseUser.currentTableIdx;
			DataTable oPrevTable = parseUser.dataTable[pTableIndex];
			int iPrevLoopCount = loopCount[pTableIndex];
			DataTable oPrevDataRow = parseUser.dataRowContainer[pTableIndex];
			try {
				parseUser.currentTableIdx = pTableIndex;
				parseUser.dataTable[parseUser.currentTableIdx] = pDataSet.Tables[0];
				loopCount[parseUser.currentTableIdx] = pDataSet.Tables[0].Rows.Count;

				// HTML作成
				StringBuilder oValueBuilder = new StringBuilder();
				oValueBuilder.AppendLine("$DATASET_LOOP_START0;");
				oValueBuilder.AppendLine(pHtmlSource);
				oValueBuilder.AppendLine("$DATASET_LOOP_END0;");
				sValue = Parse(oValueBuilder.ToString());
			} finally {
				parseUser.currentTableIdx = prevTableIndex;
				parseUser.dataTable[pTableIndex] = oPrevTable;
				parseUser.dataRowContainer[pTableIndex] = oPrevDataRow;
				loopCount[pTableIndex] = iPrevLoopCount;
			}

			return sValue;
		}

		private static readonly Regex _regexPict = new Regex(@"(?<pict>\$x.{4};)",RegexOptions.Compiled);
		public string ParseEmoji(string pHtmlSource) {
			Encoding oEnc = Encoding.GetEncoding(932);
			return _regexPict.Replace(pHtmlSource,new MatchEvaluator(delegate(Match pMatch) {
				return oEnc.GetString(parseUser.Perser(pMatch.Value,string.Empty));
			}));
		}
		private static readonly Regex _regexSpecialChar = new Regex(@"(\$180);",RegexOptions.Compiled);
		public string ParseSpecialChar(string pHtmlSource) {
			Encoding oEnc = Encoding.GetEncoding(932);
			return _regexSpecialChar.Replace(pHtmlSource,new MatchEvaluator(delegate(Match pMatch){
				return oEnc.GetString(parseUser.Perser(pMatch.Groups[1].Value,string.Empty));
			}));
		}

		public string Parse(string pHtmlSource) {
			string sResult = "";
			bool bRepStartNoTrans = false;
			bool bRepEndNoTrans = false;
			bool bBlocking = false;
			using (MemoryStream memStrm = new MemoryStream()) {
				Encoding encSjis = Encoding.GetEncoding(932); // shift-jis

				int iCopiedPos = 0,len = 0;
				byte[] orgBytes;
				bool bLoopTag = false;
				int iLoopStartPos = 0;
				int iLoopEndPos = -1;

				Regex rgx = new Regex(@parseUser.pattern,parseUser.option);
				Match rgxMatch = rgx.Match(pHtmlSource);

				while (rgxMatch.Success) {

					bLoopTag = false;

					if (rgxMatch.Value.Equals("$NO_TRANS_END;")) {
						noTrans = false;
						bRepEndNoTrans = true;
						rgxMatch = rgxMatch.NextMatch();
						continue;
					}

					if (noTrans) {
						rgxMatch = rgxMatch.NextMatch();
						continue;
					}

					if (rgxMatch.Value.Equals("$NO_TRANS_START;")) {
						noTrans = true;
						bRepStartNoTrans = true;
						rgxMatch = rgxMatch.NextMatch();
						continue;
					}

					if ((rgxMatch.Index >= iLoopStartPos) && (rgxMatch.Index <= iLoopEndPos)) {
						bLoopTag = true;
					}

					// 絵文字またはOriginalタグが見つかった位置までメモリ・ストリームに書き出す
					if (!bBlocking) {
						len = rgxMatch.Index - iCopiedPos;
						if (len > 0) {
							// Shift-JISでストリームに書き出す
							orgBytes = encSjis.GetBytes(pHtmlSource.Substring(iCopiedPos,len));
							memStrm.Write(orgBytes,0,orgBytes.Length);
						}
					}

					if ((!rgxMatch.Value.Equals("$NO_TRANS_START;")) &&
						(!rgxMatch.Value.Equals("$NO_TRANS_END;")) &&
						(!rgxMatch.Value.StartsWith("$DATASET_LOOP_START")) &&
						(!bLoopTag)) {

						string sTag,sArgument = "";

						// 表示ﾌﾞﾛｯｸされている場合,DIVタグのNested処理のみ行う。
						if (bBlocking && !rgxMatch.Value.ToLower().StartsWith("<div") && !rgxMatch.Value.ToLower().StartsWith("</div")) {
							iCopiedPos = rgxMatch.Index + rgxMatch.Length;
							rgxMatch = rgxMatch.NextMatch();
							continue;
						}


						if (rgxMatch.Value.StartsWith("<!--")) {
							parseUser.parseErrorList.Add("COMMENT TAG ");
							iCopiedPos = rgxMatch.Index + rgxMatch.Length;
							rgxMatch = rgxMatch.NextMatch();
							continue;

						} else if (rgxMatch.Value.StartsWith("<div") || rgxMatch.Value.StartsWith("<DIV")) {
							if (!bBlocking) {
								if (rgxMatch.Groups[4].Value.Equals("")) {
									sTag = rgxMatch.Value;
									parseUser.divList.Add("0");
								} else {
									if (!ParseArgument(rgxMatch.Groups[4].Value,out sTag,out sArgument)) {
										sTag = rgxMatch.Groups[4].Value;
									}
									parseUser.divList.Add("1");
								}
							} else {
								// 表示ﾌﾞﾛｯｸ中のDIVﾀｸﾞはNested処理のみ行う。
								sTag = "<div>";
								parseUser.divList.Add("0");
							}
						} else if (rgxMatch.Value.StartsWith("</div") || rgxMatch.Value.StartsWith("</DIV")) {
							if (parseUser.divList.Count > 0) {
								if (parseUser.divList[parseUser.divList.Count - 1].Equals("0")) {
									parseUser.divList.RemoveAt(parseUser.divList.Count - 1);
									sTag = rgxMatch.Value;
								} else {
									parseUser.divList.RemoveAt(parseUser.divList.Count - 1);
									iCopiedPos = rgxMatch.Index + rgxMatch.Length;
									bBlocking = false;
									for (int i = 0;i < parseUser.divList.Count;i++) {
										if (parseUser.divList[i].Equals("2")) {
											bBlocking = true;
											break;
										}
									}
									rgxMatch = rgxMatch.NextMatch();
									continue;
								}
							} else {
								parseUser.parseErrorList.Add("DIV TAG UNMUCH");
								iCopiedPos = rgxMatch.Index + rgxMatch.Length;
								rgxMatch = rgxMatch.NextMatch();
								continue;
							}
						} else if (rgxMatch.Value.StartsWith("<")) {
							sTag = rgxMatch.Value;
						} else if (rgxMatch.Value.StartsWith("$x")) {
							if (ignoreParseEmoji == false) {
								sTag = rgxMatch.Value;
							} else {
								orgBytes = encSjis.GetBytes(rgxMatch.Value);
								memStrm.Write(orgBytes,0,orgBytes.Length);
								iCopiedPos = rgxMatch.Index + rgxMatch.Length;
								rgxMatch = rgxMatch.NextMatch();
								continue;
							}
						} else {
							sTag = rgxMatch.Groups[2].Value;
							if (rgxMatch.Groups.Count >= 4) {
								if (rgxMatch.Groups[3].Value.StartsWith("$") && rgxMatch.Groups[3].Value.EndsWith(";")) {
									sArgument = rgxMatch.Groups[3].Value.Replace('[','(');
									sArgument = sArgument.Replace(']',')');
									string[] sArg = sArgument.Split(',');
									for (int i = 0;i < sArg.Length;i++) {
										sArg[i] = this.Parse(sArg[i]);
									}
									sArgument = string.Join(",",sArg);
								} else {
									sArgument = rgxMatch.Groups[3].Value;
								}
							}
						}

						byte[] byteArray = parseUser.Perser(sTag,sArgument);
						bBlocking = false;
						for (int i = 0;i < parseUser.divList.Count;i++) {
							if (parseUser.divList[i].Equals("2")) {
								bBlocking = true;
								break;
							}
						}

						if (bBlocking == false) {
							if (byteArray.Length > 0) {
								memStrm.Write(byteArray,0,byteArray.Length);
							} else {
								orgBytes = encSjis.GetBytes(rgxMatch.Value);
							}
							iCopiedPos = rgxMatch.Index + rgxMatch.Length;
						}

					} else if (rgxMatch.Value.StartsWith("$DATASET_LOOP_START")) {
						iLoopStartPos = rgxMatch.Index + rgxMatch.Length;
						string sInedx = rgxMatch.Value.Replace("$DATASET_LOOP_START","").Replace(";","");
						int iTableIdx;
						int.TryParse(sInedx,out iTableIdx);
						byte[] byteArray = DataSetLoop(iTableIdx,pHtmlSource,iLoopStartPos,ref iCopiedPos);
						iLoopEndPos = iCopiedPos;
						if (byteArray.Length > 0) {
							memStrm.Write(byteArray,0,byteArray.Length);
						} else {
							orgBytes = encSjis.GetBytes(rgxMatch.Value);
						}
					} else {
						iCopiedPos = rgxMatch.Index + rgxMatch.Length;
					}

					rgxMatch = rgxMatch.NextMatch();
				}

				// マッチ以降をメモリ・ストリームに書き出す
				if (iCopiedPos < pHtmlSource.Length) {
					orgBytes = encSjis.GetBytes(pHtmlSource.Substring(iCopiedPos));
					memStrm.Write(orgBytes,0,orgBytes.Length);
				}
				sResult = encSjis.GetString(memStrm.ToArray()); // 文字列に戻す
			}

			if (IsPcDesign == false) {
				sResult = rgxArgumentExt.Replace(sResult,new MatchEvaluator(RegexMatchExt));
			}

			if (bRepStartNoTrans)
				sResult = sResult.Replace("$NO_TRANS_START;","");

			if (bRepEndNoTrans)
				sResult = sResult.Replace("$NO_TRANS_END;","");

			if (parseUser.isCrawler) {
				sResult = ChangePathInfo(sResult);
				sResult = Regex.Replace(sResult, @"(src[=\s]+[""]*[\.]*[\/]*)((data|image|extensionres\/beautyclock)\/.{4}\/[^>""]+)", new MatchEvaluator(RegexImagePath), RegexOptions.IgnoreCase | RegexOptions.Compiled);
			}
			return parseUser.AfterParse(sResult);
		}
		public string ChangePathInfo(string pValue) {
			//return Regex.Replace(pValue,@"(href[=\s]+[""]*[\.]*[\/]*)([\w\/()]*.aspx[^<>""]*)",new MatchEvaluator(RegexMatchPathInfo),RegexOptions.IgnoreCase | RegexOptions.Compiled);
			return Regex.Replace(pValue,@"(href[=\s]+[""]*)([\w\/()\.]*.aspx[^<>""]*)",new MatchEvaluator(RegexMatchPathInfo),RegexOptions.IgnoreCase | RegexOptions.Compiled);
		}

		public string RegexMatchPathInfo(Match match) {
			string sHref = match.Groups[1].Value;
			string sValue = match.Groups[2].Value;

			if (sValue.ToLower().IndexOf(parseUser.basePath) < 0) {
				sHref = sHref + string.Format("http://{0}/{1}",parseUser.domain,parseUser.basePath) + "/";
			}

			int iIdx = sValue.LastIndexOf("/");
			if (iIdx < 0) {
				sHref = sHref + SysPrograms.ChangeQueryToPathInfo(sValue,true);
			} else {
				sHref = sHref + sValue.Substring(0,iIdx) + SysPrograms.ChangeQueryToPathInfo(sValue.Substring(iIdx),true);
			}
			if (!SysPrograms.Expression("/crw/1",sHref)) {
				sHref = sHref + "/crw/1";
			}
			return sHref;
		}

		private string RegexImagePath(Match match) {
			string sTag = match.Groups[1].Value;
			string sPath = match.Groups[2].Value;
			return string.Format("src=\"/{0}/{1}",parseUser.basePath,sPath);
		}


		public bool ParseArgument(string pTag,out string pTagNm,out string pArgument) {
			pTagNm = "";
			pArgument = "";
			Regex rgx = rgxTag;

			Match rgxAMatch = rgx.Match(pTag);
			if (rgxAMatch.Success) {
				pTagNm = rgxAMatch.Groups[1].Value;
				if (pTagNm.Equals(pTag)) {
					// No Argument
					return false;
				}

				//変数名()を取り除いた値をArgumentにする
				string sArg = pTag.Replace(pTagNm,"");
				if (sArg.StartsWith("(")) {
					sArg = sArg.Substring(1,sArg.Length - 1);
				}
				if (sArg.EndsWith(")")) {
					sArg = sArg.Substring(0,sArg.Length - 1);
				}
				pArgument = this.Parse(sArg);
			}
			return rgxAMatch.Success;
		}

		private string RegexMatchExt(Match match) {
			return match.Groups[1].Value + match.Groups[3].Value + " " + match.Groups[2].Value + ">";
		}
	}

}

