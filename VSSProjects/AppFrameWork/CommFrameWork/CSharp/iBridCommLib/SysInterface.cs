using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.Net.Cache;

namespace iBridCommLib {

	public class SysInterface {

		public static bool SendHttpRequest(string pUrl,ref string pResponse) {

			pResponse = "";

			try {
				Encoding enc = Encoding.GetEncoding("Shift_JIS");

				WebRequest req = WebRequest.Create(pUrl);
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st,enc)) {
					pResponse = sr.ReadToEnd();
					sr.Close();
					st.Close();
				}
				return true;
			} catch {
				return false;
			}
		}

		public static bool FTPUpload(string pSrcFullPath,string pDestIP,string pDestFileNm,string pUserID,string pPassword) {
			try {
				Uri u = new Uri(string.Format("ftp://{0}/{1}",pDestIP,pDestFileNm));

				//FtpWebRequestの作成
				FtpWebRequest ftpReq = (System.Net.FtpWebRequest)WebRequest.Create(u);

				ftpReq.Credentials = new System.Net.NetworkCredential(pUserID,pPassword);
				ftpReq.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
				ftpReq.KeepAlive = false;
				ftpReq.UseBinary = true;
				ftpReq.UsePassive = false;

				System.IO.Stream reqStrm = ftpReq.GetRequestStream();
				System.IO.FileStream fs = new System.IO.FileStream(pSrcFullPath,System.IO.FileMode.Open,System.IO.FileAccess.Read);

				byte[] buffer = new byte[1024];
				while (true) {
					int readSize = fs.Read(buffer,0,buffer.Length);
					if (readSize == 0)
						break;
					reqStrm.Write(buffer,0,readSize);
				}
				fs.Close();
				reqStrm.Close();

				//FtpWebResponseを取得
				FtpWebResponse ftpRes = (FtpWebResponse)ftpReq.GetResponse();
				bool bRet = (ftpRes.StatusCode == FtpStatusCode.ClosingData);
				ftpRes.Close();
				return bRet;
			} catch {
				return false;
			}
		}

		public static bool FtpDownload(string pSrcFullPath,string pDestIP,string pDestFileNm,string pUserID,string pPassword) {
			try {
				Uri u = new Uri(string.Format("ftp://{0}/{1}",pDestIP,pDestFileNm));

				//FtpWebRequestの作成
				FtpWebRequest ftpReq = (System.Net.FtpWebRequest)WebRequest.Create(u);

				ftpReq.Credentials = new System.Net.NetworkCredential(pUserID,pPassword);
				ftpReq.Method = System.Net.WebRequestMethods.Ftp.DownloadFile;
				ftpReq.KeepAlive = false;
				ftpReq.UseBinary = true;
				ftpReq.UsePassive = false;

				//FtpWebResponseを取得
				FtpWebResponse ftpRes = (FtpWebResponse)ftpReq.GetResponse();

				Stream resStrm = ftpRes.GetResponseStream();

				FileStream fs = new FileStream(pSrcFullPath,FileMode.Create,FileAccess.Write);

				byte[] buffer = new byte[1024];
				while (true) {
					int readSize = resStrm.Read(buffer,0,buffer.Length);
					if (readSize == 0)
						break;
					fs.Write(buffer,0,readSize);
				}
				fs.Close();
				resStrm.Close();

				bool bRet = (ftpRes.StatusCode == FtpStatusCode.ClosingData);
				ftpRes.Close();
				return bRet;
			} catch {
				return false;
			}
		}
	}

}
