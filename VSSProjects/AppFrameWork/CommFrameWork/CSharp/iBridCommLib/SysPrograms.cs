﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text;
using System.Net;
using Microsoft.Win32.SafeHandles;
using System.Web.UI.MobileControls;

namespace iBridCommLib {

	public class SysPrograms {
		private const int YEAR = 6;

		public static void SetupDay(DropDownList pYYYY,DropDownList pMM,DropDownList pDD,bool pAddDummy) {
			// 年選択肢をリストに設定
			SetupYear(pYYYY);

			pMM.Items.Clear();
			if (pAddDummy) {
				pMM.Items.Add(new ListItem("---","13"));
			}
			for (int i = 1;i <= 12;i++) {
				pMM.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}

			pDD.Items.Clear();
			if (pAddDummy) {
				pDD.Items.Add(new ListItem("---","32"));
			}
			for (int i = 1;i <= 31;i++) {
				pDD.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
		}
		public static void SetupFromToDay(DropDownList pFromYYYY,DropDownList pFromMM,DropDownList pFromDD,DropDownList pToYYYY,DropDownList pToMM,DropDownList pToDD,bool pAddDummy) {
			SetupFromToDay(pFromYYYY,pFromMM,pFromDD,pToYYYY,pToMM,pToDD,pAddDummy,false);
		}
		public static void SetupFromToDay(DropDownList pFromYYYY,DropDownList pFromMM,DropDownList pFromDD,DropDownList pToYYYY,DropDownList pToMM,DropDownList pToDD,bool pAddDummy,bool pAddFuture) {
			// 年選択肢をリストに設定
			SetupYear(pFromYYYY,pAddFuture);
			SetupYear(pToYYYY,pAddFuture);

			pFromMM.Items.Clear();
			pToMM.Items.Clear();

			if (pAddDummy) {
				pFromMM.Items.Add(new ListItem("---","00"));
				pToMM.Items.Add(new ListItem("---","13"));
			}
			for (int i = 1;i <= 12;i++) {
				pFromMM.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				pToMM.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}

			pFromDD.Items.Clear();
			pToDD.Items.Clear();

			if (pAddDummy) {
				pFromDD.Items.Add(new ListItem("---","00"));
				pToDD.Items.Add(new ListItem("---","32"));
			}

			for (int i = 1;i <= 31;i++) {
				pFromDD.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				pToDD.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}

		}

		public static void SetupFromToDay(SelectionList pFromYYYY,SelectionList pFromMM,SelectionList pFromDD,SelectionList pToYYYY,SelectionList pToMM,SelectionList pToDD,bool pAddDummy) {
			int iCurYear = int.Parse(DateTime.Now.ToString("yyyy"));
			if (pFromYYYY != null) {
				pFromYYYY.Items.Clear();
				for (int i = 0;i <= YEAR;i++) {
					pFromYYYY.Items.Add(new MobileListItem(iCurYear.ToString()));
					iCurYear--;
				}
			}

			iCurYear = int.Parse(DateTime.Now.ToString("yyyy"));
			if (pToYYYY != null) {
				pToYYYY.Items.Clear();
				for (int i = 0;i <= YEAR;i++) {
					pToYYYY.Items.Add(new MobileListItem(iCurYear.ToString()));
					iCurYear--;
				}
			}

			pFromMM.Items.Clear();
			pToMM.Items.Clear();

			if (pAddDummy) {
				pFromMM.Items.Add(new MobileListItem("--",""));
				pToMM.Items.Add(new MobileListItem("--",""));
			}
			for (int i = 1;i <= 12;i++) {
				pFromMM.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
				pToMM.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
			}

			pFromDD.Items.Clear();
			pToDD.Items.Clear();

			if (pAddDummy) {
				pFromDD.Items.Add(new MobileListItem("--",""));
				pToDD.Items.Add(new MobileListItem("--",""));
			}

			for (int i = 1;i <= 31;i++) {
				pFromDD.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
				pToDD.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
			}
		}

		public static void SetupFromToDayTime(DropDownList pFromYYYY,DropDownList pFromMM,DropDownList pFromDD,DropDownList pFromHH,DropDownList pToYYYY,DropDownList pToMM,DropDownList pToDD,DropDownList pToHH,bool pAddDummy) {
			SetupFromToDayTime(pFromYYYY,pFromMM,pFromDD,pFromHH,pToYYYY,pToMM,pToDD,pToHH,pAddDummy,false);
		}

		public static void SetupFromToDayTime(DropDownList pFromYYYY,DropDownList pFromMM,DropDownList pFromDD,DropDownList pFromHH,DropDownList pToYYYY,DropDownList pToMM,DropDownList pToDD,DropDownList pToHH,bool pAddDummy,bool pAddFuture) {
			SetupFromToDay(pFromYYYY,pFromMM,pFromDD,pToYYYY,pToMM,pToDD,pAddDummy,pAddFuture);
			if (pAddDummy) {
				pFromHH.Items.Add(new ListItem("---","00"));
				pToHH.Items.Add(new ListItem("---","24"));
			}

			for (int i = 0;i <= 23;i++) {
				pFromHH.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				pToHH.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
		}

		public static void SetupTime(DropDownList pFromHH,DropDownList pToHH) {
			if (pFromHH != null) {
				pFromHH.Items.Add(new ListItem("---","00"));
			}
			if (pToHH != null) {
				pToHH.Items.Add(new ListItem("---","23"));
			}
			for (int i = 0;i <= 23;i++) {
				if (pFromHH != null) {
					pFromHH.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				}
				if (pToHH != null) {
					pToHH.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				}
			}
		}

		/// <summary>
		/// 指定されたリストオブジェクトの選択肢に年を設定する
		/// （現在年～2009年）
		/// </summary>
		/// <param name="pYYYY"></param>
		public static void SetupYear(DropDownList pYYYY) {
			SetupYear(pYYYY, false);
		}

		/// <summary>
		/// 指定されたリストオブジェクトの選択肢に年を設定する
		/// </summary>
		/// <param name="pYYYY">リスト</param>
		/// <param name="pAddFuture">未来年表示フラグ</param>
		public static void SetupYear(DropDownList pYYYY,bool pAddFuture) {
			int iCurYear = int.Parse(DateTime.Now.ToString("yyyy"));
			int iStartYear = iCurYear;
			int iEndYear = SysConst.SYSTEM_FIRST_YEAR;
			// 現在年がシステムの開始年より前の場合の対応
			if (iCurYear < SysConst.SYSTEM_FIRST_YEAR) {
				iStartYear = SysConst.SYSTEM_FIRST_YEAR;
				iEndYear = iCurYear;
			}
			// 未来年を表示する場合の年追加
			if (pAddFuture) {
				iStartYear += YEAR;
			}
			// リストオブジェクトが存在しない場合は何もしない
			if (pYYYY == null) {
				return;
			}
			pYYYY.Items.Clear();
			for (int i = iStartYear;i >= iEndYear;i--) {
				pYYYY.Items.Add(new ListItem(i.ToString()));
			}
		}

		public static string IsDateToLastDate(string pDate) {
			DateTime retDate = DateTime.Now;


			if (pDate.Length == 10) {
				try {
					retDate = DateTime.ParseExact(pDate,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
					return pDate;
				} catch {
					string[] aryDate = pDate.Split('/');
					bool bNum = true;

					for (int i = 0;i < aryDate.Length;i++) {
						if (!IsNumber(aryDate[0])) {
							bNum = false;
							break;
						}
					}
					if (bNum && aryDate.Length >= 1) {
						if (aryDate[0].Length == 4 && int.Parse(aryDate[1]) >= 1 && int.Parse(aryDate[1]) <= 12) {
							retDate = DateTime.ParseExact(pDate.Substring(0,7) + "/01","yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
							return retDate.AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");
						}
					}
				}
			}
			return retDate.ToString("YYYY/MM/DD");
		}

		public static bool IsNumber(string pStr) {
			Regex regex = new System.Text.RegularExpressions.Regex(@"^[0-9]+$",RegexOptions.Compiled);
			return regex.IsMatch(pStr);
		}

		public static void SqlAppendWhere(string pQuery,ref string pWhere) {
			if (pWhere.Length == 0) {
				pWhere = "  WHERE " + pQuery;
			} else {
				pWhere = pWhere + " AND " + pQuery;
			}
		}

		public static string RegexMatchEvaluator1(Match match) {
			return match.Value.Trim();
		}

		public static string RegexMatchEvaluator2(Match match) {
			return match.Value.Trim();
		}

		public static string RegexMatchEvaluator3(Match match) {
			return match.Value.Trim();
		}

		public static string RegexMatchEvaluator5(Match match) {
			return Environment.NewLine;
		}

		public static void SeparateHtml(string pDoc,int pMaxArraySize,out string[] pHtmlDoc,out int pBlockCount) {
			SeparateHtml(pDoc,pMaxArraySize,false,false,out pHtmlDoc,out pBlockCount);
		}

		public static void SeparateSiteHtml(string pDoc,int pMaxArraySize,out string[] pHtmlDoc,out int pBlockCount) {
			SeparateHtml(pDoc,pMaxArraySize,true,false,out pHtmlDoc,out pBlockCount);
		}

		public static void SeparateSiteHtmlText(string pDoc,int pMaxArraySize,out string[] pHtmlDoc,out int pBlockCount) {
			SeparateHtml(pDoc,pMaxArraySize,true,true,out pHtmlDoc,out pBlockCount);
		}

		private static void SeparateHtml(string pDoc,int pMaxAarrySize,bool pValueReplace,bool pTextFlag,out string[] pHtmlDoc,out int pBlockCount) {
			pHtmlDoc = new string[pMaxAarrySize];
			pDoc = pDoc.Replace("middle","center");
			pDoc = pDoc.Replace("<TBODY>","");
			pDoc = pDoc.Replace("</TBODY>","");
			pDoc = pDoc.Replace("utn=\"\">","utn>");
			Regex regex4;
			string sAllDeleteSpaceFlag = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["PinEditDeleteSpaceFlag"]);

			if (sAllDeleteSpaceFlag.Equals("1")) {
				regex4 = new Regex(@"(\s<.*?>)|(<.*?>\s?)",RegexOptions.Compiled);
			} else {
				regex4 = new Regex(@"((\s<[^\/]*>))",RegexOptions.Compiled);
			}
			pDoc = pTextFlag ? pDoc : regex4.Replace(pDoc,new MatchEvaluator(RegexMatchEvaluator3));

			pBlockCount = pDoc.Length / 994;
			if ((pDoc.Length % 994) > 0) {
				pBlockCount++;
			}

			Regex regex1 = new Regex(@"(\$\w{1,}\(.+?\); |\$\w{1,}; )",RegexOptions.Compiled);
			Regex regex2 = new Regex(@"(\s</.+?>)",RegexOptions.Compiled);
			Regex regex3 = new Regex(@"((?<!value=)\$\w{1,}\(.+?\);\s?|(?<!value=)\$\w{1,};\s?)",RegexOptions.Compiled);

			if (pBlockCount > pMaxAarrySize) {
				pBlockCount = pMaxAarrySize;
			}
			int iPos = 0;
			pDoc = iBridUtil.GetStringValue(pDoc);

			for (int i = 0;i < pBlockCount;i++) {
				if ((iPos + 1006) <= pDoc.Length) {
					pHtmlDoc[i] = SysPrograms.Substring(pDoc.Substring(iPos,1006),994);
					iPos += pHtmlDoc[i].Length;
					if (!pTextFlag) {
						if (pValueReplace) {
							pHtmlDoc[i] = regex3.Replace(pHtmlDoc[i],new MatchEvaluator(RegexMatchEvaluator3));
						} else {
							pHtmlDoc[i] = regex1.Replace(pHtmlDoc[i],new MatchEvaluator(RegexMatchEvaluator1));
						}
						pHtmlDoc[i] = regex2.Replace(pHtmlDoc[i],new MatchEvaluator(RegexMatchEvaluator2));
					}
					pHtmlDoc[i] = pHtmlDoc[i].Replace("OPTION selected","OPTION");
				} else {
					pHtmlDoc[i] = pDoc.Substring(iPos,pDoc.Length - iPos);
					if (!pTextFlag) {
						if (pValueReplace) {
							pHtmlDoc[i] = regex3.Replace(pHtmlDoc[i],new MatchEvaluator(RegexMatchEvaluator3));
						} else {
							pHtmlDoc[i] = regex1.Replace(pHtmlDoc[i],new MatchEvaluator(RegexMatchEvaluator1));
						}
						pHtmlDoc[i] = regex2.Replace(pHtmlDoc[i],new MatchEvaluator(RegexMatchEvaluator2));
					}
					pHtmlDoc[i] = pHtmlDoc[i].Replace("OPTION selected","OPTION");
					pBlockCount = i + 1;
					break;
				}
			}
		}

		private static readonly Regex substringRegex = new Regex(@"^\$x[0-9A-FM]{4};$",RegexOptions.Compiled);
		/// <summary>
		/// 文字列インスタンスから部分インスタンスを取得します。 
		/// 絵文字ｺｰﾄﾞ($x0000)が末尾に含まれる場合は絵文字ｺｰﾄﾞを保障する形で部分インスタンスを取得します。 
		/// </summary>
		/// <param name="pSubject">対象文字列</param>
		/// <param name="pLength">取得する文字長</param>
		/// <param name="pResultLength">処理結果の文字長</param>
		/// <returns>処理結果文字列</returns>
		public static string Substring(string pSubject,int pLength) {

			// assertion
			if (pLength < 0)
				throw new ArgumentException("pLength");
			if (string.IsNullOrEmpty(pSubject))
				return pSubject;

			// implement
			string result = string.Empty;

			if (pSubject.Length > pLength) {
				int pictStartIndex = pSubject.IndexOf("$",Math.Max(pLength - 6,0));
				if ((pictStartIndex > 0) && (pictStartIndex < pLength)) {
					if (pictStartIndex + 7 <= pSubject.Length) {
						if (substringRegex.IsMatch(pSubject.Substring(pictStartIndex,7))) {
							pLength = pictStartIndex + 7;
						}
					}
				}
			}

			// return
			result = (pSubject.Length < pLength) ? pSubject : OmitBreakShitIn(pSubject.Substring(0,pLength));
			return result;
		}

		public static bool IsWithinByteCount(string pValue,int pByteCount) {
			return IsWithinByteCount(pValue,pByteCount,Encoding.UTF8);
		}

		public static bool IsWithinByteCount(string pValue,int pByteCount,Encoding pEncoding) {
			return pEncoding.GetByteCount(pValue) <= pByteCount;
		}

		public static string OmitBreakShitIn(string pSource) {
			Encoding oEncSjis = Encoding.GetEncoding(932);
			byte[] byteSjisSource = oEncSjis.GetBytes(pSource);
			int iPos = 0;
			int iIn = 0;

			for (iPos = 0;iPos < byteSjisSource.Length;iPos++) {
				byte byteFirst = byteSjisSource[iPos];
				byte byteSecond;
				if (iPos + 1 < byteSjisSource.Length) {
					byteSecond = byteSjisSource[iPos + 1];
				} else {
					byteSecond = 0x00;
				}
				int iEmojiCode = (((int)byteFirst) << 8) | (int)byteSecond;

				// SHIT-JIS全角2Byteの場合読みとばし 
				if (((0x81 <= byteFirst) && (byteFirst <= 0x9F)) || ((0xE0 <= byteFirst) && (byteFirst <= 0xEF))) {
					iPos++;
					continue;
				}
				if (byteFirst == 0x1b) {
					iIn = iPos;
				} else if (byteFirst == 0x0f) {
					iIn = 0;
				}
			}
			if (iIn == 0) {
				return pSource;
			} else {
				string sResult = oEncSjis.GetString(byteSjisSource,0,iIn);
				return sResult;
			}
		}

		public static void JoinLabelCells(GridView pGrd,int pCol) {
			JoinCells(pGrd,1,pCol);
		}

		public static void JoinLinkCells(GridView pGrd,int pCol) {
			JoinCells(pGrd,2,pCol);
		}

		private static void JoinCells(GridView pGrd,int pType,int pCol) {
			int iRow = pGrd.Rows.Count;
			int iBaseidx = 0;
			string sBase = "",sNext = "";
			while (iBaseidx < iRow) {
				int iNextIdx = iBaseidx + 1;
				TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

				while (iNextIdx < iRow) {

					TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];
					switch (pType) {
						case 1:
							sBase = GetLabelText(celBase);
							sNext = GetLabelText(celNext);
							break;
						case 2:
							sBase = GetLinkText(celBase);
							sNext = GetLinkText(celNext);
							break;
					}

					if (sBase.Equals(sNext)) {
						if (celBase.RowSpan == 0) {
							celBase.RowSpan = 2;
						} else {
							celBase.RowSpan++;
						}
						pGrd.Rows[iNextIdx].Cells.Remove(celNext);
						iNextIdx++;
					} else {
						break;
					}
				}
				iBaseidx = iNextIdx;
			}
		}


		private static string GetLabelText(TableCell tc) {
			System.Web.UI.WebControls.Label dblc =
			  (System.Web.UI.WebControls.Label)tc.Controls[1];
			return dblc.Text;
		}

		private static string GetLinkText(TableCell tc) {
			HyperLink dblc =
			  (HyperLink)tc.Controls[1];
			return dblc.Text;
		}


		public static bool Expression(string pPattern,string pText) {
			Regex rgx = new Regex(pPattern);
			Match rgxMatch = rgx.Match(pText);
			return rgxMatch.Success;
		}

		public static string ChangeQueryToPathInfo(string pQuery,bool pIsCrawler) {
			if (pIsCrawler) {
				Regex rgxNullValue = new Regex(@"([^=?&]*=&)|([^=&]*=\z)",RegexOptions.Compiled);
				//				pQuery = rgxNullValue.Replace(pQuery,"=NULL&");
				pQuery = rgxNullValue.Replace(pQuery,"");
				pQuery = pQuery.Replace("&&","&");

				Regex rgx = new Regex(@"([?&\=])",RegexOptions.Compiled);
				string sPathInfo = rgx.Replace(pQuery,"/");
				if (sPathInfo.EndsWith("/")) {
					sPathInfo = sPathInfo.Substring(0,sPathInfo.Length - 1);
				}
				return sPathInfo;
			} else {
				return pQuery;
			}
		}

		public static string[] SplitBytes(System.Text.Encoding pEncoding,string sValue,int pSplitLength) {
			return SplitBytes(pEncoding,sValue,pSplitLength,false);
		}
		/// <summary>
		/// 指定されたバイト数で分割します。







		/// マルチバイト文字が分割教会となる場合は文字化けが発生しない範囲で分割します(指定バイト数以下で分割する)。







		/// </summary>
		/// <param name="pEncoding">エンコーディング</param>
		/// <param name="sValue">分割対象の文字列</param>
		/// <param name="pSplitLength">分割するバイト数</param>
		/// <param name="pCheckHtmlTag">HTMLのタグをチェックするかどうかを示す値</param>
		/// <returns>指定バイト数で分割した文字列の配列</returns>		
		public static string[] SplitBytes(System.Text.Encoding pEncoding,string sValue,int pSplitLength,bool pCheckHtmlTag) {
			//
			// Argsﾁｪｯｸ
			//
			if (string.IsNullOrEmpty(sValue))
				return new string[] { sValue };
			if (pSplitLength <= 0)
				return new string[] { string.Empty };

			byte[] bytes = pEncoding.GetBytes(sValue);

			if (pSplitLength >= bytes.Length) {
				return new string[] { sValue };
			}

			//
			// Split処理







			//
			int iStringIndex = 0;
			int iActualSplitLength = 0;
			int iLastIndexOfTagStart = 0;
			int iLastIndexOfTagEnd = 0;

			int iCharLength = 1;
			for (int byteIndex = 0;byteIndex < pSplitLength;byteIndex++) {

				string sPart = pEncoding.GetString(bytes,iActualSplitLength,iCharLength);

				if (sPart.Equals("<"))
					iLastIndexOfTagStart = iActualSplitLength;
				if (sPart.Equals(">"))
					iLastIndexOfTagEnd = iActualSplitLength;

				if (sValue.Substring(iStringIndex,1).Equals(sPart)) {
					iStringIndex += 1;
					iActualSplitLength = byteIndex + 1;
					iCharLength = 1;
				} else {
					iCharLength += 1;
				}
			}

			if (pCheckHtmlTag
				&& (iLastIndexOfTagStart > 0)
				&& (iLastIndexOfTagEnd < iLastIndexOfTagStart)) {
				iActualSplitLength = iLastIndexOfTagStart;
			}

			string sSubValue = pEncoding.GetString(bytes,iActualSplitLength,bytes.Length - iActualSplitLength);
			string[] oSplittedSubValue = SplitBytes(pEncoding,sSubValue,pSplitLength,pCheckHtmlTag);

			System.Collections.Generic.List<string> oValueList = new System.Collections.Generic.List<string>();
			oValueList.Add(pEncoding.GetString(bytes,0,iActualSplitLength));
			oValueList.AddRange(oSplittedSubValue);

			return oValueList.ToArray();
		}


		///
		/// ＩＰアドレスのチェック
		/// 指定したＩＰアドレスが、指定したネットワークアドレスに含まるかどうかをチェックする。



		///チェックしたいＩＰアドレス
		///xxx.xxx.xxx.xxx/xx形式 ///
		public static bool CheckNetMask(string strCheckIP,string strNetworkIP) {
			IPAddress ipCheck;
			if (!IPAddress.TryParse(strCheckIP,out ipCheck)) {
				return false;
			}

			string[] astr = strNetworkIP.Split('/');
			IPAddress ipNetwork;
			if (!IPAddress.TryParse(astr[0],out ipNetwork)) {
				return false;
			}
			if (astr.Length == 1) {
				if (ipCheck.Equals(ipNetwork)) {
					return true;
				} else {
					return false;
				}
			} else {
				int intNetwordBit = Int32.Parse(astr[1]);
				IPAddress ipCheckMasked = GetNetMaskedAddress(ipCheck,intNetwordBit);
				IPAddress ipNetworkMasked = GetNetMaskedAddress(ipNetwork,intNetwordBit);
				if (ipCheckMasked.Equals(ipNetworkMasked)) {
					return true;
				} else {
					return false;
				}
			}
		}

		private static IPAddress GetNetMaskedAddress(IPAddress ipCheck,int intNetworkBit) {
			UInt32 uintNetworkMask = System.UInt32.MaxValue;
			uintNetworkMask = uintNetworkMask << (32 - intNetworkBit);

			// チェックするＩＰアドレスをUInt32に置き換える。



			Byte[] bytes = ipCheck.GetAddressBytes();
			UInt32 uintChecked = (UInt32)(bytes[0] * 0x1000000 + bytes[1] * 0x10000 + bytes[2] * 0x100 + bytes[3]);

			// バイトオーダの変換
			// ここでは、ＩＰアドレスをネットマスクしてから、一旦32bit intに変換してからバイトオーダ変換を行う。



			int intCheckMasked = (int)(uintChecked & uintNetworkMask);
			UInt32 uintCheckMasked = (UInt32)IPAddress.NetworkToHostOrder(intCheckMasked);

			// マスクされたＩＰアドレスの新しいオブジェクトを生成する。



			IPAddress ipCheckMasked = new IPAddress((long)uintCheckMasked);
			return ipCheckMasked;
		}


		#region □ TryParseOrDafult □ --------

		public static DateTime? TryParseOrDafult(string pValue,DateTime? pDefaultValue) {
			DateTime dtTmp;
			if (DateTime.TryParse(pValue,out dtTmp)) {
				return dtTmp;
			}
			return pDefaultValue;
		}
		public static int? TryParseOrDafult(string pValue,int? pDefaultValue) {
			int iTmp;
			if (int.TryParse(pValue,out iTmp)) {
				return iTmp;
			}
			return pDefaultValue;
		}

		/// <summary>
		/// 年リストの選択肢を設定
		/// </summary>
		/// <param name="lstYYYY">年リスト</param>
		/// <param name="fromYYYY">開始年</param>
		/// <param name="addFutureNum">未来年の表示数</param>
		/// <param name="sortAsc">昇順で設定するか</param>
		public static void SetYearList(DropDownList lstYYYY,int fromYYYY,int addFutureNum,bool sortAsc) {
			int iCurYear = int.Parse(DateTime.Now.ToString("yyyy"));
			int iStartYear = fromYYYY;
			int iEndYear = iCurYear;
			// 未来年を表示する場合、追加する
			if (addFutureNum > 0) {
				iEndYear += addFutureNum;
			}
			// 終了年が開始年より前の場合、開始と終了を逆にする
			if (iEndYear < iStartYear) {
				int tmpYear = iStartYear;
				iStartYear = iEndYear;
				iEndYear = tmpYear;
			}
			lstYYYY.Items.Clear();
			// 昇順で設定
			if (sortAsc) {
				for (int i = iStartYear;i <= iEndYear;i++) {
					lstYYYY.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				}
			}
			// 降順で設定
			else {
				for (int i = iEndYear;i >= iStartYear;i--) {
					lstYYYY.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				}
			}
		}

		/// <summary>
		/// 月リストの選択肢を設定
		/// </summary>
		/// <param name="lstMM"></param>
		public static void SetMonthList(DropDownList lstMM) {
			lstMM.Items.Clear();
			for (int i = 1;i <= 12;i++) {
				lstMM.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
		}

		/// <summary>
		/// 日リストの選択肢を設定
		/// </summary>
		/// <param name="lstDD"></param>
		public static void SetDayList(DropDownList lstDD) {
			lstDD.Items.Clear();
			for (int i = 1;i <= 31;i++) {
				lstDD.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
		}

		/// <summary>
		/// 時リストの選択肢を設定
		/// </summary>
		/// <param name="lstHH"></param>
		public static void SetHourList(DropDownList lstHH) {
			lstHH.Items.Clear();
			for (int i = 0;i <= 23;i++) {
				lstHH.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
		}

		/// <summary>
		/// 分リストの選択肢を設定
		/// </summary>
		/// <param name="lstMI"></param>
		public static void SetMinuteList(DropDownList lstMI) {
			lstMI.Items.Clear();
			for (int i = 0;i <= 59;i++) {
				lstMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
		}
		#endregion ---------------------------
	}

	public class NamedAutoResetEvent:WaitHandle {
		[DllImport("kernel32.dll",SetLastError = true,CharSet = CharSet.Auto)]
		private static extern IntPtr CreateEvent(IntPtr security,bool isManualReset,
												  bool initialState,string name);
		[DllImport("kernel32.dll")]
		private static extern void SetLastError(int errorCode);
		[DllImport("kernel32.dll")]
		private static extern bool SetEvent(IntPtr handle);
		[DllImport("kernel32.dll")]
		private static extern bool ResetEvent(IntPtr handle);

		public NamedAutoResetEvent(bool initialState,string name,out bool createdNew) {
			const int alreadyExists = 0xB7;
			IntPtr handle = CreateEvent(IntPtr.Zero,false,initialState,name);
			int error = Marshal.GetLastWin32Error();
			if (handle == IntPtr.Zero) {
				Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
			}
			base.SafeWaitHandle = new SafeWaitHandle(handle,true);
			createdNew = !(error == alreadyExists);
		}
		public bool Set() {
			return SetEvent(base.SafeWaitHandle.DangerousGetHandle());
		}
		public bool Reset() {
			return ResetEvent(base.SafeWaitHandle.DangerousGetHandle());
		}


	}


}
