﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

namespace iBridCommLib {
	public class HtmlFilter:Stream {
		private Stream stream;
		private StreamWriter streamWriter;
		private Decoder dec;
		private System.Text.Encoding enc;
		private string m_carrier;
		private ParseHTML parseHtml;

		public static bool _delayParseEmoji = false;
		public static bool DelayParseEmoji {
			get {
				return _delayParseEmoji;
			}
			set {
				_delayParseEmoji = value;
			}
		}


		public HtmlFilter(Stream stm,string carrier)
			: this(stm,carrier,(ParseHTML)HttpContext.Current.Session["parse"]) {
		}

		public HtmlFilter(Stream stm,string carrier,ParseHTML pParseHtml) {
			enc = System.Text.Encoding.GetEncoding("shift_jis");
			stream = stm;
			streamWriter = new StreamWriter(stream,enc);
			dec = enc.GetDecoder();
			m_carrier = carrier;
			parseHtml = pParseHtml;
			parseHtml.noTrans = false;
		}

		// ブラウザにコンテンツを書き出す 
		public override void Write(byte[] buffer,int offset,int count) {
			char[] temp;
			int nCharCnt = dec.GetCharCount(buffer,offset,count);
			char[] cBuffer = new char[nCharCnt];
			int nDecoded = dec.GetChars(buffer,offset,count,cBuffer,0);
			string str = new string(cBuffer);

			parseHtml.parseUser.divList.Clear();
			bool preIgnoreParseEmoji = parseHtml.ignoreParseEmoji;

			// 絵文字以外パース
			try {
				parseHtml.ignoreParseEmoji = _delayParseEmoji;
				str = parseHtml.Parse(str);
			} finally {
				parseHtml.ignoreParseEmoji = preIgnoreParseEmoji;
			}
			// 絵文字だけパース
			if (_delayParseEmoji) {
				str = parseHtml.ParseEmoji(str);
				str = parseHtml.ParseSpecialChar(str);
			}




			if (parseHtml.IsPcDesign == false) {
				if (parseHtml.parseUser.desingDebug) {
					str = str + "<font size=1>";
					bool bFindTable = false;
#if (PREVIOUS)
						foreach (KeyValuePair<string,int> oTable in parseHtml.parseUser.tableDictionary) {
							if (oTable.Value == parseHtml.parseUser.currentTableIdx) {
								bFindTable = true;
								str = str + "<br><font color=\"blue\">" + string.Format("<br>TABLE:{0}",oTable.Key) + "</font>";
								break;
							}
						}
#else
					string sTableNm;
					bFindTable = parseHtml.parseUser.datasetDictionary.TryGetValue(parseHtml.parseUser.currentTableIdx,out sTableNm);
					if (bFindTable) {
						str = str + "<br><font color=\"blue\">" + string.Format("<br>TABLE:{0}",sTableNm) + "</font>";
					}
#endif
					if (bFindTable == false) {
						str = str + "<br><font color=\"blue\">" + string.Format("<br>TABLE:{0} INDEX:{1}","NOT FOUND",parseHtml.parseUser.currentTableIdx) + "</font>";
					}
					foreach (string sErr in parseHtml.parseUser.parseErrorList) {
						str = str + "<br><font color=\"red\">" + sErr + "</font>";
					}
					foreach (string sDiv in parseHtml.parseUser.divList) {
						str = str + "<br><font color=\"green\">" + sDiv + "</font>";
					}
					foreach (string sDiv in parseHtml.parseUser.parseList) {
						str = str + "<br><font color=\"green\">" + sDiv + "</font>";
					}
					str = str + "</font>";
				}

				parseHtml.parseUser.parseErrorList.Clear();
				str = str.Replace("<input name=\"cmdDummy\" type=\"submit\" value=\"\"/>","");
			}

			temp = str.ToCharArray();
			streamWriter.Write(temp,0,temp.Length); // あらためてストリームに出力

			streamWriter.Flush();
		}

		public override int Read(byte[] buffer,int offset,int count) {
			throw new NotSupportedException();
		}

		public override bool CanRead {
			get {
				return false;
			}
		}
		public override bool CanSeek {
			get {
				return false;
			}
		}

		public override bool CanWrite {
			get {
				return true;
			}
		}

		public override long Length {
			get {
				throw new NotSupportedException();
			}
		}

		public override long Position {
			get {
				throw new NotSupportedException();
			}
			set {
				throw new NotSupportedException();
			}
		}

		public override void Flush() {
			stream.Flush();
		}

		public override long Seek(long offset,SeekOrigin origin) {
			throw new NotSupportedException();
		}

		public override void SetLength(long value) {
			throw new NotSupportedException();
		}
	}
}