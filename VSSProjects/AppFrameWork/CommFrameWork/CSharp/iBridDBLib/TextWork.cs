﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

/// <summary>
/// Manager の概要の説明です


/// </summary>
public class TextWork : DbSession {


	public TextWork() {
	}

	public DataSet GetList(string pSeqNo) {
		DataSet ds;

		conn = DbConnect("TextWork.GetList");
		ds = new DataSet();

		string sSql = "SELECT * FROM T_TEXT WHERE SEQ_NO =:SEQ_NO";
		sSql = sSql + " ORDER BY SEQ_NO,TEXT_LINE_NO";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("SEQ_NO", pSeqNo);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}

}
