﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

/// <summary>
/// Manager の概要の説明です


/// </summary>
public class CodeDtl : DbSession {


	public string codeType;
	public string code;
	public string codeNm;

	public CodeDtl() {
	}

	public bool GetOne(string pCodeType,string pCode) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect("CodeDtl.GetOne");

		using (cmd = CreateSelectCommand("SELECT * FROM T_CODE_DTL WHERE CODE_TYPE =:CODE_TYPE AND CODE =:CODE",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("CODE_TYPE"	, pCodeType);
			cmd.Parameters.Add("CODE"		, pCode);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "CODE_DTL");
				if (ds.Tables["CODE_DTL"].Rows.Count != 0) {
					dr = ds.Tables["CODE_DTL"].Rows[0];

					codeType = dr["CODE_TYPE"].ToString();
					code = dr["CODE"].ToString();
					codeNm = dr["CODE_NM"].ToString();

					bExist = true;
				}
			}
		}

		conn.Close();

		return bExist;
	}

	public DataSet GetList(string pCodeType) {
		DataSet ds;

		conn = DbConnect("CodeDtl.GetList");
		ds = new DataSet();

		string sSql = "SELECT * FROM T_CODE_DTL WHERE CODE_TYPE =:CODE_TYPE";
		sSql = sSql + " ORDER BY CODE_TYPE,CODE";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("CODE_TYPE", pCodeType);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}

	public DataSet GetListOrderByCodeAsNumber(string pCodeType) {
		DataSet ds;

		conn = DbConnect("CodeDtl.GetList");
		ds = new DataSet();

		string sSql = "SELECT * FROM T_CODE_DTL WHERE CODE_TYPE =:CODE_TYPE";
		sSql = sSql + " ORDER BY CODE_TYPE,TO_NUMBER(CODE)";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("CODE_TYPE",pCodeType);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}
}
