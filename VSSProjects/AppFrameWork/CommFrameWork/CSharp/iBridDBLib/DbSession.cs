﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Diagnostics;

using iBridCommLib;

[Serializable]
public class DbSession:IDisposable {

	private const string TRACE_DB_SESSION_SWITCH_NM = "TRACE_DB_SESSION_SWITCH";

	public enum DbType {
		VARCHAR2 = 1,
		NUMBER = 2,
		DATE = 3,
		ARRAY_VARCHAR2 = 4,
	}

	public enum InOut {
		IN = 1,
		OUT = 2,
		BOTH = 3,
	}

	[NonSerialized,System.Xml.Serialization.XmlIgnore]
	public OracleConnection conn;

	[NonSerialized,System.Xml.Serialization.XmlIgnore]
	public OracleCommand cmd;

	[NonSerialized,System.Xml.Serialization.XmlIgnore]
	public OracleDataAdapter da;

	[NonSerialized,System.Xml.Serialization.XmlIgnore]
	public string requesterId;

	public DbSession() {
		conn = null;
		cmd = null;
		da = null;
	}

	public virtual void Dispose() {
		if (conn != null) {
			conn.Dispose();
		}

		if (cmd != null) {
			cmd.Dispose();
		}

		if (da != null) {
			da.Dispose();
		}
	}

	public OracleConnection DbConnect() {
		return DbConnect("NoName");
	}

	public OracleConnection DbConnect(string pRequesterId) {
		if (conn == null) {
			string sLifeTime = iBridCommLib.iBridUtil.GetStringValue(ConfigurationManager.AppSettings["Lifetime"]);
			if (sLifeTime.Equals(string.Empty)) {
				sLifeTime = "600";
			}
			conn = new OracleConnection();
			conn.ConnectionString = string.Format("User Id={0}; Password={1}; Data Source={2};Min Pool Size={3};Max Pool Size={4};Connection Lifetime={5};Connection Timeout=30;Incr Pool Size=1; Decr Pool Size=1",
					ConfigurationManager.AppSettings["user"],
					ConfigurationManager.AppSettings["password"],
					ConfigurationManager.AppSettings["server"],
					ConfigurationManager.AppSettings["MinPoolSize"],
					ConfigurationManager.AppSettings["MaxPoolSize"],
					sLifeTime);
		}
		if (conn.State == ConnectionState.Closed) {
			conn.Open();
		}
		return conn;
	}


	public void PrepareProcedure(string pName) {
		conn = DbConnect(pName);

		cmd = new OracleCommand();
		cmd.CommandType = CommandType.StoredProcedure;
		cmd.CommandText = pName;
		cmd.Connection = conn;
	}

	public void ProcedureInParm(string pName,DbType pType,bool pValue) {
		if (pValue) {
			ProcedureParm(pName,pType,InOut.IN,1);
		} else {
			ProcedureParm(pName,pType,InOut.IN,0);
		}
	}

	public void ProcedureInParm(string pName,DbType pType,object pValue) {
		ProcedureParm(pName,pType,InOut.IN,pValue);
	}

	public void ProcedureBothParm(string pName,DbType pType,object pValue) {
		ProcedureParm(pName,pType,InOut.BOTH,pValue);
	}

	public void ProcedureOutParm(string pName,DbType pType) {
		ProcedureParm(pName,pType,InOut.OUT,"");
	}

	public void ProcedureParm(string pName,DbType pType,InOut pInOut,object pValue) {
		int iSize;
		OracleDbType objType;
		OracleParameter objParam;

		objType = Oracle.DataAccess.Client.OracleDbType.Varchar2;
		iSize = 4000;

		switch (pType) {
			case DbType.NUMBER:
				objType = Oracle.DataAccess.Client.OracleDbType.Decimal;
				iSize = 20;
				break;

			case DbType.DATE:
				objType = Oracle.DataAccess.Client.OracleDbType.Date;
				iSize = 0;
				break;

		}

		objParam = new OracleParameter(pName,objType);

		switch (pInOut) {
			case InOut.IN:
				objParam.Direction = System.Data.ParameterDirection.Input;
				break;

			case InOut.OUT:
				objParam.Direction = System.Data.ParameterDirection.Output;
				break;

			case InOut.BOTH:
				objParam.Direction = System.Data.ParameterDirection.InputOutput;
				break;
		}

		objParam.Value = pValue;
		objParam.Size = iSize;
		cmd.Parameters.Add(objParam);
	}


	public void ProcedureOutArrayParm(string pName,DbType pType) {
		ProcedureArrayParm(pName,pType,InOut.OUT,128,null);
	}

	public void ProcedureOutArrayParm(string pName,DbType pType,int pRowCount) {
		ProcedureArrayParm(pName,pType,InOut.OUT,pRowCount,null);
	}

	public void ProcedureInArrayParm(string pName,DbType pType,string[] pValue) {
		ProcedureArrayParm(pName,pType,InOut.IN,pValue.Length,pValue);
	}

	public void ProcedureInArrayParm(string pName,DbType pType,int pRowCount,string[] pValue) {
		ProcedureArrayParm(pName,pType,InOut.IN,pRowCount,pValue);
	}

	public void ProcedureBothArrayParm(string pName,DbType pType,int pRowCount,string[] pValue) {
		ProcedureArrayParm(pName,pType,InOut.BOTH,pRowCount,pValue);
	}

	public void ProcedureArrayParm(string pName,DbType pType,InOut pInOut,int pRowCount,string[] pValue) {
		OracleDbType objType;
		OracleParameter objParam;
		int iSize;

		objType = Oracle.DataAccess.Client.OracleDbType.Varchar2;
		iSize = 4000;

		switch (pType) {
			case DbType.NUMBER:
				objType = Oracle.DataAccess.Client.OracleDbType.Decimal;
				iSize = 20;
				break;
		}

		objParam = new OracleParameter(pName,objType);
		switch (pInOut) {
			case InOut.IN:
				objParam.Direction = System.Data.ParameterDirection.Input;
				break;

			case InOut.OUT:
				objParam.Direction = System.Data.ParameterDirection.Output;
				break;

			case InOut.BOTH:
				objParam.Direction = System.Data.ParameterDirection.InputOutput;
				break;
		}

		objParam.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
		if (pRowCount == 0) {
			objParam.ArrayBindSize = new int[1];
			objParam.ArrayBindSize[0] = iSize;
			objParam.Value = new string[1];
			objParam.Size = 1;
			cmd.Parameters.Add(objParam);
			return;
		}

		objParam.ArrayBindSize = new int[pRowCount];
		for (int i = 0;i < pRowCount;i++) {
			objParam.ArrayBindSize[i] = iSize;
		}

		if (pInOut != InOut.OUT) {
			objParam.Value = pValue;
		} else {
			objParam.Value = "";
		}

		objParam.Size = pRowCount;
		cmd.Parameters.Add(objParam);
	}

	public void ExecuteProcedure() {
		cmd.ExecuteNonQuery();
		if (!cmd.Parameters["PSTATUS"].Value.ToString().Equals("I000")) {
			throw new ApplicationException("STORED PROCEDURE ERROR " + cmd.CommandText + " " + cmd.Parameters["PSTATUS"].Value.ToString());
		}
	}

	public decimal GetDecimalValue(string pParmNm) {
		return decimal.Parse(cmd.Parameters[pParmNm].Value.ToString());
	}

	public int GetIntValue(string pParmNm) {
		return int.Parse(cmd.Parameters[pParmNm].Value.ToString());
	}

	public string GetStringValue(string pParmNm) {
		if (cmd.Parameters[pParmNm].Size == 0) {
			return "";
		} else {
			return cmd.Parameters[pParmNm].Value.ToString();
		}
	}

	public DateTime? GetDateTimeValue(string pParmNm) {
		DateTime dtValue;
		if (!((Oracle.DataAccess.Types.OracleDate)cmd.Parameters[pParmNm].Value).IsNull) {
			if (DateTime.TryParse(((Oracle.DataAccess.Types.OracleDate)cmd.Parameters[pParmNm].Value).Value.ToString(),out dtValue)) {
				return dtValue;
			}
		}
		return null;
	}

	public int GetArrySize(string pParmNm) {
		OracleParameter prmOra = cmd.Parameters[pParmNm];
		return (prmOra.Value as Array).Length;
	}

	public string GetArryStringValue(string pParmNm,int pIdx) {
		OracleParameter prmOra = cmd.Parameters[pParmNm];
		OracleString objValue = (OracleString)(prmOra.Value as Array).GetValue(pIdx);

		if (objValue.IsNull) {
			return "";
		} else {
			return objValue.ToString();
		}
	}

	public string[] GetArrayString(string pParamNm) {
		OracleParameter prmOra = cmd.Parameters[pParamNm];
		return Array.ConvertAll<OracleString,string>(prmOra.Value as OracleString[],delegate(OracleString os) {
			return os.IsNull ? string.Empty : os.Value;
		});
	}

	public string GetArrayStringConcatValue(string pParamNm) {
		StringBuilder oValueBuilder = new StringBuilder();
		for (int i = 0;i < GetArrySize(pParamNm);i++) {
			oValueBuilder.Append(GetArryStringValue(pParamNm,i));
		}
		return oValueBuilder.ToString();
	}

	public string GetArryIntValue(string pParmNm,int pIdx) {
		OracleParameter prmOra = cmd.Parameters[pParmNm];
		OracleDecimal objValue = (OracleDecimal)(prmOra.Value as Array).GetValue(pIdx);

		if (objValue.IsNull) {
			return "";
		} else {
			return objValue.ToString();
		}
	}

	public int[] GetArrayInt(string pParamNm) {
		OracleParameter prmOra = cmd.Parameters[pParamNm];
		decimal[] dTemp= Array.ConvertAll<OracleDecimal,decimal>(prmOra.Value as OracleDecimal[],delegate(OracleDecimal od) { return od.IsNull ? 0 : od.Value; });
		int[] iTemp = new int[dTemp.Length];
		for (int i = 0;i < dTemp.Length;i++) {
			iTemp[i] = Convert.ToInt32(dTemp[i]);
		}
		return iTemp;
	}

	/*
		public OracleCommand CreateSelectCommand(string pSql,string pView,string pMView,OracleConnection pConn) {
			return CreateSelectCommand(pSql,pView,pMView,string.Empty,pConn);
		}

		public OracleCommand CreateSelectCommand(string pSql,string pView,string pMView,string pHint,OracleConnection pConn) {
			string sSql = pSql;
			if (iBridCommLib.iBridUtil.GetStringValue(ConfigurationManager.AppSettings["UseMV"]).Equals("1")) {
				sSql = pSql.Replace(pView,pMView);
			}
			if (string.IsNullOrEmpty(pHint) == false && iBridCommLib.iBridUtil.GetStringValue(ConfigurationManager.AppSettings["UseHint"]).Equals("1")) {
				if (sSql.ToUpper().StartsWith("SELECT")) {
					sSql = string.Format("SELECT {0} ",pHint) + sSql.Substring(6);
				}
			}
			return CreateSelectCommand(sSql,pConn);
		}
	*/

	public OracleCommand CreateSelectCommand(string pSql,OracleConnection pConn) {
		OracleCommand oracleCommand = new OracleCommand(pSql,conn);
		String sCommandTimeout = iBridUtil.GetStringValue(System.Configuration.ConfigurationManager.AppSettings["CommandTimeout"]);
		if (!string.IsNullOrEmpty(sCommandTimeout)) {
			oracleCommand.CommandTimeout = int.Parse(sCommandTimeout);
		}
		return oracleCommand;
	}

	#region □ ExecuteSelectQueryBase □ ------------------------------------------------
	public DataSet ExecuteSelectQueryBase(StringBuilder pSqlBuilder,OracleParameter[] pParamList) {
		return ExecuteSelectQueryBase(pSqlBuilder.ToString(),pParamList);
	}
	public DataSet ExecuteSelectQueryBase(string pSql,OracleParameter[] pParamList) {
		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(pSql,this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(pParamList);

				using (da = new OracleDataAdapter(this.cmd)) {
					this.Fill(da,oDataSet);
				}
			}

		} finally {
			this.conn.Close();
		}
		return oDataSet;
	}
	#endregion --------------------------------------------------------------------------

	#region □ ExecuteSelectCountQueryBase □ ------------------------------------------------
	public int ExecuteSelectCountQueryBase(StringBuilder pSqlBuilder,OracleParameter[] pParamList) {
		return ExecuteSelectCountQueryBase(pSqlBuilder.ToString(),pParamList);
	}
	public int ExecuteSelectCountQueryBase(string pSql,OracleParameter[] pParamList) {
		int iCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = this.CreateSelectCommand(pSql,this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(pParamList);

				SqlTrace.Write(this.GetType(),this.cmd);

				iCount = int.Parse(iBridUtil.GetStringValue(this.cmd.ExecuteScalar()));
			}
		} finally {
			this.conn.Close();
		}

		return iCount;
	}
	#endregion --------------------------------------------------------------------------

	public void Fill(OracleDataAdapter pDataAdapter,DataSet pDataSet) {
		this.Fill(pDataAdapter,pDataSet,"Table");
	}
	public void Fill(OracleDataAdapter pDataAdapter,DataSet pDataSet, string pSrcTable) {
		SqlTrace.Write(this.GetType(),pDataAdapter.SelectCommand);
		pDataAdapter.Fill(pDataSet,pSrcTable);
	}

	private class SqlTrace {
		private static readonly TraceSwitch _switch = new TraceSwitch(TRACE_DB_SESSION_SWITCH_NM,string.Empty);

		/// <summary>
		/// SQLのトレースログを出力する。TraceスイッチがINFO以下の場合は処理しない

		/// </summary>
		/// <param name="pCallType"></param>
		/// <param name="pCommand"></param>
		public static void Write(Type pCallType,OracleCommand pCommand) {
			if (_switch.Level < TraceLevel.Verbose)
				return;

			/*
			 <?xml version="1.0"?>
			 <configuration>
			  <system.diagnostics>
				<switches>
				  <add name="TRACE_DB_SESSION_SWITCH" value="4" />
				</switches>
				<trace autoflush="true" indentsize="4">
				  <listeners>
					<add name="LogFile"
						type="System.Diagnostics.TextWriterTraceListener"
						initializeData="d:\log\test.sql" />
					<remove name="Default"/>
				  </listeners>
				</trace>	
			  </system.diagnostics>  
			 </configuration>
			 */

			Trace.WriteLine(" -- ******************************************************************");
			Trace.WriteLine(string.Format(" -- {0:yyyy/MM/dd HH:mm:ss}\t{1}",DateTime.Now,pCallType.FullName));
			Trace.WriteLine(" -- ******************************************************************");
			foreach (OracleParameter oParam in pCommand.Parameters) {
				bool bIsVarchar = (oParam.OracleDbType == OracleDbType.Varchar2);

				// 型の変換。必要に応じて追加する
				string sDbType = oParam.OracleDbType.ToString();
				switch (oParam.OracleDbType) {
					case OracleDbType.Int16:
					case OracleDbType.Int32:
					case OracleDbType.Int64:
						sDbType = "NUMBER";
						break;
					case OracleDbType.Date:
						sDbType = "VARCHAR2";
						bIsVarchar = true;
						break;
				}
				string sParamName = oParam.ParameterName;
				if (sParamName.StartsWith(":")) {
					sParamName = sParamName.Substring(1,sParamName.Length - 1);
				}
				Trace.WriteLine(string.Format("VAR  {0} {1}{2}	;",sParamName,sDbType,bIsVarchar ? "(4000)" : string.Empty));
				Trace.WriteLine(string.Format("EXEC :{0} := {1}{2}{1} ;",sParamName,bIsVarchar ? "'" : string.Empty,oParam.Value));
			}
			Trace.WriteLine(string.Empty);
			Trace.WriteLine(RemoveEmptyLine(pCommand.CommandText));
			Trace.WriteLine(" -- ******************************************************************");
			Trace.WriteLine(" / ");
		}

		private static string RemoveEmptyLine(string sValue) {
			StringBuilder oValueBuilder = new StringBuilder();
			foreach (string sLine in sValue.Split(new string[] { Environment.NewLine },StringSplitOptions.RemoveEmptyEntries)) {
				if (sLine.Trim().Length == 0)
					continue;

				oValueBuilder.AppendLine(sLine);
			}
			return oValueBuilder.ToString();
		}

	}

}
