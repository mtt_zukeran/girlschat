﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

/// <summary>
/// Manager の概要の説明です


/// </summary>
public class Code : DbSession {


	public string codeType;
	public string codeTypeNm;
	public string codeLen;

	public Code() {
	}

	public bool GetOne(string pCodeType) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect("Code.GetOne");

		using (cmd = CreateSelectCommand("SELECT * FROM T_CODE WHERE CODE_TYPE =:CODE_TYPE",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("CODE_TYPE"	, pCodeType);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "CODE");
				if (ds.Tables["CODE"].Rows.Count != 0) {
					dr = ds.Tables["CODE"].Rows[0];

					codeType	= dr["CODE_TYPE"].ToString();
					codeTypeNm	= dr["CODE_TYPE_NM"].ToString();
					codeLen		= dr["CODE_LEN"].ToString();

					bExist = true;
				}
			}
		}

		conn.Close();

		return bExist;
	}

	public DataSet GetList() {
		DataSet ds;

		conn = DbConnect("Code.GetList");
		ds = new DataSet();

		string sSql = "SELECT * FROM T_CODE";
		sSql = sSql + " ORDER BY CODE_TYPE";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}
}
