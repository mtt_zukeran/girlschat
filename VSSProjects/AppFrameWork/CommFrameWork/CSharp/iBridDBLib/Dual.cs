﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

/// <summary>
/// Manager の概要の説明です


/// </summary>
public class Dual : DbSession {


	public Dual() {
	}

	public decimal GetUploadSeq() {
		return GetSeq("SELECT SEQ_UPLOAD_NO.NEXTVAL SEQ FROM DUAL");
	}

	public decimal GetTextSeq() {
		return GetSeq("SELECT SEQ_TEXT_PROC_NO.NEXTVAL SEQ FROM DUAL");
	}


	private decimal GetSeq(string pSql) {
		DataSet ds;
		DataRow dr;
		decimal dSeq=0;
		conn = DbConnect(pSql);
		using (cmd = new OracleCommand(pSql, conn))
		using (ds = new DataSet()) {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "DUAL");
				if (ds.Tables["DUAL"].Rows.Count != 0) {
					dr = ds.Tables["DUAL"].Rows[0];
					dSeq = decimal.Parse(dr["SEQ"].ToString());
				}
			}
		}
		conn.Close();

		return dSeq;
	}
}
