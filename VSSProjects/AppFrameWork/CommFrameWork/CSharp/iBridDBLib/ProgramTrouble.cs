﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ProgramTrouble:DbSession {

	public ProgramTrouble() {
	}

	public int GetPageCount(string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		conn = DbConnect("ProgramTrouble.GetPageCount");

		string sSql = "SELECT COUNT(SEQ_NO) AS ROW_COUNT FROM T_PROGRAM_TROUBLE";
		string sWhere = "";

		OracleParameter[] objParms = CreateWhere(pReportDayFrom,pReportDayTo,ref sWhere);

		sSql = sSql + sWhere;

		using (cmd = CreateSelectCommand(sSql,conn))
		using (da = new OracleDataAdapter(cmd))
		using (ds = new DataSet()) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			da.Fill(ds);
			if (ds.Tables[0].Rows.Count != 0) {
				dr = ds.Tables[0].Rows[0];
				iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
			}
		}

		conn.Close();
		return iPageCount;
	}

	public DataSet GetPageCollection(string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect("ProgramTrouble.GetPageCollection");
		ds = new DataSet();

		string sWhere = "";
		string sSql = "SELECT " +
						"SEQ_NO ," +
						"TROUBLE_DATE," +
						"SYSTEM_TYPE," +
						"PROGRAM_NM," +
						"TROUBLE_MSG " +
						"FROM(" +
						" SELECT T_PROGRAM_TROUBLE.*, ROW_NUMBER() OVER (ORDER BY TROUBLE_DATE DESC,SEQ_NO DESC) AS RNUM FROM T_PROGRAM_TROUBLE ";

		OracleParameter[] objParms = CreateWhere(pReportDayFrom,pReportDayTo,ref sWhere);

		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW";

		using (cmd = CreateSelectCommand(sSql,conn)) {

			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_PROGRAM_TROUBLE");
			}
		}
		conn.Close();
		return ds;
	}

	private OracleParameter[] CreateWhere(string pReportDayFrom,string pReportDayTo,ref string pWhere) {
		ArrayList list = new ArrayList();

		pWhere = " WHERE TROUBLE_DATE >= :TROUBLE_DATE_FROM AND TROUBLE_DATE < :TROUBLE_DATE_TO ";

		DateTime dtFrom = System.DateTime.ParseExact(pReportDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		DateTime dtTo = System.DateTime.ParseExact(pReportDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		dtTo = dtTo.AddDays(1);
		list.Add(new OracleParameter("TROUBLE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		list.Add(new OracleParameter("TROUBLE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
