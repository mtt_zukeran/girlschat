using System.Web.UI;
using System.Configuration;
using iBridCommLib;

namespace MobileLib {

	public class DocomoImageAdapter:System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {

			MobileLib.iBMobileImage image = (MobileLib.iBMobileImage)this.Control;

			Mobile.DocomoBeginTagCenter(image,writer);

			if (iBridUtil.GetStringValue(image.NavigateUrl).Equals("") == false) {
				writer.WriteBeginTag("a");
				writer.WriteAttribute("href",image.NavigateUrl);
				if (iBridUtil.GetStringValue(image.AccessKey).Equals("") == false) {
					writer.WriteAttribute("accesskey",image.AccessKey);
				}
				writer.Write(">");
			}

			writer.WriteBeginTag("img");
			writer.WriteAttribute("src",image.ImageUrl);
			writer.WriteAttribute("alt","");

			if (image.Width != null) {
				writer.WriteAttribute("width",image.Width);
			}

			if (image.Align != null) {
				writer.WriteAttribute("align",image.Align);
			}

			if (image.HSpace != null) {
				writer.WriteAttribute("hspace",image.HSpace);
			}

			if (image.VSpace != null) {
				writer.WriteAttribute("vspace",image.VSpace);
			}

			writer.Write(">");

			if (iBridUtil.GetStringValue(image.NavigateUrl).Equals("") == false) {
				writer.WriteEndTag("a");
			}

			Mobile.DocomoEndTagCenter(image,writer);

			if (image.BreakAfter) {
				writer.Write("<br>");
			}
		}
	}
}
