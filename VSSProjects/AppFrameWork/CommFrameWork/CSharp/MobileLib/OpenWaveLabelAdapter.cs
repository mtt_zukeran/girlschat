using System.Web.UI;
using System.Web.UI.MobileControls;
using iBridCommLib;

namespace MobileLib {
	public class OpenWaveLabelAdapter:System.Web.UI.Adapters.ControlAdapter {
		protected override void Render(HtmlTextWriter writer) {

			MobileLib.iBMobileLabel label = (MobileLib.iBMobileLabel)this.Control;

			if ((!label.Visible) || (iBridUtil.GetStringValue(label.Text).Equals(""))) {
				return;
			}

			Mobile.OpenWaveBeginTagCenter(label,writer);
			Mobile.OpenWaveBeginTagFont(label, writer);

			if ((label.StyleReference != null) && (label.StyleReference.Equals("title")) && (label.Font.Bold == BooleanOption.False)) {
				writer.Write("<b>" + label.Text + "</b>");
			} else {
				writer.Write(label.Text);
			}

			Mobile.OpenWaveEndTagFont(label, writer);
			Mobile.OpenWaveEndTagCenter(label, writer);

			if (label.BreakAfter) {
				writer.Write("<br>");
			}
			writer.Write("\n");
		}
	}
}
