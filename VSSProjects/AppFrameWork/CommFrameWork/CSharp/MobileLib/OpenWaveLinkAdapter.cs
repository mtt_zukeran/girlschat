using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Drawing;
using iBridCommLib;

namespace MobileLib {

	public class OpenWaveLinkAdapter:System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {
			MobileLib.iBMobileLink link = (MobileLib.iBMobileLink)this.Control;

			if (!link.Visible) {
				return;
			}

			Mobile.OpenWaveBeginTagCenter(link,writer);

			if (iBridUtil.GetStringValue(link.Mark).Equals("") == false) {
				Mobile.OpenWaveBeginTagFontSize(link,writer);
				writer.WriteBeginTag("a>");
				writer.Write(link.Mark);
				writer.WriteEndTag("a");
				Mobile.OpenWaveEndTagFontSize(link,writer);
			}

			if (link.Enabled) {
				writer.WriteBeginTag("a");
				writer.WriteAttribute("href",link.NavigateUrl);
				if (iBridUtil.GetStringValue(link.AccessKey).Equals("") == false) {
					writer.WriteAttribute("accesskey",link.AccessKey);
				}
				writer.Write(">");
			} else {
				link.ForeColor = Color.FromName(link.DisableColor);
			}

			if (link.ForeColor == Color.Empty) {
				string sLinkColor = iBridUtil.GetStringValue(HttpContext.Current.Session["LinkColor"]);
				if (!sLinkColor.Equals("")) {
					link.ForeColor = Color.FromName(sLinkColor);
				}
			}

			Mobile.OpenWaveBeginTagFont(link,writer);

			writer.Write(link.Text);

			Mobile.OpenWaveEndTagFont(link,writer);

			if (link.Enabled) {
				writer.WriteEndTag("a");
			}

			Mobile.OpenWaveEndTagCenter(link,writer);

			if (link.BreakAfter) {
				writer.Write("<br>");
			}
			writer.Write("\n");
		}
	}
}
