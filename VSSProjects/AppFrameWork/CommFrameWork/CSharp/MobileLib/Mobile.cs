﻿
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib.Pictograph;

namespace MobileLib {

	public class Mobile {

		/*----------------------------------*/
		/* Carrier Type						*/
		/*----------------------------------*/
		public const string DOCOMO = "01";
		public const string SOFTBANK = "02";
		public const string SOFTBANK_MAIL = "M2";
		public const string KDDI = "03";
		public const string ACCELMAIL = "09";
		public const string ANDROID = "21";
		public const string IPHONE = "22";
		public const string CARRIER_OTHERS = "11";

		public static string GetCarrier(string pBrowser) {
			if (pBrowser.Equals("i-mode")) {
				return DOCOMO;
			} else if (CapabilityEvaluators.isSoftBank(null,null)) {
				return SOFTBANK;
			} else if (pBrowser.Equals("Phone.com")) {
				return KDDI;
			} else if (pBrowser.Equals("Android")) {
				return ANDROID;
			} else if (pBrowser.Equals("iPhone")) {
				return IPHONE;
			} else {
				return CARRIER_OTHERS;
			}

		}
		public static bool IsFeaturePhone(string pCarrier) {
			bool bIsFeaturePhone = false;
			bIsFeaturePhone = bIsFeaturePhone || pCarrier.Equals(DOCOMO);
			bIsFeaturePhone = bIsFeaturePhone || pCarrier.Equals(KDDI);
			bIsFeaturePhone = bIsFeaturePhone || pCarrier.Equals(SOFTBANK);
			return bIsFeaturePhone;
		}


		public static string GetUtn(string pCarrierCd,HttpRequest pRequest) {
			string sUtn = "",sPattern = "";

			if (pCarrierCd.Equals(KDDI)) {
				return iBridUtil.GetStringValue(pRequest.Headers["x-up-subno"]);

			} else if (pCarrierCd.Equals(DOCOMO)) {
				sPattern = @"(;ser\w{15}|/ser\w{11})";

			} else if (pCarrierCd.Equals(SOFTBANK)) {
				sPattern = @"(/SN\w{11,15}\s)";
			}
			Regex rgx = new Regex(sPattern);
			Match rgxMatch = rgx.Match(pRequest.UserAgent);
			if (rgxMatch.Success) {
				sUtn = rgxMatch.Value.Trim();
			}
			return sUtn;
		}


		public static string GetiModeId(string pCarrierCd,HttpRequest pRequest) {
			string sid = "";
			if (pCarrierCd.Equals(DOCOMO)) {
				sid = iBridUtil.GetStringValue(pRequest.ServerVariables["HTTP_X_DCMGUID"]);

			} else if (pCarrierCd.Equals(SOFTBANK)) {
				sid = iBridUtil.GetStringValue(pRequest.ServerVariables["HTTP_X_JPHONE_UID"]);

			} else if (pCarrierCd.Equals(KDDI)) {
				sid = iBridUtil.GetStringValue(pRequest.ServerVariables["HTTP_X_UP_SUBNO"]);
				if (sid.Length > 14) {
					sid = sid.Substring(0,14);
				}
			}
			return sid;
		}


		public static string HrTag(int pSize,string pColor) {
			return string.Format("<hr size=\"{0}\" color=\"{1}\" width=\"100%\" >",pSize,pColor);
		}

		/*--------------------------------------------------*/
		/*  <cener> </center>								*/
		/*--------------------------------------------------*/
		public static void DocomoBeginTagCenter(MobileControl pControl,HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("<center>");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("<div align='right'>");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("<div align='left'>");
			}
		}

		public static void DocomoEndTagCenter(MobileControl pControl,HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("</center>");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("</div>");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("</div>");
			}
		}

		public static void SoftBankBeginTagCenter(MobileControl pControl,HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("<center>");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("<div align='right'>");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("<div align='left'>");
			}
		}

		public static void SoftBankEndTagCenter(MobileControl pControl,HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("</center>");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("</div>");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("</div>");
			}
		}

		public static void OpenWaveBeginTagCenter(MobileControl pControl,HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("<div style=\"text-align:center\">");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("<div style=\"text-align:right\">");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("<div style=\"text-align:left\">");
			}
		}

		public static void OpenWaveEndTagCenter(MobileControl pControl,HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("</div>");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("</div>");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("</div>");
			}
		}

		/*--------------------------------------------------*/
		/*  <Font Color> </Font>							*/
		/*--------------------------------------------------*/
		public static void DocomoBeginTagFont(MobileControl pControl,HtmlTextWriter writer) {
			if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("0")) ||
				(!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet"))) {

				writer.WriteBeginTag("font");

				if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("")) && (pControl.ForeColor.Name.Equals("0") == false)) {
					if ((pControl.ForeColor.Name.StartsWith("ff")) && (pControl.ForeColor.Name.Length == 8)) {
						writer.WriteAttribute("color","#" + pControl.ForeColor.Name.Substring(2));
					} else {
						writer.WriteAttribute("color",pControl.ForeColor.Name);
					}
				}

				if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
					if (pControl.Font.Size == FontSize.Small) {
						writer.WriteAttribute("size","2");
					}
				}
				writer.Write(">");
			}
		}

		public static void DocomoEndTagFont(MobileControl pControl,HtmlTextWriter writer) {
			if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("0")) ||
				(!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet"))) {
				writer.WriteEndTag("font");
			}
		}

		public static void DocomoBeginTagFontSize(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
				writer.WriteBeginTag("font");
				if (pControl.Font.Size == FontSize.Small) {
					writer.WriteAttribute("size","2");
				}
				writer.Write(">");
			}
		}

		public static void DocomoEndTagFontSize(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
				writer.WriteEndTag("font");
			}
		}

		public static void SoftBankBeginTagFont(MobileControl pControl,HtmlTextWriter writer) {
			if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("0")) ||
				(!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet"))) {

				writer.WriteBeginTag("font");

				if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("")) && (pControl.ForeColor.Name.Equals("0") == false)) {
					if ((pControl.ForeColor.Name.StartsWith("ff")) && (pControl.ForeColor.Name.Length == 8)) {
						writer.WriteAttribute("color","#" + pControl.ForeColor.Name.Substring(2));
					} else {
						writer.WriteAttribute("color",pControl.ForeColor.Name);
					}
				}

				if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
					if (pControl.Font.Size == FontSize.Small) {
						writer.WriteAttribute("size","2");
					}
				}
				writer.Write(">");
			}
		}

		public static void SoftBankEndTagFont(MobileControl pControl,HtmlTextWriter writer) {
			if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("0")) ||
				(!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet"))) {
				writer.WriteEndTag("font");
			}
		}

		public static void SoftBankBeginTagFontSize(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
				writer.WriteBeginTag("font");
				if (pControl.Font.Size == FontSize.Small) {
					writer.WriteAttribute("size","2");
				}
				writer.Write(">");
			}
		}

		public static void SoftBankEndTagFontSize(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
				writer.WriteEndTag("font");
			}
		}

		public static void OpenWaveBeginTagFont(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("")) {
				writer.WriteBeginTag("span");
				if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("")) && (pControl.ForeColor.Name.Equals("0") == false)) {
					if ((pControl.ForeColor.Name.StartsWith("ff")) && (pControl.ForeColor.Name.Length == 8)) {
						writer.WriteAttribute("style","color:#" + pControl.ForeColor.Name.Substring(2));
					} else {
						writer.WriteAttribute("style","color:" + pControl.ForeColor.Name);
					}
				}
				writer.Write(">");
			}

			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
				if (pControl.Font.Size == FontSize.Small) {
					writer.WriteBeginTag("small");
					writer.Write(">");
					//writer.WriteAttribute("font-size","12px");
				}
			}
		}

		public static void OpenWaveEndTagFont(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("")) {
				writer.WriteEndTag("span");
			}

			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
				if (pControl.Font.Size == FontSize.Small) {
					writer.WriteEndTag("small");
				}
			}
		}

		public static void OpenWaveBeginTagFontSize(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
				if (pControl.Font.Size == FontSize.Small) {
					writer.WriteBeginTag("small");
					writer.Write(">");
				}
			}
		}

		public static void OpenWaveEndTagFontSize(MobileControl pControl,HtmlTextWriter writer) {
			if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("")) {
				writer.WriteEndTag("small");
			}
		}

		public static List<int[]> GetEmojiScopeSJIS(string pCarrier) {
			List<int[]> oEmojiRangeList = new List<int[]>();
			if (pCarrier == DOCOMO || pCarrier == CARRIER_OTHERS) {
				oEmojiRangeList.Add(new int[] { 0xf89f,0xf8fc });
				oEmojiRangeList.Add(new int[] { 0xf940,0xf9fc });
			} else if (pCarrier == KDDI) {
				oEmojiRangeList.Add(new int[] { 0xf340,0xf3fc });
				oEmojiRangeList.Add(new int[] { 0xf440,0xf48d });
				oEmojiRangeList.Add(new int[] { 0xf640,0xf6fc });
				oEmojiRangeList.Add(new int[] { 0xf740,0xf7fc });
			} else if (pCarrier == SOFTBANK) {
				oEmojiRangeList.Add(new int[] { 0xf941,0xf99b });
				oEmojiRangeList.Add(new int[] { 0xf741,0xf79b });
				oEmojiRangeList.Add(new int[] { 0xf7a1,0xf7f3 });
				oEmojiRangeList.Add(new int[] { 0xf9a1,0xf9ed });
				oEmojiRangeList.Add(new int[] { 0xfb41,0xfb8d });
				oEmojiRangeList.Add(new int[] { 0xfba1,0xfbd7 });
			}

			return (oEmojiRangeList);
		}

		private static readonly Regex _regexEmojiText = new Regex(@"(?<pict_text>[\[]{1}.*?[\]]{1})",RegexOptions.Compiled);
		public static string EmojiToCommTag(string pCarrier,string pSource) {
			string sResult = "";

			pSource = _regexEmojiText.Replace(pSource,delegate(Match pMatch) {
				return EmojiChange.GetCommCodeFromEmojiText(pMatch.Value);
			});

			using (MemoryStream oStream = new MemoryStream()) {
				List<int[]> oEmojiRangeList = GetEmojiScopeSJIS(pCarrier);
				Encoding oEncSjis = Encoding.GetEncoding(932);
				byte[] byteSjisSource = oEncSjis.GetBytes(pSource);
				int iPos = 0;
				int iCopiedPos = 0;

				for (iPos = 0;iPos < byteSjisSource.Length - 1;iPos++) {
					byte byteFirst = byteSjisSource[iPos];
					byte byteSecond = byteSjisSource[iPos + 1];
					int iEmojiCode = (((int)byteFirst) << 8) | (int)byteSecond;

					// SHIT-JIS全角2Byteの場合読みとばし 
					if (((0x81 <= byteFirst) && (byteFirst <= 0x9F)) || ((0xE0 <= byteFirst) && (byteFirst <= 0xEF))) {
						iPos++;
						continue;
					}

					foreach (int[] iEmojiRange in oEmojiRangeList) {
						if (iEmojiRange[0] <= iEmojiCode && iEmojiCode <= iEmojiRange[1]) {
							int iLen = iPos - iCopiedPos;
							if (iLen > 0) {
								oStream.Write(byteSjisSource,iCopiedPos,iLen);
							}
							iCopiedPos = iPos + 2;

							string sCommCode = EmojiChange.GetCommCode(pCarrier,iEmojiCode);
							if (!sCommCode.Equals("")) {
								byte[] byteCommCode = oEncSjis.GetBytes(sCommCode);
								oStream.Write(byteCommCode,0,byteCommCode.Length);
							}
							iPos++;
							break;
						}
					}
				}

				if (iCopiedPos < byteSjisSource.Length) {
					oStream.Write(byteSjisSource,iCopiedPos,byteSjisSource.Length - iCopiedPos);
				}
				sResult = oEncSjis.GetString(oStream.ToArray());
			}

			sResult = EmojiChange.ReplaceAcute(sResult);
			return (sResult);
		}


		/*----------------------------------------------------------------------------------*/
		/* Clipho To Maqia 絵文字変換														*/
		/*----------------------------------------------------------------------------------*/
		public static List<int[]> GetCliphoEmojiScopeSJIS() {
			List<int[]> oEmojiRangeList = new List<int[]>();
			// Docomo
			oEmojiRangeList.Add(new int[] { 0xf89f,0xf8fc });
			oEmojiRangeList.Add(new int[] { 0xf940,0xf9fc });
			// AU
			oEmojiRangeList.Add(new int[] { 0xf340,0xf3fc });
			oEmojiRangeList.Add(new int[] { 0xf440,0xf48d });
			oEmojiRangeList.Add(new int[] { 0xf640,0xf6fc });
			oEmojiRangeList.Add(new int[] { 0xf740,0xf7fc });
			// SoftBank
			oEmojiRangeList.Add(new int[] { 0x1b24,0x1b24 });

			return (oEmojiRangeList);
		}

		public static string CliphoEmojiToCommTag(string pSource) {
			string sResult = "",sCommCode;
			int i;
			using (MemoryStream oStream = new MemoryStream()) {
				List<int[]> oEmojiRangeList = GetCliphoEmojiScopeSJIS();
				Encoding oEncSjis = Encoding.GetEncoding(932);
				byte[] byteSjisSource = oEncSjis.GetBytes(pSource);
				byte[] byteWebChar = new byte[2];
				int iPos = 0;
				int iCopiedPos = 0;

				for (iPos = 0;iPos < byteSjisSource.Length - 1;iPos++) {
					byte byteFirst = byteSjisSource[iPos];
					byte byteSecond = byteSjisSource[iPos + 1];
					int iEmojiCode = (((int)byteFirst) << 8) | (int)byteSecond;

					// SHIT-JIS全角2Byteの場合読みとばし


					if (((0x81 <= byteFirst) && (byteFirst <= 0x9F)) || ((0xE0 <= byteFirst) && (byteFirst <= 0xEF))) {
						iPos++;
						continue;
					}
					i = 0;
					foreach (int[] iEmojiRange in oEmojiRangeList) {
						if (iEmojiRange[0] <= iEmojiCode && iEmojiCode <= iEmojiRange[1]) {
							int iLen = iPos - iCopiedPos;
							if (iLen > 0) {
								oStream.Write(byteSjisSource,iCopiedPos,iLen);
							}
							sCommCode = "";
							if (i >= 6) {
								byteWebChar[0] = byteSjisSource[iPos + 2];
								byteWebChar[1] = byteSjisSource[iPos + 3];
								sCommCode = EmojiChange.GetWebCharToCommCode(oEncSjis.GetString(byteWebChar));
								iCopiedPos = iPos + 5;
							} else if (i >= 2) {
								sCommCode = EmojiChange.GetCommCode(KDDI,iEmojiCode);
								iCopiedPos = iPos + 2;
							} else {
								sCommCode = EmojiChange.GetCommCode(DOCOMO,iEmojiCode);
								iCopiedPos = iPos + 2;
							}

							if (!sCommCode.Equals("")) {
								byte[] byteCommCode = oEncSjis.GetBytes(sCommCode);
								oStream.Write(byteCommCode,0,byteCommCode.Length);
							}
							iPos++;
							break;
						}
						i++;
					}
				}

				if (iCopiedPos < byteSjisSource.Length) {
					oStream.Write(byteSjisSource,iCopiedPos,byteSjisSource.Length - iCopiedPos);
				}
				sResult = oEncSjis.GetString(oStream.ToArray());
			}
			return (sResult);
		}

		public static string GetInputValue(string pValue,int pByteLength,Encoding pEnc) {
			byte[] oByteStr = pEnc.GetBytes(pValue);
			string sResult = pValue;

			if (oByteStr.Length > pByteLength) {
				sResult = pEnc.GetString(oByteStr,0,pByteLength);
			}
			if (pEnc.GetBytes(sResult).Length > pByteLength) {
				sResult = sResult.Substring(0,sResult.Length - 1);
			}
			return sResult;
		}
		
		public static string ConvertSpecialChar(string sValue) {
			return Regex.Replace(sValue,@"\$180;","´",RegexOptions.Compiled);
		}
	}


	public class EmojiChange {

		static Dictionary<string,string> cnvSoftbank;
		static Dictionary<string,string> cnvDocomo;
		static Dictionary<string,string> cnvKddi;
		static Dictionary<string,string> cnvWebChar;
		static Dictionary<string,string> cnvEmojiText;

		public EmojiChange() {
		}


		public static string GetCommCode(string pCarrier,int pEmojiCode) {

			string sOriginalCode = Convert.ToString(pEmojiCode,16);
			string sResult = sOriginalCode;


			if (pCarrier.Equals(Mobile.DOCOMO) || pCarrier.Equals(Mobile.CARRIER_OTHERS)) {
				if (cnvDocomo == null) {
					SetupDocomoDic();
				}
				sResult = GetCommCodeFromDocomo(sOriginalCode.ToUpper());

			} else if (pCarrier.Equals(Mobile.SOFTBANK)) {
				if (cnvSoftbank == null) {
					SetupSoftbankDic();
				}
				sResult = GetCommCodeFromSoftbank(sOriginalCode.ToUpper());

			} else if (pCarrier.Equals(Mobile.KDDI)) {
				if (cnvKddi == null) {
					SetupKddiDic();
				}
				sResult = GetCommCodeFromKddi(sOriginalCode.ToUpper());
			}

			return sResult;
		}

		public static string GetCommCodeFromEmojiText(string pText) {
			if (cnvEmojiText == null) {
				SetupEmojiTextDic();
			}
			return GetCommCodeFormEmojiText(pText);
		}



		private static void SetupDocomoDic() {
			cnvDocomo = new Dictionary<string,string>();
			foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
				if (oPictograph.DocomoReadonly || string.IsNullOrEmpty(oPictograph.DocomoShiftJIS))
					continue;
				if (!cnvDocomo.ContainsKey(oPictograph.DocomoShiftJIS)) {
					cnvDocomo.Add(oPictograph.DocomoShiftJIS,string.Format("$x{0};",oPictograph.CommonCode));
				}
			}
		}

		private static void SetupSoftbankDic() {
			cnvSoftbank = new Dictionary<string,string>();
			foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
				if (oPictograph.SoftbankReadonly || string.IsNullOrEmpty(oPictograph.SoftbankShiftJIS))
					continue;
				if (!cnvSoftbank.ContainsKey(oPictograph.SoftbankShiftJIS)) {
					cnvSoftbank.Add(oPictograph.SoftbankShiftJIS,string.Format("$x{0};",oPictograph.CommonCode));
				}
			}
		}

		public static void SetupKddiDic() {
			cnvKddi = new Dictionary<string,string>();
			foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
				if (oPictograph.KddiReadonly || string.IsNullOrEmpty(oPictograph.KddiShiftJIS))
					continue;
				if (!cnvKddi.ContainsKey(oPictograph.KddiShiftJIS)) {
					cnvKddi.Add(oPictograph.KddiShiftJIS,string.Format("$x{0};",oPictograph.CommonCode));
				}
			}
		}

		public static void SetupEmojiTextDic() {
			cnvEmojiText = new Dictionary<string,string>();
			foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
				if (string.IsNullOrEmpty(oPictograph.Text))
					continue;
				if (!cnvEmojiText.ContainsKey(oPictograph.Text)) {
					cnvEmojiText.Add(oPictograph.Text,string.Format("$x{0};",oPictograph.CommonCode));
				}
			}
		}

		static string GetCommCodeFromDocomo(string pOriginalCode) {
			if (cnvDocomo.ContainsKey(pOriginalCode)) {
				return cnvDocomo[pOriginalCode];
			} else {
				return string.Empty;
			}
		}

		static string GetCommCodeFromSoftbank(string pOriginalCode) {
			if (cnvSoftbank.ContainsKey(pOriginalCode)) {
				return cnvSoftbank[pOriginalCode];
			} else {
				return string.Empty;
			}
		}

		static string GetCommCodeFromKddi(string pOriginalCode) {
			if (cnvKddi.ContainsKey(pOriginalCode)) {
				return cnvKddi[pOriginalCode];
			} else {
				return string.Empty;
			}
		}
		static string GetCommCodeFormEmojiText(string pOriginalCode) {
			if (cnvEmojiText.ContainsKey(pOriginalCode)) {
				return cnvEmojiText[pOriginalCode];
			} else {
				return pOriginalCode;
			}
		}

		public static string GetWebCharToCommCode(string pOriginalCode) {
			if (cnvWebChar == null) {
				cnvWebChar = new Dictionary<string,string>();
				foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
					if (oPictograph.SoftbankReadonly || string.IsNullOrEmpty(oPictograph.SoftbankWebCode))
						continue;
					string sCode = oPictograph.SoftbankWebCode.Substring(1,oPictograph.SoftbankWebCode.Length - 1);
					if (!cnvWebChar.ContainsKey(sCode)) {
						cnvWebChar.Add(sCode,oPictograph.CommonCode);
					}
				}
			}

			if (cnvWebChar.ContainsKey(pOriginalCode)) {
				return "$x" + cnvWebChar[pOriginalCode] + ";";
			} else {
				return "";
			}
		}

		/// <summary>
		/// 文字列インスタンスから部分インスタンスを取得します。
		/// 絵文字ｺｰﾄﾞ($x0000)が末尾に含まれる場合は絵文字ｺｰﾄﾞを保障する形で部分インスタンスを取得します。
		/// </summary>
		/// <param name="pSubject">対象文字列</param>
		/// <param name="pLength">取得する文字長</param>
		/// <param name="pResultLength">処理結果の文字長</param>
		/// <returns>処理結果文字列</returns>
		public static string Substring(string pSubject,int pLength,out int pResultLength) {
			// assertion
			if (string.IsNullOrEmpty(pSubject))
				throw new ArgumentException("pSubject");
			if (pLength < 0)
				throw new ArgumentException("pLength");

			// implement
			pResultLength = 0;
			string result = string.Empty;

			int pictStartIndex = pSubject.IndexOf("$",pLength - 6);
			if (pictStartIndex > 0) {
				if (Regex.IsMatch(pSubject.Substring(pictStartIndex,7),@"^\$x[0-9]{4};$")) {
					pLength = pictStartIndex + 7;
				}
			}

			// return
			result = (pSubject.Length < pLength) ? pSubject : pSubject.Substring(0,pLength);
			pResultLength = result.Length;

			return result;
		}

		public static string ReplaceAcute(string sValue) {
			return Regex.Replace(sValue,"´|&acute;|&#180;","$180;",RegexOptions.Compiled);
		}
	}

}
