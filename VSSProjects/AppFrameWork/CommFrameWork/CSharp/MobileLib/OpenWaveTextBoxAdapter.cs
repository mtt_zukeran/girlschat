using System.Web.UI;
using System.Configuration;
using iBridCommLib;

namespace MobileLib {
	public class OpenWaveTextBoxAdapter:System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {
			iBMobileTextBox textBox = (iBMobileTextBox)this.Control;

			string sType = "text";
			if (!iBridUtil.GetStringValue(ConfigurationManager.AppSettings["DisableInputPasswordFeaturePhone"]).Equals("1")) {
				if (textBox.Password) {
					sType = "password";
				}
			}
			writer.WriteBeginTag("input");
			writer.WriteAttribute("name",textBox.ID);
			writer.WriteAttribute("type",sType);
			writer.WriteAttribute("value", textBox.Text);
			writer.WriteAttribute("size",textBox.Size.ToString());
			writer.WriteAttribute("maxlength",textBox.MaxLength.ToString());

			if (textBox.ZenkakuKana) {
				writer.WriteAttribute("istyle","1");
			}
			if (textBox.HankakuKana) {
				writer.WriteAttribute("istyle","2");
			}
			if (textBox.Alphameric) {
				writer.WriteAttribute("istyle","3");
			}
			if (textBox.Numeric) {
				writer.WriteAttribute("istyle","4");
			}

			writer.Write(" />");

			if (textBox.BreakAfter) {
				writer.Write("<br>");
			}
		}
	}
}