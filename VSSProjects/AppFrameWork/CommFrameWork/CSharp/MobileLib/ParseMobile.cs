using System;
using System.Web;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;

using iBridCommLib;
using MobileLib.Pictograph;

namespace MobileLib {
	[Serializable]
	public class ParseMobile:iBridCommLib.ParseUser {

		public string carrierCd;
		
		public ParseMobile() {}
		public ParseMobile(string pPattern,bool pIsCrawler,string pSessionId,string pDomain,string pBasePath,RegexOptions pOption,string pCarrierCd)
			: base(pPattern,pIsCrawler,pSessionId,pDomain,pBasePath,pOption) {
			carrierCd = pCarrierCd;
		}

		public override byte[] Perser(string pTag,string pArgument) {
			return ConvertEmoji(pTag);
		}

		public byte[] ConvertEmoji(string pTag) {
			return PictographConverter.Create(carrierCd).ConvertTag2Pict(pTag);
		}

	}
}