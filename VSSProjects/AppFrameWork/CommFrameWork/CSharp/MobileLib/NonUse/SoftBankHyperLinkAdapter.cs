using System.Web.UI;

namespace MobileLib {
	public class SoftBankHyperLinkAdapter:System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {
			System.Web.UI.WebControls.HyperLink hyperLink = (System.Web.UI.WebControls.HyperLink)this.Control;

			writer.WriteBeginTag("a");
			writer.WriteAttribute("id", hyperLink.ID);	
			writer.WriteAttribute("href", hyperLink.NavigateUrl);

			string sDisplay = "inline-block";
			string sBorderColor = hyperLink.BorderColor.Name;
			string sBorderWidth = hyperLink.BorderWidth.Value.ToString();
	
			writer.WriteAttribute("style", "display:" + sDisplay + ";" + "border-color:" + sBorderColor + ";" + "border-width:" + sBorderWidth);	// style�̏o��
			writer.Write(">");

			if (hyperLink.ImageUrl != null && hyperLink.ImageUrl.Equals("") != false) {
				writer.WriteBeginTag("img");
				writer.WriteAttribute("src", hyperLink.ImageUrl);
				writer.WriteAttribute("alt", "");
				writer.Write(">");
			}
			writer.WriteEndTag("a");
		}
	}
}
