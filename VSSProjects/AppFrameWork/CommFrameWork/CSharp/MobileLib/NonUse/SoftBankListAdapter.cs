using System.Web.UI;

namespace MobileLib {
	public class SoftBankListAdapter : System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {
			System.Web.UI.MobileControls.List list = (System.Web.UI.MobileControls.List)this.Control;
			list.RenderChildren(writer);
		}
	}
}
