using System.Web.UI;

namespace MobileLib {
	public class SoftBankCommandAdapter:System.Web.UI.Adapters.ControlAdapter {
		protected override void Render(HtmlTextWriter writer) {

			System.Web.UI.MobileControls.Command command =
			  (System.Web.UI.MobileControls.Command)this.Control;

			Mobile.SoftBankBeginTagCenter(command, writer);

			writer.WriteBeginTag("input");
			writer.WriteAttribute("type", "submit");
			writer.WriteAttribute("name", command.ID);
			writer.WriteAttribute("value", command.Text);
			writer.Write(">");

			Mobile.SoftBankEndTagCenter(command, writer);

			if (command.BreakAfter) {
				writer.Write("<br>");
			}
		}
	}
}
