using System.Web.UI;

namespace MobileLib {
	public class SoftBankFormAdapter:System.Web.UI.Adapters.PageAdapter {
		System.Web.UI.MobileControls.Form form;

		protected override void Render(HtmlTextWriter writer) {
			// モバイルFormコントロールを取得する。
			form = (System.Web.UI.MobileControls.Form)this.Control;
			writer.Write("<head>");
			writer.Write("</head>");
			writer.Write("\n");
			writer.WriteBeginTag("body");
			writer.WriteAttribute("bgcolor", form.BackColor.Name);
			writer.WriteAttribute("text", form.ForeColor.Name);
			writer.Write(">");
			writer.Write("\n");

			writer.WriteBeginTag("form");
			writer.WriteAttribute("method", form.Method.ToString());
			writer.WriteAttribute("action", form.MobilePage.RelativeFilePath + "?" + form.MobilePage.UniqueFilePathSuffix);
			writer.Write(">");
			writer.Write("\n");
			writer.WriteBeginTag("input");
			writer.WriteAttribute("type", "hidden");
			writer.WriteAttribute("name", "__VIEWSTATE");
			writer.WriteAttribute("value", form.MobilePage.ClientViewState);
			writer.Write(">");
			writer.Write("\n");

			RenderChildren(writer);

			writer.WriteEndTag("form");
			writer.WriteEndTag("body");
			writer.Write("\n");
		}

		protected override void RenderChildren(HtmlTextWriter output) {
			if (form.Controls.Count > 0) {
				// Render Children in reverse order.
				for (int i = 0;i < form.Controls.Count;i++) {
					if (form.Controls[i].ClientID.StartsWith("ctl")) {
						output.Write("<br>");
					} else {
						form.Controls[i].RenderControl(output);
					}
				}
			}
		}
	}
}
