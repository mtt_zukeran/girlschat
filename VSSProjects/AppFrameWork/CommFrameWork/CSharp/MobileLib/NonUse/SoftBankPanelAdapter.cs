using System.Web.UI;

namespace MobileLib {
	public class SoftBankPanelAdapter:System.Web.UI.Adapters.ControlAdapter {
		System.Web.UI.MobileControls.Panel panel;

		protected override void Render(HtmlTextWriter writer) {
			panel = (System.Web.UI.MobileControls.Panel)this.Control;

			writer.WriteBeginTag("div");
			string sAlign = panel.Alignment.ToString();
			string sSize = panel.Font.Size.ToString();
			writer.WriteAttribute("style", "text-align : " + sAlign + ";" + "text-size : " + sSize);	// style�̏o��
			writer.Write(">");
			RenderChildren(writer);
			writer.WriteEndTag("div");
		}

		protected override void RenderChildren(HtmlTextWriter output) {
			if (panel.Controls.Count > 0) {
				// Render Children in reverse order.
				for (int i = 0;i < panel.Controls.Count;i++) {
					if (panel.Controls[i].ClientID.StartsWith("ctl")) {
						output.Write("&nbsp;");
					} else {
						panel.Controls[i].RenderControl(output);
					}
				}
			}
		}
	}
}
