using System.Web.UI;

namespace MobileLib {

	public class SoftBankTextViewAdapter:System.Web.UI.Adapters.ControlAdapter {
		protected override void Render(HtmlTextWriter writer) {
			System.Web.UI.MobileControls.TextView textView = (System.Web.UI.MobileControls.TextView)this.Control;
			writer.Write(textView.Text);
		}
	}
}
