using System.Web.UI;

namespace MobileLib {
	public class SoftBankLiteralAdapter:System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {
			System.Web.UI.WebControls.Literal literal = (System.Web.UI.WebControls.Literal)this.Control;
			writer.Write(literal.Text);
		}
	}
}
