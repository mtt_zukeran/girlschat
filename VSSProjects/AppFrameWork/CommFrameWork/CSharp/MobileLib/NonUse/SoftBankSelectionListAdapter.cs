using System.Web.UI;

namespace MobileLib {
	public class SoftBankSelectionListAdapter:System.Web.UI.Adapters.ControlAdapter {
		protected override void Render(HtmlTextWriter writer) {

			System.Web.UI.MobileControls.SelectionList selectionList =(System.Web.UI.MobileControls.SelectionList)this.Control;

			writer.WriteBeginTag("select");
			writer.WriteAttribute("name", selectionList.ID); // nameの出力
			writer.WriteAttribute("id", selectionList.ID); // idの出力
			writer.Write(">");

			for (int i = 0; i < selectionList.Items.Count; i++) {
				writer.WriteBeginTag("option");
				writer.WriteAttribute("value", i.ToString());
				if (selectionList.Items[i].Selected) {
					writer.Write(" selected=\"selected\" ");
				}
				writer.Write(">");
				writer.Write(selectionList.Items[i].Text);
				writer.WriteEndTag("option");
			}

			writer.WriteEndTag("select");
		}
	}
}
