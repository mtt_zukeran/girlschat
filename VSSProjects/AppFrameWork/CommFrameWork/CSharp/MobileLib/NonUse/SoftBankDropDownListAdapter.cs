using System.Web.UI;

namespace MobileLib {
	public class SoftBankDropDownListAdapter:System.Web.UI.Adapters.ControlAdapter {
		protected override void Render(HtmlTextWriter writer) {

			System.Web.UI.WebControls.DropDownList dropDownList = (System.Web.UI.WebControls.DropDownList)this.Control;

			writer.WriteBeginTag("select");
			writer.WriteAttribute("name", dropDownList.ID);
			writer.WriteAttribute("id", dropDownList.ID);
			writer.Write(">");

			for (int i = 0;i < dropDownList.Items.Count;i++) {
				writer.WriteBeginTag("option");
				writer.WriteAttribute("value", dropDownList.Items[i].Value);
				if (dropDownList.Items[i].Selected) {
					writer.Write(" selected=\"selected\" ");
				}

				writer.Write(">");
				writer.Write(dropDownList.Items[i].Text);
				writer.WriteEndTag("option");
			}

			writer.WriteEndTag("select");
		}
	}
}
