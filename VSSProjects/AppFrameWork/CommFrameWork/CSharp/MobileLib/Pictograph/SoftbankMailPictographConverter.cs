using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iBridCommLib;

namespace MobileLib.Pictograph {
	public abstract partial class PictographConverter {
		/// <summary>
		/// Softbank用の絵文字変換機能を提供するクラス
		/// </summary>
		private class SoftbankMailPictographConverter : SoftbankPictographConverter {

			private static readonly Dictionary<string, string> _pictDef = new Dictionary<string, string>();
			protected override IDictionary<string, string> PictDef { get { return _pictDef; } }

			static SoftbankMailPictographConverter() {
				foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
					if (string.IsNullOrEmpty(oPictograph.SoftbankShiftJIS)) continue;
					_pictDef.Add(oPictograph.CommonCode, oPictograph.SoftbankShiftJIS);
				}
			}


			protected override byte[] ConvertPictCode2Pict(string pPictCode) {
				byte[] oPictBuffer = new byte[2];
				oPictBuffer[0] = Convert.ToByte(pPictCode.Substring(0, 2), 16);
				oPictBuffer[1] = Convert.ToByte(pPictCode.Substring(2, 2), 16);
				return oPictBuffer;
			}
			
			protected override string[] ConvertCommonCode2PictCodeList(string pCommonCode) {
				List<string> oPictCodeList = new List<string>();
				if (this.PictDef.ContainsKey(pCommonCode)) {
					foreach (string sPictCode in this.PictDef[pCommonCode].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries)) {
						if (sPictCode.Length < 3) {
							oPictCodeList.Add(sPictCode + "/");
						} else {
							oPictCodeList.Add(sPictCode);
						}
					}
				}
				return oPictCodeList.ToArray();
			}
		}
	}
}
