using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iBridCommLib;

namespace MobileLib.Pictograph {
	public abstract partial class PictographConverter {
		/// <summary>
		/// Softbank用の絵文字変換機能を提供するクラス
		/// </summary>
		private class SoftbankPictographConverter : PictographConverter {
			protected override bool GetNeedConvert2Image() {
				return iBridUtil.GetStringValue(ConfigurationManager.AppSettings["enabledPictograph"]).Equals("1");
			}

			private static readonly Dictionary<string, string> _pictDef = new Dictionary<string, string>();
			protected override IDictionary<string, string> PictDef { get { return _pictDef; } }

			static SoftbankPictographConverter() {
				foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
					if (string.IsNullOrEmpty(oPictograph.SoftbankWebCode)) continue;
					_pictDef.Add(oPictograph.CommonCode, oPictograph.SoftbankWebCode);
				}
			}
		
			protected override byte[] ConvertPictCode2Pict(string pPictCode) {
				byte[] oPictBuffer = new byte[5];
				char[] pPictCodeCharList = pPictCode.ToCharArray();
				oPictBuffer[0] = 0x1b; // SIコード
				oPictBuffer[1] = (byte)pPictCodeCharList[0];
				oPictBuffer[2] = (byte)pPictCodeCharList[1];
				oPictBuffer[3] = (byte)pPictCodeCharList[2];
				oPictBuffer[4] = 0x0f; // SOコード
				return oPictBuffer;
			}

			protected override string[] ConvertCommonCode2PictCodeList(string pCommonCode) {
				List<string> oPictCodeList = new List<string>();
				if (this.PictDef.ContainsKey(pCommonCode)) {
					foreach(string sPictCode in this.PictDef[pCommonCode].Split(new char[]{'/'},StringSplitOptions.RemoveEmptyEntries)){
						if (sPictCode.Length < 3) {
							oPictCodeList.Add(sPictCode + "/");
						} else {
							oPictCodeList.Add(sPictCode);
						}
					}
				}
				return oPictCodeList.ToArray();
			}
		}
	}
}
