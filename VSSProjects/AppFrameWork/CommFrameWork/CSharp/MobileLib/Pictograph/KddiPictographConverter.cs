using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iBridCommLib;

namespace MobileLib.Pictograph {
	public abstract partial class PictographConverter {
		/// <summary>
		/// KDDI用の絵文字変換機能を提供するクラス
		/// </summary>
		private class KddiPictographConverter : PictographConverter {
			protected override bool GetNeedConvert2Image() {
				return iBridUtil.GetStringValue(ConfigurationManager.AppSettings["enabledPictograph"]).Equals("1");
			}

			private static readonly Dictionary<string, string> _pictDef = new Dictionary<string, string>();
			protected override IDictionary<string, string> PictDef { get { return _pictDef; } }

			static KddiPictographConverter() {
				foreach (PictographDataSet.PictographRow oPictograph in PictographDefHolder.PictographDef.Pictograph) {
					if(string.IsNullOrEmpty(oPictograph.KddiShiftJIS)) continue;
					_pictDef.Add(oPictograph.CommonCode, oPictograph.KddiShiftJIS);
				}
			}

			protected override byte[] ConvertPictCode2Pict(string pPictCode) {
				byte[] oPictBuffer = new byte[2];
				oPictBuffer[0] = Convert.ToByte(pPictCode.Substring(0, 2), 16);
				oPictBuffer[1] = Convert.ToByte(pPictCode.Substring(2, 2), 16);
				return oPictBuffer;
			}
		}
	}
}
