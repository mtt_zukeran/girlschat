using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

using iBridCommLib;

namespace MobileLib.Pictograph {
	
	public abstract partial class PictographConverter {
		public const string PICT_IMG_TAG_PATTERN = "(<img src=/pictograph/[^.]*[.]gif [^>]*>)";
		
		protected static readonly Encoding encodingShiftJis = Encoding.GetEncoding(932);
		private static readonly Regex _regexTag = new Regex(@"^(?:\$x)(?<common_code>.*)(?:;)$", RegexOptions.Compiled);
		
		private bool needConvert2Image = false;
		public bool NeedConvert2Image{
			get{return this.needConvert2Image;}
			set{this.needConvert2Image = value;}
		}

		/// <summary>
		/// 絵文字変換に失敗した場合、GIFイメージで出力するかどうかを示す値を取得する
		/// </summary>
		protected abstract bool GetNeedConvert2Image();
		protected abstract IDictionary<string,string> PictDef {get;}
		protected abstract byte[] ConvertPictCode2Pict(string pPictCode);
		
		/// <summary>
		/// ｷｬﾘｱｺｰﾄﾞに応じた絵文字変換クラスを生成するFactoryメソッド
		/// </summary>
		/// <param name="pCarrierCd">ｷｬﾘｱｺｰﾄﾞ</param>
		/// <returns>絵文字変換クラス</returns>
		public static PictographConverter Create(string pCarrierCd){
			PictographConverter oPictConverter = null;
			switch(pCarrierCd){
				case Mobile.DOCOMO:
					oPictConverter = new DocomoPictographConverter();
					break;
				case Mobile.KDDI:
					oPictConverter = new KddiPictographConverter();
					break;
				case Mobile.SOFTBANK:
					oPictConverter = new SoftbankPictographConverter();
					break;
				case Mobile.SOFTBANK_MAIL:
					oPictConverter = new SoftbankMailPictographConverter();
					break;
				case Mobile.ANDROID: 
				case Mobile.IPHONE:
					oPictConverter = new DefaultPictographConverter();
					break;				
				case Mobile.ACCELMAIL:
					oPictConverter = new AccelMailPictographConverter();
					break;
				default:
					oPictConverter = new DefaultPictographConverter();
					break;
			}
			return oPictConverter;
		}
		private PictographConverter() {
			this.needConvert2Image = GetNeedConvert2Image();
		}
		public static PictographConverter CreateTextPictographConverter() {
			return new TextPictographConverter();
		}
		
		public virtual byte[] ConvertTag2Pict(string pTag) {
			return ConvertTag2Pict(pTag,false);
		}

		
		/// <summary>
		/// 変数(タグ)を絵文字に変換する
		/// </summary>
		/// <param name="pTag">変数(タグ)</param>
		/// <returns>絵文字</returns>
		public virtual byte[] ConvertTag2Pict(string pTag,bool pMissmatchOnly) {
			List<byte> oPictBuffer = new List<byte>();
			
			string sCommonCode = ConvertTag2CommonCode(pTag);
						
			foreach(string sPictCode in ConvertCommonCode2PictCodeList(sCommonCode)){
				if (oPictBuffer.Count > 0) {
					oPictBuffer.Add(47);// '/'
				}
				oPictBuffer.AddRange(this.ConvertPictCode2Pict(sPictCode));
			}
			
			// 変換仕切れなかったものをイメージに変換する
			if (oPictBuffer.Count == 0) {
				if(this.NeedConvert2Image){
					oPictBuffer = new List<byte>(this.ConvertCommonCode2ImagePict(sCommonCode));
				}else{
					oPictBuffer = new List<byte>(this.ConvertCommonCode2Text(sCommonCode)); 
				}
			}else{
				if(pMissmatchOnly){
					oPictBuffer.Clear();
					oPictBuffer.AddRange(encodingShiftJis.GetBytes(pTag));
				}
			}
			
			return oPictBuffer.ToArray();
		}


		protected virtual string[] ConvertCommonCode2PictCodeList(string pCommonCode) {
			string[] oPictCodeList = new string[] { };
			if (this.PictDef.ContainsKey(pCommonCode)) {
				oPictCodeList = this.PictDef[pCommonCode].Split('/');
			}
			return oPictCodeList;
		}
		

		/// <summary>
		/// 変数を共通コードに変換する
		/// </summary>
		/// <param name="pTag">変数</param>
		/// <returns>共通コード</returns>
		private string ConvertTag2CommonCode(string pTag){
			Match oMatch = _regexTag.Match(pTag);
			return (oMatch.Success) ? oMatch.Groups["common_code"].Value.Trim() : string.Empty;
		}
		
		/// <summary>
		/// 共通コードを絵文字画像に変換する
		/// </summary>
		/// <param name="pCommCode">共通コード</param>
		/// <returns>絵文字画像</returns>
		private byte[] ConvertCommonCode2ImagePict(string pCommCode) {
			string sImageTag = string.Format("<img src=/pictograph/{0}.gif height='12' width='12' border='0'>", pCommCode);
			return encodingShiftJis.GetBytes(sImageTag);
		}
		
		protected virtual byte[] ConvertCommonCode2Text(string pCommonCode){
			string sText = string.Empty;
			
			PictographDataSet.PictographRow oPictograph = PictographDefHolder.PictographDef.Pictograph.FindByCommonCode(pCommonCode);
			if(oPictograph != null){
				sText = oPictograph.Text ?? string.Empty;
			}

			return encodingShiftJis.GetBytes(sText);						
		}



		#region □■□ inner class(DefaultPictographConverter) □■□ =================================================
		private class DefaultPictographConverter : PictographConverter {
			protected override bool GetNeedConvert2Image() {
				return iBridUtil.GetStringValue(ConfigurationManager.AppSettings["enabledPictograph"]).Equals("1");
			}
			private static readonly Dictionary<string, string> _pictDef = new Dictionary<string, string>();
			protected override IDictionary<string, string> PictDef { get { return _pictDef; } }

			protected override string[] ConvertCommonCode2PictCodeList(string pCommonCode) {
				return new string[0];
			}

			protected override byte[] ConvertPictCode2Pict(string pPictCode) {
				return new byte[0];
			}
		}
		#endregion ====================================================================================================
		
	}
}
