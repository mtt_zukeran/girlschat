using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iBridCommLib;

namespace MobileLib.Pictograph {
	public abstract partial class PictographConverter {
		/// <summary>
		/// Text用の絵文字変換機能を提供するクラス
		/// </summary>
		private class TextPictographConverter : PictographConverter {
			protected override bool GetNeedConvert2Image() { return false; }

			private static readonly Dictionary<string, string> _pictDef = new Dictionary<string, string>();
			protected override IDictionary<string, string> PictDef { get { return _pictDef; } }

			protected override byte[] ConvertPictCode2Pict(string pPictCode) {
				return new byte[]{};
			}

			protected override byte[] ConvertCommonCode2Text(string pCommonCode) {
				byte[] oBuffer = base.ConvertCommonCode2Text(pCommonCode);
				
				if(oBuffer.Length == 0){
					// 該当のタグが存在しない場合はもとの値をそのまま利用する
					oBuffer = SysConst.EncodingShiftJIS.GetBytes(pCommonCode);
				}
				return oBuffer;				
			}
		}
	}
}
