using System.Web.UI;
using System.Configuration;
using iBridCommLib;

namespace MobileLib {
	public class SoftBankTextBoxAdapter:System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {
			iBMobileTextBox textBox = (iBMobileTextBox)this.Control;

			string sType = "text";
			if (!iBridUtil.GetStringValue(ConfigurationManager.AppSettings["DisableInputPasswordFeaturePhone"]).Equals("1")) {
				if (textBox.Password) {
					sType = "password";
				}
			}

			writer.WriteBeginTag("span");
			writer.WriteAttribute("style","color:#000000");
			writer.Write(">\n");

			writer.WriteBeginTag("input");
			writer.WriteAttribute("type",sType);
			writer.WriteAttribute("value", textBox.Text);
			writer.WriteAttribute("name", textBox.ID);
			writer.WriteAttribute("size", textBox.Size.ToString());
			writer.WriteAttribute("maxlength",textBox.MaxLength.ToString());

			if (textBox.ZenkakuKana) {
				writer.WriteAttribute("istyle","1");
				writer.WriteAttribute("mode", "hiragana");
			}
			if (textBox.HankakuKana) {
				writer.WriteAttribute("istyle","2");
				writer.WriteAttribute("mode", "hankakukana");
			}
			if (textBox.Alphameric) {
				writer.WriteAttribute("istyle","3");
				writer.WriteAttribute("mode", "alphabet");
			}
			if (textBox.Numeric) {
				writer.WriteAttribute("istyle","4");
				writer.WriteAttribute("mode", "numeric");
			}

			writer.Write(">\n");

			writer.Write("</span>\n");
			
			if (textBox.BreakAfter) {
				writer.Write("<br>");
			}
		}
	}
}