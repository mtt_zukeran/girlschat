using System.Web.UI;
using System.Web.UI.MobileControls;
using iBridCommLib;

namespace MobileLib {
	public class DocomoLabelAdapter:System.Web.UI.Adapters.ControlAdapter {
		protected override void Render(HtmlTextWriter writer) {

			MobileLib.iBMobileLabel label = (MobileLib.iBMobileLabel)this.Control;

			if ((!label.Visible) || (iBridUtil.GetStringValue(label.Text).Equals(""))) {
				return;
			}

			Mobile.DocomoBeginTagCenter(label,writer);
			Mobile.DocomoBeginTagFont(label,writer);

			if ((label.StyleReference != null) && (label.StyleReference.Equals("title")) && (label.Font.Bold == BooleanOption.False)) {
				writer.Write("<b>" + label.Text + "</b>");
			} else {
				writer.Write(label.Text);
			}

			Mobile.DocomoEndTagFont(label,writer);
			Mobile.DocomoEndTagCenter(label,writer);

			if (label.BreakAfter) {
				writer.Write("<br>");
			}
			writer.Write("\n");
		}
	}
}
