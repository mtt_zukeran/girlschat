using System;
using System.Web;
using System.Web.Mobile;

namespace MobileLib {
	public class CapabilityEvaluators {

		private readonly static string[] SoftBankUserAgentList =
		  new string[]{
			  "J-PHONE",
			  "Vodafone",
			  "SoftBank",
			  "J-EMULATOR",  // エミュレータ用
			  "Vemulator",   // エミュレータ用
			};

		public static bool isSoftBank(MobileCapabilities capabilities,string argument) {
			try {
				string[] agentInfo =
				  HttpContext.Current.Request.UserAgent.Split('/');

				if (agentInfo.Length == 0)
					return false;

				return Array.IndexOf(SoftBankUserAgentList,agentInfo[0]) != -1;
			} catch {
				return false;
			}
		}
	}
}