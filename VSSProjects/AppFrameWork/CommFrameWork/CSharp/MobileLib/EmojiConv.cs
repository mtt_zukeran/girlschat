using System;
using System.Web;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace MobileLib {

	public class EmojiConv {

		public Dictionary<uint,string> cnvDocomo2Softbank;

		private uint defaultCode = 0xE6E3; // 変換できない場合の代替絵文字

		public string carrier;

		public EmojiConv(string pCarrier) {
			carrier = pCarrier;
			cnvDocomo2Softbank = new Dictionary<uint,string>();
			cnvDocomo2Softbank.Add(0,"G@j");
			cnvDocomo2Softbank.Add(63647,"$Gj");
			cnvDocomo2Softbank.Add(63704,"$G]");
			cnvDocomo2Softbank.Add(63717,"$O4");
			cnvDocomo2Softbank.Add(63721,"$G*");
			cnvDocomo2Softbank.Add(63730,"$P9");
			cnvDocomo2Softbank.Add(63733,"$G1");
			cnvDocomo2Softbank.Add(63826,"$O!");
			cnvDocomo2Softbank.Add(63863,"$E!");
			cnvDocomo2Softbank.Add(63866,"$F5");
			cnvDocomo2Softbank.Add(63867,"$F6");
			cnvDocomo2Softbank.Add(63873,"$E4");
			cnvDocomo2Softbank.Add(63879,"$F<");
			cnvDocomo2Softbank.Add(63880,"$F=");
			cnvDocomo2Softbank.Add(63881,"$F>");
			cnvDocomo2Softbank.Add(63882,"$F?");
			cnvDocomo2Softbank.Add(63883,"$F@");
			cnvDocomo2Softbank.Add(63884,"$FA");
			cnvDocomo2Softbank.Add(63885,"$FB");
			cnvDocomo2Softbank.Add(63886,"$FC");
			cnvDocomo2Softbank.Add(63887,"$FD");
			cnvDocomo2Softbank.Add(63889,"$GB");
			cnvDocomo2Softbank.Add(63893,"$P5");
			cnvDocomo2Softbank.Add(63898,"$FR");
			cnvDocomo2Softbank.Add(63949,"$P)");
			cnvDocomo2Softbank.Add(63976,"$O$");
			cnvDocomo2Softbank.Add(63988,"$QC");
		}

		public string ConvertEmoji(string htmlSource) {
			string sResult = "";

			using (MemoryStream memStrm = new MemoryStream()) {
				Encoding encSjis = Encoding.GetEncoding(932); // shift-jis

				int iCopiedPos = 0;
				byte[] orgBytes;

				Regex rgx = new Regex(@"@@\d{4,5};");
				Match rgxMatch = rgx.Match(htmlSource);

				while (rgxMatch.Success) {

					// 絵文字が見つかった位置までメモリ・ストリームに書き出す
					int len = rgxMatch.Index - iCopiedPos;
					if (len > 0) {
						// Shift-JISでストリームに書き出す
						orgBytes = encSjis.GetBytes(htmlSource.Substring(iCopiedPos,len));
						memStrm.Write(orgBytes,0,orgBytes.Length);
					}

					// 絵文字を変換
					uint emojiCode = 0;
					int emojiLen = 0;
					byte[] sjisEmoji = new byte[8];
					emojiCode = parseEmojiCode(rgxMatch.Value);

					if (this.carrier.Equals(Mobile.DOCOMO)) {
						sjisEmoji = getDocomo(emojiCode,out emojiLen);
					} else if (this.carrier.Equals(Mobile.SOFTBANK)) {
						sjisEmoji = getSoftBank(emojiCode,out emojiLen);
					} else if (this.carrier.Equals(Mobile.KDDI)) {
						sjisEmoji = getDocomo(emojiCode,out emojiLen);
					} else {
						sjisEmoji[0] = (byte)'@';
						emojiLen = 1;
					}

					memStrm.Write(sjisEmoji,0,emojiLen);

					iCopiedPos = rgxMatch.Index + rgxMatch.Length;
					rgxMatch = rgxMatch.NextMatch();
				}

				// 絵文字以降をメモリ・ストリームに書き出す
				if (iCopiedPos < htmlSource.Length) {
					orgBytes = encSjis.GetBytes(htmlSource.Substring(iCopiedPos));
					memStrm.Write(orgBytes,0,orgBytes.Length);
				}
				sResult = encSjis.GetString(memStrm.ToArray()); // 文字列に戻す
			}

			return sResult;

		}

		private uint parseEmojiCode(string sval) {
			uint code;
			try {
				sval = sval.Replace(";","");
				code = Convert.ToUInt32(sval.Substring(2),10);
			} catch {
				code = 0;
			}
			return code;

		}

		private byte[] getDocomo(uint docomoCode,out int length) {
			length = 2;
			byte[] webcode = new byte[length];
			if (docomoCode == 0) { // 絵文字コードが判別できない場合 
				docomoCode = defaultCode;
			}
			webcode[0] = (byte)(docomoCode >> 8);
			webcode[1] = (byte)(docomoCode);
			return webcode;
		}

		private byte[] getSoftBank(uint docomoCode,out int length) {
			length = 5;
			byte[] webcode = new byte[length];
			string sWebCode;

			if (this.cnvDocomo2Softbank.ContainsKey(docomoCode)) {
				sWebCode = this.cnvDocomo2Softbank[docomoCode];
				char[] s = sWebCode.ToCharArray();
				webcode[0] = 0x1b; // SIコード
				webcode[1] = (byte)s[0];
				webcode[2] = (byte)s[1];
				webcode[3] = (byte)s[2];
				webcode[4] = 0x0f; // SOコード
			} else {
				webcode[0] = 0x1b;
				webcode[1] = (byte)'$';
				webcode[2] = (byte)'G';
				webcode[3] = (byte)'j';
				webcode[4] = 0x0f;
			}
			return webcode;
		}
	}
}