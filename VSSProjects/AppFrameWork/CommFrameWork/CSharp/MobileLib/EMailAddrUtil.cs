using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MobileLib {
	/// <summary>
	/// メールアドレスに関する機能を提供するUtilityクラス
	/// </summary>
	public sealed class EMailAddrUtil {

		private static readonly Regex _regexMail = new Regex(@"^(.*)(@)(?<domain>.*)$", RegexOptions.Singleline | RegexOptions.Compiled);
		private static IList<string> _docomoDomainList = new List<string>();
		private static IList<string> _kddiDomainList = new List<string>();
		private static IList<string> _softbankDomainList = new List<string>();

		static EMailAddrUtil() {
			// for docomo
			_docomoDomainList.Add("docomo.ne.jp");
			// for kddi
			_kddiDomainList.Add("ezweb.ne.jp");
			// for softbank
			_softbankDomainList.Add("softbank.ne.jp");
			_softbankDomainList.Add("disney.ne.jp");
			_softbankDomainList.Add("d.vodafone.ne.jp");
			_softbankDomainList.Add("h.vodafone.ne.jp");
			_softbankDomainList.Add("t.vodafone.ne.jp");
			_softbankDomainList.Add("c.vodafone.ne.jp");
			_softbankDomainList.Add("k.vodafone.ne.jp");
			_softbankDomainList.Add("r.vodafone.ne.jp");
			_softbankDomainList.Add("n.vodafone.ne.jp");
			_softbankDomainList.Add("s.vodafone.ne.jp");
			_softbankDomainList.Add("q.vodafone.ne.jp");
		}

		private EMailAddrUtil() {}
		

		public static string GetCarrierCd(string pEMailAddr) {
			string sCarrierCd = Mobile.CARRIER_OTHERS;
			
			Match oMatch = _regexMail.Match(pEMailAddr);
			if(oMatch.Success){
				string sDomain =oMatch.Groups["domain"].Value ?? string.Empty;
				sDomain = sDomain.ToLower();
				
				if(_docomoDomainList.Contains(sDomain)){
					sCarrierCd = Mobile.DOCOMO;
				} else if (_kddiDomainList.Contains(sDomain)) {
					sCarrierCd = Mobile.KDDI;
				} else if (_softbankDomainList.Contains(sDomain)) {
					sCarrierCd = Mobile.SOFTBANK;
				}
				
			}
			
			return sCarrierCd;			
		}

		public static bool IsCorrectEmailAddress(string pEmailAddr) {
			if (Regex.IsMatch(pEmailAddr,@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$",RegexOptions.IgnoreCase)) {
				return true;
			} else {
				return false;
			}
		}
	}
}
