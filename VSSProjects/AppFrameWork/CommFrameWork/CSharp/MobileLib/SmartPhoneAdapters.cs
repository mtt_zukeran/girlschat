using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.Adapters;
using System.Web.UI;
using System.Web.UI.MobileControls;

using iBridCommLib;

namespace MobileLib {
	#region ������ Base ControlAdapte ������ ==========================================================================

	public abstract class SmartPhoneControlAdapter :ControlAdapter{
		protected virtual void WriteBeginTagCenter(MobileControl pControl, HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("<center>");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("<div align='right'>");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("<div align='left'>");
			}
		}

		protected virtual void WriteEndTagCenter(MobileControl pControl, HtmlTextWriter writer) {
			if (pControl.Alignment == Alignment.Center) {
				writer.Write("</center>");
			} else if (pControl.Alignment == Alignment.Right) {
				writer.Write("</div>");
			} else if (pControl.Alignment == Alignment.Left) {
				writer.Write("</div>");
			}
		}

		public static void WriteBeginTagFont(MobileControl pControl, HtmlTextWriter writer) {
			if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("0")) ||
				(!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet"))) {

				writer.WriteBeginTag("font");

				if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("")) && (pControl.ForeColor.Name.Equals("0") == false)) {
					if ((pControl.ForeColor.Name.StartsWith("ff")) && (pControl.ForeColor.Name.Length == 8)) {
						writer.WriteAttribute("color", "#" + pControl.ForeColor.Name.Substring(2));
					} else {
						writer.WriteAttribute("color", pControl.ForeColor.Name);
					}
				}

				if (!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet")) {
					if (pControl.Font.Size == FontSize.Small) {
						writer.WriteAttribute("size", "2");
					}
				}
				writer.Write(">");
			}
		}
		

		public static void WriteEndTagFont(MobileControl pControl, HtmlTextWriter writer) {
			if ((!iBridUtil.GetStringValue(pControl.ForeColor.Name).Equals("0")) ||
				(!iBridUtil.GetStringValue(pControl.Font.Size).Equals("NotSet"))) {
				writer.WriteEndTag("font");
			}
		}

	}
	#endregion ========================================================================================================


	#region ������ Label ControlAdapte ������ ==========================================================================
	public abstract class SmartPhoneLableAdapter : SmartPhoneControlAdapter {

		protected override void Render(HtmlTextWriter writer) {

			iBMobileLabel label = (iBMobileLabel)this.Control;

			if ((!label.Visible) || (iBridUtil.GetStringValue(label.Text).Equals(""))) {
				return;
			}

			WriteBeginTagCenter(label, writer);
			WriteBeginTagFont(label, writer);

			if ((label.StyleReference != null) && (label.StyleReference.Equals("title")) && (label.Font.Bold == BooleanOption.False)) {
				writer.Write("<b>" + label.Text + "</b>");
			} else {
				writer.Write(label.Text);
			}

			WriteEndTagFont(label, writer);
			WriteEndTagCenter(label, writer);

			if (label.BreakAfter) {
				writer.Write("<br>");
			}
			writer.Write(Environment.NewLine);
		}
	}
	#endregion ========================================================================================================

	/// <summary>
	/// SmartPhone�p��TextBoxAdapter���ی^
	/// </summary>
	public abstract class SmartPhoneTextBoxAdapter : ControlAdapter
	{
		protected override void Render(HtmlTextWriter writer)
		{
			if (this.Control is iBMobileTextBox) {
				iBMobileTextBox textBox = (iBMobileTextBox)this.Control;

				string sType = "text";

				if (textBox.Password) {
					sType = "password";
				} 
				
				if (textBox.DetailType) {
					if (textBox.Numeric) {
						sType = "tel";
					} else if (textBox.Email) {
						sType = "email";
					}
				}

				writer.WriteBeginTag("input");
				writer.WriteAttribute("id",textBox.ID);
				writer.WriteAttribute("type",sType);
				writer.WriteAttribute("value",textBox.Text);
				writer.WriteAttribute("name",textBox.ID);
				writer.WriteAttribute("size",textBox.Size.ToString());
				writer.WriteAttribute("maxlength",textBox.MaxLength.ToString());
				if (textBox.EmojiPaletteEnabled) {
					writer.WriteAttribute("class","emoji_enabled");
				}

				writer.Write(" />");

				if (textBox.BreakAfter) {
					writer.Write("<br />");
				}

			} else {
				base.Render(writer);
			}
		}
	}
}
