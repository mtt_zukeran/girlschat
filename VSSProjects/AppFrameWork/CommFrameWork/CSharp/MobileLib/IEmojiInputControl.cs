using System;
using System.Collections.Generic;
using System.Text;

namespace MobileLib
{
	/// <summary>
	/// 絵文字表示用のインタフェース
	/// </summary>
	public interface IEmojiInputControl
	{
		bool EmojiPaletteEnabled
		{
			get;
			set;
		}
	}
}
