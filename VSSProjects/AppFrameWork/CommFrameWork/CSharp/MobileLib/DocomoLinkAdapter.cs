using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Drawing;
using iBridCommLib;

namespace MobileLib {

	public class DocomoLinkAdapter:System.Web.UI.Adapters.ControlAdapter {

		protected override void Render(HtmlTextWriter writer) {
			MobileLib.iBMobileLink link = (MobileLib.iBMobileLink)this.Control;

			if (!link.Visible) {
				return;
			}

			Mobile.DocomoBeginTagCenter(link,writer);

			if (iBridUtil.GetStringValue(link.Mark).Equals("") == false) {
				Mobile.DocomoBeginTagFontSize(link,writer);
				writer.WriteBeginTag("a>");
				writer.Write(link.Mark);
				writer.WriteEndTag("a");
				Mobile.DocomoEndTagFontSize(link,writer);
			}

			if (link.Enabled) {
				writer.WriteBeginTag("a");
				writer.WriteAttribute("href",link.NavigateUrl);
				if (iBridUtil.GetStringValue(link.AccessKey).Equals("") == false) {
					writer.WriteAttribute("accesskey",link.AccessKey);
				}
				writer.Write(">");
			} else {
				link.ForeColor = Color.FromName(link.DisableColor);
			}

			if (link.ForeColor == Color.Empty) {
				string sLinkColor = iBridUtil.GetStringValue(HttpContext.Current.Session["LinkColor"]);
				if (!sLinkColor.Equals("")) {
					link.ForeColor = Color.FromName(sLinkColor);
				}
			}
			Mobile.DocomoBeginTagFont(link,writer);

			writer.Write(link.Text);

			Mobile.DocomoEndTagFont(link,writer);

			if (link.Enabled) {
				writer.WriteEndTag("a");
			}

			Mobile.DocomoEndTagCenter(link,writer);

			if (link.BreakAfter) {
				writer.Write("<br>");
			}
			writer.Write("\n");
		}
	}
}
