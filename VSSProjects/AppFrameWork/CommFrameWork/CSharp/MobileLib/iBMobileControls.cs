using System;
using System.Web;
using System.Web.UI;
using System.Collections.Specialized;
using System.Web.UI.MobileControls;

namespace MobileLib {
	public class iBMobileTextBox:TextBox,IPostBackDataHandler,System.Web.UI.ITextControl, IEmojiInputControl {
		private bool zenkakuKana;
		private bool hankakuKana;
		private bool alphameric;
		private bool email = false;
		private bool emojiPaletteEnabled = false;
		private bool detailType = false;

		protected override void OnPreRender(EventArgs e) {
			base.OnPreRender(e);
			if(Page is IEarlyParseEmoji){
				((IEarlyParseEmoji)Page).EarlyParseEmoji(this);
			}
		}
		
		public new bool LoadPostData(string postDataKey,NameValueCollection values) {
			string presentValue = Text;
			string postedValue = values[postDataKey];
			if (postedValue.IndexOf("\n") < 0) {
				Text = postedValue;
				return true;
			}
			return false;
		}

		public bool ZenkakuKana {
			get {
				return zenkakuKana;
			}
			set {
				zenkakuKana = value;
			}
		}

		public bool HankakuKana {
			get {
				return hankakuKana;
			}
			set {
				hankakuKana = value;
			}
		}

		public bool Alphameric {
			get {
				return alphameric;
			}
			set {
				alphameric = value;
			}
		}

		public bool Email {
			get {
				return email;
			}
			set {
				email = value;
			}
		}

		public bool DetailType {
			get {
				return detailType;
			}
			set {
				detailType = value;
			}
		}

		#region IEmojiInputControl の実装

		public bool EmojiPaletteEnabled
		{
			get
			{
				return emojiPaletteEnabled;
			}
			set
			{
				emojiPaletteEnabled = value;
			}
		}
		#endregion
	}

	public class iBMobileImage:Image {
		private string align;
		private string accessKey;
		private string width;
		private string vspace;
		private string hspace;

		public string Align {
			get {
				return align;
			}
			set {
				align = value;
			}
		}

		public string Width {
			get {
				return width;
			}
			set {
				width = value;
			}
		}

		public string VSpace {
			get {
				return vspace;
			}
			set {
				vspace = value;
			}
		}

		public string HSpace {
			get {
				return hspace;
			}
			set {
				hspace = value;
			}
		}

		public string AccessKey {
			get {
				return accessKey;
			}
			set {
				accessKey = value;
			}
		}

		public iBMobileImage() {
		}
	}

	public class iBMobileLiteralText:LiteralText {

		public new string Text {
			get {
				return base.Text;
			}
			set {
				value = value.Replace("\n","");
				base.Text = value.Replace("\r","");
			}
		}

		public iBMobileLiteralText() {
		}

		protected override void Render(HtmlTextWriter writer) {
			if (this.Visible) {
				writer.Write(this.Text);
			}
		}
	}

	public class iBMobileMenuLabel:LiteralText {

		private string bgColor = "#3366ff";
		private string frameColor = "#3366ff";
		private string width = "100%";
		private string fontSize = "2";
		private string foreColor = "#FFFFFF";

		public iBMobileMenuLabel() {
		}

		public string BgColor {
			get {
				return bgColor;
			}
			set {
				bgColor = value;
			}
		}

		public string FrameColor {
			get {
				return frameColor;
			}
			set {
				frameColor = value;
			}
		}

		public string Width {
			get {
				return width;
			}
			set {
				width = value;
			}
		}

		public string FontSize {
			get {
				return fontSize;
			}
			set {
				fontSize = value;
			}
		}

		protected override void Render(HtmlTextWriter writer) {
			string sTag = string.Format(
				"<table bgcolor=\"{5}\" width=\"{2}\" border=\"0\">" +
					"<tr>" +
						"<td bgcolor=\"{1}\">" +
							"<div align=\"center\">" +
								"<font size=\"{3}\" color=\"{4}\">{0}</font>" +
							"</div>" +
						"</td>" +
					"</tr>" +
				"</table>"
				,Text,bgColor,width,fontSize,foreColor,frameColor);
			writer.Write(sTag);
		}
	}

	public class iBMobileCalender:LiteralText {

		private string curMonth;

		private string colorSunday;
		private string colorSatday;
		private string colorWeekDay;

		private string[] aryDate;
		private string[] aryLink;
		private bool[] aryLinkFlag;
		private int[] aryDayOfWeek;

		public iBMobileCalender() {
		}

		public string CurMonth {
			get {
				return curMonth;
			}
			set {
				curMonth = value;
			}
		}

		public string ColorSunday {
			get {
				return colorSunday;
			}
			set {
				colorSunday = value;
			}
		}

		public string ColorSatday {
			get {
				return colorSatday;
			}
			set {
				colorSatday = value;
			}
		}

		public string ColorWeekday {
			get {
				return colorWeekDay;
			}
			set {
				colorWeekDay = value;
			}
		}

		public bool[] DateLinkFlag {
			set {
				aryLinkFlag = value;
			}
		}

		public string[] DateText {
			set {
				aryDate = value;
			}
		}

		public int[] DayOfWeek {
			set {
				aryDayOfWeek = value;
			}
		}

		public string[] DateLink {
			set {
				aryLink = value;
			}
		}


		protected override void Render(HtmlTextWriter writer) {
			writer.Write(string.Format("<span id=\"cdrClender_lblYearMonth\" style=\"font-size:Small;\">{0}年{1}月</span><br>",curMonth.Substring(0,4),curMonth.Substring(5,2)));
			writer.Write(string.Format("<font color=\"{0}\">日</font>\n",colorSunday));
			writer.Write(string.Format("<font color=\"{0}\">月</font>\n",colorWeekDay));
			writer.Write(string.Format("<font color=\"{0}\">火</font>\n",colorWeekDay));
			writer.Write(string.Format("<font color=\"{0}\">水</font>\n",colorWeekDay));
			writer.Write(string.Format("<font color=\"{0}\">木</font>\n",colorWeekDay));
			writer.Write(string.Format("<font color=\"{0}\">金</font>\n",colorWeekDay));
			writer.Write(string.Format("<font color=\"{0}\">土</font><br>\n",colorSatday));
			writer.Write("<div id=\"cdrClender_pnlCalendar\">");

			string sDate = "",sDayColor = "";
			bool bDate = true;
			int iCmp;

			for (int i = 1;i <= aryDate.Length;i++) {
				sDate = "";
				sDayColor = "";
				bDate = true;
				iCmp = curMonth.CompareTo(aryDate[i - 1].Substring(0,7));

				if (iCmp == 0) {
					sDate = aryDate[i - 1].Substring(8,2);
				} else if (iCmp > 0) {
					sDate = "--";
					bDate = false;
				} else {
					sDate = "";
					bDate = false;
				}

				switch (aryDayOfWeek[i - 1]) {
					case 0:
						sDayColor = colorSunday;
						break;
					case 6:
						sDayColor = colorSatday;
						break;
					default:
						sDayColor = colorWeekDay;
						break;
				}

				if ((aryLinkFlag[i - 1]) && (bDate)) {
					writer.Write(string.Format("<a href=\"{0}\"><font color=\"{1}\">{2}</font></a>\n",aryLink[i - 1],sDayColor,sDate));
				} else {
					writer.Write(string.Format("<font color=\"{0}\">{1}</font>\n",sDayColor,sDate));
				}
				if ((i % 7) == 0) {
					writer.Write("<br />\n");
				}
			}
		}
	}

	public class iBMobileLink:Link {
		private string disableColor = "#000000";
		private string mark;
		private string accessKey;
		private bool enabled = true;

		public string DisableColor {
			get {
				return disableColor;
			}
			set {
				disableColor = value;
			}
		}

		public string Mark {
			get {
				return mark;
			}
			set {
				mark = value;
			}
		}

		public string AccessKey {
			get {
				return accessKey;
			}
			set {
				accessKey = value;
			}
		}

		public bool Enabled {
			get {
				return enabled;
			}
			set {
				enabled = value;
			}
		}

		public iBMobileLink() {
		}
	}

	public class iBMobileLabel:Label {
		public iBMobileLabel() {
		}
	}
	public class iBMobilePlaceHolder:Label {

		public new string Text {
			get {
				return this.ViewState["Text"] as string ?? string.Empty;
			}
			set {
				this.ViewState["Text"] = value;
			}
		}
		public iBMobilePlaceHolder() {
		}
		protected override void Render(HtmlTextWriter pWriter) {
			pWriter.Write(this.Text);
		}
	}
}

