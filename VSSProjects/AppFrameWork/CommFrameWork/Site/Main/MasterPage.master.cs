﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage : System.Web.UI.MasterPage {

	protected void Page_Load(object sender, EventArgs e) {
	}

	protected string GetNodeUrl() {
		string sRoot = Session["Root"].ToString();

		if (Request.FilePath.StartsWith(sRoot+"/Master")) {
			return "~/Master/DefaultMaster.aspx";
		} else if (Request.FilePath.StartsWith(sRoot + "/System")) {
			return "~/System/DefaultSystem.aspx";
		}
		return "";
	}

	protected void lnkLogout_Command(object sender, CommandEventArgs e) {
		string sRoot = ConfigurationManager.AppSettings["Root"];
		string sUrl = "http://" + Request.Url.Authority + sRoot + "/LoginAdmin.aspx";
		Session.RemoveAll();
		Response.Redirect(sUrl);
	}
}
