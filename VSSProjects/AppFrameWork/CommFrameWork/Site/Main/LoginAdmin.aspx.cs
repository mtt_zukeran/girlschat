﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class LoginAdmin : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
		}
	}

	protected void ctlLogin_Authenticate(object sender, AuthenticateEventArgs e) {
		if (Membership.ValidateUser(ctlLogin.UserName, ctlLogin.Password)) {
			Session["PageSize"] = ConfigurationManager.AppSettings["PageSize"];
			Session["Root"] = ConfigurationManager.AppSettings["Root"];
			Session["Login"] = "1";
			FormsAuthentication.RedirectFromLoginPage(ctlLogin.UserName, false);
		}
	}

}
