﻿<%@ Application Language="C#" %>

<script RunAt="server">
	void Application_Start(object sender, EventArgs e) {
	}

	void Application_End(object sender, EventArgs e) {
	}

	void Application_Error(object sender, EventArgs e) {
	}

	void Session_Start(object sender, EventArgs e) {

		string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
		string sUrl = Request.Url.ToString().ToLower();

		if (sUrl.IndexOf(sRoot) > 0) {
			if (sUrl.IndexOf("loginadmin.aspx") < 0) {
				if (Session["Login"] == null) {
					Response.Redirect(sRoot + "/error/WebError.aspx");
				}
			}
		}
	}

	void Session_End(object sender, EventArgs e) {
	}
       
</script>

