/*************************************************************************/
--	System			: ＩＴＢシステム
--  Sub System Name	: 共通
--  Title			: 経過時間取得
--	Progaram ID		: GET_V$TIMER
--	Compile Turn	: 1
--  Creation Date	: 03.01.27
--	Update Date		:
--  Author			: i-Brid(K.Ono)
/*************************************************************************/
CREATE OR REPLACE FUNCTION GET_V$TIMER
	RETURN BINARY_INTEGER
IS

	BUF_SEED					BINARY_INTEGER;

BEGIN

	SELECT HSECS INTO BUF_SEED FROM V$TIMER;

	RETURN	BUF_SEED;

END GET_V$TIMER;
/
SHOW ERROR;

/*************************************************************************/
--	�@：SYSに作成する
--	�A：PUBLICにオブジェクト権限を与える
--			CONN SYS/***********
--			GRANT EXECUTE ON SYS.GET_V$TIMER TO PUBLIC;
/*************************************************************************/
