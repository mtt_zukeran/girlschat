import jp.co.ibrid.DB;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class predummy extends HttpServlet {

	private static String	RET_NAME		= "result=";
	private static String	OK				= "0";
	private static String	NG				= "1";

	private static int		IN_PARAM		= 0;
	private static int		OUT_PARAM		= 1;
	private static int		BOTH_PARAM		= 2;

	private static String	RETURN_NORMAL	= "I000";

	private boolean			isDebug			= false;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) {

		String debugFlag = getServletContext().getInitParameter("DEBUG");
		if (debugFlag.equals("1")) {
			this.isDebug = true;
		}

		String ret = "";
		String reqId = getParam(request, response, "request");
		int intReqId = 0;

		if (this.isDebug) {
			log("QUERY : " + request.getQueryString());
		}

		try {
			intReqId = Integer.parseInt(reqId);
		} catch (NumberFormatException ex) {
			log("BAD REQUEST ID : " + getParam(request, response, "request"));
			output(request, response, RET_NAME + NG);
			return;
		}

		DB db = null;

		try {

			db = new DB();

			switch (intReqId) {

			case 11 :
				ret = GetUserInfo(request, response, db);
				break;

			case 3 :
				ret = UpdateUserInfo(request, response, db);
				break;

			}

		} catch (Exception ex) {
			ex.printStackTrace();
			ret = RET_NAME + "9&" + ex.toString();
		} finally {
			db.release();
		}

		output(request, response, ret);
	}

	/*----------------------------------------------*/
	/* Get User Info								*/
	/*----------------------------------------------*/
	public String GetUserInfo(HttpServletRequest request, HttpServletResponse response, DB db) {

	//	String retCd = getServletContext().getInitParameter("RETCD");

		String ret = RET_NAME + OK;
		String id = getParam(request, response, "userid");


		db.query("SELECT TEL,BAL_POINT,WEB_LOC_CD FROM T_TEST_USER WHERE USER_ID ='" + id + "'");

		if (db.next()) {
			return RET_NAME + OK + "&userid=" + id +  "&phone=" + 
						db.getString("TEL") + "&point=" + db.getString("BAL_POINT") + "&retcd=" + db.getString("WEB_LOC_CD") + "&userpasswd=1234";
		}else{
			return RET_NAME + NG;
		}
	}

	/*----------------------------------------------*/
	/* Update User Info								*/
	/*----------------------------------------------*/
	public String UpdateUserInfo(HttpServletRequest request, HttpServletResponse response, DB db) {


		String ret = RET_NAME + OK;
		String id = getParam(request, response, "userid");
		String point = getParam(request, response, "timepoint");

		db.execute("UPDATE T_TEST_USER SET BAL_POINT = BAL_POINT - " + point +  "  WHERE USER_ID ='" + id + "'");
		return RET_NAME + OK;
	}

	// レスポンス出力
	private void output(HttpServletRequest request, HttpServletResponse response, String ret) {
		response.setContentType("text/html; charset=Shift_JIS");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.print(ret);
			response.flushBuffer();
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private String getParam(HttpServletRequest request, HttpServletResponse response, String name) {
		String str = request.getParameter(name);
		if (str == null) {
			return "";
		} else {
			return str;
		}
	}
}