//---------------------------------------------
// System    : 汎用
// Name      : SP実行例外クラス
// Date      : 2001-10-27
// Programer : TimeLine Co.,LTD.
// Author    : TimeLine Co.,LTD.
// $id:$
//---------------------------------------------
package jp.co.ibrid;

public class SPErrorException extends Exception {

	private String message;

	public SPErrorException() {
		super();
	}

	public SPErrorException(String message){

		super();
		this.message = message;

	}

	public String getError(){
		if(this.message == null) {
			return "";
		}
		else{
			return this.message;
		}
	}
}