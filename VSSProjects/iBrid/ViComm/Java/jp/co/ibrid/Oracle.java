//---------------------------------------------
// Name      : Oracle操作抽象クラス
// Date      : 2001-09-20
// Programer : TimeLine Co.,LTD.
// Author    : TimeLine Co.,LTD.
// $id:$
//---------------------------------------------
package jp.co.ibrid;

import java.util.*;
import java.sql.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;

public abstract class Oracle
{
	public String sid;
	public String user;
	public String pass;
	public String host;
	public String sql = null;
	public ResultSet rs = null;
	public Statement stmt = null;
	public CallableStatement cs = null;
	public Connection con = null;

	public static String TYPE_STRING_ARRAY = null;
	public static String TYPE_NUMBER_ARRAY = null;
	public static String TYPE_DATE_ARRAY = null;

	protected String procName = null;
	protected int procParamIndex = 0;

	public Oracle()
	{
	}

	public void connect()
	{
		try {
			// JDBCドライバの読み込み
			DriverManager.registerDriver(
				new OracleDriver()
			);
			// 接続 (Type4 thin)
			this.con = DriverManager.getConnection(
				"jdbc:oracle:thin:@" + this.host + ":1521:" + this.sid,
				this.user,
				this.pass
			);
			// 接続 (Type2 oci)
			/* this.con = DriverManager.getConnection(
				"jdbc:oracle:oci8:@サービス名",
				ORACLE_ID,
				ORACLE_PW
			); */

			this.con.setAutoCommit(true);
			this.stmt = this.con.createStatement();

		} catch (SQLException e) {
			System.err.println("DB接続に失敗");
			System.err.println(e);
		}

	}

	// デストラクタ
	public void destroy()
	{
		try {
			if (this.rs != null) this.rs.close();
			if (this.stmt != null) this.stmt.close();
			if (this.con != null) this.con.close();
			this.rs = null;
			this.stmt = null;
			this.con = null;
		} catch(SQLException e) {
			System.err.println("DBクローズに失敗:" + e);
		}
	}

	// 参照系SQL発行
	public void query(String sql)
	{
		this.setSql(sql);

		try {
			this.rs = this.stmt.executeQuery(this.sql);
		} catch (SQLException e) {
			this.outputError(e);
		}
	}

	// 更新系SQL発行
	public void execute(String sql)
	{
		this.setSql(sql);

		try {
			this.stmt.executeUpdate(this.sql);
		} catch (SQLException e) {
			this.outputError(e);
			this.rollback();
		}
	}

	public void setSql(String sql)
	{
		this.sql = sql;
	}

	public void outputError(SQLException e)
	{
		System.err.println(
			e.getErrorCode() + ":" +
			e + ":" +
			this.sql
		);
	}

	public String getSql()
	{
		return this.sql;
	}

	public boolean next()
	{
		try {
			return this.rs.next();
		} catch (SQLException e) {
			System.err.println("カーソルの移動に失敗");
			return false;
		}
	}

	public void commit()
	{
		try {
			this.stmt.executeUpdate("commit");
		} catch (SQLException e) {
			System.err.println("COMMITに失敗");
		}
	}

	public void rollback()
	{
		try {
			this.stmt.executeUpdate("rollback");
		} catch (SQLException e) {
			System.err.println("ROLL BACKに失敗");
		}
	}

	public String getString(String columnName)
	{
		try {
			String str = this.rs.getString(columnName);
			if (str == null) {
				return "";
			} else {
				return str;
			}
		} catch (SQLException e) {
			System.err.println(e);
		}

		return "";
	}

	public String getString(int i)
	{
		try {
			String str = this.rs.getString(i);
			if (str == null) {
				return "";
			} else {
				return str;
			}
		} catch (SQLException e) {
			System.err.println(e);
		}

		return "";
	}

	public int getInt(String columnName)
	{
		try {
			return this.rs.getInt(columnName);
		} catch (SQLException e) {
			System.err.println(e);
		}

		return 0;
	}

	public int getInt(int i)
	{
		try {
			return this.rs.getInt(i);
		} catch (SQLException e) {
			System.err.println(e);
		}

		return 0;
	}

	// プロシージャ実行前準備
	// 引数: プロシージャ名, パラメータの数
	public void prepareCall(String procName, int paramCount)
	{
		this.procParamIndex = 0;
		this.procName = procName;

		String sql = new String("begin " + procName + "(");
		Calendar cal = Calendar.getInstance();
		System.err.println("[" + cal.getTime() + "]" + "MAKE PL/SQL CALL START");
		for (int i=0; i<paramCount; i++) {
			if (i != 0) sql += ",";
			sql += "?";
		}
		cal = Calendar.getInstance();
		System.err.println("[" + cal.getTime() + "]" + "MAKE PL/SQL CALL END");
		sql += "); end;";


		try {
			this.cs = this.con.prepareCall(sql);
		} catch (SQLException e) {
			System.err.println(
				this.procName + " ERROR AT PREPARE"
			);
			System.err.println(e);
		}
	}

	// パラメータのセット(数値)
	// 引数: パラメータデータ, 入出力タイプ(0=IN, 1=OUT, 2=IN/OUT)
	// タイプが1の場合は必然的に第1引数はダミーになります
	public void setProcParam(int data, int type)
	{
		this.procParamIndex++;

		try {
			switch (type) {
				case 0:
					this.cs.setInt(this.procParamIndex, data);
					break;
				case 1:
					this.cs.registerOutParameter(this.procParamIndex, Types.INTEGER);
					break;
				case 2:
					this.cs.setInt(this.procParamIndex, data);
					this.cs.registerOutParameter(this.procParamIndex, Types.INTEGER);
					break;
			}
		} catch (SQLException e) {
			System.err.println(
				this.procName + "ERROR SET PARAMETER " + data
			);
			System.err.println(e);
		}
	}

	// パラメータのセット(文字)
	// 引数: パラメータデータ, 入出力タイプ(0=IN, 1=OUT, 2=IN/OUT)
	// タイプが1の場合は必然的に第1引数はダミーになります
	public void setProcParam(String data, int type)
	{
		this.procParamIndex++;

		try {
			switch (type) {
				case 0:
					this.cs.setString(this.procParamIndex, data);
					break;
				case 1:
					this.cs.registerOutParameter(this.procParamIndex, Types.VARCHAR);
					break;
				case 2:
					this.cs.setString(this.procParamIndex, data);
					this.cs.registerOutParameter(this.procParamIndex, Types.VARCHAR);
					break;
			}
		} catch (SQLException e) {
			System.err.println(
				this.procName + "ERROR SET PARAMETER " + data
			);
			System.err.println(e);
		}
	}

	// パラメータのセット(日付)
	// 引数: パラメータデータ, 入出力タイプ(0=IN, 1=OUT, 2=IN/OUT)
	// タイプが1の場合は必然的に第1引数はダミーになります
	public void setProcParam(java.sql.Date data, int type)
	{
		this.procParamIndex++;

		try {
			switch (type) {
				case 0:
					this.cs.setDate(this.procParamIndex, data);
					break;
				case 1:
					this.cs.registerOutParameter(this.procParamIndex, Types.DATE);
					break;
				case 2:
					this.cs.setDate(this.procParamIndex, data);
					this.cs.registerOutParameter(this.procParamIndex, Types.DATE);
					break;
			}
		} catch (SQLException e) {
			System.err.println(
				this.procName + "ERROR SET PARAMETER " + data
			);
			System.err.println(e);
		}
	}

	// 配列オブジェクトの登録
	// 0: varchar 1:number 2:date
	// INのパラメータはダミーオブジェクトです
	public void setProcParamArray(int type)
	{
		this.procParamIndex++;
		ARRAY dummy = null;

		try {
			switch (type) {
				case 0:
					/*
					this.cs.setArray(
						this.procParamIndex,
						new ARRAY(
							ArrayDescriptor.createDescriptor(
								this.TYPE_STRING_ARRAY,
								this.con
							),
							this.con,
							new String[0]
						)
					);
					*/

					this.cs.registerOutParameter(
						this.procParamIndex,
						OracleTypes.ARRAY,
						this.TYPE_STRING_ARRAY
					);
					break;

				case 1:
					/*
					this.cs.setArray(
						this.procParamIndex,
						new ARRAY(
							ArrayDescriptor.createDescriptor(
								this.TYPE_NUMBER_ARRAY,
								this.con
							),
							this.con,
							new int[0]
						)
					);
					*/

					this.cs.registerOutParameter(
						this.procParamIndex,
						OracleTypes.ARRAY,
						this.TYPE_NUMBER_ARRAY
					);
					break;

				case 2:
					/*
					this.cs.setArray(
						this.procParamIndex,
						new ARRAY(
							ArrayDescriptor.createDescriptor(
								this.TYPE_DATE_ARRAY,
								this.con
							),
							this.con,
							new java.sql.Date[0]
						)
					);
					*/

					this.cs.registerOutParameter(
						this.procParamIndex,
						OracleTypes.ARRAY,
						this.TYPE_DATE_ARRAY
					);
					break;
			}

		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	public void executeProcedure() throws SPErrorException
	{
		try {
			this.cs.execute();
		} catch (SQLException e) {
			System.err.println(
				this.procName + " ERROR AT EXECUTING"
			);
			System.err.println(e);
			throw new SPErrorException((String)e.getMessage());
		}
	}

	public String getStringProc(int i)
	{
		try {
			String ret = this.cs.getString(i);

			if (ret != null) {
				return ret;
			} else {
				return "";
			}
		} catch (SQLException e) {
			System.err.println(
				this.procName + " ERROR GETTING PARAMETER " + i
			);
			System.err.println(e);

			return "";
		}
	}

	public String getIntProc(int i)
	{
		try {
			return String.valueOf(this.cs.getInt(i));
		} catch (SQLException e) {
			System.err.println(
				this.procName + " ERROR GETTING PARAMETER " + i
			);
			System.err.println(e);

			return "";
		} catch(Exception e) {
			return "";
		}
	}

	public String getDateProc(int i)
	{
		try {
			java.sql.Date d = this.cs.getDate(i);

			if (d != null) {
				return d.toString();
			} else {
				return "";
			}
		} catch (SQLException e) {
			System.err.println(
				this.procName + " ERROR GETTING PARAMETER " + i
			);
			System.err.println(e);

			return "";
		}
	}

	// プロシージャOUT配列オブジェクトの取り出し
	public String[] getStringArrayProc(int i)
	{
		String[] arr = null;

		try {
			ARRAY obj = (ARRAY)(((OracleCallableStatement)this.cs).getOracleObject(i));
			arr = new String[ obj.length() ];

			ResultSet rset = obj.getResultSet();
			for (int j=0; rset.next(); j++) {
				arr[j] = (String)rset.getObject(2);
			}
		} catch (SQLException e) {
			System.err.println(e);
		}

		return arr;
	}

	// プロシージャOUT配列オブジェクトの取り出し
	public String[] getIntArrayProc(int i)
	{
		String[] arr = null;

		try {
			ARRAY obj = (ARRAY)(((OracleCallableStatement)this.cs).getOracleObject(i));
			arr = new String[ obj.length() ];

			ResultSet rset = obj.getResultSet();
			for (int j=0; rset.next(); j++) {
				if (rset.getObject(2) != null) {
					arr[j] = rset.getObject(2).toString();
				} else {
					arr[j] = "";
				}
			}
		} catch (SQLException e) {
			System.err.println(e);
		}

		return arr;
	}

	// プロシージャOUT配列オブジェクトの取り出し
	public String[] getDateArrayProc(int i)
	{
		String[] arr = null;

		try {
			ARRAY obj = (ARRAY)(((OracleCallableStatement)this.cs).getOracleObject(i));
			arr = new String[ obj.length() ];

			ResultSet rset = obj.getResultSet();
			for (int j=0; rset.next(); j++) {
				arr[j] = rset.getObject(2).toString();
			}

		} catch (SQLException e) {
			System.err.println(e);
		}

		return arr;
	}

	// プロシージャーカーソルのクローズ
	public void finishProc()
	{

		try {
			this.cs.clearParameters();
			this.cs.close();
		} catch(Exception e) {
			System.err.println(e);
		} finally {
			this.cs = null;
		}
	}
}
