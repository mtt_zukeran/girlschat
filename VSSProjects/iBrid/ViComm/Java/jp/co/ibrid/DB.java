//---------------------------------------------
// System    : I-Brid
// Name      : データベース操作抽象クラス
// Date      : 2001-09-20
// Programer : iBrid Co.,LTD.
// Author    : I-Brid
// $id:$
//---------------------------------------------
package jp.co.ibrid;

import java.sql.*;
import javax.sql.*;

public class DB extends Oracle
{
	private static char dlmt = '|';

	// コンストラクタ
	public DB(){
		this.doCommon();
	}

	public void doCommon(){
		this.TYPE_STRING_ARRAY = "TB_VARCHAR2";
		this.TYPE_NUMBER_ARRAY = "TB_NUMBER";
		this.TYPE_DATE_ARRAY = "TB_DATE";
	}

	public void setConnection(){
		if (this.con != null) {
			return;
		}
		// コネクションの取得
		this.connect();
	}

	// プロシージャ実行前準備
	// 引数: プロシージャ名, パラメータの数
	public void prepareCall(String procName, int paramCount){
		
		this.setConnection();
		this.procParamIndex = 0;
		this.procName = procName;
		
		String sql = new String("begin " + procName + "(");
		for (int i=0; i<paramCount; i++) {
			if (i != 0) sql += ",";
			sql += "?";
		}
		sql += "); end;";

		try {
			this.cs = this.con.prepareCall(sql);
		} catch (SQLException e) {
			System.err.println(
				this.procName + " ERROR AT PREPARE"
			);
			System.err.println(e);
		}
	}

	public void executeProcedure() throws SPErrorException	{

		String status = "";

		try {
			this.cs.execute();
			status = this.getStringProc(this.procParamIndex);
			if (!status.equals("I000")) {
				System.err.println(
					this.procName + ":" + status
				);
				this.finishProc();
				throw new SPErrorException(this.procName + ":" + status);
			}
		} catch (SQLException e) {
			System.err.println(
				this.procName + " ERROR AT EXECUTING"
			);
			System.err.println(e);
			throw new SPErrorException(this.procName + ":" + status + ":" + e.getMessage());

		}
	}

	// 参照系SQL発行
	public void query(String sql){
		this.setConnection();
		this.setSql(sql);

		try {
			this.rs = this.stmt.executeQuery(this.sql);
		} catch (SQLException e) {
			e.printStackTrace();
			//this.outputError(e);
			//System.out.println(e.getMessage());
		}
	}

	// 更新系SQL発行
	public void execute(String sql){
		this.setConnection();
		this.setSql(sql);

		try {
			this.stmt.executeUpdate(this.sql);
		} catch (SQLException e) {
			//this.outputError(e);
			e.printStackTrace();
			this.rollback();
		}

	}

	public void connect()
	{
		try{
			DataSource ds = com.codestudio.sql.PoolMan.findDataSource("jndi-oracle");
			this.con = ds.getConnection();
			this.con.setAutoCommit(true);
			this.stmt = this.con.createStatement();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void release(){
		try {
			if (this.rs != null) this.rs.close();
			if (this.stmt != null) this.stmt.close();
			if (this.con != null) {
				this.con.close();
				this.con = null;
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

	// ---------------------------------------------------
	// ここから配列パラメータをパイプ区切りに変更したもの
	// ---------------------------------------------------
	// OUTパラメータの登録
	// 0: varchar 1:number 3:date
	public void setProcParamArray(int type){
		this.procParamIndex++;

		try {
			// 全てVARCHAR
			this.cs.registerOutParameter(this.procParamIndex, Types.VARCHAR);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private String[] getSplit(String str){
		// DEBUG
		//System.err.println("RET ARRAY STRING : " + str);

		if (str == null || str.equals("")){
			String[] emptyStringArray = new String[1];
			emptyStringArray[0] = "";
			return emptyStringArray;
		}

		// 区切り文字のカウント
		int dlmtCount = 0;
		int s = 0;
		int e;
		while((e = str.indexOf(this.dlmt, s)) != -1) {
			s = e + 1;
			dlmtCount++;
		}

		String[] arr = new String[dlmtCount + 1];

		s = 0;
		e = 0;
		int i = 0;
		int svEnd = 0;
		while((e = str.indexOf(this.dlmt, s)) != -1) {
			try {
				if (s == e) {
					arr[i] = "";
				} else {
					arr[i] = str.substring(s, e);
				}
			} catch (IndexOutOfBoundsException ex) {
				arr[i] = "";
			}
			s = e + 1;
			i++;
			svEnd = e + 1;
		}
		arr[i] = str.substring(svEnd, str.length());

		/*
		System.err.println("----< ARRAY LENGTH : " + arr.length + " >-----------");
		for (int j=0;j<arr.length;j++) {
			System.err.println(arr[j]);
		}
		System.err.println("----------------------------------------------------");
		*/

		return arr;
	}

	// VARCHARパイプ区切り配列の取得
	public String[] getStringArrayProc(int i)
	{
		String[] arr = null;

		try {
			arr = this.getSplit(this.cs.getString(i));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return arr;
	}

	// INTEGERパイプ区切り配列の取得
	public String[] getIntArrayProc(int i)
	{
		String[] arr = null;

		try {
			arr = this.getSplit(this.cs.getString(i));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return arr;
	}

	// DATEパイプ区切り配列の取得
	public String[] getDateArrayProc(int i)
	{
		String[] arr = null;

		try {
			arr = this.getSplit(this.cs.getString(i));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return arr;
	}

}
