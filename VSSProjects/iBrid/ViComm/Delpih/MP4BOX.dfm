object Form1: TForm1
  Left = 563
  Top = 431
  Width = 783
  Height = 443
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 636
    Top = 8
    Width = 128
    Height = 12
    Caption = 'output'#12399'E:\SEARCH.TXT'
  end
  object Label2: TLabel
    Left = 510
    Top = 6
    Width = 39
    Height = 12
    Caption = 'KB'#20197#19978
  end
  object Label3: TLabel
    Left = 538
    Top = 30
    Width = 111
    Height = 12
    Caption = 'Dir'#12398#26368#24460#12398#65509#12399#19981#29992
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 6
    Top = 50
    Width = 26
    Height = 12
    Caption = 'SEQ '
  end
  object Button1: TButton
    Left = 556
    Top = 2
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object edtDir: TEdit
    Left = 0
    Top = 2
    Width = 403
    Height = 20
    ImeMode = imClose
    TabOrder = 1
    Text = 'edtDir'
  end
  object Memo1: TMemo
    Left = 0
    Top = 70
    Width = 775
    Height = 346
    Align = alBottom
    Lines.Strings = (
      '')
    TabOrder = 2
  end
  object edtExt: TEdit
    Left = 408
    Top = 2
    Width = 55
    Height = 20
    ImeMode = imClose
    TabOrder = 3
    Text = '3gp'
  end
  object edtKB: TEdit
    Left = 466
    Top = 2
    Width = 39
    Height = 20
    ImeMode = imClose
    TabOrder = 4
    Text = '0'
  end
  object rdoMMP4: TRadioButton
    Left = 2
    Top = 24
    Width = 87
    Height = 17
    Caption = 'add mmp4'
    Checked = True
    TabOrder = 5
    TabStop = True
  end
  object rdoSoftBank: TRadioButton
    Left = 96
    Top = 24
    Width = 121
    Height = 17
    Caption = 'Create SB Movie'
    TabOrder = 6
  end
  object rdoPic: TRadioButton
    Left = 226
    Top = 24
    Width = 106
    Height = 17
    Caption = 'Create All Pics'
    TabOrder = 7
  end
  object rdoTensai: TRadioButton
    Left = 336
    Top = 24
    Width = 78
    Height = 17
    Caption = 'CopyRight'
    TabOrder = 8
  end
  object rdoMovieConvert: TRadioButton
    Left = 420
    Top = 24
    Width = 93
    Height = 17
    Caption = 'MovieConvert'
    TabOrder = 9
  end
  object edtFromNo: TEdit
    Left = 36
    Top = 44
    Width = 87
    Height = 20
    ImeMode = imClose
    TabOrder = 10
  end
  object edtToNo: TEdit
    Left = 126
    Top = 44
    Width = 87
    Height = 20
    ImeMode = imClose
    TabOrder = 11
  end
end
