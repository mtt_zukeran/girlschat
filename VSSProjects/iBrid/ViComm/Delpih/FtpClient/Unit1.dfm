object Form1: TForm1
  Left = 431
  Top = 276
  Width = 382
  Height = 352
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  ActiveControl = Button1
  BorderIcons = [biSystemMenu]
  Caption = 'Indy FTP-'#12501#12449#12452#12523#12480#12454#12531#12525#12540#12489'_TIdAntiFreeze'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    374
    325)
  PixelsPerInch = 96
  TextHeight = 12
  object Button1: TButton
    Left = 65
    Top = 127
    Width = 75
    Height = 25
    Caption = #12480#12454#12531#12525#12540#12489
    TabOrder = 0
    OnClick = Button1Click
  end
  object LabeledEdit1: TLabeledEdit
    Left = 82
    Top = 10
    Width = 279
    Height = 20
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 60
    EditLabel.Height = 12
    EditLabel.Caption = #12469#12540#12496#12540#21517
    LabelPosition = lpLeft
    TabOrder = 2
    Text = 'telmin.jp'
  end
  object LabeledEdit2: TLabeledEdit
    Left = 82
    Top = 33
    Width = 279
    Height = 20
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 50
    EditLabel.Height = 12
    EditLabel.Caption = #12450#12459#12454#12531#12488
    LabelPosition = lpLeft
    TabOrder = 3
    Text = 'ftpuserdata'
  end
  object LabeledEdit3: TLabeledEdit
    Left = 82
    Top = 55
    Width = 279
    Height = 20
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 54
    EditLabel.Height = 12
    EditLabel.Caption = #12497#12473#12527#12540#12489
    LabelPosition = lpLeft
    TabOrder = 4
    Text = 'gazou'
  end
  object LabeledEdit4: TLabeledEdit
    Left = 82
    Top = 78
    Width = 279
    Height = 20
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 63
    EditLabel.Height = 12
    EditLabel.Caption = 'GET'#12501#12449#12452#12523
    LabelPosition = lpLeft
    TabOrder = 5
  end
  object ProgressBar1: TProgressBar
    Left = 14
    Top = 160
    Width = 350
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 6
  end
  object Button2: TButton
    Left = 144
    Top = 127
    Width = 75
    Height = 25
    Caption = #12461#12515#12531#12475#12523
    TabOrder = 1
    OnClick = Button2Click
  end
  object chkPassive: TCheckBox
    Left = 18
    Top = 102
    Width = 77
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Passive'
    TabOrder = 7
  end
  object Memo1: TMemo
    Left = 0
    Top = 190
    Width = 374
    Height = 135
    Align = alBottom
    ImeMode = imClose
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    TabOrder = 8
  end
  object IdFTP1: TIdFTP
    MaxLineAction = maException
    ReadTimeout = 0
    OnWork = IdFTP1Work
    OnWorkBegin = IdFTP1WorkBegin
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 144
    Top = 40
  end
  object IdAntiFreeze1: TIdAntiFreeze
    Left = 175
    Top = 40
  end
end
