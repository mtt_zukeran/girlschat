{$WARNINGS OFF}
unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdFTP, IdAntiFreezeBase, IdAntiFreeze,
  ComCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    LabeledEdit1 : TLabeledEdit;
    LabeledEdit2 : TLabeledEdit;
    LabeledEdit3 : TLabeledEdit;
    LabeledEdit4 : TLabeledEdit;
    IdFTP1: TIdFTP;
    ProgressBar1: TProgressBar;
    Button2: TButton;
    IdAntiFreeze1: TIdAntiFreeze;
    chkPassive: TCheckBox;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IdFTP1Work(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure IdFTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
	  const AWorkCountMax: Integer);
    procedure Button2Click(Sender: TObject);
  private
    { Private 宣言 }
    IniFileName : String;
    SourceFile  : String;
    SaveFile    : String;
    FileSize    : Integer;
  public
    { Public 宣言 }
  end;

var
  Form1: TForm1;

implementation

uses HTTPApp,IniFiles;

{$R *.dfm}

//=============================================================================
//  EditにFTP関係情報をセット
//  1つ上の階層からFTPInfo.iniを読み出してセット
//
//  LabelEdit4.textはダウンロードするサーバ上のフルパスファイル名
//  ex1. mrxray.on.coocan.jp/homepage/LzhFiles/plSetPrinter.lzh
//  ex2. /homepage/LzhFiles/plSetPrinter.lzh
//=============================================================================
procedure TForm1.FormShow(Sender: TObject);
var
    AIni : TInifile;
begin
	IniFileName := 'FTPInfo.ini';
    AIni := TIniFile.Create(IniFileName);
    try
     if FileExists(IniFileName) then begin
       LabeledEdit1.Text := AIni.ReadString('FTPInfo', 'ServerName','');
       LabeledEdit2.Text := AIni.ReadString('FTPInfo', 'UserID','');
       LabeledEdit3.Text := AIni.ReadString('FTPInfo', 'PassWord','');
       LabeledEdit4.Text := AIni.ReadString('Others',  'GetFileName','');
     end;
    finally
      FreeAndNil(AIni);
    end;
end;
//=============================================================================
//  設定をIniファイルに保存
//=============================================================================
procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var
    AIni : TInifile;
begin
    AIni := TIniFile.Create(IniFileName);
    try
      try
        AIni.WriteString('FTPInfo','ServerName', LabeledEdit1.Text);
        AIni.WriteString('FTPInfo','UserID',     LabeledEdit2.Text);
        AIni.WriteString('FTPInfo','PassWord',   LabeledEdit3.Text);
        AIni.WriteString('Others', 'GetFileName',LabeledEdit4.Text);
      except
      end;
    finally
      FreeAndNil(AIni);
    end;

    //ダウンロード中にフォームを閉じてしまった場合に必要
    if IdFTP1.Connected then begin
      IdFTP1.Abort;
      IdFTP1.Disconnect;
    end;
end;
//=============================================================================
//  FTPでファイルをダウンロードする
//  UnixPathToDosPath関数にはusesにHTTPAppが必要
//=============================================================================
procedure TForm1.Button1Click(Sender: TObject);
var
    FStream : TFileStream;
	F: TextFile;
	buf	:String;
	sDir:String;
	iCnt:Integer;
	bError:Boolean;
	iRetry:Integer;

begin
    Screen.Cursor := crHourGlass;

	IdFTP1.Host     := LabeledEdit1.Text;
	IdFTP1.Username := LabeledEdit2.Text;
	IdFTP1.Password := LabeledEdit3.Text;
	IdFTP1.Passive	:= chkPassive.Checked;

	try
	  IdFTP1.Connect;
	except
	  Screen.Cursor := crDefault;
	  IdFTP1.Quit;
	  IdFTP1.Disconnect;
	  MessageDlg('サーバの接続に失敗しました．1',mtInformation,[mbOK],0);
	  exit;
	end;

	AssignFile(F,LabeledEdit4.Text);
	ProgressBar1.Position := 0;

	try
		Reset(F);
		iCnt := 0;
		while not EOF(F) do begin
			Readln(F,buf);
			iCnt := iCnt + 1;
		end;
	finally
		CloseFile(F);
	end;
	ProgressBar1.Max := iCnt;

	Memo1.Lines.Clear;

	try
		Reset(F);
		iCnt := 0;
		while not EOF(F) do begin
			Readln(F,buf);
			iCnt := iCnt + 1;
			ProgressBar1.Position := iCnt;

			//保存ファイル名はこのプロジェクトのフォルダでサーバ上と同名に
			//SourceFileはサーバ上のファイル名
			SourceFile := buf;
			sDir	   := ExtractFilePath(ParamStr(0))+ ExtractFilePath(UnixPathToDosPath(SourceFile));
			SaveFile   := sDir + ExtractFileName(UnixPathToDosPath(SourceFile));
			if(DirectoryExists(sDir) = false)then begin
				ForceDirectories(sDir);
			end;

//			FileSize := IdFTP1.Size(SourceFile);

//			if FileSize>0 then begin
			iRetry := 0;
			repeat
				bError := False;
				FStream := TFileStream.Create(SaveFile,fmCreate);
				try
					try
						iRetry := iRetry + 1;
						IdFTP1.Get(SourceFile,FStream);
							Memo1.Lines.Add('OK ' + SaveFile);
					Except
						On FtpErr : Exception Do Begin
							Memo1.Lines.Add(FtpErr.Message + ' ' + SaveFile);
							bError := True;
							Sleep(100);
							try
							  IdFTP1.Disconnect;
							  IdFTP1.Connect;
							except
							  Screen.Cursor := crDefault;
							  IdFTP1.Quit;
							  IdFTP1.Disconnect;
							  MessageDlg('サーバの接続に失敗しました．2',mtInformation,[mbOK],0);
							  exit;
							end;
						end;
					end;
				finally
					FreeAndNil(FStream);
				end;
			until(bError = false) or (iRetry > 10);
//			end else begin
//			end;
		end;
	finally
		CloseFile(F);
	end;

	Screen.Cursor := crDefault;
	IdFTP1.Disconnect;
	MessageDlg('Process Complite',mtInformation,[mbOK],0);
end;
//=============================================================================
//  キャンセルボタン
//  ダウンロード途中のファイルは削除した方がいいかも知れない
//=============================================================================
procedure TForm1.Button2Click(Sender: TObject);
begin
	if IdFTP1.Connected then begin
	  IdFTP1.Abort;
	  IdFTP1.Disconnect;
    end;
end;
//=============================================================================
//  IdHTTPのOnWorkBeginイベント
//  プログレスバーの最大値をセット
//=============================================================================
procedure TForm1.IdFTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
end;
//=============================================================================
//  IdFTPのOnWorkイベント
//  ダウンロードの進捗(進行)状況をプログレスバーに表示
//=============================================================================
procedure TForm1.IdFTP1Work(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
    //Abortメソッドで処理の中断を可能にする
    IdAntiFreeze1.Process;
	ProgressBar1.Position := AWorkCount;
end;

end.
