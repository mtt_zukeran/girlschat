unit MP4BOX;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
	Button1: TButton;
	edtDir: TEdit;
	Memo1: TMemo;
	edtExt: TEdit;
	Label1: TLabel;
	edtKB: TEdit;
	Label2: TLabel;
	rdoMMP4: TRadioButton;
	rdoSoftBank: TRadioButton;
	Label3: TLabel;
	rdoPic: TRadioButton;
	rdoTensai: TRadioButton;
	rdoMovieConvert: TRadioButton;
    edtFromNo: TEdit;
    edtToNo: TEdit;
    Label4: TLabel;
	procedure Button1Click(Sender: TObject);
  private
	procedure FileListCreator(TopDir: TFileName);
	function Replace(Str, Before, After: string): string;
  public
	{ Public �錾 }
 	TxtOutFile		: TextFile;
 end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
	Memo1.Clear;

	AssignFile(TxtOutFile, 'E:\Search.txt');
		Rewrite(TxtOutFile);

	FileListCreator(edtDir.Text);
	CloseFile(TxtOutFile);
end;


procedure TForm1.FileListCreator(TopDir: TFileName);
var
	Handle	: THandle;
	Data	: TWin32FindData;
	sFileNm	:String;
	sPicDir	:String;
	sExt	:String;
	dSize	:DWORD;
	iFileSeq:Integer;
	iFromSeq:Integer;
	iToSeq:Integer;
begin
	Handle := Windows.FindFirstFile(PChar(TopDir + '\*.*'), Data);
	sExt := UpperCase(edtExt.Text);
	dSize:= StrToInt(edtKB.Text) * 1000;

	iFromSeq := -1;
	iToSeq := -1;

	if (edtFromNo.Text <> '') then begin
		iFromSeq := StrToInt(edtFromNo.Text);
	end;

	if (edtToNo.Text <> '') then begin
		iToSeq := StrToInt(edtToNo.Text);
	end;


	if Handle = INVALID_HANDLE_VALUE then Exit;

	if not ((StrPas(Data.cFileName) = '.') or (StrPas(Data.cFileName) = '..')) then begin
		if (Data.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) <> 0 then begin
			FileListCreator(TopDir + '\' + StrPas(Data.cFileName));
		end;
	end;

	while Windows.FindNextFile(Handle, Data) = True do begin
		if not ((StrPas(Data.cFileName) = '.') or (StrPas(Data.cFileName) = '..')) then begin
			if (Data.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) <> 0 then begin
				FileListCreator(TopDir + '\' + StrPas(Data.cFileName));
			end;
		end;

		sFileNm := StrPas(Data.cFileName);
		if (UpperCase(Copy(sFileNm,Length(sFileNm) - 2,3)) = sExt) then begin

			if (Data.nFileSizeLow > dSize) then begin
				if (rdoMMP4.Checked) then begin
					Memo1.Lines.Add(TopDir + '\' + sFileNm);
					Writeln(TxtOutFile,'MP4BOX -add '+ TopDir + '\' + sFileNm + ' -brand mmp4:1 -new ' +  TopDir + '\' + sFileNm );
				end;

				if (rdoSoftBank.Checked) then begin
					Writeln(TxtOutFile,'MP4BOX -split-size 300 '+ TopDir + '\' + sFileNm );
					Writeln(TxtOutFile,'COPY '+ TopDir + '\' + Copy(sFileNm,1,Length(sFileNm)-4) +'_001.3gp ' +  TopDir + '\' + Copy(sFileNm,1,Length(sFileNm)-4) +'s.3gp');
					Writeln(TxtOutFile,'DEL /Q '+ TopDir + '\' +  Copy(sFileNm,1,Length(sFileNm)-4) +'_*');
					Memo1.Lines.Add(TopDir + '\' + sFileNm + '   ' + IntToStr(Data.nFileSizeLow));
				end;

				if (rdoPic.Checked) and (Length(sFileNm) = 19) then begin
					Memo1.Lines.Add(TopDir + '\' + sFileNm);
					Writeln(TxtOutFile,'call E:\i-Brid\MailTools\iBrid\ConvertImage.bat A001 '+ TopDir + '\ ' + TopDir + ' ' + Copy(sFileNm,1,Length(sFileNm)-4) + ' '  +  sFileNm );
				end;

				if (rdoTensai.Checked) and (Length(sFileNm) = 19) then begin
					Memo1.Lines.Add(TopDir + '\' + sFileNm);
					Writeln(TxtOutFile,'call E:\CopyRight\CopyRight.bat '+ TopDir + '\' +  sFileNm );
				end;

				if (rdoMovieConvert.Checked) and (Length(sFileNm) = 19) then begin
					sPicDir := Replace(TopDir,'movie','data');
					if (iFromSeq <> -1) and (iToSeq <> -1) then begin
						iFileSeq := StrToInt(ChangeFileExt(sFileNm, ''));
						if ( iFileSeq >= iFromSeq) and (iFileSeq <= iToSeq) then begin
							Memo1.Lines.Add(TopDir + '\' + sFileNm);
							Writeln(TxtOutFile,'call E:\ConvertMovie\Convert.bat '+ TopDir + ' ' +  sFileNm + ' '+ TopDir + ' ' + Copy(sFileNm,1,Length(sFileNm)-4) + ' 0 0 ' + sPicDir);
						end;
					end;
				end;
			end;
		end;
	end;
	Windows.FindClose(Handle);
end;

function TForm1.Replace(Str, Before, After: string): string;
var
  Len, N: Integer;
begin
	Result := '';
  Len := Length(Before) - 1;
  N := AnsiPos(Before, Str);
  while N > 0 do begin
    Result := Result + Copy(Str, 1, N - 1) + After;
    Delete(Str, 1, N + Len);
    N := AnsiPos(Before, Str)
  end;
  Result := Result + Str
end;


end.
