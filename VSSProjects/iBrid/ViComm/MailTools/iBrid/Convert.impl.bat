@echo off

REM /*************************************************************************
REM --	System			: ViComm
REM --	Sub System Name	: 
REM --	Title			: 動画変換バッチ
REM --	Progaram ID		: Convert.impl.bat
REM --
REM --  Creation Date	: 
REM --  Creater			: i-Brid
REM --
REM -- [Arguments]
REM -- %~1:入力ディレクトリ(MailtToolディレクトリ)
REM -- %~2:入力ファイル名
REM -- %~3:出力ディレクトリ(キャストのMovieディレクトリ)
REM -- %~4:ファイル番号
REM -- %~5:アスペクト比		0:画像サイズに合わせる、1:normal(4:3)、2:wide(16:9)
REM -- %~6:販売動画フラグ	（1：動画を分割作成する、未指定or1以外の値：QCIFで3gp,3g2,3gp(小)の作成のみ）
REM -- %~7:サムネイル画像出力パス
REM -- %~8:サイトコード
REM **************************************************************************/
REM [ Update History ]
REM /*------------------------------------------------------------------------
REM 
REM   Date        Updater    Update Explain
REM   2010/06/02  伊藤和明   各キャリアにあわせた動画サイズ分割の機能を追加
REM                          マルチスレッド対応
REM   2010/08/27  伊藤和明   画像のサイズ比を指定できるように変更
REM   2010/11/08  伊藤和明   TVCC→ffmpegへ変更
REM 
REM -------------------------------------------------------------------------*/

set T=%TIME:/=%                                                                 

echo p1=%~1
echo p2=%~2
echo p3=%~3
echo p4=%~4
echo p5=%~5
echo p6=%~6
echo p7=%~7
echo p8=%~8

set current_dir=%~d0%~p0
set work_dir=%~1
set input_file_name=%~2
set output_path=%~3
set obj_seq=%~4
set aspect_type=%~5
set with_split_flag=%~6
set output_thumbnail_path=%~7
set site_cd=%~8
set has_error=0


set error_dir=%work_dir%error_log
set mp4box_dir="D:\i-Brid\MP4BOX"
set ffmpeg_dir=%work_dir%\lib\ffmpeg
set timer_executor=%work_dir%\lib\timer_executor\timer_executor.exe
set temp_file_prefix=temp_%obj_seq%_
set result_file=%~d0%~p0%obj_seq%.result
set ffmpeg_timeout=90000

call %CURRENT_DIR%_env.bat

if "%MOVIE_SAVE_ORIGINAL%"=="" (
	SET MOVIE_SAVE_ORIGINAL=1
)
if "%MOVIE_HIGH_QUALITY%"=="" (
	SET MOVIE_HIGH_QUALITY=0
)

rem -- UNCの場合はアクセスできるように認証を済ませておく
REM call :NET_USE %output_path% vicomm vicomm
REM 共有ディスクにアクセスできるようにする
call %current_dir%_netuse.bat "%output_path%" "vicomm" "vicomm"

if not exist "%output_path%" (
	mkdir "%output_path%"
)


rem -- 元となる動画の保存
if "%MOVIE_SAVE_ORIGINAL%"=="1" (
	copy /Y %work_dir%\%input_file_name% %work_dir%\original\%input_file_name%
)

rem 動画ファイル形式のチェック
"%ffmpeg_dir%\ffmpeg.exe" -i %work_dir%\%input_file_name% > %work_dir%\%input_file_name%.info
find /C "%work_dir%\%input_file_name%.info" "ffmpeg file decoder"
if "%ERRORLEVEL%"=="0" (
	ECHO ファイル形式が不正です。
	EXIT;
)

rem qvgaの作成有無決定
set mov_type_array=qcif
if "%with_split_flag%" == "1" (
	set mov_type_array=qcif qvga
)

for %%f in (%mov_type_array%)do (
	call :CONVERT "%work_dir%\%input_file_name%" "%work_dir%\%temp_file_prefix%%%f.3gp" %%f "libopencore_amrnb" 3gp
	call :CONVERT "%work_dir%\%input_file_name%" "%work_dir%\%temp_file_prefix%%%f.3g2" %%f "libopencore_amrnb" 3g2
)
call :CONVERT_MP4 "%work_dir%\%input_file_name%" "%work_dir%\%temp_file_prefix%.mp4"

rem サムネイル画像作成
call :GENERATE_THUMBNAIL "%work_dir%\%temp_file_prefix%qcif.3gp" "%work_dir%\%~n2.jpg"

rem 
rem -- 動画を指定サイズに分割
rem for docomo
call :SPLIT_FILE 1990 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_d_s.3gp 12
if "%with_split_flag%" == "1" (
    call :SPLIT_FILE 490 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_d_d.3gp 11
    call :SPLIT_FILE 490 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_d_d.3gp 13
    call :SPLIT_FILE 1990 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_d_s.3gp 14
	
	call :SPLIT_FILE 9990 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_d_s2.3gp 15
	call :SPLIT_FILE 9990 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_d_s2.3gp 16	
)
rem for softbank
call :SPLIT_FILE 290 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_s_d.3gp 21
if "%with_split_flag%" == "1" (
    call :SPLIT_FILE 290 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_s_s.3gp 22
    call :SPLIT_FILE 290 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_s_s.3gp 24
    call :SPLIT_FILE 290 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_s_d.3gp 23
)
rem for au
call :SPLIT_FILE 1490 %work_dir%\%temp_file_prefix%qcif.3g2 %work_dir%\%temp_file_prefix%qcif_a_d.3g2 31
if "%with_split_flag%" == "1" (
    call :SPLIT_FILE 99 %work_dir%\%temp_file_prefix%qcif.3g2 %work_dir%\%temp_file_prefix%qcif_a_s.3g2 32
    call :SPLIT_FILE 99 %work_dir%\%temp_file_prefix%qvga.3g2 %work_dir%\%temp_file_prefix%qvga_a_s.3g2 34
    call :SPLIT_FILE 1490 %work_dir%\%temp_file_prefix%qvga.3g2 %work_dir%\%temp_file_prefix%qvga_a_d.3g2 33
)

rem -- docomoのストリーミング用にATOM構造を組み替える(MP4BOX)
call :MP4BOX_FOR_DOCOMO  %work_dir%\%temp_file_prefix%qcif_d_s*.3gp
rem call :MP4BOX_FOR_DOCOMO  %work_dir%\%temp_file_prefix%qvga_d_s*.3gp

rem -- for AU (atom change) 再生できるように動画ﾌｧｲﾙを偽装する
call :ATOM_CHANGE %work_dir%\%temp_file_prefix%*.3g2 

rem -- publish(旧)　
copy /Y %work_dir%\%temp_file_prefix%qcif_d_s_001.3gp %output_path%\%obj_seq%.3gp
copy /Y %work_dir%\%temp_file_prefix%qcif_a_d_001.3g2 %output_path%\%obj_seq%.3g2
copy /Y %work_dir%\%temp_file_prefix%qcif_s_d_001.3gp %output_path%\%obj_seq%s.3gp

copy /Y %work_dir%\%temp_file_prefix%.mp4 %output_path%\%obj_seq%.mp4

rem -- publish(新)
rem for %%f in (%work_dir%\%temp_file_prefix%????_*) do (
rem   call :PUBLISH "%%f" 
rem )

if exist "%work_dir%\%~n2.jpg" (
	if "%site_cd%"=="" (
	 	call %work_dir%\CopyRight.bat "%work_dir%" "%~n2.jpg"
	 	move "%work_dir%\%~n2.jpg" "%output_thumbnail_path%\%~n2.jpg"
	) else (
	 	if "%DISABLE_THUMBNAIL_DTL%"=="" ( 
	 		call %work_dir%\ConvertImage.bat "%site_cd%" "%work_dir%\" "%output_thumbnail_path%" "%~n2"
	 	) else (
	 		call %work_dir%\CopyRight.bat "%work_dir%" "%~n2.jpg"
	 		move "%work_dir%\%~n2.jpg" "%output_thumbnail_path%\%~n2.jpg"		
	 	)
	)	
)
rem -- ★ﾐﾗｰﾘﾝｸﾞ
call %work_dir%\Mirror.bat "%output_path%\%obj_seq%*"
call %work_dir%\Mirror.bat "%output_thumbnail_path%\%obj_seq%*"


rem -- 処理後ファイルの削除
del /Q %work_dir%\%input_file_name%*	
del /Q %work_dir%\%temp_file_prefix%*

set T1=%TIME:/=%                                                                                                                                               
echo 開始時間：%T%                                                              
echo 終了時間：%T1%                                                             
                                                                                 
goto END:



REM ********************************************************
REM -- :GENERATE_THUMBNAIL
REM -- サムネイル画像を作成する
REM ********************************************************
:GENERATE_THUMBNAIL
if not "%has_error%"=="0" exit /B %has_error%

set input_movie=%~1
set output_thumbnail=%~2

for /L %%i IN (5,-1,0) DO (
	"%ffmpeg_dir%\ffmpeg.exe" -y -itsoffset -%%i  -i "%input_movie%" -vcodec mjpeg -vframes 1 -an -f rawvideo -s 176x144 "%output_thumbnail%" 	
	
	if exist "%output_thumbnail%" (
		for %%s in ( "%output_thumbnail%" ) DO (
			if NOT "%%~zs"=="0" (
				exit /B 0
			)
		)
	)
)

exit /B 0


REM ********************************************************
REM -- :CONVERT
REM -- 動画ファイルを変換する
REM ********************************************************
:CONVERT
if not "%has_error%"=="0" exit /B %has_error%

set width=176
set height=144
set pad_y=0
set bitrate=64k
set aspect=11:9
set acodec=%~4

if "%MOVIE_HIGH_QUALITY%"=="1" (
	set bitrate=384k
)

if "%~3"=="qvga" (
	set width=320
	set height=240
	set tmp_height=240
	set aspect=4:3
	
	if "%aspect_type%" == "1" (
		set pad_y=0
		set tmp_height=240
		set aspect=4:3
	)
	if "%aspect_type%" == "2" (
		set pad_y=30
		set tmp_height=180
		set aspect=16:9
	)	
) else (
	set width=176
	set height=144
	set tmp_height=%height%
	if "%aspect_type%" == "1" (
		set pad_y=6
		set tmp_height=132
		set aspect=4:3
	)
	if "%aspect_type%" == "2" (
		set pad_y=22
		set tmp_height=100
		set aspect=16:9
	)	
)

echo height=%height%
echo width=%width%
echo pad_y=%pad_y%
echo tmp_height=%tmp_height%
echo aspect=%aspect%
echo acodec=%acodec%
	
"%timer_executor%" %ffmpeg_timeout% "%ffmpeg_dir%\ffmpeg.exe" -i "%~1" ^
		-acodec %acodec% 			^
		-ab 12.2k							^
		-ar 8000							^
		-ac 1 								^
		-vcodec mpeg4						^
		-r 15								^
		-b %bitrate%						^
		-s %width%x%tmp_height%				^
		-aspect %aspect%					^
		-f %~5								^
		"%~2.tmp"		
if "%errorlevel%"=="-1" ( 
	call :ON_ERROR 
	exit /B %has_error%
)
		
if "%pad_y%"=="0" (
	rename "%~2.tmp" "%~nx2"	
) else (
	"%timer_executor%" %ffmpeg_timeout% "%ffmpeg_dir%\ffmpeg.exe" -i "%~2.tmp"			^
			-acodec %acodec% 						^
			-ab 12.2k								^
			-ar 8000								^
			-ac 1 									^
			-vcodec mpeg4							^
			-r 15									^
			-b %bitrate%							^
			-vf "pad=0:%height%:0:%pad_y%:black"	^
			-f %~5									^
			"%~2"
	if "%errorlevel%"=="-1" ( 
		call :ON_ERROR 
		exit /B %has_error%
	)
	
	DEL /Q "%~2.tmp"
)
		
exit /B 0


REM ********************************************************
REM -- :CONVERT_MP4
REM -- 動画ファイルをmp4に変換する
REM ********************************************************
:CONVERT_MP4
if not "%has_error%"=="0" exit /B %has_error%

"%timer_executor%" %ffmpeg_timeout% "%ffmpeg_dir%\ffmpeg.exe" -i "%~1" ^
	-acodec libfaac	^
	-ab 12.2k	^
	-ar 8000	^
	-ac 1		^
	-vcodec libx264	^
	-preset veryfast^
	-r 15		^
	-b 115k		^
	-s 320x240	^
	-aspect 4:3	^
	-f mp4		^
	"%~2.tmp"

if "%errorlevel%"=="-1" ( 
	call :ON_ERROR 
	exit /B %has_error%
)

rename "%~2.tmp" "%~nx2"

exit /B 0


REM ********************************************************
REM -- :PUBLISH
REM -- イメージ格納ディレクトリにコピーする
REM -- 　%~1:コピー元ファイル
REM ********************************************************
:PUBLISH
if not "%has_error%"=="0" exit /B %has_error%

set fname=%~n1%~x1
set fname=%fname:temp_=%
copy /Y %~1 %output_path%\%fname%
exit /B 0


REM ********************************************************
REM -- MP4BOX_FOR_DOCOMO
REM -- イメージ格納ディレクトリにコピーする
REM -- 　%~1:ファイル(ワイルドカード可)
REM ********************************************************
:MP4BOX_FOR_DOCOMO
if not "%has_error%"=="0" exit /B %has_error%

for %%f in ( %~1 ) do (
  %mp4box_dir%\mp4box.exe -add %%f -brand mmp4:1 -new %%f -quiet
)
exit /B 0

REM ********************************************************
REM -- :SPLIT_FILE
REM -- 指定されたファイルを分割する。
REM -- 分割後のファイルには_001からの連番をふる
REM -- 　%~1:サイズ
REM -- 　%~2:ファイル名
REM -- 　%~3:分割後のファイル名
REM ********************************************************
:SPLIT_FILE
if not "%has_error%"=="0" exit /B %has_error%

copy /Y %~2 %~3
%mp4box_dir%\mp4box.exe -split-size %~1 %~3 -quiet
if not exist %~d3%~p3%~n3_001%~x3 copy %~3 %~d3%~p3%~n3_001%~x3
del /Q %~3

set file_count=0
for %%f in ("%~d3%~p3%~n3_*.*") do set /a file_count=file_count+1
@echo on
echo %~4,%file_count% >> %result_file%
@echo off

exit /B 0

REM ********************************************************
REM -- :ATOM_CHANGE
REM -- 指定された条件に該当するファイルに対してAtomChangerを適用する
REM -- 　%~1:ファイル(ワイルドカード可)
REM ********************************************************
:ATOM_CHANGE
if not "%has_error%"=="0" exit /B %has_error%

copy /Y %~1 %~1.actmp 
for %%f in (%~1.actmp ) do %work_dir%\ATOMChanger.exe %%f %%~df%%~pf%%~nf %work_dir%\kddicopyrigt.ini
del /Q %~1.actmp
exit /B 0


REM ********************************************************
REM -- :ON_ERROR
REM ********************************************************
:ON_ERROR
if not "%has_error%"=="0" exit /B %has_error%

xcopy /Y "%work_dir%%input_file_name%" "%error_dir%\"
set has_error=1

exit /B %has_error%

REM ********************************************************
REM -- :END
REM ********************************************************
:END
exit /B %has_error%
