@ECHO OFF

REM MailToolsディレクトリのパス
SET MAIL_TOOLS_DIR=%~d0%~p0
REM DEBUGフラグ
SET IS_DEBUG=0

REM == 環境依存の設定項目 ============================================

REM MailParseプロンプトのタイトル
SET MAIL_PARSE_PROMPT_TITLE=PWILD_MAIL_PARSE


REM DB接続情報
SET DB_DSN=dbi:Oracle:host=192.168.5.198.;sid=IBDB
SET DB_USER=VICOMM_PWILD
SET DB_PWD=VICOMM_PWILD

REM MAIL情報
SET POP_HOST=101.110.20.115
SET POP_USER=vicomm@girls-chat.tv
SET POP_PWD=vicomm

REM ERROR-MAIL情報
SET POP_HOST_FOR_ERR=101.110.20.115
SET POP_USER_FOR_ERR=returnmail@girls-chat.tv
SET POP_PWD_FOR_ERR=vicomm

REM エラーメール処理間隔
SET ERRMAIL_INTERVAL=180

REM エラーメール処理タイプ(accelmail,postfix)
SET ERRMAIL_TYPE=postfix

REM FTP情報
SET FTP_HOST=
SET FTP_USER=
SET FTP_PWD=

REM オブジェクトの配置先(SLAVE)
SET SLAVE_HOST=
REM SET SLAVE_HOST=\\192.168.5.100
REM SET SLAVE_HOST=\\127.0.0.1

REM ==================================================================

SET OUTPUT_VALUE=%~1

if "%OUTPUT_VALUE%"=="1" (
    ECHO == 設定値 =======================================================
    ECHO  MAIL_TOOLS_DIR               : "%MAIL_TOOLS_DIR%"
    ECHO  MAIL_PARSE_PROMPT_TITLE      : "%MAIL_PARSE_PROMPT_TITLE%"
    ECHO  DB_DSN                       : "%DB_DSN%"
    ECHO  DB_USER                      : "%DB_USER%"
    ECHO  DB_PWD                       : "%DB_PWD%"
    ECHO  POP_HOST                     : "%POP_HOST%"
    ECHO  POP_USER                     : "%POP_USER%"
    ECHO  POP_PWD                      : "%POP_PWD%"
    ECHO  POP_HOST_FOR_ERR             : "%POP_HOST_FOR_ERR%"
    ECHO  POP_USER_FOR_ERR             : "%POP_USER_FOR_ERR%"
    ECHO  POP_PWD_FOR_ERR              : "%POP_PWD_FOR_ERR%"    
    ECHO  FTP_HOST                     : "%FTP_HOST%"
    ECHO  FTP_USER                     : "%FTP_USER%"
    ECHO  FTP_PWD                      : "%FTP_PWD%"
    ECHO  ERRMAIL_INTERVAL             : "%ERRMAIL_INTERVAL%"
    ECHO  ERRMAIL_TYPE                 : "%ERRMAIL_TYPE%"
    ECHO =================================================================
)


