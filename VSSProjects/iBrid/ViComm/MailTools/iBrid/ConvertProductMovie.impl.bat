@echo on
REM /*************************************************************************
REM --	System			: ViComm
REM --	Sub System Name	: 
REM --	Title			: 動画変換バッチ
REM --	Progaram ID		: ConvertProductMovie.impl.bat
REM --  Creation Date	: 
REM --  Creater			: i-Brid
REM --
REM -- [Arguments]
REM -- %~1:入力ディレクトリ(MailtToolディレクトリ)
REM -- %~2:入力ファイル名
REM -- %~3:出力ディレクトリ(キャストのMovieディレクトリ)
REM -- %~4:ファイル番号
REM -- %~5:アスペクト比		0:画像サイズに合わせる、1:normal(4:3)、2:wide(16:9)
REM -- %~6:販売動画フラグ	
REM -- %~7:サムネイル画像出力パス
REM **************************************************************************/
REM [ Update History ]
REM /*------------------------------------------------------------------------
REM 
REM   Date        Updater    Update Explain
REM   2010/06/02  伊藤和明   各キャリアにあわせた動画サイズ分割の機能を追加
REM                          マルチスレッド対応
REM   2010/08/27  伊藤和明   画像のサイズ比を指定できるように変更
REM   2010/11/08  伊藤和明   TVCC→ffmpegへ変更
REM 
REM -------------------------------------------------------------------------*/

set T=%TIME:/=%                                                                 

echo p1=%~1
echo p2=%~2
echo p3=%~3
echo p4=%~4
echo p5=%~5
echo p6=%~6
echo p7=%~7

set current_dir=%~d0%~p0
set work_dir=%~1
set input_file_name=%~2
set output_path=%~3
set obj_seq=%~4
set aspect_type=%~5
set with_split_flag=%~6
set output_thumbnail_path=%~7

set mp4box_dir="D:\i-Brid\MP4BOX"
set ffmpeg_dir=%work_dir%\lib\ffmpeg
set temp_file_prefix=temp_%obj_seq%_
set result_file=%~d0%~p0%obj_seq%.result

call %CURRENT_DIR%_env.bat

if "%MOVIE_SAVE_ORIGINAL%"=="" (
	SET MOVIE_SAVE_ORIGINAL=1
)
rem -- UNCの場合はアクセスできるように認証を済ませておく
call %current_dir%_netuse.bat "%output_path%" "vicomm" "vicomm"

rem -- 元となる画像の保存
if "%MOVIE_SAVE_ORIGINAL%"=="1" (
	copy /Y %work_dir%\%input_file_name% %work_dir%\original\%input_file_name%
)

rem 動画ファイル形式のチェック
%ffmpeg_dir%\ffmpeg.exe -i %work_dir%\%input_file_name% > %work_dir%\%input_file_name%.info
find /C "%work_dir%\%input_file_name%.info" "ffmpeg file decoder"
if "%ERRORLEVEL%"=="0" (
	ECHO ファイル形式が不正です。
	EXIT;
)


rem 変換
call :CONVERT "%work_dir%\%input_file_name%"         "%work_dir%\%temp_file_prefix%tmp.3gp"  qvga 3gp 384k 0
call :CONVERT "%work_dir%\%temp_file_prefix%tmp.3gp" "%work_dir%\%temp_file_prefix%qvga.3gp" qvga 3gp 384k %aspect_type%
call :CONVERT "%work_dir%\%temp_file_prefix%tmp.3gp" "%work_dir%\%temp_file_prefix%qcif.3gp" qcif 3gp 64k  %aspect_type%
call :CONVERT "%work_dir%\%temp_file_prefix%tmp.3gp" "%work_dir%\%temp_file_prefix%qvga.3g2" qvga 3g2 128k %aspect_type%

rem 分割
rem for docomo
call :SPLIT_FILE 30 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_d_d.3gp 11
echo 12,0 >> %result_file%
echo 13,0 >> %result_file%
echo 14,0 >> %result_file%
echo 15,0 >> %result_file%
call :SPLIT_FILE 180 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_d_s2.3gp 16	

rem for softbank
call :SPLIT_FILE 20 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_s_d.3gp 21
echo 22,0 >> %result_file%
echo 23,0 >> %result_file%
echo 24,0 >> %result_file%

rem for au
echo 31,0 >> %result_file%
echo 32,0 >> %result_file%
call :SPLIT_FILE 60 %work_dir%\%temp_file_prefix%qvga.3g2 %work_dir%\%temp_file_prefix%qvga_a_d.3g2 33
echo 34,0 >> %result_file%


copy /Y %work_dir%\%temp_file_prefix%qvga_d_s2_001.3gp %output_path%\%obj_seq%.3gp

rem -- docomoのストリーミング用にATOM構造を組み替える(MP4BOX)
call :MP4BOX_FOR_DOCOMO  %work_dir%\%temp_file_prefix%qcif_d_s*.3gp
call :MP4BOX_FOR_DOCOMO  %work_dir%\%temp_file_prefix%qvga_d_s*.3gp

rem -- for AU (atom change) 再生できるように動画ﾌｧｲﾙを偽装する
call :ATOM_CHANGE %work_dir%\%temp_file_prefix%*.3g2 

rem -- publish(新)
for %%f in (%work_dir%\%temp_file_prefix%????_*) do (
  call :PUBLISH "%%f" 
)

rem -- ★ﾐﾗｰﾘﾝｸﾞ
call %work_dir%\Mirror.bat "%output_path%\%obj_seq%*"

rem -- 処理後ファイルの削除
del /Q %work_dir%\%input_file_name%*
del /Q %work_dir%\%temp_file_prefix%*.3gp
del /Q %work_dir%\%temp_file_prefix%*.3g2

set T1=%TIME:/=%                                                                                                                                               
echo 開始時間：%T%                                                              
echo 終了時間：%T1%                                                             
                                                                                 
goto END:




REM ********************************************************
REM -- :CONVERT
REM -- 動画ファイルを変換する
REM ********************************************************
:CONVERT

set width=176
set height=144
set pad_y=0
set bitrate=%~5
if "%~3"=="qvga" (
	set width=320
	set height=240
	set tmp_height=240
	if "%~6" == "1" (
		set pad_y=0
		set tmp_height=240
	)
	if "%~6" == "2" (
		set pad_y=30
		set tmp_height=180
	)	
) else (
	set width=176
	set height=144
	set tmp_height=%height%
	if "%~6" == "1" (
		set pad_y=6
		set tmp_height=132
	)
	if "%~6" == "2" (
		set pad_y=22
		set tmp_height=100
	)	
)

echo height=%height%
echo width=%width%
echo pad_y=%pad_y%
echo tmp_height=%tmp_height%

%ffmpeg_dir%\ffmpeg.exe -i "%~1" ^
		-acodec libopencore_amrnb 			^
		-ab 12.2k							^
		-ar 8000							^
		-ac 1 								^
		-vcodec mpeg4						^
		-r 15								^
		-b %bitrate%						^
		-vf "scale=%width%:%tmp_height%,pad=0:%height%:0:%pad_y%:black"	^
		-f %~4								^
		"%~2.tmp"		
		
rename "%~2.tmp" "%~nx2"	
	
exit /B 0

REM ********************************************************
REM -- :PUBLISH
REM -- イメージ格納ディレクトリにコピーする
REM -- 　%~1:コピー元ファイル
REM ********************************************************
:PUBLISH
set fname=%~n1%~x1
set fname=%fname:temp_=%
copy /Y %~1 %output_path%\%fname%
exit /B 0


REM ********************************************************
REM -- MP4BOX_FOR_DOCOMO
REM -- イメージ格納ディレクトリにコピーする
REM -- 　%~1:ファイル(ワイルドカード可)
REM ********************************************************
:MP4BOX_FOR_DOCOMO
for %%f in ( %~1 ) do (
  %mp4box_dir%\mp4box.exe -add %%f -brand mmp4:1 -new %%f -quiet
)
exit /B 0

REM ********************************************************
REM -- :SPLIT_FILE
REM -- 指定されたファイルを分割する。
REM -- 分割後のファイルには_001からの連番をふる
REM -- 　%~1:分割する再生時間
REM -- 　%~2:ファイル名
REM -- 　%~3:分割後のファイル名
REM ********************************************************
:SPLIT_FILE
copy /Y %~2 %~3
%mp4box_dir%\mp4box.exe -split %~1 %~3 -quiet
if not exist %~d3%~p3%~n3_001%~x3 copy %~3 %~d3%~p3%~n3_001%~x3
del /Q %~3

set file_count=0
for %%f in ("%~d3%~p3%~n3_*.*") do set /a file_count=file_count+1
@echo on
echo %~4,%file_count% >> %result_file%
@echo off

exit /B 0

REM ********************************************************
REM -- :ATOM_CHANGE
REM -- 指定された条件に該当するファイルに対してAtomChangerを適用する
REM -- 　%~1:ファイル(ワイルドカード可)
REM ********************************************************
:ATOM_CHANGE
copy /Y %~1 %~1.actmp 
for %%f in (%~1.actmp ) do %work_dir%\ATOMChanger.exe %%f %%~df%%~pf%%~nf %work_dir%\kddicopyrigt.ini
del /Q %~1.actmp
exit /B 0






:END
exit /B 0
