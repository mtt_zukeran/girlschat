#!C:/Perl/bin/perl.exe
use strict;
use warnings;
use iBridLib::ImageUtil;
use iBridLib::Env;
use Image::Magick;
use Time::Piece;

if($iBridLib::Env::is_debug eq "1"){
	print "-- CopyRight.cgi args ------ \r\n";
	foreach ( @ARGV ){
		print $_, "\n";	# 呼び出し時の引数をすべて出力
	}
	print "------------------------------- \r\n";
}

my $input_dir_path		= shift;
my $input_file_name		= shift;
my $stamp_file_path		= shift;

my $input_file_path = "$input_dir_path\\$input_file_name";

&iBridLib::ImageUtil::StampImage($input_file_path, $stamp_file_path );
&iBridLib::ImageUtil::CommentKddiCopyright($input_file_path);

# ﾐﾗｰﾘﾝｸﾞ
system("$ENV{'CURRENT_DIR'}Mirror.bat","$input_file_path");
