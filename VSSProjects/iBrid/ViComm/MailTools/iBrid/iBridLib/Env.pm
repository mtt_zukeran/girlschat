#!C:/Perl/bin/perl.exe
package iBridLib::Env;

use strict;
use warnings;

our $is_debug		= "$ENV{'IS_DEBUG'}" || "0";
our $mail_tools_dir	= $ENV{'MAIL_TOOLS_DIR'};

# データベース接続情報（DSN, USER, PASSWORD ）
our $db_dsn		= $ENV{'DB_DSN'};
our $db_user	= $ENV{'DB_USER'};
our $db_pwd		= $ENV{'DB_PWD'};

# POP接続情報（HOST, USER, PASSWORD ）
our $pop_host	= $ENV{'POP_HOST'};
our $pop_user	= $ENV{'POP_USER'};
our $pop_pwd	= $ENV{'POP_PWD'};

# ｴﾗｰﾒｰﾙ用POP接続情報（HOST, USER, PASSWORD ）
our $pop_host_for_err	= $ENV{'POP_HOST_FOR_ERR'};
our $pop_user_for_err	= $ENV{'POP_USER_FOR_ERR'};
our $pop_pwd_for_err	= $ENV{'POP_PWD_FOR_ERR'};

# FTP接続情報（HOST, USER, PASSWORD ）
our $ftp_host	= $ENV{'FTP_HOST'};
our $ftp_user	= $ENV{'FTP_USER'};
our $ftp_pwd	= $ENV{'FTP_PWD'};

# エラーメールの処理間隔
our $errmail_interval	= $ENV{'ERRMAIL_INTERVAL'} || 30;
our $errmail_type = $ENV{'ERRMAIL_TYPE'};

return 1;