#!C:/Perl/bin/perl.exe
package iBridLib::DbSession;

use strict;
use warnings;
use DBI;

sub new {
	my $class 	= shift;
	my $self 	= { password => undef };
	
	bless( $self, $class );
	return $self;
}

sub connect{
	my $self			= shift;
	$self->{db}			= shift;
	$self->{user}		= shift;
	$self->{password}	= shift;

	print "Connect DB\n";
	$self->{connection} = DBI->connect( $self->{db}, $self->{user}, $self->{password} )	or die "CONNECT ERROR $DBI::errstr"; 
}

sub disconnect{
	my $self		= shift;

	print "Disconnect DB\n";
	$self->{connection}->disconnect() or warn $self->{connection}->errstr;;
}

sub getConnection(){
	my $self	= shift;
	return $self->{connection};
}

sub getSiteInfo(){
	my $self		= $_[0];
	my $siteCd		= $_[1];
	
	my $cmd = $self->{connection}->prepare(q/
		SELECT * FROM T_SITE WHERE SITE_CD=:SITE_CD
	/) || die $self->{connection}->errstr;

	$cmd->bind_param(':SITE_CD', $siteCd);
	$cmd->execute() || die $cmd->errstr;

	my $site_info = $cmd->fetchrow_hashref;

	undef $cmd;
	
	return $site_info;
}

sub getMailParseUse{
	my $self		= $_[0];
	my $siteCd		= $_[1];
	
	my ($stampFileNm, $stampSampleFileNm, $picSizeLarge, $picSizeSmall, $scaleType, $recordCount, $status);
	
	# prepare
	my $cmd = $self->{connection}->prepare(q/
		BEGIN
			GET_MAILPARSE_USE(
				:pSITE_CD				,
				:pSTAMP_FILE_NM			,
				:pSTAMP_SAMPLE_FILE_NM	,
				:pPIC_SIZE_LARGE		,
				:pPIC_SIZE_SMALL		,
				:pSCALE_TYPE			,
				:pRECORD_COUNT			,
				:pSTATUS				
			);
		END;
	/) || die $self->{connection}->errstr;
	
	# execute
	$cmd->bind_param(':pSITE_CD'					, $siteCd);
	$cmd->bind_param_inout(':pSTAMP_FILE_NM'		, \$stampFileNm			, 256	);
	$cmd->bind_param_inout(':pSTAMP_SAMPLE_FILE_NM'	, \$stampSampleFileNm	, 256	);
	$cmd->bind_param_inout(':pPIC_SIZE_LARGE'		, \$picSizeLarge		, 3		);
	$cmd->bind_param_inout(':pPIC_SIZE_SMALL'		, \$picSizeSmall		, 3		);
	$cmd->bind_param_inout(':pSCALE_TYPE'			, \$scaleType			, 1		);
	$cmd->bind_param_inout(':pRECORD_COUNT'			, \$recordCount			, 1		);
	$cmd->bind_param_inout(':pSTATUS'				, \$status				, 10	);
	
	$cmd->execute() || die $cmd->errstr;
	
	undef $cmd;
	
	if($status ne 'I000') {
		die "STORED PROCEDURE ERROR $status"; 
	}
	
	return ($stampFileNm, $stampSampleFileNm, $picSizeLarge, $picSizeSmall, $scaleType, $recordCount);
}

sub attachedFileUpload{
	my $self			= $_[0];
	my $mail_from		= $_[1];
	my $mail_to			= $_[2];
	my $mail_subject	= $_[3];
	my $mail_body		= $_[4];
	my $mime_type		= $_[5];
	
	my ($obj_type, $out_dir, $file_nm, $movie_seq, $ftp_file_nm, $site_cd,$out_thumbnail_dir, $status);
	
	# prepare
	my $cmd = $self->{connection}->prepare(q/
		BEGIN
			ATTACHED_FILE_UPLOAD(
				:pFROM_ADDR			,
				:pTO_ADDR			,
				:pSUBJECT			,
				:pBODY				,
				:pMIME_TYPE			,
				:pOBJECT_TYPE		,
				:pOUT_DIR			,
				:pOUT_THUMBNAIL_DIR	,
				:pFILE_NM			,
				:pMOVIE_SEQ			,
				:pFTP_FILE_NM		,
				:pSITE_CD			,
				:pSTATUS
			);
		END;
	/) || die $self->{connection}->errstr;
	
	# execute
	$cmd->bind_param(':pFROM_ADDR'	, $mail_from	);
	$cmd->bind_param(':pTO_ADDR'	, $mail_to		);
	$cmd->bind_param(':pSUBJECT'	, $mail_subject	);
	$cmd->bind_param(':pBODY'		, $mail_body	);
	$cmd->bind_param(':pMIME_TYPE'	, $mime_type);
	$cmd->bind_param_inout(':pOBJECT_TYPE'			, \$obj_type			, 1		);
	$cmd->bind_param_inout(':pOUT_DIR'				, \$out_dir				, 128	);
	$cmd->bind_param_inout(':pOUT_THUMBNAIL_DIR'	, \$out_thumbnail_dir	, 128	);
	$cmd->bind_param_inout(':pFILE_NM'				, \$file_nm 			, 15	);
	$cmd->bind_param_inout(':pMOVIE_SEQ'			, \$movie_seq 			, 15	);
	$cmd->bind_param_inout(':pFTP_FILE_NM'			, \$ftp_file_nm 		, 15	);
	$cmd->bind_param_inout(':pSITE_CD'				, \$site_cd				, 10	);
	$cmd->bind_param_inout(':pSTATUS'				, \$status				, 10	);

	$cmd->execute() || die $cmd->errstr;

	undef $cmd;
	
	if($status ne 'I000') {
		die "STORED PROCEDURE ERROR $status"; 
	}
	
	return ($obj_type, $out_dir, $out_thumbnail_dir, $file_nm, $movie_seq, $ftp_file_nm, $site_cd, $status);
}

sub attachedFileCountSet{
	my ($self , $movie_seq , @file_num_arr) = @_;
	my ($status);
	
	# prepare
	my $cmd = $self->{connection}->prepare(q/
		BEGIN
			ATTACHED_FILE_COUNT_SET(
				:pMOVIE_SEQ ,
				:pFILE_COUNT_01 ,
				:pFILE_COUNT_02 ,
				:pFILE_COUNT_03 ,
				:pFILE_COUNT_04 ,
				:pFILE_COUNT_05 ,
				:pFILE_COUNT_06 ,
				:pFILE_COUNT_07 ,
				:pFILE_COUNT_08 ,
				:pFILE_COUNT_09 ,
				:pFILE_COUNT_10 ,
				:pFILE_COUNT_11 ,
				:pFILE_COUNT_12 ,
				:pSTATUS
			);
		END;
	/) || die $self->{connection}->errstr;
	
	# execute
	$cmd->bind_param(':pMOVIE_SEQ'		, $movie_seq		);
	$cmd->bind_param(':pFILE_COUNT_01'	, $file_num_arr[0]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_02'	, $file_num_arr[1]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_03'	, $file_num_arr[2]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_04'	, $file_num_arr[3]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_05'	, $file_num_arr[4]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_06'	, $file_num_arr[5]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_07'	, $file_num_arr[6]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_08'	, $file_num_arr[7]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_09'	, $file_num_arr[8]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_10'	, $file_num_arr[9]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_11'	, $file_num_arr[10]	|| 0	);
	$cmd->bind_param(':pFILE_COUNT_12'	, $file_num_arr[11]	|| 0	);
	$cmd->bind_param_inout(':pSTATUS'	, \$status		, 10);
	
	$cmd->execute() || die $cmd->errstr;

	undef $cmd;
	
	if($status ne 'I000') {
		die "STORED PROCEDURE ERROR $status"; 
	}
	
}
return 1;