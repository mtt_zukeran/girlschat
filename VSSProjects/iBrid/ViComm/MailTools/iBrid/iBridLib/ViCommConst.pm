#!C:/Perl/bin/perl.exe
package iBridLib::ViCommConst;

use strict;
use warnings;

#
# メール添付種別
#
our $ATTACHED_MAIL_PIC		= "1";	# 写真
our $ATTACHED_MAIL_MOVIE	= "2";	# 動画
our $ATTACHED_MAIL_DIARY	= "3";	# 日記
our $ATTACHED_MAIL_TWEET	= "9";	# つぶやき
our $ATTACHED_MAIL_JYANKEN	= "7";	# ジャンケン画像
our $ATTACHED_MAIL_YAKYUKEN	= "8";	# 野球拳画像

return 1;