#!C:/Perl/bin/perl.exe
package iBridLib::ImageUtil;

use strict;
use warnings;
use Image::Magick;
use Image::ExifTool;
use Time::Piece;
use Getopt::Std;


	###################################################################################################
	#
	# 画像方向にあわせて回転させる
	#
	#	使い方:
	#		&CorrectRotationImage($src_file);
	#	
	#		$src_file	: 入力ファイルパス
	#
	sub CorrectRotationImage{
		my $src_file 		= $_[0];

		# オブジェクト生成
		my $image = Image::Magick->new(magick=>'jpg');

		# 画像読み込み
		$image->Read($src_file);

		# Exifデータを読み取る
		my $srcExifTool = new Image::ExifTool;
		$srcExifTool->ExtractInfo($src_file) or die $!;

		my $exifRotate = $srcExifTool->GetValue("Orientation", "Raw");
		print "Orientation:";

		# 画像の向きが設定されている場合
		if(defined($exifRotate)){
			print $srcExifTool->GetValue("Orientation") . "($exifRotate)";

			if($exifRotate == 1){
				# そのまま表示
			}elsif($exifRotate == 2){
				# 左右反転
				$image->Flop;

			}elsif($exifRotate == 3){
				# 時計回りに180度回転
				$image->Rotate(degrees=>180);

			}elsif($exifRotate == 4){
				# 上下反転 (左右反転&時計周りに180度回転でも可能)
				$image->Flip;

			}elsif($exifRotate == 5){
				# 時計周りに90度回転&左右反転 (左右反転&時計周りに270度回転でも可能)
				$image->Rotate(degrees=>90);
				$image->Flop;

			}elsif($exifRotate == 6){
				# 時計回りに90度回転
				$image->Rotate(degrees=>90);

			}elsif($exifRotate == 7){
				# 時計周りに270度回転&左右反転 (左右反転&時計周りに90度回転でも可能)
				$image->Rotate(degrees=>270);
				$image->Flop;

			}elsif($exifRotate == 8){
				# 時計回りに270度回転
				$image->Rotate(degrees=>270);
			}

			# 画像を上書き保存
			$image->Write($src_file);
		}else{
			print "--none";
		}
		print "\n";

		# オブジェクトを削除する
		undef $image;
	}

	###################################################################################################
	#
	# 比率をサイズに変更する
	#
	#	使い方:
	#		($large_resize_width, $small_resize_width) = ConvertRatio2Width($input_file_path, $large_resize_ratio, $small_resize_ratio );
	#	
	#		$input_file_path	: 入力ファイルパス
	#		$large_resize_ratio	: 大画像の横比率
	#		$small_resize_ratio	: 小画像の横比率
	#
	sub ConvertRatio2Width{
		my $input_file_path 		= $_[0];
		my $large_resize_ratio		= $_[1];
		my $small_resize_ratio		= $_[2];

		# オブジェクト生成
		my $img					= Image::Magick->new(magick=>'jpg');
		
		# ファイルの読み込み
		my $status = $img->Read($input_file_path);
	
		# 画像の縦、横サイズ取得
		my ($original_width,$original_height) = $img->Get('width','height');
		
		# オブジェクト破棄
		undef $img;

		return ($original_width * $large_resize_ratio,$original_width * $small_resize_ratio);
	}
	
	###################################################################################################
	#
	# 画像をリサイズするサブルーチン
	#
	#	使い方:
	#		&ResizeImage($input_file_path, $output_file_path, $length_of_side, $is_square);
	#	
	#		$input_file_path	: 入力ファイルパス
	#		$output_file_path	: 出力ファイルパス
	#		$length_of_side		: リサイズ後の画像の横幅
	#		$is_square			: 正方形に収まるサイズにするかどうかを示す値。1:正方形、2:それ以外
	#
	sub ResizeImage{
		my $input_file_path 	= $_[0];
		my $output_file_path 	= $_[1];
		my $length_of_side		= $_[2];
		my $is_square			= $_[3] || 0;
		
		# オブジェクト生成
		my $img					= Image::Magick->new(magick=>'jpg');
		
		my $status = $img->Read($input_file_path);
		my ($original_width,$original_height) = $img->Get('width','height');

		my $resize_width	= 0;
		my $resize_height 	= 0;
		
		if(($is_square==1) and ($original_width<$original_height)){
			$resize_width 	= $original_width * ($length_of_side / $original_height);
			$resize_height	= $length_of_side;
		}else{
			$resize_width	= $length_of_side;
			$resize_height	= $original_height * ($length_of_side / $original_width);
		}

		$img->Scale(width => $resize_width, height=> $resize_height );
		$img->Write($output_file_path);
	
		undef $img;
	}
	


	###################################################################################################
	
	###################################################################################################
	#
	# 余白を塗りつぶす
	#
	#	使い方:
	#		&FillBlankSpace($file_path,$length_of_side, $fill_color );
	#	
	#		$file_path			: 入力ファイルパス
	#		$length_of_side		: リサイズ後の画像の横幅
	#		$fill_color			: 余白を塗りつぶす色
	#
	sub FillBlankSpace{
		my $file_path 		= $_[0];
		my $length_of_side	= $_[1];
		my $fill_color		= $_[2] || "#FFFFFF";
		
		# オブジェクト生成
		my $img			= Image::Magick->new(magick=>'jpg');
		my $back_img	= Image::Magick->new();
		
		$img->Read($file_path);
		
		$back_img->Read("xc:$fill_color");
		$back_img->Scale(width=>$length_of_side,height=>$length_of_side);
		$back_img->Composite(image=>$img, compose=>'over', gravity=>'Center');
		$back_img->Write($file_path);

		undef $back_img;
		undef $img;
	}
	


	###################################################################################################

	###################################################################################################
	#
	# 元となる画像にスタンプ画像を合成する
	#
	#	使い方:
	#		StampImage($base_file_path, $stamp_file_path, $resize_width );
	#	
	#		$base_file_path		: 入力ファイルパス
	#		$stamp_file_path	: 出力ファイルパス
	#
	sub StampImage{
		my $base_file_path 	= $_[0];
		my $stamp_file_path	= $_[1];

		# オブジェクト生成
		my $base_img		= Image::Magick->new(magick=>'jpg');
		my $stamp_img		= Image::Magick->new();
		
		# ファイルの読み込み
		$base_img->Read($base_file_path);
		$stamp_img->Read($stamp_file_path);
	
		# スタンプ画像を元となる画像に合成する。
		$base_img->Composite(image=>$stamp_img, compose=>'over', gravity=>'SouthEast');
		
		# ファイルの書き出し
		$base_img->Write($base_file_path);
		
		# オブジェクト破棄
		undef $base_img;
		undef $stamp_img;
		
	}

	###################################################################################################

	###################################################################################################
	#
	# 元となる画像にスタンプ画像を合成する
	#
	#	使い方:
	#		&CommentKddiCopyright($file_path);
	#	
	#		$file_path	: 画像ファイルパス
	#
	sub CommentKddiCopyright{
		my $file_path 	= $_[0];
			
		# オブジェクト生成
		my $img		= Image::Magick->new(magick=>'jpg');
	
		# ファイルの読み込み
		$img->Read($file_path);
		
		# Au
		$img->CommentImage('kddi_copyright=on,copy="NO"');	
		
		# ファイルの書き出し
		$img->Write($file_path);

		# オブジェクト破棄
		undef $img;
		
	}
	###################################################################################################
	
	###################################################################################################
	#
	# 元にぼかしを入れる
	#
	#	使い方:
	#		BlurImage($input_file_path,$output_file_path);
	#	
	#		$input_file_path	: 入力ファイルパス
	#		$output_file_path	: 出力ファイルパス
	#
	sub BlurImage{
		my $input_file_path 	= $_[0];
		my $output_file_path 	= $_[1];
		my $size				= $_[2];
		my $is_square			= $_[3] || 0;
				
		# オブジェクト生成
		my $img				= Image::Magick->new(magick=>'jpg');
		
		# ファイルの読み込み
		$img->Read($input_file_path);
		
		if($is_square==1){
			# 正方形に切り抜く
			$img->Crop(width=>$size, height=>$size, gravity=>'Center');
		}
								 
		#ぼかしをかける
		$img->Blur('7x7');
	
		# ファイルの書き出し
		if($is_square==1){
			$img->Set(page=>$size . "x" . $size . "+0+0");
		}
		$img->Write($output_file_path);
		
		# オブジェクト破棄
		undef $img;
		
	}
	###################################################################################################

	###################################################################################################
	#
	# 画像を重ねる
	#
	#	使い方:
	#		BlurImage($input_file_path,$stamp_file_path);
	#	
	#		$input_file_path	: 入力ファイルパス
	#		$stamp_file_path	: 重ねるﾌｧｲﾙのファイルﾊﾟｽ
	#
	sub Superimposes{
		my $input_file_path 	= $_[0];
		my $stamp_file_path 	= $_[1];
				
		# オブジェクト生成
		my $img				= Image::Magick->new(magick=>'jpg');
		my $stamp_img		= Image::Magick->new();
		
		if (not -f $stamp_file_path) {
			 return;
		}
		# ファイルの読み込み
		$img->Read($input_file_path);
		$stamp_img->Read($stamp_file_path);
		
		my ($resize_width)	= $img->Get('width');
		my ($original_width	,$original_height)	= $stamp_img->Get('width','height');
		my $resize_height	= $original_height * ($resize_width / $original_width);

		#画像を重ねる
		$stamp_img->Scale( width => $resize_width, height=> $resize_height );
		$img->Composite(image=>$stamp_img, compose=>'over', gravity=>'Center');
		
		# ファイルの書き出し
		$img->Write($input_file_path);
		
		# オブジェクト破棄
		undef $img;
		
	}
	###################################################################################################
	
	
	sub CopyExif{
		my $src_file 	= $_[0];
		my $dest_file 	= $_[1];
		
		eval{
		
			my $srcExifTool = new Image::ExifTool;
			$srcExifTool->ExtractInfo($src_file) or die $!;
			
			
			my $destExifTool = new Image::ExifTool;
			$destExifTool->ExtractInfo($dest_file) or die $!;
			
			my @target_attr_list = (
			# --- IFD0 --- 
				"ImageDescription"			, #画像説明
				"Make"						, #カメラの製造元
				"Model"						, #カメラのモデル
				"DateTime"					, #この画像が最後に変更された日時を示します
				"Software"					, #デジカメのファームウェア(内蔵ソフトウェア)の版数
			# --- SubIFD --- 
				"ExposureTime"			 	, #露出時間
				"FNumber"				 	, #レンズのF値 	
				"DateTimeOriginal"		 	, #オリジナル撮影日時
				"ShutterSpeedValue"		 	, #シャッタースピード	
				"ApertureValue"			 	, #絞り値
				"Flash"					 	, #フラッシュモード
				"MaxApertureValue"		 	, #最大絞り値
				"MeteringMode"			 	, #測光モード	
				"FocalLength"			 	, #焦点距離
				"FlashpixVersion"		 	, #フラッシュピックスのバージョン	
				"UserComment"			 	, #ユーザーコメント
				"ExifVersion"			 	, #EXIFバージョン
				"ColorSpace"				, #色の表現
				"MakerNote"					, #カメラの内部情報等、メーカー依存データー
				
			# --- その他
				"WhiteBalance"			 	, # ホワイトバランス
				"ExposureMode"			 	, #露出モード
				"SubjectDistanceRange"	 	, #被写体距離範囲
				"DigitalZoomRatio"			, #デジタルズーム比率
				"SceneCaptureType"		 	, #シーン撮影タイプ
				"ExposureCompensation"	 	, #露出補正
				""      
			);
			
			#$destExifTool->SetNewGroups("ExifIFD");
			foreach my $target_attr ( @target_attr_list ){
				print "$target_attr:";
				if(defined($srcExifTool->GetValue($target_attr))){
					print $srcExifTool->GetValue($target_attr);
					$destExifTool->SetNewValue($target_attr, $srcExifTool->GetValue($target_attr));		
				}else{
					print "--none";
				}
				print "\n";
			
			}
						
			# 保存
			rename($dest_file,$dest_file.".tmp");
			
			eval{
				unlink($dest_file);
				$destExifTool->WriteInfo($dest_file.".tmp",$dest_file) or die $!;
			};
			if( $@ ){
				print "catch!! $@\n";
				rename($dest_file.".tmp",$dest_file);
			}
			
			unlink($dest_file.".tmp");	
		};
		
		if( $@ ){
			print "catch! $@\n";
		}

	}

return 1;