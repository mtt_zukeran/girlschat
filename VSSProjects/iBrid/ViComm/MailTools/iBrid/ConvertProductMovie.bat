call %~d0%~p0\ConvertProductMovie.impl.bat %1 %2 %3 %4 %5 %6 %7>%~d0%~p0\%~4_convert.log
if not "%errorlevel%"=="0" (
	echo "-- %~4 logs ------ \r\n"
	type %~d0%~p0%~4_convert.log >>%~d0%~p0\logs\cmovie_product_%date:~0,4%%date:~5,2%%date:~8,2%.log
	echo "------------------------------- \r\n"
)
del /Q %~d0%~p0\%~4_convert.log
