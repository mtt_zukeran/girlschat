#!C:/Perl/bin/perl.exe

use MIME::Parser;
use MIME::WordDecoder;
use MIME::Words qw(decode_mimewords);
use Image::Magick;
use Jcode;
use DBI;
use File::Copy;
use File::Path;
use Net::POP3;
use Time::Piece;
use HTTP::Date;
use iBridLib::Env;
use iBridLib::DbSession;
use iBridLib::ViCommConst;

my $existMail;

my $parser = MIME::Parser->new();
my $dbSession = new iBridLib::DbSession();
my $err_mail_file_dir = '.\\err_mail_file';

print "[ErrMailToolPostfix]\t".localtime(time)."\tSTART\n";
eval{
	#-----------------------------------------------------------#
	# Error Mail Parse                                          #
	#-----------------------------------------------------------#
	$parser->output_dir($err_mail_file_dir);
	
	$pop = Net::POP3->new($iBridLib::Env::pop_host_for_err) or die "Can't not open account.";
	$pop->login($iBridLib::Env::pop_user_for_err,$iBridLib::Env::pop_pwd_for_err) or die 'POP authentication error';
	$existMail = 0;

	$mailbox = $pop->list();

	foreach $msgid (keys %$mailbox) {
		if ($existMail == 0) {
			$dbSession->connect($iBridLib::Env::db_dsn,$iBridLib::Env::db_user,$iBridLib::Env::db_pwd) or die "CONNECT ERROR $DBI::errstr"; 
			$db =$dbSession->getConnection();
			$cmd_error_mail = $db->prepare(q/
				BEGIN
					RX_ERROR_MAIL(:pMAIL_HOST,:pNON_EXIST_MAIL_ADDR,:pRX_DATE,:pSTATUS,:pMAIL_LOCAL,:pERROR_TYPE,:pMULLER3_FT_ERR_FLAG);
				END;
			/);
			$existMail = 1;
		}

		eval{
			my $message = $parser->parse_data($pop->get($msgid));
			my $mail_local = '';
			my $mail_host = '';
			my $mail_rx_date = '';
			my $mail_body = '';
			my $mail_addr = '';
			my $error_flag = 0;
			my $error_type = 1;
			my $muller3_ft_err_flag = 0;

			my $head = $message->head();
			my $mail_to = $message->head->get('To');
			my $date = $message->head->get('Date');
			my $parts = $message->parts;

			if($mail_to =~ /([a-zA-Z\d._%+-]+)@([a-zA-Z\d.-]+\.[a-zA-Z]{2,4})/){
				$mail_local = $1;
				$mail_host = $2;
			}

			$mail_rx_date = substr(HTTP::Date::parse_date($date), 0, 19);

			if ($parts > 0) {
				for (my $i = 0;$i < $parts; $i++) {
					my $part = $message->parts($i);
					my $mime_type = $part->mime_type;

					if ($mime_type eq 'text/plain') {
						my $mail_body = $part->bodyhandle->as_string;

						if ($mail_body =~ /<([a-zA-Z\d._%+-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,4})>:/) {
							$mail_addr = $1;
						}

						if ($mail_body =~ /I couldn't find any host/s) {
							$error_flag = 1;
						} elsif ($mail_body =~ /550\s+Unknown\s+user/s) {
							$error_flag = 2;
						} elsif ($mail_body =~ /550\s+Invalid\s+recipient/s) {
							$error_flag = 3;
						} elsif ($mail_body =~ /account\s+is\s+disabled/s) {
							$error_flag = 4;
						} elsif ($mail_body =~ /following\s+recipients\s+was\s+rejected/s) {
							$error_flag = 5;
						} elsif ($mail_body =~ /550.+Unknown\s+user\s+Invalid.+recipient/s) {
							$error_flag = 6;
						} elsif ($mail_body =~ /550.+Unknown\s+user/s) {
							$error_flag = 7;
						} elsif  ($mail_body =~ /550.+User\s+unknown/s) {
							$error_flag = 8;
						} elsif ($mail_body =~ /550.+Recipient\s+rejected/s) {
							$error_flag = 9;
						} elsif ($mail_body =~ /550_Unknown_user/s) {
							$error_flag = 10;
						} elsif ($mail_body =~ /550.+OU-001/s) {
							$error_flag = 11;
						} elsif ($mail_body =~ /550.+Recipient/sm) {
							$error_flag = 12;
						} elsif ($mail_body =~ /550.+known\s+SPAM\s+source/s) {
							$error_flag = 13;
						} elsif ($mail_body =~ /550.+illegal\s+alias/s) {
							$error_flag = 14;
						} elsif ($mail_body =~ /550.+Mail\s+rejected/s) {
							$error_flag = 15;
						} elsif ($mail_body =~ /550.+Recipient\s+verify/s) {
							$error_flag = 16;
						} elsif ($mail_body =~ /The\s+email\s+account\s+that\s+you\s+tried\s+to\s+reach\s+does\s+not\s+exist/s) {
							$error_flag = 17;
						}

						if ($mail_body =~ /in\s+reply\s+to\s+RCPT\s+TO\s+command/s) {
							#不通エラー
							$error_type = 2;
						} elsif ($mail_body =~ /in\s+reply\s+to\s+end\s+of\s+DATA\s+command/s) {
							#フィルタリングエラー
							$error_type = 1;
						}
					} else {
						print "not text\n";
					}
				}
			} else {
				if ($mail_rx_date eq '') {
					$mail_rx_date = substr(HTTP::Date::parse_date(localtime(time)), 0, 19);
				}
				
				my $mime_type = $message->mime_type;

				if ($mime_type eq 'text/plain') {
					my $mail_body = $message->bodyhandle->as_string;

					if ($mail_body =~ /following\s+recipients\s+was\s+rejected/s) {
						$error_flag = 1;
						#不通エラー
						$error_type = 2;

						if ($mail_body =~ /Recipient:\s+<([a-zA-Z\d._%+-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,4})>/) {
							$mail_addr = $1;
						}
					} elsif ($mail_body =~ /destination\s+computer\s+refused/s) {
						$error_flag = 1;
						#フィルタリングエラー
						$error_type = 1;
						
						$muller3_ft_err_flag = 1;
						
						if ($mail_body =~ /Final-Recipient:\s+RFC822;\s+<([a-zA-Z\d._%+-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,4})>/) {
							$mail_addr = $1;
						}
					}
				}
			}

			if ($mail_host ne '' && $mail_addr ne '' && $mail_rx_date ne '' && $error_flag != 0) {
				$mail_addr = lc($mail_addr);
				$cmd_error_mail->bind_param(':pMAIL_HOST', $mail_host);
				$cmd_error_mail->bind_param(':pNON_EXIST_MAIL_ADDR', $mail_addr);
				$cmd_error_mail->bind_param(':pRX_DATE', $mail_rx_date);
				$cmd_error_mail->bind_param_inout(':pSTATUS', \$status, 10);
				$cmd_error_mail->bind_param(':pMAIL_LOCAL', $mail_local);
				$cmd_error_mail->bind_param(':pERROR_TYPE', $error_type);
				$cmd_error_mail->bind_param(':pMULLER3_FT_ERR_FLAG', $muller3_ft_err_flag);
				$cmd_error_mail->execute;
				print "RX_ERROR_MAIL(",$mail_host,",",$mail_addr,",",$mail_rx_date,",",$status,",",$mail_local,",",$error_type,",",$muller3_ft_err_flag,")\n";
			} else {
				print "not error mail\n";
			}
		};
		$pop->delete($msgid);
	}
	$pop->quit;
};

# ファイル削除
opendir(DIR, $err_mail_file_dir) or die;
my @file =  grep { /^.*/i  && -f "$err_mail_file_dir/$_"} readdir(DIR);
closedir(DIR);
foreach(@file) {
	unlink "$err_mail_file_dir/$_";
}

if ($existMail){$dbSession->disconnect();}
if( $@ ){print $@;}

print "[ErrMailToolPostfix]\t".localtime(time)."\tEND\n";

