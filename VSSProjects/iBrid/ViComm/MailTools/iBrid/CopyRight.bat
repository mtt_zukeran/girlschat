@ECHO OFF
REM SETが効かなかったので遅延環境変数を利用可能にする
SETLOCAL ENABLEDELAYEDEXPANSION

SET CURRENT_DIR=%~d0%~p0
CD %CURRENT_DIR%

IF "!STR_DATE!"=="" (
	SET STR_DATE=%date:~0,4%%date:~5,2%%date:~8,2%
	SET STR_TIME=%time:~3,2%%time:~6,2%
)

REM 出力するログファイル名 定義
IF "!CRIGHT_OUTPUT_LOG!"=="" (
	SET CRIGHT_TEST_LOG=%CURRENT_DIR%logs\cright_test_stdout_!STR_DATE!.log
	SET CRIGHT_OUTPUT_LOG=%CURRENT_DIR%logs\cright_stdout_!STR_DATE!.log
	SET CRIGHT_ERROR_LOG=%CURRENT_DIR%logs\cright_stderr_!STR_DATE!.log
	SET CRIGHT_RETRY_BAT=%CURRENT_DIR%retry_files\cright_%~4_!STR_DATE!.bat
)

REM 環境変数の読み込み
CALL %CURRENT_DIR%_env.bat

REM 共有ディスクにアクセスできるようにする
CALL %CURRENT_DIR%_netuse.bat "%~1" "vicomm" "vicomm"

REM Copyright処理実行
Perl -I%CURRENT_DIR% %CURRENT_DIR%CopyRight.cgi "%~1" "%~2" "%~3" 2>>!CRIGHT_ERROR_LOG! 1>>!CRIGHT_OUTPUT_LOG!

REM 遅延環境変数の利用終了
ENDLOCAL
EXIT /B 0
