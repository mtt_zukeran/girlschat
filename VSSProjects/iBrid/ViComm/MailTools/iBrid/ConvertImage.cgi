#!C:/Perl/bin/perl.exe

# /*************************************************************************
# --  System			: ViComm
# --  Sub System Name	: 
# --  Title				: 画像変換スクリプト
# --  Progaram ID		: ConvertImage.cgi
# --
# --  Creation Date		: 2010/08/18
# --  Creater			: K.Itoh
# --
# **************************************************************************/
# [ Update History ]
# /*------------------------------------------------------------------------
#   Date		Updater			Update Explain
#   yyyy/MM/dd	updater			[explain]
# 
# -------------------------------------------------------------------------*/
use strict;
use warnings;
use File::Copy;
use iBridLib::ImageUtil;
use iBridLib::Env;
use iBridLib::DbSession;

my $dbSession = new iBridLib::DbSession();

my $site_cd							= $ARGV[0];		# サイトコード
my $input_dir						= $ARGV[1];		# 入力ディレクトリ
my $output_dir						= $ARGV[2];		# 出力ディレクトリ
my $file_name_without_extension		= $ARGV[3];		# ファイル名(拡張子含まない)
my $input_file_name					= $ARGV[4];		# (省略可)入力ファイル名
if (defined($input_file_name)) {
	print "$input_file_name\r\n";
} else {
	print "\$input_file_name is null\r\n";
}

#if($iBridLib::Env::is_debug eq "1"){
	print "-- ConvertImage.cgi args ------ \r\n";
	foreach ( @ARGV ){
		print $_, "\n";	# 呼び出し時の引数をすべて出力
	}
	print "------------------------------- \r\n";
#}

# ==================
# サイト情報取得
# ==================
$dbSession->connect($iBridLib::Env::db_dsn,$iBridLib::Env::db_user,$iBridLib::Env::db_pwd) or die "CONNECT ERROR $DBI::errstr"; 
my $site_info 	= $dbSession->getSiteInfo($site_cd);
my $pic_back_color 			= $site_info->{"PIC_BACK_COLOR"}		|| "";
my $scale_type 				= $site_info->{"SCALE_TYPE"}			|| "";
my $large_img_size 			= $site_info->{"PIC_SIZE_LARGE"}		|| "";
my $small_img_size 			= $site_info->{"PIC_SIZE_SMALL"}		|| "";
my $stamp_file_nm 			= $site_info->{"STAMP_FILE_NM"}			|| "";
my $stamp_smple_file_nm 	= $site_info->{"STAMP_SAMPLE_FILE_NM"}	|| "";

print "-- [SiteInfo] -------------------------------\r\n";
print "ｽﾀﾝﾌﾟﾌｧｲﾙ名        : $stamp_file_nm\r\n";
print "ｻﾝﾌﾟﾙｽﾀﾝﾌﾟﾌｧｲﾙ名   : $stamp_smple_file_nm\r\n";
print "写真ｻｲｽﾞ大         : $large_img_size\r\n";
print "写真ｻｲｽﾞ小         : $small_img_size\r\n";
print "縮尺方式           : $scale_type\r\n";
print "---------------------------------------------\r\n";


# 出力先のディレクトリが存在しない場合は作成する
if (!-d "$output_dir"){
	mkdir ("$output_dir") || die "ディレクトリ生成失敗 : $!";
}
my $stamp_file_path			= "$iBridLib::Env::mail_tools_dir$stamp_file_nm";			# スタンプファイルのパス(png)
my $stamp_sample_file_path	= "$iBridLib::Env::mail_tools_dir$stamp_smple_file_nm";		# サンプル用スタンプファイルのパス(png)
my $is_square 				= ($scale_type == 3)? 1 : 0 ;								#画像を正方形にするかどうかを示すフラグ。1:正方形

my $original_file_name		= $file_name_without_extension . "_o.jpg";		# オリジナル画像ファイル名
my $file_name				= $file_name_without_extension . ".jpg";		# 画像大ファイル名
my $blur_file_name			= $file_name_without_extension . "_m.jpg";		# 画像大ぼかしファイル名
my $small_file_name			= $file_name_without_extension . "_s.jpg";		# 画像小ファイル名
my $small_blur_file_name	= $file_name_without_extension . "_s_m.jpg";	# 画像小ぼかしファイル名

my $input_file_path 		= defined($input_file_name) ? "$input_dir$input_file_name" : "$input_dir$file_name";						# オリジナルファイルパス
my $original_file_path		= "$output_dir\\$original_file_name";			# オリジナル画像ファパス
my $large_file_path			= "$output_dir\\$file_name";					# 画像大ファイルパス
my $blur_file_path			= "$output_dir\\$blur_file_name";				# 画像大ぼかしファイルパス
my $small_file_path			= "$output_dir\\$small_file_name";				# 画像小ファイルパス
my $small_blur_file_path	= "$output_dir\\$small_blur_file_name";			# 画像小ぼかしファイルパス



if( $scale_type == 2 ){
	# 倍率
	($large_img_size,$small_img_size) = &iBridLib::ImageUtil::ConvertRatio2Width($input_file_path,$large_img_size/100,$small_img_size/100);
}

if($iBridLib::Env::is_debug eq "1"){
	print "[画像サイズ]\r\n";
	print "large_img_size : $large_img_size\r\n";
	print "small_img_size : $small_img_size\r\n";
}

##############################
# 各種画像作成
##############################

#画像の向き補正
&iBridLib::ImageUtil::CorrectRotationImage	($input_file_path);
#大画像
&iBridLib::ImageUtil::ResizeImage	($input_file_path, $large_file_path, $large_img_size, $is_square);
&iBridLib::ImageUtil::StampImage	($large_file_path, $stamp_file_path );
#大ぼかし
&iBridLib::ImageUtil::BlurImage		($large_file_path, $blur_file_path, $large_img_size, $is_square);
#小画像
&iBridLib::ImageUtil::ResizeImage	($large_file_path, $small_file_path, $small_img_size, $is_square);
#小ぼかし
&iBridLib::ImageUtil::BlurImage		($small_file_path, $small_blur_file_path, $small_img_size, $is_square);

if($is_square==1) {
	&iBridLib::ImageUtil::FillBlankSpace	($large_file_path, $large_img_size, $pic_back_color);
}
&iBridLib::ImageUtil::CommentKddiCopyright	($large_file_path );

if($is_square==1) {
	&iBridLib::ImageUtil::FillBlankSpace	($blur_file_path,$large_img_size, $pic_back_color);
}
&iBridLib::ImageUtil::Superimposes			($blur_file_path,$stamp_sample_file_path);
&iBridLib::ImageUtil::CommentKddiCopyright	($blur_file_path );

if($is_square==1) {
	&iBridLib::ImageUtil::FillBlankSpace	($small_file_path, $small_img_size, $pic_back_color);
}
&iBridLib::ImageUtil::CommentKddiCopyright	($small_file_path );

if($is_square==1) {
	&iBridLib::ImageUtil::FillBlankSpace	($small_blur_file_path, $small_img_size, $pic_back_color);
}
&iBridLib::ImageUtil::Superimposes			($small_blur_file_path,$stamp_sample_file_path);
&iBridLib::ImageUtil::CommentKddiCopyright	($small_blur_file_path );

copy($input_file_path, $original_file_path);
&iBridLib::ImageUtil::CopyExif($input_file_path,$large_file_path		);
&iBridLib::ImageUtil::CopyExif($input_file_path,$small_file_path		);
&iBridLib::ImageUtil::CopyExif($input_file_path,$blur_file_path			);
&iBridLib::ImageUtil::CopyExif($input_file_path,$small_blur_file_path	);



# ﾐﾗｰﾘﾝｸﾞ
system("$ENV{'CURRENT_DIR'}Mirror.bat","$output_dir\\$file_name_without_extension*");


#if(!defined($input_file_name)){
#	unlink($input_file_path);
#}
