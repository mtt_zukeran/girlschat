REM 環境変数の読み込み
call %CURRENT_DIR%_env.bat

if "%SLAVE_HOST%"=="" exit /B 0;

rem UNC指定のサイトのみ対象
if "%~d1"=="\\" (
	for /f "tokens=2* delims=\" %%a in ("%~dp1") do ( 
		REM 共有ディスクにアクセスできるようにする
		call %CURRENT_DIR%_netuse.bat "%SLAVE_HOST%\%%a\%%b" "vicomm" "vicomm"
		
		if not exist "%SLAVE_HOST%\%%a\%%b" (
			mkdir "%SLAVE_HOST%\%%a\%%b"
		)
		call copy /Y "%~1" "%SLAVE_HOST%\%%a\%%b"
	)
)
exit /B 0;


