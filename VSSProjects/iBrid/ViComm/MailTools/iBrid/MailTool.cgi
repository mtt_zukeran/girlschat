#!C:/Perl/bin/perl.exe

use MIME::Parser;
use MIME::WordDecoder;
use MIME::Words qw(decode_mimewords);
use Image::Magick;
use Jcode;
use DBI;
use File::Copy;
use Net::POP3;
use Time::Piece;
use iBridLib::Env;
use iBridLib::DbSession;
use iBridLib::ViCommConst;

use constant POP_TIMEOUT => 600;

my $t;
my $existMail;
my $interval	= 3;
my $parser 		= new MIME::Parser;
my $dbSession 	= new iBridLib::DbSession();
my $prev_exec_time = 0;
my $curr_exec_time = 0;
my $mail_file_dir 	= '.\\mail_file';

my $logFileName;

while (true){
	$curr_exec_time = time;

	#-----------------------------------------------------------#
	# Vicomm Mail Parse											#
	#-----------------------------------------------------------#
	eval{
		$parser->output_dir($mail_file_dir);
		$pop = Net::POP3->new($iBridLib::Env::pop_host, Timeout=>POP_TIMEOUT) or die "Can't not open account.";
		$pop->login($iBridLib::Env::pop_user,$iBridLib::Env::pop_pwd);
		$existMail = 0;
        $parser->filer->ignore_filename(1);
		$mailbox = $pop->list();

		foreach $msgid (keys %$mailbox) {

			if ($existMail == 0){
				$dbSession->connect($iBridLib::Env::db_dsn,$iBridLib::Env::db_user,$iBridLib::Env::db_pwd) or die "CONNECT ERROR $DBI::errstr";
				$db =$dbSession->getConnection();

				$cmd_rx_mail = $db->prepare(q/
					BEGIN
						MAIL_RX(:pFROM_ADDR,:pTO_ADDR,:pSTATUS);
					END;
				/);
				$existMail = 1;
			}

			$t = localtime;
			$logFileName = ".\\logs\\mailtool_".$t->ymd('').".log";
			open(LOGFILE, ">>", $logFileName);

			eval{
				$entity = $parser->parse_data($pop->get($msgid));

				my ($mail_body, $filename);

				# HEAD
				my $mail_from = $entity->head->get('From');
				my $mail_date = $entity->head->get('Date');
				my $mail_to   = $entity->head->get('To');
				my $subject   = $entity->head->get('Subject');

				my $status;
				my $pass;

				if($mail_to 	=~ /(?:.*<(.*@.*)>.*)/) {
					$mail_to 	= $1;
				}
				if($mail_from 	=~ /(?:.*<(.*@.*)>.*)/){
					$mail_from 	= $1;
				}

				# ダブルクォートを除外
				if($mail_from 	=~ /\"(.*)\"(@)(.*)/) {
					$mail_from = "$1$2$3";
				}

				$mail_from = lc($mail_from);
				$mail_to =~ s/[<]//g;
				$mail_to =~ s/[>]//g;

				print LOGFILE "from   :$mail_from\n";
				print LOGFILE "to     :$mail_to\n";
				print LOGFILE "date   :$mail_date\n";

				$mail_from =~ s/\r\n//;
				$mail_from =~ s/\r//;
				$mail_from =~ s/\n//;

				$mail_to =~ s/\r\n//;
				$mail_to =~ s/\r//;
				$mail_to =~ s/\n//;


				$mail_subject = "";
				if ($subject eq ""){
					print LOGFILE "no subject\n"
				}else{
					chomp($subject);
					@decode = decode_mimewords( $subject );
					$text = "";
					foreach $subject ( @decode ){
						($target,$charset) = @$subject;
						print LOGFILE "charset=$charset<br>\n";
						Jcode::convert(\$target, 'sjis', 'jis');
						$target =~ s/\n//g;
						$mail_subject .= $target
					}
				}

				$t = localtime;
				print LOGFILE $t->ymd('') ," " , $t->hms('') ,"\n";

				#送信先メールアドレスより頭4文字取り出し
				$mail_to_head4 = substr($mail_to,0,4);
				$mail_to_head6 = substr($mail_to,0,6);
				$mail_to_head7 = substr($mail_to,0,7);
				#頭4文字がRX_MAIL内の対象(regm,regp,modm,fogm,catm,catf,utnm,inquiry)であったら添付メールとしない


				my $is_target = $entity->is_multipart || ($entity->mime_type eq "image/jpeg") || ($entity->mime_type eq "video/3gpp");

				if ($is_target
					and (
						($mail_to_head4 eq 'regm')
						|| ($mail_to_head4 eq 'regp')
						|| ($mail_to_head4 eq 'modm')
						|| ($mail_to_head4 eq 'fogm')
						|| ($mail_to_head4 eq 'catm')
						|| ($mail_to_head4 eq 'catf')
						|| ($mail_to_head4 eq 'utnm')
						|| ($mail_to_head7 eq 'inquiry')
					) == 0
				){

RECURSION:
					my $sub_entity = '';
					my $count = $entity->parts;

					if($entity->is_multipart){
						print LOGFILE "multi  :Yes\n";
					}else{
						print LOGFILE "multi  :No\n";
						$count = 1;
					}


					for (my $i = 0; $i < $count; $i++) {
						#q_text or binary
						my $target_entity;

						if($entity->is_multipart){
							$target_entity = $entity->parts($i);
						}else{
							print LOGFILE "multi  :No\n";
							$target_entity = $entity;
						}

						my ($type, $subtype) = split('/', $target_entity->head->mime_type);
						print LOGFILE "type   :","$type\n";
						print LOGFILE "subtype:","$subtype\n";

						if ($type =~ /^(text|message)$/) {
							# text
							$mail_body .= $target_entity->bodyhandle->as_string;
							unlink($target_entity->bodyhandle->path);

							Jcode::convert(\$mail_body, 'sjis', 'jis');

							#print "subject;" ,$mail_subject,"\n";
							#print "body   :" ,$mail_body,"\n";

							if ($i eq 0){
								$cmd_rx_mail->bind_param(':pFROM_ADDR'	, $mail_from);
								$cmd_rx_mail->bind_param(':pTO_ADDR'	, $mail_to);
								$cmd_rx_mail->bind_param_inout(':pSTATUS', \$status	, 10);
								$cmd_rx_mail->execute;
								print LOGFILE "mail_rx output status :","$status\n";
							}

						} else {
							if($target_entity->is_multipart){
								print LOGFILE "parts  : $i (Nested)\n";
								#if ($mail_from =~ /.*\@docomo.ne.jp$/) {
									$sub_entity = $target_entity;
								#}
							}else{

								my ($obj_type, $out_dir, $out_thumbnail_dir, $file_nm, $movie_seq, $ftp_file_nm, $site_cd, $status)
									= $dbSession->attachedFileUpload($mail_from,$mail_to,$mail_subject,$mail_body,$type);

								print LOGFILE "objtype     	      :","$obj_type\n";
								print LOGFILE "status      	      :","$status\n";
								print LOGFILE "output      	      :","$out_dir/$file_nm\n";
								print LOGFILE "out_thumbnail_dir :","$out_thumbnail_dir\n";
								print LOGFILE "ftp_file_nm       :$ftp_file_nm\n";
								print LOGFILE "site_cd           :$site_cd\n";
								print LOGFILE "\n\n";

								if ($status eq 'I000' and $file_nm ne ''){
									if ($obj_type eq $iBridLib::ViCommConst::ATTACHED_MAIL_PIC
										or $obj_type eq $iBridLib::ViCommConst::ATTACHED_MAIL_DIARY
										or $obj_type eq $iBridLib::ViCommConst::ATTACHED_MAIL_TWEET
										or $obj_type eq $iBridLib::ViCommConst::ATTACHED_MAIL_JYANKEN
										or $obj_type eq $iBridLib::ViCommConst::ATTACHED_MAIL_YAKYUKEN
									){
										$filename= $file_nm . '.jpg';

										# メール添付ファイルをMailToolsフォルダへ移動
										print LOGFILE "move file : ", $target_entity->bodyhandle->path, " to $iBridLib::Env::mail_tools_dir/$filename", "\n";
										move($target_entity->bodyhandle->path,"$iBridLib::Env::mail_tools_dir/$filename");

										# 画像変換処理
										my $cnv_ret = system(
											"ConvertImage.bat"
											,"$site_cd"
											,"$iBridLib::Env::mail_tools_dir"
											,"$out_dir"
											,"$file_nm"
										);

										# 画像をFTPでアップロードする
										if ($ftp_file_nm ne ''){
											print LOGFILE "ftp upload $iBridLib::Env::ftp_host\n $iBridLib::Env::ftp_user\n $iBridLib::Env::ftp_pwd\n put $out_dir/$filename $ftp_file_nm\n";
											open(OUTFILE, '>' . $iBridLib::Env::mail_tools_dir . 'FTPCMD.txt') or die "file open error: $!";
											print OUTFILE "open $iBridLib::Env::ftp_host\n";
											print OUTFILE "$iBridLib::Env::ftp_user\n";
											print OUTFILE "$iBridLib::Env::ftp_pwd\n";
											print OUTFILE "put $out_dir/$filename $ftp_file_nm\n";
											print OUTFILE "quit\n";
											close (OUTFILE);

											my $cnv_ret = system("ftp -s:" . $iBridLib::Env::mail_tools_dir . "FTPCMD.txt");
										}

										# 処理した画像ファイルを削除
										unlink("$iBridLib::Env::mail_tools_dir/$filename");
										unlink($target_entity->bodyhandle->path);
									}

									if ($obj_type eq $iBridLib::ViCommConst::ATTACHED_MAIL_MOVIE){
										my @movile_file_type_arr = ();
										my @file_num_arr = ();

										# 変換処理
										$filename= $file_nm . '.3gp';
										print LOGFILE "move file : ", $target_entity->bodyhandle->path, " to $iBridLib::Env::mail_tools_dir/$filename", "\n";
										move($target_entity->bodyhandle->path,"$iBridLib::Env::mail_tools_dir/$filename");
										my $cnv_ret = system(
											"Convert.bat"
											,"$iBridLib::Env::mail_tools_dir"
											,"$filename"
											,"$out_dir"
											,"$file_nm"
											,"0"
											,"0"
											,"$out_thumbnail_dir"
											,"$site_cd"
										);

										open(IN_RESULT, "$iBridLib::Env::mail_tools_dir/$file_nm".".result") || die("error :$!");
										# 結果ファイルを解析(動画種別,ファイル件数)
										while(my $line = <IN_RESULT>){
											# 1行を4つに分ける
											chomp($line);
											my ($movile_file_type, $file_num) = split(/,/, $line, 2);

											$file_num =~ s/^\s*(.*?)\s*$/$1/;
											push @movile_file_type_arr, $movile_file_type ;
											push @file_num_arr, $file_num ;
										}
										close(IN_RESULT);

										unlink "$iBridLib::Env::mail_tools_dir/$file_nm".".result";

										#$dbSession->attachedFileCountSet($movie_seq,@file_num_arr);

										print LOGFILE "convert result :","$cnv_ret\n";
									}
								}

								$sub_entity = '';
								break;
							}
						}
					}

					if($sub_entity){
						$entity = $sub_entity;
						$sub_entity = '';
						goto RECURSION;
					}
				}else{
					if (  ($mail_to_head6 eq 'PHOTO-') || ($mail_to_head6 eq 'DIARY-') || ($mail_to_head6 eq 'MOVIE-')){
						print LOGFILE "multi  :No (not exist attached file)\n";
						my ($obj_type, $out_dir, $out_thumbnail_dir, $file_nm, $movie_seq, $ftp_file_nm, $site_cd, $status)
							= $dbSession->attachedFileUpload($mail_from,$mail_to,$mail_subject,"NONE","NONE");
						print LOGFILE "status :","$status\n";
					}else{
						print LOGFILE "multi  :No\n";
						$cmd_rx_mail->bind_param(':pFROM_ADDR'		, $mail_from);
						$cmd_rx_mail->bind_param(':pTO_ADDR'		, $mail_to	);
						$cmd_rx_mail->bind_param_inout(':pSTATUS'	, \$status	, 10);
						$cmd_rx_mail->execute;
						print LOGFILE "status :","$status\n";
					}
				}

			};

			$pop->delete($msgid) or die "delete error. $!";

		}

		$pop->quit or die "quit error. $!";

		if( $curr_exec_time-$prev_exec_time > $iBridLib::Env::errmail_interval){

			if($iBridLib::Env::errmail_type eq 'postfix'){
				system("start perl ErrMailToolPostfix.cgi &");
			}else{
				system("start perl ErrMailTool.cgi &");
			}

			$prev_exec_time = $curr_exec_time;
		}

	};

	# ファイル削除
	opendir(DIR, $mail_file_dir) or die;
	my @file =  grep { /^.*/i  && -f "$mail_file_dir/$_"} readdir(DIR);
	closedir(DIR);
	foreach(@file) {
		unlink "$mail_file_dir/$_";
	}


	if ($existMail){$dbSession->disconnect();}
	sleep $interval;

	if( $@ ){print $@;}
}

$dbSession->disconnect;

