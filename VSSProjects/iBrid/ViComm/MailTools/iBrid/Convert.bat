REM SETが効かなかったので遅延環境変数を利用可能にする
SETLOCAL ENABLEDELAYEDEXPANSION

SET CURRENT_DIR=%~d0%~p0
CD %CURRENT_DIR%

IF "!STR_DATE!"=="" (
	SET STR_DATE=%date:~0,4%%date:~5,2%%date:~8,2%
	SET STR_TIME=%time:~3,2%%time:~6,2%
)

REM 出力するログファイル名 定義
IF "!CMOVIE_OUTPUT_LOG!"=="" (
	SET CMOVIE_TEST_LOG=%CURRENT_DIR%logs\cmovie_test_stdout_!STR_DATE!.log
	SET CMOVIE_OUTPUT_LOG=%CURRENT_DIR%logs\cmovie_stdout_!STR_DATE!.log
	SET CMOVIE_ERROR_LOG=%CURRENT_DIR%logs\cmovie_stderr_!STR_DATE!.log
	SET CMOVIE_RETRY_BAT=%CURRENT_DIR%retry_files\movie_%~4_!STR_DATE!.bat
)


REM 起動バッチ判定用変数 定義
IF "!EXEC_BAT!"=="" (
	SET EXEC_BAT=CONVERT_MOVIE
)

REM 初回実行時:リトライ用に動画ファイルをコピー
IF "%~9"=="" (
	COPY /B /Y %~1\%~2 %CURRENT_DIR%retry_files\

REM リトライ実行時:変換処理用に動画ファイルをコピー
) ELSE (
	COPY /B /Y %CURRENT_DIR%retry_files\%~2 %~1\
)

REM 動画変換処理の実施
SET TEMP_MOVIE_ERROR_LOG=%CURRENT_DIR%logs\movie_%~4_!STR_DATE!_!STR_TIME!.log
ECHO [%DATE% %TIME%]>>!CMOVIE_OUTPUT_LOG!
CALL %CURRENT_DIR%Convert.impl.bat %1 %2 %3 %4 %5 %6 %7 %8 1>>!CMOVIE_OUTPUT_LOG! 2>>!TEMP_MOVIE_ERROR_LOG!
SET CONVERT_MOVIE_ERROR_LEVEL=%ERRORLEVEL%

REM 動画変換でエラーがあればログに出力
FOR %%i IN (!TEMP_MOVIE_ERROR_LOG!) DO IF NOT %%~zi==0 (
	ECHO [%DATE% %TIME%]>>!CMOVIE_ERROR_LOG!
	TYPE !TEMP_MOVIE_ERROR_LOG!>>!CMOVIE_ERROR_LOG!
)
IF EXIST !TEMP_MOVIE_ERROR_LOG! (
	DEL /Q !TEMP_MOVIE_ERROR_LOG!
)

REM 変換処理用の動画ファイルを削除 (変換に成功していれば削除済み)
IF EXIST %~1\%~2 (
	DEL /Q %~1\%~2
)

REM 動画変換 正常終了
IF "!CONVERT_MOVIE_ERROR_LEVEL!"=="0" IF EXIST %~3\%~4.3gp (
	REM リトライ用に保存した動画を削除
	DEL /Q %CURRENT_DIR%retry_files\%~2
	GOTO END
)

REM 画像変換がメインでない場合はここで終了
IF NOT "!EXEC_BAT!"=="CONVERT_MOVIE" (
	DEL /Q %CURRENT_DIR%retry_files\%~2
	GOTO END
)

REM リトライ用バッチファイルを生成
IF "%~9"=="" (
	ECHO CALL %CURRENT_DIR%Convert.bat %~1 %~2 %~3 %~4 %~5 %~6 %~7 %~8 retry_files\ >!CMOVIE_RETRY_BAT!
	ECHO DEL /Q %~1\%~4*>>!CMOVIE_RETRY_BAT!
	ECHO DEL /Q !CMOVIE_RETRY_BAT!>>!CMOVIE_RETRY_BAT!

REM リトライ時にここまできたら変換処理に失敗しているため強制終了
REM (ここを素通りするとリトライバッチが削除されるため)
) ELSE (
	EXIT 1
)

GOTO END

REM ********************************************************
REM -- :END
REM -- 処理終了
REM ********************************************************
:END

REM 遅延環境変数の利用終了
ENDLOCAL
EXIT /B 0
