#!C:/Perl/bin/perl.exe

use MIME::Parser;
use MIME::WordDecoder;
use MIME::Words qw(decode_mimewords);
use Image::Magick;
use Jcode;
use DBI;
use File::Copy;
use File::Path;
use Net::POP3;
use Time::Piece;
use HTTP::Date;
use iBridLib::Env;
use iBridLib::DbSession;
use iBridLib::ViCommConst;


my $existMail;
my $parser 				= new MIME::Parser;
my $dbSession 			= new iBridLib::DbSession();
my $err_mail_file_dir 	= '.\\err_mail_file';

print "[ErrMailTool]\t".localtime(time)."\tSTART\n";
eval{
	#-----------------------------------------------------------#
	# Error Mail Parse											#
	#-----------------------------------------------------------#
	$parser->output_dir($err_mail_file_dir);
	
	$pop = Net::POP3->new($iBridLib::Env::pop_host_for_err) or die "Can't not open account.";
	$pop->login($iBridLib::Env::pop_user_for_err,$iBridLib::Env::pop_pwd_for_err) or die 'POP authentication error';
	$existMail = 0;

	$mailbox = $pop->list();

	foreach $msgid (keys %$mailbox) {

		if ($existMail == 0){
			$dbSession->connect($iBridLib::Env::db_dsn,$iBridLib::Env::db_user,$iBridLib::Env::db_pwd) or die "CONNECT ERROR $DBI::errstr"; 
			$db =$dbSession->getConnection();
			$cmd_error_mail = $db->prepare(q/
				BEGIN
					RX_ERROR_MAIL(:pMAIL_HOST,:pNON_EXIST_MAIL_ADDR,:pRX_DATE,:pSTATUS,:pMAIL_LOCAL);
				END;
			/);
			$existMail = 1;
		}

		eval{
			$entity = $parser->parse_data($pop->get($msgid));

			# HEAD
			my $site			= '';
			my $mail_local		= '';
			my $mail_to   		= $entity->head->get('To');
			my $mail_rx_date	= '';
			my $mail_body .= $entity->bodyhandle->as_string;

			my $mail_head	= $entity->head->get('Received');
			$mail_head =~ /;(.*)$/ms;

			my $mail_rx_date = substr(HTTP::Date::parse_date($1), 0, 19);
#			print "mail_rx_date:" ,$mail_rx_date,"\n";

			$mail_to =~ /<(.*)@(.*)>/;

			$mail_local	= $1;			
			$site	 	= $2;
			
			my $addr 	= "";
			
			#KDDI
			if($mail_body =~ /<(.*@ezweb.ne.jp)>:/){
				$addr = $1;
			}else{
				if($mail_body =~ /Remote host said: (550) <(.*@[^>\n\r]*)>: User unknown/){
					$addr = $2;
				}else{
					if($mail_body =~ /Remote host said: (550) (Unknown user|[^<]*)\s*<?(.*@[^>\n\r]*)/){
						$addr = $3;
					} 
				}
			}

			if ($addr eq ""){
					print "body:" ,$site,"  ",$mail_body,"\n";
			}else{
				$addr = lc($addr);
				$cmd_error_mail->bind_param(':pMAIL_HOST'			, $site			);
				$cmd_error_mail->bind_param(':pNON_EXIST_MAIL_ADDR'	, $addr			);
				$cmd_error_mail->bind_param(':pRX_DATE'				, $mail_rx_date );
				$cmd_error_mail->bind_param_inout(':pSTATUS', \$status	, 10);
				$cmd_error_mail->bind_param(':pMAIL_LOCAL'			, $mail_local	);
				$cmd_error_mail->execute;
				print "addr:" ,$site,"  ",$addr,"  ",$mail_local,"  " ,$status,"\n";
			}

		};
		$pop->delete($msgid);
				
	}
	$pop->quit;
	
};

# ファイル削除
opendir(DIR, $err_mail_file_dir) or die;
my @file =  grep { /^.*/i  && -f "$err_mail_file_dir/$_"} readdir(DIR);
closedir(DIR);
foreach(@file) {
	unlink "$err_mail_file_dir/$_";
}

if ($existMail){$dbSession->disconnect();}
if( $@ ){print $@;}

print "[ErrMailTool]\t".localtime(time)."\tEND\n";

