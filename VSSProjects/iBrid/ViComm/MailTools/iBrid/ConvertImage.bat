@ECHO OFF
REM SETが効かなかったので遅延環境変数を利用可能にする
SETLOCAL ENABLEDELAYEDEXPANSION

SET CURRENT_DIR=%~d0%~p0
CD %CURRENT_DIR%

IF "!STR_DATE!"=="" (
	SET STR_DATE=%date:~0,4%%date:~5,2%%date:~8,2%
	SET STR_TIME=%time:~3,2%%time:~6,2%
)

REM 出力するログファイル名 定義
IF "!CIMAGE_OUTPUT_LOG!"=="" (
	SET CIMAGE_TEST_LOG=%CURRENT_DIR%logs\cimage_test_stdout_!STR_DATE!.log
	SET CIMAGE_OUTPUT_LOG=%CURRENT_DIR%logs\cimage_stdout_!STR_DATE!.log
	SET CIMAGE_ERROR_LOG=%CURRENT_DIR%logs\cimage_stderr_!STR_DATE!.log
	SET CIMAGE_RETRY_BAT=%CURRENT_DIR%retry_files\image_%~4_!STR_DATE!.bat
)
SET IMAGE_DIFF_BAT=%CURRENT_DIR%logs\%~4_diff.bat

REM 環境変数の読み込み
CALL %CURRENT_DIR%_env.bat

REM 共有ディスクにアクセスできるようにする
CALL %CURRENT_DIR%_netuse.bat "%~3" "vicomm" "vicomm"

REM 起動バッチ判定用変数 定義
IF "!EXEC_BAT!"=="" (
	SET EXEC_BAT=CONVERT_IMAGE
)

REM リトライ判定フラグ
SET /A RETRY_FLAG=0
IF NOT "%~6"=="" (
	SET /A RETRY_FLAG=1
)

REM 代替ファイル名、リトライ判定用
REM (ファイル名が入っていない場合、リトライ時にretry_files\が5番目の引数に入る)
ECHO "%~5" | FIND "retry_files">NUL
SET FIND_ERROR_LEVEL=%ERRORLEVEL%

REM 代替ファイル名の取得 (初期値=空文字列)
SET P5=%~7

REM 代替ファイル名が入っている場合は設定、リトライ時はリトライフラグ=ON
IF NOT "%~5"=="" (
	IF "!FIND_ERROR_LEVEL!"=="1" (
		SET P5=%~5
	) ELSE (
		SET /A RETRY_FLAG=1
	)
)

REM 入出力画像ファイル名設定
SET INPUT_IMAGE_NAME=!P5!.jpg
SET INPUT_IMAGE_PATH=%~2!P5!.jpg
SET OUTPUT_IMAGE_PATH=%~3\!P5!_o.jpg
IF "!P5!"=="" (
	SET INPUT_IMAGE_NAME=%~4.jpg
	SET INPUT_IMAGE_PATH=%~2%~4.jpg
	SET OUTPUT_IMAGE_PATH=%~3\%~4_o.jpg
)

REM 初回実行時:リトライ用に画像ファイルをコピー
IF "!RETRY_FLAG!"=="0" (
	COPY /B /Y !INPUT_IMAGE_PATH! %CURRENT_DIR%retry_files\

REM リトライ実行時:変換処理用に画像ファイルをコピー
) ELSE (
	COPY /B /Y %CURRENT_DIR%retry_files\!INPUT_IMAGE_NAME! !INPUT_IMAGE_PATH!
)

REM 画像変換処理の実施
SET TEMP_IMAGE_ERROR_LOG=%CURRENT_DIR%logs\image_%~4_!STR_DATE!_!STR_TIME!.log
ECHO [%DATE% %TIME%]>>!CIMAGE_OUTPUT_LOG!
Perl -I%CURRENT_DIR% %CURRENT_DIR%convertimage.cgi %~1 %~2 %~3 %~4 !P5! 1>>!CIMAGE_OUTPUT_LOG! 2>>!TEMP_IMAGE_ERROR_LOG!
SET CONVERT_IMAGE_ERROR_LEVEL=%ERRORLEVEL%

REM 画像変換でエラーがあればログに出力
FOR %%i IN (!TEMP_IMAGE_ERROR_LOG!) DO IF NOT %%~zi==0 (
	ECHO [%DATE% %TIME%]>>!CIMAGE_ERROR_LOG!
	TYPE !TEMP_IMAGE_ERROR_LOG!>>!CIMAGE_ERROR_LOG!
)
IF EXIST !TEMP_IMAGE_ERROR_LOG! (
	DEL /Q !TEMP_IMAGE_ERROR_LOG!
)

REM 変換処理用の画像ファイルを削除
DEL /Q !INPUT_IMAGE_PATH!

REM 画像変換 正常終了
IF "!CONVERT_IMAGE_ERROR_LEVEL!"=="0" (
	REM リトライ用に保存した画像を削除
	DEL /Q %CURRENT_DIR%retry_files\!INPUT_IMAGE_NAME!
	GOTO END
)

REM 画像変換がメインでない場合はここで終了
IF NOT "!EXEC_BAT!"=="CONVERT_IMAGE" (
	GOTO END
)

REM 画像比較用のバッチファイル生成
ECHO @ECHO OFF>!INPUT_IMAGE_PATH!>!IMAGE_DIFF_BAT!
ECHO FC /B !OUTPUT_IMAGE_PATH! !INPUT_IMAGE_PATH!>>!IMAGE_DIFF_BAT!

REM 画像比較
SET /A RESVAL=9
CALL :COMPARE_IMAGE !IMAGE_DIFF_BAT!

REM 画像比較用バッチファイル削除
IF EXIST !IMAGE_DIFF_BAT! (
	DEL /Q !IMAGE_DIFF_BAT!
)

REM 画像ファイル一致 (画像変換処理済
IF "!RESVAL!"=="1" (
	ECHO 画像ファイルが一致したため変換処理を終了します>>!CIMAGE_OUTPUT_LOG!
	DEL /Q %CURRENT_DIR%retry_files\!INPUT_IMAGE_NAME!
	GOTO END
)

REM リトライ用バッチファイルを生成
IF "!RETRY_FLAG!"=="0" (
	ECHO CALL %CURRENT_DIR%ConvertImage.bat %~1 %~2 %~3 %~4 !P5! retry_files\>!CIMAGE_RETRY_BAT!
	ECHO DEL /Q !CIMAGE_RETRY_BAT!>>!CIMAGE_RETRY_BAT!

REM リトライ時にここまできたら変換処理に失敗しているため強制終了
REM (ここを素通りするとリトライバッチが削除されるため)
) ELSE (
	EXIT 1
)

GOTO END

REM ********************************************************
REM -- :COMPARE_IMAGE
REM -- 画像を比較する
REM ********************************************************
:COMPARE_IMAGE
FOR /F "delims=: tokens=2" %%i IN (%~1) DO (
	REM 画像ファイル一致
	IF "%%i"=="相違点は検出されませんでした" (
		SET /A RESVAL=1
		EXIT /B
	)
)
SET /A RESVAL=0
EXIT /B

REM ********************************************************
REM -- :END
REM -- 処理終了
REM ********************************************************
:END

REM 遅延環境変数の利用終了
ENDLOCAL
EXIT /B 0
