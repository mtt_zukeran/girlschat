@echo off

ECHO %~1
ECHO %~2
ECHO %~3
ECHO %~4
ECHO %~5

rem -- UNCでアクセスできるように認証する
call :NET_USE %~1 vicomm vicomm > NUL

Perl %~d0%~p0CopyRight.cgi %~1 %~2 %~3 %~4 > NUL

goto END:


REM ********************************************************
REM -- :NET_USE
REM -- UNCパスの場合、共有リソースにアクセスできるようにする
REM -- 　%1:パス
REM -- 　%2:ユーザー
REM -- 　%3:パスワード
REM ********************************************************
:NET_USE
rem UNCパスじゃない場合は処理しない
if not "%~d1"=="\\" exit /B 0

for /f "tokens=1* delims=\" %%a in ("%~1") do ( 
  net use \\%%a\IPC$ %~3 /user:%~2
)
exit /B 0





:END
exit 0
