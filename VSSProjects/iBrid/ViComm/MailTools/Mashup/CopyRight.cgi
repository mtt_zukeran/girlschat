#!C:/Perl/bin/perl.exe

use Image::Magick;
use Time::Piece;

$dir	= $ARGV[0];
$file_nm= $ARGV[1];
$domain	= $ARGV[2];
$img_dir= $ARGV[3];

print "dir:","$dir\n";
print "file_nm :","$file_nm\n";
print "domain :","$domain\n";
print "img_dir :","$img_dir\n";

my $image = Image::Magick->new(magick=>'jpg');
$image->Read("$dir/$file_nm");

# Copy Right
$image->CommentImage('kddi_copyright=on,copy="NO"');

#コピーライト画像変数宣言
my $img2= Image::Magick->new;

if ($domain eq 'maqia.jp'){
	$img2->Read("$img_dir/maqia.png");
}
else{
	$img2->Read("$img_dir/marii.png");
}

#コピーライト画像合成
$image->Composite(image=>$img2, compose=>'overlay', gravity=>'SouthEast');

$image->Write("$dir/$file_nm");
