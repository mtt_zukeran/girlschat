@echo off

REM /*************************************************************************
REM --	System			: ViComm
REM --	Sub System Name	: 
REM --	Title			: 動画変換バッチ
REM --	Progaram ID		: Convert.impl.bat
REM --
REM --  Creation Date	: 
REM --  Creater			: i-Brid
REM --
REM **************************************************************************/
REM [ Update History ]
REM /*------------------------------------------------------------------------
REM 
REM   Date        Updater    Update Explain
REM   2010/06/02  伊藤和明   各キャリアにあわせた動画サイズ分割の機能を追加
REM                          マルチスレッド対応
REM 
REM -------------------------------------------------------------------------*/
                                                                                 
set T=%TIME:/=%                                                                 
set work_dir=%~1
set tvcc_dir="C:\Program Files\E.M. TVCC"
set mp4box_dir="D:\i-Brid\MP4BOX"
set output_path=%~3
set temp_file_prefix=temp_%~4_
set result_file=%~d0%~p0%~4.result

echo p1=%1
echo p2=%2
echo p3=%3
echo p4=%4
echo p5=%5

rem -- UNCの場合はアクセスできるように認証を済ませておく
call :NET_USE %output_path% vicomm vicomm

SET mov_type_array=qcif
if "%5" == "1" (
  SET mov_type_array=qcif qvga
)


for %%f in (%mov_type_array%)do (
  rem -- 変換処理(TVCC) 動画のファイルフォーマット等を変換する
  %tvcc_dir%\TVCC.exe -f %work_dir%\%~2 -o %work_dir%\%temp_file_prefix%%%f.3gp -pi cnv.ini -pn "ibrid-3gp-%%f" 
  %tvcc_dir%\TVCC.exe -f %work_dir%\%~2 -o %work_dir%\%temp_file_prefix%%%f.3g2 -pi cnv.ini -pn "ibrid-3g2-%%f" 

  rem -- docomoのストリーミング用にATOM構造を組み替える(MP4BOX)
  %mp4box_dir%\mp4box.exe -add %work_dir%\%temp_file_prefix%%%f.3gp -brand mmp4:1 -new %work_dir%\%temp_file_prefix%%%f.3gp -quiet
)

rem -- 動画を指定サイズに分割
rem for docomo
call :SPLIT_FILE 1990 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_d_s.3gp 12
if "%5" == "1" (
    call :SPLIT_FILE 490 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_d_d.3gp 11
    call :SPLIT_FILE 490 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_d_d.3gp 13
    call :SPLIT_FILE 1990 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_d_s.3gp 14
)
rem for softbank
call :SPLIT_FILE 290 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_s_d.3gp 21
if "%5" == "1" (
    call :SPLIT_FILE 1490 %work_dir%\%temp_file_prefix%qcif.3gp %work_dir%\%temp_file_prefix%qcif_s_s.3gp 22
    call :SPLIT_FILE 1490 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_s_s.3gp 24
    call :SPLIT_FILE 290 %work_dir%\%temp_file_prefix%qvga.3gp %work_dir%\%temp_file_prefix%qvga_s_d.3gp 23
)
rem for au
call :SPLIT_FILE 1490 %work_dir%\%temp_file_prefix%qcif.3g2 %work_dir%\%temp_file_prefix%qcif_a_s.3g2 32
if "%5" == "1" (
    call :SPLIT_FILE 99 %work_dir%\%temp_file_prefix%qcif.3g2 %work_dir%\%temp_file_prefix%qcif_a_d.3g2 31
    call :SPLIT_FILE 99 %work_dir%\%temp_file_prefix%qvga.3g2 %work_dir%\%temp_file_prefix%qvga_a_d.3g2 33
    call :SPLIT_FILE 1490 %work_dir%\%temp_file_prefix%qvga.3g2 %work_dir%\%temp_file_prefix%qvga_a_s.3g2 34
)



rem -- for AU (atom change) 再生できるように動画ﾌｧｲﾙを偽装する
call :ATOM_CHANGE %~1\%temp_file_prefix%*.3g2 

rem -- publish(旧)　
copy /Y %work_dir%\%temp_file_prefix%qcif_d_s_001.3gp %output_path%\%~4.3gp
copy /Y %work_dir%\%temp_file_prefix%qcif_a_s_001.3g2 %output_path%\%~4.3g2
copy /Y %work_dir%\%temp_file_prefix%qcif_s_d_001.3gp %output_path%\%~4s.3gp

rem -- publish(新)
for %%f in (%work_dir%\%temp_file_prefix%????_*) do (
  call :PUBLISH "%%f" 
)

rem -- 元となる画像の保存
copy /Y %work_dir%\%~2 %work_dir%\original\%~2

del /Q %work_dir%\%~2*
del /Q %work_dir%\%temp_file_prefix%*.3gp
del /Q %work_dir%\%temp_file_prefix%*.3g2

set T1=%TIME:/=%                                                                                                                                               
echo 開始時間：%T%                                                              
echo 終了時間：%T1%                                                             
                                                                                 
goto END:





REM ********************************************************
REM -- :NET_USE
REM -- UNCパスの場合、共有リソースにアクセスできるようにする
REM -- 　%1:パス
REM -- 　%2:ユーザー
REM -- 　%3:パスワード
REM ********************************************************
:NET_USE
rem UNCパスじゃない場合は処理しない
if not "%~d1"=="\\" exit /B 0

for /f "tokens=1* delims=\" %%a in ("%~1") do ( 
  net use \\%%a\IPC$ %~3 /user:%~2
)

REM ********************************************************
REM -- :PUBLISH
REM -- イメージ格納ディレクトリにコピーする
REM -- 　%1:コピー元ファイル
REM ********************************************************
:PUBLISH
set fname=%~n1%~x1
set fname=%fname:temp_=%
copy /Y %~1 %output_path%\%fname%
exit /B 0

REM ********************************************************
REM -- :SPLIT_FILE
REM -- 指定されたファイルを分割する。
REM -- 分割後のファイルには_001からの連番をふる
REM -- 　%1:サイズ
REM -- 　%2:ファイル名
REM -- 　%3:分割後のファイル名
REM ********************************************************
:SPLIT_FILE
copy /Y %~2 %~3
%mp4box_dir%\mp4box.exe -split-size %~1 %~3 -quiet
if not exist %~d3%~p3%~n3_001%~x3 copy %~3 %~d3%~p3%~n3_001%~x3
del /Q %~3

set file_count=0
for %%f in ("%~d3%~p3%~n3_*.*") do set /a file_count=file_count+1
echo %~4,%file_count% >> %result_file%

exit /B 0

REM ********************************************************
REM -- :ATOM_CHANGE
REM -- 指定された条件に該当するファイルに対してAtomChangerを適用する
REM -- 　%1:ファイル(ワイルドカード可)
REM ********************************************************
:ATOM_CHANGE
copy /Y %~1 %~1.actmp 
for %%f in (%~1.actmp ) do %work_dir%\ATOMChanger.exe %%f %%~df%%~pf%%~nf %work_dir%\kddicopyrigt.ini
del /Q %~1.actmp
exit /B 0






:END
exit 0
