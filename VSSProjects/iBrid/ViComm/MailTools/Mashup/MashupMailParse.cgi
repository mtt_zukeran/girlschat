#!C:/Perl/bin/perl.exe

use MIME::Parser;
use MIME::WordDecoder;
use MIME::Words qw(decode_mimewords);
use Image::Magick;
use Jcode;
use DBI;
use File::Copy;
use Net::POP3;
use Time::Piece;
use DBD::Oracle qw(:ora_types);

my $t;
my $parser = new MIME::Parser;
my $existMail;

#-----------------------------------------------------------#
# 1.$dirの訂正 (D:\i-Brid\MailToolsへ)						#
# 2.ad-motion.jpの訂正										#
# 3.$pop->loginの訂正										#
#-----------------------------------------------------------#

$dir = 'D:\i-Brid\MailTools\Mashup';

while (true){

	eval{
		$pop = Net::POP3->new('ad-motion.jp') or die "Can't not open account.";
		$pop->login('vicomm@ad-motion.jp','vicomm');
		$existMail = 0;

		$mailbox = $pop->list();

		foreach $msgid (keys %$mailbox) {

			if ($existMail == 0){
				print "Connect DB\n";

				$db =DBI->connect("dbi:Oracle:host=10.0.1.201.;sid=GMB","VICOMM_MASHUP", "VICOMM_MASHUP") or die "CONNECT ERROR $DBI::errstr"; 
				$cmd_rx_mail = $db->prepare(q/
					BEGIN
						MAIL_RX(:pFROM_ADDR,:pTO_ADDR,:pSTATUS);
					END;
				/);
	
				$cmd_attached = $db->prepare(q/
					BEGIN
						ATTACHED_FILE_UPLOAD(
							:pFROM_ADDR,
							:pTO_ADDR,
							:pSUBJECT,
							:pBODY,
							:pOBJECT_TYPE,
							:pOUT_DIR,
							:pFILE_NM,
							:pMOVIE_SEQ,
							:pFTP_FILE_NM,
							:pSTATUS							
						);
					END;
				/) || die $db->errstr;

				$cmd_counte_set = $db->prepare(q/
					BEGIN
						ATTACHED_FILE_COUNT_SET(
							:pMOVIE_SEQ ,
							:pFILE_COUNT_01 ,
							:pFILE_COUNT_02 ,
							:pFILE_COUNT_03 ,
							:pFILE_COUNT_04 ,
							:pFILE_COUNT_05 ,
							:pFILE_COUNT_06 ,
							:pFILE_COUNT_07 ,
							:pFILE_COUNT_08 ,
							:pFILE_COUNT_09 ,
							:pFILE_COUNT_10 ,
							:pFILE_COUNT_11 ,
							:pFILE_COUNT_12 ,
							:pSTATUS
						);
					END;
				/) || die $db->errstr;

				$existMail = 1;
			}

			eval{
				$entity = $parser->parse_data($pop->get($msgid));

				my ($mail_body, $filename);

				# HEAD
				my $mail_from = $entity->head->get('From');
				my $mail_date = $entity->head->get('Date');
				my $mail_to   = $entity->head->get('To');
				my $subject   = $entity->head->get('Subject');

				my $obj_type;
				my $out_dir;
				my $file_nm;
				my $ftp_file_nm;
				my $status;
				my $pass;

				$mail_to =~ s/[<]//g;
				$mail_to =~ s/[>]//g;

				print "from   :",$mail_from;
				print "date   :",$mail_date;
				print "to     :",$mail_to;
		
				$mail_from =~ s/\r\n//;
				$mail_from =~ s/\r//;
				$mail_from =~ s/\n//;

				$mail_to =~ s/\r\n//;
				$mail_to =~ s/\r//;
				$mail_to =~ s/\n//;

				$mail_subject = "";
				if ($subject eq ""){
					$mail_subject = "";
					print "no subject\n"
				}else{
					chomp($subject);
					@decode = decode_mimewords( $subject );
					$text = "";
					foreach $subject ( @decode ){
						($target,$charset) = @$subject;
						print "charset=$charset<br>\n";
						Jcode::convert(\$target, 'sjis', 'jis');
						$target =~ s/\n//g;
						$mail_subject .= $target
					}
				}

				$t = localtime;
				print $t->ymd('') ," " , $t->hms('') ,"\n";

				#送信先メールアドレスより頭4文字取り出し
				$mail_to_head4 = substr($mail_to,0,4);
				$mail_to_head7 = substr($mail_to,0,7);
				#頭4文字がRX_MAIL内の対象(regm,regp,modm,fogm,catm,catf,utnm,inquiry)であったら添付メールとしない
				if ( $entity->is_multipart and ( ($mail_to_head4 eq 'regm') || ($mail_to_head4 eq 'regp') || ($mail_to_head4 eq 'modm') || ($mail_to_head4 eq 'fogm')
											  || ($mail_to_head4 eq 'catm') || ($mail_to_head4 eq 'catf') || ($mail_to_head4 eq 'utnm') || ($mail_to_head7 eq 'inquiry') ) == 0 ){

					print "multi  :Yes\n";
					my $count = $entity->parts;

					for (my $i = 0; $i < $count; $i++) {
						#q_text or binary

						my ($type, $subtype) = split('/', $entity->parts($i)->head->mime_type);
						print "type   :","$type\n";

						if ($type =~ /^(text|message)$/) { 
							# text
							$mail_body .= $entity->parts($i)->bodyhandle->as_string;
							unlink($entity->parts($i)->bodyhandle->path);

							Jcode::convert(\$mail_body, 'sjis', 'jis');

							print "subject;" ,$mail_subject,"\n";
							print "body   :" ,$mail_body,"\n";

							if ($i eq 0){
								$cmd_rx_mail->bind_param(':pFROM_ADDR'	, $mail_from);
								$cmd_rx_mail->bind_param(':pTO_ADDR'	, $mail_to);
								$cmd_rx_mail->bind_param_inout(':pSTATUS', \$status	, 10);
								$cmd_rx_mail->execute;
								print "status :","$status\n";
							}

						} else {
							if($entity->parts($i)->is_multipart){
								print "parts  : $i (Nested)\n";
							}else{
								

								$cmd_attached->bind_param(':pFROM_ADDR'	, $mail_from);
								$cmd_attached->bind_param(':pTO_ADDR'	, $mail_to);
								$cmd_attached->bind_param(':pSUBJECT'	, $mail_subject);
								$cmd_attached->bind_param(':pBODY'		, $mail_body);

								$cmd_attached->bind_param_inout(':pOBJECT_TYPE'	, \$obj_type	, 1);
								$cmd_attached->bind_param_inout(':pOUT_DIR'		, \$out_dir		, 128);
								$cmd_attached->bind_param_inout(':pFILE_NM'		, \$file_nm 	, 15);
								$cmd_attached->bind_param_inout(':pMOVIE_SEQ'	, \$movie_seq 	, 15);
								$cmd_attached->bind_param_inout(':pFTP_FILE_NM'	, \$ftp_file_nm , 15);
								$cmd_attached->bind_param_inout(':pSTATUS'		, \$status		, 10);

								$cmd_attached->execute() || die $cmd_attached->errstr;
								print "objtype:","$obj_type\n";
								print "status :","$status\n";
								print "output :","$out_dir/$file_nm\n\n";

								if ($status eq 'I000' and $file_nm ne ''){

									if ($obj_type eq '1' or $obj_type eq '3'){
										my $image = Image::Magick->new(magick=>'jpg');
										$image->BlobToImage($entity->parts($i)->bodyhandle->as_string);

										$filename= $file_nm . '.jpg';

										my ($width,$height) = $image->Get('width','height');

										$width = $width * 0.55;
										$height = $height * 0.55;
										$image->Resize(width=>$width,height=>$height,blur=>0.7);
										
										#コピーライト画像変数宣言
										my $img2= Image::Magick->new;
										
										#Toドメイン名を取得
										$to_domain = $mail_to;
										$to_domain =~ s/.*@//g;
										
										#Toドメイン名によって読み込むコピーライト画像を切り替える
										if ($to_domain eq 'marii.tv'){
											$img2->Read("$dir/marii.png");
										}
										elsif ($to_domain eq 'erii.tv'){
											$img2->Read("$dir/erii.png");
										}
										else{
											$img2->Read("$dir/eshot2.png");
										}
										
										#コピーライト画像合成
										$image->Composite(image=>$img2, compose=>'Overlay', gravity=>'SouthEast');
										$image->Write("$out_dir/$filename");
										
										#obj_type 3の時ぼかし画像作成
										if ($obj_type eq '3'){

											my $image_blur= Image::Magick->new;
											$image_blur->Read("$out_dir/$filename");
											$filename= $file_nm. '_m.jpg';
											
											#ぼかしをかける
											$image_blur->Blur('7x7');
											
											$image_blur->Write("$out_dir/$filename");
										}
										
										if ($ftp_file_nm ne ''){
											open OUTFILE, '>', 'd:\i-Brid\MailTools\MASHUP\FTPCMD.txt' or die "file open error: $!";
											print OUTFILE "open 221.186.83.58\n";
											print OUTFILE "mashup\n";
											print OUTFILE "marii\n";
											print OUTFILE "put $out_dir/$filename $ftp_file_nm\n";
											print OUTFILE "quit\n";
											close OUTFILE;
										}

										$filename= $file_nm. '_s.jpg';
										$width = $width * 0.45;
										$height = $height * 0.45;
										$image->Resize(width=>$width,height=>$height,blur=>0.7);
										$image->Write("$out_dir/$filename");

										unlink($entity->parts($i)->bodyhandle->path);

										if ($ftp_file_nm ne ''){
											my $cnv_ret = system("ftp -s:d:\\i-Brid\\MailTools\\MASHUP\\FTPCMD.txt");
										}
									}

									if ($obj_type eq '2'){
										@movile_file_type_arr = ();
										@file_num_arr = ();
								
										# 変換処理
										$filename= $file_nm . '.3gp';
										move($entity->parts($i)->bodyhandle->path,"$dir/$filename");
										my $cnv_ret = system("Convert.bat","$dir","$filename","$out_dir","$file_nm");

										open(IN_RESULT, "$dir/$file_nm".".result") || die("error :$!");
										# 結果ファイルを解析(動画種別,ファイル件数)
										while(my $line = <IN_RESULT>){
											# 1行を4つに分ける
											chomp($line);
											my ($movile_file_type, $file_num) = split(/,/, $line, 2);
									
											$file_num =~ s/^\s*(.*?)\s*$/$1/;
											push @movile_file_type_arr, $movile_file_type ;
											push @file_num_arr, $file_num ;
										}
										close(IN_RESULT);
								
										unlink "$dir/$file_nm".".result";
								
										$cmd_counte_set->bind_param(':pMOVIE_SEQ', $movie_seq  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_01', $file_num_arr[0]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_02', $file_num_arr[1]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_03', $file_num_arr[2]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_04', $file_num_arr[3]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_05', $file_num_arr[4]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_06', $file_num_arr[5]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_07', $file_num_arr[6]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_08', $file_num_arr[7]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_09', $file_num_arr[8]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_10', $file_num_arr[9]  );
										$cmd_counte_set->bind_param(':pFILE_COUNT_11', $file_num_arr[10] );
										$cmd_counte_set->bind_param(':pFILE_COUNT_12', $file_num_arr[11] );
										$cmd_counte_set->bind_param_inout(':pSTATUS'		, \$status		, 10);
										$cmd_counte_set->execute() || die $cmd_counte_set->errstr;
										print "status :","$status\n";

										print "convert result :","$cnv_ret\n";
									}
								}
							}
						}
					}
				}else{
					print "multi  :No\n";
					unlink($entity->bodyhandle->path);
					$cmd_rx_mail->bind_param(':pFROM_ADDR'	, $mail_from);
					$cmd_rx_mail->bind_param(':pTO_ADDR'	, $mail_to);
					$cmd_rx_mail->bind_param_inout(':pSTATUS', \$status	, 10);
					$cmd_rx_mail->execute;
					print "status :","$status\n";
				}

			};

			$pop->delete($msgid)

		}
	
		$pop->quit;


		opendir(DIR, $dir) or die;
		#@file =  grep { /^.*\.msg/i|/^.*\.3gp/i|/^.*\.txt/i|/^.*\.html/i|/^.*\.gif/i|/^.*\.jpg/i  && -f "$dir/$_"} readdir(DIR);
		@file =  grep { /^.*\.msg/i|/^.*\.txt/i|/^.*\.html/i|/^.*\.gif/i|/^.*\.jpg/i  && -f "$dir/$_"} readdir(DIR);
		closedir(DIR);
		foreach(@file) {
			print "$_\n";
    		unlink "$dir/$_";
		}
	};

	if ($existMail){
		print "Disconnect DB\n";
		$db->disconnect();
	}

	sleep 10;

	if( $@ ){
		print $@;
	}
}

$db->disconnect;

