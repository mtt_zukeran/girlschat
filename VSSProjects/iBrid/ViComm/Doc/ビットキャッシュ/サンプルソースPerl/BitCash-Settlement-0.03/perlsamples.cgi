#!/usr/bin/perl

use strict;
use Data::Dumper;
use CGI;
use BitCash::Settlement;
use vars qw($bcs);


my $q = new CGI;
print $q->header;
print <<"EOM";
<FORM MEHTOD="POST" ACTION="./perlsample.cgi">
<INPUT TYPE="text" name="CARD_NUMBER" size=32 maxlength=32><BR>
<INPUT TYPE="submit" VALUE="GO!">
</FORM>
EOM


eval {

my $CARD_NUMBER = $q->param('CARD_NUMBER');
if ($CARD_NUMBER) {

	# SHOP 情報
	my $SHOP_ID     = 'SHOP_ID';
	my $SHOP_PASSWD = 'SHOP_PASSWD';

	# API URL
	my $PROXY       = 'https://ssl.bitcash.co.jp/api';
	
	# 商品情報
	my $ORDER_ID    = time;
	my $RATING      = '19';
	my $PRICE       = 1;


	$bcs = BitCash::Settlement->new(
		SHOP_ID     => $SHOP_ID,
		SHOP_PASSWD => $SHOP_PASSWD,
		PROXY       => $PROXY
	) or die $!;

	#-------------#
	# validation
	#-------------#
	print "１．カードバリデーション<BR>";
	my $ret1 = 
	$bcs->validate_card(
		ORDER_ID => $ORDER_ID,
		CARD_NUMBER => $CARD_NUMBER,
		RATING => $RATING,
		PRICE => $PRICE,
	);
	if ($bcs->{status} ne 'OK') {
		print Dumper $bcs;
		die "Can not do 'validate_card' ..";
	}
	print "validate_card:SUCCESS<BR>";


	#-------------#
	# settlement
	#-------------#
	print "２．決済<BR>";
	my $ret2 = $bcs->do_settlement(
		BCS_ID => $bcs->bcs_id,
	);
	if ($bcs->{status} ne 'OK') {
		print Dumper $bcs;
		die "Can not do 'do_settlement' ..";
	}
	print "do_settlement:SUCCESS<BR>";


	#-------------#
	# undef $bcs
	#-------------#
	my $bcs_id   = $bcs->bcs_id;
	my $sales_id = $bcs->sales_id;

	undef $bcs;
	$bcs = BitCash::Settlement->new(
		SHOP_ID     => $SHOP_ID,
		SHOP_PASSWD => $SHOP_PASSWD,
		PROXY       => $PROXY
	) or die $!;


	#-------------#
	# confirm
	#-------------#
	print "３．確認<BR>";
	my $ret3 = $bcs->confirm_settlement(
#		SALES_ID => $bcs->sales_id
#		ORDER_ID => $ORDER_ID
		BCS_ID => $bcs_id
	);
	if ($bcs->{status} ne 'OK') {
		print Dumper $bcs;
		die "Can not do 'confirm_settlement' ..";
	}
	print "confirm_settlement:SUCCESS:INFO:".$bcs->{info}."<BR>";


	#-------------#
	# cancel
	#-------------#
	print "４．取消<BR>";
	my $ret4 = $bcs->undo_settlement(
		SALES_ID => $sales_id
	);
	if ($bcs->{status} ne 'OK') {
		print Dumper $bcs;
		die "Can not do 'undo_settlement' ..";
	}
	print "undo_settlement:SUCCESS<BR>";


	#-------------#
	# undef $bcs
	#-------------#
	my $bcs_id   = $bcs->bcs_id;
	my $sales_id = $bcs->sales_id;

	undef $bcs;
	$bcs = BitCash::Settlement->new(
		SHOP_ID     => $SHOP_ID,
		SHOP_PASSWD => $SHOP_PASSWD,
		PROXY       => $PROXY
	) or die $!;


	#-------------#
	# confirm
	#-------------#
	print "５．確認2<BR>";
	my $ret5 = $bcs->confirm_settlement(
#		SALES_ID => $bcs->sales_id
#		ORDER_ID => $ORDER_ID
		BCS_ID => $bcs_id
	);
	if ($bcs->{status} ne 'OK') {
		print Dumper $bcs;
		die "Can not do 'confirm_settlement' ..";
	}
	print "confirm_settlement2:SUCCESS:INFO:".$bcs->{info}."<BR>";

}
};

print "<B>".$@."</B><BR>" if $@;

$q->end_html;

END {
	eval { $bcs->clear_all; };
}


