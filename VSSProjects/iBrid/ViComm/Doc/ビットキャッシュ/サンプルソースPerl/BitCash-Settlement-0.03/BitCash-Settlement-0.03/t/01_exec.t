use strict;
use Test::More;
use Data::Dumper;
BEGIN {
    eval { require BitCash::Settlement;};
    plan $@ ? (skip_all => 'no BitCash::Settlement'): ('no_plan');
    use_ok 'BitCash::Settlement';
}

{
    my $SHOP_ID     = 'NAKA0351';
    my $SHOP_PASSWD = 'naka4593';
#    my $PROXY = 'http://t.bitcash.co.jp/api';
    my $PROXY = 'https://ssl.bitcash.co.jp/api';

    my $ORDER_ID    = time;
    #my $CARD_NUMBER = 'あほえわままへすわくけとむこわは';
    my $CARD_NUMBER = 'いけくかわううにくなるやせみめら';
    
    my $RATING      = '99';
    my $PRICE       = 1;
    
    my $bcs = BitCash::Settlement->new(
	SHOP_ID => $SHOP_ID,SHOP_PASSWD => $SHOP_PASSWD,
	PROXY   => $PROXY
    );
    
    my $ret1 = $bcs->validate_card(
	ORDER_ID    => $ORDER_ID,
	CARD_NUMBER => $CARD_NUMBER,
	RATING      => $RATING,
	PRICE       => $PRICE,
    );
    warn qq/\n/;
    warn $bcs->status;
    warn Dumper $bcs;
    
    my $ret2 = $bcs->do_settlement;
    warn $bcs->status;
    warn Dumper $bcs;

    my $ret3 = $bcs->confirm_settlement;
    warn $bcs->status;
    warn Dumper $bcs;
    
    my $ret4 = $bcs->undo_settlement;
    warn $bcs->status;
    warn Dumper $bcs;
    
}




