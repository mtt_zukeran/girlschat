package BitCash::Settlement;

use strict;
use vars qw($VERSION);
$VERSION     = '0.03';

# ----------------------------------------------------------------------

use base qw(Class::Accessor);
use XMLRPC::Lite; # +trace => 'debug';
use Digest::MD5 qw(md5_hex);
use Unicode::Japanese;
use Data::Dumper;

# ----------------------------------------------------------------------

__PACKAGE__->mk_accessors(&accessor);
sub accessor {
    (&accessor_of_defaultsetting,
     &accessor_of_argument,
     &accessor_of_return,
     &accessor_of_need_ids,);
}

sub accessor_of_defaultsetting {
    qw(shop_id shop_passwd proxy);
}

sub accessor_of_argument {
    qw(order_id card_number rating price);
}

sub accessor_of_return {
    qw(log_id status balance error salestime info lot serial);
}

sub accessor_of_need_ids {
    qw(sales_id bcs_id);
}

sub clear_all {
    my $self = shift;
    $self->clear_arg;
    $self->clear_ret;
    $self->clear_ids;
}

sub clear_arg {
    my $self = shift;
    $self->set($_ => '') for (&accessor_of_argument);
}

sub clear_ret {
    my $self = shift;
    $self->set($_ => '') for (&accessor_of_return);
}

sub clear_ids {
    my $self = shift;
    $self->set($_ => '') for (&accessor_of_need_ids);
}

# ----------------------------------------------------------------------

sub new {
    my $class = shift;
    my %arg   = @_;

    my $self = bless {}, $class;
    $self->shop_id($arg{SHOP_ID});
    $self->shop_passwd($arg{SHOP_PASSWD});
    $self->proxy($arg{PROXY});
    return $self;
}

# ----------------------------------------------------------------------

sub logic {
    my $self = shift;
    my $meth = shift;
    my $arg  = $self->_prepare_argument(@_);
    my $ret  = $self->_exec_api($meth,$arg);
    $self->_return_behavior($ret);
}

sub validate_card      { shift->logic('Settlement.validate_card',@_) }

sub do_settlement      { shift->logic('Settlement.do_settlement',@_) }

sub undo_settlement    { shift->logic('Settlement.undo_settlement',@_) }

sub confirm_settlement { shift->logic('Settlement.confirm_settlement',@_) }

# ----------------------------------------------------------------------

sub _prepare_argument {
    my $self = shift;
    my %arg  = @_;
   
    my $order_id = $arg{ORDER_ID} ? $arg{ORDER_ID} : $self->order_id;
    $self->order_id($order_id || md5_hex(time() . $$ . rand() . {}));
	
    my $ret = {};
    $ret->{ORDER_ID}    = $self->order_id;
    $ret->{SHOP_ID}     = $self->shop_id;
    $ret->{SHOP_PASSWD} = $self->shop_passwd;

    if (defined $arg{CARD_NUMBER}){
	$self->card_number($arg{CARD_NUMBER});
	$ret->{CARD_NUMBER} = $self->card_number;
    }
    if (defined $arg{RATING}) {
	$self->rating($arg{RATING});
	$ret->{RATING} = $self->rating;
    }
    if (defined $arg{PRICE}) {
	$self->price($arg{PRICE});
	$ret->{PRICE} = $self->price;
    }

    if (defined $arg{BCS_ID}) {
	$self->bcs_id($arg{BCS_ID});
    }
    $ret->{BCS_ID} = $self->bcs_id if $self->bcs_id;

    if (defined $arg{SALES_ID}) {
	$self->sales_id($arg{SALES_ID});
    }
    $ret->{SALES_ID} = $self->sales_id if $self->sales_id;

    return $ret;
}

# ----------------------------------------------------------------------

sub _exec_api {
    my $self   = shift;
    my $method = shift;
    my $arg    = shift;
   
    use Data::Dumper;
    warn $method;
    warn Dumper $arg;
    
    my $ret;
    eval {
	$ret = XMLRPC::Lite
	    ->proxy($self->proxy)
	    ->call($method,_enc($arg))
	    ->result;
    };

    warn Dumper $ret;
    unless ($ret) {
	$ret->{STATUS} = 'FAIL';
    }
    if ($@) {
        warn Dumper $@;
	push @{$ret->{ERROR}},  'connection_failed';
    }
    return _dec($ret);
}

# ----------------------------------------------------------------------

sub _return_behavior {
    my $self = shift;
    my $ret  = shift;
    
    $self->log_id($ret->{LOG_ID})       if defined $ret->{LOG_ID};
    $self->status($ret->{STATUS})       if defined $ret->{STATUS};
    $self->balance($ret->{BALANCE})     if defined $ret->{BALANCE};
    $self->bcs_id($ret->{BCS_ID})       if defined $ret->{BCS_ID};
    $self->sales_id($ret->{SALES_ID})   if defined $ret->{SALES_ID};
    $self->error($ret->{ERROR})         if defined $ret->{ERROR};
    $self->salestime($ret->{SALESTIME}) if defined $ret->{SALESTIME};
    $self->info($ret->{INFO})           if defined $ret->{INFO};
    $self->lot($ret->{LOT})             if defined $ret->{LOT};
    $self->serial($ret->{SERIAL})       if defined $ret->{SERIAL};
    return $self->status;
}

# ----------------------------------------------------------------------
sub _dec {
    my $hash= shift;
    return {} unless ref $hash eq "HASH";
    return _nomarize($hash,'utf8','euc');
}

sub _enc {
    my $hash= shift;
    return {} unless ref $hash eq "HASH";
    return _nomarize($hash,'euc','utf8');
}

sub _from_to {
    my $str = shift;
    my ($from,$to) = @_;
    if ($to eq 'euc') {
	return Unicode::Japanese->new($str)->$to();
    }
    else {
	return Unicode::Japanese->new($str,$from)->get;
    }
}

sub _nomarize {
    my $hash = shift;
    my ($from,$to) = @_;
    my $ret = {};
    return unless $hash;
    for my $key (keys %{$hash}) {
        if (ref $hash->{$key} eq 'ARRAY') {
            for my $val (@{$hash->{$key}}) {
		if (ref $val eq 'HASH') {
		    for (keys %{$val}) {
			$val->{$_} = _from_to($val->{$_},$from,$to);
		    }
		}
		else {
		    $val = _from_to($val,$from,$to);
		}
            }
	    $ret->{$key} = $hash->{$key};
        }
        else {
            $ret->{$key} = _from_to($hash->{$key},$from,$to);
        }
    }
    return $ret;
}

1;


__END__

=head1 NAME

  BitCash::Settlement - a API client of bitcash settlement sytem

  $Id: Settlement.pm,v 1.14 2007/03/05 10:56:08 aki_t Exp $

=head1 SYNOPSYS

  my $bcs = BitCash::Settlement->new(
      SHOP_ID     => $SHOP_ID,
      SHOP_PASSWD => $SHOP_PASSWD,
      PROXY       => $PROXY
  );

  my $ret1 = $bcs->validate_card(
      ORDER_ID    => $ORDER_ID,
      CARD_NUMBER => $CARD_NUMBER,
      RATING      => $RATING,
      PRICE       => $PRICE,
  );

  my $ret2 = $bcs->do_settlement($bcs->{BCS_ID});
  my $ret3 = $bcs->undo_settlement;
  my $ret4 = $bcs->confirm_settlement;


=head1 DESCRIPTION

=head2 MAKE CONSUTRUCTOR

  use BitCash::Settlement;

  my $bcs = BitCash::Settlement->new(
      SHOP_ID     => $SHOP_ID,
      SHOP_PASSWD => $SHOP_PASSWD,
      PROXY       => $PROXY
  );

=head2 CARD VALIDATION

  * validate_card
  my $ret = $bcs->validate_card(
      ORDER_ID    => $ORDER_ID,
      CARD_NUMBER => $CARD_NUMBER,
      RATING      => $RATING,
      PRICE       => $PRICE,
  );

=head2 SETTLEMENT

  * do_settlement

  # keeping argument.
  my $ret_settlement = $bcs->do_settlement;

  # OR,you can give new argument.
  my $ret_settlement = $bcs->do_settlement(
     BCS_ID => $bcs->{BCS_ID}
  );

=head2 CONFIRM SETTLEMENT

  * confirm_settlement

  # keeping argument.
  my $ret_confirm = $bcs->confirm_settlement;

  # OR,you can give new argument.
  my $ret_confirm = $bcs->confirm_settlement(
     BCS_ID   => $bcs->{BCS_ID},
     SALES_ID => $bcs->{SALES_ID}
  );


=head2 CANCEL SETTLEMENT

  # keeping argument.
  my $ret_cancel = $bcs->undo_settlement;

  # OR, you can give new argument.
  my $ret_cancel = $bcs->undo_settlement(
      SALES_ID    => $bcs->{SALES_ID}
  );

=head2 ACCESSOR

  # Set Default (this cannot be deleted)
  my $SHOP_ID     = $bcs->shop_id;
  my $SHOP_PASSWD = $bcs->shop_passwd;
  my $PROXY       = $bcs->proxy;

  # Set while other method called if you give.
  my $order_id    = $bcs->order_id;
  my $card_number = $bcs->card_number;
  my $rating      = $bcs->rating;
  my $price       = $bcs->price;

  my $bcs_id      = $bcs->bcs_id;
  my $sales_id    = $bcs->sales_id;

  # Set After exec method.
  my $id       = $bcs->log_id;   # return bc settlement log identity
  my $status   = $bcs->status;   # return status (OK/FAIL)
  my $balance  = $bcs->balance;  # return card balance
  my @error    = $bcs->error;    # return error message

=head2 OTHERS

  $bcs->clear_all;

  $bcs->clear_arg;

  $bcs->clear_ret;

  $bcs->clear_ids;

=item

=cut
