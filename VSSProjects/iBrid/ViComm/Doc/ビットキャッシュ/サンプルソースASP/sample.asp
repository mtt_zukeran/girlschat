<html>
<head>
<title>ビットキャシュ決済サンプルコード</title>
<body bgcolor="#FFFFFF" alink="#008000" vlink="#800080" link="#0000FF">
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT_JIS">

<FORM action="./sample.asp" method="POST">
<INPUT type="text" name="CARD_NUMBER" size="30" maxlength="32">
<input type="submit">
</FORM>

<!--#include virtual="/Scripts/sample/xmlrpc.asp" -->
<hr>
１：カードの判定<br>
<%
CARD_NUMBER=Request.Form("CARD_NUMBER")
STATUS=""
If CARD_NUMBER <> "" Then

ReDim paramList(1)
set dict=Server.createObject("Scripting.Dictionary")
dict.add "SHOP_ID", "ABCD0123"
dict.add "SHOP_PASSWD", "PASSWORD"
dict.add "ORDER_ID", "1234ABCD"
dict.add "CARD_NUMBER",CARD_NUMBER
dict.add "RATING", "19"
dict.add "PRICE", "1"
set paramList(0)=dict

set myresp = xmlRPC ("https://ssl.bitcash.co.jp/api/", "Settlement.validate_card", paramList)
set dict=nothing
    
response.write("STATUS: ")
response.write(myresp.item("STATUS"))
response.write("<br> ")

response.write("LOG_ID: ")
response.write(myresp.item("LOG_ID"))
response.write("<br> ")

response.write("BALANCE: ")
response.write(myresp.item("BALANCE"))
response.write("<br> ")

response.write("BCS_ID: ")
response.write(myresp.item("BCS_ID"))
response.write("<br> ")

List=myresp.item("ERROR")
FOR Each i In List
 response.write(i)
 response.write("<br>")
Next

STATUS=myresp.item("STATUS")

END If

%>

<hr>
２：決済<br>
<%
If STATUS = "OK" Then

ReDim paramList(1)
set dict=Server.createObject("Scripting.Dictionary")
dict.add "SHOP_ID", "ABCD0123"
dict.add "SHOP_PASSWD", "PASSWORD"
dict.add "ORDER_ID", "1234ABCD"
dict.add "BCS_ID", myresp.item("BCS_ID")
set paramList(0)=dict

set myresp = xmlRPC ("https://ssl.bitcash.co.jp/api/", "Settlement.do_settlement", paramList)
set dict=nothing


response.write("STATUS: ")
response.write(myresp.item("STATUS"))
response.write("<br> ")

response.write("LOG_ID: ")
response.write(myresp.item("LOG_ID"))
response.write("<br> ")

response.write("BALANCE: ")
response.write(myresp.item("BALANCE"))
response.write("<br> ")

response.write("SALES_ID: ")
response.write(myresp.item("SALES_ID"))
response.write("<br> ")

List=myresp.item("ERROR")
FOR Each i In List
 response.write(i)
 response.write("<br>")
Next

END If

%>
<hr>
３：決済確認<br>
<%
If STATUS = "OK" Then

ReDim paramList(1)
set dict=Server.createObject("Scripting.Dictionary")
dict.add "SHOP_ID", "ABCD0123"
dict.add "SHOP_PASSWD", "PASSWORD"
dict.add "SALES_ID", myresp.item("SALES_ID")
set paramList(0)=dict

set myresp = xmlRPC("https://ssl.bitcash.co.jp/api/", "Settlement.confirm_settlement", paramList)
set dict=nothing

response.write("STATUS: ")
response.write(myresp.item("STATUS"))
response.write("<br> ")

response.write("LOG_ID: ")
response.write(myresp.item("LOG_ID"))
response.write("<br> ")

response.write("SALES_ID: ")
response.write(myresp.item("SALES_ID"))
response.write("<br> ")

response.write("PRICE: ")
response.write(myresp.item("PRICE"))
response.write("<br> ")

response.write("INFO: ")
response.write(myresp.item("INFO"))
response.write("<br> ")

response.write("LOT: ")
response.write(myresp.item("LOT"))
response.write("<br> ")

response.write("SERIAL: ")
response.write(myresp.item("SERIAL"))
response.write("<br> ")


List=myresp.item("ERROR")
FOR Each i In List
 response.write(i)
 response.write("<br>")
Next

END If


%>

</body>
</html>
