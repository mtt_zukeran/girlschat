<html>
<head>
<title>ビットキャシュ決済サンプルコード</title>
<body bgcolor="#FFFFFF" alink="#008000" vlink="#800080" link="#0000FF">
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT_JIS">

<FORM action="./confirm_sample.asp" method="POST">
<INPUT type="text" name="SALES_ID" size="30" maxlength="32">
<INPUT type="text" name="ORDER_ID" size="30" maxlength="32">

<input type="submit">
</FORM>

<!--#include virtual="/Scripts/sample/xmlrpc.asp" -->
<hr>
１：カードの確認<br>
<%

SALES_ID=Request.Form("SALES_ID")
ORDER_ID=Request.Form("ORDER_ID")
STATUS=""
If SALES_ID <> "" or ORDER_ID <> "" Then

ReDim paramList(1)
set dict=Server.createObject("Scripting.Dictionary")
dict.add "SHOP_ID", "ABCD0123"
dict.add "SHOP_PASSWD", "PASSWORD"
dict.add "ORDER_ID", ORDER_ID
dict.add "SALES_ID", SALES_ID
set paramList(0)=dict

set myresp = xmlRPC ("https://ssl.bitcash.co.jp/api/", "Settlement.confirm_settlement", paramList)
set dict=nothing

response.write("STATUS: ")
response.write(myresp.item("STATUS"))
response.write("<br> ")

response.write("LOG_ID: ")
response.write(myresp.item("LOG_ID"))
response.write("<br> ")

response.write("SALES_ID: ")
response.write(myresp.item("SALES_ID"))
response.write("<br> ")

response.write("PRICE: ")
response.write(myresp.item("PRICE"))
response.write("<br> ")

response.write("INFO: ")
response.write(myresp.item("INFO"))
response.write("<br> ")

response.write("LOT: ")
response.write(myresp.item("LOT"))
response.write("<br> ")

response.write("SERIAL: ")
response.write(myresp.item("SERIAL"))
response.write("<br> ")


List=myresp.item("ERROR")
FOR Each i In List
 response.write(i)
 response.write("<br>")
Next

END If

%>

<hr>
<br>

</body>
</html>
