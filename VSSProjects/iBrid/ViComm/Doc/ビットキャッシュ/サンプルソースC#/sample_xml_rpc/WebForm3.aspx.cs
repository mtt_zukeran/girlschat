using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CookComputing.XmlRpc;
using System.Text.RegularExpressions;


namespace sample_xml_rpc
{
	/// <summary>
	/// WebForm3 の概要の説明です。
	/// </summary>
	public class WebForm3 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.TextBox TextBox2;
	
		private Iparm apiProxy;
		protected System.Web.UI.WebControls.Label Label3;
		private XmlRpcClientProtocol clientProtocol;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// ページを初期化するユーザー コードをここに挿入します。
		}

		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Button1_Click(object sender, System.EventArgs e)
		{

			string salesid = TextBox1.Text;
			string bcsid = TextBox2.Text;

			Label3.Text = "";

			//sales_idが正規表現で数字のみ入力可能と判定
			Regex regex = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
			if (false == regex.IsMatch(salesid))
			{
				Label3.Text = "SALES_IDは数値以外は入力不可能です";
				return;
			}

			//bcs_idが正規表現で数字のみ入力可能と判定
			if (false == regex.IsMatch(bcsid))
			{
				Label3.Text = "BCS_IDは数値以外は入力不可能です";
				return;
			}


			//ハッシュ作成
			Hashtable comfirm_result = new Hashtable();

			//決済確認
			//セッションに一度入れて、ページ間の受け渡しにも対応する。
			Session["comfirm_ret"] = confirm_settlement(System.Int32.Parse(salesid),bcsid);

			//ハッシュに渡す
			comfirm_result = (Hashtable)Session["comfirm_ret"];
			string comfirm_status =  (string)comfirm_result["STATUS"];

			//正常に決済が終了した確認処理(optional)☆
			if ("OK" == comfirm_status)
			{
				int comfirm_balance =  (int)comfirm_result["PRICE"];//販売価格
				int comfirm_info =  (int)comfirm_result["INFO"];//１：通常　８：取消し済み
				string comfirm_lot =  (string)comfirm_result["LOT"];//ロット
				string comfirm_serial =  (string)comfirm_result["SERIAL"];//シリアル
				Label3.Text = "確認が終了しました。";
			}
			else
			{
				//FAILが返って来た処理（エラー処理）
				System.Array comfirm_error = (System.Array )comfirm_result["ERROR"];
				Label3.Text = "決済に異常が発生しました。" + (string)comfirm_error.GetValue(0);
			}
		}

		//決済確認API
		private XmlRpcStruct confirm_settlement(int salesid,string bcsid)
		{
			//XML_RPC用のハッシュ作成（通常のハッシュでは無い）
			XmlRpcStruct hashTable = new XmlRpcStruct();
			XmlRpcStruct ret = new XmlRpcStruct();

			hashTable.Add("SHOP_ID","NAKA0351"); // Addメソッドで追加
			hashTable.Add("SHOP_PASSWD","naka4593"); // Addメソッドで追加
			hashTable.Add("ORDER_ID",""); // Addメソッドで追加
			hashTable.Add("SALES_ID",salesid); // Addメソッドで追加
			hashTable.Add("BCS_ID",bcsid); // Addメソッドで追加

			apiProxy = (Iparm)XmlRpcProxyGen.Create(typeof(Iparm));
			clientProtocol = (XmlRpcClientProtocol)apiProxy;
			clientProtocol.Url = "https://ssl.bitcash.co.jp/api/";

			try
			{
				//実処理
				ret = apiProxy.confirm_settlement(hashTable);

			}
			catch (Exception ex)
			{
				HandleException(ex);
			}

			return ret;

		}
		
		//インタフェース定義
		interface Iparm
		{

			//決済確認
			[XmlRpcMethod("Settlement.confirm_settlement")]
			XmlRpcStruct confirm_settlement(XmlRpcStruct c);

		}

		//内部エラーハンドリング
		private string HandleException(Exception ex)
		{
			try
			{
				throw ex;
			}
			catch(XmlRpcFaultException fex)
			{
				return "Fault Response: " + fex.FaultCode + " " + fex.FaultString;
			}
			catch(XmlRpcServerException xmlRpcEx)
			{
				return "XmlRpcServerException: " + xmlRpcEx.Message;

			}
			catch(Exception defEx)
			{
				return "Exception: " + defEx.Message;

			}
		}

	}
}
