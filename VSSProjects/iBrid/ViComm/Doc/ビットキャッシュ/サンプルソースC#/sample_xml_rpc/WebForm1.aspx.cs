using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CookComputing.XmlRpc;
using System.Text.RegularExpressions;


namespace sample_xml_rpc
{
	/// <summary>
	/// WebForm1 の概要の説明です。
	/// </summary>
	public class WebForm1 : System.Web.UI.Page
	{

		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Button Button1;

		protected System.Web.UI.WebControls.Label Label1;

		private Iparm apiProxy;
		private XmlRpcClientProtocol clientProtocol;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// ページを初期化するユーザー コードをここに挿入します。
		}

		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);

		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Button1_Click(object sender, System.EventArgs e)
		{

			string s = TextBox1.Text;
			string orderid = "test";

			Session["hiragana"] = s;

			//正規表現でひらがなのみを入力可能と判定
			Regex regex = new System.Text.RegularExpressions.Regex(@"^[あ-を]+$");
			if (false == regex.IsMatch(s))
			{
				Label1.Text = "ひらがな以外は入力不可能です";
				return;
			}

			//バリデーション実行
			//セッションに一度入れて、ページ間の受け渡しにも対応する。
			Session["validate_ret"] = validate_card(s,orderid);
			//ハッシュ作成
			Hashtable validate_result = new Hashtable();
			validate_result = (Hashtable)Session["validate_ret"];

			string validate_status =  (string)validate_result["STATUS"];

			//正常にバリデーションが終了した後の処理
			if ("OK" == validate_status)
			{

				//パラメータ取得
				string validate_bcsid =  (string)validate_result["BCS_ID"];
				int validate_logid =  (int)validate_result["LOG_ID"];
				int validate_balance =  (int)validate_result["BALANCE"];

				//ハッシュ作成
				Hashtable settelement_result = new Hashtable();

				//決済実行
				//セッションに一度入れて、ページ間の受け渡しにも対応する。
				Session["settlement_ret"] = do_settlement(validate_bcsid,orderid);

				//ハッシュに渡す
				settelement_result = (Hashtable)Session["settlement_ret"];
				string settelement_status =  (string)settelement_result["STATUS"];

				//正常に決済が終了した後の処理
				if ("OK" == settelement_status)
				{
					int settelement_salesid =  (int)settelement_result["SALES_ID"];
					int settelement_logid =  (int)settelement_result["LOG_ID"];
					int settelement_balance =  (int)settelement_result["BALANCE"];
					Label1.Text = "正常に決済が終了しました。";
					
					//ハッシュ作成
					Hashtable comfirm_result = new Hashtable();

					//決済確認
					//セッションに一度入れて、ページ間の受け渡しにも対応する。
					Session["comfirm_ret"] = confirm_settlement(settelement_salesid,orderid);

					//ハッシュに渡す
					comfirm_result = (Hashtable)Session["comfirm_ret"];
					string comfirm_status =  (string)comfirm_result["STATUS"];

					//正常に決済が終了した確認処理(optional)☆
					//サービスに必要な場合は利用してください。
					if ("OK" == comfirm_status)
					{
						int comfirm_balance =  (int)comfirm_result["PRICE"];//販売価格
						int comfirm_info =  (int)comfirm_result["INFO"];//１：通常　８：取消し済み
						string comfirm_lot =  (string)comfirm_result["LOT"];//ロット
						string comfirm_serial =  (string)comfirm_result["SERIAL"];//シリアル

						Label1.Text = "正常に決済が終了しました。";
					}
					else
					{
						//FAILが返って来た処理（エラー処理）
						System.Array settelement_error = (System.Array )settelement_result["ERROR"];
						Label1.Text = "決済に異常が発生しました。" + (string)settelement_error.GetValue(0);
					}
				}
				else
				{
					//FAILが返って来た処理（エラー処理）
					System.Array settelement_error = (System.Array )settelement_result["ERROR"];
					Label1.Text = "決済に異常が発生しました。" + (string)settelement_error.GetValue(0);
				}

			}
			else
			{
				//FAILが返って来た処理（エラー処理）
				System.Array  validate_error = (System.Array )validate_result["ERROR"];
				Label1.Text = "決済に異常が発生しました。" + (string)validate_error.GetValue(0);
			}

		}

		//カードバリデーション
		private XmlRpcStruct validate_card(string hiragana,string orderid)
		{
			//XML_RPC用のハッシュ作成（通常のハッシュでは無い）
			XmlRpcStruct hashTable = new XmlRpcStruct();
			XmlRpcStruct ret = new XmlRpcStruct();

			//NAKA0351テスト用ひらがな
			//はろゆるなわりよむによさゆいるけ
			hashTable.Add("SHOP_ID","NAKA0351"); // Addメソッドで追加
			hashTable.Add("SHOP_PASSWD","naka4593"); // Addメソッドで追加
			hashTable.Add("ORDER_ID","orderid"); // Addメソッドで追加
			hashTable.Add("CARD_NUMBER",hiragana); // Addメソッドで追加
			hashTable.Add("RATING",19); // Addメソッドで追加
			hashTable.Add("PRICE",1); // Addメソッドで追加

			apiProxy = (Iparm)XmlRpcProxyGen.Create(typeof(Iparm));
			clientProtocol = (XmlRpcClientProtocol)apiProxy;
			clientProtocol.Url = "https://ssl.bitcash.co.jp/api/";

			try
			{
				//実処理
				ret = apiProxy.validate_card(hashTable);

			}
			catch (Exception ex)
			{
				HandleException(ex);
			}

			return ret;

		}

		//決済実行
		private XmlRpcStruct do_settlement(string bcsid,string orderid)
		{
			//XML_RPC用のハッシュ作成（通常のハッシュでは無い）
			XmlRpcStruct hashTable = new XmlRpcStruct();
			XmlRpcStruct ret = new XmlRpcStruct();

			//NAKA0351テスト用ひらがな
			//はろゆるなわりよむによさゆいるけ
			hashTable.Add("SHOP_ID","NAKA0351"); // Addメソッドで追加
			hashTable.Add("SHOP_PASSWD","naka4593"); // Addメソッドで追加
			hashTable.Add("BCS_ID",bcsid); // Addメソッドで追加
			hashTable.Add("ORDER_ID",orderid); // Addメソッドで追加
			apiProxy = (Iparm)XmlRpcProxyGen.Create(typeof(Iparm));
			clientProtocol = (XmlRpcClientProtocol)apiProxy;
			clientProtocol.Url = "https://ssl.bitcash.co.jp/api/";

			try
			{
				//実処理
				ret = apiProxy.do_settlement(hashTable);

			}
			catch (Exception ex)
			{
				HandleException(ex);
			}

			return ret;

		}

		//決済確認API
		private XmlRpcStruct confirm_settlement(int salesid,string orderid)
		{
			//XML_RPC用のハッシュ作成（通常のハッシュでは無い）
			XmlRpcStruct hashTable = new XmlRpcStruct();
			XmlRpcStruct ret = new XmlRpcStruct();

			hashTable.Add("SHOP_ID","NAKA0351"); // Addメソッドで追加
			hashTable.Add("SHOP_PASSWD","naka4593"); // Addメソッドで追加
			hashTable.Add("ORDER_ID","orderid"); // Addメソッドで追加
			hashTable.Add("SALES_ID",salesid); // Addメソッドで追加

			apiProxy = (Iparm)XmlRpcProxyGen.Create(typeof(Iparm));
			clientProtocol = (XmlRpcClientProtocol)apiProxy;
			clientProtocol.Url = "https://ssl.bitcash.co.jp/api/";

			try
			{
				//実処理
				ret = apiProxy.confirm_settlement(hashTable);

			}
			catch (Exception ex)
			{
				HandleException(ex);
			}

			return ret;

		}
		
		//インタフェース定義
		public interface Iparm
		{
			//カードバリデーション
			[XmlRpcMethod("Settlement.validate_card")]
			XmlRpcStruct validate_card(XmlRpcStruct a);		

			//決済実行
			[XmlRpcMethod("Settlement.do_settlement")]
			XmlRpcStruct do_settlement(XmlRpcStruct b);

			//決済確認
			[XmlRpcMethod("Settlement.confirm_settlement")]
			XmlRpcStruct confirm_settlement(XmlRpcStruct c);

		}

		//内部エラーハンドリング
		private string HandleException(Exception ex)
		{
			try
			{
				throw ex;
			}
			catch(XmlRpcFaultException fex)
			{
				return "Fault Response: " + fex.FaultCode + " " + fex.FaultString;
			}
			catch(XmlRpcServerException xmlRpcEx)
			{
				return "XmlRpcServerException: " + xmlRpcEx.Message;

			}
			catch(Exception defEx)
			{
				return "Exception: " + defEx.Message;

			}
		}

	}
}
