using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CookComputing.XmlRpc;

namespace sample_xml_rpc
{
	/// <summary>
	/// WebForm2 の概要の説明です。
	/// </summary>
	public class WebForm2 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label1;
//		private Dparm settlement_Proxy;
		private XmlRpcClientProtocol clientProtocol;

		private void Page_Load(object sender, System.EventArgs e)
		{

			string orderid = "test";

			//前の画面からの引継ぎ
			Hashtable validate_result = new Hashtable();
			validate_result = (Hashtable)Session["validate_ret"];

			Hashtable settelement_result = new Hashtable();

			//パラメータ取得
			string validate_bcsid =  (string)validate_result["BCS_ID"];
			int validate_logid =  (int)validate_result["LOG_ID"];
			int validate_balance =  (int)validate_result["BALANCE"];


			//決済実行
			//セッションに一度入れて、ページ間の受け渡しにも対応する。
			Session["settlement_ret"] = do_settlement(validate_bcsid,orderid);

			settelement_result = (Hashtable)Session["settlement_ret"];
			int settelement_salesid =  (int)settelement_result["SALES_ID"];
			string settelement_status =  (string)settelement_result["STATUS"];
			int settelement_logid =  (int)settelement_result["LOG_ID"];
			int settelement_balance =  (int)settelement_result["BALANCE"];

			// ページを初期化するユーザー コードをここに挿入します。
		}
//
//		private XmlRpcStruct do_settlement(string bcsid,string orderid)
//		{
//
//			XmlRpcStruct hashTable = new XmlRpcStruct();
//			XmlRpcStruct ret = new XmlRpcStruct();
//
//			//NAKA0351テスト用ひらがな
//			//はろゆるなわりよむによさゆいるけ
//			hashTable.Add("SHOP_ID","NAKA0351"); // Addメソッドで追加
//			hashTable.Add("SHOP_PASSWD","naka4593"); // Addメソッドで追加
//			hashTable.Add("BCS_ID",bcsid); // Addメソッドで追加
//			hashTable.Add("ORDER_ID",orderid); // Addメソッドで追加
//			settlement_Proxy = (Dparm)XmlRpcProxyGen.Create(typeof(Dparm));
//			clientProtocol = (XmlRpcClientProtocol)settlement_Proxy;
//			clientProtocol.Url = "https://ssl.bitcash.co.jp/api/";
//
//			try
//			{
//				ret = settlement_Proxy.do_settlement(hashTable);
//
//			}
//			catch (Exception ex)
//			{
//				HandleException(ex);
//			}
//
//			return ret;
//
//		}
//		
//		interface Dparm
//		{
//
//			[XmlRpcMethod("Settlement.do_settlement")]
//			XmlRpcStruct do_settlement(XmlRpcStruct b);
//
//		}

		private string HandleException(Exception ex)
		{
			try
			{
				throw ex;
			}
			catch(XmlRpcFaultException fex)
			{
				return "Fault Response: " + fex.FaultCode + " " + fex.FaultString;
			}
			catch(XmlRpcServerException xmlRpcEx)
			{
				return "XmlRpcServerException: " + xmlRpcEx.Message;

			}
			catch(Exception defEx)
			{
				return "Exception: " + defEx.Message;

			}
		}
		#region Web フォーム デザイナで生成されたコード 
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: この呼び出しは、ASP.NET Web フォーム デザイナで必要です。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
