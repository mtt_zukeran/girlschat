/*
 * AbstractBitCashTestCase.java
 */
package jp.co.bitcash.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.bitcash.BitCash;
import jp.co.bitcash.SettlementParam;
import jp.co.bitcash.SettlementResult;
import junit.framework.TestCase;

/**
 * Created on 2004/06/27 19:18:09
 * @since $Revision$ $Date$
 * @author akizuki
 */
public abstract class AbstractBitCashTestCase extends TestCase {

	protected static final String TEST_SHOP_ID			="ROOT0114";
	protected static final String TEST_SHOP_PASSWD	="ROOT0114";
	protected static final String TEST_CARD_NUMBER	="けうくひおぬのくすさわこやここけ";
	protected static final int TEST_RATING				= 99;
	protected static final int TEST_PRICE					= 100;
	protected static final String TEST_ORDER_ID			= "TEST"+new SimpleDateFormat("yyMMddHHmmss").format(new Date());

	protected static final String OTHER_SHOP_ID     = "NAKA0351";
	protected static final String OTHER_SHOP_PASSWD = "naka4593";


/*
	201\:stop_lot=ロットがストップ状態にある。(利用停止にされている)								●●●未テスト●●●テスト方法不明
	202\:stop_card=カードがストップ状態にある。(利用停止にされている)							●●●未テスト●●●テスト方法不明
	203\:stop_shop=ショップがストップ状態にある。(利用停止にされている)							●●●未テスト●●●テスト方法不明
	204\:before_campaign=キャンペーン期間前。														●●●未テスト●●●テスト方法不明
	205\:after_campaign=キャンペーン期間後。														●●●未テスト●●●テスト方法不明
	206\:balance_over=商品価格よりBitCashの残高が足りない。									testValidateCardLargePrice
	207\:already_deposit=締日を過ぎた後に、該当の決済をキャンセル処理をしようとした場合。	●●●未テスト●●●テスト方法不明
	208\:already_canceled=該当の決済は既にキャンセル済み。									testUndoSettlementTwice, testUndoSettlementTwiceOtherOrderid
	301\:no_shop_id=SHOP_IDが入力されていない。													testValidateCardEmptyParam, testValidateCardNoShopID,
																												testDoSettlementEmptyParam, testDoSettlementNoShopID,
																												testUndoSettlementEmptyParam, testUndoSettlementNoShopID
	302\:no_shop_passwd=SHOP_PASSWORDが入力されていない。								testValidateCardEmptyParam, testValidateCardNoShopPasswd,
																												testDoSettlementEmptyParam, testValidateCardNoShopPasswd,
																												testUndoSettlementEmptyParam, testUndoSettlementNoShopPasswd
	303\:no_sales_id=SALES_IDが入力されていない。												testUndoSettlementOKShopIDPasswd
	304\:no_bcs_id=BCS_IDが入力されていない。														testDoSettlementOKShopIDPasswd
	305\:no_card_number=CARD_NUMBERが入力されていない。									testValidateCardOKShopIDPasswd, testValidateCardNoCardnumber
	306\:no_rating=RATINGが入力されていない。														testValidateCardOKShopIDPasswd, testValidateCardNoRating
	307\:no_price=PRICEが入力されていない。														testValidateCardOKShopIDPasswd, testValidateCardNoPrice, testValidateCardZeroPrice
	351\:bad_shop_id=SHOP_IDが不正。																testValidateCardBadShopIDPasswd,
																												testDoSettlementBadShopid,
																												testUndoSettlementBadShopid
	352\:bad_shop_passwd=SHOP_PASSWORDが不正。											testValidateCardBadPasswd
	353\:mismatch_shop_id=ショップIDが売り上げレコードと一致しない。（認証と、決済時のIDが異なる。)	●●●未テスト●●●DoSettlementのテスト方法不明　テストカードではテストできない？
																												UndoSettlementは、testUndoSettlementBadSalesidExist, testUndoSettlementOtherShopid, testUndoSettlementTwiceOtherShopidでテスト済み
	354\:bad_card_number=カードが不正。（ひらがな情報の入力間違い。）						testValidateCardBadCardnumber
	355\:bad_rating_card=カードのレイティングが不正。（ＥＸ・標準・Ｋｉｄｓが異なる。）			●●●未テスト●●●テスト方法不明　テストカードではテストできない？
	356\:bad_rating_shop=ショップのレイティングが不正。												testValidateCardBadRating
	357\:mismatch_trn_shop_id=テストカードにおいてショップＩＤがカードと合わない。				testValidateCardOtherShopidTestCard
	358\:bad_campaign_lot_info=カードがキャンペーンであるが、店舗がキャンペーンではない。	●●●未テスト●●●テスト方法不明
	359\:bad_lot_id=ロットが不正。																		●●●未テスト●●●テスト方法不明
	360\:bad_campaign_shop=店舗がキャンペーンであるが、カードがキャンペーンではない。		●●●未テスト●●●テスト方法不明
	361\:bad_sales_id=売り上げのレコードが存在しない。											testUndoSettlementBadSalesidNotExist
	501\:fail_make_session=トランザクションの作成に失敗。											●●●未テスト●●●テスト方法不明
	502\:cannot_get_session=トランザクションレコードの取得ができない。							testDoSettlementOKIDPasswd, testDoSettlementBadBcsid, testDoSettlementBadBcsidOrderid
	503\:already_settled=すでに決済は終了している。												testDoSettlementTwice, testDoSettlementTwiceOtherOrderid, testDoSettlementTwiceOtherShopid
	504\:failed_insert_sales_info=売上レコード(該当の決済履歴)の作成に失敗。				●●●未テスト●●●
	506\:no_rec_card_info=カードのレコードが存在しない。											●●●未テスト●●●
	507\:no_rec_lot_info=ロットのレコードが存在しない。												●●●未テスト●●●
	508\:no_rec_shop_info=ショップのレコードが存在しない。											●●●未テスト●●●
*/

	protected void printLineNumber(){
		Throwable e=new Throwable();
		StackTraceElement[] ste=e.getStackTrace();
		System.out.println(ste[2].toString());
	}
		
	protected void printParamResult(SettlementParam param,SettlementResult result,String msg){
		printLineNumber();
		if(msg!=null && !msg.equals("")) System.out.println(msg);
		System.out.println(param.toString());
		System.out.println(result.toString());
	}
	
	protected String getOkBcsid(){
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		System.out.println("bcsid="+result.getBcsid());
		return result.getBcsid();
	}
	
	protected int getOkSalesid(){
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		SettlementResult result=BitCash.ValidateCard(param);
		param.setBcsid(result.getBcsid());
		result=BitCash.DoSettlement(param);
		System.out.println("salesid="+result.getSalesid());
		return result.getSalesid();
	}
}