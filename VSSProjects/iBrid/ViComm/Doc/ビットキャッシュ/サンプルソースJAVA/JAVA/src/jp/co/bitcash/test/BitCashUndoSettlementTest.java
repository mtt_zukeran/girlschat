/*
 * BitCashUndoSettlementTest.java
 */
package jp.co.bitcash.test;

import jp.co.bitcash.BitCash;
import jp.co.bitcash.SettlementParam;
import jp.co.bitcash.SettlementResult;

/**
 * BitCash.UndoSettlementテストクラス
 * Created on 2004/06/27 19:16:46
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class BitCashUndoSettlementTest extends AbstractBitCashTestCase {

	public void testUndoSettlementNullParam() {
		SettlementParam param=null;
		SettlementResult result=BitCash.UndoSettlement(param);
		assertNull(result);//NULLを渡すとNULLが返る
	}

	public void testUndoSettlementEmptyParam() {
		SettlementParam param=new SettlementParam();
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"空のパラメータで渡すと[301:no_shop_id, 302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id, 302:no_shop_passwd]",result.getError().toString());
	}

	public void testUndoSettlementNoShopID() {
		SettlementParam param=new SettlementParam();
		//param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"SHOP_IDを指定しない[301:no_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id]",result.getError().toString());
	}

	public void testUndoSettlementNoShopPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		//param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"SHOP_PASSWDを指定しない[302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[302:no_shop_passwd]",result.getError().toString());
	}

	public void testUndoSettlementOKShopIDPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"正しいSHOP_ID,正しいSHOP_PASSWDを渡すと[303:no_sales_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[303:no_sales_id]",result.getError().toString());
	}

	public void testUndoSettlementBadSalesidExist() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setSalesid(1);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"間違ったSALES_IDを渡すと[353:mismatch_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[353:mismatch_shop_id]",result.getError().toString());
	}

	public void testUndoSettlementBadSalesidNotExist() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setSalesid(-1);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"存在しないSALES_IDを渡すと[361:bad_sales_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[361:bad_sales_id]",result.getError().toString());
	}

	public void testUndoSettlementOK() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setSalesid(salesid);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"すべて正しいパラメータを渡すとエラーなし");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testUndoSettlementOnlySalesid() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		//param.setShopid(OTHER_SHOP_ID);
		//param.setShoppassword(OTHER_SHOP_PASSWD);
		param.setSalesid(salesid);
		//param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"SALES_IDのみを渡すと[301:no_shop_id, 302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id, 302:no_shop_passwd]",result.getError().toString());
	}

	public void testUndoSettlementBadShopID() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID+"ERROR");
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setSalesid(salesid);
		//param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"存在しないSHOP_IDを渡すと[351:bad_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[351:bad_shop_id]",result.getError().toString());
	}

	public void testUndoSettlementBadShopPasswd() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD+"ERROR");
		param.setSalesid(salesid);
		//param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"正しくないSHOP_PASSWDを渡すと[352:bad_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[352:bad_shop_passwd]",result.getError().toString());
	}

	public void testUndoSettlementOtherShopid() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		param.setShopid(OTHER_SHOP_ID);
		param.setShoppassword(OTHER_SHOP_PASSWD);
		param.setSalesid(salesid);
		//param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"存在する他のSHOP_IDを渡すと[353:mismatch_shop_id]");
		assertEquals("FAIL",result.getStatus());//OK
		assertEquals("[353:mismatch_shop_id]",result.getError().toString());
	}

	public void testUndoSettlementTwice() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setSalesid(salesid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"");
		result=BitCash.UndoSettlement(param);//同じパラメータでもう一度UndoSettlement呼び出し
		printParamResult(param,result,"同じsalesidで決済取消しようとすると[208:already_canceled]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[208:already_canceled]",result.getError().toString());
	}

	public void testUndoSettlementTwiceOtherOrderid() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setSalesid(salesid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"");
		param.setOrderid(TEST_ORDER_ID+"ERROR");
		result=BitCash.UndoSettlement(param);//SALES_IDはそのままで、ORDER_IDを変えてもう一度UndoSettlement呼び出し
		printParamResult(param,result,"ORDER_IDを変えて同じsalesidで決済取消しようとすると[208:already_canceled]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[208:already_canceled]",result.getError().toString());
		//結論 UndoSettlementではORDER_IDの比較をしない
	}

	public void testUndoSettlementTwiceOtherShopid() {
		int salesid=getOkSalesid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setSalesid(salesid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.UndoSettlement(param);
		printParamResult(param,result,"");
		param.setShopid(OTHER_SHOP_ID);
		param.setShoppassword(OTHER_SHOP_PASSWD);
		result=BitCash.UndoSettlement(param);//SALES_IDはそのままで、SHOP_IDを変えてもう一度DoSettlement呼び出し
		printParamResult(param,result,"SHOP_IDを変えて同じsalesidで決済取消しようとすると[353:mismatch_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[353:mismatch_shop_id]",result.getError().toString());
		//結論 UndoSettlementではSALES_IDよりもSHOP_IDを先にチェックする
	}
}