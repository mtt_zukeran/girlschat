/*
 * SettlementResult.java
 */
package jp.co.bitcash;

import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

/**
 * BitCash戻り値クラス
 * Created on 2004/06/23 23:21:18
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class SettlementResult {

	/** XMLRPCの戻り値(Hashtable型)をクラスに変換 */
    public static SettlementResult parse(Hashtable table) {
		SettlementResult result=new SettlementResult();
		result.error=(Vector)table.get("ERROR");
		result.status=(String)table.get("STATUS");
		Integer _logid=(Integer)table.get("LOG_ID");
		if(_logid!=null) result.logid=_logid.intValue();
		Integer _balance=(Integer)table.get("BALANCE");
		if(_balance!=null) result.balance=_balance.intValue();
		result.bcsid=(String)table.get("BCS_ID");
		Integer _salesid=(Integer)table.get("SALES_ID");
		if(_salesid!=null) result.salesid=_salesid.intValue();
		return result;
    }

	/** 状態表示 */
	public String toString(){
		StringBuffer s=new StringBuffer();
		s.append("Result=");
		s.append("STATUS: "	+ status		+", ");
		s.append("LOG_ID: "		+ logid		+", ");
		s.append("BALANCE: "	+ balance	+", ");
		s.append("BCS_ID: "		+ bcsid		+", ");
		s.append("SALES_ID: "	+ salesid	+", ");
		s.append("\n\t");
		s.append("ERROR: "		+ error		+", ");
		s.append("\n");
		for(int i=0;i<error.size();i++){
			s.append("\tERROR["+i+"]="+getErrormessage((String)error.get(i))+", \n");
		}
		return s.toString();
	}

	/** 日本語のエラーメッセージを取得 */
	private static ResourceBundle bundle;
	public String getErrormessage(String key){
		if(bundle==null){
			bundle=ResourceBundle.getBundle("jp.co.bitcash.BitCash");
		}
		return bundle.getString(key);
	}

	private Vector error;
	private String status;
	private int logid;
	private int balance;
	private String bcsid;
	private int salesid; 

	public Vector getError(){ return error; }
	public void setError(Vector vector) { error = vector; }
	public String getStatus(){ return status; }
	public void setStatus(String string) { status = string; }
	public int getLogid(){ return logid; }
	public void setLogid(int i) { logid = i; }
	public int getBalance(){ return balance; }
	public void setBalance(int i) { balance = i; }
	public String getBcsid(){ return bcsid; }
	public void setBcsid(String string) { bcsid = string; }
	public int getSalesid(){ return salesid; }
	public void setSalesid(int i) { salesid = i; }
}