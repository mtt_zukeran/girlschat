/*
 * BitCashValidateCardTest.java
 */
package jp.co.bitcash.test;

import jp.co.bitcash.BitCash;
import jp.co.bitcash.SettlementParam;
import jp.co.bitcash.SettlementResult;

/**
 * BitCash.ValidateCardテストクラス
 * Created on 2004/06/27 19:15:48
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class BitCashValidateCardTest extends AbstractBitCashTestCase {

	public void testValidateCardOK() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"すべて正しいパラメータを渡すとエラーなし");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testValidateCardNullParam() {
		SettlementParam param=null;
		SettlementResult result=BitCash.ValidateCard(param);
		assertNull(result);//NULLを渡すとNULLが返る
	}

	public void testValidateCardEmptyParam() {
		SettlementParam param=new SettlementParam();
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"空のパラメータで渡すと[301:no_shop_id, 302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id, 302:no_shop_passwd]",result.getError().toString());
	}

	public void testValidateCardNoShopID() {
		SettlementParam param=new SettlementParam();
		//param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"SHOP_IDを指定しない[301:no_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id]",result.getError().toString());
	}

	public void testValidateCardNoShopPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		//param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"SHOP_PASSWDを指定しない[302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[302:no_shop_passwd]",result.getError().toString());
	}

	public void testValidateCardOKShopIDPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"正しいSHOP_ID,正しいSHOP_PASSWDを渡すと[305:no_card_number, 306:no_rating, 307:no_price]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[305:no_card_number, 306:no_rating, 307:no_price]",result.getError().toString());
	}

	public void testValidateCardNoCardnumber() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		//param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"カード番号を指定しない場合[305:no_card_number]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[305:no_card_number]",result.getError().toString());
	}

	public void testValidateCardNoRating() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		//param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"RATINGを指定しない場合[306:no_rating]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[306:no_rating]",result.getError().toString());
	}

	public void testValidateCardNoPrice() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		//param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"PRICEを指定しない場合[307:no_price]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[307:no_price]",result.getError().toString());
	}

	public void testValidateCardBadShopIDPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID+"ERROR");
		param.setShoppassword(TEST_SHOP_PASSWD+"ERROR");
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"正しくないSHOP_ID,SHOP_PASSWDを指定すると[351:bad_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[351:bad_shop_id]",result.getError().toString());
	}

	public void testValidateCardBadShopID() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID+"ERROR");
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"正しくないSHOP_IDを指定すると[351:bad_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[351:bad_shop_id]",result.getError().toString());
	}

	public void testValidateCardBadShopPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD+"ERROR");
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"正しくないSHOP_PASSWDを指定すると[352:bad_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[352:bad_shop_passwd]",result.getError().toString());
	}

	public void testValidateCardBadCardnumber() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER+"ERROR");
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"間違ったカード番号を指定すると[354:bad_card_number]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[354:bad_card_number]",result.getError().toString());
	}

	public void testValidateCardBadRating() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING+100);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"間違ったRATINGを指定すると[356:bad_rating_shop]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[356:bad_rating_shop]",result.getError().toString());
	}

	public void testValidateCardZeroPrice() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE*0);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"0円決済の場合[307:no_price]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[307:no_price]",result.getError().toString());
	}

	public void testValidateCardMinusPrice() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE*(-1));
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"マイナスの金額を指定してもエラーは出ない");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testValidateCardLargePrice() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE*1000000);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"残高オーバーの場合は[206:balance_over]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[206:balance_over]",result.getError().toString());
	}

	public void testValidateCardOtherShopidTestCard() {
		SettlementParam param=new SettlementParam();
		param.setShopid(OTHER_SHOP_ID);
		param.setShoppassword(OTHER_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"テストカードで無関係の存在するSHOP_IDを指定すると[357:mismatch_trn_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[357:mismatch_trn_shop_id]",result.getError().toString());
	}
}