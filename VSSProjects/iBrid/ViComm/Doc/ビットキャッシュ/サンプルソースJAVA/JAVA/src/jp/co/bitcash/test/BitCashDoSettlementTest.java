/*
 * BitCashDoSettlementTest.java
 */
package jp.co.bitcash.test;

import jp.co.bitcash.BitCash;
import jp.co.bitcash.SettlementParam;
import jp.co.bitcash.SettlementResult;

/**
 * BitCash.DoSettlementテストクラス
 * Created on 2004/06/27 19:16:21
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class BitCashDoSettlementTest extends AbstractBitCashTestCase {

	public void testDoSettlementNullParam() {
		SettlementParam param=null;
		SettlementResult result=BitCash.DoSettlement(param);
		assertNull(result);//NULLを渡すとNULLが返る
	}

	public void testDoSettlementEmptyParam() {
		SettlementParam param=new SettlementParam();
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"空のパラメータで渡すと[301:no_shop_id, 302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id, 302:no_shop_passwd]",result.getError().toString());
	}

	public void testDoSettlementNoShopID() {
		SettlementParam param=new SettlementParam();
		//param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"SHOP_IDを指定しない[301:no_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id]",result.getError().toString());
	}

	public void testDoSettlementNoShopPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		//param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"SHOP_PASSWDを指定しない[302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[302:no_shop_passwd]",result.getError().toString());
	}

	public void testDoSettlementOKShopIDPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"正しいSHOP_ID,正しいSHOP_PASSWDを渡すと[304:no_bcs_id, 502:cannot_get_session]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[304:no_bcs_id, 502:cannot_get_session]",result.getError().toString());
	}

	public void testDoSettlementBadBcsid() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid("BADBCSID");
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"間違ったBCS_IDを渡すと[502:cannot_get_session]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[502:cannot_get_session]",result.getError().toString());
	}

	public void testDoSettlementBadBcsidOrderid() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid("BADBCSID");
		param.setOrderid("BADORDER");
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"間違ったBCS_IDを渡すと[502:cannot_get_session]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[502:cannot_get_session]",result.getError().toString());
	}

	public void testDoSettlementOK() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid(bcsid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"すべて正しいパラメータを渡すとエラーなし");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testDoSettlementOnlyBcsid() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		//param.setShopid(OTHER_SHOP_ID);
		//param.setShoppassword(OTHER_SHOP_PASSWD);
		param.setBcsid(bcsid);
		//param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"BCS_IDのみを渡すと[301:no_shop_id, 302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id, 302:no_shop_passwd]",result.getError().toString());
	}

	public void testDoSettlementBadShopID() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID+"ERROR");
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid(bcsid);
		//param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"存在しないSHOPIDを渡すと[351:bad_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[351:bad_shop_id]",result.getError().toString());
	}

	public void testDoSettlementBadShopPasswd() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD+"ERROR");
		param.setBcsid(bcsid);
		//param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"正しくないSHOP_PASSWDを渡すと[352:bad_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[352:bad_shop_passwd]",result.getError().toString());
	}

	public void testDoSettlementOtherShopid() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(OTHER_SHOP_ID);
		param.setShoppassword(OTHER_SHOP_PASSWD);
		param.setBcsid(bcsid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"存在する他のSHOPIDを渡してもエラーなし"); 
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testDoSettlementBadOrderid() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid(bcsid);
		param.setOrderid((new java.util.Date().toString())+TEST_ORDER_ID+"ERROR");
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"間違ったORDERIDを渡してもエラーなし\n");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testDoSettlementTwice() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid(bcsid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"");
		result=BitCash.DoSettlement(param);//同じパラメータでもう一度DoSettlement呼び出し
		printParamResult(param,result,"同じbcsidで決済しようとすると[503:alaeady_settled]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[503:alaeady_settled]",result.getError().toString());
	}

	public void testDoSettlementTwiceOtherOrderid() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid(bcsid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"");
		param.setOrderid(TEST_ORDER_ID+"ERROR");
		result=BitCash.DoSettlement(param);//BCS_IDはそのままで、ORDER_IDを変えてもう一度DoSettlement呼び出し
		printParamResult(param,result,"ORDER_IDを変えて同じbcsidで決済しようとすると[503:alaeady_settled]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[503:alaeady_settled]",result.getError().toString());
		//結論 DoSettlementではORDER_IDの比較をしない
	}

	public void testDoSettlementTwiceOtherShopid() {
		String bcsid=getOkBcsid();
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setBcsid(bcsid);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.DoSettlement(param);
		printParamResult(param,result,"");
		param.setShopid(OTHER_SHOP_ID);
		param.setShoppassword(OTHER_SHOP_PASSWD);
		result=BitCash.DoSettlement(param);//BCS_IDはそのままで、SHOP_IDを変えてもう一度DoSettlement呼び出し
		printParamResult(param,result,"SHOP_IDを変えて同じbcsidで決済しようとすると[503:alaeady_settled]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[503:alaeady_settled]",result.getError().toString());
		//結論 DoSettlementではSHOP_IDよりもBCS_IDを先にチェックする SHOP_IDの比較をしない？
	}
}