/*
 * AllTests.java
 */
package jp.co.bitcash.test;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Created on 2004/06/27 19:24:58
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for jp.co.bitcash");
		//$JUnit-BEGIN$
		suite.addTest(new TestSuite(BitCashValidateCardTest.class));
		suite.addTest(new TestSuite(BitCashDoSettlementTest.class));
		suite.addTest(new TestSuite(BitCashUndoSettlementTest.class));
		suite.addTest(new TestSuite(BitCashSettlementTest.class));
		//$JUnit-END$
		return suite;
	}
}
