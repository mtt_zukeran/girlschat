/*
 * BitCash.java
 */
package jp.co.bitcash;

import java.util.Hashtable;
import java.util.Vector;

import org.apache.xmlrpc.XmlRpc;
import org.apache.xmlrpc.XmlRpcClient;

/**
 * BitCash XML-RPCラッパークラス
 * Created on 2004/06/23 20:46:55
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class BitCash {

	/** カードのバリデーション */
	public static SettlementResult ValidateCard(SettlementParam param){
		if(param==null) return null;
		return call(METHODNAME_VALIDATECARD,param.getValidateCardParam());
	}

	/** 決済実行 */
	public static SettlementResult DoSettlement(SettlementParam param){
		if(param==null) return null;
		return call(METHODNAME_DOSETTLEMENT,param.getDoSettlementParam());
	}

	/** 決済取消実行 */
	public static SettlementResult UndoSettlement(SettlementParam param){
		if(param==null) return null;
		return call(METHODNAME_UNDOSETTLEMENT,param.getUndoSettlementParam());
	}

	/** カードのバリデーションの後、決済実行 */
	public static SettlementResult Settlement(SettlementParam param){
		SettlementResult result=ValidateCard(param);
		if(result==null) return result;
		if(result.getStatus()==null) return result;
		if(!result.getStatus().equals("OK")) return result;
		param.setBcsid(result.getBcsid());

		result=DoSettlement(param);
		if(result==null) return result;
		if(result.getStatus()==null) return result;
		if(!result.getStatus().equals("OK")) return result;
		param.setSalesid(result.getSalesid());

		return result;
	}

	private static final String BITCASHURL = "https://ssl.bitcash.co.jp/api/";
	private static final String BITCASHURL_TEST = "http://t.bitcash.co.jp/api/";

	private static final String METHODNAME_VALIDATECARD="Settlement.validate_card";
	private static final String METHODNAME_DOSETTLEMENT="Settlement.do_settlement";
	private static final String METHODNAME_UNDOSETTLEMENT="Settlement.undo_settlement";

	/** XMLRPC呼び出し */
	private static SettlementResult call(String method,Hashtable paramtable)
	{
		SettlementResult result=null;
		try {
			XmlRpc.setEncoding("UTF-8");
			XmlRpcClient client=new XmlRpcClient(BITCASHURL);
			Vector params=new Vector();
			params.add(paramtable);
			Hashtable table=(Hashtable)client.execute(method,params);
			result=SettlementResult.parse(table);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}