/*
 * SettlementParam.java
 */
package jp.co.bitcash;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;

/**
 * BitCashパラメータクラス
 * Created on 2004/06/24 1:23:08
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class SettlementParam {

	/** カードのバリデーションのためのパラメータHashtable生成 */
	public Hashtable getValidateCardParam(){
		Hashtable result=new Hashtable();
		if(shopid!=null) result.put("SHOP_ID",shopid);
		if(shoppassword!=null) result.put("SHOP_PASSWD",shoppassword);
		if(cardnumber!=null){
			try{
				result.put("CARD_NUMBER",cardnumber.getBytes("UTF-8"));
			}catch(UnsupportedEncodingException e){
				e.printStackTrace();
			}
		}
		result.put("RATING",new Integer(rating));
		result.put("PRICE",new Integer(price));
		if(orderid!=null) result.put("ORDER_ID",orderid);
		return result;
	}

	/** 決済実行のためのパラメータHashtable生成 */
	public Hashtable getDoSettlementParam(){
		Hashtable result=new Hashtable();
		if(shopid!=null) result.put("SHOP_ID",shopid);
		if(shoppassword!=null) result.put("SHOP_PASSWD",shoppassword);
		if(bcsid!=null) result.put("BCS_ID",bcsid);
		if(orderid!=null) result.put("ORDER_ID",orderid);
		return result;
	}

	/** 決済取消実行のためのパラメータHashtable生成 */
	public Hashtable getUndoSettlementParam() {
		Hashtable result=new Hashtable();
		if(shopid!=null) result.put("SHOP_ID",shopid);
		if(shoppassword!=null) result.put("SHOP_PASSWD",shoppassword);
		result.put("SALES_ID",new Integer(salesid));
		if(orderid!=null) result.put("ORDER_ID",orderid);
		return result;
	}

	public String toString(){
		StringBuffer s=new StringBuffer();
		s.append("Param=");
		if(shopid!=null)			s.append("SHOP_ID: "		+ shopid			+", ");
		if(shoppassword!=null)	s.append("SHOP_PASSWD: "+ shoppassword+", ");
		if(cardnumber!=null) 	s.append("CARD_NUMBER: "+ cardnumber	+", ");
		s.append("\n\t");
		if(rating!=0) 				s.append("RATING: "			+ rating			+", ");
		if(price!=0) 				s.append("PRICE: "			+ price			+", ");
		if(orderid!=null) 			s.append("ORDER_ID: "		+ orderid		+", ");
		if(bcsid!=null) 			s.append("BCS_ID: "			+ bcsid			+", ");
		if(salesid!=0) 				s.append("SALES_ID: "		+ salesid		+", ");
		return s.toString();
	}

	String shopid;
	String shoppassword;
	String cardnumber;
	int rating;
	int price;
	String orderid;
	String bcsid;
	int salesid;

	public String getBcsid() { return bcsid; }
	public void setBcsid(String string) { bcsid = string; }
	public String getCardnumber() { return cardnumber; }
	public void setCardnumber(String string) { cardnumber = string; }
	public String getOrderid() { return orderid; }
	public void setOrderid(String string) { orderid = string; }
	public int getPrice() { return price; }
	public void setPrice(int i) { price = i; }
	public int getRating() { return rating; }
	public void setRating(int i) { rating = i; }
	public int getSalesid() { return salesid; }
	public void setSalesid(int i) { salesid = i; }
	public String getShopid() { return shopid; }
	public void setShopid(String string) { shopid = string; }
	public String getShoppassword() { return shoppassword; }
	public void setShoppassword(String string) { shoppassword = string; }
}