/*
 * BitCashSettlementTest.java
 */
package jp.co.bitcash.test;

import jp.co.bitcash.BitCash;
import jp.co.bitcash.SettlementParam;
import jp.co.bitcash.SettlementResult;

/**
 * BitCash.Settlementテストクラス
 * Created on 2004/06/27 19:16:21
 * @since $Revision$ $Date$
 * @author akizuki
 */
public class BitCashSettlementTest extends AbstractBitCashTestCase {

	public void testSettlementOK() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.ValidateCard(param);
		printParamResult(param,result,"すべて正しいパラメータを渡すとエラーなし");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testSettlementNullParam() {
		SettlementParam param=null;
		SettlementResult result=BitCash.Settlement(param);
		assertNull(result);//NULLを渡すとNULLが返る
	}

	public void testSettlementEmptyParam() {
		SettlementParam param=new SettlementParam();
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"空のパラメータで渡すと[301:no_shop_id, 302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id, 302:no_shop_passwd]",result.getError().toString());
	}

	public void testSettlementNoShopID() {
		SettlementParam param=new SettlementParam();
		//param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"SHOP_IDを指定しない[301:no_shop_id]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[301:no_shop_id]",result.getError().toString());
	}

	public void testSettlementNoShopPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		//param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"SHOP_PASSWDを指定しない[302:no_shop_passwd]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[302:no_shop_passwd]",result.getError().toString());
	}

	public void testSettlementOKShopIDPasswd() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"正しいSHOP_ID,正しいSHOP_PASSWDを渡すと[305:no_card_number, 306:no_rating, 307:no_price]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[305:no_card_number, 306:no_rating, 307:no_price]",result.getError().toString());
	}

	public void testSettlementNoCardnumber() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		//param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"カード番号を指定しない場合[305:no_card_number]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[305:no_card_number]",result.getError().toString());
	}

	public void testSettlementNoRating() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		//param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"RATINGを指定しない場合[306:no_rating]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[306:no_rating]",result.getError().toString());
	}

	public void testSettlementNoPrice() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		//param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"PRICEを指定しない場合[307:no_price]");
		assertEquals("FAIL",result.getStatus());//FAIL
		assertEquals("[307:no_price]",result.getError().toString());
	}

	public void testSettlementTwice() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"");
		result=BitCash.Settlement(param);//同じパラメータでもう一度Settlement呼び出し
		printParamResult(param,result,"同じパラメータで決済してもOK");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
	}

	public void testSettlementTwiceOtherOrderid() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"");
		param.setOrderid(TEST_ORDER_ID+"ERROR");
		result=BitCash.Settlement(param);//同じパラメータでもう一度Settlement呼び出し
		printParamResult(param,result,"ORDER_IDを変えて同じパラメータで決済してもOK 別決済扱い");
		assertEquals("OK",result.getStatus());//OK
		assertEquals("[]",result.getError().toString());
		//結論 SettlementではORDER_IDの比較をしない
	}

	public void testSettlementTwiceOtherShopid() {
		SettlementParam param=new SettlementParam();
		param.setShopid(TEST_SHOP_ID);
		param.setShoppassword(TEST_SHOP_PASSWD);
		param.setCardnumber(TEST_CARD_NUMBER);
		param.setRating(TEST_RATING);
		param.setPrice(TEST_PRICE);
		param.setOrderid(TEST_ORDER_ID);
		SettlementResult result=BitCash.Settlement(param);
		printParamResult(param,result,"");
		param.setShopid(OTHER_SHOP_ID);
		param.setShoppassword(OTHER_SHOP_PASSWD);
		result=BitCash.Settlement(param);//同じパラメータでもう一度Settlement呼び出し
		printParamResult(param,result,"SHOP_IDを変えて同じパラメータで決済すると、別決済扱いになる [357:mismatch_trn_shop_id]");
		assertEquals("FAIL",result.getStatus());//OK
		assertEquals("[357:mismatch_trn_shop_id]",result.getError().toString());
	}

}