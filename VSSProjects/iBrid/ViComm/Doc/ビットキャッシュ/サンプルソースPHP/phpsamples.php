<html>
<head>
<title>ビットキャシュ決済サンプルコード</title>
<body bgcolor="#FFFFFF" alink="#008000" vlink="#800080" link="#0000FF">
<meta http-equiv="Content-Type" content="text/html; charset=SHIFT_JIS">

<FORM action="./phpsamples.php" method="POST">
<INPUT type="text" name="CARD_NUMBER" size="30" maxlength="32">
<input type="submit">
</FORM>
１：カード判定<BR>
<?php
$CARD_NUMBER = $_POST{"CARD_NUMBER"};

$mt_host = "ssl.bitcash.co.jp";             //bitcashサーバーDOMEIN
$mt_xmlrpc_path = "/api";                   //bitcashプログラムPATH
$SHOP_ID     = "ABCDEFGH";                  //SHOP_ID
$SHOP_PASSWD = "abcdefgh";                  //SHOP_PASSWD
//XMLRPC通信時、送信文字列が日本語の場合、UTF-8に変換する
$CARD_NUMBER = mb_convert_encoding($CARD_NUMBER,"UTF-8","SJIS");
$RATING      = "19";                        //STD=19 EX=99 KIDS=12
$PRICE       = "1";                         //金額
$ORDER_ID    = "";

if ($CARD_NUMBER <> ""){
  $GLOBALS['xmlrpc_defencoding'] = "UTF-8";
  require_once("mb_xmlrpc.inc");
  //XML-RPCインタフェース、ユーザ定義

  //日本語文字列はUTF-8に変換

  //クライアントの作成
  $c = new xmlrpc_client( $mt_xmlrpc_path, $mt_host, 'https' );

  //XMLコード変換
  $SHOP_ID_val     = new mb_xmlrpcval( $SHOP_ID,     'string' );
  $SHOP_PASSWD_val = new mb_xmlrpcval( $SHOP_PASSWD, 'string' );
  $CARD_NUMBER_val = new mb_xmlrpcval( $CARD_NUMBER, 'string' );
  $RATING_val      = new mb_xmlrpcval( $RATING,      'int'    );
  $PRICE_val       = new mb_xmlrpcval( $PRICE,       'int'    );
  $ORDER_ID_val    = new mb_xmlrpcval( $ORDER_ID,    'base64' );
  //受け渡し用ハッシュ作成
  $struct = new mb_xmlrpcval(array("SHOP_ID"     => $SHOP_ID_val,
			"SHOP_PASSWD" => $SHOP_PASSWD_val,
			"CARD_NUMBER" => $CARD_NUMBER_val,
			"RATING"      => $RATING_val,
			"PRICE"       => $PRICE_val,
			"ORDER_ID"    => $ORDER_ID_val), "struct");

  //メッセージ作成
  $message = new xmlrpcmsg(
		'Settlement.validate_card',
		array($struct)
  );

  //メッセージ送信
  $result = $c->send($message);
  //ConnectionErrorCheck
  if( !$result ){
 	exit('Could not connect to the server.');
  }else if( $result->faultCode() ){
 	exit('XML-RPC fault ('.$result->faultCode().'): '
 		.$result->faultString());
  }
    $struct = $result->value();
    $ERRORval   = $struct->structmem('ERROR');
    $ERROR      = $ERRORval->scalarval();

    $STATUSval  = $struct->structmem('STATUS');
    $STATUS     = $STATUSval->scalarval();

    if ($STATUS != 'OK'){
      echo "<pre>";
      print_r($ERROR);
      echo "</pre>";
      exit;
    }

    $BCS_IDval  = $struct->structmem('BCS_ID');
    $BCS_ID     = $BCS_IDval->scalarval();

    $BALANCEval = $struct->structmem('BALANCE');
    $BALANCE    = $BALANCEval->scalarval();

    $LOG_IDval  = $struct->structmem('LOG_ID');
    $LOG_ID     = $LOG_IDval->scalarval();


    print "STATUS ::" . $STATUS  . "<BR>\n";
    print "LOG_ID ::" . $LOG_ID  . "<BR>\n";
    print "BCS_ID ::" . $BCS_ID  . "<BR>\n";
    print "BALANCE::" . $BALANCE . "<BR><BR>\n";
}
?>
２：決済<BR>
<?php
if ($STATUS == "OK"){
//	$SHOP_ID     = "ABSD1234";
//	$SHOP_PASSWD = "abcdefgh";
	$ORDER_ID    = "";
	$BCS_ID      = $BCS_ID;                 //カード判定の戻り値

	$SHOP_ID_val     = new xmlrpcval( $SHOP_ID,     'string' );
	$SHOP_PASSWD_val = new xmlrpcval( $SHOP_PASSWD, 'string' );
	$ORDER_ID_val    = new xmlrpcval( $ORDER_ID,    'base64' );
	$BCS_ID_val      = new xmlrpcval( $BCS_ID,      'string' );
	$struct = new xmlrpcval(array("SHOP_ID"     => $SHOP_ID_val,
			"SHOP_PASSWD" => $SHOP_PASSWD_val,
			"BCS_ID"      => $BCS_ID_val,
			"ORDER_ID"    => $ORDER_ID_val), "struct");
	//メッセージ作成
	$message = new xmlrpcmsg(
		'Settlement.do_settlement',
		array($struct)
	);
	//メッセージ送信
	$result = $c->send($message);
	if( !$result ){
		exit('Could not connect to the server.');
	}else if( $result->faultCode() ){
		exit('XML-RPC fault ('.$result->faultCode().'): '
		.$result->faultString());
	}

	$struct = $result->value();
	$ERRORval   = $struct->structmem('ERROR');
	$ERROR      = $ERRORval->scalarval();
    
	$STATUSval  = $struct->structmem('STATUS');
    $STATUS     = $STATUSval->scalarval();

    if ($STATUS != 'OK'){
      echo "<pre>";
      print_r($ERROR);
      echo "</pre>";
      exit;
    }

	$LOG_IDval  = $struct->structmem('LOG_ID');
	$LOG_ID     = $LOG_IDval->scalarval();

	$BALANCEval = $struct->structmem('BALANCE');
	$BALANCE    = $BALANCEval->scalarval();

	$SALES_IDval= $struct->structmem('SALES_ID');
	$SALES_ID   = $SALES_IDval->scalarval();

	print "STATUS  ::" . $STATUS   . "<BR>\n";
	print "LOG_ID  ::" . $LOG_ID   . "<BR>\n";
	print "SALES_ID::" . $SALES_ID . "<BR>\n";
	print "BALANCE ::" . $BALANCE  . "<BR><BR>\n";
}
?>

３：決済確認<BR>
<?php
if ($STATUS == "OK" && $SALES_ID){
//  $SHOP_ID     = "ABSD1234";
//  $SHOP_PASSWD = "abcdefgh";
    $SALES_ID    = "";
    $BCS_ID      = $BCS_ID;                 //カード判定の戻り値

    $SHOP_ID_val     = new xmlrpcval( $SHOP_ID,     'string' );
    $SHOP_PASSWD_val = new xmlrpcval( $SHOP_PASSWD, 'string' );
    $SALES_ID_val    = new xmlrpcval( $SALES_ID,    'string' );
    $BCS_ID_val      = new xmlrpcval( $BCS_ID,      'string' );
    $struct = new xmlrpcval(array("SHOP_ID"     => $SHOP_ID_val,
            "SHOP_PASSWD" => $SHOP_PASSWD_val,
            "BCS_ID"      => $BCS_ID_val,
            "SALES_ID"    => $SALES_ID_val), "struct");
    //メッセージ作成
    $message = new xmlrpcmsg(
        'Settlement.confirm_settlement',
        array($struct)
    );
    //メッセージ送信
    $result = $c->send($message);
    if( !$result ){
        exit('Could not connect to the server.');
    }else if( $result->faultCode() ){
        exit('XML-RPC fault ('.$result->faultCode().'): '
        .$result->faultString());
    }

    $struct = $result->value();
    $ERRORval   = $struct->structmem('ERROR');
    $ERROR      = $ERRORval->scalarval();
	
	$STATUSval  = $struct->structmem('STATUS');
    $STATUS     = $STATUSval->scalarval();

    if ($STATUS != 'OK'){
      echo "<pre>";
      print_r($ERROR);
      echo "</pre>";
      exit;
    }

    $INFOval    = $struct->structmem('INFO');
    $INFO       = $INFOval->scalarval();

    print "STATUS  ::" . $STATUS   . "<BR>\n";
    print "INFO    ::" . $INFO   . "<BR>\n";
}
?>

</body>
</html>
