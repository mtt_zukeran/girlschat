<?
//
// XML-RPC で、日本語(マルチバイト文字)を送信する際、正しくエスケープされない問題を解決。
// このファイルを xmlrpc.inc と同じディレクトリに配置して、xmlrpcvalクラスの代わりに、
// mb_xmlrpcval クラスを利用するようにしてください。
// // require_once("xmlrpc.inc"); // 利用しない
// require_once("mb_xmlrpc.inc");
// // $struct = new xmlrpcval(array("key"=>"value")); // 利用しない
// $struct = new mb_xmlrpcval(array("key"=>"value"));
//
// XML-RPC 1.1.0 と 1.1.1 で動作を確認。
// https://sourceforge.net/projects/phpxmlrpc/
//

require_once("xmlrpc.inc");

function mb_xmlrpc_encode_entitites($data)
{
	mb_internal_encoding($GLOBALS["xmlrpc_defencoding"]);
	$length = mb_strlen($data);
	$escapeddata = "";
	for($position = 0; $position < $length; $position++)
	{
		$character = mb_substr($data, $position, 1);
		if (strlen($character) > 1)
		{
			$character = htmlspecialchars($character);
		}
		else
		{
			$code = Ord($character);
			switch($code) {
				case 34:
				$character = "&quot;";
				break;
				case 38:
				$character = "&amp;";
				break;
				case 39:
				$character = "&apos;";
				break;
				case 60:
				$character = "&lt;";
				break;
				case 62:
				$character = "&gt;";
				break;
				default:
				if ($code < 32 || $code > 159)
					$character = ("&#".strval($code).";");
				break;
			}
		}
		$escapeddata .= $character;
	}
	return $escapeddata;
}

class mb_xmlrpcval extends xmlrpcval
{
	function serializedata($typ, $val)
	{
		$rs='';
		global $xmlrpcTypes, $xmlrpcBase64, $xmlrpcString,
		$xmlrpcBoolean;
		switch(@$xmlrpcTypes[$typ])
		{
			case 3:
				// struct
				$rs.="<struct>\n";
				reset($val);
				while(list($key2, $val2)=each($val))
				{
					$rs.="<member><name>${key2}</name>\n";
					$rs.=$this->serializeval($val2);
					$rs.="</member>\n";
				}
				$rs.='</struct>';
				break;
			case 2:
				// array
				$rs.="<array>\n<data>\n";
				for($i=0; $i<sizeof($val); $i++)
				{
					$rs.=$this->serializeval($val[$i]);
				}
				$rs.="</data>\n</array>";
				break;
			case 1:
				switch ($typ)
				{
					case $xmlrpcBase64:
						$rs.="<${typ}>" . base64_encode($val) . "</${typ}>";
						break;
					case $xmlrpcBoolean:
						$rs.="<${typ}>" . ($val ? '1' : '0') . "</${typ}>";
						break;
					case $xmlrpcString:
						$rs.="<${typ}>" . mb_xmlrpc_encode_entitites($val). "</${typ}>";
						// $rs.="<${typ}>" . xmlrpc_encode_entitites($val). "</${typ}>";
						// $rs.="<${typ}>" . htmlentities($val). "</${typ}>";
						break;
					default:
						$rs.="<${typ}>${val}</${typ}>";
				}
				break;
			default:
				break;
		}
		return $rs;
	}
}
?>