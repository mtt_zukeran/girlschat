運営会社設定			後払パックにチェック		on
サイト基本情報設定		後払利用可能ｻｲﾄ				on
						課金開始ポイント			50 or 500
						入金期日					3
						督促日数					10

INSERT INTO T_CODE_DTL(CODE_TYPE,CODE,CODE_NM)VALUES('74','Z'	,'後払パック'			);
INSERT INTO T_CODE_DTL(CODE_TYPE,CODE,CODE_NM)VALUES('83','Z'	,'後払パック'			);
INSERT INTO T_CODE_DTL(CODE_TYPE,CODE,CODE_NM)VALUES('91','Z'	,'後払いパック'			);

休日設定
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/01/01','元旦'              );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2009/12/31','大晦日'            );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/01/02','正月'              );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/01/03','正月'              );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/01/11','成人の日'          );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/02/11','建国記念の日'      );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/03/22','春分の日（振替）'  );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/04/29','昭和の日'          );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/05/03','憲法記念日'        );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/05/04','みどりの日'        );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/05/05','こどもの日'        );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/07/19','海の日'            );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/09/20','敬老の日'          );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/09/23','秋分の日'          );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/10/11','体育の日'          );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/11/03','文化の日'          );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/11/23','勤労感謝の日'      );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/12/23','天皇誕生日'        );
INSERT INTO T_HOLIDAY(HOLIDAY,REMARKS)VALUES('2010/12/31','大晦日'			  );

後払パック
INSERT INTO T_PACK(SITE_CD,SETTLE_TYPE,SALES_AMT,EX_POINT,REMARKS)VALUES('A001','Z', 2000,200  ,' \2000');
INSERT INTO T_PACK(SITE_CD,SETTLE_TYPE,SALES_AMT,EX_POINT,REMARKS)VALUES('A001','Z', 3000,300  ,' \3000');
INSERT INTO T_PACK(SITE_CD,SETTLE_TYPE,SALES_AMT,EX_POINT,REMARKS)VALUES('A001','Z', 5000,500  ,' \5000');
INSERT INTO T_PACK(SITE_CD,SETTLE_TYPE,SALES_AMT,EX_POINT,REMARKS)VALUES('A001','Z',10000,10000,'\10000');
INSERT INTO T_PACK(SITE_CD,SETTLE_TYPE,SALES_AMT,EX_POINT,REMARKS)VALUES('A001','Z',20000,20000,'\20000');

ユーザーｖｉｅｗ
PaymentAfterPack

サイト構成HTML文章設定
891 後払パック購入完了

ユーザーランク
利用限度設定         入金   限度
A 入金0  			     0 	  0
B ﾌﾞﾛﾝｽﾞ  			     1  200
C ｼﾙﾊﾞｰ  			 10000  500
D ｺﾞｰﾙﾄﾞ  			 50000 1000
E ﾌﾟﾗﾁﾅ  			100000 2000
F ﾀﾞｲﾔﾓﾝﾄﾞ  		300000 2000
G ﾛｲﾔﾙ  			500000 2000
H キャンペーン用  99999999 2000

督促メールテンプレート
A001	1601	23	0	100	督促レベル１用	督促レベル１用	1	MAIN	0	0	0	<FONT size=2><STRONG>督促レベル１</STRONG><BR><HR color=#6633ff>いつも御利用誠に有難う御座います。 <BR>お客様がMAQIAによりご購入された <FONT color=#ff0000>後払いﾊﾟｯｸにおける料金のお支払い</FONT>が確認できておりません。 <BR><FONT color=#ff0000><BR>支払い期日はご購入から4日以内</FONT>となっております。 <BR><FONT color=#ff0000>お支払い</FONT>頂きますようお願い申し上げます。 <BR><BR>御利用料金の確認、ご精算は下記より <A  href="%%12%%/user/start.aspx?uid=%%2%%&pw=%%3%%">こちら</A>よりお願い致します。 <BR>? <BR>お支払いにはﾄﾞｺﾓ送金も御利用頂けます。 <BR><BR>お支払いの際に御不明な点など御座いましたら当ｻﾎﾟｰﾄｾﾝﾀｰまでお問い合わせ下さい。 <BR><BR><DIV align=center><FONT color=#ff0000>◆</FONT><FONT color=#00cc00>問い合わせ先</FONT><FONT color=#ff0000>◆</FONT></DIV>TEL： <A  href="tel:0300000000">03-0000-0000</A><BR>お振込先: <BR><DIV align=center><FONT color=#ff0000>◆</FONT><FONT color=#00cc00>振込先</FONT><FONT color=#ff0000>◆</FONT></DIV>銀行名： <FONT color=#0000ff>xxxxxx銀行</FONT><BR>支店名: <FONT color=#0000ff>xxxx支店</FONT><BR>口座番号： <FONT color=#0000ff>普通)99999999</FONT><BR>口座名： <FONT color=#0000ff>ｶ)xxxxx</FONT><BR><FONT color=#555555 size=1>※お振込の際の振込名義人はお客様のID番号 <FONT co	lor=#ff0000 size=3>%%2%%</FONT>を必ず入力して下さい。 <BR></FONT><BR><BR></FONT>									
A001	1602	23	0	100	督促レベル2用	督促レベル2用	1	MAIN	0	0	0	<FONT size=2><STRONG>督促レベル２</STRONG><BR><HR color=#6633ff>いつも御利用誠に有難う御座います。 <BR>お客様がMAQIAによりご購入された <FONT color=#ff0000>後払いﾊﾟｯｸにおける料金のお支払い</FONT>が確認できておりません。 <BR><FONT color=#ff0000><BR>支払い期日はご購入から4日以内</FONT>となっております。 <BR><FONT color=#ff0000>お支払い</FONT>頂きますようお願い申し上げます。 <BR><BR>御利用料金の確認、ご精算は下記より <A  href="%%12%%/user/start.aspx?uid=%%2%%&pw=%%3%%">こちら</A>よりお願い致します。 <BR>? <BR>お支払いにはﾄﾞｺﾓ送金も御利用頂けます。 <BR><BR>お支払いの際に御不明な点など御座いましたら当ｻﾎﾟｰﾄｾﾝﾀｰまでお問い合わせ下さい。 <BR><BR><DIV align=center><FONT color=#ff0000>◆</FONT><FONT color=#00cc00>問い合わせ先</FONT><FONT color=#ff0000>◆</FONT></DIV>TEL： <A  href="tel:0300000000">03-0000-0000</A><BR>お振込先: <BR><DIV align=center><FONT color=#ff0000>◆</FONT><FONT color=#00cc00>振込先</FONT><FONT color=#ff0000>◆</FONT></DIV>銀行名： <FONT color=#0000ff>xxxxxx銀行</FONT><BR>支店名: <FONT color=#0000ff>xxxx支店</FONT><BR>口座番号： <FONT color=#0000ff>普通)99999999</FONT><BR>口座名： <FONT color=#0000ff>ｶ)xxxxx</FONT><BR><FONT color=#555555 size=1>※お振込の際の振込名義人はお客様のID番号 <FONT co	lor=#ff0000 size=3>%%2%%</FONT>を必ず入力して下さい。 <BR></FONT><BR><BR></FONT>									
A001	1603	23	0	100	督促レベル3用	督促レベル3用	1	MAIN	0	0	0	<FONT size=2><STRONG>督促レベル３</STRONG><BR><HR color=#6633ff>いつも御利用誠に有難う御座います。 <BR>お客様がMAQIAによりご購入された <FONT color=#ff0000>後払いﾊﾟｯｸにおける料金のお支払い</FONT>が確認できておりません。 <BR><FONT color=#ff0000><BR>支払い期日はご購入から4日以内</FONT>となっております。 <BR><FONT color=#ff0000>お支払い</FONT>頂きますようお願い申し上げます。 <BR><BR>御利用料金の確認、ご精算は下記より <A  href="%%12%%/user/start.aspx?uid=%%2%%&pw=%%3%%">こちら</A>よりお願い致します。 <BR>? <BR>お支払いにはﾄﾞｺﾓ送金も御利用頂けます。 <BR><BR>お支払いの際に御不明な点など御座いましたら当ｻﾎﾟｰﾄｾﾝﾀｰまでお問い合わせ下さい。 <BR><BR><DIV align=center><FONT color=#ff0000>◆</FONT><FONT color=#00cc00>問い合わせ先</FONT><FONT color=#ff0000>◆</FONT></DIV>TEL： <A  href="tel:0300000000">03-0000-0000</A><BR>お振込先: <BR><DIV align=center><FONT color=#ff0000>◆</FONT><FONT color=#00cc00>振込先</FONT><FONT color=#ff0000>◆</FONT></DIV>銀行名： <FONT color=#0000ff>xxxxxx銀行</FONT><BR>支店名: <FONT color=#0000ff>xxxx支店</FONT><BR>口座番号： <FONT color=#0000ff>普通)99999999</FONT><BR>口座名： <FONT color=#0000ff>ｶ)xxxxx</FONT><BR><FONT color=#555555 size=1>※お振込の際の振込名義人はお客様のID番号 <FONT co	lor=#ff0000 size=3>%%2%%</FONT>を必ず入力して下さい。 <BR></FONT><BR><BR></FONT>									


督促レベル設定
LVL DAYS 
1   3
2   6
3   9

督促スケジュール
1  19:00-19:30

2  11:00-11:30
   19:00-19:30

3  11:00-11:30
   19:00-19:30


DECLARE
	CURSOR CS_USER_MAN IS SELECT SITE_CD,TOTAL_RECEIPT_AMT FROM T_USER_MAN 
		WHERE SITE_CD='A002' FOR UPDATE;

	CURSOR CS_USER_RANK(prmSITE_CD T_USER_RANK.SITE_CD%TYPE,prmUSER_RANK T_USER_RANK.USER_RANK%TYPE) IS SELECT LIMIT_POINT FROM T_USER_RANK
		WHERE
			SITE_CD		= prmSITE_CD 		AND
			USER_RANK	= prmUSER_RANK;

	REC_USER_RANK		CS_USER_RANK		%ROWTYPE;
	BUF_RANK VARCHAR(10);

BEGIN
	FOR REC_MAN IN CS_USER_MAN LOOP
		BUF_RANK := GET_MAN_USER_RANK(REC_MAN.SITE_CD,REC_MAN.TOTAL_RECEIPT_AMT);
		OPEN CS_USER_RANK(REC_MAN.SITE_CD,BUF_RANK);
		FETCH CS_USER_RANK INTO REC_USER_RANK;
		IF CS_USER_RANK%FOUND THEN
			UPDATE T_USER_MAN SET LIMIT_POINT = REC_USER_RANK.LIMIT_POINT
				WHERE
					CURRENT OF CS_USER_MAN;
		END IF;
		CLOSE CS_USER_RANK;
	END LOOP;

	COMMIT;
END;
/

