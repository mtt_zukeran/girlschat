@echo off
REM -----最新ファイルを予め本番環境のKドライブにコピーしておくこと
REM -----.NET		K:\Site
REM -----PL/SQL		K:\Oracle

REM ----------	バックアップ作成処理	ここから	----------
REM -----ここで作られたバックアップのAPP_DATAはそのままだと使用不可

REM ----------	DBStatus.exe終了					----------
TASKKILL /IM DBStatus.exe
net stop "World Wide Web Publishing Service"

D:
RD /S /Q D:\SiteBackUp
RD /S /Q D:\OracleBackUp
MKDIR D:\SiteBackUp\ViComm
MKDIR D:\OracleBackUp\Oracle

xcopy D:\i-Brid\Site\ViCOMM D:\SiteBackUp\ViComm /EXCLUDE:ex.txt /I /E
xcopy K:\VssProjects\iBrid\Vicomm\Oracle D:\OracleBackUp\Oracle /I /E

REM ----------	バックアップ作成処理	ここまで	----------
echo バックアップ作成完了
echo MailParseを終了してください
echo SQLコンパイルを開始するには何かキーを押してください . . .
pause > NUL


REM	----------	SQLコンパイル			ここから	----------

REM -----ALTERがある場合は、記入すること
REM -----ALTERの最後にはEXIT;　改行　を忘れずに入れること

RD /S /Q K:\VssProjects\iBrid\ViComm\Oracle
MKDIR K:\VssProjects\iBrid\ViComm\Oracle
xcopy K:\Oracle K:\VssProjects\iBrid\ViComm\Oracle /I /E

REM SQLPLUS VICOMM_USER/VICOMM_USER@VICOMM @K:\VSSProjects\iBrid\ViComm\Oracle\5_TABLE\ALTER00000000.SQL
SQLPLUS VICOMM_USER/VICOMM_USER@VICOMM @K:\VSSProjects\iBrid\ViComm\Oracle\2_COMPILE\COMPILE_VICOMM.SQL

start C:\VICOMM-COMPILE.txt

REM ----------	SQLコンパイル			ここまで	----------

echo SQLコンパイル終了です。SQLコンパイルが成功しているか確認してください
echo WebConfigのコピーを開始するには何かキーを押してください . . .
pause > NUL

COPY D:\i-Brid\Site\ViCOMM\Admin\Web.config			K:\Site\Admin
COPY D:\i-Brid\Site\ViCOMM\AutoRegist\Web.config	K:\Site\AutoRegist
COPY D:\i-Brid\Site\ViCOMM\CastPC\Web.config		K:\Site\CastPC
COPY D:\i-Brid\Site\ViCOMM\Dispatch\Web.config		K:\Site\Dispatch
COPY D:\i-Brid\Site\ViCOMM\IVPInterface\Web.config	K:\Site\IVPInterface
COPY D:\i-Brid\Site\ViCOMM\Mail\Web.config			K:\Site\Mail
COPY D:\i-Brid\Site\ViCOMM\Settle\Web.config		K:\Site\Settle
COPY D:\i-Brid\Site\ViCOMM\User\Web.config			K:\Site\User

echo Web.Configのコピーが終了しました
echo サイトの削除を開始するには何かキーを押してください. . .
pause > NUL

RD /S /Q D:\i-Brid\Site\ViCOMM\Admin
RD /S /Q D:\i-Brid\Site\ViCOMM\AutoRegist
RD /S /Q D:\i-Brid\Site\ViCOMM\CastPC
RD /S /Q D:\i-Brid\Site\ViCOMM\Dispatch
RD /S /Q D:\i-Brid\Site\ViCOMM\IVPInterface
RD /S /Q D:\i-Brid\Site\ViCOMM\Mail
RD /S /Q D:\i-Brid\Site\ViCOMM\Settle
RD /S /Q D:\i-Brid\Site\ViCOMM\User

echo サイトの削除が終了しました
echo サイトのコピーを開始するには何かキーを押してください. . .
pause > NUL

xcopy K:\Site\Admin			D:\i-Brid\Site\ViCOMM\Admin			/I /E
xcopy K:\Site\AutoRegist	D:\i-Brid\Site\ViCOMM\AutoRegist	/I /E
xcopy K:\Site\CastPC		D:\i-Brid\Site\ViCOMM\CastPC		/I /E
xcopy K:\Site\Dispatch		D:\i-Brid\Site\ViCOMM\Dispatch		/I /E
xcopy K:\Site\IVPInterface	D:\i-Brid\Site\ViCOMM\IVPInterface	/I /E
xcopy K:\Site\Mail			D:\i-Brid\Site\ViCOMM\Mail			/I /E
xcopy K:\Site\Settle		D:\i-Brid\Site\ViCOMM\Settle		/I /E
xcopy K:\Site\User			D:\i-Brid\Site\ViCOMM\User			/I /E


REM -----サイトに権限付与
cacls D:\i-Brid\Site\ViComm\Admin\App_Data /T /E /G Users:F
cacls D:\i-Brid\Site\ViComm\Admin\App_Data /T /E /G Administrators:F
cacls D:\i-Brid\Site\ViComm\Admin\App_Data /T /E /G SYSTEM:F
cacls D:\i-Brid\Site\ViComm\CastPC\App_Data /T /E /G Users:F  
cacls D:\i-Brid\Site\ViComm\CastPC\App_Data /T /E /G Administrators:F
cacls D:\i-Brid\Site\ViComm\CastPC\App_Data /T /E /G SYSTEM:F

echo サイトのコピーが終了しました
pause

net start "World Wide Web Publishing Service"
start D:\i-Brid\Bin\DBStatus.exe
D:
cd D:\i-Brid\MailTools
start MailParse.cgi.bat

echo 終了するには何かキーを押してください . . .
pause > NUL