﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// ConvertMobileMovieResult の概要の説明です
/// </summary>
[Serializable]
public class ConvertMobileMovieResult
{
	private int fileNum = 0;
	private string mobileFileType = string.Empty;

	public int FileNum {
		get{return this.fileNum;}
		set{this.fileNum = value;}
	}
	public string MobileFileType
	{
		get { return this.mobileFileType; }
		set { this.mobileFileType = value; }
	}
	public ConvertMobileMovieResult() {}
	public ConvertMobileMovieResult(int pFileNum, string pMobileFileType)
	{
		this.fileNum = pFileNum;
		this.mobileFileType = pMobileFileType;
	}
}
