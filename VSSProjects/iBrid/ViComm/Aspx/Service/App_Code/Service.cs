﻿using System;
using System.IO;
using System.DirectoryServices;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using ViComm;

[WebService(Namespace = "http://ibrid.co.jp/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Service:System.Web.Services.WebService {
	public Service() {

		//デザインされたコンポーネントを使用する場合、次の行をコメントを解除してください 
		//InitializeComponent(); 
	}


	[WebMethod(Description = "Ping Service")]
	public string Ping(string hostName) {
		ProcessStartInfo info = new ProcessStartInfo();
		info.FileName = "ping.exe";
		info.Arguments = hostName;
		info.RedirectStandardOutput = true;
		info.UseShellExecute = false;
		Process p = Process.Start(info);
		p.WaitForExit();
		return p.StandardOutput.ReadToEnd();
	}

	[System.Xml.Serialization.XmlInclude(typeof(ConvertMobileMovieResult))]
	[WebMethod(Description = "Convert Mobile Movie Service")]
	public ConvertMobileMovieResult[] ConvertMobileMovie(string sBatPath, string sSiteCd, string pInPath, string pInFile, string pOutPath, string pOutFileNoExtent, string pOutputThumbnailPath) {
		return ConvertMobileMovieImple(sBatPath, sSiteCd, pInPath, pInFile, pOutPath, pOutFileNoExtent, ViComm.ViCommConst.MovieAspectType.NONE, false, pOutputThumbnailPath,false);
	}
	[System.Xml.Serialization.XmlInclude(typeof(ConvertMobileMovieResult))]
	[WebMethod(Description = "Convert Mobile Movie Service")]
	public ConvertMobileMovieResult[] ConvertMobileMovieWithAspectRate(string sBatPath, string sSiteCd, string pInPath, string pInFile, string pOutPath, string pOutFileNoExtent, string pAspectType, string pOutputThumbnailPath) {
		return ConvertMobileMovieImple(sBatPath,sSiteCd,pInPath,pInFile,pOutPath,pOutFileNoExtent,pAspectType,false,pOutputThumbnailPath,false);
	}
	[System.Xml.Serialization.XmlInclude(typeof(ConvertMobileMovieResult))]
	[WebMethod(Description = "Convert Mobile Movie Service")]
	public ConvertMobileMovieResult[] ConvertSplitMobileMovie(string sBatPath, string sSiteCd, string pInPath, string pInFile, string pOutPath, string pOutFileNoExtent, string pOutputThumbnailPath) {
		return ConvertMobileMovieImple(sBatPath,sSiteCd,pInPath,pInFile,pOutPath,pOutFileNoExtent,ViComm.ViCommConst.MovieAspectType.NONE,true,pOutputThumbnailPath,false);
	}
	[System.Xml.Serialization.XmlInclude(typeof(ConvertMobileMovieResult))]
	[WebMethod(Description = "Convert Mobile Movie Service")]
	public ConvertMobileMovieResult[] ConvertSplitMobileMovieWithAspectRate(string sBatPath, string sSiteCd, string pInPath, string pInFile, string pOutPath, string pOutFileNoExtent, string pAspectType, string pOutputThumbnailPath) {
		return ConvertMobileMovieImple(sBatPath,sSiteCd,pInPath,pInFile,pOutPath,pOutFileNoExtent,pAspectType,true,pOutputThumbnailPath,false);
	}
	[System.Xml.Serialization.XmlInclude(typeof(ConvertMobileMovieResult))]
	[WebMethod(Description = "Convert Mobile Product Movie Service")]
	public ConvertMobileMovieResult[] ConvertProdSplitMobileMovieWithAspectRate(string sBatPath,string sSiteCd,string pInPath,string pInFile,string pOutPath,string pOutFileNoExtent,string pAspectType,string pOutputThumbnailPath) {
		return ConvertMobileMovieImple(sBatPath,sSiteCd,pInPath,pInFile,pOutPath,pOutFileNoExtent,pAspectType,true,pOutputThumbnailPath,true);
	}

	[WebMethod(Description = "Set CopyRight")]
	public string SetCopyRight(string sBatPath,string pInPath,string pInFile,string pPngPath) {
		ProcessStartInfo info = new ProcessStartInfo();
		info.FileName = sBatPath + "\\CopyRight.bat";
		info.Arguments = string.Format("{0} {1} {2}",pInPath,pInFile,pPngPath);
		info.RedirectStandardOutput = true;
		info.UseShellExecute = false;
		Process p = Process.Start(info);
		p.WaitForExit();
		return p.StandardOutput.ReadToEnd();
	}

	[WebMethod(Description = "IIS Replace Host Header")]
	public string IISReplaceHostHeader(string pHostHeaders,string pWebsiteID) {

		DirectoryEntry site = new DirectoryEntry("IIS://localhost/w3svc/" + pWebsiteID);
		try {
			//Get everything currently in the serverbindings propery. 
			PropertyValueCollection serverBindings = site.Properties["ServerBindings"];

			//Add the new binding
			string[] oHeader = pHostHeaders.Split(',');
			serverBindings.Clear();
			for (int i = 0;i < oHeader.Length;i++) {
				serverBindings.Add(oHeader[i]);
			}

			//Create an object array and copy the content to this array
			Object[] newList = new Object[serverBindings.Count];
			serverBindings.CopyTo(newList,0);

			//Write to metabase
			site.Properties["ServerBindings"].Value = newList;

			//Commit the changes
			using (Impersonator oImpersonator = new Impersonator("vicomm","","vicomm")) {
				site.CommitChanges();
			}

		} catch (Exception e) {
			Console.WriteLine(e);
			return "-1";
		}
		return "0";
	}
	[WebMethod(Description = "ConvertMobileImage Service")]
	public void ConvertMobileImage(string pBatPath, string pSiteCd, string pInputDir, string pOutputDir, string pFileNm) {
		ProcessStartInfo info = new ProcessStartInfo();
		info.FileName = pBatPath + "\\ConvertImage.bat";
		info.Arguments = string.Format("{0} {1} {2} {3}",pSiteCd, pInputDir, pOutputDir, pFileNm);
		info.RedirectStandardOutput = true;
		info.UseShellExecute = false;
		Process p = Process.Start(info);
		p.WaitForExit();
	}



	private ConvertMobileMovieResult[] ConvertMobileMovieImple(string sBatPath, string sSiteCd, string pInPath, string pInFile, string pOutPath, string pOutFileNoExtent, string pAspectType, bool pNeedSplit, string pOutputThumbnailPath, bool pConvertProductFlag) {
		string resultFile = string.Format(@"{0}\{1}.result", pInPath, pOutFileNoExtent);

		try {

			string sNeedSplitFlag = pNeedSplit?"1":"0";

			ProcessStartInfo info = new ProcessStartInfo();
			if (pConvertProductFlag) {
				info.FileName = sBatPath + "\\ConvertProductMovie.bat";
			} else {
				info.FileName = sBatPath + "\\Convert.bat";
			}
			info.Arguments = string.Format("{0} {1} {2} {3} {4} {5} {6} {7}", pInPath, pInFile, pOutPath, pOutFileNoExtent,pAspectType, sNeedSplitFlag, pOutputThumbnailPath, sSiteCd);
			info.RedirectStandardOutput = true;
			info.UseShellExecute = false;
			Process p = Process.Start(info);
			p.WaitForExit();

			List<ConvertMobileMovieResult> resultList = new List<ConvertMobileMovieResult>();
			if(pNeedSplit){
				using (StreamReader reader = new StreamReader(resultFile)) {
					while (reader.Peek() >= 0) {
						string[] values = reader.ReadLine().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

						ConvertMobileMovieResult result = new ConvertMobileMovieResult();
						result.MobileFileType = values[0];
						result.FileNum = int.Parse(values[1]);

						resultList.Add(result);
					}
				}
			}else{
				resultList.Add(new ConvertMobileMovieResult(0, "11"));
				resultList.Add(new ConvertMobileMovieResult(0, "12"));
				resultList.Add(new ConvertMobileMovieResult(0, "13"));
				resultList.Add(new ConvertMobileMovieResult(0, "14"));
				resultList.Add(new ConvertMobileMovieResult(0, "21"));
				resultList.Add(new ConvertMobileMovieResult(0, "22"));
				resultList.Add(new ConvertMobileMovieResult(0, "23"));
				resultList.Add(new ConvertMobileMovieResult(0, "24"));
				resultList.Add(new ConvertMobileMovieResult(0, "31"));
				resultList.Add(new ConvertMobileMovieResult(0, "32"));
				resultList.Add(new ConvertMobileMovieResult(0, "33"));
				resultList.Add(new ConvertMobileMovieResult(0, "34"));
			}
			return resultList.ToArray();
		} finally {
			if (File.Exists(resultFile))
				File.Delete(resultFile);
		}
	}


}
