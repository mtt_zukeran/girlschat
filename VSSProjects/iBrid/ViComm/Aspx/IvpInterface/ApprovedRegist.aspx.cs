﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: アフリエーター設定
--	Progaram ID		: ApprovedRegist
--
--  Creation Date	: 2010.03.16
--  Creater			: iBrid(A.Koyanagi)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Threading;
using iBridCommLib;
using ViComm;

public partial class ApprovedRegist:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		UpdateDispatchRegistDtl();
	}

	private void UpdateDispatchRegistDtl() {
		string sHistorySeq = iBridUtil.GetStringValue(Request.QueryString["hisseq"]);
		string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		string sRegistResult = iBridUtil.GetStringValue(Request.QueryString["result"]);
		string sPartnerSiteLoginId = iBridUtil.GetStringValue(Request.QueryString["ploginid"]);

		if (!string.IsNullOrEmpty(sHistorySeq) && !string.IsNullOrEmpty(sRegistResult) && !string.IsNullOrEmpty(sUserSeq)) {

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("UPDATE_DISPATCH_REGIST");
				db.ProcedureInParm("PHISTORY_SEQ",DbSession.DbType.VARCHAR2,sHistorySeq);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureInParm("PREGIST_RESULT",DbSession.DbType.NUMBER,int.Parse(sRegistResult));
				db.ProcedureInParm("PPARTNER_SITE_LOGIN_ID",DbSession.DbType.VARCHAR2,sPartnerSiteLoginId);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}

			Response.ContentType = "text/html";
			Response.Write(string.Format("result={0}","0"));
			Response.End();

		} else {
			Response.ContentType = "text/html";
			Response.Write(string.Format("result={0}","1"));
			Response.End();
		}
	}
}
