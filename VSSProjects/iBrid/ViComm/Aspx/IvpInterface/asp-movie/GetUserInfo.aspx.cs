﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: 会員存在確認
--	Progaram ID		: GetUserInfo
--
--  Creation Date	: 2011.02.25
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web;
using iBridCommLib;
using ViComm;

public partial class GetUserInfo:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		IsExistUser();
	}

	private void IsExistUser() {
		bool bExist = false;
		string sSiteCd = string.Empty;
		string sUserSeq = string.Empty;
		string sResult = string.Empty;
		string sIfKey = iBridUtil.GetStringValue(Request.QueryString["memberid"]);

		using (UserMan oUserMan = new UserMan()) {
			bExist = oUserMan.IsExistUserByIfKey(sIfKey,out sSiteCd,out sUserSeq);
		}
		if (bExist) {
			sResult = "OK";
		} else {
			sResult = "NG";
		}
		Response.ContentType = "text/html";
		Response.Write(sResult);
		Response.End();
	}
}
