﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: ポイント確認
--	Progaram ID		: CheckBalance
--
--  Creation Date	: 2011.02.25
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web;
using iBridCommLib;
using ViComm;

public partial class CheckBalance:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		CheckBalanceAspMoviePoint();
	}

	private void CheckBalanceAspMoviePoint() {
		string sResult = string.Empty;
		string sSiteCd = string.Empty;
		string sIfKey = iBridUtil.GetStringValue(Request.QueryString["memberid"]);
		int iUsePoint = 0;
		int.TryParse(iBridUtil.GetStringValue(Request.QueryString["c_point"]),out iUsePoint);

		using (UserMan oUserMan = new UserMan()) {
			sResult = oUserMan.CheckBalanceAspMoviePoint(sIfKey,iUsePoint);
		}

		Response.ContentType = "text/html";
		Response.Write(sResult);
		Response.End();
	}
}
