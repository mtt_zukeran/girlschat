﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: ポイント利用処理
--	Progaram ID		: ReportUsedPoint
--
--  Creation Date	: 2011.02.25
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web;
using iBridCommLib;
using ViComm;

public partial class ReportUsedPoint:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		UsedPoint();
	}

	private void UsedPoint() {
		string sSiteCd = string.Empty;
		string sUserSeq  = string.Empty;
		string sResult = string.Empty;
		string sIfKey = iBridUtil.GetStringValue(Request.QueryString["memberid"]);
		int iUsePoint = 0;
		int.TryParse(iBridUtil.GetStringValue(Request.QueryString["u_point"]),out iUsePoint);

		using (UserMan oUserMan = new UserMan()) {
			sResult = oUserMan.CheckBalanceAspMoviePoint(sIfKey,iUsePoint);
		}

		//ポイント不足もしくは会員が存在しない場合レスポンスを返してしまう
		if (!sResult.Equals("OK")) {
			Response.ContentType = "text/html";
			Response.Write(sResult);
			Response.End();
		} else {
			using (UserMan oUserMan = new UserMan()) {
				oUserMan.IsExistUserByIfKey(sIfKey,out sSiteCd,out sUserSeq);
				oUserMan.CreateWebUsedReport(sSiteCd,sUserSeq,ViCommConst.CHARGE_DOWNLOAD_MOVIE,iUsePoint,string.Empty,string.Empty,string.Empty);
			}
			Response.ContentType = "text/html";
			Response.Write(sResult);
			Response.End();
		}
	}
}
