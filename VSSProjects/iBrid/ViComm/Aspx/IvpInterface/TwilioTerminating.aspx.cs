﻿using System;
using System.Web;
using System.Threading;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class TwilioTerminating:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sAccountSid = iBridUtil.GetStringValue(Request.Form["AccountSid"]);
		string sCallSid = iBridUtil.GetStringValue(Request.Form["CallSid"]);
		string sDialCallSid = iBridUtil.GetStringValue(Request.Form["DialCallSid"]);
		string sDialCallStatus = iBridUtil.GetStringValue(Request.Form["DialCallStatus"]);
		string sTwilioAccountSid = string.Empty;
		string sTwilioAuthToken = string.Empty;
		string sStartDate = DateTime.Now.ToString("yyyyMMddHHmmss");
		string sEndDate = sStartDate;
		int iDuration = 0;
		string sIvpRequestSeq = string.Empty;
		int iAvailableSec = 0;
		string sTalkEndReason = string.Empty;
		string sDisconnectReason = string.Empty;

		if (string.IsNullOrEmpty(sAccountSid) || string.IsNullOrEmpty(sCallSid) || string.IsNullOrEmpty(sDialCallSid) || string.IsNullOrEmpty(sDialCallStatus)) {
			ViCommInterface.WriteIFLog("TwilioTerminating-Bad Request","Request:" + Request.Form.ToString());
			ResponseXml();
			return;
		}

		using (Sys oSys = new Sys()) {
			if (oSys.GetOne()) {
				sTwilioAccountSid = oSys.twilioAccountSid;
				sTwilioAuthToken = oSys.twilioAuthToken;

				if (!sAccountSid.Equals(sTwilioAccountSid)) {
					ViCommInterface.WriteIFLog("TwilioTerminating-Incorrect AccountSid","AccountSid:" + sAccountSid);
					ResponseXml();
					return;
				}
			}
		}

		if (string.IsNullOrEmpty(sTwilioAccountSid) || string.IsNullOrEmpty(sTwilioAuthToken)) {
			ViCommInterface.WriteIFLog("TwilioTerminating-No Setting",string.Format("sTwilioAccountSid:{0},sTwilioAuthToken:{1}",sTwilioAccountSid,sTwilioAuthToken));
			ResponseXml();
			return;
		}

		using (IVPRequest oIVPRequest = new IVPRequest()) {
			if (oIVPRequest.GetOneByTwilioCallSid(sCallSid)) {
				sIvpRequestSeq = oIVPRequest.ivpRequestSeq.ToString();
				iAvailableSec = oIVPRequest.availableSec;
			}
		}

		if (string.IsNullOrEmpty(sIvpRequestSeq) || iAvailableSec.Equals(0)) {
			ViCommInterface.WriteIFLog("TwilioTerminating-Not Exist T_IVP_REQUEST",string.Format("sCallSid:{0}",sCallSid));
			ResponseXml();
			return;
		}

		if (!sDialCallStatus.Equals(PwViCommConst.TwilioDialCallStatus.COMPLETED)) {
			sTalkEndReason = PwViCommConst.TalkEndReason.FAILED;

			switch (sDialCallStatus) {
				case PwViCommConst.TwilioDialCallStatus.BUSY:
					sDisconnectReason = PwViCommConst.DisconnectReason.BUSY;
					break;
				case PwViCommConst.TwilioDialCallStatus.NO_ANSWER:
					sDisconnectReason = PwViCommConst.DisconnectReason.NO_ANSWER;
					break;
				case PwViCommConst.TwilioDialCallStatus.FAILED:
				case PwViCommConst.TwilioDialCallStatus.CANCELED:
					sDisconnectReason = PwViCommConst.DisconnectReason.FAIL_SYS_ERROR;
					break;
				default:
					sDisconnectReason = PwViCommConst.DisconnectReason.FAIL_OTHERS;
					break;
			}
		} else {
			string sCallsResult = GetTwilioCall(sTwilioAccountSid,sTwilioAuthToken,sDialCallSid);

			if (sCallsResult.Equals("-1")) {
				ResponseXml();
				return;
			}

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(sCallsResult);
			XmlNodeList callList = doc.SelectNodes("TwilioResponse/Call");

			string sStartTime;
			if (callList.Count > 0) {
				sStartTime = callList[0].SelectSingleNode("StartTime").InnerText;
				string sEndTime = callList[0].SelectSingleNode("EndTime").InnerText;
				string sDuration = callList[0].SelectSingleNode("Duration").InnerText;

				sStartDate = DateTime.Parse(sStartTime).ToString("yyyyMMddHHmmss");
				sEndDate = DateTime.Parse(sEndTime).ToString("yyyyMMddHHmmss");
				iDuration = int.Parse(sDuration);
			} else {
				ViCommInterface.WriteIFLog("TwilioTerminating-Not Exist CallLog",string.Format("sDialCallSid:{0}",sDialCallSid));
				ResponseXml();
				return;
			}

			if (iDuration.Equals(0) && !sStartDate.Equals(sEndDate)) {
				sStartDate = sEndDate;
			}

			//切断理由の判別
			if ((iAvailableSec - iDuration) <= 5) {
				sTalkEndReason = PwViCommConst.TalkEndReason.POINT_DISCONNECT;
				
				//Twilio側で接続秒数が通話可能秒数を数秒超過してしまう場合がある為
				iDuration = iAvailableSec;
				sEndDate = DateTime.Parse(sStartTime).AddSeconds(iDuration).ToString("yyyyMMddHHmmss");
			} else {
				sTalkEndReason = PwViCommConst.TalkEndReason.OWN_DISCONNECT;
			}
		}

		UpdateIncommingCallStatus(sIvpRequestSeq,1,"");//着信状態設定（切断）
		UpdateTalkStatus(sIvpRequestSeq,"3",sTalkEndReason,sDisconnectReason);//会話状態設定（切断）
		CreateIvpUsedReport(sIvpRequestSeq,sStartDate,sEndDate,iDuration);//IVP利用明細作成
		ResponseXml();
	}

	private void ResponseXml() {
		Response.ContentType = "text/xml";
		Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Response.Write("<Response>");
		Response.Write("</Response>");
		Response.End();
	}

	private string UpdateIncommingCallStatus(string pIvpRequestSeq,int pDisconnectFlag,string pCID) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_INCOMMING_CALL_STATUS");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("pDISCONNECT_FLAG",DbSession.DbType.NUMBER,pDisconnectFlag);
			db.ProcedureInParm("pCID",DbSession.DbType.VARCHAR2,pCID);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	private string UpdateTalkStatus(string pIvpRequestSeq,string pCallStatus,string pTalkEndReason,string pDisconnectReason) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TALK_STATUS");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("pCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,"-1");
			db.ProcedureInParm("pCALL_STATUS",DbSession.DbType.VARCHAR2,pCallStatus);
			db.ProcedureInParm("pTALK_END_REASON",DbSession.DbType.VARCHAR2,pTalkEndReason);
			db.ProcedureInParm("pDISCONNECT_REASON",DbSession.DbType.VARCHAR2,pDisconnectReason);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("pCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCAST_USE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	private void CreateIvpUsedReport(string pIvpRequestSeq,string pStartDate,string pEndDate,int pChargeSec) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CREATE_IVP_USED_REPORT");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("pADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("pSTART_DATE",DbSession.DbType.VARCHAR2,pStartDate);
			db.ProcedureInParm("pEND_DATE",DbSession.DbType.VARCHAR2,pEndDate);
			db.ProcedureInParm("pCHARGE_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.REQUEST_TALK_LIGHT);
			db.ProcedureInParm("pCHARGE_SEC",DbSession.DbType.NUMBER,pChargeSec);
			db.ProcedureInParm("pVCS_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("pCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private string GetTwilioCall(string pAccountSid,string pAuthToken,string pCallSid) {
		string sUrl = string.Format("https://api.twilio.com/2010-04-01/Accounts/{0}/Calls/{1}",pAccountSid,pCallSid);

		try {
			//ViCommInterface.WriteIFLog("GetTwilioCall",sUrl);
			WebRequest req = WebRequest.Create(sUrl);
			req.Credentials = new NetworkCredential(pAccountSid,pAuthToken);
			req.Timeout = 30000;
			WebResponse res = req.GetResponse();
			Encoding encoding = Encoding.GetEncoding("UTF-8");
			Stream st = res.GetResponseStream();

			using (StreamReader sr = new StreamReader(st,encoding)) {
				string sResponse = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sResponse;
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"GetTwilioCall",sUrl);
			return "-1";
		}
	}
}
