﻿using System;
using System.Web;
using System.Threading;
using System.Configuration;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class TwilioIncall:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sAccountSid = iBridUtil.GetStringValue(Request.Form["AccountSid"]);
		string sParentCallSid = iBridUtil.GetStringValue(Request.Form["ParentCallSid"]);
		string sCallStatus = iBridUtil.GetStringValue(Request.Form["CallStatus"]);
		string sIvpRequestSeq = string.Empty;
		string sResult = string.Empty;

		if (string.IsNullOrEmpty(sAccountSid) || string.IsNullOrEmpty(sParentCallSid) || string.IsNullOrEmpty(sCallStatus)) {
			ViCommInterface.WriteIFLog("TwilioIncall-Bad Request","Request:" + Request.Form.ToString());
			ResponseHangup();
			return;
		}

		if (!sCallStatus.Equals(PwViCommConst.TwilioCallStatus.IN_PROGRESS)) {
			ViCommInterface.WriteIFLog("TwilioIncall-Bad Status","sCallStatus:" + sCallStatus);
			ResponseHangup();
			return;
		}

		using (Sys oSys = new Sys()) {
			if (oSys.GetOne()) {
				if (!sAccountSid.Equals(oSys.twilioAccountSid)) {
					ViCommInterface.WriteIFLog("TwilioIncall-Incorrect AccountSid","AccountSid:" + sAccountSid);
					ResponseHangup();
					return;
				}
			}
		}

		using (IVPRequest oIVPRequest = new IVPRequest()) {
			if (oIVPRequest.GetOneByTwilioCallSid(sParentCallSid)) {
				sIvpRequestSeq = oIVPRequest.ivpRequestSeq.ToString();
			}
		}

		if (string.IsNullOrEmpty(sIvpRequestSeq)) {
			ViCommInterface.WriteIFLog("TwilioIncall-Not Exist T_IVP_REQUEST","sParentCallSid:" + sParentCallSid);
			ResponseHangup();
			return;
		}

		sResult = UpdateTalkStatus(sIvpRequestSeq,"2","","");//会話状態設定（接続完了）

		if (!sResult.Equals("0")) {
			ResponseHangup();
			return;
		}

		Response.ContentType = "text/xml";
		Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Response.Write("<Response>");
		Response.Write("</Response>");
		Response.End();
	}

	private void ResponseHangup() {
		Response.ContentType = "text/xml";
		Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Response.Write("<Response>");
		Response.Write("<Hangup/>");
		Response.Write("</Response>");
		Response.End();
	}

	private string UpdateTalkStatus(string pIvpRequestSeq,string pCallStatus,string pTalkEndReason,string pDisconnectReason) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TALK_STATUS");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("pCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,"-1");
			db.ProcedureInParm("pCALL_STATUS",DbSession.DbType.VARCHAR2,pCallStatus);
			db.ProcedureInParm("pTALK_END_REASON",DbSession.DbType.VARCHAR2,pTalkEndReason);
			db.ProcedureInParm("pDISCONNECT_REASON",DbSession.DbType.VARCHAR2,pDisconnectReason);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("pCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCAST_USE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}
}
