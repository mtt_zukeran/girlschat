﻿using System;
using System.Data;
using System.Web;
using iBridCommLib;
using ViComm;

public partial class AcceptRegist:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {

		SetNoExistUser();
	}

	private void SetNoExistUser() {
		string sResult = "";
		int iLockDays;
		int.TryParse(Request.QueryString["lock"],out iLockDays);
		
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCEPT_REGIST_USER_MAN");
			db.ProcedureInParm("PSUB_HOST_NM",DbSession.DbType.VARCHAR2,Page.Request.Url.Host);
			db.ProcedureInParm("PAS_INFO_PREFIX",DbSession.DbType.VARCHAR2,Request.QueryString["infprefix"]);
			db.ProcedureInParm("REGIST_AFFILIATE_CD",DbSession.DbType.VARCHAR2,Request.QueryString["hisseq"]);
			db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,Request.QueryString["mobcaricd"]);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,Request.QueryString["tmluniqid"]);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,Request.QueryString["imodeid"]);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,Request.QueryString["tel"]);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,Request.QueryString["pass"]);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,Request.QueryString["handlenm"]);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,Request.QueryString["birthday"]);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,Request.QueryString["mobtmlnm"]);
			db.ProcedureInParm("PMAQIA_AF_LOGIN_ID",DbSession.DbType.VARCHAR2,Request.QueryString["afloginid"]);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,Request.QueryString["email"]);
			db.ProcedureInParm("PACCOUNT_LOCK_DAYS",DbSession.DbType.NUMBER,iLockDays);

			db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTRACKING_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = string.Format(db.GetStringValue("PTRACKING_URL"),Request.QueryString["hisseq"],db.GetStringValue("PRESULT"),db.GetStringValue("PLOGIN_ID"),Request.QueryString["userseq"]);
			if ((db.GetStringValue("PRESULT").Equals("0") || db.GetStringValue("PRESULT").Equals("1")) && sResult.Length > 0) {
				ViCommInterface.TransToParent(sResult,false);
			} else {
				ViCommInterface.WriteIFLog("SetNoExistUser:ACCEPT_REGIST_USER_MAN",string.Format("アフリエーター設定がありません。{0}",Request.QueryString["infprefix"]));
				sResult += string.Format("result={0}",db.GetStringValue("PRESULT"));
			}
		}

		Response.ContentType = "text/html";
		Response.Write(sResult);
		Response.End();
	}
}
