﻿using System;
using System.Web;
using System.Threading;
using System.Configuration;
using iBridCommLib;
using ViComm;

public partial class Dispatch:System.Web.UI.Page {

	public const int REQ_UPDATE_LIVE_CAMERA_STATUS = 2;
	public const int REQ_AVAILABLE_SEC = 3;
	public const int REQ_REPORT_IVP_USED_SEC = 4;
	public const int REQ_REPORT_CALL_STATUS = 5;
	public const int REQ_REPORT_INCOMMING = 6;
	public const int REQ_REPORT_REC_STATUS = 7;
	public const int REQ_CHANGE_TALK = 8;
	public const int REQ_SIP_LOGIN = 9;
	public const int REQ_SIP_LOGOUT = 10;
	public const int REQ_REPORT_SHARE_LIVE_USED_SEC = 11;
	public const int REQ_REPORT_TEL_AUTH = 12;

	protected void Page_Load(object sender,EventArgs e) {
		int iReq;
		try {
			iReq = int.Parse(Request.QueryString["req"]);
		} catch {
			iReq = 0;
		}

		switch (iReq) {
			case REQ_UPDATE_LIVE_CAMERA_STATUS:
				UpdateLiveCameraStatus();
				break;

			case REQ_AVAILABLE_SEC:
				AvailableSec();
				break;

			case REQ_REPORT_IVP_USED_SEC:
				ReportIvpUsedSec();
				break;

			case REQ_REPORT_SHARE_LIVE_USED_SEC:
				ReportShareLiveUsedSec();
				break;

			case REQ_REPORT_CALL_STATUS:
				ReportCallStatus();
				break;

			case REQ_REPORT_INCOMMING:
				ReportIncommingStatus();
				break;

			case REQ_REPORT_REC_STATUS:
				ReportRecordingStatus();
				break;

			case REQ_CHANGE_TALK:
				ChangeTalk(iBridUtil.GetStringValue(Request.QueryString["dtmf"]));
				break;

			case REQ_SIP_LOGIN:
				CastSipLogin();
				break;

			case REQ_SIP_LOGOUT:
				CastSipLogout();
				break;

			case REQ_REPORT_TEL_AUTH:
				SmartPhoneTelCertify();
				break;

			default:
				Response.ContentType = "text/html";
				Response.Write(string.Format("result={0}","9"));
				Response.End();
				break;
		}
	}

	private void UpdateLiveCameraStatus() {
		string sCamera = iBridUtil.GetStringValue(Request.QueryString["camera"]);
		string sConnect = iBridUtil.GetStringValue(Request.QueryString["connect"]);

		if (!sConnect.Equals("1")) {
			sConnect = "0";
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_LIVE_CAMERA_STATUS");
			db.ProcedureInParm("PSHARED_MEDIA_NM",DbSession.DbType.VARCHAR2,sCamera);
			db.ProcedureInParm("PCONNECT_FLAG",DbSession.DbType.VARCHAR2,int.Parse(sConnect));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

	private void AvailableSec() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sProcSeq = iBridUtil.GetStringValue(Request.QueryString["procseq"]);
		string sUrl;
		int iBalPoint = 0;
		string pTel = "";
		string pPassword = "";
		string pCondition = "";
		string pRetCd = "";
		string sResult = "9";
		int iSec = 0;

		using (IVPRequest oRequest = new IVPRequest()) {
			if (oRequest.GetRequest(sIvpRequestSeq)) {

				switch (oRequest.requestId) {
					case ViCommConst.REQUEST_DIALOUT_RECORDING:
						sResult = "0";
						iSec = 86400;
						break;

					default:
						if (oRequest.useOtherSysInfoFlag != 0) {
							using (OtherWebSite oOtherSys = new OtherWebSite()) {
								oOtherSys.GetOne(oRequest.otherWebSysId);
								sUrl = oOtherSys.userInfoUrl;
							}
							if (ViCommInterface.GetWebUserInfo(sUrl,oRequest.otherWebUserId,out iBalPoint,out  pTel,out  pPassword,out  pCondition,out  pRetCd) == ViCommConst.USER_INFO_OK) {
								sResult = "0";
								if (oRequest.chargePoint > 0) {
									iSec = (iBalPoint / oRequest.chargePoint) * oRequest.chargeUnitSec;
									if (iSec > 86400) {
										iSec = 86400;
									}
								} else {
									iSec = 86400;
								}
							}
						} else {
							sResult = "0";
							StartIVPService(sIvpRequestSeq,"",sProcSeq,out iSec);
						}
						break;
				}
			}
		}

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}&sec={1}",sResult,iSec));
		Response.End();
	}

	private void ChangeTalk(string pDtmf) {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sUrl;
		int iBalPoint = 0;
		string pTel = "";
		string pPassword = "";
		string pCondition = "";
		string pRetCd = "";
		string sResult = "9";
		int iSec = 0;
		string sSubRequestNo = "";
		string sGuidance = "";

		using (IVPRequest oRequest = new IVPRequest()) {
			if (oRequest.GetRequest(sIvpRequestSeq)) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("ADD_TALK_REQUEST");
					db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
					db.ProcedureInParm("PDTMF_ACTION",DbSession.DbType.VARCHAR2,pDtmf);
					db.ProcedureOutParm("PADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER);
					db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
					db.ProcedureOutParm("PSTART_GUIDANCE",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
					if (!db.GetStringValue("PSTART_GUIDANCE").Equals("")) {
						sGuidance = string.Format("&gsec=20&gdest={0}",db.GetStringValue("PSTART_GUIDANCE"));
					}
					oRequest.chargeUnitSec = db.GetIntValue("PCHARGE_UNIT_SEC");
					oRequest.chargePoint = db.GetIntValue("PCHARGE_POINT");
					sSubRequestNo = db.GetStringValue("PADD_IVP_REQUEST_SEQ");
				}

				if (oRequest.useOtherSysInfoFlag != 0) {
					using (OtherWebSite oOtherSys = new OtherWebSite()) {
						oOtherSys.GetOne(oRequest.otherWebSysId);
						sUrl = oOtherSys.userInfoUrl;
					}
					if (ViCommInterface.GetWebUserInfo(sUrl,oRequest.otherWebUserId,out iBalPoint,out  pTel,out  pPassword,out  pCondition,out  pRetCd) == ViCommConst.USER_INFO_OK) {
						sResult = "0";
						if (oRequest.chargePoint > 0) {
							iSec = (iBalPoint / oRequest.chargePoint) * oRequest.chargeUnitSec;
							if (iSec > 86400) {
								iSec = 86400;
							}
						} else {
							iSec = 86400;
						}
					}
				} else {
					sResult = "0";
					StartIVPService(sIvpRequestSeq,sSubRequestNo,"0",out iSec);
				}
			}
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}&sec={1}&subreqno={2}",sResult,iSec,sSubRequestNo) + sGuidance);
		Response.End();
	}

	private void ReportIvpUsedSec() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sStartDate = iBridUtil.GetStringValue(Request.QueryString["start"]);
		string sEndDate = iBridUtil.GetStringValue(Request.QueryString["end"]);
		string sChargeType = iBridUtil.GetStringValue(Request.QueryString["type"]);
		string sChargeSec = iBridUtil.GetStringValue(Request.QueryString["sec"]);
		string sAddIvpReuqestSeq = iBridUtil.GetStringValue(Request.QueryString["subreqno"]);
		string sUrl;
		int iUsedPoint;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CREATE_IVP_USED_REPORT");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sAddIvpReuqestSeq);
			db.ProcedureInParm("PSTART_DATE",DbSession.DbType.VARCHAR2,sStartDate);
			db.ProcedureInParm("PEND_DATE",DbSession.DbType.VARCHAR2,sEndDate);
			db.ProcedureInParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,sChargeType);
			db.ProcedureInParm("PCHARGE_SEC",DbSession.DbType.NUMBER,int.Parse(sChargeSec));
			db.ProcedureInParm("PVCS_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iUsedPoint = int.Parse(db.GetStringValue("PCHARGE_POINT"));
		}

		using (IVPRequest oRequest = new IVPRequest()) {
			if (oRequest.GetRequest(sIvpRequestSeq)) {
				if (oRequest.useOtherSysInfoFlag != 0) {
					using (OtherWebSite oOtherSys = new OtherWebSite()) {
						oOtherSys.GetOne(oRequest.otherWebSysId);
						sUrl = oOtherSys.userInfoUrl;
					}
					ViCommInterface.TransUsedInfo(sUrl,oRequest.requestSiteCd,oRequest.otherWebUserId,iUsedPoint,sChargeType);
				}
			}
		}

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

	private void ReportCallStatus() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sProcSeq = iBridUtil.GetStringValue(Request.QueryString["procseq"]);
		string sStatus = iBridUtil.GetStringValue(Request.QueryString["status"]);
		string sTalkEndReason = iBridUtil.GetStringValue(Request.QueryString["talkend"]);
		string sDisconnReason = iBridUtil.GetStringValue(Request.QueryString["disconn"]);

		string sCastLoginId = string.Empty;
		string sCastUseTerminalType = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TALK_STATUS");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,sProcSeq);
			db.ProcedureInParm("PCALL_STATUS",DbSession.DbType.VARCHAR2,sStatus);
			db.ProcedureInParm("PTALK_END_REASON",DbSession.DbType.VARCHAR2,sTalkEndReason);
			db.ProcedureInParm("PDISCONNECT_REASON",DbSession.DbType.VARCHAR2,sDisconnReason);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("pCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCAST_USE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			sCastLoginId = db.GetStringValue("pCAST_LOGIN_ID");
			sCastUseTerminalType = db.GetStringValue("pCAST_USE_TERMINAL_TYPE");
		}

		#region MASHUP SSDB用処理
		string sSsdbUrl = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["ssdbUrl"]);
		//SIPかつWEBCONFIG設定済の場合のみﾘｸｴｽﾄ送信
		if (!sSsdbUrl.Equals(string.Empty) && sCastUseTerminalType.Equals(ViCommConst.TERM_SIP)) {
			string sRequest = string.Format(sSsdbUrl,sCastLoginId,sStatus.Equals(ViCommConst.CALL_RPT_DISCONNECT) ? "0" : "1");
			
			ViCommInterface.TransToParent(sRequest,true);
		}
		#endregion
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

	private void ReportIncommingStatus() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sStatus = iBridUtil.GetStringValue(Request.QueryString["status"]);
		string sCID = iBridUtil.GetStringValue(Request.QueryString["cid"]);
		int pDisconnectFlag = 0;
		if (sStatus.Equals("3")) {
			pDisconnectFlag = 1;
		}
		string sResult = string.Empty;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_INCOMMING_CALL_STATUS");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PDISCONNECT_FLAG",DbSession.DbType.NUMBER,pDisconnectFlag);
			db.ProcedureInParm("PCID",DbSession.DbType.VARCHAR2,sCID);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("PRESULT");
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",sResult));
		Response.End();
	}

	private void ReportRecordingStatus() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sFileNM = iBridUtil.GetStringValue(Request.QueryString["filenm"]);
		string sRecSec = iBridUtil.GetStringValue(Request.QueryString["recsec"]);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TALK_RECORDING_COMPLITE");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PIVP_FILE_NM",DbSession.DbType.VARCHAR2,sFileNM);
			db.ProcedureInParm("PRECORDING_SEC",DbSession.DbType.NUMBER,decimal.Parse(sRecSec));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

	private void ReportShareLiveUsedSec() {
		string sShareLiveKey = iBridUtil.GetStringValue(Request.QueryString["shkey"]);
		string sStartDate = iBridUtil.GetStringValue(Request.QueryString["start"]);
		string sEndDate = iBridUtil.GetStringValue(Request.QueryString["end"]);
		string sChargeSec = iBridUtil.GetStringValue(Request.QueryString["sec"]);
		string sAcceptSeq = iBridUtil.GetStringValue(Request.QueryString["accept"]);
		string sIvpSiteNm = System.Web.HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["sitenm"]));

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CREATE_SHARE_LIVE_SALES_REPORT");
			db.ProcedureInParm("PSHARE_LIVE_TALK_KEY",DbSession.DbType.VARCHAR2,sShareLiveKey);
			db.ProcedureInParm("PCHARGE_START_DATE",DbSession.DbType.VARCHAR2,sStartDate);
			db.ProcedureInParm("PCHARGE_END_DATE",DbSession.DbType.VARCHAR2,sEndDate);
			db.ProcedureInParm("PCHARGE_SEC",DbSession.DbType.NUMBER,int.Parse(sChargeSec));
			db.ProcedureInParm("PIVP_SITE_NM",DbSession.DbType.VARCHAR2,sIvpSiteNm);
			db.ProcedureInParm("PIVP_ACCEPT_SEQ",DbSession.DbType.VARCHAR2,sAcceptSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}

	private void CastSipLogin() {
		string sId = iBridUtil.GetStringValue(Request.QueryString["id"]);
		string sPassword = iBridUtil.GetStringValue(Request.QueryString["pw"]);
		string sHour = iBridUtil.GetStringValue(Request.QueryString["hour"]);
		string sMin = iBridUtil.GetStringValue(Request.QueryString["min"]);
		string sJson = iBridUtil.GetStringValue(Request.QueryString["json"]);
		int iSipLocalConnect;
		int.TryParse(iBridUtil.GetStringValue(Request.QueryString["localdomain"]),out iSipLocalConnect);

		DateTime dEndDate = new DateTime();
		bool bNoEnd = false;

		if ((!sHour.Equals("")) && (!sMin.Equals("")) && (!sHour.Equals("----")) && (!sMin.Equals("----"))) {
			dEndDate = DateTime.Parse(System.DateTime.Now.ToString().Substring(0,10) + " " + sHour + ":" + sMin + ":00");
			if (dEndDate < System.DateTime.Now) {
				dEndDate = dEndDate.AddDays(1);
			}
		} else if ((!sMin.Equals("")) && (!sMin.Equals("----"))) {
			int iMin = -1;
			if (!sMin.Equals("")) {
				int.TryParse(sMin,out iMin);
			};
			if (iMin > 0) {
				dEndDate = System.DateTime.Now.AddMinutes(iMin);
			}
		} else {
			bNoEnd = true;
		}

		using (Cast oCast = new Cast()) {
			if (oCast.GetOne(sId,sPassword)) {
				if (oCast.uri.Equals("")) {
					oCast.UriCreate(oCast.userSeq);
				}
			}
		}

		string sResult,sSipId,sSipPW,pUID,sSipDomain;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_SIP_LOGIN");
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sId);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,sPassword);
			if (bNoEnd == false) {
				db.ProcedureInParm("PEND_DATE",DbSession.DbType.DATE,dEndDate);
			} else {
				db.ProcedureInParm("PEND_DATE",DbSession.DbType.DATE,null);
			}
			db.ProcedureInParm("pSIP_LOCAL_CONNET_FLAG",DbSession.DbType.NUMBER,iSipLocalConnect);
			db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIP_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIP_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIP_DOMIAN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("PRESULT");
			sSipId = db.GetStringValue("PSIP_ID");
			sSipPW = db.GetStringValue("PSIP_PASSWORD");
			sSipDomain = db.GetStringValue("PSIP_DOMIAN");
			pUID = db.GetStringValue("PUSER_SEQ");
		}

		if (sJson.Equals("1")) {
			LoginResultByJson(sResult,sSipId,sSipPW,sSipDomain,pUID);
		} else {
			Response.ContentType = "text/html";
			sResult = string.Format("result={0}&sipid={1}&sippw={2}&uid={3}&sipdomain={4}",sResult,sSipId,sSipPW,pUID,sSipDomain);
			Response.Write(sResult);
		}

		Response.End();
	}

	private void LoginResultByJson(string pResult,string pSipId,string pSipPW,string pSipDomain,string pUID) {
		string jsondata = "{\"result\":\"" + pResult + "\"," +
							"\"sipid\":\"" + pSipId + "\"," +
							"\"sippw\":\"" + pSipPW + "\"," +
							"\"sipdomain\":\"" + pSipDomain + "\"," +
							"\"uid\":\"" + pUID + "\"}";

		Response.ContentType = "text/plain";
		Response.Write("if (typeof(JsonpData) == 'undefined') JsonpData = {};\n");
		Response.Write("JsonpData.data=" + jsondata + ";\n");
		Response.Write("if (typeof(JsonpData.onload) == 'function') JsonpData.onload(JsonpData.data);");

	}

	private void CastSipLogout() {
		string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["uid"]);
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_SIP_LOGOUT");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sCastSeq);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = string.Format("result={0}",db.GetStringValue("PRESULT"));
		}

		Response.ContentType = "text/html";
		Response.Write(sResult);
		Response.End();
	}

	public void StartIVPService(string pIvpRequestSeq,string pAddIvpRequestSeq,string pCallStatuSeq,out int pAvaSec) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("START_IVP_SERVICE");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("PADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pAddIvpRequestSeq);
			db.ProcedureInParm("PCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,pCallStatuSeq);
			db.ProcedureOutParm("PAVAILABLE_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pAvaSec = db.GetIntValue("PAVAILABLE_SEC");
		}
	}

	private void SmartPhoneTelCertify() {
		string sIvpRequestSeq = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sTel = iBridUtil.GetStringValue(Request.QueryString["cid"]);
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CERTIFY_SMART_PHONE");
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,sIvpRequestSeq);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,sTel);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
	}
}