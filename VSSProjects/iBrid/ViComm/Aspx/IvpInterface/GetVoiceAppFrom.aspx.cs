﻿using System;
using System.Web;
using System.IO;
using System.Data;
using System.Threading;
using System.Configuration;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[DataContract]
public class VoiceAppFrom {
	[DataMember]
	public string handle_nm;
}

public partial class GetVoiceAppFrom:System.Web.UI.Page {
	private static readonly Regex regEmoji = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);

	protected void Page_Load(object sender,EventArgs e) {
		string sFromUserSeq = iBridUtil.GetStringValue(Request.QueryString["from"]);
		int iFromUserSeq;

		if (string.IsNullOrEmpty(sFromUserSeq)) {
			ResponseError("-1");
			return;
		} else if (!int.TryParse(sFromUserSeq,out iFromUserSeq)) {
			ResponseError("-2");
			return;
		}

		DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(VoiceAppFrom));
		VoiceAppFrom oVoiceAppFrom = new VoiceAppFrom();

		using (User oUser = new User()) {
			if (oUser.GetOne(sFromUserSeq)) {
				if (oUser.sexCd.Equals(ViCommConst.MAN)) {
					using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
						if (oUserManCharacter.GetOne(PwViCommConst.MAIN_SITE_CD,sFromUserSeq,ViCommConst.MAIN_CHAR_NO)) {
							oVoiceAppFrom.handle_nm = oUserManCharacter.handleNm;
						} else {
							ResponseError("-5");
							return;
						}
					}
				} else if (oUser.sexCd.Equals(ViCommConst.OPERATOR)) {
					using (CastCharacter oCastCharacter = new CastCharacter()) {
						if (oCastCharacter.GetOne(PwViCommConst.MAIN_SITE_CD,sFromUserSeq,ViCommConst.MAIN_CHAR_NO)) {
							oVoiceAppFrom.handle_nm = oCastCharacter.handleNm;
						} else {
							ResponseError("-6");
							return;
						}
					}
				} else {
					ResponseError("-4");
					return;
				}
			} else {
				ResponseError("-3");
				return;
			}
		}

		oVoiceAppFrom.handle_nm = string.Format("{0}さん",regEmoji.Replace(oVoiceAppFrom.handle_nm,""));

		MemoryStream stream = new MemoryStream();
		serializer.WriteObject(stream,oVoiceAppFrom);
		stream.Position = 0;
		StreamReader reader = new StreamReader(stream);
		string sJson = reader.ReadToEnd();

		Response.ContentType = "application/json";
		Response.Write("[" + sJson + "]");
		Response.End();
		return;
	}

	private void ResponseError(string sValue) {
		Response.ContentType = "application/json";
		Response.Write("[{\"error\":\"" + sValue + "\"}]");
		Response.End();
	}
}