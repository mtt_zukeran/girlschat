﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 出演者

--	Progaram ID		: Cast
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Cast:DbSession {
	public string userSeq;
	public string uri;

	public Cast() {
	}

	public bool GetOne(string pLoginID,string pLoginPassword) {
		DataSet ds;

		bool bExist = false;
		conn = DbConnect();
		ds = new DataSet();
		string sSql = "SELECT " +
						"USER_SEQ	," +
						"URI		" +
						"FROM VW_CAST06 WHERE LOGIN_ID =:LOGIN_ID AND LOGIN_PASSWORD =:LOGIN_PASSWORD ";
		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("LOGIN_ID",pLoginID);
			cmd.Parameters.Add("LOGIN_PASSWORD",pLoginPassword);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST06");
				if (ds.Tables["VW_CAST06"].Rows.Count != 0) {
					userSeq = iBridUtil.GetStringValue(ds.Tables["VW_CAST06"].Rows[0]["USER_SEQ"]);
					uri = iBridUtil.GetStringValue(ds.Tables["VW_CAST06"].Rows[0]["URI"]);
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public string UriCreate(string pUserSeq) {
		string sIvpLocCd = "";
		string sIvpSiteCd = "";
		string sUrl = "";
		string sSipId = "";
		string sSipPw = "";
		string sResult = string.Empty;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("SIP_IVP_LOC_CD",ref sIvpLocCd);
			oSys.GetValue("SIP_IVP_SITE_CD",ref sIvpSiteCd);
			oSys.GetValue("SIP_REGIST_URL",ref sUrl);

			if ((!sIvpLocCd.Equals("")) && (!sIvpSiteCd.Equals("")) && (!sUrl.Equals(""))) {
				sResult = ViCommInterface.RegistSIPTerminal(string.Format("{0}?loc={1}&site={2}",sUrl,sIvpLocCd,sIvpSiteCd),ref sSipId,ref sSipPw);
			}

		}
		if (sResult.Equals("0")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("URI_MAINTE");
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
				db.ProcedureInParm("PURI",DbSession.DbType.VARCHAR2,sSipId);
				db.ProcedureInParm("PTERMINAL_PASSWORD",DbSession.DbType.VARCHAR2,sSipPw);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}

		return sSipId;
	}
}