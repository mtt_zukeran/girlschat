/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: パートナーサイト
--	Progaram ID		: PartnerSiteHis
--
--  Creation Date	: 2010.03.17
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class PartnerSiteHis:DbSession {

	public PartnerSiteHis() {
	}

	public DataSet GetOne(string pHistorySeq) {
		DataSet ds = new DataSet();
		conn = DbConnect();

		string sSql = "SELECT " +
							"PARTNER_SITE_SEQ		," +
							"REGIST_USER_ACCEPT_URL	," +
							"HISTORY_SEQ			," +
							"ACCOUNT_LOCK_DAYS		" +
						" FROM " +
							"VW_PARTNER_SITE_HIS01" +
						" WHERE " +
							"HISTORY_SEQ = :HISTORY_SEQ ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("PARTNER_SITE_SEQ",pHistorySeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}
}
