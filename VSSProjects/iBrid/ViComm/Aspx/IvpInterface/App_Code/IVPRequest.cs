﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

/// <summary>
/// Production の概要の説明です


/// </summary>
public class IVPRequest:DbSession {

	public decimal ivpRequestSeq;
	public int chargeUnitSec;
	public int chargePoint;
	public string manUserSiteCd;
	public string manUserSeq;
	public string manCharNo;
	public string castUserSeq;
	public string otherWebSysId;
	public string otherWebUserId;
	public int useOtherSysInfoFlag;
	public string requestSiteCd;
	public string requestId;
	public string actCategorySeq;
	public string outgoingTerminalId;
	public string requesterSexCd;
	public string meetingKey;
	public int availableSec;
	public int useManVoiceappFlag;
	public int useCastVoiceappFlag;

	public IVPRequest() {

	}

	public bool GetRequest(string pIvpRequestSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"IVP_REQUEST_SEQ	," +
						"REQUEST_SITE_CD	," +
						"MAN_USER_SEQ		," +
						"CHARGE_UNIT_SEC	," +
						"CHARGE_POINT		," +
						"REQUEST_ID			," +
						"OTHER_WEB_SYS_ID	," +
						"OTHER_WEB_USER_ID	," +
						"USE_OTHER_SYS_INFO_FLAG," +
						"ACT_CATEGORY_SEQ	" +
					"FROM " +
						"VW_IVP_REQUEST01 " +
					"WHERE " +
						"IVP_REQUEST_SEQ =:IVP_REQUEST_SEQ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("IVP_REQUEST_SEQ",pIvpRequestSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"IVP_REQUEST");
				if (ds.Tables["IVP_REQUEST"].Rows.Count != 0) {
					dr = ds.Tables["IVP_REQUEST"].Rows[0];

					ivpRequestSeq = decimal.Parse(dr["IVP_REQUEST_SEQ"].ToString());
					requestSiteCd = dr["REQUEST_SITE_CD"].ToString();
					manUserSeq = dr["MAN_USER_SEQ"].ToString();
					chargeUnitSec = int.Parse(dr["CHARGE_UNIT_SEC"].ToString());
					chargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
					requestId = dr["REQUEST_ID"].ToString();
					actCategorySeq = dr["ACT_CATEGORY_SEQ"].ToString();
					otherWebSysId = dr["OTHER_WEB_SYS_ID"].ToString();
					otherWebUserId = dr["OTHER_WEB_USER_ID"].ToString();
					useOtherSysInfoFlag = int.Parse(dr["USE_OTHER_SYS_INFO_FLAG"].ToString());
					bExist = true;
				}
			}
		}

		conn.Close();

		return bExist;
	}

	public bool GetOneIncoming(string pFromSid,string pUseFreeDialFlag,bool bFromAppFlag) {
		bool bExist = false;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (bFromAppFlag) {
			SysPrograms.SqlAppendWhere("FROM_USER_SEQ = :FROM_USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":FROM_USER_SEQ",pFromSid));
		} else {
			SysPrograms.SqlAppendWhere("REQUEST_SRC_ID = :REQUEST_SRC_ID",ref sWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_SRC_ID",pFromSid));
		}

		SysPrograms.SqlAppendWhere("REQUEST_DATE > SYSDATE - 1/48",ref sWhereClause);//通話要求から30分以内

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	IVP_REQUEST_SEQ,");
		oSqlBuilder.AppendLine("	OUTGOING_TERMINAL_ID,");
		oSqlBuilder.AppendLine("	MAN_USER_SITE_CD,");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("	MAN_CHAR_NO,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	REQUESTER_SEX_CD,");
		oSqlBuilder.AppendLine("	MEETING_KEY,");
		oSqlBuilder.AppendLine("	REQUEST_ID,");
		oSqlBuilder.AppendLine("	MENU_ID,");
		oSqlBuilder.AppendLine("	TALK_TYPE,");
		oSqlBuilder.AppendLine("	CONNECT_TYPE,");
		oSqlBuilder.AppendLine("	OUTGOING_TERMINAL_TYPE,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE,");
		oSqlBuilder.AppendLine("	SESSION_CONNECT_FLAG,");
		oSqlBuilder.AppendLine("	USE_FREE_DIAL_FLAG,");
		oSqlBuilder.AppendLine("	TALK_SEQ,");
		oSqlBuilder.AppendLine("	USE_MAN_VOICEAPP_FLAG,");
		oSqlBuilder.AppendLine("	USE_CAST_VOICEAPP_FLAG");
		oSqlBuilder.AppendLine("FROM (");
		oSqlBuilder.AppendLine("	SELECT");
		oSqlBuilder.AppendLine("		IVP_REQUEST_SEQ,");
		oSqlBuilder.AppendLine("		OUTGOING_TERMINAL_ID,");
		oSqlBuilder.AppendLine("		MAN_USER_SITE_CD,");
		oSqlBuilder.AppendLine("		MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("		MAN_CHAR_NO,");
		oSqlBuilder.AppendLine("		CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("		REQUESTER_SEX_CD,");
		oSqlBuilder.AppendLine("		MEETING_KEY,");
		oSqlBuilder.AppendLine("		REQUEST_ID,");
		oSqlBuilder.AppendLine("		MENU_ID,");
		oSqlBuilder.AppendLine("		TALK_TYPE,");
		oSqlBuilder.AppendLine("		CONNECT_TYPE,");
		oSqlBuilder.AppendLine("		OUTGOING_TERMINAL_TYPE,");
		oSqlBuilder.AppendLine("		CHARGE_TYPE,");
		oSqlBuilder.AppendLine("		SESSION_CONNECT_FLAG,");
		oSqlBuilder.AppendLine("		USE_FREE_DIAL_FLAG,");
		oSqlBuilder.AppendLine("		TALK_SEQ,");
		oSqlBuilder.AppendLine("		USE_MAN_VOICEAPP_FLAG,");
		oSqlBuilder.AppendLine("		USE_CAST_VOICEAPP_FLAG");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_IVP_REQUEST");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	ORDER BY");
		oSqlBuilder.AppendLine("		REQUEST_DATE DESC");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM <= 1");

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count != 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			string sRequestId = iBridUtil.GetStringValue(dr["REQUEST_ID"]);
			string sMenuId = iBridUtil.GetStringValue(dr["MENU_ID"]);
			string sTalkType = iBridUtil.GetStringValue(dr["TALK_TYPE"]);
			string sConnectType = iBridUtil.GetStringValue(dr["CONNECT_TYPE"]);
			string sOutgoingTerminalType = iBridUtil.GetStringValue(dr["OUTGOING_TERMINAL_TYPE"]);
			string sChargeType = iBridUtil.GetStringValue(dr["CHARGE_TYPE"]);
			string sSessionConnectFlag = iBridUtil.GetStringValue(dr["SESSION_CONNECT_FLAG"]);
			string sUseFreeDialFlag = iBridUtil.GetStringValue(dr["USE_FREE_DIAL_FLAG"]);
			string sTalkSeq = iBridUtil.GetStringValue(dr["TALK_SEQ"]);

			if (sRequestId.Equals("06") && //会話要求(Light)
				sMenuId.Equals("03") &&	//Voice(WShot)
				sTalkType.Equals("1") && //2SHOTチャット
				sConnectType.Equals("2") && //VOICE
				sOutgoingTerminalType.Equals("1") && //携帯電話
				(sChargeType.Equals("03") || sChargeType.Equals("53")) && //「音声電話2shot」または「待機・音声電話2shot」
				sSessionConnectFlag.Equals(ViCommConst.FLAG_OFF_STR) && //未接続
				sUseFreeDialFlag.Equals(pUseFreeDialFlag) && //フリーダイヤル利用フラグ
				sTalkSeq.Equals("")//まだ接続していない
			) {
				ivpRequestSeq = decimal.Parse(iBridUtil.GetStringValue(dr["IVP_REQUEST_SEQ"]));
				outgoingTerminalId = iBridUtil.GetStringValue(dr["OUTGOING_TERMINAL_ID"]);
				manUserSiteCd = iBridUtil.GetStringValue(dr["MAN_USER_SITE_CD"]);
				manUserSeq = iBridUtil.GetStringValue(dr["MAN_USER_SEQ"]);
				manCharNo = iBridUtil.GetStringValue(dr["MAN_CHAR_NO"]);
				castUserSeq = iBridUtil.GetStringValue(dr["CAST_USER_SEQ"]);
				requesterSexCd = iBridUtil.GetStringValue(dr["REQUESTER_SEX_CD"]);
				meetingKey = iBridUtil.GetStringValue(dr["MEETING_KEY"]);
				useManVoiceappFlag = int.Parse(iBridUtil.GetStringValue(dr["USE_MAN_VOICEAPP_FLAG"]));
				useCastVoiceappFlag = int.Parse(iBridUtil.GetStringValue(dr["USE_CAST_VOICEAPP_FLAG"]));
				bExist = true;
			}
		}

		return bExist;
	}

	public bool  GetOneByTwilioCallSid(string pTwilioCallSid) {
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	IVP_REQUEST_SEQ,");
		oSqlBuilder.AppendLine("	AVAILABLE_SEC");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_IVP_REQUEST");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	TWILIO_CALL_SID = :TWILIO_CALL_SID");

		oParamList.Add(new OracleParameter(":TWILIO_CALL_SID",pTwilioCallSid));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count != 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			ivpRequestSeq = decimal.Parse(iBridUtil.GetStringValue(dr["IVP_REQUEST_SEQ"]));
			availableSec = int.Parse(iBridUtil.GetStringValue(dr["AVAILABLE_SEC"]));
			bExist = true;
		}

		return bExist;
	}
}
