﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Interface
--	Title			: システム設定
--	Progaram ID		: Sys
--
--  Creation Date	: 2009.10.22
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

public class Sys:DbSession {
	public string twilioAccountSid;
	public string twilioAuthToken;
	public string twilioTelNo;

	public Sys() {
	}

	public bool GetValue(string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		conn = DbConnect();
		string sSql = "SELECT " +
							"SIP_IVP_LOC_CD		," +
							"SIP_IVP_SITE_CD	," +
							"SIP_REGIST_URL		," +
							"IVP_ADMIN_URL		," +
							"SIP_DOMAIN	" +
						"FROM " +
							"T_SYS ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SYS");
				if (ds.Tables["T_SYS"].Rows.Count != 0) {
					dr = ds.Tables["T_SYS"].Rows[0];
					pValue = dr[pItem].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}

	public bool GetOne() {
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	TWILIO_ACCOUNT_SID,");
		oSqlBuilder.AppendLine("	TWILIO_AUTH_TOKEN,");
		oSqlBuilder.AppendLine("	TWILIO_TEL_NO");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_SYS");

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			twilioAccountSid = iBridUtil.GetStringValue(dr["TWILIO_ACCOUNT_SID"]);
			twilioAuthToken = iBridUtil.GetStringValue(dr["TWILIO_AUTH_TOKEN"]);
			twilioTelNo = iBridUtil.GetStringValue(dr["TWILIO_TEL_NO"]);
			bExist = true;
		}

		return bExist;		
	}
}
