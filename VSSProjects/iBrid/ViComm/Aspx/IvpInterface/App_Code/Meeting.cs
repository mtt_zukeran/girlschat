﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Interface
--	Title			: 会話待ち合わせ（Love待機）--	Progaram ID		: Meeting
--  Creation Date	: 2015.01.19
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;

public class Meeting:DbSession {
	public Meeting() {
	}

	public bool IsExistWaitingVoice(string pMeetingKey,string pUserSeq,string pPartnerUserSeq) {
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MEETING");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	MEETING_KEY = :MEETING_KEY AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND");
		oSqlBuilder.AppendLine("	MENU_ID = '03' AND");//Voice(WShot)
		oSqlBuilder.AppendLine("	REQUEST_ID = '00' AND");//会話要求(暫定)
		oSqlBuilder.AppendLine("	CHARGE_TYPE = '03' AND");//音声電話2shot
		oSqlBuilder.AppendLine("	MEETING_END_SCH_DATE > SYSDATE");

		oParamList.Add(new OracleParameter(":MEETING_KEY",pMeetingKey));
		oParamList.Add(new OracleParameter(":USER_SEQ",int.Parse(pUserSeq)));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",int.Parse(pPartnerUserSeq)));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			bExist = true;
		}

		return bExist;
	}
}
