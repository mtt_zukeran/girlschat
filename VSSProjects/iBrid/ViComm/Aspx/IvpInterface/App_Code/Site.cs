﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: サイト--	Progaram ID		: Site
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using iBridCommLib;

public class Site:DbSession {

	public Site() {
	}

	public bool GetValue(string pSiteCd,string pItem,out string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		conn = DbConnect();
		string sSql = "SELECT " +
							"SITE_CD    ," +
							"SITE_NM	," +
							"SUB_HOST_NM " +
						"FROM " +
							"T_SITE " +
						"WHERE " +
							"SITE_CD = :SITE_CD ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {

			cmd.Parameters.Add("SITE_CD",pSiteCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					dr = ds.Tables["T_SITE"].Rows[0];
					pValue = dr[pItem].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
