﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Interface
--	Title			: 出演者待機--	Progaram ID		: LoginUser
--  Creation Date	: 2015.01.14
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;

public class LoginUser:DbSession {
	public LoginUser() {
	}

	public bool IsExistWaiting(string pUserSeq) {
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_LOGIN_USER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	LOGOFF_DATE IS NULL AND");
		oSqlBuilder.AppendLine("	DUMMY_TALK_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	ADMIN_WAIT_FLAG = 0");

		oParamList.Add(new OracleParameter(":USER_SEQ",int.Parse(pUserSeq)));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			bExist = true;
		}

		return bExist;
	}
}
