/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: 男性会員キャラクター
--	Progaram ID		: UserManCharacter
--
--  Creation Date	: 2010.03.17
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using System.Text;

public class UserManCharacter:DbSession {
	public string handleNm;

	public UserManCharacter() {
	}

	public DataSet GetOne(string pUserSeq) {
		DataSet ds = new DataSet();
		conn = DbConnect();
		
		string sSql = "SELECT " +
							"MOBILE_CARRIER_CD	," +
							"TERMINAL_UNIQUE_ID	," +
							"IMODE_ID			," +
							"TEL				," +
							"EMAIL_ADDR			," +
							"LOGIN_PASSWORD		," +
							"HANDLE_NM			," +
							"BIRTHDAY			," +
							"MOBILE_TERMINAL_NM	," +
							"TEL				," +
							"LOGIN_ID			" +
						" FROM " +
							"VW_USER_MAN_CHARACTER03" +
						" WHERE VW_USER_MAN_CHARACTER03.USER_SEQ = :USER_SEQ ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds);
			}
		}
		conn.Close();

		return ds;
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo) {
		bool bExist = false;
		int iUserSeq;

		if (string.IsNullOrEmpty(pSiteCd) || string.IsNullOrEmpty(pUserSeq) || string.IsNullOrEmpty(pUserCharNo)) {
			throw new ArgumentNullException();
		} else if (!int.TryParse(pUserSeq,out iUserSeq)) {
			throw new ArgumentException("pUserSeq");
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	HANDLE_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count != 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			handleNm = iBridUtil.GetStringValue(dr["HANDLE_NM"]);
			bExist = true;
		}

		return bExist;
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		int pUserRankMask,
		int pUserStatusRank,
		int pUserOnlineStatus,
		int pCarrier,
		string pTel,
		string pLoginID,
		string pBalPointFrom,
		string pBalPointTo,
		string pEMailAddr,
		string pHandleNm,
		string pAdCd,
		string pRemarks,
		string pTotalReceiptAmtFrom,
		string pTotalReceiptAmtTo,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pFirstLoginDayFrom,
		string pFirstLoginDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pLastPointUsedDayFrom,
		string pLastPointUsedDayTo,
		string pFirstReceiptDayFrom,
		string pFirstReceiptDayTo,
		string pLastReceiptDayFrom,
		string pLastReceiptDayTo,
		string pTotalReceiptCountFrom,
		string pTotalReceiptCountTo,
		string pNonExistMailAddrFlag,
		string pFirstLoginTimeFrom,
		string pFirstLoginTimeTo,
		string pLastLoginTimeFrom,
		string pLastLoginTimeTo,
		string pLastPointUsedTimeFrom,
		string pLastPointUsedTimeTo,
		string pRegistTimeFrom,
		string pRegistTimeTo,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		string pBillAmtFrom,
		string pBillAmtTo,
		string pMchaTotalReceiptAmtFrom,
		string pMchaTotalReceiptAmtTo,
		string pMchaTotalReceiptCountFrom,
		string pMchaTotalReceiptCountTo,
		string pUrgeLevel,
		bool pCreditMeasured,
		bool pMchaUsedFlag,
		bool pCreditUsedFlag,
		string pNaFlag,
		string pRegistServicePointFlag,
		bool pNotExistFirstPointUsedDate,
		bool pExcludeEmailAddr,
		string pRichinoRankFrom,
		string pRichinoRankTo,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,USER_SEQ ";
		string sSql = "SELECT USER_SEQ,LOGIN_ID " +
						"FROM(" +
						" SELECT " +
							"VW_USER_MAN_CHARACTER03.*			, " +
							"ROW_NUMBER() OVER ( ORDER BY VW_USER_MAN_CHARACTER03.SITE_CD,VW_USER_MAN_CHARACTER03.USER_SEQ ) AS RNUM " +
						" FROM " +
							"VW_USER_MAN_CHARACTER03 ";

		// リッチーノランクが指定されてる場合、テーブル連結追加
		if (!string.IsNullOrEmpty(pRichinoRankFrom) || !string.IsNullOrEmpty(pRichinoRankTo)) {
			sSql = sSql + ",T_USER_MAN_CHARACTER_EX ";
		}

		string sWhere = "";
		OracleParameter[] oParms = CreateWhere(
										pSiteCd,
										pUserRankMask,
										pUserStatusRank,
										pUserOnlineStatus,
										pCarrier,
										pTel,
										pLoginID,
										pBalPointFrom,
										pBalPointTo,
										pEMailAddr,
										pHandleNm,
										pAdCd,
										pRemarks,
										pTotalReceiptAmtFrom,
										pTotalReceiptAmtTo,
										pRegistDayFrom,
										pRegistDayTo,
										pFirstLoginDayFrom,
										pFirstLoginDayTo,
										pLastLoginDayFrom,
										pLastLoginDayTo,
										pLastPointUsedDayFrom,
										pLastPointUsedDayTo,
										pFirstReceiptDayFrom,
										pFirstReceiptDayTo,
										pLastReceiptDayFrom,
										pLastReceiptDayTo,
										pTotalReceiptCountFrom,
										pTotalReceiptCountTo,
										pNonExistMailAddrFlag,
										pFirstLoginTimeFrom,
										pFirstLoginTimeTo,
										pLastLoginTimeFrom,
										pLastLoginTimeTo,
										pLastPointUsedTimeFrom,
										pLastPointUsedTimeTo,
										pRegistTimeFrom,
										pRegistTimeTo,
										pLastBlackDayFrom,
										pLastBlackDayTo,
										pBillAmtFrom,
										pBillAmtTo,
										pMchaTotalReceiptAmtFrom,
										pMchaTotalReceiptAmtTo,
										pMchaTotalReceiptCountFrom,
										pMchaTotalReceiptCountTo,
										pUrgeLevel,
										pCreditMeasured,
										pMchaUsedFlag,
										pCreditUsedFlag,
										pNaFlag,
										pRegistServicePointFlag,
										pNotExistFirstPointUsedDate,
										pExcludeEmailAddr,
										pRichinoRankFrom,
										pRichinoRankTo,
										ref sWhere);

		sSql = sSql + sWhere +
				string.Format(" AND	VW_USER_MAN_CHARACTER03.USER_CHAR_NO = '{0}'", ViCommConst.MAIN_CHAR_NO);

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < oParms.Length;i++) {
				cmd.Parameters.Add((OracleParameter)oParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_MAN_CHARACTER03");
			}
		}
		conn.Close();
		return ds;
	}

	private OracleParameter[] CreateWhere(
		string pSiteCd,
		int pUserRankMask,
		int pUserStatusRank,
		int pUserOnlineStatus,
		int pCarrier,
		string pTel,
		string pLoginID,
		string pBalPointFrom,
		string pBalPointTo,
		string pEMailAddr,
		string pHandleNm,
		string pAdCd,
		string pRemarks,
		string pTotalReceiptAmtFrom,
		string pTotalReceiptAmtTo,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pFirstLoginDayFrom,
		string pFirstLoginDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pLastPointUsedDayFrom,
		string pLastPointUsedDayTo,
		string pFirstReceiptDayFrom,
		string pFirstReceiptDayTo,
		string pLastReceiptDayFrom,
		string pLastReceiptDayTo,
		string pTotalReceiptCountFrom,
		string pTotalReceiptCountTo,
		string pNonExistMailAddrFlag,
		string pFirstLoginTimeFrom,
		string pFirstLoginTimeTo,
		string pLastLoginTimeFrom,
		string pLastLoginTimeTo,
		string pLastPointUsedTimeFrom,
		string pLastPointUsedTimeTo,
		string pRegistTimeFrom,
		string pRegistTimeTo,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		string pBillAmtFrom,
		string pBillAMtTo,
		string pMchaTotalReceiptAmtFrom,
		string pMchaTotalReceiptAmtTo,
		string pMchaTotalReceiptCountFrom,
		string pMchaTotalReceiptCountTo,
		string pUrgeLevel,
		bool pCreditMeasured,
		bool pMchaUsedFlag,
		bool pCreditUsedFlag,
		string pNaFlag,
		string pRegistServicePointFlag,
		bool pNotExistFirstPointUsedDate,
		bool pExcludeEmailAddr,
		string pRichinoRankFrom,
		string pRichinoRankTo,
		ref string pWhere
	) {
		pWhere = "";

		string[] sValues = new string[10];
		int iCnt = 0;

		SetDefaultTimeValue(ref pFirstLoginTimeFrom,ref pFirstLoginTimeTo);
		SetDefaultTimeValue(ref pLastLoginTimeFrom,ref pLastLoginTimeTo);
		SetDefaultTimeValue(ref pLastPointUsedTimeFrom,ref pLastPointUsedTimeTo);
		SetDefaultTimeValue(ref pRegistTimeFrom,ref pRegistTimeTo);

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE VW_USER_MAN_CHARACTER03.SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (pUserRankMask != 0) {
			if ((pUserRankMask & ViCommConst.MASK_RANK_A) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_A;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_B) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_B;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_C) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_C;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_D) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_D;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_E) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_E;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_F) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_F;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_G) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_G;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_H) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_H;
			}

			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.USER_RANK IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":USER_RANK" + i.ToString() + ",";
				list.Add(new OracleParameter("USER_RANK",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		iCnt = 0;
		if (pUserStatusRank != 0) {
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NEW) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NEW;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_BLACK) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_BLACK;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NO_RECEIPT) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NO_RECEIPT;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NO_USED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NO_USED;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NORMAL) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NORMAL;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_DELAY) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_DELAY;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_HOLD) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_HOLD;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_RESIGNED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_RESIGNED;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_STOP) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_STOP;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_TEL_NO_AUTH) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_TEL_NO_AUTH;
			}
			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.USER_STATUS IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":USER_STATUS" + i.ToString() + ",";
				list.Add(new OracleParameter("USER_STATUS",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}
		if (pCreditMeasured) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.CREDIT_MEASURED_RATE_TARGET = :CREDIT_MEASURED_RATE_TARGET",ref pWhere);
			list.Add(new OracleParameter("CREDIT_MEASURED_RATE_TARGET",ViCommConst.FLAG_ON));
		}
		iCnt = 0;
		if (pUserOnlineStatus != 0) {
			if ((pUserOnlineStatus & ViCommConst.MASK_LOGINED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_LOGINED.ToString();
			}
			if ((pUserOnlineStatus & ViCommConst.MASK_OFFLINE) != 0) {
				sValues[iCnt++] = ViCommConst.USER_OFFLINE.ToString();
			}
			if ((pUserOnlineStatus & ViCommConst.MASK_TALKING) != 0) {
				sValues[iCnt++] = ViCommConst.USER_TALKING.ToString();
			}
			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.CHARACTER_ONLINE_STATUS IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":CHARACTER_ONLINE_STATUS" + i.ToString() + ",";
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		iCnt = 0;
		if (pCarrier != 0) {
			if ((pCarrier & ViCommConst.MASK_DOCOMO) != 0) {
				sValues[iCnt++] = ViCommConst.DOCOMO;
			}
			if ((pCarrier & ViCommConst.MASK_KDDI) != 0) {
				sValues[iCnt++] = ViCommConst.KDDI;
			}
			if ((pCarrier & ViCommConst.MASK_SOFTBANK) != 0) {
				sValues[iCnt++] = ViCommConst.SOFTBANK;
			}
			if ((pCarrier & ViCommConst.MASK_ANDROID) != 0) {
				sValues[iCnt++] = ViCommConst.ANDROID;
			}
			if ((pCarrier & ViCommConst.MASK_IPHONE) != 0) {
				sValues[iCnt++] = ViCommConst.IPHONE;
			}
			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.MOBILE_CARRIER_CD IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":MOBILE_CARRIER_CD" + i.ToString() + ",";
				list.Add(new OracleParameter("MOBILE_CARRIER_CD",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		if (!pTel.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.TEL LIKE :TEL",ref pWhere);
			list.Add(new OracleParameter("TEL",pTel + "%"));
		}

		if (!pLoginID.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.LOGIN_ID LIKE :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginID + "%"));
		}

		if (!pBalPointFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.BAL_POINT >= :BAL_POINT_FROM AND VW_USER_MAN_CHARACTER03.BAL_POINT <= :BAL_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("BAL_POINT_FROM",int.Parse(pBalPointFrom)));
			list.Add(new OracleParameter("BAL_POINT_TO",int.Parse(pBalPointTo)));
		}

		if (!pEMailAddr.Equals("")) {
			if (pExcludeEmailAddr == true) {
				SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.EMAIL_ADDR NOT LIKE :EMAIL_ADDR",ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.EMAIL_ADDR LIKE :EMAIL_ADDR",ref pWhere);
			}
			list.Add(new OracleParameter("EMAIL_ADDR",'%' + pEMailAddr + "%"));
		}

		if (!pHandleNm.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.HANDLE_NM LIKE :HANDLE_NM",ref pWhere);
			list.Add(new OracleParameter("HANDLE_NM",'%' + pHandleNm + "%"));
		}

		if (!pAdCd.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.AD_CD = :AD_CD",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pAdCd));
		}

		if (!pRemarks.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.REMARKS1 LIKE :REMARKS1 OR VW_USER_MAN_CHARACTER03.REMARKS2 LIKE :REMARKS2 OR VW_USER_MAN_CHARACTER03.REMARKS3 LIKE :REMARKS3)",ref pWhere);
			list.Add(new OracleParameter("REMARKS1",'%' + pRemarks + "%"));
			list.Add(new OracleParameter("REMARKS2",'%' + pRemarks + "%"));
			list.Add(new OracleParameter("REMARKS3",'%' + pRemarks + "%"));
		}

		if (!pTotalReceiptAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_AMT >= :TOTAL_RECEIPT_AMT_FROM AND VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_AMT <= :TOTAL_RECEIPT_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("TOTAL_RECEIPT_AMT_FROM",int.Parse(pTotalReceiptAmtFrom)));
			list.Add(new OracleParameter("TOTAL_RECEIPT_AMT_TO",int.Parse(pTotalReceiptAmtTo)));
		}

		if (!pBillAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.BILL_AMT >= :BILL_AMT_FROM AND VW_USER_MAN_CHARACTER03.BILL_AMT <= :BILL_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("BILL_AMT_FROM",int.Parse(pBillAmtFrom)));
			list.Add(new OracleParameter("BILL_AMT_TO",int.Parse(pBillAMtTo)));
		}

		if (!pTotalReceiptCountFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_COUNT >= :TOTAL_RECEIPT_COUNT_FROM AND VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_COUNT <= :TOTAL_RECEIPT_COUNT_TO)",ref pWhere);
			list.Add(new OracleParameter("TOTAL_RECEIPT_COUNT_FROM",int.Parse(pTotalReceiptCountFrom)));
			list.Add(new OracleParameter("TOTAL_RECEIPT_COUNT_TO",int.Parse(pTotalReceiptCountTo)));
		}

		if (!pMchaTotalReceiptAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_AMT >= :POINT_AFFILIATE_TOTAL_AMT_FROM AND VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_AMT <= :POINT_AFFILIATE_TOTAL_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_AMT_FROM",int.Parse(pMchaTotalReceiptAmtFrom)));
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_AMT_TO",int.Parse(pMchaTotalReceiptAmtTo)));
		}

		if (!pMchaTotalReceiptCountFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_COUNT >= :POINT_AFFILIATE_TOTAL_COUNT_FROM AND VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_COUNT <= :POINT_AFFILIATE_TOTAL_COUNT_TO)",ref pWhere);
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_COUNT_FROM",int.Parse(pMchaTotalReceiptCountFrom)));
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_COUNT_TO",int.Parse(pMchaTotalReceiptCountTo)));
		}

		if (!pRegistDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pRegistDayFrom + " " + pRegistTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pRegistDayTo + " " + pRegistTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.REGIST_DATE >= :REGIST_DATE_FROM AND VW_USER_MAN_CHARACTER03.REGIST_DATE < :REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pFirstLoginDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pFirstLoginDayFrom + " " + pFirstLoginTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pFirstLoginDayTo + " " + pFirstLoginTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.FIRST_LOGIN_DATE >= :FIRST_LOGIN_DATE_FROM AND VW_USER_MAN_CHARACTER03.FIRST_LOGIN_DATE < :FIRST_LOGIN_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("FIRST_LOGIN_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("FIRST_LOGIN_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pLastLoginDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pLastLoginDayFrom + " " + pLastLoginTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pLastLoginDayTo + " " + pLastLoginTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.LAST_LOGIN_DATE >= :LAST_LOGIN_DATE_FROM AND VW_USER_MAN_CHARACTER03.LAST_LOGIN_DATE < :LAST_LOGIN_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_LOGIN_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_LOGIN_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pLastPointUsedDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pLastPointUsedDayFrom + " " + pLastPointUsedTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pLastPointUsedDayTo + " " + pLastPointUsedTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.LAST_POINT_USED_DATE >= :LAST_POINT_USED_DATE_FROM AND VW_USER_MAN_CHARACTER03.LAST_POINT_USED_DATE < :LAST_POINT_USED_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_POINT_USED_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_POINT_USED_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pFirstReceiptDayFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.FIRST_RECEIPT_DAY >= :FIRST_RECEIPT_DAY_FROM AND VW_USER_MAN_CHARACTER03.FIRST_RECEIPT_DAY <= :FIRST_RECEIPT_DAY_TO)",ref pWhere);
			list.Add(new OracleParameter("FIRST_RECEIPT_DAY_FROM",pFirstReceiptDayFrom));
			list.Add(new OracleParameter("FIRST_RECEIPT_DAY_TO",pFirstReceiptDayTo));
		}

		if (!pLastReceiptDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pLastReceiptDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pLastReceiptDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.LAST_RECEIPT_DATE >= :LAST_RECEIPT_DATE_FROM AND VW_USER_MAN_CHARACTER03.LAST_RECEIPT_DATE <= :LAST_RECEIPT_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_RECEIPT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_RECEIPT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pNonExistMailAddrFlag.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.NON_EXIST_MAIL_ADDR_FLAG = :NON_EXIST_MAIL_ADDR_FLAG",ref pWhere);
			list.Add(new OracleParameter("NON_EXIST_MAIL_ADDR_FLAG",pNonExistMailAddrFlag));
		}

		if (!pLastBlackDayFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.LAST_BLACK_DAY >= :LAST_BLACK_DAY_FROM AND VW_USER_MAN_CHARACTER03.LAST_BLACK_DAY <= :LAST_BLACK_DAY_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_BLACK_DAY_FROM",pLastBlackDayFrom));
			list.Add(new OracleParameter("LAST_BLACK_DAY_TO",pLastBlackDayTo));
		}

		if (!string.IsNullOrEmpty(pUrgeLevel)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.URGE_LEVEL = :URGE_LEVEL",ref pWhere);
			list.Add(new OracleParameter("URGE_LEVEL",pUrgeLevel));
		}

		if (pMchaUsedFlag) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_COUNT > :POINT_AFFILIATE_TOTAL_COUNT",ref pWhere);
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_COUNT","0"));
		}

		if (pCreditUsedFlag) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.EXIST_CREDIT_DEAL_FLAG = :EXIST_CREDIT_DEAL_FLAG",ref pWhere);
			list.Add(new OracleParameter("EXIST_CREDIT_DEAL_FLAG",ViCommConst.FLAG_ON));
		}

		if (!pNaFlag.Equals("")) {
			StringBuilder oClauseBuilder = new StringBuilder();
			oClauseBuilder.Append("VW_USER_MAN_CHARACTER03.NA_FLAG IN (");
			int iNum = 1;
			foreach (string sNaFlag in pNaFlag.Split(',')) {
				string sParamName = string.Format(":NA_FLAG{0}",iNum);

				if (iNum != 1)
					oClauseBuilder.Append(",");
				oClauseBuilder.Append(sParamName);
				list.Add(new OracleParameter(sParamName,sNaFlag));

				iNum += 1;
			}
			oClauseBuilder.Append(" )").AppendLine();
			SysPrograms.SqlAppendWhere(oClauseBuilder.ToString(),ref pWhere);

		} else {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.NA_FLAG = :NA_FLAG",ref pWhere);
			list.Add(new OracleParameter("NA_FLAG",ViCommConst.NA_CHAR_NONE));
		}

		if (!string.IsNullOrEmpty(pRegistServicePointFlag)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.REGIST_SERVICE_POINT_FLAG = :REGIST_SERVICE_POINT_FLAG",ref pWhere);
			list.Add(new OracleParameter("REGIST_SERVICE_POINT_FLAG",pRegistServicePointFlag));
		}
		
		if (pNotExistFirstPointUsedDate) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.FIRST_POINT_USED_DATE IS NULL",ref pWhere);
		}

		// リッチーノランク
		if (!string.IsNullOrEmpty(pRichinoRankFrom) || !string.IsNullOrEmpty(pRichinoRankTo)) {
			SysPrograms.SqlAppendWhere("T_USER_MAN_CHARACTER_EX.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ",ref pWhere);
		}
		if (!string.IsNullOrEmpty(pRichinoRankFrom)) {
			SysPrograms.SqlAppendWhere("NVL(T_USER_MAN_CHARACTER_EX.RICHINO_RANK, 0) >= :RICHINO_RANK_FROM",ref pWhere);
			list.Add(new OracleParameter("RICHINO_RANK_FROM",pRichinoRankFrom));
		}
		if (!string.IsNullOrEmpty(pRichinoRankTo)) {
			SysPrograms.SqlAppendWhere("NVL(T_USER_MAN_CHARACTER_EX.RICHINO_RANK, 0) <= :RICHINO_RANK_TO",ref pWhere);
			list.Add(new OracleParameter("RICHINO_RANK_TO",pRichinoRankTo));
		}
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}
}
