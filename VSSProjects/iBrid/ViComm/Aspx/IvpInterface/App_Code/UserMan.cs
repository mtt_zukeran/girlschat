/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: 男性会員
--	Progaram ID		: UserMan
--
--  Creation Date	: 2011.02.25
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class UserMan:DbSession {

	public UserMan() {
	}

	public bool IsExistUserByIfKey(string pIfKey,out string pSiteCd,out string pUserSeq) {
		bool bExist = false;
		pSiteCd = string.Empty;
		pUserSeq = string.Empty;
		DataSet ds = new DataSet();
		DataRow dr;
		conn = DbConnect();

		string sSql = "SELECT " +
							"SITE_CD	," +
							"USER_SEQ	" +
						" FROM " +
							"VW_USER_MAN_CHARACTER00" +
						" WHERE " +
							"IF_KEY = :IF_KEY";

		using (cmd = new OracleCommand(sSql,conn)) {
			cmd.Parameters.Add("IF_KEY",pIfKey);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_USER_MAN_CHARACTER00");
				if (ds.Tables["VW_USER_MAN_CHARACTER00"].Rows.Count != 0) {
					bExist = true;
					dr = ds.Tables["VW_USER_MAN_CHARACTER00"].Rows[0];

					pSiteCd = dr["SITE_CD"].ToString();
					pUserSeq = dr["USER_SEQ"].ToString();
				}
			}
		}
		conn.Close();

		return bExist;
	}

	public string CheckBalanceAspMoviePoint(string pIfKey,int pUsePoint) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_BALANCE_ASP_MOVIE_POINT");
			db.ProcedureInParm("PIF_KEY",DbSession.DbType.VARCHAR2,pIfKey);
			db.ProcedureInParm("PUSE_POINT",DbSession.DbType.NUMBER,pUsePoint);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("PRESULT");
		}
		return sResult;
	}

	public void CreateWebUsedReport(string pSiteCd,string pUserSeq,string pChargeType,int pChargePoint,string pCastUserSeq,string pCastCharNo,string pChargeSupplement) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CREATE_WEB_USED_REPORT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,pChargeType);
			db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,pChargePoint);
			db.ProcedureInParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("PCHARGE_SUPPLEMENT",DbSession.DbType.VARCHAR2,pChargeSupplement);
			db.ProcedureInParm("PWITHOUT_COMMIT",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PPAYMENT_AMT",DbSession.DbType.NUMBER,-1);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
