﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Interface
--	Title			: ユーザー--	Progaram ID		: User
--  Creation Date	: 2015.01.14
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;

public class User:DbSession {
	public string sexCd;

	public User() {
	}

	public bool IsTalking(string pUserSeq) {
		bool bTalking = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER,");
		oSqlBuilder.AppendLine("	T_MANAGE_COMPANY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		TALKING_FLAG = 1 OR");
		oSqlBuilder.AppendLine("		(TALK_LOCK_FLAG = 1 AND TALK_LOCK_DATE > SYSDATE - (GREATEST(T_MANAGE_COMPANY.CALL_LOCK_TIMEOUT_SEC,T_MANAGE_COMPANY.CALL_TIMEOUT_SEC))/86400)");
		oSqlBuilder.AppendLine("	)");

		oParamList.Add(new OracleParameter(":USER_SEQ",int.Parse(pUserSeq)));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			bTalking = true;
		}

		return bTalking;
	}

	public bool GetOne(string pUserSeq) {
		bool bExist = false;
		int iUserSeq;

		if (string.IsNullOrEmpty(pUserSeq)) {
			throw new ArgumentNullException();
		} else if (!int.TryParse(pUserSeq,out iUserSeq)) {
			throw new ArgumentException("pUserSeq");
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SEX_CD");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ");

		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count != 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			sexCd = iBridUtil.GetStringValue(dr["SEX_CD"]);
			bExist = true;
		}

		return bExist;
	}
}
