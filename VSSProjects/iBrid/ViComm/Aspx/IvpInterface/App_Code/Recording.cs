﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Interface
--	Title			: 会員待機--	Progaram ID		: Recording
--  Creation Date	: 2015.01.14
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;

public class Recording:DbSession {
	public Recording() {
	}

	public bool IsExistWaitingVoice(string pSiteCd,string pUserSeq,string pUserCharNo) {
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_RECORDING");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	FOWARD_TERM_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	WAITING_TYPE IN('2','3')");//2:音声通話のみ希望、3:TV/音声どちらでもOK

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",int.Parse(pUserSeq)));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			bExist = true;
		}

		return bExist;
	}
}
