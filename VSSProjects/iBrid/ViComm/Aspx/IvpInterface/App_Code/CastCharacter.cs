﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者キャラクター
--	Progaram ID		: CastCharacter
--
--  Creation Date	: 2010.06.02
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using iBridCommLib;
using Oracle.DataAccess.Client;
using System.Text;
using ViComm;

/// <summary>
/// 出演者キャラクター
/// </summary>
public class CastCharacter:DbSession {
	public string handleNm;

	public DataSet GetPageCollection(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pTalkType,
		string pTerminalType,
		string pHandleNm,
		string pActCategorySeq,
		string pEmailAddr,
		string pNonExistMailAddrFlag,
		string pMainCharOnlyFlag,
		string pManagerSeq,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		string pUserRank,
		string pAdCd,
		string pNaFlag,
		bool pExcludeEmailAddr,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();

		string sOrder = "ORDER BY SITE_CD,LOGIN_ID,USER_CHAR_NO ";


		string sSql = "SELECT DISTINCT " +
							"SITE_CD			," +
							"USER_SEQ			" +
						"FROM(" +
						" SELECT VW_CAST01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST01  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(
											pOnline,
											pLoginId,
											pProductionSeq,
											pCastNm,
											pSiteCd,
											pTalkType,
											pTerminalType,
											pHandleNm,
											pActCategorySeq,
											pEmailAddr,
											pNonExistMailAddrFlag,
											pMainCharOnlyFlag,
											pManagerSeq,
											pRegistDayFrom,
											pRegistDayTo,
											pLastLoginDayFrom,
											pLastLoginDayTo,
											pTotalPaymentAmtFrom,
											pTotalPaymentAmtTo,
											pUserRank,
											pAdCd,
											pNaFlag,
											pExcludeEmailAddr,
											ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add(objParms[i]);
			}

			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_CAST01");
			}
		}
		conn.Close();
		return ds;
	}

	private OracleParameter[] CreateWhere(
	string pOnline,
	string pLoginId,
	string pProductionSeq,
	string pCastNm,
	string pSiteCd,
	string pConnectType,
	string pTerminalType,
	string pHandleNm,
	string pActCategorySeq,
	string pEmailAddr,
	string pNonExistMailAddrFlag,
	string pMainCharOnlyFlag,
	string pManagerSeq,
	string pRegistDayFrom,
	string pRegistDayTo,
	string pLastLoginDayFrom,
	string pLastLoginDayTo,
	string pTotalPaymentAmtFrom,
	string pTotalPaymentAmtTo,
	string pUserRank,
	string pAdCd,
	string pNaFlag,
	bool pExcludeEmailAddr,
	ref string pWhere
) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (pActCategorySeq.Equals("") == false) {
			SysPrograms.SqlAppendWhere("ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ",ref pWhere);
			list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pActCategorySeq));
		}

		if (pOnline.Equals("") == false) {
			switch (pOnline) {
				case "#":
					SysPrograms.SqlAppendWhere("CHARACTER_ONLINE_STATUS = :CHARACTER_ONLINE_STATUS",ref pWhere);
					list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED));
					break;

				case "*":
					SysPrograms.SqlAppendWhere("(CHARACTER_ONLINE_STATUS IN (:CHARACTER_ONLINE_STATUS1,:CHARACTER_ONLINE_STATUS2))",ref pWhere);
					list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS1",ViCommConst.USER_WAITING));
					list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS2",ViCommConst.USER_TALKING));
					break;

				default:
					SysPrograms.SqlAppendWhere("CHARACTER_ONLINE_STATUS = :CHARACTER_ONLINE_STATUS",ref pWhere);
					list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS",pOnline));
					break;
			}

		}

		if (pLoginId.Equals("") == false) {
			SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId + "%"));
		}

		if (pProductionSeq.Equals("") == false) {
			SysPrograms.SqlAppendWhere("PRODUCTION_SEQ = :PRODUCTION_SEQ",ref pWhere);
			list.Add(new OracleParameter("PRODUCTION_SEQ",pProductionSeq));
		}

		if (pCastNm.Equals("") == false) {
			SysPrograms.SqlAppendWhere("CAST_NM LIKE :CAST_NM",ref pWhere);
			list.Add(new OracleParameter("CAST_NM","%" + pCastNm + "%"));
		}

		if (pHandleNm.Equals("") == false) {
			SysPrograms.SqlAppendWhere("HANDLE_NM LIKE :HANDLE_NM",ref pWhere);
			list.Add(new OracleParameter("HANDLE_NM","%" + pHandleNm + "%"));
		}

		if (pConnectType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("CONNECT_TYPE = :CONNECT_TYPE",ref pWhere);
			list.Add(new OracleParameter("CONNECT_TYPE",pConnectType));
		}

		if (pTerminalType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("USE_TERMINAL_TYPE = :USE_TERMINAL_TYPE",ref pWhere);
			list.Add(new OracleParameter("USE_TERMINAL_TYPE",pTerminalType));
		}

		if (pEmailAddr.Equals("") == false) {
			if (pExcludeEmailAddr == true) {
				SysPrograms.SqlAppendWhere("EMAIL_ADDR NOT LIKE :EMAIL_ADDR",ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("EMAIL_ADDR LIKE :EMAIL_ADDR",ref pWhere);
			}
			list.Add(new OracleParameter("EMAIL_ADDR","%" + pEmailAddr + "%"));
		}

		if (pNonExistMailAddrFlag.Equals("") == false) {
			SysPrograms.SqlAppendWhere("NON_EXIST_MAIL_ADDR_FLAG = :NON_EXIST_MAIL_ADDR_FLAG",ref pWhere);
			list.Add(new OracleParameter("NON_EXIST_MAIL_ADDR_FLAG",pNonExistMailAddrFlag));
		}

		if (pMainCharOnlyFlag.Equals("1")) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		} else if (pMainCharOnlyFlag.Equals("0")) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO > :USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		}
		if (!string.IsNullOrEmpty(pManagerSeq)) {
			SysPrograms.SqlAppendWhere("MANAGER_SEQ = :MANAGER_SEQ",ref pWhere);
			list.Add(new OracleParameter("MANAGER_SEQ",pManagerSeq));
		}

		if (!pRegistDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pRegistDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pRegistDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(REGIST_DATE >= :REGIST_DATE_FROM AND REGIST_DATE < :REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pLastLoginDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pLastLoginDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pLastLoginDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(LAST_ACTION_DATE >= :LAST_ACTION_DATE_FROM AND LAST_ACTION_DATE < :LAST_ACTION_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_ACTION_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_ACTION_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pTotalPaymentAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(TOTAL_PAYMENT_AMT >= :TOTAL_PAYMENT_AMT_FROM AND TOTAL_PAYMENT_AMT <= :TOTAL_PAYMENT_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("TOTAL_PAYMENT_AMT_FROM",int.Parse(pTotalPaymentAmtFrom)));
			list.Add(new OracleParameter("TOTAL_PAYMENT_AMT_TO",int.Parse(pTotalPaymentAmtTo)));
		}
		if (!pUserRank.Equals("")) {
			SysPrograms.SqlAppendWhere("USER_RANK = :USER_RANK",ref pWhere);
			list.Add(new OracleParameter("USER_RANK",pUserRank));
		}
		if (!pAdCd.Equals("")) {
			SysPrograms.SqlAppendWhere("AD_CD = :AD_CD",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pAdCd));
		}

		if (!pNaFlag.Equals("")) {
			StringBuilder oClauseBuilder = new StringBuilder();
			oClauseBuilder.Append("NA_FLAG IN (");
			int iNum = 1;
			foreach (string sNaFlag in pNaFlag.Split(',')) {
				string sParamName = string.Format(":NA_FLAG{0}",iNum);

				if (iNum != 1)
					oClauseBuilder.Append(",");
				oClauseBuilder.Append(sParamName);
				list.Add(new OracleParameter(sParamName,sNaFlag));

				iNum += 1;
			}
			oClauseBuilder.Append(" )").AppendLine();
			SysPrograms.SqlAppendWhere(oClauseBuilder.ToString(),ref pWhere);

		} else {
			SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhere);
			list.Add(new OracleParameter("NA_FLAG",ViCommConst.NA_CHAR_NONE));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo) {
		bool bExist = false;
		int iUserSeq;

		if (string.IsNullOrEmpty(pSiteCd) || string.IsNullOrEmpty(pUserSeq) || string.IsNullOrEmpty(pUserCharNo)) {
			throw new ArgumentNullException();
		} else if (!int.TryParse(pUserSeq,out iUserSeq)) {
			throw new ArgumentException("pUserSeq");
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	HANDLE_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count != 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			handleNm = iBridUtil.GetStringValue(dr["HANDLE_NM"]);
			bExist = true;
		}

		return bExist;
	}
}
