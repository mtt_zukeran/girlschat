﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Interface
--	Title			: ユーザーデータ送信

--	Progaram ID		: TransUserData
--
--  Creation Date	: 2010.03.16
--  Creater			: iBrid(A.Koyanagi)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web;
using iBridCommLib;
using ViComm;

public partial class TransUserData:System.Web.UI.Page {
	string sUserSeq;
	string sHistorySeq;
	string sAffiliaterId;

	protected void Page_Load(object sender,EventArgs e) {

		sAffiliaterId = iBridUtil.GetStringValue(Request.QueryString["afid"]);
		sUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		sHistorySeq = iBridUtil.GetStringValue(Request.QueryString["hisseq"]);

		if (!string.IsNullOrEmpty(sAffiliaterId) && !string.IsNullOrEmpty(sUserSeq) && !string.IsNullOrEmpty(sHistorySeq)) {
			TransUserDataToPartnerSite();
		}
	}

	private void TransUserDataToPartnerSite() {

		//ユーザー情報取得
		DataTable dtUser;
		using (UserManCharacter oUser = new UserManCharacter()) {
			dtUser = oUser.GetOne(sUserSeq).Tables[0];
			if (dtUser.Rows.Count == 0) {
				Response.ContentType = "text/html";
				Response.Write(string.Format("UserData NotFound {0}",sUserSeq));
				Response.End();
			}
		}
		DataRow drUser = dtUser.Rows[0];

		//ユーザーデータ送信
		string[] sArrHistorySeq = sHistorySeq.Split(',');
		for (int i = 0;i < sArrHistorySeq.Length;i++) {

			//パートナーサイト送信先URL取得

			DataTable dtPartnerSite;
			using (PartnerSiteHis oPartnerSite = new PartnerSiteHis()) {
				dtPartnerSite = oPartnerSite.GetOne(sArrHistorySeq[i]).Tables[0];

				if (dtPartnerSite.Rows.Count != 0) {
					DataRow dr = dtPartnerSite.Rows[0];
					string sURL = string.Format(
											dr["REGIST_USER_ACCEPT_URL"].ToString(),
											sArrHistorySeq[i],
											sAffiliaterId,
											drUser["MOBILE_CARRIER_CD"].ToString(),
											drUser["TERMINAL_UNIQUE_ID"].ToString(),
											drUser["IMODE_ID"].ToString(),
											drUser["TEL"].ToString(),
											drUser["LOGIN_PASSWORD"].ToString(),
											drUser["HANDLE_NM"].ToString(),
											drUser["BIRTHDAY"].ToString(),
											drUser["MOBILE_TERMINAL_NM"].ToString(),
											drUser["LOGIN_ID"].ToString(),
											sUserSeq,
											drUser["EMAIL_ADDR"],
											dr["ACCOUNT_LOCK_DAYS"].ToString()
					);

					//非同期で送信
					ViCommInterface.TransToParent(sURL,true);
				}
			}
		}

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}","0"));
		Response.End();
	}
}
