﻿using System;
using System.Collections;
using System.Data;
using iBridCommLib;
using ViComm;

public partial class ScheduleMailSend:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		MailSend();
	}

	private void MailSend() {
		string sRet;

		string sexecdatetime = iBridUtil.GetStringValue(Request.QueryString["execdatetime"]) != null ?
								iBridUtil.GetStringValue(Request.QueryString["execdatetime"]) : DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

		ArrayList alSeq = new ArrayList();
		ArrayList alEndSeq = new ArrayList();
		ArrayList alName;
		ArrayList alNameWoman;
		ArrayList alValue = new ArrayList();
		bool isMan = false;

		SetPatternName(out alName);
		SetPatternNameWoman(out alNameWoman);

		using (SchedulePattern oSchedule = new SchedulePattern()) {
			DataSet dsMain = oSchedule.GetPatternList(sexecdatetime);
			int RecCount = 0;

			foreach (DataRow dr in dsMain.Tables[0].Rows) {
				alEndSeq.Add(iBridUtil.GetStringValue(dr["PATTERN_SEQ"]));
			}
			sRet = ScheduleDataUpdate((string[])alEndSeq.ToArray(typeof(string)));

			foreach (DataRow dr in dsMain.Tables[0].Rows) {
				RecCount++;
				int iPos;

				if (alSeq.Count == 0) {
					alSeq.Add(iBridUtil.GetStringValue(dr["PATTERN_SEQ"]));
					isMan = iBridUtil.GetStringValue(dr["SEX_CD"]) == ViCommConst.MAN;
					InitList(out alValue,isMan ? alName.Count : alNameWoman.Count);
				} else if (alSeq[alSeq.Count - 1].ToString() != iBridUtil.GetStringValue(dr["PATTERN_SEQ"])) {

					//検索 
					string[] sUserSeq = GetUserSeq((string[])alValue.ToArray(typeof(string)),isMan);

					//MAIL 
					MailSend(alValue,sUserSeq,isMan);

					alSeq.Add(iBridUtil.GetStringValue(dr["PATTERN_SEQ"]));
					isMan = iBridUtil.GetStringValue(dr["SEX_CD"]) == ViCommConst.MAN;
					alValue.Clear();
					InitList(out alValue,isMan ? alName.Count : alNameWoman.Count);
				}

				//検索条件取得 
				if (isMan) {
					iPos = alName.IndexOf(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_NAME"]));
				} else {
					iPos = alNameWoman.IndexOf(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_NAME"]));
				}
				while (iPos >= 0) {
					alValue.RemoveAt(iPos);
					alValue.Insert(iPos,iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]));
					if (iPos < alName.Count) {
						if (isMan) {
							iPos = alName.IndexOf(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_NAME"]),iPos + 1);
						} else {
							iPos = alNameWoman.IndexOf(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_NAME"]),iPos + 1);
						}
					} else {
						iPos = -1;
					}
				}

				if (dsMain.Tables[0].Rows.Count == RecCount) {
					//検索 
					string[] sUserSeq = GetUserSeq((string[])alValue.ToArray(typeof(string)),isMan);

					//MAIL 
					MailSend(alValue,sUserSeq,isMan);
				}

			}
		}

		if (sRet.Equals("") || sRet.Equals("I000")) {
			sRet = "0";
		} else {
			sRet = "1";
		}

		Response.ContentType = "text/html";
		Response.Write(string.Format("result={0}",sRet));
		Response.End();

	}

	private string ScheduleDataUpdate(string[] sSeqList) {
		string sRet = "";
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SCHEDULE_PATTERN_UPDATE");
			db.ProcedureInArrayParm("PPATTERN_SEQ",DbSession.DbType.VARCHAR2,sSeqList.Length,sSeqList);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sRet = db.GetStringValue("PSTATUS");
		}
		return sRet;
	}

	private void SetPatternName(out ArrayList lsName) {
		lsName = new ArrayList();
		lsName.Add("SITE_CD");							//0
		lsName.Add("USER_RANK");						//1
		lsName.Add("MAN_USER_STATUS");					//2
		lsName.Add("CHARACTER_ONLINE_STATUS");			//3
		lsName.Add("MOBILE_CARRIER_CD");				//4
		lsName.Add("TEL");								//5
		lsName.Add("LOGIN_ID");							//6
		lsName.Add("BAL_POINT_FROM");					//7
		lsName.Add("BAL_POINT_TO");						//8
		lsName.Add("EMAIL_ADDR");						//9
		lsName.Add("HANDLE_NM");						//10
		lsName.Add("AD_CD");							//11
		lsName.Add("REMARKS1");							//12
		lsName.Add("TOTAL_RECEIPT_AMT_FROM");			//13
		lsName.Add("TOTAL_RECEIPT_AMT_TO");				//14
		lsName.Add("REGIST_DATE");						//15
		lsName.Add("REGIST_DATE");						//16
		lsName.Add("FIRST_LOGIN_DATE");					//17
		lsName.Add("FIRST_LOGIN_DATE");					//18
		lsName.Add("LAST_LOGIN_DATE");					//19
		lsName.Add("LAST_LOGIN_DATE");					//20
		lsName.Add("LAST_POINT_USED_DATE");				//21
		lsName.Add("LAST_POINT_USED_DATE");				//22
		lsName.Add("FIRST_RECEIPT_DAY");				//23
		lsName.Add("FIRST_RECEIPT_DAY");				//24
		lsName.Add("LAST_RECEIPT_DATE");				//25
		lsName.Add("LAST_RECEIPT_DATE");				//26
		lsName.Add("TOTAL_RECEIPT_COUNT_FROM");			//27
		lsName.Add("TOTAL_RECEIPT_COUNT_TO");			//28
		lsName.Add("NON_EXIST_MAIL_ADDR_FLAG");			//29
		lsName.Add("");//FIRST_LOGIN_TIME_FROM			//30
		lsName.Add("");//FIRST_LOGIN_TIME_TO			//31
		lsName.Add("");//LAST_LOGIN_TIME_FROM			//32
		lsName.Add("");//LAST_LOGIN_TIME_TO				//33
		lsName.Add("");//LAST_POINT_USED_TIME_FROM		//34
		lsName.Add("");//LAST_POINT_USED_TIME_TO		//35
		lsName.Add("");//REGIST_TIME_FROM				//36
		lsName.Add("");//REGIST_TIME_TO					//37
		lsName.Add("LAST_BLACK_DAY");					//38
		lsName.Add("LAST_BLACK_DAY");					//39
		lsName.Add("BILL_AMT_FROM");					//40
		lsName.Add("BILL_AMT_TO");						//41
		lsName.Add("POINT_AFFILIATE_TOTAL_AMT_FROM");	//42
		lsName.Add("POINT_AFFILIATE_TOTAL_AMT_TO");		//43
		lsName.Add("POINT_AFFILIATE_TOTAL_COUNT_FROM");	//44
		lsName.Add("POINT_AFFILIATE_TOTAL_COUNT_TO");	//45
		lsName.Add("URGE_LEVEL");						//46
		lsName.Add("CREDIT_MEASURED_RATE_TARGET");		//47
		lsName.Add("POINT_AFFILIATE_TOTAL_COUNT");		//48
		lsName.Add("EXIST_CREDIT_DEAL_FLAG");			//49
		lsName.Add("MAIL_TEMPLATE_NO");					//50
		lsName.Add("SERVICE_POINT");					//51
		lsName.Add("SERVICE_POINT_EFFECTIVE_DATE");		//52
		lsName.Add("POINT_TRANSFER_AVA_HOUR");			//53
		lsName.Add("CAST_LOGIN_ID");					//54
		lsName.Add("USER_CHAR_NO");						//55
		lsName.Add("NA_FLAG");							//56
		lsName.Add("REGIST_SERVICE_POINT_FLAG");		//57
		lsName.Add("NOT_EXIST_FIRST_POINT_USED_DATE");	//58
		lsName.Add("EXCLUDE_EMAIL_ADDR_FLAG");			//59
		lsName.Add("MAIL_SERVER");						//60
		lsName.Add("RICHINO_RANK_FROM");				//61
		lsName.Add("RICHINO_RANK_TO");					//62
	}

	private void SetPatternNameWoman(out ArrayList lsName) {
		lsName = new ArrayList();
		lsName.Add("CHARACTER_ONLINE_STATUS");		// 0
		lsName.Add("LOGIN_ID");						// 1
		lsName.Add("PRODUCTION_SEQ");				// 2
		lsName.Add("CAST_NM");						// 3
		lsName.Add("SITE_CD");						// 4
		lsName.Add("CONNECT_TYPE");					// 5
		lsName.Add("USE_TERMINAL_TYPE");			// 6
		lsName.Add("HANDLE_NM");					// 7
		lsName.Add("ACT_CATEGORY_SEQ");				// 8
		lsName.Add("EMAIL_ADDR");					// 9
		lsName.Add("NON_EXIST_MAIL_ADDR_FLAG");		// 10
		lsName.Add("MANAGER_SEQ");					// 11
		lsName.Add("REGIST_DATE");					// 12
		lsName.Add("REGIST_DATE");					// 13
		lsName.Add("LAST_LOGIN_DATE");				// 14
		lsName.Add("LAST_LOGIN_DATE");				// 15
		lsName.Add("TOTAL_PAYMENT_AMT_FROM");		// 16
		lsName.Add("TOTAL_PAYMENT_AMT_TO");			// 17
		lsName.Add("USER_RANK");					// 18
		lsName.Add("AD_CD");						// 19
		lsName.Add("NA_FLAG");						// 20
		lsName.Add("MAIL_TEMPLATE_NO");				// 21
		lsName.Add("EXCLUDE_EMAIL_ADDR_FLAG");		// 22
		lsName.Add("MAIL_SERVER");					// 23
	}

	private void InitList(out ArrayList aList,int pListCount) {
		aList = new ArrayList();
		for (int iCnt = 0;iCnt < pListCount;iCnt++) {
			aList.Add("");
		}
	}

	private string[] GetUserSeq(string[] pPrmList,bool isMan) {
		string[] sUserSeq = null;

		if (isMan) {
			using (UserManCharacter oCharacter = new UserManCharacter())
			using (DataSet ds = oCharacter.GetPageCollection(
				pPrmList[0],
				SysPrograms.IsNumber(pPrmList[1]) ? int.Parse(pPrmList[1]) : 0,
				SysPrograms.IsNumber(pPrmList[2]) ? int.Parse(pPrmList[2]) : 0,
				SysPrograms.IsNumber(pPrmList[3]) ? int.Parse(pPrmList[3]) : 0,
				SysPrograms.IsNumber(pPrmList[4]) ? int.Parse(pPrmList[4]) : 0,
				pPrmList[5],
				pPrmList[6],
				pPrmList[7],
				pPrmList[8],
				pPrmList[9],
				pPrmList[10],
				pPrmList[11],
				pPrmList[12],
				pPrmList[13],
				pPrmList[14],
				SysPrograms.IsNumber(pPrmList[15]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[15]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[16]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[16]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[17]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[17]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[18]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[18]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[19]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[19]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[20]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[20]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[21]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[21]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[22]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[22]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[23]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[23]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[24]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[24]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[25]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[25]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[26]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[26]) + 1).ToString("yyyy/MM/dd") : "",
				pPrmList[27],
				pPrmList[28],
				pPrmList[29],
				pPrmList[30],
				pPrmList[31],
				pPrmList[32],
				pPrmList[33],
				pPrmList[34],
				pPrmList[35],
				pPrmList[36],
				pPrmList[37],
				SysPrograms.IsNumber(pPrmList[38]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[38]) + 1).ToString("yyyy/MM/dd") : "",
				SysPrograms.IsNumber(pPrmList[39]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[39]) + 1).ToString("yyyy/MM/dd") : "",
				pPrmList[40],
				pPrmList[41],
				pPrmList[42],
				pPrmList[43],
				pPrmList[44],
				pPrmList[45],
				pPrmList[46],
				pPrmList[47].Equals("1") ? true : false,
				pPrmList[48].Equals("1") ? true : false,
				pPrmList[49].Equals("1") ? true : false,
				pPrmList[56],
				pPrmList[57],
				pPrmList[58].Equals("1") ? true : false,
				pPrmList[59].Equals("1") ? true : false,
				pPrmList[61],
				pPrmList[62],
				0,
				SysConst.DB_MAX_ROWS)) {

				DataRow dr = null;
				sUserSeq = new string[ds.Tables[0].Rows.Count];
				for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
					dr = ds.Tables[0].Rows[i];
					sUserSeq[i] = dr["USER_SEQ"].ToString();
				}
			}
		} else {
			// 女性側の検索実行 
			using (CastCharacter oCharacter = new CastCharacter()) {
				DataSet ds = oCharacter.GetPageCollection(
					pPrmList[0],
					pPrmList[1],
					pPrmList[2],
					pPrmList[3],
					pPrmList[4],
					pPrmList[5],
					pPrmList[6],
					pPrmList[7],
					pPrmList[8],
					pPrmList[9],
					pPrmList[10],
					"1",
					pPrmList[11],
					SysPrograms.IsNumber(pPrmList[12]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[12]) + 1).ToString("yyyy/MM/dd") : "",
					SysPrograms.IsNumber(pPrmList[13]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[13]) + 1).ToString("yyyy/MM/dd") : "",
					SysPrograms.IsNumber(pPrmList[14]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[14]) + 1).ToString("yyyy/MM/dd") : "",
					SysPrograms.IsNumber(pPrmList[15]) ? DateTime.Now.AddDays(~int.Parse(pPrmList[15]) + 1).ToString("yyyy/MM/dd") : "",
					pPrmList[16],
					pPrmList[17],
					pPrmList[18],
					pPrmList[19],
					pPrmList[20],
					pPrmList[22].Equals("1") ? true : false,
					0,
					SysConst.DB_MAX_ROWS
				);

				DataRow dr = null;
				sUserSeq = new string[ds.Tables[0].Rows.Count];
				for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
					dr = ds.Tables[0].Rows[i];
					sUserSeq[i] = dr["USER_SEQ"].ToString();
				}
			}
		}
		return sUserSeq;
	}

	private string MailSend(ArrayList alValue,string[] sUserSeq,bool isMan) {

		string[] sDoc;
		int iDocCount;
		string sRet = "";

		if (sUserSeq.Length > 0) {
			SysPrograms.SeparateHtml("",ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

			if (isMan) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("TX_ADMIN_TO_MAN_MAIL");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,alValue[0]);
					db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,alValue[50]);
					db.ProcedureInParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,alValue[54]);
					db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,alValue[55]);
					db.ProcedureInArrayParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq.Length,sUserSeq);
					db.ProcedureInParm("PMAN_USER_COUNT",DbSession.DbType.NUMBER,sUserSeq.Length);
					db.ProcedureInParm("PMAIL_AVA_HOUR",DbSession.DbType.NUMBER,
									   SysPrograms.IsNumber(alValue[52].ToString()) ? int.Parse(alValue[52].ToString()) : 0);
					db.ProcedureInParm("PPOINT_TRANSFER_AVA_HOUR",DbSession.DbType.NUMBER,
									   SysPrograms.IsNumber(alValue[53].ToString()) ? int.Parse(alValue[53].ToString()) : 0);
					db.ProcedureInParm("PSERVICE_POINT",DbSession.DbType.NUMBER,
									   SysPrograms.IsNumber(alValue[51].ToString()) ? int.Parse(alValue[51].ToString()) : 0);
					db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,"");
					db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
					db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ProcedureInParm("PTEST_SEND_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
					db.ProcedureInParm("pMAIL_SEND_TYPE",DbSession.DbType.VARCHAR2,"");
					db.ProcedureInParm("pPOINT_MAIL_OUTCOME_SEQ",DbSession.DbType.NUMBER,0);
					db.ProcedureInParm("PMAIL_SERVER",DbSession.DbType.VARCHAR2,
									   !string.IsNullOrEmpty(iBridUtil.GetStringValue(alValue[60])) ? iBridUtil.GetStringValue(alValue[60]) : ViCommConst.MailServer.BEAM);
					db.ExecuteProcedure();
					sRet = db.GetStringValue("PSTATUS");
				}
			} else {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("TX_ADMIN_TO_CAST_MAIL");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,alValue[4]);
					db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,alValue[21]);
					db.ProcedureInArrayParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq.Length,sUserSeq);
					db.ProcedureInParm("PCAST_USER_COUNT",DbSession.DbType.NUMBER,sUserSeq.Length);
					db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,"");
					db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
					db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ProcedureInParm("PTEST_SEND_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
					db.ProcedureInParm("pMAIL_SEND_TYPE",DbSession.DbType.VARCHAR2,"");
					db.ProcedureInParm("PMAIL_SERVER",DbSession.DbType.VARCHAR2,
									   !string.IsNullOrEmpty(iBridUtil.GetStringValue(alValue[23])) ? iBridUtil.GetStringValue(alValue[23]) : ViCommConst.MailServer.BEAM);
					db.ExecuteProcedure();
				}
			}
		}

		return sRet;
	}
}
