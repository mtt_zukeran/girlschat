﻿using System;
using System.Web;
using System.Threading;
using System.Configuration;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class TwilioIncoming:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sAccountSid = iBridUtil.GetStringValue(Request.Form["AccountSid"]);
		string sCallSid = iBridUtil.GetStringValue(Request.Form["CallSid"]);
		string sFrom = iBridUtil.GetStringValue(Request.Form["From"]);
		string sTo = iBridUtil.GetStringValue(Request.Form["To"]);
		string sUseFreeDialFlag = ViCommConst.FLAG_OFF_STR;
		string sIvpRequestSeq = string.Empty;
		string sDialSid = string.Empty;
		string sRequesterSexCd = string.Empty;
		int iAvailableSec = 0;
		string sTwilioTelNo = string.Empty;
		string sSubHostNm = string.Empty;
		string sResult = string.Empty;
		bool bFromAppFlag = false;
		bool bToAppFlag = false;
		string sFromUserSeq = string.Empty;

		if (string.IsNullOrEmpty(sAccountSid) || string.IsNullOrEmpty(sCallSid) || string.IsNullOrEmpty(sFrom)) {
			ViCommInterface.WriteIFLog("TwilioIncoming-Bad Request","Request:" + Request.Form.ToString());
			ResponseReject();
			return;
		}

		using (Sys oSys = new Sys()) {
			if (oSys.GetOne()) {
				sTwilioTelNo = oSys.twilioTelNo;

				if (!sAccountSid.Equals(oSys.twilioAccountSid)) {
					ViCommInterface.WriteIFLog("TwilioIncoming-Incorrect AccountSid","AccountSid:" + sAccountSid);
					ResponseReject();
					return;
				}
			}
		}

		using (Site oSite = new Site()) {
			oSite.GetValue(PwViCommConst.MAIN_SITE_CD,"SUB_HOST_NM",out sSubHostNm);
		}

		if (string.IsNullOrEmpty(sTwilioTelNo) || string.IsNullOrEmpty(sSubHostNm)) {
			ViCommInterface.WriteIFLog("TwilioIncoming-No Setting",string.Format("sTwilioTelNo:{0},sSubHostNm:{1}",sTwilioTelNo,sSubHostNm));
			ResponseError("エラーが発生したため接続できませんでした");
			return;
		}

		sTwilioTelNo = "+81" + sTwilioTelNo.TrimStart('0');

		if (sFrom.StartsWith("client:")) {
			bFromAppFlag = true;
			sFrom = sFrom.Replace("client:","");
		} else {
			sFrom = sFrom.Replace("+81","0");
			sTo = sTo.Replace("+81","0");

			if (sTo.StartsWith("0120")) {
				sUseFreeDialFlag = ViCommConst.FLAG_ON_STR;
			}
		}

		using (IVPRequest oIVPRequest = new IVPRequest()) {
			if (oIVPRequest.GetOneIncoming(sFrom,sUseFreeDialFlag,bFromAppFlag)) {
				sIvpRequestSeq = oIVPRequest.ivpRequestSeq.ToString();
				sRequesterSexCd = oIVPRequest.requesterSexCd;

				if (oIVPRequest.requesterSexCd.Equals(ViCommConst.MAN)) {//会員発信
					if (oIVPRequest.useCastVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
						bToAppFlag = true;
						sDialSid = oIVPRequest.castUserSeq;
						sFromUserSeq = oIVPRequest.manUserSeq;
					} else {
						sDialSid = "+81" + oIVPRequest.outgoingTerminalId.TrimStart('0');
					}

					if (string.IsNullOrEmpty(oIVPRequest.meetingKey)) {//通常待機
						using (LoginUser oLoginUser = new LoginUser()) {
							if (!oLoginUser.IsExistWaiting(oIVPRequest.castUserSeq)) {
								ViCommInterface.WriteIFLog("TwilioIncoming-Not Exist T_LOGIN_USER",string.Format("CAST_USER_SEQ:{0}",oIVPRequest.castUserSeq));
								ResponseError("お相手の女性は既に待機終了しています");
								return;
							}
						}
					} else {//Love待機
						using (Meeting oMeeting = new Meeting()) {
							if (!oMeeting.IsExistWaitingVoice(oIVPRequest.meetingKey,oIVPRequest.castUserSeq,oIVPRequest.manUserSeq)) {
								ViCommInterface.WriteIFLog("TwilioIncoming-Not Exist T_MEETING",string.Format("MEETING_KEY:{0}",oIVPRequest.meetingKey));
								ResponseError("お相手の女性は既に待機終了しています");
								return;
							}
						}
					}

					using (User oUser = new User()) {
						if (oUser.IsTalking(oIVPRequest.castUserSeq)) {
							ViCommInterface.WriteIFLog("TwilioIncoming-Cast Talking",string.Format("CAST_USER_SEQ:{0}",oIVPRequest.castUserSeq));
							ResponseError("お相手の女性はほかのかたと通話中です");
							return;
						}
					}
				} else if (oIVPRequest.requesterSexCd.Equals(ViCommConst.OPERATOR)) {//出演者発信
					if (oIVPRequest.useManVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
						bToAppFlag = true;
						sDialSid = oIVPRequest.manUserSeq;
						sFromUserSeq = oIVPRequest.castUserSeq;
					} else {
						sDialSid = "+81" + oIVPRequest.outgoingTerminalId.TrimStart('0');
					}

					using (Recording oRecording = new Recording()) {
						if (!oRecording.IsExistWaitingVoice(oIVPRequest.manUserSiteCd,oIVPRequest.manUserSeq,oIVPRequest.manCharNo)) {
							ViCommInterface.WriteIFLog("TwilioIncoming-Not Exist User Waiting",string.Format("MAN_USER_SEQ:{0}",oIVPRequest.manUserSeq));
							ResponseError("お相手の男性は既に待機終了しています");
							return;
						}
					}

					using (User oUser = new User()) {
						if (oUser.IsTalking(oIVPRequest.manUserSeq)) {
							ViCommInterface.WriteIFLog("TwilioIncoming-User Talking",string.Format("MAN_USER_SEQ:{0}",oIVPRequest.manUserSeq));
							ResponseError("お相手の男性はほかのかたと通話中です");
							return;
						}
					}
				} else {
					ViCommInterface.WriteIFLog("TwilioIncoming-Incorrect REQUESTER_SEX_CD",string.Format("REQUESTER_SEX_CD:{0}",oIVPRequest.requesterSexCd));
					ResponseError("エラーが発生したため接続できませんでした");
					return;
				}

			} else {
				ViCommInterface.WriteIFLog("TwilioIncoming-Not Exist T_IVP_REQUEST",string.Format("sFrom:{0},sUseFreeDialFlag:{1}",sFrom,sUseFreeDialFlag));
				ResponseError("エラーが発生したため接続できませんでした");
				return;
			}
		}

		if (sRequesterSexCd.Equals(ViCommConst.MAN) && !bFromAppFlag) {
			sResult = UpdateIncommingCallStatus(sIvpRequestSeq,0,sFrom);//着信状態設定（接続）電話番号重複チェックあり
		} else {
			sResult = UpdateIncommingCallStatus(sIvpRequestSeq,0,"");//着信状態設定（接続）電話番号重複チェックなし
		}

		if (!sResult.Equals("0")) {
			ViCommInterface.WriteIFLog("TwilioIncoming-Error UPDATE_INCOMMING_CALL_STATUS",string.Format("sIvpRequestSeq:{0},sFrom:{1}",sIvpRequestSeq,sFrom));
			DisconnectCall(sIvpRequestSeq);
			ResponseError("こちらの電話番号はほかのかたが利用しているためご利用できません");
			return;
		}

		iAvailableSec = StartIvpService(sIvpRequestSeq);//ユーザー状態の更新・通話可能秒数の取得

		if (iAvailableSec == 0) {
			ViCommInterface.WriteIFLog("TwilioIncoming-Error START_IVP_SERVICE",string.Format("sIvpRequestSeq:{0},iAvailableSec:{1}",sIvpRequestSeq,iAvailableSec));
			DisconnectCall(sIvpRequestSeq);
			ResponseError("エラーが発生したため接続できませんでした");
			return;
		}

		sResult = UpdateTwilioIncoming(int.Parse(sIvpRequestSeq),sCallSid,iAvailableSec);

		if (!sResult.Equals("0")) {
			ViCommInterface.WriteIFLog("TwilioIncoming-Error UPDATE_TWILIO_INCOMING",string.Format("sIvpRequestSeq:{0},sCallSid:{1},iAvailableSec:{2}",sIvpRequestSeq,sCallSid,iAvailableSec));
			DisconnectCall(sIvpRequestSeq);
			ResponseError("エラーが発生したため接続できませんでした");
			return;
		}

		sResult = UpdateTalkStatus(sIvpRequestSeq,"1","","");//会話状態設定（呼出中）

		if (!sResult.Equals("0")) {
			ViCommInterface.WriteIFLog("TwilioIncoming-Error UPDATE_TALK_STATUS",string.Format("sIvpRequestSeq:{0},sResult:{1}",sIvpRequestSeq,sResult));
			DisconnectCall(sIvpRequestSeq);
			ResponseError("エラーが発生したため接続できませんでした");
			return;
		}

		if (bToAppFlag) {
			ResponseDialClient(sDialSid,sSubHostNm,iAvailableSec,sFromUserSeq);
			return;
		} else {
			ResponseDialNumber(sDialSid,sSubHostNm,iAvailableSec,sTwilioTelNo);
			return;
		}
	}

	private void ResponseDialClient(string pClientSid,string pSubHostNm,int pAvailableSec,string pClient) {
		Response.ContentType = "text/xml";
		Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Response.Write("<Response>");
		Response.Write(string.Format("<Dial callerId=\"client:{0}\" timeLimit=\"{1}\" action=\"http://{2}:8080/ivp-if/TwilioTerminating.aspx\">",pClient,pAvailableSec,pSubHostNm));
		Response.Write(string.Format("<Client url=\"http://{0}:8080/ivp-if/TwilioIncall.aspx\">",pSubHostNm));
		Response.Write(pClientSid);
		Response.Write("</Client>");
		Response.Write("</Dial>");
		Response.Write("</Response>");
		Response.End();
	}

	private void ResponseDialNumber(string pDialNo,string pSubHostNm,int pAvailableSec,string pTwilioTelNo) {
		Response.ContentType = "text/xml";
		Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Response.Write("<Response>");
		Response.Write(string.Format("<Dial callerId=\"{0}\" timeLimit=\"{1}\" action=\"http://{2}:8080/ivp-if/TwilioTerminating.aspx\">",pTwilioTelNo,pAvailableSec,pSubHostNm));
		Response.Write(string.Format("<Number url=\"http://{0}:8080/ivp-if/TwilioIncall.aspx\">",pSubHostNm));
		Response.Write(pDialNo);
		Response.Write("</Number>");
		Response.Write("</Dial>");
		Response.Write("</Response>");
		Response.End();
	}

	private void ResponseError(string sErrorMsg) {
		Response.ContentType = "text/xml";
		Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Response.Write("<Response>");
		Response.Write(string.Format("<Say voice=\"woman\" language=\"ja-jp\">{0}</Say>",sErrorMsg));
		Response.Write("</Response>");
		Response.End();
	}

	private void ResponseReject() {
		Response.ContentType = "text/xml";
		Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Response.Write("<Response>");
		Response.Write("<Reject />");
		Response.Write("</Response>");
		Response.End();
	}

	private void DisconnectCall(string pIvpRequestSeq) {
		UpdateIncommingCallStatus(pIvpRequestSeq,1,"");//着信状態設定（切断）
		UpdateTalkStatus(pIvpRequestSeq,"3",PwViCommConst.TalkEndReason.FAILED,PwViCommConst.DisconnectReason.NO_CALL);//会話状態設定（切断）
	}

	private string UpdateIncommingCallStatus(string pIvpRequestSeq,int pDisconnectFlag,string pCID) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_INCOMMING_CALL_STATUS");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("pDISCONNECT_FLAG",DbSession.DbType.NUMBER,pDisconnectFlag);
			db.ProcedureInParm("pCID",DbSession.DbType.VARCHAR2,pCID);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	private int StartIvpService(string pIvpRequestSeq) {
		int iAvailableSec = 0;//通話可能秒数

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("START_IVP_SERVICE");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("pADD_IVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("pCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,"-1");
			db.ProcedureOutParm("pAVAILABLE_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iAvailableSec = db.GetIntValue("pAVAILABLE_SEC");
		}

		return iAvailableSec;
	}

	public string UpdateTwilioIncoming(int pIvpRequestSeq,string pTwilioCallSid,int pAvailableSec) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TWILIO_INCOMING");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.NUMBER,pIvpRequestSeq);
			db.ProcedureInParm("pTWILIO_CALL_SID",DbSession.DbType.VARCHAR2,pTwilioCallSid);
			db.ProcedureInParm("pAVAILABLE_SEC",DbSession.DbType.NUMBER,pAvailableSec);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	private string UpdateTalkStatus(string pIvpRequestSeq,string pCallStatus,string pTalkEndReason,string pDisconnectReason) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TALK_STATUS");
			db.ProcedureInParm("pIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pIvpRequestSeq);
			db.ProcedureInParm("pCALL_STATUS_SEQ",DbSession.DbType.VARCHAR2,"-1");
			db.ProcedureInParm("pCALL_STATUS",DbSession.DbType.VARCHAR2,pCallStatus);
			db.ProcedureInParm("pTALK_END_REASON",DbSession.DbType.VARCHAR2,pTalkEndReason);
			db.ProcedureInParm("pDISCONNECT_REASON",DbSession.DbType.VARCHAR2,pDisconnectReason);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("pCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCAST_USE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}
}
