﻿using System;
using System.Threading;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class RegistCast:System.Web.UI.Page {

	private const int MAX_ATTR = 16;

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sResult = "1";
			string sLoginId = string.Empty;
			string sFriendIntroCd = iBridUtil.GetStringValue(Request.Form["introcd"]);
			bool bIsExistFriendIntroCd = false;

			string[] sAttrValue = new string[MAX_ATTR];

			for (int i = 0;i < MAX_ATTR;i++) {
				sAttrValue[i] = iBridUtil.GetStringValue(Request.Form[string.Format("item{0:D2}",i + 1)]);
			}

			string sOKList = iBridUtil.GetStringValue(Request.Form["oklist"]);
			string[] sPlayValue = sOKList.Split(',');
			string sSiteCd = iBridUtil.GetStringValue(Request.Form["sitecd"]);
			if (sSiteCd.Equals(string.Empty)) {
				sSiteCd = "A001";
			}

			if (!sFriendIntroCd.Equals(string.Empty)) {
				using (CastCharacter oCastCharacter = new CastCharacter()) {
					bIsExistFriendIntroCd = oCastCharacter.IsExistFriendIntroCd(sFriendIntroCd);
				}
				if (!bIsExistFriendIntroCd) {
					sFriendIntroCd = string.Empty;
				}
			}

			string sManagerSeq = iBridUtil.GetStringValue(Request.Form["manager"]);
			if (sManagerSeq.Equals(string.Empty)) {
				sManagerSeq = "1";
			}
			
			string sMailAddr = iBridUtil.GetStringValue(Request.Form["mailaddr"]).ToLower();
			string sTel = iBridUtil.GetStringValue(Request.Form["tel"]);

			if (string.IsNullOrEmpty(sMailAddr.Trim())) {
				throw new ApplicationException("mailaddr is empty");
			}
			if (string.IsNullOrEmpty(sTel.Trim())) {
				throw new ApplicationException("tel is empty");
			}
			
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("CAST_WEB_REGIST");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
				db.ProcedureInParm("PCAST_NM",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(Request.Form["name"]));
				db.ProcedureInParm("PCAST_KANA_NM",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureInParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.WAIT_TYPE_BOTH);
				db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(Request.Form["birthday"]));
				db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,sTel);
				db.ProcedureInParm("PEMAIL_ADDR", DbSession.DbType.VARCHAR2, sMailAddr);
				db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(Request.Form["password"]));
				db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(Request.Form["handle"]));
				db.ProcedureInParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(Request.Form["commentlist"]));
				db.ProcedureInParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(Request.Form["commentdetail"]));
				db.ProcedureInParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2,sManagerSeq);
				db.ProcedureInArrayParm("PCAST_ATTR_VALUE",DbSession.DbType.VARCHAR2,MAX_ATTR,sAttrValue);
				db.ProcedureInParm("PCAST_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,MAX_ATTR);
				db.ProcedureInArrayParm("POK_PLAY_VALUE",DbSession.DbType.VARCHAR2,sPlayValue.Length,sPlayValue);
				db.ProcedureInParm("POK_PLAY_RECORD_COUNT",DbSession.DbType.NUMBER,sPlayValue.Length);
				db.ProcedureInParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,sFriendIntroCd);
				db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
				sLoginId = db.GetStringValue("PLOGIN_ID");
			}

			string sWebPhisicalDir = string.Empty;
			using (Site oSite = new Site()) {
				oSite.GetValue(sSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			string sDir = string.Empty;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				sDir = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + sSiteCd + string.Format("\\Operator\\{0}",sLoginId);
				Directory.CreateDirectory(sDir);

				sDir = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + sSiteCd + string.Format("\\Operator\\{0}",sLoginId);
				Directory.CreateDirectory(sDir);
			}
			Response.ContentType = "text/plain";
			Response.Write(string.Format("result={0}&loginid={1}",sResult,sLoginId));

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Write(string.Format("result={0}&loginid={1}","-1",string.Empty));
		}
	}
}
