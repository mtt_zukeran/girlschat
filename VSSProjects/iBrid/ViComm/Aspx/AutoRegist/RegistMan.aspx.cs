﻿using System;
using System.Threading;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class RegistMan : System.Web.UI.Page {
    private const string RESULT_OK = "0";
    private const string RESULT_DUP = "1";
    private const string RESULT_PARM_ERR = "2";

    protected void Page_Load(object sender,EventArgs e) {
        try {
            string sResult = RESULT_OK;
            string sSite = iBridUtil.GetStringValue(Request.QueryString["site"]);
            string sCarrier = iBridUtil.GetStringValue(Request.QueryString["carrier"]);
            string sAgent = iBridUtil.GetStringValue(Request.QueryString["agent"]);
            string sSubscriber = iBridUtil.GetStringValue(Request.QueryString["subscriber"]);
            string sAdCd = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
            string sTempRegistId = string.Empty;
            string sLoginId = string.Empty;
            string sMailHost = string.Empty;
            string sAdNm = string.Empty;

			// 通常の登録と合わせる
			// MobileLib.Mobile.GetiModeId()
			if (sCarrier.Equals(ViCommConst.KDDI)) {
				if (sSubscriber.Length > 14) {
					sSubscriber = sSubscriber.Substring(0,14);
				}
			}

            if (sSite.Equals(string.Empty)) {
                sResult = RESULT_PARM_ERR;
            } else {
                using (Site oSite = new Site()) {
                    if (!oSite.GetValue(sSite,"MAIL_HOST",ref sMailHost)) {
                        sResult = RESULT_PARM_ERR;
                    }
                }
            }

            if (!sAdCd.Equals(string.Empty)) {
                using (Ad oAd = new Ad()) {
                    if (!oAd.IsExist(sAdCd,out sAdNm)) {
                        sResult = RESULT_PARM_ERR;
                    }
                }
            }


            if (sCarrier.Equals(string.Empty)) {
                sResult = RESULT_PARM_ERR;
            } else if (sCarrier.Equals(ViCommConst.DOCOMO) ||
                        sCarrier.Equals(ViCommConst.SOFTBANK) ||
                        sCarrier.Equals(ViCommConst.KDDI) ||
                        sCarrier.Equals(ViCommConst.ANDROID) ||
                        sCarrier.Equals(ViCommConst.IPHONE) ||
                        sCarrier.Equals(ViCommConst.CARRIER_OTHERS)) {
                if (sCarrier.Equals(ViCommConst.DOCOMO) || sCarrier.Equals(ViCommConst.SOFTBANK) || sCarrier.Equals(ViCommConst.KDDI)) {
                    if (sSubscriber.Equals(string.Empty)) {
                        sResult = RESULT_PARM_ERR;
                    }
                }
            } else {
                sResult = RESULT_PARM_ERR;
            }
            if (sResult.Equals(RESULT_OK)) {

                Random rnd = new Random();
                int iPass = rnd.Next(10000);


                using (DbSession db = new DbSession()) {
                    db.PrepareProcedure("REGIST_USER_MAN_TEMP");
                    db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSite);
                    db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,sAdCd);
                    db.ProcedureInParm("PAFFILIATER_CD",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PREGIST_AFFILIATE_CD",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,sCarrier);
                    db.ProcedureInParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,sSubscriber);
                    db.ProcedureInParm("PREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,string.Format("{0:D4}",iPass));
                    db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,sAgent);
                    db.ProcedureInParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PPREPAID_ID",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("PPREPAID_PW",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("POPEN_ID",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("POPEN_ID_TYPE",DbSession.DbType.VARCHAR2,string.Empty);
                    db.ProcedureInParm("POUTSIDE_IF_TEMP_REGIST_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
					db.ProcedureInParm("PGAME_REGIST_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
                    db.ProcedureInArrayParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,0,null);
                    db.ProcedureInArrayParm("PMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,0,null);
                    db.ProcedureInArrayParm("PMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,0,null);
                    db.ProcedureInParm("PMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,0);
                    db.ProcedureInArrayParm("PSYNC_REGIST_SITE_CD",DbSession.DbType.VARCHAR2,0,null);
                    db.ProcedureInParm("PSYNC_REGIST_COUNT",DbSession.DbType.NUMBER,0);
                    db.ProcedureOutParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2);
                    db.ProcedureOutParm("PEXIST_LOGIN_ID",DbSession.DbType.VARCHAR2);
                    db.ProcedureOutParm("PEXIST_PASSWORD",DbSession.DbType.VARCHAR2);
                    db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
                    db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
                    db.ExecuteProcedure();
                    if (db.GetStringValue("PRESULT").Equals(RESULT_OK)) {
                        sTempRegistId = "utnm" + db.GetStringValue("PTEMP_REGIST_ID") + "@" + sMailHost;
                    } else if (db.GetStringValue("PRESULT").Equals("2")) {
                        sLoginId = db.GetStringValue("PEXIST_LOGIN_ID");
                        sResult = RESULT_DUP;
                    } else {
                        sResult = RESULT_PARM_ERR;
                    }
                }
            }
            Response.ContentType = "text/plain";
            Response.Write(string.Format("result={0}&toaddr={1}&id={2}",sResult,sTempRegistId,sLoginId));

        } catch (Exception) {
            Response.ContentType = "text/plain";
            Response.Write("result=2");
        }
    }
}
