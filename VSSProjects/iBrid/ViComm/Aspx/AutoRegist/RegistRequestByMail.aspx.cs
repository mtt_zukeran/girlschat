﻿using System;
using System.Threading;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class RegistRequestByMail:System.Web.UI.Page {

	private const int MAX_ATTR = 16;

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			string sMailAddr = iBridUtil.GetStringValue(Request.QueryString["registmail"]);
			string sAdCd = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
			string sCodeType = ViCommConst.REGIST_FROM_NONE;
			string sCode = "";
			string sMailHost = "";
			bool bOk = true;

			if (sMailAddr.Equals("")) {
				bOk = false;
			}

			using (Site oSite = new Site()) {
				if (oSite.GetOne(sSiteCd)) {
					sMailHost = oSite.mailHost;
				} else {
					bOk = false;
				}
			}

			if (sAdCd.Equals("") == false) {
				using (Ad oAd = new Ad()) {
					string sAdNm;
					if (oAd.IsExist(sAdCd,out sAdNm)) {
						sCodeType = ViCommConst.REGIST_FROM_AD;
						sCode = sAdCd;
					}
				}
			}

			string sToAddr = string.Format("regm{0}00{1}{2}{3}@{4}",
				sSiteCd,
				ViCommConst.DOCOMO,
				sCodeType,
				sCode,
				sMailHost);

			if (bOk) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("MAIL_RX");
					db.ProcedureInParm("PFROM_ADDR",DbSession.DbType.VARCHAR2,sMailAddr);
					db.ProcedureInParm("PTO_ADDR",DbSession.DbType.VARCHAR2,sToAddr);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}

			Response.ContentType = "text/plain";
			if (bOk) {
				Response.Write("0");
			} else {
				Response.Write("1");
			}

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Write(string.Format("9"));
		}
	}

}
