﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: AutoRegist
--	Title			: 出演者認証
--	Progaram ID		: ApplyCast
--  Creation Date	: 2014.01.15
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.IO;
using System.Data;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ApplyCast:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
        try {
			string sTempRegistId = iBridUtil.GetStringValue(Request.QueryString["temp_regist_id"]);
			string sManagerSeq = iBridUtil.GetStringValue(Request.QueryString["manager_seq"]);
			DataSet dsTempRegist;
			string sSiteCd = string.Empty;
			string sLoginId = string.Empty;
			string sUserSeq = string.Empty;
			string sResult = string.Empty;
			string sRegistStatus = string.Empty;
			string sWebPhisicalDir = string.Empty;

			if (string.IsNullOrEmpty(sTempRegistId) || string.IsNullOrEmpty(sManagerSeq)) {
				Response.ContentType = "text/plain";
				Response.Write("error parameter");
				return;
			}

			using (TempRegist oTempRegist = new TempRegist()) {
				dsTempRegist = oTempRegist.GetOne(sTempRegistId);
			}

			if (dsTempRegist.Tables[0].Rows.Count == 0) {
				Response.ContentType = "text/plain";
				Response.Write("error temp_regist");
				return;
			}

			using (TempRegist oTempRegist = new TempRegist()) {
				oTempRegist.ApplyCast(sTempRegistId,sManagerSeq,string.Empty,out sSiteCd,out sLoginId,out sUserSeq,out sResult,out sRegistStatus);
			}

			if (sResult.Equals("0")) {
				using (Site oSite = new Site()) {
					oSite.GetValue(sSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
				}

				using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
					string sDir = "";

					sDir = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + sSiteCd + string.Format("\\Operator\\{0}",sLoginId);
					Directory.CreateDirectory(sDir);

					sDir = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + sSiteCd + string.Format("\\Operator\\{0}",sLoginId);
					Directory.CreateDirectory(sDir);

					if (sRegistStatus.Equals(ViCommConst.REGIST_COMPLITE)) {
						string sRegistAffileateCd = iBridUtil.GetStringValue(dsTempRegist.Tables[0].Rows[0]["REGIST_AFFILIATE_CD"]);
						string sMobileCarrireCd = iBridUtil.GetStringValue(dsTempRegist.Tables[0].Rows[0]["MOBILE_CARRIER_CD"]);
						string sTerminalUniqueId = iBridUtil.GetStringValue(dsTempRegist.Tables[0].Rows[0]["TERMINAL_UNIQUE_ID"]);
						string sIModeId = iBridUtil.GetStringValue(dsTempRegist.Tables[0].Rows[0]["IMODE_ID"]);
						string sTrackingUrl = iBridUtil.GetStringValue(dsTempRegist.Tables[0].Rows[0]["TRACKING_URL"]);
						string sTrackingAdditionInfo = iBridUtil.GetStringValue(dsTempRegist.Tables[0].Rows[0]["TRACKING_ADDITION_INFO"]);
						string sRegistIpAddr = iBridUtil.GetStringValue(dsTempRegist.Tables[0].Rows[0]["REGIST_IP_ADDR"]);
						string sUrl = string.Empty;

						if (!string.IsNullOrEmpty(sRegistAffileateCd)) {
							string sUID;

							if (sMobileCarrireCd.Equals(ViCommConst.KDDI)) {
								sUID = sTerminalUniqueId;
							} else {
								sUID = sIModeId;
							}

							sUrl = string.Format(sTrackingUrl + sTrackingAdditionInfo,sRegistAffileateCd,sLoginId,sRegistIpAddr,sUID);

							if (!sUrl.Equals(string.Empty)) {
								ViCommInterface.TransToParent(sUrl,true);
							}
						}
					}
				}

				Response.ContentType = "text/plain";
				Response.Write("ok");
				return;
			} else {
				Response.ContentType = "text/plain";
				Response.Write(string.Format("error apply_cast result={0}",sResult));
				return;
			}
		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Write("error exception");
			return;
		}
	}
}
