﻿using System;
using System.Threading;
using System.IO;
using System.Configuration;
using System.Text;
using iBridCommLib;
using ViComm;

public partial class RegistUserManByAAA:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		try {
			string sSiteCd = "A001";
			string sHandleNm = iBridUtil.GetStringValue(Request.QueryString["name"]);
			string sArea = iBridUtil.GetStringValue(Request.QueryString["area"]);
			string sEmailAddr = iBridUtil.GetStringValue(Request.QueryString["email"]);
			string sAdCd = iBridUtil.GetStringValue(Request.QueryString["adcd"]);

			string sManAttrSeq01 = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["AAAManAttrSeq01"]);
			string sManAttrSeq02 = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["AAAManAttrSeq02"]);
			string sManAttrSeq03 = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["AAAManAttrSeq03"]);
			string sManAttrSeq04 = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["AAAManAttrSeq04"]);
			string sManAttrSeq05 = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["AAAManAttrSeq05"]);


			Random rnd = new Random();
			int iPass = rnd.Next(10000);
			StringBuilder sText = new StringBuilder();
			sText.Append(string.Format("{0:D4}",iPass) + "\t");
			sText.Append(sEmailAddr + "\t");
			sText.Append("0\t");
			sText.Append(sAdCd + "\t");
			sText.Append(sHandleNm + "\t");
			sText.Append("\t");					// Birthday
			sText.Append(sArea);			// 都道府県

			decimal dSeq;
			using (BulkImpManHistory oBulkImpManHistory = new BulkImpManHistory()) {
				dSeq = oBulkImpManHistory.GetSeq();
			}
			// 一括会員取込履歴を作成
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("REGIST_BULK_IMP_MAN_HISTORY");
				db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
				db.ProcedureInParm("pBULK_IMP_MAN_HISTORY_SEQ",DbSession.DbType.NUMBER,dSeq);
				db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.MAN);
				db.ProcedureInParm("pMAN_ATTR_SEQ01",DbSession.DbType.VARCHAR2,sManAttrSeq01);
				db.ProcedureInParm("pMAN_ATTR_SEQ02",DbSession.DbType.VARCHAR2,sManAttrSeq02);
				db.ProcedureInParm("pMAN_ATTR_SEQ03",DbSession.DbType.VARCHAR2,sManAttrSeq03);
				db.ProcedureInParm("pMAN_ATTR_SEQ04",DbSession.DbType.VARCHAR2,sManAttrSeq04);
				db.ProcedureInParm("pMAN_ATTR_SEQ05",DbSession.DbType.VARCHAR2,sManAttrSeq05);
				db.ProcedureInParm("pMAN_ATTR_SEQ06",DbSession.DbType.VARCHAR2,null);// 予備
				db.ProcedureInParm("pMAN_ATTR_SEQ07",DbSession.DbType.VARCHAR2,null);// 予備
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("REGIST_BULK_IMP_MAN_DATA");
				db.ProcedureInParm("PNO_TRAN",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
				db.ProcedureInParm("PBULK_IMP_MAN_HISTORY_SEQ",DbSession.DbType.NUMBER,dSeq);
				db.ProcedureInParm("PTEXT_LINE_NO",DbSession.DbType.NUMBER,1);
				db.ProcedureInParm("PTEXT_DTL",DbSession.DbType.VARCHAR2,sText.ToString());
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("BULK_IMPORT_MAN");
				db.ProcedureInParm("PBULK_IMP_MAN_HISTORY_SEQ",DbSession.DbType.NUMBER,dSeq);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}

			Response.ContentType = "text/plain";
			Response.Write("result=0");

		} catch (Exception) {
			Response.ContentType = "text/plain";
			Response.Write("result=9");
		}
	}
}
