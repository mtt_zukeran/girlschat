/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 投票順位XML取得(エントリー制)
--	Progaram ID		: GetElectionRankXml
--  Creation Date	: 2013.12.17
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Xml;
using System.IO;
using System.Data;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class GetElectionRankXml:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		XmlDocument oDocument = new XmlDocument();
		XmlDeclaration oDeclaration = oDocument.CreateXmlDeclaration("1.0","Shift_JIS",null);
		oDocument.AppendChild(oDeclaration);

		XmlElement oRoot = oDocument.CreateElement("root");
		oDocument.AppendChild(oRoot);

		DataSet dsElectionPeriod;

		using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
			dsElectionPeriod = oElectionPeriod.GetCurrent(PwViCommConst.MAIN_SITE_CD);
		}

		if (dsElectionPeriod.Tables[0].Rows.Count >= 1) {
			DataRow drElectionPeriod = dsElectionPeriod.Tables[0].Rows[0];

			XmlElement oElectionPeriod = oDocument.CreateElement("election_period");
			oRoot.AppendChild(oElectionPeriod);

			XmlElement oSecondPeriodEndDate = oDocument.CreateElement("second_period_end_date");
			XmlElement oFixRankFlag = oDocument.CreateElement("fix_rank_flag");
			XmlElement oFixRankDate = oDocument.CreateElement("fix_rank_date");

			oSecondPeriodEndDate.InnerText = iBridUtil.GetStringValue(drElectionPeriod["SECOND_PERIOD_END_DATE"]);
			oFixRankFlag.InnerText = iBridUtil.GetStringValue(drElectionPeriod["FIX_RANK_FLAG"]);
			oFixRankDate.InnerText = iBridUtil.GetStringValue(drElectionPeriod["FIX_RANK_DATE"]);
			
			DateTime dtFirstPeriodEndDate = DateTime.Parse(drElectionPeriod["FIRST_PERIOD_END_DATE"].ToString());

			oElectionPeriod.AppendChild(oSecondPeriodEndDate);
			oElectionPeriod.AppendChild(oFixRankFlag);
			oElectionPeriod.AppendChild(oFixRankDate);

			ElectionRank.SearchCondition oCondition = new ElectionRank.SearchCondition();
			oCondition.SiteCd = iBridUtil.GetStringValue(drElectionPeriod["SITE_CD"]);
			oCondition.ElectionPeriodSeq = iBridUtil.GetStringValue(drElectionPeriod["ELECTION_PERIOD_SEQ"]);
			oCondition.FixRankFlag = iBridUtil.GetStringValue(drElectionPeriod["FIX_RANK_FLAG"]);
			
			string sElectionPeriodStatus = iBridUtil.GetStringValue(this.Request.QueryString["electionperiodstatus"]);
			if (string.IsNullOrEmpty(sElectionPeriodStatus)) {
				if (DateTime.Now <= dtFirstPeriodEndDate) {
					oCondition.ElectionPeriodStatus = PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD;
				} else {
					oCondition.SecondPeriodFlag = ViCommConst.FLAG_ON_STR;
				}
			} else {
				oCondition.ElectionPeriodStatus = sElectionPeriodStatus;
				
				if (!sElectionPeriodStatus.Equals(PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD)) {
					oCondition.SecondPeriodFlag = ViCommConst.FLAG_ON_STR;
				}
			}
			
			oCondition.PageNo = 1;
			oCondition.RecPerPage = 8;

			DataSet dsElectionRank;

			using (ElectionRank oElectionRank = new ElectionRank()) {
				dsElectionRank = oElectionRank.GetPageCollection(oCondition);
			}

			foreach (DataRow drElectionRank in dsElectionRank.Tables[0].Rows) {
				XmlElement oCast = oDocument.CreateElement("cast");
				oRoot.AppendChild(oCast);

				XmlElement oHandleNm = oDocument.CreateElement("handle_nm");
				XmlElement oAge = oDocument.CreateElement("age");
				XmlElement oPhotoImgPath = oDocument.CreateElement("photo_img_path");
				XmlElement oVoteRank = oDocument.CreateElement("vote_rank");
				XmlElement oVoteCount = oDocument.CreateElement("vote_count");

				oHandleNm.InnerText = iBridUtil.GetStringValue(drElectionRank["HANDLE_NM"]);
				oAge.InnerText = iBridUtil.GetStringValue(drElectionRank["AGE"]);
				oPhotoImgPath.InnerText = iBridUtil.GetStringValue(drElectionRank["PHOTO_IMG_PATH"]);
				oVoteRank.InnerText = iBridUtil.GetStringValue(drElectionRank["VOTE_RANK"]);
				oVoteCount.InnerText = iBridUtil.GetStringValue(drElectionRank["VOTE_COUNT"]);

				oCast.AppendChild(oHandleNm);
				oCast.AppendChild(oAge);
				oCast.AppendChild(oPhotoImgPath);
				oCast.AppendChild(oVoteRank);
				oCast.AppendChild(oVoteCount);
			}
		}

		Response.ContentType = "text/xml";
		Response.Write(oDocument.OuterXml);
	}
}
