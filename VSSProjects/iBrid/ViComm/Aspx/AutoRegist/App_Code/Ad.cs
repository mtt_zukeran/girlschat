/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: 広告コード
--	Progaram ID		: Ad
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Ad:DbSession {

	public Ad() {
	}

	public bool IsExist(string pAdCd,out string pAdNm) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pAdNm = "";

		conn = DbConnect("Ad.IsExist");

		using (cmd = CreateSelectCommand("SELECT AD_NM FROM T_AD WHERE AD_CD =:AD_CD",conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("AD_CD",pAdCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_AD");
				if (ds.Tables["T_AD"].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pAdNm = dr["AD_NM"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
