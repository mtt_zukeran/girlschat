﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャストキャラクター

--	Progaram ID		: CastCharacter
--
--  Creation Date	: 2009.10.09
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class CastCharacter:DbSession {
	public CastCharacter() {
	}

	public bool IsExistFriendIntroCd(string pFriendIntroCd) {
		DataSet ds;
		bool bExist = false;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT USER_SEQ FROM T_CAST_CHARACTER WHERE FRIEND_INTRO_CD = :FRIEND_INTRO_CD ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("FRIEND_INTRO_CD",pFriendIntroCd);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_CAST_CHARACTER");

				if (ds.Tables["T_CAST_CHARACTER"].Rows.Count != 0) {
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
