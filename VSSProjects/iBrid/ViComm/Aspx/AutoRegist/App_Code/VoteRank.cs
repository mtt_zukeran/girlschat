﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: AutoRegist
--	Title			: 投票順位
--	Progaram ID		: VoteRank
--  Creation Date	: 2013.10.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class VoteRank:DbSession {
	public VoteRank() {
	}

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string voteTermSeq;
		public string VoteTermSeq {
			get {
				return this.voteTermSeq;
			}
			set {
				this.voteTermSeq = value;
			}
		}

		private string fixRankFlag;
		public string FixRankFlag {
			get {
				return this.fixRankFlag;
			}
			set {
				this.fixRankFlag = value;
			}
		}

		private int pageNo;
		public int PageNo {
			get {
				return this.pageNo;
			}
			set {
				this.pageNo = value;
			}
		}

		private int recPerPage;
		public int RecPerPage {
			get {
				return this.recPerPage;
			}
			set {
				this.recPerPage = value;
			}
		}
	}

	public DataSet GetPageCollection(SearchCondition pCondition) {
		int iStartIndex = (pCondition.PageNo - 1) * pCondition.RecPerPage;
		string sViewNm = string.Empty;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		if (pCondition.FixRankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sViewNm = "VW_VOTE_FIX_RANK00";
		} else {
			sViewNm = "VW_VOTE_RANK00";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	AGE,");
		oSqlBuilder.AppendLine("	GET_PHOTO_IMG_PATH(SITE_CD,LOGIN_ID,PROFILE_PIC_SEQ) AS PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	VOTE_RANK,");
		oSqlBuilder.AppendLine("	VOTE_COUNT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendFormat("	{0} P",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = CreateOrderExpresion(pCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pCondition.RecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.VoteTermSeq)) {
			SysPrograms.SqlAppendWhere("VOTE_TERM_SEQ = :VOTE_TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":VOTE_TERM_SEQ",pCondition.VoteTermSeq));
		}

		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(SearchCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = "ORDER BY SITE_CD,VOTE_TERM_SEQ,VOTE_COUNT DESC,LAST_VOTE_DATE ASC";
		return sSortExpression;
	}
}
