﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: AutoRegist
--	Title			: 投票設定(エントリー制)
--	Progaram ID		: ElectionPeriod
--  Creation Date	: 2013.12.17
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class ElectionPeriod:DbSession {
	public ElectionPeriod() {
	}

	public DataSet GetCurrent(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		SITE_CD,");
		oSqlBuilder.AppendLine("		ELECTION_PERIOD_SEQ,");
		oSqlBuilder.AppendLine("		FIRST_PERIOD_END_DATE,");
		oSqlBuilder.AppendLine("		SECOND_PERIOD_END_DATE,");
		oSqlBuilder.AppendLine("		FIX_RANK_FLAG,");
		oSqlBuilder.AppendLine("		FIX_RANK_DATE");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_ELECTION_PERIOD");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("		FIRST_PERIOD_START_DATE <= SYSDATE");
		oSqlBuilder.AppendLine("	ORDER BY");
		oSqlBuilder.AppendLine("		SITE_CD,FIRST_PERIOD_START_DATE DESC");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM = 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}
}
