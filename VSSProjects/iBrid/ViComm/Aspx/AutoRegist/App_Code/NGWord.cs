﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: NGワード
--	Progaram ID		: NGWord
--
--  Creation Date	: 2014.10.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class NGWord:DbSession {
	private string regNGWord;
	private ArrayList arrNGWord;

	public NGWord() {
	}

	public NGWord(string pSiteCd) {
		regNGWord = "";
		arrNGWord = new ArrayList();
		arrNGWord.Clear();

		//正規表現用NGワード 
		DataSet regDs;
		try {
			conn = DbConnect("NGWord.NGWord");
			using (cmd = CreateSelectCommand("SELECT NG_WORD FROM T_NG_WORD WHERE SITE_CD = :SITE_CD AND NG_WORD_MARK_TYPE = :NG_WORD_MARK_TYPE ",conn))
			using (regDs = new DataSet())
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("NG_WORD_MARK_TYPE",ViCommConst.NG_WORD_MARK_TYPE_REGULAR);
				da.Fill(regDs,"T_NG_WORD");
				foreach (DataRow dr in regDs.Tables[0].Rows) {
					regNGWord += dr["NG_WORD"].ToString() + "|";
				}
			}
			if (regNGWord.Length > 1) {
				regNGWord = regNGWord.Substring(0,regNGWord.Length - 1);
			}
		} finally {
			conn.Close();
		}

		//通常文章NGワード 
		DataSet strDs;
		try {
			conn = DbConnect("NGWord.NGWord");
			using (cmd = CreateSelectCommand("SELECT NG_WORD FROM T_NG_WORD WHERE SITE_CD = :SITE_CD AND NG_WORD_MARK_TYPE =: NG_WORD_MARK_TYPE",conn))
			using (strDs = new DataSet())
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("NG_WORD_MARK_TYPE",ViCommConst.NG_WORD_MARK_TYPE_STRING);
				da.Fill(strDs,"T_NG_WORD");
				foreach (DataRow dr in strDs.Tables[0].Rows) {
					arrNGWord.Add(dr["NG_WORD"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
	}

	public bool VaidateDoc(string pDoc,out string pNGWord) {
		pNGWord = "";
		bool bOK = true;

		//正規表現用NGワード 
		if (regNGWord.Equals("") == false) {
			Regex rgx = new Regex(regNGWord);
			Match rgxMatch = rgx.Match(pDoc);
			if (rgxMatch.Success) {
				bOK = false;
				pNGWord = rgxMatch.Value;
				pNGWord = pNGWord.Replace("\r\n","<br />");
				EventLogWriter.Force("NG WORLD REGEX:" + rgxMatch.Value + " " + pDoc);
			}
		}

		//通常文章NGワード 
		if (bOK) {
			if (arrNGWord.Count > 0) {
				for (int i = 0;i < arrNGWord.Count;i++) {
					if (pDoc.IndexOf(arrNGWord[i].ToString()) >= 0) {
						bOK = false;
						pNGWord = arrNGWord[i].ToString();
						pNGWord = pNGWord.Replace("\r\n","<br />");
						EventLogWriter.Force("NG WORLD NORMAL:" + arrNGWord[i].ToString() + " " + pDoc);
						break;
					}
				}
			}
		}
		return bOK;
	}

	public string ReplaceMask(string pDoc) {
		string sMaskDoc = pDoc;
		//正規表現用NGワード 
		if (regNGWord.Equals("") == false) {
			sMaskDoc = Regex.Replace(sMaskDoc,regNGWord,"****");
		}

		//通常文章NGワード 
		if (arrNGWord.Count > 0) {
			for (int i = 0;i < arrNGWord.Count;i++) {
				if (sMaskDoc.IndexOf(arrNGWord[i].ToString()) >= 0) {
					sMaskDoc = sMaskDoc.Replace(arrNGWord[i].ToString(),"****");
				}
			}
		}
		return sMaskDoc;
	}
}
