﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: AutoRegist
--	Title			: 仮登録
--	Progaram ID		: TempRegist
--
--  Creation Date	: 2013.05.23
--  Creater			: K.Miyazato
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class TempRegist:DbSession {
	public TempRegist() {
	}

	public void RegistUserWomanTemp(
		string pSiteCd,
		string pAdCd,
		string pAffiliaterCd,
		string pRegistAffiliateCd,
		string pMobileCarrierCd,
		string pTerminalUniqueId,
		string pIModeId,
		string pRegistIpAddr,
		string pTel,
		string pLoginPassword,
		string pCastNm,
		string pCastKanaNm,
		string pBirthday,
		string pMobileTerminalNm,
		string pIntroducerFriendCd,
		string pQuestionDoc,
		string pHandleNm,
		int pGameRegistFlag,
		out string pTempRegistId,
		out string pResult,
		int pIdolRegistFlag,
		string pPrefectureCd,
		string pAddress1,
		string pGuardianNm,
		string pGuardianTel,
		string pIdolSexAttrSeq,
		string pEmailAddr,
		string pIdolWishCastTypeAttrSeq
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_USER_WOMAN_TEMP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pAD_CD",DbSession.DbType.VARCHAR2,pAdCd);
			db.ProcedureInParm("pAFFILIATER_CD",DbSession.DbType.VARCHAR2,pAffiliaterCd);
			db.ProcedureInParm("pREGIST_AFFILIATE_CD",DbSession.DbType.VARCHAR2,pRegistAffiliateCd);
			db.ProcedureInParm("pMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,pMobileCarrierCd);
			db.ProcedureInParm("pTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("pIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureInParm("pREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,pRegistIpAddr);
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("pLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureInParm("pCAST_NM",DbSession.DbType.VARCHAR2,pCastNm);
			db.ProcedureInParm("pCAST_KANA_NM",DbSession.DbType.VARCHAR2,pCastKanaNm);
			db.ProcedureInParm("pBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("pMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureInParm("pINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,pIntroducerFriendCd);
			db.ProcedureInParm("pQUESTION_DOC",DbSession.DbType.VARCHAR2,pQuestionDoc);
			db.ProcedureInParm("pHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("pGAME_REGIST_FLAG",DbSession.DbType.NUMBER,pGameRegistFlag);
			db.ProcedureOutParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pIDOL_REGIST_FLAG",DbSession.DbType.NUMBER,pIdolRegistFlag);
			db.ProcedureInParm("pPREFECTURE_CD",DbSession.DbType.VARCHAR2,pPrefectureCd);
			db.ProcedureInParm("pADDRESS1",DbSession.DbType.VARCHAR2,pAddress1);
			db.ProcedureInParm("pGUARDIAN_NM",DbSession.DbType.VARCHAR2,pGuardianNm);
			db.ProcedureInParm("pGUARDIAN_TEL",DbSession.DbType.VARCHAR2,pGuardianTel);
			db.ProcedureInParm("pIDOL_SEX_ATTR_SEQ",DbSession.DbType.VARCHAR2,pIdolSexAttrSeq);
			db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,pEmailAddr);
			db.ProcedureInParm("pIDOL_WISH_CAST_TYPE_ATTR_SEQ",DbSession.DbType.VARCHAR2,pIdolWishCastTypeAttrSeq);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pTempRegistId = db.GetStringValue("pTEMP_REGIST_ID");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void TxApplyRegistCastPCMail(string pTempRegistId,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_APPLY_REGIST_CAST_PC_MAIL");
			db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,pTempRegistId);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public DataSet GetOne(string pTempRegistId) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_TEMP_REGIST02");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	TEMP_REGIST_ID = :TEMP_REGIST_ID");

		oParamList.Add(new OracleParameter(":TEMP_REGIST_ID",pTempRegistId));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public void ApplyCast(
		string pTempRegistId,
		string pManagerSeq,
		string pRemarks,
		out string pSiteCd,
		out string pLoginId,
		out string pUserSeq,
		out string pResult,
		out string pRegistStatus
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("APPLY_CAST");
			db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,pTempRegistId);
			db.ProcedureInParm("pMANAGER_SEQ",DbSession.DbType.VARCHAR2,pManagerSeq);
			db.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,pRemarks);
			db.ProcedureOutParm("pSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREGIST_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pSiteCd = db.GetStringValue("pSITE_CD");
			pLoginId = db.GetStringValue("pLOGIN_ID");
			pUserSeq = db.GetStringValue("pUSER_SEQ");
			pResult = db.GetStringValue("pRESULT");
			pRegistStatus = db.GetStringValue("pREGIST_STATUS");
		}		
	}

	public void RegistUserManTemp(
		string pSiteCd,
		string pAdCd,
		string pAffiliaterCd,
		string pRegistAffiliateCd,
		string pMobileCarrierCd,
		string pTerminalUniqueId,
		string pIModeId,
		string pRegistIpAddr,
		string pTel,
		string pLoginPassword,
		string pHandleNm,
		string pBirthday,
		string pMobileTerminalNm,
		string pIntroducerFriendCd,
		string pPrepaidId,
		string pPrepaidPw,
		string pOpenId,
		string pOpenIdType,
		string[] pAttrTypeSeq,
		string[] pAttrSeq,
		string[] pAttrInputValue,
		int pMaxAttrCount,
		string[] pRegistSiteCd,
		int pGameRegistFlag,
		out string pTempRegistId,
		out string pResult,
		string pEmailAddr
	) {
		using (DbSession db = new DbSession()) {
			int iRegistSiteCount = 0;

			if (pRegistSiteCd != null) {
				iRegistSiteCount = pRegistSiteCd.Length;
			}
			db.PrepareProcedure("REGIST_USER_MAN_TEMP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pAD_CD",DbSession.DbType.VARCHAR2,pAdCd);
			db.ProcedureInParm("pAFFILIATER_CD",DbSession.DbType.VARCHAR2,pAffiliaterCd);
			db.ProcedureInParm("pREGIST_AFFILIATE_CD",DbSession.DbType.VARCHAR2,pRegistAffiliateCd);
			db.ProcedureInParm("pMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,pMobileCarrierCd);
			db.ProcedureInParm("pCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("pTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("pIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureInParm("pREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,pRegistIpAddr);
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("pLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureInParm("pHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("pBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("pMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureInParm("pINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,pIntroducerFriendCd);
			db.ProcedureInParm("pPREPAID_ID",DbSession.DbType.VARCHAR2,pPrepaidId);
			db.ProcedureInParm("pPREPAID_PW",DbSession.DbType.VARCHAR2,pPrepaidPw);
			db.ProcedureInParm("pOPEN_ID",DbSession.DbType.VARCHAR2,pOpenId);
			db.ProcedureInParm("pOPEN_ID_TYPE",DbSession.DbType.VARCHAR2,pOpenIdType);
			db.ProcedureInParm("pOUTSIDE_IF_TEMP_REGIST_FLAG",DbSession.DbType.NUMBER,ViComm.ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pGAME_REGIST_FLAG",DbSession.DbType.NUMBER,pGameRegistFlag);
			db.ProcedureInArrayParm("pMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrTypeSeq);
			db.ProcedureInArrayParm("pMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrSeq);
			db.ProcedureInArrayParm("pMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrInputValue);
			db.ProcedureInParm("pMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,pMaxAttrCount);
			db.ProcedureInArrayParm("pMAN_SYNC_REGIST_SITE",DbSession.DbType.VARCHAR2,iRegistSiteCount,pRegistSiteCd);
			db.ProcedureInParm("pMAN_SYNC_REGIST_COUNT",DbSession.DbType.NUMBER,iRegistSiteCount);
			db.ProcedureOutParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pEXIST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pEXIST_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,pEmailAddr);
			db.ExecuteProcedure();
			pTempRegistId = db.GetStringValue("pTEMP_REGIST_ID");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void TxApplyRegistUserPCMail(string pTempRegistId,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_APPLY_REGIST_USER_PC_MAIL");
			db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,pTempRegistId);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
