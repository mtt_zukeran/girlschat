﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員
--	Progaram ID		: UserMan
--
--  Creation Date	: 2014.09.25
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/16  i-Brid(Y.Inoue)

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using iBridCommLib;

[System.Serializable]
public class UserMan:DbSession {
	public List<UserManAttr> attrList;
	
	public UserMan() {
		attrList = new List<UserManAttr>();
	}
	
	public void GetAttrType(string pSiteCd) {
		DataSet ds;
		UserManAttr attr;
		attrList.Clear();

		using (UserManAttrType oAttrType = new UserManAttrType()) {
			ds = oAttrType.GetList(pSiteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				attr = new UserManAttr();
				attr.attrTypeSeq = dr["MAN_ATTR_TYPE_SEQ"].ToString();
				attr.attrTypeNm = dr["MAN_ATTR_TYPE_NM"].ToString();
				attr.inputType = dr["INPUT_TYPE"].ToString();
				attr.registInputFlag = dr["REGIST_INPUT_FLAG"].ToString().Equals("1");
				attr.profileReqItemFlag = dr["PROFILE_REQ_ITEM_FLAG"].ToString().Equals("1");
				attr.proirity = dr["PRIORITY"].ToString();
				attr.rowCount = dr["ROW_COUNT"].ToString();
				attr.itemNo = dr["ITEM_NO"].ToString();
				attrList.Add(attr);
			}
		}
	}
}

[System.Serializable]
public class UserManAttr {
	public string attrTypeSeq;
	public string attrTypeNm;
	public string attrSeq;
	public string attrNm;
	public string attrImputValue;
	public string inputType;
	public string proirity;
	public string rowCount;
	public string itemNo;
	public string itemCd;
	public bool registInputFlag;
	public bool profileReqItemFlag;

	public UserManAttr() {
		attrTypeSeq = "";
		attrTypeNm = "";
		attrSeq = "";
		attrNm = "";
		attrImputValue = "";
		inputType = "";
		proirity = "";
		rowCount = "";
		itemNo = "";
		itemCd = "";
		registInputFlag = false;
		profileReqItemFlag = false;
	}
}

[System.Serializable]
public class UserManAttrType:DbSession {

	public UserManAttrType() {
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect("UserManAttrType.GetList");

			string sSql = "SELECT " +
								"MAN_ATTR_TYPE_SEQ		, " +
								"MAN_ATTR_TYPE_NM		, " +
								"MAN_ATTR_TYPE_FIND_NM	, " +
								"INPUT_TYPE				, " +
								"PRIORITY				, " +
								"ITEM_NO				, " +
								"ROW_COUNT				, " +
								"GROUPING_CATEGORY_CD	, " +
								"OMIT_SEEK_CONTION_FLAG	, " +
								"PROFILE_REQ_ITEM_FLAG  ," +
								"REGIST_INPUT_FLAG        " +
							"FROM " +
								"T_MAN_ATTR_TYPE " +
							"WHERE " +
								"SITE_CD			= :SITE_CD	AND " +
								"NA_FLAG			= 0			AND	" +
								"REGIST_INPUT_FLAG	= 1				" +
							"ORDER BY " +
								"PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_ATTR_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}

[System.Serializable]
public class UserManAttrTypeValue:DbSession {

	public UserManAttrTypeValue() {
	}

	public string GetAttrSeqByItemCd(string pSiteCd,string pManAttrTypeSeq,string pItemCd) {
		DataSet ds;
		string sValue;
		try {
			conn = DbConnect("UserManAttrTypeValue.GetAttrTypeSeq");

			string sSql = "SELECT " +
								"MAN_ATTR_SEQ			 " +
							"FROM " +
								"T_MAN_ATTR_TYPE_VALUE " +
							"WHERE " +
								"SITE_CD			= :SITE_CD					AND " +
								"MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ		AND	" +
								"ITEM_CD			= :ITEM_CD						";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAN_ATTR_TYPE_SEQ",pManAttrTypeSeq);
				cmd.Parameters.Add("ITEM_CD",pItemCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_ATTR_TYPE_VALUE");
					sValue = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["MAN_ATTR_SEQ"]);
				}
			}
		} finally {
			conn.Close();
		}
		return sValue;
	}
}