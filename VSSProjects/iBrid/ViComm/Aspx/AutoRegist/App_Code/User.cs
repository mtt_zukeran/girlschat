﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: AutoRegist
--	Title			: ユーザー
--	Progaram ID		: User
--
--  Creation Date	: 2013.05.20
--  Creater			: K.Miyazato
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class User:DbSession {
	public User() {
	}

	public bool IsBlackUser(string pNoType,string pTermIdOrTel) {
		DataSet ds;

		bool bBlack = false;
		try {
			conn = DbConnect("User.IsBlackUser");

			string sSql = "SELECT 1 " +
							"FROM " +
							" T_UNABLE_REGIST_NO " +
							"WHERE " +
							" NO_TYPE					= :NO_TYPE AND " +
							" TERMINAL_UNIQUE_ID_OR_TEL	= :TERMINAL_UNIQUE_ID_OR_TEL ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("NO_TYPE",pNoType);
				cmd.Parameters.Add("TERMINAL_UNIQUE_ID_OR_TEL",pTermIdOrTel);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
				if (ds.Tables[0].Rows.Count > 0) {
					bBlack = true;
				}
			}
		} finally {
			conn.Close();
		}
		return bBlack;
	}

	public bool IsTelephoneRegistered(string pTelephoneNo) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect("User.IsTelephoneRegistered");
			ds = new DataSet();

			string sSql = "SELECT TEL,USER_STATUS " +
							"FROM " +
								"T_USER " +
							"WHERE " +
								"TEL = :TEL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("TEL",pTelephoneNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
				}
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_HOLD)) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void CheckCastEmailAddrExist(string pSiteCd,string pEmailAddr,out int iExistFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_CAST_EMAIL_ADDR_EXIST");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,pEmailAddr);
			db.ProcedureOutParm("pEXIST_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			iExistFlag = db.GetIntValue("pEXIST_FLAG");
        }
	}
}
