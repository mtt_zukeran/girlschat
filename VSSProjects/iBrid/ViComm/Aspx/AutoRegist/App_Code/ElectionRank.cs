﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: AutoRegist
--	Title			: 投票順位(エントリー制)
--	Progaram ID		: ElectionRank
--  Creation Date	: 2013.12.17
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class ElectionRank:DbSession {
	public ElectionRank() {
	}

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string electionPeriodSeq;
		public string ElectionPeriodSeq {
			get {
				return this.electionPeriodSeq;
			}
			set {
				this.electionPeriodSeq = value;
			}
		}

		private string fixRankFlag;
		public string FixRankFlag {
			get {
				return this.fixRankFlag;
			}
			set {
				this.fixRankFlag = value;
			}
		}
		
		private string electionPeriodStatus;
		public string ElectionPeriodStatus {
			get {
				return this.electionPeriodStatus;
			}
			set {
				this.electionPeriodStatus = value;
			}
		}

		private string secondPeriodFlag;
		public string SecondPeriodFlag {
			get {
				return this.secondPeriodFlag;
			}
			set {
				this.secondPeriodFlag = value;
			}
		}

		private int pageNo;
		public int PageNo {
			get {
				return this.pageNo;
			}
			set {
				this.pageNo = value;
			}
		}

		private int recPerPage;
		public int RecPerPage {
			get {
				return this.recPerPage;
			}
			set {
				this.recPerPage = value;
			}
		}
	}

	public DataSet GetPageCollection(SearchCondition pCondition) {
		int iStartIndex = (pCondition.PageNo - 1) * pCondition.RecPerPage;
		string sViewNm = string.Empty;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		if (pCondition.FixRankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sViewNm = "VW_ELECTION_RANK_FIX00";
		} else {
			sViewNm = "VW_ELECTION_ENTRY00";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	AGE,");
		oSqlBuilder.AppendLine("	GET_PHOTO_IMG_PATH(SITE_CD,LOGIN_ID,PROFILE_PIC_SEQ) AS PHOTO_IMG_PATH,");
		if (pCondition.ElectionPeriodStatus.Equals(PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD)) {
			oSqlBuilder.AppendLine("	FIRST_VOTE_COUNT AS VOTE_COUNT	,	");
			oSqlBuilder.AppendLine("	FIRST_VOTE_RANK AS VOTE_RANK,	");
			oSqlBuilder.AppendLine("	FIRST_LAST_VOTE_DATE AS LAST_VOTE_DATE	,	");
		} else if (pCondition.ElectionPeriodStatus.Equals(PwViCommConst.ElectionPeriodStatus.SECOND_PERIOD)) {
			oSqlBuilder.AppendLine("	SECOND_VOTE_COUNT AS VOTE_COUNT	,	");
			oSqlBuilder.AppendLine("	SECOND_VOTE_RANK AS VOTE_RANK	,	");
			oSqlBuilder.AppendLine("	SECOND_LAST_VOTE_DATE AS LAST_VOTE_DATE	,	");
		} else {
			oSqlBuilder.AppendLine("	VOTE_COUNT								,	");
			oSqlBuilder.AppendLine("	VOTE_RANK	,	");
			oSqlBuilder.AppendLine("	SECOND_LAST_VOTE_DATE AS LAST_VOTE_DATE	,	");
		}
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendFormat("	{0} P",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = CreateOrderExpresion(pCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pCondition.RecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.ElectionPeriodSeq)) {
			SysPrograms.SqlAppendWhere("ELECTION_PERIOD_SEQ = :ELECTION_PERIOD_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ELECTION_PERIOD_SEQ",pCondition.ElectionPeriodSeq));
		}
		
		if (!string.IsNullOrEmpty(pCondition.SecondPeriodFlag)) {
			SysPrograms.SqlAppendWhere("SECOND_PERIOD_FLAG = :SECOND_PERIOD_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SECOND_PERIOD_FLAG",pCondition.SecondPeriodFlag));
		}

		SysPrograms.SqlAppendWhere("RETIRE_FLAG = :RETIRE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":RETIRE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("REFUSE_FLAG = :REFUSE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":REFUSE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(SearchCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = "ORDER BY SITE_CD,ELECTION_PERIOD_SEQ,VOTE_COUNT DESC,LAST_VOTE_DATE ASC";
		return sSortExpression;
	}
}
