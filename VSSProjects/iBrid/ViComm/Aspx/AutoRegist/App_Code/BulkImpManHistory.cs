﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 一括会員取込履歴

--	Progaram ID		: BulkImpManHistory
--
--  Creation Date	: 2010.10.18
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;


/// <summary>
/// T_BULK_IMP_MAN_HISTORYテーブルへのデータアクセスを提供するクラス。
/// </summary>
public class BulkImpManHistory:DbSession {
	public BulkImpManHistory() {}

	/// <summary>
	/// 一括会員取込履歴SEQを採番し、取得する
	/// </summary>
	/// <returns>一括会員取込履歴SEQ</returns>
	public decimal GetSeq() {
		string sSql = "SELECT BULK_IMP_MAN_HISTORY_SEQ.NEXTVAL FROM DUAL";
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sSql,conn)) {
				return (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}
	}
	

}
