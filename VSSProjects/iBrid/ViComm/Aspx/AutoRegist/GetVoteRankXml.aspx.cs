﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 投票順位XML取得
--	Progaram ID		: GetVoteRankXml
--  Creation Date	: 2013.10.11
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Xml;
using System.IO;
using System.Data;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class GetVoteRankXml:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		XmlDocument oDocument = new XmlDocument();
		XmlDeclaration oDeclaration = oDocument.CreateXmlDeclaration("1.0","Shift_JIS",null);
		oDocument.AppendChild(oDeclaration);

		XmlElement oRoot = oDocument.CreateElement("root");
		oDocument.AppendChild(oRoot);

		DataSet dsVoteTerm;

		using (VoteTerm oVoteTerm = new VoteTerm()) {
			dsVoteTerm = oVoteTerm.GetCurrent(PwViCommConst.MAIN_SITE_CD);
		}

		if (dsVoteTerm.Tables[0].Rows.Count >= 1) {
			DataRow drVoteTerm = dsVoteTerm.Tables[0].Rows[0];

			XmlElement oVoteTerm = oDocument.CreateElement("vote_term");
			oRoot.AppendChild(oVoteTerm);

			XmlElement oVoteEndDate = oDocument.CreateElement("vote_end_date");
			XmlElement oFixRankFlag = oDocument.CreateElement("fix_rank_flag");
			XmlElement oFixRankDate = oDocument.CreateElement("fix_rank_date");

			oVoteEndDate.InnerText = iBridUtil.GetStringValue(drVoteTerm["VOTE_END_DATE"]);
			oFixRankFlag.InnerText = iBridUtil.GetStringValue(drVoteTerm["FIX_RANK_FLAG"]);
			oFixRankDate.InnerText = iBridUtil.GetStringValue(drVoteTerm["FIX_RANK_DATE"]);

			oVoteTerm.AppendChild(oVoteEndDate);
			oVoteTerm.AppendChild(oFixRankFlag);
			oVoteTerm.AppendChild(oFixRankDate);

			VoteRank.SearchCondition oCondition = new VoteRank.SearchCondition();
			oCondition.SiteCd = iBridUtil.GetStringValue(drVoteTerm["SITE_CD"]);
			oCondition.VoteTermSeq = iBridUtil.GetStringValue(drVoteTerm["VOTE_TERM_SEQ"]);
			oCondition.FixRankFlag = iBridUtil.GetStringValue(drVoteTerm["FIX_RANK_FLAG"]);
			oCondition.PageNo = 1;
			oCondition.RecPerPage = 8;

			DataSet dsVoteRank;

			using (VoteRank oVoteRank = new VoteRank()) {
				dsVoteRank = oVoteRank.GetPageCollection(oCondition);
			}

			foreach (DataRow drVoteRank in dsVoteRank.Tables[0].Rows) {
				XmlElement oCast = oDocument.CreateElement("cast");
				oRoot.AppendChild(oCast);

				XmlElement oHandleNm = oDocument.CreateElement("handle_nm");
				XmlElement oAge = oDocument.CreateElement("age");
				XmlElement oPhotoImgPath = oDocument.CreateElement("photo_img_path");
				XmlElement oVoteRank = oDocument.CreateElement("vote_rank");
				XmlElement oVoteCount = oDocument.CreateElement("vote_count");

				oHandleNm.InnerText = iBridUtil.GetStringValue(drVoteRank["HANDLE_NM"]);
				oAge.InnerText = iBridUtil.GetStringValue(drVoteRank["AGE"]);
				oPhotoImgPath.InnerText = iBridUtil.GetStringValue(drVoteRank["PHOTO_IMG_PATH"]);
				oVoteRank.InnerText = iBridUtil.GetStringValue(drVoteRank["VOTE_RANK"]);
				oVoteCount.InnerText = iBridUtil.GetStringValue(drVoteRank["VOTE_COUNT"]);

				oCast.AppendChild(oHandleNm);
				oCast.AppendChild(oAge);
				oCast.AppendChild(oPhotoImgPath);
				oCast.AppendChild(oVoteRank);
				oCast.AppendChild(oVoteCount);
			}
		}

		Response.ContentType = "text/xml";
		Response.Write(oDocument.OuterXml);
	}
}
