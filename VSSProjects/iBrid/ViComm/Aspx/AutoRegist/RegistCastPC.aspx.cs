﻿using System;
using System.Threading;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class RegistCastPC:System.Web.UI.Page {
	private const string RESULT_OK = "0";
	private const string RESULT_PARM_ERR = "1";
	private const string RESULT_INPUT_ERR = "2";
	private const string RESULT_SP_ERR = "3";

	private string sSiteCd;
	private string sTel;
	private string sCastNm;
	private string sCastKanaNm;
	private string sBirthdayYear;
	private string sBirthdayMonth;
	private string sBirthdayDay;
	private string sPrefectureCd;
	private string sSex;
	private string sWishCastType;
	private string sEmailAddr;
	private string sEmailAddrConf;
	private string sGuardianNm;
	private string sGuardianTel;
	private string sAdCd;
	private int iNeedGuardianAge = 0;
	private string sErrorMessage;

	protected void Page_Load(object sender,EventArgs e) {
		if (!SetRequest()) {
			ReturnResponse(RESULT_PARM_ERR);
			return;
		}

		SetDefault();

		if (!CheckInput()) {
			ReturnResponse(RESULT_INPUT_ERR);
			return;
		}

		if (!ExecRegist()) {
			ReturnResponse(RESULT_SP_ERR);
			return;
		}

		ReturnResponse(RESULT_OK);
	}

	private bool ExecRegist() {
		using (TempRegist oTempRegist = new TempRegist()) {
			string sTempRegistId;
			string sResultRegist;
			string sResultMail;

			oTempRegist.RegistUserWomanTemp(
				sSiteCd,
				sAdCd,
				string.Empty,			//pAffiliaterCd
				string.Empty,			//pRegistAffiliateCd
				string.Empty,			//pMobileCarrierCd
				string.Empty,			//pTerminalUniqueId
				string.Empty,			//pIModeId
				string.Empty,			//pRegistIpAddr
				sTel,
				string.Empty,			//pLoginPassword
				sCastNm,
				sCastKanaNm,
				string.Format("{0}{1}{2}",sBirthdayYear,sBirthdayMonth,sBirthdayDay),
				string.Empty,			//pMobileTerminalNm
				string.Empty,			//pIntroducerFriendCd
				string.Empty,			//pQuestionDoc
				string.Empty,			//pHandleNm
				ViCommConst.FLAG_OFF,	//pGameRegistFlag
				out sTempRegistId,
				out sResultRegist,
				ViCommConst.FLAG_ON,	//pIdolRegistFlag
				sPrefectureCd,
				string.Empty,			//pAddress1
				sGuardianNm,
				sGuardianTel,
				sSex,					//pIdolSexAttrSeq
				sEmailAddr,
				sWishCastType			//pIdolWishCastTypeAttrSeq
			);

			if (!sResultRegist.Equals(PwViCommConst.SimpleSPResult.RESULT_OK) || string.IsNullOrEmpty(sTempRegistId)) {
				return false;
			}

			oTempRegist.TxApplyRegistCastPCMail(sTempRegistId,out sResultMail);

			if (!sResultMail.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				return false;
			}
		}

		return true;
	}

	private bool SetRequest() {
		try {
			sSiteCd = iBridUtil.GetStringValue(Request.Form["site_cd"]);
			sTel = iBridUtil.GetStringValue(Request.Form["tel"]);
			sCastNm = iBridUtil.GetStringValue(Request.Form["cast_nm"]);
			sCastKanaNm = iBridUtil.GetStringValue(Request.Form["cast_kana_nm"]);
			sBirthdayYear = iBridUtil.GetStringValue(Request.Form["birthday_year"]);
			sBirthdayMonth = iBridUtil.GetStringValue(Request.Form["birthday_month"]);
			sBirthdayDay = iBridUtil.GetStringValue(Request.Form["birthday_day"]);
			sPrefectureCd = iBridUtil.GetStringValue(Request.Form["prefecture_cd"]);
			sSex = iBridUtil.GetStringValue(Request.Form["sex"]);
			sEmailAddr = iBridUtil.GetStringValue(Request.Form["email_addr"]);
			sEmailAddrConf = iBridUtil.GetStringValue(Request.Form["email_addr_conf"]);
			sGuardianNm = iBridUtil.GetStringValue(Request.Form["guardian_nm"]);
			sGuardianTel = iBridUtil.GetStringValue(Request.Form["guardian_tel"]);
			sAdCd = iBridUtil.GetStringValue(Request.Form["ad_cd"]);
			sWishCastType = iBridUtil.GetStringValue(Request.Form["wish_cast_type"]);
			int.TryParse(iBridUtil.GetStringValue(Request.Form["need_guardian_age"]),out iNeedGuardianAge);

			return true;
		} catch (Exception) {
			return false;
		}
	}

	private void SetDefault() {
		if (string.IsNullOrEmpty(sSiteCd)) {
			sSiteCd = "A001";
		}

		if (iNeedGuardianAge == 0) {
			iNeedGuardianAge = 18;
		}

		if (!string.IsNullOrEmpty(sAdCd)) {
			using (Ad oAd = new Ad()) {
				string sAdNm;
				if (!oAd.IsExist(sAdCd,out sAdNm)) {
					sAdCd = string.Empty;
				}
			}
		}
	}

	private void ReturnResponse(string sResult) {
		Response.ContentType = "text/plain";
		Response.Write(string.Format("result={0}&err={1}",sResult,sErrorMessage));
	}

	private bool CheckInput() {
		bool bOk = true;
		int iAge = 0;
		sErrorMessage = string.Empty;

		if (string.IsNullOrEmpty(sCastNm)) {
			bOk = false;
			sErrorMessage += "本名（漢字）を入力して下さい。<br>";
		} else if (Encoding.GetEncoding(932).GetByteCount(sCastNm) > 24) {
			bOk = false;
			sErrorMessage += "本名（漢字）が長すぎます。<br>";
		}

		if (string.IsNullOrEmpty(sCastKanaNm)) {
			bOk = false;
			sErrorMessage += "本名（カナ）を入力して下さい。<br>";
		} else if (Encoding.GetEncoding(932).GetByteCount(sCastKanaNm) > 30) {
			bOk = false;
			sErrorMessage += "本名（カナ）が長すぎます。<br>";
		}

		if (string.IsNullOrEmpty(sTel)) {
			bOk = false;
			sErrorMessage += "携帯電話番号を入力して下さい。<br>";
		} else if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",sTel)) {
			bOk = false;
			sErrorMessage += "携帯電話番号が正しくありません。<br>";
		} else {
			using (User oUser = new User()) {
				if (oUser.IsBlackUser(ViCommConst.NO_TYPE_TEL,sTel)) {
					bOk = false;
					sErrorMessage += "大変申し訳ございませんが、こちらの携帯電話番号ではご利用頂く事が出来ません。<br>";
				} else if (oUser.IsTelephoneRegistered(sTel)) {
					bOk = false;
					sErrorMessage += "こちらの携帯電話番号は既に登録されています。<br>";
				}
			}
		}

		if (string.IsNullOrEmpty(sBirthdayYear) || string.IsNullOrEmpty(sBirthdayMonth) || string.IsNullOrEmpty(sBirthdayDay)) {
			bOk = false;
			sErrorMessage += "生年月日を入力してさい。<br>";
		} else {
			string sDate = sBirthdayYear + "/" + sBirthdayMonth + "/" + sBirthdayDay;
			iAge = ViCommPrograms.Age(sDate);

			if (iAge > 99 || iAge == 0) {
				bOk = false;
				sErrorMessage += "生年月日を正しく入力して下さい。<br>";
			} else if (iAge < 10) {
				bOk = false;
				sErrorMessage += "10才未満の方は登録できません。<br>";
			}
		}

		if (string.IsNullOrEmpty(sPrefectureCd)) {
			bOk = false;
			sErrorMessage += "現住所(都道府県)を選択してください。<br>";
		}

		if (string.IsNullOrEmpty(sSex)) {
			bOk = false;
			sErrorMessage += "性別を選択してください。<br>";
		}

		if (string.IsNullOrEmpty(sEmailAddr) || string.IsNullOrEmpty(sEmailAddrConf)) {
			bOk = false;
			sErrorMessage += "メールアドレスを入力してください。<br>";
		} else if (!sEmailAddr.Equals(sEmailAddrConf)) {
			bOk = false;
			sErrorMessage += "メールアドレスが正しくありません。<br>";
		} else {
			if (!Regex.IsMatch(sEmailAddr,@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$",RegexOptions.IgnoreCase)) {
				bOk = false;
				sErrorMessage += "メールアドレスが正しくありません。<br>";
			} else {
				using (User oUser = new User()) {
					int iExistFlag;
					oUser.CheckCastEmailAddrExist(sSiteCd,sEmailAddr,out iExistFlag);
					if (iExistFlag.Equals(ViCommConst.FLAG_ON)) {
						bOk = false;
						sErrorMessage += "こちらのメールアドレスは既に登録されています。<br>";
					}
				}
			}
		}

		if (iAge != 0 && iAge < iNeedGuardianAge) {
			if (string.IsNullOrEmpty(sGuardianNm)) {
				bOk = false;
				sErrorMessage += "保護者氏名を入力して下さい。<br>";
			}

			if (string.IsNullOrEmpty(sGuardianTel)) {
				bOk = false;
				sErrorMessage += "保護者の携帯番号を入力して下さい。<br>";
			}
		}

		if (!string.IsNullOrEmpty(sGuardianTel) && !SysPrograms.Expression(@"0(7|8|9)0\d{8}",sGuardianTel)) {
			bOk = false;
			sErrorMessage += "保護者の携帯番号が正しくありません。<br>";
		}

		return bOk;
	}
}
