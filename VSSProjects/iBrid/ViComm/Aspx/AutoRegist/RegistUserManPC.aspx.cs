﻿using System;
using System.Threading;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class RegistUserManPC:System.Web.UI.Page {
	private const string RESULT_OK = "0";
	private const string RESULT_PARM_ERR = "1";
	private const string RESULT_INPUT_ERR = "2";
	private const string RESULT_SP_ERR = "3";

	private string sSiteCd;
	private string sHandleNm;
	private string sBirthdayYear;
	private string sBirthdayMonth;
	private string sBirthdayDay;
	private string sPrefectureCd;
	private string sEmailAddr;
	private string sEmailAddrConf;
	private string sAdCd;
	private string sErrorMessage;

	protected void Page_Load(object sender,EventArgs e) {
		if (!SetRequest()) {
			ReturnResponse(RESULT_PARM_ERR);
			return;
		}

		SetDefault();

		if (!CheckInput()) {
			ReturnResponse(RESULT_INPUT_ERR);
			return;
		}

		if (!ExecRegist()) {
			ReturnResponse(RESULT_SP_ERR);
			return;
		}

		ReturnResponse(RESULT_OK);
	}

	private bool ExecRegist() {
		string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
		int iCtrl = 0;
		
		using (UserMan oUserMan = new UserMan()) {
			oUserMan.GetAttrType(sSiteCd);
			for (int i = 0;i < oUserMan.attrList.Count;i++) {
				if (oUserMan.attrList[i].attrTypeNm.Equals("地域")) {
					pAttrTypeSeq[iCtrl] = oUserMan.attrList[i].attrTypeSeq;
					using (UserManAttrTypeValue oUserManAttrTypeValue = new UserManAttrTypeValue()) {
						pAttrSeq[iCtrl] = oUserManAttrTypeValue.GetAttrSeqByItemCd(sSiteCd,oUserMan.attrList[i].attrTypeSeq,sPrefectureCd);
					}
				}
			}
		}
		
		using (TempRegist oTempRegist = new TempRegist()) {
			string sTempRegistId;
			string sResultRegist;
			string sResultMail;

			oTempRegist.RegistUserManTemp(
				sSiteCd,
				sAdCd,
				string.Empty,			//pAffiliaterCd
				string.Empty,			//pRegistAffiliateCd
				string.Empty,			//pMobileCarrierCd
				string.Empty,			//pTerminalUniqueId
				string.Empty,			//pIModeId
				string.Empty,			//pRegistIpAddr
				string.Empty,			//pTel
				string.Empty,			//pLoginPassword
				sHandleNm,				//pHandleNm
				string.Format("{0}/{1}/{2}",sBirthdayYear,sBirthdayMonth,sBirthdayDay),
				string.Empty,			//pMobileTerminalNm
				string.Empty,			//pIntroducerFriendCd
				string.Empty,			//pPrepaidId
				string.Empty,			//pPrepaidPw
				string.Empty,			//pOpenId
				string.Empty,			//pOpenIdType
				pAttrTypeSeq,			//pAttrTypeSeq
				pAttrSeq,				//pAttrSeq
				pAttrInputValue,		//pAttrInputValue
				1,						//pMaxAttrCount
				new string[] {sSiteCd},	//pRegistSiteCd
				ViCommConst.FLAG_OFF,	//pGameRegistFlag
				out sTempRegistId,
				out sResultRegist,
				sEmailAddr
			);

			if (!sResultRegist.Equals(PwViCommConst.SimpleSPResult.RESULT_OK) || string.IsNullOrEmpty(sTempRegistId)) {
				return false;
			}

			oTempRegist.TxApplyRegistUserPCMail(sTempRegistId,out sResultMail);

			if (!sResultMail.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				return false;
			}
		}

		return true;
	}

	private bool SetRequest() {
		try {
			sSiteCd = iBridUtil.GetStringValue(Request.Form["site_cd"]);
			sHandleNm = iBridUtil.GetStringValue(Request.Form["handle_nm"]);
			sBirthdayYear = iBridUtil.GetStringValue(Request.Form["birthday_year"]);
			sBirthdayMonth = iBridUtil.GetStringValue(Request.Form["birthday_month"]);
			sBirthdayDay = iBridUtil.GetStringValue(Request.Form["birthday_day"]);
			sPrefectureCd = iBridUtil.GetStringValue(Request.Form["prefecture_cd"]);
			sEmailAddr = iBridUtil.GetStringValue(Request.Form["email_addr"]);
			sEmailAddrConf = iBridUtil.GetStringValue(Request.Form["email_addr_conf"]);
			sAdCd = iBridUtil.GetStringValue(Request.Form["ad_cd"]);

			return true;
		} catch (Exception) {
			return false;
		}
	}

	private void SetDefault() {
		if (string.IsNullOrEmpty(sSiteCd)) {
			sSiteCd = "A001";
		}

		if (!string.IsNullOrEmpty(sAdCd)) {
			using (Ad oAd = new Ad()) {
				string sAdNm;
				if (!oAd.IsExist(sAdCd,out sAdNm)) {
					sAdCd = string.Empty;
				}
			}
		}
	}

	private void ReturnResponse(string sResult) {
		Response.ContentType = "text/plain";
		Response.Write(string.Format("result={0}&err={1}",sResult,sErrorMessage));
	}

	private bool CheckInput() {
		bool bOk = true;
		int iAge = 0;
		string sNGWord = string.Empty;
		sErrorMessage = string.Empty;

		using (NGWord oNGWord = new NGWord(sSiteCd)) {
			if (string.IsNullOrEmpty(sHandleNm)) {
				bOk = false;
				sErrorMessage += "ハンドル名を入力して下さい。<br>";
			} else if (sHandleNm.Length > 20) {
				bOk = false;
				sErrorMessage += "ハンドル名が長すぎます。<br>";
			} else if (oNGWord.VaidateDoc(sHandleNm,out sNGWord) == false) {
				bOk = false;
				sErrorMessage += "※禁止語句が含まれています。";
			}
		}

		if (string.IsNullOrEmpty(sBirthdayYear) || string.IsNullOrEmpty(sBirthdayMonth) || string.IsNullOrEmpty(sBirthdayDay)) {
			bOk = false;
			sErrorMessage += "生年月日を入力してさい。<br>";
		} else {
			string sDate = sBirthdayYear + "/" + sBirthdayMonth + "/" + sBirthdayDay;
			iAge = ViCommPrograms.Age(sDate);

			if (iAge > 99 || iAge == 0) {
				bOk = false;
				sErrorMessage += "生年月日を正しく入力して下さい。<br>";
			} else if (iAge < 18) {
				bOk = false;
				sErrorMessage += "18才未満の方は登録できません。<br>";
			}
		}

		if (string.IsNullOrEmpty(sPrefectureCd)) {
			bOk = false;
			sErrorMessage += "現住所(都道府県)を選択してください。<br>";
		}

		if (string.IsNullOrEmpty(sEmailAddr) || string.IsNullOrEmpty(sEmailAddrConf)) {
			bOk = false;
			sErrorMessage += "メールアドレスを入力してください。<br>";
		} else if (!sEmailAddr.Equals(sEmailAddrConf)) {
			bOk = false;
			sErrorMessage += "メールアドレスが正しくありません。<br>";
		} else {
			if (!Regex.IsMatch(sEmailAddr,@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$",RegexOptions.IgnoreCase)) {
				bOk = false;
				sErrorMessage += "メールアドレスが正しくありません。<br>";
			} else {
				using (User oUser = new User()) {
					int iExistFlag;
					oUser.CheckCastEmailAddrExist(sSiteCd,sEmailAddr,out iExistFlag);
					if (iExistFlag.Equals(ViCommConst.FLAG_ON)) {
						bOk = false;
						sErrorMessage += "こちらのメールアドレスは既に登録されています。<br>";
					}
				}
			}
		}

		return bOk;
	}
}
