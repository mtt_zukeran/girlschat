using System;
using System.Web;
using iBridCommLib;


namespace Redirect {
	public class UrlRedirectModule:IHttpModule {
		public void Init(HttpApplication application) {
			application.BeginRequest += new EventHandler
									 (this.OnBeginRequest);
		}

		private void OnBeginRequest(Object source,EventArgs e) {
			HttpApplication application = (HttpApplication)source;

			string sRedirect = "";

			string sEShotId = iBridUtil.GetStringValue(application.Request.QueryString["IDCODE"]);
			string sEShotPw = iBridUtil.GetStringValue(application.Request.QueryString["PASS"]);

			string sCliphoId = iBridUtil.GetStringValue(application.Request.QueryString["loginid"]);
			string sCliphoPw = iBridUtil.GetStringValue(application.Request.QueryString["loginpass"]);

			if ((sEShotId.Equals("") == false) && (sEShotPw.Equals("") == false)) {
				sRedirect = string.Format("http://{0}/user/start.aspx?host={1}&uid={2}&pw={3}",
							application.Request.Url.Host,"e-0721.com",sEShotId,sEShotPw
							);
				application.Response.Redirect(sRedirect);
			}

			if ((sCliphoId.Equals("") == false) && (sCliphoPw.Equals("") == false)) {
				sRedirect = string.Format("http://{0}/user/start.aspx?host={1}&uid={2}&pw={3}",
							application.Request.Url.Host,"nowmo.biz",sCliphoId,sCliphoPw
							);
				application.Response.Redirect(sRedirect);
			}

			sRedirect = string.Format("http://{0}/user/start.aspx?{1}",
						application.Request.Url.Host,
						application.Request.QueryString.ToString()
						);

			string[] sUrl = iBridUtil.GetStringValue(application.Request.QueryString["url"]).Split(' ');
			string sAdCd = "";


			if (sUrl.Length > 0) {
				sAdCd = sUrl[0].Replace("/","");
				if (!sAdCd.Equals("")) {
					if (sAdCd.IndexOf(".") == -1) {
						sRedirect = sRedirect + "&adcd=" + sAdCd;
					}
				}
			}

			application.Response.Redirect(sRedirect);
		}

		public void Dispose() {
		}
	}
}
