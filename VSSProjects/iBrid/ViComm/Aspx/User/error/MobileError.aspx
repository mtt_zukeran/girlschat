<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MobileError.aspx.cs" Inherits="error_MobileError" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="Form1" Runat="server">
		<cc1:iBMobileLabel ID="lblTitle" runat="server" Alignment="Center" ForeColor="Red" StyleReference="title"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagHR01" runat="server" Text='<hr color="#C71585" />' />
		<cc1:iBMobileLabel ID="lblErrorMsg1" runat="server" StyleReference="error"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblErrorMsg2" runat="server" StyleReference="error"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="ltlDoc" runat="server" />
		<br />
		<mobile:Command ID="lnkLogin" Runat="server" Format="Link" OnClick="lnkLogin_Click">再ログイン</mobile:Command>
	</mobile:Form>
</body>
</html>
