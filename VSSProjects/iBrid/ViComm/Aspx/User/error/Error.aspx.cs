﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: Error
--
--  Creation Date	: 2011.01.31
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Diagnostics;
using iBridCommLib;
using ViComm;

public partial class error_Error : MobilePage {
	protected void Page_Load(object sender, EventArgs e) {
		try {
			if(SessionObjs.Current != null){
				if (SessionObjs.Current is SessionMan) {
					this.Response.Filter = ((SessionMan)SessionObjs.Current).InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
				} else {
					this.Response.Filter = ((SessionWoman)SessionObjs.Current).InitScreen(this.Response.Filter,this,this.Request,this.ViewState,this.IsPostBack);
				}			
			}else{
				Response.Redirect(string.Format("http://{0}:{1}",Request.Url.Host,Request.Url.Port),false);
			}
		} catch (Exception ex) {
			string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
			string sSource = "ViCOMM";
			string sLog = "Application";
			string err = string.Empty;

			Exception oException = ex.GetBaseException();
			err = string.Format("ViCOMM User Error Caught in Error Page\nError in: {0}\nError Message: {1}\nStack Trace: {2}",
				Request.Url.ToString(),
				oException.Message.ToString(),
				oException.StackTrace.ToString());


			if (!EventLog.SourceExists(sSource)) {
				EventLog.CreateEventSource(sSource, sLog);
			}

			EventLog.WriteEntry(sSource, err, EventLogEntryType.Error);
			Server.ClearError();

			Response.Redirect(string.Format("http://{0}:{1}", Request.Url.Host, Request.Url.Port));
		}
	}
}
