using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using iBridCommLib;
using ViComm;

public partial class error_MobileError:System.Web.UI.MobileControls.MobilePage {

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			string sMode = Request.QueryString["mode"];
			int iErrtype = int.Parse(Request.QueryString["errtype"]);

			ViewState["ERROR_TYPE"] = iErrtype.ToString();
			ViewState["MODEL"] = sMode;
			Site oSite = GetSite();
			if ((oSite == null) && (iErrtype != ViCommConst.USER_INFO_ILLIGAL_ACCESS)) {
				Response.StatusCode = 404;
				Response.End();
				return;
			}

			string sUrl;

			if (oSite.useOtherSysInfoFlag == 0) {
				try {
//					sUrl = string.Format("http://{0}/User/Start.aspx?host={1}",oSite.hostNm,oSite.hostNm);
					sUrl = string.Format("http://{0}/User/Start.aspx?host={1}",oSite.hostNm,oSite.hostNm);
					Response.Redirect(sUrl);
				} catch (Exception ex) {
					ViCommInterface.WriteIFError(ex,"MobileError","");
					Response.StatusCode = 404;
					Response.End();
					return;
				}
			} else {
				switch (iErrtype) {
					case ViCommConst.USER_INFO_TIMEOUT:
						lblTitle.Text = "セッションタイムアウト";
						lnkLogin.Text = oSite.parentWebNm + "へ戻る。";
						break;

					case ViCommConst.USER_INFO_INTERNAL_ERROR:
						lblTitle.Text = "内部エラー";
						lblErrorMsg1.Text = "ご迷惑をおかけ致します。";
						lblErrorMsg2.Text = "再度ログインして下さい。";
						lnkLogin.Text = oSite.parentWebNm + "へ戻る。";
						break;

					case ViCommConst.USER_INFO_UNAUTH_TEL:
						lblTitle.Text = "電話番号未認証";
						lnkLogin.Text = "会員メニューへ戻る。";
						break;

					case ViCommConst.USER_INFO_NO_BAL:
						lblTitle.Text = "ポイントなし";
						this.lnkLogin.Text = "会員メニューへ戻る。";
						break;

					case ViCommConst.USER_INFO_ILLIGAL_ACCESS:
						lblTitle.Text = "セッションエラー";
						lblErrorMsg1.Text = "アクセスが不正です。";
						lblErrorMsg2.Text = "";
						this.lnkLogin.Text = "";
						lnkLogin.Visible = false;
						break;

					case ViCommConst.USER_INFO_NG:
					default:
						lblTitle.Text = "セッションエラー";
						lnkLogin.Text = oSite.parentWebNm + "へ戻る。";
						break;

				}
			}
		}
	}

	private Site GetSite() {
		try {
			Site oSite = new Site();
			string sHost = Page.Request.Url.Host;
			if (oSite.GetOneByHost(sHost) == false) {
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestEnv"]).Equals("1")) {
					if (oSite.GetOneByHost(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestLocation"])) == false) {
						return null;
					}
				}
			}
			return oSite;
		} catch {
			return null;
		}
	}

	protected void lnkLogin_Click(object sender,EventArgs e) {
		SessionObjs userObjs = (SessionObjs)Session["objs"];

		int iErrType = int.Parse(ViewState["ERROR_TYPE"].ToString());
		string sMode = ViewState["MODEL"].ToString();
		string sUrl = "";

		switch (iErrType) {
			case ViCommConst.USER_INFO_TIMEOUT:
			case ViCommConst.USER_INFO_INTERNAL_ERROR:
				sUrl = userObjs.site.parentWebTopPageUrl;
				break;

			case ViCommConst.USER_INFO_UNAUTH_TEL:
			case ViCommConst.USER_INFO_NO_BAL:
				sUrl = userObjs.site.parentWebTopPageUrl;
				if (userObjs.sexCd.Equals(ViCommConst.MAN)) {
					SessionMan oSessionMan = (SessionMan)userObjs;
					sUrl = string.Format(userObjs.site.parentWebLoginUrl,oSessionMan.userMan.loginId,oSessionMan.userMan.loginPassword);
				}
				break;

			case ViCommConst.USER_INFO_NG:
			default:
				sUrl = userObjs.site.parentWebTopPageUrl;
				break;
		}
		Session.RemoveAll();
		RedirectToMobilePage(sUrl);
	}

}
