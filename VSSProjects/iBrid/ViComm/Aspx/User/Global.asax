﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="MobileLib" %>
<%@ Import Namespace="ViComm" %>

<script RunAt="server">

	
	void Application_Start(object sender,EventArgs e) {
		DateTime dt = DateTime.Parse("2008/06/11 14:00:28");
		SessionEndModule.SessionObjectKey = "SessionId";
		SessionEndModule.SessionEnd += new SessionEndEventHandler(SessionTimoutModule_SessionEnd);
		SessionEndModule.SessionEntry += new SessionEntryEventHandler(SessionEndModule_SessionEntry);

		using (WebSession oWebSession = new WebSession()) {
			oWebSession.StartUpSystem(Environment.MachineName);
		}

		iBridCommLib.HtmlFilter.DelayParseEmoji = true;
	}

	private void Application_BeginRequest(object sender,EventArgs e) {
		if (this.IsAspx()) {
			PageRewriteHelper.Initialize();
			PageRewriteHelper.Rewrite(HttpContext.Current);
		}
	}
	private void Application_PreRequestHandlerExecute(object sender,EventArgs e) {
		if (this.IsAspx()) {
			PageRewriteHelper.Reset(HttpContext.Current);


			string enableSessionCheck = ConfigurationManager.AppSettings["enableSessionCheck"];
			
			if(!string.IsNullOrEmpty(enableSessionCheck) && enableSessionCheck.Equals("1")){

                // セッション重複チェック対象外のaspxかどうか判定

                bool isIgnore = false;
                string ignoreSessionCheckAspxList = ConfigurationManager.AppSettings["ignoreSessionCheckAspx"];
                if (!string.IsNullOrEmpty(ignoreSessionCheckAspxList))
                {
                    foreach (string ignoreSessionCheckAspx in ignoreSessionCheckAspxList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (HttpContext.Current.Request.Path.Contains(ignoreSessionCheckAspx))
                        {
                            // 本aspxはセッション重複チェック対象外である
                            isIgnore = true;
                            break;
                        }
                    }
                }

                if (!isIgnore)
                {
                    if (SessionObjs.Current != null && !string.IsNullOrEmpty(SessionObjs.Current.mobileUserAgent)) {
					    if (!SessionObjs.Current.mobileUserAgent.Equals(HttpContext.Current.Request.UserAgent)) {

						    string ignoreSessionCheckUAList = ConfigurationManager.AppSettings["ignoreSessionCheckUA"];
						    if (!string.IsNullOrEmpty(ignoreSessionCheckUAList)) {
							    foreach (string ignoreSessionCheckUA in ignoreSessionCheckUAList.Split(new string[] { "," },StringSplitOptions.RemoveEmptyEntries)) {
								    if (HttpContext.Current.Request.UserAgent.Contains(ignoreSessionCheckUA)) {
									    isIgnore = true;
									    break;
								    }
							    }
						    }

							if (!string.IsNullOrEmpty(SessionObjs.Current.carrier)) {
								if (Mobile.IsFeaturePhone(SessionObjs.Current.carrier)) {
									if (!string.IsNullOrEmpty(SessionObjs.Current.modelName)) {
										string sAccessCarrier = Mobile.GetCarrier(HttpContext.Current.Request.Browser.Browser);
										string sAccessModelName;
										using (ModelDiscriminant modelDiscriminant = new ModelDiscriminant()) {
											sAccessModelName = modelDiscriminant.GetModelName(sAccessCarrier,HttpContext.Current.Request.UserAgent);
										}

										if (SessionObjs.Current.carrier.Equals(sAccessCarrier) && SessionObjs.Current.modelName.Equals(sAccessModelName)) {
											isIgnore = true;
										}
									}
								}
							}

						    if (!isIgnore) {
							    this.Session.Abandon();
							    return;
						    }
                        }
				    }
			    }
            }

			if (SessionObjs.Current != null) {
				SessionObjs.Current.InitRandomDictionary();
			}
		}
	}

	void Application_End(object sender,EventArgs e) {
	}


	void Application_Error(object sender,EventArgs e) {
		Exception objLastErr = HttpContext.Current.Server.GetLastError();

		string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
		string sSource = "ViCOMM";
		string sLog = "Application";
		string err = "";

		if (objLastErr != null) {
			Exception objErr = objLastErr.GetBaseException();
			err = "ViCOMM User Error Caught in Application_Error event\n" +
				 "Error in: " + Request.Url.ToString() +
				 "\nError Message:" + objErr.Message.ToString() +
				 "\nStack Trace:" + objErr.StackTrace.ToString();
		} else {
			err = "NO ASPX ERROR";
		}

		if (!EventLog.SourceExists(sSource)) {
			EventLog.CreateEventSource(sSource,sLog);
		}

		EventLog.WriteEntry(sSource,err,EventLogEntryType.Error);
		Server.ClearError();

		if (ViCommConst.FLAG_ON_STR.Equals(ConfigurationManager.AppSettings["enabledErrorPage"])) {
			Response.Redirect("~/error/Error.aspx");
		} else {
			Response.Redirect(string.Format("http://{0}:{1}",Request.Url.Host,Request.Url.Port));
		}
	}

	void Session_Start(object sender,EventArgs e) {
		Session["SessionId"] = Session.SessionID;
		string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
		string sUrl = Request.Url.ToString().ToLower();
		string sSessionTimeOutId = ConfigurationManager.AppSettings["SessionTimeOutId"];
		bool bIsCrawler,bPassIP;
		bool bIsCashAccess = iBridUtil.GetStringValue(Request.QueryString["cashAccess"]).Equals(ViCommConst.FLAG_ON_STR);
		int iPcRedirectFlag = 0;

		using (AuthIP oAuthIP = new AuthIP()) {
			bPassIP = oAuthIP.IsExist(Request.UserHostAddress,Request.Url.Host,Request.UserAgent,Request.QueryString,out iPcRedirectFlag,out bIsCrawler);
		}

		if (sUrl.IndexOf(sRoot) > 0) {
			if (((!bIsCrawler) || (iPcRedirectFlag != 0)) && (!bIsCashAccess)) {
				if ((sUrl.IndexOf("start.aspx") < 0) && (sUrl.IndexOf("mobileerror.aspx") < 0)) {
					if (Session["objs"] == null) {
						if (sRoot.Equals(string.Empty)) {
							Session.Abandon();
							Response.Redirect(string.Format("http://{0}/user/start.aspx?{1}",Request.Url.Host,Request.QueryString.ToString()));
						} else {
							string sSexCd;
							string[] sCallProgram = Request.Path.Split('/');
							if (Request.FilePath.ToLower().Contains("/woman/")) {
								sSexCd = ViCommConst.OPERATOR;
							} else {
								sSexCd = ViCommConst.MAN;
							}

							string sBijoColleFlag = ViCommConst.FLAG_OFF_STR;
							Regex rgx = new Regex("^(http|https)://(game|bijo)[.]");
							Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(Request.Url));
							if (rgxMatch.Success) {
								sBijoColleFlag = ViCommConst.FLAG_ON_STR;
							}
							
							UrlBuilder oUrl = new UrlBuilder(string.Format("http://{0}:{1}/user/start.aspx",Request.Url.Host,Request.Url.Port),Request.QueryString);
							oUrl.Parameters.Remove("timeout");
							oUrl.Parameters.Remove("urlsex");
							oUrl.Parameters.Remove("call");
							oUrl.Parameters.Remove("bijo");
							oUrl.Parameters.Add("timeout","1");
							oUrl.Parameters.Add("urlsex",sSexCd);
							oUrl.Parameters.Add("call",sCallProgram[sCallProgram.Length - 1]);
							oUrl.Parameters.Add("bijo",sBijoColleFlag);
							Session.Abandon();
							Response.Redirect(oUrl.ToString());
						}
					}
				} else {
					using (WebSession oWebSession = new WebSession()) {
						oWebSession.StartSession(Session.SessionID,Environment.MachineName);
					}

				}
			} else {
				if (sUrl.IndexOf("start.aspx") < 0) {
					string sHost = Request.Url.Host;
					string sTestEnv = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestEnv"]);
					string sTestDevice = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestDevice"]);
					string sCarrierType = Mobile.GetCarrier(HttpContext.Current.Request.Browser.Browser);
					string sBrowser = HttpContext.Current.Request.Browser.Id;
					string sRedirect = "";

					if (sTestEnv.Equals("1")) {
						sHost = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestLocation"]);
					}

					if (sTestDevice.Equals("1")) {
						sCarrierType = ConfigurationManager.AppSettings["TestCarrier"];
					}

					using (Site oSite = new Site()) {
						if (oSite.GetOneByHost(sHost)) {
							if (iPcRedirectFlag != 0) {
								sRedirect = oSite.PcRedirectUrl;
							} else if (sCarrierType.Equals(ViCommConst.CARRIER_OTHERS) && (bPassIP == false)) {
								sRedirect = oSite.PcRedirectUrl;
							}
						} else {
							throw new ApplicationException("Host name not found " + sHost);
						}

						if (!sRedirect.Equals("")) {
							Response.Redirect(sRedirect);
						}
					}
					SessionObjs oSessionBasic;
					SessionHelper oHelper = new SessionHelper(SessionObjs.CurrentSexCd,sCarrierType,sBrowser,string.Empty,string.Empty);
					oHelper.CreateSessionInfo(sHost,Session.SessionID,bIsCrawler,bIsCrawler,out oSessionBasic);
					oSessionBasic.currentIModeId = Mobile.GetiModeId(sCarrierType,Request);

					string sRedirectUrl = string.Format("http://{0}",Request.Url.Host) + oSessionBasic.GetNavigateUrl(Request.Url.Segments[Request.Url.Segments.Length - 1].ToString() + Request.Url.Query.ToString());
					
					if (!bIsCrawler) {
						Response.Redirect(sRedirectUrl);
					}
				}
			}
		}
	}

	void Session_End(object sender,EventArgs e) {
		using (WebSession oWebSession = new WebSession()) {
			oWebSession.EndSession(Session.SessionID);
		}
	}
	private static void SessionTimoutModule_SessionEnd(object sender,SessionEndedEventArgs e) {
		using (WebSession oWebSession = new WebSession()) {
			oWebSession.EndSession(e.SessionId);
		}
	}

	void SessionEndModule_SessionEntry(object sender,SessionEntryEventArgs e) {
		try {
			using (WebSession oWebSession = new WebSession()) {
				oWebSession.VoteSession(e.SessionId);
			}
		} catch {
		}
	}

	private bool IsAspx() {
		return HttpContext.Current.Request.Path.Contains(".aspx");
	}

	void Application_OnEndRequest(object sender,EventArgs e) {
		if (!iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableStartPagePost2Get"]).Equals(ViCommConst.FLAG_ON_STR)) {
			return;
		}
		string sUrl = Request.Url.ToString().ToLower();
		if (sUrl.Contains("start.aspx")) {
			bool bIsSessionAcquired = HttpContext.Current.Items.Contains("AspCookielessSession");
			if (!bIsSessionAcquired && this.Request.HttpMethod.Equals("POST")) {
				if (this.Request.Form.Count > 0) {
					StringBuilder oUrlBuilder = new StringBuilder(this.Request.Url.PathAndQuery);
					oUrlBuilder.Append((!this.Request.Url.PathAndQuery.Contains("?")) ? "?" : "&");
					for (int i = 0;i < this.Request.Form.Count;i++) {
						string sKey = this.Request.Form.AllKeys[i];
						string sValue = this.Request.Form[i];
						oUrlBuilder.AppendFormat("{0}={1}",HttpUtility.UrlEncode(sKey),HttpUtility.UrlEncode(sValue));

						if ((i + 1) < this.Request.Form.Count) {
							oUrlBuilder.Append("&");
						}
					}

					Response.Redirect(oUrlBuilder.ToString());
				}
			}
		}
	}
	
</script>

