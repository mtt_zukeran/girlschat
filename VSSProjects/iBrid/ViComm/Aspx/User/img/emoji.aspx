﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="emoji.aspx.cs" Inherits="img_Emoji" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <div id="<%# EmojiBehavior.EMOJI_CONTAINER_ID %>">
		<asp:Repeater ID="EmojiContainer" runat="server" >
			<ItemTemplate>
				<div class="<%# EmojiBehavior.EMOJI_ITEM_CLASS %>" id="<%# HttpUtility.UrlEncode( DataBinder.Eval(Container.DataItem, "Text").ToString()) %>">
					<div style="background-image:url(/pictograph/<%# DataBinder.Eval(Container.DataItem,"CommonCode") %>.gif);" 
						class="<%# EmojiBehavior.EMOJI_ITEM_INNER_CLASS %>">
					</div>
				</div>
			</ItemTemplate>
		</asp:Repeater>
    </div>
</body>
</html>
