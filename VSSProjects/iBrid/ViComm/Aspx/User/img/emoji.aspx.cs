﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using ViComm;

public partial class img_Emoji : System.Web.UI.Page
{
	/// <summary>
	/// Page Load Event
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void Page_Load(object sender,EventArgs e) 
	{
		try
		{
			NameValueCollection gets = Page.Request.QueryString;
			string type = NameValueCollectionUtil.GetValue(gets, EmojiBehavior.EMOJI_TYPE, ViCommConst.DOCOMO);
			int limit = NameValueCollectionUtil.GetValue(gets, EmojiBehavior.LIMIT, 30);
			int offset = NameValueCollectionUtil.GetValue(gets, EmojiBehavior.OFFSET, 1);

			this.EmojiContainer.DataSource = EmojiDAL.GetData(type, limit, offset);
			this.DataBind();
		}
		catch(Exception ex)
		{
			string msg = ex.Message;
			throw;
		}
	}
}

