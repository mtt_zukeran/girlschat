/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 開始処理
--	Progaram ID		: Start
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Rakuten;
using ViComm.Extension.Pwild;

public partial class User_Start:System.Web.UI.MobileControls.MobilePage {
	private UserAuth rakutenUserAuth = new UserAuth();
	SessionObjs oSessionBasic;
	private string carrierType;
	private string browserId;
	private string hostNm;
	private string adCd;
	private string introCdMan;
	private string introCdWoman;
	private string urlSexCd;
	private bool isCrawler;


	protected void Page_Load(object sender,EventArgs e) {

		string sBaseUrl;
		string sRedirect = "";

		if (!IsPostBack) {
			hostNm = iBridUtil.GetStringValue(Request.QueryString["host"]);
			adCd = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
			carrierType = Mobile.GetCarrier(HttpContext.Current.Request.Browser.Browser);
			browserId = HttpContext.Current.Request.Browser.Id;
			introCdMan = iBridUtil.GetStringValue(Request.QueryString["intm"]);
			introCdWoman = iBridUtil.GetStringValue(Request.QueryString["intw"]);

			// Session StartがStart.aspx以外始まる場合に付与される 
			urlSexCd = iBridUtil.GetStringValue(Request.QueryString["urlsex"]);

			string sSessionTimeOut = iBridUtil.GetStringValue(Request.QueryString["timeout"]);
			string sSessionTimeOutId = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SessionTimeOutId"]);
			string sSessionTimeOutIdWoman = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SessionTimeOutIdWoman"]);
			string sTestEnv = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestEnv"]);
			string sTestDevice = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestDevice"]);
			string sImodeId = Mobile.GetiModeId(carrierType,Request);
			string sRequestQuery = Request.QueryString.ToString();
			string sHostSiteCd = string.Empty;
			string sBijoColleFlag = iBridUtil.GetStringValue(Request.QueryString["bijo"]);
			bool bJobSite = false;
			string sReleaseNgAddrSeq = iBridUtil.GetStringValue(Request.QueryString["releaseaddr"]);
			if (!sReleaseNgAddrSeq.Equals(string.Empty)) {
				ReleaseNgMailAddr(sReleaseNgAddrSeq);
			}
			this.ResetMailErrorCount();
			
			if(sBijoColleFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sSessionTimeOutId = string.Format("GameDisplayDoc.aspx?doc={0}",PwViCommConst.SCR_MAN_GAME_SESSION_TIME_OUT);
				sSessionTimeOutIdWoman = string.Format("GameDisplayDoc.aspx?doc={0}",PwViCommConst.SCR_WOMAN_GAME_SESSION_TIME_OUT);
			}

			if (sTestEnv.Equals("1") && hostNm.Equals(string.Empty)) {
				hostNm = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestLocation"]);
			}

			if (hostNm.Equals("")) {
				hostNm = Page.Request.Url.Host;
			}

			if (sTestDevice.Equals("1")) {
				carrierType = ConfigurationManager.AppSettings["TestCarrier"];
			}

			int iPcRedirectFlag = 0;
			bool bPassIP = false;

			using (AuthIP oAuthIP = new AuthIP()) {
				bPassIP = oAuthIP.IsExist(Request.UserHostAddress,hostNm,HttpContext.Current.Request.UserAgent,Request.QueryString,out iPcRedirectFlag,out isCrawler);
			}

			using (Site oSite = new Site()) {
				if (oSite.GetOneByJobOfferHost(hostNm)) {
					bJobSite = true;
					if (iPcRedirectFlag != 0) {
						sRedirect = oSite.PcRedirectUrl;
					} else if (carrierType.Equals(ViCommConst.CARRIER_OTHERS) && (iBridUtil.GetStringValue(Request.QueryString["nopc"]).Equals("1") == false) && (bPassIP == false)) {
						using (ManageCompany oManageCompany = new ManageCompany())　{
							if (!oManageCompany.IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_CAST_PC,2)) {
								sRedirect = oSite.PcRedirectUrl;
							}
						}
					}
				} else if (oSite.GetOneByHost(hostNm)) {
					if (iPcRedirectFlag != 0) {
						sRedirect = oSite.PcRedirectUrl;
					} else if (carrierType.Equals(ViCommConst.CARRIER_OTHERS) && (iBridUtil.GetStringValue(Request.QueryString["nopc"]).Equals("1") == false) && (bPassIP == false)) {
						if (urlSexCd.Equals(ViCommConst.OPERATOR)) {
							using (ManageCompany oManageCompany = new ManageCompany())　{
								if (!oManageCompany.IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_CAST_PC,2)) {
									sRedirect = oSite.PcRedirectUrl;
								}
							}
						} else {
							sRedirect = oSite.PcRedirectUrl;
						}
					}
				} else {
					throw new ApplicationException("Host name not found " + hostNm);
				}
				sHostSiteCd = oSite.siteCd;

				if (!bPassIP) {
					string sCarrierIpConfigDir = System.IO.Path.Combine(oSite.webPhisicalDir,@"config\carrierip");
					if (iBridUtil.GetStringValue(Request.QueryString["_reload_carrierip"]).Equals("1")) {
						CarrierIpValidator.Clear();
					}

					if (sRedirect.Equals("")) {
						CarrierIpValidator.Configure(sCarrierIpConfigDir);
						if (!CarrierIpValidator.Validate(carrierType,Request.UserHostAddress)) {
							throw new HttpException(403,string.Format("Forbidden.(carrier:{0},ip:{1})",carrierType,Request.UserHostAddress));
						}
					}
				}

				if (!sRedirect.Equals("")) {
					if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["ReplacePcPageUrl"]).Equals(ViCommConst.FLAG_ON_STR)) {
						string sRedirectHost,sUri;
						Match m = Regex.Match(sRedirect,@"http:\/\/(?<HOST>[^\/]*)(?<URI>\/?.*)?",RegexOptions.IgnoreCase | RegexOptions.Compiled);
						if (m.Success) {
							sRedirectHost = m.Groups["HOST"].Captures[0].Value;
							sUri = m.Groups["URI"].Captures[0].Value;
							if (!sRedirectHost.Equals(hostNm)) {
								sRedirect = "http://" + hostNm + sUri;
							}

							if (!string.IsNullOrEmpty(adCd)) {
								if (sRedirect.Contains("?")) {
									sRedirect = string.Format("{0}&adcd={1}",sRedirect,adCd);
								} else {
									sRedirect = string.Format("{0}?adcd={1}",sRedirect,adCd);
								}								
							}
						}
					}
					Response.Redirect(sRedirect);
				}

				if (!iBridUtil.GetStringValue(Request.QueryString["mailseq"]).Equals("")) {
					string sRxSiteCd,sRxMailSexCd,sTxUserSeq,sTxUserCharNo;
					using (MailLog oMailLog = new MailLog()) {
						oMailLog.GetRxMailInfo(iBridUtil.GetStringValue(Request.QueryString["mailseq"]),out sRxSiteCd,out sRxMailSexCd,out sTxUserSeq,out sTxUserCharNo);
					}
					if (!sRxMailSexCd.Equals(ViCommConst.MAN)) {
						if (oSite.GetCastMultiCtlSite()) {
							hostNm = oSite.subHostNm;
						}
					}
				}

				SessionObjs.CurrentSexCd
					= ((oSite.castMultiCtlSiteFlag == 0) && (oSite.jobOfferSiteFlag == false)) ? ViCommConst.MAN : ViCommConst.WOMAN;

				if (SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN) && !urlSexCd.Equals(string.Empty)) {
					SessionObjs.CurrentSexCd = urlSexCd;
				}

				string sMainMenu = "";
				SessionHelper oHelper = new SessionHelper(SessionObjs.CurrentSexCd,carrierType,browserId,adCd,introCdMan.Equals(string.Empty) ? introCdWoman : introCdMan);
				if (!SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN)) {
					HttpContext.Current.Session["BASIC_SITE"] = oSite.siteCd;
				}

				if ((oSite.useOtherSysInfoFlag != 0) && (SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN))) {
					oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
					OtherSysIF((SessionMan)Session["objs"]);
				} else {
					if (SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN)) {
						sMainMenu = oSite.nonUserTopId;
					} else {
						sMainMenu = "NonUserTop.aspx";
					}
					if (iBridUtil.GetStringValue(Request.QueryString["game"]).Equals(ViCommConst.FLAG_ON_STR)) {
						sMainMenu = "GameNonUserTop.aspx";
					}

					int iGroupType = 0;
					if (!adCd.Equals(string.Empty)) {
						using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
							string sGroupCd;
							oSiteAdGroup.GetAdGroupCd(oSite.siteCd,adCd,out sGroupCd,out iGroupType);
						}
					}

					string sAddQuery = "";
					bool bQueryClear = false;

					if (this.IsRakutenAuthRequest()) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						this.CheckAuthentication();

						sMainMenu = "RegistUserRequestByTermId.aspx";
						using (UserOpenId oUserOpenId = new UserOpenId()) {
							string sUserSeq = oUserOpenId.GetUserSeq(SessionObjs.Current.site.siteCd,SessionObjs.Current.tempOpenId,ViCommConst.OpenIdTypes.RAKUTEN);
							if (!string.IsNullOrEmpty(sUserSeq)) {
								if (SessionObjs.Current.sexCd.Equals(ViCommConst.MAN)) {
									using (UserMan oUserMan = new UserMan()) {
										if (oUserMan.GetCurrentInfo(SessionObjs.Current.site.siteCd,sUserSeq)) {
											sAddQuery = string.Format("&loginid={0}&password={1}",oUserMan.loginId,oUserMan.loginPassword);
											sMainMenu = "LoginUser.aspx";
										}
									}
								}
							}
						}
						sSessionTimeOut = "";
					} else if (!iBridUtil.GetStringValue(Request.QueryString["regid"]).Equals("")) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = "RegistForm.aspx";
						sSessionTimeOut = "";
					} else if (!iBridUtil.GetStringValue(Request.QueryString["FUKA"]).Equals("")) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = "CCheckComplite.aspx";
						sSessionTimeOut = "";
					} else if ((!this.GetStringValue(Request.QueryString["loginid"]).Equals("")) && (!this.GetStringValue(Request.QueryString["password"]).Equals(""))) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						if (this.GetStringValue(Request.QueryString["game"]).Equals(ViCommConst.FLAG_ON_STR)) {
							sMainMenu = "GameLoginUser.aspx";
						} else {
							sMainMenu = "LoginUser.aspx";
						}
						sSessionTimeOut = "";
					} else if ((!this.GetStringValue(Request.QueryString["uid"]).Equals("")) && (!this.GetStringValue(Request.QueryString["pw"]).Equals(""))) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = "LoginUser.aspx";
						sSessionTimeOut = "";
					} else if (iBridUtil.GetStringValue(Request.QueryString["goto"]).Equals("ProfileDirect.aspx")) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = "Profile.aspx";
						sSessionTimeOut = "";
						if (oSessionBasic.site.charNoItemNm.Equals("") == false) {
							sAddQuery = string.Format("&loginid={0}&direct=1",iBridUtil.GetStringValue(Request.QueryString["castid"]));
						}
					} else if (iBridUtil.GetStringValue(Request.QueryString["goto"]).Equals("RegistUserRequestByTermId.aspx")) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = "RegistUserRequestByTermId.aspx";
						sSessionTimeOut = "";
					} else if (iBridUtil.GetStringValue(Request.QueryString["call"]).Equals("LoginUser.aspx")) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = iBridUtil.GetStringValue(Request.QueryString["call"]);
						bQueryClear = true;
						sSessionTimeOut = "";
					} else if (iBridUtil.GetStringValue(Request.QueryString["goto"]).Equals("ViewPersonalBlogArticle.aspx")) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = "ViewPersonalBlogArticle.aspx";
						if (!iBridUtil.GetStringValue(Request.QueryString["beforesystemid"]).Equals(string.Empty)) {
							using (Blog oBlog = new Blog()) {
								sAddQuery = string.Format("&blog_seq={0}",oBlog.GetBlogSeqByBeforeSystemId(oSite.siteCd,iBridUtil.GetStringValue(Request.QueryString["beforesystemid"])));
								Regex oRegex = new Regex(@"(&beforesystemid=\d*)",RegexOptions.Compiled);
								sRequestQuery = oRegex.Replace(sRequestQuery,string.Empty);
							}
						}
						sSessionTimeOut = "";
					} else if (!iBridUtil.GetStringValue(Request.QueryString["goto"]).Equals(string.Empty)) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = iBridUtil.GetStringValue(Request.QueryString["goto"]);
						sSessionTimeOut = "";
					} else if (SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN) && ((iGroupType != 0) || (iBridUtil.GetStringValue(Request.QueryString["registedprepaid"]).Equals("1")))) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						// registedprepaidは登録ﾒｰﾙの受信時にREGIST_USER_MANより設定される
						sMainMenu = "PrepaidUserTop.aspx";
					} else if (iBridUtil.GetStringValue(Request.QueryString["call"]).Equals("GameLoginUser.aspx")) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = iBridUtil.GetStringValue(Request.QueryString["call"]);
						bQueryClear = true;
						sSessionTimeOut = "";
					} else if (iBridUtil.GetStringValue(Request.QueryString["call"]).Equals("DisplayDoc.aspx") && iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals("600")) {	
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = iBridUtil.GetStringValue(Request.QueryString["call"]);
						sRequestQuery = "doc=600&guid=ON";
						bQueryClear = false;
						sSessionTimeOut = "";
					} else if (SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN) && iBridUtil.GetStringValue(Request.QueryString["googlelogin"]).Equals(ViCommConst.FLAG_ON_STR) && !string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["code"]))) {
						oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						sMainMenu = "LoginUserByGoogleId.aspx";
						sSessionTimeOut = "";
					} else {
						// Subscriber Noにより性別判定
						bool bFindImodeId = false;
						string sFlag = ViCommConst.FLAG_OFF_STR;
						string sUid = string.Empty;
						string sPw = string.Empty;
						using (ManageCompany oCompany = new ManageCompany())
						using (User oUser = new User()) {
							// JOB SITE以外またはJOBｻｲﾄからの自動ログインがONの場合、iModeIDによるﾕｰｻﾞｰの自動ログインを行う。
							if (!bJobSite || this.GetStringValue(ConfigurationManager.AppSettings["EnableJobSiteEasyLogin"]).Equals(ViCommConst.FLAG_ON_STR)) {
								// Cookie
								if (carrierType.Equals(ViCommConst.ANDROID) || carrierType.Equals(ViCommConst.IPHONE) || carrierType.Equals(ViCommConst.CARRIER_OTHERS)) {
									if (this.GetStringValue(ConfigurationManager.AppSettings["EnableCookie"]).Equals(ViCommConst.FLAG_ON_STR)) {
										if (Request.Cookies["maqia"] != null) {
											if (oUser.GetOne(this.GetStringValue(Request.Cookies["maqia"]["maqiauid"]),this.GetStringValue(Request.Cookies["maqia"]["maqiasex"]))) {
												if (!oUser.sexCd.Equals(ViCommConst.MAN)) {
													oUser.sexCd = ViCommConst.OPERATOR;
												}
												if (!ViCommConst.FLAG_ON_STR.Equals(oUser.adminUserFlag)) {
													bFindImodeId = oUser.loginPassword.Equals(this.GetStringValue(Request.Cookies["maqia"]["maqiapw"]));
													sUid = oUser.loginId;
													sPw = oUser.loginPassword;
													sSessionTimeOut = "";
												}
											}
										}
									}
								} else {
									bFindImodeId = oUser.GetUserSexByIModeId(sImodeId,oSite.siteCd);

									if (bFindImodeId) {
										sSessionTimeOut = "";
									}
								}
							}

							if (oUser.adminUserFlag.Equals(ViCommConst.FLAG_ON_STR)) {
								sImodeId = string.Empty;
								bFindImodeId = false;
							}

							if (bFindImodeId) {
								if (oUser.sexCd.Equals(ViCommConst.MAN)) {
									oCompany.GetValue("MAN_USED_EASY_LOGIN",out sFlag);
								} else {
									oCompany.GetValue("WOMAN_USED_EASY_LOGIN",out sFlag);
								}
							}

							if (sFlag.Equals(ViCommConst.FLAG_ON_STR)) {
								if (!oUser.sexCd.Equals(SessionObjs.CurrentSexCd)) {
									if (oUser.sexCd.Equals(ViCommConst.WOMAN) || oUser.sexCd.Equals(ViCommConst.OPERATOR)) {
										SessionObjs.CurrentSexCd = ViCommConst.WOMAN;
										if (oSite.GetCastMultiCtlSite()) {
											hostNm = oSite.subHostNm;
										}
									} else {
										SessionObjs.CurrentSexCd = ViCommConst.MAN;
										if (oSite.GetOneByHost(hostNm.Replace("cast.",""))) {
											hostNm = oSite.hostNm;
										}
									}
								}
								sMainMenu = "LoginUser.aspx";

								if (oUser.sexCd.Equals(ViCommConst.MAN)) {
									sRequestQuery = string.Format("id={0}&uid={1}&pw={2}",sImodeId,sUid,sPw);
								} else {
									sRequestQuery = string.Format("id={0}&uid={1}&pw={2}&site={3}&goto=UserTop.aspx",sImodeId,sUid,sPw,sHostSiteCd);
								}

								if (iBridUtil.GetStringValue(Request.QueryString["game"]).Equals(ViCommConst.FLAG_ON_STR)) {
									sMainMenu = "GameLoginUser.aspx";
									sRequestQuery = string.Format("id={0}&uid={1}&pw={2}",sImodeId,sUid,sPw);
								}
								if (oSite.manAutoLoginResignedFlag.Equals(ViCommConst.FLAG_ON) && oUser.userStatus.Equals(ViCommConst.USER_MAN_RESIGNED) && oUser.sexCd.Equals(ViCommConst.MAN)) {
									sMainMenu = "ResignedUserTop.aspx";
								} else if (oSite.castAutoLoginResignedFlag.Equals(ViCommConst.FLAG_ON) && oUser.userStatus.Equals(ViCommConst.USER_WOMAN_RESIGNED) && oUser.sexCd.Equals(ViCommConst.WOMAN)) {
									sMainMenu = "ResignedUserTop.aspx";
									sRequestQuery = string.Format("id={0}&uid={1}&pw={2}&site={3}",sImodeId,sUid,sPw,sHostSiteCd);
								}
							}
							oHelper.sexCd = SessionObjs.CurrentSexCd;
							oHelper.CreateSessionInfo(hostNm,Session.SessionID,isCrawler,isCrawler,out oSessionBasic);
						}

					}
					if (SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN)) {
						//男性はStart.aspxを通った時にﾄｯﾌﾟﾍﾟｰｼﾞｶｳﾝﾄ数のｶｳﾝﾄｱｯﾌﾟをする。女性はUserTopでｶｳﾝﾄｱｯﾌﾟをする。 
						using (Access oAccess = new Access()) {
							oAccess.AccessTopPage(oSessionBasic.site.siteCd,string.Empty,string.Empty,oSessionBasic.adCd,SessionObjs.CurrentSexCd);
						}
						
						if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["intw"]))) {
							using (ManageCompany oManageCompany = new ManageCompany())　{
								if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_INTRO_ACCESS_COUNT,2)) {
									this.RegistIntroAccessCount(iBridUtil.GetStringValue(this.Request.QueryString["intw"]));
								}
							}
						}
					}

					oSessionBasic.mainMenu = sMainMenu;
					oSessionBasic.currentIModeId = sImodeId;
					oSessionBasic.adminFlg = iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["adminflg"]).Equals(ViCommConst.FLAG_ON_STR);

					string sQuery = sRequestQuery + sAddQuery;
					if (bQueryClear) {
						sQuery = "";
					}
					Regex regex2 = new Regex(@"((\&?|\?)doc=)\d{1,3}(\&?)",RegexOptions.Compiled);
					sBaseUrl = oSessionBasic.root + oSessionBasic.sysType;
					if (HttpContext.Current.Request.Browser.Browser == "Firefox" && !iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["framevw"]).Equals("1")) {
						sQuery = regex2.Replace(sQuery,"");
						sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,"DisplayDoc.aspx") + "?doc=" + ViCommConst.ERR_FRAME_BROWSER + "&" + sQuery;
					} else if (sSessionTimeOut.Equals("1") && SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN) && !sSessionTimeOutId.Equals("")) {
						//(男性)セッションタイムアウトにてStart.aspxが呼ばれた時
						if(sBijoColleFlag.Equals(ViCommConst.FLAG_ON_STR)) {
							if (iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["call"]).Equals("GameNonUserTop.aspx")) {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,"GameNonUserTop.aspx");
							} else {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sSessionTimeOutId);
							}
						} else {
							if (iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["call"]).Equals(oSite.nonUserTopId)) {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl, Session.SessionID, oSite.nonUserTopId);
							} else {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl, Session.SessionID, sSessionTimeOutId);
							}
						}
					} else if (sSessionTimeOut.Equals("1") && !SessionObjs.CurrentSexCd.Equals(ViCommConst.MAN) && !string.IsNullOrEmpty(sSessionTimeOutIdWoman)) {
						//(女性)セッションタイムアウトにてStart.aspxが呼ばれた時
						if (sBijoColleFlag.Equals(ViCommConst.FLAG_ON_STR)) {
							if (iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["call"]).Equals("GameNonUserTop.aspx")) {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,"GameNonUserTop.aspx");
							} else {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sSessionTimeOutIdWoman);
							}
						} else {
							if (iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["call"]).Equals(oSite.nonUserTopId)) {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl, Session.SessionID, oSite.nonUserTopId);
							} else {
								sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl, Session.SessionID, sSessionTimeOutIdWoman);
							}
						}
					} else {
						bool bRedirectJobDoc = true;

						if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableJobSiteEasyLogin"]).Equals(ViCommConst.FLAG_ON_STR)) {
							using (UserWoman oUserWoman = new UserWoman()) {
								string sTmpLoginId = string.Empty;
								string sTmpPassword = string.Empty;
								if (!string.IsNullOrEmpty(sImodeId) && oUserWoman.UsedTermId(string.Empty,sImodeId,out sTmpLoginId,out sTmpPassword)) {
									bRedirectJobDoc = false;
								}
							}
						}
						if (oSessionBasic.site.jobOfferSiteFlag && bRedirectJobDoc) {

							//女性求人ページの場合もカウントアップさせる
							using (Access oAccess = new Access()) {
								oAccess.AccessTopPage(oSessionBasic.site.siteCd,string.Empty,string.Empty,oSessionBasic.adCd,ViCommConst.OPERATOR);
							}
							switch (sMainMenu) {
								case "RegistUserRequestByTermId.aspx":
									sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sMainMenu);
									break;

								case "LoginUser.aspx":
								case "DisplayDoc.aspx":
									sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sMainMenu + "?" + sQuery);
									break;

								default:
									sMainMenu = "DisplayDoc.aspx";
									sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sMainMenu) + "?doc=" + oSessionBasic.site.jobOfferStartDoc;

									sQuery = regex2.Replace(sQuery,"");
									if (!sQuery.Equals("")) {
										sRedirect = sRedirect + "&" + sQuery;
									}
									break;

							}

						} else if ((sMainMenu.Equals(oSite.nonUserTopId)) && (IsExistAgeAuthScreen(oSite.siteCd))) {
							sMainMenu = "DisplayDoc.aspx";
							sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sMainMenu) + "?doc=" + ViCommConst.SCR_AGE_AUTH;
							sQuery = regex2.Replace(sQuery,"");
							if (!sQuery.Equals("")) {
								sRedirect = sRedirect + "&" + sQuery;
							}

						} else if (iBridUtil.GetStringValue(Request.QueryString["tran"]).Equals("secession")) {
							sMainMenu = "DisplayDoc.aspx";
							sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sMainMenu) +
										 "?doc=" + iBridUtil.GetStringValue(Request.QueryString["doc"]) +
										 "&sc=" + iBridUtil.GetStringValue(Request.QueryString["sc"]);
						} else {
							sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,sMainMenu) + "?" + sQuery;
						}
					}
					RedirectToMobilePage(SysPrograms.ChangeQueryToPathInfo(sRedirect,oSessionBasic.isCrawler));
					DataBind();
				}
			}
		}
	}

	private void ReleaseNgMailAddr(string pRequestTxMailSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RELEASE_NG_MAIL_ADDR");
			db.ProcedureInParm("pREQUEST_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pRequestTxMailSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private void ResetMailErrorCount() {
		string[] sMailRequest = iBridUtil.GetStringValue(this.Request.QueryString["mailrequest"]).Split('-');
		if (sMailRequest.Length == 2) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("RESET_MAIL_ERROR_COUNT");
				oDbSession.ProcedureInParm("pREQUEST_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,sMailRequest[0]);
				oDbSession.ProcedureInParm("pREQUEST_TX_MAIL_SUBSEQ",DbSession.DbType.VARCHAR2,sMailRequest[1]);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}
	}

	private bool IsExistAgeAuthScreen(string pSiteCd) {
		bool bRet;
		using (SiteHtmlDoc oSiteHtmlDoc = new SiteHtmlDoc()) {
			bRet = !oSiteHtmlDoc.GetDocSeq(pSiteCd,ViCommConst.SCR_AGE_AUTH).Equals("");
		}
		return bRet;
	}

	private void OtherSysIF(SessionMan pUserObjs) {

		pUserObjs.userMan.webUserId = Request.QueryString["userid"];

		string sTel,sPassword,sCondition,sWebLocCd;
		int iBalPoint = 0;

		int iRet = ViCommInterface.GetWebUserInfo(pUserObjs.site.userInfoUrl,pUserObjs.userMan.webUserId,out iBalPoint,out sTel,out sPassword,out sCondition,out sWebLocCd);

		pUserObjs.userMan.webLocCd = sWebLocCd;

		switch (iRet) {
			case ViCommConst.USER_INFO_OK:
			case ViCommConst.USER_INFO_NO_BAL:


				using (Access oAccess = new Access()) {
					oAccess.AccessTopPage(pUserObjs.site.siteCd,string.Empty,string.Empty,"",ViCommConst.MAN);
				}

				pUserObjs.mainMenu = pUserObjs.site.GetSupplement(ViCommConst.SITE_DEFAULT_MENU);

				int iMenuId;
				if (int.TryParse(iBridUtil.GetStringValue(Request.QueryString["menuid"]),out iMenuId)) {
					string sMenu = pUserObjs.site.GetSupplement(string.Format("M{0}",iMenuId));
					if (sMenu.Equals("") == false) {
						pUserObjs.mainMenu = sMenu;
					}
				}
				string sBaseUrl = pUserObjs.root + pUserObjs.sysType;
				string sRedirect = oSessionBasic.GetNavigateUrl(sBaseUrl,Session.SessionID,pUserObjs.mainMenu) + "?" + Request.QueryString;

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("REGIST_WEB_USER");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pUserObjs.site.siteCd);
					db.ProcedureInParm("PWEB_LOC_CD",DbSession.DbType.VARCHAR2,sWebLocCd);
					db.ProcedureInParm("PWEB_USER_ID",DbSession.DbType.VARCHAR2,pUserObjs.userMan.webUserId);
					db.ProcedureInParm("PBAL_POINT",DbSession.DbType.NUMBER,iBalPoint);
					db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,sTel);
					db.ProcedureInParm("PPASSWORD",DbSession.DbType.NUMBER,sPassword);
					db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.NUMBER,pUserObjs.carrier);
					db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.NUMBER,"");
					db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
					pUserObjs.userMan.siteCd = pUserObjs.site.siteCd;
					pUserObjs.userMan.userSeq = db.GetStringValue("PUSER_SEQ");
					pUserObjs.userMan.loginPassword = sPassword;
					pUserObjs.userMan.balPoint = iBalPoint;
					pUserObjs.userMan.tel = sTel;
					pUserObjs.userMan.userCharNo = ViCommConst.MAIN_CHAR_NO;
				}

				pUserObjs.logined = true;
				RedirectToMobilePage(sRedirect);
				break;

			case ViCommConst.USER_INFO_UNAUTH_TEL:
				pUserObjs.userMan.loginId = pUserObjs.userMan.webUserId;
				pUserObjs.userMan.loginPassword = sPassword;
				RedirectToMobilePage(oSessionBasic.GetNavigateErrorUrl(Session.SessionID,"MobileError.aspx?mode=user&errtype=" + ViCommConst.USER_INFO_UNAUTH_TEL));
				break;

			case ViCommConst.USER_INFO_NG:
				pUserObjs.userMan.loginId = pUserObjs.userMan.webUserId;
				pUserObjs.userMan.loginPassword = sPassword;
				RedirectToMobilePage(oSessionBasic.GetNavigateErrorUrl(Session.SessionID,"MobileError.aspx?mode=user&errtype=" + ViCommConst.USER_INFO_NG));
				break;
		}
	}
	
	private void RegistIntroAccessCount(string pIntroducerFriendCd) {
		try {
			using (IntroAccessCountDaily oIntroAccessCountDaily = new IntroAccessCountDaily()) {
				oIntroAccessCountDaily.RegistIntroAccessCount(pIntroducerFriendCd);
			}
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"RegistPageAccessLog","");
			/* memo:
			 * 　ｱｸｾｽﾛｸﾞ集計のエラー程度で他機能に影響を与えるのは適切ではないので、 
			 * 　エラーログ出力にとどめ、再スローは行わない 
			 */
		}
	}

	//マルチスレッド問題回避
	private string GetStringValue(object value) {
		if (value == null) {
			return "";
		} else {
			return value.ToString();
		}
	}

	#region □■□ 楽天関連 □■□ ================================================================

	/// <summary>
	/// [ViComm⇒楽天]
	/// 認証結果確認要求URLにアクセスし楽天ID認証結果が正しいかどうかチェックする。
	/// </summary>
	private void CheckAuthentication() {

		SessionObjs.Current.tempOpenId = null;
		SessionObjs.Current.tempOpenIdType = null;

		// ユーザー認証結果確認用のURLを生成する

		string sCheckAuthenticaionUrl = this.rakutenUserAuth.GenerateCheckAuthenticaionUrl(this.Request);

		// ユーザー認証結果確認処理

		if (this.rakutenUserAuth.CheckAuthenticaion(sCheckAuthenticaionUrl)) {

			// ﾕｰｻﾞｰ認証結果のリクエストからｵｰﾌﾟﾝID(及びユーザー情報)を抽出
			UserAuthResultEntity oRakutenUserAuthResult = this.rakutenUserAuth.ParseUserAuthResult(this.Request);

			SessionObjs.Current.tempOpenId = oRakutenUserAuthResult.OpenId;
			SessionObjs.Current.tempOpenIdType = ViCommConst.OpenIdTypes.RAKUTEN;
			SessionObjs.Current.adCd = ViCommConst.AD_CD_RAKUTEN;


		} else {
			// 楽天認証の確認が取れない場合はシステムエラー扱い。

			StringBuilder oErrMsgBuilder = new StringBuilder();
			oErrMsgBuilder.Append("楽天ID認証で認証結果の確認が取れませんでした。").AppendLine();
			oErrMsgBuilder.AppendFormat("URL:{0}",sCheckAuthenticaionUrl).AppendLine();
			throw new Exception(oErrMsgBuilder.ToString());
		}
	}

	private bool IsRakutenAuthRequest() {
		string openIdEndPoint = this.Request.Params["openid.op_endpoint"];
		if (string.IsNullOrEmpty(openIdEndPoint))
			return false;

		EventLogWriter.Debug("楽天認証受信 openid.op_endpoint:{0}",openIdEndPoint);

		return openIdEndPoint.Contains("api.id.rakuten.co.jp");
	}
	#endregion ====================================================================================

	/*
		private void CreateSessionInfo(string pSexCd,string pSiteCd) {
			string sPattern;

			if (pSexCd.Equals(ViCommConst.MAN)) {
				if (!carrierType.Equals(ViCommConst.KDDI)) {
					sPattern = @"(<!--.*-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<body.*?>|<form.*?>|<\/form.*?>)";
				} else {
					sPattern = @"(<!--.*-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<\/head.*?>|<title>|<body.*?>|<form.*?>|<\/form.*?>)";
				}
			} else {
				if (!carrierType.Equals(ViCommConst.KDDI)) {
					sPattern = @"(<!--.*-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<body.*?>|<form.*?>|<\/form.*?>|<a +href.*?>)";
				} else {
					sPattern = @"(<!--.*-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<\/head.*?>|<title>|<body.*?>|<form.*?>|<\/form.*?>|<a +href.*?>)";
				}
			}

			bool bDebug = iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["design"]).Equals("1");

			SessionObjs prevObj = (SessionObjs)Session["objs"];
			if (prevObj is SessionObjs) {
				if (adCd.Equals(string.Empty)) {
					adCd = prevObj.adCd;
				}
				if (introCdMan.Equals(string.Empty)) {
					if (prevObj is SessionMan) {
						introCdMan = ((SessionMan)prevObj).userMan.introducerFriendCd;
					}
				}
				if (introCdWoman.Equals(string.Empty)) {
					if (prevObj is SessionWoman) {
						introCdWoman = ((SessionWoman)prevObj).userWoman.introducerFriendCd;
					}
				}
				if (!bDebug) {
					bDebug = prevObj.designDebug;
				}
			}

			if (pSexCd.Equals(ViCommConst.MAN)) {

				SessionMan userObjs = new SessionMan(
												hostNm,
												Session.SessionID,
												carrierType,
												HttpContext.Current.Request.UserAgent,
												adCd,
												Request.QueryString,
												isCrawler,
												isCrawler
											);
				userObjs.userMan.introducerFriendCd = introCdMan;
				Session["objs"] = userObjs;
				userObjs.designDebug = bDebug;

				ParseMan oParseMan = new ParseMan(
						sPattern,
						isCrawler,
						Session.SessionID,
						Page.Request.Url.Host,			// 開発環境でも実行できるようにするため
						"user/vicomm/man",
						RegexOptions.IgnoreCase,
						userObjs.carrier,
						userObjs);
				Session["parse"] = new ParseHTML(oParseMan);
				oParseMan.parseContainer = (ParseHTML)Session["parse"];
				userObjs.parseContainer = (ParseHTML)Session["parse"];
				userObjs.sysType = "/ViComm/man";
			} else {
				SessionWoman userObjs = new SessionWoman(
												hostNm,
												Session.SessionID,
												carrierType,
												HttpContext.Current.Request.UserAgent,
												adCd,
												Request.QueryString,
												isCrawler,
												isCrawler
											);
				userObjs.userWoman.introducerFriendCd = introCdWoman;
				Session["objs"] = userObjs;
				Session["BASIC_SITE"] = pSiteCd;
				userObjs.designDebug = iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["design"]).Equals("1");
				userObjs.sysType = "/ViComm/woman";

				ParseWoman oParseWoman = new ParseWoman(
						sPattern,
						isCrawler,
						Session.SessionID,
						Page.Request.Url.Host,			// 開発環境でも実行できるようにするため
						"user/vicomm/woman",
						RegexOptions.IgnoreCase,
						userObjs.carrier,
						userObjs);
				Session["parse"] = new ParseHTML(oParseWoman);
				oParseWoman.parseContainer = (ParseHTML)Session["parse"];
				userObjs.parseContainer = (ParseHTML)Session["parse"];
			}

			oSessionBasic = (SessionObjs)Session["objs"];
			oSessionBasic.adminFlg = iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["adminflg"]).Equals(ViCommConst.FLAG_ON_STR);
			oSessionBasic.currentIModeId = Mobile.GetiModeId(oSessionBasic.carrier,Request);
		}
	 */
}
