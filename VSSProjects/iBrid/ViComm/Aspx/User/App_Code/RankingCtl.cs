﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 足あとランク設定
--	Progaram ID		: RankingCtl
--
--  Creation Date	: 2011.04.07
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class RankingCtl:DbSession {

	public RankingCtl() {
	}

	public DataSet GetList(string pSiteCd,string pSummaryType) {
		DataSet ds;
		try {
			conn = DbConnect("RankingCtl.GetList");
			ds = new DataSet();

			string sSql = "SELECT RANKING_CTL_SEQ,TO_CHAR(SUMMARY_END_DATE,'YYYY/MM/DD') AS SUMMARY_END_DAY FROM T_RANKING_CTL WHERE SITE_CD = :SITE_CD AND SUMMARY_TYPE = :SUMMARY_TYPE AND SUMMARY_END_DATE < :SUMMARY_END_DATE ";
			sSql = sSql + " ORDER BY SITE_CD,SUMMARY_END_DATE DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				cmd.Parameters.Add("SUMMARY_END_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public string GetNowSeq(string pSiteCd,string pSummaryType) {
		DataSet ds;
		DataRow dr;
		string sSeq = string.Empty;

		try {
			conn = DbConnect("RankingCtl.GetNowSeq");
			ds = new DataSet();

			string sSql = "SELECT RANKING_CTL_SEQ FROM T_RANKING_CTL WHERE SITE_CD = :SITE_CD AND SUMMARY_TYPE = :SUMMARY_TYPE AND SUMMARY_START_DATE < :SUMMARY_START_DATE ";
			sSql = sSql + " ORDER BY SITE_CD,SUMMARY_END_DATE DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				cmd.Parameters.Add("SUMMARY_START_DATE",OracleDbType.Date,DateTime.Now.AddHours(-1),ParameterDirection.Input);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sSeq = dr["RANKING_CTL_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sSeq;
	}

	public string GetOldSeq(string pSiteCd,string pSummaryType) {
		DataSet ds;
		DataRow dr;
		string sSeq = string.Empty;

		try {
			conn = DbConnect("RankingCtl.GetOldSeq");
			ds = new DataSet();

			string sSql = "SELECT RANKING_CTL_SEQ FROM T_RANKING_CTL WHERE SITE_CD = :SITE_CD AND SUMMARY_TYPE = :SUMMARY_TYPE AND SUMMARY_END_DATE < :SUMMARY_END_DATE ";
			sSql = sSql + " ORDER BY SITE_CD,SUMMARY_END_DATE DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				cmd.Parameters.Add("SUMMARY_END_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sSeq = dr["RANKING_CTL_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sSeq;
	}

	public string GetLastSeq(string pSiteCd,string pSummaryType) {
		DataSet ds;
		DataRow dr;
		string sSeq = string.Empty;

		try {
			conn = DbConnect("RankingCtl.GetLastSeq");
			ds = new DataSet();

			string sSql = "SELECT RANKING_CTL_SEQ FROM T_RANKING_CTL WHERE SITE_CD = :SITE_CD AND SUMMARY_TYPE = :SUMMARY_TYPE ";
			sSql = sSql + " ORDER BY SITE_CD,SUMMARY_END_DATE DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sSeq = dr["RANKING_CTL_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sSeq;
	}

	public string GetSummaryEndDate(string pRankingCtlSeq) {
		DataSet ds;
		DataRow dr;
		string sSummaryEndDate = string.Empty;

		try {
			conn = DbConnect("RankingCtl.GetSummaryEndDate");
			ds = new DataSet();

			string sSql = "SELECT SUMMARY_END_DATE FROM T_RANKING_CTL WHERE RANKING_CTL_SEQ	= :RANKING_CTL_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("RANKING_CTL_SEQ",pRankingCtlSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sSummaryEndDate = dr["SUMMARY_END_DATE"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sSummaryEndDate;
	}

	public bool GetOngoingMarkingRankingFlag(string pSiteCd,string pSummaryType) {
		//現在ランキングの集計が進行中かどうかを返すフラグ
		//終了から1時間以内の場合、停止中とする。
		DataSet ds;
		DataRow dr;
		bool bOngoinMarkingRank = false;
		DateTime dtEnd;

		try {
			conn = DbConnect("RankingCtl.GetOngoingMarkingRankingFlag");
			ds = new DataSet();

			string sSql = "SELECT SUMMARY_END_DATE FROM T_RANKING_CTL WHERE SITE_CD = :SITE_CD AND SUMMARY_TYPE = :SUMMARY_TYPE AND SUMMARY_START_DATE < :SUMMARY_START_DATE ";
			sSql = sSql + " ORDER BY SITE_CD,SUMMARY_END_DATE DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				cmd.Parameters.Add("SUMMARY_START_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count == 1) {
						dr = ds.Tables[0].Rows[0];
						dtEnd = DateTime.Parse(dr["SUMMARY_END_DATE"].ToString());
						if (DateTime.Now <= dtEnd) {
							bOngoinMarkingRank = true;
						} else {
							bOngoinMarkingRank = false;
						}
					} else if (ds.Tables[0].Rows.Count > 1) {
						dr = ds.Tables[0].Rows[0];
						dtEnd = DateTime.Parse(dr["SUMMARY_END_DATE"].ToString());
						if (DateTime.Now <= dtEnd) {
							bOngoinMarkingRank = true;
							dr = ds.Tables[0].Rows[1];
							dtEnd = DateTime.Parse(dr["SUMMARY_END_DATE"].ToString());
							if (DateTime.Now <= dtEnd.AddHours(1)) {
								bOngoinMarkingRank = false;
							}
						} else {
							bOngoinMarkingRank = false;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bOngoinMarkingRank;
	}

	public string GetNextSeq(string pSiteCd,string pSummaryType) {
		DataSet ds;
		DataRow dr;
		string sSeq = string.Empty;

		try {
			conn = DbConnect("RankingCtl.GetNextSeq");
			ds = new DataSet();

			string sSql = "SELECT RANKING_CTL_SEQ FROM T_RANKING_CTL WHERE SITE_CD = :SITE_CD AND SUMMARY_TYPE = :SUMMARY_TYPE AND SUMMARY_END_DATE > :SUMMARY_END_DATE ";
			sSql = sSql + " ORDER BY SITE_CD,SUMMARY_END_DATE ASC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				cmd.Parameters.Add("SUMMARY_END_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sSeq = dr["RANKING_CTL_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sSeq;
	}
}
