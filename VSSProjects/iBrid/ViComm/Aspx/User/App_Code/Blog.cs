﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾞﾛｸﾞ

--	Progaram ID		: Blog
--
--  Creation Date	: 2011.04.11
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[Serializable]
public class BlogSeekCondition:SeekConditionBase {


	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	public string LoginId {
		get {
			return this.Query["loginid"];
		}
		set {
			this.Query["loginid"] = value;
		}
	}
	public string BlogSeq {
		get {
			return this.Query["blog_seq"];
		}
		set {
			this.Query["blog_seq"] = value;
		}
	}
	public string RankType {
		get {
			return this.Query["ranktype"];
		}
		set {
			this.Query["ranktype"] = value;
		}
	}
	public BlogSeekCondition()
		: this(new NameValueCollection()) {
	}
	public BlogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["loginid"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["loginid"]));
		this.Query["blog_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_seq"]));
		this.Query["ranktype"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ranktype"]));
	}
}
#endregion ============================================================================================================

public class Blog:CastBase {
	public Blog()
		: base() {
	}
	public Blog(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(BlogSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	VW_BLOG00 P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(BlogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}
	private DataSet GetPageCollectionBase(BlogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	" + CastBasicField() + ",   ");
		oSqlBuilder.AppendLine("	P.BLOG_SEQ				,	");
		oSqlBuilder.AppendLine("	P.BLOG_TITLE			,	");
		oSqlBuilder.AppendLine("	P.BLOG_DOC				,	");
		oSqlBuilder.AppendLine("	P.BLOG_LIKE_COUNT			");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_BLOG00 P        			");
		
		if (pCondtion.RankType.Equals(PwViCommConst.BlogRankType.WEEKLY)) {
			oSqlBuilder.AppendLine(" ,	T_BLOG_LIKE_COUNT_WEEKLY W			");
			sWhereClause = " WHERE P.SITE_CD = W.SITE_CD (+) AND P.BLOG_SEQ = W.BLOG_SEQ (+) ";
		} else if (pCondtion.RankType.Equals(PwViCommConst.BlogRankType.MONTHLY)) {
			oSqlBuilder.AppendLine(" ,	T_BLOG_LIKE_COUNT_MONTHLY M			");
			sWhereClause = " WHERE P.SITE_CD = M.SITE_CD (+) AND P.BLOG_SEQ = M.BLOG_SEQ (+) ";
		}

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));


		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		AppendCastAttr(oDataSet);
		return oDataSet;
	}

	private OracleParameter[] CreateWhere(BlogSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null)
			throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.USER_STATUS = :USER_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_STATUS",ViCommConst.USER_WOMAN_NORMAL));

		SysPrograms.SqlAppendWhere(" P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		if (!string.IsNullOrEmpty(pCondition.BlogSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_SEQ	= :BLOG_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_SEQ",pCondition.BlogSeq));
		} else {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

			if (!string.IsNullOrEmpty(pCondition.LoginId)) {
				SysPrograms.SqlAppendWhere(" P.LOGIN_ID	= :LOGIN_ID",ref pWhereClause);
				oParamList.Add(new OracleParameter(":LOGIN_ID",pCondition.LoginId));
			}

			if (!string.IsNullOrEmpty(pCondition.UserSeq) && !string.IsNullOrEmpty(pCondition.UserCharNo)) {
				SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
				SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= :USER_CHAR_NO",ref pWhereClause);
				oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
				oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
			}
		}



		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SysPrograms.SqlAppendWhere(" P.ENABLED_BLOG_FLAG = :ENABLED_BLOG_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ENABLED_BLOG_FLAG",ViCommConst.FLAG_ON_STR));
			
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(FlexibleQueryType.NotInScope);
			oFlexibleQueryTarget.JealousyBlog = true;

			ArrayList oTmpList = new ArrayList();
			pWhereClause += SetFlexibleQuery(oFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref oTmpList);
			foreach (OracleParameter oParam in oTmpList) {
				oParamList.Add(oParam);
			}

			SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_SEQ",oSessionMan.userMan.userSeq));
			oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",oSessionMan.userMan.userCharNo));
		}

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(BlogSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.SeekMode) {
			case PwViCommConst.INQUIRY_BLOG_RANKING:
				if (pCondition.RankType.Equals(PwViCommConst.BlogRankType.MONTHLY)) {
					sSortExpression = " ORDER BY NVL(M.LIKE_COUNT_MONTHLY,0) DESC,BLOG_SEQ DESC";
				} else {
					sSortExpression = " ORDER BY NVL(W.LIKE_COUNT_WEEKLY,0) DESC,BLOG_SEQ DESC";
				}
				break;
			default:
				sSortExpression = " ORDER BY BLOG_SEQ DESC ";
				break;
		}
		return sSortExpression;
	}

	#endregion ========================================================================================================


	public string GetBlogSeqByBeforeSystemId(string pSiteCd,string pBeforeSystemId) {
		string sBlogSeq = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT			").AppendLine();
		oSqlBuilder.Append("    T_BLOG.BLOG_SEQ		").AppendLine();
		oSqlBuilder.Append(" FROM			").AppendLine();
		oSqlBuilder.Append("    T_USER,		").AppendLine();
		oSqlBuilder.Append("    T_BLOG		").AppendLine();
		oSqlBuilder.Append(" WHERE			").AppendLine();
		oSqlBuilder.Append("    T_USER.USER_SEQ			= T_BLOG.USER_SEQ	AND	").AppendLine();
		oSqlBuilder.Append("	T_BLOG.SITE_CD			= :SITE_CD			AND ").AppendLine();
		oSqlBuilder.Append("	T_USER.BEFORE_SYSTEM_ID	= :BEFORE_SYSTEM_ID		").AppendLine();
		oSqlBuilder.Append(" ORDER BY		").AppendLine();
		oSqlBuilder.Append("	T_BLOG.SITE_CD,T_BLOG.USER_SEQ,T_BLOG.USER_CHAR_NO	").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("BEFORE_SYSTEM_ID",pBeforeSystemId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BLOG");
					if (ds.Tables["T_BLOG"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sBlogSeq = dr["BLOG_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sBlogSeq;
	}
}
