﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員属性
--	Progaram ID		: UserManAttrType
--
--  Creation Date	: 2009.07.15
--  Creater			: i-Brid（R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/16  i-Brid(Y.Inoue)

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;

[System.Serializable]
public class UserManAttrType:DbSession {

	public UserManAttrType() {
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect("UserManAttrType.GetList");

			string sSql = "SELECT " +
								"MAN_ATTR_TYPE_SEQ		, " +
								"MAN_ATTR_TYPE_NM		, " +
								"MAN_ATTR_TYPE_FIND_NM	, " +
								"INPUT_TYPE				, " +
								"PRIORITY				, " +
								"ITEM_NO				, " +
								"ROW_COUNT				, " +
								"GROUPING_CATEGORY_CD	, " +
								"OMIT_SEEK_CONTION_FLAG	, " +
								"PROFILE_REQ_ITEM_FLAG  ," +
								"REGIST_INPUT_FLAG        " +
							"FROM " +
								"T_MAN_ATTR_TYPE " +
							"WHERE " +
								"SITE_CD	= :SITE_CD	AND " +
								"NA_FLAG	= 0				" +
							"ORDER BY " +
								"PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_ATTR_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	
	public string GetSeqByItemNo(string pSiteCd,string pItemNo) {

		System.Text.StringBuilder objSqlBuilder = new System.Text.StringBuilder();
		objSqlBuilder.Append(" SELECT						").AppendLine();
		objSqlBuilder.Append("     MAN_ATTR_TYPE_SEQ	").AppendLine();
		objSqlBuilder.Append(" FROM							").AppendLine();
		objSqlBuilder.Append("     T_MAN_ATTR_TYPE		").AppendLine();
		objSqlBuilder.Append(" WHERE						").AppendLine();
		objSqlBuilder.Append("	SITE_CD = :SITE_CD AND		").AppendLine();
		objSqlBuilder.Append("	ITEM_NO = :ITEM_NO			").AppendLine();

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":ITEM_NO",pItemNo);

				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
	}
}