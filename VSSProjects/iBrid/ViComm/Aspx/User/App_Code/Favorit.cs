﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using MobileLib;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Favorit:DbSession {

	public Favorit() {
	}


	public void FavoritMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pFavoritComment,int pDelFlag,int pUpdateCommentFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("FAVORIT_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("PFAVORIT_COMMENT",DbSession.DbType.VARCHAR2,pFavoritComment);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PUPDATE_COMMENT_FLAG",DbSession.DbType.NUMBER,pUpdateCommentFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	//ヤキモチ防止機能のメンテ　相手の情報がNULLの場合、一括変更とする。
	public void JealousyMainte(string pSiteCd, string pUserSeq, string pUserCharNo, string pPartnerUserSeq, string pPartnerUserCharNo, int pLoginViewStatus, int pWaitingViewStatus, int pBbsNonPublishFlag, int pRankingNonPublishFlag, int pNotSendBatchMailFlag, int pBlogNonPublishFlag, int pDiaryNonPublishFlag, int pProfilePicNonPublishFlag,int pUseCommentDetailFlag,string pFavoritGroupSeq,string pMailSendFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("JEALOUSY_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("PLOGIN_VIEW_STATUS",DbSession.DbType.NUMBER,pLoginViewStatus);
			db.ProcedureInParm("PWAITING_VIEW_STATUS",DbSession.DbType.NUMBER,pWaitingViewStatus);
			db.ProcedureInParm("PBBS_NON_PUBLISH_FLAG",DbSession.DbType.NUMBER,pBbsNonPublishFlag);
			db.ProcedureInParm("PRANKING_NON_PUBLISH_FLAG",DbSession.DbType.NUMBER,pRankingNonPublishFlag);
			db.ProcedureInParm("PNOT_SEND_BATCH_MAIL_FLAG",DbSession.DbType.NUMBER,pNotSendBatchMailFlag);
			db.ProcedureInParm("pBLOG_NON_PUBLISH_FLAG", DbSession.DbType.NUMBER, pBlogNonPublishFlag);
			db.ProcedureInParm("pDIARY_NON_PUBLISH_FLAG",DbSession.DbType.NUMBER,pDiaryNonPublishFlag);
			db.ProcedureInParm("pPROFILE_PIC_NON_PUBLISH_FLAG",DbSession.DbType.NUMBER,pProfilePicNonPublishFlag);
			db.ProcedureInParm("pUSE_COMMENT_DETAIL_FLAG",DbSession.DbType.NUMBER,pUseCommentDetailFlag);
			db.ProcedureInParm("pFAVORIT_GROUP_SEQ",DbSession.DbType.VARCHAR2,pFavoritGroupSeq);
			db.ProcedureInParm("pMAIL_SEND_FLAG",DbSession.DbType.VARCHAR2,pMailSendFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	public string GetFavoritComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;
		string sFavoritComment = "";
		try {
			conn = DbConnect("Favorit.GetFavoritComment");

			string sSql = "SELECT FAVORIT_COMMENT FROM T_FAVORIT " +
							"WHERE " +
								"SITE_CD				= :SITE_CD			AND " +
								"USER_SEQ				= :USER_SEQ			AND " +
								"USER_CHAR_NO			= :USER_CHAR_NO		AND " +
								"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ AND " +
								"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sFavoritComment = iBridUtil.GetStringValue(dr["FAVORIT_COMMENT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sFavoritComment;
	}

	//相手から見た時のｽﾃｰﾀｽを取得する

	public int GetIsSeenOnlineStatus(string sSiteCd,string pCastUserSeq,string pCastCharNo,string pManUserSeq,string pManCharNo,int pCastOnlineStatus) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		string sLoginViewStatus = string.Empty;
		string sWaitingViewStatus = string.Empty;

		int iIsSeenOnlineStatus;

		try {
			conn = DbConnect("Favorit.GetIsSeenPartner");

			StringBuilder oSql = new StringBuilder();

			oSql.Append("SELECT						").AppendLine();
			oSql.Append("	LOGIN_VIEW_STATUS	,	").AppendLine();
			oSql.Append("	WAITING_VIEW_STATUS		").AppendLine();
			oSql.Append("FROM						").AppendLine();
			oSql.Append("	T_FAVORIT				").AppendLine();
			oSql.Append("WHERE						").AppendLine();
			oSql.Append("	SITE_CD					= :SITE_CD			AND	").AppendLine();
			oSql.Append("	USER_SEQ				= :USER_SEQ			AND	").AppendLine();
			oSql.Append("	USER_CHAR_NO			= :USER_CHAR_NO		AND	").AppendLine();
			oSql.Append("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ AND	").AppendLine();
			oSql.Append("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	").AppendLine();

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",sSiteCd);
				cmd.Parameters.Add("USER_SEQ",pCastUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pCastCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pManUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pManCharNo);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sLoginViewStatus = iBridUtil.GetStringValue(dr["LOGIN_VIEW_STATUS"]);
						sWaitingViewStatus = iBridUtil.GetStringValue(dr["WAITING_VIEW_STATUS"]);
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		//Jealousy
		//0:Default 1:Offline 2:Logined
		if (bExist) {
			if (pCastOnlineStatus == ViCommConst.USER_OFFLINE) {
				iIsSeenOnlineStatus = pCastOnlineStatus;
			} else if (pCastOnlineStatus == ViCommConst.USER_LOGINED) {
				if (sLoginViewStatus.Equals("1")) {
					iIsSeenOnlineStatus = ViCommConst.USER_OFFLINE;
				} else {
					iIsSeenOnlineStatus = pCastOnlineStatus;
				}
			} else if (pCastOnlineStatus == ViCommConst.USER_WAITING || pCastOnlineStatus == ViCommConst.USER_TALKING) {
				if (sWaitingViewStatus.Equals("1")) {
					iIsSeenOnlineStatus = ViCommConst.USER_OFFLINE;
				} else if (sWaitingViewStatus.Equals("2")) {
					iIsSeenOnlineStatus = ViCommConst.USER_LOGINED;
				} else {
					iIsSeenOnlineStatus = pCastOnlineStatus;
				}
			} else {
				iIsSeenOnlineStatus = pCastOnlineStatus;
			}
		} else {
			iIsSeenOnlineStatus = pCastOnlineStatus;
		}

		return iIsSeenOnlineStatus;
	}

	public void LogingMailNotRxMainte(string pSiteCd, string pUserSeq, string pUserCharNo, string pPartnerUserSeq, string pPartnerUserCharNo, int pLoginMailNotRxFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LOGIN_MAIL_NOT_RX_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, pUserCharNo);
			oDbSession.ProcedureInParm("pPARTNER_USER_SEQ", DbSession.DbType.VARCHAR2, pPartnerUserSeq);
			oDbSession.ProcedureInParm("pPARTNER_USER_CHAR_NO", DbSession.DbType.VARCHAR2, pPartnerUserCharNo);
			oDbSession.ProcedureInParm("pLOGIN_MAIL_NOT_RX_FLAG", DbSession.DbType.NUMBER, pLoginMailNotRxFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}

	public int GetLoginMailNotRxFlag(string pSiteCd, string pUserSeq, string pUserCharNo, string pPartnerUserSeq, string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;

		try {
			conn = DbConnect();

			StringBuilder oSql = new StringBuilder();

			oSql.Append("SELECT						").AppendLine();
			oSql.Append("	LOGIN_MAIL_NOT_RX_FLAG	").AppendLine();
			oSql.Append("FROM						").AppendLine();
			oSql.Append("	T_FAVORIT				").AppendLine();
			oSql.Append("WHERE						").AppendLine();
			oSql.Append("	SITE_CD					= :SITE_CD			AND	").AppendLine();
			oSql.Append("	USER_SEQ				= :USER_SEQ			AND	").AppendLine();
			oSql.Append("	USER_CHAR_NO			= :USER_CHAR_NO		AND	").AppendLine();
			oSql.Append("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ AND	").AppendLine();
			oSql.Append("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	").AppendLine();

			using (cmd = CreateSelectCommand(oSql.ToString(), conn)) {
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.Add("USER_SEQ", pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO", pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ", pPartnerUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO", pPartnerUserCharNo);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						return int.Parse(iBridUtil.GetStringValue(dr["LOGIN_MAIL_NOT_RX_FLAG"]));
					}
				}
			}
		} finally {
			conn.Close();
		}

		return -1;
	}

	public string GetProfilePicNonPublishFlag(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;

		try {
			conn = DbConnect();

			StringBuilder oSql = new StringBuilder();

			oSql.Append("SELECT								").AppendLine();
			oSql.Append("	PROFILE_PIC_NON_PUBLISH_FLAG	").AppendLine();
			oSql.Append("FROM								").AppendLine();
			oSql.Append("	T_FAVORIT						").AppendLine();
			oSql.Append("WHERE								").AppendLine();
			oSql.Append("	SITE_CD					= :SITE_CD			AND	").AppendLine();
			oSql.Append("	USER_SEQ				= :USER_SEQ			AND	").AppendLine();
			oSql.Append("	USER_CHAR_NO			= :USER_CHAR_NO		AND	").AppendLine();
			oSql.Append("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ AND	").AppendLine();
			oSql.Append("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	").AppendLine();

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						return iBridUtil.GetStringValue(dr["PROFILE_PIC_NON_PUBLISH_FLAG"]);
					}
				}
			}
		} finally {
			conn.Close();
		}

		return "0";
	}

	public int GetSelfFavoritCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd) {
		int iUserCount = 0;
		string sCharacterTbl = string.Empty;

		if (pSexCd.Equals(ViCommConst.MAN)) {
			sCharacterTbl = "T_CAST_CHARACTER";
		} else {
			sCharacterTbl = "T_USER_MAN_CHARACTER";
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*) AS USER_COUNT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FAVORIT F,");
		oSqlBuilder.AppendFormat("	{0} C",sCharacterTbl).AppendLine();
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	F.SITE_CD		= :SITE_CD					AND");
		oSqlBuilder.AppendLine("	F.USER_SEQ		= :USER_SEQ					AND");
		oSqlBuilder.AppendLine("	F.USER_CHAR_NO	= :USER_CHAR_NO				AND");
		oSqlBuilder.AppendLine("	C.SITE_CD		= F.SITE_CD					AND");
		oSqlBuilder.AppendLine("	C.USER_SEQ		= F.PARTNER_USER_SEQ		AND");
		oSqlBuilder.AppendLine("	C.USER_CHAR_NO	= F.PARTNER_USER_CHAR_NO	AND");
		oSqlBuilder.AppendLine("	C.NA_FLAG		= :NA_FLAG					AND");
		oSqlBuilder.AppendLine("	NOT EXISTS(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			1");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_REFUSE R");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			R.SITE_CD				= F.SITE_CD					AND");
		oSqlBuilder.AppendLine("			R.USER_SEQ				= F.PARTNER_USER_SEQ		AND");
		oSqlBuilder.AppendLine("			R.USER_CHAR_NO			= F.PARTNER_USER_CHAR_NO	AND");
		oSqlBuilder.AppendLine("			R.PARTNER_USER_SEQ		= F.USER_SEQ				AND");
		oSqlBuilder.AppendLine("			R.PARTNER_USER_CHAR_NO	= F.USER_CHAR_NO");
		oSqlBuilder.AppendLine("	)");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count != 0) {
			iUserCount = int.Parse(oDataSet.Tables[0].Rows[0]["USER_COUNT"].ToString());
		}

		return iUserCount;
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	FG.FAVORIT_GROUP_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FAVORIT F,");
		oSqlBuilder.AppendLine("	T_FAVORIT_GROUP FG");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	F.SITE_CD				= :SITE_CD				AND");
		oSqlBuilder.AppendLine("	F.USER_SEQ				= :USER_SEQ				AND");
		oSqlBuilder.AppendLine("	F.USER_CHAR_NO			= :USER_CHAR_NO			AND");
		oSqlBuilder.AppendLine("	F.PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND");
		oSqlBuilder.AppendLine("	F.PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	AND");
		oSqlBuilder.AppendLine("	F.SITE_CD				= FG.SITE_CD (+)		AND");
		oSqlBuilder.AppendLine("	F.FAVORIT_GROUP_SEQ		= FG.FAVORIT_GROUP_SEQ (+)");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pPartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pPartnerUserCharNo));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
	
	//ソーシャルゲーム用ヤキモチ防止機能のメンテ　相手の情報がNULLの場合、一括変更とする。 
	public void GameJealousyMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,int pGamePicNonPublishFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GAME_JEALOUSY_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pGAME_PIC_NON_PUBLISH_FLAG",DbSession.DbType.NUMBER,pGamePicNonPublishFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}
	
	public bool CheckJealousyDialy(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	1														");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_FAVORIT												");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	DIARY_NON_PUBLISH_FLAG	= :DIARY_NON_PUBLISH_FLAG		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pPartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pPartnerUserCharNo));
		oParamList.Add(new OracleParameter(":DIARY_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public int GetJealousyOfflineCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FAVORIT");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	(LOGIN_VIEW_STATUS = 1 OR WAITING_VIEW_STATUS = 1)");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		iCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iCount;
	}
}
