﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾕｰｻﾞ定義ﾀｸﾞ

--	Progaram ID		: UserDefineTag
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Common;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class UserDefineTag:DbSession {
	public string htmlDoc;

	public UserDefineTag() {
	}

	public bool GetOne(string pSiteCd,string pVariableId) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect("SiteHtmlDoc.GetOne");

			string sSql = "SELECT * FROM T_USER_DEFINE_TAG WHERE SITE_CD = :SITE_CD AND VARIABLE_ID = :VARIABLE_ID";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("VARIABLE_ID",pVariableId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_DEFINE_TAG");

					if (ds.Tables["T_USER_DEFINE_TAG"].Rows.Count != 0) {
						dr = ds.Tables["T_USER_DEFINE_TAG"].Rows[0];
						htmlDoc = dr["HTML_DOC1"].ToString() 
							+ dr["HTML_DOC2"].ToString()
							+ dr["HTML_DOC3"].ToString()
							+ dr["HTML_DOC4"].ToString()
							+ dr["HTML_DOC5"].ToString()
							+ dr["HTML_DOC6"].ToString()
							+ dr["HTML_DOC7"].ToString()
							+ dr["HTML_DOC8"].ToString()
							+ dr["HTML_DOC9"].ToString()
							+ dr["HTML_DOC10"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	public bool GetUserDefineTagInfo(string pSiteCd,string pVariableId,string pAgentType,bool pDesignDebug) {
		DataSet ds;

		bool bFound = false;
		try {
			conn = DbConnect("SiteHtmlDoc.GetOne");

			string sSql = "SELECT * FROM T_USER_DEFINE_TAG WHERE SITE_CD = :SITE_CD AND VARIABLE_ID = :VARIABLE_ID ORDER BY SITE_CD,USER_AGENT_TYPE DESC";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("VARIABLE_ID",pVariableId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_DEFINE_TAG");

					foreach (DataRow dr in ds.Tables[0].Rows) {
						string sUserAgentType = dr["USER_AGENT_TYPE"].ToString();
						bool bEnableSmartPhoneDesign = pDesignDebug;
						if (!bEnableSmartPhoneDesign) {
							if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesign"]).Equals("1")) {
								if(!iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesignSiteCd"]).Equals(string.Empty)) {
									string[] oEnableSmartPhoneDesignSiteCd = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesignSiteCd"]).Split(',');
									foreach (string sTempSiteCd in oEnableSmartPhoneDesignSiteCd) {
										if (pSiteCd.Equals(sTempSiteCd)) {
											bEnableSmartPhoneDesign = true;
											break;
										}
									}
								} else {
									bEnableSmartPhoneDesign = true;
								}
							}
						}
						if (bEnableSmartPhoneDesign) {
							if (pAgentType.Equals(sUserAgentType)) {
								bFound = true;
							} else if ((pAgentType.Equals(ViCommConst.DEVICE_ANDROID) || (pAgentType.Equals(ViCommConst.DEVICE_IPHONE))) && sUserAgentType.Equals(ViCommConst.DEVICE_SMART_PHONE)) {
								bFound = true;
							} else if (sUserAgentType.Equals(ViCommConst.DEVICE_3G)) {
								bFound = true;
							}
						} else {
							if (sUserAgentType.Equals(ViCommConst.DEVICE_3G)) {
								bFound = true;
							}
						}

						if (bFound) {
							htmlDoc = dr["HTML_DOC1"].ToString()
								+ dr["HTML_DOC2"].ToString()
								+ dr["HTML_DOC3"].ToString()
								+ dr["HTML_DOC4"].ToString()
								+ dr["HTML_DOC5"].ToString()
								+ dr["HTML_DOC6"].ToString()
								+ dr["HTML_DOC7"].ToString()
								+ dr["HTML_DOC8"].ToString()
								+ dr["HTML_DOC9"].ToString()
								+ dr["HTML_DOC10"].ToString();
							break;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bFound;
	}
}
