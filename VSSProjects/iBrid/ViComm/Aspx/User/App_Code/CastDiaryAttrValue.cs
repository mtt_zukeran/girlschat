﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者日記属性
--	Progaram ID		: CastDiaryAttrValue
--
--  Creation Date	: 2011.08.29
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;

[System.Serializable]
public class CastDiaryAttrValue:DbSession {

	public string attrTypeNm;
	public string groupingCategoryCd;

    public CastDiaryAttrValue() {
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
            conn = DbConnect("CastDiaryAttrValue.GetList");

			string sSql = "SELECT " +
								"CAST_DIARY_ATTR_SEQ		, " +
								"CAST_DIARY_ATTR_NM         , " +
								"PRIORITY                     " +
							"FROM " +
								"T_CAST_DIARY_ATTR_VALUE " +
							"WHERE " +
								"SITE_CD	= :SITE_CD " +
							"ORDER BY " +
								"PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(ds, "T_CAST_DIARY_ATTR_VALUE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}