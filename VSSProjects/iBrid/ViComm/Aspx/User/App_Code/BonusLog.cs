﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ボーナス履歴
--	Progaram ID		: BonusLog
--
--  Creation Date	: 2009.10.14
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class BonusLog:DbSession {


	public decimal recCount;

	public BonusLog() {
	}


	public int GetPageCount(
		string pUserSeq,
		int pRecPerPage,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("BonusLog.GetPageCount");
			ds = new DataSet();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_BONUS_LOG ";
			string sWhere = "WHERE USER_SEQ = :USER_SEQ ";

			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {

				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				da.Fill(ds,"T_BONUS_LOG");

				if (ds.Tables["T_BONUS_LOG"].Rows.Count != 0) {
					dr = ds.Tables["T_BONUS_LOG"].Rows[0];
					recCount = Decimal.Parse(dr["ROW_COUNT"].ToString());
					pRecCount = recCount;
					iPages = (int)Math.Ceiling(recCount / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}

		return iPages;
	}

	public DataSet GetPageCollection(
		string pUserSeq,
		int pPageNo,
		int pRecPerPage) {

		DataSet ds;
		try {
			conn = DbConnect("BonusLog.GetPageCollection");
			ds = new DataSet();

			string sSql = "SELECT " +
								"* " +
							"FROM " +
								"( SELECT " +
									"BONUS_POINT	," +
									"BONUS_AMT		," +
									"REMARKS		," +
									"PAYMENT_FLAG	," +
									"PAYMENT_DATE	," +
									"CREATE_DATE	," +
									"ROW_NUMBER() OVER ( ORDER BY CREATE_DATE DESC ) AS RNUM " +
								"FROM " +
									"T_BONUS_LOG " +
								"WHERE " +
									"USER_SEQ = :USER_SEQ " +
								") " +
							"WHERE " +
								"RNUM >= :FIRST_ROW AND " +
								"RNUM <= :LAST_ROW ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BONUS_LOG");
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public void GetPaymentSum(string pUserSeq,string pBonusType,out int pPaymentAmt,out int pPaymentPoint) {
		string sNotPayExpireMonth;
		int iNotPayExpireMonth;
		string sReportDayTime;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			oManageCompany.GetValue("NOT_PAY_EXPIRE_MONTH",out sNotPayExpireMonth);
		}
		int.TryParse(sNotPayExpireMonth,out iNotPayExpireMonth);
		if (iNotPayExpireMonth != 0) {
			sReportDayTime = DateTime.Now.AddMonths(iNotPayExpireMonth * -1).ToString("yyyyMM") + "0100";
		} else {
			sReportDayTime = "1900010100";
		}

		DataSet ds;
		DataRow dr;
		try {
			conn = DbConnect("BonusLog.GetSum");
			ds = new DataSet();

			string sSql = "SELECT NVL(SUM(BONUS_AMT),0) AS TOTAL_BONUS_AMT,NVL(SUM(BONUS_POINT),0) AS TOTAL_BONUS_POINT FROM T_BONUS_LOG ";
			string sWhere = "WHERE USER_SEQ = :USER_SEQ AND PAYMENT_FLAG = :PAYMENT_FLAG AND REPORT_DAY_TIME>=:REPORT_DAY_TIME AND BONUS_POINT_TYPE = :BONUS_POINT_TYPE ";

			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {

				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("PAYMENT_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("REPORT_DAY_TIME",sReportDayTime);
				cmd.Parameters.Add("BONUS_POINT_TYPE",pBonusType);
				da.Fill(ds,"T_BONUS_LOG");

				dr = ds.Tables["T_BONUS_LOG"].Rows[0];
				int.TryParse(dr["TOTAL_BONUS_AMT"].ToString(),out pPaymentAmt);
				int.TryParse(dr["TOTAL_BONUS_POINT"].ToString(),out pPaymentPoint);

			}
		} finally {
			conn.Close();
		}
	}
}