﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using MobileLib;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class BingoCard:DbSession {
	public string[] bingoHitMask;
	public string[] bingoCardMask;
	public int bingoCardNo;
	public bool haveCard;
	private string[] bingoCardColorList;
	public string bingoCardColor;
	public int bingoCardColorNum;
	public int bingoEntrySeq;
	public string bingoStatus;
	public int bingoBallFlag;

	public BingoCard() {
		bingoHitMask = new string[25];
		bingoCardMask = new string[25];
		bingoCardNo = 0;
		haveCard = false;
		bingoCardColorList = new string[] { "#FFCC00", "#009900", "#0099FF", "#9933FF", "#FF0099", "#FF6600", "#006633", "#FF00FF", "#0066FF", "#FF3300" };
		bingoCardColorNum = 0;
		bingoBallFlag = 0;
	}

	public string GetBingoCard(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd) {
		string sResult = string.Empty;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_BINGO_CARD");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureOutArrayParm("pBINGO_CARD_MASK",DbSession.DbType.VARCHAR2,25);
			db.ProcedureOutArrayParm("pBINGO_HIT_MASK",DbSession.DbType.VARCHAR2,25);
			db.ProcedureOutParm("pBINGO_ENTRY_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pBINGO_CARD_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pBINGO_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pBINGO_BALL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
			if (sResult.Equals("0")) {
				haveCard = true;
				bingoStatus = db.GetStringValue("pBINGO_STATUS");
				bingoEntrySeq = db.GetIntValue("pBINGO_ENTRY_SEQ");
				bingoCardNo = db.GetIntValue("PBINGO_CARD_NO");
				bingoBallFlag = db.GetIntValue("pBINGO_BALL_FLAG");
				bingoCardColorNum = (bingoCardNo-1) % bingoCardColorList.Length;
				bingoCardColor = bingoCardColorList[bingoCardColorNum];
				for (int i = 0;i < 25;i++) {
					bingoCardMask[i] = db.GetArryStringValue("pBINGO_CARD_MASK",i);
					bingoHitMask[i] = db.GetArryStringValue("pBINGO_HIT_MASK",i);
				}
			} else {
				haveCard = false;
			}
		}
		return sResult;
	}

	public string GetBingoBall(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		string pMailSeq,
		string pPartnerMailSeq,
		out int pGetNewBallFlag,
		out int pGetOldBallFlag,
		out int pGetCardBallFlag,
		out int pGetBallNo,
		out int pCompliateFlag,
		out int pBingoBallFlag
		) {
		string sResult = string.Empty;
		pGetNewBallFlag = 0;
		pGetOldBallFlag = 0;
		pGetCardBallFlag = 0;
		pGetBallNo = 0;
		pCompliateFlag = 0;
		pBingoBallFlag = 0;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_BINGO_BALL");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pMAIL_SEQ",DbSession.DbType.VARCHAR2,pMailSeq);
			db.ProcedureInParm("pPARTNER_MAIL_SEQ",DbSession.DbType.VARCHAR2,pPartnerMailSeq);
			db.ProcedureOutParm("pGET_NEW_BALL_FLAG",DbSession.DbType.NUMBER);	//持っていないボールを獲得した 
			db.ProcedureOutParm("pGET_OLD_BALL_FLAG",DbSession.DbType.NUMBER);	//既に持っているボールを獲得した 
			db.ProcedureOutParm("pGET_CARD_BALL_FLAG",DbSession.DbType.NUMBER);	//カードにある番号のボールを獲得した 
			db.ProcedureOutParm("pGET_BALL_NO",DbSession.DbType.NUMBER);		//獲得したボールの番号 
			db.ProcedureOutParm("pCOMPLIATE_FLAG",DbSession.DbType.NUMBER);		//ビンゴが完成した
			db.ProcedureOutParm("pBINGO_BALL_FLAG",DbSession.DbType.NUMBER);	//BINGOボールを獲得した 
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);			//下記参照
			//'0';	--正常終了 
			//'1';	--既にビンゴが完成しています 
			//'2';	--カードを持っていません 
			//'3';	--メールから既に番号取得済み 
			//'4';	--ビンゴが開催されていません 
			//'5';	--添付をまだ送信していない 
			//'9';	--不正なメールSEQ 
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
			if (sResult.Equals("0")) {
				pGetNewBallFlag = db.GetIntValue("pGET_NEW_BALL_FLAG");
				pGetOldBallFlag = db.GetIntValue("pGET_OLD_BALL_FLAG");
				pGetCardBallFlag = db.GetIntValue("pGET_CARD_BALL_FLAG");
				pGetBallNo = db.GetIntValue("pGET_BALL_NO");
				pCompliateFlag = db.GetIntValue("pCOMPLIATE_FLAG");
				pBingoBallFlag = db.GetIntValue("pBINGO_BALL_FLAG");
			}
		}
		return sResult;

	}
}
