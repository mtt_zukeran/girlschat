﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

/// <summary>
/// Manager の概要の説明です




/// </summary>
[System.Serializable]
public class ErrorDtl:DbSession {


	public ErrorDtl() {
	}

	public string GetErrorDtl(string pSiteCd,string pErrorCd) {
		DataSet ds;
		DataRow dr;
		string sErrorDtl = "";
		try{
			conn = DbConnect("Error.GetErrorDtl");

			using (cmd = CreateSelectCommand("SELECT * FROM T_ERROR WHERE SITE_CD = :SITE_CD AND ERROR_CD =:ERROR_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ERROR_CD",pErrorCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ERROR");
					if (ds.Tables["T_ERROR"].Rows.Count != 0) {
						dr = ds.Tables["T_ERROR"].Rows[0];
						sErrorDtl = dr["ERROR_DTL"].ToString();
					} else {
						sErrorDtl = string.Format("未定義ﾒｯｾｰｼﾞ  ｻｲﾄ:{0} ｺｰﾄﾞ:{1}",pSiteCd,pErrorCd);
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sErrorDtl;
	}
}
