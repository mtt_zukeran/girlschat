﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using iBridCommLib;

using ViComm;

public partial class ParseWoman:ParseViComm {
	public virtual string PerserEx(string pTag,string pArgument,out bool pParsed) {
		string sValue = string.Empty;
		pParsed = true;
		UserWomanEx oUserWomanEx = null;
		if (sessionWoman.userWoman.CurCharacter != null) {
			oUserWomanEx = sessionWoman.userWoman.CurCharacter.characterEx;
		}

		switch (pTag) {
			case "$ROW_COUNT_OF_PAGE":
				sValue = GetRowCountOfPage();
				break;

			#region ---------- ブログ 関係 ------------------------------------

			case "$BLOG_PIC_SEND_ADDR":
				sValue = GetPictureUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_BLOG);
				break;
			case "$BLOG_MOVIE_SEND_ADDR":
				sValue = GetMovieUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_BLOG);
				break;
			case "$SELF_BLOG_SEQ":
				sValue = (oUserWomanEx != null) ? oUserWomanEx.blogSeq : string.Empty;
				break;
			case "$SELF_BLOG_TITLE":
				sValue = (oUserWomanEx != null) ? oUserWomanEx.blogTitle.Replace(Environment.NewLine,"<br />") : string.Empty;
				break;
			case "$SELF_BLOG_DOC":
				sValue = (oUserWomanEx != null) ? oUserWomanEx.blogDoc.Replace(Environment.NewLine,"<br />") : string.Empty;
				break;

			// ﾃﾞｻﾞｲﾝﾊﾟｰﾂ =======================
			case "$BLOG_PIC_UNUSED_CONF_PARTS":
				sValue = GetListBlogPicUnusedConf(pTag,pArgument,sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
				break;
			case "$BLOG_MOVIE_UNUSED_CONF_PARTS":
				sValue = GetListBlogMovieUnusedConf(pTag,pArgument,sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
				break;
			case "$BLOG_ARTICLE_CONF_PARTS":
				sValue = GetListBlogArticleConf(pTag,pArgument,sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
				break;
			case "$BLOG_ARTICLE_PARTS":
				sValue = GetListBlogArticle(pTag,pArgument,sessionWoman.site.siteCd);
				break;
			case "$BLOG_ARTICLE_PICKUP_PARTS":
				sValue = GetListBlogArticlePickup(pTag,pArgument,sessionWoman.site.siteCd);
				break;
			#endregion --------------------------------------------------------

			#region ---------- 日記 関係 ------------------------------------
			case "$CAST_DIARY_PARTS":
				sValue = GetOneCastDiary(pTag,pArgument);
				break;
			#endregion --------------------------------------------------------

			#region ---------- ｶﾞﾙﾁｬGAME 関係 ---------------------------------

			// ﾃﾞｻﾞｲﾝﾊﾟｰﾂ =======================
			case "$GAME_SCORE_PARTS":
				sValue = GetListGameScore(pTag,pArgument);
				break;

			#endregion --------------------------------------------------------

			#region ---------- しゃべり場 関係 ---------------------------------

			// ﾃﾞｻﾞｲﾝﾊﾟｰﾂ =======================
			case "$BBS_THREAD_PARTS":
				sValue = GetListBbsThread(pTag,pArgument);
				break;

			#endregion --------------------------------------------------------

			#region ---------- 待機ポイント関係 ----------
			//今日は待機ポイントを追加済 
			case "$DIV_IS_TODAY_ADD_WAITING_POINT":
				SetNoneDisplay(!IsTodayAddWaitingPoint());
				break;
			//今日はまだ待機ポイントを追加していない 
			case "$DIV_IS_NOT_TODAY_ADD_WAITING_POINT":
				SetNoneDisplay(IsTodayAddWaitingPoint());
				break;
			//今日は待機解除ポイントを追加済 
			case "$DIV_IS_TODAY_RELEASE_WAITING_POINT":
				SetNoneDisplay(!IsTodayReleaseWaitingPoint());
				break;
			//今日はまだ待機解除ポイントを追加していない 
			case "$DIV_IS_NOT_TODAY_RELEASE_WAITING_POINT":
				SetNoneDisplay(IsTodayReleaseWaitingPoint());
				break;
			#endregion

			#region ---------- RANKING関係 ----------
			case "$ONGOING_MARKING_RANKING_FLAG":
				//現在ランキング進行中だった場合1(ランキング終了から1時間は進行中としない)
				sValue = GetOngoingMarkingRankingFlag(pTag);
				break;
			case "$MARKING_RANKING_END_DATE":
				sValue = GetSelfRankingEndDate(pTag,ViCommConst.ExRanking.EX_RANKING_MARKING);
				break;
			case "$CREATE_MARKING_RANKING_LIST":
				sValue = CreateMarkingRankingSelect(pTag);
				break;
			case "$SELF_MARKING_COUNT":
				sValue = GetSelfRankingPoint(pTag,ViCommConst.ExRanking.EX_RANKING_MARKING);
				break;
			case "$SELF_MARKING_RANK":
				sValue = GetSelfRankingRank(pTag,ViCommConst.ExRanking.EX_RANKING_MARKING);
				break;
			case "$SELF_MOTE_CON_POINT":
				sValue = GetSelfRankingPoint(pTag,ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN);
				break;
			case "$SELF_MOTE_CON_RANK":
				sValue = GetSelfRankingRank(pTag,ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN);
				break;
			case "$MARKING_RANKING_NEXT_END_DATE":
				sValue = GetSelfRankingNextEndDate(pTag,ViCommConst.ExRanking.EX_RANKING_MARKING);
				break;
			#endregion

			#region ---------- この娘を探せ関係 ----------
			case "$SELF_WANTED_TARGET_REFUSE_FLAG":
				if (sessionWoman.userWoman.CurCharacter != null) {
					sValue = iBridUtil.GetStringValue(sessionWoman.userWoman.CurCharacter.characterEx.wantedTargetRefuseFlag);
				}
				break;
			case "$DIV_IS_NOT_PROFILE_COMPLETE":
				SetNoneDisplay(IsProfileComplete(pTag,pArgument));
				break;
			case "$YESTERDAY_TX_MAIL_COUNT":
				sValue = this.GetYesterdayMailCount(pTag);
				break;
			#endregion

			#region ---------- 10分で返事できる女の子関係 ----------
			case "$SELF_QUICK_RESPONSE_ROOM_NOW":
				sValue = (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.quickResFlag == 1 &&
						sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.quickResOutSchTime > DateTime.Now) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;
			case "$SELF_QUICK_RESPONSE_OUT_SCH_TIME":
				sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.quickResOutSchTime.ToString();
				break;
			case "$QUICK_RESPONSE_ROOM_IN_COUNT":
				if (sessionWoman.currentAspx.Equals("UserTop.aspx") && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCastUserTopDataSet"]).Equals("1")) {
					SetFieldValue(pTag,"QUICK_RESPONSE_ROOM_IN_COUNT",out sValue);
				} else {
					sValue = GetQuickResponseRoomInCount(pTag);
				}
				break;
			#endregion

			#region ---------- メールおねだり関係 ----------
			case "$SELF_MAIL_REQUEST_REFUSE_FLAG":
				if (sessionWoman.userWoman.CurCharacter != null) {
					sValue = iBridUtil.GetStringValue(sessionWoman.userWoman.CurCharacter.characterEx.mailRequestRefuseFlag);
				}
				break;
			#endregion
			
			#region ---------- PARTS ------------------------------------------
			case "$CAST_MOTECON_PARTS":
				sValue = GetListMoteCon(pTag,pArgument);
				break;
			case "$CAST_MARKING_RANK_PARTS":
				sValue = GetListMarkingRank(pTag,pArgument);
				break;
			case "$BEAUTY_CLOCK_PARTS":
				sValue = GetListBeautyClock(pTag,pArgument);
				break;
			#endregion --------------------------------------------------------




			default:
				#region ---------- データセット処理 ----------
				switch (this.currentTableIdx) {
					case ViCommConst.DATASET_EX_GAME:
						sValue = ParseGame(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_GAME_SCORE:
						sValue = ParseGameScore(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_BLOG_OBJ:
						sValue = ParseCast(pTag,pArgument,out pParsed);
						if (!pParsed) {
							sValue = ParseBlogObj(pTag,pArgument,out pParsed);
						}
						break;
					case ViCommConst.DATASET_EX_BLOG_ARTICLE:
						sValue = ParseCast(pTag,pArgument,out pParsed);
						if (!pParsed) {
							sValue = ParseBlogArticle(pTag,pArgument,out pParsed);
						}
						break;
					case ViCommConst.DATASET_EX_BBS_THREAD:
						sValue = ParseBbsThread(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_BBS_RES:
						sValue = ParseBbsRes(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_BEAUTY_CLOCK:
						sValue = this.ParseBeautyClock(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_BINGO_ENTRY:
						sValue = this.ParseBingoEntry(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_INTRODUCER_FRIEND:
						sValue = this.ParseIntroducerFriend(pTag,pArgument,out pParsed);
						break;
					default:
						pParsed = false;
						break;
				}
				break;
				#endregion
		}

		return sValue;
	}

	private bool IsTodayAddWaitingPoint() {
		return sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.waitingPointLastAddDay.Equals(DateTime.Today.ToString("yyyy/MM/dd"));
	}

	private bool IsTodayReleaseWaitingPoint() {
		return sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.waitingPointLastReleaseDay.Equals(DateTime.Today.ToString("yyyy/MM/dd"));
	}

	private string CreateMarkingRankingSelect(string pTag) {
		string sHtml = "<select name=\"ctlseq\">\r\n";
		string sSeq = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["ctlseq"]);

		using (RankingCtl oRankingCtl = new RankingCtl()) {
			DataSet ds;
			ds = oRankingCtl.GetList(sessionWoman.site.siteCd,ViCommConst.ExRanking.EX_RANKING_MARKING);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (sSeq.Equals(dr["RANKING_CTL_SEQ"].ToString())) {
					sHtml += string.Format("<option value=\"{0}\" selected>{1}</option>\r\n",dr["RANKING_CTL_SEQ"].ToString(),dr["SUMMARY_END_DAY"].ToString());
				} else {
					sHtml += string.Format("<option value=\"{0}\">{1}</option>\r\n",dr["RANKING_CTL_SEQ"].ToString(),dr["SUMMARY_END_DAY"].ToString());
				}
			}
		}
		return sHtml += "</select>";
	}

	private string GetSelfRankingPoint(string pTag,string pExRankingType) {
		string sSeq = string.Empty;
		if (pExRankingType.Equals(ViCommConst.ExRanking.EX_RANKING_MARKING)) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				sSeq = oRankingCtl.GetNowSeq(sessionWoman.site.siteCd,pExRankingType);
			}
		} else {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				sSeq = oRankingCtl.GetLastSeq(sessionWoman.site.siteCd,pExRankingType);
			}
		}
		using (RankingEx oRankingEx = new RankingEx()) {
			return oRankingEx.GetValueNow(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sSeq,pExRankingType,"SUMMARY_COUNT");
		}
	}

	private string GetSelfRankingRank(string pTag,string pExRankingType) {
		string sSeq = string.Empty;
		if (pExRankingType.Equals(ViCommConst.ExRanking.EX_RANKING_MARKING)) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				sSeq = oRankingCtl.GetNowSeq(sessionWoman.site.siteCd,pExRankingType);
			}
		} else {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				sSeq = oRankingCtl.GetLastSeq(sessionWoman.site.siteCd,pExRankingType);
			}
		}
		using (RankingEx oRankingEx = new RankingEx()) {
			return oRankingEx.GetRankNow(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sSeq,pExRankingType,ViCommConst.OPERATOR);
		}
	}

	private string GetSelfRankingEndDate(string pTag,string pExRankingType) {
		string sSeq = string.Empty;
		if (pExRankingType.Equals(ViCommConst.ExRanking.EX_RANKING_MARKING)) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				sSeq = oRankingCtl.GetNowSeq(sessionWoman.site.siteCd,pExRankingType);
			}
		} else {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				sSeq = oRankingCtl.GetLastSeq(sessionWoman.site.siteCd,pExRankingType);
			}
		}
		using (RankingCtl oRankingCtl = new RankingCtl()) {
			return oRankingCtl.GetSummaryEndDate(sSeq);
		}
	}
	private string GetSelfRankingNextEndDate(string pTag,string pExRankingType){
		string sSeq = string.Empty;
		using (RankingCtl oRankingCtl = new RankingCtl()) {
			sSeq = oRankingCtl.GetNextSeq(sessionWoman.site.siteCd,pExRankingType);
			return oRankingCtl.GetSummaryEndDate(sSeq);
		}
		
	}

	private string GetOngoingMarkingRankingFlag(string pTag) {
		using (RankingCtl oRankingCtl = new RankingCtl()) {
			if (oRankingCtl.GetOngoingMarkingRankingFlag(sessionWoman.site.siteCd,ViCommConst.ExRanking.EX_RANKING_MARKING)) {
				return ViCommConst.FLAG_ON_STR;
			} else {
				return ViCommConst.FLAG_OFF_STR;
			}
		}
	}

	private string GetQuickResponseRoomInCount(string pTag) {
		decimal iRecCount = 0;
		using (Cast oCast = new Cast()) {
			SeekCondition oSeekCondition = new SeekCondition();
			oSeekCondition.conditionFlag = ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE;
			oSeekCondition.siteCd = sessionWoman.site.siteCd;
			oSeekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
			oCast.GetPageCount(oSeekCondition,1,out iRecCount);
		}
		return iRecCount.ToString();
	}
	
	private string GetMyFanClubMemberRank(string pUserSeq) {
		string sValue = "0";
		FanClubStatusSeekCondition oCondition = new FanClubStatusSeekCondition();
		
		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			sValue = oFanClubStatus.GetMyFanClubMemberRank(
				sessionWoman.site.siteCd,
				pUserSeq,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo
			);
		}
		
		return sValue;
	}

	private string GetRowCountOfPage() {
		int iRowCountOfPage = 0;

		if (iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["list"]).Equals("1")) {
			int.TryParse(iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["lrows"]),out iRowCountOfPage);

			if (iRowCountOfPage == 0) {
				iRowCountOfPage = sessionWoman.site.mobileWomanListCount;
			}
		} else {
			int.TryParse(iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["drows"]),out iRowCountOfPage);

			if (iRowCountOfPage == 0) {
				iRowCountOfPage = sessionWoman.site.mobileWomanDetailListCount;
			}
		}

		return iRowCountOfPage.ToString();
	}

	private bool IsProfileComplete(string pTag,string pArgument) {
		bool bValue = true;
		
		string[] sExNoArr = pArgument.Split(',');

		int i = 0;
		int iIdx;

		Regex regex = new Regex("不明",RegexOptions.Compiled);
		Match rgxMatch;
		
		for (i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			
			iIdx = Array.IndexOf(sExNoArr,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].itemNo);
			
			if (iIdx == -1) {
				if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
					if (!string.IsNullOrEmpty(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrImputValue)) {
						bValue = false;
						break;
					}
				} else {
					if (string.IsNullOrEmpty(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrSeq)) {
						bValue = false;
						break;
					} else {
						rgxMatch = regex.Match(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrNm);
						if (rgxMatch.Success) {
							bValue = false;
							break;
						}
					}
				}
			}
		}

		if (string.IsNullOrEmpty(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].commentList)) {
			bValue = false;
		}
		
		return bValue;
	}
}
