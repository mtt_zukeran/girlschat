﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップオブジェクト
--	Progaram ID		: PickupObjs
--
--  Creation Date	: 2011.11.29
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using ViComm;
using iBridCommLib;

[System.Serializable]
public class PickupObjs : DbSession {
	public bool pickupFlag;
	public bool publishFlag;

	public PickupObjs() { }

	public bool IsExist(string pSiteCd,string pUserSeq,string pUserCharNo,string pPickupId,string pPickupType) {
		bool bExists = false;

		string sViewName = string.Empty;
		switch (pPickupType) {
			case ViCommConst.PicupTypes.PROFILE_PIC:
			case ViCommConst.PicupTypes.BBS_PIC:
				sViewName = "VW_PICKUP_OBJS01";
				break;
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.BBS_MOVIE:
				sViewName = "VW_PICKUP_OBJS02";
				break;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				sViewName = "VW_PICKUP_OBJS03";
				break;
			default:
				throw new NotSupportedException(string.Format("不正なピックアップ区分 {0}",pPickupType));
		}

		StringBuilder sSql = new StringBuilder();
		sSql.AppendLine("SELECT											");
		sSql.AppendLine("	PICKUP_FLAG								,	");
		sSql.AppendLine("	CASE										");
		sSql.AppendLine("	WHEN										");
		sSql.AppendLine("		(TO_DATE(PICKUP_START_PUB_DAY,'YYYY/MM/DD') <= SYSDATE	OR PICKUP_START_PUB_DAY IS NULL) AND");
		sSql.AppendLine("		(SYSDATE < TO_DATE(PICKUP_END_PUB_DAY,'YYYY/MM/DD')		OR PICKUP_END_PUB_DAY IS NULL)		");
		sSql.AppendLine("		THEN 1									");
		sSql.AppendLine("	ELSE	0	END AS PUBLISH_FLAG				");
		sSql.AppendLine("FROM											");
		sSql.AppendFormat("	{0}											",sViewName).AppendLine();
		sSql.AppendLine("WHERE											");
		sSql.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		sSql.AppendLine("	USER_SEQ			= :USER_SEQ			AND	");
		sSql.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO		AND	");
		sSql.AppendLine("	PICKUP_TYPE			= :PICKUP_TYPE		AND	");
		sSql.AppendLine("	PICKUP_FLAG			= :PICKUP_FLAG		AND	");
		sSql.AppendLine("	PICKUP_ID			= :PICKUP_ID			");
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add(":PICKUP_TYPE",pPickupType);
				cmd.Parameters.Add(":PICKUP_FLAG",ViCommConst.FLAG_ON);
				cmd.Parameters.Add(":PICKUP_ID",pPickupId);

				using (da = new OracleDataAdapter(cmd))
				using (DataSet ds = new DataSet()) {
					da.Fill(ds);

					if (ds.Tables[0].Rows.Count != 0) {
						bExists = true;
						foreach (DataRow oRow in ds.Tables[0].Rows) {
							pickupFlag = pickupFlag | iBridUtil.GetStringValue(oRow["PICKUP_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
							publishFlag = publishFlag | iBridUtil.GetStringValue(oRow["PUBLISH_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
						}
					}
				}

			}
		} finally {
			conn.Close();
		}

		return bExists;
	}

	/// <summary>
	/// オブジェクトSEQからピックアップ情報を取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pObjSeq"></param>
	/// <param name="pIsCastPublishOnly"></param>
	/// <returns></returns>
	public DataSet GetPickupInfoByObjSeq(string pSiteCd,string pObjSeq,bool pIsCastPublishOnly) {
		if (string.IsNullOrEmpty(pSiteCd) || string.IsNullOrEmpty(pObjSeq)) {
			return null;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 現在公開中のピックアップのみ対象
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	P.PICKUP_TITLE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_PICKUP P INNER JOIN T_PICKUP_OBJS O");
		oSqlBuilder.AppendLine("		ON O.SITE_CD = :SITE_CD AND O.OBJ_SEQ = :OBJ_SEQ AND P.PICKUP_ID = O.PICKUP_ID");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	P.PICKUP_FLAG = 1 AND P.PUBLISH_START_DATE <= SYSDATE AND P.PUBLISH_END_DATE >= SYSDATE");
		oSqlBuilder.AppendLine("		AND O.PICKUP_FLAG = 1 AND O.PICKUP_START_PUB_DAY <= SYSDATE AND O.PICKUP_END_PUB_DAY >= SYSDATE");
		// 出演者への公開対象のみ
		if (pIsCastPublishOnly) {
			oSqlBuilder.AppendLine("		AND P.CAST_PUBLISH_FLAG = 1");
		}
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	P.PICKUP_ID");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// ピックアップタイトルを連結
	/// </summary>
	/// <param name="pDataSet"></param>
	/// <param name="pSeparator"></param>
	/// <returns></returns>
	public string ConcatPickupTitle(DataSet pDataSet,string pSeparator) {
		if (pDataSet == null || pDataSet.Tables[0].Rows.Count <= 0) {
			return string.Empty;
		}

		StringBuilder sbTitle = new StringBuilder();
		string sPickupTitle = string.Empty;

		foreach (DataRow dr in pDataSet.Tables[0].Rows) {
			sbTitle.AppendFormat("{0}{1}",dr["PICKUP_TITLE"],pSeparator);
		}
		sPickupTitle = sbTitle.ToString();
		return sPickupTitle.Substring(0,sPickupTitle.Length - pSeparator.Length);
	}
}
