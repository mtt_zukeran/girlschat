﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャンペーン設定--	Progaram ID		: Campain
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

public class Campain:DbSession {

	public Campain() {
	}

	public string GetCurrentCampainCd(string pSiteCd) {
		DataSet ds;
		string sCampainCd = "";
		try {
			conn = DbConnect("Campain.GetCurrentCampain");
			ds = new DataSet();

			string sSql = "SELECT CAMPAIN_CD FROM T_CAMPAIN " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND APPLICATION_START_DATE <= :APPLICATION_START_DATE AND APPLICATION_END_DATE >= :APPLICATION_START_DATE AND CAMPAIN_CD != :CAMPAIN_CD " +
							"ORDER BY " +
								"SITE_CD,CAMPAIN_CD DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("APPLICATION_START_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				cmd.Parameters.Add("APPLICATION_END_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				cmd.Parameters.Add("CAMPAIN_CD","00");

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAMPAIN");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow dr = ds.Tables[0].Rows[0];
						sCampainCd = dr["CAMPAIN_CD"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sCampainCd;
	}

}