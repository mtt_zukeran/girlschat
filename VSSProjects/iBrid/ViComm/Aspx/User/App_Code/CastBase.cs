﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public enum FlexibleQueryType {
	/// <summary>なし</summary>
	None,
	/// <summary>包含</summary>
	InScope,
	/// <summary>除外</summary>
	NotInScope
}


[System.Serializable]
public class CastBase:DbSession {
	private SessionObjs _sessionObj = null;
	protected SessionObjs sessionObj {
		get {
			if (_sessionObj == null) {
				_sessionObj = SessionObjs.Current;
			}
			return _sessionObj;
		}
	}

	public CastBase()
		: this(SessionObjs.Current) {
	}
	public CastBase(SessionObjs pSessionObj) {
		this._sessionObj = pSessionObj;
	}

	protected void InnnerCondition(ref ArrayList pParmList) {
		SessionObjs oObj = sessionObj;
		string sUserSeq,sUserCharNo;
		if (oObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan sessionMan = (SessionMan)sessionObj;
			sUserSeq = sessionMan.userMan.userSeq;
			sUserCharNo = sessionMan.userMan.userCharNo;
		} else {
			SessionWoman sessionWoman = (SessionWoman)sessionObj;
			sUserSeq = sessionWoman.userWoman.userSeq;
			sUserCharNo = sessionWoman.userWoman.curCharNo;
		}
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ1",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO1",sUserCharNo));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ2",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO2",sUserCharNo));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ3",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO3",sUserCharNo));
		pParmList.Add(new OracleParameter("MOVIE_TYPE",ViCommConst.ATTACHED_MOVIE));
		pParmList.Add(new OracleParameter("OBJ_NA_FLAG1",ViCommConst.FLAG_OFF));
		pParmList.Add(new OracleParameter("VOICE_TYPE",ViCommConst.ATTACHED_VOICE));
		pParmList.Add(new OracleParameter("OBJ_NA_FLAG2",ViCommConst.FLAG_OFF));
		pParmList.Add(new OracleParameter("PIC_TYPE",ViCommConst.ATTACHED_PROFILE));
		pParmList.Add(new OracleParameter("OBJ_NA_FLAG3",ViCommConst.FLAG_OFF));
		pParmList.Add(new OracleParameter("PIC_TYPE1",ViCommConst.ATTACHED_PROFILE));
		pParmList.Add(new OracleParameter("OBJ_NA_FLAG4",ViCommConst.FLAG_OFF));
		pParmList.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));
		pParmList.Add(new OracleParameter("ADMIN_DEL_FLAG",ViCommConst.FLAG_OFF));
		pParmList.Add(new OracleParameter("REQUEST_ID",ViCommConst.REQUEST_VIEW_TALK));
		pParmList.Add(new OracleParameter("SESSION_CONNECT_FLAG",ViCommConst.FLAG_ON));
	}

	protected void JoinCondition(ref ArrayList pParmList) {
		SessionObjs oObj = sessionObj;
		string sUserSeq,sUserCharNo;
		if (oObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan sessionMan = (SessionMan)sessionObj;
			sUserSeq = sessionMan.userMan.userSeq;
			sUserCharNo = sessionMan.userMan.userCharNo;
		} else {
			SessionWoman sessionWoman = (SessionWoman)sessionObj;
			sUserSeq = sessionWoman.userWoman.userSeq;
			sUserCharNo = sessionWoman.userWoman.curCharNo;
		}
		pParmList.Add(new OracleParameter("LATEST_FLAG",ViCommConst.FLAG_ON));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ4",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO4",sUserCharNo));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ5",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO5",sUserCharNo));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ6",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO6",sUserCharNo));
		pParmList.Add(new OracleParameter("CODE_TYPE1","51"));
		pParmList.Add(new OracleParameter("CODE_TYPE2","61"));
		pParmList.Add(new OracleParameter("CODE_TYPE3","52"));
	}

	protected string CastBasicField() {
		StringBuilder sSql = new StringBuilder();
		sSql.Append("P.SITE_CD					,").AppendLine();
		sSql.Append("P.USER_SEQ					,").AppendLine();
		sSql.Append("P.USER_CHAR_NO				,").AppendLine();
		sSql.Append("P.LOGIN_ID					,").AppendLine();
		sSql.Append("P.CHARACTER_ONLINE_STATUS	,").AppendLine();
		sSql.Append("P.USER_STATUS				,").AppendLine();
		sSql.Append("P.CONNECT_TYPE				,").AppendLine();
		sSql.Append("P.HANDLE_NM				,").AppendLine();
		sSql.Append("P.HANDLE_KANA_NM			,").AppendLine();
		sSql.Append("P.COMMENT_LIST				,").AppendLine();
		sSql.Append("P.COMMENT_DETAIL			,").AppendLine();
		sSql.Append("P.COMMENT_ADMIN			,").AppendLine();
		sSql.Append("P.COMMENT_PICKUP			,").AppendLine();
		sSql.Append("P.WAITING_COMMENT			,").AppendLine();
		sSql.Append("P.START_PERFORM_DAY		,").AppendLine();
		sSql.Append("P.OK_PLAY_MASK				,").AppendLine();
		sSql.Append("P.LAST_ACTION_DATE			,").AppendLine();
		sSql.Append("P.LAST_LOGIN_DATE			,").AppendLine();
		sSql.Append("P.PRIORITY					,").AppendLine();
		sSql.Append("P.MONITOR_TALK_TYPE		,").AppendLine();
		sSql.Append("P.WSHOT_NA_FLAG			,").AppendLine();
		sSql.Append("P.MAIL_NA_FLAG				,").AppendLine();
		sSql.Append("P.USE_TERMINAL_TYPE		,").AppendLine();
		sSql.Append("P.REC_SEQ					,").AppendLine();
		sSql.Append("P.IVP_ACCEPT_SEQ			,").AppendLine();
		sSql.Append("P.TALK_LOCK_FLAG			,").AppendLine();
		sSql.Append("P.TALK_LOCK_DATE			,").AppendLine();
		sSql.Append("P.TALKING_FLAG				,").AppendLine();
		sSql.Append("P.USER_RANK				,").AppendLine();
		sSql.Append("P.ACT_CATEGORY_SEQ			,").AppendLine();
		sSql.Append("P.IVP_REQUEST_SEQ			,").AppendLine();
		sSql.Append("P.PROFILE_PIC_SEQ			,").AppendLine();
		sSql.Append("P.PROFILE_MOVIE_SEQ		,").AppendLine();
		sSql.Append("P.LAST_BBS_SEQ				,").AppendLine();
		sSql.Append("P.LAST_BBS_PIC_SEQ			,").AppendLine();
		sSql.Append("P.LAST_BBS_MOVIE_SEQ		,").AppendLine();
		sSql.Append("P.LAST_TALK_MOVIE_SEQ		,").AppendLine();
		sSql.Append("P.PICKUP_MASK				,").AppendLine();
		sSql.Append("GREATEST(NVL(P.LAST_BBS_SEQ,0),NVL(P.LAST_BBS_PIC_SEQ,0),NVL(P.LAST_BBS_MOVIE_SEQ,0)) AS MAX_BBS_SEQ,").AppendLine();
		sSql.Append("P.LOGIN_SEQ				,").AppendLine();
		sSql.Append("P.AGE						,").AppendLine();
		sSql.Append("P.MONITOR_ENABLE_FLAG		,").AppendLine();
		sSql.Append("P.QUICK_RES_FLAG			,").AppendLine();
		sSql.Append("P.QUICK_RES_IN_TIME		,").AppendLine();
		sSql.Append("P.QUICK_RES_OUT_TIME		,").AppendLine();
		sSql.Append("P.QUICK_RES_OUT_SCH_TIME	,").AppendLine();
		sSql.Append("P.CHARACTER_BIRTHDAY		,").AppendLine();
		sSql.Append("P.NA_FLAG					").AppendLine();
		return sSql.ToString();
	}

	protected virtual DataSet ExecuteTemplateQuery(string pInnerSql,List<OracleParameter> pParamList,string pJoinTables,string pJoinConditions,string pOuterFields) {
		// =================================================
		//  TEMPLATE 適用
		// =================================================
		ArrayList oTemplateParamList = new ArrayList();
		JoinCondition(ref oTemplateParamList);
		InnnerCondition(ref oTemplateParamList);
		pParamList.AddRange((OracleParameter[])oTemplateParamList.ToArray(typeof(OracleParameter)));

		string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
		string sSql = sessionObj.sqlSyntax[sTemplate].ToString();
		sSql = string.Format(sSql,pInnerSql,pJoinTables,pJoinConditions,pOuterFields);

		return ExecuteSelectQueryBase(sSql,pParamList.ToArray());
	}

	protected void FakeOnlineStatus(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();
		}
	}

	protected void AppendCastAttr(DataSet pDS) {

		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		string sSql = "SELECT DISPLAY_VALUE,ITEM_NO,ITEM_CD FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD	AND " +
						"USER_SEQ		= :USER_SEQ AND " +
						"USER_CHAR_NO	= :USER_CHAR_NO ";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
			col = new DataColumn(string.Format("CAST_ATTR_VALUE_CD{0:D2}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
				dr[string.Format("CAST_ATTR_VALUE_CD{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"VW_CAST_ATTR_VALUE01");
					}
				}
				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"].ToString())] = drSub["DISPLAY_VALUE"].ToString();
					dr[string.Format("CAST_ATTR_VALUE_CD{0}",drSub["ITEM_NO"].ToString())] = drSub["ITEM_CD"].ToString();
				}
			}
		}
	}


	protected int CalcExcludeCount(ref decimal pRowCount,decimal pExcludeRowCnt,int pRecPerPage) {
		pRowCount = pRowCount - pExcludeRowCnt;
		return (int)Math.Ceiling(pRowCount / pRecPerPage);
	}

	protected virtual string SetFlexibleQuery(
		FlexibleQueryTargetInfo pFlexibleQueryTarget,
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		ref ArrayList pList) {

		if (pFlexibleQueryTarget.QueryType == FlexibleQueryType.None) {
			return string.Empty;
		}

		StringBuilder sWhereBuilder = new StringBuilder();
		bool bNeedCheck = false;

		StringBuilder sWhereBaseBuidler = new StringBuilder();
		string sVoctor = (pFlexibleQueryTarget.QueryType == FlexibleQueryType.InScope) ? "EXISTS" : "NOT EXISTS";

		sWhereBaseBuidler.AppendLine();
		sWhereBaseBuidler.AppendFormat(" AND ( {0} (SELECT 1 FROM T_FAVORIT ",sVoctor).AppendLine();
		sWhereBaseBuidler.Append("	WHERE ");
		sWhereBaseBuidler.Append("		P.SITE_CD				= T_FAVORIT.SITE_CD				 AND ").AppendLine();
		sWhereBaseBuidler.Append(" 		P.USER_SEQ				= T_FAVORIT.USER_SEQ			 AND ").AppendLine();
		sWhereBaseBuidler.Append(" 		P.USER_CHAR_NO			= T_FAVORIT.USER_CHAR_NO		 AND ").AppendLine();
		sWhereBaseBuidler.Append(" 		:PARTNER_USER_SEQ		= T_FAVORIT.PARTNER_USER_SEQ	 AND ").AppendLine();
		sWhereBaseBuidler.Append("		:PARTNER_USER_CHAR_NO	= T_FAVORIT.PARTNER_USER_CHAR_NO AND ").AppendLine();
		sWhereBaseBuidler.Append("		{0} ").AppendLine();
		sWhereBaseBuidler.Append("	)) ").AppendLine();

		if (pFlexibleQueryTarget.JealousyProfilePic) {
			//1:プロフのみ表示
			//2:全て非表示
			string sWhereParts = "(:PROFILE_PIC_NON_PUBLISH_FLAG   = T_FAVORIT.PROFILE_PIC_NON_PUBLISH_FLAG OR (:PROFILE_PIC_NON_PUBLISH_FLAG = T_FAVORIT.PROFILE_PIC_NON_PUBLISH_FLAG AND P.PROFILE_PIC_FLAG = :PROFILE_PIC_FLAG) )";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);
			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("PROFILE_PIC_NON_PUBLISH_FLAG","2"));
			pList.Add(new OracleParameter("PROFILE_PIC_NON_PUBLISH_FLAG","1"));
			pList.Add(new OracleParameter("PROFILE_PIC_FLAG",ViCommConst.FLAG_OFF));			

			bNeedCheck = true;			
		}
		
		if (pFlexibleQueryTarget.JealousyBbs) {
			string sWhereParts = ":BBS_NON_PUBLISH_FLAG   = T_FAVORIT.BBS_NON_PUBLISH_FLAG";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON));

			bNeedCheck = true;
		}
		if (pFlexibleQueryTarget.JealousyBlog) {
			string sWhereParts = ":BLOG_NON_PUBLISH_FLAG   = T_FAVORIT.BLOG_NON_PUBLISH_FLAG";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("BLOG_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON));

			bNeedCheck = true;
		}
		if (pFlexibleQueryTarget.JealousyDiary) {
			string sWhereParts = ":DIARY_NON_PUBLISH_FLAG   = T_FAVORIT.DIARY_NON_PUBLISH_FLAG";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("DIARY_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON));

			bNeedCheck = true;
		}
		if (pFlexibleQueryTarget.JealousyRanking) {
			string sWhereParts = ":RANKING_NON_PUBLISH_FLAG   = T_FAVORIT.RANKING_NON_PUBLISH_FLAG";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("RANKING_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON));

			bNeedCheck = true;
		}
		if (pFlexibleQueryTarget.JealousyWaitingStatus) {
			string sWhereParts = " T_FAVORIT.WAITING_VIEW_STATUS IN (:STATUS1,:STATUS2) AND P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS1,:ONLINE_STATUS2) ";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("STATUS1","1"));	// Offline
			pList.Add(new OracleParameter("STATUS2","2"));	// Logined
			pList.Add(new OracleParameter("ONLINE_STATUS1",ViCommConst.USER_WAITING));
			pList.Add(new OracleParameter("ONLINE_STATUS2",ViCommConst.USER_TALKING));

			bNeedCheck = true;
		}
		if (pFlexibleQueryTarget.JealousyLoginStatus) {
			string sWhereParts = string.Empty;
			sWhereParts += " ( ";
			sWhereParts += " ( T_FAVORIT.WAITING_VIEW_STATUS = :STATUS1 AND P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS1,:ONLINE_STATUS2) AND (T_FAVORIT.OFFLINE_LAST_UPDATE_DATE < SYSDATE OR T_FAVORIT.OFFLINE_LAST_UPDATE_DATE IS NULL) )";
			sWhereParts += " OR ";
			sWhereParts += " ( T_FAVORIT.LOGIN_VIEW_STATUS = :STATUS2 AND P.CHARACTER_ONLINE_STATUS = :ONLINE_STATUS3 AND (T_FAVORIT.OFFLINE_LAST_UPDATE_DATE < SYSDATE OR T_FAVORIT.OFFLINE_LAST_UPDATE_DATE IS NULL) ) ";
			sWhereParts += " ) ";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("STATUS1","1"));	// Offline
			pList.Add(new OracleParameter("ONLINE_STATUS1",ViCommConst.USER_WAITING));
			pList.Add(new OracleParameter("ONLINE_STATUS2",ViCommConst.USER_TALKING));

			pList.Add(new OracleParameter("STATUS2","1"));	// Offline
			pList.Add(new OracleParameter("ONLINE_STATUS3",ViCommConst.USER_LOGINED));

			bNeedCheck = true;
		}
		if (pFlexibleQueryTarget.JealousySetOfflineIsVisible) {
			string sWhereParts = string.Empty;
			sWhereParts += " ( ";
			sWhereParts += " ( T_FAVORIT.WAITING_VIEW_STATUS = :STATUS1 AND (T_FAVORIT.OFFLINE_LAST_UPDATE_DATE < SYSDATE OR T_FAVORIT.OFFLINE_LAST_UPDATE_DATE IS NULL) )";
			sWhereParts += " OR ";
			sWhereParts += " ( T_FAVORIT.LOGIN_VIEW_STATUS = :STATUS2 AND (T_FAVORIT.OFFLINE_LAST_UPDATE_DATE < SYSDATE OR T_FAVORIT.OFFLINE_LAST_UPDATE_DATE IS NULL) ) ";
			sWhereParts += " ) ";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("STATUS1","1"));	// Offline
			pList.Add(new OracleParameter("STATUS2","1"));	// Offline

			bNeedCheck = true;
		}
		if (!string.IsNullOrEmpty(pFlexibleQueryTarget.JealousyLastActionDate)) {
			string sWhereParts = string.Empty;
			sWhereParts += " ( ";
			sWhereParts += " T_FAVORIT.LOGIN_VIEW_STATUS = :LOGIN_VIEW_STATUS ";
			sWhereParts += " AND ";
			sWhereParts += " T_FAVORIT.WAITING_VIEW_STATUS = :WAITING_VIEW_STATUS ";
			sWhereParts += " AND ";
			sWhereParts += " T_FAVORIT.OFFLINE_LAST_UPDATE_DATE >= :OFFLINE_LAST_UPDATE_DATE ";
			sWhereParts += " ) ";
			sWhereBuilder.AppendFormat(sWhereBaseBuidler.ToString(),sWhereParts);

			pList.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			pList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			pList.Add(new OracleParameter("LOGIN_VIEW_STATUS","1"));	// Offline
			pList.Add(new OracleParameter("WAITING_VIEW_STATUS","1"));	// Offline
			pList.Add(new OracleParameter("OFFLINE_LAST_UPDATE_DATE",OracleDbType.Date,DateTime.Parse(pFlexibleQueryTarget.JealousyLastActionDate),ParameterDirection.Input));
			bNeedCheck = true;
		}

		return bNeedCheck ? sWhereBuilder.ToString() : string.Empty;
	}

	protected DataSet Exclude(DataSet pSrcDataSet,int pNeedRowCount,FlexibleQueryTargetInfo pFlexibleQueryTarget) {
		DataSet oDestDataSet = new DataSet();
		oDestDataSet.Tables.Add(this.Exclude(pSrcDataSet.Tables[0],pNeedRowCount,pFlexibleQueryTarget));
		return oDestDataSet;
	}

	protected DataTable Exclude(DataTable pSrcDataTable,int pNeedRowCount,FlexibleQueryTargetInfo pFlexibleQueryTarget) {
		DataTable oDestDataTable = pSrcDataTable.Clone();

		foreach (DataRow oSrcRow in pSrcDataTable.Rows) {
			bool bIsExclude = false;

			bool bBbsNonPublishFlag = iBridUtil.GetStringValue(oSrcRow["BBS_NON_PUBLISH_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
			bool bRankingNonPublishFlag = iBridUtil.GetStringValue(oSrcRow["RANKING_NON_PUBLISH_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
			bool bBlogNonPublishFlag = iBridUtil.GetStringValue(oSrcRow["BLOG_NON_PUBLISH_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
			bool bDiaryNonPublishFlag = iBridUtil.GetStringValue(oSrcRow["DIARY_NON_PUBLISH_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
			string sLoginViewStatus = iBridUtil.GetStringValue(oSrcRow["LOGIN_VIEW_STATUS"]);
			string sWaitingViewStatus = iBridUtil.GetStringValue(oSrcRow["WAITING_VIEW_STATUS"]);
			string sOnlineStatus = iBridUtil.GetStringValue(oSrcRow["CHARACTER_ONLINE_STATUS"]);
			string sLastActionDate = iBridUtil.GetStringValue(oSrcRow["FAKE_LAST_ACTION_DATE"]);
			string sOfflineLastUpdateDate = iBridUtil.GetStringValue(oSrcRow["OFFLINE_LAST_UPDATE_DATE"]);
			string sProfilePicNonPublishFlag = iBridUtil.GetStringValue(oSrcRow["PROFILE_PIC_NON_PUBLISH_FLAG"]);
			string sPrifilePicFlag = string.Empty;
			if (pSrcDataTable.Columns.Contains("CUR_PIC_IS_PROFILE")) {
				sPrifilePicFlag = iBridUtil.GetStringValue(oSrcRow["CUR_PIC_IS_PROFILE"]);
			}

			// 掲示板非表示 
			if (pFlexibleQueryTarget.JealousyBbs) {
				bIsExclude = bIsExclude || bBbsNonPublishFlag;
			}
			// ランキング非表示 
			if (pFlexibleQueryTarget.JealousyRanking) {
				// 掲示板非表示 
				bIsExclude = bIsExclude || bRankingNonPublishFlag;
			}
			// ブログ非表示 
			if (pFlexibleQueryTarget.JealousyBlog) {
				bIsExclude = bIsExclude || bBlogNonPublishFlag;
			}
			// 日記非表示 
			if (pFlexibleQueryTarget.JealousyDiary) {
				bIsExclude = bIsExclude || bDiaryNonPublishFlag;
			}
			// 待機 
			if (pFlexibleQueryTarget.JealousyWaitingStatus) {
				if (!bIsExclude) {
					if (sOnlineStatus.Equals(ViCommConst.USER_WAITING.ToString()) || sOnlineStatus.Equals(ViCommConst.USER_TALKING.ToString())) {
						if (sWaitingViewStatus.Equals("1") || sWaitingViewStatus.Equals("2")) {
							bIsExclude = true;
						}
					}
				}
			}
			// ﾛｸﾞｲﾝ 
			if (pFlexibleQueryTarget.JealousyLoginStatus) {
				if (!bIsExclude) {
					if (sOnlineStatus.Equals(ViCommConst.USER_WAITING.ToString()) || sOnlineStatus.Equals(ViCommConst.USER_TALKING.ToString())) {
						if (sWaitingViewStatus.Equals("1")) {
							if (string.IsNullOrEmpty(sOfflineLastUpdateDate) || DateTime.Parse(sOfflineLastUpdateDate) < DateTime.Now) {
								bIsExclude = true;
							}
						}
					}
					if (sOnlineStatus.Equals(ViCommConst.USER_LOGINED.ToString())) {
						if (sLoginViewStatus.Equals("1")) {
							if (string.IsNullOrEmpty(sOfflineLastUpdateDate) || DateTime.Parse(sOfflineLastUpdateDate) < DateTime.Now) {
								bIsExclude = true;
							}
						}
					}
				}
			}
			// ｵﾌﾗｲﾝに設定している場合非表示
			if (pFlexibleQueryTarget.JealousySetOfflineIsVisible) {
				if (!bIsExclude) {
					if (sWaitingViewStatus.Equals("1")) {
						if (string.IsNullOrEmpty(sOfflineLastUpdateDate) || DateTime.Parse(sOfflineLastUpdateDate) < DateTime.Now) {
							bIsExclude = true;
						}
					}
					if (sLoginViewStatus.Equals("1")) {
						if (string.IsNullOrEmpty(sOfflineLastUpdateDate) || DateTime.Parse(sOfflineLastUpdateDate) < DateTime.Now) {
							bIsExclude = true;
						}
					}
				}
			}
			// 最終ｱｸｼｮﾝ時間にて非表示
			if (!string.IsNullOrEmpty(pFlexibleQueryTarget.JealousyLastActionDate)) {
				if (!bIsExclude) {
					if (sLastActionDate.CompareTo(pFlexibleQueryTarget.JealousyLastActionDate) <= 0) {
						bIsExclude = true;
					}
				}
			}
			// ﾌﾟﾛﾌ画像非表示
			if (pFlexibleQueryTarget.JealousyProfilePic) {
				if (!bIsExclude) {
					if (sProfilePicNonPublishFlag.Equals("1") && !sPrifilePicFlag.Equals("1")) {
						bIsExclude = true;
					} else if (sProfilePicNonPublishFlag.Equals("2")) {
						bIsExclude = true;
					}
				}
			}

			if (!bIsExclude) {
				oDestDataTable.ImportRow(oSrcRow);
				if (oDestDataTable.Rows.Count >= pNeedRowCount) {
					break;
				}
			}

		}

		return oDestDataTable;
	}

	protected virtual int GetFlexibleQueryBuffer() {
		int iValue;

		string sFlexibleQueryBuffer = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["FlexibleQueryBuffer"]);
		if (!int.TryParse(sFlexibleQueryBuffer,out iValue)) {
			iValue = 20;
		}

		return iValue;
	}

	[Serializable]
	public class FlexibleQueryTargetInfo {
		private FlexibleQueryType queryType = FlexibleQueryType.None;

		private bool jealousyBbs = false;
		private bool jealousyRanking = false;
		private bool jealousyLoginStatus = false;
		private bool jealousyWaitingStatus = false;
		private bool jealousyBlog = false;
		private bool jealousyDiary = false;
		private bool jealousySetOfflineIsVisible = false;
		private bool jealousyProfilePic = false;
		private string jealousyLastActionDate = string.Empty;

		public FlexibleQueryType QueryType {
			get {
				return this.queryType;
			}
		}


		public bool JealousyBbs {
			get {
				return this.jealousyBbs;
			}
			set {
				this.jealousyBbs = value;
			}
		}
		public bool JealousyRanking {
			get {
				return this.jealousyRanking;
			}
			set {
				this.jealousyRanking = value;
			}
		}
		public bool JealousyLoginStatus {
			get {
				return this.jealousyLoginStatus;
			}
			set {
				this.jealousyLoginStatus = value;
			}
		}
		public bool JealousyWaitingStatus {
			get {
				return this.jealousyWaitingStatus;
			}
			set {
				this.jealousyWaitingStatus = value;
			}
		}
		public bool JealousyBlog {
			get {
				return this.jealousyBlog;
			}
			set {
				this.jealousyBlog = value;
			}
		}
		public bool JealousyDiary {
			get {
				return this.jealousyDiary;
			}
			set {
				this.jealousyDiary = value;
			}
		}
		public bool JealousySetOfflineIsVisible {
			get {
				return this.jealousySetOfflineIsVisible;
			}
			set {
				this.jealousySetOfflineIsVisible = value;
			}
		}
		public bool JealousyProfilePic {
			get {
				return this.jealousyProfilePic;
			}
			set {
				this.jealousyProfilePic = value;
			}
		}
		public string JealousyLastActionDate {
			get {
				return this.jealousyLastActionDate;
			}
			set {
				this.jealousyLastActionDate = value;
			}
		}

		public bool HasTarget {
			get {
				bool bHasTarget = false;
				bHasTarget = bHasTarget || this.jealousyBbs;
				bHasTarget = bHasTarget || this.jealousyRanking;
				bHasTarget = bHasTarget || this.jealousyLoginStatus;
				bHasTarget = bHasTarget || this.jealousyWaitingStatus;
				bHasTarget = bHasTarget || this.jealousyBlog;
				bHasTarget = bHasTarget || this.jealousyDiary;
				bHasTarget = bHasTarget || this.jealousySetOfflineIsVisible;
				bHasTarget = bHasTarget || this.jealousyProfilePic;
				bHasTarget = bHasTarget || !string.IsNullOrEmpty(this.jealousyLastActionDate);
				return bHasTarget;
			}
		}


		public FlexibleQueryTargetInfo(FlexibleQueryType pQueryType) {
			this.queryType = pQueryType;
		}
	}

}
