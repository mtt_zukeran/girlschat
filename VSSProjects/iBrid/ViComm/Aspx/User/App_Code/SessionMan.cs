﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[System.Serializable]
public partial class SessionMan:SessionObjs {

	public UserMan userMan;
	public SessionWoman originalWomanObj;

	public bool usedEasyLoginFlag;
	public bool getTermIdFlag;
	public bool rakutenDebug = false;
	public string markingReferer = "none";
	public string payGameFlag = ViCommConst.FLAG_OFF_STR;

	public DataSet wantedSchedule = null;

	public virtual string actCategorySeq {
		get {
			if (currentCategoryIndex > 0) {
				return ((ActCategory)categoryList[currentCategoryIndex]).actCategorySeq;
			} else {
				return "";
			}
		}
	}
	public SessionMan() {
	}

	public SessionMan(string pHost,string pSessionId,string pCarrierCd,string pBrowserId,string pUserAgent,string pAdCd,NameValueCollection pQuery,bool pIsCrawler,bool pCookieLess)
		: base(pHost,pSessionId,pCarrierCd,pBrowserId,pUserAgent,pAdCd,pQuery,pIsCrawler,pCookieLess) {
		sexCd = ViCommConst.MAN;
		userMan = new UserMan();
		arAdNo = new string[10];
		iAdCnt = 0;
		originalWomanObj = null;
		using (ManageCompany oCompany = new ManageCompany()) {
			oCompany.GetOne();
			usedEasyLoginFlag = (oCompany.manUsedEasyLoginFlag == 1);
			getTermIdFlag = (oCompany.manGetTermIdFlag == 1);
		}
	}

	public HtmlFilter InitScreen(Stream pFilter,Form pForm,HttpRequest pRequest,StateBag pViewState) {

		string sUserRank = ViCommConst.DEFAUL_USER_RANK;
		string sAdGroupCd = ViCommConst.DEFAUL_AD_GROUP_CD;

		if (logined) {
			sAdGroupCd = userMan.adGroupCd;
			sUserRank = userMan.userRankCd;
		} else {
			if (adGroup != null) {
				sAdGroupCd = adGroup.adGroupCd;
			}
		}
		arAdNo = new string[10];
		iAdCnt = 0;

		return base.InitScreen(pFilter,pForm,pRequest,pViewState,sUserRank,sAdGroupCd);
	}

	public HtmlFilter InitScreen(Stream pFilter,Form pForm,HttpRequest pRequest,string pCurrentAspx,StateBag pViewState) {
		string sUserRank = ViCommConst.DEFAUL_USER_RANK;
		string sAdGroupCd = ViCommConst.DEFAUL_AD_GROUP_CD;

		if (logined) {
			sAdGroupCd = userMan.adGroupCd;
			sUserRank = userMan.userRankCd;
		} else {
			if (adGroup != null) {
				sAdGroupCd = adGroup.adGroupCd;
			}
		}
		arAdNo = new string[10];
		iAdCnt = 0;

		return base.InitScreen(pFilter,pForm,pRequest,pCurrentAspx,pViewState,sUserRank,sAdGroupCd);
	}

	public override string GetUserSeq() {
		string sUserSeq = null;
		if (this.userMan != null && !string.IsNullOrEmpty(this.userMan.userSeq)) {
			sUserSeq = this.userMan.userSeq;
		}
		return sUserSeq;
	}

	public bool SetCastDataSetByLoginId(string pLoginId,string pCharNo,int pCurrentPage) {
		DataRow dr;
		return SetCastDataSetByLoginId(pLoginId,pCharNo,pCurrentPage,out dr);
	}

	public bool SetCastDataSetByLoginId(string pLoginId,string pCharNo,int pCurrentPage,out DataRow pDr) {
		int iIdx;
		iIdx = ViCommConst.DATASET_CAST;
		DataSet dsDataSet = userMan.castCharacter.GetOneByLoginId(site.siteCd,pLoginId,pCharNo,pCurrentPage);
		ParseHTML oParseMan = this.parseContainer;
		if (dsDataSet.Tables[0].Rows.Count > 0) {
			oParseMan.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
			oParseMan.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
			oParseMan.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
			pDr = dsDataSet.Tables[0].Rows[0];
			return true;
		} else {
			oParseMan.currentRecPos[iIdx] = -1;
			oParseMan.parseUser.SetDataRow(iIdx,null);
			pDr = null;
			return false;
		}
	}

	public bool SetCastDataSetByUserSeq(string pUserSeq,string pCharNo,int pCurrentPage) {
		int iIdx;
		iIdx = ViCommConst.DATASET_CAST;

		ParseHTML oParseMan = this.parseContainer;
		using (Cast oCast = new Cast()) {
			DataSet dsDataSet = oCast.GetOne(site.siteCd,pUserSeq,pCharNo,pCurrentPage);
			if (dsDataSet.Tables[0].Rows.Count != 0) {
				oParseMan.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
				oParseMan.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
				oParseMan.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
				return true;
			} else {
				oParseMan.currentRecPos[iIdx] = -1;
				oParseMan.parseUser.SetDataRow(iIdx,null);
				return false;
			}
		}
	}

	public bool SetMailDataSetByMailSeq(string pMailSeq) {
		int iIdx;
		iIdx = ViCommConst.DATASET_MAIL;

		ParseHTML oParseMan = this.parseContainer;
		using (MailBox oMailBox = new MailBox()) {
			DataSet dsDataSet = oMailBox.GetPageCollectionBySeq(pMailSeq,site.siteCd,userMan.userSeq,userMan.userCharNo,ViCommConst.MAN,ViCommConst.MAIL_BOX_USER,ViCommConst.RX);
			if (dsDataSet.Tables[0].Rows.Count != 0) {
				oParseMan.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
				oParseMan.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
				oParseMan.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
				return true;
			} else {
				oParseMan.currentRecPos[iIdx] = -1;
				oParseMan.parseUser.SetDataRow(iIdx,null);
				return false;
			}
		}
	}

	public bool ControlList(
		HttpRequest pRequest,
		ulong pMode,
		Form pActiveForm
	) {
		DataSet ds;
		return base.ControlList(pRequest,pMode,pActiveForm,out ds);
	}

	protected override DataSet CreateListDataSet(HttpRequest pRequest,ulong pMode,out int pCurrentPage,out int pRowCountOfPage,out int pMaxPage) {
		int iTmp;
		bool bReCalcRNum = iBridUtil.GetStringValue(pRequest.QueryString["recalcrnum"]).ToString().Equals("1");
		string sLoginId = iBridUtil.GetStringValue(pRequest.QueryString["loginid"]);
		string sBeforeSystemId = iBridUtil.GetStringValue(pRequest.QueryString["beforesystemid"]);
		if (sLoginId.Equals(string.Empty) && !sBeforeSystemId.Equals(string.Empty)) {
			using (User oUser = new User()) {
				sLoginId = oUser.GetLoginIdByBeforeSystemId(sBeforeSystemId,ViCommConst.OPERATOR);
			}
		}
		string sAttrTypeSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]);
		string sSortType = iBridUtil.GetStringValue(pRequest.QueryString["sorttype"]);
		string sMovieSeries = iBridUtil.GetStringValue(pRequest.QueryString["movieseries"]);
		string sNotApproveStatus = string.Empty;
		bool bAllBbs = iBridUtil.GetStringValue(pRequest.QueryString["allbbs"]).Equals("1");
		string sOrder = iBridUtil.GetStringValue(pRequest.QueryString["order"]);
		bool bRandom = iBridUtil.GetStringValue(pRequest.QueryString["random"]).Equals(ViCommConst.FLAG_ON_STR);
		string sObjSeq,sPrevObjSeq,sNextObjSeq;
		int? iRondomNum = null;
		if (int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["randomnum"]),out iTmp)) {
			iRondomNum = iTmp;
		};
		seekConditionEx = null;


		string sCharNo = ViCommConst.MAIN_CHAR_NO;

		int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["rows"]),out pRowCountOfPage);
		if (iBridUtil.GetStringValue(pRequest.QueryString["list"]).Equals("1")) {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["lrows"]),out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileManListCount;
			}
		} else {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["drows"]),out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileManDetailListCount;
			}
		}

		pMaxPage = 1;

		parseContainer.parseUser.isExistPrevRec[parseContainer.parseUser.currentTableIdx] = false;
		parseContainer.parseUser.isExistNextRec[parseContainer.parseUser.currentTableIdx] = false;

		if (int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["pageno"]),out pCurrentPage) == false) {
			switch (currentAspx) {
				case "ViewDiary.aspx":
					if (int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["diaryrecno"]),out pCurrentPage) == false) {
						using (CastDiary oCastDiary = new CastDiary()) {
							pCurrentPage = oCastDiary.CollectionRNumByReportDay(site.siteCd,sLoginId,sCharNo,iBridUtil.GetStringValue(pRequest.QueryString["diaryday"]),iBridUtil.GetStringValue(pRequest.QueryString["subseq"]),true);
						}
					}
					break;

				case "ViewPic.aspx":
					int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["picrecno"]),out pCurrentPage);
					break;

				// Profile.aspxからの遷移時にRecNumにProfileのRNumが設定さているため、 
				// 検索時にRNumの対象範囲がずれているため。 
				case "ListPersonalPic.aspx":
				case "ListPersonalProfileMovie.aspx":
				case "ListPersonalDiary.aspx":
				case "ListPersonalBbsPic.aspx":
				case "ListPersonalBbsMovie.aspx":
				case "ListPersonalSaleMovie.aspx":
				case "ListPersonalSaleVoice.aspx":
				case "ListPersonalProfilePic.aspx":
				case "ListPersonalBbsObj.aspx":
					pCurrentPage = 1;
					break;

				default:
					int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),out pCurrentPage);
					break;
			}
			if (pCurrentPage == 0) {
				pCurrentPage = 1;
			}
		}

		double dwPages;
		DataSet ds = null;
		int iRNum = pCurrentPage;


		#region □ 商品動画関連処理 □ ------------------------------------------------------------------------------------------
		if (pMode >= ViCommConst.INQUIRY_PRODUCT && pMode <= ViCommConst.INQUIRY_PRODUCT_END) {


			ProductSeekCondition oCondition = new ProductSeekCondition(pRequest.QueryString);
			oCondition.SiteCd = site.siteCd;
			oCondition.SeekMode = pMode;
			oCondition.UserSeq = userMan.userSeq;
			oCondition.UserCharNo = userMan.userCharNo;
			if (!string.IsNullOrEmpty(oCondition.ProductId)) {
				using (Product oProduct = new Product()) {
					oCondition.ProductSeq = oProduct.ConvertProductId2Seq(oCondition.ProductId);
				}
				if (string.IsNullOrEmpty(oCondition.ProductSeq)) {
					throw new ApplicationException();
				}
			}

			switch (pMode) {
				case ViCommConst.INQUIRY_PRODUCT_MOVIE:
				case ViCommConst.INQUIRY_PRODUCT_MOVIE_BUYING_HIST:
				case ViCommConst.INQUIRY_PRODUCT_MOVIE_FAVORITE:
				case ViCommConst.INQUIRY_PRODUCT_MOVIE_WEELKY_RANKING:
				case ViCommConst.INQUIRY_PRODUCT_MOVIE_MONTHLY_RANKING: {
						using (ProductMovie oProductMovie = new ProductMovie()) {
							pMaxPage = oProductMovie.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oProductMovie.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
					}
					productSeekCondition = oCondition;
					break;
				case ViCommConst.INQUIRY_PRODUCT_PIC:
				case ViCommConst.INQUIRY_PRODUCT_PIC_BUYING_HIST:
				case ViCommConst.INQUIRY_PRODUCT_PIC_FAVORITE:
				case ViCommConst.INQUIRY_PRODUCT_PIC_WEELKY_RANKING:
				case ViCommConst.INQUIRY_PRODUCT_PIC_MONTHLY_RANKING: {
						using (ProductPic oProductPic = new ProductPic()) {
							pMaxPage = oProductPic.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oProductPic.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
					}
					productSeekCondition = oCondition;
					break;
				case ViCommConst.INQUIRY_PRODUCT_AUCTION:
				case ViCommConst.INQUIRY_PRODUCT_AUCTION_FAVORITE:
				case ViCommConst.INQUIRY_PRODUCT_AUCTION_ENTRY:
				case ViCommConst.INQUIRY_PRODUCT_AUCTION_WIN: {
						if (pMode == ViCommConst.INQUIRY_PRODUCT_AUCTION_ENTRY) {
							oCondition.IsEntered = ViCommConst.FLAG_ON_STR;
						}
						if (pMode == ViCommConst.INQUIRY_PRODUCT_AUCTION_WIN) {
							oCondition.IsWin = ViCommConst.FLAG_ON_STR;
						}

						using (ProductAuction oProductAuction = new ProductAuction()) {
							pMaxPage = oProductAuction.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oProductAuction.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
					}
					productSeekCondition = oCondition;
					break;
				case ViCommConst.INQUIRY_PRODUCT_AUCTION_BID_LOG: {
						using (ProductAuctionBidLog oProductAuctionBidLog = new ProductAuctionBidLog()) {
							pMaxPage = oProductAuctionBidLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oProductAuctionBidLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
					}
					productSeekCondition = oCondition;
					break;
				case ViCommConst.INQUIRY_PRODUCT_THEME:
				case ViCommConst.INQUIRY_PRODUCT_THEME_BUYING_HIST:
				case ViCommConst.INQUIRY_PRODUCT_THEME_FAVORITE: {
						using (ProductTheme oProductTheme = new ProductTheme()) {
							pMaxPage = oProductTheme.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oProductTheme.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
					}
					productSeekCondition = oCondition;
					break;
			}
		}
		#endregion --------------------------------------------------------------------------------------------------------------


		#region □ 拡張機能関連処理 □ ------------------------------------------------------------------------------------------
		if (pMode >= ViCommConst.INQUIRY_EXTENSION && pMode <= ViCommConst.INQUIRY_EXTENSION_END) {
			switch (pMode) {
				case ViCommConst.INQUIRY_EXTENSION_GAME: {
						GameSeekCondition oCondition = new GameSeekCondition(pRequest.Params);
						oCondition.SiteCd = site.siteCd;
						oCondition.SexCd = sexCd;
						oCondition.SeekMode = pMode;
						oCondition.UserSeq = userMan.userSeq;
						oCondition.UserCharNo = userMan.userCharNo;
						using (Game oGame = new Game()) {
							pMaxPage = oGame.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oGame.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
					}
					break;
				case ViCommConst.INQUIRY_EXTENSION_BLOG: {
						BlogSeekCondition oCondition = new BlogSeekCondition(pRequest.QueryString);
						oCondition.SeekMode = pMode;
						oCondition.SiteCd = site.siteCd;
						oCondition.UseTemplate = true;

						using (Blog oBlog = new Blog(this)) {
							pMaxPage = oBlog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oBlog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}

						seekConditionEx = oCondition;
					}
					sLoginId = string.Empty;
					break;
				case ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE:
				case ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP: {
						BlogArticleSeekCondition oCondition = new BlogArticleSeekCondition(pRequest.QueryString);
						oCondition.SeekMode = pMode;
						oCondition.SiteCd = site.siteCd;
						oCondition.UseTemplate = true;
						oCondition.BlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;

						switch (currentAspx) {
							case "ViewBlogArticle.aspx":
							case "ViewPersonalBlogArticle.aspx":
								pRowCountOfPage = 1;
								break;
						}
						switch (sOrder) {
							case ViCommConst.ORDER_PREVIOUS:
								oCondition.BlogArticleSeq = oCondition.PrevSeq;
								break;
							case ViCommConst.ORDER_NEXT:
								oCondition.BlogArticleSeq = oCondition.NextSeq;
								break;
						}
						oCondition.PrevSeq = string.Empty;
						oCondition.NextSeq = string.Empty;

						using (BlogArticle oBlogArticle = new BlogArticle(this)) {
							pMaxPage = oBlogArticle.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oBlogArticle.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}

						if (!string.IsNullOrEmpty(oCondition.BlogArticleSeq) && (!string.IsNullOrEmpty(oCondition.BlogSeq) || !string.IsNullOrEmpty(oCondition.UserSeq)) && ds.Tables[0].Rows.Count > 0) {
							DataRow oDataRow = ds.Tables[0].Rows[0];
							oCondition.PrevSeq = iBridUtil.GetStringValue(oDataRow["PREV_SEQ"]);
							oCondition.NextSeq = iBridUtil.GetStringValue(oDataRow["NEXT_SEQ"]);
							pCurrentPage = int.Parse(iBridUtil.GetStringValue(oDataRow["RNUM"]));
							parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_EX_BLOG_ARTICLE] = !oCondition.PrevSeq.Equals(string.Empty);
							parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_EX_BLOG_ARTICLE] = !oCondition.NextSeq.Equals(string.Empty);
						}
						seekConditionEx = oCondition;
					}
					break;
				case ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ: {
						BlogObjSeekCondition oCondition = new BlogObjSeekCondition(pRequest.QueryString);
						oCondition.SeekMode = pMode;
						oCondition.SiteCd = site.siteCd;
						oCondition.UseTemplate = true;
						oCondition.UsedFlag = ViCommConst.FLAG_ON_STR;
						oCondition.BlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;

						switch (currentAspx) {
							case "ViewBlogPic.aspx":
							case "ViewPersonalBlogPic.aspx":
							case "ListBlogPic.aspx":
							case "ListPersonalBlogPic.aspx":
								oCondition.FileType = ViCommConst.BlogFileType.PIC;
								break;
							case "ViewBlogMovie.aspx":
							case "ListBlogMovie.aspx":
							case "ViewPersonalBlogMovie.aspx":
							case "ListPersonalBlogMovie.aspx":
								oCondition.FileType = ViCommConst.BlogFileType.MOVIE;
								break;
						}

						// ページング前処理











						switch (sOrder) {
							case ViCommConst.ORDER_PREVIOUS:
								oCondition.ObjSeq = oCondition.PrevSeq;
								break;
							case ViCommConst.ORDER_NEXT:
								oCondition.ObjSeq = oCondition.NextSeq;
								break;
						}
						oCondition.PrevSeq = string.Empty;
						oCondition.NextSeq = string.Empty;


						// データ取得











						using (BlogObj oBlogObj = new BlogObj(this)) {
							pMaxPage = oBlogObj.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oBlogObj.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}

						// ページング後処理











						if (!string.IsNullOrEmpty(oCondition.ObjSeq) && ds.Tables[0].Rows.Count > 0) {
							DataRow oDataRow = ds.Tables[0].Rows[0];
							oCondition.PrevSeq = iBridUtil.GetStringValue(oDataRow["PREV_SEQ"]);
							oCondition.NextSeq = iBridUtil.GetStringValue(oDataRow["NEXT_SEQ"]);
							pCurrentPage = int.Parse(iBridUtil.GetStringValue(oDataRow["RNUM"]));
							parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_EX_BLOG_OBJ] = !oCondition.PrevSeq.Equals(string.Empty);
							parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_EX_BLOG_OBJ] = !oCondition.NextSeq.Equals(string.Empty);
						}

						seekConditionEx = oCondition;
					}
					break;
				case ViCommConst.INQUIRY_EXTENSION_BEAUTY_CLOCK: {
						BeautyClockSeekCondition oCondition = new BeautyClockSeekCondition(pRequest.QueryString);
						oCondition.SiteCd = site.siteCd;
						oCondition.SeekMode = pMode;
						oCondition.UseTemplate = true;
						using (BeautyClock oBeautyClock = new BeautyClock()) {
							pMaxPage = oBeautyClock.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oBeautyClock.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
						seekConditionEx = oCondition;
					}
					break;
			}


		}
		#endregion --------------------------------------------------------------------------------------------------------------

		if (pMode >= ViCommConst.INQUIRY_EXTENSION_PWILD && pMode <= ViCommConst.INQUIRY_EXTENSION_PWILD_END) {
			ds = CreateListDataSetEx(pRequest,pMode,out pCurrentPage,out pRowCountOfPage,out pMaxPage,out sLoginId);
		}

		switch (currentAspx) {
			case "ListBbsObj.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					seekCondition.unusedFlag = iBridUtil.GetStringValue(pRequest.QueryString["unused"]);
					seekCondition.rankType = iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]);
					seekCondition.bookmarkFlag = iBridUtil.GetStringValue(pRequest.QueryString["bkm"]);

					using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
						pMaxPage = oCastBbsObj.GetPageCount(site.siteCd,"","",ViCommConst.FLAG_OFF_STR,false,actCategorySeq,sAttrTypeSeq,sAttrSeq,bAllBbs,bOnlyUsed,seekCondition,pRowCountOfPage,true,out totalRowCount);
						ds = oCastBbsObj.GetPageCollection(site.siteCd,"","",ViCommConst.FLAG_OFF_STR,false,actCategorySeq,sAttrTypeSeq,sAttrSeq,bAllBbs,bOnlyUsed,seekCondition,pCurrentPage,pRowCountOfPage,true);
					}
				}
				break;

			case "ViewRichinoBbsObj.aspx":
			case "ViewBbsObj.aspx":
			case "ViewBbsObjFree.aspx":
				using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
					ds = oCastBbsObj.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["objseq"]),bAllBbs,true);
				}
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListCastBbs.aspx":
				seekCondition.InitCondtition();
				ExtendSeekCondition(seekCondition,pRequest);

				using (CastBbs oCastBbs = new CastBbs()) {
					pMaxPage = oCastBbs.GetPageCount(site.siteCd,sLoginId,sCharNo,pRowCountOfPage,true,out totalRowCount,seekCondition);
					ds = oCastBbs.GetPageCollection(site.siteCd,sLoginId,sCharNo,iRNum,pRowCountOfPage,true,seekCondition);
				}
				break;

			case "ViewCastBbs.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["bbsseq"]);
				using (CastBbs oCastBbs = new CastBbs()) {
					// Next/Previousガイダンスのためレコード位置再取得 
					if (this.enableViewPageControl) {
						oCastBbs.GetPrevNextBbsSeq(sObjSeq,site.siteCd,"","",true,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sObjSeq = sPrevObjSeq;
							} else {
								sObjSeq = sNextObjSeq;
							}
							oCastBbs.GetPrevNextBbsSeq(sObjSeq,site.siteCd,"","",true,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					ds = oCastBbs.GetPageCollectionBySeq(sObjSeq,true);
				}
				parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_PARTNER_BBS_DOC] = !sPrevObjSeq.Equals("");
				parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_PARTNER_BBS_DOC] = !sNextObjSeq.Equals("");
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListBbsPic.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					seekCondition.unusedFlag = iBridUtil.GetStringValue(pRequest.QueryString["unused"]);
					seekCondition.rankType = iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]);
					seekCondition.bookmarkFlag = iBridUtil.GetStringValue(pRequest.QueryString["bkm"]);
					using (CastPic oCastPic = new CastPic()) {
						pMaxPage = oCastPic.GetPageCount(site.siteCd,"","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pRowCountOfPage,out totalRowCount);
						ds = oCastPic.GetPageCollection(site.siteCd,"","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pCurrentPage,pRowCountOfPage);
					}
				}
				break;

			case "ViewBbsPic.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					sPrevObjSeq = "";
					sNextObjSeq = "";
					using (CastPic oCastPic = new CastPic()) {
						sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
						// Next/Previousガイダンスのためレコード位置再取得 
						if (this.enableViewPageControl) {
							oCastPic.GetPrevNextPicSeq(sObjSeq,site.siteCd,"","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,out sPrevObjSeq,out sNextObjSeq);
							if (!sOrder.Equals(string.Empty)) {
								if (sOrder == ViCommConst.ORDER_PREVIOUS) {
									sObjSeq = sPrevObjSeq;
								} else {
									sObjSeq = sNextObjSeq;
								}
								oCastPic.GetPrevNextPicSeq(sObjSeq,site.siteCd,"","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,out sPrevObjSeq,out sNextObjSeq);
							}
						}
						ds = oCastPic.GetPageCollectionBySeq(sObjSeq);
					}

					parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_PARTNER_PIC] = !sPrevObjSeq.Equals("");
					parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_PARTNER_PIC] = !sNextObjSeq.Equals("");
					pRowCountOfPage = 1;
					pMaxPage = ds.Tables[0].Rows.Count;
				}
				break;


			case "ListGallery.aspx":
				using (CastPic oCastPic = new CastPic()) {
					seekCondition.InitCondtition();
					seekCondition = ExtendSeekCondition(seekCondition,pRequest);
					seekCondition.rankType = iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]);
					pMaxPage = oCastPic.GetPageCount(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,seekCondition,iRNum,pRowCountOfPage);
				}
				break;

			case "ViewGallery.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				using (CastPic oCastPic = new CastPic()) {
					sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["picseq"]);
					// Next/Previousガイダンスのためレコード位置再取得 
					if (this.enableViewPageControl) {
						oCastPic.GetPrevNextPicSeq(sObjSeq,site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,false,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sObjSeq = sPrevObjSeq;
							} else {
								sObjSeq = sNextObjSeq;
							}
							oCastPic.GetPrevNextPicSeq(sObjSeq,site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,false,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					ds = oCastPic.GetPageCollectionBySeq(sObjSeq);
				}

				parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_PARTNER_PIC] = !sPrevObjSeq.Equals("");
				parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_PARTNER_PIC] = !sNextObjSeq.Equals("");
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListBbsMovie.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					seekCondition.unusedFlag = iBridUtil.GetStringValue(pRequest.QueryString["unused"]);
					seekCondition.rankType = iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]);
					seekCondition.bookmarkFlag = iBridUtil.GetStringValue(pRequest.QueryString["bkm"]);
					using (CastMovie oCastMovie = new CastMovie()) {
						pMaxPage = oCastMovie.GetPageCount(site.siteCd,"","","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pRowCountOfPage,true,out totalRowCount);
						ds = oCastMovie.GetPageCollection(site.siteCd,"","","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
					}
				}
				break;

			case "ViewBbsMovie.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					sPrevObjSeq = "";
					sNextObjSeq = "";
					using (CastMovie oCastMovie = new CastMovie()) {
						sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
						// Next/Previousガイダンスのためレコード位置再取得 
						if (this.enableViewPageControl) {
							oCastMovie.GetPrevNextMovieSeq(site.siteCd,"","",sObjSeq,false,false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,"",bOnlyUsed,seekCondition,ViCommConst.SORT_NO,true,out sPrevObjSeq,out sNextObjSeq);
							if (!sOrder.Equals(string.Empty)) {
								if (sOrder == ViCommConst.ORDER_PREVIOUS) {
									sObjSeq = sPrevObjSeq;
								} else {
									sObjSeq = sNextObjSeq;
								}
								oCastMovie.GetPrevNextMovieSeq(site.siteCd,"","",sObjSeq,false,false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,"",bOnlyUsed,seekCondition,ViCommConst.SORT_NO,true,out sPrevObjSeq,out sNextObjSeq);
							}
						}
						ds = oCastMovie.GetPageCollectionBySeq(sObjSeq,true);
					}
					parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_PARTNER_MOVIE] = !sPrevObjSeq.Equals("");
					parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_PARTNER_MOVIE] = !sNextObjSeq.Equals("");
					pRowCountOfPage = 1;
					pMaxPage = ds.Tables[0].Rows.Count;
					break;
				}

			case "ListGameMovie.aspx": {
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					using (CastMovie oCastMovie = new CastMovie()) {
						pMaxPage = oCastMovie.GetPageCount(site.siteCd,string.Empty,string.Empty,string.Empty,false,ViCommConst.ATTACHED_SOCIAL_GAME.ToString(),actCategorySeq,string.Empty,string.Empty,bOnlyUsed,pRowCountOfPage,true,out totalRowCount);
						ds = oCastMovie.GetPageCollection(site.siteCd,string.Empty,string.Empty,string.Empty,false,ViCommConst.ATTACHED_SOCIAL_GAME.ToString(),actCategorySeq,string.Empty,string.Empty,bOnlyUsed,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
					}
				}
				break;

			case "ViewGameMovie.aspx": {
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					sPrevObjSeq = string.Empty;
					sNextObjSeq = string.Empty;
					using (CastMovie oCastMovie = new CastMovie()) {
						sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
						// Next/Previousガイダンスのためレコード位置再取得 
						if (this.enableViewPageControl) {
							oCastMovie.GetPrevNextMovieSeq(site.siteCd,string.Empty,string.Empty,sObjSeq,false,false,ViCommConst.ATTACHED_SOCIAL_GAME.ToString(),actCategorySeq,string.Empty,string.Empty,string.Empty,bOnlyUsed,ViCommConst.SORT_NO,true,out sPrevObjSeq,out sNextObjSeq);
							if (!sOrder.Equals(string.Empty)) {
								if (sOrder == ViCommConst.ORDER_PREVIOUS) {
									sObjSeq = sPrevObjSeq;
								} else {
									sObjSeq = sNextObjSeq;
								}
								oCastMovie.GetPrevNextMovieSeq(site.siteCd,string.Empty,string.Empty,sObjSeq,false,false,ViCommConst.ATTACHED_SOCIAL_GAME.ToString(),actCategorySeq,string.Empty,string.Empty,string.Empty,bOnlyUsed,ViCommConst.SORT_NO,true,out sPrevObjSeq,out sNextObjSeq);
							}
						}
						ds = oCastMovie.GetPageCollectionBySeq(sObjSeq,true);
					}
					parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_PARTNER_MOVIE] = !sPrevObjSeq.Equals(string.Empty);
					parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_PARTNER_MOVIE] = !sNextObjSeq.Equals(string.Empty);
					pRowCountOfPage = 1;
					pMaxPage = ds.Tables[0].Rows.Count;
					break;
				}
			case "ListUserBbsConf.aspx":
				using (UserManBbs oUserManBbs = new UserManBbs()) {
					pMaxPage = oUserManBbs.GetPageCount(site.siteCd,userMan.loginId,userMan.userCharNo,pRowCountOfPage,out totalRowCount);
					ds = oUserManBbs.GetPageCollection(site.siteCd,userMan.loginId,userMan.userCharNo,iRNum,pRowCountOfPage);
				}
				break;

			case "ViewUserBbsConf.aspx":
				using (UserManBbs oUserManBbs = new UserManBbs()) {
					ds = oUserManBbs.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["bbsseq"]));
				}
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListAdminReport.aspx":
				using (AdminReport oAdminReport = new AdminReport()) {
					pMaxPage = oAdminReport.GetPageCount(site.siteCd,ViCommConst.MAN,pRowCountOfPage,out totalRowCount);
					ds = oAdminReport.GetPageCollection(site.siteCd,ViCommConst.MAN,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ViewAdminReport.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["docseq"]);
				using (AdminReport oAdminReport = new AdminReport()) {
					// Next/Previousガイダンスのためレコード位置再取得 
					if (this.enableViewPageControl) {
						oAdminReport.GetPrevNextDocSeq(sObjSeq,site.siteCd,ViCommConst.MAN,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sObjSeq = sPrevObjSeq;
							} else {
								sObjSeq = sNextObjSeq;
							}
							oAdminReport.GetPrevNextDocSeq(sObjSeq,site.siteCd,ViCommConst.MAN,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					ds = oAdminReport.GetPageCollectionBySeq(sObjSeq);
				}
				parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_ADMIN_REPORT] = !sPrevObjSeq.Equals("");
				parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_ADMIN_REPORT] = !sNextObjSeq.Equals("");
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListPersonalProfileMovie.aspx":
				using (CastMovie oCastMovie = new CastMovie()) {
					string sMovieSeq = iBridUtil.GetStringValue(pRequest.QueryString["directmovie"]);
					string sDirect = iBridUtil.GetStringValue(pRequest.QueryString["direct"]);
					if (sDirect.Equals("1")) {
						ds = oCastMovie.GetPageCollectionBySeq(sMovieSeq,true);
						pRowCountOfPage = 1;
						pMaxPage = ds.Tables[0].Rows.Count;
					} else {
						pMaxPage = oCastMovie.GetPageCount(site.siteCd,sLoginId,sCharNo,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"","",pRowCountOfPage,true,out totalRowCount);
						ds = oCastMovie.GetPageCollection(site.siteCd,sLoginId,sCharNo,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"","",ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
					}
				}
				break;

			case "ListProfileMovieObj.aspx":
				using (CastMovie oCastMovie = new CastMovie()) {
					seekCondition.InitCondtition();
					seekCondition.rankType = iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]);
					pMaxPage = oCastMovie.GetPageCount(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,true,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",sAttrTypeSeq,sAttrSeq,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
				}
				break;

			case "ListPersonalProfilePic.aspx":
				using (CastPic oCastPic = new CastPic()) {
					pMaxPage = oCastPic.GetPageCount(site.siteCd,sLoginId,sCharNo,false,ViCommConst.ATTACHED_PROFILE.ToString(),"",sAttrTypeSeq,sAttrSeq,pRowCountOfPage,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,sLoginId,sCharNo,false,ViCommConst.ATTACHED_PROFILE.ToString(),"",sAttrTypeSeq,sAttrSeq,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ViewPersonalProfilePic.aspx":
				using (CastPic oCastPic = new CastPic()) {
					ds = oCastPic.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["picseq"]));
				}
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ViewPersonalProfileMovie.aspx":
				using (CastMovie oCastPic = new CastMovie()) {
					ds = oCastPic.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["objseq"]),true);
				}
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListUserMailBox.aspx":
			case "ListInfoMailBox.aspx":
			case "ListBlogMailBox.aspx":
			case "ListTxMailBox.aspx":
			case "ListMailHistory.aspx":
				using (MailBox oMailBox = new MailBox()) {
					string sMailSearchKey = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["search_key"]));
					string sMailReadFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["read_flag"]));
					string sMailReturnFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["return_flag"]));
					string sDelProtectFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["prt_flg"]));
					string sExistObjFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["obj_flag"]));
					string sTxOnlyFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["tx_only"]));
					
					int? iMailReadFlag = null;
					int? iMailReturnFlag = null;
					if (!sMailReadFlag.Equals("")) {
						iMailReadFlag = int.Parse(sMailReadFlag);
					}
					if (!sMailReturnFlag.Equals("")) {
						iMailReturnFlag = int.Parse(sMailReturnFlag);
					}
					string sMailBoxType = "";
					string sTxRxType = "";
					switch (currentAspx) {
						case "ListUserMailBox.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.RX;
							break;
						case "ListInfoMailBox.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_INFO;
							sTxRxType = ViCommConst.RX;
							break;
						case "ListBlogMailBox.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_BLOG;
							sTxRxType = ViCommConst.RX;
							break;
						case "ListTxMailBox.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TX;
							break;
						case "ListMailHistory.aspx":
							seekCondition.partnerLoginId = "";
							seekCondition.handleNm = iBridUtil.GetStringValue(pRequest.QueryString["handle"]);
							sMailSearchKey = string.Empty;
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TXRX;
							break;
					}
					pMaxPage = oMailBox.GetPageCount(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,sexCd,sMailBoxType,sTxRxType,true,sMailSearchKey,iMailReadFlag,iMailReturnFlag,sDelProtectFlag,sExistObjFlag,sTxOnlyFlag,pRowCountOfPage,out totalRowCount);
					ds = oMailBox.GetPageCollection(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,sexCd,sMailBoxType,sTxRxType,true,sMailSearchKey,iMailReadFlag,iMailReturnFlag,true,sDelProtectFlag,sExistObjFlag,sTxOnlyFlag,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ViewBlogMail.aspx":
			case "ViewUserMail.aspx":
			case "ViewInfoMail.aspx":
			case "ViewTxMail.aspx":
			case "ViewMailHistory.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";

				using (MailBox oMailBox = new MailBox()) {
					string sMailSeq = iBridUtil.GetStringValue(pRequest.QueryString["mailseq"]);
					string sMailSearchKey = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["search_key"]));
					string sMailBoxType = "";
					string sTxRxType = "";

					switch (currentAspx) {
						case "ViewBlogMail.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_BLOG;
							sTxRxType = ViCommConst.RX;
							break;
						case "ViewUserMail.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.RX;
							break;
						case "ViewInfoMail.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_INFO;
							sTxRxType = ViCommConst.RX;
							break;
						case "ViewTxMail.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TX;
							break;
						case "ViewMailHistory.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TXRX;
							break;
					}

					if (this.enableViewPageControl) {
						oMailBox.GetPrevNextMailSeq(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,sexCd,sMailBoxType,sTxRxType,true,sMailSeq,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sMailSeq = sPrevObjSeq;
							} else {
								sMailSeq = sNextObjSeq;
							}
							oMailBox.GetPrevNextMailSeq(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,sexCd,sMailBoxType,sTxRxType,true,sMailSeq,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_MAIL] = !sPrevObjSeq.Equals("");
					parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_MAIL] = !sNextObjSeq.Equals("");
					pRowCountOfPage = 1;
					ds = oMailBox.GetPageCollectionBySeq(sMailSeq,site.siteCd,userMan.userSeq,userMan.userCharNo,ViCommConst.MAN,sMailBoxType,sTxRxType);
					pMaxPage = ds.Tables[0].Rows.Count;
				}
				break;


			case "ListPersonalLongMovie.aspx":
				using (TalkMovie oTalkMovie = new TalkMovie()) {
					ds = oTalkMovie.GetCastMovieListByLoginId(site.siteCd,sLoginId,sCharNo,"");
				}
				dwPages = ds.Tables[0].Rows.Count / (double)pRowCountOfPage;
				pMaxPage = (int)Math.Ceiling(dwPages);
				totalRowCount = ds.Tables[0].Rows.Count;
				break;

			case "ListMoviePart.aspx":
				string sTalkMovieSeq = iBridUtil.GetStringValue(pRequest.QueryString["movieseq"]);
				using (TalkMovie oTalkMovie = new TalkMovie()) {
					ds = oTalkMovie.GetPartList(site.siteCd,sTalkMovieSeq);
				}
				dwPages = ds.Tables[0].Rows.Count / (double)pRowCountOfPage;
				pMaxPage = (int)Math.Ceiling(dwPages);
				totalRowCount = ds.Tables[0].Rows.Count;
				break;

			case "ListLongMovie.aspx":
			case "ListLongMoviePickup.aspx":
				string sPickupFlag = "";
				if (currentAspx.Equals("ListLongMoviePickup.aspx")) {
					sPickupFlag = ViCommConst.FLAG_ON.ToString();
				}
				using (TalkMovie oTalkMovie = new TalkMovie()) {
					ds = oTalkMovie.GetCastMovieListByLoginId(site.siteCd,sLoginId,sCharNo,sPickupFlag);
				}
				dwPages = ds.Tables[0].Rows.Count / (double)pRowCountOfPage;
				pMaxPage = (int)Math.Ceiling(dwPages);
				totalRowCount = ds.Tables[0].Rows.Count;
				break;

			case "ListPersonalDiary.aspx":
			case "ViewDiary.aspx":
				seekCondition.InitCondtition();
				seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
				bool bDiarySwitchScreenFlag = false;
				using (ManageCompany oManageCompany = new ManageCompany()) {
					bDiarySwitchScreenFlag = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DIARY_SWITCH_SCREEN,2);
				}
				if (currentAspx.Equals("ViewDiary.aspx")) {
					pRowCountOfPage = 1;
				}
				using (CastDiary oCastDiary = new CastDiary()) {
					pMaxPage = oCastDiary.GetPageCountByLoginId(site.siteCd,sLoginId,sCharNo,false,bDiarySwitchScreenFlag,seekCondition,pRowCountOfPage,true,out totalRowCount);
					ds = oCastDiary.GetPageCollectionByLoginId(site.siteCd,sLoginId,sCharNo,false,bDiarySwitchScreenFlag,seekCondition,pCurrentPage,pRowCountOfPage,true);
				}
				break;

			case "ListDiary.aspx":
				seekCondition.InitCondtition();
				seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
				bool bNewDiaryOnlyFlag = false;
				bDiarySwitchScreenFlag = false;
				using (ManageCompany oManageCompany = new ManageCompany()) {
					if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISP_NEW_DIARY_ONLY,2)) {
						bNewDiaryOnlyFlag = true;
					}
					bDiarySwitchScreenFlag = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DIARY_SWITCH_SCREEN,2);
				}
				using (CastDiary oCastDiary = new CastDiary()) {
					pMaxPage = oCastDiary.GetPageCountByLoginId(site.siteCd,"","",bNewDiaryOnlyFlag,bDiarySwitchScreenFlag,seekCondition,pRowCountOfPage,true,out totalRowCount);
					ds = oCastDiary.GetPageCollectionByLoginId(site.siteCd,"","",bNewDiaryOnlyFlag,bDiarySwitchScreenFlag,seekCondition,pCurrentPage,pRowCountOfPage,true);
				}
				break;

			case "ListSaleMovie.aspx":
			case "ViewSaleMovie.aspx":
				if (currentAspx.Equals("ViewSaleMovie.aspx")) {
					pRowCountOfPage = 1;
				}
				string sSort = iBridUtil.GetStringValue(requestQuery.QueryString["sort"]);
				if (sSort.Equals("")) {
					sSort = "0";
				}
				using (CastMovie oCastMovie = new CastMovie()) {
					string sMovieSeq = iBridUtil.GetStringValue(pRequest.QueryString["movieseq"]);
					pMaxPage = oCastMovie.GetPageCount(site.siteCd,"","",sMovieSeq,false,ViCommConst.ATTACHED_MOVIE.ToString(),"",sMovieSeries,pRowCountOfPage,true,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,"","",sMovieSeq,false,ViCommConst.ATTACHED_MOVIE.ToString(),"",sMovieSeries,sSort,pCurrentPage,pRowCountOfPage,true);
				}
				break;

			case "ListPersonalSaleMovie.aspx":
			case "ViewPersonalSaleMovie.aspx":
				if (currentAspx.Equals("ViewPersonalSaleMovie.aspx")) {
					pRowCountOfPage = 1;
				}

				using (CastMovie oCastMovie = new CastMovie()) {
					string sMovieSeq = iBridUtil.GetStringValue(pRequest.QueryString["movieseq"]);
					pMaxPage = oCastMovie.GetPageCount(site.siteCd,sLoginId,sCharNo,sMovieSeq,false,ViCommConst.ATTACHED_MOVIE.ToString(),"","",pRowCountOfPage,true,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,sLoginId,sCharNo,sMovieSeq,false,ViCommConst.ATTACHED_MOVIE.ToString(),"","",ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
				}
				break;

			case "ViewSaleMovieEx.aspx":
			case "ConfirmSaleMovieEx.aspx":
				using (CastMovie oCastMovie = new CastMovie()) {
					string sMovieSeq = iBridUtil.GetStringValue(pRequest.QueryString["movieseq"]);

					pMaxPage = oCastMovie.GetPageCount(site.siteCd,sLoginId,sCharNo,sMovieSeq,false,ViCommConst.ATTACHED_MOVIE.ToString(),"",sMovieSeries,pRowCountOfPage,true,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,sLoginId,sCharNo,sMovieSeq,false,ViCommConst.ATTACHED_MOVIE.ToString(),"",sMovieSeries,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
				}
				break;

			case "ViewSaleVoice.aspx":
			case "ConfirmSaleVoice.aspx": {
					string sParamVoiceSeq = iBridUtil.GetStringValue(requestQuery.QueryString["voiceseq"]);

					using (CastVoice oCastVoice = new CastVoice()) {
						ds = oCastVoice.GetPageCollectionBySeq(sParamVoiceSeq,false,false,ViCommConst.ATTACHED_VOICE.ToString());
					}
					pRowCountOfPage = 1;
					pMaxPage = ds.Tables[0].Rows.Count;
					break;
				}

			case "ListSaleVoice.aspx":
			case "ListPersonalSaleVoice.aspx": {
					string sParamCharLoginId = null;
					string sParamCharNo = null;
					string sParamVoiceSort = iBridUtil.GetStringValue(requestQuery.QueryString["sort"]);

					if (currentAspx.ToUpper().Equals("ListPersonalSaleVoice.aspx".ToUpper())) {
						sParamCharLoginId = sLoginId;
						sParamCharNo = sCharNo;
					}
					if (string.IsNullOrEmpty(sParamVoiceSort))
						sParamVoiceSort = ViCommConst.SORT_NO;

					using (CastVoice oCastVoice = new CastVoice()) {
						pMaxPage = oCastVoice.GetPageCount(site.siteCd,sParamCharLoginId,sParamCharNo,false,false,ViCommConst.ATTACHED_VOICE.ToString(),null,pRowCountOfPage,out totalRowCount);
						ds = oCastVoice.GetPageCollection(site.siteCd,userMan.userSeq,sParamCharLoginId,sParamCharNo,false,false,ViCommConst.ATTACHED_VOICE.ToString(),null,sParamVoiceSort,pCurrentPage,pRowCountOfPage);
					}
					break;
				}
			case "ViewPic.aspx":
				using (CastPic oCastPic = new CastPic()) {
					pMaxPage = oCastPic.GetPageCount(site.siteCd,sLoginId,sCharNo,true,ViCommConst.ATTACHED_PROFILE.ToString(),"",1,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,sLoginId,sCharNo,true,ViCommConst.ATTACHED_PROFILE.ToString(),"",pCurrentPage,1);
				}
				break;

			case "ListPersonalPic.aspx":
				using (CastPic oCastPic = new CastPic()) {
					pMaxPage = oCastPic.GetPageCount(site.siteCd,sLoginId,sCharNo,true,ViCommConst.ATTACHED_PROFILE.ToString(),"",1,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,sLoginId,sCharNo,true,ViCommConst.ATTACHED_PROFILE.ToString(),"",1,9);
				}
				break;


			case "ListMailPicConf.aspx":
			case "ViewMailPicConf.aspx":
			case "ListMailPic.aspx":
			case "TxPicMailConfirm.aspx":
				string picType = ViCommConst.ATTACHED_MAIL.ToString();

				pRowCountOfPage = 9;
				if (currentAspx.Equals("ViewMailPicConf.aspx") ||
					currentAspx.Equals("TxPicMailConfirm.aspx")) {
					pRowCountOfPage = 1;
				}
				if (currentAspx.Equals("ListMailPic.aspx") ||
					currentAspx.Equals("TxPicMailConfirm.aspx")) {
					sNotApproveStatus = ViCommConst.FLAG_OFF_STR;
				}
				using (UserManPic oUserManPic = new UserManPic()) {
					pMaxPage = oUserManPic.GetPageCountByLoginId(site.siteCd,userMan.loginId,picType,sNotApproveStatus,pRowCountOfPage,out totalRowCount);
					ds = oUserManPic.GetPageCollectionByLoginId(site.siteCd,userMan.loginId,picType,sNotApproveStatus,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListMailMovie.aspx":
			case "ListMailMovieConf.aspx":
			case "ViewMailMovieConf.aspx":
			case "TxMovieMailConfirm.aspx":
				string movieType = ViCommConst.ATTACHED_MAIL.ToString();

				if (currentAspx.Equals("ViewMailMovieConf.aspx") ||
					currentAspx.Equals("TxMovieMailConfirm.aspx")) {
					pRowCountOfPage = 1;
				}
				if (currentAspx.Equals("ListMailMovie.aspx") ||
					currentAspx.Equals("TxMovieMailConfirm.aspx")) {
					sNotApproveStatus = ViCommConst.FLAG_OFF_STR;
				}
				using (UserManMovie oUserManMovie = new UserManMovie()) {
					pMaxPage = oUserManMovie.GetPageCountByLoginId(site.siteCd,userMan.loginId,string.Empty,movieType,sNotApproveStatus,pRowCountOfPage,out totalRowCount);
					ds = oUserManMovie.GetPageCollectionByLoginId(site.siteCd,userMan.loginId,string.Empty,movieType,sNotApproveStatus,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListPersonalBbsPic.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = false;
					bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					seekCondition.unusedFlag = iBridUtil.GetStringValue(pRequest.QueryString["unused"]);
					seekCondition.bookmarkFlag = iBridUtil.GetStringValue(pRequest.QueryString["bkm"]);
					using (CastPic oCastPic = new CastPic()) {
						pMaxPage = oCastPic.GetPageCount(site.siteCd,sLoginId,sCharNo,false,ViCommConst.ATTACHED_BBS.ToString(),"",sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pRowCountOfPage,out totalRowCount);
						ds = oCastPic.GetPageCollection(site.siteCd,sLoginId,sCharNo,false,ViCommConst.ATTACHED_BBS.ToString(),"",sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pCurrentPage,pRowCountOfPage);
					}
					break;
				}

			case "ViewPersonalBbsPic.aspx": {
					using (CastPic oCastPic = new CastPic()) {
						ds = oCastPic.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["objseq"]));
					}
					pRowCountOfPage = 1;
					pMaxPage = ds.Tables[0].Rows.Count;
					break;
				}

			case "ListMessage.aspx":
			case "ViewMessage.aspx":
				using (Message oMessage = new Message()) {
					if (currentAspx.Equals("ViewMessage.aspx")) {
						pRowCountOfPage = 1;
					}
					pMaxPage = oMessage.GetPageCount(site.siteCd,ViCommConst.MAN,pRowCountOfPage,out totalRowCount);
					ds = oMessage.GetPageCollection(site.siteCd,ViCommConst.MAN,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListWrittenMessage.aspx":
				using (Message oMessage = new Message()) {
					pMaxPage = oMessage.GetWrittenMessagePageCount(site.siteCd,ViCommConst.MAN,userMan.userSeq,userMan.userCharNo,pRowCountOfPage,out totalRowCount);
					ds = oMessage.GetWrittenMessagePageCollection(site.siteCd,ViCommConst.MAN,userMan.userSeq,userMan.userCharNo,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListPersonalBbsMovie.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = false;
					bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					seekCondition.unusedFlag = iBridUtil.GetStringValue(pRequest.QueryString["unused"]);
					seekCondition.bookmarkFlag = iBridUtil.GetStringValue(pRequest.QueryString["bkm"]);
					using (CastMovie oCastMovie = new CastMovie()) {
						pMaxPage = oCastMovie.GetPageCount(site.siteCd,sLoginId,sCharNo,"",false,ViCommConst.ATTACHED_BBS.ToString(),"",sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pRowCountOfPage,true,out totalRowCount);
						ds = oCastMovie.GetPageCollection(site.siteCd,sLoginId,sCharNo,"",false,ViCommConst.ATTACHED_BBS.ToString(),"",sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
					}
					break;
				}

			case "ViewPersonalBbsMovie.aspx": {
					using (CastMovie oCastMovie = new CastMovie()) {
						ds = oCastMovie.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["objseq"]),true);
					}
					pRowCountOfPage = 1;
					pMaxPage = ds.Tables[0].Rows.Count;
					break;
				}

			case "ListAllTalkHistory.aspx":
			case "ModifyTalkHistoryMemo.aspx":
				if (currentAspx.Equals("ModifyTalkHistoryMemo.aspx")) {
					pRowCountOfPage = 1;
				}
				using (CastTalkHistory oTalkHistory = new CastTalkHistory()) {
					pMaxPage = oTalkHistory.GetPageCount(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,false,pRowCountOfPage,out totalRowCount);
					ds = oTalkHistory.GetPageCollection(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,false,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListTalkHistory.aspx":
				using (CastTalkHistory oTalkHistory = new CastTalkHistory()) {
					pMaxPage = oTalkHistory.GetPageCount(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,true,pRowCountOfPage,out totalRowCount);
					ds = oTalkHistory.GetPageCollection(site.siteCd,userMan.userSeq,userMan.userCharNo,sLoginId,sCharNo,true,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListFindAttrType.aspx":
				pRowCountOfPage = 99;
				using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue()) {
					pMaxPage = oCastAttrTypeValue.GetPageCount(site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["itemno"]),iBridUtil.GetStringValue(pRequest.QueryString["group"]),pRowCountOfPage,out totalRowCount);
					ds = oCastAttrTypeValue.GetPageCollection(site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["itemno"]),iBridUtil.GetStringValue(pRequest.QueryString["group"]),pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListFindGroup.aspx":
				pRowCountOfPage = 99;
				using (CastAttrType oCastAttrType = new CastAttrType()) {
					pMaxPage = oCastAttrType.GetGroupingPageCount(site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["itemno"]),pRowCountOfPage,out totalRowCount);
					ds = oCastAttrType.GetGroupingPageCollection(site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["itemno"]),pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListPersonalBbsObj.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					bool bOnlyUsed = false;
					bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					seekCondition.unusedFlag = iBridUtil.GetStringValue(pRequest.QueryString["unused"]);
					seekCondition.bookmarkFlag = iBridUtil.GetStringValue(pRequest.QueryString["bkm"]);
					using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
						pMaxPage = oCastBbsObj.GetPageCount(site.siteCd,sLoginId,sCharNo,ViCommConst.FLAG_OFF_STR,false,"",sAttrTypeSeq,sAttrSeq,bAllBbs,bOnlyUsed,seekCondition,pRowCountOfPage,true,out totalRowCount);
						ds = oCastBbsObj.GetPageCollection(site.siteCd,sLoginId,sCharNo,ViCommConst.FLAG_OFF_STR,false,"",sAttrTypeSeq,sAttrSeq,bAllBbs,bOnlyUsed,seekCondition,pCurrentPage,pRowCountOfPage,true);
					}
					break;
				}

			case "ViewPersonalBbsObj.aspx": {
					using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
						ds = oCastBbsObj.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["objseq"]),bAllBbs,true);
					}
					pRowCountOfPage = 1;
					pMaxPage = ds.Tables[0].Rows.Count;
					break;
				}

			case "NonUserTop.aspx":
			case "ResignedUserTop.aspx": {
/*
					string sSiteCd = site.siteCd;
					string sPickupId = null;
					int iNeedCount = int.MinValue;

					sPickupId = site.GetSupplement(ViCommConst.SITE_MAN_TOP_PICKUP_ID) ?? string.Empty;
					string sNeedCount = site.GetSupplement(ViCommConst.SITE_MAN_TOP_PICKUP_DROWS);

					if (!int.TryParse(sNeedCount,out iNeedCount)) {
						iNeedCount = 1;
					}

					using (Cast oCast = new Cast()) {
						ds = oCast.GetRecommendDataSet(sSiteCd,sPickupId,iNeedCount);
					}
*/
					ds = new DataSet();
					ds.Tables.Add(new DataTable());
					pRowCountOfPage = 1;
					pMaxPage = 1;
					break;
				}

			case "ListAspAd.aspx":
				string sTopic = iBridUtil.GetStringValue(pRequest.QueryString["adtopic"]);
				string sPointFrom = iBridUtil.GetStringValue(pRequest.QueryString["affpointfrom"]);
				string sPointTo = iBridUtil.GetStringValue(pRequest.QueryString["affpointto"]);
				string sCategory = iBridUtil.GetStringValue(pRequest.QueryString["adcategory"]);
				string sFeeFrom = iBridUtil.GetStringValue(pRequest.QueryString["monthlyfeefrom"]);
				string sFeeTo = iBridUtil.GetStringValue(pRequest.QueryString["monthlyfeeto"]);
				bool bExcludeSettled = iBridUtil.GetStringValue(pRequest.QueryString["excludesettled"]).Equals(ViCommConst.FLAG_ON_STR);
				bool bOrderByNew = iBridUtil.GetStringValue(pRequest.QueryString["neworder"]).Equals(ViCommConst.FLAG_ON_STR);
				string sOrderType = iBridUtil.GetStringValue(pRequest.QueryString["order"]);

				using (AspAd oAspAd = new AspAd()) {
					pMaxPage = oAspAd.GetPageCount(site.siteCd,ViCommConst.UsableSexCd.MAN,sTopic,sPointFrom,sPointTo,sCategory,sFeeFrom,sFeeTo,carrier,bExcludeSettled,userMan.userSeq,pRowCountOfPage,out totalRowCount);
					ds = oAspAd.GetPageCollection(site.siteCd,ViCommConst.UsableSexCd.MAN,sTopic,sPointFrom,sPointTo,sCategory,sFeeFrom,sFeeTo,carrier,bExcludeSettled,userMan.userSeq,bOrderByNew,sOrderType,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListGetAspAd.aspx":
				ds = CreateAspAdUseApi(pRequest.QueryString,pCurrentPage,pRowCountOfPage,out pMaxPage,out totalRowCount);
				break;

			case "ListMoteKing.aspx": {

					seekCondition.InitCondtition();
					seekCondition.siteCd = site.siteCd;
					seekCondition.conditionFlag = pMode;
					seekCondition.newCastDay = 0;
					seekCondition.userRank = string.Empty;
					seekCondition.random = ViCommConst.FLAG_OFF;
					seekCondition.listDisplay = iBridUtil.GetStringValue(pRequest.QueryString["list"]);
					seekCondition.orderAsc = iBridUtil.GetStringValue(pRequest.QueryString["orderasc"]);
					seekCondition.orderDesc = iBridUtil.GetStringValue(pRequest.QueryString["orderdesc"]);
					seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
					seekCondition.rankingCtlSeq = iBridUtil.GetStringValue(pRequest.QueryString["ctlseq"]);
					if (seekCondition.rankingCtlSeq.Equals(string.Empty)) {
						using (RankingCtl oRankingCtl = new RankingCtl()) {
							seekCondition.rankingCtlSeq = oRankingCtl.GetLastSeq(site.siteCd,ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN);
						}
					}
					using (Man oMan = new Man()) {
						pMaxPage = oMan.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						ds = oMan.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
					}
				}
				break;

			case "ListRichinoBbsObj.aspx":
			case "ListBbsObjFree.aspx":
				//2:画像　3:動画　NULL:全て 
				string sViewMode = iBridUtil.GetStringValue(requestQuery.QueryString["viewmode"]);
				seekCondition.InitCondtition();
				seekCondition.unusedFlag = iBridUtil.GetStringValue(pRequest.QueryString["unused"]).Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;

				using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
					pMaxPage = 1;
					ds = oCastBbsObj.GetPageCollectionRandom(site.siteCd,sAttrTypeSeq,sAttrSeq,sViewMode,seekCondition,pRowCountOfPage,out totalRowCount,true);
				}
				break;

			case "ListBingoEntryWinner.aspx":
			case "ViewBingoCard.aspx":
				string sBingoTermSeq = iBridUtil.GetStringValue(requestQuery.QueryString["bingotermseq"]);
				seekCondition.InitCondtition();
				seekCondition.siteCd = site.siteCd;
				if (sBingoTermSeq.Equals(string.Empty)) {
					using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
						seekCondition.bingoTermSeq = oMailDeBingo.GetCurrentTermSeq(site.siteCd,ViCommConst.MAN,DateTime.Now);
					}
				} else {
					seekCondition.bingoTermSeq = sBingoTermSeq;
				}
				seekCondition.conditionFlag = pMode;
				using (Man oMan = new Man()) {
					pMaxPage = oMan.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
					ds = oMan.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
				}
				break;

			default:
				// ▽▽▽ ListPickup.aspx  ｷｬｽﾄの検索の可能性があるため例外的にcaseに含めずここで処理する 
				if (currentAspx.Equals("ListPickup.aspx")) {
					string sPickupId = iBridUtil.GetStringValue(pRequest.QueryString["pickupid"]);
					string sPickupStartPubDay = iBridUtil.GetStringValue(pRequest.QueryString["pickupstartday"]);
					seekCondition.InitCondtition();
					seekCondition = ExtendSeekCondition(seekCondition,pRequest);
					seekCondition.pickupId = sPickupId;
					seekCondition.pickupStartPubDay = sPickupStartPubDay;
					seekCondition.sortType = sSortType;
					seekCondition.rankType = iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]);
					bool bOnlyUsed = iBridUtil.GetStringValue(pRequest.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
					seekCondition.lastTxMailDays = iBridUtil.GetStringValue(pRequest.QueryString["lasttxmaildays"]);

					if (iBridUtil.GetStringValue(pRequest.QueryString["random"]).Equals(ViCommConst.FLAG_ON_STR)) {
						seekCondition.directRandom = ViCommConst.FLAG_ON;
						bRandom = false;// シャッフルのランダム処理は使わない						
					}


					if (pMode == ViCommConst.INQUIRY_PICKUP_MOVIE) {
						//
						// PICKUP 動画
						//
						using (CastMovie oCastMovie = new CastMovie()) {
							pMaxPage = oCastMovie.GetPageCount(site.siteCd,sLoginId,sCharNo,string.Empty,false,string.Empty,actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pRowCountOfPage,true,out totalRowCount);
							ds = oCastMovie.GetPageCollection(site.siteCd,sLoginId,sCharNo,string.Empty,false,string.Empty,actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,sSortType,iRNum,pRowCountOfPage,true);
						}
						break;
					} else if (pMode == ViCommConst.INQUIRY_PICKUP_PIC) {
						//
						// PICKUP 写真
						//
						string sPickupType = string.Empty;
						string sPicType = string.Empty;

						using (Pickup oPickup = new Pickup()) {
							sPickupType = oPickup.GetPickupType(this.site.siteCd,sPickupId);
						}

						switch (sPickupType) {
							case ViCommConst.PicupTypes.BBS_PIC:
								sPicType = ViCommConst.ATTACHED_BBS.ToString();
								break;
							case ViCommConst.PicupTypes.PROFILE_PIC:
								sPicType = ViCommConst.ATTACHED_PROFILE.ToString();
								break;
						}
						using (CastPic oCastPic = new CastPic()) {
							pMaxPage = oCastPic.GetPageCount(site.siteCd,sLoginId,sCharNo,false,sPicType,actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,pRowCountOfPage,out totalRowCount);
							ds = oCastPic.GetPageCollection(site.siteCd,sLoginId,sCharNo,false,sPicType,actCategorySeq,sAttrTypeSeq,sAttrSeq,bOnlyUsed,seekCondition,iRNum,pRowCountOfPage);
						}
						break;
					}
				}
				// △△△ ListPickup.aspx

				seekCondition.InitCondtition();
				seekCondition.siteCd = site.siteCd;
				seekCondition.userSeq = userMan.userSeq;
				seekCondition.userCharNo = userMan.userCharNo;
				seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
				seekCondition.conditionFlag = pMode;
				seekCondition.newCastDay = 0;
				seekCondition.userRank = "";
				seekCondition.random = (bRandom) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
				seekCondition.listDisplay = iBridUtil.GetStringValue(pRequest.QueryString["list"]);
				seekCondition.orderAsc = iBridUtil.GetStringValue(pRequest.QueryString["orderasc"]);
				seekCondition.orderDesc = iBridUtil.GetStringValue(pRequest.QueryString["orderdesc"]);
				string sHasRecvMail = iBridUtil.GetStringValue(pRequest.QueryString["has_recvmail"]);
				if (!string.IsNullOrEmpty(sHasRecvMail)) {
					seekCondition.hasRecvMail = sHasRecvMail.Equals(ViCommConst.FLAG_ON_STR) ? true : false;
				}

				bool bMoveProfile = false;
				string sPrevUserSeq = iBridUtil.GetStringValue(pRequest.QueryString["prevuserseq"]);
				string sPrevCharNo = iBridUtil.GetStringValue(pRequest.QueryString["prevcharno"]);

				// ProfileのNext/Previousの場合、元ﾍﾟｰｼﾞのUSER_SEQ,USER_CHAR_NO,+αでRNUMを再度算出する
				if (currentAspx.Equals("Profile.aspx") || currentAspx.Equals("ViewProfileMovie.aspx")) {
					pRowCountOfPage = 1;
					if (!sPrevUserSeq.Equals("")) {
						bMoveProfile = true;
					}
				}

				if (currentCategoryIndex >= 0) {
					seekCondition.categorySeq = ((ActCategory)categoryList[currentCategoryIndex]).actCategorySeq;
				} else {
					seekCondition.categorySeq = "";
				}

				switch (pMode) {
					case ViCommConst.INQUIRY_ONLINE:
					case ViCommConst.INQUIRY_CAST_ONLINE_TV_PHONE:
					case ViCommConst.INQUIRY_CAST_MONITOR:
					case ViCommConst.INQUIRY_ROOM_MONITOR:
						seekCondition = ExtendSeekCondition(seekCondition,pRequest);
						if (pMode.Equals(ViCommConst.INQUIRY_ONLINE)) {
							seekCondition.userRank = iBridUtil.GetStringValue(pRequest.QueryString["userrank"]);						
						}
						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					case ViCommConst.INQUIRY_CAST_LOGINED:
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
						seekCondition.sortExType = iBridUtil.GetStringValue(pRequest.QueryString["sortex"]);
						seekCondition = ExtendSeekCondition(seekCondition,pRequest);
						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					case ViCommConst.INQUIRY_CAST_TV_PHONE:
					case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2:
					case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2:
					case ViCommConst.INQUIRY_CAST_PICKUP:
					case ViCommConst.INQUIRY_TALK_MOVIE_PICKUP:
					case ViCommConst.INQUIRY_FAVORIT:
					case ViCommConst.INQUIRY_REFUSE:
					case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
					case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
					case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
					case ViCommConst.INQUIRY_CAST_RECOMMEND:
					case ViCommConst.INQUIRY_CAST_MOVIE:
					case ViCommConst.INQUIRY_TALK_MOVIE:
					case ViCommConst.INQUIRY_LIKE_ME:
					case ViCommConst.INQUIRY_FORWARD:
					case ViCommConst.INQUIRY_MEMO:
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
						seekCondition = ExtendSeekCondition(seekCondition,pRequest);

						if (pMode.Equals(ViCommConst.INQUIRY_FAVORIT)) {
							seekCondition.favoritGroupSeq = iBridUtil.GetStringValue(pRequest.QueryString["grpseq"]);
						}

						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);

						switch (pMode) {
							case ViCommConst.INQUIRY_FAVORIT:
							case ViCommConst.INQUIRY_LIKE_ME:
							case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
								seekCondition.sortExType = iBridUtil.GetStringValue(pRequest.QueryString["sortex"]);
								break;
						}

						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;
					case ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE:
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
						seekCondition = ExtendSeekCondition(seekCondition,pRequest);

						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);

						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;
					case ViCommConst.INQUIRY_CAST_NEW_FACE:
						seekCondition.newCastDay = site.newFaceDays;
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
						seekCondition = ExtendSeekCondition(seekCondition,pRequest);
						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					case ViCommConst.INQUIRY_ONLINE_CAST_NEW_FACE:
						seekCondition.newCastDay = site.newFaceDays;
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					case ViCommConst.INQUIRY_RECOMMEND:
						if (currentAspx.Equals("UserTop.aspx") || currentAspx.Equals("UserTopFind.aspx")) {
							using (Pickup oPickup = new Pickup()) {
								seekCondition.pickupId = oPickup.GetTopPagePickupId(site.siteCd);
							}
						} else {
							seekCondition.pickupId = iBridUtil.GetStringValue(pRequest.QueryString["pickupid"]);

							if (!string.IsNullOrEmpty(sSortType)) {
								seekCondition.sortType = sSortType;
							}
						}
						seekCondition.lastLoginDate = iBridUtil.GetStringValue(pRequest.QueryString["lastlogin"]);
						seekCondition.lastTxMailDays = iBridUtil.GetStringValue(pRequest.QueryString["lasttxmaildays"]);
						seekCondition = ExtendSeekCondition(seekCondition,pRequest);

						if (!int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["online"]),out seekCondition.onlineStatus)) {
							seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
						}

						if (iBridUtil.GetStringValue(pRequest.QueryString["random"]).Equals(ViCommConst.FLAG_ON_STR)) {
							seekCondition.directRandom = ViCommConst.FLAG_ON;
							bRandom = false;// シャッフルのランダム処理は使わない						
						}


						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					case ViCommConst.INQUIRY_CONDITION:

						Int64 iPlayMask;
						Int64.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["play"]),out iPlayMask);
						seekCondition.handleNm = iBridUtil.GetStringValue(pRequest.QueryString["handle"]);
						int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["online"]),out seekCondition.onlineStatus);
						seekCondition.comment = iBridUtil.GetStringValue(pRequest.QueryString["comment"]);
						seekCondition.userRank = iBridUtil.GetStringValue(pRequest.QueryString["userrank"]);
						seekCondition.ageFrom = iBridUtil.GetStringValue(pRequest.QueryString["agefrom"]);
						seekCondition.ageTo = iBridUtil.GetStringValue(pRequest.QueryString["ageto"]);
						if (iBridUtil.GetStringValue(pRequest.QueryString["newcast"]).Equals("1")) {
							seekCondition.newCastDay = site.newFaceDays;
						}
						seekCondition.movie = iBridUtil.GetStringValue(pRequest.QueryString["movie"]);
						seekCondition.waitType = iBridUtil.GetStringValue(pRequest.QueryString["waittype"]);
						seekCondition.enabledBlog = iBridUtil.GetStringValue(pRequest.QueryString["enabledblog"]);
						seekCondition.hasProfilePic = iBridUtil.GetStringValue(pRequest.QueryString["hasprofpic"]);
						seekCondition.playMask = iPlayMask;
						seekCondition.lastLoginDate = iBridUtil.GetStringValue(pRequest.QueryString["lastlogin"]);
						seekCondition.directRandom = iBridUtil.GetStringValue(pRequest.QueryString["directrandom"]).Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;

						this.SetAttrParam(seekCondition,pRequest);
						seekCondition = this.ExtendSeekCondition(seekCondition,pRequest);

						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					// List内に複数回同一ｷｬｽﾄが出現する
					case ViCommConst.INQUIRY_CAST_DIARY:
					case ViCommConst.INQUIRY_MARKING:
					case PwViCommConst.INQUIRY_SELF_MARKING:
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
						pMaxPage = userMan.castCharacter.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = userMan.castCharacter.GetRNumByUserSeqAndCondtion(
											seekCondition,
											sPrevUserSeq,
											sPrevCharNo,
											iBridUtil.GetStringValue(pRequest["conditem"]),
											iBridUtil.GetStringValue(pRequest["condvalue"])
											);
						}
						break;

					/*------------------------------------------------------*/
					/* Profile表示制御										*/
					/*------------------------------------------------------*/
					// List内に複数回同一ｷｬｽﾄが出現しかつ、ﾒｲﾝﾙｰﾌﾟがCASTではない 
					case ViCommConst.INQUIRY_TX_USER_MAIL_BOX:
					case ViCommConst.INQUIRY_RX_USER_MAIL_BOX:
					case ViCommConst.INQUIRY_MAIL_HISTORY:
					case ViCommConst.INQUIRY_BBS_MOVIE:
					case ViCommConst.INQUIRY_SALE_MOVIE:
					case ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE:
					case ViCommConst.INQUIRY_GALLERY:
					case ViCommConst.INQUIRY_BBS_PIC:
					case ViCommConst.INQUIRY_BBS_DOC:
					case ViCommConst.INQUIRY_ALL_TALK_HISTORY:
					case ViCommConst.INQUIRY_BBS_OBJ:
					case ViCommConst.INQUIRY_PICKUP_MOVIE:
					case ViCommConst.INQUIRY_PICKUP_PIC:
					case ViCommConst.INQUIRY_TALK_HISTORY:
						// RNumの取得・再計算が複雑なため対応しない 
						break;

					case ViCommConst.INQUIRY_TALK_MOVIE_PART:
						// 1つの動画の分割なので、1人しかありえない 
						break;

					case ViCommConst.INQUIRY_RANDOM_RECOMMEND:
						// 現在1人しか出力されないので、1人しかありえない 
						break;


					default:
						break;
				}


				if (bMoveProfile) {
					// 前へか次へなのかによってRNUMを変更します。 
					if (iBridUtil.GetStringValue(pRequest.QueryString["order"]) == ViCommConst.ORDER_PREVIOUS) {
						pCurrentPage--;
						if (pCurrentPage <= 0) {
							pCurrentPage = 1;
						}
					} else {
						pCurrentPage++;
						if (totalRowCount < pCurrentPage) {
							pCurrentPage = (int)totalRowCount;
						}
					}
					DataSet dsSub;
					DataRow drSub;
					ds = null;

					switch (pMode) {
						case ViCommConst.INQUIRY_BBS_MOVIE:
							using (CastMovie oCastMovie = new CastMovie()) {
								dsSub = oCastMovie.GetPageCollection(site.siteCd,"","","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["USER_SEQ"].ToString(),drSub["USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_SALE_MOVIE:
						case ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE:
							string sMovieSeq = "";
							if (pMode == ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE) {
								sMovieSeq = iBridUtil.GetStringValue(requestQuery["movieseq"]);
							}
							using (CastMovie oCastMovie = new CastMovie()) {
								dsSub = oCastMovie.GetPageCollection(site.siteCd,"","",sMovieSeq,false,ViCommConst.ATTACHED_MOVIE.ToString(),"","",iBridUtil.GetStringValue(requestQuery.QueryString["sort"]),pCurrentPage,pRowCountOfPage,true);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["USER_SEQ"].ToString(),drSub["USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_GALLERY:
							using (CastPic oCastPic = new CastPic()) {
								dsSub = oCastPic.GetPageCollection(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,"","",pCurrentPage,pRowCountOfPage);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["USER_SEQ"].ToString(),drSub["USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_BBS_PIC:
							using (CastPic oCastPic = new CastPic()) {
								dsSub = oCastPic.GetPageCollection(site.siteCd,"","",false,ViCommConst.ATTACHED_BBS.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,pCurrentPage,pRowCountOfPage);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["USER_SEQ"].ToString(),drSub["USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_BBS_DOC:
							using (CastBbs oCastBbs = new CastBbs()) {
								dsSub = oCastBbs.GetPageCollection(site.siteCd,"","",pCurrentPage,pRowCountOfPage,true);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["USER_SEQ"].ToString(),drSub["USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_TX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_RX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_MAIL_HISTORY:
							// Profile.aspxに引渡す条件が複雑であるため、今ﾊﾞｰｼﾞｮﾝは1Profileのみ表示
							break;

						case ViCommConst.INQUIRY_TALK_MOVIE_PART:
							// 1つの動画の分割なので、1人しかありえない 
							break;

						case ViCommConst.INQUIRY_RANDOM_RECOMMEND:
							// 現在1人しか出力されないので、1人しかありえない 
							break;

						case ViCommConst.INQUIRY_TALK_HISTORY:
							using (CastTalkHistory oTalkHistory = new CastTalkHistory()) {
								dsSub = oTalkHistory.GetPageCollection(site.siteCd,userMan.userSeq,userMan.userCharNo,string.Empty,string.Empty,true,pCurrentPage,pRowCountOfPage);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["PARTNER_USER_SEQ"].ToString(),drSub["PARTNER_USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_ALL_TALK_HISTORY:
							using (CastTalkHistory oTalkHistory = new CastTalkHistory()) {
								dsSub = oTalkHistory.GetPageCollection(site.siteCd,userMan.userSeq,userMan.userCharNo,string.Empty,string.Empty,false,pCurrentPage,pRowCountOfPage);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["PARTNER_USER_SEQ"].ToString(),drSub["PARTNER_USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_BBS_OBJ:
							using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
								dsSub = oCastBbsObj.GetPageCollection(site.siteCd,"","",false,false,actCategorySeq,sAttrTypeSeq,sAttrSeq,bAllBbs,false,pCurrentPage,pRowCountOfPage,true);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = userMan.castCharacter.GetOne(site.siteCd,drSub["USER_SEQ"].ToString(),drSub["USER_CHAR_NO"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						default:
							ds = userMan.castCharacter.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							break;
					}
					if (ds == null) {
						ds = userMan.castCharacter.GetOneByLoginId(site.siteCd,sPrevUserSeq,sPrevCharNo,1);
					}
				} else if (!sLoginId.Equals("")) {
					switch (pMode) {
						case ViCommConst.INQUIRY_TALK_MOVIE_PART:
							ds = userMan.castCharacter.GetOneByLoginId(site.siteCd,sLoginId,sCharNo,1);
							pMode = ViCommConst.INQUIRY_TALK_MOVIE;
							totalRowCount = ds.Tables[0].Rows.Count;
							break;

						case ViCommConst.INQUIRY_RANDOM_RECOMMEND:
						case ViCommConst.INQUIRY_DIRECT:
							ds = userMan.castCharacter.GetOneByLoginId(site.siteCd,sLoginId,sCharNo,1);
							pMaxPage = 1;
							totalRowCount = ds.Tables[0].Rows.Count;
							break;

						// CastがMain Loopでない検索からのProfile表示
						case ViCommConst.INQUIRY_PICKUP_MOVIE:
						case ViCommConst.INQUIRY_PICKUP_PIC:
						case ViCommConst.INQUIRY_BBS_MOVIE:
						case ViCommConst.INQUIRY_BBS_PIC:
						case ViCommConst.INQUIRY_BBS_DOC:
						case ViCommConst.INQUIRY_GALLERY:
						case ViCommConst.INQUIRY_TX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_RX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_MAIL_HISTORY:
						case ViCommConst.INQUIRY_SALE_MOVIE:
						case ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE:
						case ViCommConst.INQUIRY_BBS_OBJ:
							ds = userMan.castCharacter.GetOneByLoginId(site.siteCd,sLoginId,sCharNo,pCurrentPage);
							if (!iBridUtil.GetStringValue(pRequest["conditem"]).Equals("") && !iBridUtil.GetStringValue(pRequest["condvalue"]).Equals("")) {
								ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = iBridUtil.GetStringValue(pRequest["condvalue"]);
							}
							if (ds.Tables[0].Rows.Count > 0) {
								if (pMode == ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE) {
									pCurrentPage = 1;
								}
							}
							break;

						case ViCommConst.INQUIRY_TALK_HISTORY:
						case ViCommConst.INQUIRY_ALL_TALK_HISTORY:
							ds = userMan.castCharacter.GetOneByLoginId(site.siteCd,sLoginId,sCharNo,pCurrentPage);
							if (!iBridUtil.GetStringValue(pRequest["conditem"]).Equals("") && !iBridUtil.GetStringValue(pRequest["condvalue"]).Equals("")) {
								ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = iBridUtil.GetStringValue(pRequest["condvalue"]);
							}
							break;

						default:
							string sUserSeq = userMan.castCharacter.GetUserSeqByLoginId(sLoginId);
							ds = userMan.castCharacter.GetOneByUserSeqAndCondtion(
											seekCondition,
											sUserSeq,
											sCharNo,
											iBridUtil.GetStringValue(pRequest["conditem"]),
											iBridUtil.GetStringValue(pRequest["condvalue"]),
											pCurrentPage);
							totalRowCount = ds.Tables[0].Rows.Count;
							break;
					}
				}
				break;
		}
		return ds;
	}


	public bool ContorIVP(
		MobilePage pPage,
		NameValueCollection pQuery,
		out bool pLogin
	) {
		string sRequestId = iBridUtil.GetStringValue(pQuery["request"]);
		string sMenuId = iBridUtil.GetStringValue(pQuery["menuid"]);
		string sVicommMenuId = iBridUtil.GetStringValue(pQuery["menuid"]);
		string sCastSeq = iBridUtil.GetStringValue(pQuery["castseq"]);
		string sLiveSeq = iBridUtil.GetStringValue(pQuery["liveseq"]);
		string sCameraId = iBridUtil.GetStringValue(pQuery["camera"]);
		string sMovieSeq = iBridUtil.GetStringValue(pQuery["movieseq"]);
		string sMoviePartNo = iBridUtil.GetStringValue(pQuery["moviepart"]);
		string sShareLiveTalkKey = iBridUtil.GetStringValue(pQuery["shkey"]);
		string sMeetingKey = iBridUtil.GetStringValue(pQuery["meeting"]);
		string sNewRequestId = "";
		string sPartnerAcceptNo = "";
		string sIvpResult;
		string sResult;
		bool bRequest = false;

		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

		pLogin = false;
		bool bCheckBalance = false;
		errorMessage = "";

		if (this.site.useOtherSysInfoFlag == 0) {
			if (userMan.GetCurrentInfo(site.siteCd,userMan.userSeq)) {
				pLogin = logined;
			}
		} else {
			string sTel,sPassword,sCondition,sRetCd;
			int iBalPoint = 0;

			int iResult = ViCommInterface.GetWebUserInfo(site.userInfoUrl,userMan.webUserId,out iBalPoint,out sTel,out sPassword,out sCondition,out sRetCd);
			switch (iResult) {
				case ViCommConst.USER_INFO_OK:
					userMan.balPoint = iBalPoint;
					pLogin = true;
					break;
				case ViCommConst.USER_INFO_UNAUTH_TEL:
					pPage.RedirectToMobilePage(GetNavigateErrorUrl(HttpContext.Current.Session.SessionID,"MobileError.aspx?mode=user&errtype=" + ViCommConst.USER_INFO_UNAUTH_TEL));
					break;

				case ViCommConst.USER_INFO_NO_BAL:
					pPage.RedirectToMobilePage(GetNavigateErrorUrl(HttpContext.Current.Session.SessionID,"MobileError.aspx?mode=user&errtype=" + ViCommConst.USER_INFO_NO_BAL));
					break;

				case ViCommConst.USER_INFO_NG:
					pPage.RedirectToMobilePage(GetNavigateErrorUrl(HttpContext.Current.Session.SessionID,"MobileError.aspx?mode=user&errtype=" + ViCommConst.USER_INFO_NG));
					break;
			}
		}


		if (pLogin) {
			if (sRequestId.Equals(ViCommConst.REQUEST_LIVE_MONITOR)) {
				bCheckBalance = CheckLiveBalance(sCastSeq,sLiveSeq);

			} else if (sRequestId.Equals(ViCommConst.REQUEST_DIALIN_PLAY)) {
				bCheckBalance = CheckPlayMovieBalance(sCastSeq,sCastCharNo,sMovieSeq);

			} else if (sRequestId.Equals(ViCommConst.REQUEST_TEL_AUTH_LIGHT)) {
				bCheckBalance = true;
			} else {
				bCheckBalance = CheckTalkBalance(sCastSeq,sCastCharNo,sRequestId,sMenuId);
			}
			if (!bCheckBalance) {
				if (this.site.useOtherSysInfoFlag == 0) {
					errorMessage = ViCommConst.ERR_STATUS_NO_BALANCE;
				} else {
					pPage.RedirectToMobilePage(GetNavigateErrorUrl(HttpContext.Current.Session.SessionID,"MobileError.aspx?mode=user&errtype=" + ViCommConst.USER_INFO_NO_BAL));
				}
			} else {
				if (sRequestId.Equals(ViCommConst.REQUEST_VIEW_SHARE_LIVE_TALK)) {
					sPartnerAcceptNo = GetShareLiveTalkKeyToAcceptNo(sShareLiveTalkKey);
				}
				sResult = ivpRequest.Request(ref sRequestId,ViCommConst.MAN,userMan.userSeq,sCastSeq,sCastCharNo,sMenuId,sLiveSeq,sCameraId,sMovieSeq,sMoviePartNo,sShareLiveTalkKey,sPartnerAcceptNo,sMeetingKey);

				if (!sResult.Equals(ViCommConst.SP_RESULT_OK)) {

					SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];

					if (oObj.site.supportChgMonitorToTalkFlag != 0) {
						if ((sRequestId.Equals(ViCommConst.REQUEST_VIEW_BROADCASTING)) && (sMenuId.Equals("") == false)) {
							sNewRequestId = ViCommConst.REQUEST_TALK;
							sMenuId = ViCommConst.MENU_VIDEO_WTALK;

							if (sVicommMenuId.Equals(ViCommConst.DTMF_VIDEO_PTALK)) {
								sMenuId = ViCommConst.MENU_VIDEO_PTALK;
							} else if (sVicommMenuId.Equals(ViCommConst.DTMF_VOICE_WTALK)) {
								sMenuId = ViCommConst.MENU_VOICE_WTALK;
							} else if (sVicommMenuId.Equals(ViCommConst.DTMF_VOICE_PTALK)) {
								sMenuId = ViCommConst.MENU_VOICE_PTALK;
							}
							sResult = ivpRequest.Request(ref sNewRequestId,ViCommConst.MAN,userMan.userSeq,sCastSeq,sCastCharNo,sMenuId,sLiveSeq,sCameraId,sMovieSeq,sMoviePartNo,sShareLiveTalkKey,sPartnerAcceptNo,sMeetingKey);

						} else if (sRequestId.Equals(ViCommConst.REQUEST_TALK)) {
							sNewRequestId = ViCommConst.REQUEST_VIEW_BROADCASTING;
							sMenuId = ViCommConst.DTMF_VIDEO_WTALK;

							if (sVicommMenuId.Equals(ViCommConst.MENU_VIDEO_PTALK)) {
								sMenuId = ViCommConst.DTMF_VIDEO_PTALK;
							} else if (sVicommMenuId.Equals(ViCommConst.MENU_VOICE_WTALK)) {
								sMenuId = ViCommConst.DTMF_VOICE_WTALK;
							} else if (sVicommMenuId.Equals(ViCommConst.DTMF_VOICE_PTALK)) {
								sMenuId = ViCommConst.MENU_VOICE_PTALK;
							}
							sResult = ivpRequest.Request(ref sNewRequestId,ViCommConst.MAN,userMan.userSeq,sCastSeq,sCastCharNo,sMenuId,sLiveSeq,sCameraId,sMovieSeq,sMoviePartNo,sShareLiveTalkKey,sPartnerAcceptNo,sMeetingKey);
						}
					}
				} else {
					sNewRequestId = sRequestId;
				}

				if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
					sRequestId = sNewRequestId;
				}

				sIvpResult = ivpRequest.interfaceResult;


				if (!sResult.Equals(ViCommConst.SP_RESULT_OK)) {
					switch (sResult) {
						case ViCommConst.SP_RESULT_NO_TEL:
							errorMessage = ViCommConst.ERR_STATUS_IVP_REQ_NO_TEL;
							break;
						case ViCommConst.SP_RESULT_NO_WAITING:
							errorMessage = ViCommConst.ERR_STATUS_IVP_REQ_NO_WAITING;
							break;
						default:
							switch (sRequestId) {
								case ViCommConst.REQUEST_LIVE_MONITOR:
									errorMessage = ViCommConst.ERR_STATUS_END_LIVE;
									break;

								case ViCommConst.REQUEST_VIEW_TALK:
								case ViCommConst.REQUEST_MONITOR_VOICE:
									errorMessage = ViCommConst.ERR_STATUS_END_MONIOR_TALK;
									break;

								default:
									if ((sRequestId.Equals(ViCommConst.REQUEST_VIEW_BROADCASTING)) && (sMenuId.Equals(""))) {
										if (sIvpResult.Equals(ViCommConst.IVP_RESULT_OVER_SESSION)) {
											errorMessage = ViCommConst.ERR_STATUS_OVER_ONLINE_MONIOR;
										} else {
											errorMessage = ViCommConst.ERR_STATUS_END_ONLINE_MONIOR;
										}
									} else {
										errorMessage = ViCommConst.ERR_STATUS_BUSY;
									}
									break;
							}
							break;
					}
				} else {
					bRequest = true;
				}
			}
		}

		if (!errorMessage.Equals("")) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage = string.Format("{0}<br />",oErrorDtl.GetErrorDtl(site.siteCd,errorMessage));
			}
		}
		return bRequest;
	}

	private bool CheckTalkBalance(string pCastSeq,string pCastCharNo,string pRequestId,string pMenuId) {
		int iUnitSec = 0,iPoint = 0;
		int iLimitPoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if ((pCastSeq.Equals("") == false) && (pCastCharNo.Equals("") == false)) {
			if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
				if (pRequestId.Equals(ViCommConst.REQUEST_VIEW_TALK)) {
					userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_VIEW_TALK,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);

				} else if ((pRequestId.Equals(ViCommConst.REQUEST_MONITOR_VOICE)) && (pMenuId.Equals(""))) {
					userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_WIRETAPPING,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);

				} else if ((pRequestId.Equals(ViCommConst.REQUEST_VIEW_BROADCASTING)) && (pMenuId.Equals(""))) {
					userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_VIEW_ONLINE,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);

				} else if (pRequestId.Equals(ViCommConst.REQUEST_VIEW_SHARE_LIVE_TALK)) {
					userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_VIEW_LIVE,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);

				} else if (pRequestId.Equals(ViCommConst.REQUEST_VCS_MENU)) {
					switch (pMenuId) {
						case ViCommConst.VCS_MENU_DIRECT_TALK:
							userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TALK_VOICE_WSHOT,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);
							break;
						case ViCommConst.VCS_MENU_DIRECT_REC_PF:
							userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_REC_PF_VOICE,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);
							break;
						case ViCommConst.VCS_MENU_DIRECT_PLAY_PF:
							userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_PLAY_PF_VOICE,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);
							break;
						case ViCommConst.VCS_MENU_GPF_TALK:
							userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_GPF_TALK_VOICE,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);
							break;
					}
				} else {
					if (pMenuId.Equals(ViCommConst.MENU_VIDEO_WTALK)) {
						userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TALK_WSHOT,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);

					} else if (pMenuId.Equals(ViCommConst.MENU_VIDEO_PTALK)) {
						userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TALK_PUBLIC,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);

					} else if (pMenuId.Equals(ViCommConst.MENU_VOICE_WTALK)) {
						userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TALK_VOICE_WSHOT,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);

					} else if (pMenuId.Equals(ViCommConst.MENU_VOICE_PTALK)) {
						userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TALK_VOICE_PUBLIC,userMan.castCharacter.actCategorySeq,out iUnitSec,out iPoint);
					}
				}
			}
		} else {
			if (pRequestId.Equals(ViCommConst.REQUEST_VCS_MENU)) {
				switch (pMenuId) {
					case ViCommConst.VCS_MENU_WIRETAPPING:
						userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_WIRETAPPING,"",out iUnitSec,out iPoint);
						break;
					case ViCommConst.VCS_MENU_PLAY_PV_MSG:
						userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_PLAY_PV_MSG,"",out iUnitSec,out iPoint);
						break;
				}
			}
		}
		return ((userMan.balPoint + iLimitPoint + userMan.inviteServicePoint) >= iPoint);
	}


	private bool CheckLiveBalance(string pCastSeq,string pLiveSeq) {
		int iPoint = 0;
		int iLimitPoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		using (LiveBroadcast oLiveBroadcast = new LiveBroadcast()) {
			iPoint = oLiveBroadcast.GetLiveCharge(site.siteCd,pLiveSeq);
		}
		return ((userMan.balPoint + iLimitPoint + userMan.inviteServicePoint) >= iPoint);
	}


	private bool CheckPlayMovieBalance(string pCastSeq,string pCastCharNo,string pMovieSeq) {
		int iUnitSec,iPoint = 0;
		int iLimitPoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		using (Cast oCast = new Cast()) {
			if (oCast.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
				using (TalkMovie oTalkMovie = new TalkMovie()) {
					iPoint = oTalkMovie.GetMovieCharge(site.siteCd,pMovieSeq);
				}
				if (iPoint == 0) {
					userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_VIEW_TALK,oCast.actCategorySeq,out iUnitSec,out iPoint);
				}
			}
		}
		return ((userMan.balPoint + iLimitPoint + userMan.inviteServicePoint) >= iPoint);
	}


	public bool CheckMailBalance(string pCastSeq,string pCastCharNo,out int pChargePoint) {
		int iUnitSec = 0;
		pChargePoint = 0;
		int iLimitPoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
			userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TX_MAIL,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
		}
		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}

	public bool CheckTxMailPicBalance(string pCastSeq,string pCastCharNo,out int pChargePoint) {
		int iUnitSec = 0;
		pChargePoint = 0;
		int iLimitPoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
			userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TX_MAIL_WITH_PIC,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
		}
		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}

	public bool CheckTxMailMovieBalance(string pCastSeq,string pCastCharNo,out int pChargePoint) {
		int iUnitSec = 0;
		pChargePoint = 0;
		int iLimitPoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
			userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_TX_MAIL_WITH_MOVIE,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
		}
		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}

	public bool CheckPFDownloadBalance(string pCastSeq,string pCastCharNo,string pMovieSeq,out int pChargePoint) {
		int iUnitSec = 0;
		int iLimitPoint = 0;
		pChargePoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			pChargePoint = 0;
			return true;
		}

		using (CastMovie oCastMovie = new CastMovie()) {
			pChargePoint = oCastMovie.GetCastMovieCharge(site.siteCd,pCastSeq,pCastCharNo,pMovieSeq);
		}

		if (pChargePoint == 0) {
			if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
				userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_DOWNLOAD_PF_MOVIE,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
			}
		}
		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		if ((userMan.balPoint + iLimitPoint) < pChargePoint) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage += string.Format("{0}<br />",oErrorDtl.GetErrorDtl(site.siteCd,ViCommConst.ERR_STATUS_NO_BALANCE));
			}
		}
		return (userMan.balPoint + iLimitPoint >= pChargePoint);
	}
	public bool CheckDownloadMovieBalance(string pCastSeq,string pCastCharNo,string pMovieSeq,out int pChargePoint) {
		int iUnitSec = 0;
		int iLimitPoint = 0;
		pChargePoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			pChargePoint = 0;
			return true;
		}

		using (CastMovie oCastMovie = new CastMovie()) {
			pChargePoint = oCastMovie.GetCastMovieCharge(site.siteCd,pCastSeq,pCastCharNo,pMovieSeq);
		}

		if (pChargePoint == 0) {
			if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
				userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_DOWNLOAD_MOVIE,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
			}
		}
		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		return (userMan.balPoint + iLimitPoint >= pChargePoint);
	}
	public bool CheckDownloadVoiceBalance(string pCastSeq,string pCastCharNo,string pVoiceSeq,out int pChargePoint) {
		int iUnitSec = 0;
		int iLimitPoint = 0;
		pChargePoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			pChargePoint = 0;
			return true;
		}

		using (CastVoice oCastVoice = new CastVoice()) {
			pChargePoint = oCastVoice.GetChargePoint(site.siteCd,pCastSeq,pCastCharNo,pVoiceSeq);
		}

		if (pChargePoint == 0) {
			if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
				userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_DOWNLOAD_VOICE,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
			}
		}
		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		return (userMan.balPoint + iLimitPoint >= pChargePoint);
	}
	public bool CheckProductBalance(string pProductSeq,out int pChargePoint) {
		int iLimitPoint = 0;
		pChargePoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			pChargePoint = 0;
			return true;
		}

		using (Product oProduct = new Product()) {
			pChargePoint = oProduct.GetChargePoint(pProductSeq);
		}


		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		return (userMan.balPoint + iLimitPoint >= pChargePoint);
	}

	public bool CheckPicMailBalance(string pCastSeq,string pCastCharNo,out int pChargePoint) {
		int iUnitSec = 0;
		int iLimitPoint = 0;
		pChargePoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			return true;
		}

		if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
			userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_MAIL_WITH_PIC,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
		}

		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		if ((userMan.balPoint + iLimitPoint) < pChargePoint) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage += string.Format("{0}<br />",oErrorDtl.GetErrorDtl(site.siteCd,ViCommConst.ERR_STATUS_NO_BALANCE));
			}
		}
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}

	public bool CheckMovieMailBalance(string pCastSeq,string pCastCharNo,out int pChargePoint) {
		int iUnitSec = 0;
		int iLimitPoint = 0;
		pChargePoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			return true;
		}

		if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
			userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_MAIL_WITH_MOVIE,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
		}

		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		if ((userMan.balPoint + iLimitPoint) < pChargePoint) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage += string.Format("{0}<br />",oErrorDtl.GetErrorDtl(site.siteCd,ViCommConst.ERR_STATUS_NO_BALANCE));
			}
		}
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}

	public bool CheckBbsPicBalance(string pCastSeq,string pCastCharNo,string pPicSeq,out int pChargePoint,out int pPaymentAmt) {
		int iUnitSec = 0;
		int iLimitPoint = 0;
		int iUseIndividualChargeFlag;
		int iViewerChargePoint;
		int iUseIndividualPayFlag;
		int iViewePayAmt;

		pChargePoint = 0;
		pPaymentAmt = -1;

		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			return true;
		}

		using (CastPic oCastPic = new CastPic()) {
			oCastPic.GetChargeInfoByPicSeq(site.siteCd,pPicSeq,out iUseIndividualChargeFlag,out iViewerChargePoint,out iUseIndividualPayFlag,out iViewePayAmt);
			if (iUseIndividualChargeFlag != 0) {
				pChargePoint = iViewerChargePoint;
			}
			if (iUseIndividualPayFlag != 0) {
				pPaymentAmt = iViewePayAmt;
			}
		}

		if (iUseIndividualChargeFlag == 0) {
			if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
				userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_BBS_PIC,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
			}
		}

		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		if ((userMan.balPoint + iLimitPoint) < pChargePoint) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage += string.Format("{0}<br />",oErrorDtl.GetErrorDtl(userMan.siteCd,ViCommConst.ERR_STATUS_NO_BALANCE));
			}
		}
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}

	public bool CheckBbsMovieBalance(string pCastSeq,string pCastCharNo,string pMovieSeq,out int pChargePoint,out int pPaymentAmt) {
		int iUnitSec = 0;
		int iLimitPoint = 0;
		int iUseIndividualChargeFlag;
		int iViewerChargePoint;
		int iUseIndividualPayFlag;
		int iViewePayAmt;

		pChargePoint = 0;
		pPaymentAmt = -1;

		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (this.logined == false) {
			pChargePoint = 0;
			return true;
		}

		using (CastMovie oCastMovie = new CastMovie()) {
			oCastMovie.GetChargeInfoByMovieSeq(site.siteCd,pMovieSeq,out iUseIndividualChargeFlag,out iViewerChargePoint,out iUseIndividualPayFlag,out iViewePayAmt);
			if (iUseIndividualChargeFlag != 0) {
				pChargePoint = iViewerChargePoint;
			}
			if (iUseIndividualPayFlag != 0) {
				pPaymentAmt = iViewePayAmt;
			}
		}

		if (iUseIndividualChargeFlag == 0) {
			using (CastMovie oCastMovie = new CastMovie()) {
				pChargePoint = oCastMovie.GetCastMovieCharge(site.siteCd,pCastSeq,pCastCharNo,pMovieSeq);
			}

			if (pChargePoint == 0) {
				if (userMan.castCharacter.GetCastInfo(site.siteCd,pCastSeq,pCastCharNo)) {
					userMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_BBS_MOVIE,userMan.castCharacter.actCategorySeq,out iUnitSec,out pChargePoint);
				}
			}
		}
		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		if ((userMan.balPoint + iLimitPoint) < pChargePoint) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage += string.Format("{0}<br />",oErrorDtl.GetErrorDtl(userMan.siteCd,ViCommConst.ERR_STATUS_NO_BALANCE));
			}
		}
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}


	public bool CheckWriteBbsBalance(out int pChargePoint) {
		int iUnitSec;
		int iLimitPoint = 0;
		pChargePoint = 0;
		if (adminFlg)
			return true;

		if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
			iLimitPoint = userMan.limitPoint;
		}

		if (logined == false) {
			return true;
		}

		userMan.GetCornerCharge(site.siteCd,"",ViCommConst.CHARGE_WRITE_BBS,null,out iUnitSec,out pChargePoint);

		userMan.GetCurrentInfo(userMan.siteCd,userMan.userSeq);

		if ((userMan.balPoint + iLimitPoint) < pChargePoint) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage += string.Format("{0}<br />",oErrorDtl.GetErrorDtl(userMan.siteCd,ViCommConst.ERR_STATUS_NO_BALANCE));
			}
		}
		return ((userMan.balPoint + iLimitPoint) >= pChargePoint);
	}

	public string GetShareLiveTalkKeyToAcceptNo(string pShKey) {
		string sUrl = "",sLocCd,sSiteCd,sRequestUrl,sAcceptNo;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("SIP_IVP_LOC_CD",out sLocCd);
			oSys.GetValue("SIP_IVP_SITE_CD",out sSiteCd);
			oSys.GetValue("IVP_ISSUE_LIVE_KEY_URL",out sRequestUrl);
		}

		sUrl = string.Format("{0}?req={1}&loc={2}&site={3}&shkey={4}",sRequestUrl,ViCommConst.SHARE_LIVE_KEY_TO_ACCEPT_NO,sLocCd,sSiteCd,pShKey);

		if (ViCommInterface.ShareLiveTalkKey(sUrl,out sAcceptNo).Equals("0")) {
			return sAcceptNo;
		} else {
			return "";
		}
	}

	public string GetCastValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_CAST);
	}

	public int GetCastCount() {
		return GetRowCount(ViCommConst.DATASET_CAST);
	}

	public string GetTalkHistoryValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_TALK_HISTORY);
	}

	public string GetMailValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_MAIL);
	}

	public void SetMailValue(string pFieldNm,string pValue) {
		SetRowValue(pFieldNm,pValue,ViCommConst.DATASET_MAIL);
	}

	public int GetMailCount() {
		return GetRowCount(ViCommConst.DATASET_MAIL);
	}

	public string GetBbsMovieValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_MOVIE);
	}

	public string GetBbsPicValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_PIC);
	}

	public string GetBbsDocValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_BBS_DOC);
	}

	public string GetManBbsDocValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_BBS_DOC);
	}

	public string GetProfilePicValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_PIC);
	}

	public string GetManMovieValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_MOVIE);
	}

	public string GetSalesMovieValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_MOVIE);
	}

	public string GetBbsObjValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_BBS_OBJ,out bExist);
	}

	public string GetAspAdValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_ASP_AD,out bExist);
	}

	public string GetBlogArticleValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_EX_BLOG_ARTICLE,out bExist);
	}
	public string GetProductAuctionValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PRODUCT_AUCTION,out bExist);
	}
	public string GetGameMovieValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_MOVIE,out bExist);
	}

	public string GetDiaryValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_DIARY);
	}

	public virtual SeekCondition ExtendSeekCondition(SeekCondition pSeekCondtion,HttpRequest pRequest) {

		string sQueryHandle = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_handle"]));
		string sQueryAgeRange = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_agerange"]));
		string sQueryUserStatus = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_userstatus"]));
		string sQueryWaitType = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_waittype"]));
		string sQueryTaking = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_talking"]));
		string sQueryMonitorEnable = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_monitor"]));

		// ハンドル名 
		if (!string.IsNullOrEmpty(sQueryHandle)) {
			pSeekCondtion.handleNm = sQueryHandle;
			pSeekCondtion.hasExtend = true;
		}
		// 年齢 
		if (!string.IsNullOrEmpty(sQueryAgeRange)) {
			if (!sQueryAgeRange.Equals(":")) {
				string[] ageRangeFromTo = sQueryAgeRange.Split(':');
				pSeekCondtion.ageFrom = ageRangeFromTo[0];
				pSeekCondtion.ageTo = (ageRangeFromTo.Length > 1) ? ageRangeFromTo[1] : string.Empty;
				pSeekCondtion.hasExtend = true;
			}
		}
		// ｵﾝﾗｲﾝ/新人/ﾛｸﾞｲﾝ 
		if (!string.IsNullOrEmpty(sQueryUserStatus)) {
			switch (sQueryUserStatus) {
				case "1":
					seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
					pSeekCondtion.hasExtend = true;
					break;
				case "2":
					pSeekCondtion.newCastDay = site.newFaceDays;
					pSeekCondtion.hasExtend = true;
					break;
				case "3":
					seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED;
					pSeekCondtion.hasExtend = true;
					break;
			}
		}

		// 待機種別
		if (!string.IsNullOrEmpty(sQueryWaitType)) {
			switch (sQueryWaitType) {
				case "1":
					seekCondition.waitType = ViCommConst.WAIT_TYPE_VIDEO;
					pSeekCondtion.hasExtend = true;
					break;
				case "2":
					seekCondition.waitType = ViCommConst.WAIT_TYPE_VOICE;
					pSeekCondtion.hasExtend = true;
					break;
			}
		}

		// 会話種別
		if (!string.IsNullOrEmpty(sQueryTaking)) {
			switch (sQueryTaking) {
				case "1":
					seekCondition.talkingType = ViCommConst.TALK_TYPE_WSHOT;
					pSeekCondtion.hasExtend = true;
					break;
				case "2":
					seekCondition.talkingType = ViCommConst.TALK_TYPE_PUBLIC;
					pSeekCondtion.hasExtend = true;
					break;
			}
		}

		// モニター利用待機





		if (!string.IsNullOrEmpty(sQueryMonitorEnable)) {
			switch (sQueryMonitorEnable) {
				case "1":
					seekCondition.monitorEnableFlag = 1;
					pSeekCondtion.hasExtend = true;
					break;
			}
		}

		// ｷｬｽﾄ属性 
		using (CastAttrType oCastAttrType = new CastAttrType())
		using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue()) {

			foreach (string sKey in pRequest.QueryString.AllKeys) {
				if (sKey == null)
					continue;

				if (!sKey.StartsWith("ext_item"))
					continue;

				string sItemNo = sKey.Replace("ext_item",string.Empty);
				string sValue = HttpUtility.UrlDecode(pRequest.QueryString[sKey]);
				if (string.IsNullOrEmpty(sValue))
					continue;

				if (sValue.Equals(":") || sValue.Equals("*"))
					continue;


				string[] sAttrItemNoArry = sValue.Split(':');

				string sAttrTypeSeq = oCastAttrType.GetSeqByItemNo(this.site.siteCd,sItemNo);
				if (string.IsNullOrEmpty(sAttrTypeSeq))
					continue;

				System.Text.StringBuilder oSb = new System.Text.StringBuilder();
				for (int i = 0;i < sAttrItemNoArry.Length;i++) {
					if (i != 0)
						oSb.Append(",");
					oSb.Append(oCastAttrTypeValue.GetSeqByItemNo(this.site.siteCd,sAttrTypeSeq,sAttrItemNoArry[i]));
				}

				pSeekCondtion.attrTypeSeq.Add(sAttrTypeSeq);
				pSeekCondtion.attrValue.Add(oSb.ToString());
				pSeekCondtion.groupingFlag.Add("0");
				pSeekCondtion.likeSearchFlag.Add("0");
				pSeekCondtion.notEqualFlag.Add("0");
				pSeekCondtion.rangeFlag.Add("0");
				pSeekCondtion.seqRangeFlag.Add("0");

				pSeekCondtion.hasExtend = true;
			}
		}

		return pSeekCondtion;
	}

	public DataSet CreateAspAdUseApi(NameValueCollection pQuery,int pPageNo,int pRecPerPage,out int pPageCnt,out decimal pRecCount) {
		pRecCount = 0;
		pPageCnt = 0;

		DataSet ds = new DataSet();
		ds.Tables.Add("GET_ASP_AD");
		ds.Tables["GET_ASP_AD"].Columns.Add("AD_ID");
		ds.Tables["GET_ASP_AD"].Columns.Add("AD_NM");
		ds.Tables["GET_ASP_AD"].Columns.Add("AD_COMMENT");
		ds.Tables["GET_ASP_AD"].Columns.Add("MONTH_PRICE_COMMENT");
		ds.Tables["GET_ASP_AD"].Columns.Add("END_DATE");
		ds.Tables["GET_ASP_AD"].Columns.Add("AD_URL");
		ds.Tables["GET_ASP_AD"].Columns.Add("AD_TEXT");
		ds.Tables["GET_ASP_AD"].Columns.Add("AD_IMAGE");
		ds.Tables["GET_ASP_AD"].Columns.Add("CLICK_UNIT_PRICE");
		ds.Tables["GET_ASP_AD"].Columns.Add("ACTION_UNIT_PRICE");
		ds.Tables["GET_ASP_AD"].Columns.Add("ACTION_TARIFF");
		ds.Tables["GET_ASP_AD"].Columns.Add("GET_POINT");
		ds.Tables["GET_ASP_AD"].Columns.Add("REGISTED_AD_FLAG");
		ds.Tables["GET_ASP_AD"].Columns.Add("AD_OWNER_ID");
		ds.Tables["GET_ASP_AD"].Columns.Add("COUNT_COMMENT");

		string sId = iBridUtil.GetStringValue(pQuery["settleid"]);
		string sUId = userMan.loginId;
		string sTId = iBridUtil.GetStringValue(pQuery["t_id"]);
		string sCtId = iBridUtil.GetStringValue(pQuery["ct_id"]);
		string sPtMin = iBridUtil.GetStringValue(pQuery["pt_min"]);
		string sPtMax = iBridUtil.GetStringValue(pQuery["pt_max"]);
		string sOrder = iBridUtil.GetStringValue(pQuery["order"]);
		if (sOrder.Equals(string.Empty)) {
			sOrder = "new";
		}
		string sLimit = "99999";
		string sPage = "0";
		string sRes = "csv";
		string sCareer = string.Empty;
		if (carrier.Equals(ViCommConst.DOCOMO)) {
			sCareer = "d";
		} else if (carrier.Equals(ViCommConst.SOFTBANK)) {
			sCareer = "v";
		} else if (carrier.Equals(ViCommConst.KDDI)) {
			sCareer = "e";
		} else {
			sCareer = "pc";
		}
		//imodeIDを渡すと何故か既登録ﾁｪｯｸが行われないので、USER_SEQでのみﾁｪｯｸをしてもらう 
		//string sSub = currentIModeId;
		string sMbFlg = iBridUtil.GetStringValue(pQuery["mb_flg"]);
		string sRcFlg = iBridUtil.GetStringValue(pQuery["rc_flg"]);
		string sCustomFlg1 = iBridUtil.GetStringValue(pQuery["custom_flg_1"]);
		string sCustomFlg2 = iBridUtil.GetStringValue(pQuery["custom_flg_2"]);
		string sCustomFlg3 = iBridUtil.GetStringValue(pQuery["custom_flg_3"]);
		string sCustomFlg4 = iBridUtil.GetStringValue(pQuery["custom_flg_4"]);

		string sRequestUrl = string.Empty;
		string sMId = string.Empty;
		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(site.siteCd,sId,ViCommConst.SETTLE_POINT_AFFILIATE)) {
				sRequestUrl = oSiteSettle.settleUrl;
				sMId = oSiteSettle.cpIdNo;
			}
		}

		if (sRequestUrl.Equals(string.Empty)) {
			return ds;
		} else {
			sRequestUrl = string.Format(sRequestUrl + "?m_id={0}&u_id={1}&t_id={2}&ct_id={3}&pt_min={4}&pt_max={5}&order={6}&limit={7}&page={8}&res={9}&career={10}&mb_flg={11}&rc_flg={12}&custom_flg_1={13}&custom_flg_2={14}&custom_flg_3={15}&custom_flg_4={16}",
										sMId,sUId,sTId,sCtId,sPtMin,sPtMax,sOrder,sLimit,sPage,sRes,sCareer,sMbFlg,sRcFlg,sCustomFlg1,sCustomFlg2,sCustomFlg3,sCustomFlg4);
		}

		WebRequest req = WebRequest.Create(sRequestUrl);
		req.Timeout = 20000;
		WebResponse res = req.GetResponse();

		Stream st = res.GetResponseStream();
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		using (StreamReader sr = new StreamReader(st,enc)) {
			string sResponse = sr.ReadToEnd();
			string[] sItem = sResponse.Split('\n');
			sr.Close();
			st.Close();

			if (sItem.Length > 0 && !sResponse.Equals(string.Empty)) {
				string[] sAspAdData;

				int iPageMin = (pPageNo - 1) * pRecPerPage;
				int iPageMax = pPageNo * pRecPerPage - 1;
				int iDataSetCnt = 0;

				for (int i = 0;i < sItem.Length - 1;i++) {
					if (i >= iPageMin && i <= iPageMax) {
						sAspAdData = sItem[i].Split('\t');
						if (sAspAdData.Length > 13) {
							ds.Tables["GET_ASP_AD"].Rows.Add();
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["AD_ID"] = sAspAdData[0];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["AD_NM"] = sAspAdData[1];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["AD_COMMENT"] = sAspAdData[2];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["MONTH_PRICE_COMMENT"] = sAspAdData[3];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["END_DATE"] = sAspAdData[4];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["AD_URL"] = sAspAdData[5];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["AD_TEXT"] = sAspAdData[6];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["AD_IMAGE"] = sAspAdData[7];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["CLICK_UNIT_PRICE"] = sAspAdData[8];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["ACTION_UNIT_PRICE"] = sAspAdData[9];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["ACTION_TARIFF"] = sAspAdData[10];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["GET_POINT"] = sAspAdData[11];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["REGISTED_AD_FLAG"] = sAspAdData[12];
							ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["AD_OWNER_ID"] = sAspAdData[13];
							if (sAspAdData.Length > 14) {
								ds.Tables["GET_ASP_AD"].Rows[iDataSetCnt]["COUNT_COMMENT"] = sAspAdData[14];
							}
							iDataSetCnt++;
						}
					}
					pRecCount++;
				}
			}
		}

		pPageCnt = (int)Math.Ceiling(pRecCount / pRecPerPage);
		return ds;
	}

	public override SessionObjs GetOriginalObj() {
		return this.originalWomanObj;
	}

	private void SetAttrParam(SeekCondition pSeekCondition,HttpRequest pRequest) {

		for (int i = 0;i < ViCommConst.MAX_ATTR_COUNT;i++) {
			pSeekCondition.attrTypeSeq.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("typeseq{0:D2}",i + 1)]));
			pSeekCondition.attrValue.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("item{0:D2}",i + 1)]));
			pSeekCondition.groupingFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("group{0:D2}",i + 1)]));
			pSeekCondition.likeSearchFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("like{0:D2}",i + 1)]));
			pSeekCondition.notEqualFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("noteq{0:D2}",i + 1)]));
			pSeekCondition.rangeFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("range{0:D2}",i + 1)]));
			pSeekCondition.seqRangeFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("seqrange{0:D2}",i + 1)]));
		}

	}
}

