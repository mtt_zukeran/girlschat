/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: キャスト報酬設定
--	Progaram ID		: CastCharge
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class CastCharge:DbSession {

	public CastCharge() {
	}

	public int GetPayAmt(string pSiteCd,string pChargeType,string pActCategorySeq,string pCastRank) {
		DataSet ds;
		DataRow dr;

		string sValue = "0";

		try {
			conn = DbConnect("CastCharge.GetPayAmt");
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("CHARGE_POINT_NORMAL	, ").AppendLine();
			sSql.Append("CHARGE_POINT_NORMAL2	, ").AppendLine();
			sSql.Append("CHARGE_POINT_NORMAL3	, ").AppendLine();
			sSql.Append("CHARGE_POINT_NORMAL4	, ").AppendLine();
			sSql.Append("CHARGE_POINT_NORMAL5	").AppendLine();
			sSql.Append("FROM T_CAST_CHARGE ").AppendLine();
			sSql.Append("WHERE").AppendLine();
			sSql.Append("SITE_CD			= :SITE_CD			AND").AppendLine();
			sSql.Append("CHARGE_TYPE		= :CHARGE_TYPE		AND").AppendLine();
			sSql.Append("ACT_CATEGORY_SEQ	= :ACT_CATEGORY_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CHARGE_TYPE",pChargeType);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_CHARGE");
					if (ds.Tables["T_CAST_CHARGE"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sValue = dr["CHARGE_POINT_NORMAL"].ToString();
						switch (pCastRank) {
							case "B":
								sValue = dr["CHARGE_POINT_NORMAL2"].ToString();
								break;
							case "C":
								sValue = dr["CHARGE_POINT_NORMAL3"].ToString();
								break;
							case "D":
								sValue = dr["CHARGE_POINT_NORMAL4"].ToString();
								break;
							case "E":
								sValue = dr["CHARGE_POINT_NORMAL5"].ToString();
								break;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return int.Parse(sValue);
	}
}
