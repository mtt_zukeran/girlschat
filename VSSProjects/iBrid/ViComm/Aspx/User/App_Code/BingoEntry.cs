﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾋﾞﾝｺﾞｴﾝﾄﾘｰ
--	Progaram ID		: BingoEntry
--
--  Creation Date	: 2011.07.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class BingoEntry : DbSession {
	public BingoEntry() { }

	public string EntryBingo(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ENTRY_BINGO");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			return oDbSession.GetStringValue("pRESULT");
		}
	}
}
