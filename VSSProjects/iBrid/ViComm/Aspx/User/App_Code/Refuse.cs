﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 拒否
--	Progaram ID		: Refuse
--
--  Creation Date	: 2009.08.07
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Refuse:DbSession {
	public string refuseType;
	public string refuseComment;

	public Refuse() {
		refuseType = "";
		refuseComment = "";
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_REFUSE02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,USER_SEQ,REFUSE_DATE DESC ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"LOGIN_ID				," +
								"USER_SEQ				," +
								"PARTNER_USER_SEQ		," +
								"CHARACTER_ONLINE_STATUS," +
								"REFUSE_DATE			," +
								"HANDLE_NM				," +
								"LAST_LOGIN_DATE		," +
								"REFUSE_COMMENT			" +
							"FROM(" +
							" SELECT VW_REFUSE02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_REFUSE02  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_REFUSE02");
				}
			}
		} finally {
			conn.Close();
		}

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));

		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
			list.Add(new OracleParameter("pUserCharNo",pUserCharNo));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public void RefuseMainte(
		string pSiteCd,
		string pUserSeq,
		string pSexCd,
		string pPartnerUserSeq,
		string pPartnerUserCharNo,
		string pRefuseType,
		string pRefuseComment,
		int pDelFlag
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REFUSE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_SEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("PREFUSE_TYPE",DbSession.DbType.VARCHAR2,pRefuseType);
			db.ProcedureInParm("PREFUSE_COMMENT",DbSession.DbType.VARCHAR2,pRefuseComment);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
								"REFUSE_TYPE," +
								"REFUSE_COMMENT " +
							 "FROM T_REFUSE " +
								"WHERE " +
									"SITE_CD				=:SITE_CD				AND " +
									"USER_SEQ				=:USER_SEQ				AND " +
									"USER_CHAR_NO			=:USER_CHAR_NO			AND " +
									"PARTNER_USER_SEQ		=:PARTNER_USER_SEQ		AND " +
									"PARTNER_USER_CHAR_NO	=:PARTNER_USER_CHAR_NO		";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_REFUSE");
					if (ds.Tables["T_REFUSE"].Rows.Count != 0) {
						dr = ds.Tables["T_REFUSE"].Rows[0];
						refuseType = dr["REFUSE_TYPE"].ToString();
						refuseComment = dr["REFUSE_COMMENT"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
