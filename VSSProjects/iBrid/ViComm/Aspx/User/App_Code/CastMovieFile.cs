﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画
--	Progaram ID		: CastMovieFile
--
--  Creation Date	: 2010.06.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class CastMovieFile : DbSession {
	public CastMovieFile() {}
	
	public void GetCastMovieFile(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieSeq,string pMobileCarrierCd,string pMobileModel, string pTargetUseType,out string[] pFileNmArray, out string pLoginId,out string pSizeType,out string pFileFormat){

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_CAST_MOVIE_FILE");
			db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			db.ProcedureInParm("PUSER_SEQ", DbSession.DbType.VARCHAR2, pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO", DbSession.DbType.VARCHAR2, pUserCharNo);
			db.ProcedureInParm("PMOVIE_SEQ", DbSession.DbType.VARCHAR2, pMovieSeq);
			db.ProcedureInParm("PMOBILE_CARRIER_CD", DbSession.DbType.VARCHAR2, pMobileCarrierCd);
			db.ProcedureInParm("PMOBILE_MODEL", DbSession.DbType.VARCHAR2, pMobileModel);
			db.ProcedureInParm("PTARGET_USE_TYPE", DbSession.DbType.VARCHAR2, pTargetUseType);

			db.ProcedureOutParm("PSIZE_TYPE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFILE_FORMAT", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFILE_NUM", DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("PMOVIE_FILE_NM_LIST", DbSession.DbType.VARCHAR2,999);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_ID", DbSession.DbType.VARCHAR2);
			
			db.ExecuteProcedure();

			pSizeType = db.GetStringValue("PSIZE_TYPE");
			pFileFormat = db.GetStringValue("PFILE_FORMAT");
			pLoginId = db.GetStringValue("PLOGIN_ID");
			int iFileNum = db.GetIntValue("PFILE_NUM");
			
			pFileNmArray = new string[iFileNum];
			for(int i=0;i<pFileNmArray.Length;i++) {
				pFileNmArray[i] = db.GetArryStringValue("PMOVIE_FILE_NM_LIST",i);												
			}
		}
		
	}
	
	
}
