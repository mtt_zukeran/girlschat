﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者写真
--	Progaram ID		: CastPic
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System.Data;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Configuration;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Collections.Generic;

[System.Serializable]
public class CastPicGacha:DbSession {

	public CastPicGacha()
		: base() {
	}
	
	public void GetCastPicGacha(string pSiteCd,string pUserSeq,string pUserCharNo,out string pWinFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_CAST_PIC_GACHA");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pWIN_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pWinFlag = db.GetStringValue("pWIN_FLAG");
		}
	}
	
	public string GetCastPicGachaWinCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sValue = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	WIN_COUNT									");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_CAST_PIC_GACHA_WIN						");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) { 
			sValue = oDataSet.Tables[0].Rows[0]["WIN_COUNT"].ToString();
		}

		return sValue;
	}
}