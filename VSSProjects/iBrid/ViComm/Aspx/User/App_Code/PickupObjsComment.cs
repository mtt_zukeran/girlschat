﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ピックアップオブジェコメント
--	Progaram ID		: PickupObjsComment
--
--  Creation Date	: 2013.05.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class PickupObjsCommentSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;
	public string SelfUserCharNo;
	
	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["usercharno"];
		}
		set {
			this.Query["usercharno"] = value;
		}
	}

	public string PickupId {
		get {
			return this.Query["pickupid"];
		}
		set {
			this.Query["pickupid"] = value;
		}
	}
	
	public string ObjSeq {
		get {
			return this.Query["objseq"];
		}
		set {
			this.Query["objseq"] = value;
		}
	}

	public string PickupObjsCommentSeq {
		get {
			return this.Query["commentseq"];
		}
		set {
			this.Query["commentseq"] = value;
		}
	}

	public PickupObjsCommentSeekCondition()
		: this(new NameValueCollection()) {
	}

	public PickupObjsCommentSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["usercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["usercharno"]));
		this.Query["pickupid"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["pickupid"]));
		this.Query["objseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["objseq"]));
		this.Query["commentseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["commentseq"]));
	}
}

#endregion ============================================================================================================

public class PickupObjsComment:ManBase {
	public PickupObjsComment()
		: base() {
	}
	public PickupObjsComment(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(PickupObjsCommentSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PICKUP_OBJS_COMMENT00 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(PickupObjsCommentSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "						,	");
		oSqlBuilder.AppendLine("	P.PICKUP_OBJS_COMMENT_SEQ					,	");
		oSqlBuilder.AppendLine("	P.PICKUP_ID									,	");
		oSqlBuilder.AppendLine("	P.OBJ_SEQ									,	");
		oSqlBuilder.AppendLine("	P.COMMENT_TEXT								,	");
		oSqlBuilder.AppendLine("	P.COMMENT_DATE								,	");
		oSqlBuilder.AppendLine("	P.NG_SKULL_COUNT								");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_PICKUP_OBJS_COMMENT00 P						");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.COMMENT_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(PickupObjsCommentSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.PickupId)) {
			SysPrograms.SqlAppendWhere(" P.PICKUP_ID	= :PICKUP_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PICKUP_ID",pCondition.PickupId));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjSeq)) {
			SysPrograms.SqlAppendWhere(" P.OBJ_SEQ	= :OBJ_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_SEQ",pCondition.ObjSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PickupObjsCommentSeq)) {
			SysPrograms.SqlAppendWhere(" P.PICKUP_OBJS_COMMENT_SEQ		= :PICKUP_OBJS_COMMENT_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PICKUP_OBJS_COMMENT_SEQ",pCondition.PickupObjsCommentSeq));
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" P.ADMIN_DEL_FLAG		= :ADMIN_DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":ADMIN_DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.DEL_FLAG		= :DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		return oParamList.ToArray();
	}

	public void WritePickupObjsComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pPickupId,string pObjSeq,string pCommentText,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_PICKUP_OBJS_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPICKUP_ID",DbSession.DbType.VARCHAR2,pPickupId);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,pObjSeq);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void DeletePickupObjsComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pPickupObjsCommentSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_PICKUP_OBJS_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPICKUP_OBJS_COMMENT_SEQ",DbSession.DbType.VARCHAR2,pPickupObjsCommentSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public string GetLastCommentDay(string pSiteCd,string pUserSeq,string pUserCharNo,string pPickupId,string pObjSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	TO_CHAR(MAX(COMMENT_DATE),'YYYY/MM/DD') AS LAST_COMMENT_DAY		");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_PICKUP_OBJS_COMMENT						");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	PICKUP_ID		= :PICKUP_ID			AND	");
		oSqlBuilder.AppendLine("	OBJ_SEQ			= :OBJ_SEQ					");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PICKUP_ID",pPickupId));
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return oDataSet.Tables[0].Rows[0]["LAST_COMMENT_DAY"].ToString();
		} else {
			return string.Empty;
		}
	}
}