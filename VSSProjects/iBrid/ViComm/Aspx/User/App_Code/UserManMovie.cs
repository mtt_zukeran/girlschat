﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員動画

--	Progaram ID		: UserManMovie
--
--  Creation Date	: 2009.08.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using ViComm;

[System.Serializable]
public class UserManMovie:DbSession {

	public UserManMovie() {
	}

	public bool GetUserManMovieInfo(string pMovieSeq,out string pLoginId) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		pLoginId = "";
		try {
			conn = DbConnect("UserManMovie.GetUserManMovieInfo");

			string sSql = "SELECT LOGIN_ID FROM VW_USER_MAN_MOVIE02 " +
							"WHERE " +
							" MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_MOVIE02");
					if (ds.Tables["VW_USER_MAN_MOVIE02"].Rows.Count != 0) {
						dr = ds.Tables["VW_USER_MAN_MOVIE02"].Rows[0];
						pLoginId = dr["LOGIN_ID"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCountByLoginId(string pSiteCd,string pLoginId,string pMovieSeq,string pMovieType,string pNotApproveStatus,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("UserManMovie.GetPageCountByLoginId");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_MOVIE01 ";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhereByLoginId(pSiteCd,pLoginId,pMovieSeq,pMovieType,pNotApproveStatus,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollectionByLoginId(string pSiteCd,string pLoginId,string pMovieSeq,string pMovieType,string pNotApproveStatus,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try {
			conn = DbConnect("UserManMovie.GetPageCollectionByLoginId");
			ds = new DataSet();

			string sOrder = "ORDER BY D.USER_SEQ,D.MOVIE_SEQ DESC";
			string sOrder2 = "ORDER BY USER_SEQ,MOVIE_SEQ DESC";


			string sSql = "SELECT * FROM " +
							"(" +
							"SELECT " +
								"USER_SEQ				," +
								"MOVIE_SEQ				," +
								"MOVIE_TITLE			," +
								"UPLOAD_DATE			," +
								"OBJ_NOT_APPROVE_FLAG	," +
								"OBJ_NOT_PUBLISH_FLAG	," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							"FROM VW_USER_MAN_MOVIE01 D ";

			string sWhere = "";

			OracleParameter[] objParms = CreateWhereByLoginId(pSiteCd,pLoginId,pMovieSeq,pMovieType,pNotApproveStatus,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder2;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_MOVIE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhereByLoginId(string pSiteCd,string pLoginId,string pMovieSeq,string pMovieType,string pNotApproveStatus,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = " WHERE SITE_CD = :SITE_CD AND  LOGIN_ID = :LOGIN_ID ";

		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("LOGIN_ID",pLoginId));

		if (!pMovieSeq.Equals("")) {
			pWhere = pWhere + " AND MOVIE_SEQ = :MOVIE_SEQ ";
			list.Add(new OracleParameter("MOVIE_SEQ",pMovieSeq));
		}
		if (!pMovieType.Equals("")) {
			pWhere = pWhere + " AND MOVIE_TYPE = :MOVIE_TYPE ";
			list.Add(new OracleParameter("MOVIE_TYPE",pMovieType));
		}
		if (!pNotApproveStatus.Equals(string.Empty)) {
			pWhere += "AND OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG ";
			list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveStatus));
		}
		pWhere += "AND OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG ";
		list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",ViComm.ViCommConst.FLAG_OFF_STR));
		

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void UpdateProfileMovie(string pSiteCd,string pUserSeq,string pMovieSeq,string pMovieTitle,int pDelFlag,int pMovieType) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_USER_MAN_PROFILE_MOVIE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,pMovieSeq);
			db.ProcedureInParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2,pMovieTitle);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PMOVIE_TYPE",DbSession.DbType.NUMBER,pMovieType);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public string GetMovieTitleByMovieSeq(string pMovieSeq,string pSiteCd) {
		DataSet ds;
		DataRow dr;

		string sMovieTitle = "";
		try {
			conn = DbConnect("UserManMovie.GetMovieTitleByMovieSeq");

			string sSql = "SELECT MOVIE_TITLE FROM T_USER_MAN_MOVIE " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_MAN_MOVIE");

					if (ds.Tables["T_USER_MAN_MOVIE"].Rows.Count != 0) {
						dr = ds.Tables["T_USER_MAN_MOVIE"].Rows[0];
						sMovieTitle = dr["MOVIE_TITLE"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sMovieTitle;
	}
	public string GetValueByMovieSeq(string pSiteCd,string pMovieSeq,string pValue) {
		DataSet ds;
		DataRow dr;

		string sValue = string.Empty;
		try {
			conn = DbConnect("UserManMovie.GetValueByMovieSeq");

			string sSql = "SELECT MOVIE_TITLE,UPLOAD_DATE FROM T_USER_MAN_MOVIE " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_MAN_MOVIE");

					if (ds.Tables["T_USER_MAN_MOVIE"].Rows.Count != 0) {
						dr = ds.Tables["T_USER_MAN_MOVIE"].Rows[0];
						sValue = dr[pValue].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sValue;
	}

}
