﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者(基本クラス)
--	Progaram ID		: Cast
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[System.Serializable]
public class Cast:CastBase {

	public string loginId;
	public string actCategoryNm;
	public string castSeq;
	public string castNm;
	public string castPicPath;
	public string castPicPathSmall;
	public string commentList;
	public string commentDetail;
	public string handleNm;
	public string characterOnlineStatus;
	public string actCategorySeq;
	public string diaryHeaderTitle;

	public decimal recCount;

	public Cast()
		: base() {
	}
	public Cast(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}


	public bool GetCastInfo(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("Cast.GetCastInfo");

			string sSql = "SELECT " +
								"ACT_CATEGORY_NM	," +
								"LOGIN_ID			," +
								"USER_SEQ			," +
								"CAST_NM			," +
								"ACT_CATEGORY_SEQ	," +
								"SMALL_PHOTO_IMG_PATH," +
								"PHOTO_IMG_PATH		," +
								"COMMENT_LIST		," +
								"HANDLE_NM			," +
								"DIARY_TITLE DIARY_HEADER_TITLE	," +
								"CHARACTER_ONLINE_STATUS " +
							"FROM " +
							" VW_CAST01 " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND USER_SEQ = :USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST01");

					if (ds.Tables["VW_CAST01"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST01"].Rows[0];
						actCategoryNm = dr["ACT_CATEGORY_NM"].ToString();
						castSeq = dr["USER_SEQ"].ToString();
						loginId = dr["LOGIN_ID"].ToString();
						castNm = dr["CAST_NM"].ToString();
						castPicPathSmall = dr["SMALL_PHOTO_IMG_PATH"].ToString();
						castPicPath = dr["PHOTO_IMG_PATH"].ToString();
						commentList = dr["COMMENT_LIST"].ToString();
						handleNm = dr["HANDLE_NM"].ToString();
						characterOnlineStatus = dr["CHARACTER_ONLINE_STATUS"].ToString();
						actCategorySeq = dr["ACT_CATEGORY_SEQ"].ToString();
						diaryHeaderTitle = dr["DIARY_HEADER_TITLE"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
	public int GetPageCount(SeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pCondition,pRecPerPage,out pRecCount);
	}
	private int GetPageCountBase(
		FlexibleQueryType pFlexibleQueryType,
		SeekCondition pCondition,
		int pRecPerPage,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(pFlexibleQueryType,pCondition);

		try {
			conn = DbConnect("Cast.GetPageCount");
			ds = new DataSet();

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCast"]).Equals("1")) {
				sSuffix = "MV";
			}
			StringBuilder sSql = new StringBuilder(string.Format("SELECT COUNT(*) AS ROW_COUNT FROM {0}_CAST_CHARACTER00 P ",sSuffix));
			string sJoinTables = "";
			string sOrder = "";
			string sWhere = "";

			SetJoinTable(pCondition,out sJoinTables,out sOrder);
			sSql.Append(sJoinTables);

			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pCondition,false,ref sWhere);
			sSql.Append(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				this.Fill(da,ds,"VW_CAST_CHARACTER00");

				if (ds.Tables["VW_CAST_CHARACTER00"].Rows.Count != 0) {
					dr = ds.Tables["VW_CAST_CHARACTER00"].Rows[0];
					recCount = Decimal.Parse(dr["ROW_COUNT"].ToString());
					pRecCount = recCount;
					iPages = (int)Math.Ceiling(recCount / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}

		if (pFlexibleQueryType == FlexibleQueryType.None && oFlexibleQueryTarget.HasTarget) {
			decimal dExcludeRecCount = decimal.Zero;
			GetPageCountBase(FlexibleQueryType.InScope,pCondition,pRecPerPage,out dExcludeRecCount);
			iPages = CalcExcludeCount(ref pRecCount,dExcludeRecCount,pRecPerPage);
			recCount = pRecCount;
		}

		return iPages;
	}

	public DataSet GetPageCollection(
		SeekCondition pCondition,
		int pPageNo,
		int pRecPerPage
	) {
		return GetPageCollectionBase(pCondition,pPageNo,pRecPerPage,true,"","","","");
	}

	public DataSet GetPageCollection(SeekCondition pCondition,int pPageNo,int pRecPerPage,bool pGetDetailInfo) {
		return GetPageCollectionBase(pCondition,pPageNo,pRecPerPage,pGetDetailInfo,"","","","");
	}

	public int GetRNumByUserSeq(
		SeekCondition pCondition,
		string pUserSeq,
		string pUserCharNo
	) {
		DataSet ds = GetPageCollectionBase(pCondition,1,1,false,pUserSeq,pUserCharNo,"","");
		if (ds.Tables[0].Rows.Count > 0) {
			return int.Parse(ds.Tables[0].Rows[0]["RNUM"].ToString());
		} else {
			return 0;
		}
	}

	public int GetRNumByUserSeqAndCondtion(
		SeekCondition pCondition,
		string pUserSeq,
		string pUserCharNo,
		string pConditionFieldNm,
		string pConditionFieldValue
	) {
		DataSet ds = GetPageCollectionBase(pCondition,1,1,false,pUserSeq,pUserCharNo,pConditionFieldNm,pConditionFieldValue);
		if (ds.Tables[0].Rows.Count > 0) {
			return int.Parse(ds.Tables[0].Rows[0]["RNUM"].ToString());
		} else {
			return 0;
		}
	}

	public DataSet GetOneByUserSeqAndCondtion(
		SeekCondition pCondition,
		string pUserSeq,
		string pUserCharNo,
		string pConditionFieldNm,
		string pConditionFieldValue,
		int pRNum
	) {
		DataSet ds = GetPageCollectionBase(pCondition,1,1,true,pUserSeq,pUserCharNo,pConditionFieldNm,pConditionFieldValue);
		if (ds.Tables[0].Rows.Count > 0) {
			ds.Tables[0].Rows[0]["RNUM"] = pRNum;
		}
		return ds;
	}


	public DataSet GetPageCollectionBase(
		SeekCondition pCondition,
		int pPageNo,
		int pRecPerPage,
		bool pGetDetailInfo,
		string pUserSeq,
		string pUserCharNo,
		string pConditionFieldNm,
		string pConditionFieldValue
	) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = CreateFlexibleQueryTarget(FlexibleQueryType.None,pCondition);

		DataSet ds;
		try {
			conn = DbConnect("Cast.GetPageCollection");
			ds = new DataSet();

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCast"]).Equals("1")) {
				sSuffix = "MV";
			}

			StringBuilder sInnerSql = new StringBuilder();
			sInnerSql.Append("SELECT * FROM ").AppendLine();
			sInnerSql.Append("(SELECT ROWNUM AS RNUM,INNER.* FROM (").AppendLine();
			sInnerSql.Append("	SELECT " + CastInfoFields(pCondition.conditionFlag) + string.Format(" FROM {0}_CAST_CHARACTER00 P ",sSuffix));
			string sJoinTables = "";
			string sOrder = "";
			string sWhere = "";

			SetJoinTable(pCondition,out sJoinTables,out sOrder);
			sInnerSql.Append(sJoinTables);

			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pCondition,true,ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			if (pUserSeq.Equals(string.Empty)) {
				sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sInnerSql.Append(")INNER ");
				sInnerSql.Append(")WHERE USER_SEQ = :USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO ");
				if (!pConditionFieldNm.Equals("")) {
					sInnerSql.Append(string.Format(" AND {0} = :{0} ",pConditionFieldNm));
				}
			}

			SessionObjs oObj = this.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			string sJoinField = "--";
			if (pCondition.conditionFlag == ViCommConst.INQUIRY_CAST_MOVIE) {
				sJoinField = " GET_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T_CAST_MOVIE.THUMBNAIL_PIC_SEQ,T1.PROFILE_PIC_SEQ) AS THUMBNAIL_IMG_PATH , ";
			}
			sSql = string.Format(sSql,sInnerSql.ToString(),"--","--",sJoinField);

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				if (pUserSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
				} else {
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
					if (!pConditionFieldNm.Equals("")) {
						cmd.Parameters.Add(pConditionFieldNm,pConditionFieldValue);
					}
				}
				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_CAST_CHARACTER00");
				}
			}
		} finally {
			conn.Close();
		}
		ds = this.Exclude(ds,pRecPerPage,oFlexibleQueryTarget);

		FakeOnlineStatus(ds);

		if (pGetDetailInfo == true) {
			AppendCastAttr(ds);
		}
		return ds;
	}

	private void SetJoinTable(SeekCondition pCondition,out string pJoinTables,out string pOrder) {
		StringBuilder sJoinTable = new StringBuilder();
		pOrder = "";
		pJoinTables = "";

		switch (pCondition.conditionFlag) {
			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2:
			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2:
				sJoinTable.Append(",T_CAST_RANKING R ");
				pOrder = " ORDER BY P.SITE_CD,R.RANKING_TYPE,R.RANKING ";
				break;

			case ViCommConst.INQUIRY_CAST_NEW_FACE:
				pOrder = " ORDER BY P.START_PERFORM_DAY DESC,P.SITE_CD,P.NA_FLAG,P.PRIORITY,P.USER_SEQ DESC,P.USER_CHAR_NO ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING:
				sJoinTable.Append(",T_RANKING_EX MR ");
				pOrder = " ORDER BY MR.SITE_CD,MR.SUMMARY_COUNT DESC,MR.LAST_LOGIN_DATE DESC,MR.USER_SEQ ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_MOTE_CON:
				sJoinTable.Append(",T_RANKING_EX MC ");
				pOrder = " ORDER BY MC.SITE_CD,MC.SUMMARY_COUNT DESC,MC.USER_SEQ ";
				break;

			case ViCommConst.INQUIRY_FAVORIT:
				sJoinTable.Append(",T_FAVORIT F,T_FAVORIT_GROUP FG ");
				if ("1".Equals(pCondition.sortExType)) {
					pOrder = "ORDER BY P.SITE_CD, F.LAST_COMM_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
				} else if ("2".Equals(pCondition.sortExType)) {
					pOrder = "ORDER BY P.SITE_CD, P.LAST_LOGIN_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
				} else {
					pOrder = " ORDER BY P.SITE_CD,F.REGIST_DATE DESC,P.USER_SEQ,P.USER_CHAR_NO ";
				}
				break;

			case ViCommConst.INQUIRY_LIKE_ME:
				sJoinTable.Append(",T_FAVORIT F ");
				if ("1".Equals(pCondition.sortExType)) {
					pOrder = " ORDER BY P.SITE_CD,F.LAST_COMM_DATE DESC,P.USER_SEQ,P.USER_CHAR_NO ";
				} else if ("2".Equals(pCondition.sortExType)) {
					pOrder = "ORDER BY P.SITE_CD, P.LAST_LOGIN_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
				} else {
					pOrder = " ORDER BY P.SITE_CD,F.REGIST_DATE DESC,P.USER_SEQ,P.USER_CHAR_NO ";
				}
				break;

			case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
				sJoinTable.Append(",T_FAVORIT F1, T_FAVORIT F2 ");
				if ("1".Equals(pCondition.sortExType)) {
					pOrder = "ORDER BY P.SITE_CD, F1.LAST_COMM_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
				}
				break;

			case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
			case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
				sJoinTable.Append(",T_FAVORIT LL ");
				pOrder = " ORDER BY P.SITE_CD,LL.LAST_VIEW_PROFILE_DATE DESC,P.USER_SEQ,P.USER_CHAR_NO ";
				break;

			case ViCommConst.INQUIRY_MARKING:
			case PwViCommConst.INQUIRY_SELF_MARKING:
				sJoinTable.Append(",T_MARKING MK ");
				pOrder = " ORDER BY P.SITE_CD,MK.MARKING_DATE DESC,P.PRIORITY,P.USER_SEQ,P.USER_CHAR_NO ";
				break;

			case ViCommConst.INQUIRY_CAST_DIARY:
				sJoinTable.Append(",T_CAST_DIARY D ");
				pOrder = " ORDER BY P.SITE_CD,D.REPORT_DAY DESC,D.CREATE_DATE DESC,P.PRIORITY,P.USER_SEQ,P.USER_CHAR_NO ";
				break;

			case ViCommConst.INQUIRY_FORWARD:
				sJoinTable.Append(",VW_RECORDING01 RC ");
				pOrder = " ORDER BY P.SITE_CD,RC.RECORDING_DATE DESC,P.USER_SEQ,P.USER_CHAR_NO ";
				break;

			case ViCommConst.INQUIRY_CAST_MOVIE:
				pOrder = " ORDER BY P.SITE_CD,P.PROFILE_MOVIE_SEQ DESC,P.USER_SEQ,P.USER_CHAR_NO ";
				break;

			case ViCommConst.INQUIRY_REFUSE:
				sJoinTable.Append(",T_REFUSE RF ");
				pOrder = " ORDER BY P.SITE_CD,RF.REGIST_DATE DESC,P.USER_SEQ ";
				break;

			case ViCommConst.INQUIRY_RECOMMEND:
				sJoinTable.Append(",VW_CHARACTER_PICKUP02 PC ");
				if (pCondition.directRandom == ViCommConst.FLAG_ON) {
					pOrder = " ORDER BY DBMS_RANDOM.RANDOM,PC.PRIORITY ASC ";
				} else if (!string.IsNullOrEmpty(pCondition.sortType)) {
					switch (pCondition.sortType) {
						case ViCommConst.SortType.ONLINE_STATUS:	//オンライン優先
							pOrder = "ORDER BY P.MONITOR_TALK_TYPE DESC,DECODE(P.CHARACTER_ONLINE_STATUS,2,5,1,4,P.CHARACTER_ONLINE_STATUS) DESC,PC.PRIORITY ASC,P.LOGIN_ID ASC";
							break;
						default:
							pOrder = " ORDER BY PC.PRIORITY ASC, P.LOGIN_ID ";
							break;
					}
				} else {
					pOrder = " ORDER BY PC.PRIORITY ASC, P.LOGIN_ID ";
				}
				break;

			case ViCommConst.INQUIRY_MEMO:
				sJoinTable.Append(",T_MEMO ME ");
				pOrder = " ORDER BY P.SITE_CD,ME.UPDATE_DATE DESC,P.USER_SEQ ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE:
				pOrder = " ORDER BY P.SITE_CD,P.QUICK_RES_FLAG,P.QUICK_RES_IN_TIME DESC,P.QUICK_RES_OUT_SCH_TIME ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
				sJoinTable.Append(",T_BINGO_ENTRY BE ");
				pOrder = " ORDER BY P.SITE_CD,BE.BINGO_APPLICATION_DATE DESC,P.USER_SEQ ";
				break;

			case ViCommConst.INQUIRY_CAST_LOGINED:
				if ("1".Equals(pCondition.sortExType)) {
					pOrder = " ORDER BY P.SITE_CD,P.LAST_LOGIN_DATE DESC,P.NA_FLAG,P.PRIORITY,P.CHARACTER_ONLINE_STATUS,P.USER_SEQ,P.USER_CHAR_NO ";
				}
				break;
			case ViCommConst.INQUIRY_ONLINE:
				pOrder = " ORDER BY P.SITE_CD,P.CHARACTER_ONLINE_STATUS,P.LAST_ACTION_DATE DESC ";
				break;

			default:
				break;
		}

		if (string.IsNullOrEmpty(pOrder)) {
			string sRandomColumn = string.Empty;
			//NULL_LAST
			if (pCondition.random == 1) {
				sRandomColumn = string.Format("P.RANDOM_{0:D2},",SessionObjs.Current.randomNum4Cast);
			}
			pOrder = string.Format(" ORDER BY P.SITE_CD,P.NA_FLAG,{0}P.PRIORITY,P.LAST_ACTION_DATE DESC,P.CHARACTER_ONLINE_STATUS,P.USER_SEQ,P.USER_CHAR_NO ",sRandomColumn);

			if (!pCondition.categorySeq.Equals("")) {
				pOrder += ",P.ACT_CATEGORY_SEQ";
			}

			if (!pCondition.userRank.Equals("")) {
				pOrder += ",P.USER_RANK";
			}

		}
		if (pCondition.conditionFlag == ViCommConst.INQUIRY_CONDITION || pCondition.hasExtend) {
			for (int i = 0;i < pCondition.attrValue.Count;i++) {
				if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
					sJoinTable.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
				}
			}
		}


		//ORDER BYのパラメータがきた場合をパラメータによりORDER BY文を生成する
		if (!pCondition.orderAsc.Equals(string.Empty) || !pCondition.orderDesc.Equals(string.Empty)) {
			pOrder = " ORDER BY P.SITE_CD ";
			if (!pCondition.orderAsc.Equals(string.Empty)) {
				pOrder += "," + pCondition.orderAsc + " ASC ";
			}
			if (!pCondition.orderDesc.Equals(string.Empty)) {
				pOrder += "," + pCondition.orderDesc + " DESC ";
			}
			pOrder += "," + " P.USER_SEQ,P.USER_CHAR_NO ";
		}

		if (pCondition.directRandom.Equals(ViCommConst.FLAG_ON)) {
			pOrder = " ORDER BY DBMS_RANDOM.RANDOM ";
		}

		pJoinTables = sJoinTable.ToString();
		return;
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,int pRecNo) {

		DataSet ds;
		try {
			conn = DbConnect("Cast.GetOne");
			ds = new DataSet();

			string sInnerSql = "SELECT " +
									CastInfoFields(0) +
								",'" + pRecNo + "' RNUM " +
							"FROM " +
								"VW_CAST_CHARACTER00 P ";

			sInnerSql += "WHERE ";
			sInnerSql += "P.SITE_CD			= :SITE_CD	AND " +
						 "P.USER_SEQ		= :USER_SEQ	AND " +
						 "P.USER_CHAR_NO	= :USER_CHAR_NO	";

			SessionObjs oObj = this.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();
			sSql = string.Format(sSql,sInnerSql,"","","");

			ArrayList list = new ArrayList();
			InnnerCondition(ref list);
			OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				// Join Condition
				list.Clear();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARACTER00");
				}
			}
		} finally {
			conn.Close();
		}

		FakeOnlineStatus(ds);
		AppendCastAttr(ds);

		return ds;
	}

	public DataSet GetOneByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,int pRecNo) {
		DataSet ds;
		try {
			conn = DbConnect("Cast.GetOneByLoginId");
			ds = new DataSet();

			string sInnerSql = "SELECT " +
								CastInfoFields(0) +
								",'" + pRecNo + "' RNUM " +
							"FROM " +
								"VW_CAST_CHARACTER00 P  ";

			sInnerSql += "WHERE ";
			sInnerSql += "P.SITE_CD			= :SITE_CD	AND " +
						 "P.LOGIN_ID		= :LOGIN_ID	AND " +
						 "P.USER_CHAR_NO	= :USER_CHAR_NO	";

			SessionObjs oObj = this.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();
			sSql = string.Format(sSql,sInnerSql,"","","");

			ArrayList list = new ArrayList();
			InnnerCondition(ref list);
			OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("LOGIN_ID",pLoginId);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				// Join Condition
				list.Clear();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_CAST_CHARACTER00");
				}
			}
		} finally {
			conn.Close();
		}

		FakeOnlineStatus(ds);
		AppendCastAttr(ds);

		return ds;
	}

	private string CastInfoFields(ulong pConditionFlag) {
		string sField = CastBasicField();

		sField = sField + ",ENABLED_BLOG_FLAG ";
		sField = sField + ",DECODE(ENABLED_BLOG_FLAG,1,BLOG_SEQ,NULL) AS BLOG_SEQ ";
		sField = sField + ",BLOG_TITLE ";
		sField = sField + ",BLOG_DOC ";

		switch (pConditionFlag) {
			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2:
			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2:
				sField = sField + ",R.RANKING_TYPE,R.RANKING,R.CREATE_START_DATE,R.CREATE_END_DATE ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING:
				sField = sField + ",'D' AS RANKING_TYPE,ROWNUM AS RANKING ,MR.SUMMARY_COUNT,MR.RANKING_CTL_SEQ ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_MOTE_CON:
				sField = sField + ",ROWNUM AS RANKING, MC.SUMMARY_COUNT ";
				break;

			case ViCommConst.INQUIRY_FAVORIT:
				sField = sField + ",F.REGIST_DATE,F.TALK_COUNT,F.LAST_TALK_DATE,F.FAVORIT_GROUP_SEQ,FG.FAVORIT_GROUP_NM ";
				break;

			case ViCommConst.INQUIRY_LIKE_ME:
				sField = sField + ",F.REGIST_DATE,F.TALK_COUNT,F.LAST_TALK_DATE ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
			case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
				sField = sField + ",LL.LAST_VIEW_PROFILE_DATE ";
				break;

			case ViCommConst.INQUIRY_REFUSE:
				sField = sField + ",RF.REGIST_DATE REFUSE_REGIST_DATE";
				break;

			case ViCommConst.INQUIRY_MARKING:
			case PwViCommConst.INQUIRY_SELF_MARKING:
				sField = sField + ",MK.MARKING_DATE ,MK.REPORT_DAY	MARKING_DAY ";
				break;

			case ViCommConst.INQUIRY_CAST_DIARY:
				sField = sField + ",D.REPORT_DAY DIARY_DAY ";
				break;

			case ViCommConst.INQUIRY_FORWARD:
				sField = sField + ",RC.RECORDING_DATE ";
				break;

			case ViCommConst.INQUIRY_RECOMMEND:
				sField = sField + ",PC.PICKUP_HEADER_DOC ";
				sField = sField + ",PC.COMMENT_PICKUP COMMENT_RECOMMEND ";
				break;

			case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
				sField = sField + ",BE.BINGO_APPLICATION_DATE ";
				sField = sField + ",BE.BINGO_ELECTION_AMT ";
				sField = sField + ",BE.BINGO_ELECTION_POINT ";
				sField = sField + ",BE.BINGO_BALL_FLAG ";
				break;
		}
		return sField;
	}

	private OracleParameter[] CreateWhere(FlexibleQueryTargetInfo pFlexibleQueryTarget,SeekCondition pCondition,bool bAllInfo,ref string pWhere) {

		ArrayList list = new ArrayList();
		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		if (pCondition.enabledBlog.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" P.ENABLED_BLOG_FLAG = :ENABLED_BLOG_FLAG  ",ref pWhere);
			list.Add(new OracleParameter(":ENABLED_BLOG_FLAG",ViCommConst.FLAG_ON));
			SysPrograms.SqlAppendWhere(" P.BLOG_SEQ IS NOT NULL  ",ref pWhere);
		}

		switch (pCondition.onlineStatus) {
			case ViCommConst.SeekOnlineStatus.WAITING:
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) ",ref pWhere);
				list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));

				SysPrograms.SqlAppendWhere("P.LAST_ACTION_DATE >= :LAST_ACTION_DATE ",ref pWhere);
				list.Add(new OracleParameter("LAST_ACTION_DATE",OracleDbType.Date,DateTime.Now.AddDays(-1),ParameterDirection.Input));

				SysPrograms.SqlAppendWhere("P.PRIORITY > :PRIORITY ",ref pWhere);
				list.Add(new OracleParameter("PRIORITY",ViCommConst.FLAG_OFF));

				// 性能差が出ないためコメントアウト 
				//				if (pCondition.random == 1) {
				//					SysPrograms.SqlAppendWhere(string.Format("P.RANDOM_{0:D2}",SessionObjs.Current.randomNum4Cast) + " > :RANDOM ",ref pWhere);
				//					list.Add(new OracleParameter("RANDOM",ViCommConst.FLAG_OFF));
				//				}

				if (pCondition.conditionFlag == ViCommConst.INQUIRY_CAST_ONLINE_TV_PHONE) {
					SysPrograms.SqlAppendWhere("P.CONNECT_TYPE IN(:CONNECT_TYPE1,:CONNECT_TYPE2)",ref pWhere);
					list.Add(new OracleParameter("CONNECT_TYPE1",ViCommConst.WAIT_TYPE_VIDEO));
					list.Add(new OracleParameter("CONNECT_TYPE2",ViCommConst.WAIT_TYPE_BOTH));
				}
				if (pCondition.conditionFlag == ViCommConst.INQUIRY_CAST_MONITOR) {
					SysPrograms.SqlAppendWhere("P.MONITOR_TALK_TYPE > :MONITOR_TALK_TYPE AND P.SITE_CD = :SITE_CD AND P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
					list.Add(new OracleParameter("MONITOR_TALK_TYPE",ViCommConst.FLAG_OFF));
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
				}
				break;

			case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhere);
				list.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));

				SysPrograms.SqlAppendWhere("P.LAST_ACTION_DATE >= :LAST_ACTION_DATE ",ref pWhere);
				list.Add(new OracleParameter("LAST_ACTION_DATE",OracleDbType.Date,DateTime.Now.AddDays(-1),ParameterDirection.Input));

				SysPrograms.SqlAppendWhere("P.PRIORITY > :PRIORITY ",ref pWhere);
				list.Add(new OracleParameter("PRIORITY",ViCommConst.FLAG_OFF));

				// 性能差が出ないためコメントアウト 
				//				if (pCondition.random == 1) {
				//					SysPrograms.SqlAppendWhere(string.Format("P.RANDOM_{0:D2}",SessionObjs.Current.randomNum4Cast) + " > :RANDOM ",ref pWhere);
				//					list.Add(new OracleParameter("RANDOM",ViCommConst.FLAG_OFF));
				//				}

				break;

			default:
				if (pCondition.conditionFlag == ViCommConst.INQUIRY_CAST_TV_PHONE) {
					SysPrograms.SqlAppendWhere("P.CONNECT_TYPE IN(:CONNECT_TYPE1,:CONNECT_TYPE2) ",ref pWhere);
					list.Add(new OracleParameter("CONNECT_TYPE1",ViCommConst.WAIT_TYPE_VIDEO));
					list.Add(new OracleParameter("CONNECT_TYPE2",ViCommConst.WAIT_TYPE_BOTH));
				}
				break;
		}

		if (pCondition.newCastDay != 0) {
			SysPrograms.SqlAppendWhere("P.START_PERFORM_DAY  >= :NEW_DAY ",ref pWhere);
			list.Add(new OracleParameter("NEW_DAY",DateTime.Now.AddDays(-1 * pCondition.newCastDay).ToString("yyyy/MM/dd")));
		}

		if (pCondition.conditionFlag != 0) {

			switch (pCondition.conditionFlag) {
				case ViCommConst.INQUIRY_CAST_RECOMMEND:
					SysPrograms.SqlAppendWhere("((P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) OR (OFFLINE_DISPLAY_FLAG = :OFFLINE_DISPLAY_FLAG))",ref pWhere);
					list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					list.Add(new OracleParameter("OFFLINE_DISPLAY_FLAG",ViCommConst.FLAG_ON));
					break;

				case ViCommConst.INQUIRY_CAST_MOVIE:
					SysPrograms.SqlAppendWhere("P.PROFILE_MOVIE_SEQ IS NOT NULL",ref pWhere);
					break;

				case ViCommConst.INQUIRY_CAST_PICKUP:
					SysPrograms.SqlAppendWhere("P.PICKUP_FLAG = :PICKUP_FLAG",ref pWhere);
					list.Add(new OracleParameter("PICKUP_FLAG",ViCommConst.FLAG_ON));
					break;

				case ViCommConst.INQUIRY_RECOMMEND:
					SysPrograms.SqlAppendWhere("P.SITE_CD		= PC.SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= PC.USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= PC.USER_CHAR_NO	",ref pWhere);
					SysPrograms.SqlAppendWhere("PC.PICKUP_ID	= :PICKUP_ID		",ref pWhere);
					SysPrograms.SqlAppendWhere("PC.PICKUP_FLAG	= :PICKUP_FLAG		",ref pWhere);
					list.Add(new OracleParameter("PICKUP_ID",pCondition.pickupId));
					list.Add(new OracleParameter("PICKUP_FLAG",ViCommConst.FLAG_ON));
					pWhere = pWhere + " AND P.PROFILE_PIC_SEQ IS NOT NULL ";
					break;

				case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2:
					SysPrograms.SqlAppendWhere("R.SITE_CD			= P.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("R.USER_SEQ			= P.USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("R.USER_CHAR_NO		= P.USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("R.ACT_CATEGORY_SEQ	= P.ACT_CATEGORY_SEQ	",ref pWhere);
					SysPrograms.SqlAppendWhere("R.RANKING_TYPE		= :RANKING_TYPE			",ref pWhere);
					list.Add(new OracleParameter("RANKING_TYPE",ViCommConst.RANKING_DIARY_ACCESS_WEEKLY2));
					break;

				case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2:
					SysPrograms.SqlAppendWhere("R.SITE_CD			= P.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("R.USER_SEQ			= P.USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("R.USER_CHAR_NO		= P.USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("R.ACT_CATEGORY_SEQ	= P.ACT_CATEGORY_SEQ	",ref pWhere);
					SysPrograms.SqlAppendWhere("R.RANKING_TYPE		= :RANKING_TYPE			",ref pWhere);
					list.Add(new OracleParameter("RANKING_TYPE",ViCommConst.RANKING_DIARY_ACCESS_MONTHLY2));
					break;

				case ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING:
					SysPrograms.SqlAppendWhere("MR.SITE_CD			= P.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("MR.SUMMARY_TYPE		= :SUMMARY_TYPE			",ref pWhere);
					SysPrograms.SqlAppendWhere("MR.USER_SEQ			= P.USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("MR.USER_CHAR_NO		= P.USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("MR.RANKING_CTL_SEQ	= :RANKING_CTL_SEQ		",ref pWhere);
					list.Add(new OracleParameter("SUMMARY_TYPE",ViCommConst.ExRanking.EX_RANKING_MARKING));
					list.Add(new OracleParameter("RANKING_CTL_SEQ",pCondition.rankingCtlSeq));
					break;

				case ViCommConst.INQUIRY_EXTENSION_MOTE_CON:
					SysPrograms.SqlAppendWhere("MC.SITE_CD			= P.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("MC.SUMMARY_TYPE		= :SUMMARY_TYPE			",ref pWhere);
					SysPrograms.SqlAppendWhere("MC.USER_SEQ			= P.USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("MC.USER_CHAR_NO		= P.USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("MC.RANKING_CTL_SEQ	= :RANKING_CTL_SEQ		",ref pWhere);
					list.Add(new OracleParameter("SUMMARY_TYPE",ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN));
					list.Add(new OracleParameter("RANKING_CTL_SEQ",pCondition.rankingCtlSeq));
					break;

				case ViCommConst.INQUIRY_FAVORIT:
					SysPrograms.SqlAppendWhere("F.SITE_CD		= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					if (!string.IsNullOrEmpty(pCondition.favoritGroupSeq)) {
						if (pCondition.favoritGroupSeq.Equals("null")) {
							SysPrograms.SqlAppendWhere("F.FAVORIT_GROUP_SEQ	IS NULL ",ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere("F.FAVORIT_GROUP_SEQ = :FAVORIT_GROUP_SEQ ",ref pWhere);
						}
					}
					SysPrograms.SqlAppendWhere("P.SITE_CD		= F.SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= F.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= F.PARTNER_USER_CHAR_NO	",ref pWhere);
					SysPrograms.SqlAppendWhere("F.SITE_CD			= FG.SITE_CD (+)			",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_SEQ			= FG.USER_SEQ (+)			",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_CHAR_NO		= FG.USER_CHAR_NO (+)		",ref pWhere);
					SysPrograms.SqlAppendWhere("F.FAVORIT_GROUP_SEQ	= FG.FAVORIT_GROUP_SEQ (+)	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					if (!string.IsNullOrEmpty(pCondition.favoritGroupSeq) && !pCondition.favoritGroupSeq.Equals("null")) {
						list.Add(new OracleParameter("FAVORIT_GROUP_SEQ",pCondition.favoritGroupSeq));
					}
					break;

				case ViCommConst.INQUIRY_REFUSE:
					SysPrograms.SqlAppendWhere("RF.SITE_CD		= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("RF.USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("RF.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= RF.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= RF.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= RF.PARTNER_USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_LIKE_ME:
					SysPrograms.SqlAppendWhere("F.SITE_CD				= :SITE_CD			",ref pWhere);
					SysPrograms.SqlAppendWhere("F.PARTNER_USER_SEQ		= :USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("F.PARTNER_USER_CHAR_NO	= :USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= F.SITE_CD			",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= F.USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= F.USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					using (ManageCompany oManageCompany = new ManageCompany()) {
						if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_LIKE_ME,2)) {
							SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = F.SITE_CD AND USER_SEQ = F.PARTNER_USER_SEQ AND USER_CHAR_NO = F.PARTNER_USER_CHAR_NO AND PARTNER_USER_SEQ = F.USER_SEQ AND PARTNER_USER_CHAR_NO = F.USER_CHAR_NO)",ref pWhere);
						}
					}
					break;

				case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
					SysPrograms.SqlAppendWhere("F1.SITE_CD				= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("F1.USER_SEQ				= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("F1.USER_CHAR_NO			= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= F1.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= F1.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= F1.PARTNER_USER_CHAR_NO	",ref pWhere);
					SysPrograms.SqlAppendWhere("F2.SITE_CD				= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("F2.PARTNER_USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("F2.PARTNER_USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= F2.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= F2.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= F2.USER_CHAR_NO			",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
					SysPrograms.SqlAppendWhere("LL.SITE_CD					= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_SEQ			= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_CHAR_NO		= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD					= LL.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ					= LL.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO				= LL.USER_CHAR_NO			",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.LAST_VIEW_PROFILE_DATE	> :LAST_VIEW_PROFILE_DATE	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					list.Add(new OracleParameter("LAST_VIEW_PROFILE_DATE",OracleDbType.Date,DateTime.Now.AddDays(int.Parse(sessionObj.site.loveListProfileDays) * -1),ParameterDirection.Input));
					break;

				case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
					SysPrograms.SqlAppendWhere("LL.SITE_CD					= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_SEQ			= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_CHAR_NO		= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD					= LL.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ					= LL.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO				= LL.USER_CHAR_NO			",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.LAST_VIEW_PROFILE_DATE	<= :LAST_VIEW_PROFILE_DATE	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					list.Add(new OracleParameter("LAST_VIEW_PROFILE_DATE",OracleDbType.Date,DateTime.Now.AddDays(int.Parse(sessionObj.site.loveListProfileDays) * -1),ParameterDirection.Input));
					break;

				case ViCommConst.INQUIRY_MARKING:
					SysPrograms.SqlAppendWhere("MK.SITE_CD				= :PARTNER_SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= MK.SITE_CD			",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= MK.USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= MK.USER_CHAR_NO		",ref pWhere);
					list.Add(new OracleParameter("PARTNER_SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("PARTNER_USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pCondition.userCharNo));
					break;

				case PwViCommConst.INQUIRY_SELF_MARKING:
					SysPrograms.SqlAppendWhere("MK.SITE_CD		= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= MK.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= MK.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= MK.PARTNER_USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_CAST_DIARY:
					SysPrograms.SqlAppendWhere("P.SITE_CD		= D.SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= D.USER_SEQ	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= D.USER_CHAR_NO",ref pWhere);
					break;

				case ViCommConst.INQUIRY_FORWARD:
					SysPrograms.SqlAppendWhere("RC.SITE_CD		= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("RC.USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("RC.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("RC.REC_TYPE		= :REC_TYPE					",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= RC.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= RC.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= RC.PARTNER_USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					list.Add(new OracleParameter("REC_TYPE",ViCommConst.REC_TYPE_PROFILE));
					break;

				case ViCommConst.INQUIRY_CAST_WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS = :ONLINE_STATUS_ONLINE2) ",ref pWhere);
					list.Add(new OracleParameter("ONLINE_STATUS_ONLINE2",ViCommConst.USER_WAITING));
					break;

				case ViCommConst.INQUIRY_ROOM_MONITOR:
					SysPrograms.SqlAppendWhere("(P.MONITOR_TALK_TYPE = :MONITOR_TALK_TYPE2) ",ref pWhere);
					list.Add(new OracleParameter("MONITOR_TALK_TYPE2",ViCommConst.MONITOR_ROOM));
					break;

				case ViCommConst.INQUIRY_MEMO:
					SysPrograms.SqlAppendWhere("ME.SITE_CD		= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("ME.USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("ME.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= ME.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= ME.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= ME.PARTNER_USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE:
					SysPrograms.SqlAppendWhere("P.QUICK_RES_FLAG			= :QUICK_RES_FLAG			",ref pWhere);
					SysPrograms.SqlAppendWhere("P.QUICK_RES_IN_TIME			<= :QUICK_RES_IN_TIME		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.QUICK_RES_OUT_SCH_TIME	>= :QUICK_RES_OUT_SCH_TIME	",ref pWhere);
					list.Add(new OracleParameter("QUICK_RES_FLAG",ViCommConst.FLAG_ON));
					list.Add(new OracleParameter("QUICK_RES_IN_TIME",OracleDbType.Date,DateTime.Now,ParameterDirection.Input));
					list.Add(new OracleParameter("QUICK_RES_OUT_SCH_TIME",OracleDbType.Date,DateTime.Now,ParameterDirection.Input));

					System.Text.StringBuilder oSqlCommBuilder = new System.Text.StringBuilder();
					oSqlCommBuilder.AppendLine(" AND NOT EXISTS(	");
					oSqlCommBuilder.AppendLine(" SELECT 1 FROM T_COMMUNICATION                                              ");
					oSqlCommBuilder.AppendLine(" WHERE																		");
					oSqlCommBuilder.AppendLine("	T_COMMUNICATION.SITE_CD					= P.SITE_CD					AND ");
					oSqlCommBuilder.AppendLine("	T_COMMUNICATION.USER_SEQ				= :COMM_USER_SEQ			AND	");
					oSqlCommBuilder.AppendLine("	T_COMMUNICATION.USER_CHAR_NO		 	= :COMM_USER_CHAR_NO		AND ");
					oSqlCommBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_SEQ 		= P.USER_SEQ 				AND ");
					oSqlCommBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_CHAR_NO 	= P.USER_CHAR_NO 			AND ");
					oSqlCommBuilder.AppendLine("	T_COMMUNICATION.TOTAL_TX_MAIL_COUNT 	> 0	 						");
					oSqlCommBuilder.AppendLine(" )                                                                          ");

					list.Add(new OracleParameter("COMM_USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("COMM_USER_CHAR_NO",pCondition.userCharNo));

					pWhere += oSqlCommBuilder.ToString();
					
					break;

				case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
					SysPrograms.SqlAppendWhere("BE.BINGO_TERM_SEQ			= :BINGO_TERM_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("BE.BINGO_APPLICATION_FLAG	= :BINGO_APPLICATION_FLAG	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD					= BE.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ					= BE.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO				= BE.USER_CHAR_NO			",ref pWhere);
					list.Add(new OracleParameter("BINGO_TERM_SEQ",pCondition.bingoTermSeq));
					list.Add(new OracleParameter("BINGO_APPLICATION_FLAG",ViCommConst.FLAG_ON));
					break;

				case ViCommConst.INQUIRY_CONDITION:
					if (!pCondition.handleNm.Equals("")) {
						SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE '%' || :HANDLE_NM || '%' ",ref pWhere);
						list.Add(new OracleParameter("HANDLE_NM",pCondition.handleNm));
					}

					if (!pCondition.ageFrom.Equals("")) {
						SysPrograms.SqlAppendWhere("P.AGE >= :AGE_FROM ",ref pWhere);
						list.Add(new OracleParameter("AGE_FROM",pCondition.ageFrom));
					}
					if (!pCondition.ageTo.Equals("")) {
						SysPrograms.SqlAppendWhere("P.AGE <= :AGE_TO ",ref pWhere);
						list.Add(new OracleParameter("AGE_TO",pCondition.ageTo));
					}

					if (!pCondition.comment.Trim().Equals("")) {
						string[] sComment = pCondition.comment.Trim().Split(' ');
						StringBuilder sWhere = new StringBuilder();
						sWhere.Append("(");
						for (int i = 0;i < sComment.Length;i++) {
							if (sComment[i] != "") {
								if (i != 0) {
									sWhere.Append(" OR ");
								}
								sWhere.Append(string.Format(" (P.COMMENT_LIST LIKE '%' || :COMMENT_LIST{0} || '%' OR P.COMMENT_DETAIL LIKE '%' || :COMMENT_DETAIL{0} || '%' OR P.COMMENT_ADMIN LIKE '%' || :COMMENT_ADMIN{0} || '%') ",i.ToString()));
								list.Add(new OracleParameter(string.Format("COMMENT_LIST{0}",i.ToString()),sComment[i]));
								list.Add(new OracleParameter(string.Format("COMMENT_DETAIL{0}",i.ToString()),sComment[i]));
								list.Add(new OracleParameter(string.Format("COMMENT_ADMIN{0}",i.ToString()),sComment[i]));
							}
						}
						sWhere.Append(")");
						SysPrograms.SqlAppendWhere(sWhere.ToString(),ref pWhere);

					}

					if (pCondition.hasRecvMail != null) {
						System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
						if (pCondition.hasRecvMail.Value) {
							oSqlBuilder.AppendLine(" AND EXISTS(		");
						} else {
							oSqlBuilder.AppendLine(" AND NOT EXISTS(	");
						}
						oSqlBuilder.AppendLine(" SELECT 1 FROM T_COMMUNICATION                                              ");
						oSqlBuilder.AppendLine(" WHERE                                                                      ");
						oSqlBuilder.AppendLine("   T_COMMUNICATION.SITE_CD					= P.SITE_CD 				AND ");
						oSqlBuilder.AppendLine("   T_COMMUNICATION.PARTNER_USER_SEQ 		= P.USER_SEQ 				AND ");
						oSqlBuilder.AppendLine("   T_COMMUNICATION.PARTNER_USER_CHAR_NO 	= P.USER_CHAR_NO 			AND ");
						oSqlBuilder.AppendLine("   T_COMMUNICATION.TOTAL_TX_MAIL_COUNT 		> 0 							");
						oSqlBuilder.AppendLine(" )                                                                          ");

						pWhere += oSqlBuilder.ToString();
					}

					if (pCondition.playMask != 0) {
						SysPrograms.SqlAppendWhere("(BITAND(P.OK_PLAY_MASK,:OK_PLAY_MASK1) = :OK_PLAY_MASK2)",ref pWhere);
						list.Add(new OracleParameter("OK_PLAY_MASK1",pCondition.playMask));
						list.Add(new OracleParameter("OK_PLAY_MASK2",pCondition.playMask));
					}

					if (pCondition.movie.Equals("1")) {
						SysPrograms.SqlAppendWhere("P.PROFILE_MOVIE_SEQ IS NOT NULL ",ref pWhere);
					}

					if (pCondition.waitType.Equals("1")) {
						SysPrograms.SqlAppendWhere("P.CONNECT_TYPE IN(:CONNECT_TYPE1,:CONNECT_TYPE2) ",ref pWhere);
						list.Add(new OracleParameter("CONNECT_TYPE1",ViCommConst.WAIT_TYPE_VIDEO));
						list.Add(new OracleParameter("CONNECT_TYPE2",ViCommConst.WAIT_TYPE_BOTH));
					} else if (pCondition.waitType.Equals("2")) {
						SysPrograms.SqlAppendWhere("P.CONNECT_TYPE IN(:CONNECT_TYPE1,:CONNECT_TYPE2) ",ref pWhere);
						list.Add(new OracleParameter("CONNECT_TYPE1",ViCommConst.WAIT_TYPE_VOICE));
						list.Add(new OracleParameter("CONNECT_TYPE2",ViCommConst.WAIT_TYPE_BOTH));
					}

					CreateAttrQuery(pCondition,ref pWhere,ref list);
					break;
			}
		}

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD AND P.NA_FLAG = :NA_FLAG ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
		list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));

		if (!pCondition.categorySeq.Equals("")) {
			SysPrograms.SqlAppendWhere("P.ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ ",ref pWhere);
			list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pCondition.categorySeq));
		}

		if (!pCondition.userRank.Equals("")) {
			SysPrograms.SqlAppendWhere("USER_RANK = :USER_RANK ",ref pWhere);
			list.Add(new OracleParameter("USER_RANK",pCondition.userRank));
		}

		if (pCondition.hasProfilePic.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("P.PROFILE_PIC_SEQ IS NOT NULL ",ref pWhere);
		}

		if (!string.IsNullOrEmpty(pCondition.lastLoginDate)) {
			SysPrograms.SqlAppendWhere("P.LAST_LOGIN_DATE >= :LAST_LOGIN_DATE ",ref pWhere);
			list.Add(new OracleParameter("LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Parse(pCondition.lastLoginDate),ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pCondition.lastActionDate)) {
			SysPrograms.SqlAppendWhere("P.LAST_ACTION_DATE >= :LAST_ACTION_DATE",ref pWhere);
			list.Add(new OracleParameter("LAST_ACTION_DATE",OracleDbType.Date,DateTime.Parse(pCondition.lastActionDate),ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pCondition.lastTxMailDays)) {
			SysPrograms.SqlAppendWhere("P.LAST_TX_MAIL_DATE >= SYSDATE - :LAST_TX_MAIL_DAYS",ref pWhere);
			list.Add(new OracleParameter("LAST_TX_MAIL_DAYS",pCondition.lastTxMailDays));
		}

		// 拡張検索(一覧下部の検索フォームからの検索)の条件追加 
		if (pCondition.hasExtend) {
			if (!string.IsNullOrEmpty(pCondition.handleNm)) {
				SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE '%' || :EXT_HANDLE_NM || '%' ",ref pWhere);
				list.Add(new OracleParameter("EXT_HANDLE_NM",pCondition.handleNm));
			}
			if (!string.IsNullOrEmpty(pCondition.ageFrom)) {
				SysPrograms.SqlAppendWhere("P.AGE >= :EXT_AGE_FROM ",ref pWhere);
				list.Add(new OracleParameter("EXT_AGE_FROM",pCondition.ageFrom));
			}
			if (!string.IsNullOrEmpty(pCondition.ageTo)) {
				SysPrograms.SqlAppendWhere("P.AGE <= :EXT_AGE_TO ",ref pWhere);
				list.Add(new OracleParameter("EXT_AGE_TO",pCondition.ageTo));
			}

			if (!string.IsNullOrEmpty(pCondition.waitType)) {
				if (pCondition.waitType.Equals("1")) {
					SysPrograms.SqlAppendWhere("P.CONNECT_TYPE IN(:EXT_CONNECT_TYPE1,:EXT_CONNECT_TYPE2) ",ref pWhere);
					list.Add(new OracleParameter("EXT_CONNECT_TYPE1",ViCommConst.WAIT_TYPE_VIDEO));
					list.Add(new OracleParameter("EXT_CONNECT_TYPE2",ViCommConst.WAIT_TYPE_BOTH));
				} else if (pCondition.waitType.Equals("2")) {
					SysPrograms.SqlAppendWhere("P.CONNECT_TYPE IN(:CONNECT_TYPE1,:CONNECT_TYPE2) ",ref pWhere);
					list.Add(new OracleParameter("EXT_CONNECT_TYPE1",ViCommConst.WAIT_TYPE_VOICE));
					list.Add(new OracleParameter("EXT_CONNECT_TYPE2",ViCommConst.WAIT_TYPE_BOTH));
				}
			}

			if (!string.IsNullOrEmpty(pCondition.talkingType)) {
				if (pCondition.talkingType.Equals("1")) {
					SysPrograms.SqlAppendWhere("P.CHARACTER_ONLINE_STATUS = :ETX_ONLINE_STATUS_TALKING AND P.MONITOR_TALK_TYPE = :EXT_MONITOR_TALK_TYPE ",ref pWhere);
					list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					list.Add(new OracleParameter("MONITOR_TALK_TYPE",ViCommConst.FLAG_OFF));
				} else if (pCondition.talkingType.Equals("2")) {
					SysPrograms.SqlAppendWhere("P.CHARACTER_ONLINE_STATUS = :ETX_ONLINE_STATUS_TALKING AND P.MONITOR_TALK_TYPE > :EXT_MONITOR_TALK_TYPE ",ref pWhere);
					list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					list.Add(new OracleParameter("MONITOR_TALK_TYPE",ViCommConst.FLAG_OFF));
				}
			}

			if (pCondition.monitorEnableFlag == 1) {
				SysPrograms.SqlAppendWhere("P.MONITOR_ENABLE_FLAG = :MONITOR_ENABLE_FLAG ",ref pWhere);
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS != :EXT_ONLINE_STATUS_TALKING1 OR (P.CHARACTER_ONLINE_STATUS = :EXT_ONLINE_STATUS_TALKING2 AND P.MONITOR_TALK_TYPE > :MONITOR_TALK_TYPE ) )",ref pWhere);
				list.Add(new OracleParameter("MONITOR_ENABLE_FLAG",ViCommConst.FLAG_ON));
				list.Add(new OracleParameter("EXT_ONLINE_STATUS_TALKING1",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("EXT_ONLINE_STATUS_TALKING2",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("MONITOR_TALK_TYPE",ViCommConst.FLAG_OFF));
			}

			if (pCondition.attrValue.Count > 0) {
				this.CreateAttrQuery(pCondition,ref pWhere,ref list);
			}
		}

		if (!pCondition.userSeq.Equals(string.Empty) && pCondition.conditionFlag != ViCommConst.INQUIRY_REFUSE) {
			SysPrograms.SqlAppendWhere("(NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE P.SITE_CD = T_REFUSE.SITE_CD AND P.USER_SEQ = T_REFUSE.USER_SEQ AND P.USER_CHAR_NO = T_REFUSE.USER_CHAR_NO AND :PARTNER_USER_SEQ = T_REFUSE.PARTNER_USER_SEQ AND :PARTNER_USER_CHAR_NO = T_REFUSE.PARTNER_USER_CHAR_NO))",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pCondition.userSeq));
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pCondition.userCharNo));
		}

		if (pCondition.userAdminFlag.Equals(ViCommConst.FLAG_ON_STR) || pCondition.userAdminFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
			SysPrograms.SqlAppendWhere("P.ADMIN_FLAG = :ADMIN_FLAG",ref pWhere);
			list.Add(new OracleParameter("ADMIN_FLAG",pCondition.userAdminFlag));
		}

		pWhere += base.SetFlexibleQuery(pFlexibleQueryTarget,pCondition.siteCd,pCondition.userSeq,pCondition.userCharNo,ref list);

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private FlexibleQueryTargetInfo CreateFlexibleQueryTarget(FlexibleQueryType pFlexibleQueryType,SeekCondition pCondition) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(pFlexibleQueryType);

		if (!pCondition.userSeq.Equals(string.Empty) && pCondition.conditionFlag != ViCommConst.INQUIRY_REFUSE) {
			oFlexibleQueryTarget.JealousyLastActionDate = pCondition.lastLoginDate;
			switch (pCondition.onlineStatus) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					oFlexibleQueryTarget.JealousyWaitingStatus = true;
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
					oFlexibleQueryTarget.JealousyLoginStatus = true;
					break;
				default:
					switch (pCondition.conditionFlag) {
						case ViCommConst.INQUIRY_CAST_WAITING:
							oFlexibleQueryTarget.JealousyLoginStatus = true;
							break;
						case ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING:
							oFlexibleQueryTarget.JealousyRanking = true;
							break;
						case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2:
						case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2:
							oFlexibleQueryTarget.JealousyDiary = true;
							oFlexibleQueryTarget.JealousyRanking = true;
							break;
						case ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE:
							oFlexibleQueryTarget.JealousySetOfflineIsVisible = true;
							break;
					}
					break;
			}
		}
		return oFlexibleQueryTarget;
	}

	public int GetRandomCast(int pNeedCount,string pSiteCd,string pManUserSeq,string pManCharNo,string pActCtegorySeq,bool pIsSmallImage,out string[] pCastSeq,out string[] pCryptValue,out string[] pLoginId,out string[] pHandleNm,out string[] pImagePath,out string[] pRecNum) {
		int iRecCnt = 0;
		string sPrimaryActSeq = pActCtegorySeq;
		if (sPrimaryActSeq.Equals("")) {
			using (ActCategory oActCategory = new ActCategory()) {
				sPrimaryActSeq = oActCategory.GetPrimaryCategorySeq(pSiteCd);
			}
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_RANDOM_CAST");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pManCharNo);
			db.ProcedureInParm("PNEED_COUNT",DbSession.DbType.NUMBER,pNeedCount);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,sPrimaryActSeq);
			db.ProcedureOutArrayParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PCRYPT_VALUE",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PSMALL_PHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PPHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PRUM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iRecCnt = db.GetIntValue("PRECORD_COUNT");
			pCastSeq = new string[iRecCnt];
			pCryptValue = new string[iRecCnt];
			pLoginId = new string[iRecCnt];
			pImagePath = new string[iRecCnt];
			pRecNum = new string[iRecCnt];
			pHandleNm = new string[iRecCnt];
			for (int i = 0;i < iRecCnt;i++) {
				pCastSeq[i] = db.GetArryStringValue("PUSER_SEQ",i);
				pCryptValue[i] = db.GetArryStringValue("PCRYPT_VALUE",i);
				pLoginId[i] = db.GetArryStringValue("PLOGIN_ID",i);
				if (pIsSmallImage) {
					pImagePath[i] = db.GetArryStringValue("PSMALL_PHOTO_IMG_PATH",i);
				} else {
					pImagePath[i] = db.GetArryStringValue("PPHOTO_IMG_PATH",i);
				}
				pRecNum[i] = db.GetArryStringValue("PRUM",i);
				pHandleNm[i] = db.GetArryStringValue("PHANDLE_NM",i);
			}
		}
		return iRecCnt;
	}

	/// <summary>
	/// ﾋﾟｯｸｱｯﾌﾟ(お勧め)ｷｬｽﾄの一覧を取得する。 
	/// </summary>
	/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
	/// <param name="pPickupId">ﾋﾟｯｸｱｯﾌﾟID</param>
	/// <param name="pNeedCount">必要な件数</param>
	/// <returns>ﾋﾟｯｸｱｯﾌﾟ(お勧め)ｷｬｽﾄの一覧</returns>
	public DataSet GetRecommendDataSet(string pSiteCd,string pPickupId,int pNeedCount) {
		string[] oUserSeqArray = null;
		string[] oCryptValueArray = null;

		this.GetRecommend(pSiteCd,pPickupId,pNeedCount,out oUserSeqArray,out oCryptValueArray);

		DataSet oDataSet = new DataSet();
		if (oUserSeqArray.Length == 0) {
			oDataSet.Tables.Add(new DataTable());
		} else {
			using (Cast oCast = new Cast(this.sessionObj)) {
				for (int i = 0;i < oUserSeqArray.Length;i++) {
					string sUserSeq = oUserSeqArray[i];
					string sUserCharNo = ViCommConst.MAIN_CHAR_NO;

					oDataSet.Merge(oCast.GetOne(pSiteCd,sUserSeq,sUserCharNo,i + 1));
				}
			}
		}
		return oDataSet;
	}

	/// <summary>
	/// ﾋﾟｯｸｱｯﾌﾟ(お勧め)ｷｬｽﾄのキーリストを取得する。 
	/// </summary>
	/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
	/// <param name="pPickupId">ﾋﾟｯｸｱｯﾌﾟID</param>
	/// <param name="pNeedCount">必要な件数</param>
	/// <param name="pUserSeq">ユーザーSEQ配列</param>
	/// <param name="pCryptValue">難読化されたｷｬﾗｸﾀｰ№の配列</param>
	/// <returns>該当件数</returns>
	private int GetRecommend(string pSiteCd,string pPickupId,int pNeedCount,out string[] pUserSeq,out string[] pCryptValue) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GET_RECOMMEND");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pPICKUP_ID",DbSession.DbType.VARCHAR2,pPickupId);
			oDbSession.ProcedureInParm("pNEED_COUNT",DbSession.DbType.NUMBER,pNeedCount);
			oDbSession.ProcedureOutArrayParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pNeedCount);
			oDbSession.ProcedureOutArrayParm("pCRYPT_VALUE",DbSession.DbType.VARCHAR2,pNeedCount);
			oDbSession.ProcedureOutArrayParm("pRNUM",DbSession.DbType.VARCHAR2,pNeedCount);
			oDbSession.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);

			oDbSession.ExecuteProcedure();

			int iRecordCount = oDbSession.GetIntValue("PRECORD_COUNT");
			List<string> pUserSeqList = new List<string>();
			List<string> pCryptValueList = new List<string>();

			for (int i = 0;i < iRecordCount;i++) {
				pUserSeqList.Add(oDbSession.GetArryStringValue("pUSER_SEQ",i));
				pCryptValueList.Add(oDbSession.GetArryStringValue("pCRYPT_VALUE",i));
			}

			pUserSeq = pUserSeqList.ToArray();
			pCryptValue = pCryptValueList.ToArray();

			return iRecordCount;
		}
	}

	public int GetRandomMovie(string pSiteCd,string pManUserSeq,string pManCharNo,string pPickupId,bool pIsSmallImage,out string[] pCastSeq,out string[] pCryptValue,out string[] pLoginId,out string[] pHandleNm,out string[] pImagePath,out string[] pActCategoryIdx,out string[] pObjSeq,out string[] pRecNum) {
		int iRecCnt = 0;

		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("GET_RANDOM_PICKUP_MOVIE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pManCharNo);
			db.ProcedureInParm("PNEED_COUNT",DbSession.DbType.NUMBER,3);
			db.ProcedureInParm("PPICKUP_ID",DbSession.DbType.VARCHAR2,pPickupId);
			db.ProcedureOutArrayParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PCRYPT_VALUE",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PSMALL_PHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PPHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PRUM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PACT_CATEGORY_IDX",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("POBJ_SEQ",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();



			iRecCnt = db.GetIntValue("PRECORD_COUNT");
			pCastSeq = new string[iRecCnt];
			pCryptValue = new string[iRecCnt];
			pLoginId = new string[iRecCnt];
			pImagePath = new string[iRecCnt];
			pRecNum = new string[iRecCnt];
			pHandleNm = new string[iRecCnt];
			pActCategoryIdx = new string[iRecCnt];
			pObjSeq = new string[iRecCnt];
			for (int i = 0;i < iRecCnt;i++) {
				pCastSeq[i] = db.GetArryStringValue("PUSER_SEQ",i);
				pCryptValue[i] = db.GetArryStringValue("PCRYPT_VALUE",i);
				pLoginId[i] = db.GetArryStringValue("PLOGIN_ID",i);
				if (pIsSmallImage) {
					pImagePath[i] = db.GetArryStringValue("PSMALL_PHOTO_IMG_PATH",i);
				} else {
					pImagePath[i] = db.GetArryStringValue("PPHOTO_IMG_PATH",i);
				}
				pRecNum[i] = db.GetArryStringValue("PRUM",i);
				pHandleNm[i] = db.GetArryStringValue("PHANDLE_NM",i);
				pActCategoryIdx[i] = db.GetArryStringValue("PACT_CATEGORY_IDX",i);
				pObjSeq[i] = db.GetArryStringValue("POBJ_SEQ",i);
			}
		}
		return iRecCnt;
	}


	public int GetRandomPic(string pSiteCd,string pManUserSeq,string pManCharNo,string pPickupId,bool pIsSmallImage,out string[] pCastSeq,out string[] pCryptValue,out string[] pLoginId,out string[] pHandleNm,out string[] pImagePath,out string[] pActCategoryIdx,out string[] pObjSeq,out string[] pRecNum) {
		int iRecCnt = 0;

		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("GET_RANDOM_PICKUP_PIC");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pManCharNo);
			db.ProcedureInParm("PNEED_COUNT",DbSession.DbType.NUMBER,3);
			db.ProcedureInParm("PPICKUP_ID",DbSession.DbType.VARCHAR2,pPickupId);
			db.ProcedureOutArrayParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PCRYPT_VALUE",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PSMALL_PHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PPHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PRUM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PACT_CATEGORY_IDX",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("POBJ_SEQ",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();



			iRecCnt = db.GetIntValue("PRECORD_COUNT");
			pCastSeq = new string[iRecCnt];
			pCryptValue = new string[iRecCnt];
			pLoginId = new string[iRecCnt];
			pImagePath = new string[iRecCnt];
			pRecNum = new string[iRecCnt];
			pHandleNm = new string[iRecCnt];
			pActCategoryIdx = new string[iRecCnt];
			pObjSeq = new string[iRecCnt];
			for (int i = 0;i < iRecCnt;i++) {
				pCastSeq[i] = db.GetArryStringValue("PUSER_SEQ",i);
				pCryptValue[i] = db.GetArryStringValue("PCRYPT_VALUE",i);
				pLoginId[i] = db.GetArryStringValue("PLOGIN_ID",i);
				if (pIsSmallImage) {
					pImagePath[i] = db.GetArryStringValue("PSMALL_PHOTO_IMG_PATH",i);
				} else {
					pImagePath[i] = db.GetArryStringValue("PPHOTO_IMG_PATH",i);
				}
				pRecNum[i] = db.GetArryStringValue("PRUM",i);
				pHandleNm[i] = db.GetArryStringValue("PHANDLE_NM",i);
				pActCategoryIdx[i] = db.GetArryStringValue("PACT_CATEGORY_IDX",i);
				pObjSeq[i] = db.GetArryStringValue("POBJ_SEQ",i);
			}
		}
		return iRecCnt;
	}


	public int GetPickup(string pSiteCd,string pManUserSeq,string pManCharNo,string pActCtegorySeq,bool pIsSmallImage,out string[] pCastSeq,out string[] pCryptValue,out string[] pLoginId,out string[] pHandleNm,out string[] pImagePath,out string[] pRecNum) {
		int iRecCnt = 0;
		string sPrimaryActSeq = pActCtegorySeq;
		if (sPrimaryActSeq.Equals("")) {
			using (ActCategory oActCategory = new ActCategory()) {
				sPrimaryActSeq = oActCategory.GetPrimaryCategorySeq(pSiteCd);
			}
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_PICKUP");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pManCharNo);
			db.ProcedureInParm("PNEED_COUNT",DbSession.DbType.NUMBER,3);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,sPrimaryActSeq);
			db.ProcedureOutArrayParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PCRYPT_VALUE",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PSMALL_PHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PPHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PRUM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iRecCnt = db.GetIntValue("PRECORD_COUNT");
			pCastSeq = new string[iRecCnt];
			pCryptValue = new string[iRecCnt];
			pLoginId = new string[iRecCnt];
			pImagePath = new string[iRecCnt];
			pRecNum = new string[iRecCnt];
			pHandleNm = new string[iRecCnt];
			for (int i = 0;i < iRecCnt;i++) {
				pCastSeq[i] = db.GetArryStringValue("PUSER_SEQ",i);
				pCryptValue[i] = db.GetArryStringValue("PCRYPT_VALUE",i);
				pLoginId[i] = db.GetArryStringValue("PLOGIN_ID",i);
				if (pIsSmallImage) {
					pImagePath[i] = db.GetArryStringValue("PSMALL_PHOTO_IMG_PATH",i);
				} else {
					pImagePath[i] = db.GetArryStringValue("PPHOTO_IMG_PATH",i);
				}
				pRecNum[i] = db.GetArryStringValue("PRUM",i);
				pHandleNm[i] = db.GetArryStringValue("PHANDLE_NM",i);
			}
		}
		return iRecCnt;
	}

	public int GetRandomRecommend(string pSiteCd,string pManUserSeq,string pManCharNo,string pPickupId,bool pIsSmallImage,out string[] pCastSeq,out string[] pCryptValue,out string[] pLoginId,out string[] pHandleNm,out string[] pImagePath,out string[] pActCategoryIdx,out string[] pRecNum) {
		int iRecCnt = 0;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_RANDOM_RECOMMEND");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pManCharNo);
			db.ProcedureInParm("PNEED_COUNT",DbSession.DbType.NUMBER,3);
			db.ProcedureInParm("PPICKUP_ID",DbSession.DbType.VARCHAR2,pPickupId);
			db.ProcedureOutArrayParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PCRYPT_VALUE",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PSMALL_PHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PPHOTO_IMG_PATH",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PRUM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutArrayParm("PACT_CATEGORY_IDX",DbSession.DbType.VARCHAR2,10);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iRecCnt = db.GetIntValue("PRECORD_COUNT");
			pCastSeq = new string[iRecCnt];
			pCryptValue = new string[iRecCnt];
			pLoginId = new string[iRecCnt];
			pImagePath = new string[iRecCnt];
			pRecNum = new string[iRecCnt];
			pHandleNm = new string[iRecCnt];
			pActCategoryIdx = new string[iRecCnt];
			for (int i = 0;i < iRecCnt;i++) {
				pCastSeq[i] = db.GetArryStringValue("PUSER_SEQ",i);
				pCryptValue[i] = db.GetArryStringValue("PCRYPT_VALUE",i);
				pLoginId[i] = db.GetArryStringValue("PLOGIN_ID",i);
				if (pIsSmallImage) {
					pImagePath[i] = db.GetArryStringValue("PSMALL_PHOTO_IMG_PATH",i);
				} else {
					pImagePath[i] = db.GetArryStringValue("PPHOTO_IMG_PATH",i);
				}
				pRecNum[i] = db.GetArryStringValue("PRUM",i);
				pHandleNm[i] = db.GetArryStringValue("PHANDLE_NM",i);
				pActCategoryIdx[i] = db.GetArryStringValue("PACT_CATEGORY_IDX",i);
			}
		}
		return iRecCnt;
	}

	public string GetOkPlayList(string pSiteCd,Int64 pOkPlayMask) {
		string sList = "";
		string sSql = "SELECT OK_PLAY,OK_PLAY_NM FROM T_OK_PLAY_LIST " +
					"WHERE " +
						"SITE_CD	= :SITE_CD " +
					"ORDER BY SITE_CD,OK_PLAY";

		using (DataSet ds = new DataSet()) {
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_OK_PLAY_LIST");
				}
			}
			Int64 iMask;
			foreach (DataRow dr in ds.Tables[0].Rows) {
				iMask = Int64.Parse(dr["OK_PLAY"].ToString());
				if ((iMask & pOkPlayMask) != 0) {
					sList = sList + dr["OK_PLAY_NM"].ToString() + ",";
				}
			}
		}
		if (sList.Length != 0) {
			sList = sList.Substring(0,sList.Length - 1);
		}
		return sList;
	}

	public int GetFavoritCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		string sFavoritSql = "SELECT NVL(COUNT(*),0) AS CNT FROM T_FAVORIT " +
				   "WHERE " +
						"SITE_CD				= :SITE_CD			AND " +
						"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ	AND " +
						"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ";
		try {
			conn = DbConnect("Cast.GetFavoritCount");

			using (DataSet ds = new DataSet())
			using (cmd = CreateSelectCommand(sFavoritSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pUserCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_FAVORIT");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow dr = ds.Tables[0].Rows[0];
						iCount = int.Parse(dr["CNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iCount;
	}

	public DataSet GetLoginCharacterList(string pSiteCd,string pUserSeq) {
		DataSet ds;
		try {
			conn = DbConnect("Cast.GetLoginCharacterList");
			ds = new DataSet();

			string sSql = "SELECT " +
								"SITE_CD			," +
								"USER_SEQ			," +
								"USER_CHAR_NO		," +
								"CONNECT_TYPE		," +
								"HANDLE_NM			," +
								"HANDLE_KANA_NM		," +
								"COMMENT_LIST		," +
								"COMMENT_DETAIL		," +
								"NA_FLAG			," +
								"CHARACTER_ONLINE_STATUS " +
							"FROM " +
								"VW_CAST_CHARACTER03 " +
							"WHERE " +
								"SITE_CD	= :SITE_CD	AND " +
								"USER_SEQ	= :USER_SEQ AND " +
								"NA_FLAG	IN (:NA_FLAG1,:NA_FLAG2,:NA_FLAG3) " +
							"ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("NA_FLAG1",ViCommConst.NaFlag.OK);
				cmd.Parameters.Add("NA_FLAG2",ViCommConst.NaFlag.NO_AUTH);
				cmd.Parameters.Add("NA_FLAG3",ViCommConst.NaFlag.IMPORT);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARACTER03");
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public bool EixstTermId(string pTerminalUniqueId,string piModeId,string pUserSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("Cast.EixstTermId");

			string sSql = "SELECT " +
								"USER_SEQ			," +
								"USER_STATUS		 " +
							"FROM " +
							" T_USER ";

			string sWhere = "";


			ArrayList list = new ArrayList();

			string sWhereId = string.Empty;
			if (!pTerminalUniqueId.Equals("")) {
				sWhereId = " ( TERMINAL_UNIQUE_ID = :TERMINAL_UNIQUE_ID	";
				list.Add(new OracleParameter("TERMINAL_UNIQUE_ID",pTerminalUniqueId));
			}

			if (!piModeId.Equals("")) {
				if (!sWhereId.Equals(string.Empty)) {
					sWhereId += " OR ";
				} else {
					sWhereId = " ( ";
				}
				sWhereId += "	IMODE_ID = :IMODE_ID	";
				list.Add(new OracleParameter("IMODE_ID",piModeId));
			}
			if (!sWhereId.Equals(string.Empty)) {
				sWhereId += " ) ";
				sWhere = " WHERE	" + sWhereId;
			}

			if (!sWhere.Equals(string.Empty)) {
				sWhere += "	AND		SEX_CD	= :SEX_CD	";
			} else {
				sWhere += "	WHERE	SEX_CD	= :SEX_CD	";
			}
			list.Add(new OracleParameter("SEX_CD",ViCommConst.OPERATOR));

			OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));

			sSql += sWhere;
			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count == 0) {
						bExist = true;
					} else {
						bool bTmp = true;
						for (int j = 0;j < ds.Tables["T_USER"].Rows.Count;j++) {
							dr = ds.Tables["T_USER"].Rows[j];
							if (!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_HOLD)) {
								if (!dr["USER_SEQ"].ToString().Equals(pUserSeq)) {
									bTmp = false;
								}
							}
						}
						bExist = bTmp;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public string GetUserSeqByLoginId(string pLoginId) {
		DataSet ds;
		DataRow dr;
		string sUserSeq = "";
		try {
			conn = DbConnect("Cast.GetUserSeqByLoginId");

			string sSql = "SELECT " +
								"USER_SEQ " +
							"FROM " +
							"	T_USER " +
							"WHERE " +
							"	LOGIN_ID = :LOGIN_ID ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("LOGIN_ID",pLoginId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count > 0) {
						dr = ds.Tables["T_USER"].Rows[0];
						sUserSeq = dr["USER_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sUserSeq;
	}

	public bool IsHaveMultiCharacter(string pSiteCd,string pUserSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("Cast.IsHaveMultiCharacter");
			string sSql = "SELECT " +
							"	COUNT(*) AS CNT " +
							"FROM " +
							"	T_CAST_CHARACTER " +
							"WHERE " +
							"	SITE_CD			= :SITE_CD		AND " +
							"	USER_SEQ		= :USER_SEQ		AND " +
							"	CONNECT_TYPE	= :CONNECT_TYPE	AND	" +
							"	NA_FLAG			= :NA_FLAG			";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF.ToString());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_CHARACTER");
					if (ds.Tables["T_CAST_CHARACTER"].Rows.Count > 0) {
						dr = ds.Tables["T_CAST_CHARACTER"].Rows[0];
						if (int.Parse(dr["CNT"].ToString()) > 1) {
							bExist = true;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	private void CreateAttrQuery(SeekCondition pCondition,ref string pWhere,ref ArrayList pList) {
		string sQuery = string.Empty;
		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.SITE_CD			= P.SITE_CD					",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_SEQ			= P.USER_SEQ				",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO			",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0}	",i + 1),ref pWhere);
				pList.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						sQuery = " ( ";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									sQuery += " OR ";
								}
								sQuery += string.Format(" (AT{0}.CAST_ATTR_INPUT_VALUE	LIKE '%' || :CAST_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						sQuery += ")";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');

					if (sValues.Length > 1) {
						if (pCondition.rangeFlag[i].Equals("1")) {
							if (!string.IsNullOrEmpty(sValues[0])) {
								SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_INPUT_VALUE >= :CAST_ATTR_INPUT_VALUE_FROM{0} ",i + 1),ref pWhere);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE_FROM{0}",i + 1),sValues[0]));
							}
							if (!string.IsNullOrEmpty(sValues[1])) {
								SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_INPUT_VALUE <= :CAST_ATTR_INPUT_VALUE_TO{0} ",i + 1),ref pWhere);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE_TO{0}",i + 1),sValues[1]));
							}
						} else if (pCondition.seqRangeFlag[i].Equals("1")) {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ IS NOT NULL ",i + 1),ref pWhere);

							if (!string.IsNullOrEmpty(sValues[0])) {
								SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ >= :CAST_ATTR_SEQ_FROM{0} ",i + 1),ref pWhere);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ_FROM{0}",i + 1),sValues[0]));
							}
							if (!string.IsNullOrEmpty(sValues[1])) {
								SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ <= :CAST_ATTR_SEQ_TO{0} ",i + 1),ref pWhere);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ_TO{0}",i + 1),sValues[1]));
							}
						} else {
							sQuery = string.Format(" AT{0}.CAST_ATTR_SEQ IN ( ",i + 1);
							for (int j = 0;j < sValues.Length;j++) {
								if (j != 0) {
									sQuery += " , ";
								}
								sQuery += string.Format(" :CAST_ATTR_SEQ{0}_{1}	",i + 1,j);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
							}
							sQuery += " ) ";
							SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
						}
					} else {
						if (pCondition.notEqualFlag.Count > i && pCondition.notEqualFlag[i].Equals("1")) {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ <> :CAST_ATTR_SEQ{0} ",i + 1),ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ = :CAST_ATTR_SEQ{0} ",i + 1),ref pWhere);
						}
						pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					SysPrograms.SqlAppendWhere(string.Format(" AT{0}.GROUPING_CD	= :GROUPING_CD{0}	",i + 1),ref pWhere);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}
	}

	public string GetUserSeq(string pSiteCd,string pLoginId,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine(" 	USER_SEQ                    ");
		oSqlBuilder.AppendLine(" FROM                           ");
		oSqlBuilder.AppendLine(" 	VW_CAST_CHARACTER00		   ");
		oSqlBuilder.AppendLine(" WHERE                          ");
		oSqlBuilder.AppendLine(" 	SITE_CD		 = :SITE_CD			AND ");
		oSqlBuilder.AppendLine(" 	LOGIN_ID	 = :LOGIN_ID		AND ");
		oSqlBuilder.AppendLine(" 	USER_CHAR_NO = :USER_CHAR_NO		");

		string sUserSeq = string.Empty;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":LOGIN_ID",pLoginId);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				sUserSeq = iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return sUserSeq;
	}


	public void GetCastData(string pSiteCd,string pUserSeq,string pUserCharNo,out string pHandelNm,out string pAge,out string pProfilePicSeq,out string pCommentList,out string pCommentDetail,out string pWaitingComment) {
		DataSet ds;
		DataRow dr;

		pHandelNm = string.Empty;
		pAge = string.Empty;
		pProfilePicSeq = string.Empty;
		pCommentList = string.Empty;
		pCommentDetail = string.Empty;
		pWaitingComment = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT			");
		oSqlBuilder.AppendLine(" 	HANDLE_NM		,	");
		oSqlBuilder.AppendLine(" 	AGE				,	");
		oSqlBuilder.AppendLine("	PROFILE_PIC_SEQ	,	");
		oSqlBuilder.AppendLine("	COMMENT_LIST	,	");
		oSqlBuilder.AppendLine("	COMMENT_DETAIL	,	");
		oSqlBuilder.AppendLine("	WAITING_COMMENT		");
		oSqlBuilder.AppendLine(" FROM                           ");
		oSqlBuilder.AppendLine(" 	VW_CAST_CHARACTER00     ");
		oSqlBuilder.AppendLine(" WHERE                          ");
		oSqlBuilder.AppendLine(" 	SITE_CD		 = :SITE_CD			AND ");
		oSqlBuilder.AppendLine(" 	USER_SEQ	 = :USER_SEQ		AND ");
		oSqlBuilder.AppendLine(" 	USER_CHAR_NO = :USER_CHAR_NO		");

		string sUserSeq = string.Empty;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count > 0) {
					dr = ds.Tables[0].Rows[0];
					pHandelNm = iBridUtil.GetStringValue(dr["HANDLE_NM"]);
					pAge = iBridUtil.GetStringValue(dr["AGE"]);
					pProfilePicSeq = iBridUtil.GetStringValue(dr["PROFILE_PIC_SEQ"]);
					pCommentList = iBridUtil.GetStringValue(dr["COMMENT_LIST"]);
					pCommentDetail = iBridUtil.GetStringValue(dr["COMMENT_DETAIL"]);
					pWaitingComment = iBridUtil.GetStringValue(dr["WAITING_COMMENT"]);
				}
			}
		} finally {
			conn.Close();
		}
	}
	
	public string GetBirthDayByUserSeq(string pUserSeq) {
		string sValue = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	BIRTHDAY					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	T_CAST						");
		oSqlBuilder.AppendLine("WHERE							");
		oSqlBuilder.AppendLine("	USER_SEQ	= :USER_SEQ		");
		
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["BIRTHDAY"].ToString();
		}
		
		return sValue;
	}

	public DataSet GetIdolAttrSeq(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	IDOL_SEX_ATTR_SEQ,");
		oSqlBuilder.AppendLine("	IDOL_WISH_CAST_TYPE_ATTR_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER_EX");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public void UpdateReadLiked(string pSiteCd,string pUserSeq,string pUserCharNo) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("UPDATE_READ_LIKED");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}
	
	public bool IsQuickResponseEntry(string pSiteCd,string pSelfUserSeq,string pSelfUserCharNo,string pUserSeq,string pUserCharNo) {
		bool bOK = false;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	1																		");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER P														");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	P.SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ					= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO				= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("	P.QUICK_RES_FLAG			= :QUICK_RES_FLAG						AND	");
		oSqlBuilder.AppendLine("	P.QUICK_RES_IN_TIME			<= SYSDATE								AND	");
		oSqlBuilder.AppendLine("	P.QUICK_RES_OUT_SCH_TIME	>= SYSDATE								AND	");
		oSqlBuilder.AppendLine("	P.CHARACTER_ONLINE_STATUS	IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)	AND");
		oSqlBuilder.AppendLine("	P.LAST_ACTION_DATE			>= :LAST_ACTION_DATE					AND	");
		oSqlBuilder.AppendLine("	P.PRIORITY					> :PRIORITY								AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			1																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_COMMUNICATION C												");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			C.SITE_CD				= P.SITE_CD							AND	");
		oSqlBuilder.AppendLine("			C.USER_SEQ				= :SELF_USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			C.USER_CHAR_NO			= :SELF_USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			C.PARTNER_USER_SEQ		= P.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			C.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			C.TOTAL_TX_MAIL_COUNT	> 0										");
		oSqlBuilder.AppendLine("	)																	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			1																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_FAVORIT F														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			F.SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			F.USER_SEQ					= P.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			F.USER_CHAR_NO				= P.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			F.PARTNER_USER_SEQ			= :SELF_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			F.PARTNER_USER_CHAR_NO		= :SELF_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			F.LOGIN_VIEW_STATUS			= :LOGIN_VIEW_STATUS			AND	");
		oSqlBuilder.AppendLine("			F.OFFLINE_LAST_UPDATE_DATE	<= SYSDATE							");
		oSqlBuilder.AppendLine("	)																		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":QUICK_RES_FLAG",ViCommConst.FLAG_ON));
		oParamList.Add(new OracleParameter("LAST_ACTION_DATE",OracleDbType.Date,DateTime.Now.AddDays(-1),ParameterDirection.Input));
		oParamList.Add(new OracleParameter("PRIORITY",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
		oParamList.Add(new OracleParameter(":ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
		oParamList.Add(new OracleParameter(":ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
		
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pSelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pSelfUserCharNo));
		
		oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS",ViCommConst.FLAG_ON_STR));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			bOK = true;
		}

		return bOK;
	}

	public bool IsSelfQuickResponseEntry(string pSiteCd,string pUserSeq,string pUserCharNo) {
		bool bOK = false;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	1																		");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER P														");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	P.SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ					= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO				= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("	P.QUICK_RES_FLAG			= :QUICK_RES_FLAG						AND	");
		oSqlBuilder.AppendLine("	P.QUICK_RES_IN_TIME			<= SYSDATE								AND	");
		oSqlBuilder.AppendLine("	P.QUICK_RES_OUT_SCH_TIME	>= SYSDATE									");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":QUICK_RES_FLAG",ViCommConst.FLAG_ON));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			bOK = true;
		}

		return bOK;
	}

	/// <summary>
	/// 出演者の未清算ポイント、金額を取得する
	/// </summary>
	/// <param name="pUserSeq"></param>
	/// <returns></returns>
	public DataSet GetNotPayment(string pUserSeq) {
		// 入力値が存在しない場合
		if (string.IsNullOrEmpty(pUserSeq)) {
			return null;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	GET_NOT_PAYMENT_POINT(USER_SEQ) AS NOT_PAYMENT_POINT");
		oSqlBuilder.AppendLine("	,GET_NOT_PAYMENT_AMT(USER_SEQ) AS NOT_PAYMENT_AMT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ");

		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
}
