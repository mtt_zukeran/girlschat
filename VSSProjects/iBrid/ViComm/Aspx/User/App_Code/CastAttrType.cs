﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者属性
--	Progaram ID		: CastAttrType
--
--  Creation Date	: 2009.07.16
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;

[System.Serializable]
public class CastAttrType:DbSession {

	public string attrTypeNm;
	public string groupingCategoryCd;
	
	public CastAttrType() {
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect("CastAttrType.GetList");

			string sSql = "SELECT " +
								"CAST_ATTR_TYPE_SEQ		, " +
								"CAST_ATTR_TYPE_NM		, " +
								"CAST_ATTR_TYPE_FIND_NM	, " +
								"INPUT_TYPE				, " +
								"PRIORITY				, " +
								"ITEM_NO				, " +
								"ROW_COUNT				, " +
								"GROUPING_CATEGORY_CD	, " +
								"PROFILE_REQ_ITEM_FLAG	, " +
								"OMIT_SEEK_CONTION_FLAG	  " +
							"FROM " +
								"T_CAST_ATTR_TYPE " +
							"WHERE " +
								"SITE_CD	= :SITE_CD	AND " +
								"NA_FLAG	= 0				" +
							"ORDER BY " +
								"PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_ATTR_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	
	public string GetSeqByItemNo(string pSiteCd,string pItemNo) {
	
		System.Text.StringBuilder objSqlBuilder = new System.Text.StringBuilder();
		objSqlBuilder.Append(" SELECT					").AppendLine();
		objSqlBuilder.Append("     CAST_ATTR_TYPE_SEQ	").AppendLine();
		objSqlBuilder.Append(" FROM						").AppendLine();
		objSqlBuilder.Append("     T_CAST_ATTR_TYPE		").AppendLine();
		objSqlBuilder.Append(" WHERE					").AppendLine();
		objSqlBuilder.Append("	SITE_CD = :SITE_CD AND	").AppendLine();
		objSqlBuilder.Append("	ITEM_NO = :ITEM_NO		").AppendLine();
		
		try{
			conn  = DbConnect();
			using (cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":ITEM_NO", pItemNo);
			
				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		}finally{
			conn.Close();
		}
	}

	public string GetOneByItemNo(string pSiteCd,string pItemNo) {
		DataSet ds;
		DataRow dr;
		string sAttrTypeNm = "";
		try{
			conn = DbConnect("CastAttrType.GetAttrTypeNmByItemNo");

			string sSql = "SELECT " +
								"CAST_ATTR_TYPE_NM		," +
								"GROUPING_CATEGORY_CD	" +
							"FROM " +
								"T_CAST_ATTR_TYPE " +
							"WHERE " +
								"SITE_CD	= :SITE_CD	AND " +
								"ITEM_NO	= :ITEM_NO		";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ITEM_NO",pItemNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_ATTR_TYPE");

					if (ds.Tables["T_CAST_ATTR_TYPE"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_ATTR_TYPE"].Rows[0];
						attrTypeNm = dr["CAST_ATTR_TYPE_NM"].ToString();
						groupingCategoryCd = iBridUtil.GetStringValue(dr["GROUPING_CATEGORY_CD"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sAttrTypeNm;
	}

	public int GetGroupingPageCount(string pSiteCd,string pItemNo,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try{
			conn = DbConnect("CastAttrType.GetGroupingPageCountBy");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT " +
								"FROM " +
									"VW_CAST_ATTR_TYPE03 ";

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pItemNo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetGroupingPageCollection(string pSiteCd,string pItemNo,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try{
			conn = DbConnect("CastAttrType.GetGroupingPageCollection");
			ds = new DataSet();

			string sSql = "SELECT * FROM " +
							"(" +
								"SELECT " +
									"CAST_ATTR_TYPE_SEQ		," +
									"CAST_ATTR_TYPE_NM		," +
									"ITEM_NO				," +
									"GROUPING_CD			," +
									"GROUPING_NM			," +
									"ROW_NUMBER() OVER (ORDER BY GROUPING_CD) AS RNUM " +
								"FROM " +
									"VW_CAST_ATTR_TYPE03 ";
									
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pItemNo,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + "ORDER BY CAST_ATTR_TYPE_SEQ,GROUPING_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_ATTR_TYPE03");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pItemNo,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pItemNo.Equals("")) {
			SysPrograms.SqlAppendWhere("ITEM_NO = :ITEM_NO ",ref pWhere);
			list.Add(new OracleParameter("ITEM_NO",pItemNo));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}