﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using MobileLib;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class UserManAttr {
	public string attrTypeSeq;
	public string attrTypeNm;
	public string attrSeq;
	public string attrNm;
	public string attrImputValue;
	public string inputType;
	public string proirity;
	public string rowCount;
	public string itemNo;
	public string itemCd;
	public bool registInputFlag;
	public bool profileReqItemFlag;

	public UserManAttr() {
		attrTypeSeq = "";
		attrTypeNm = "";
		attrSeq = "";
		attrNm = "";
		attrImputValue = "";
		inputType = "";
		proirity = "";
		rowCount = "";
		itemNo = "";
		itemCd = "";
		registInputFlag = false;
		profileReqItemFlag = false;
	}
}

[System.Serializable]
public class UserManMailInfo:Object {
	public string rxUserSeq;
	public string rxUserCharNo;
	public string rxUserNm;
	public string mailTitle;
	public string mailDoc;
	public string mailTemplateNo;
	public string queryString;
	public string returnMailSeq;
	public string objTempId;
	public bool isReturnMail;
	public int quickResMailFlag;
	public string mailAttachType;
	public string draftMailSeq;
	public int chatMailFlag;
	public string rxLoginId;
	public int presentMailFlag;
	public string presentMailItemSeq;

	public UserManMailInfo() {
		rxUserSeq = string.Empty;
		rxUserCharNo = string.Empty;
		rxUserNm = string.Empty;
		mailTitle = string.Empty;
		mailDoc = string.Empty;
		mailTemplateNo = string.Empty;
		queryString = string.Empty;
		returnMailSeq = string.Empty;
		objTempId = string.Empty;
		isReturnMail = false;
		quickResMailFlag = 0;
		mailAttachType = string.Empty;
		draftMailSeq = string.Empty;
		chatMailFlag = 0;
		rxLoginId = string.Empty;
		presentMailFlag = 0;
		presentMailItemSeq = string.Empty;
	}
}


[System.Serializable]
public class UserManEx {

	public string blogMailRxType;
	public int waitMailRxTyp;
	public string richinoRank;
	public string siteUseStatus;
	public string spPremiumEndDate;
	public string unconfirmMarkingCount;  // 足あとの未確認件数

	public UserManEx() {
		blogMailRxType = string.Empty;
		waitMailRxTyp = ViCommConst.FLAG_OFF;
		richinoRank = string.Empty;
		siteUseStatus = string.Empty;
		spPremiumEndDate = string.Empty;
		unconfirmMarkingCount = string.Empty;
	}
}

[System.Serializable]
public class UserMan:DbSession {
	public string siteCd;
	public string userSeq;
	public string userCharNo;
	public int omikujiPoint;
	public int balPoint;
	public int realBalPoint;
	public int servicePoint;
	public int totalReceiptAmt;
	public int totalReceiptCount;
	public int mchaTotalReceiptAmt;
	public int limitPoint;
	public int billAmt;
	public string receiptLimitDay;
	public string userStatus;
	public string userRankCd;
	public string adCd;
	public string adGroupCd;
	public string tel;
	public int useFreeDialFlag;
	public int useWhitePlanFlag;
	public int useCrosmileFlag;
	public string uri;
	public string crosmileUri;
	public string crosmileRegistKey;
	public string skypeId;
	public string useTerminalType;
	public int telAttestedFlag;
	public string webLocCd;
	public string webUserId;
	public string tempRegistId;
	public string registCastLoginId;
	public string registCastHandleNm;
	public string loginId;
	public string loginPassword;
	public string beforeSystemId;	//ｸﾘﾌｫUSER_SEQ ACT_SYSTEMポイント決済に利用
	public string rankNm;
	public string handleNm;
	public string smallPhotoImgPath;
	public string photoImgPath;
	public string profilePicSeq;
	public string birthday;
	public string registDay;
	public string age;
	public int profileOkFlag;
	public string emailAddr;
	public int nonExistEmailAddrFlag;
	public string castMailRxType;
	public string InfoMailRxType;
	public int talkEndMailRxFlag;
	public int loginMailRxFlag1;
	public int loginMailRxFlag2;
	public int tweetCommentMailRxFlag;
	public int mobileMailRxTimeUseFlag;
	public string mobileMailRxStartTime;
	public string mobileMailRxEndTime;
	public int mobileMailRxTimeUseFlag2;
	public string mobileMailRxStartTime2;
	public string mobileMailRxEndTime2;
	public int existCreditDealFlag;
	public string inviteTalkKey;
	public string inviteCastSeq;
	public string inviteCastCharNo;
	public int inviteServicePoint;
	public bool inviteServiceReuqest;
	public Cast castCharacter;
	public List<UserManAttr> attrList;
	public Dictionary<string,UserManMailInfo> mailData;
	public int mailDataSeq;
	public string friendIntroCd;		//自分の紹介コード 
	public int ggFlag;
	public string settleId;
	public string familyNm;
	public string givenNm;
	public string prepaidId;
	public string prepaidPw;
	public string csvdlname;
	public string csvdlreceiptno;
	public DateTime csvdlreceiptLimitDate;
	public string csvdlreceiptUrl;
	public string terminalUniqueId;
	public string iModeId;
	public int zeroSettleOkFlag;
	public int settleRequestAmt;
	public int settleRequestPoint;
	public int settleServicePoint;
	public string rakutenOpenId;
	public bool rakutenOpenIdEnabled = false;
	public string aspAdTitle;
	public int handleNmResettingFlag;
	public int registIncompleteFlag;
	public int nonUsedEasyLoginFlag;
	public int registServicePointFlag;
	public int monthlyReceiptAmt;
	public int characterOnlineStatus;
	public string mobileOnlineStatusNm;
	public string manPfWaitingTypeNm;
	public string manPfWaitingType;
	public int bulkImpNonUseFlag;
	public int noReportAffiliateFlag;
	public string registCarrierCd;
	public int totalTalkCount;
	public int totalRxMailCount;
	public int totalTxMailCount;
	public int userDefineMask;
	public string lastPointUsedDate;
	public string omikujiNo;
	public int getOmikujiPoint;
	public UserManEx characterEx;
	public GameCharacter gameCharacter;
	public int? reductionReceiptSumAmt;
	public string reductionPointType;
	public string ifKey;
	public string lastReceiptDate;
	public string profileUpdateDate;
	public int ngCount;
	public int ngSkullCount;
	public int useVoiceappFlag;
	public int profilePicPointType;
	public string profilePointResult;
	public int existDeviceTokenFlag;
	/// <summary>退会申請中フラグ</summary>
	public string existAcceptWithdrawalFlag = string.Empty;

	public UserMan() {
		siteCd = "";
		userSeq = string.Empty;
		userCharNo = string.Empty;
		omikujiPoint = 0;
		balPoint = 0;
		realBalPoint = 0;
		servicePoint = 0;
		totalReceiptAmt = 0;
		totalReceiptCount = 0;
		mchaTotalReceiptAmt = 0;
		limitPoint = 0;
		billAmt = 0;
		receiptLimitDay = string.Empty;
		userStatus = string.Empty;
		userRankCd = string.Empty;
		adCd = string.Empty;
		adGroupCd = string.Empty;
		tel = string.Empty;
		telAttestedFlag = 0;
		useFreeDialFlag = 0;
		useWhitePlanFlag = 0;
		useCrosmileFlag = 0;
		uri = string.Empty;
		crosmileUri = string.Empty;
		crosmileRegistKey = string.Empty;
		loginId = string.Empty;
		loginPassword = string.Empty;
		beforeSystemId = string.Empty;
		webLocCd = string.Empty;
		webUserId = string.Empty;
		tempRegistId = string.Empty;
		registCastLoginId = string.Empty;
		registCastHandleNm = string.Empty;
		handleNm = string.Empty;
		smallPhotoImgPath = string.Empty;
		photoImgPath = string.Empty;
		profilePicSeq = string.Empty;
		birthday = string.Empty;
		age = string.Empty;
		profileOkFlag = 0;
		emailAddr = string.Empty;
		nonExistEmailAddrFlag = 0;
		castMailRxType = string.Empty;
		InfoMailRxType = string.Empty;
		talkEndMailRxFlag = 0;
		loginMailRxFlag1 = 0;
		loginMailRxFlag2 = 0;
		tweetCommentMailRxFlag = 0;
		mobileMailRxTimeUseFlag = 0;
		mobileMailRxStartTime = string.Empty;
		mobileMailRxEndTime = string.Empty;
		mobileMailRxTimeUseFlag2 = 0;
		mobileMailRxStartTime2 = string.Empty;
		mobileMailRxEndTime2 = string.Empty;
		existCreditDealFlag = 0;
		inviteTalkKey = string.Empty;
		inviteCastSeq = string.Empty;
		inviteCastCharNo = string.Empty;
		inviteServicePoint = 0;
		inviteServiceReuqest = false;
		castCharacter = new Cast();
		attrList = new List<UserManAttr>();
		mailData = new Dictionary<string,UserManMailInfo>();
		mailDataSeq = 0;
		friendIntroCd = "";
		ggFlag = 0;
		settleId = string.Empty;
		familyNm = string.Empty;
		givenNm = string.Empty;
		prepaidId = string.Empty;
		prepaidPw = string.Empty;
		csvdlname = string.Empty;
		csvdlreceiptno = string.Empty;
		terminalUniqueId = string.Empty;
		iModeId = string.Empty;
		zeroSettleOkFlag = 0;
		settleRequestAmt = 0;
		settleRequestPoint = 0;
		settleServicePoint = 0;
		aspAdTitle = string.Empty;
		handleNmResettingFlag = 0;
		registIncompleteFlag = 0;
		nonUsedEasyLoginFlag = 0;
		registServicePointFlag = 0;
		monthlyReceiptAmt = 0;
		characterOnlineStatus = 0;
		mobileOnlineStatusNm = string.Empty;
		manPfWaitingTypeNm = string.Empty;
		manPfWaitingType = string.Empty;
		bulkImpNonUseFlag = 0;
		noReportAffiliateFlag = 0;
		registCarrierCd = string.Empty;
		registDay = string.Empty;
		characterEx = new UserManEx();
		gameCharacter = new GameCharacter();
		totalTalkCount = 0;
		totalRxMailCount = 0;
		totalTxMailCount = 0;
		omikujiNo = string.Empty;
		getOmikujiPoint = 0;
		lastPointUsedDate = string.Empty;
		userDefineMask = 0;
		reductionReceiptSumAmt = null;
		reductionPointType = string.Empty;
		ifKey = string.Empty;
		lastReceiptDate = string.Empty;
		profileUpdateDate = string.Empty;
		ngCount = 0;
		ngSkullCount = 0;
		useVoiceappFlag = 0;
		profilePicPointType = 0;
		profilePointResult = string.Empty;
		existDeviceTokenFlag = 0;
		existAcceptWithdrawalFlag = string.Empty;
	}

	public int NextMailDataSeq() {
		mailDataSeq += 1;
		return mailDataSeq;
	}

	public void GetCornerCharge(string pSiteCd,string pCastUserSeq,string pChargeType,string pActCategorySeq,out int pChargeUnitSec,out int pChargePoint) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_CORNER_CHARGE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,pChargeType);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,pActCategorySeq);
			db.ProcedureInParm("pUSE_MAN_CROSMILE_FLAG",DbSession.DbType.VARCHAR2,0);
			db.ProcedureInParm("pUSE_CAST_CROSMILE_FLAG",DbSession.DbType.VARCHAR2,0);
			db.ProcedureOutParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pChargeUnitSec = db.GetIntValue("PCHARGE_UNIT_SEC");
			pChargePoint = db.GetIntValue("PCHARGE_POINT");
		}
	}

	public void RegistUser(
		string pTempRegistId,
		string pLoginPassword,
		string pTel,
		string pHandleNm,
		string pBirthday,
		string pMobileTerminalNm,
		string pTerminalUniqueId,
		string pIModeId,
		string[] pAttrTypeSeq,
		string[] pAttrSeq,
		string[] pAttrInputValue,
		string[] pAttrRowId,
		int pMaxAttrCount,
		out string pUserSeq,
		out string pLoginId,
		out string pResult
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];

		using (DbSession db = new DbSession()) {
			string[] sAttrInputValue = new string[pMaxAttrCount];

			for (int i = 0;i < pMaxAttrCount;i++) {
				if (pAttrInputValue[i] != null) {
					sAttrInputValue[i] = Mobile.EmojiToCommTag(userObjs.carrier,pAttrInputValue[i]);
				}
			}

			string sHandleNm = Mobile.EmojiToCommTag(userObjs.carrier,pHandleNm);

			db.PrepareProcedure("REGIST_USER_MAN");
			db.ProcedureInParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,pTempRegistId);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sHandleNm);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureInParm("PTX_TRACKING_FLAG",DbSession.DbType.NUMBER,0);	// 登録完了後IMGタグで送信するためTracking不要 
			db.ProcedureInParm("PREGIST_FROM_MAQIA_AF_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInArrayParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrTypeSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,pMaxAttrCount,sAttrInputValue);
			db.ProcedureInArrayParm("PMAN_ATTR_ROWID",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrRowId);
			db.ProcedureInParm("PMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,pMaxAttrCount);
			db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pUserSeq = db.GetStringValue("PUSER_SEQ");
			pLoginId = db.GetStringValue("PLOGIN_ID");
			pResult = db.GetStringValue("PRESULT");
		}
	}

	public void LoginUser(
		string pLoginMode,
		string pUtnOrGuId,
		string pLoginId,
		string pLoginPassword,
		string pSessionId,
		out string pUserSeq,
		out string pUserStatus,
		out string pResult
	) {
		string sPreviousFlag = string.Empty;
		LoginUser(pLoginMode,pUtnOrGuId,pLoginId,pLoginPassword,pSessionId,out pUserSeq,out pUserStatus,out sPreviousFlag,out pResult);
	}

	public void LoginUser(
		string pLoginMode,
		string pUtnOrGuId,
		string pLoginId,
		string pLoginPassword,
		string pSessionId,
		out string pUserSeq,
		out string pUserStatus,
		out string pPreviousFlag,
		out string pResult
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOGIN_USER_MAN");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("pLOGIN_MODE",DbSession.DbType.VARCHAR2,pLoginMode);
			db.ProcedureInParm("pUTN_OR_GUID",DbSession.DbType.VARCHAR2,pUtnOrGuId);
			db.ProcedureInParm("pLOGIN_ID",DbSession.DbType.VARCHAR2,pLoginId);
			db.ProcedureInParm("pLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureInParm("pWEB_SESSION_ID",DbSession.DbType.VARCHAR2,pSessionId);
			db.ProcedureInParm("pGCAPP_VERSION",DbSession.DbType.VARCHAR2,userObjs.gcappVersion);
			db.ProcedureInParm("pSNS_ACCOUNT_ID",DbSession.DbType.VARCHAR2,userObjs.snsId);
			db.ProcedureInParm("pSNS_TYPE",DbSession.DbType.VARCHAR2,userObjs.snsType);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pUSER_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPREVIOUS_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pADMIN_FLG",DbSession.DbType.NUMBER,SessionObjs.Current.adminFlg ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ExecuteProcedure();
			pUserSeq = db.GetStringValue("pUSER_SEQ");
			pUserStatus = db.GetStringValue("pUSER_STATUS");
			userObjs.userMan.loginId = db.GetStringValue("pRESULT_LOGIN_ID");
			pPreviousFlag = db.GetStringValue("pPREVIOUS_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}
		if (pResult.Equals("0")) {
			userObjs.logined = true;
			GetCurrentInfo(userObjs.site.siteCd,pUserSeq);
			userObjs.userRank.GetOne(userObjs.site.siteCd,userRankCd,userObjs.sexCd);
			userObjs.SetAdInfo(userObjs.userMan.adCd);
		}
	}

	public void ModifyUserHandleNm(
		string pSiteCd,
		string pUserSeq,
		string pHandleNm,
		out string pResult
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];

		using (DbSession db = new DbSession()) {
			string sHandleNm = Mobile.EmojiToCommTag(userObjs.carrier,pHandleNm);

			db.PrepareProcedure("MODIFY_USER_MAN_HANDLE_NM");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,userObjs.carrier);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,userObjs.mobileUserAgent);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,userObjs.currentIModeId);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}

		if (pResult.Equals("0")) {
			GetCurrentInfo(userObjs.userMan.siteCd,pUserSeq);
		}
	}

	public void ModifyUserTel(string pUserSeq,string pTel,string pSiteCd,out string pResult) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_USER_TEL");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}
		if (pResult.Equals("0")) {
			GetCurrentInfo(userObjs.userMan.siteCd,pUserSeq);
		}
	}

	public void ModifyUser(
		string pUserSeq,
		string pLoginPassword,
		string pTel,
		string pHandleNm,
		string pBirthday,
		out string pResult,
		string[] pAttrTypeSeq,
		string[] pAttrSeq,
		string[] pAttrInputValue,
		int pMaxAttrCount,
		int pTxSetupCompliteMail
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];

		using (DbSession db = new DbSession()) {
			string sHandleNm = Mobile.EmojiToCommTag(userObjs.carrier,pHandleNm);

			string[] sAttrInputValue = new string[pMaxAttrCount];

			for (int i = 0;i < pMaxAttrCount;i++) {
				if (pAttrInputValue[i] != null) {
					sAttrInputValue[i] = Mobile.EmojiToCommTag(userObjs.carrier,pAttrInputValue[i]);
				}
			}
			db.PrepareProcedure("MODIFY_USER_MAN");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sHandleNm);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,userObjs.carrier);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,userObjs.mobileUserAgent);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,userObjs.currentIModeId);
			db.ProcedureInArrayParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrTypeSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,pMaxAttrCount,sAttrInputValue);
			db.ProcedureInParm("PMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,pMaxAttrCount);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PTX_SETUP_COMPLITE_MAIL",DbSession.DbType.NUMBER,pTxSetupCompliteMail);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}

		if (pResult.Equals("0")) {
			if (userObjs.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY)) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("SITE_USE_STATUS_MAINTE");
					db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
					db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,userObjs.userMan.userSeq);
					db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
					db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.MAN);
					db.ProcedureInParm("pUSE_LIVE_CHAT_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON);
					db.ProcedureInParm("pUSE_GAME_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
			GetCurrentInfo(userObjs.userMan.siteCd,pUserSeq);
		}
	}

	public void SetupRxMail(
		string pSiteCd,
		string pUserSeq,
		string pCastMailRxType,
		string pInfoMailRxType,
		int pTalkEndMailRxFlag,
		bool pLoginMailRxFlag1,
		bool pLoginMailRxFlag2,
		string pMobileMailRxStartTime,
		string pMobileMailRxEndTime,
		int pClearMailAddrNgFlag
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_RX_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PCAST_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pCastMailRxType);
			db.ProcedureInParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pInfoMailRxType);
			db.ProcedureInParm("PTALK_END_MAIL_RX_FLAG",DbSession.DbType.NUMBER,pTalkEndMailRxFlag);
			db.ProcedureInParm("PLOGIN_MAIL_RX_FLAG1",DbSession.DbType.NUMBER,pLoginMailRxFlag1);
			db.ProcedureInParm("PLOGIN_MAIL_RX_FLAG2",DbSession.DbType.NUMBER,pLoginMailRxFlag2);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxStartTime);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxEndTime);
			db.ProcedureInParm("pCLEAR_MAIL_ADDR_NG_FLAG",DbSession.DbType.VARCHAR2,pClearMailAddrNgFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetCurrentInfo(userObjs.userMan.siteCd,pUserSeq);
	}

	public void SetupRxMailDouble(
		string pSiteCd,
		string pUserSeq,
		string pCastMailRxType,
		string pInfoMailRxType,
		int pTalkEndMailRxFlag,
		bool pLoginMailRxFlag1,
		bool pLoginMailRxFlag2,
		int pMobileMailRxTimeUseFlag,
		string pMobileMailRxStartTime,
		string pMobileMailRxEndTime,
		int pMobileMailRxTimeUseFlag2,
		string pMobileMailRxStartTime2,
		string pMobileMailRxEndTime2,
		int pClearMailAddrNgFlag
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_RX_MAIL_DOUBLE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PCAST_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pCastMailRxType);
			db.ProcedureInParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pInfoMailRxType);
			db.ProcedureInParm("PTALK_END_MAIL_RX_FLAG",DbSession.DbType.NUMBER,pTalkEndMailRxFlag);
			db.ProcedureInParm("PLOGIN_MAIL_RX_FLAG1",DbSession.DbType.NUMBER,pLoginMailRxFlag1);
			db.ProcedureInParm("PLOGIN_MAIL_RX_FLAG2",DbSession.DbType.NUMBER,pLoginMailRxFlag2);
			db.ProcedureInParm("PMOBILE_MAIL_RX_TIME_USE_FLAG",DbSession.DbType.NUMBER,pMobileMailRxTimeUseFlag);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxStartTime);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxEndTime);
			db.ProcedureInParm("PMOBILE_MAIL_RX_TIME_USE_FLAG2",DbSession.DbType.NUMBER,pMobileMailRxTimeUseFlag2);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME2",DbSession.DbType.VARCHAR2,pMobileMailRxStartTime2);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME2",DbSession.DbType.VARCHAR2,pMobileMailRxEndTime2);
			db.ProcedureInParm("pCLEAR_MAIL_ADDR_NG_FLAG",DbSession.DbType.VARCHAR2,pClearMailAddrNgFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetCurrentInfo(userObjs.userMan.siteCd,pUserSeq);
	}

	public void SetupFreeDial(
		string pUserSeq,
		bool pUseFreeDialFalg
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_FREE_DIAL");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSE_FREE_DIAL_FLAG",DbSession.DbType.NUMBER,pUseFreeDialFalg);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		useFreeDialFlag = (pUseFreeDialFalg) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
	}

	public void SetupWhitePlan(
		string pUserSeq,
		bool pUseWhitePlanFalg
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_WHITE_PLAN");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSE_WHITE_PLAN_FLAG",DbSession.DbType.NUMBER,pUseWhitePlanFalg);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		useWhitePlanFlag = (pUseWhitePlanFalg) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
	}

	public void SetupCrosmile(
		string pUserSeq,
		bool pUseCrosmileFalg
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_CROSMILE");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSE_CROSMILE_FLAG",DbSession.DbType.NUMBER,pUseCrosmileFalg);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		useCrosmileFlag = (pUseCrosmileFalg) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
	}

	public string SetupVoiceapp(string pSiteCd,string pUserSeq,int pUseVoiceappFlag) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_VOICEAPP");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,int.Parse(pUserSeq));
			db.ProcedureInParm("pUSE_VOICEAPP_FLAG",DbSession.DbType.NUMBER,pUseVoiceappFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		if (sResult.Equals("0")) {
			useVoiceappFlag = pUseVoiceappFlag;
		}

		return sResult;
	}

	public void SetupTermId(
		string pUserSeq,
		string pTermId,
		string pImodeId
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_TERM_ID");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTermId);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,pImodeId);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetCurrentInfo(userObjs.userMan.siteCd,pUserSeq);
	}

	public void RemoveProfilePic(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_MAN_PIC_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
			db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetCurrentInfo(userObjs.userMan.siteCd,pUserSeq);
	}

	public bool GetCurrentInfo(string pSiteCd,string pUserSeq) {

		SessionObjs userObjs = (SessionObjs)HttpContext.Current.Session["objs"];
		if (!(userObjs is SessionObjs)) {
			ViCommInterface.WriteIFLog("GetCurrentInfo","sessionObj is null");
		}

		return GetCurrentInfo(pSiteCd,pUserSeq,userObjs);
	}
	public bool GetCurrentInfo(string pSiteCd,string pUserSeq,SessionObjs pSessionObjs) {
		DataSet ds;
		DataRow dr;

		SessionObjs userObjs = pSessionObjs;
		if (!(userObjs is SessionObjs)) {
			ViCommInterface.WriteIFLog("GetCurrentInfo","sessionObj is null");
		}

		bool bExist = false;
		try {
			conn = DbConnect("UserMan.GetCurrentInfo");

			string sSql = "SELECT " +
								"P.SITE_CD							," +
								"P.USER_SEQ							," +
								"P.USER_CHAR_NO						," +
								"P.USER_RANK						," +
								"P.USER_STATUS						," +
								"P.TEL								," +
								"P.URI								," +
								"P.CROSMILE_URI						," +
								"P.CROSMILE_REGIST_KEY				," +
								"P.SKYPE_ID							," +
								"P.USE_TERMINAL_TYPE				," +
								"P.EMAIL_ADDR						," +
								"P.NON_EXIST_MAIL_ADDR_FLAG			," +
								"P.TEL_ATTESTED_FLAG				," +
								"P.USE_FREE_DIAL_FLAG				," +
								"P.USE_WHITE_PLAN_FLAG				," +
								"P.USE_CROSMILE_FLAG				," +
								"P.LOGIN_ID							," +
								"P.LOGIN_PASSWORD					," +
								"P.BEFORE_SYSTEM_ID					," +
								"P.OMIKUJI_POINT					," +
								"P.BAL_POINT						," +
								"P.LIMIT_POINT						," +
								"P.BILL_AMT							," +
								"P.RECEIPT_LIMIT_DAY				," +
								"P.LAST_RECEIPT_DATE				," +
								"P.SERVICE_POINT					," +
								"P.SERVICE_POINT_EFFECTIVE_DATE		," +
								"P.HANDLE_NM						," +
								"P.FAMILY_NM						," +
								"P.GIVEN_NM							," +
								"P.FRIEND_INTRO_CD					," +
								"P.SMALL_PHOTO_IMG_PATH				," +
								"P.PHOTO_IMG_PATH					," +
								"P.BIRTHDAY							," +
								"P.AGE								," +
								"P.PROFILE_OK_FLAG					," +
								"P.AD_CD							," +
								"P.EXIST_CREDIT_DEAL_FLAG			," +
								"P.CAST_MAIL_RX_TYPE				," +
								"P.INFO_MAIL_RX_TYPE				," +
								"P.LOGIN_MAIL_RX_FLAG1				," +
								"P.LOGIN_MAIL_RX_FLAG2				," +
								"P.TALK_END_MAIL_RX_FLAG			," +
								"P.MOBILE_MAIL_RX_TIME_USE_FLAG		," +
								"P.MOBILE_MAIL_RX_START_TIME		," +
								"P.MOBILE_MAIL_RX_END_TIME			," +
								"P.MOBILE_MAIL_RX_TIME_USE_FLAG2	," +
								"P.MOBILE_MAIL_RX_START_TIME2		," +
								"P.MOBILE_MAIL_RX_END_TIME2			," +
								"P.TOTAL_RECEIPT_AMT				," +
								"P.TOTAL_RECEIPT_COUNT				," +
								"P.POINT_AFFILIATE_TOTAL_AMT		," +
								"P.GG_FLAG							," +
								"P.TERMINAL_UNIQUE_ID				," +
								"P.IMODE_ID							," +
								"P.EXIST_CREDIT_DEAL_FLAG			," +
								"P.ZERO_SETTLE_OK_FLAG				," +
								"P.MAN_DEFAULT_HANDLE_NM			," +
								"P.RAKUTEN_OPEN_ID					," +
								"P.RAKUTEN_OPEN_ID_ENABLED			," +
								"P.HANDLE_NM_RESETTING_FLAG			," +
								"P.REGIST_SERVICE_POINT_FLAG		," +
								"P.NOT_USED_EASY_LOGIN				," +
								"P.MONTHLY_RECEIPT_AMT				," +
								"P.CHARACTER_ONLINE_STATUS			," +
								"P.MOBILE_ONLINE_STATUS_NM			," +
								"P.MAN_PF_WAITING_TYPE_NM			," +
								"P.MAN_PF_WAITING_TYPE				," +
								"P.BULK_IMP_NON_USE_FLAG			," +
								"P.NO_REPORT_AFFILIATE_FLAG			," +
								"P.REGIST_CARRIER_CD				," +
								"P.REGIST_DATE						," +
								"P.NA_FLAG							," +
								"P.PIC_SEQ							," +
								"P.USER_DEFINE_MASK					," +
								"P.TOTAL_TALK_COUNT					," +
								"P.TOTAL_RX_MAIL_COUNT				," +
								"P.TOTAL_TX_MAIL_COUNT				," +
								"P.LAST_POINT_USED_DATE				," +
								"P.USER_MAN_CHARACTER_UPDATE_DATE	," +
								"P.IF_KEY							," +
								"P.PROFILE_UPDATE_DATE				," +
								"P.NG_COUNT							," +
								"P.NG_SKULL_COUNT					," +
								"P.USE_VOICEAPP_FLAG				," +
								"P.DEVICE_TOKEN						," +
								"EX.BLOG_MAIL_RX_TYPE				," +
								"EX.WAIT_MAIL_RX_FLAG				," +
								"EX.RICHINO_RANK					," +
								"EX.SITE_USE_STATUS					," +
								"EX.SP_PREMIUM_END_DATE				," +
								"EX.TWEET_COMMENT_MAIL_RX_FLAG		" +
							"FROM " +
							" VW_USER_MAN_CHARACTER03 P,T_USER_MAN_CHARACTER_EX EX " +
							"WHERE " +
							" P.SITE_CD = :SITE_CD AND P.USER_SEQ = :USER_SEQ AND P.USER_CHAR_NO = :USER_CHAR_NO AND" +
							" P.SITE_CD			= EX.SITE_CD      (+) AND " +
							" P.USER_SEQ		= EX.USER_SEQ     (+) AND " +
							" P.USER_CHAR_NO	= EX.USER_CHAR_NO (+)     ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER03");

					if (ds.Tables["VW_USER_MAN_CHARACTER03"].Rows.Count != 0) {
						dr = ds.Tables["VW_USER_MAN_CHARACTER03"].Rows[0];
						siteCd = dr["SITE_CD"].ToString();
						userSeq = dr["USER_SEQ"].ToString();
						userCharNo = dr["USER_CHAR_NO"].ToString();
						omikujiPoint = int.Parse(dr["OMIKUJI_POINT"].ToString());
						balPoint = int.Parse(dr["BAL_POINT"].ToString());
						realBalPoint = int.Parse(dr["BAL_POINT"].ToString());
						totalReceiptAmt = int.Parse(dr["TOTAL_RECEIPT_AMT"].ToString());
						totalReceiptCount = int.Parse(dr["TOTAL_RECEIPT_COUNT"].ToString());
						mchaTotalReceiptAmt = int.Parse(dr["POINT_AFFILIATE_TOTAL_AMT"].ToString());
						limitPoint = int.Parse(dr["LIMIT_POINT"].ToString());
						billAmt = int.Parse(dr["BILL_AMT"].ToString());
						receiptLimitDay = dr["RECEIPT_LIMIT_DAY"].ToString();
						userStatus = dr["USER_STATUS"].ToString();

						if (!dr["SERVICE_POINT_EFFECTIVE_DATE"].ToString().Equals("")) {
							DateTime dtEffect = DateTime.Parse(dr["SERVICE_POINT_EFFECTIVE_DATE"].ToString());
							if (dtEffect > DateTime.Now) {
								balPoint += int.Parse(dr["SERVICE_POINT"].ToString());
								servicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
							}
						}
						userRankCd = dr["USER_RANK"].ToString();
						adCd = dr["AD_CD"].ToString();

						tel = dr["TEL"].ToString();
						useTerminalType = dr["USE_TERMINAL_TYPE"].ToString();

						if (useTerminalType.Equals(ViCommConst.TERM_SIP) || useTerminalType.Equals(ViCommConst.TERM_H323)) {
							tel = dr["URI"].ToString();
						} else if (useTerminalType.Equals(ViCommConst.TERM_SKYPE)) {
							tel = dr["SKYPE_ID"].ToString();
						}
						uri = dr["URI"].ToString();
						crosmileUri = dr["CROSMILE_URI"].ToString();
						crosmileRegistKey = dr["CROSMILE_REGIST_KEY"].ToString();

						emailAddr = dr["EMAIL_ADDR"].ToString();
						nonExistEmailAddrFlag = int.Parse(dr["NON_EXIST_MAIL_ADDR_FLAG"].ToString());
						telAttestedFlag = int.Parse(dr["TEL_ATTESTED_FLAG"].ToString());
						useFreeDialFlag = int.Parse(dr["USE_FREE_DIAL_FLAG"].ToString());
						useWhitePlanFlag = int.Parse(dr["USE_WHITE_PLAN_FLAG"].ToString());
						useCrosmileFlag = int.Parse(dr["USE_CROSMILE_FLAG"].ToString());
						loginId = dr["LOGIN_ID"].ToString();
						loginPassword = dr["LOGIN_PASSWORD"].ToString();
						beforeSystemId = dr["BEFORE_SYSTEM_ID"].ToString();
						if (string.IsNullOrEmpty(beforeSystemId)) {
							beforeSystemId = loginId;
						}
						handleNm = dr["HANDLE_NM"].ToString();

						if (handleNm.Equals(string.Empty)) {
							handleNm = dr["MAN_DEFAULT_HANDLE_NM"].ToString();
						}

						birthday = dr["BIRTHDAY"].ToString();
						age = dr["AGE"].ToString();
						profileOkFlag = int.Parse(dr["PROFILE_OK_FLAG"].ToString());
						castMailRxType = dr["CAST_MAIL_RX_TYPE"].ToString();
						InfoMailRxType = dr["INFO_MAIL_RX_TYPE"].ToString();
						talkEndMailRxFlag = int.Parse(dr["TALK_END_MAIL_RX_FLAG"].ToString());
						loginMailRxFlag1 = int.Parse(dr["LOGIN_MAIL_RX_FLAG1"].ToString());
						loginMailRxFlag2 = int.Parse(dr["LOGIN_MAIL_RX_FLAG2"].ToString());
						tweetCommentMailRxFlag = int.Parse(dr["TWEET_COMMENT_MAIL_RX_FLAG"].ToString());
						mobileMailRxTimeUseFlag = int.Parse(dr["MOBILE_MAIL_RX_TIME_USE_FLAG"].ToString());
						mobileMailRxStartTime = dr["MOBILE_MAIL_RX_START_TIME"].ToString();
						mobileMailRxEndTime = dr["MOBILE_MAIL_RX_END_TIME"].ToString();
						mobileMailRxTimeUseFlag2 = int.Parse(dr["MOBILE_MAIL_RX_TIME_USE_FLAG2"].ToString());
						mobileMailRxStartTime2 = dr["MOBILE_MAIL_RX_START_TIME2"].ToString();
						mobileMailRxEndTime2 = dr["MOBILE_MAIL_RX_END_TIME2"].ToString();
						existCreditDealFlag = int.Parse(dr["EXIST_CREDIT_DEAL_FLAG"].ToString());
						smallPhotoImgPath = dr["SMALL_PHOTO_IMG_PATH"].ToString();
						photoImgPath = dr["PHOTO_IMG_PATH"].ToString();
						profilePicSeq = dr["PIC_SEQ"].ToString();
						friendIntroCd = dr["FRIEND_INTRO_CD"].ToString();
						ggFlag = int.Parse(dr["GG_FLAG"].ToString());
						familyNm = dr["FAMILY_NM"].ToString();
						givenNm = dr["GIVEN_NM"].ToString();
						terminalUniqueId = dr["TERMINAL_UNIQUE_ID"].ToString();
						iModeId = dr["IMODE_ID"].ToString();
						zeroSettleOkFlag = int.Parse(dr["ZERO_SETTLE_OK_FLAG"].ToString());
						if (zeroSettleOkFlag == 0) {
							zeroSettleOkFlag = int.Parse(dr["EXIST_CREDIT_DEAL_FLAG"].ToString());
						}
						rakutenOpenId = dr["RAKUTEN_OPEN_ID"].ToString();
						rakutenOpenIdEnabled = (dr["RAKUTEN_OPEN_ID_ENABLED"].ToString().Equals("1"));
						handleNmResettingFlag = int.Parse(dr["HANDLE_NM_RESETTING_FLAG"].ToString());
						nonUsedEasyLoginFlag = int.Parse(dr["NOT_USED_EASY_LOGIN"].ToString());
						registServicePointFlag = int.Parse(dr["REGIST_SERVICE_POINT_FLAG"].ToString());
						monthlyReceiptAmt = int.Parse(dr["MONTHLY_RECEIPT_AMT"].ToString());
						characterOnlineStatus = int.Parse(dr["CHARACTER_ONLINE_STATUS"].ToString());
						mobileOnlineStatusNm = dr["MOBILE_ONLINE_STATUS_NM"].ToString();
						manPfWaitingTypeNm = dr["MAN_PF_WAITING_TYPE_NM"].ToString();
						manPfWaitingType = dr["MAN_PF_WAITING_TYPE"].ToString();
						bulkImpNonUseFlag = int.Parse(dr["BULK_IMP_NON_USE_FLAG"].ToString());
						noReportAffiliateFlag = int.Parse(dr["NO_REPORT_AFFILIATE_FLAG"].ToString());
						registCarrierCd = dr["REGIST_CARRIER_CD"].ToString();
						registDay = dr["REGIST_DATE"].ToString();
						userDefineMask = int.Parse(dr["USER_DEFINE_MASK"].ToString());
						lastPointUsedDate = iBridUtil.GetStringValue(dr["LAST_POINT_USED_DATE"]);
						ifKey = dr["IF_KEY"].ToString();
						lastReceiptDate = iBridUtil.GetStringValue(dr["LAST_RECEIPT_DATE"]);
						profileUpdateDate = iBridUtil.GetStringValue(dr["PROFILE_UPDATE_DATE"]);
						ngCount = int.Parse(dr["NG_COUNT"].ToString());
						ngSkullCount = int.Parse(dr["NG_SKULL_COUNT"].ToString());
						useVoiceappFlag = int.Parse(dr["USE_VOICEAPP_FLAG"].ToString());
						existDeviceTokenFlag = !string.IsNullOrEmpty(iBridUtil.GetStringValue(dr["DEVICE_TOKEN"])) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;

						switch (int.Parse(dr["NA_FLAG"].ToString())) {
							case ViCommConst.NA_CHAR_PF_NOT_APPROVED:
								registIncompleteFlag = 1;
								break;
							case ViCommConst.NA_CHAR_OUTSIDE_REGIST:
								registIncompleteFlag = 2;
								break;
							case ViCommConst.NA_CHAR_INVISIBLE:
								using (ManageCompany oManageCompany = new ManageCompany()) {
									if (dr["USER_MAN_CHARACTER_UPDATE_DATE"].ToString().Equals(string.Empty)) {
										if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_MAN_REG_PROF_RESETTING)) {
											registIncompleteFlag = 1;
										} else {
											registIncompleteFlag = 0;
										}
									} else {
										registIncompleteFlag = 0;
									}
								}
								break;
							default:
								// ImportﾕｰｻﾞｰはUserTop.aspxで処理






								registIncompleteFlag = 0;
								break;
						}
						totalTalkCount = int.Parse(dr["TOTAL_TALK_COUNT"].ToString());
						totalRxMailCount = int.Parse(dr["TOTAL_RX_MAIL_COUNT"].ToString());
						totalTxMailCount = int.Parse(dr["TOTAL_TX_MAIL_COUNT"].ToString());

						// Extend
						characterEx.blogMailRxType = iBridUtil.GetStringValue(dr["BLOG_MAIL_RX_TYPE"]);
						characterEx.waitMailRxTyp = int.Parse(dr["WAIT_MAIL_RX_FLAG"].ToString());
						characterEx.richinoRank = iBridUtil.GetStringValue(dr["RICHINO_RANK"]);
						characterEx.siteUseStatus = iBridUtil.GetStringValue(dr["SITE_USE_STATUS"]);
						characterEx.spPremiumEndDate = iBridUtil.GetStringValue(dr["SP_PREMIUM_END_DATE"]);

						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		if (bExist) {
			string sAdGroupCd = "";
			int iGroupType = 0;
			if (!adCd.Equals("")) {
				using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
					oSiteAdGroup.GetAdGroupCd(pSiteCd,adCd,out sAdGroupCd,out iGroupType);
				}
			}
			adGroupCd = sAdGroupCd;

			using (UserRank oRank = new UserRank()) {
				if (oRank.GetOne(siteCd,userRankCd,userObjs.sexCd)) {
					rankNm = oRank.userRanNm;
				}
			}

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_KICK_BACK_INTRO_SYSTEM)) {
					using (PersonalAd oPersonalAd = new PersonalAd()) {
						userObjs.personalAdCd = oPersonalAd.GetPersonalAdCd(userSeq);
					}
				}
			}

			if (reductionReceiptSumAmt == null) {
				using (PointReductionSchdule oPointReductionSchdule = new PointReductionSchdule()) {
					oPointReductionSchdule.GetReductionInfo(pSiteCd,pUserSeq,out reductionReceiptSumAmt,out reductionPointType);
				}
			}

			GetAttrValue(pSiteCd,pUserSeq,ViCommConst.MAIN_CHAR_NO);
		}
		return bExist;
	}


	public void GetAttrType(string pSiteCd) {
		if (attrList.Count == 0) {
			DataSet ds;
			UserManAttr attr;

			using (UserManAttrType oAttrType = new UserManAttrType()) {
				ds = oAttrType.GetList(pSiteCd);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					attr = new UserManAttr();
					attr.attrTypeSeq = dr["MAN_ATTR_TYPE_SEQ"].ToString();
					attr.attrTypeNm = dr["MAN_ATTR_TYPE_NM"].ToString();
					attr.inputType = dr["INPUT_TYPE"].ToString();
					attr.registInputFlag = dr["REGIST_INPUT_FLAG"].ToString().Equals("1");
					attr.profileReqItemFlag = dr["PROFILE_REQ_ITEM_FLAG"].ToString().Equals("1");
					attr.proirity = dr["PRIORITY"].ToString();
					attr.rowCount = dr["ROW_COUNT"].ToString();
					attr.itemNo = dr["ITEM_NO"].ToString();
					attrList.Add(attr);
				}
			}
		}
	}

	private void GetAttrValue(string pSiteCd,string pUserSeq,string pUserCharNo) {
		GetAttrType(pSiteCd);

		DataSet ds;
		UserManAttr oManAttr;
		using (UserManAttrValue oAttrValue = new UserManAttrValue()) {
			ds = oAttrValue.GetList(pSiteCd,pUserSeq,pUserCharNo);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				for (int i = 0;i < attrList.Count;i++) {
					oManAttr = attrList[i];
					if (oManAttr.attrTypeSeq.Equals(dr["MAN_ATTR_TYPE_SEQ"].ToString())) {
						oManAttr.attrImputValue = dr["MAN_ATTR_INPUT_VALUE"].ToString();
						oManAttr.attrSeq = dr["MAN_ATTR_SEQ"].ToString();
						oManAttr.attrNm = dr["MAN_ATTR_NM"].ToString();
						oManAttr.itemCd = dr["ITEM_CD"].ToString();
						break;
					}
				}
			}
		}
	}


	public bool TrasnServicePoint(
		string pInfoMailKey
	) {
		string sResult;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TRANS_SERVICE_POINT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PINFO_MAIL_KEY",DbSession.DbType.VARCHAR2,pInfoMailKey);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("PRESULT");
		}
		GetCurrentInfo(siteCd,userSeq);
		return sResult.Equals("0");
	}

	public void ModifyUserName(string pFamilyNm,string pGivenNm) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_MAN_NAME");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PFAMILY_NM",DbSession.DbType.VARCHAR2,pFamilyNm);
			db.ProcedureInParm("PGIVEN_NM",DbSession.DbType.VARCHAR2,pGivenNm);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void StartWaiting(DateTime pWaitEndDate,string pWaitingComment,string pWaitingType,int pWaitEndFlag,string pSessionId) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("START_MAN_WAITING");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,userCharNo);
			db.ProcedureInParm("PPARTNER_SITE_CD",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PWAIT_END_DATE",DbSession.DbType.DATE,pWaitEndDate);
			db.ProcedureInParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2,pWaitingComment);
			db.ProcedureInParm("PWAITING_TYPE",DbSession.DbType.VARCHAR2,pWaitingType);
			db.ProcedureInParm("PWAIT_END_FLAG",DbSession.DbType.NUMBER,pWaitEndFlag);
			db.ProcedureInParm("PWEB_SESSION_ID",DbSession.DbType.VARCHAR2,pSessionId);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		GetCurrentInfo(siteCd,userSeq);
	}

	public void ResignedReturn(
	string pLoginMode,
	string pLoginId,
	string pLoginPassword,
	out string pResult
	) {
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RESIGNED_RETURN_MAN");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PLOGIN_MODE",DbSession.DbType.VARCHAR2,pLoginMode);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,pLoginId);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}
	}

	public void WithdrawalMainte(ref string pWithDrawalSeq,string pReason) {
		string userSeq = string.Empty;
		string sReasonStatus = string.Empty;
		string sReasonCd = string.Empty;
		string sReasonDoc = string.Empty;

		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		userSeq = Convert.ToString(userObjs.userMan.userSeq);

		if (userSeq == string.Empty) {
			return;
		}
		if (pWithDrawalSeq.Equals(string.Empty)) {
			sReasonStatus = ViCommConst.WITHDRAWAL_STATUS_CHOICE;
			sReasonCd = pReason;
		} else {
			sReasonStatus = ViCommConst.WITHDRAWAL_STATUS_ACCEPT;
			sReasonDoc = pReason;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WITHDRAWAL_MAINTE");
			db.ProcedureBothParm("PWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithDrawalSeq);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("PWITHDRAWAL_STATUS",DbSession.DbType.VARCHAR2,sReasonStatus);
			db.ProcedureInParm("PWITHDRAWAL_REASON_CD",DbSession.DbType.VARCHAR2,sReasonCd);
			db.ProcedureInParm("PWITHDRAWAL_REASON_DOC",DbSession.DbType.VARCHAR2,sReasonDoc);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pWithDrawalSeq = db.GetStringValue("PWITHDRAWAL_SEQ");
		}
	}

	public void SecessionUser(out string pResult) {
		string userSeq = string.Empty;
		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		userSeq = Convert.ToString(userObjs.userMan.userSeq);

		if (userSeq == string.Empty) {
			pResult = "";
			return;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SECESSION_USER_MAN");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}
	}

	public bool CheckManHandleNmDupli(string sSiteCd,string sUserSeq,string sHandleNm) {
		bool bDupliFlag = false;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_MAN_HANDLE_NM_DUPLI");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
			db.ProcedureInParm("pHANDLE_NM",DbSession.DbType.VARCHAR2,sHandleNm);
			db.ProcedureOutParm("pDUPLI_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			bDupliFlag = iBridUtil.GetStringValue(db.GetStringValue("pDUPLI_FLAG")).Equals(ViCommConst.FLAG_ON_STR);
		}

		return bDupliFlag;
	}

	public bool CheckRichinoObjFreeView(string pSiteCd,string pUserSeq,string pObjType) {
		bool bViewOk = false;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_RICHINO_OBJ_FREE_VIEW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pOBJ_TYPE",DbSession.DbType.VARCHAR2,pObjType);
			db.ProcedureOutParm("pVIEW_OK_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			bViewOk = db.GetStringValue("pVIEW_OK_FLAG").Equals(ViCommConst.FLAG_ON_STR);
		}
		return bViewOk;
	}

	public bool IsExistPayment(string pSiteCd,string pUserSeq,string pPaymentType) {
		DataSet ds;
		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT 1 FROM DUAL WHERE EXISTS (SELECT * FROM T_RECEIPT WHERE SITE_CD = :SITE_CD AND USER_SEQ = :USER_SEQ AND SETTLE_TYPE = :SETTLE_TYPE)");

		bool bExist = false;
		conn = DbConnect();

		using (cmd = CreateSelectCommand(sSql.ToString(),conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("SITE_CD",pSiteCd);
			cmd.Parameters.Add("USER_SEQ",pUserSeq);
			cmd.Parameters.Add("SETTLE_TYPE",pPaymentType);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_RECEIPT");
				bExist = (ds.Tables["T_RECEIPT"].Rows.Count != 0);
			}
		}
		conn.Close();
		return bExist;
	}

	public string GetLastReceiptDate(string pSiteCd,string pUserSeq) {
		string sQuery = "SELECT LAST_RECEIPT_DATE FROM T_USER_MAN WHERE USER_SEQ = :USER_SEQ";

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sQuery,conn))
			using (DataSet ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow dr = ds.Tables[0].Rows[0];
						return iBridUtil.GetStringValue(dr["LAST_RECEIPT_DATE"]);
					}
				}
			}
		} finally {
			conn.Close();
		}

		return string.Empty;
	}

	public void RegistUserManByOpenId(
			string pSiteCd,
			string pIpAddr,
			string pTel,
			string pLoginPassword,
			string pHandleNm,
			string pBirthday,
			string[] pAttrTypeSeq,
			string[] pAttrSeq,
			string[] pAttrInputValue,
			int pMaxAttrCount,
			string[] pRegistSiteCd,
			out string pTempRegistId,
			out string pResult
		) {
		using (DbSession db = new DbSession()) {

			SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];

			int iRegistSiteCount = 0;

			if (pRegistSiteCd != null) {
				iRegistSiteCount = pRegistSiteCd.Length;
			}
			db.PrepareProcedure("REGIST_USER_MAN_BY_OPEN_ID");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("pAD_CD",DbSession.DbType.VARCHAR2,userObjs.adCd);
			db.ProcedureInParm("pAFFILIATER_CD",DbSession.DbType.VARCHAR2,userObjs.affiliateCompany);
			db.ProcedureInParm("pREGIST_AFFILIATE_CD",DbSession.DbType.VARCHAR2,userObjs.affiliaterCd);
			db.ProcedureInParm("pMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,userObjs.carrier);
			db.ProcedureInParm("pREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,pIpAddr);
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("pLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureInParm("pHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("pBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("pMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,userObjs.mobileUserAgent);
			db.ProcedureInParm("pINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,userObjs.introducerFriendCd);
			db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,userObjs.snsEmailAddr);
			db.ProcedureInParm("pSNS_ACCOUNT_ID",DbSession.DbType.VARCHAR2,userObjs.snsId);
			db.ProcedureInParm("pSNS_TYPE",DbSession.DbType.VARCHAR2,userObjs.snsType);
			db.ProcedureInArrayParm("pMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrTypeSeq);
			db.ProcedureInArrayParm("pMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrSeq);
			db.ProcedureInArrayParm("pMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrInputValue);
			db.ProcedureInParm("pMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,pMaxAttrCount);
			db.ProcedureInArrayParm("pSYNC_REGIST_SITE_CD",DbSession.DbType.VARCHAR2,iRegistSiteCount,pRegistSiteCd);
			db.ProcedureInParm("pSYNC_REGIST_COUNT",DbSession.DbType.NUMBER,iRegistSiteCount);
			db.ProcedureOutParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pTempRegistId = db.GetStringValue("pTEMP_REGIST_ID");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	/// <summary>
	/// 退会申請レコードを手動対応に変更
	/// </summary>
	/// <param name="pWithDrawalSeq"></param>
	public void UpdateManualWithdrawal(string pWithDrawalSeq) {
		string userSeq = string.Empty;

		SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];
		userSeq = Convert.ToString(userObjs.userMan.userSeq);

		if (userSeq.Equals(string.Empty)) {
			return;
		}
		if (pWithDrawalSeq.Equals(string.Empty)) {
			return;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MANUAL_WITHDRAWAL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithDrawalSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	/// <summary>
	/// つぶやき無制限判定
	/// </summary>
	/// <returns></returns>
	public bool IsTargetTweetUnlimited(string pSiteCd,string pUserSeq) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("			USER_SEQ");
		oSqlBuilder.AppendLine("			,TO_DATE(FIRST_RECEIPT_DAY,'YYYY/MM/DD') AS FIRST_RECEIPT_DATE");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_USER_MAN");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			USER_SEQ = :USER_SEQ");
		oSqlBuilder.AppendLine("			AND TOTAL_RECEIPT_AMT >= 1000");
		oSqlBuilder.AppendLine("	) UM");
		oSqlBuilder.AppendLine("	,(SELECT");
		oSqlBuilder.AppendLine("			ELAPSED_DAYS");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_TWEET_UNLIMITED");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("			AND SEX_CD = :SEX_CD");
		oSqlBuilder.AppendLine("			AND START_DATE <= SYSDATE");
		oSqlBuilder.AppendLine("			AND END_DATE >= SYSDATE");
		oSqlBuilder.AppendLine("	) TU");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	NVL(TU.ELAPSED_DAYS,-1) = 0");
		oSqlBuilder.AppendLine("	OR (");
		oSqlBuilder.AppendLine("		NVL(TU.ELAPSED_DAYS,-1) > 0");
		oSqlBuilder.AppendLine("		AND UM.FIRST_RECEIPT_DATE + NVL(TU.ELAPSED_DAYS,0) >= TRUNC(SYSDATE)");
		oSqlBuilder.AppendLine("	)");

		oParamList.Add(new OracleParameter("USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter("SEX_CD",ViCommConst.MAN));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return (oDataSet.Tables[0].Rows.Count > 0);
	}
}
