﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アクセス集計

--	Progaram ID		: Access
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Access:DbSession {
	private const int ACCESS_CAST_TYPE_PROFILE = 1;
	private const int ACCESS_CAST_TYPE_DIARY = 2;

	public Access() {
	}

	public void AccessPage(string pSiteCd,string pProgramRoot,string pProgramId,string pAction,string pUserSeq) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_PAGE");
			// [in]
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pPROGRAM_ROOT",DbSession.DbType.VARCHAR2,pProgramRoot);
			db.ProcedureInParm("pPROGRAM_ID",DbSession.DbType.VARCHAR2,pProgramId);
			db.ProcedureInParm("pACTION",DbSession.DbType.VARCHAR2,pAction);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			// [out]
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			string sStatus = db.GetStringValue("PSTATUS");

		}
	}


	public void AccessTopPage(string pSiteCd,string pUserSeq,string pUserCharNo,string pAdCd,string pSexCd) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_TOP_PAGE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,pAdCd);
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void AccessCastPage(string pSiteCd,string pUserSeq,string pUserCharNo,string pManUserSeq) {
		if (SessionObjs.Current.adminFlg)
			return;
		this.AccessCast(pSiteCd,pUserSeq,pUserCharNo,pManUserSeq,ACCESS_CAST_TYPE_PROFILE);
	}

	public void AccessMoviePage(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieSeq) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_MOVIE_PAGE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,pMovieSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
	public void AccessVoicePage(string pSiteCd,string pUserSeq,string pUserCharNo,string pVoiceSeq) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_VOICE_PAGE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PVOICE_SEQ",DbSession.DbType.VARCHAR2,pVoiceSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void AccessPicPage(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicSeq) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_PIC_PAGE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void AccessDiaryPage(string pSiteCd,string pUserSeq,string pUserCharNo,string pManUserSeq) {
		if (SessionObjs.Current.adminFlg)
			return;
		this.AccessCast(pSiteCd,pUserSeq,pUserCharNo,pManUserSeq,ACCESS_CAST_TYPE_DIARY);
	}
	private void AccessCast(string pSiteCd,string pUserSeq,string pUserCharNo,string pManUserSeq,int pAccessCastType) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_CAST");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PACCESS_CAST_TYPE",DbSession.DbType.NUMBER,pAccessCastType);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public bool GetCount(string pSiteCd,string pSexCd,int pPreviousDays,out int pLoginCount,out int pLoginUniqueCount) {
		DataSet ds;
		DataRow dr;
		pLoginCount = 0;
		pLoginUniqueCount = 0;

		bool bExist = false;

		string sReportDay = DateTime.Now.AddDays(pPreviousDays * -1).ToString("yyyy/MM/dd");
		try {
			conn = DbConnect("Access.GetCount");

			string sSql = "SELECT " +
							"LOGIN_SUCCESS_COUNT		," +
							"LOGIN_SUCCESS_COUNT_UNIQUE " +
						"FROM " +
							"T_ACCESS_TOP_PAGE " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND REPORT_DAY = :REPORT_DAY AND SEX_CD = :SEX_CD";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("REPORT_DAY",sReportDay);
				cmd.Parameters.Add("SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ACCESS_TOP_PAGE");

					if (ds.Tables["T_ACCESS_TOP_PAGE"].Rows.Count != 0) {
						dr = ds.Tables["T_ACCESS_TOP_PAGE"].Rows[0];
						pLoginCount = int.Parse(dr["LOGIN_SUCCESS_COUNT"].ToString());
						pLoginUniqueCount = int.Parse(dr["LOGIN_SUCCESS_COUNT_UNIQUE"].ToString());
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

}
