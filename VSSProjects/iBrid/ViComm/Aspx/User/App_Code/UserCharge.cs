﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザー課金

--	Progaram ID		: UserCharge
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using ViComm;
[System.Serializable]
public class UserCharge:DbSession {

	int chargePointNormal;
	int chargePointNew;
	int chargePointNoReceipt;
	int chargePointNoUse;
	int chargePointFreeDail;
	int chargePointWhitePlan;

	public UserCharge() {
		chargePointNormal = 0;
		chargePointNew = 0;
		chargePointNoReceipt = 0;
		chargePointNoUse = 0;
		chargePointFreeDail = 0;
		chargePointWhitePlan = 0;
	}

	public bool GetOne(string pSiteCd,string pUserRank,string pChargeType,string pActCategoryIdx,string pCastRank) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("UserCharge.GetChargePoint");

			string sSql = "SELECT " +
							"CHARGE_POINT_NORMAL		," +
							"CHARGE_POINT_NEW			," +
							"CHARGE_POINT_NO_RECEIPT	," +
							"CHARGE_POINT_NO_USE		," +
							"CHARGE_POINT_FREE_DIAL		," +
							"CHARGE_POINT_WHITE_PLAN " +
						"FROM " +
							"VW_USER_CHARGE01 " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND USER_RANK = :USER_RANK AND CHARGE_TYPE = :CHARGE_TYPE AND CATEGORY_INDEX = :CATEGORY_INDEX AND CAST_RANK = :CAST_RANK";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_RANK",pUserRank);
				cmd.Parameters.Add("CHARGE_TYPE",pChargeType);
				cmd.Parameters.Add("CATEGORY_INDEX",pActCategoryIdx);
				cmd.Parameters.Add("CAST_RANK",pCastRank);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_CHARGE01");

					if (ds.Tables["VW_USER_CHARGE01"].Rows.Count != 0) {
						dr = ds.Tables["VW_USER_CHARGE01"].Rows[0];
						chargePointNormal = int.Parse(dr["CHARGE_POINT_NORMAL"].ToString());
						chargePointNew = int.Parse(dr["CHARGE_POINT_NEW"].ToString());
						chargePointNoReceipt = int.Parse(dr["CHARGE_POINT_NO_RECEIPT"].ToString());
						chargePointNoUse = int.Parse(dr["CHARGE_POINT_NO_USE"].ToString());
						chargePointFreeDail = int.Parse(dr["CHARGE_POINT_FREE_DIAL"].ToString());
						chargePointWhitePlan = int.Parse(dr["CHARGE_POINT_WHITE_PLAN"].ToString());
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetChargePoint(string pSiteCd,string pUserRank,string pChargeType,string pActCategoryIdx,string pCastRank,string pUserStatus,int pUseFreeDialFlag,int pUseWhitePlanFlag) {
		int iCharge = 0;
		if (GetOne(pSiteCd,pUserRank,pChargeType,pActCategoryIdx,pCastRank)) {
			switch (pUserStatus) {
				case ViCommConst.USER_MAN_NORMAL:
					iCharge = chargePointNormal;
					break;
				case ViCommConst.USER_MAN_NEW:
					iCharge = chargePointNew;
					break;
				case ViCommConst.USER_MAN_NO_RECEIPT:
					iCharge = chargePointNoReceipt;
					break;
				case ViCommConst.USER_MAN_NO_USED:
					iCharge = chargePointNoUse;
					break;
				default:
					iCharge = chargePointNormal;
					break;
			}
			if (pUseFreeDialFlag != 0) {
				iCharge += chargePointFreeDail;
			}
		}
		return iCharge;
	}
}
