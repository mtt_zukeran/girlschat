﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 男性会員キャラクター拡張
--	Progaram ID		: UserManCharacterEx
--
--  Creation Date	: 2011.04.13
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class UserManCharacterEx:DbSession {
	public UserManCharacterEx() {	}

	public void SetupRxMailEx(string pSiteCd, string pUserSeq, string pBlogMailRxType, int pWaitMailRxType, int pTweetCommentMailRxFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("SETUP_RX_MAIL_EX");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, pUserSeq);
			oDbSession.ProcedureInParm("pBLOG_MAIL_RX_TYPE", DbSession.DbType.VARCHAR2, pBlogMailRxType);
			oDbSession.ProcedureInParm("pWAIT_MAIL_RX_TYPE", DbSession.DbType.NUMBER, pWaitMailRxType);
			oDbSession.ProcedureInParm("pTWEET_COMMENT_MAIL_RX_TYPE",DbSession.DbType.NUMBER,pTweetCommentMailRxFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}
}
