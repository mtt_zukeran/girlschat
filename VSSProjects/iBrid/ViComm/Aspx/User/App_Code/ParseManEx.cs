﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

using ViComm;

public partial class ParseMan:ParseViComm {
	public virtual string PerserEx(string pTag,string pArgument,out bool pParsed) {
		string sValue = string.Empty;
		pParsed = true;

		switch (pTag) {
			case "$BEAUTY_CLOCK_PARTS":
				sValue = GetListBeautyClock(pTag,pArgument);
				break;
			case "$GAME_SCORE_PARTS":
				sValue = GetListGameScore(pTag,pArgument);
				break;
			case "$SELF_MOTE_KING_POINT":
				sValue = GetSelfRankingPoint(pTag,ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN);
				break;
			case "$SELF_MOTE_KING_RANK":
				sValue = GetSelfRankingRank(pTag,ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN);
				break;
			case "$QUICK_RESPONSE_ROOM_IN_COUNT":
				sValue = GetQuickResponseRoomInCount(pTag);
				break;
			case "$SELF_CAST_HANDLE_NM":
				if (sessionMan.IsImpersonated) {
					sValue = parseContainer.Parse(sessionMan.originalWomanObj.userWoman.characterList[sessionMan.originalWomanObj.userWoman.curKey].handleNm);
				}
				break;
			case "$SELF_CAST_LOGIN_ID":
				if (sessionMan.IsImpersonated) {
					sValue = sessionMan.originalWomanObj.userWoman.loginId;
				}
				break;
			case "$ROW_COUNT_OF_PAGE":
				sValue = GetRowCountOfPage();
				break;

			#region ------------リッチーノ-------------------------------------
			case "$SELF_RICHINO_RANK":
				sValue = sessionMan.userMan.characterEx.richinoRank;
				break;
			case "$SELF_RICHINO_RANK_NM":
				sValue = GetRichinoValue(pTag,"RICHINO_RANK_NM");
				break;
			case "$SELF_RICHINO_MIN_RECEIPT_AMT":
				sValue = GetRichinoValue(pTag,"RICHINO_MIN_RECEIPT_AMT");
				break;
			case "$SELF_RICHINO_RANK_KEEP_AMT":
				sValue = GetRichinoValue(pTag,"RICHINO_RANK_KEEP_AMT");
				break;
			case "$SELF_RICHINO_NEXT_RANK_NM":
				sValue = GetRichinoNextValue(pTag,"RICHINO_RANK_NM");
				break;
			case "$SELF_RICHINO_NEXT_RECEIPT_AMT":
				sValue = GetRichinoNextValue(pTag,"RICHINO_MIN_RECEIPT_AMT");
				break;
			case "$DIV_IS_RICHINO_RANK_KEEP_OK":
				SetNoneDisplay(!IsRichinoRankKeepOK(pTag));
				break;
			case "$DIV_IS_RICHINO_RANK_KEEP_NG":
				SetNoneDisplay(IsRichinoRankKeepOK(pTag));
				break;

			#endregion---------------------------------------------------------
			
			case "$SELF_BLOG_MAIL_UNREAD_COUNT":
				sValue = GetUnreadMailCount(ViCommConst.MAIL_BOX_BLOG);
				break;
			#region ---------- ブログ 関係 ------------------------------------
			case "$BLOG_ARTICLE_PARTS":
				sValue = GetListBlogArticle(pTag,pArgument,sessionMan.site.siteCd);
				break;
			case "$BLOG_ARTICLE_PICKUP_PARTS":
				sValue = GetListBlogArticlePickup(pTag,pArgument,sessionMan.site.siteCd);
				break;
			case "$BLOG_PARTS":
				sValue = GetListBlog(pTag,pArgument,sessionMan.site.siteCd);
				break;
			#endregion --------------------------------------------------------

			#region ---------- この娘を探せ関係 ----------
			case "$INVESTIGATE_TARGET_PARTS":
				sValue = GetListInvestigateTarget(pTag,pArgument);
				break;
			case "$DIV_IS_INVESTIGATE_ENTER":
				SetNoneDisplay(!IsInvestigateEnter(pTag));
				break;
			case "$DIV_IS_NOT_INVESTIGATE_ENTER":
				SetNoneDisplay(IsInvestigateEnter(pTag));
				break;
			case "$DIV_IS_EXIST_INVESTIGATE_ACQUIRED_POINT":
				SetNoneDisplay(!IsInvestigateAcquiredPoint(pTag));
				break;
			#endregion

			#region ---------- PARTS ------------------------------------------
			case "$MAN_MOTEKING_PARTS":
				sValue = GetListMoteKing(pTag,pArgument);
				break;
			#endregion --------------------------------------------------------
			default:
				#region ---------- データセット処理 ----------
				switch (this.currentTableIdx) {
					case ViCommConst.DATASET_EX_GAME:
						sValue = ParseGame(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_GAME_SCORE:
						sValue = ParseGameScore(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_BLOG:
						sValue = ParseCast(pTag,pArgument,out pParsed);
						if (!pParsed) {
							sValue = ParseBlog(pTag,pArgument,out pParsed);
						}
						break;
					case ViCommConst.DATASET_EX_BLOG_ARTICLE:
						sValue = ParseCast(pTag,pArgument,out pParsed);
						if (!pParsed) {
							sValue = ParseBlogArticle(pTag,pArgument,out pParsed);
						}
						break;
					case ViCommConst.DATASET_EX_BLOG_OBJ:
						sValue = ParseCast(pTag,pArgument,out pParsed);
						if (!pParsed) {
							sValue = ParseBlogObj(pTag,pArgument,out pParsed);
						}
						break;
					case ViCommConst.DATASET_EX_BEAUTY_CLOCK:
						sValue = this.ParseBeautyClock(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_WANTED_TARGET:
						sValue = this.ParseWantedTarget(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_WANTED_SCHEDULE:
						sValue = this.ParseWantedSchedule(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_EX_BINGO_ENTRY:
						sValue = this.ParseBingoEntry(pTag,pArgument,out pParsed);
						break;
					default:
						pParsed = false;
						break;
				}
				break;
				#endregion
		}

		return sValue;
	}

	private string GetSelfRankingPoint(string pTag,string pExRankingType) {
		string sSeq = string.Empty;
		using (RankingCtl oRankingCtl = new RankingCtl()) {
			sSeq = oRankingCtl.GetLastSeq(sessionMan.site.siteCd,pExRankingType);
		}
		using (RankingEx oRankingEx = new RankingEx()) {
			return oRankingEx.GetValueNow(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sSeq,pExRankingType,"SUMMARY_COUNT");
		}

	}

	private string GetSelfRankingRank(string pTag,string pExRankingType) {
		string sSeq = string.Empty;
		using (RankingCtl oRankingCtl = new RankingCtl()) {
			sSeq = oRankingCtl.GetLastSeq(sessionMan.site.siteCd,pExRankingType);
		}
		using (RankingEx oRankingEx = new RankingEx()) {
			return oRankingEx.GetRankNow(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sSeq,pExRankingType,ViCommConst.MAN);
		}

	}

	//現在のリッチーノランクのデータを取得 
	private string GetRichinoValue(string pTag,string pFieldNm) {
		string sValue = string.Empty;
		using (RichinoRank oRichinoRank = new RichinoRank()) {
			oRichinoRank.GetValue(sessionMan.site.siteCd,sessionMan.userMan.characterEx.richinoRank,pFieldNm,ref sValue);
		}
		return sValue;
	}

	//次のリッチーノランクのデータを取得 
	private string GetRichinoNextValue(string pTag,string pFieldNm) {
		string sValue = string.Empty;
		string sTempRichinoRank = string.Empty;
		bool bNext = false;
		if (sessionMan.userMan.characterEx.richinoRank.Equals(string.Empty)) {
			bNext = true;
		}
		using (RichinoRank oRichinoRank = new RichinoRank()) {
			DataSet ds = oRichinoRank.GetRichinoRankList(sessionMan.site.siteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (bNext) {
					sValue = iBridUtil.GetStringValue(dr[pFieldNm]);
					break;
				}
				sTempRichinoRank = iBridUtil.GetStringValue(dr["RICHINO_RANK"]);
				if (sTempRichinoRank.Equals(sessionMan.userMan.characterEx.richinoRank)) {
					bNext = true;
				}
			}
		}
		return sValue;
	}
	private bool IsRichinoRankKeepOK(string pTag) {
		string sValue = string.Empty;
		int iKeepAmt = 0;
		sValue = GetRichinoValue(pTag,"RICHINO_RANK_KEEP_AMT");
		int.TryParse(sValue,out iKeepAmt);
		if (iKeepAmt <= sessionMan.userMan.monthlyReceiptAmt) {
			return true;
		} else {
			return false;
		}
	}


	private string GetQuickResponseRoomInCount(string pTag) {
		decimal iRecCount = 0;
		using (Cast oCast = new Cast()) {
			SeekCondition oSeekCondition = new SeekCondition();
			oSeekCondition.conditionFlag = ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE;
			oSeekCondition.siteCd = sessionMan.site.siteCd;
			oSeekCondition.userSeq = sessionMan.userMan.userSeq;
			oSeekCondition.userCharNo = sessionMan.userMan.userCharNo;
			oSeekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
			oCast.GetPageCount(oSeekCondition,1,out iRecCount);
		}
		return iRecCount.ToString();
	}

	private string GetSelfFanClubAdmissionEnable(string pCastUserSeq,string pCastCharNo) {
		string sResult;

		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			sResult = oFanClubStatus.GetFanClubAdmissionEnable(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				pCastUserSeq,
				pCastCharNo
			);
		}

		return sResult;
	}

	public string GetSelfFanClubMemberRank(string pCastUserSeq,string pCastCharNo) {
		string sValue = "0";

		if (DateTime.Parse(sessionMan.userMan.characterEx.spPremiumEndDate) >= DateTime.Now) {
			sValue = "5";
		} else {
			using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
				sValue = oFanClubStatus.GetMemberRank(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					pCastUserSeq,
					pCastCharNo
				);
			}
		}

		return sValue;
	}

	private string GetSelfFanClubMemberRankWithoutSpPremium(string pCastUserSeq,string pCastCharNo) {
		string sValue = "0";
		
		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			sValue = oFanClubStatus.GetMemberRank(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				pCastUserSeq,
				pCastCharNo
			);
		}

		return sValue;
	}

	private string GetRowCountOfPage() {
		int iRowCountOfPage = 0;

		if (iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["list"]).Equals("1")) {
			int.TryParse(iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["lrows"]),out iRowCountOfPage);

			if (iRowCountOfPage == 0) {
				iRowCountOfPage = sessionMan.site.mobileManListCount;
			}
		} else {
			int.TryParse(iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["drows"]),out iRowCountOfPage);

			if (iRowCountOfPage == 0) {
				iRowCountOfPage = sessionMan.site.mobileManDetailListCount;
			}
		}

		return iRowCountOfPage.ToString();
	}
}
