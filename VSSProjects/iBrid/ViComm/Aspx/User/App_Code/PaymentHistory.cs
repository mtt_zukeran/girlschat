﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 支払履歴
--	Progaram ID		: PaymentHistory
--
--  Creation Date	: 2010.09.14
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class PaymentHistory:DbSession {


	public decimal recCount;

	public PaymentHistory() {
	}


	public int GetPageCount(string pUserSeq,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try{
			conn = DbConnect("PaymentHistory.GetPageCount");
			ds = new DataSet();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_PAYMENT_HISTORY " +
							"WHERE USER_SEQ = :USER_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {

				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				da.Fill(ds,"T_PAYMENT_HISTORY");

				if (ds.Tables["T_PAYMENT_HISTORY"].Rows.Count != 0) {
					dr = ds.Tables["T_PAYMENT_HISTORY"].Rows[0];
					recCount = Decimal.Parse(dr["ROW_COUNT"].ToString());
					pRecCount = recCount;
					iPages = (int)Math.Ceiling(recCount / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}

		return iPages;
	}

	public DataSet GetPageCollection(string pUserSeq,int pPageNo,int pRecPerPage) {

		DataSet ds;
		try{
			conn = DbConnect("PaymentHistory.GetPageCollection");
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM ");
			sSql.Append("	(");
			sSql.Append("	SELECT ");
			sSql.Append("		USER_SEQ	,");
			sSql.Append("		PAYMENT_DATE,");
			sSql.Append("		TO_DATE(PAYMENT_CLOSE_DAY || ' ' || PAYMENT_CLOSE_HOUR, 'YYYY/MM/DD HH24') PAYMENT_CLOSE_DATE,");
			sSql.Append("		PAYMENT_AMT	,");
			sSql.Append("		ROW_NUMBER() OVER ( ORDER BY PAYMENT_DATE DESC ) AS RNUM ");
			sSql.Append("	FROM ");
			sSql.Append("		T_PAYMENT_HISTORY ");
			sSql.Append("	WHERE ");
			sSql.Append("		USER_SEQ = :USER_SEQ ");
			sSql.Append("	) ");
			sSql.Append("WHERE ");
			sSql.Append("	RNUM >= :FIRST_ROW AND ");
			sSql.Append("	RNUM <= :LAST_ROW ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd)) {

				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				da.Fill(ds,"T_PAYMENT_HISTORY");
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public bool GetLastPaymentDate(string pUserSeq,out DateTime dtMaxPaymentDate) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect("PaymentHistory.GetPageCollection");
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("	USER_SEQ	,");
			sSql.Append("	MAX(PAYMENT_DATE) AS MAX_PAYMENT_DATE ");
			sSql.Append("FROM ");
			sSql.Append("	T_PAYMENT_HISTORY ");
			sSql.Append("WHERE ");
			sSql.Append("	USER_SEQ = :USER_SEQ ");
			sSql.Append("GROUP BY ");
			sSql.Append("	USER_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd)) {

				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				da.Fill(ds,"T_PAYMENT_HISTORY");

				if (ds.Tables["T_PAYMENT_HISTORY"].Rows.Count != 0) {
					dr = ds.Tables["T_PAYMENT_HISTORY"].Rows[0];
					DateTime.TryParse(dr["MAX_PAYMENT_DATE"].ToString(),out dtMaxPaymentDate);
					bExist = true;
				} else {
					dtMaxPaymentDate = DateTime.MinValue;
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

}