﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ランキング拡張
--	Progaram ID		: RankingEx
--
--  Creation Date	: 2011.04.07
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Text;
using System;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class RankingEx:DbSession {

	public RankingEx() {
	}

	public string GetValueNow(string pSiteCd,string pUserSeq,string pUserCharNo,string pSeq,string pSummaryType,string sFieldNm) {
		DataSet ds;
		DataRow dr;
		string sValue = string.Empty;

		try {
			conn = DbConnect("RankingEx.GetValueNow");
			ds = new DataSet();

			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT	");
			oSql.AppendLine("	T_RANKING_EX.*	");
			oSql.AppendLine("FROM	");
			oSql.AppendLine("	T_RANKING_EX	");
			oSql.AppendLine("WHERE	");
			oSql.AppendLine("	SITE_CD			= :SITE_CD			AND	");
			oSql.AppendLine("	SUMMARY_TYPE	= :SUMMARY_TYPE		AND	");
			oSql.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
			oSql.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
			oSql.AppendLine("	RANKING_CTL_SEQ	= :RANKING_CTL_SEQ		");

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("RANKING_CTL_SEQ",pSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sValue = dr[sFieldNm].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sValue;
	}

	public string GetRankNow(string pSiteCd,string pUserSeq,string pUserCharNo,string pSeq,string pSummaryType,string pSexCd) {
		DataSet ds;
		DataRow dr;
		string sRank = string.Empty;

		try {
			conn = DbConnect("RankingEx.GetRankNow");
			ds = new DataSet();

			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT *	");
			oSql.AppendLine("	FROM	");
			oSql.AppendLine("		(SELECT	");
			oSql.AppendLine("			INNER.*	,		");
			oSql.AppendLine("			ROWNUM AS RNUM	");
			oSql.AppendLine("		FROM	");
			oSql.AppendLine("			(SELECT	");
			oSql.AppendLine("				T_RANKING_EX.*	");
			oSql.AppendLine("			FROM	");
			oSql.AppendLine("				T_RANKING_EX	,");
			if (pSexCd.Equals(ViCommConst.MAN)) {
				oSql.AppendLine("			T_USER_MAN_CHARACTER	C	");
			} else {
				oSql.AppendLine("			T_CAST_CHARACTER		C	");
			}
			oSql.AppendLine("			WHERE	");
			oSql.AppendLine("				T_RANKING_EX.SITE_CD		= :SITE_CD				AND	");
			oSql.AppendLine("				T_RANKING_EX.SUMMARY_TYPE	= :SUMMARY_TYPE			AND	");
			oSql.AppendLine("				T_RANKING_EX.RANKING_CTL_SEQ= :RANKING_CTL_SEQ		AND	");
			oSql.AppendLine("				T_RANKING_EX.SITE_CD		= C.SITE_CD				AND	");
			oSql.AppendLine("				T_RANKING_EX.USER_SEQ		= C.USER_SEQ			AND	");
			oSql.AppendLine("				T_RANKING_EX.USER_CHAR_NO	= C.USER_CHAR_NO		AND	");
			oSql.AppendLine("				C.NA_FLAG					= :NA_FLAG					");
			oSql.AppendLine("			ORDER BY	");
			oSql.AppendLine("				T_RANKING_EX.SITE_CD				,");
			oSql.AppendLine("				T_RANKING_EX.SUMMARY_COUNT DESC		,");
			oSql.AppendLine("				T_RANKING_EX.LAST_LOGIN_DATE DESC	,");
			oSql.AppendLine("				T_RANKING_EX.USER_SEQ			)INNER)	");
			oSql.AppendLine("	WHERE	");
			oSql.AppendLine("		SITE_CD		= :SITE_CD		AND	");
			oSql.AppendLine("		USER_SEQ	= :USER_SEQ		AND	");
			oSql.AppendLine("		USER_CHAR_NO= :USER_CHAR_NO	");

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUMMARY_TYPE",pSummaryType);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("RANKING_CTL_SEQ",pSeq);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);
				
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sRank = dr["RNUM"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return sRank;
	}
}
