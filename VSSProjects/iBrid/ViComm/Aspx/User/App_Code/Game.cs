﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｶﾞﾙﾁｬGAME
--	Progaram ID		: Game
--
--  Creation Date	: 2011.03.29
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[Serializable]
public class GameSeekCondition{
	private NameValueCollection query = new NameValueCollection();

	public ulong SeekMode {
		get { return ulong.Parse(this.query["seekmode"] ?? "-1"); }
		set { this.query["seekmode"] = value.ToString(); }
	}
	public string SiteCd {
		get { return this.query["site_cd"]; }
		set { this.query["site_cd"] = value; }
	}
	public string GameType {
		get { return this.query["game_type"]; }
		set { this.query["game_type"] = value; }
	}
	public string SexCd {
		get { return this.query["sex_cd"]; }
		set {
			this.query["sex_cd"] = value.Equals(ViCommConst.WOMAN) ? ViCommConst.OPERATOR : value;
		}
	}
	public string UserSeq {
		get { return this.query["user_seq"]; }
		set { this.query["user_seq"] = value; }
	}
	public string UserCharNo {
		get { return this.query["user_char_no"]; }
		set { this.query["user_char_no"] = value; }
	}
	public string Sort {
		get { return this.query["sort"]; }
		set { this.query["sort"] = value; }
	}

	
	public GameSeekCondition():this(new NameValueCollection()) {}
	public GameSeekCondition(NameValueCollection pQuery) {
		this.query["seekmode"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["seekmode"]));
		this.query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.query["game_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_type"]));
		this.query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
		this.query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.query["sort"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sort"]));
	}

	public NameValueCollection GetQuery() { return this.query; }

}

[System.Serializable]
public class Game : DbSession{
	
	public Game() {}

	public DataSet GetOne(string pSiteCd, string pGameType, string pSexCd) {
		if(pSexCd.Equals(ViCommConst.WOMAN)){
			pSexCd = ViCommConst.OPERATOR;
		}
	
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	SITE_CD		,	");
		oSqlBuilder.AppendLine("	GAME_TYPE	,	");
		oSqlBuilder.AppendLine("	SEX_CD		,	");
		oSqlBuilder.AppendLine("	FILE_NM			");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_GAME			");
		oSqlBuilder.AppendLine(" WHERE		");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND		");
		oSqlBuilder.AppendLine("	GAME_TYPE	= :GAME_TYPE	AND		");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD				");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":GAME_TYPE",pGameType));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));
		
		return ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
	}



	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(GameSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	T_GAME		");
		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);



		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder, oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion, pPageNo, pRecPerPage);
	}
	private DataSet GetPageCollectionBase(GameSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD			,	");
		oSqlBuilder.AppendLine("	GAME_TYPE		,	");
		oSqlBuilder.AppendLine("	SEX_CD			,	");
		oSqlBuilder.AppendLine("	FILE_NM			,	");
		oSqlBuilder.AppendLine("	GAME_PLAY_COUNT		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	VW_GAME01		");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion, ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		
		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, iStartIndex, pRecPerPage, out sExecSql));
		
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		AppendScoreInfo(oDataSet.Tables[0], pCondtion);
		return oDataSet;
	}
	
	private OracleParameter[] CreateWhere(GameSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" SEX_CD		= :SEX_CD", ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD", pCondition.SexCd));
		
		if(!string.IsNullOrEmpty(pCondition.GameType)){
			SysPrograms.SqlAppendWhere(" GAME_TYPE		= :GAME_TYPE", ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_TYPE", pCondition.GameType));
		}

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(GameSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		if (string.IsNullOrEmpty(pCondition.Sort)) {
			sSortExpression = " ORDER BY SITE_CD,GAME_TYPE DESC ";
		} else {
			sSortExpression = string.Format("ORDER BY {0} ", pCondition.Sort);
		}
		return sSortExpression;
	}

	#endregion ========================================================================================================
	
	public void AppendScoreInfo(DataTable pDataTable, GameSeekCondition pCondition){
		pDataTable.Columns.Add(new DataColumn("GAME_HIGH_SCORE", typeof(decimal)));
		pDataTable.Columns.Add(new DataColumn("GAME_HIGH_RANK", typeof(decimal)));
		pDataTable.Columns.Add(new DataColumn("GAME_LATEST_SCORE", typeof(decimal)));
		pDataTable.Columns.Add(new DataColumn("GAME_LATEST_RANK", typeof(decimal)));

		StringBuilder oMergeCondition = new StringBuilder();
		oMergeCondition.Append(" [SITE_CD] = '{0}' AND ");
		oMergeCondition.Append(" [GAME_TYPE] = '{1}' AND ");
		oMergeCondition.Append(" [SEX_CD] = '{2}' ");
		
		using(GameScore oGameScore = new GameScore()){
			// 最高得点情報
			using(DataSet oHighScoreDataSet = oGameScore.GetHighScoreInfo(pCondition.SiteCd,pCondition.GameType,pCondition.SexCd,pCondition.UserSeq,pCondition.UserCharNo)){
				foreach(DataRow oHighScoreDataRow in oHighScoreDataSet.Tables[0].Rows){
					string sSiteCd = iBridUtil.GetStringValue(oHighScoreDataRow["SITE_CD"]);
					string sGameType = iBridUtil.GetStringValue(oHighScoreDataRow["GAME_TYPE"]);
					string sSexCd = iBridUtil.GetStringValue(oHighScoreDataRow["SEX_CD"]);
				
					DataRow[] oDataRows = pDataTable.Select(string.Format(oMergeCondition.ToString(),sSiteCd,sGameType,sSexCd));
					if(oDataRows.Length > 0){
						oDataRows[0]["GAME_HIGH_SCORE"] = oHighScoreDataRow["GAME_HIGH_SCORE"];
						oDataRows[0]["GAME_HIGH_RANK"] = oHighScoreDataRow["GAME_HIGH_RANK"];
					}
				}
			}
			// 最新得点情報
			using (DataSet oHighScoreDataSet = oGameScore.GetLatestScoreInfo(pCondition.SiteCd, pCondition.GameType, pCondition.SexCd, pCondition.UserSeq, pCondition.UserCharNo)) {
				foreach (DataRow oHighScoreDataRow in oHighScoreDataSet.Tables[0].Rows) {
					string sSiteCd = iBridUtil.GetStringValue(oHighScoreDataRow["SITE_CD"]);
					string sGameType = iBridUtil.GetStringValue(oHighScoreDataRow["GAME_TYPE"]);
					string sSexCd = iBridUtil.GetStringValue(oHighScoreDataRow["SEX_CD"]);

					DataRow[] oDataRows = pDataTable.Select(string.Format(oMergeCondition.ToString(), sSiteCd, sGameType, sSexCd));
					if (oDataRows.Length > 0) {
						oDataRows[0]["GAME_LATEST_SCORE"] = oHighScoreDataRow["GAME_LATEST_SCORE"];
						oDataRows[0]["GAME_LATEST_RANK"] = oHighScoreDataRow["GAME_LATEST_RANK"];
					}
				}
			}

		}
		
	}
	
	
}
