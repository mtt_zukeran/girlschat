﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: IVP 要求



--	Progaram ID		: IvpRequest
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Configuration;
using System;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class IvpRequest:DbSession {

	public string acceptSeq;
	public string dialNo;
	public string videoPrefix;
	public string audioPrefix;
	public string interfaceResult;
	public bool useCrosmile;
	public bool crosmileDirectFlag;
	public string useXsmType;
	public bool useTwilioFlag;
	public bool useVoiceapp;

	// 絵文字消去用
	private static readonly Regex regex2 = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);
	public IvpRequest() {
		acceptSeq = string.Empty;
		dialNo = string.Empty;
		videoPrefix = string.Empty;
		audioPrefix = string.Empty;
		interfaceResult = string.Empty;
		useCrosmile = false;
		crosmileDirectFlag = false;
		useXsmType = string.Empty;
		useTwilioFlag = false;
		useVoiceapp = false;
	}

	public string Request(
		ref string pRequestId,
		string pRequesterSexCd,
		string pUserSeq,
		string pCastSeq,
		string pCastCharNo,
		string pMenuId,
		string pLiveSeq,
		string pCameraId,
		string pMovieSeq,
		string pMoviePartNo,
		string pShareLiveTalkKey,
		string pShareLiveAcceptNo,
		string pMeetingKey
	) {
		SessionObjs oUserObj = (SessionObjs)(HttpContext.Current.Session["objs"]);
		string sResult = string.Empty;
		string sRequestSeq = string.Empty;
		string sUrl = string.Empty;
		string sConnectType = string.Empty;
		string sDestNo = string.Empty;
		string sCastLoginId = string.Empty;
		string sRecSeq = string.Empty;
		string sDialOutNo = string.Empty;
		string sUseTerminalType = string.Empty;

		string sTel,sInviteTalkKey;
		string sUacSipUri = string.Empty;
		string sUasSipUri = string.Empty;
		string sReqDisplayNm = string.Empty;
		string sServiceCode = string.Empty;
		int iUseManCrosmileFlag = 0;
		int iUseCastCrosmileFlag = 0;
		int iUseManVoiceappFlag = 0;
		int iUseCastVoiceappFlag = 0;

		useXsmType = string.Empty;
		useCrosmile = false;
		crosmileDirectFlag = false;
		useTwilioFlag = false;
		useVoiceapp = false;
		bool bUseCrosmileFlag = true;
		string sUserTelNo = string.Empty;
		string sXsmVoicePhoneLineFlag = string.Empty;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("XSM_VOICE_PHONE_LINE_FLAG",out sXsmVoicePhoneLineFlag);
		}
		using (User oUser = new User()) {
			//VCSﾒﾆｭｰはｸﾛｽﾏｲﾙ利用不可
			if (pRequestId.Equals(ViCommConst.REQUEST_VCS_MENU) || pRequestId.Equals(ViCommConst.REQUEST_TEL_AUTH_LIGHT)) {
				bUseCrosmileFlag = false;
				//音声ﾓﾆﾀはｸﾛｽﾏｲﾙ利用不可
			} else if (pRequestId.Equals(ViCommConst.REQUEST_MONITOR_VOICE)) {
				bUseCrosmileFlag = false;
			} else if (sXsmVoicePhoneLineFlag.Equals(ViCommConst.FLAG_ON_STR) && (pMenuId.Equals(ViCommConst.MENU_VOICE_PTALK) || pMenuId.Equals(ViCommConst.MENU_VOICE_WTALK))) {
				// 音声通話時にCrosmileを利用しない(IVP LIGHTを利用)
				bUseCrosmileFlag = false;
			}
		}

		// ANDROID以外はｸﾛｽﾏｲﾙ利用しない



		if (oUserObj.carrier.Equals(ViCommConst.ANDROID) == false && oUserObj.carrier.Equals(ViCommConst.IPHONE) == false) {
			bUseCrosmileFlag = false;
		}

		if (oUserObj.carrier.Equals(ViCommConst.IPHONE) && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["disableTalkAppForIPhone"]).Equals(ViCommConst.FLAG_ON_STR)) {
			bUseCrosmileFlag = false;
		}

		string sLastVer = string.Empty;

		if (pRequesterSexCd.Equals(ViCommConst.OPERATOR) == false) {
			SessionMan oUser = (SessionMan)(HttpContext.Current.Session["objs"]);
			sTel = oUser.userMan.tel;
			sUserTelNo = oUser.userMan.tel;
			sUseTerminalType = oUser.userMan.useTerminalType;
			sInviteTalkKey = oUser.userMan.inviteTalkKey;
			if (oUser.userMan.useCrosmileFlag == ViCommConst.FLAG_ON && pMeetingKey.Equals(string.Empty)) {
				if (bUseCrosmileFlag) {
					oUser.GetCrosmileSipUriByRegistKey(oUser.userMan.crosmileRegistKey,out oUser.userMan.crosmileUri,out sLastVer);
					oUser.ModifyUserCrosmileUri(oUser.site.siteCd,oUser.userMan.userSeq,ViCommConst.MAN,oUser.userMan.crosmileUri,string.Empty,sLastVer);
					sTel = oUser.userMan.crosmileUri;
					sUacSipUri = oUser.userMan.crosmileUri;
					useCrosmile = true;
				}
			}
		} else {
			SessionWoman oUser = (SessionWoman)(HttpContext.Current.Session["objs"]);
			sTel = oUser.userWoman.tel;
			sUserTelNo = oUser.userWoman.tel;
			sUseTerminalType = ViCommConst.TERM_MOBILE;
			sInviteTalkKey = "";
			if (oUser.userWoman.useCrosmileFlag == ViCommConst.FLAG_ON) {
				if (bUseCrosmileFlag) {
					oUser.GetCrosmileSipUriByRegistKey(oUser.userWoman.crosmileRegistKey,out oUser.userWoman.crosmileUri,out sLastVer);
					oUser.ModifyUserCrosmileUri(oUser.site.siteCd,oUser.userWoman.userSeq,ViCommConst.OPERATOR,oUser.userWoman.crosmileUri,string.Empty,sLastVer);
					sTel = oUser.userWoman.crosmileUri;
					sUacSipUri = oUser.userWoman.crosmileUri;
					useCrosmile = true;
				}
			}
		}

		int iVcsFlag = 0,iTvCallFlag = 0;
		string sRequestUrl;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_IVP_REQUEST");
			db.ProcedureInParm("PREQUEST_SRC_ID",DbSession.DbType.VARCHAR2,sTel);
			db.ProcedureInParm("PCARRIER_TYPE",DbSession.DbType.VARCHAR2,oUserObj.carrier);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,oUserObj.site.siteCd);
			db.ProcedureBothParm("PREQUEST_ID",DbSession.DbType.VARCHAR2,pRequestId);
			db.ProcedureInParm("PMENU_ID",DbSession.DbType.VARCHAR2,pMenuId);
			db.ProcedureInParm("PREQUESTER_SEX_CD",DbSession.DbType.VARCHAR2,pRequesterSexCd);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("PLIVE_SEQ",DbSession.DbType.VARCHAR2,pLiveSeq);
			db.ProcedureInParm("PCAMERA_ID",DbSession.DbType.VARCHAR2,pCameraId);
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,pMovieSeq);
			db.ProcedureInParm("PMOVIE_PART_NO",DbSession.DbType.VARCHAR2,pMoviePartNo);
			db.ProcedureInParm("PINVITE_TALK_KEY",DbSession.DbType.VARCHAR2,sInviteTalkKey);
			db.ProcedureInParm("PBROADCASTING_SEC",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PPARTNER_SHARE_LIVE_ACCEPT_NO",DbSession.DbType.VARCHAR2,pShareLiveAcceptNo);
			db.ProcedureInParm("PPARTNER_SHARE_LIVE_KEY",DbSession.DbType.VARCHAR2,pShareLiveTalkKey);
			db.ProcedureInParm("PMEETING_KEY",DbSession.DbType.VARCHAR2,pMeetingKey);
			db.ProcedureOutParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSHARED_MEDIA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTALK_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PIVP_MENU_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ACTION_DTMF",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ACCEPT_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_FILE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_OFFSET_SEC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_SEC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDIAL_OUT_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSRC_USER_AGENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDEST_USER_AGENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_REQUEST_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSHARE_LIVE_TALK_KEY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSHARE_LIVE_TALK_END_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUAS_SIP_URI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSE_MAN_CROSMILE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_CAST_CROSMILE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSE_MAN_VOICEAPP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSE_CAST_VOICEAPP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			sResult = db.GetStringValue("PRESULT");
			pRequestId = db.GetStringValue("PREQUEST_ID");
			sRequestSeq = db.GetStringValue("PIVP_REQUEST_SEQ");
			iVcsFlag = db.GetIntValue("PVCS_REQUEST_FLAG");
			sConnectType = db.GetStringValue("PCONNECT_TYPE");
			sDestNo = db.GetStringValue("PDEST_NO");
			sCastLoginId = db.GetStringValue("PCAST_LOGIN_ID");
			sRecSeq = db.GetStringValue("PREC_SEQ");
			sDialOutNo = db.GetStringValue("PDIAL_OUT_NO");
			sUasSipUri = db.GetStringValue("PUAS_SIP_URI");
			iUseManCrosmileFlag = db.GetIntValue("PUSE_MAN_CROSMILE_FLAG");
			iUseCastCrosmileFlag = db.GetIntValue("PUSE_CAST_CROSMILE_FLAG");
			iUseManVoiceappFlag = db.GetIntValue("pUSE_MAN_VOICEAPP_FLAG");
			iUseCastVoiceappFlag = db.GetIntValue("pUSE_CAST_VOICEAPP_FLAG");

			if (sUacSipUri.Equals(string.Empty) && sUasSipUri.Equals(string.Empty)) {
				useXsmType = ViCommConst.UseCrosmile.NOT_USE;
			} else if (!sUacSipUri.Equals(string.Empty) && sUasSipUri.Equals(string.Empty)) {
				useXsmType = ViCommConst.UseCrosmile.USE_UAC_ONLY;
			} else if (sUacSipUri.Equals(string.Empty) && !sUasSipUri.Equals(string.Empty)) {
				useXsmType = ViCommConst.UseCrosmile.USE_UAS_ONLY;
			} else if (!sUacSipUri.Equals(string.Empty) && !sUasSipUri.Equals(string.Empty)) {
				useXsmType = ViCommConst.UseCrosmile.USE_UAC_AND_UAS;
			}

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLE_TWILIO,2)) {
					if (pMenuId.Equals(ViCommConst.MENU_VOICE_WTALK) && pRequestId.Equals(ViCommConst.REQUEST_TALK_LIGHT) && sConnectType.Equals(ViCommConst.IVP_CONNECT_TYPE_VOICE)) {
						if (string.IsNullOrEmpty(pMeetingKey)) {//通常待機
							if (iUseManCrosmileFlag.Equals(ViCommConst.FLAG_OFF) && iUseCastCrosmileFlag.Equals(ViCommConst.FLAG_OFF)) {
								useTwilioFlag = true;
							}
						} else {//Love待機
							useTwilioFlag = true;
						}
					}
				}

				if (useTwilioFlag) {
					if (pRequesterSexCd.Equals(ViCommConst.OPERATOR) == false) {
						if (iUseManVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
							useVoiceapp = true;
						}
					} else {
						if (iUseCastVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
							useVoiceapp = true;
						}
					}
				}

				if (string.IsNullOrEmpty(sUserTelNo) && oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_EMPTY_TEL_TWILIO,2)) {
					if (!useVoiceapp) {
						useTwilioFlag = false;
					}
				}
			}

			using (Sys oSys = new Sys()) {
				oSys.GetValue("IVP_REQUEST_URL",out sRequestUrl);

				string sIvpTerminalType = oUserObj.carrier;
				if (sUseTerminalType.Equals(ViCommConst.TERM_SKYPE)) {
					sIvpTerminalType = "04";
				}
				if (useXsmType.Equals(ViCommConst.UseCrosmile.USE_UAC_ONLY) || useXsmType.Equals(ViCommConst.UseCrosmile.USE_UAC_AND_UAS)) {
					sIvpTerminalType = ViCommConst.CARRIER_OTHERS;
				}

				if (pRequestId.Equals(ViCommConst.REQUEST_TALK_SMART_DIRECT) && bUseCrosmileFlag) {
					sServiceCode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartDirectServiceCode"]);
					crosmileDirectFlag = true;
				} else if (pRequestId.Equals(ViCommConst.REQUEST_TALK_SMART_IVP)) {
					sServiceCode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartServiceCode"]);
					crosmileDirectFlag = false;
				} else {
					sServiceCode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartServiceCode"]);
				}

				sUrl = string.Format(
							"{0}?tel={1}&sid={2}&carrier={3}&req={4}&menu={5}&desttype={6}&destno={7}&loc={8}&site={9}" +
							"&camera={10}&accept={11}&filenm={12}&offset={13}&playsec={14}&dialout={15}&action={16}&srcagent={17}&destagent={18}&remarks={19}&shkey={20}&shend={21}&acceptonly={22}" +
							"&xsm={23}",
							sRequestUrl,
							sTel,
							sRequestSeq,
							sIvpTerminalType,
							pRequestId,
							db.GetStringValue("PIVP_MENU_ID"),
							db.GetStringValue("PDEST_TYPE"),
							db.GetStringValue("PDEST_NO"),
							oUserObj.site.ivpLocCd,
							oUserObj.site.ivpSiteCd,
							db.GetStringValue("PSHARED_MEDIA_NM"),
							db.GetStringValue("PIVP_ACCEPT_SEQ"),
							db.GetStringValue("PPLAY_FILE_NM"),
							db.GetStringValue("PPLAY_OFFSET_SEC"),
							db.GetStringValue("PPLAY_SEC"),
							db.GetStringValue("PDIAL_OUT_NO"),
							db.GetStringValue("PIVP_ACTION_DTMF"),
							db.GetStringValue("PSRC_USER_AGENT"),
							db.GetStringValue("PDEST_USER_AGENT"),
							db.GetStringValue("PREMARKS"),
							db.GetStringValue("PSHARE_LIVE_TALK_KEY"),
							db.GetStringValue("PSHARE_LIVE_TALK_END_DATE"),
							iVcsFlag,
							useXsmType
						);
				if (useXsmType.Equals(ViCommConst.UseCrosmile.USE_UAC_ONLY) ||
					useXsmType.Equals(ViCommConst.UseCrosmile.USE_UAS_ONLY) ||
					useXsmType.Equals(ViCommConst.UseCrosmile.USE_UAC_AND_UAS)) {

					Encoding encUtf8 = Encoding.UTF8;

					string[] sManAttrValue = new string[20];
					string[] sCastAttrValue = new string[20];
					string sManHandleNm = string.Empty;
					string sManBalPoint = string.Empty;
					string sCastHandleNm = string.Empty;
					string sManAge = string.Empty;
					string sCastAge = string.Empty;
					string sManPicImgPath = string.Empty;
					string sCastPicImgPath = string.Empty;
					string sCastWaitingComment = string.Empty;
					string sCastCommentList = string.Empty;
					string sCastCommentDetail = string.Empty;

					//男性ハンドル名




					//所持ポイント取得 
					using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
						string sProfilePicSeq = string.Empty;
						oUserManCharacter.GetUserManData(oUserObj.site.siteCd,pUserSeq,ViCommConst.MAIN_CHAR_NO,out sManHandleNm,out sManBalPoint,out sManAge,out sProfilePicSeq);
						sManHandleNm = regex2.Replace(sManHandleNm,string.Empty);

						if (sProfilePicSeq.Equals(string.Empty)) {
							sManPicImgPath = string.Format("http://{0}/User/ViComm/man/image/{1}/photo_men1.gif",
															oUserObj.site.hostNm,
															oUserObj.site.siteCd);
						} else {
							sManPicImgPath = string.Format("http://{0}/User/ViComm/man/data/{1}/man/{2:D15}.jpg",
														oUserObj.site.hostNm,
														oUserObj.site.siteCd,
														int.Parse(sProfilePicSeq));
						}
					}
					//男性属性取得 
					using (UserManAttrTypeValue oUserManAttrTypeValue = new UserManAttrTypeValue()) {
						DataTable dt = oUserManAttrTypeValue.GetAttrDataTable(oUserObj.site.siteCd,pUserSeq,ViCommConst.MAIN_CHAR_NO);
						int i = 0;
						foreach (DataRow dr in dt.Rows) {
							sManAttrValue[i++] = regex2.Replace(iBridUtil.GetStringValue(dr["DISPLAY_VALUE"]),string.Empty);
						}
						for (int j = i;j < 20;j++) {
							sManAttrValue[j] = string.Empty;
						}
					}
					//女性ハンドル名取得 
					//女性プロフ画像パス取得




					using (Cast oCast = new Cast()) {
						string sProfilePicSeq = string.Empty;

						oCast.GetCastData(oUserObj.site.siteCd,pCastSeq,pCastCharNo,out sCastHandleNm,out sCastAge,out sProfilePicSeq,out sCastCommentList,out sCastCommentDetail,out sCastWaitingComment);
						sCastHandleNm = regex2.Replace(sCastHandleNm,string.Empty);
						sCastCommentList = regex2.Replace(sCastCommentList,string.Empty);
						sCastCommentDetail = regex2.Replace(sCastCommentDetail,string.Empty);
						sCastWaitingComment = regex2.Replace(sCastWaitingComment,string.Empty);

						if (sProfilePicSeq.Equals(string.Empty)) {
							sCastPicImgPath = string.Format("http://{0}/User/ViComm/man/image/{1}/nopic.gif",
															oUserObj.site.hostNm,
															oUserObj.site.siteCd);
						} else {
							sCastPicImgPath = string.Format("http://{0}/User/ViComm/man/data/{1}/operator/{2}/{3:D15}.jpg",
														oUserObj.site.hostNm,
														oUserObj.site.siteCd,
														sCastLoginId,
														int.Parse(sProfilePicSeq));
						}
					}
					//女性属性取得

					using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue()) {
						DataTable dt = oCastAttrTypeValue.GetAttrDataTable(oUserObj.site.siteCd,pCastSeq,pCastCharNo);
						int i = 0;
						foreach (DataRow dr in dt.Rows) {
							sCastAttrValue[i++] = regex2.Replace(iBridUtil.GetStringValue(dr["DISPLAY_VALUE"]),string.Empty);
						}
						for (int j = i;j < 20;j++) {
							sCastAttrValue[j] = string.Empty;
						}
					}

					String confXsmIvpParamLengthLimit = ConfigurationManager.AppSettings["xsmIvpParamLengthLimit"];
					int xsmIvpParamLengthLimit;
					if (!int.TryParse(confXsmIvpParamLengthLimit,out xsmIvpParamLengthLimit)) {
						xsmIvpParamLengthLimit = 50;
					}

					sUrl += string.Format("&screenPatternId={0}&outsideDisplayNm={1}&uacSipUri={2}&uasSipUri={3}&serviceCode={4}" +
							"&p1={5}&p2={6}&p3={7}&p4={8}&p5={9}&p6={10}&p7={11}&p8={12}&p9={13}&p10={14}&p11={15}&p12={16}&p13={17}" +
							"&p14={18}&p15={19}&p16={20}&p17={21}&p18={22}&p19={23}&p20={24}&p21={25}&p22={26}&p23={27}&p24={28}&p25={29}" +
							"&p26={30}&p27={31}&p28={32}&p29={33}&p30={34}&p31={35}&p32={36}&p33={37}&p34={38}&p35={39}&p36={40}&p37={41}" +
							"&p38={42}&p39={43}&p40={44}&p41={45}&p42={46}&p43={47}&p44={48}&p45={49}&p46={50}&p47={51}&p48={52}&p49={53}" +
							"&p50={54}&p51={55}",
							iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmScreenPatternId"]),
							iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmOutsideDisplayNm"]),
							sUacSipUri,
							sUasSipUri,
							sServiceCode,
							pRequesterSexCd,																					//p1	:発信側の性別
							HttpUtility.UrlEncode(SysPrograms.Substring(sManHandleNm,xsmIvpParamLengthLimit),encUtf8),			//p2	:男性のハンドル名


							HttpUtility.UrlEncode(SysPrograms.Substring(sCastHandleNm,xsmIvpParamLengthLimit),encUtf8),			//p3	:女性のハンドル名


							HttpUtility.UrlEncode(SysPrograms.Substring(sManBalPoint,xsmIvpParamLengthLimit),encUtf8),			//p4	:男性の所持ポイント


							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[0],xsmIvpParamLengthLimit),encUtf8),		//p5	:男性の属性1
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[1],xsmIvpParamLengthLimit),encUtf8),		//p6	:男性の属性2
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[2],xsmIvpParamLengthLimit),encUtf8),		//p7	:男性の属性3
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[3],xsmIvpParamLengthLimit),encUtf8),		//p8	:男性の属性4
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[4],xsmIvpParamLengthLimit),encUtf8),		//p9	:男性の属性5
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[5],xsmIvpParamLengthLimit),encUtf8),		//p10	:男性の属性6
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[6],xsmIvpParamLengthLimit),encUtf8),		//p11	:男性の属性7
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[7],xsmIvpParamLengthLimit),encUtf8),		//p12	:男性の属性8
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[8],xsmIvpParamLengthLimit),encUtf8),		//p13	:男性の属性9
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[9],xsmIvpParamLengthLimit),encUtf8),		//p14	:男性の属性10
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[10],xsmIvpParamLengthLimit),encUtf8),		//p15	:男性の属性11
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[11],xsmIvpParamLengthLimit),encUtf8),		//p16	:男性の属性12
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[12],xsmIvpParamLengthLimit),encUtf8),		//p17	:男性の属性13
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[13],xsmIvpParamLengthLimit),encUtf8),		//p18	:男性の属性14
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[14],xsmIvpParamLengthLimit),encUtf8),		//p19	:男性の属性15
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[15],xsmIvpParamLengthLimit),encUtf8),		//p20	:男性の属性16
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[16],xsmIvpParamLengthLimit),encUtf8),		//p21	:男性の属性17
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[17],xsmIvpParamLengthLimit),encUtf8),		//p22	:男性の属性18
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[18],xsmIvpParamLengthLimit),encUtf8),		//p23	:男性の属性19
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAttrValue[19],xsmIvpParamLengthLimit),encUtf8),		//p24	:男性の属性20
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[0],xsmIvpParamLengthLimit),encUtf8),		//p25	:女性の属性1
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[1],xsmIvpParamLengthLimit),encUtf8),		//p26	:女性の属性2
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[2],xsmIvpParamLengthLimit),encUtf8),		//p27	:女性の属性3
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[3],xsmIvpParamLengthLimit),encUtf8),		//p28	:女性の属性4
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[4],xsmIvpParamLengthLimit),encUtf8),		//p29	:女性の属性5
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[5],xsmIvpParamLengthLimit),encUtf8),		//p30	:女性の属性6
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[6],xsmIvpParamLengthLimit),encUtf8),		//p31	:女性の属性7
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[7],xsmIvpParamLengthLimit),encUtf8),		//p32	:女性の属性8
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[8],xsmIvpParamLengthLimit),encUtf8),		//p33	:女性の属性9
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[9],xsmIvpParamLengthLimit),encUtf8),		//p34	:女性の属性10
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[10],xsmIvpParamLengthLimit),encUtf8),	//p35	:女性の属性11
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[11],xsmIvpParamLengthLimit),encUtf8),	//p36	:女性の属性12
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[12],xsmIvpParamLengthLimit),encUtf8),	//p37	:女性の属性13
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[13],xsmIvpParamLengthLimit),encUtf8),	//p38	:女性の属性14
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[14],xsmIvpParamLengthLimit),encUtf8),	//p39	:女性の属性15
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[15],xsmIvpParamLengthLimit),encUtf8),	//p40	:女性の属性16
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[16],xsmIvpParamLengthLimit),encUtf8),	//p41	:女性の属性17
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[17],xsmIvpParamLengthLimit),encUtf8),	//p42	:女性の属性18
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[18],xsmIvpParamLengthLimit),encUtf8),	//p43	:女性の属性19
							HttpUtility.UrlEncode(SysPrograms.Substring(sCastAttrValue[19],xsmIvpParamLengthLimit),encUtf8),	//p44	:女性の属性20
							HttpUtility.UrlEncode(SysPrograms.Substring(sManAge,xsmIvpParamLengthLimit),encUtf8),				//p45	:男性年齢
							HttpUtility.UrlEncode(sCastAge,encUtf8),	//p46	:女性年齢
							HttpUtility.UrlEncode(sManPicImgPath,encUtf8),    //p47	:男性プロフ画像パス
							HttpUtility.UrlEncode(sCastPicImgPath,encUtf8), 	//p48	:女性プロフ画像パス
							string.Empty,	//p49	:女性一覧コメント

							string.Empty,	//p50	:女性詳細コメント

							string.Empty	//p51	:女性待機コメント

							);
				}

				if (!db.GetStringValue("PSTART_GUIDANCE").Equals("")) {
					sUrl = sUrl + string.Format("&gsec=20&gdest={0}",db.GetStringValue("PSTART_GUIDANCE"));
				}
			}
		}

		if (sResult.Equals(ViCommConst.SP_RESULT_OK)) {
			sResult = ExecRequest(pRequesterSexCd,sUrl,sRequestSeq,pRequestId,sConnectType,useXsmType);

			/*------------------------------*/
			/* VCS要求						*/
			/*------------------------------*/
			if ((iVcsFlag != 0) && (sResult.Equals(ViCommConst.SP_RESULT_OK))) {
				using (Sys oSys = new Sys()) {
					string sVcsRequest = "10"; // VCS遊びMENU
					oSys.GetValue("VCS_REQUEST_URL",out sRequestUrl);
					if (pRequestId.Equals(ViCommConst.REQUEST_TALK_LIGHT)) {
						pMenuId = ViCommConst.VCS_MENU_DIRECT_TALK;
					} else if (pRequestId.Equals(ViCommConst.REQUEST_TEL_AUTH_LIGHT)) {
						sVcsRequest = "1";
						pMenuId = string.Empty;
					}

					if (pMenuId.Equals(ViCommConst.VCS_MENU_DIRECT_TALK)) {
						if (sConnectType.Equals(ViCommConst.IVP_CONNECT_TYPE_VIDEO)) {
							iTvCallFlag = 1;
						} else {
							iTvCallFlag = 0;
						}
					} else {
						iTvCallFlag = 0;
					}

					SessionMan oUserMan = (SessionMan)(HttpContext.Current.Session["objs"]);

					sUrl = string.Format(
									"{0}?request={1}&loc={2}&pgm={3}&menu={4}&userid={5}&tel={6}&womanid={7}&womantel={8}&sid={9}&carrier={10}&vcstenant={11}&tvcall={12}&accept={13}&sex=1&castloginid={14}&recno={15}&charno={16}&dialout={17}&dialincid={18}",
									sRequestUrl,
									sVcsRequest,
									oUserMan.site.VcsWeLocCd,
									oUserMan.site.siteCd,
									pMenuId,
									oUserMan.userMan.loginId,
									oUserMan.userMan.tel,
									sCastLoginId,
									sDestNo,
									sRequestSeq,
									oUserMan.carrier,
									oUserMan.site.VcsTenantCd,
									iTvCallFlag,
									acceptSeq,
									sCastLoginId,
									sRecSeq,
									pCastCharNo,
									sDialOutNo,
									dialNo.Length > 3 ? dialNo.Substring(3) : ""
								);
				}
				sResult = ExecRequest(ViCommConst.MAN,sUrl,sRequestSeq,pRequestId,sConnectType,useXsmType);
			}

		}

		return sResult;
	}

	string ExecRequest(string pSexCd,string pUrl,string pRequestSeq,string pRequestId,string pConnectType,string pUseXsm) {
		string sTel,sWhitePlanNo = "";
		int iUseFreeDialFlag,iWhitePlanFlag = 0;

		if (pSexCd.Equals(ViCommConst.OPERATOR)) {
			SessionWoman oUserWoman = (SessionWoman)(HttpContext.Current.Session["objs"]);
			sTel = oUserWoman.userWoman.tel;
			iUseFreeDialFlag = oUserWoman.userWoman.useFreeDialFlag;
		} else {
			SessionMan oUserMan = (SessionMan)(HttpContext.Current.Session["objs"]);
			sTel = oUserMan.userMan.tel;
			iUseFreeDialFlag = oUserMan.userMan.useFreeDialFlag;
			if (oUserMan.carrier.Equals(ViCommConst.SOFTBANK)) {
				iWhitePlanFlag = oUserMan.userMan.useWhitePlanFlag;
			}
		}

		string sResult = ViCommConst.SP_RESULT_NG;

		if (!useTwilioFlag) {
			if (!pRequestId.Equals(ViCommConst.REQUEST_TALK_SMART_DIRECT)) {
				if (!pUseXsm.Equals(ViCommConst.UseCrosmile.USE_UAC_ONLY)) {
					if (sTel.Equals("")) {
						pUrl = pUrl + "&getcid=1";
						// Regler / PremiumはFreeDial CID取得未対応




						if (iUseFreeDialFlag != 0 && (pRequestId.Equals(ViCommConst.REQUEST_TALK_LIGHT) || pRequestId.Equals(ViCommConst.REQUEST_MONITOR_VOICE))) {
							pUrl = pUrl + "&freedial=1";
						}

					} else {
						if (iWhitePlanFlag != 0 && pRequestId.Equals(ViCommConst.REQUEST_TALK_LIGHT) && pConnectType.Equals(ViCommConst.IVP_CONNECT_TYPE_VOICE)) {
							using (Sys oSys = new Sys()) {
								if (oSys.IsAvailableWhitePlan()) {
									sWhitePlanNo = ((SessionObjs)(HttpContext.Current.Session["objs"])).site.whitePlanNo;
								}
							}
						}

						if (iUseFreeDialFlag != 0 && sWhitePlanNo.Equals(string.Empty)) {
							pUrl = pUrl + "&freedial=1";
						}
					}
				}
			}
			if (ViCommInterface.TransIVPUserRequest(pUrl,ref acceptSeq,ref dialNo,ref videoPrefix,ref audioPrefix,ref interfaceResult)) {
				if (sWhitePlanNo.Equals(string.Empty) == false) {
					dialNo = "186" + sWhitePlanNo;
				}

				sResult = UpdateAcceptSeq(pSexCd,pRequestSeq,acceptSeq,dialNo);
			}
		} else {
			//Twilio音声通話の場合
			videoPrefix = "tel-av:";
			audioPrefix = "tel:";
			interfaceResult = ViCommConst.IVP_RESULT_OK;

			if (useVoiceapp) {
				dialNo = string.Empty;
				sResult = ViCommConst.SP_RESULT_OK;
			} else {
				using (Sys oSys = new Sys()) {
					if (oSys.GetOne()) {
						if (iWhitePlanFlag != 0 && oSys.IsAvailableWhitePlan()) {
							dialNo = "186" + oSys.twilioWhiteplanTelNo;
						} else if (iUseFreeDialFlag != 0) {
							dialNo = "186" + oSys.twilioFreedialTelNo;
						} else {
							dialNo = "186" + oSys.twilioTelNo;
						}
					}
				}

				sResult = UpdateAcceptSeq(pSexCd,pRequestSeq,acceptSeq,dialNo);
			}
		}

		return sResult;
	}

	private string UpdateAcceptSeq(string pSexCd,string pRequestSeq,string pAcceptSeq,string pDialNo) {
		string sResult = ViCommConst.SP_RESULT_NG;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_ACCEPT_SEQ");
			db.ProcedureInParm("PREQUESTER_SEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("PIVP_REQUEST_SEQ",DbSession.DbType.VARCHAR2,pRequestSeq);
			db.ProcedureInParm("PIVP_ACCEPT_SEQ",DbSession.DbType.VARCHAR2,pAcceptSeq);
			db.ProcedureInParm("PDIALIN_NO",DbSession.DbType.VARCHAR2,pDialNo);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("PRESULT");
		}
		
		return sResult;
	}
}