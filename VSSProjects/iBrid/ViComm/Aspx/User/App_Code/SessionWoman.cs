﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;


[System.Serializable]
public partial class SessionWoman:SessionObjs {

	public UserWoman userWoman;
	public bool usedEasyLoginFlag;
	public bool getTermIdFlag;
	public bool batchMailFlag;
	public string batchMailSeekDevice = string.Empty;

	public SessionMan originalManObj;

	public override SessionObjs GetOriginalObj() {
		return this.originalManObj;
	}

	/// <summary>
	/// 一括送信メール送信先ユーザー一覧
	/// </summary>
	private List<string> batchMailUserList;
	private DateTime batchMailListCreateDate;

	public List<string> BatchMailUserList {
		get {
			return batchMailUserList;
		}
		set {
			batchMailUserList = value;
			batchMailListCreateDate = DateTime.Now;
		}
	}

	public DateTime BatchMailListCreateDate {
		get {
			return batchMailListCreateDate;
		}
	}

	public override string GetUserSeq() {
		string sUserSeq = null;
		if (this.userWoman != null && !string.IsNullOrEmpty(this.userWoman.userSeq)) {
			sUserSeq = this.userWoman.userSeq;
		}
		return sUserSeq;
	}

	/// <summary>
	/// 一括送信メール検索条件一覧
	/// </summary>
	private Dictionary<string,string> batchMailSearchConditions;

	public Dictionary<string,string> BatchMailSearchConditions {
		get {
			return batchMailSearchConditions;
		}
		set {
			batchMailSearchConditions = value;
		}
	}

	public SessionWoman() {
	}
	public SessionWoman(string pHost,string pSessionId,string pCarrierCd,string pBrowserId,string pUserAgent,string pAdCd,NameValueCollection pQuery,bool pIsCrawler,bool pCookieLess)
		: base(pHost,pSessionId,pCarrierCd,pBrowserId,pUserAgent,pAdCd,pQuery,pIsCrawler,pCookieLess) {
		sexCd = ViCommConst.WOMAN;
		userWoman = new UserWoman();
		originalManObj = null;
		using (ManageCompany oCompany = new ManageCompany()) {
			oCompany.GetOne();
			usedEasyLoginFlag = (oCompany.womanUsedEasyLoginFlag == 1);
			getTermIdFlag = (oCompany.womanGetTermIdFlag == 1);
		}
	}

	public HtmlFilter InitScreen(Stream pFilter,MobilePage pPage,HttpRequest pRequest,StateBag pViewState,bool pIsPostBack) {
		string sUserRank = ViCommConst.DEFAUL_USER_RANK;
		string sAdGroupCd = ViCommConst.DEFAUL_AD_GROUP_CD;

		string sSiteCd,sUserCharNo;
		bool bChangeSite = false;
		bool bChangeChar = false;

		if (!pIsPostBack) {
			sSiteCd = iBridUtil.GetStringValue(pRequest.QueryString["site"]);
			sUserCharNo = iBridUtil.GetStringValue(pRequest.QueryString["charno"]);
		} else {
			sSiteCd = iBridUtil.GetStringValue(pViewState["SITE_CD"]);
			sUserCharNo = iBridUtil.GetStringValue(pViewState["USER_CHAR_NO"]);
		}

		// Legnth Checkはｻｲﾄｺｰﾄﾞ・ｷｬﾗNo重複指定時のBUG対応のため 
		if (!sSiteCd.Equals("") && !site.siteCd.Equals(sSiteCd) && sSiteCd.Length == 4) {
			bChangeSite = true;
		}
		if (!sUserCharNo.Equals("") && !userWoman.curCharNo.Equals(sUserCharNo) && sUserCharNo.Length == 2) {
			bChangeChar = true;
		}

		if (bChangeSite) {
			site.GetOne(sSiteCd);
			userWoman.UpdateCharacterInfo(site.siteCd);
			userWoman.curCharNo = sUserCharNo;
			pViewState["SITE_CD"] = sSiteCd;
			pViewState["USER_CHAR_NO"] = sUserCharNo;
			userWoman.curKey = sSiteCd + sUserCharNo;
		} else if (bChangeChar) {
			userWoman.curCharNo = sUserCharNo;
			pViewState["USER_CHAR_NO"] = sUserCharNo;
			userWoman.curKey = sSiteCd + sUserCharNo;
		} else {
			pViewState["SITE_CD"] = sSiteCd;
			pViewState["USER_CHAR_NO"] = sUserCharNo;
		}

		if (this.logined) {
			userWoman.SetLastActionDate(userWoman.userSeq);
			if (ViCommConst.FLAG_ON_STR.Equals(System.Configuration.ConfigurationManager.AppSettings["enabledCastAdGroupCd"])) {
				sAdGroupCd = this.GetAdGroupCd();
			}
		} else {
			if (adGroup != null) {
				sAdGroupCd = adGroup.adGroupCd;
			}
		}

		arAdNo = new string[10];
		iAdCnt = 0;

		return base.InitScreen(pFilter,pPage.GetForm("frmMain"),pRequest,pViewState,sUserRank,sAdGroupCd);
	}

	public HtmlFilter InitScreen(Stream pFilter,MobilePage pPage,HttpRequest pRequest,string pCurrentAspx,StateBag pViewState,bool pIsPostBack) {
		string sUserRank = ViCommConst.DEFAUL_USER_RANK;
		string sAdGroupCd = ViCommConst.DEFAUL_AD_GROUP_CD;

		string sSiteCd,sUserCharNo;
		bool bChangeSite = false;
		bool bChangeChar = false;

		if (!pIsPostBack) {
			sSiteCd = iBridUtil.GetStringValue(pRequest.QueryString["site"]);
			sUserCharNo = iBridUtil.GetStringValue(pRequest.QueryString["charno"]);
		} else {
			sSiteCd = iBridUtil.GetStringValue(pViewState["SITE_CD"]);
			sUserCharNo = iBridUtil.GetStringValue(pViewState["USER_CHAR_NO"]);
		}

		// Legnth Checkはｻｲﾄｺｰﾄﾞ・ｷｬﾗNo重複指定時のBUG対応のため 
		if (!sSiteCd.Equals("") && !site.siteCd.Equals(sSiteCd) && sSiteCd.Length == 4) {
			bChangeSite = true;
		}
		if (!sUserCharNo.Equals("") && !userWoman.curCharNo.Equals(sUserCharNo) && sUserCharNo.Length == 2) {
			bChangeChar = true;
		}

		if (bChangeSite) {
			site.GetOne(sSiteCd);
			userWoman.UpdateCharacterInfo(site.siteCd);
			userWoman.curCharNo = sUserCharNo;
			pViewState["SITE_CD"] = sSiteCd;
			pViewState["USER_CHAR_NO"] = sUserCharNo;
			userWoman.curKey = sSiteCd + sUserCharNo;
		} else if (bChangeChar) {
			userWoman.curCharNo = sUserCharNo;
			pViewState["USER_CHAR_NO"] = sUserCharNo;
			userWoman.curKey = sSiteCd + sUserCharNo;
		} else {
			pViewState["SITE_CD"] = sSiteCd;
			pViewState["USER_CHAR_NO"] = sUserCharNo;
		}

		if (this.logined) {
			userWoman.SetLastActionDate(userWoman.userSeq);
			if (ViCommConst.FLAG_ON_STR.Equals(System.Configuration.ConfigurationManager.AppSettings["enabledCastAdGroupCd"])) {
				sAdGroupCd = this.GetAdGroupCd();
			}
		} else {
			if (adGroup != null) {
				sAdGroupCd = adGroup.adGroupCd;
			}
		}

		arAdNo = new string[10];
		iAdCnt = 0;

		return base.InitScreen(pFilter,pPage.GetForm("frmMain"),pRequest,pCurrentAspx,pViewState,sUserRank,sAdGroupCd);
	}

	public HtmlFilter InitScreen(Stream pFilter,Form pForm,HttpRequest pRequest,string pCurrentAspx,StateBag pViewState) {
		string sUserRank = ViCommConst.DEFAUL_USER_RANK;
		string sAdGroupCd = ViCommConst.DEFAUL_AD_GROUP_CD;

		arAdNo = new string[10];
		iAdCnt = 0;

		if (ViCommConst.FLAG_ON_STR.Equals(System.Configuration.ConfigurationManager.AppSettings["enabledCastAdGroupCd"])) {
			sAdGroupCd = this.GetAdGroupCd();
		}

		return base.InitScreen(pFilter,pForm,pRequest,pCurrentAspx,pViewState,sUserRank,sAdGroupCd);
	}

	public bool SetManDataSetByLoginId(string pLoginId,int pRNum) {
		DataRow dr;
		return SetManDataSetByLoginId(pLoginId,pRNum,out dr);
	}

	public bool SetManDataSetByLoginId(string pLoginId,int pRNum,out DataRow pDr) {
		int iIdx;
		iIdx = ViCommConst.DATASET_MAN;
		ParseHTML oParseWoman = this.parseContainer;
		UserWomanCharacter oWomanCharacter = userWoman.characterList[userWoman.curKey];
		DataSet dsDataSet = oWomanCharacter.manCharacter.GetOneByLoginId(site.siteCd,pLoginId,pRNum);
		if (dsDataSet.Tables[0].Rows.Count > 0) {
			oParseWoman.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
			oParseWoman.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
			oParseWoman.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
			pDr = dsDataSet.Tables[0].Rows[0];
			return true;
		} else {
			oParseWoman.currentRecPos[iIdx] = -1;
			oParseWoman.parseUser.SetDataRow(iIdx,null);
			pDr = null;
			return false;
		}
	}

	public bool SetManDataSetByUserSeq(string pUserSeq,int pRNum) {
		int iIdx;
		iIdx = ViCommConst.DATASET_MAN;
		DataSet dsDataSet;
		ParseHTML oParseWoman = this.parseContainer;

		using (Man oMan = new Man()) {
			dsDataSet = oMan.GetOne(site.siteCd,pUserSeq,pRNum);
		}
		if (dsDataSet.Tables[0].Rows.Count > 0) {
			oParseWoman.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
			oParseWoman.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
			oParseWoman.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
			return true;
		} else {
			oParseWoman.currentRecPos[iIdx] = -1;
			oParseWoman.parseUser.SetDataRow(iIdx,null);
			return false;
		}
	}

	public bool SetMailDataSetByMailSeq(string pMailSeq) {
		int iIdx;
		iIdx = ViCommConst.DATASET_MAIL;
		DataSet dsDataSet;
		ParseHTML oParseWoman = this.parseContainer;

		using (MailBox oMailBox = new MailBox()) {
			dsDataSet = oMailBox.GetPageCollectionBySeq(pMailSeq,site.siteCd,userWoman.userSeq,userWoman.curCharNo,ViCommConst.OPERATOR,ViCommConst.MAIL_BOX_USER,ViCommConst.RX);
		}
		if (dsDataSet.Tables[0].Rows.Count > 0) {
			oParseWoman.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
			oParseWoman.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
			oParseWoman.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
			return true;
		} else {
			oParseWoman.currentRecPos[iIdx] = -1;
			oParseWoman.parseUser.SetDataRow(iIdx,null);
			return false;
		}
	}

	public bool SetMailDataSetByRecentMail(string pPartnerLoginId) {
		int iIdx;
		iIdx = ViCommConst.DATASET_MAIL;
		DataSet dsDataSet;
		ParseHTML oParseWoman = this.parseContainer;

		using (MailBox oMailBox = new MailBox()) {
			dsDataSet = oMailBox.GetPageCollectionBase(site.siteCd,userWoman.userSeq,userWoman.curCharNo,pPartnerLoginId,ViCommConst.MAIN_CHAR_NO,ViCommConst.OPERATOR,ViCommConst.MAIL_BOX_USER,ViCommConst.RX,true,string.Empty,null,null,null,true,null,null,null,1,1);
		}
		if (dsDataSet.Tables[0].Rows.Count > 0) {
			oParseWoman.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
			oParseWoman.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
			oParseWoman.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
			return true;
		} else {
			oParseWoman.currentRecPos[iIdx] = -1;
			oParseWoman.parseUser.SetDataRow(iIdx,null);
			return false;
		}
	}

	public bool ControlList(
		HttpRequest pRequest,
		ulong pMode,
		Form pActiveForm
	) {
		DataSet ds;
		bool bRet = base.ControlList(pRequest,pMode,pActiveForm,out ds);
		return bRet;
	}

	protected override DataSet CreateListDataSet(HttpRequest pRequest,ulong pMode,out int pCurrentPage,out int pRowCountOfPage,out int pMaxPage) {
		seekConditionEx = null;
		pCurrentPage = 0;
		pRowCountOfPage = 0;
		pMaxPage = 1;
		string sFindType = string.Empty;
		string picType = string.Empty;
		string movieType = string.Empty;
		string sLoginId = iBridUtil.GetStringValue(pRequest.QueryString["loginid"]);
		string sPartnerId = iBridUtil.GetStringValue(pRequest.QueryString["partnerid"]);
		string sAttrTypeSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]);
		string sObjSeq,sPrevObjSeq,sNextObjSeq;
		string sOrder = iBridUtil.GetStringValue(pRequest.QueryString["order"]);
		string sOrderAsc = iBridUtil.GetStringValue(pRequest.QueryString["orderasc"]);
		string sOrderDesc = iBridUtil.GetStringValue(pRequest.QueryString["orderdesc"]);
		string sPrevUserSeq = iBridUtil.GetStringValue(pRequest.QueryString["prevuserseq"]);
		string sPrevCharNo = iBridUtil.GetStringValue(pRequest.QueryString["prevcharno"]);
		string sSortType = iBridUtil.GetStringValue(pRequest.QueryString["sorttype"]);
		bool pIncludeNotAprroveFlag = false;

		if (int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["pageno"]),out pCurrentPage) == false) {
			switch (currentAspx) {
				case "ViewMessage.aspx":
					int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),out pCurrentPage);
					break;
				default:
					int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),out pCurrentPage);
					break;
			}
			if (pCurrentPage == 0) {
				pCurrentPage = 1;
			}
		}

		if (iBridUtil.GetStringValue(pRequest.QueryString["list"]).Equals("1")) {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["lrows"]),out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileWomanListCount;
			}
		} else {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["drows"]),out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileWomanDetailListCount;
			}
		}

		parseContainer.parseUser.isExistPrevRec[parseContainer.parseUser.currentTableIdx] = false;
		parseContainer.parseUser.isExistNextRec[parseContainer.parseUser.currentTableIdx] = false;

		DataSet ds = null;
		double dwPages;
		int iRNum = pCurrentPage;

		#region □ 拡張機能関連処理 □ ------------------------------------------------------------------------------------------
		if (pMode >= ViCommConst.INQUIRY_EXTENSION && pMode <= ViCommConst.INQUIRY_EXTENSION_END) {
			switch (pMode) {
				case ViCommConst.INQUIRY_EXTENSION_GAME: {
						GameSeekCondition oCondition = new GameSeekCondition(pRequest.Params);
						oCondition.SeekMode = pMode;

						oCondition.SiteCd = site.siteCd;
						oCondition.SexCd = sexCd;
						oCondition.UserSeq = userWoman.userSeq;
						oCondition.UserCharNo = userWoman.curCharNo;
						using (Game oGame = new Game()) {
							pMaxPage = oGame.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oGame.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
					}
					break;
				case ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ: {
						BlogObjSeekCondition oCondition = new BlogObjSeekCondition(pRequest.QueryString);
						oCondition.SeekMode = pMode;
						oCondition.SiteCd = site.siteCd;
						oCondition.UseTemplate = false;
						switch (currentAspx) {
							case "ListBlogPicConf.aspx":
								oCondition.FileType = ViCommConst.BlogFileType.PIC;
								break;
							case "ListBlogMovieConf.aspx":
								oCondition.FileType = ViCommConst.BlogFileType.MOVIE;
								break;
						}

						switch (currentAspx) {
							case "ModifyBlogArticle.aspx":
								oCondition.UserSeq = userWoman.userSeq;
								oCondition.UserCharNo = userWoman.curCharNo;
								oCondition.Sort = "OBJ_SEQ ASC";
								break;
							case "ListBlogPicConf.aspx":
							case "ListBlogMovieConf.aspx":
								oCondition.UserSeq = userWoman.userSeq;
								oCondition.UserCharNo = userWoman.curCharNo;
								oCondition.UsedFlag = ViCommConst.FLAG_ON_STR;
								break;
						}

						// ページング前処理








						switch (sOrder) {
							case ViCommConst.ORDER_PREVIOUS:
								oCondition.ObjSeq = oCondition.PrevSeq;
								break;
							case ViCommConst.ORDER_NEXT:
								oCondition.ObjSeq = oCondition.NextSeq;
								break;
						}
						oCondition.PrevSeq = string.Empty;
						oCondition.NextSeq = string.Empty;

						using (BlogObj oBlogObj = new BlogObj()) {
							pMaxPage = oBlogObj.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oBlogObj.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}

						// ページング後処理








						if (!string.IsNullOrEmpty(oCondition.ObjSeq) && ds.Tables[0].Rows.Count > 0) {
							DataRow oDataRow = ds.Tables[0].Rows[0];
							oCondition.PrevSeq = iBridUtil.GetStringValue(oDataRow["PREV_SEQ"]);
							oCondition.NextSeq = iBridUtil.GetStringValue(oDataRow["NEXT_SEQ"]);
							pCurrentPage = int.Parse(iBridUtil.GetStringValue(oDataRow["RNUM"]));
							parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_EX_BLOG_OBJ] = !oCondition.PrevSeq.Equals(string.Empty);
							parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_EX_BLOG_OBJ] = !oCondition.NextSeq.Equals(string.Empty);
						}

						seekConditionEx = oCondition;
					}
					break;
				case ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE: {
						BlogArticleSeekCondition oCondition = new BlogArticleSeekCondition(pRequest.QueryString);
						oCondition.SeekMode = pMode;
						oCondition.SiteCd = site.siteCd;
						oCondition.UseTemplate = false;
						switch (currentAspx) {
							case "ListBlogArticleConf.aspx":
							case "ViewBlogArticleConf.aspx":
								oCondition.UserSeq = userWoman.userSeq;
								oCondition.UserCharNo = userWoman.curCharNo;
								break;
						}

						switch (sOrder) {
							case ViCommConst.ORDER_PREVIOUS:
								oCondition.BlogArticleSeq = oCondition.PrevSeq;
								break;
							case ViCommConst.ORDER_NEXT:
								oCondition.BlogArticleSeq = oCondition.NextSeq;
								break;
						}
						oCondition.PrevSeq = string.Empty;
						oCondition.NextSeq = string.Empty;

						using (BlogArticle oBlogArticle = new BlogArticle(this)) {
							pMaxPage = oBlogArticle.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oBlogArticle.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}

						if (!string.IsNullOrEmpty(oCondition.BlogArticleSeq) && (!string.IsNullOrEmpty(oCondition.BlogSeq) || !string.IsNullOrEmpty(oCondition.UserSeq)) && ds.Tables[0].Rows.Count > 0) {
							DataRow oDataRow = ds.Tables[0].Rows[0];
							oCondition.PrevSeq = iBridUtil.GetStringValue(oDataRow["PREV_SEQ"]);
							oCondition.NextSeq = iBridUtil.GetStringValue(oDataRow["NEXT_SEQ"]);
							pCurrentPage = int.Parse(iBridUtil.GetStringValue(oDataRow["RNUM"]));
							parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_EX_BLOG_ARTICLE] = !oCondition.PrevSeq.Equals(string.Empty);
							parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_EX_BLOG_ARTICLE] = !oCondition.NextSeq.Equals(string.Empty);
						}


						seekConditionEx = oCondition;
					}
					break;
				case ViCommConst.INQUIRY_EXTENSION_BEAUTY_CLOCK: {
						BeautyClockSeekCondition oCondition = new BeautyClockSeekCondition(pRequest.QueryString);
						oCondition.SiteCd = site.siteCd;
						oCondition.SeekMode = pMode;
						oCondition.UseTemplate = true;
						using (BeautyClock oBeautyClock = new BeautyClock()) {
							pMaxPage = oBeautyClock.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
							ds = oBeautyClock.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
						}
						seekConditionEx = oCondition;
					}
					break;
				case ViCommConst.INQUIRY_EXTENSION_INTRODUCER_FRIEND:
					{
						IntroducerFriendSeekCondition oCondition = new IntroducerFriendSeekCondition(pRequest.QueryString);
						oCondition.SiteCd = site.siteCd;
						oCondition.UserSeq = this.userWoman.userSeq;
						oCondition.UserCharNo = this.userWoman.curCharNo;
						oCondition.IntroducerFriendCd = this.userWoman.CurCharacter.friendIntroCd;
						oCondition.SeekMode = pMode;
						switch (currentAspx) {
							case "ListCastIntroducerFriend.aspx": {
									using (CastIntroducerFriend oCastIntroducerFriend = new CastIntroducerFriend()) {
										pMaxPage = oCastIntroducerFriend.GetPageCount(oCondition, pRowCountOfPage, out totalRowCount);
										ds = oCastIntroducerFriend.GetPageCollection(oCondition, pCurrentPage, pRowCountOfPage);
									}
									seekConditionEx = oCondition;
								}
								break;
							case "ListManIntroducerFriend.aspx": {
									using (ManIntroducerFriend oManIntroducerFriend = new ManIntroducerFriend()) {
										pMaxPage = oManIntroducerFriend.GetPageCount(oCondition, pRowCountOfPage, out totalRowCount);
										ds = oManIntroducerFriend.GetPageCollection(oCondition, pCurrentPage, pRowCountOfPage);
									}
									seekConditionEx = oCondition;
								}
								break;
							default:
								break;
						}
					}
					break;
			}
		}
		#endregion --------------------------------------------------------------------------------------------------------------
		if (pMode >= ViCommConst.INQUIRY_EXTENSION_PWILD && pMode <= ViCommConst.INQUIRY_EXTENSION_PWILD_END) {
			return CreateListDataSetEx(pRequest,pMode,out pCurrentPage,out pRowCountOfPage,out pMaxPage);
		}
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		seekCondition.listDisplay = iBridUtil.GetStringValue(pRequest.QueryString["list"]);
		switch (currentAspx) {

			case "SelectSite.aspx":
				ds = userWoman.GetCharacterList("");
				dwPages = ds.Tables[0].Rows.Count / (double)pRowCountOfPage;
				pMaxPage = (int)Math.Ceiling(dwPages);
				totalRowCount = ds.Tables[0].Rows.Count;
				break;

			case "ListUserBbs.aspx":
				seekCondition.InitCondtition();
				ExtendSeekCondition(seekCondition,pRequest);

				using (UserManBbs oUserBbs = new UserManBbs()) {
					pMaxPage = oUserBbs.GetPageCount(site.siteCd,sLoginId,ViCommConst.MAIN_CHAR_NO,pRowCountOfPage,seekCondition,out totalRowCount);
					ds = oUserBbs.GetPageCollection(site.siteCd,sLoginId,ViCommConst.MAIN_CHAR_NO,iRNum,pRowCountOfPage,seekCondition,this.userWoman.userSeq,this.userWoman.curCharNo);
				}
				break;

			case "ViewUserBbs.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["bbsseq"]);
				using (UserManBbs oUserBbs = new UserManBbs()) {
					// Next/Previousガイダンスのためレコード位置再取得 
					if (this.enableViewPageControl) {
						oUserBbs.GetPrevNextBbsSeq(sObjSeq,site.siteCd,"","",out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sObjSeq = sPrevObjSeq;
							} else {
								sObjSeq = sNextObjSeq;
							}
							oUserBbs.GetPrevNextBbsSeq(sObjSeq,site.siteCd,"","",out sPrevObjSeq,out sNextObjSeq);
						}
					}
					ds = oUserBbs.GetPageCollectionBySeq(sObjSeq);
				}
				parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_PARTNER_BBS_DOC] = !sPrevObjSeq.Equals("");
				parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_PARTNER_BBS_DOC] = !sNextObjSeq.Equals("");
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListCastBbsConf.aspx":
				using (CastBbs oCastBbs = new CastBbs()) {
					pMaxPage = oCastBbs.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,pRowCountOfPage,false,out totalRowCount);
					ds = oCastBbs.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,iRNum,pRowCountOfPage,false);
				}
				break;

			case "ViewCastBbsConf.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["bbsseq"]);
				using (CastBbs oCastBbs = new CastBbs()) {
					// Next/Previousガイダンスのためレコード位置再取得 
					if (this.enableViewPageControl) {
						oCastBbs.GetPrevNextBbsSeq(sObjSeq,site.siteCd,userWoman.loginId,userWoman.curCharNo,false,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sObjSeq = sPrevObjSeq;
							} else {
								sObjSeq = sNextObjSeq;
							}
							oCastBbs.GetPrevNextBbsSeq(sObjSeq,site.siteCd,userWoman.loginId,userWoman.curCharNo,false,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					ds = oCastBbs.GetPageCollectionBySeq(sObjSeq,false);
				}
				parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_SELF_BBS_DOC] = !sPrevObjSeq.Equals("");
				parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_SELF_BBS_DOC] = !sNextObjSeq.Equals("");
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListAdminReport.aspx":
				using (AdminReport oAdminReport = new AdminReport()) {
					pMaxPage = oAdminReport.GetPageCount(site.siteCd,ViCommConst.OPERATOR,pRowCountOfPage,out totalRowCount);
					ds = oAdminReport.GetPageCollection(site.siteCd,ViCommConst.OPERATOR,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ViewAdminReport.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["docseq"]);
				using (AdminReport oAdminReport = new AdminReport()) {
					// Next/Previousガイダンスのためレコード位置再取得 
					if (this.enableViewPageControl) {
						oAdminReport.GetPrevNextDocSeq(sObjSeq,site.siteCd,ViCommConst.OPERATOR,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sObjSeq = sPrevObjSeq;
							} else {
								sObjSeq = sNextObjSeq;
							}
							oAdminReport.GetPrevNextDocSeq(sObjSeq,site.siteCd,ViCommConst.OPERATOR,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					ds = oAdminReport.GetPageCollectionBySeq(sObjSeq);
				}
				parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_ADMIN_REPORT] = !sPrevObjSeq.Equals("");
				parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_ADMIN_REPORT] = !sNextObjSeq.Equals("");
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListPersonalProfileMovie.aspx":
				using (CastMovie oCastMovie = new CastMovie()) {
					pMaxPage = oCastMovie.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,"",true,ViCommConst.ATTACHED_PROFILE.ToString(),"","",pRowCountOfPage,false,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,"",true,ViCommConst.ATTACHED_PROFILE.ToString(),"","",ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,false);
				}
				break;

			case "ViewPersonalProfileMovie.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				using (CastMovie oCastMovie = new CastMovie()) {
					sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["movieseq"]);
					// Next/Previousガイダンスのためレコード位置再取得 
					if (this.enableViewPageControl) {
						oCastMovie.GetPrevNextMovieSeq(site.siteCd,userWoman.loginId,userWoman.curCharNo,sObjSeq,true,false,ViCommConst.ATTACHED_PROFILE.ToString(),"","","","",false,ViCommConst.SORT_NO,false,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sObjSeq = sPrevObjSeq;
							} else {
								sObjSeq = sNextObjSeq;
							}
							oCastMovie.GetPrevNextMovieSeq(site.siteCd,userWoman.loginId,userWoman.curCharNo,sObjSeq,true,false,ViCommConst.ATTACHED_PROFILE.ToString(),"","","","",false,ViCommConst.SORT_NO,false,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					ds = oCastMovie.GetPageCollectionBySeq(sObjSeq,false);
				}
				parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_SELF_MOVIE] = !sPrevObjSeq.Equals("");
				parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_SELF_MOVIE] = !sNextObjSeq.Equals("");
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListGalleryPicConf.aspx":
				picType = ViCommConst.ATTACHED_PROFILE.ToString();
				pRowCountOfPage = 9;
				using (CastPic oCastPic = new CastPic()) {
					pMaxPage = oCastPic.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,true,picType,"",sAttrTypeSeq,sAttrSeq,true,false,pRowCountOfPage,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,true,picType,"",sAttrTypeSeq,sAttrSeq,true,false,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ViewGalleryPicConf.aspx":
				picType = ViCommConst.ATTACHED_PROFILE.ToString();
				using (CastPic oCastPic = new CastPic()) {
					ds = oCastPic.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["picseq"]));
				}
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;


			case "ListProfilePicConf.aspx": {
					picType = ViCommConst.ATTACHED_PROFILE.ToString();
					bool bWithoutHide = iBridUtil.GetStringValue(pRequest.QueryString["withouthide"]).Equals(ViCommConst.FLAG_ON_STR);
					bool bHideonly = iBridUtil.GetStringValue(pRequest.QueryString["hideonly"]).Equals(ViCommConst.FLAG_ON_STR);
					if (bHideonly) {
						picType = ViCommConst.ATTACHED_HIDE.ToString();
					}

					string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);
					string sPfNotApproved = ViCommConst.FLAG_ON_STR;
					using (CastPic oCastPic = new CastPic()) {
						pMaxPage = oCastPic.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,picType,"",sAttrTypeSeq,sAttrSeq,!bWithoutHide,false,sPfNotApproved,pRowCountOfPage,out totalRowCount);
						ds = oCastPic.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,picType,"",sAttrTypeSeq,sAttrSeq,!bWithoutHide,false,sPfNotApproved,pCurrentPage,pRowCountOfPage);
					}
				}
				break;

			case "ViewProfilePicConf.aspx":
				picType = ViCommConst.ATTACHED_PROFILE.ToString();
				using (CastPic oCastPic = new CastPic()) {
					ds = oCastPic.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["picseq"]));
				}
				pRowCountOfPage = 1;
				pMaxPage = ds.Tables[0].Rows.Count;
				break;

			case "ListUserMailBox.aspx":
			case "ListInfoMailBox.aspx":
			case "ListTxMailBox.aspx":
			case "ListMailHistory.aspx":
				using (MailBox oMailBox = new MailBox()) {
					string sMailSearchKey = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["search_key"]));
					string sMailReadFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["read_flag"]));
					string sMailReturnFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["return_flag"]));
					string sDelProtectFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["prt_flg"]));
					string sExistObjFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["obj_flag"]));
					string sTxOnlyFlag = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["tx_only"]));
					
					int? iMailReadFlag = null;
					int? iMailReturnFlag = null;
					if (!sMailReadFlag.Equals("")) {
						iMailReadFlag = int.Parse(sMailReadFlag);
					}
					if (!sMailReturnFlag.Equals("")) {
						iMailReturnFlag = int.Parse(sMailReturnFlag);
					}
					string sMailBoxType = "";
					string sTxRxType = "";
					bool bWithBatchMail = iBridUtil.GetStringValue(pRequest.QueryString["withbatch"]).Equals("1");
					switch (currentAspx) {
						case "ListUserMailBox.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.RX;

							// 3分以内メール返信ボーナス イベント期間内
							if (ViCommConst.FLAG_ON_STR.Equals(userWoman.characterList[userWoman.curKey].characterEx.mailResBonusStatus)) {
								oMailBox.exParam.mailResBonusSeq = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusSeq;
								oMailBox.exParam.mailResBonusLimitSec1 = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec1;
								oMailBox.exParam.mailResBonusLimitSec2 = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec2;
								oMailBox.exParam.mailResBonusMaxNum = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusMaxNum;
								oMailBox.exParam.mailResBonusUnreceivedDays = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusUnreceivedDays;
							}
							break;
						case "ListInfoMailBox.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_INFO;
							sTxRxType = ViCommConst.RX;
							break;
						case "ListTxMailBox.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TX;
							break;
						case "ListMailHistory.aspx":
							seekCondition.partnerLoginId = "";
							seekCondition.handleNm = iBridUtil.GetStringValue(pRequest.QueryString["handle"]);
							sMailSearchKey = string.Empty;
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TXRX;
							break;
					}
					pMaxPage = oMailBox.GetPageCount(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sexCd,sMailBoxType,sTxRxType,bWithBatchMail,sMailSearchKey,iMailReadFlag,iMailReturnFlag,sDelProtectFlag,sExistObjFlag,sTxOnlyFlag,pRowCountOfPage,out totalRowCount);
					ds = oMailBox.GetPageCollection(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sexCd,sMailBoxType,sTxRxType,bWithBatchMail,sMailSearchKey,iMailReadFlag,iMailReturnFlag,true,sDelProtectFlag,sExistObjFlag,sTxOnlyFlag,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ViewUserMail.aspx":
			case "ViewInfoMail.aspx":
			case "ViewTxMail.aspx":
			case "ViewMailHistory.aspx":
				sPrevObjSeq = "";
				sNextObjSeq = "";
				using (MailBox oMailBox = new MailBox()) {
					string sMailSeq = iBridUtil.GetStringValue(pRequest.QueryString["mailseq"]);
					string sMailSearchKey = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["search_key"]));
					bool bWithBatchMail = iBridUtil.GetStringValue(pRequest.QueryString["withbatch"]).Equals("1");
					string sMailBoxType = "";
					string sTxRxType = "";
					switch (currentAspx) {
						case "ViewUserMail.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.RX;
							break;
						case "ViewInfoMail.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_INFO;
							sTxRxType = ViCommConst.RX;
							break;
						case "ViewTxMail.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TX;
							break;
						case "ViewMailHistory.aspx":
							sMailBoxType = ViCommConst.MAIL_BOX_USER;
							sTxRxType = ViCommConst.TXRX;
							break;
					}
					if (this.enableViewPageControl) {
						oMailBox.GetPrevNextMailSeq(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sexCd,sMailBoxType,sTxRxType,bWithBatchMail,sMailSeq,out sPrevObjSeq,out sNextObjSeq);
						if (!sOrder.Equals(string.Empty)) {
							if (sOrder == ViCommConst.ORDER_PREVIOUS) {
								sMailSeq = sPrevObjSeq;
							} else {
								sMailSeq = sNextObjSeq;
							}
							oMailBox.GetPrevNextMailSeq(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sexCd,sMailBoxType,sTxRxType,bWithBatchMail,sMailSeq,out sPrevObjSeq,out sNextObjSeq);
						}
					}
					parseContainer.parseUser.isExistPrevRec[ViCommConst.DATASET_MAIL] = !sPrevObjSeq.Equals("");
					parseContainer.parseUser.isExistNextRec[ViCommConst.DATASET_MAIL] = !sNextObjSeq.Equals("");
					pRowCountOfPage = 1;
					ds = oMailBox.GetPageCollectionBySeq(sMailSeq,site.siteCd,userWoman.userSeq,userWoman.curCharNo,ViCommConst.OPERATOR,sMailBoxType,sTxRxType);
					pMaxPage = ds.Tables[0].Rows.Count;
					//					pMaxPage = oMailBox.GetPageCount(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sexCd,sMailBoxType,sTxRxType,bWithBatchMail,sMailSearchKey,pRowCountOfPage,out totalRowCount);
				}
				break;


			case "ListPersonalDiary.aspx":
			case "ViewDiary.aspx":
			case "WriteDiary.aspx":
				seekCondition.InitCondtition();
				string sHasPic = iBridUtil.GetStringValue(pRequest.QueryString["haspic"]);
				seekCondition.hasPic = sHasPic;
				seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
				if (currentAspx.Equals("ViewDiary.aspx") || currentAspx.Equals("WriteDiary.aspx")) {
					pRowCountOfPage = 1;
				}
				bool bDiarySwitchScreenFlag = false;
				using (ManageCompany oManageCompany = new ManageCompany()) {
					bDiarySwitchScreenFlag = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DIARY_SWITCH_SCREEN,2);
				}
				using (CastDiary oCastDiary = new CastDiary()) {
					pMaxPage = oCastDiary.GetPageCountByLoginId(site.siteCd,userWoman.loginId,userWoman.curCharNo,false,bDiarySwitchScreenFlag,seekCondition,pRowCountOfPage,false,out totalRowCount);
					ds = oCastDiary.GetPageCollectionByLoginId(site.siteCd,userWoman.loginId,userWoman.curCharNo,false,bDiarySwitchScreenFlag,seekCondition,pCurrentPage,pRowCountOfPage,false);
				}
				break;

			case "ListPersonalDiaryLike.aspx": {
					CastDiaryLikeSeekCondition oCondition = new CastDiaryLikeSeekCondition(pRequest.Params);
					oCondition.SiteCd = site.siteCd;
					oCondition.UserSeq = userWoman.userSeq;
					oCondition.UserCharNo = userWoman.curCharNo;
					using (CastDiaryLike oCastDiaryLike = new CastDiaryLike()) {
						pMaxPage = oCastDiaryLike.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
						ds = oCastDiaryLike.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					}
					seekConditionEx = oCondition;
				}
				break;

			case "ListMessage.aspx":
			case "ViewMessage.aspx":
				using (Message oMessage = new Message()) {
					if (currentAspx.Equals("ViewMessage.aspx")) {
						pRowCountOfPage = 1;
					}
					pMaxPage = oMessage.GetPageCount(site.siteCd,ViCommConst.OPERATOR,pRowCountOfPage,out totalRowCount);
					ds = oMessage.GetPageCollection(site.siteCd,ViCommConst.OPERATOR,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListWrittenMessage.aspx":
				using (Message oMessage = new Message()) {
					pMaxPage = oMessage.GetWrittenMessagePageCount(site.siteCd,ViCommConst.OPERATOR,userWoman.userSeq,userWoman.curCharNo,pRowCountOfPage,out totalRowCount);
					ds = oMessage.GetWrittenMessagePageCollection(site.siteCd,ViCommConst.OPERATOR,userWoman.userSeq,userWoman.curCharNo,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListBbsPicConf.aspx": {
					seekCondition.InitCondtition();
					seekCondition.sortType = sSortType;
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					picType = ViCommConst.ATTACHED_BBS.ToString();
					string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);
					using (CastPic oCastPic = new CastPic()) {
						pMaxPage = oCastPic.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,picType,"",sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,out totalRowCount);
						ds = oCastPic.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,picType,"",sAttrTypeSeq,sAttrSeq,seekCondition,pCurrentPage,pRowCountOfPage);
					}
				}
				break;

			case "ViewBbsPicConf.aspx": {
					pRowCountOfPage = 1;
					sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
					seekCondition.InitCondtition();
					seekCondition.sortType = sSortType;
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					picType = ViCommConst.ATTACHED_BBS.ToString();
					string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);

					using (CastPic oCastPic = new CastPic()) {
						if (string.IsNullOrEmpty(sObjSeq)) {
							pMaxPage = oCastPic.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,picType,"",sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,out totalRowCount);
							ds = oCastPic.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,picType,"",sAttrTypeSeq,sAttrSeq,seekCondition,pCurrentPage,pRowCountOfPage);
						} else {
							ds = oCastPic.GetPageCollectionBySeq(sObjSeq);
							pMaxPage = ds.Tables[0].Rows.Count;
						}
					}
				}
				break;

			case "ListBbsMovieConf.aspx": {
					seekCondition.InitCondtition();
					seekCondition.sortType = sSortType;
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					movieType = ViCommConst.ATTACHED_BBS.ToString();
					string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);
					if (currentAspx.Equals("ViewBbsMovieConf.aspx")) {
						pRowCountOfPage = 1;
					}
					using (CastMovie oCastMovie = new CastMovie()) {
						pMaxPage = oCastMovie.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,"",sUnAuth,movieType,"",sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,false,out totalRowCount);
						ds = oCastMovie.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,"",sUnAuth,movieType,"",sAttrTypeSeq,sAttrSeq,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,false);
					}
				}
				break;

			case "ViewBbsMovieConf.aspx": {
					pRowCountOfPage = 1;
					sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
					seekCondition.InitCondtition();
					seekCondition.sortType = sSortType;
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					movieType = ViCommConst.ATTACHED_BBS.ToString();
					string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);

					using (CastMovie oCastMovie = new CastMovie()) {
						if (string.IsNullOrEmpty(sObjSeq)) {
							pMaxPage = oCastMovie.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,"",sUnAuth,movieType,"",sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,false,out totalRowCount);
							ds = oCastMovie.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,"",sUnAuth,movieType,"",sAttrTypeSeq,sAttrSeq,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,false);
						} else {
							ds = oCastMovie.GetPageCollectionBySeq(sObjSeq,false);
							pMaxPage = ds.Tables[0].Rows.Count;
						}
					}
				}
				break;

			case "ListBonusLog.aspx":
				using (BonusLog oBonusLog = new BonusLog()) {
					pMaxPage = oBonusLog.GetPageCount(userWoman.userSeq,pRowCountOfPage,out totalRowCount);
					ds = oBonusLog.GetPageCollection(userWoman.userSeq,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListBatchMailLog.aspx":
			case "ViewBatchMailLog.aspx":
				if (currentAspx.Equals("ViewBatchMailLog.aspx")) {
					pRowCountOfPage = 1;
				}
				using (BatchMail oBatchMail = new BatchMail()) {
					pMaxPage = oBatchMail.GetPageCount(site.siteCd,userWoman.userSeq,userWoman.curCharNo,pRowCountOfPage,out totalRowCount);
					ds = oBatchMail.GetPageCollection(site.siteCd,userWoman.userSeq,userWoman.curCharNo,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListManageBatchMail.aspx":
				using (BatchMail oBatchMail = new BatchMail()) {
					pMaxPage = oBatchMail.GetPageCountByBatchManage(site.siteCd,userWoman.userSeq,userWoman.curCharNo,pRowCountOfPage,out totalRowCount);
					ds = oBatchMail.GetPageCollectionByBatchManage(site.siteCd,userWoman.userSeq,userWoman.curCharNo,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListTalkHistory.aspx":
			case "ListRequestHistory.aspx":
				if (pMode.Equals(ViCommConst.INQUIRY_TALK_HISTORY)) {
					sFindType = ViCommConst.FIND_CALL_RESULT_GROUP_MAN;
				} else {
					sFindType = userWoman.talkRequestFindType;
				}
				using (ManTalkHistory oTalkHistory = new ManTalkHistory()) {
					pMaxPage = oTalkHistory.GetPageCount(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sFindType,pRowCountOfPage,out totalRowCount);
					ds = oTalkHistory.GetPageCollection(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sFindType,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListBbsObjConf.aspx": {
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);

					using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
						pMaxPage = oCastBbsObj.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,false,string.Empty,sAttrTypeSeq,sAttrSeq,false,false,seekCondition,pRowCountOfPage,false,out totalRowCount);
						ds = oCastBbsObj.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,false,string.Empty,sAttrTypeSeq,sAttrSeq,false,false,seekCondition,pCurrentPage,pRowCountOfPage,false);
					}
				}
				break;

			case "ViewBbsObjConf.aspx": {
					pRowCountOfPage = 1;
					sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
					seekCondition.InitCondtition();
					seekCondition.screenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
					string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);

					using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
						if (string.IsNullOrEmpty(sObjSeq)) {
							pMaxPage = oCastBbsObj.GetPageCount(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,false,string.Empty,sAttrTypeSeq,sAttrSeq,false,false,seekCondition,pRowCountOfPage,false,out totalRowCount);
							ds = oCastBbsObj.GetPageCollection(site.siteCd,userWoman.loginId,userWoman.curCharNo,sUnAuth,false,string.Empty,sAttrTypeSeq,sAttrSeq,false,false,seekCondition,pCurrentPage,pRowCountOfPage,false);
						} else {
							ds = oCastBbsObj.GetPageCollectionBySeq(sObjSeq,false,false);
							pMaxPage = ds.Tables[0].Rows.Count;
						}
					}
				}
				break;

			case "ListPaymentHistory.aspx":
				using (PaymentHistory oPaymentHistory = new PaymentHistory()) {
					pMaxPage = oPaymentHistory.GetPageCount(userWoman.userSeq,pRowCountOfPage,out totalRowCount);
					ds = oPaymentHistory.GetPageCollection(userWoman.userSeq,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListBbsViewHistory.aspx":
				using (BbsUsedHistory oBbsUsedHistory = new BbsUsedHistory()) {
					sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
					pMaxPage = oBbsUsedHistory.GetPageCount(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sObjSeq,pRowCountOfPage,out totalRowCount);
					ds = oBbsUsedHistory.GetPageCollection(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sLoginId,ViCommConst.MAIN_CHAR_NO,sObjSeq,sOrderAsc,sOrderDesc,pCurrentPage,pRowCountOfPage);
				}
				break;

			case "ListMarkingRanking.aspx": {

					seekCondition.InitCondtition();
					seekCondition.siteCd = site.siteCd;
					seekCondition.conditionFlag = pMode;
					seekCondition.newCastDay = 0;
					seekCondition.userRank = string.Empty;
					seekCondition.random = ViCommConst.FLAG_OFF;
					seekCondition.listDisplay = iBridUtil.GetStringValue(pRequest.QueryString["list"]);
					seekCondition.orderAsc = iBridUtil.GetStringValue(pRequest.QueryString["orderasc"]);
					seekCondition.orderDesc = iBridUtil.GetStringValue(pRequest.QueryString["orderdesc"]);
					seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
					seekCondition.rankingCtlSeq = iBridUtil.GetStringValue(pRequest.QueryString["ctlseq"]);
					if (seekCondition.rankingCtlSeq.Equals(string.Empty)) {
						using (RankingCtl oRankingCtl = new RankingCtl()) {
							if (iBridUtil.GetStringValue(requestQuery.QueryString["new"]).Equals("1")) {
								seekCondition.rankingCtlSeq = oRankingCtl.GetNowSeq(site.siteCd,ViCommConst.ExRanking.EX_RANKING_MARKING);
							} else {
								seekCondition.rankingCtlSeq = oRankingCtl.GetOldSeq(site.siteCd,ViCommConst.ExRanking.EX_RANKING_MARKING);
							}
						}
					}
					using (Cast oCast = new Cast()) {
						pMaxPage = oCast.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						ds = oCast.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage,false);
					}
				}
				break;

			case "ListMoteCon.aspx": {

					seekCondition.InitCondtition();
					seekCondition.siteCd = site.siteCd;
					seekCondition.conditionFlag = pMode;
					seekCondition.newCastDay = 0;
					seekCondition.userRank = string.Empty;
					seekCondition.random = ViCommConst.FLAG_OFF;
					seekCondition.listDisplay = iBridUtil.GetStringValue(pRequest.QueryString["list"]);
					seekCondition.orderAsc = iBridUtil.GetStringValue(pRequest.QueryString["orderasc"]);
					seekCondition.orderDesc = iBridUtil.GetStringValue(pRequest.QueryString["orderdesc"]);
					seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
					seekCondition.rankingCtlSeq = iBridUtil.GetStringValue(pRequest.QueryString["ctlseq"]);
					if (seekCondition.rankingCtlSeq.Equals(string.Empty)) {
						using (RankingCtl oRankingCtl = new RankingCtl()) {
							seekCondition.rankingCtlSeq = oRankingCtl.GetLastSeq(site.siteCd,ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN);
						}
					}
					using (Cast oCast = new Cast()) {
						pMaxPage = oCast.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						ds = oCast.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage,false);
					}
				}
				break;

			case "ListBbsThread.aspx":
				seekCondition.InitCondtition();
				string sBbsThreadSearchAdminType = ViCommConst.BbsThreadSearchAdminType.ADMIN_FLAG_IS_FALSE;
				if (!iBridUtil.GetStringValue(requestQuery.QueryString["searchadmintype"]).Equals(string.Empty)) {
					sBbsThreadSearchAdminType = iBridUtil.GetStringValue(requestQuery.QueryString["searchadmintype"]);
				}
				string sBbsThreadSearchKey = HttpUtility.UrlDecode(iBridUtil.GetStringValue(requestQuery.QueryString["searchKey"]),enc);

				using (BbsThread oBbsThread = new BbsThread()) {
					pMaxPage = oBbsThread.GetPageCount(site.siteCd,string.Empty,ViCommConst.BbsThreadSearchAdminType.ADMIN_FLAG_IS_FALSE,sBbsThreadSearchKey,pRowCountOfPage,out totalRowCount);
					ds = oBbsThread.GetPageCollection(site.siteCd,string.Empty,ViCommConst.BbsThreadSearchAdminType.ADMIN_FLAG_IS_FALSE,iBridUtil.GetStringValue(requestQuery.QueryString["sort"]),sBbsThreadSearchKey,iRNum,pRowCountOfPage);
				}
				break;

			case "DeleteBbsThread.aspx":
			case "ModifyBbsThread.aspx":
			case "ConfirmModifyBbsThread.aspx":
			case "WriteBbsRes.aspx":
			case "ConfirmWriteBbsRes.aspx":
				pRowCountOfPage = 1;
				using (BbsThread oBbsThread = new BbsThread()) {
					pMaxPage = oBbsThread.GetPageCount(site.siteCd,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),ViCommConst.BbsThreadSearchAdminType.ALL,string.Empty,pRowCountOfPage,out totalRowCount);
					ds = oBbsThread.GetPageCollection(site.siteCd,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),ViCommConst.BbsThreadSearchAdminType.ALL,iBridUtil.GetStringValue(requestQuery.QueryString["sort"]),string.Empty,iRNum,pRowCountOfPage);
				}
				break;

			case "ListBbsRes.aspx":
				seekCondition.InitCondtition();
				string sResNo = iBridUtil.GetStringValue(requestQuery.QueryString["resno"]);
				string sBbsResSearchKey = HttpUtility.UrlDecode(iBridUtil.GetStringValue(requestQuery.QueryString["searchKey"]),enc);

				using (BbsRes oBbsRes = new BbsRes()) {
					pMaxPage = oBbsRes.GetPageCount(site.siteCd,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),iBridUtil.GetStringValue(requestQuery.QueryString["subseq"]),sBbsResSearchKey,pRowCountOfPage,out totalRowCount);
					if (!sResNo.Equals(string.Empty)) {
						pCurrentPage = (int)Math.Ceiling(oBbsRes.GetRNum(site.siteCd,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),sResNo,iBridUtil.GetStringValue(requestQuery.QueryString["sort"])) / (double)pRowCountOfPage);
						iRNum = pCurrentPage;
					}
					ds = oBbsRes.GetPageCollection(site.siteCd,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),iBridUtil.GetStringValue(requestQuery.QueryString["subseq"]),iBridUtil.GetStringValue(requestQuery.QueryString["sort"]),sBbsResSearchKey,iRNum,pRowCountOfPage);
				}
				break;

			case "WriteBbsReturnRes.aspx":
			case "ConfirmWriteBbsReturnRes.aspx":
				pRowCountOfPage = 1;
				using (BbsRes oBbsRes = new BbsRes()) {
					pMaxPage = oBbsRes.GetPageCount(site.siteCd,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),iBridUtil.GetStringValue(requestQuery.QueryString["subseq"]),string.Empty,pRowCountOfPage,out totalRowCount);
					ds = oBbsRes.GetPageCollection(site.siteCd,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),iBridUtil.GetStringValue(requestQuery.QueryString["subseq"]),iBridUtil.GetStringValue(requestQuery.QueryString["sort"]),string.Empty,iRNum,pRowCountOfPage);
				}
				break;

			case "ListBingoEntryWinner.aspx":
			case "ViewBingoCard.aspx":
				string sBingoTermSeq = iBridUtil.GetStringValue(requestQuery.QueryString["bingotermseq"]);
				seekCondition.InitCondtition();
				seekCondition.siteCd = site.siteCd;
				if (sBingoTermSeq.Equals(string.Empty)) {
					using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
						seekCondition.bingoTermSeq = oMailDeBingo.GetCurrentTermSeq(site.siteCd,ViCommConst.OPERATOR,DateTime.Now);
					}
				} else {
					seekCondition.bingoTermSeq = sBingoTermSeq;
				}
				seekCondition.conditionFlag = pMode;
				using (Cast oCast = new Cast()) {
					pMaxPage = oCast.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
					ds = oCast.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
				}
				break;
			case "ListPickup.aspx":
				if (pMode.Equals(ViCommConst.INQUIRY_PICKUP_MOVIE)) {
					seekCondition.InitCondtition();
					seekCondition.pickupId = iBridUtil.GetStringValue(pRequest.QueryString["pickupid"]);
					seekCondition.sortType = sSortType;
					using (CastMovie oCastMovie = new CastMovie()) {
						pMaxPage = oCastMovie.GetPageCount(site.siteCd,string.Empty,string.Empty,string.Empty,false,string.Empty,string.Empty,sAttrTypeSeq,sAttrSeq,false,seekCondition,pRowCountOfPage,false,out totalRowCount);
						ds = oCastMovie.GetPageCollection(site.siteCd,string.Empty,string.Empty,string.Empty,false,string.Empty,string.Empty,sAttrTypeSeq,sAttrSeq,false,seekCondition,sSortType,iRNum,pRowCountOfPage,false);
					}
				}
				break;
			default:
				seekCondition.InitCondtition();
				seekCondition.siteCd = site.siteCd;
				seekCondition.userSeq = userWoman.userSeq;
				seekCondition.userCharNo = userWoman.curCharNo;
				seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
				seekCondition.conditionFlag = pMode;
				seekCondition.newCastDay = 0;
				seekCondition.newManDay = 0;
				seekCondition.orderAsc = iBridUtil.GetStringValue(pRequest.QueryString["orderasc"]);
				seekCondition.orderDesc = iBridUtil.GetStringValue(pRequest.QueryString["orderdesc"]);
				seekCondition = ExtendSeekCondition(seekCondition,pRequest);
				seekCondition.mailSendFlag = iBridUtil.GetStringValue(pRequest.QueryString["mailsend"]);
				seekCondition.mailSendCount = iBridUtil.GetStringValue(pRequest.QueryString["mailsendcnt"]);
				seekCondition.noTxMailDays = iBridUtil.GetStringValue(pRequest.QueryString["notxmaildays"]);

				bool bMoveProfile = false;

				// ProfileのNext/Previousの場合、元ﾍﾟｰｼﾞのUSER_SEQ,USER_CHAR_NO,+αでRNUMを再度算出する
				if (currentAspx.Equals("Profile.aspx")) {
					pRowCountOfPage = 1;
					if (!sPrevUserSeq.Equals("")) {
						bMoveProfile = true;
					}
				}

				if (currentCategoryIndex >= 0) {
					seekCondition.categorySeq = ((ActCategory)categoryList[currentCategoryIndex]).actCategorySeq;
				}


				Man oMan = userWoman.characterList[userWoman.curKey].manCharacter;

				switch (pMode) {
					case ViCommConst.INQUIRY_ONLINE:
						pMaxPage = oMan.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = oMan.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = oMan.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					case ViCommConst.INQUIRY_FAVORIT:
					case ViCommConst.INQUIRY_LIKE_ME:
					case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
					case ViCommConst.INQUIRY_WAITING_MAN:
					case ViCommConst.INQUIRY_REFUSE:
					case ViCommConst.INQUIRY_REFUSE_ME:
					case ViCommConst.INQUIRY_MAN_NEW:
					case ViCommConst.INQUIRY_MAN_LOGINED:
					case ViCommConst.INQUIRY_MAN_LOGINED_NEW:
					case ViCommConst.INQUIRY_MEMO:
					case ViCommConst.INQUIRY_MAN_ADD_POINT:
					case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
					case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
					case ViCommConst.INQUIRY_EXTENSION_RICHINO_USER:
						if (currentAspx.Equals("UserTop.aspx")) {
							int iDrows = int.MinValue;
							string sDrows = iBridUtil.GetStringValue(site.GetSupplement(ViCommConst.SITE_WOMAN_TOP_MANNEW_DROWS));
							if (!int.TryParse(sDrows,out iDrows)) {
								iDrows = 1;
							}
							pRowCountOfPage = iDrows;
						}

						if ((pMode == ViCommConst.INQUIRY_MAN_NEW) || (pMode == ViCommConst.INQUIRY_MAN_LOGINED_NEW)) {
							seekCondition.newManDay = site.manNewFaceDays;
						}
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
						if (pMode == ViCommConst.INQUIRY_WAITING_MAN) {
							seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
						}
						if (pMode == ViCommConst.INQUIRY_MAN_LOGINED || pMode == ViCommConst.INQUIRY_MAN_LOGINED_NEW) {
							seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
						}
						if (pMode == ViCommConst.INQUIRY_EXTENSION_RICHINO_USER) {
							seekCondition.richinoRank = iBridUtil.GetStringValue(requestQuery.QueryString["richinorank"]);
						}

						if (pMode.Equals(ViCommConst.INQUIRY_FAVORIT)) {
							seekCondition.favoritGroupSeq = iBridUtil.GetStringValue(pRequest.QueryString["grpseq"]);
						}

						pMaxPage = oMan.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);

						switch (pMode) {
							case ViCommConst.INQUIRY_FAVORIT:
							case ViCommConst.INQUIRY_LIKE_ME:
							case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
							case ViCommConst.INQUIRY_MAN_LOGINED:
								seekCondition.sortExType = iBridUtil.GetStringValue(pRequest.QueryString["sortex"]);
								break;
						}

						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = oMan.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = oMan.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					// List内に複数回同一ｷｬｽﾄが出現する
					case ViCommConst.INQUIRY_MARKING:
						seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
						pMaxPage = oMan.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = oMan.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = oMan.GetRNumByUserSeqAndCondtion(
											seekCondition,
											sPrevUserSeq,
											sPrevCharNo,
											iBridUtil.GetStringValue(pRequest["conditem"]),
											iBridUtil.GetStringValue(pRequest["condvalue"]));
						}
						break;

					case ViCommConst.INQUIRY_CONDITION:
						Int64 iPlayMask;
						Int64.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["play"]),out iPlayMask);
						seekCondition.handleNm = iBridUtil.GetStringValue(pRequest.QueryString["handle"]);
						int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["online"]),out seekCondition.onlineStatus);
						seekCondition.playMask = iPlayMask;
						seekCondition.ageFrom = iBridUtil.GetStringValue(pRequest.QueryString["agefrom"]);
						seekCondition.ageTo = iBridUtil.GetStringValue(pRequest.QueryString["ageto"]);
						seekCondition.birthDayFrom = iBridUtil.GetStringValue(pRequest.QueryString["birthfrom"]);
						seekCondition.birthDayTo = iBridUtil.GetStringValue(pRequest.QueryString["birthto"]);
						seekCondition.registDateFrom = iBridUtil.GetStringValue(pRequest.QueryString["registfrom"]);
						seekCondition.registDateTo = iBridUtil.GetStringValue(pRequest.QueryString["registto"]);
						seekCondition.lastLoginDate = iBridUtil.GetStringValue(pRequest.QueryString["lastlogin"]);
						seekCondition.targetMailMax = iBridUtil.GetStringValue(pRequest.QueryString["target_mail_max"]);
						seekCondition.targetFlag = iBridUtil.GetStringValue(pRequest.QueryString["targetflag"]);
						SetAttrParam(seekCondition,pRequest);
						if (iBridUtil.GetStringValue(pRequest.QueryString["newman"]).Equals(ViCommConst.FLAG_ON_STR)) {
							seekCondition.newManDay = site.manNewFaceDays;
						}

						pMaxPage = oMan.GetPageCount(seekCondition,pRowCountOfPage,out totalRowCount);
						if (bMoveProfile == false) {
							if (sLoginId.Equals("")) {
								ds = oMan.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							}
						} else {
							pCurrentPage = oMan.GetRNumByUserSeq(seekCondition,sPrevUserSeq,sPrevCharNo);
						}
						break;

					/*------------------------------------------------------*/
					/* Profile表示制御										*/
					/*------------------------------------------------------*/
					// List内に複数回同一ｷｬｽﾄが出現しかつ、ﾒｲﾝﾙｰﾌﾟがMANではない 
					/*
					case ViCommConst.SEL_BBS_DOC:
						using (UserManBbs oUserBbs = new UserManBbs()) {
							pPageCnt = oUserBbs.GetPageCountByLoginId(site.siteCd,"","",pLineCnt,out totalRowCount);
							if (sLoginId.Equals("") && !iBridUtil.GetStringValue(pRequest["condvalue"]).Equals("")) {
								pRNum = oUserBbs.GetRNum(iBridUtil.GetStringValue(pRequest["condvalue"]));
							}
						}
						break;

					case ViCommConst.SEL_TX_USER_MAIL_BOX:
					case ViCommConst.SEL_RX_USER_MAIL_BOX:
					case ViCommConst.SEL_MAIL_HISTORY:
						// Profile.aspxに引渡す条件が複雑であるため、今ﾊﾞｰｼﾞｮﾝは1Profileのみ表示 
						break;
					 */

				}

				if (bMoveProfile) {
					// 前へか次へなのかによってRNUMを変更します。  
					if (iBridUtil.GetStringValue(pRequest.QueryString["order"]) == ViCommConst.ORDER_PREVIOUS) {
						pCurrentPage--;
						if (pCurrentPage <= 0) {
							pCurrentPage = 1;
						}
					} else {
						pCurrentPage++;
						if (totalRowCount < pCurrentPage) {
							pCurrentPage = (int)totalRowCount;
						}
					}

					DataSet dsSub;
					DataRow drSub;
					ds = null;

					switch (pMode) {
						case ViCommConst.INQUIRY_BBS_DOC:
							using (UserManBbs oUserBbs = new UserManBbs()) {
								dsSub = oUserBbs.GetPageCollection(site.siteCd,"","",pCurrentPage,pRowCountOfPage);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = oMan.GetOne(site.siteCd,drSub["USER_SEQ"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_TALK_HISTORY:
						case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_ALL:
						case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_GROUP_MAN:
						case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_SUCCESS:
							if (pMode.Equals(ViCommConst.INQUIRY_TALK_HISTORY)) {
								sFindType = ViCommConst.FIND_CALL_RESULT_GROUP_MAN;
							} else {
								sFindType = userWoman.talkRequestFindType;
							}
							using (ManTalkHistory oTalkHistory = new ManTalkHistory()) {
								dsSub = oTalkHistory.GetPageCollection(site.siteCd,userWoman.userSeq,userWoman.curCharNo,string.Empty,string.Empty,sFindType,pCurrentPage,pRowCountOfPage);
								if (dsSub.Tables[0].Rows.Count > 0) {
									drSub = dsSub.Tables[0].Rows[0];
									ds = oMan.GetOne(site.siteCd,drSub["PARTNER_USER_SEQ"].ToString(),pCurrentPage);
									ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = drSub[iBridUtil.GetStringValue(pRequest["conditem"])].ToString();
								}
							}
							break;

						case ViCommConst.INQUIRY_TX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_RX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_MAIL_HISTORY:
							// Profile.aspxに引渡す条件が複雑であるため、今ﾊﾞｰｼﾞｮﾝは1Profileのみ表示
							break;

						default:
							ds = oMan.GetPageCollection(seekCondition,pCurrentPage,pRowCountOfPage);
							break;
					}
					if (ds == null) {
						ds = oMan.GetOneByLoginId(site.siteCd,sPrevUserSeq,1);
					}
				} else if (!sLoginId.Equals("")) {
					switch (pMode) {
						case ViCommConst.INQUIRY_DIRECT:
							ds = oMan.GetOneByLoginId(site.siteCd,sLoginId,1);
							pMaxPage = 1;
							totalRowCount = ds.Tables[0].Rows.Count;
							break;

						// ManがMain Loopでない検索からのProfile表示
						case ViCommConst.INQUIRY_BBS_DOC:
						case ViCommConst.INQUIRY_MAIL_HISTORY:
						case ViCommConst.INQUIRY_TX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_RX_USER_MAIL_BOX:
						case ViCommConst.INQUIRY_BBS_VIEW_HISTORY:
							ds = oMan.GetOneByLoginId(site.siteCd,sLoginId,pCurrentPage);
							if (!iBridUtil.GetStringValue(pRequest["conditem"]).Equals("") && !iBridUtil.GetStringValue(pRequest["condvalue"]).Equals("")) {
								ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = iBridUtil.GetStringValue(pRequest["condvalue"]);
							}
							break;

						case ViCommConst.INQUIRY_TALK_HISTORY:
						case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_GROUP_MAN:
							ds = oMan.GetOneByLoginId(site.siteCd,sLoginId,pCurrentPage);
							if (!iBridUtil.GetStringValue(pRequest["conditem"]).Equals("") && !iBridUtil.GetStringValue(pRequest["condvalue"]).Equals("")) {
								ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = iBridUtil.GetStringValue(pRequest["condvalue"]);
							}
							break;

						case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_ALL:
						case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_SUCCESS:
							if (seekCondition.partnerLoginId.Equals("") == false || sPartnerId.Equals("") == false) {
								// Profileからの通話履歴照会の場合、同一男性ﾕｰｻﾞｰが連続するため 
								// Next,Previousは不用
								pCurrentPage = 1;
								pMaxPage = 1;
							}
							ds = oMan.GetOneByLoginId(site.siteCd,sLoginId,pCurrentPage);
							if (!iBridUtil.GetStringValue(pRequest["conditem"]).Equals("") && !iBridUtil.GetStringValue(pRequest["condvalue"]).Equals("")) {
								ds.Tables[0].Rows[0][iBridUtil.GetStringValue(pRequest["conditem"])] = iBridUtil.GetStringValue(pRequest["condvalue"]);
							}
							break;

						default:
							string sUserSeq = oMan.GetUserSeqByLoginId(sLoginId);
							ds = oMan.GetOneByUserSeqAndCondtion(
											seekCondition,
											sUserSeq,
											ViCommConst.MAIN_CHAR_NO,
											iBridUtil.GetStringValue(pRequest["conditem"]),
											iBridUtil.GetStringValue(pRequest["condvalue"]),
											pCurrentPage);
							totalRowCount = ds.Tables[0].Rows.Count;
							break;
					}
				}
				break;

		}
		return ds;
	}

	public DataSet SearchManListBatch(int iOnlineFlag,int iNewFaceFlag,string sHandleNm,string sAgeFrom,string sAgeTo,string sBirthdayFrom,string sBirthdayTo,List<string> valueList,List<string> typeList,List<string> groupList,List<string> likeList,List<string> notEqualList,int pLimitCnt,string pDevice,string pMailSendCount,out decimal pTotalRowCount) {
		// データセット検索処理 
		SeekCondition seekCondition = new SeekCondition();
		seekCondition.userSeq = userWoman.userSeq;
		seekCondition.userCharNo = userWoman.curCharNo;
		seekCondition.siteCd = site.siteCd;
		seekCondition.conditionFlag = ViCommConst.INQUIRY_CONDITION;
		seekCondition.notSendBatchMailFlag = ViCommConst.FLAG_ON;

		seekCondition.handleNm = sHandleNm;
		seekCondition.ageFrom = sAgeFrom;
		seekCondition.ageTo = sAgeTo;
		seekCondition.birthDayFrom = sBirthdayFrom;
		seekCondition.birthDayTo = sBirthdayTo;
		seekCondition.onlineStatus = iOnlineFlag;
		seekCondition.findFavoriteType = ViCommConst.FIND_ALL;
		seekCondition.device = pDevice;
		seekCondition.random = ViCommConst.FLAG_ON;

		if (pMailSendCount.Equals("0")) {
			seekCondition.mailSendFlag = ViCommConst.FLAG_OFF_STR;
		} else {
			seekCondition.mailSendCount = pMailSendCount;
		}

		if (iNewFaceFlag == 1) {
			seekCondition.newManDay = site.manNewFaceDays;
		}

		for (int i = 0;i < valueList.Count;i++) {
			seekCondition.attrTypeSeq.Add(typeList[i]);
			seekCondition.attrValue.Add(valueList[i]);
			seekCondition.groupingFlag.Add(groupList[i]);
			seekCondition.likeSearchFlag.Add(likeList[i]);
			seekCondition.notEqualFlag.Add(notEqualList[i]);
			seekCondition.rangeFlag.Add("0");
			seekCondition.seqRangeFlag.Add("0");
		}

		DataSet ds = new DataSet();

		using (Man oMan = new Man()) {
			if (iBridUtil.GetStringValue(System.Configuration.ConfigurationManager.AppSettings["EnableSelectBatchMail"]).Equals(ViCommConst.FLAG_ON_STR)) {
				ds = oMan.GetPageCollectionBatchMail(seekCondition,1,(int)pLimitCnt);
			} else {
				ds = oMan.GetListBatchMail(seekCondition,(int)pLimitCnt);
			}
		}

		pTotalRowCount = ds.Tables[0].Rows.Count;

		return ds;
	}


	public bool ContorIVP(
		MobilePage pPage,
		HttpRequest pRequest
	) {
		string sRequestId = iBridUtil.GetStringValue(pRequest.QueryString["request"]);
		string sMenuId = iBridUtil.GetStringValue(pRequest.QueryString["menuid"]);
		string sVicommMenuId = iBridUtil.GetStringValue(pRequest.QueryString["menuid"]);
		string sUserSeq = iBridUtil.GetStringValue(pRequest.QueryString["userseq"]);
		string sIvpResult;
		string sResult;
		bool bRequest = false;

		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

		errorMessage = "";

		if (!CheckTalkBalance(sUserSeq,userWoman.userSeq,sCastCharNo,sRequestId,sMenuId)) {
			errorMessage = ViCommConst.ERR_STATUS_MAN_NO_BALANCE;
		} else {
			sResult = ivpRequest.Request(ref sRequestId,ViCommConst.OPERATOR,sUserSeq,userWoman.userSeq,sCastCharNo,sMenuId,"","","","","","","");
			sIvpResult = ivpRequest.interfaceResult;

			if (!sResult.Equals(ViCommConst.SP_RESULT_OK)) {
				switch (sResult) {
					case ViCommConst.SP_RESULT_NO_TEL:
						errorMessage = ViCommConst.ERR_STATUS_IVP_REQ_NO_TEL;
						break;

					default:
						errorMessage = ViCommConst.ERR_STATUS_MAN_BUSY;
						break;
				}
			} else {
				bRequest = true;
			}
		}

		if (!errorMessage.Equals("")) {
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				errorMessage = string.Format("{0}<br />",oErrorDtl.GetErrorDtl(site.siteCd,errorMessage));
			}
		}
		return bRequest;
	}

	private bool CheckTalkBalance(string pUserSeq,string pCastSeq,string pCastCharNo,string pRequestId,string pMenuId) {
		int iUnitSec = 0,iPoint = 0;
		int iLimitPoint = 0;
		int iBalPoint = 0;

		using (UserMan oUserMan = new UserMan()) {
			if (oUserMan.GetCurrentInfo(site.siteCd,pUserSeq) == false) {
				return false;
			}
			iBalPoint = oUserMan.balPoint;

			if (site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
				iLimitPoint = oUserMan.limitPoint;
			}
			if (pMenuId.Equals(ViCommConst.MENU_VIDEO_WTALK)) {
				oUserMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_CAST_TALK_WSHOT,userWoman.characterList[userWoman.curKey].actCategorySeq,out iUnitSec,out iPoint);

			} else if (pMenuId.Equals(ViCommConst.MENU_VIDEO_PTALK)) {
				oUserMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_CAST_TALK_PUBLIC,userWoman.characterList[userWoman.curKey].actCategorySeq,out iUnitSec,out iPoint);

			} else if (pMenuId.Equals(ViCommConst.MENU_VOICE_WTALK)) {
				oUserMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT,userWoman.characterList[userWoman.curKey].actCategorySeq,out iUnitSec,out iPoint);

			} else if (pMenuId.Equals(ViCommConst.MENU_VOICE_PTALK)) {
				oUserMan.GetCornerCharge(site.siteCd,pCastSeq,ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC,userWoman.characterList[userWoman.curKey].actCategorySeq,out iUnitSec,out iPoint);
			}
		}

		return ((iBalPoint + iLimitPoint) >= iPoint);
	}

	public string GetManValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_MAN);
	}

	public string GetMailValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_MAIL);
	}

	public void SetMailValue(string pFieldNm,string pValue) {
		SetRowValue(pFieldNm,pValue,ViCommConst.DATASET_MAIL);
	}

	public int GetMailCount() {
		return GetRowCount(ViCommConst.DATASET_MAIL);
	}

	public string GetSiteValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_CAST_REGISTED_SITE_LIST);
	}

	public int GetSiteCount() {
		return GetRowCount(ViCommConst.DATASET_CAST_REGISTED_SITE_LIST);
	}

	public string GetBbsMovieValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_MOVIE);
	}

	public string GetBbsPicValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_PIC);
	}

	public string GetBbsDocValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_BBS_DOC);
	}

	public string GetManBbsDocValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_PARTNER_BBS_DOC);
	}

	public string GetDiaryValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_DIARY);
	}
	public string GetPicValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_PIC);
	}
	public string GetMailMoiveValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_MOVIE);
	}
	public string GetMoiveValue(string pFieldNm) {
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_MOVIE);
	}
	public string GetBbsObjValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_SELF_BBS_OBJ,out bExist);
	}
	public string GetBlogObjValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_EX_BLOG_OBJ,out bExist);
	}
	public string GetBlogArticleValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_EX_BLOG_ARTICLE,out bExist);
	}
	public string GetBbsResValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_EX_BBS_RES,out bExist);
	}
	public string GetBbsThreadValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,ViCommConst.DATASET_EX_BBS_THREAD,out bExist);
	}

	protected virtual SeekCondition ExtendSeekCondition(SeekCondition pSeekCondtion,HttpRequest pRequest) {

		string sQueryHandle = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_handle"]));
		string sQueryAgeRange = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_agerange"]));
		string sQueryUserStatus = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_userstatus"]));
		string sQueryCommunication = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_communication"]));
		string sQueryWithoutBatchMail = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pRequest.QueryString["ext_without_batchmail"]));

		// ハンドル名 
		if (!string.IsNullOrEmpty(sQueryHandle)) {
			pSeekCondtion.handleNm = sQueryHandle;
			pSeekCondtion.hasExtend = true;
		}
		// 年齢 
		if (!string.IsNullOrEmpty(sQueryAgeRange)) {
			if (!sQueryAgeRange.Equals(":")) {
				string[] ageRangeFromTo = sQueryAgeRange.Split(':');
				pSeekCondtion.ageFrom = ageRangeFromTo[0];
				pSeekCondtion.ageTo = (ageRangeFromTo.Length > 1) ? ageRangeFromTo[1] : string.Empty;
				pSeekCondtion.hasExtend = true;
			}
		}
		// ｵﾝﾗｲﾝ/新人 
		if (!string.IsNullOrEmpty(sQueryUserStatus)) {
			switch (sQueryUserStatus) {
				case "1":
					seekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
					pSeekCondtion.hasExtend = true;
					break;
				case "2":
					pSeekCondtion.newManDay = site.newFaceDays;
					pSeekCondtion.hasExtend = true;
					break;
			}
		}
		// ｷｬｽﾄ属性 
		using (UserManAttrType oUserManAttrType = new UserManAttrType())
		using (UserManAttrTypeValue oUserManAttrTypeValue = new UserManAttrTypeValue()) {

			foreach (string sKey in pRequest.QueryString.AllKeys) {
				if (sKey == null)
					continue;

				if (!sKey.StartsWith("ext_item"))
					continue;

				string sItemNo = sKey.Replace("ext_item",string.Empty);
				string sValue = HttpUtility.UrlDecode(pRequest.QueryString[sKey]);
				if (string.IsNullOrEmpty(sValue))
					continue;

				if (sValue.Equals(":") || sValue.Equals("*"))
					continue;


				string[] sAttrItemNoArry = sValue.Split(':');

				string sAttrTypeSeq = oUserManAttrType.GetSeqByItemNo(this.site.siteCd,sItemNo);
				if (string.IsNullOrEmpty(sAttrTypeSeq))
					continue;

				System.Text.StringBuilder oSb = new System.Text.StringBuilder();
				for (int i = 0;i < sAttrItemNoArry.Length;i++) {
					if (i != 0)
						oSb.Append(",");
					oSb.Append(oUserManAttrTypeValue.GetSeqByItemNo(this.site.siteCd,sAttrTypeSeq,sAttrItemNoArry[i]));
				}

				pSeekCondtion.attrTypeSeq.Add(sAttrTypeSeq);
				pSeekCondtion.attrValue.Add(oSb.ToString());
				pSeekCondtion.groupingFlag.Add("0");
				pSeekCondtion.likeSearchFlag.Add("0");
				pSeekCondtion.notEqualFlag.Add("0");
				pSeekCondtion.rangeFlag.Add("0");
				pSeekCondtion.seqRangeFlag.Add("0");

				pSeekCondtion.hasExtend = true;
			}
		}

		if (!string.IsNullOrEmpty(sQueryCommunication)) {
			pSeekCondtion.hasCommunication = sQueryCommunication.Equals(ViCommConst.FLAG_ON_STR);
			pSeekCondtion.hasExtend = true;
		}

		if (!string.IsNullOrEmpty(sQueryWithoutBatchMail)) {
			if (sQueryWithoutBatchMail.Equals(ViCommConst.FLAG_ON_STR)) {
				pSeekCondtion.withoutBatchMail = true;
				pSeekCondtion.hasExtend = true;
			}
		}

		return pSeekCondtion;
	}

	private void SetAttrParam(SeekCondition pCondition,HttpRequest pRequest) {
		for (int i = 0;i < ViCommConst.MAX_ATTR_COUNT;i++) {
			pCondition.attrTypeSeq.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("typeseq{0:D2}",i + 1)]));
			pCondition.attrValue.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("item{0:D2}",i + 1)]));
			pCondition.groupingFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("group{0:D2}",i + 1)]));
			pCondition.likeSearchFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("like{0:D2}",i + 1)]));
			pCondition.notEqualFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("noteq{0:D2}",i + 1)]));
			pCondition.rangeFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("range{0:D2}",i + 1)]));
			pCondition.seqRangeFlag.Add(iBridUtil.GetStringValue(pRequest.QueryString[string.Format("seqrange{0:D2}",i + 1)]));
		}
	}

	private string GetAdGroupCd() {
		string sAdGroupCd = ViCommConst.DEFAUL_AD_GROUP_CD;
		int iGroupType = 0;
		if (userWoman.characterList.ContainsKey(userWoman.curKey)) {
			UserWomanCharacter oWomanCharacter = userWoman.characterList[userWoman.curKey];
			if (!string.IsNullOrEmpty(oWomanCharacter.adCd)) {
				this.SetAdInfo(oWomanCharacter.adCd);
				using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
					oSiteAdGroup.GetAdGroupCd(oWomanCharacter.siteCd,oWomanCharacter.adCd,out sAdGroupCd,out iGroupType);
				}
			}
		}

		return sAdGroupCd;
	}
}

