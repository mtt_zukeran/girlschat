﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾒｯｾｰｼﾞ

--	Progaram ID		: Message
--  Creation Date	: 2010.06.22
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

/// <summary>
/// T_MESSAGEテーブルへのデータアクセスを提供するクラス
/// </summary>
[System.Serializable]
public class Message : DbSession {

	public Message(){}

	/// <summary>
	/// ﾒｯｾｰｼﾞに書き込みを行います。

	/// </summary>
	/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
	/// <param name="pSexCd">性別</param>
	/// <param name="pUserSeq">ﾕｰｻﾞｰSEQ</param>
	/// <param name="pUserCharNo">ﾕｰｻﾞｰCharNo</param>
	/// <param name="pPartnerUserSeq">相手ﾕｰｻﾞｰSEQ</param>
	/// <param name="pPartnerUserCharNo">相手ﾕｰｻﾞｰCharNo</param>
	/// <param name="pMessageDoc">ﾒｯｾｰｼﾞ本文</param>
	public void WriteMessage(string pSiteCd, string pSexCd, string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pMessageDoc) {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("WRITE_MESSAGE");
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ", DbSession.DbType.VARCHAR2, pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO", DbSession.DbType.VARCHAR2, pPartnerUserCharNo);
			db.ProcedureInParm("pMESSAGE_DOC", DbSession.DbType.VARCHAR2, pMessageDoc);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			
			db.ExecuteProcedure();
		}
	}

	#region □■□ ListMessage □■□ ===========================================================================================

	public int GetPageCount(string pSiteCd,string pSexCd,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try{
			conn = DbConnect("Message.GetPageCount");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT ";
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sSql += " FROM VW_MESSAGE01 ";
			} else {
				sSql += " FROM VW_MESSAGE02 ";
			}
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int pPageNo,int pRecPerPage)
	{
		DataSet ds;
		try{
			conn = DbConnect("Message.GetPageCollection");
			ds = new DataSet();

			string sOrder = "ORDER BY CREATE_DATE DESC,MESSAGE_SEQ DESC ";
			string sFrom;
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sFrom = " FROM VW_MESSAGE01 MESSAGE ";
			} else {
				sFrom = " FROM VW_MESSAGE02 MESSAGE ";
			}

			string sSql = "SELECT * FROM " +
							"(" +
							"SELECT " +
								"MESSAGE.*		," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							sFrom;

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd, pSexCd, ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW", (pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW", pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "VW_MESSAGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,ref string pWhere)
	{
		ArrayList list = new ArrayList();
		pWhere = " WHERE SITE_CD = :SITE_CD ";
		pWhere += " AND  SEX_CD  = :SEX_CD ";
		
		list.Add(new OracleParameter("SITE_CD", pSiteCd));
		list.Add(new OracleParameter("SEX_CD",pSexCd));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

    #endregion ===================================================================================================================

	#region □■□ ListWrittenMessage □■□ =====================================================================================

	public int GetWrittenMessagePageCount(string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try{
			conn = DbConnect("Message.GetPageCount");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT ";
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sSql += " FROM VW_MESSAGE01 ";
			} else {
				sSql += " FROM VW_MESSAGE02 ";
			}
			string sWhere = "";

			OracleParameter[] objParms = CreateWrittenMessageWhere(pSiteCd, pSexCd, pUserSeq, pUserCharNo, ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}


	public DataSet GetWrittenMessagePageCollection(string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try{
			conn = DbConnect("Message.GetWrittenMessagePageCollection");
			ds = new DataSet();

			string sOrder = "ORDER BY CREATE_DATE DESC,MESSAGE_SEQ DESC ";
			string sFrom;
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sFrom = " FROM VW_MESSAGE01 MESSAGE ";
			} else {
				sFrom = " FROM VW_MESSAGE02 MESSAGE ";
			}

			string sSql = "SELECT * FROM " +
							"(" +
							"SELECT " +
								"MESSAGE.*		," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							sFrom;

			string sWhere = "";

			OracleParameter[] objParms = CreateWrittenMessageWhere(pSiteCd,pSexCd,pUserSeq,pUserCharNo,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW", (pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW", pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "VW_MESSAGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	private OracleParameter[] CreateWrittenMessageWhere(string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,ref string pWhere) {

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.Append("WHERE").AppendLine();
		sqlBuilder.Append("     SITE_CD      = :SITE_CD ").AppendLine();
		sqlBuilder.Append(" AND SEX_CD       = :SEX_CD ").AppendLine();
		sqlBuilder.Append(" AND USER_SEQ     = :USER_SEQ ").AppendLine();
		sqlBuilder.Append(" AND USER_CHAR_NO = :USER_CHAR_NO ").AppendLine();
		pWhere = sqlBuilder.ToString();

		ArrayList list = new ArrayList();
		list.Add(new OracleParameter("SITE_CD", pSiteCd));
		list.Add(new OracleParameter("SEX_CD",pSexCd));
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("USER_CHAR_NO", pUserCharNo));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	#endregion ===================================================================================================================

	public void Delete(string[] pMessageSeqArray){
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_MESSAGE");
			db.ProcedureInArrayParm("pMESSAGE_SEQ_ARRAY", DbType.VARCHAR2,pMessageSeqArray.Length, pMessageSeqArray);
			db.ProcedureOutParm("PSTATUS", DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

}
