﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: Rss取得 
--	Progaram ID		: Rss
--
--  Creation Date	: 2011.11.10
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.ServiceModel.Syndication;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Rss:DbSession {

	public Rss() {
	}

	[System.ThreadStatic]
	private static Hashtable _rss;
	[System.ThreadStatic]
	private static Hashtable _rss_LastUpdate;

	public static string GetRss(string pUrl,int pItemNo,string pItemName) {

		string sValue = string.Empty;

		bool bRefresh = false;
		if (_rss == null) {
			_rss = new Hashtable();
			_rss_LastUpdate = new Hashtable();
			_rss_LastUpdate.Add(pUrl,DateTime.Now);
			bRefresh = true;
		} else {
			if (!_rss_LastUpdate.ContainsKey(pUrl)) {
				_rss_LastUpdate.Add(pUrl,DateTime.Now);
				bRefresh = true;
			}
		}
		if ((DateTime)_rss_LastUpdate[pUrl] < DateTime.Now.AddMinutes(-10)) {
			_rss_LastUpdate[pUrl] = DateTime.Now;
			bRefresh = true;
		}

		if (bRefresh) {
			RssItems rssItem = new RssItems();

			using (XmlReader rd = XmlReader.Create(pUrl)) {
				SyndicationFeed feed = SyndicationFeed.Load(rd);
				try {
					rssItem.channel.title = feed.Title.Text;
				} catch {
					rssItem.channel.title = string.Empty;
				}

				try {
					if (feed.Links.Count > 0) {
						rssItem.channel.link = iBridUtil.GetStringValue(feed.Links[0].Uri);
					} else {
						rssItem.channel.link = string.Empty;
					}
				} catch {
					rssItem.channel.link = string.Empty;
				}

				try {
					rssItem.channel.description = feed.Description.Text;
				} catch {
					rssItem.channel.description = string.Empty;
				}

				try {
					rssItem.channel.lastBuildDate = iBridUtil.GetStringValue(feed.LastUpdatedTime);
				} catch {
					rssItem.channel.lastBuildDate = string.Empty;
				}

				try {
					if (feed.Categories.Count > 0) {
						rssItem.channel.category = iBridUtil.GetStringValue(feed.Categories[0].Label);
					} else {
						rssItem.channel.category = string.Empty;
					}
				} catch {
					rssItem.channel.category = string.Empty;
				}

				try {
					rssItem.channel.image = iBridUtil.GetStringValue(feed.ImageUrl);
				} catch {
					rssItem.channel.image = string.Empty;
				}

				foreach (SyndicationItem item in feed.Items) {
					
					try {
						rssItem.item.title.Add(item.Title.Text);
					} catch {
						rssItem.item.title.Add(string.Empty);
					}

					try {
						if (item.Links.Count > 0) {
							rssItem.item.link.Add(iBridUtil.GetStringValue(item.Links[0].Uri));
						} else {
							rssItem.item.link.Add(string.Empty);
						}
					} catch {
						rssItem.item.link.Add(string.Empty);
					}

					try {
						if (item.Summary != null) {
							rssItem.item.description.Add(iBridUtil.GetStringValue(item.Summary.Text));
						} else {
							rssItem.item.description.Add(string.Empty);
						}
					} catch {
						rssItem.item.description.Add(string.Empty);
					}

					try {
						if (item.Categories.Count > 0) {
							rssItem.item.category.Add(iBridUtil.GetStringValue(item.Categories[0].Label));
						} else {
							rssItem.item.category.Add(string.Empty);
						}
					} catch {
						rssItem.item.category.Add(string.Empty);
					}

					try {
						rssItem.item.pubDate.Add(iBridUtil.GetStringValue(item.PublishDate));
					} catch {
						rssItem.item.pubDate.Add(string.Empty);
					}
				}
			}
			_rss.Remove(pUrl);
			_rss.Add(pUrl,rssItem);
		}
		if (pItemNo < 0) {
			if (pItemName.Equals("title")) {
				sValue = ((RssItems)_rss[pUrl]).channel.title;
			} else if (pItemName.Equals("link")) {
				sValue = ((RssItems)_rss[pUrl]).channel.link;
			} else if (pItemName.Equals("description")) {
				sValue = ((RssItems)_rss[pUrl]).channel.description;
			} else if (pItemName.Equals("lastBuildDate")) {
				sValue = ((RssItems)_rss[pUrl]).channel.lastBuildDate;
			} else if (pItemName.Equals("category")) {
				sValue = ((RssItems)_rss[pUrl]).channel.category;
			} else if (pItemName.Equals("image")) {
				sValue = ((RssItems)_rss[pUrl]).channel.image;
			}
		} else {
			if (pItemName.Equals("title")) {
				sValue = ((RssItems)_rss[pUrl]).item.title[pItemNo];
			} else if (pItemName.Equals("link")) {
				sValue = ((RssItems)_rss[pUrl]).item.link[pItemNo];
			} else if (pItemName.Equals("description")) {
				sValue = ((RssItems)_rss[pUrl]).item.description[pItemNo];
			} else if (pItemName.Equals("category")) {
				sValue = ((RssItems)_rss[pUrl]).item.category[pItemNo];
			} else if (pItemName.Equals("pubDate")) {
				sValue = ((RssItems)_rss[pUrl]).item.pubDate[pItemNo];
			}
		}
		return sValue;
	}

	[System.Serializable]
	public class RssItems {
		//channel要素
		public class Channel {
			public string title;
			public string link;
			public string description;
			public string lastBuildDate;
			public string category;
			public string image;

			public Channel() {
				title = string.Empty;
				link = string.Empty;
				description = string.Empty;
				lastBuildDate = string.Empty;
				category = string.Empty;
				image = string.Empty;
			}
		}

		public class Item {
			//item要素
			public List<string> title;
			public List<string> link;
			public List<string> description;
			public List<string> category;
			public List<string> pubDate;

			public Item() {
				title = new List<string>();
				link = new List<string>();
				description = new List<string>();
				category = new List<string>();
				pubDate = new List<string>();
			}
		}

		public Channel channel;
		public Item item;

		public RssItems() {
			channel = new Channel();
			item = new Item();
		}
	}
}
