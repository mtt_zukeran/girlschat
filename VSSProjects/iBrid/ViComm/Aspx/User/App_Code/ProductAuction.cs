﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品ｵｰｸｼｮﾝ
--	Progaram ID		: ProductAuction
--
--  Creation Date	: 2011.06.20
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using System.Collections.Generic;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using ViComm;
using iBridCommLib;

[System.Serializable]
public class ProductAuction  : ProductBase {
	public ProductAuction() : base() { }
	public ProductAuction(SessionObjs pSessionObj) : base(pSessionObj) { }

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(ProductSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT	");
		oSqlBuilder.AppendLine(base.CreateCountFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(), this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oWhereParams);

				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			this.conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionBySeq(string pProductSeq) {
		ProductSeekCondition oCondtion = new ProductSeekCondition();
		oCondtion.ProductSeq = pProductSeq;
		return this.GetPageCollectionBase(oCondtion, 0, 0);
	}
	public DataSet GetPageCollection(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion, pPageNo, pRecPerPage);
	}
	private DataSet GetPageCollectionBase(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {

		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT													");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.AUCTION_START_DATE			,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.BLIND_END_DATE				,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.AUCTION_END_DATE              ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.RESERVE_AMT                   ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.MINIMUM_BID_AMT               ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.ENTRY_POINT                   ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.BID_POINT                     ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.BID_COUNT                     ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.PRODUCT_AUCTION_BID_LOG_SEQ   ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.LAST_BID_DATE                 ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.AUCTION_STATUS                ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.BLIND_ENTRY_POINT             ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.BLIND_BID_POINT               ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.MAX_BID_AMT		            ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_AUCTION.AUTO_EXTENSION_FLAG           ,   "); 
		oSqlBuilder.Append(base.CreateFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where		
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresionBase(pCondtion);

		return ExecutePageCollection(oSqlBuilder.ToString(), pCondtion, oWhereParams, sSortExpression, iStartIndex, pRecPerPage);
	}

	protected override OracleParameter[] CreateOuterProductSql(ProductSeekCondition pCondition, string pBaseSql, out string pSql) {
		SessionMan oSessionMan = (SessionMan)this.sessionObj;
	
		StringBuilder oJoinTables = new StringBuilder();
		oJoinTables.AppendLine("VW_PRODUCT_AUCTION_BID_LOG01	SUB_BID_LOG		,");
		oJoinTables.AppendLine("T_PRODUCT_AUCTION_ENTRY_LOG		SUB_ENTRY_LOG	,");

		List<OracleParameter> oTemplateParams = new List<OracleParameter>();
		StringBuilder oJoinConditions = new StringBuilder();
		oJoinConditions.AppendLine(" T1.PRODUCT_AUCTION_BID_LOG_SEQ = SUB_BID_LOG.PRODUCT_AUCTION_BID_LOG_SEQ	(+) AND ");
		oJoinConditions.AppendLine(" -- ");
		oJoinConditions.AppendLine(" T1.SITE_CD						= SUB_ENTRY_LOG.SITE_CD						(+) AND ");
		oJoinConditions.AppendLine(" T1.PRODUCT_AGENT_CD			= SUB_ENTRY_LOG.PRODUCT_AGENT_CD			(+) AND ");
		oJoinConditions.AppendLine(" T1.PRODUCT_SEQ					= SUB_ENTRY_LOG.PRODUCT_SEQ					(+) AND ");
		oJoinConditions.AppendLine(" :SUB_ENTRY_LOG_USER_SEQ		= SUB_ENTRY_LOG.USER_SEQ					(+) AND ");
		oJoinConditions.AppendLine(" :SUB_ENTRY_LOG_USER_CHAR_NO	= SUB_ENTRY_LOG.USER_CHAR_NO				(+) AND ");
		oTemplateParams.Add(new OracleParameter(":SUB_ENTRY_LOG_USER_SEQ", oSessionMan.userMan.userSeq));
		oTemplateParams.Add(new OracleParameter(":SUB_ENTRY_LOG_USER_CHAR_NO", oSessionMan.userMan.userCharNo));

		StringBuilder oOuterFields = new StringBuilder();
		oOuterFields.AppendLine(" SUB_BID_LOG.SITE_CD		AS MAX_BIDDER_SITE_CD		, ");
		oOuterFields.AppendLine(" SUB_BID_LOG.USER_SEQ		AS MAX_BIDDER_USER_SEQ		, ");
		oOuterFields.AppendLine(" SUB_BID_LOG.USER_CHAR_NO	AS MAX_BIDDER_USER_CHAR_NO	, ");
		oOuterFields.AppendLine(" SUB_BID_LOG.HANDLE_NM		AS MAX_BIDDER_HANDLE_NM		, ");
		oOuterFields.AppendLine(" -- ");
		oOuterFields.AppendLine(" DECODE(SUB_ENTRY_LOG.USER_SEQ,NULL,0,1) AS AUCTION_ENTRY_FLAG	, ");



		oTemplateParams.AddRange(this.CreateOuterProductSql(pCondition,pBaseSql, out pSql, oJoinTables.ToString(), oJoinConditions.ToString(), oOuterFields.ToString()));
		return oTemplateParams.ToArray();
	}




	private OracleParameter[] CreateWhere(ProductSeekCondition pCondition, ref string pWhereClause) {

		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 商品共通のWHERE条件を作成
		oParamList.AddRange(base.CreateWhereBase(pCondition, ref pWhereClause));

		SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.SITE_CD			= VW_PRODUCT00.SITE_CD				", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.PRODUCT_AGENT_CD	= VW_PRODUCT00.PRODUCT_AGENT_CD		", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.PRODUCT_SEQ		= VW_PRODUCT00.PRODUCT_SEQ			", ref pWhereClause);
		
		if (iBridUtil.GetStringValue(pCondition.IsEntered).Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_ENTRY_LOG.SITE_CD			= VW_PRODUCT00.SITE_CD				", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_ENTRY_LOG.PRODUCT_AGENT_CD	= VW_PRODUCT00.PRODUCT_AGENT_CD		", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_ENTRY_LOG.PRODUCT_SEQ		= VW_PRODUCT00.PRODUCT_SEQ			", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_ENTRY_LOG.SITE_CD			= :ENTRY_LOG_SITE_CD				", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_ENTRY_LOG.USER_SEQ			= :ENTRY_LOG_USER_SEQ				", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_ENTRY_LOG.USER_CHAR_NO		= :ENTRY_LOG_USER_CHAR_NO			", ref pWhereClause);
			oParamList.Add(new OracleParameter(":ENTRY_LOG_SITE_CD", pCondition.SiteCd));
			oParamList.Add(new OracleParameter(":ENTRY_LOG_USER_SEQ", pCondition.UserSeq));
			oParamList.Add(new OracleParameter(":ENTRY_LOG_USER_CHAR_NO", pCondition.UserCharNo));

		}
		if (iBridUtil.GetStringValue(pCondition.IsWin).Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_STATUS IN ( :AUCTION_STATUS1,:AUCTION_STATUS2,:AUCTION_STATUS3 )	", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_BID_LOG.PRODUCT_AUCTION_BID_LOG_SEQ	= T_PRODUCT_AUCTION.PRODUCT_AUCTION_BID_LOG_SEQ	", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_BID_LOG.SITE_CD			= :BID_LOG_SITE_CD			", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_BID_LOG.USER_SEQ			= :BID_LOG_USER_SEQ			", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION_BID_LOG.USER_CHAR_NO		= :BID_LOG_USER_CHAR_NO		", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_STATUS1", ViCommConst.AuctionStatus.WinBid));
			oParamList.Add(new OracleParameter(":AUCTION_STATUS2", ViCommConst.AuctionStatus.DeclinationBid));
			oParamList.Add(new OracleParameter(":AUCTION_STATUS3", ViCommConst.AuctionStatus.End));
			oParamList.Add(new OracleParameter(":BID_LOG_SITE_CD", pCondition.SiteCd));
			oParamList.Add(new OracleParameter(":BID_LOG_USER_SEQ", pCondition.UserSeq));
			oParamList.Add(new OracleParameter(":BID_LOG_USER_CHAR_NO", pCondition.UserCharNo));
		}


		if (!string.IsNullOrEmpty(pCondition.AuctionStartGreater)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_START_DATE > TO_DATE(:AUCTION_START_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_START_DATE_FROM", pCondition.AuctionStartGreater));
		} 
		if (!string.IsNullOrEmpty(pCondition.AuctionStartGreaterEq)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_START_DATE >= TO_DATE(:AUCTION_START_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_START_DATE_FROM", pCondition.AuctionStartGreaterEq));
		}
		if (!string.IsNullOrEmpty(pCondition.AuctionStartLess)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_START_DATE < TO_DATE(:AUCTION_START_DATE_TO,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_START_DATE_TO", pCondition.AuctionStartLess));
		} if (!string.IsNullOrEmpty(pCondition.AuctionStartLessEq)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_START_DATE <= TO_DATE(:AUCTION_START_DATE_TO,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_START_DATE_TO", pCondition.AuctionStartLessEq));
		}
		if (!string.IsNullOrEmpty(pCondition.AuctionEndGreater)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_END_DATE > TO_DATE(:AUCTION_END_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_END_DATE_FROM", pCondition.AuctionEndGreater));
		}
		if (!string.IsNullOrEmpty(pCondition.AuctionEndGreaterEq)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_END_DATE >= TO_DATE(:AUCTION_END_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_END_DATE_FROM", pCondition.AuctionEndGreaterEq));
		}
		if (!string.IsNullOrEmpty(pCondition.AuctionEndLess)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_END_DATE < TO_DATE(:AUCTION_END_DATE_TO,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_END_DATE_TO", pCondition.AuctionEndLess));
		}
		if (!string.IsNullOrEmpty(pCondition.AuctionEndLessEq)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.AUCTION_END_DATE <= TO_DATE(:AUCTION_END_DATE_TO,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUCTION_END_DATE_TO", pCondition.AuctionEndLessEq));
		}
		if (!string.IsNullOrEmpty(pCondition.BlindEndGreater)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.BLIND_END_DATE > TO_DATE(:BLIND_END_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLIND_END_DATE_FROM", pCondition.BlindEndGreater));
		}
		if (!string.IsNullOrEmpty(pCondition.BlindEndGreaterEq)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.BLIND_END_DATE >= TO_DATE(:BLIND_END_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLIND_END_DATE_FROM", pCondition.BlindEndGreaterEq));
		}
		if (!string.IsNullOrEmpty(pCondition.BlindEndLess)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.BLIND_END_DATE < TO_DATE(:BLIND_END_DATE_TO,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLIND_END_DATE_TO", pCondition.BlindEndLess));
		}
		if (!string.IsNullOrEmpty(pCondition.BlindEndLessEq)) {
			SysPrograms.SqlAppendWhere(" T_PRODUCT_AUCTION.BLIND_END_DATE <= TO_DATE(:BLIND_END_DATE_TO,'YYYY/MM/DD HH24:MI:SS')	", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLIND_END_DATE_TO", pCondition.BlindEndLessEq));
		}
		return oParamList.ToArray();
	}

	private string CreateFrom(ProductSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AUCTION			,	");
		if(iBridUtil.GetStringValue(pCondition.IsEntered).Equals(ViCommConst.FLAG_ON_STR)){
			oSqlBuilder.AppendLine(" 	T_PRODUCT_AUCTION_ENTRY_LOG			,	");
		}
		if (iBridUtil.GetStringValue(pCondition.IsWin).Equals(ViCommConst.FLAG_ON_STR)) {
			oSqlBuilder.AppendLine(" 	T_PRODUCT_AUCTION_BID_LOG			,	");
		}
		
		oSqlBuilder.AppendLine(base.CreateFromBase(pCondition));
		return oSqlBuilder.ToString();
	}


	#endregion ========================================================================================================

	protected override string GetProductType(ProductSeekCondition pCondition) {
		return ProductHelper.GetAuctionProductType(pCondition.AdultFlag);
	}
	
	public string Bid(string pSiteCd,string pUserSeq,string pUserCharNo,string pProductAgentCd,string pProductSeq,string pBidAmt){
		using(DbSession oDbSession = new DbSession()){
			oDbSession.PrepareProcedure("PRODUCT_AUCTION_BID");
			oDbSession.ProcedureInParm("pSITE_CD", DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbType.NUMBER, pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbType.VARCHAR2, pUserCharNo);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbType.VARCHAR2, pProductAgentCd);
			oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbType.NUMBER, pProductSeq);
			oDbSession.ProcedureInParm("pBID_AMT", DbType.NUMBER, pBidAmt);
			oDbSession.ProcedureOutParm("pRESULT", DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS", DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			return oDbSession.GetStringValue("pRESULT");
		}
	}
}
