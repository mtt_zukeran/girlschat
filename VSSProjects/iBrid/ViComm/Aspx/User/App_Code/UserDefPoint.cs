﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using MobileLib;
using ViComm;

[System.Serializable]
public class UserDefPoint:DbSession {
	public void AddBonusPoint(string pSiteCd,string pUserSeq,string pAddPointId,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_USER_DEF_POINT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pADD_POINT_ID",DbSession.DbType.VARCHAR2,pAddPointId);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void CheckBonusPoint(string pSiteCd,string pUserSeq,string pAddPointId,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_USER_DEF_POINT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pADD_POINT_ID",DbSession.DbType.VARCHAR2,pAddPointId);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pCHECK_ONLY",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void AddRegistProfilePoint(string pSiteCd,string pUserSeq,int pCheckOnlyFlag,out int pProfilePicPointType,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_REGIST_PROFILE_POINT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCHECK_ONLY_FLAG",DbSession.DbType.NUMBER,pCheckOnlyFlag);
			db.ProcedureOutParm("pPROFILE_PIC_POINT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pProfilePicPointType = db.GetIntValue("pPROFILE_PIC_POINT_TYPE");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void AddWaitingPoint(string pSiteCd,string pUserSeq,out string pAddPointId,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_WAITING_POINT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("pADD_POINT_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pAddPointId = db.GetStringValue("PADD_POINT_ID");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public int GetPayPoint(string pSiteCd,string pAddPointId) {
		DataSet ds;
		DataRow dr;

		string sValue = "0";

		try {
			conn = DbConnect("UserDefPoint.GetPayPoint");
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("ADD_POINT	").AppendLine();
			sSql.Append("FROM T_USER_DEF_POINT ").AppendLine();
			sSql.Append("WHERE").AppendLine();
			sSql.Append("SITE_CD		= :SITE_CD			AND").AppendLine();
			sSql.Append("ADD_POINT_ID	= :ADD_POINT_ID ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ADD_POINT_ID",pAddPointId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_DEF_POINT");
					if (ds.Tables["T_USER_DEF_POINT"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sValue = dr["ADD_POINT"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return int.Parse(sValue);
	}
}
