﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者写真
--	Progaram ID		: CastBbsObj
--  Creation Date	: 2010.09.09
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Web;
using System.Configuration;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[System.Serializable]
public class CastBbsObj:CastBase {

	public CastBbsObj()
		: base() {
	}
	public CastBbsObj(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pActCategorySeq,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		bool pAllBbs,
		bool pOnlyUsed,
		int pRecPerPage,
		bool pUseMV,
		out decimal pRecCount
	) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,pOnlyUsed,new SeekCondition(),pRecPerPage,pUseMV,out pRecCount);
	}

	public int GetPageCount(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pActCategorySeq,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		bool pAllBbs,
		bool pOnlyUsed,
		SeekCondition pCondition,
		int pRecPerPage,
		bool pUseMV,
		out decimal pRecCount
	) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,pOnlyUsed,pCondition,pRecPerPage,pUseMV,out pRecCount);
	}

	private int GetPageCountBase(
		FlexibleQueryType pFlexibleQueryType,
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pActCategorySeq,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		bool pAllBbs,
		bool pOnlyUsed,
		SeekCondition pCondition,
		int pRecPerPage,
		bool pUseMV,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = CreateFlexibleQueryTarget(pFlexibleQueryType);

		try {
			conn = DbConnect("CastBbsObj.GetPageCountByLoginId");

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}
			string sSql = string.Format("SELECT COUNT(OBJ_SEQ) AS ROW_COUNT FROM {0}_BBS_OBJS00 P ",sSuffix);

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,"",pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,false,pOnlyUsed,pCondition,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				this.Fill(da,ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}

		if (pFlexibleQueryType == FlexibleQueryType.None && oFlexibleQueryTarget.HasTarget) {
			decimal dExcludeRecCount = decimal.Zero;
			GetPageCountBase(FlexibleQueryType.InScope,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,pOnlyUsed,pCondition,pRecPerPage,pUseMV,out dExcludeRecCount);
			iPages = CalcExcludeCount(ref pRecCount,dExcludeRecCount,pRecPerPage);
		}

		return iPages;
	}

	private void GetOrderBy(string pLoginId,string pObjAttrTypeSeq,string pObjAttrSeq,SeekCondition pCondition,out string pOrder) {
		string sOrderSpec = string.Empty;
		if (!pLoginId.Equals(string.Empty)) {
			sOrderSpec = sOrderSpec + "P.LOGIN_ID,";
		}
		if (!pObjAttrTypeSeq.Equals(string.Empty)) {
			sOrderSpec = sOrderSpec + "P.CAST_OBJ_ATTR_TYPE_SEQ,";
		}
		if (!pObjAttrSeq.Equals(string.Empty)) {
			sOrderSpec = sOrderSpec + "P.CAST_OBJ_ATTR_SEQ,";
		}
		if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
			sOrderSpec = sOrderSpec + "GOOD_POINT_DAILY DESC NULLS LAST,";
		} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
			sOrderSpec = sOrderSpec + "GOOD_POINT_WEEKLY DESC NULLS LAST,";
		} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
			sOrderSpec = sOrderSpec + "GOOD_POINT_MONTHLY DESC NULLS LAST,";
		}
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			pOrder = string.Format("ORDER BY P.SITE_CD,P.NA_FLAG,P.OBJ_NA_FLAG,OBJ_NON_BBS_FLAG,{0}P.OBJ_SEQ DESC ",sOrderSpec);
		} else {
			pOrder = string.Format("ORDER BY P.SITE_CD,P.NA_FLAG,P.OBJ_NA_FLAG,{0}P.OBJ_SEQ DESC,P.OBJ_KIND ",sOrderSpec);
		}
	}


	public DataSet GetPageCollectionBySeq(string pObjsSeq,bool pAllBbs,bool pUseMV) {
		return GetPageCollectionBase(pObjsSeq,"","","",string.Empty,true,"","","",pAllBbs,false,new SeekCondition(),1,1,pUseMV);
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		bool pIncludeNotAprroveFlag,
		bool pIncludeNotPublish,
		string pActCategorySeq,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		bool pAllBbs,
		bool pOnlyUsed,
		int pPageNo,
		int pRecPerPage,
		bool pUseMV) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,pOnlyUsed,new SeekCondition(),pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollection(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pActCategorySeq,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		bool pAllBbs,
		bool pOnlyUsed,
		int pPageNo,
		int pRecPerPage,
		bool pUseMV) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,pOnlyUsed,new SeekCondition(),pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollection(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pActCategorySeq,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		bool pAllBbs,
		bool pOnlyUsed,
		SeekCondition pCondition,
		int pPageNo,
		int pRecPerPage,
		bool pUseMV) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,pOnlyUsed,pCondition,pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollectionBase(
		string pObjSeq,
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pActCategorySeq,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		bool pAllBbs,
		bool pOnlyUsed,
		SeekCondition pCondition,
		int pPageNo,
		int pRecPerPage,
		bool pUseMV) {

		DataSet ds;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.None);

		try {
			conn = DbConnect("CastBbsObj.GetPageCollectionByLoginId");
			ds = new DataSet();

			string sOrder;
			GetOrderBy(pLoginId,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pCondition,out sOrder);

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder sInnerSql = new StringBuilder();
			sInnerSql.Append("SELECT * FROM( ");
			sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT 											").AppendLine();
			sInnerSql.Append("			P.*, 										").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			D.GOOD_POINT AS GOOD_POINT_DAILY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			W.GOOD_POINT AS GOOD_POINT_WEEKLY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			M.GOOD_POINT AS GOOD_POINT_MONTHLY,			").AppendLine();
			}
			sInnerSql.Append("			NVL(T.REVIEW_COUNT,0) AS REVIEW_COUNT,		").AppendLine();
			sInnerSql.Append("			NVL(T.COMMENT_COUNT,0) AS COMMENT_COUNT,	").AppendLine();
			sInnerSql.Append("			NVL(T.GOOD_STAR_TOTAL,0) AS GOOD_STAR_TOTAL,").AppendLine();

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sInnerSql.AppendLine("		1 AS BOOKMARK_FLAG	");
				} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
				} else {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						sInnerSql.AppendLine("CASE WHEN EXISTS(");
						sInnerSql.AppendLine("SELECT 1 FROM T_OBJ_BOOKMARK BKM WHERE");
						sInnerSql.AppendLine(" BKM.SITE_CD = P.SITE_CD AND");
						sInnerSql.AppendLine(" BKM.OBJ_SEQ = P.OBJ_SEQ AND");
						sInnerSql.AppendFormat(" BKM.USER_SEQ = {0} AND",sSelfUserSeq).AppendLine();
						sInnerSql.AppendFormat(" BKM.USER_CHAR_NO = {0}",sSelfUserCharNo).AppendLine();
						sInnerSql.AppendLine(") THEN 1 ELSE 0 END AS BOOKMARK_FLAG");
					} else {
						sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
					}
				}
			} else {
				sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
			}

			sInnerSql.Append("		FROM											").AppendLine();
			sInnerSql.Append("			(SELECT " + CastBasicField() + ",").AppendLine();
			if (sSuffix.Equals("MV")) {
				sInnerSql.Append("				P.OBJ_NON_BBS_FLAG			,").AppendLine();
			}
			sInnerSql.Append("				P.OBJ_KIND				,").AppendLine();
			sInnerSql.Append("				P.OBJ_SEQ				,").AppendLine();
			sInnerSql.Append("				P.OBJ_TITLE				,").AppendLine();
			sInnerSql.Append("				P.OBJ_DOC				,").AppendLine();
			sInnerSql.Append("				P.OBJ_NOT_APPROVE_FLAG	,").AppendLine();
			sInnerSql.Append("				P.OBJ_NOT_PUBLISH_FLAG	,").AppendLine();
			sInnerSql.Append("				P.READING_COUNT			,").AppendLine();
			sInnerSql.Append("				P.OBJ_UPLOAD_DATE		,").AppendLine();
			sInnerSql.Append("				P.CAST_OBJ_ATTR_TYPE_SEQ,").AppendLine();
			sInnerSql.Append("				P.CAST_OBJ_ATTR_SEQ		,").AppendLine();
			sInnerSql.Append("				P.PLANNING_TYPE			,").AppendLine();
			sInnerSql.Append("				P.OBJ_NA_FLAG			,").AppendLine();
			sInnerSql.Append("				P.OBJ_RANKING_FLAG		").AppendLine();
			sInnerSql.Append(string.Format("FROM {0}_BBS_OBJS00 P	",sSuffix)).AppendLine();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pObjSeq,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pActCategorySeq,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pAllBbs,true,pOnlyUsed,pCondition,ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();

			sInnerSql.Append("			) P,							").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_DAILY D,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_WEEKLY W,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_MONTHLY M,			").AppendLine();
			}
			sInnerSql.Append("			T_OBJ_REVIEW_TOTAL	T			").AppendLine();
			sInnerSql.Append("		WHERE								").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			P.SITE_CD	= D.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.OBJ_SEQ	= D.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			P.SITE_CD	= W.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.OBJ_SEQ	= W.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			P.SITE_CD	= M.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.OBJ_SEQ	= M.OBJ_SEQ	(+) AND	").AppendLine();
			}
			sInnerSql.Append("			P.SITE_CD	= T.SITE_CD	(+) AND	").AppendLine();
			sInnerSql.Append("			P.OBJ_SEQ	= T.OBJ_SEQ	(+)		").AppendLine();

			sInnerSql.Append(sOrder).AppendLine();
			if (pObjSeq.Equals(string.Empty)) {
				sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sInnerSql.Append(")INNER ) ");
			}

			SessionObjs oObj = this.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			string sHint = string.Empty;
			if (oObj.sqlHint.ContainsKey("CAST")) {
				sHint = oObj.sqlHint["CAST"].ToString();
			}

			StringBuilder sTables = new StringBuilder();
			sTables.Append("T_CAST_PIC					T_OBJ_PIC	,").AppendLine();
			sTables.Append("T_CAST_MOVIE				T_OBJ_MOVIE	,").AppendLine();
			sTables.Append("T_CAST_PIC_ATTR_TYPE		,").AppendLine();
			sTables.Append("T_CAST_PIC_ATTR_TYPE_VALUE	,");

			StringBuilder sJoinTable = new StringBuilder();
			sJoinTable.Append("T1.OBJ_SEQ					= T_OBJ_PIC.PIC_SEQ									(+)	AND").AppendLine();
			sJoinTable.Append("T1.OBJ_SEQ					= T_OBJ_MOVIE.MOVIE_SEQ								(+)	AND").AppendLine();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_PIC_ATTR_TYPE.SITE_CD						(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ		(+)	AND").AppendLine();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD				(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ	(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_OBJ_ATTR_SEQ			= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ		(+) AND");

			StringBuilder sJoinField = new StringBuilder();

			//PICとMOVIEのみの項目------------------------------------------------
			sJoinField.Append("T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_NM	AS CAST_OBJ_ATTR_TYPE_NM,").AppendLine();
			sJoinField.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_NM	AS CAST_OBJ_ATTR_NM		,").AppendLine();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開中', '非公開'), '申請中') AS UNAUTH_MARK,").AppendLine();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開', '非公開'), '認証待ち')	AS UNAUTH_MARK_ADMIN,").AppendLine();
			//PICのみの項目-------------------------------------------------------
			sJoinField.Append("T_OBJ_PIC.PIC_SEQ				,").AppendLine();
			sJoinField.Append("GET_PHOTO_IMG_PATH				(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_PHOTO_IMG_PATH			,").AppendLine();
			sJoinField.Append("GET_SMALL_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_SMALL_PHOTO_IMG_PATH		,").AppendLine();
			sJoinField.Append("GET_BLUR_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_BLUR_PHOTO_IMG_PATH		,").AppendLine();
			sJoinField.Append("GET_SMALL_BLUR_PHOTO_IMG_PATH	(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_SMALL_BLUR_PHOTO_IMG_PATH	,").AppendLine();
			//MOVIEのみの項目-------------------------------------------------------
			sJoinField.Append("T_OBJ_MOVIE.MOVIE_TYPE			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.MOVIE_SEQ			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.CHARGE_POINT			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.PLAY_TIME			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.MOVIE_SERIES			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.SAMPLE_MOVIE_SEQ		,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.THUMBNAIL_PIC_SEQ	,").AppendLine();
			sJoinField.Append("GET_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_MOVIE.THUMBNAIL_PIC_SEQ,T1.PROFILE_PIC_SEQ) AS THUMBNAIL_IMG_PATH,");

			sSql = string.Format(sSql,sInnerSql,sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				if (pObjSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
				}

				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_BBS_OBJS00");
				}
			}
		} finally {
			conn.Close();
		}

		ds = this.Exclude(ds,pRecPerPage,oFlexibleQueryTarget);

		AppendAttr(ds);
		AppendObjUsedHistory(ds);

		return ds;
	}

	private OracleParameter[] CreateWhere(FlexibleQueryTargetInfo pFlexibleQueryTarget,string pObjSeq,string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,bool pIncludeNotPublish,string pActCategorySeq,string pCastBbsObjAttrTypeSeq,string pCastBbsObjAttrSeq,bool pAllBbbsObjs,bool bAllInfo,bool pOnlyUsed,SeekCondition pCondition,ref string pWhere) {
		ArrayList list = new ArrayList();
		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		if (!pObjSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.OBJ_SEQ = :OBJ_SEQ",ref pWhere);
			list.Add(new OracleParameter("OBJ_SEQ",pObjSeq));
		} else {
			if (pSiteCd.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
				list.Add(new OracleParameter("SITE_CD",pSiteCd));
			}

			SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhere);
			list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));

			if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR) && pIncludeNotPublish == false) {
				SysPrograms.SqlAppendWhere("P.OBJ_NA_FLAG = :OBJ_NA_FLAG",ref pWhere);
				list.Add(new OracleParameter("OBJ_NA_FLAG",ViCommConst.FLAG_OFF));
			} else {
				if (!string.IsNullOrEmpty(pApproveStatus)) {
					SysPrograms.SqlAppendWhere("P.OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pApproveStatus));
				}
				if (pIncludeNotPublish == false) {
					SysPrograms.SqlAppendWhere("P.OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));
				}
			}

			if (pLoginId.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.LOGIN_ID = :LOGIN_ID",ref pWhere);
				SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginId));
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}

			if (!string.IsNullOrEmpty(pCastBbsObjAttrTypeSeq)) {
				SysPrograms.SqlAppendWhere("P.CAST_OBJ_ATTR_TYPE_SEQ = :CAST_OBJ_ATTR_TYPE_SEQ",ref pWhere);
				list.Add(new OracleParameter("CAST_OBJ_ATTR_TYPE_SEQ",pCastBbsObjAttrTypeSeq));
			}

			if (!string.IsNullOrEmpty(pCastBbsObjAttrSeq)) {
				SysPrograms.SqlAppendWhere("P.CAST_OBJ_ATTR_SEQ = :CAST_OBJ_ATTR_SEQ",ref pWhere);
				list.Add(new OracleParameter("CAST_OBJ_ATTR_SEQ",pCastBbsObjAttrSeq));
			}

			if (!string.IsNullOrEmpty(pCondition.screenId)) {
				if (!string.IsNullOrEmpty(pCastBbsObjAttrSeq) && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
					using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
						if (oCastMovieAttrTypeValue.GetOne(pSiteCd,pCastBbsObjAttrSeq)) {
							SysPrograms.SqlAppendWhere("P.PLANNING_TYPE = :PLANNING_TYPE",ref pWhere);
							list.Add(new OracleParameter("PLANNING_TYPE",oCastMovieAttrTypeValue.planningType));
						}
					}
				} else {
					SysPrograms.SqlAppendWhere("P.PLANNING_TYPE > 0",ref pWhere);
				}
			} else {
				SysPrograms.SqlAppendWhere("P.PLANNING_TYPE = 0",ref pWhere);
			}

			if (pAllBbbsObjs == false) {
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
					SysPrograms.SqlAppendWhere("P.OBJ_NON_BBS_FLAG = :OBJ_NON_BBS_FLAG ",ref pWhere);
					list.Add(new OracleParameter("OBJ_NON_BBS_FLAG",ViCommConst.FLAG_OFF));
				} else {
					SysPrograms.SqlAppendWhere("P.OBJ_KIND IN (:OBJ_KIND1,:OBJ_KIND2) ",ref pWhere);
					list.Add(new OracleParameter("OBJ_KIND1",ViCommConst.BbsObjType.PIC));
					list.Add(new OracleParameter("OBJ_KIND2",ViCommConst.BbsObjType.MOVIE));
				}
			}

			if (pActCategorySeq.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ",ref pWhere);
				list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pActCategorySeq));
			}
			if (pOnlyUsed) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ				");
				oSubQueryBuilder.AppendLine(" ) ");
				pWhere = pWhere + oSubQueryBuilder.ToString();

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));

			} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ				");
				oSubQueryBuilder.AppendLine(" ) ");
				pWhere = pWhere + oSubQueryBuilder.ToString();

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));

			}

			if (!string.IsNullOrEmpty(pCondition.rankType)) {
				SysPrograms.SqlAppendWhere("P.OBJ_RANKING_FLAG = 1",ref pWhere);
			}

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						StringBuilder oSubQueryBuilder = new StringBuilder();
						oSubQueryBuilder.AppendLine(" AND EXISTS(										");
						oSubQueryBuilder.AppendLine("	SELECT											");
						oSubQueryBuilder.AppendLine("		1											");
						oSubQueryBuilder.AppendLine("	FROM											");
						oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
						oSubQueryBuilder.AppendLine("	WHERE											");
						oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
						oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.OBJ_SEQ			AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
						oSubQueryBuilder.AppendLine(" )													");
						pWhere = pWhere + oSubQueryBuilder.ToString();

						list.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
						list.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
					}
				}
			}
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			pWhere += SetFlexibleQuery(pFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private string SetExtednCondition(SeekCondition pCondition,ref System.Collections.Generic.List<OracleParameter> pParamList) {
		StringBuilder oSubQueryBuilder = new StringBuilder();
		// 拡張検索(一覧下部の検索フォームからの検索)の条件追加
		if (pCondition.hasExtend) {
			if (pCondition.onlineStatus == ViCommConst.SeekOnlineStatus.WAITING) {
				oSubQueryBuilder.AppendLine(" AND P.CHARACTER_ONLINE_STATUS = :EXT_ONLINE_STATUS_WAITING ");
				pParamList.Add(new OracleParameter(":EXT_ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
			}
			if (!string.IsNullOrEmpty(pCondition.handleNm)) {
				oSubQueryBuilder.AppendLine(" AND P.HANDLE_NM LIKE '%' || :EXT_HANDLE_NM || '%' ");
				pParamList.Add(new OracleParameter(":EXT_HANDLE_NM",pCondition.handleNm));
			}
			if (!string.IsNullOrEmpty(pCondition.ageFrom)) {
				oSubQueryBuilder.AppendLine(" AND P.AGE >= :EXT_AGE_FROM ");
				pParamList.Add(new OracleParameter(":EXT_AGE_FROM",pCondition.ageFrom));
			}
			if (!string.IsNullOrEmpty(pCondition.ageTo)) {
				oSubQueryBuilder.AppendLine(" AND P.AGE <= :EXT_AGE_TO ");
				pParamList.Add(new OracleParameter(":EXT_AGE_TO",pCondition.ageTo));
			}

			if (pCondition.newCastDay != 0) {
				oSubQueryBuilder.AppendLine(" AND SYSDATE <= TO_DATE(P.START_PERFORM_DAY, 'YYYY/MM/DD') + :NEW_CAST_DAY ");
				pParamList.Add(new OracleParameter(":NEW_CAST_DAY",pCondition.newCastDay));
			}

			if (pCondition.attrValue.Count > 0) {
				string sWhere = string.Empty;
				this.CreateAttrQuery(pCondition,ref sWhere,ref pParamList);
				oSubQueryBuilder.Append(sWhere);
			}
		}
		return oSubQueryBuilder.ToString();
	}
	private void CreateAttrQuery(SeekCondition pCondition,ref string pWhere,ref System.Collections.Generic.List<OracleParameter> pList) {
		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				pWhere = pWhere + string.Format(" AND AT{0}.SITE_CD				= P.SITE_CD				",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_SEQ			= P.USER_SEQ			",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO		",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0}",i + 1);
				pList.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						pWhere += " AND (";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									pWhere += " OR ";
								}
								pWhere = pWhere + string.Format(" (AT{0}.CAST_ATTR_INPUT_VALUE	LIKE '%' || :CAST_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						pWhere += ")";
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');
					if (sValues.Length > 1) {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	IN (		",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0)
								pWhere = pWhere + " , ";
							pWhere = pWhere + string.Format(" :CAST_ATTR_SEQ{0}_{1}		",i + 1,j);

							pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						pWhere = pWhere + " ) ";
					} else {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	= :CAST_ATTR_SEQ{0}		",i + 1);
						pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					pWhere = pWhere + string.Format(" AND AT{0}.GROUPING_CD		= :GROUPING_CD{0}	",i + 1);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}
	}
	private FlexibleQueryTargetInfo CreateFlexibleQueryTarget(FlexibleQueryType pFlexibleQueryType) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(pFlexibleQueryType);
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			oFlexibleQueryTarget.JealousyBbs = true;
		}
		return oFlexibleQueryTarget;
	}

	private void AppendAttr(DataSet pDS) {
		string sSql;

		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD	AND " +
						"USER_SEQ		= :USER_SEQ	AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO	";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();

			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"].ToString())] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	private void AppendObjUsedHistory(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_DATE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_FLAG",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_USED_SPECIAL_FREE_FLAG",Type.GetType("System.String"));

		string sSiteCd = string.Empty;
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			sSiteCd = oSessionMan.site.siteCd;
			sUserSeq = oSessionMan.userMan.userSeq;
			sUserCharNo = oSessionMan.userMan.userCharNo;
		}

		if (string.IsNullOrEmpty(sSiteCd) || string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sUserCharNo)) {
			return;
		}

		string sWhereClause = String.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> sObjSeqList = new List<string>();

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			sObjSeqList.Add(iBridUtil.GetStringValue(dr["OBJ_SEQ"]));

			dr["OBJ_DOWNLOAD_FLAG"] = "0";
			dr["OBJ_DOWNLOAD_DATE"] = "";
			dr["SELF_USED_SPECIAL_FREE_FLAG"] = "0";
		}

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		SysPrograms.SqlAppendWhere(string.Format("OBJ_SEQ IN ({0})",string.Join(",",sObjSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	OBJ_SEQ,");
		oSqlBuilder.AppendLine("	USED_DATE,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG2");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_OBJ_USED_HISTORY");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["OBJ_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["OBJ_SEQ"]);

			pDR["OBJ_DOWNLOAD_FLAG"] = "1";
			pDR["OBJ_DOWNLOAD_DATE"] = subDR["USED_DATE"].ToString();

			if (subDR["SPECIAL_FREE_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR) || subDR["SPECIAL_FREE_FLAG2"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				pDR["SELF_USED_SPECIAL_FREE_FLAG"] = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	/// <summary>
	/// 指定されたカテゴリ毎の投稿数を要素名と共に取得する。 
	/// </summary>
	/// <param name="pSiteCd">サイトコード</param>
	/// <param name="pUserSeq">ユーザSEQ</param>
	/// <param name="pUserCharNo">ユーザキャラクタNO</param>
	/// <param name="pActCategorySeq">ユーザカテゴリ連番</param>
	/// <param name="pObjKind">
	/// オブジェクト種類 
	///		1	:	新着 
	///		2	:	写真 
	///		3		動画 
	/// </param>
	/// <returns></returns>
	/// 
	public DataSet GetObjAttrTypeCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pObjKind,string pApproveStatus,bool pRecentNotLogin,bool pUseMV,bool pOnlyUsed,string pPlanningType,SeekCondition pCondition) {
		DataSet ds;
		try {
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableGetObjAttrNew"]).Equals(ViCommConst.FLAG_ON_STR) && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				return GetObjAttrTypeCountNew(pSiteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjKind,pApproveStatus,pRecentNotLogin,pUseMV,pOnlyUsed,pCondition);
			}

			conn = DbConnect("CastBbsObj.GetObjAttrTypeCount");

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder oString = new StringBuilder();
			System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();

			oString.Append("SELECT																			").AppendLine();
			oString.Append("	NVL(T1.CNT,0) AS CNT					,									").AppendLine();
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ		AS OBJ_ATTR_TYPE_SEQ	,	").AppendLine();
				oString.Append("T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_NM		AS OBJ_ATTR_TYPE_NM			").AppendLine();
			} else {
				oString.Append("T_CAST_MOVIE_ATTR_TYPE.CAST_MOVIE_ATTR_TYPE_SEQ	AS OBJ_ATTR_TYPE_SEQ	,	").AppendLine();
				oString.Append("T_CAST_MOVIE_ATTR_TYPE.CAST_MOVIE_ATTR_TYPE_NM	AS OBJ_ATTR_TYPE_NM			").AppendLine();
			}
			oString.Append("FROM											").AppendLine();
			oString.Append("	(SELECT										").AppendLine();
			oString.Append("		P.CAST_OBJ_ATTR_TYPE_SEQ,				").AppendLine();
			oString.Append("		COUNT(P.CAST_OBJ_ATTR_TYPE_SEQ) AS CNT	").AppendLine();
			oString.Append("	FROM										").AppendLine();
			oString.Append(string.Format("{0}_BBS_OBJS00 P",sSuffix)).AppendLine();
			if (pCondition != null && pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						oString.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
					}
				}
			}
			oString.Append("	WHERE											").AppendLine();
			oString.Append("		P.SITE_CD				=:SITE_CD		AND	").AppendLine();
			oString.Append("		P.NA_FLAG				=:NA_FLAG			").AppendLine();

			if (!pUserSeq.Equals(string.Empty)) {
				oString.Append("	AND P.USER_SEQ			=:USER_SEQ					").AppendLine();
				oString.Append("	AND P.USER_CHAR_NO		=:USER_CHAR_NO				").AppendLine();
			}
			if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
				oString.Append("	AND P.OBJ_NA_FLAG			=:OBJ_NA_FLAG			").AppendLine();
			} else {
				if (!string.IsNullOrEmpty(pApproveStatus)) {
					oString.Append("	AND P.OBJ_NOT_APPROVE_FLAG	=:OBJ_NOT_APPROVE_FLAG	").AppendLine();
				}
				oString.Append("	AND P.OBJ_NOT_PUBLISH_FLAG	=:OBJ_NOT_PUBLISH_FLAG	").AppendLine();
			}

			if (!pObjKind.Equals(string.Empty)) {
				oString.Append("	AND P.OBJ_KIND			=:OBJ_KIND					").AppendLine();
			} else {
				oString.Append("	AND P.OBJ_KIND IN		(:OBJ_KIND1,:OBJ_KIND2)		").AppendLine();
			}
			if (!pActCategorySeq.Equals(string.Empty)) {
				oString.Append("	AND P.ACT_CATEGORY_SEQ	=:ACT_CATEGORY_SEQ			").AppendLine();
			}
			if (pRecentNotLogin) {
				oString.Append("	AND	P.LAST_LOGIN_DATE	<:LAST_LOGIN_DATE			").AppendLine();
			}
			if (pOnlyUsed) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				if (!string.IsNullOrEmpty(pObjKind)) {
					oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				}
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				if (!string.IsNullOrEmpty(pObjKind)) {
					if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
						oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
					} else {
						oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
					}
				}
			} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				if (!string.IsNullOrEmpty(pObjKind)) {
					oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				}
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				if (!string.IsNullOrEmpty(pObjKind)) {
					if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
						oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
					} else {
						oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
					}
				}
			}

			if (!string.IsNullOrEmpty(pCondition.rankType)) {
				oString.AppendLine(" AND P.OBJ_RANKING_FLAG = 1");
			}

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						StringBuilder oSubQueryBuilder = new StringBuilder();
						oSubQueryBuilder.AppendLine(" AND EXISTS(										");
						oSubQueryBuilder.AppendLine("	SELECT											");
						oSubQueryBuilder.AppendLine("		1											");
						oSubQueryBuilder.AppendLine("	FROM											");
						oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
						oSubQueryBuilder.AppendLine("	WHERE											");
						oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
						oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.OBJ_SEQ			AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
						oSubQueryBuilder.AppendLine(" )													");
						oString.AppendLine(oSubQueryBuilder.ToString());

						oParamList.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
						oParamList.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
					}
				}
			}

			if (pCondition.hasExtend) {
				oString.AppendLine(SetExtednCondition(pCondition,ref oParamList));
			}

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
					string sUserSeq = string.Empty;
					string sUserCharNo = string.Empty;
					if (this.sessionObj != null) {
						if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
							SessionMan oSessionMan = (SessionMan)this.sessionObj;
							sUserSeq = oSessionMan.userMan.userSeq;
							sUserCharNo = oSessionMan.userMan.userCharNo;
						}
					}
					if (!sUserSeq.Equals(string.Empty)) {
						oString.AppendLine(" AND NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)");
						oParamList.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
						oParamList.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
					}
				}
			}

			//やきもち防止 
			ArrayList list = new ArrayList();
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				oString.Append(SetFlexibleQuery(this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope),oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list));
			}
			//
			oString.Append("	GROUP BY												").AppendLine();
			oString.Append("		P.CAST_OBJ_ATTR_TYPE_SEQ) T1,						").AppendLine();
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("T_CAST_PIC_ATTR_TYPE			").AppendLine();
			} else {
				oString.Append("T_CAST_MOVIE_ATTR_TYPE			").AppendLine();
			}
			oString.Append("WHERE								").AppendLine();
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				if (!pPlanningType.Equals(string.Empty)) {
					oString.Append("T_CAST_PIC_ATTR_TYPE.PLANNING_TYPE			= :PLANNING_TYPE					AND	").AppendLine();
				}
				oString.Append("T_CAST_PIC_ATTR_TYPE.SITE_CD					= :SITE_CD							AND	").AppendLine();
				oString.Append("T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ		= T1.CAST_OBJ_ATTR_TYPE_SEQ	(+)	 		").AppendLine();
			} else {
				if (!pPlanningType.Equals(string.Empty)) {
					oString.Append("T_CAST_MOVIE_ATTR_TYPE.PLANNING_TYPE		= :PLANNING_TYPE					AND	").AppendLine();
				}
				oString.Append("T_CAST_MOVIE_ATTR_TYPE.SITE_CD					= :SITE_CD							AND	").AppendLine();
				oString.Append("T_CAST_MOVIE_ATTR_TYPE.CAST_MOVIE_ATTR_TYPE_SEQ	= T1.CAST_OBJ_ATTR_TYPE_SEQ	(+)			").AppendLine();
			}
			oString.Append("ORDER BY							").AppendLine();
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("T_CAST_PIC_ATTR_TYPE.PRIORITY	").AppendLine();
			} else {
				oString.Append("T_CAST_MOVIE_ATTR_TYPE.PRIORITY	").AppendLine();
			}


			using (cmd = CreateSelectCommand(oString.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);
				if (!pUserSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}
				if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
					cmd.Parameters.Add("OBJ_NA_FLAG",ViCommConst.FLAG_OFF);
				} else {
					if (!string.IsNullOrEmpty(pApproveStatus)) {
						cmd.Parameters.Add("OBJ_NOT_APPROVE_FLAG",pApproveStatus);
					}
					cmd.Parameters.Add("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF);
				}

				if (!pObjKind.Equals(string.Empty)) {
					cmd.Parameters.Add("OBJ_KIND",pObjKind);
				} else {
					cmd.Parameters.Add("OBJ_KIND1",ViCommConst.BbsObjType.PIC);
					cmd.Parameters.Add("OBJ_KIND2",ViCommConst.BbsObjType.MOVIE);
				}
				if (!pActCategorySeq.Equals(string.Empty)) {
					cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);
				}
				if (pRecentNotLogin) {
					cmd.Parameters.Add("LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Now.AddDays(ViCommConst.FREE_BBS_VIEW_NOT_LOGIN_DAYS * -1),ParameterDirection.Input);
				}

				cmd.Parameters.AddRange(oParamList.ToArray());

				//やきもち防止
				if (list != null) {
					OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
					for (int i = 0;i < objParms.Length;i++) {
						cmd.Parameters.Add((OracleParameter)objParms[i]);
					}
				}
				if (!pPlanningType.Equals(string.Empty)) {
					cmd.Parameters.Add("PLANNING_TYPE",pPlanningType);
				}
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_BBS_OBJS00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	/// <summary>
	/// 指定されたカテゴリ毎の投稿数を要素名と共に取得する。 
	/// </summary>
	/// <param name="pSiteCd">サイトコード</param>
	/// <param name="pUserSeq">ユーザSEQ</param>
	/// <param name="pUserCharNo">ユーザキャラクタNO</param>
	/// <param name="pActCategorySeq">ユーザカテゴリ連番</param>
	/// <param name="pObjKind">
	/// オブジェクト種類 
	///		1	:	新着 
	///		2	:	写真 
	///		3		動画 
	/// </param>
	/// <returns></returns>
	public DataSet GetObjAttrTypeCountNew(string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pObjKind,string pApproveStatus,bool pRecentNotLogin,bool pUseMV,bool pOnlyUsed,SeekCondition pCondition) {
		DataSet ds;
		try {
			conn = DbConnect("CastBbsObj.GetObjAttrTypeCount");

			StringBuilder oString = new StringBuilder();
			System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();

			string sSubQueryAll = string.Empty;
			string sSubQueryJealousy = string.Empty;
			oParamList.AddRange(CreateObjAttrTypeSubQuery(FlexibleQueryType.None,pSiteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjKind,pApproveStatus,pRecentNotLogin,pUseMV,pOnlyUsed,pCondition,out sSubQueryAll));
			oParamList.AddRange(CreateObjAttrTypeSubQuery(FlexibleQueryType.InScope,pSiteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjKind,pApproveStatus,pRecentNotLogin,pUseMV,pOnlyUsed,pCondition,out sSubQueryJealousy));


			oString.Append("SELECT																			").AppendLine();
			oString.AppendFormat("	NVL((		").AppendLine();
			oString.AppendFormat("	{0}			",sSubQueryAll).AppendLine();
			oString.AppendFormat("	),0)		").AppendLine();
			oString.AppendFormat("	-			").AppendLine();
			oString.AppendFormat("	NVL((		").AppendLine();
			oString.AppendFormat("	{0}			",sSubQueryJealousy).AppendLine();
			oString.AppendFormat("	),0)		").AppendLine();
			oString.AppendFormat("	AS CNT  ,	").AppendLine(); // 総ﾚｺｰﾄﾞ-ヤキモチ防止に該当するﾚｺｰﾄﾞ



			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("	T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ		AS OBJ_ATTR_TYPE_SEQ	,	").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_NM		AS OBJ_ATTR_TYPE_NM			").AppendLine();
				oString.Append("FROM											").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE						").AppendLine();
				oString.Append("WHERE											").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE.SITE_CD	= :SITE_CD	").AppendLine();
				oString.Append("ORDER BY										").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE.PRIORITY				").AppendLine();
			} else {
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE.CAST_MOVIE_ATTR_TYPE_SEQ	AS OBJ_ATTR_TYPE_SEQ	,	").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE.CAST_MOVIE_ATTR_TYPE_NM	AS OBJ_ATTR_TYPE_NM			").AppendLine();
				oString.Append("FROM											").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE						").AppendLine();
				oString.Append("WHERE											").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE.SITE_CD	= :SITE_CD	").AppendLine();
				oString.Append("ORDER BY										").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE.PRIORITY				").AppendLine();
			}
			using (cmd = CreateSelectCommand(oString.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);

				cmd.Parameters.AddRange(oParamList.ToArray());

				cmd.Parameters.Add(":SITE_CD",pSiteCd);

				cmd.BindByName = true;
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_BBS_OBJS00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	private List<OracleParameter> CreateObjAttrTypeSubQuery(FlexibleQueryType pFlexibleQueryType,string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pObjKind,string pApproveStatus,bool pRecentNotLogin,bool pUseMV,bool pOnlyUsed,SeekCondition pCondition,out string pQuery) {
		string sSuffix = "VW";
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			sSuffix = "MV";
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oString = new StringBuilder();

		oString.Append("	SELECT										").AppendLine();
		oString.Append("		COUNT(*) AS CNT	").AppendLine();
		oString.Append("	FROM										").AppendLine();
		oString.Append(string.Format("{0}_BBS_OBJS00 P",sSuffix)).AppendLine();
		if (pCondition != null && pCondition.hasExtend) {
			for (int i = 0;i < pCondition.attrValue.Count;i++) {
				if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
					oString.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
				}
			}
		}
		oString.Append("	WHERE											").AppendLine();
		if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
			oString.Append("P.SITE_CD					= T_CAST_PIC_ATTR_TYPE.SITE_CD						AND ").AppendLine();
			oString.Append("P.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ		AND	").AppendLine();
		} else {
			oString.Append("P.SITE_CD					= T_CAST_MOVIE_ATTR_TYPE.SITE_CD					AND ").AppendLine();
			oString.Append("P.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_MOVIE_ATTR_TYPE.CAST_MOVIE_ATTR_TYPE_SEQ	AND	").AppendLine();
		}
		oString.Append("		P.NA_FLAG				=:NA_FLAG			").AppendLine();
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		if (!pUserSeq.Equals(string.Empty)) {
			oString.Append("	AND P.USER_SEQ			=:USER_SEQ					").AppendLine();
			oString.Append("	AND P.USER_CHAR_NO		=:USER_CHAR_NO				").AppendLine();
			oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		}

		if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
			oString.Append("	AND P.OBJ_NA_FLAG			=:OBJ_NA_FLAG			").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",ViCommConst.FLAG_OFF));
		} else {
			if (!string.IsNullOrEmpty(pApproveStatus)) {
				oString.Append("	AND P.OBJ_NOT_APPROVE_FLAG	=:OBJ_NOT_APPROVE_FLAG	").AppendLine();
				oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",pApproveStatus));
			}
			oString.Append("	AND P.OBJ_NOT_PUBLISH_FLAG	=:OBJ_NOT_PUBLISH_FLAG	").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));
		}

		if (!pObjKind.Equals(string.Empty)) {
			oString.Append("	AND P.OBJ_KIND			=:OBJ_KIND					").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_KIND",pObjKind));
		} else {
			oString.Append("	AND P.OBJ_KIND IN		(:OBJ_KIND1,:OBJ_KIND2)		").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_KIND1",ViCommConst.BbsObjType.PIC));
			oParamList.Add(new OracleParameter(":OBJ_KIND2",ViCommConst.BbsObjType.MOVIE));
		}
		if (!pActCategorySeq.Equals(string.Empty)) {
			oString.Append("	AND P.ACT_CATEGORY_SEQ	=:ACT_CATEGORY_SEQ			").AppendLine();
			oParamList.Add(new OracleParameter(":ACT_CATEGORY_SEQ",pActCategorySeq));
		}
		if (pRecentNotLogin) {
			oString.Append("	AND	P.LAST_LOGIN_DATE	<:LAST_LOGIN_DATE			").AppendLine();
			oParamList.Add(new OracleParameter(":LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Now.AddDays(ViCommConst.FREE_BBS_VIEW_NOT_LOGIN_DAYS * -1),ParameterDirection.Input));
		}
		if (pOnlyUsed) {
			StringBuilder oSubQueryBuilder = new StringBuilder();
			oSubQueryBuilder.AppendLine(" AND EXISTS( ");
			oSubQueryBuilder.AppendLine("	SELECT										");
			oSubQueryBuilder.AppendLine("		1										");
			oSubQueryBuilder.AppendLine("	FROM										");
			oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
			oSubQueryBuilder.AppendLine("	WHERE										");
			oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
			oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
			if (!string.IsNullOrEmpty(pObjKind)) {
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
			}
			oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
			oSubQueryBuilder.AppendLine(" ) ");
			oString.AppendLine(oSubQueryBuilder.ToString());

			oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
			oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
			if (!string.IsNullOrEmpty(pObjKind)) {
				if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
				} else {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
				}
			}
		} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			StringBuilder oSubQueryBuilder = new StringBuilder();
			oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
			oSubQueryBuilder.AppendLine("	SELECT										");
			oSubQueryBuilder.AppendLine("		1										");
			oSubQueryBuilder.AppendLine("	FROM										");
			oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
			oSubQueryBuilder.AppendLine("	WHERE										");
			oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
			oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
			if (!string.IsNullOrEmpty(pObjKind)) {
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
			}
			oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
			oSubQueryBuilder.AppendLine(" ) ");
			oString.AppendLine(oSubQueryBuilder.ToString());

			oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
			oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
			if (!string.IsNullOrEmpty(pObjKind)) {
				if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
				} else {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
				}
			}
		}

		if (!string.IsNullOrEmpty(pCondition.rankType)) {
			oString.AppendLine(" AND P.OBJ_RANKING_FLAG = 1");
		}

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
			if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SessionMan sessionMan = (SessionMan)sessionObj;
				string sSelfUserSeq = sessionMan.userMan.userSeq;
				string sSelfUserCharNo = sessionMan.userMan.userCharNo;

				if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
					StringBuilder oSubQueryBuilder = new StringBuilder();
					oSubQueryBuilder.AppendLine(" AND EXISTS(										");
					oSubQueryBuilder.AppendLine("	SELECT											");
					oSubQueryBuilder.AppendLine("		1											");
					oSubQueryBuilder.AppendLine("	FROM											");
					oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
					oSubQueryBuilder.AppendLine("	WHERE											");
					oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
					oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.OBJ_SEQ			AND	");
					oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
					oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
					oSubQueryBuilder.AppendLine(" )													");
					oString.AppendLine(oSubQueryBuilder.ToString());

					oParamList.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
					oParamList.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
				}
			}
		}

		if (pCondition.hasExtend) {
			oString.AppendLine(SetExtednCondition(pCondition,ref oParamList));
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					oString.AppendLine(" AND NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)");
					oParamList.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					oParamList.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}

		//やきもち防止 
		ArrayList list = new ArrayList();
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			oString.Append(SetFlexibleQuery(this.CreateFlexibleQueryTarget(pFlexibleQueryType),oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list));

			foreach (OracleParameter oParam in list) {
				oParamList.Add(oParam);
			}
		}
		//
		oString.Append("	GROUP BY								").AppendLine();
		oString.Append("		P.CAST_OBJ_ATTR_TYPE_SEQ			").AppendLine();


		pQuery = oString.ToString();
		return oParamList;
	}

	public DataSet GetObjAttrCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pObjKind,string pApproveStatus,bool pRecentNotLogin,bool pUseMV,bool pOnlyUsed,SeekCondition pCondition) {

		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableGetObjAttrNew"]).Equals(ViCommConst.FLAG_ON_STR) && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			return GetObjAttrCountNew(pSiteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjKind,pApproveStatus,pRecentNotLogin,pUseMV,pOnlyUsed,pCondition);
		}

		DataSet ds;
		try {
			conn = DbConnect("CastBbsObj.GetObjAttrCount");

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder oString = new StringBuilder();
			System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();
			oString.Append("SELECT																				").AppendLine();
			oString.Append("	NVL(T1.CNT,0) AS CNT					,										").AppendLine();
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ		AS OBJ_ATTR_TYPE_SEQ,	").AppendLine();
				oString.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ			AS OBJ_ATTR_SEQ		,	").AppendLine();
				oString.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_NM				AS OBJ_ATTR_NM			").AppendLine();
			} else {
				oString.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_TYPE_SEQ	AS OBJ_ATTR_TYPE_SEQ,	").AppendLine();
				oString.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_SEQ		AS OBJ_ATTR_SEQ		,	").AppendLine();
				oString.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_NM			AS OBJ_ATTR_NM			").AppendLine();
			}
			oString.Append("FROM								").AppendLine();
			oString.Append("	(SELECT							").AppendLine();
			oString.Append("		P.CAST_OBJ_ATTR_TYPE_SEQ,	").AppendLine();
			oString.Append("		P.CAST_OBJ_ATTR_SEQ		,	").AppendLine();
			oString.Append("		COUNT(*) AS CNT				").AppendLine();
			oString.Append("	FROM							").AppendLine();
			oString.Append(string.Format("{0}_BBS_OBJS00 P	",sSuffix)).AppendLine();
			if (pCondition != null && pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						oString.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
					}
				}
			}
			oString.Append("	WHERE							").AppendLine();
			oString.Append("		P.SITE_CD					=:SITE_CD					AND	").AppendLine();
			oString.Append("		P.NA_FLAG					=:NA_FLAG						").AppendLine();

			if (!pUserSeq.Equals(string.Empty)) {
				oString.Append("	AND P.USER_SEQ		=:USER_SEQ				").AppendLine();
				oString.Append("	AND P.USER_CHAR_NO	=:USER_CHAR_NO			").AppendLine();
			}

			if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
				oString.Append("	AND P.OBJ_NA_FLAG			=:OBJ_NA_FLAG			").AppendLine();
			} else {
				if (!string.IsNullOrEmpty(pApproveStatus)) {
					oString.Append("	AND P.OBJ_NOT_APPROVE_FLAG	=:OBJ_NOT_APPROVE_FLAG	").AppendLine();
				}
				oString.Append("	AND P.OBJ_NOT_PUBLISH_FLAG	=:OBJ_NOT_PUBLISH_FLAG	").AppendLine();
			}
			if (!pObjKind.Equals(string.Empty)) {
				oString.Append("	AND P.OBJ_KIND		=:OBJ_KIND				").AppendLine();
			} else {
				oString.Append("	AND P.OBJ_KIND IN	(:OBJ_KIND1,:OBJ_KIND2)	").AppendLine();
			}
			if (!pActCategorySeq.Equals(string.Empty)) {
				oString.Append("	AND P.ACT_CATEGORY_SEQ	=:ACT_CATEGORY_SEQ	").AppendLine();
			}
			if (pRecentNotLogin) {
				oString.Append("	AND	P.LAST_LOGIN_DATE	<:LAST_LOGIN_DATE	").AppendLine();
			}
			if (pOnlyUsed) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
				} else {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
				}
			} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
				} else {
					oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
				}
			}

			if (!string.IsNullOrEmpty(pCondition.rankType)) {
				oString.AppendLine(" AND P.OBJ_RANKING_FLAG = 1");
			}

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						StringBuilder oSubQueryBuilder = new StringBuilder();
						oSubQueryBuilder.AppendLine(" AND EXISTS(										");
						oSubQueryBuilder.AppendLine("	SELECT											");
						oSubQueryBuilder.AppendLine("		1											");
						oSubQueryBuilder.AppendLine("	FROM											");
						oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
						oSubQueryBuilder.AppendLine("	WHERE											");
						oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
						oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.OBJ_SEQ			AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
						oSubQueryBuilder.AppendLine(" )													");
						oString.AppendLine(oSubQueryBuilder.ToString());

						oParamList.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
						oParamList.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
					}
				}
			}

			if (pCondition.hasExtend) {
				oString.AppendLine(SetExtednCondition(pCondition,ref oParamList));
			}

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
					string sUserSeq = string.Empty;
					string sUserCharNo = string.Empty;
					if (this.sessionObj != null) {
						if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
							SessionMan oSessionMan = (SessionMan)this.sessionObj;
							sUserSeq = oSessionMan.userMan.userSeq;
							sUserCharNo = oSessionMan.userMan.userCharNo;
						}
					}
					if (!sUserSeq.Equals(string.Empty)) {
						oString.AppendLine(" AND NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)");
						oParamList.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
						oParamList.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
					}
				}
			}

			//やきもち防止 
			ArrayList list = new ArrayList();
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				oString.Append(SetFlexibleQuery(this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope),oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list));
			}

			oString.Append("	GROUP BY").AppendLine();
			oString.Append("		P.SITE_CD,P.NA_FLAG ");
			if (!pUserSeq.Equals(string.Empty)) {
				oString.Append(",P.USER_SEQ,P.USER_CHAR_NO");
			}

			if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
				oString.Append(",P.OBJ_NA_FLAG");
			} else {
				if (!string.IsNullOrEmpty(pApproveStatus)) {
					oString.Append(",P.OBJ_NOT_APPROVE_FLAG");
				}
				oString.Append(",P.OBJ_NOT_PUBLISH_FLAG");
			}
			if (!pObjKind.Equals(string.Empty)) {
				oString.Append(",P.OBJ_KIND");
			}
			if (!pActCategorySeq.Equals(string.Empty)) {
				oString.Append(",P.ACT_CATEGORY_SEQ	");
			}
			oString.Append(",P.CAST_OBJ_ATTR_TYPE_SEQ,P.CAST_OBJ_ATTR_SEQ	) T1,").AppendLine();

			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE		").AppendLine();
			} else {
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE_VALUE	").AppendLine();
			}
			oString.Append("WHERE									").AppendLine();
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD					= :SITE_CD							AND	").AppendLine();
				oString.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ	= T1.CAST_OBJ_ATTR_TYPE_SEQ		(+)	AND	").AppendLine();
				oString.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ		= T1.CAST_OBJ_ATTR_SEQ			(+)		").AppendLine();
			} else {
				oString.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.SITE_CD					= :SITE_CD						AND	").AppendLine();
				oString.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_TYPE_SEQ	= T1.CAST_OBJ_ATTR_TYPE_SEQ	(+)	AND	").AppendLine();
				oString.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_SEQ		= T1.CAST_OBJ_ATTR_SEQ		(+)		").AppendLine();
			}
			oString.Append("ORDER BY									").AppendLine();
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("T_CAST_PIC_ATTR_TYPE_VALUE.PRIORITY		").AppendLine();
			} else {
				oString.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.PRIORITY	").AppendLine();
			}
			using (cmd = CreateSelectCommand(oString.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);

				if (!pUserSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}

				if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
					cmd.Parameters.Add("OBJ_NA_FLAG",ViCommConst.FLAG_OFF);
				} else {
					if (!string.IsNullOrEmpty(pApproveStatus)) {
						cmd.Parameters.Add("OBJ_NOT_APPROVE_FLAG",pApproveStatus);
					}
					cmd.Parameters.Add("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF);
				}

				if (!pObjKind.Equals(string.Empty)) {
					cmd.Parameters.Add("OBJ_KIND",pObjKind);
				} else {
					cmd.Parameters.Add("OBJ_KIND1",ViCommConst.BbsObjType.PIC);
					cmd.Parameters.Add("OBJ_KIND2",ViCommConst.BbsObjType.MOVIE);
				}
				if (!pActCategorySeq.Equals(string.Empty)) {
					cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);
				}
				if (pRecentNotLogin) {
					cmd.Parameters.Add("LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Now.AddDays(ViCommConst.FREE_BBS_VIEW_NOT_LOGIN_DAYS * -1),ParameterDirection.Input);
				}
				cmd.Parameters.AddRange(oParamList.ToArray());
				//やきもち防止
				if (list != null) {
					OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
					for (int i = 0;i < objParms.Length;i++) {
						cmd.Parameters.Add((OracleParameter)objParms[i]);
					}
				}
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_BBS_OBJS00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	public DataSet GetObjAttrCountNew(string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pObjKind,string pApproveStatus,bool pRecentNotLogin,bool pUseMV,bool pOnlyUsed,SeekCondition pCondition) {
		DataSet ds;
		try {
			conn = DbConnect("CastBbsObj.GetObjAttrCount");

			StringBuilder oString = new StringBuilder();
			List<OracleParameter> oParamList = new List<OracleParameter>();

			string sSubQueryAll = string.Empty;
			string sSubQueryJealousy = string.Empty;
			oParamList.AddRange(CreateObjAttrSubQuery(FlexibleQueryType.None,pSiteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjKind,pApproveStatus,pRecentNotLogin,pUseMV,pOnlyUsed,pCondition,out sSubQueryAll));
			oParamList.AddRange(CreateObjAttrSubQuery(FlexibleQueryType.InScope,pSiteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjKind,pApproveStatus,pRecentNotLogin,pUseMV,pOnlyUsed,pCondition,out sSubQueryJealousy));

			oString.Append("SELECT				").AppendLine();
			oString.AppendFormat("	NVL((		").AppendLine();
			oString.AppendFormat("	{0}			",sSubQueryAll).AppendLine();
			oString.AppendFormat("	),0)		").AppendLine();
			oString.AppendFormat("	-			").AppendLine();
			oString.AppendFormat("	NVL((		").AppendLine();
			oString.AppendFormat("	{0}			",sSubQueryJealousy).AppendLine();
			oString.AppendFormat("	),0)		").AppendLine();
			oString.AppendFormat("	AS CNT  ,	").AppendLine(); // 総ﾚｺｰﾄﾞ-ヤキモチ防止に該当するﾚｺｰﾄﾞ


			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ		AS OBJ_ATTR_TYPE_SEQ,	").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ			AS OBJ_ATTR_SEQ		,	").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_NM				AS OBJ_ATTR_NM			").AppendLine();
				oString.Append("FROM								").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE							").AppendLine();
				oString.Append("WHERE													").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD	= :SITE_CD		").AppendLine();
				oString.Append("ORDER BY												").AppendLine();
				oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.PRIORITY					").AppendLine();
			} else {
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_TYPE_SEQ	AS OBJ_ATTR_TYPE_SEQ,	").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_SEQ		AS OBJ_ATTR_SEQ		,	").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_NM			AS OBJ_ATTR_NM			").AppendLine();
				oString.Append("FROM								").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE_VALUE						").AppendLine();
				oString.Append("WHERE													").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE_VALUE.SITE_CD = :SITE_CD		").AppendLine();
				oString.Append("ORDER BY												").AppendLine();
				oString.Append("	T_CAST_MOVIE_ATTR_TYPE_VALUE.PRIORITY				").AppendLine();
			}

			using (cmd = CreateSelectCommand(oString.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.AddRange(oParamList.ToArray());
				cmd.BindByName = true;
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_BBS_OBJS00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private List<OracleParameter> CreateObjAttrSubQuery(FlexibleQueryType pFlexibleQueryType,string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pObjKind,string pApproveStatus,bool pRecentNotLogin,bool pUseMV,bool pOnlyUsed,SeekCondition pCondition,out string pQuery) {
		string sSuffix = "VW";
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			sSuffix = "MV";
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oString = new StringBuilder();


		oString.Append("	SELECT							").AppendLine();
		oString.Append("		COUNT(*) AS CNT				").AppendLine();
		oString.Append("	FROM							").AppendLine();
		oString.Append(string.Format("{0}_BBS_OBJS00 P	",sSuffix)).AppendLine();
		if (pCondition != null && pCondition.hasExtend) {
			for (int i = 0;i < pCondition.attrValue.Count;i++) {
				if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
					oString.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
				}
			}
		}
		oString.Append("	WHERE							").AppendLine();
		if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
			oString.Append("P.SITE_CD					= T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD					AND ").AppendLine();
			oString.Append("P.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ		AND	").AppendLine();
			oString.Append("P.CAST_OBJ_ATTR_SEQ			= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ			AND	").AppendLine();
		} else {
			oString.Append("P.SITE_CD					= T_CAST_MOVIE_ATTR_TYPE_VALUE.SITE_CD					AND ").AppendLine();
			oString.Append("P.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_TYPE_SEQ		AND	").AppendLine();
			oString.Append("P.CAST_OBJ_ATTR_SEQ			= T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_SEQ			AND	").AppendLine();
		}

		oString.Append("		P.NA_FLAG					=:NA_FLAG						").AppendLine();
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		if (!pUserSeq.Equals(string.Empty)) {
			oString.Append("	AND P.USER_SEQ		=:USER_SEQ				").AppendLine();
			oString.Append("	AND P.USER_CHAR_NO	=:USER_CHAR_NO			").AppendLine();
			oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		}

		if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
			oString.Append("	AND P.OBJ_NA_FLAG			=:OBJ_NA_FLAG			").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",ViCommConst.FLAG_OFF));

		} else {
			if (!string.IsNullOrEmpty(pApproveStatus)) {
				oString.Append("	AND P.OBJ_NOT_APPROVE_FLAG	=:OBJ_NOT_APPROVE_FLAG	").AppendLine();
				oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",pApproveStatus));
			}
			oString.Append("	AND P.OBJ_NOT_PUBLISH_FLAG	=:OBJ_NOT_PUBLISH_FLAG	").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));
		}


		if (!pObjKind.Equals(string.Empty)) {
			oString.Append("	AND P.OBJ_KIND		=:OBJ_KIND				").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_KIND",pObjKind));

		} else {
			oString.Append("	AND P.OBJ_KIND IN	(:OBJ_KIND1,:OBJ_KIND2)	").AppendLine();
			oParamList.Add(new OracleParameter(":OBJ_KIND1",ViCommConst.BbsObjType.PIC));
			oParamList.Add(new OracleParameter(":OBJ_KIND2",ViCommConst.BbsObjType.MOVIE));

		}
		if (!pActCategorySeq.Equals(string.Empty)) {
			oString.Append("	AND P.ACT_CATEGORY_SEQ	=:ACT_CATEGORY_SEQ	").AppendLine();
			oParamList.Add(new OracleParameter(":ACT_CATEGORY_SEQ",pActCategorySeq));

		}
		if (pRecentNotLogin) {
			oString.Append("	AND	P.LAST_LOGIN_DATE	<:LAST_LOGIN_DATE	").AppendLine();
			oParamList.Add(new OracleParameter(":LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Now.AddDays(ViCommConst.FREE_BBS_VIEW_NOT_LOGIN_DAYS * -1),ParameterDirection.Input));
		}
		if (pOnlyUsed) {
			StringBuilder oSubQueryBuilder = new StringBuilder();
			oSubQueryBuilder.AppendLine(" AND EXISTS( ");
			oSubQueryBuilder.AppendLine("	SELECT										");
			oSubQueryBuilder.AppendLine("		1										");
			oSubQueryBuilder.AppendLine("	FROM										");
			oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
			oSubQueryBuilder.AppendLine("	WHERE										");
			oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
			oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
			oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
			oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
			oSubQueryBuilder.AppendLine(" ) ");
			oString.AppendLine(oSubQueryBuilder.ToString());

			oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
			oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
			} else {
				oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
			}
		} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			StringBuilder oSubQueryBuilder = new StringBuilder();
			oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
			oSubQueryBuilder.AppendLine("	SELECT										");
			oSubQueryBuilder.AppendLine("		1										");
			oSubQueryBuilder.AppendLine("	FROM										");
			oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
			oSubQueryBuilder.AppendLine("	WHERE										");
			oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
			oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
			oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
			oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.OBJ_SEQ	");
			oSubQueryBuilder.AppendLine(" ) ");
			oString.AppendLine(oSubQueryBuilder.ToString());

			oParamList.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
			oParamList.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
			if (pObjKind.Equals(ViCommConst.BbsObjType.PIC)) {
				oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));
			} else {
				oParamList.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
			}
		}

		if (!string.IsNullOrEmpty(pCondition.rankType)) {
			oString.AppendLine(" AND P.OBJ_RANKING_FLAG = 1");
		}

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
			if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SessionMan sessionMan = (SessionMan)sessionObj;
				string sSelfUserSeq = sessionMan.userMan.userSeq;
				string sSelfUserCharNo = sessionMan.userMan.userCharNo;

				if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
					StringBuilder oSubQueryBuilder = new StringBuilder();
					oSubQueryBuilder.AppendLine(" AND EXISTS(										");
					oSubQueryBuilder.AppendLine("	SELECT											");
					oSubQueryBuilder.AppendLine("		1											");
					oSubQueryBuilder.AppendLine("	FROM											");
					oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
					oSubQueryBuilder.AppendLine("	WHERE											");
					oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
					oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.OBJ_SEQ			AND	");
					oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
					oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
					oSubQueryBuilder.AppendLine(" )													");
					oString.AppendLine(oSubQueryBuilder.ToString());

					oParamList.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
					oParamList.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
				}
			}
		}

		if (pCondition.hasExtend) {
			oString.AppendLine(SetExtednCondition(pCondition,ref oParamList));
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					oString.AppendLine(" AND NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)");
					oParamList.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					oParamList.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}

		//やきもち防止 
		ArrayList list = new ArrayList();
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			oString.Append(SetFlexibleQuery(this.CreateFlexibleQueryTarget(pFlexibleQueryType),oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list));
			foreach (OracleParameter oParam in list) {
				oParamList.Add(oParam);
			}

		}

		oString.Append("	GROUP BY").AppendLine();
		oString.Append("		P.SITE_CD,P.NA_FLAG ");
		if (!pUserSeq.Equals(string.Empty)) {
			oString.Append(",P.USER_SEQ,P.USER_CHAR_NO");
		}

		if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
			oString.Append(",P.OBJ_NA_FLAG");
		} else {
			if (!string.IsNullOrEmpty(pApproveStatus)) {
				oString.Append(",P.OBJ_NOT_APPROVE_FLAG");
			}
			oString.Append(",P.OBJ_NOT_PUBLISH_FLAG");
		}
		if (!pObjKind.Equals(string.Empty)) {
			oString.Append(",P.OBJ_KIND");
		}
		if (!pActCategorySeq.Equals(string.Empty)) {
			oString.Append(",P.ACT_CATEGORY_SEQ	");
		}
		oString.Append(",P.CAST_OBJ_ATTR_TYPE_SEQ,P.CAST_OBJ_ATTR_SEQ	").AppendLine();


		pQuery = oString.ToString();
		return oParamList;
	}

	public DataSet GetPageCollectionRandom(
		string pSiteCd,
		string pCastBbsObjAttrTypeSeq,
		string pCastBbsObjAttrSeq,
		string pObjKind,
		SeekCondition pCondition,
		int pNeedCount,
		out decimal pTotalRowCount,
		bool pUseMV) {
		DataSet ds;
		try {
			conn = DbConnect("CastBbsObj.GetPageCollectionRandom");
			ds = new DataSet();

			string sOrder = string.Empty;
			//string sOrder = " ORDER BY DBMS_RANDOM.RANDOM ";

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBbsObjs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder sInnerSql = new StringBuilder();
			sInnerSql.Append("SELECT * FROM( ");
			sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT " + CastBasicField() + ",").AppendLine();
			sInnerSql.Append("			P.OBJ_KIND				,").AppendLine();
			sInnerSql.Append("			P.OBJ_SEQ				,").AppendLine();
			sInnerSql.Append("			P.OBJ_TITLE				,").AppendLine();
			sInnerSql.Append("			P.OBJ_DOC				,").AppendLine();
			sInnerSql.Append("			P.OBJ_NOT_APPROVE_FLAG	,").AppendLine();
			sInnerSql.Append("			P.OBJ_NOT_PUBLISH_FLAG	,").AppendLine();
			sInnerSql.Append("			P.READING_COUNT			,").AppendLine();
			sInnerSql.Append("			P.OBJ_UPLOAD_DATE		,").AppendLine();
			sInnerSql.Append("			P.CAST_OBJ_ATTR_TYPE_SEQ,").AppendLine();
			sInnerSql.Append("			P.CAST_OBJ_ATTR_SEQ		 ").AppendLine();
			sInnerSql.Append(string.Format("FROM {0}_BBS_OBJS00 P ",sSuffix));

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereRandom(pSiteCd,pCastBbsObjAttrTypeSeq,pCastBbsObjAttrSeq,pObjKind,pCondition,pNeedCount,ref sWhere,out pTotalRowCount);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(")INNER ) ");

			SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			string sHint = string.Empty;
			if (oObj.sqlHint.ContainsKey("CAST")) {
				sHint = oObj.sqlHint["CAST"].ToString();
			}

			StringBuilder sTables = new StringBuilder();
			sTables.Append("T_CAST_PIC					T_OBJ_PIC	,").AppendLine();
			sTables.Append("T_CAST_MOVIE				T_OBJ_MOVIE	,").AppendLine();
			sTables.Append("T_CAST_PIC_ATTR_TYPE		,").AppendLine();
			sTables.Append("T_CAST_PIC_ATTR_TYPE_VALUE	,");

			StringBuilder sJoinTable = new StringBuilder();
			sJoinTable.Append("T1.OBJ_SEQ					= T_OBJ_PIC.PIC_SEQ									(+)	AND").AppendLine();
			sJoinTable.Append("T1.OBJ_SEQ					= T_OBJ_MOVIE.MOVIE_SEQ								(+)	AND").AppendLine();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_PIC_ATTR_TYPE.SITE_CD						(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ		(+)	AND").AppendLine();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD				(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_OBJ_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ	(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_OBJ_ATTR_SEQ			= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ		(+) AND");

			StringBuilder sJoinField = new StringBuilder();

			//PICとMOVIEのみの項目------------------------------------------------
			sJoinField.Append("T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_NM	AS CAST_OBJ_ATTR_TYPE_NM,").AppendLine();
			sJoinField.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_NM	AS CAST_OBJ_ATTR_NM		,").AppendLine();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開中', '非公開'), '申請中') AS UNAUTH_MARK,").AppendLine();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開', '非公開'), '認証待ち')	AS UNAUTH_MARK_ADMIN,").AppendLine();
			//PICのみの項目-------------------------------------------------------
			sJoinField.Append("T_OBJ_PIC.PIC_SEQ				,").AppendLine();
			sJoinField.Append("GET_PHOTO_IMG_PATH				(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_PHOTO_IMG_PATH			,").AppendLine();
			sJoinField.Append("GET_SMALL_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_SMALL_PHOTO_IMG_PATH		,").AppendLine();
			sJoinField.Append("GET_BLUR_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_BLUR_PHOTO_IMG_PATH		,").AppendLine();
			sJoinField.Append("GET_SMALL_BLUR_PHOTO_IMG_PATH	(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_PIC.PIC_SEQ) AS OBJ_SMALL_BLUR_PHOTO_IMG_PATH	,").AppendLine();
			//MOVIEのみの項目-------------------------------------------------------
			sJoinField.Append("T_OBJ_MOVIE.MOVIE_TYPE			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.MOVIE_SEQ			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.CHARGE_POINT			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.PLAY_TIME			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.MOVIE_SERIES			,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.SAMPLE_MOVIE_SEQ		,").AppendLine();
			sJoinField.Append("T_OBJ_MOVIE.THUMBNAIL_PIC_SEQ	,").AppendLine();
			sJoinField.Append("GET_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_MOVIE.THUMBNAIL_PIC_SEQ,T1.PROFILE_PIC_SEQ) AS THUMBNAIL_IMG_PATH,");

			sSql = string.Format(sSql,sInnerSql,sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_BBS_OBJS00");
				}
			}
		} finally {
			conn.Close();
		}

		AppendAttr(ds);

		return ds;
	}

	private OracleParameter[] CreateWhereRandom(string pSiteCd,string pCastBbsObjAttrTypeSeq,string pCastBbsObjAttrSeq,string pObjKind,SeekCondition pCondition,int pNeedCount,ref string pWhere,out decimal pTotalCount) {
		SessionMan sessionMan = (SessionMan)sessionObj;
		int iRecordCount = 0;
		string[] sObjSeq;
		ArrayList list = new ArrayList();
		InnnerCondition(ref list);
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_RANDOM_OBJ");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,sessionMan.userMan.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("POBJ_KIND",DbSession.DbType.VARCHAR2,pObjKind);
			db.ProcedureInParm("POBJ_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pCastBbsObjAttrTypeSeq);
			db.ProcedureInParm("POBJ_ATTR_SEQ",DbSession.DbType.VARCHAR2,pCastBbsObjAttrSeq);
			db.ProcedureInParm("PUNUSED_FLAG",DbSession.DbType.VARCHAR2,pCondition.unusedFlag);
			db.ProcedureInParm("PNEED_COUNT",DbSession.DbType.NUMBER,pNeedCount);
			db.ProcedureOutArrayParm("POBJ_SEQ",DbSession.DbType.VARCHAR2,pNeedCount);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTOTAL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iRecordCount = db.GetIntValue("PRECORD_COUNT");
			pTotalCount = (decimal)(db.GetIntValue("PTOTAL_COUNT"));

			sObjSeq = new string[iRecordCount];
			for (int i = 0;i < iRecordCount;i++) {
				sObjSeq[i] = db.GetArryStringValue("POBJ_SEQ",i);
			}
		}

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("ROWNUM<= :P_ROWNUM",ref pWhere);
		list.Add(new OracleParameter("P_ROWNUM",iRecordCount));

		StringBuilder oSqlIn = new StringBuilder();

		for (int i = 0;i < iRecordCount;i++) {
			if (i == 0) {
				oSqlIn.Append(" P.OBJ_SEQ IN ( ");
				oSqlIn.Append(string.Format(":OBJ_SEQ{0}",i));
			} else {
				oSqlIn.Append(string.Format(",:OBJ_SEQ{0}",i));
			}
			list.Add(new OracleParameter(string.Format("OBJ_SEQ{0}",i),sObjSeq[i]));
		}

		if (iRecordCount > 0) {
			oSqlIn.Append(" ) ");
			SysPrograms.SqlAppendWhere(oSqlIn.ToString(),ref pWhere);
		}

		// SP側の抽出で対応しているため不要


		//		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
		//			SessionMan oSessionMan = (SessionMan)this.sessionObj;
		//			pWhere += SetFlexibleQuery(this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope),oSessionMan.site.siteCd, oSessionMan.userMan.userSeq, oSessionMan.userMan.userCharNo, ref list);
		//		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void CheckSpecialFreeBbsObj(string pSiteCd,string pUserSeq,string pUserCharNo,string pObjSeq,out string pFreeOkFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_SPECIAL_FREE_BBS_OBJ");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,pObjSeq);
			db.ProcedureOutParm("pFREE_OK_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pFreeOkFlag = db.GetStringValue("pFREE_OK_FLAG");
		}
	}
	
	public bool CheckExistsByObjSeq(string pObjSeq) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	1						");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_CAST_BBS_OBJS			");
		oSqlBuilder.AppendLine("WHERE						");
		oSqlBuilder.AppendLine("	OBJ_SEQ	= :OBJ_SEQ		");
		
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}

