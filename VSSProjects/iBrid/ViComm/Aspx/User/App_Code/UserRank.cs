﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーランク
--	Progaram ID		: UserRank
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

[System.Serializable]
public class UserRank:DbSession {

	public string userRank;
	public string userRanNm;
	public string colorBack;
	public string colorChar;
	public string colorLine;
	public string colorLink;
	public string colorALink;
	public string colorVLink;
	public int sizeLine;
	public int sizeChar;

	public UserRank() {
	}

	public UserRank(string pSiteCd,string pUserRank,string pSexCd) {
		userRank = pUserRank;
		userRanNm = string.Empty;
		colorBack = string.Empty;
		colorChar = string.Empty;
		colorLine = string.Empty;
		colorLink = string.Empty;
		colorALink = string.Empty;
		colorVLink = string.Empty;
		sizeLine = 0;
		sizeChar = 0;
		GetOne(pSiteCd,pUserRank,pSexCd);
	}


	public bool GetOne(string pSiteCd,string pUserRank,string pSexCd) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect("UserRank.GetOne");

			string sSql = "SELECT " +
							"USER_RANK			," +
							"COLOR_LINE			," +
							"COLOR_INDEX		," +
							"COLOR_CHAR			," +
							"COLOR_BACK			," +
							"COLOR_LINK			," +
							"COLOR_ALINK		," +
							"COLOR_VLINK		," +
							"SIZE_LINE			," +
							"SIZE_CHAR			," +
							"COLOR_LINE_WOMAN	," +
							"COLOR_INDEX_WOMAN	," +
							"COLOR_CHAR_WOMAN	," +
							"COLOR_BACK_WOMAN	," +
							"COLOR_LINK_WOMAN	," +
							"COLOR_ALINK_WOMAN	," +
							"COLOR_VLINK_WOMAN	," +
							"SIZE_LINE_WOMAN	," +
							"SIZE_CHAR_WOMAN	," +
							"USER_RANK_NM		 " +
						"FROM " +
							"VW_USER_RANK01 " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND USER_RANK = :USER_RANK";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_RANK",pUserRank);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_RANK01");

					if (ds.Tables["VW_USER_RANK01"].Rows.Count != 0) {
						dr = ds.Tables["VW_USER_RANK01"].Rows[0];
						userRank = dr["USER_RANK"].ToString();
						userRanNm = dr["USER_RANK_NM"].ToString();
						if (pSexCd.Equals(ViComm.ViCommConst.MAN)) {
							colorBack = dr["COLOR_BACK"].ToString();
							colorChar = dr["COLOR_CHAR"].ToString();
							colorLine = dr["COLOR_LINE"].ToString();
							colorLink = dr["COLOR_LINK"].ToString();
							colorALink = dr["COLOR_ALINK"].ToString();
							colorVLink = dr["COLOR_VLINK"].ToString();
							sizeChar = int.Parse(dr["SIZE_CHAR"].ToString());
							sizeLine = int.Parse(dr["SIZE_LINE"].ToString());
						}else{
							colorBack = dr["COLOR_BACK_WOMAN"].ToString();
							colorChar = dr["COLOR_CHAR_WOMAN"].ToString();
							colorLine = dr["COLOR_LINE_WOMAN"].ToString();
							colorLink = dr["COLOR_LINK_WOMAN"].ToString();
							colorALink = dr["COLOR_ALINK_WOMAN"].ToString();
							colorVLink = dr["COLOR_VLINK_WOMAN"].ToString();
							sizeChar = int.Parse(dr["SIZE_CHAR_WOMAN"].ToString());
							sizeLine = int.Parse(dr["SIZE_LINE_WOMAN"].ToString());
						}
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
