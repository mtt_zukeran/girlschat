﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MobileLib;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Omikuji:DbSession {

	public Omikuji() {
	}

	public void AddOmikujiPoint(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,out string pOmikujiNo,out int pOmikujiPoint) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_OMIKUJI_POINT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureOutParm("pOMIKUJI_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pOMIKUJI_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pOmikujiNo = db.GetStringValue("pOMIKUJI_NO");
			int.TryParse(db.GetStringValue("pOMIKUJI_POINT"),out pOmikujiPoint);
		}
	}
}
