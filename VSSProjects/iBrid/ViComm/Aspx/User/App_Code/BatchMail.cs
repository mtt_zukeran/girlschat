﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 一括送信メール
--	Progaram ID		: BatchMail
--  Creation Date	: 2010.05.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System.Data;
using System.Collections;
using System.Text;
using System;
using ViComm;

/// <summary>
/// 一括メール送信クラス
/// </summary>
[System.Serializable]
public class BatchMail:DbSession {
	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,int pRecPerPage,out decimal totalRowCount) {
		DataSet ds;
		int iPages = 0;
		totalRowCount = 0;
		try {
			conn = DbConnect("BatchMail.GetPageCount");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_BATCH_MAIL_LOG01 ";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {

				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				da.Fill(ds);

				if (ds.Tables[0].Rows.Count != 0) {
					DataRow dr = ds.Tables[0].Rows[0];
					totalRowCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(totalRowCount / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try {
			conn = DbConnect("BatchMail.GetPageCollection");
			ds = new DataSet();

			string sOrder = " ORDER BY REQUEST_TX_MAIL_SEQ DESC ";
			string sSql = string.Format("SELECT * FROM (SELECT VW_BATCH_MAIL_LOG01.*, ROW_NUMBER() OVER ({0}) AS RNUM FROM VW_BATCH_MAIL_LOG01 ",sOrder);
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ") WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_REQ_TX_MAIL");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = " WHERE TX_SITE_CD = :TX_SITE_CD AND TX_USER_SEQ = :TX_USER_SEQ AND TX_USER_CHAR_NO = :TX_USER_CHAR_NO AND WAIT_TX_FLAG = :WAIT_TX_FLAG ";
		list.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		list.Add(new OracleParameter("TX_USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("TX_USER_CHAR_NO",pUserCharNo));
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCountByBatchManage(string pSiteCd,string pUserSeq,string pUserCharNo,int pRecPerPage,out decimal totalRowCount) {
		DataSet ds;
		int iPages = 0;
		totalRowCount = 0;
		try {
			conn = DbConnect("BatchMail.GetPageCountByBatchManage");

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhereByBatchManage(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);

			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	COUNT(*) AS ROW_COUNT	");
			sSql.AppendLine("FROM	");
			sSql.AppendLine("	VW_REQ_TX_MAIL_DTL03	");
			sSql.Append(sWhere);
			sSql.AppendLine("GROUP BY	");
			sSql.AppendLine("	REQUEST_TX_MAIL_SEQ	");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {

				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				da.Fill(ds);

				if (ds.Tables[0].Rows.Count != 0) {
					DataRow dr = ds.Tables[0].Rows[0];
					totalRowCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(totalRowCount / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollectionByBatchManage(string pSiteCd,string pUserSeq,string pUserCharNo,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try {
			conn = DbConnect("BatchMail.GetPageCollectionByBatchManage");
			ds = new DataSet();

			string sOrder = " ORDER BY REQUEST_TX_MAIL_SEQ DESC ";
			string sWhere = "";
			OracleParameter[] objParms = CreateWhereByBatchManage(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	*	");
			sSql.AppendLine("FROM	");
			sSql.AppendLine("	(SELECT	");
			sSql.AppendLine("		ROWNUM AS RNUM	,");
			sSql.AppendLine("		INNER.*	");
			sSql.AppendLine("	FROM	");
			sSql.AppendLine("		(SELECT	");
			sSql.AppendLine("			T2.REQUEST_TX_MAIL_SEQ		,");
			sSql.AppendLine("			T2.REQUEST_TX_DATE			,");
			sSql.AppendLine("			T2.TX_SITE_CD				,");
			sSql.AppendLine("			T2.TX_USER_SEQ				,");
			sSql.AppendLine("			T2.TX_USER_CHAR_NO			,");
			sSql.AppendLine("			T2.TX_SEX_CD				,");
			sSql.AppendLine("			T2.TX_HANDLE_NM				,");
			sSql.AppendLine("			T2.TX_LOGIN_ID				,");
			sSql.AppendLine("			T2.TX_USER_IMG_PATH			,");
			sSql.AppendLine("			T2.TX_ATTR1					,");
			sSql.AppendLine("			T2.TX_ATTR2					,");
			sSql.AppendLine("			T2.RX_SEX_CD				,");
			sSql.AppendLine("			T2.ORIGINAL_TITLE  AS MAIL_TITLE,");
			sSql.AppendLine("			T2.ORIGINAL_DOC1			,");
			sSql.AppendLine("			T2.ORIGINAL_DOC2			,");
			sSql.AppendLine("			T2.ORIGINAL_DOC3			,");
			sSql.AppendLine("			T2.ORIGINAL_DOC4			,");
			sSql.AppendLine("			T2.ORIGINAL_DOC5			,");
			sSql.AppendLine("			T2.REGIST_TRACKING_URL		,");
			sSql.AppendLine("			T2.BATCH_MAIL_FLAG			,");
			sSql.AppendLine("			T2.MAIL_TEMPLATE_SITE_CD	,");
			sSql.AppendLine("			T2.MAIL_TEMPLATE_NO			,");
			sSql.AppendLine("			T2.ATTACHED_OBJ_TYPE		,");
			sSql.AppendLine("			T2.MAIL_ATTACHED_METHOD		,");
			sSql.AppendLine("			T2.PIC_SEQ					,");
			sSql.AppendLine("			T2.MOVIE_SEQ				,");
			sSql.AppendLine("			T2.OBJ_TEMP_ID				,");
			sSql.AppendLine("			T2.WAIT_TX_FLAG				,");
			sSql.AppendLine("			T3.MAIL_COUNT	");
			sSql.AppendLine("		FROM	");
			sSql.AppendLine("			T_REQ_TX_MAIL T2,	");
			sSql.AppendLine("			(SELECT				");
			sSql.AppendLine("				REQUEST_TX_MAIL_SEQ,	");
			sSql.AppendLine("				COUNT(*) AS MAIL_COUNT	");
			sSql.AppendLine("			FROM	");
			sSql.AppendLine("				VW_REQ_TX_MAIL_DTL03			");
			sSql.AppendLine(sWhere);
			sSql.AppendLine("			GROUP BY	");
			sSql.AppendLine("				REQUEST_TX_MAIL_SEQ	");
			sSql.AppendLine(sOrder);
			sSql.AppendLine("			) T3	");
			sSql.AppendLine("		WHERE	");
			sSql.AppendLine("			T3.REQUEST_TX_MAIL_SEQ = T2.REQUEST_TX_MAIL_SEQ	");
			sSql.AppendLine("		)INNER	");
			sSql.AppendLine("	WHERE	");
			sSql.AppendLine("		ROWNUM <= :MAX_ROW)	");
			sSql.AppendLine("WHERE	");
			sSql.AppendLine("	RNUM >= :FIRST_ROW	AND	");
			sSql.AppendLine("	RNUM <= :LAST_ROW		");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_REQ_TX_MAIL");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetNotSendBatchMailCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		int iCount = 0;
		try {
			conn = DbConnect("BatchMail.GetNoSendBatchMailCount");

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhereByBatchManage(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);

			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	COUNT(*) AS ROW_COUNT	");
			sSql.AppendLine("FROM	");
			sSql.AppendLine("	VW_REQ_TX_MAIL_DTL03	");
			sSql.Append(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {

				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				da.Fill(ds);

				if (ds.Tables[0].Rows.Count != 0) {
					DataRow dr = ds.Tables[0].Rows[0];
					iCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iCount;
	}
	private OracleParameter[] CreateWhereByBatchManage(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();
		StringBuilder sWhere = new StringBuilder();
		sWhere.AppendLine("	WHERE	");
		sWhere.AppendLine("		TX_SITE_CD		= :TX_SITE_CD		AND	");
		sWhere.AppendLine("		TX_USER_SEQ		= :TX_USER_SEQ		AND	");
		sWhere.AppendLine("		TX_USER_CHAR_NO	= :TX_USER_CHAR_NO	AND	");
		sWhere.AppendLine("		REQUEST_TX_DATE	>= TRUNC(SYSDATE)	AND	");
		sWhere.AppendLine("		WAIT_TX_FLAG	= :WAIT_TX_FLAG		AND	");
		sWhere.AppendLine("		BATCH_MAIL_FLAG	= :BATCH_MAIL_FLAG		");

		list.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		list.Add(new OracleParameter("TX_USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("TX_USER_CHAR_NO",pUserCharNo));
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_ON));
		list.Add(new OracleParameter("BATCH_MAIL_FLAG",ViCommConst.FLAG_ON));

		pWhere = sWhere.ToString();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int CancelBatchMail(string[] pMailSeq,string[] pMailCount) {
		int iCancelCount = 0;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CANCEL_BATCH_MAIL");
			db.ProcedureInArrayParm("PREQUEST_TX_MAIL_SEQ",DbType.VARCHAR2,pMailSeq.Length,pMailSeq);
			db.ProcedureInArrayParm("PMAIL_COUNT",DbType.VARCHAR2,pMailCount.Length,pMailCount);
			db.ProcedureInParm("PATTR_COUNT",DbType.NUMBER,pMailSeq.Length);
			db.ProcedureOutParm("PSUCCESS_CANCEL_COUNT",DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();

			iCancelCount = int.Parse(db.GetStringValue("PSUCCESS_CANCEL_COUNT"));
		}

		return iCancelCount;
	}
}
