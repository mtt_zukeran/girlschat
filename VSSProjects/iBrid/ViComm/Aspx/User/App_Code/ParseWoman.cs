﻿using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Collections;
using System.Web;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

[Serializable]
public partial class ParseWoman:ParseViComm {

	public SessionWoman sessionWoman;

	public ParseWoman() {
	}
	public ParseWoman(string pPattern,bool pIsCrawler,string pSessionId,string pDomain,string pBasePath,RegexOptions pOption,string pCarrierCd,SessionWoman pSessionWoman)
		: base(pPattern,pIsCrawler,pSessionId,pDomain,pBasePath,pOption,pCarrierCd,pSessionWoman) {
		sessionWoman = pSessionWoman;
		divNestCount = 0;
	}

	public override byte[] Perser(string pTag,string pArgument) {
		string sValue = "";
		bool bParsed = true;

		try {
			if (pTag.StartsWith("$x")) {
				return ConvertEmoji(pTag);
			} else {
				Encoding encSjis = Encoding.GetEncoding(932); // shift-jis

				switch (pTag) {
					#region ---------- セッション開始後利用可能・システム関連 ----------
					//CROSMILE連携
					case "$SELF_CAN_USE_CROSMILE":
						sValue = sessionWoman.IsRegistedCrosmile(sessionWoman.userWoman.crosmileUri) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$ADD_BONUS_POINT":
						AddBonusPoint(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,pArgument);
						break;
					case "$ADD_OMIKUJI_POINT":
						AddOmikujiPoint();
						break;
					case "$SELF_ADD_OMIKUJI_POINT":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].getOmikujiPoint.ToString();
						break;
					case "$SELF_OMIKUJI_NO":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].omikujiNo;
						break;
					//ID・PASS忘れ -> ParseViCommへ移動 
					case "$DIV_IS_LOGINED":
						SetNoneDisplay(!(sessionWoman.logined.Equals(true)));
						break;
					case "$DIV_IS_NOT_LOGINED":
						SetNoneDisplay((sessionWoman.logined.Equals(true)));
						break;

					// icloudメールユーザフラグ
					case "$ICLOUD_MAIL_USER_FLAG": {
							string sIcloudom = "icloud.com";
							string sMecom = "me.com";
							string sUserMail = sessionWoman.userWoman.emailAddr;
							// メールドメインの取得
							string[] sMailAddr = sUserMail.Split('@');
							// メアドなし又は取得できない場合
							if (sMailAddr.Length < 2 || string.IsNullOrEmpty(sUserMail)) {
								// 表示しない
								sValue = "0";
							} else {
								// icloudユーザ
								if (sMailAddr[1] == sIcloudom) {
									sValue = "1";
								}
								// me.comユーザ
								else if (sMailAddr[1] == sMecom) {
									sValue = "1";
								}
								// その他ユーザ
								else
									sValue = "0";
							}
						}
						break;

					// ezwebメールユーザフラグ
					case "$EZWEB_MAIL_USER_FLAG": {
							string sEzweb = "ezweb.ne.jp";
							string sUserMail = sessionWoman.userWoman.emailAddr;
							// メールドメインの取得
							string[] sMailAddr = sUserMail.Split('@');
							// メアドなし又は取得できない場合
							if (sMailAddr.Length < 2 || string.IsNullOrEmpty(sUserMail)) {
								// 表示しない
								sValue = "0";
							} else {
								// ezwebユーザ
								if (sMailAddr[1] == sEzweb) {
									sValue = "1";
								}
								// その他ユーザ
								else
									sValue = "0";
							}
						}
						break;

					#endregion

					#region ---------- セッション開始後利用可能・男性会員 ----------
					case "$MAN_LOGINED_COUNT":
						if (sessionWoman.currentAspx.Equals("UserTop.aspx") && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCastUserTopDataSet"]).Equals("1")) {
							SetFieldValue(pTag,"MAN_LOGINED_COUNT",out sValue);
						} else {
							sValue = GetManStatusCount(pTag,pArgument,ViCommConst.INQUIRY_MAN_LOGINED,string.Empty).ToString();
						}
						break;
					case "$MAN_WAITING_COUNT":
						sValue = GetManStatusCount(pTag,pArgument,ViCommConst.INQUIRY_WAITING_MAN,string.Empty).ToString();
						break;
					case "$MAN_ONLINE_COUNT":
						if (sessionWoman.currentAspx.Equals("UserTop.aspx") && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCastUserTopDataSet"]).Equals("1")) {
							SetFieldValue(pTag,"MAN_ONLINE_COUNT",out sValue);
						} else {
							sValue = GetManStatusCount(pTag,pArgument,ViCommConst.INQUIRY_ONLINE,string.Empty).ToString();
						}
						break;
					case "$MAN_TODAY_LOGINED_COUNT":
						sValue = GetManStatusCount(pTag,pArgument,0,DateTime.Now.AddDays(-1).ToString()).ToString();
						break;
					case "$MAN_CROSMILE_FLAG":
						sValue = GetPartnerCrosmileFlag(pTag);
						break;
					#endregion

					#region ---------- セッション開始後利用可能 ----------
					case "$ADMIN_NEW_REPORT_DATE":
						sValue = GetNewAdminReport(pTag,pArgument,"ADMIN_REPORT_DATE");
						break;
					case "$ADMIN_NEW_REPORT_TITLE":
						sValue = parseContainer.Parse(GetNewAdminReport(pTag,pArgument,"DOC_TITLE"));
						break;
					case "$CAST_LOGINED_COUNT":
					case "$CAST_NEW_COUNT":
					case "$CAST_ONLINE_COUNT":
					case "$CAST_TALKING_COUNT":
					case "$CAST_TVTEL_COUNT":
					case "$CAST_WAITING_COUNT":
						sValue = GetCastCount(pTag,pArgument);
						break;
					#endregion
					
					#region ---------- セッション開始後利用可能・ピックアップ関連 ----------
					case "$PICKUP_PARTS":
						sValue = GetListPickup(pTag,pArgument);
						break;
					#endregion

					#region ---------- PGM依存・IVP関連 ----------
					case "$HREF_VIDEO_TEL":
						sValue = GetHrefVideoTel(pArgument);
						break;
					case "$HREF_VOICE_TEL":
						sValue = GetHrefVoiceTel(pArgument);
						break;
					case "$CALL_TEL":
						if (sessionWoman.errorMessage.Equals("")) {
							sValue = sessionWoman.ivpRequest.dialNo;
						}
						break;
					#endregion

					#region ---------- PGM依存・メール書込み ----------
					case "$DIV_IS_BATCH_MAIL":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["batch"]).Equals("1"));
						break;
					case "$DIV_IS_NOT_BATCH_MAIL":
						SetNoneDisplay(iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["batch"]).Equals("1"));
						break;
					case "$DIV_IS_ATTACHED_PIC":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["mailtype"]).Equals(ViCommConst.ATTACH_PIC_INDEX.ToString()));
						break;
					case "$DIV_IS_ATTACHED_MOVIE":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["mailtype"]).Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString()));
						break;
					case "$DIV_IS_ATTACHED_NOTHING":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["mailtype"]).Equals(ViCommConst.ATTACH_NOTHING_INDEX.ToString()));
						break;
					case "$DIV_IS_ATTACHED_MAILER":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["mailer"]).Equals(ViCommConst.FLAG_ON_STR));
						break;
					case "$DIV_IS_NOT_ATTACHED_MAILER":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["mailer"]).Equals(ViCommConst.FLAG_OFF_STR));
						break;
					case "$DIV_IS_RETURN_MAIL":
						SetNoneDisplay(!sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].isReturnMail);
						break;
					case "$DIV_IS_NOT_RETURN_MAIL":
						SetNoneDisplay(sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].isReturnMail);
						break;
					case "$SELF_IS_NEW_MEMBER":
						sValue = (this.IsNewMember() == true) ? "1" : "0";
						break;
					case "$DIV_IS_WITHIN_START_PERFORM_DAY":
						SetNoneDisplay(!this.IsWithinStartPerformDay(pTag,pArgument));
						break;
					case "$DIV_IS_NOT_WITHIN_START_PERFORM_DAY":
						SetNoneDisplay(this.IsWithinStartPerformDay(pTag,pArgument));
						break;
					case "$SELF_MAIL_ATTACHED_MAILER_TEMP_ID":
						if (sessionWoman.userWoman.mailData.ContainsKey(iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"]))) {
							sValue = sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].objTempId;
						}
						if (string.IsNullOrEmpty(sValue)) {
							string sRequestTxMailSeq;
							if (GetValue(pTag,ViCommConst.DATASET_MAIL,"REQUEST_TX_MAIL_SEQ",out sRequestTxMailSeq)) {
								using (MailLog oMailLog = new MailLog()) {
									sValue = oMailLog.GetObjTempId(sRequestTxMailSeq);
								}
							}
						}
						break;
					case "$SELF_MAIL_ATTACHED_PIC_TITLE":
						sValue = GetAttachedValue("PIC_TITLE");
						break;
					case "$SELF_MAIL_ATTACHED_MOVIE_TITLE":
						sValue = GetAttachedValue("MOVIE_TITLE");
						break;
					case "$SELF_MAIL_ATTACHED_PIC_IMG_PATH":
						sValue = GetAttachedValue("OBJ_PHOTO_IMG_PATH");
						break;
					case "$SELF_MAIL_ATTACHED_PIC_UPLOAD_DATE":
						sValue = GetAttachedValue("UPLOAD_DATE");
						break;
					case "$SELF_MAIL_ATTACHED_MOVIE_UPLOAD_DATE":
						sValue = GetAttachedValue("UPLOAD_DATE");
						break;
					case "$SELF_MAIL_WRITE_TO_MAN_HANDLE_NM":
						sValue = GetSelfMailWriteToManHandleNm();
						sValue = ManHandleNmEmptyFill(sValue);
						break;
					case "$SELF_MAIL_WRITE_TITLE":
						sValue = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,parseContainer.Parse(sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].mailTitle)));
						break;
					case "$SELF_MAIL_WRITE_DOC":
						sValue = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,parseContainer.Parse(sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].mailDoc)));
						break;
					case "$SELF_BATCH_MAIL_SEARCH_COUNT":
						sValue = sessionWoman.BatchMailUserList.Count.ToString();
						break;
					case "$SELF_WAITING_FLAG":
						sValue = IsSelfWaiting();
						break;
					case "$SELF_DUMMY_TALK_FLAG":
						sValue = sessionWoman.userWoman.dummyTalkFlag.ToString();
						break;
					case "$SELF_MAIL_TEMPLATE_NO":
						sValue = sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].mailTemplateNo;
						break;
					case "$SELF_MAIL_ATTACK_LOGIN_FLAG":
						if (sessionWoman.currentAspx.Equals("TxMail.aspx") || sessionWoman.currentAspx.Equals("TxMail01.aspx") ||
								sessionWoman.currentAspx.Equals("ReturnMail.aspx") || sessionWoman.currentAspx.Equals("ReturnMail01.aspx") ||
								sessionWoman.currentAspx.Equals("Profile.aspx")
						) {
							sValue = sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag.ToString();
						}
						break;
					case "$SELF_MAIL_ATTACK_NEW_FLAG":
						if (sessionWoman.currentAspx.Equals("TxMail.aspx") || sessionWoman.currentAspx.Equals("TxMail01.aspx") ||
								sessionWoman.currentAspx.Equals("ReturnMail.aspx") || sessionWoman.currentAspx.Equals("ReturnMail01.aspx") ||
								sessionWoman.currentAspx.Equals("Profile.aspx")
						) {
							sValue = sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag.ToString();
						}
						break;
					// 3分以内メール返信ボーナス対象
					case "$SELF_MAIL_RES_BONUS2_MAIN_FLAG":
						if (sessionWoman.currentAspx.Equals("TxMail.aspx") || sessionWoman.currentAspx.Equals("TxMail01.aspx") ||
								sessionWoman.currentAspx.Equals("ReturnMail.aspx") || sessionWoman.currentAspx.Equals("ReturnMail01.aspx") ||
								sessionWoman.currentAspx.Equals("Profile.aspx")
						) {
							sValue = sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag;
						}
						break;
					// 10分以内メール返信ボーナス対象
					case "$SELF_MAIL_RES_BONUS2_SUB_FLAG":
						if (sessionWoman.currentAspx.Equals("TxMail.aspx") || sessionWoman.currentAspx.Equals("TxMail01.aspx") ||
								sessionWoman.currentAspx.Equals("ReturnMail.aspx") || sessionWoman.currentAspx.Equals("ReturnMail01.aspx") ||
								sessionWoman.currentAspx.Equals("Profile.aspx")
						) {
							sValue = sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag;
						}
						break;

					#endregion

					#region ---------- PGM依存・登録関連 ----------
					case "$REGIST_MAIL_TEMP_ID":
						sValue = GetRegistMailTempId();
						break;
					case "$AFFILIATE_COOKEI_IMG":
						sValue = GetAffiliateCookieImg();
						break;
					#endregion

					#region ---------- 女性会員 ----------
					case "$CONER_PAY_AMT":
						sValue = GetConerPayAmt(pArgument);
						break;
					case "$CONER_PAY_UNIT":
						sValue = GetConerUnit(pArgument);
						break;
					case "$CONER_PAY_POINT":
						sValue = GetConerPayPoint(pArgument);
						break;
					case "$CONER_POINT":
						sValue = GetCornerCharge(pArgument,true);
						break;
					case "$CONER_AMT":
						sValue = GetCornerCharge(pArgument,false);
						break;

					case "$CAST_MINIMUM_PAYMENT_AMT":
						sValue = GetMinimumPayment(false);
						break;
					case "$CAST_MINIMUM_PAYMENT_POINT":
						sValue = GetMinimumPayment(true);
						break;
					case "$CAST_AUTO_MINIMUM_PAYMENT_AMT":
						sValue = GetAutoMinimumPayment(false);
						break;
					case "$CAST_AUTO_MINIMUM_PAYMENT_POINT":
						sValue = GetAutoMinimumPayment(true);
						break;
					case "$DIV_IS_ALL_CHARACTER_OK":
						SetNoneDisplay(!IsAllCharacterOk());
						break;
					case "$DIV_IS_NOT_ALL_CHARACTER_OK":
						SetNoneDisplay(IsAllCharacterOk());
						break;
					case "$DIV_IS_NOT_REGISTED_SITE":
						SetNoneDisplay(sessionWoman.userWoman.characterList.ContainsKey(pArgument + ViCommConst.MAIN_CHAR_NO));
						break;
					case "$DIV_IS_OFFLINE":
						if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
							SetNoneDisplay(!((sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterOnlineStatus.Equals(ViCommConst.USER_OFFLINE)) || (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterOnlineStatus.Equals(ViCommConst.USER_LOGINED))));
						} else {
							SetNoneDisplay(IsAnyCharacterOnline());
						}
						break;
					case "$DIV_IS_ONLINE":
						if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
							SetNoneDisplay(((sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterOnlineStatus.Equals(ViCommConst.USER_OFFLINE)) || (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterOnlineStatus.Equals(ViCommConst.USER_LOGINED))));
						} else {
							SetNoneDisplay(!IsAnyCharacterOnline());
						}
						break;
					case "$DIV_IS_REGISTED_SITE":
						SetNoneDisplay(!(sessionWoman.userWoman.characterList.ContainsKey(pArgument + ViCommConst.MAIN_CHAR_NO)));
						break;
					case "$DIV_IS_HANDLE_NM_DUPLI":
						SetNoneDisplay(!(this.CheckHandleNmDupli()));
						break;
					case "$DIV_IS_NOT_HANDLE_NM_DUPLI":
						SetNoneDisplay((this.CheckHandleNmDupli()));
						break;
					case "$DIV_IS_ADDABLE_BONUS_POINT":
						SetNoneDisplay(!this.CheckBonusPoint(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,pArgument).Equals(ViCommConst.UserDefPointResult.NOT_ADD_POINT));
						break;
					case "$DIV_IS_NOT_ADDABLE_BONUS_POINT":
						SetNoneDisplay(this.CheckBonusPoint(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,pArgument).Equals(ViCommConst.UserDefPointResult.NOT_ADD_POINT));
						break;
					case "$DIV_IS_SELF_ENABLE_PROF_IMG_REG_CP":
						SetNoneDisplay(!this.IsSelfEnableProfileImageRegistCampaign(pTag,pArgument));
						break;

					case "$SELF_AGE":
						sValue = parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].age);
						break;
					//現在選択しているｷｬﾗｸﾀｰ、もしくは指定したｻｲﾄの累計報酬
					case "$SELF_TOTAL_PAY_AMT":
						sValue = GetSelectCharacterOperationPayment(false,pArgument);
						break;
					case "$SELF_TOTAL_PAY_POINT":
						sValue = GetSelectCharacterOperationPayment(true,pArgument);
						break;
					//全ｻｲﾄの累計報酬
					case "$SELF_PAYOUT_TOTAL_AMT":
						sValue = (sessionWoman.userWoman.totalPaymentAmt + GetBonusAmt(string.Empty)).ToString();
						break;
					case "$SELF_PAYOUT_TOTAL_POINT":
						sValue = (sessionWoman.userWoman.totalPaymentPoint + GetBonusPoint(string.Empty)).ToString();
						break;
					case "$SELF_PAYMENT_UNDER_AMT":
						sValue = GetPaymentUnder(false,pArgument);
						break;
					case "$SELF_PAYMENT_UNDER_POINT":
						sValue = GetPaymentUnder(true,pArgument);
						break;
					case "$SELF_AD_CD":
						if (sessionWoman.logined) {
							sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].adCd;
						} else {
							sValue = sessionWoman.adCd;
						}
						break;
					case "$SELF_AD_GROUP_CD":
						if (sessionWoman.logined) {
							sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].adGroupCd;
						} else {
							if (sessionWoman.adGroup != null) {
								sValue = sessionWoman.adGroup.adGroupCd;
							}
						}
						break;
					case "$SELF_AUTO_PAYMENT_UNDER_AMT":
						sValue = GetSelfPaymentUnder(false);
						break;
					case "$SELF_AUTO_PAYMENT_UNDER_POINT":
						sValue = GetSelfPaymentUnder(true);
						break;
					case "$SELF_BANK_ACCOUNT_HOLDER_NM":
						sValue = sessionWoman.userWoman.bankAccountHolderNm;
						break;
					case "$SELF_BANK_ACCOUNT_NO":
						sValue = sessionWoman.userWoman.bankAccountNo;
						break;
					case "$SELF_BANK_ACCOUNT_TYPE":
						using (ManageCompany oManageCompany = new ManageCompany()) {
							if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_BANK_TYPE_RADIO)) {
								if (sessionWoman.userWoman.bankAccountType2.Equals(ViCommConst.BankAccountType.NORMAL_ACCOUNT)) {
									sValue = "普通";
								} else if (sessionWoman.userWoman.bankAccountType2.Equals(ViCommConst.BankAccountType.CHECKING_ACCOUNT)) {
									sValue = "当座";
								} else if (sessionWoman.userWoman.bankAccountType2.Equals(ViCommConst.BankAccountType.SAVINGS_ACCOUNTS)) {
									sValue = "貯蓄";
								} else {
									sValue = string.Empty;
								}
							} else {
								sValue = sessionWoman.userWoman.bankAccountType;
							}
						}
						break;
					case "$SELF_BANK_INVALID_FLAG":
						sValue = iBridUtil.GetStringValue(sessionWoman.userWoman.bankAccountInvalidFlag);
						break;
					case "$SELF_BANK_NM":
						sValue = sessionWoman.userWoman.bankNm;
						break;
					case "$SELF_BANK_OFFICE_KANA_NM":
						sValue = sessionWoman.userWoman.bankOfficeKanaNm;
						break;
					case "$SELF_BANK_OFFICE_NM":
						sValue = sessionWoman.userWoman.bankOfficeNm;
						break;
					case "$SELF_BATCH_MAIL_COUNT":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].batchMailCount.ToString();
						break;
					case "$SELF_BATCH_MAIL_LIMIT":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castToManBatchMailLimit.ToString();
						break;
					case "$SELF_BATCH_MAIL_REMAINING":
						sValue = (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castToManBatchMailLimit - sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].batchMailCount).ToString();
						break;
					case "$SELF_INVITE_COUNT":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].inviteMailCount.ToString();
						break;
					case "$SELF_INVITE_LIMIT":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].inviteMailLimit.ToString();
						break;
					case "$SELF_INVITE_REMAINING":
						sValue = (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].inviteMailLimit - sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].inviteMailCount).ToString();
						break;
					case "$SELF_BONUS_TOTAL_AMT":
						sValue = GetBonusAmt(pArgument).ToString();
						break;
					case "$SELF_BONUS_TOTAL_POINT":
						sValue = GetBonusPoint(pArgument).ToString();
						break;
					case "$SELF_CAN_SELECT_MONITOR_FLAG":
						sValue = sessionWoman.site.castCanSelectMonitorFlag.ToString();
						break;
					case "$SELF_COMMENT_DETAIL":
						sValue = ReplaceBr(parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].commentDetail));
						break;
					case "$SELF_COMMENT_LIST":
						sValue = ReplaceBr(parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].commentList));
						break;
					case "$SELF_WAITING_COMMENT":
						sValue = ReplaceBr(parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].waitingComment));
						break;
					case "$SELF_EMAIL_ADDR":
						sValue = sessionWoman.userWoman.emailAddr;
						break;
					case "$SELF_EMAIL_INVALID_FLAG":
						sValue = sessionWoman.userWoman.nonExistEmailAddrFlag.ToString();
						break;
					case "$SELF_FREE_DIAL_FLAG":
						sValue = sessionWoman.userWoman.useFreeDialFlag.ToString();
						break;
					case "$SELF_CROSMILE_FLAG":
						sValue = sessionWoman.userWoman.useCrosmileFlag.ToString();
						break;
					case "$SELF_VOICEAPP_FLAG":
						sValue = sessionWoman.userWoman.useVoiceappFlag.ToString();
						break;
					case "$SETUP_FREE_DIAL_URL":
						sValue = GetSetupFreeDialUrl(pTag,pArgument);
						break;
					case "$SETUP_CROSMILE_URL":
						sValue = GetSetupCrosmileUrl(pTag,pArgument);
						break;
					case "$SETUP_VOICEAPP_URL":
						sValue = GetSetupVoiceappUrl(pTag,pArgument);
						break;
					case "$SELF_FRIEND_INTRO_CD":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].friendIntroCd;
						break;
					case "$SELF_HANDLE_KANA_NM":
						sValue = parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleKanaNm);
						break;
					case "$SELF_HANDLE_NM":
						if (sessionWoman.userWoman.CurCharacter != null) {
							sValue = parseContainer.Parse(sessionWoman.userWoman.CurCharacter.handleNm);
						}
						break;
					case "$SELF_CAST_NM":
						sValue = parseContainer.Parse(sessionWoman.userWoman.castNm);
						break;
					case "$SELF_IMG_PATH":
						sValue = "../" + sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].smallPhotoImgPath;
						break;
					case "$SELF_INFO_MAIL_UNREAD_COUNT":
						sValue = GetUnreadMailCount(ViCommConst.MAIL_BOX_INFO,pArgument);
						break;
					case "$SELF_ITEM":
						sValue = ReplaceBr(parseContainer.Parse(GetUserWomanAttrValue(pArgument)));
						sValue = ReplaceBr(sValue);
						break;
					case "$SELF_ITEM_CD":
						sValue = GetUserWomanAttrValueCd(pArgument);
						break;
					case "$SELF_LARGE_IMG_PATH":
						sValue = "../" + sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].photoImgPath;
						break;
					case "$SELF_LAST_PAYMENT_DATE":
						sValue = GetLastPaymentDate();
						break;
					case "$SELF_LOGIN_DATE":
						sValue = sessionWoman.userWoman.startDate.ToString("yyyy/MM/dd HH:mm:ss");
						break;
					case "$SELF_LOGIN_ID":
						sValue = sessionWoman.userWoman.loginId;
						break;
					case "$SELF_LOGIN_PASSWORD":
						sValue = sessionWoman.userWoman.loginPassword;
						break;
					case "$SELF_LOGIN_FLAG":
						sValue = sessionWoman.userWoman.loginCharacter[sessionWoman.userWoman.curKey] ? "1" : "0";
						break;
					case "$SELF_LOGOUT_SCH_DATE":
						sValue = sessionWoman.userWoman.endDate.ToString("yyyy/MM/dd HH:mm:ss");
						break;
					case "$SELF_CROSMILE_APP_RUN":
						sValue = GetCrosmileAppRunIntent();
						break;
					case "$SELF_MAN_MAIL_UNREAD_COUNT":
						if (sessionWoman.currentAspx.Equals("UserTop.aspx") && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCastUserTopDataSet"]).Equals("1")) {
							SetFieldValue(pTag,"SELF_MAN_MAIL_UNREAD_COUNT",out sValue);
						} else {
							sValue = GetUnreadMailCount(ViCommConst.MAIL_BOX_USER,pArgument);
						}
						break;
					case "$SELF_MARKING_UNCONFIRM_COUNT":  // 足あとリストの未確認件数
						sValue = GetUnconfirmMarkingCount();
						break;
					case "$SELF_MODIFY_MAIL_ADDR":
						sValue = GetModifyMailAddr();
						break;
					case "$SELF_NEED_SETUP_PROFILE":
						sValue = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].registIncompleteFlag.ToString();
						break;
					case "$SELF_NEW_INFO_MAIL_DATE":
						sValue = GetNewMailInfo(ViCommConst.MAIL_BOX_INFO,ViCommConst.RX,"CREATE_DATE");
						break;
					case "$SELF_NEW_INFO_MAIL_TITLE":
						sValue = GetNewMailInfo(ViCommConst.MAIL_BOX_INFO,ViCommConst.RX,"MAIL_TITLE");
						break;
					case "$SELF_OK_LIST":
						sValue = parseContainer.Parse(sessionWoman.userWoman.GetOkPlayList(sessionWoman.site.siteCd,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].okPlayMask));
						break;
					case "$SELF_PERSONAL_AD":
						sValue = sessionWoman.personalAdCd;
						break;
					case "$SELF_PAY_AMT_TOTAL":
						sValue = sessionWoman.userWoman.totalPaymentAmt.ToString();
						break;
					case "$SELF_PAY_POINT_TOTAL":
						sValue = sessionWoman.userWoman.totalPaymentPoint.ToString();
						break;
					case "$SELF_PAYMENT_FLAG":
						sValue = sessionWoman.userWoman.paymentFlag.ToString();
						break;
					case "$SELF_PROFILE_PIC_OK_FLAG":
						sValue = ProfilePicOkFlag(pArgument);
						break;
					case "$SELF_PROFILE_PIC_COUNT":
						sValue = GetProfilePicCount(pArgument);
						break;
					case "$SELF_REGIST_DATE":
						sValue = sessionWoman.userWoman.registDate.ToString("yyyy/MM/dd HH:mm:ss");
						break;
					case "$SELF_STOCK_BBS_MOVIE_ATTR_TYPE_ADDR_LIST":
						sValue = GetMovieUploadAttrTypeAddr(pArgument);
						break;
					case "$SELF_STOCK_BBS_MOVIE_SEND_ADDR":
						sValue = GetMovieUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_BBS);
						break;
					case "$SELF_STOCK_BBS_MOVIE_ATTR_TYPE_SEND_ADDR":
						sValue = GetMovieUploadAttrTypeValueAddr(pTag,pArgument);
						break;
					case "$SELF_STOCK_BBS_PIC_ATTR_TYPE_ADDR_LIST":
						sValue = GetPictureUploadAttrTypeAddr(pArgument);
						break;
					case "$SELF_STOCK_BBS_PIC_SELECT_CATEGORY":
						sValue = GetUploadObjSelectCategoryAddr(ViCommConst.ATTACHED_BBS.ToString(),ViCommConst.ATTACH_PIC_INDEX.ToString(),pArgument);
						break;
					case "$SELF_STOCK_BBS_MOVIE_SELECT_CATEGORY":
						sValue = GetUploadObjSelectCategoryAddr(ViCommConst.ATTACHED_BBS.ToString(),ViCommConst.ATTACH_MOVIE_INDEX.ToString(),pArgument);
						break;
					case "$SELF_STOCK_PROFILE_PIC_SELECT_CATEGORY":
						sValue = GetUploadObjSelectCategoryAddr(ViCommConst.ATTACHED_PROFILE.ToString(),ViCommConst.ATTACH_PIC_INDEX.ToString(),pArgument);
						break;
					case "$SELF_SELECT_PIC_ATTR_TYPE_NM":
						using (CastPicAttrType oCastPicAttrType = new CastPicAttrType()) {
							sValue = oCastPicAttrType.GetAttrTypeNm(sessionWoman.site.siteCd,iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrtypeseq"]));
						}
						break;
					case "$SELF_SELECT_MOVIE_ATTR_TYPE_NM":
						using (CastMovieAttrType oCastMovieAttrType = new CastMovieAttrType()) {
							sValue = oCastMovieAttrType.GetAttrTypeNm(sessionWoman.site.siteCd,iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrtypeseq"]));
						}
						break;
					case "$SELF_SELECT_PIC_ATTR_NM":
						using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
							sValue = oCastPicAttrTypeValue.GetAttrNm(sessionWoman.site.siteCd,iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrseq"]));
						}
						break;
					case "$SELF_SELECT_MOVIE_ATTR_NM":
						using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
							sValue = oCastMovieAttrTypeValue.GetAttrNm(sessionWoman.site.siteCd,iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrseq"]));
						}
						break;
					case "$SELF_STOCK_GAME_PIC_SEND_ADDR":
						sValue = GetPictureUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_SOCIAL_GAME);
						break;
					case "$SELF_STOCK_GAME_MOVIE_SEND_ADDR":
						sValue = GetMovieUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_SOCIAL_GAME);
						break;
					case "$SELF_STOCK_BBS_PIC_SEND_ADDR":
						sValue = GetPictureUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_BBS);
						break;
					case "$SELF_STOCK_BBS_PIC_ATTR_TYPE_SEND_ADDR":
						sValue = GetPictureUploadAttrTypeValueAddr(pTag,pArgument);
						break;
					case "$SELF_STOCK_OBJ_TEMP_ID":
						sValue = sessionWoman.userWoman.ObjTempId;
						break;
					case "$SELF_STOCK_MAIL_MOVIE_SEND_ADDR":
						sValue = GetMovieUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_MAIL);
						break;
					case "$SELF_STOCK_MAIL_PIC_SEND_ADDR":
						sValue = GetPictureUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_MAIL);
						break;
					case "$SELF_STOCK_PROFILE_MOVIE_SEND_ADDR":
						sValue = GetMovieUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_PROFILE);
						break;
					case "$SELF_STOCK_PROFILE_PIC_SEND_ADDR":
						sValue = GetPictureUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_PROFILE);
						break;
					case "$SELF_TEL":
						sValue = sessionWoman.userWoman.tel;
						break;
					case "$SELF_TERMINAL_UNIQUE_ID_FLAG":
						sValue = IsEqual(pTag,!sessionWoman.userWoman.terminalUniqueId.Equals(""));
						break;
					case "$SELF_GUID_FLAG":
						sValue = IsEqual(pTag,!sessionWoman.userWoman.iModeId.Equals(""));
						break;
					case "$SELF_WAIT_PAYMENT_FLAG":
						sValue = sessionWoman.userWoman.waitPaymentFlag.ToString();
						break;
					case "$SELF_BATCH_MAIL_NA_FLAG":
						sValue = GetBatchMailNaFlag(pArgument);
						break;
					case "$SELF_ONLINE_STATUS":
						sValue = parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].mobileOnlineStatusNm.ToString());
						break;
					case "$SELF_WAIT_TYPE":
						sValue = parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].mobileConnectTypeNm.ToString());
						break;
					case "$SELF_NEW_SIGN":
						sValue = parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castNewSign.ToString());
						break;
					case "$SELF_USER_RANK":
						sValue = sessionWoman.userWoman.userRankCd;
						break;
					case "$SELF_RANK_NM":
						sValue = sessionWoman.userWoman.rankNm;
						break;
					case "$SELF_NON_USED_EASY_LOGIN_FLAG":
						sValue = sessionWoman.userWoman.nonUsedEasyLoginFlag.ToString();
						break;
					case "$SELF_PARTNER_MAIL_RX_TYPE":
						if (sessionWoman.userWoman.CurCharacter != null) {
							sValue = sessionWoman.userWoman.CurCharacter.manMailRxType;
						}
						break;
					case "$SELF_INFO_MAIL_RX_TYPE":
						if (sessionWoman.userWoman.CurCharacter != null) {
							sValue = sessionWoman.userWoman.CurCharacter.infoMailRxType;
						}
						break;
					case "$SELF_PROFILE_ALL_OK":
						sValue = GetAttrValueNotEmpty() ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$SELF_USER_DEF_FLAG":
						sValue = IsValidMask(sessionWoman.userWoman.CurCharacter.userDefineMask,pArgument);
						break;
					case "$SELF_ATTR_PIC_COUNT":
						sValue = this.GetPicCountByAttr(pArgument);
						break;
					case "$SELF_ATTR_MOVIE_COUNT":
						sValue = this.GetMovieCountByAttr(pArgument);
						break;
					case "$DIV_IS_BLACK": {
							bool bBlacked = false;
							using (CastCharacterBlack oBlack = new CastCharacterBlack()) {
								string sBlackType = parseContainer.Parse(pArgument);
								bBlacked = oBlack.IsBlack(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,pArgument);
							}
							SetNoneDisplay(!bBlacked);
						}
						break;
					case "$DIV_IS_NOT_BLACK": {
							bool bBlacked = false;
							using (CastCharacterBlack oBlack = new CastCharacterBlack()) {
								string sBlackType = parseContainer.Parse(pArgument);
								bBlacked = oBlack.IsBlack(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,pArgument);
							}
							SetNoneDisplay(bBlacked);
						}
						break;

					#endregion

					case "$CAST_TODAY_PAY_PARTS":
						sValue = GetListCastTodayPay(pTag,pArgument);
						break;
					case "$SELF_CAN_REQUEST_PAYMENT":
						sValue = (sessionWoman.userWoman.paymentTimingType == ViCommConst.PaymentTimingType.AutoMonthAndRequest || sessionWoman.userWoman.paymentTimingType == ViCommConst.PaymentTimingType.RequestOnly) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$TEMP_REGIST_NAME":
						using (TempRegist oTempRegist = new TempRegist()) {
							oTempRegist.GetValue(this.sessionWoman.userWoman.tempRegistId,"CAST_NM",ref sValue);
						}
						break;
					case "$TEMP_REGIST_KANA_NAME":
						using (TempRegist oTempRegist = new TempRegist()) {
							oTempRegist.GetValue(this.sessionWoman.userWoman.tempRegistId,"CAST_KANA_NM",ref sValue);
						}
						break;
					case "$TEMP_REGIST_AGE":
						using (TempRegist oTempRegist = new TempRegist()) {
							string sBirthDay = string.Empty;
							oTempRegist.GetValue(this.sessionWoman.userWoman.tempRegistId,"BIRTHDAY",ref sBirthDay);
							if (!string.IsNullOrEmpty(sBirthDay)) {
								DateTime oDateFrom = DateTime.Parse(string.Format("{0}/{1}/{2} 00:00:00",sBirthDay.Substring(0,4),sBirthDay.Substring(4,2),sBirthDay.Substring(6,2)));
								DateTime oDateTo = DateTime.Now;

								long d1 = Convert.ToInt64(oDateTo.ToString("yyyyMMdd")); //基準日を数値に変換
								long d2 = Convert.ToInt64(oDateFrom.ToString("yyyyMMdd")); //誕生日を数値に変換
								int age = (int)Math.Floor((double)((d1 - d2) / 10000));
								sValue = age.ToString();
							}
						}
						break;
					case "$TEMP_REGIST_QUESTION":
						using (TempRegist oTempRegist = new TempRegist()) {
							oTempRegist.GetValue(this.sessionWoman.userWoman.tempRegistId,"QUESTION_DOC",ref sValue);
						}
						break;
					case "$SELF_IS_PICKUP":
						sValue = IsPickup(pTag,pArgument).ToString();
						break;
					case "$SELF_EXIST_DEVICE_TOKEN_FLAG":
						sValue = iBridUtil.GetStringValue(sessionWoman.userWoman.existDeviceTokenFlag);
						break;
					// 退会処理完了
					case "$DIV_IS_WITHDRAWAL_STATUS_COMPLETE":
						SetNoneDisplay(!EqualsWithdrawalStatus(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE));
						break;
					// 退会申請中
					case "$DIV_IS_WITHDRAWAL_STATUS_ACCEPT":
						SetNoneDisplay(!EqualsWithdrawalStatus(ViCommConst.WITHDRAWAL_STATUS_ACCEPT));
						break;
					// 退会申請なし
					case "$DIV_IS_WITHDRAWAL_STATUS_NONE":
						SetNoneDisplay(!EqualsWithdrawalStatus(string.Empty));
						break;
					// 退会申請不可
					case "$DIV_IS_NOT_APPROVAL_WITHDRAWAL":
						SetNoneDisplay(IsApprovalWithdrawal());
						break;
					// 退会申請可
					case "$DIV_IS_APPROVAL_WITHDRAWAL":
						SetNoneDisplay(!IsApprovalWithdrawal());
						break;

					default:
						#region ---------- データセット処理 ----------
						switch (this.currentTableIdx) {
							case ViCommConst.DATASET_MAN:
								sValue = this.ParseMan(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_ADMIN_REPORT:
								sValue = this.ParseAdminReport(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_CAST_BONUS:
								sValue = this.ParseCastBonus(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_CAST_PAY:
								sValue = this.ParseCastPay(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_RANKING:
								sValue = this.ParseCastRanking(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_CAST_REGISTED_SITE_LIST:
								sValue = this.ParseCastRegistedSiteList(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_MAIL:
								sValue = this.ParseMail(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PARTNER_BBS_DOC:
								sValue = this.ParseManBbsDoc(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_REPUTATION:
								sValue = this.ParseReputation(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_SELF_BBS_DOC:
								sValue = this.ParseSelfBbsDoc(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_SELF_DIARY:
								sValue = this.ParseSelfDiary(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_SELF_MOVIE:
								sValue = this.ParseSelfMovie(pTag,pArgument,out bParsed);
								if (bParsed == false) {
									//ゲーム用動画の場合のみ、別口で変数をパースする。									sValue = ParseGameSelfMovie(pTag,pArgument,out bParsed);
								}
								break;
							case ViCommConst.DATASET_SELF_PIC:
								sValue = this.ParseSelfPic(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_TALK_HISTORY:
								sValue = this.ParseTalkHistory(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_TX_BATCH_MAIL:
								sValue = this.ParseTxBatchMail(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_SELF_BBS_OBJ:
								sValue = this.ParseSelfBbsObj(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_BBS_VIEW_HISTORY:
								sValue = this.ParseBbsViewHistory(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_CAST:
								sValue = this.ParseCast(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PARTNER_MOVIE:
								sValue = this.ParseCastMovie(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_CAST_LIKE_NOTICE:
								sValue = this.ParseCastLikeNotice(pTag,pArgument,out bParsed);
								break;
							// ギフト券検索結果
							case ViCommConst.DATASET_GIFT_CODE:
								sValue = this.ParseGiftCode(pTag,pArgument,out bParsed);
								break;
							default:
								bParsed = false;
								break;
						}
						break;
						#endregion
				}

				if (bParsed == false) {
					sValue = PerserEx(pTag,pArgument,out bParsed);
				}
				if (bParsed == false) {
					sValue = new PwParseWoman(this).Parse(pTag,pArgument,out bParsed);
				}

				if (bParsed == false) {
					if (pTag.StartsWith("$DIV_")) {
						ParseDiv(pTag,pArgument);
					} else if (pTag.ToUpper().StartsWith("<A ")) {
						sValue = parseContainer.Parse(pTag.Substring(2));
						sValue = "<a" + sValue;
						sValue = GetHRefHasSiteCd(sValue);
						sValue = GetHRefHasCharNo(sValue);
					} else {
						return base.Perser(pTag,pArgument);
					}
				}

				byte[] byteArray = encSjis.GetBytes(sValue ?? string.Empty);
				return byteArray;
			}
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"ParseWoman",pTag);
			this.parseErrorList.Add(pTag + " " + ex.Message);
			throw (ex);
		}
	}

	private string GetHrefVideoTel(string pLabel) {
		if (sessionWoman.errorMessage.Equals("")) {
			if (sessionWoman.ivpRequest.useCrosmile) {
				return string.Format("<a href=\"{0}/MakeCall/?callType=2&userAgentKey={1}&userAgentVersion={2}&serviceCode={3}&uasUserId={4}&uasUserIdType={5}&screenPatternId={6}&extendKey={7}&accountableSecond={8}\">{9}</a>",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]),
					sessionWoman.site.ivpSiteCd,
					"00.00.0",
					sessionWoman.ivpRequest.crosmileDirectFlag ? iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartDirectServiceCode"]) : iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartServiceCode"]),
					sessionWoman.ivpRequest.dialNo,
					"4",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmScreenPatternId"]),
					sessionWoman.ivpRequest.acceptSeq,
					0,
					pLabel);
			} else {
				return string.Format("<a href=\"{0}{1}\">{2}</a>",sessionWoman.ivpRequest.videoPrefix,sessionWoman.ivpRequest.dialNo,pLabel);
			}
		} else {
			return "";
		}
	}

	private string GetHrefVoiceTel(string pLabel) {
		if (sessionWoman.errorMessage.Equals("")) {
			if (sessionWoman.ivpRequest.useCrosmile) {
				return string.Format("<a href=\"{0}/MakeCall/?callType=1&userAgentKey={1}&userAgentVersion={2}&serviceCode={3}&uasUserId={4}&uasUserIdType={5}&screenPatternId={6}&extendKey={7}&accountableSecond={8}\">{9}</a>",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]),
					sessionWoman.site.ivpSiteCd,
					"00.00.0",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartDirectServiceCode"]),
					sessionWoman.ivpRequest.dialNo,
					"4",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmScreenPatternId"]),
					sessionWoman.ivpRequest.acceptSeq,
					0,
					pLabel);
			} else if (sessionWoman.ivpRequest.useVoiceapp) {
				string sData = "outgoing=1";
				sData = AEScryptHelper.Encrypt(sData);
				return string.Format("<a href=\"girlschatapp://call/?data={0}\">{1}</a>",sData,pLabel);
			} else {
				return string.Format("<a href=\"{0}{1}\">{2}</a>",sessionWoman.ivpRequest.audioPrefix,sessionWoman.ivpRequest.dialNo,pLabel);
			}
		} else {
			return "";
		}
	}

	private string GetSelfMailWriteToManHandleNm() {
		if (sessionWoman.currentAspx.Equals("TxMail.aspx") || sessionWoman.currentAspx.Equals("ReturnMail.aspx")) {
			return parseContainer.Parse(sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.userWoman.mailDataSeq)].rxUserNm);
		} else {
			return parseContainer.Parse(sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].rxUserNm);
		}
	}

	private string GetRegistMailTempId() {
		return string.Format("utnm{0}@{1}",sessionWoman.userWoman.tempRegistId,sessionWoman.site.mailHost);
	}

	private string GetAffiliateCookieImg() {
		string sResult = "";
		if (sessionWoman.userWoman.registCarrierCd.Equals(ViCommConst.ANDROID) || sessionWoman.userWoman.registCarrierCd.Equals(ViCommConst.IPHONE)) {

			if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
				UserWomanCharacter oCharacter = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey];

				if (oCharacter.noReportAffiliateFlag == ViCommConst.FLAG_ON && !oCharacter.adCd.Equals("")) {

					using (TempRegist oTemp = new TempRegist()) {
						if (oTemp.GetOneByUserSeq(sessionWoman.userWoman.userSeq)) {
							string sTrackingUrl;
							switch (oTemp.authResultTransType) {
								case ViCommConst.AUTH_AFFILIATE_TRASN_IMAGE_TAG:

									string sUID;
									if (oTemp.mobileCarrierCd.Equals(ViCommConst.KDDI)) {
										sUID = oTemp.terminalUniqueId;
									} else {
										sUID = oTemp.imodeId;
									}
									sTrackingUrl = string.Format(oTemp.trackingUrl,string.Empty,sessionWoman.userWoman.loginId,oTemp.registIpAddr,sUID);
									sTrackingUrl += oTemp.trackingAdditionInfo;
									sResult = string.Format("<img src=\"{0}\" width=\"1\" height=\"1\" />",sTrackingUrl);
									oCharacter.noReportAffiliateFlag = 0;
									oTemp.CompliteTrackingReport(sessionWoman.userWoman.userSeq);
									break;
								case ViCommConst.AUTH_AFFILIATE_TRASN_JS_TAG:
									sResult = oTemp.trackingJs;
									oCharacter.noReportAffiliateFlag = 0;
									oTemp.CompliteTrackingReport(sessionWoman.userWoman.userSeq);
									break;
							}
						}
					}
				}
			}
		}

		return sResult;
	}


	private string GetConerPayAmt(string pChargeType) {
		return sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castPaymentList[pChargeType].chargeAmt.ToString();
	}

	private string GetConerUnit(string pChargeType) {
		return sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castPaymentList[pChargeType].chargeUnit.ToString();
	}

	private string GetConerPayPoint(string pChargeType) {
		return sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castPaymentList[pChargeType].chargePoint.ToString();
	}

	private string GetSelectCharacterOperationPayment(bool pPointFlag,string pArgument) {
		int iTotalPayAmt = 0;
		int iTotalPayPoint = 0;

		if (pArgument.Equals(string.Empty)) {
			foreach (KeyValuePair<string,CastPayment> chargePoint in sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castPaymentList) {
				iTotalPayAmt += chargePoint.Value.chargeAmt;
				iTotalPayPoint += chargePoint.Value.chargePoint;
			}
			iTotalPayAmt += GetBonusAmt(string.Empty);
			iTotalPayPoint += GetBonusPoint(string.Empty);
		} else {
			foreach (KeyValuePair<string,CastPayment> chargePoint in sessionWoman.userWoman.characterList[pArgument + ViCommConst.MAIN_CHAR_NO].castPaymentList) {
				iTotalPayAmt += chargePoint.Value.chargeAmt;
				iTotalPayPoint += chargePoint.Value.chargePoint;
			}
		}
		if (pPointFlag) {
			return iTotalPayPoint.ToString();
		} else {
			return iTotalPayAmt.ToString();
		}
	}

	private string GetUnreadMailCount(string pMailType,string pArgument) {
		int iRecCount = 0;
		string sSiteCd,sCharNo;
		if (pArgument.Equals(string.Empty)) {
			sSiteCd = sessionWoman.site.siteCd;
			sCharNo = sessionWoman.userWoman.curCharNo;
		} else {
			sSiteCd = pArgument;
			sCharNo = ViCommConst.MAIN_CHAR_NO;
		}
		using (MailBox oMailLog = new MailBox()) {
			iRecCount = (int)(oMailLog.GetMailCount(sSiteCd,sessionWoman.userWoman.userSeq,sCharNo,ViCommConst.OPERATOR,pMailType,true));
		}
		return iRecCount.ToString();
	}

	/// <summary>
	/// 足あとリストの未確認件数を取得
	/// </summary>
	/// <returns></returns>
	private string GetUnconfirmMarkingCount() {
		// 取得済みの場合
		if (!string.IsNullOrEmpty(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.unconfirmMarkingCount)) {
			return sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.unconfirmMarkingCount;
		}

		int iRecCount = 0;
		using (Marking oMarking = new Marking()) {
			iRecCount = (int)(oMarking.GetUnconfirmMarkingCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.WOMAN));
		}

		// 取得した足あとの未確認件数をセッションに保持しておく
		sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.unconfirmMarkingCount = iRecCount.ToString();

		return iRecCount.ToString();
	}

	private string GetUserWomanAttrValue(string itemNo) {
		string ret = string.Empty;
		List<UserWomanAttr> oAttr = ((UserWomanCharacter)sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey]).attrList;
		foreach (UserWomanAttr attr in oAttr) {
			if (itemNo == attr.itemNo) {
				if (attr.inputType.Equals(ViCommConst.INPUT_TYPE_TEXT)) {
					ret = attr.attrImputValue;
				} else {
					ret = attr.attrNm;
				}
				break;
			}
		}
		return ret;
	}

	private bool GetAttrValueNotEmpty() {
		bool bExist = true;
		List<UserWomanAttr> oAttr = ((UserWomanCharacter)sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey]).attrList;
		foreach (UserWomanAttr attr in oAttr) {
			if (attr.inputType.Equals(ViCommConst.INPUT_TYPE_TEXT)) {
				if (attr.attrImputValue.Equals(string.Empty)) {
					bExist = false;
				}
			} else {
				if (attr.attrSeq.Equals(string.Empty)) {
					bExist = false;
				}
			}
		}
		return bExist;
	}

	private string GetUserWomanAttrValueCd(string itemNo) {
		string ret = string.Empty;
		List<UserWomanAttr> oAttr = ((UserWomanCharacter)sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey]).attrList;
		foreach (UserWomanAttr attr in oAttr) {
			if (itemNo == attr.itemNo) {
				ret = attr.itemCd;
				break;
			}
		}
		return ret;
	}

	private string GetModifyMailAddr() {
		return string.Format("catm{0:D2}{1}{2}{3}@{4}",
			sessionWoman.userWoman.userSeq.Length,
			sessionWoman.userWoman.userSeq,
			sessionWoman.userWoman.loginPassword.Length,
			sessionWoman.userWoman.loginPassword,
			sessionWoman.site.mailHost);
	}

	private string GetPictureUploadAttrTypeAddr(string pArgument) {
		string sResultHtml = "";
		int i = 0;
		string[] oArgs = pArgument.Split(',');
		string sItemNo = oArgs[0];
		string sMailTo = this.ParseArg(oArgs,1,string.Empty);
		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailTo)) {
			sUrlHeader = ViCommConst.PHOTO_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailTo,ViCommConst.PHOTO_URL_HEADER2);
		}

		using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
			DataSet ds = oCastPicAttrTypeValue.GetListByItemNo(sessionWoman.site.siteCd,sItemNo);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (i != 0) {
					sResultHtml += ",";
				} else {
					i++;
				}
				sResultHtml += "<a href=\"" +
					sUrlHeader + ViCommConst.ATTACHED_BBS.ToString() +
					sessionWoman.site.siteCd + "_" +
					sessionWoman.userWoman.loginId + sessionWoman.userWoman.curCharNo + "__" +
					dr["CAST_PIC_ATTR_SEQ"].ToString() +
					"@" + sessionWoman.site.mailHost + "\">";
				sResultHtml += dr["CAST_PIC_ATTR_NM"].ToString() + "</a>";
			}
		}
		return sResultHtml;
	}

	private string GetPictureUploadAttrTypeValueAddr(string pTag,string pArguments) {
		string[] oArgs = pArguments.Split(new char[] { ',' });
		string sSeq = ParseArg(oArgs,0,string.Empty);
		string sLinkNm = ParseArg(oArgs,1,string.Empty);
		string sMailTo = this.ParseArg(oArgs,2,string.Empty);

		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailTo)) {
			sUrlHeader = ViCommConst.PHOTO_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailTo,ViCommConst.PHOTO_URL_HEADER2);
		}

		string sPictureUploadAttrTypeAddr = string.Empty;
		using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
			string sATttrTypeNm = oCastPicAttrTypeValue.GetAttrNm(sessionWoman.site.siteCd,sSeq);
			if (string.IsNullOrEmpty(sLinkNm)) {
				sLinkNm = sATttrTypeNm;
			}
			if (!string.IsNullOrEmpty(sATttrTypeNm)) {
				sPictureUploadAttrTypeAddr += "<a href=\"" +
						sUrlHeader + ViCommConst.ATTACHED_BBS.ToString() +
						sessionWoman.site.siteCd + "_" +
						sessionWoman.userWoman.loginId + sessionWoman.userWoman.curCharNo + "__" +
						sSeq +
						"@" + sessionWoman.site.mailHost + "\">";
				sPictureUploadAttrTypeAddr += sLinkNm + "</a>";
			}
		}
		return sPictureUploadAttrTypeAddr;
	}



	private string GetUploadObjSelectCategoryAddr(string sAttachedType,string pObjType,string pItemNo) {
		string sResultHtml = string.Empty;
		string sSendUrl = string.Empty;
		string sObjNm = string.Empty;

		int i = 0;
		DataSet ds;
		if (pObjType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
			using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
				ds = oCastMovieAttrTypeValue.GetListByItemNo(sessionWoman.site.siteCd,pItemNo);
			}
			sObjNm = "CAST_MOVIE";
		} else if (pObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
			using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
				ds = oCastPicAttrTypeValue.GetListByItemNo(sessionWoman.site.siteCd,pItemNo);
			}
			sObjNm = "CAST_PIC";
		} else {
			return string.Empty;
		}

		if (sAttachedType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			sSendUrl = "SendProfilePic.aspx";
		} else if (sAttachedType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			sSendUrl = "SendBbsObj.aspx";
		} else {
			return string.Empty;
		}

		foreach (DataRow dr in ds.Tables[0].Rows) {
			if (i != 0) {
				sResultHtml += ",";
			} else {
				i++;
			}

			sResultHtml += string.Format("<a href=\"{0}?attrtypeseq={1}&attrseq={2}\">{3}</a>",sSendUrl,dr[sObjNm + "_ATTR_TYPE_SEQ"].ToString(),dr[sObjNm + "_ATTR_SEQ"].ToString(),dr[sObjNm + "_ATTR_NM"].ToString());
		}

		return sResultHtml;
	}

	private string GetMovieUploadAttrTypeAddr(string pArguments) {
		string sResultHtml = "";
		int i = 0;
		string[] oArgs = pArguments.Split(',');
		string sItemNo = oArgs[0];
		string sMailTo = ParseArg(oArgs,1,string.Empty);
		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailTo)) {
			sUrlHeader = ViCommConst.MOVIE_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailTo,ViCommConst.MOVIE_URL_HEADER2);
		}

		using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
			DataSet ds = oCastMovieAttrTypeValue.GetListByItemNo(sessionWoman.site.siteCd,sItemNo);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (i != 0) {
					sResultHtml += ",";
				} else {
					i++;
				}
				sResultHtml += "<a href=\"" +
					sUrlHeader + ViCommConst.ATTACHED_BBS.ToString() +
					sessionWoman.site.siteCd + "_" +
					sessionWoman.userWoman.loginId + sessionWoman.userWoman.curCharNo + "__" +
					dr["CAST_MOVIE_ATTR_SEQ"].ToString() +
					"@" + sessionWoman.site.mailHost + "\">";
				sResultHtml += dr["CAST_MOVIE_ATTR_NM"].ToString() + "</a>";
			}
		}
		return sResultHtml;
	}
	private string GetMovieUploadAttrTypeValueAddr(string pTag,string pArguments) {
		string[] oArgs = pArguments.Split(new char[] { ',' });
		string sSeq = ParseArg(oArgs,0,string.Empty);
		string sLinkNm = ParseArg(oArgs,1,string.Empty);
		string sMailTo = ParseArg(oArgs,2,string.Empty);

		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailTo)) {
			sUrlHeader = ViCommConst.MOVIE_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailTo,ViCommConst.MOVIE_URL_HEADER2);
		}

		string sMovieUploadAttrTypeValueAddr = string.Empty;
		using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
			string sAttrTypeNm = oCastMovieAttrTypeValue.GetAttrNm(sessionWoman.site.siteCd,sSeq);
			if (!string.IsNullOrEmpty(sAttrTypeNm)) {
				if (string.IsNullOrEmpty(sLinkNm)) {
					sLinkNm = sAttrTypeNm;
				}

				sMovieUploadAttrTypeValueAddr += "<a href=\"" +
					sUrlHeader + ViCommConst.ATTACHED_BBS.ToString() +
					sessionWoman.site.siteCd + "_" +
					sessionWoman.userWoman.loginId + sessionWoman.userWoman.curCharNo + "__" +
					sSeq +
					"@" + sessionWoman.site.mailHost + "\">";
				sMovieUploadAttrTypeValueAddr += sLinkNm + "</a>";
			}

		}
		return sMovieUploadAttrTypeValueAddr;
	}

	private string GetMailObjTempId(string pTag) {
		string sObjTempId = string.Empty;
		if (sessionWoman.userWoman.mailData.ContainsKey(iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"]))) {
			sObjTempId = sessionWoman.userWoman.mailData[iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"])].objTempId;
		}
		if (string.IsNullOrEmpty(sObjTempId)) {
			string sRequestTxMailSeq;
			if (GetValue(pTag,ViCommConst.DATASET_MAIL,"REQUEST_TX_MAIL_SEQ",out sRequestTxMailSeq)) {
				using (MailLog oMailLog = new MailLog()) {
					sObjTempId = oMailLog.GetObjTempId(sRequestTxMailSeq);
				}
			}
		}
		return sObjTempId;
	}

	private string GetPictureUploadAddr(string pTag,string pArgument,int attachType) {
		string sObjTempId = sessionWoman.userWoman.ObjTempId;
		if (attachType.Equals(ViCommConst.ATTACHED_MAIL)) {
			sObjTempId = GetMailObjTempId(pTag);
		}

		string sMailto = parseContainer.Parse(pArgument);
		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailto)) {
			sUrlHeader = ViCommConst.PHOTO_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailto,ViCommConst.PHOTO_URL_HEADER2);
		}

		return sUrlHeader +
				attachType.ToString() +
				sessionWoman.site.siteCd + "_" +
				sessionWoman.userWoman.loginId + sessionWoman.userWoman.curCharNo + "_" +
				sObjTempId +
				"@" +
				sessionWoman.site.mailHost;
	}

	private string GetMovieUploadAddr(string pTag,string pArgument,int attachType) {
		string sObjTempId = sessionWoman.userWoman.ObjTempId;
		if (attachType.Equals(ViCommConst.ATTACHED_MAIL)) {
			sObjTempId = GetMailObjTempId(pTag);
		}

		string sMailto = parseContainer.Parse(pArgument);
		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailto)) {
			sUrlHeader = ViCommConst.MOVIE_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailto,ViCommConst.MOVIE_URL_HEADER2);
		}

		return sUrlHeader +
				attachType.ToString() +
				sessionWoman.site.siteCd + "_" +
				sessionWoman.userWoman.loginId + sessionWoman.userWoman.curCharNo + "_" +
				sObjTempId + "@" +
				sessionWoman.site.mailHost;
	}

	private bool IsAllCharacterOk() {
		foreach (UserWomanCharacter oCharacter in sessionWoman.userWoman.characterList.Values) {
			if (oCharacter.registIncompleteFlag == 1) {
				return false;
			}
		}
		return true;
	}

	private string GetHRefHasSiteCd(string pValue) {
		string sResult = "";
		if (pValue.Contains("site=")) {
			sResult = pValue;
		} else if ((sessionWoman.isCrawler) && (pValue.Contains("site/"))) {
			sResult = pValue;
		} else if (pValue.Contains("SelectSite.aspx")) {
			sResult = pValue;
		} else if (pValue.Contains("Start.aspx")) {
			sResult = pValue;
		} else if (pValue.Contains("mailto:")) {
			sResult = pValue;
		} else if (pValue.Contains("sms:")) {
			sResult = pValue;
		} else if (pValue.Contains("tel:")) {
			sResult = pValue;
		} else if (pValue.Contains("#")) {
			sResult = pValue;
		} else if (pValue.Contains("accesskey")) {
			sResult = pValue;
		} else if (pValue.Contains("http://")) {
			sResult = pValue;
		} else if (pValue.Contains("https://")) {
			sResult = pValue;
		} else if (pValue.Contains("girlschatapp://")) {
			sResult = pValue;
		} else {
			string sReplaceValue;
			if (sessionObjs.carrier.Equals(ViCommConst.KDDI)) {
				sReplaceValue = string.Format("{0}&site={1}",GetAuRefresh(),sessionWoman.site.siteCd);
			} else {
				sReplaceValue = string.Format("site={0}",sessionWoman.site.siteCd);
			}
			if (pValue.Contains("?")) {
				sResult = pValue.Replace("\">","&" + sReplaceValue + "\">");
			} else {
				sResult = pValue.Replace("\">","?" + sReplaceValue + "\">");
			}
		}
		if (sessionWoman.isCrawler) {
			sResult = Regex.Replace(sResult,@"(\?+[^"">]*)",new MatchEvaluator(RegexMatchPathInfo),RegexOptions.IgnoreCase | RegexOptions.Compiled);
			return sResult;
		} else {
			return sResult;
		}
	}

	private string GetHRefHasCharNo(string pValue) {
		string sResult = "";
		if (pValue.Contains("charno=")) {
			sResult = pValue;
		} else if (((sessionWoman.isCrawler)) && (pValue.Contains("charno/"))) {
			sResult = pValue;
		} else if (pValue.Contains("SelectSite.aspx")) {
			sResult = pValue;
		} else if (pValue.Contains("Start.aspx")) {
			sResult = pValue;
		} else if (pValue.Contains("mailto:")) {
			sResult = pValue;
		} else if (pValue.Contains("sms:")) {
			sResult = pValue;
		} else if (pValue.Contains("tel:")) {
			sResult = pValue;
		} else if (pValue.Contains("#")) {
			sResult = pValue;
		} else if (pValue.Contains("accesskey")) {
			sResult = pValue;
		} else if (pValue.Contains("http://")) {
			sResult = pValue;
		} else if (pValue.Contains("https://")) {
			sResult = pValue;
		} else if (pValue.Contains("girlschatapp://")) {
			sResult = pValue;
		} else {
			if (pValue.Contains("?")) {
				sResult = pValue.Replace("\">","&charno=" + sessionWoman.userWoman.curCharNo + "\">");
			} else {
				sResult = pValue.Replace("\">","?charno=" + sessionWoman.userWoman.curCharNo + "\">");
			}
		}
		if (sessionWoman.isCrawler) {
			sResult = Regex.Replace(sResult,@"(\?+[^"">]*)",new MatchEvaluator(RegexMatchPathInfo),RegexOptions.IgnoreCase | RegexOptions.Compiled);
			return sResult;
		} else {
			return sResult;
		}
	}

	private string RegexMatchPathInfo(Match match) {
		return SysPrograms.ChangeQueryToPathInfo(match.Value,true);
	}
	public override string ParseSiteTop() {
		return ParseSiteTop(true);
	}
	public string ParseSiteTop(bool pWithImpersonate) {
		if (pWithImpersonate) {
			if (sessionWoman.IsImpersonated) {
				return ((ParseMan)sessionWoman.originalManObj.parseContainer.parseUser).ParseSiteTop(false);
			}
		}

		string sBaseUrl = sessionWoman.root + sessionWoman.sysType;
		if (sessionWoman.logined) {
			if (sessionWoman.site.siteCd.Equals(ViCommConst.CAST_SITE_CD)) {
				return sessionWoman.GetNavigateUrl(sBaseUrl,sessionWoman.sessionId,"SelectSite.aspx?" + GetAuRefresh());
			} else {
				return sessionWoman.GetNavigateUrl(sBaseUrl,sessionWoman.sessionId,"UserTop.aspx?site=" + sessionWoman.site.siteCd + "&charno=" + sessionWoman.userWoman.curCharNo + "&" + GetAuRefresh());
			}
		} else {
			return sessionWoman.GetNavigateUrl(sBaseUrl,sessionWoman.sessionId,"NonUserTop.aspx?" + GetAuRefresh());
		}
	}

	private string GetHtmlDocFull(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "",sDoc5 = "";
		if (GetValue(pTag,"HTML_DOC1",out sDoc1) && GetValue(pTag,"HTML_DOC2",out sDoc2) && GetValue(pTag,"HTML_DOC3",out sDoc3) && GetValue(pTag,"HTML_DOC4",out sDoc4) && GetValue(pTag,"HTML_DOC5",out sDoc5)) {
			sValue = parseContainer.Parse(sDoc1 + sDoc2 + sDoc3 + sDoc4 + sDoc5);
		}
		return sValue;
	}

	private string GetDoc(string pTag,string pFiledNm) {
		string sValue = "";
		if (GetValue(pTag,pFiledNm,out sValue)) {
			sValue = parseContainer.Parse(Regex.Replace(sValue.ToString(),"\r\n","<br />"));
		}
		return sValue;
	}

	private string GetAttachedValue(string sFieldNm) {
		string sValue = string.Empty;
		string sMovieSeq = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["movieseq"]);
		string sPicSeq = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["picseq"]);

		if (!sMovieSeq.Equals(string.Empty)) {
			using (CastMovie oCastMovie = new CastMovie()) {
				sValue = oCastMovie.GetValueByMovieSeq(sessionWoman.site.siteCd,sMovieSeq,sFieldNm);
			}
		} else if (!sPicSeq.Equals(string.Empty)) {
			using (CastPic oCastPic = new CastPic()) {
				sValue = oCastPic.GetValueByPicSeq(sessionWoman.site.siteCd,sPicSeq,sFieldNm);
			}
		}
		return sValue;
	}

	protected string GetCastCount(string pTag,string pArgument) {
		string[] sValue = pArgument.Split(',');
		string sSiteCd = sValue[0];
		int iIdx = 1;
		if (sValue.Length >= 2) {
			int.TryParse(sValue[1],out iIdx);
		}
		if (!sessionWoman.userWoman.curKey.Equals("")) {
			sSiteCd = sessionWoman.site.siteCd;
		}

		using (Cast oCast = new Cast())
		using (ActCategory oCategory = new ActCategory()) {
			if (oCategory.GetCategorySeq(sSiteCd,iIdx)) {
				return GetCastCount(pTag,sSiteCd,"","",oCategory.actCategorySeq,oCast);
			} else {
				return "0";
			}
		}
	}



	private decimal GetManStatusCount(string pTag,string pArgument,ulong pConiditionFlag,string pLastLoginDate) {
		decimal iCount = 0;
		using (SeekCondition oSeekCondition = new SeekCondition()) {
			if (!sessionWoman.userWoman.curKey.Equals("") && sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
				oSeekCondition.siteCd = sessionWoman.site.siteCd;
				oSeekCondition.userSeq = sessionWoman.userWoman.userSeq;
				oSeekCondition.userCharNo = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].userCharNo;
			} else {
				oSeekCondition.siteCd = pArgument;
				oSeekCondition.userSeq = "";
				oSeekCondition.userCharNo = "";
			}

			oSeekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
			switch (pConiditionFlag) {
				case ViCommConst.INQUIRY_WAITING_MAN:
				case ViCommConst.INQUIRY_ONLINE:
					oSeekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
					break;
				case ViCommConst.INQUIRY_MAN_LOGINED:
					oSeekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
					break;
			}
			oSeekCondition.conditionFlag = pConiditionFlag;

			if (!string.IsNullOrEmpty(pLastLoginDate)) {
				oSeekCondition.lastLoginDate = pLastLoginDate;
				oSeekCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
			}
			using (Man oMan = new Man()) {
				oMan.GetPageCount(oSeekCondition,1,out iCount);
			}
		}
		return iCount;
	}

	private string GetNewAdminReport(string pTag,string pArgument,string pField) {
		string[] sValue = pArgument.Split(',');
		string sSiteCd = "";
		int iRNum;
		int.TryParse(sValue[0],out iRNum);

		if (sValue.Length >= 2) {
			sSiteCd = sValue[1];
		}
		if (!sessionWoman.userWoman.curKey.Equals("")) {
			sSiteCd = sessionWoman.site.siteCd;
		} else {
			if (sSiteCd.Equals("")) {
				parseContainer.parseUser.parseErrorList.Add(string.Format("{0} Syntax Error No define site code",pTag));
				return "";
			}
		}
		return base.GetNewAdminReport(sSiteCd,ViCommConst.OPERATOR,iRNum,pField);
	}

	private string GetMinimumPayment(bool pPointFlag) {
		if (pPointFlag) {
			return (sessionWoman.userWoman.selfMinimumPayment / sessionWoman.userWoman.pointPrice).ToString();
		} else {
			return sessionWoman.userWoman.selfMinimumPayment.ToString();
		}
	}

	private string GetAutoMinimumPayment(bool pPointFlag) {
		if (pPointFlag) {
			return (sessionWoman.userWoman.autoMinimumPayment / sessionWoman.userWoman.pointPrice).ToString();
		} else {
			return sessionWoman.userWoman.autoMinimumPayment.ToString();
		}
	}

	private string GetSelfPaymentUnder(bool pPointFlag) {
		int iValue = 0;
		if (pPointFlag) {
			//後何PTで自動精算なのかを求めているので、(精算申請可能金額-所持報酬金額)/pointPriceでOK
			iValue = (sessionWoman.userWoman.autoMinimumPayment - sessionWoman.userWoman.totalPaymentAmt - GetBonusAmt(string.Empty)) / sessionWoman.userWoman.pointPrice;
		} else {
			iValue = sessionWoman.userWoman.autoMinimumPayment - sessionWoman.userWoman.totalPaymentAmt - GetBonusAmt(string.Empty);
		}
		return iValue.ToString();
	}

	private string GetPaymentUnder(bool pPointFlag,string pArgument) {
		int iValue = 0;
		int iTotalPaymentAmt = sessionWoman.userWoman.totalPaymentAmt;
		int iTotalPaymentPoint = sessionWoman.userWoman.totalPaymentPoint;
		if (!pArgument.Equals(string.Empty)) {
			string sSiteCd = pArgument.ToString();
			using (TimeOperation oTimeOperation = new TimeOperation()) {
				oTimeOperation.GetTotalPayment(sSiteCd,sessionWoman.userWoman.userSeq,out iTotalPaymentAmt,out iTotalPaymentPoint);
			}
		}

		if (pPointFlag) {
			//お友達報酬キックバックの金額とPtでズレが生じるので、(精算申請可能金額-所持報酬金額)/pointPriceではNG。Ptベースで算出。
			iValue = (sessionWoman.userWoman.selfMinimumPayment / sessionWoman.userWoman.pointPrice) - (iTotalPaymentPoint + GetBonusPoint(string.Empty));
		} else {
			iValue = sessionWoman.userWoman.selfMinimumPayment - iTotalPaymentAmt - GetBonusAmt(string.Empty);
		}
		return iValue.ToString();
	}

	private string GetLastPaymentDate() {
		if (sessionWoman.userWoman.paymentFlag == ViCommConst.FLAG_ON) {
			return sessionWoman.userWoman.lastPaymentDate.ToString("yyyy/MM/dd HH:mm:ss");
		} else {
			return string.Empty;
		}
	}
	private string IsSelfWaiting() {
		if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
			return (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterOnlineStatus < ViCommConst.USER_WAITING) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}

	}
	private string GetNewMailInfo(string pMailType,string pTxRxType,string pField) {
		string sValue;
		using (MailBox oMailBox = new MailBox()) {
			DataSet ds = oMailBox.GetNewMail(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.OPERATOR,pMailType,pTxRxType);
			if (ds.Tables[0].Rows.Count != 0) {
				sValue = ds.Tables[0].Rows[0][pField].ToString();
			} else {
				sValue = "";
			}
		}
		return sValue;
	}

	private bool CheckHandleNmDupli() {
		bool bDupliFlag;
		using (UserWoman oUserWoman = new UserWoman()) {
			bDupliFlag = oUserWoman.CheckCastHandleNmDupli(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNm);
		}
		return bDupliFlag;
	}

	private string ProfilePicOkFlag(string pArgument) {
		if (sessionWoman.userWoman.characterList.ContainsKey(pArgument + ViCommConst.MAIN_CHAR_NO)) {
			return sessionWoman.userWoman.characterList[pArgument + ViCommConst.MAIN_CHAR_NO].picSeq.Equals("0") ? "0" : "1";
		} else {
			return sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].picSeq.Equals("0") ? "0" : "1";
		}
	}

	private string GetProfilePicCount(string pArgument) {
		using (CastPic oCastPic = new CastPic(sessionWoman)) {
			return oCastPic.GetPicCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.ATTACHED_PROFILE.ToString()).ToString();
		}
	}

	private string GetPicCountByAttr(string pAttr) {
		using (CastPic oCastPic = new CastPic(sessionWoman)) {
			return oCastPic.GetPicCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.ATTACHED_BBS.ToString(),pAttr,string.Empty).ToString();
		}
	}

	private string GetMovieCountByAttr(string pAttr) {
		using (CastMovie oCastMovie = new CastMovie(sessionWoman)) {
			return oCastMovie.GetMovieCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.ATTACHED_BBS.ToString(),pAttr,string.Empty).ToString();
		}
	}

	private string GetBatchMailNaFlag(string pArgument) {
		if (sessionWoman.userWoman.characterList.ContainsKey(pArgument + ViCommConst.MAIN_CHAR_NO)) {
			return parseContainer.Parse(sessionWoman.userWoman.characterList[pArgument + ViCommConst.MAIN_CHAR_NO].castToManBatchMailNaFlag.ToString());
		} else {
			return parseContainer.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castToManBatchMailNaFlag.ToString());
		}
	}

	private string ManHandleNmEmptyFill(string pHandleNm) {
		if (pHandleNm.Equals(string.Empty)) {
			return sessionWoman.site.manHandleDefaultNm;
		} else {
			return pHandleNm;
		}
	}

	private bool IsAnyCharacterOnline() {
		bool bOnline = false;
		foreach (KeyValuePair<string,UserWomanCharacter> oCharacterList in sessionWoman.userWoman.characterList) {
			if (oCharacterList.Value.characterOnlineStatus >= ViCommConst.USER_WAITING) {
				bOnline = true;
				break;
			}
		}
		return bOnline;
	}

	private int GetBonusAmt(string pBonusPointType) {
		int iBonusAmt = 0;

		foreach (KeyValuePair<string,int> oBonusAmt in sessionWoman.userWoman.bonusAmtList) {
			if (pBonusPointType.Equals(string.Empty) || oBonusAmt.Key.Equals(pBonusPointType)) {
				iBonusAmt += oBonusAmt.Value;
			}
		}
		return iBonusAmt;
	}

	private int GetBonusPoint(string pBonusPointType) {
		int iBonusPoint = 0;

		foreach (KeyValuePair<string,int> oBonusPoint in sessionWoman.userWoman.bonusPointList) {
			if (pBonusPointType.Equals(string.Empty) || oBonusPoint.Key.Equals(pBonusPointType)) {
				iBonusPoint += oBonusPoint.Value;
			}
		}
		return iBonusPoint;
	}

	private bool IsNewMember() {
		bool bRet = false;
		string sNewDay = DateTime.Now.AddDays(-1 * sessionWoman.site.newFaceDays).ToString("yyyy/MM/dd");

		if (sessionWoman.userWoman.CurCharacter.startPerformDay.Equals("") == false) {
			if (sNewDay.CompareTo(sessionWoman.userWoman.CurCharacter.startPerformDay) <= 0) {
				bRet = true;
			}
		}
		return bRet;
	}

	private bool IsWithinStartPerformDay(string pTag,string pArgument) {
		bool bRet = false;
		int iDays = 0;

		if (int.TryParse(pArgument,out iDays)) {
			string sDaysAgo = DateTime.Now.AddDays(-(iDays)).ToString("yyyy/MM/dd");

			if (sessionWoman.userWoman.CurCharacter.startPerformDay.Equals("") == false) {
				if (sDaysAgo.CompareTo(sessionWoman.userWoman.CurCharacter.startPerformDay) <= 0) {
					bRet = true;
				}
			}
		}

		return bRet;
	}

	private string GetCornerCharge(string pChargeType,bool pPointFlag) {
		int iAmt = 0;
		string sValue = string.Empty;

		if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
			int iType;
			int.TryParse(pChargeType,out iType);
			if (iType != 0) {
				using (CastCharge oCastCharge = new CastCharge()) {
					iAmt = oCastCharge.GetPayAmt(sessionWoman.site.siteCd,pChargeType,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].actCategorySeq,sessionWoman.userWoman.userRankCd);
					if (pPointFlag) {
						iAmt = iAmt / sessionWoman.userWoman.pointPrice;
					}
				}
			} else if (pChargeType.Equals("CAST_FAVORIT")) {
				if (sessionWoman.site.GetValue(sessionWoman.site.siteCd,"CAST_FAVORIT_POINT",ref sValue)) {
					int.TryParse(sValue,out iAmt);
					if (!pPointFlag) {
						iAmt = iAmt * sessionWoman.userWoman.pointPrice;
					}
				}
			} else {
				using (UserDefPoint oDefPoint = new UserDefPoint()) {
					iAmt = oDefPoint.GetPayPoint(sessionWoman.site.siteCd,pChargeType + "_POINT");
					if (!pPointFlag) {
						iAmt = iAmt * sessionWoman.userWoman.pointPrice;
					}
				}
			}
		}
		return iAmt.ToString();
	}

	private string GetListMoteCon(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		DataSet oTmpDataSet = null;

		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.conditionFlag = ViCommConst.INQUIRY_EXTENSION_MOTE_CON;
		oCondition.siteCd = sessionWoman.site.siteCd;
		oCondition.newCastDay = 0;
		oCondition.userRank = string.Empty;
		oCondition.random = ViCommConst.FLAG_OFF;
		oCondition.listDisplay = iBridUtil.GetStringValue(pPartsArgs.Query["list"]);
		oCondition.orderAsc = iBridUtil.GetStringValue(pPartsArgs.Query["orderasc"]);
		oCondition.orderDesc = iBridUtil.GetStringValue(pPartsArgs.Query["orderdesc"]);
		oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
		oCondition.rankingCtlSeq = iBridUtil.GetStringValue(pPartsArgs.Query["ctlseq"]);
		if (oCondition.rankingCtlSeq.Equals(string.Empty)) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				oCondition.rankingCtlSeq = oRankingCtl.GetLastSeq(sessionWoman.site.siteCd,ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN);
			}
		}

		using (Cast oCast = new Cast(sessionObjs)) {
			oTmpDataSet = oCast.GetPageCollection(oCondition,1,pPartsArgs.NeedCount,false);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_RANKING);
	}

	private string GetListMarkingRank(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		DataSet oTmpDataSet = null;

		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.conditionFlag = ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING;
		oCondition.siteCd = sessionWoman.site.siteCd;
		oCondition.newCastDay = 0;
		oCondition.userRank = string.Empty;
		oCondition.random = ViCommConst.FLAG_OFF;
		oCondition.listDisplay = iBridUtil.GetStringValue(pPartsArgs.Query["list"]);
		oCondition.orderAsc = iBridUtil.GetStringValue(pPartsArgs.Query["orderasc"]);
		oCondition.orderDesc = iBridUtil.GetStringValue(pPartsArgs.Query["orderdesc"]);
		oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
		oCondition.rankingCtlSeq = iBridUtil.GetStringValue(pPartsArgs.Query["ctlseq"]);
		if (oCondition.rankingCtlSeq.Equals(string.Empty)) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				if (iBridUtil.GetStringValue(pPartsArgs.Query["new"]).Equals("1")) {
					oCondition.rankingCtlSeq = oRankingCtl.GetNowSeq(sessionWoman.site.siteCd,ViCommConst.ExRanking.EX_RANKING_MARKING);
				} else {
					oCondition.rankingCtlSeq = oRankingCtl.GetOldSeq(sessionWoman.site.siteCd,ViCommConst.ExRanking.EX_RANKING_MARKING);
				}
			}
		}

		using (Cast oCast = new Cast(sessionObjs)) {
			oTmpDataSet = oCast.GetPageCollection(oCondition,1,pPartsArgs.NeedCount,false);
		}

		if (oTmpDataSet == null)
			return string.Empty;


		decimal dTmpTotalRowCount = sessionWoman.totalRowCount;
		int iTmpStartRNum = sessionWoman.startRNum;
		int iTmpEndRNum = sessionWoman.endRNum;

		sessionWoman.totalRowCount = decimal.Parse(oTmpDataSet.Tables[0].Rows.Count.ToString());
		sessionObjs.startRNum = 1;
		sessionWoman.endRNum = oTmpDataSet.Tables[0].Rows.Count;

		string sParseDirect = parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_RANKING);

		sessionWoman.totalRowCount = dTmpTotalRowCount;
		sessionWoman.startRNum = iTmpStartRNum;
		sessionWoman.endRNum = iTmpEndRNum;

		return sParseDirect;
	}

	private string GetAnchor(string pBbsThreadSeq,string pBbsThreadSubSeq,string pText,string pTag) {
		string sValue = string.Empty;
		bool bIsExist = false;
		if (!pBbsThreadSeq.Equals(string.Empty) && !pBbsThreadSubSeq.Equals(string.Empty)) {
			//レスが存在しているかのチェック 
			using (BbsRes oBbsRes = new BbsRes()) {
				bIsExist = oBbsRes.IsExist(sessionWoman.site.siteCd,pBbsThreadSeq,pBbsThreadSubSeq);
			}
		} else if (!pBbsThreadSeq.Equals(string.Empty)) {
			//スレッドが存在しているかのチェック 
			using (BbsThread oBbsThread = new BbsThread()) {
				bIsExist = oBbsThread.IsExist(sessionWoman.site.siteCd,pBbsThreadSeq);
			}
		}
		//存在していればリンク生成 
		if (bIsExist) {
			if (!pBbsThreadSeq.Equals(string.Empty) && !pBbsThreadSubSeq.Equals(string.Empty)) {
				sValue = string.Format("<a href=\"ListBbsRes.aspx?bbsthreadseq={0}&resno={1}&sort={2}#ResNo{1}\">{3}</a>",pBbsThreadSeq,pBbsThreadSubSeq,iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["sort"]),pText);
			} else {
				sValue = string.Format("<a href=\"ListBbsRes.aspx?bbsthreadseq={0}\">{1}</a>",pBbsThreadSeq,pText);
			}
		} else {
			sValue = pText;
		}
		return sValue;
	}

	private int IsPickup(string pTag,string pArgument) {
		int isPickup = ViCommConst.FLAG_OFF;

		string[] sArguments = pArgument.Split(',');
		string sPickupId = sArguments[0];
		string isPickupFlag = ViCommConst.FLAG_OFF_STR;
		if (sArguments.Length > 1) {
			isPickupFlag = sArguments[1];
		}
		string sPickupType = string.Empty;
		using (Pickup oPickup = new Pickup()) {
			if (oPickup.GetOne(sessionWoman.site.siteCd,sPickupId)) {
				sPickupType = oPickup.pickupType;
				long lPickupMask = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].pickupMask;
				isPickup = ((lPickupMask & oPickup.pickupMask) > 0) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
			}
		}

		switch (sPickupType) {
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				if ((isPickup == ViCommConst.FLAG_ON) && isPickupFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					using (PickupCharacter oPickupCharacter = new PickupCharacter()) {
						if (oPickupCharacter.GetOne(sessionWoman.site.siteCd,
													sessionWoman.userWoman.userSeq,
													sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].userCharNo,
													sPickupId)) {
							if (oPickupCharacter.pickupFlag &&
								oPickupCharacter.pickupStartPubDay.CompareTo(DateTime.Now) <= 0 &&
								DateTime.Now.CompareTo(oPickupCharacter.pickupEndPubDay) < 0) {
								isPickup = ViCommConst.FLAG_ON;
							} else {
								isPickup = ViCommConst.FLAG_OFF;
							}
						}
					}
				}
				break;
			case ViCommConst.PicupTypes.BBS_MOVIE:
			case ViCommConst.PicupTypes.BBS_PIC:
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.PROFILE_PIC:
				using (PickupObjs oPickupObjs = new PickupObjs()) {
					if (oPickupObjs.IsExist(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].userCharNo,
							sPickupId,
							sPickupType)) {
						isPickup = ViCommConst.FLAG_ON;
						if (isPickupFlag.Equals(ViCommConst.FLAG_ON_STR) && (!oPickupObjs.pickupFlag || !oPickupObjs.publishFlag)) {
							isPickup = ViCommConst.FLAG_OFF;
						}
					} else {
						isPickup = ViCommConst.FLAG_OFF;
					}
				}
				break;
			default:
				break;
		}

		return isPickup;
	}

	private void AddOmikujiPoint() {
		using (Omikuji oOmikuji = new Omikuji()) {
			oOmikuji.AddOmikujiPoint(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].userCharNo,ViCommConst.OPERATOR,out sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].omikujiNo,out sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].getOmikujiPoint);
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].getOmikujiPoint > 0) {
				sessionWoman.userWoman.bonusPointList[ViCommConst.BONUS_TYPE_OMIKUJI] += sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].getOmikujiPoint;
				sessionWoman.userWoman.bonusAmtList[ViCommConst.BONUS_TYPE_OMIKUJI] += sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].getOmikujiPoint * sessionWoman.userWoman.pointPrice;
			}
		}
	}

	private string GetManLastReceiptDate(string pTag) {
		string sValue;
		string sFieldNm = string.Format("{0}USER_SEQ",sessionObjs.tableIndex.Equals(ViCommConst.DATASET_MAIL) ? "PARTNER_" : String.Empty);
		if (GetValue(pTag,sFieldNm,out sValue)) {
			using (UserMan oUserMan = new UserMan()) {
				return oUserMan.GetLastReceiptDate(sessionWoman.site.siteCd,sValue);
			}
		}
		return string.Empty;
	}

	private string GetLastLoginMin(string pTag) {
		DateTime dtLastLoginDate;
		GetValue(pTag,"LAST_LOGIN_DATE",out dtLastLoginDate);
		TimeSpan tsLastLoginMin = DateTime.Now - dtLastLoginDate;
		if (tsLastLoginMin.TotalMinutes < 0) {
			//DBｻｰﾊﾞとWEBｻｰﾊﾞの時間差を考慮 
			return "0";
		} else {
			return tsLastLoginMin.TotalMinutes.ToString("0");
		}
	}

	private string GetCrosmileAppRunIntent() {
		if (sessionWoman.userWoman.useCrosmileFlag == 0) {
			return string.Empty;
		}
		
		TimeSpan ts;
		ts = sessionWoman.userWoman.endDate - DateTime.Now;
		if (ts.TotalMinutes > 0) {
			return string.Format("{0}/Home/?waitingTime={1}",
						iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]),
						ts.TotalMinutes.ToString("0")
					);
		} else {
			return string.Empty;
		}
	}


	private string GetPartnerCrosmileFlag(string pTag) {
		string sUserSeq = string.Empty;
		if (GetValue(pTag,"USER_SEQ",out sUserSeq)) {
			using (User oUser = new User()) {
				if (oUser.GetOne(sUserSeq,ViCommConst.MAN)) {
					return oUser.useCrosmileFlag;
				}
			}
		}
		return string.Empty;
	}

	private string GetListPickup(string pTag,string pArgument) {
		DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		string sPickupId = iBridUtil.GetStringValue(oPartsArgs.Query["pickupid"]);
		string sSortType = iBridUtil.GetStringValue(oPartsArgs.Query["sorttype"]);
		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		string sRandom = iBridUtil.GetStringValue(oPartsArgs.Query["random"]);
		string sPickupStartDay = iBridUtil.GetStringValue(oPartsArgs.Query["pickupstartday"]);
		string sLastLoginDate = iBridUtil.GetStringValue(oPartsArgs.Query["lastlogin"]);


		if (string.IsNullOrEmpty(sPickupId)) {
			throw new ApplicationException("pickupid is empty.");
		}


		string sPickupType = string.Empty;
		using (Pickup oPickup = new Pickup()) {
			sPickupType = oPickup.GetPickupType(sessionObjs.site.siteCd,sPickupId);
		}
		if (string.IsNullOrEmpty(sPickupType)) {
			throw new ApplicationException("pickupid not found.id:" + sPickupId);
		}


		SeekCondition oSeekCondition = new SeekCondition();
		oSeekCondition.InitCondtition();
		oSeekCondition.pickupId = sPickupId;
		oSeekCondition.sortType = sSortType;
		oSeekCondition.pickupStartPubDay = sPickupStartDay;
		oSeekCondition.directRandom = sRandom.Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		oSeekCondition.lastLoginDate = sLastLoginDate;
		oSeekCondition.orderAsc = iBridUtil.GetStringValue(oPartsArgs.Query["orderasc"]);
		oSeekCondition.orderDesc = iBridUtil.GetStringValue(oPartsArgs.Query["orderdesc"]);

		int iDataSetIndex = 0;
		string sActCategory = string.Empty;
		
		DataSet oDataSet = null;
		switch (sPickupType) {
			case ViCommConst.PicupTypes.BBS_MOVIE:
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
				using (CastMovie oCastMovie = new CastMovie(sessionObjs)) {
					oDataSet = oCastMovie.GetPageCollection(sessionObjs.site.siteCd,string.Empty,string.Empty,string.Empty,false,string.Empty,sActCategory,sAttrTypeSeq,sAttrSeq,oSeekCondition,sSortType,1,oPartsArgs.NeedCount,true);
				}
				iDataSetIndex = ViCommConst.DATASET_PARTNER_MOVIE;
				break;
			case ViCommConst.PicupTypes.BBS_PIC:
				using (CastPic oCastPic = new CastPic(sessionObjs)) {
					oDataSet = oCastPic.GetPageCollection(sessionObjs.site.siteCd,string.Empty,string.Empty,false,ViCommConst.ATTACHED_BBS.ToString(),sActCategory,sAttrTypeSeq,sAttrSeq,oSeekCondition,1,oPartsArgs.NeedCount);
				}
				iDataSetIndex = ViCommConst.DATASET_PARTNER_PIC;
				break;
			case ViCommConst.PicupTypes.PROFILE_PIC:
				using (CastPic oCastPic = new CastPic(sessionObjs)) {
					oDataSet = oCastPic.GetPageCollection(sessionObjs.site.siteCd,string.Empty,string.Empty,false,ViCommConst.ATTACHED_PROFILE.ToString(),sActCategory,sAttrTypeSeq,sAttrSeq,oSeekCondition,1,oPartsArgs.NeedCount);
				}
				iDataSetIndex = ViCommConst.DATASET_PARTNER_PIC;
				break;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				oSeekCondition.siteCd = sessionObjs.site.siteCd;
				oSeekCondition.userSeq = sessionWoman.userWoman.userSeq;
				oSeekCondition.userCharNo = sessionWoman.userWoman.curCharNo;
				oSeekCondition.conditionFlag = ViCommConst.INQUIRY_RECOMMEND;
				oSeekCondition.newCastDay = 0;
				oSeekCondition.userRank = "";

				oSeekCondition.categorySeq = sActCategory;

				using (Cast oCast = new Cast(sessionObjs)) {

					oDataSet = oCast.GetPageCollection(oSeekCondition,1,oPartsArgs.NeedCount);
				}

				iDataSetIndex = ViCommConst.DATASET_CAST;
				break;
		}

		if (oDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,iDataSetIndex);
	}

	private string GetSetupFreeDialUrl(string pTag,string pArgument) {
		string sValue = string.Empty;
		UrlBuilder oUrl = new UrlBuilder(Path.GetFileName(sessionWoman.requestQuery.Url.AbsolutePath),sessionWoman.requestQuery.QueryString);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (sessionWoman.userWoman.useFreeDialFlag == 1) {
			sValue = string.Format("SetupFreeDial.aspx?setup=0&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		} else {
			sValue = string.Format("SetupFreeDial.aspx?setup=1&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		}

		return sValue;
	}

	private string GetSetupCrosmileUrl(string pTag,string pArgument) {
		string sValue = string.Empty;
		UrlBuilder oUrl = new UrlBuilder(Path.GetFileName(sessionWoman.requestQuery.Url.AbsolutePath),sessionWoman.requestQuery.QueryString);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (sessionWoman.userWoman.useCrosmileFlag == 1) {
			sValue = string.Format("SetupTalkApp.aspx?setup=0&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		} else {
			sValue = string.Format("SetupTalkApp.aspx?setup=1&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		}

		return sValue;
	}

	private string GetSetupVoiceappUrl(string pTag,string pArgument) {
		string sValue = string.Empty;
		UrlBuilder oUrl = new UrlBuilder(Path.GetFileName(sessionWoman.requestQuery.Url.AbsolutePath),sessionWoman.requestQuery.QueryString);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (sessionWoman.userWoman.useVoiceappFlag == 1) {
			sValue = string.Format("SetupVoiceApp.aspx?setup=0&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		} else {
			sValue = string.Format("SetupVoiceApp.aspx?setup=1&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		}

		return sValue;
	}

	private string GetManHandleNmByQueryLoginId(string pTag) {
		string sValue = string.Empty;
		string sLoginId = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["loginid"]);
		
		if (string.IsNullOrEmpty(sLoginId)) {
			return string.Empty;
		}
		
		using (Man oMan = new Man()) {
			sValue = oMan.GetManHandleNmByLoginId(sessionWoman.site.siteCd,sLoginId);
		}
		
		return sValue;
	}

	private bool IsSelfEnableProfileImageRegistCampaign(string pTag,string pArgument) {
		if (sessionWoman.userWoman.CurCharacter != null) {
			if (this.IsNewMember()) {
				if (IsValidMask(sessionWoman.userWoman.CurCharacter.userDefineMask,"12").Equals(ViCommConst.FLAG_OFF_STR)) {
					if (sessionWoman.userWoman.CurCharacter.picSeq.Equals("0")) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/// <summary>
	/// 退会申請状態を判定する
	/// </summary>
	/// <param name="pWithdrawalStatus"></param>
	/// <returns></returns>
	private bool EqualsWithdrawalStatus(string pWithdrawalStatus) {
		User oUser = null;
		DataSet ds = null;
		DataRow dr = null;
		string userStatus = string.Empty;
		string existsAccept = string.Empty;

		// すでに取得済みの場合、セッションに保存している値を使う
		if (!string.IsNullOrEmpty(sessionWoman.userWoman.existAcceptWithdrawalFlag)) {
			userStatus = sessionWoman.userWoman.userStatus;
			existsAccept = sessionWoman.userWoman.existAcceptWithdrawalFlag;
		} else {
			oUser = new User();
			ds = oUser.GetWithdrawalStatus(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			if (ds.Tables[0].Rows.Count <= 0) {
				// ユーザ情報が取得できなかった場合
				return false;
			}
			dr = ds.Tables[0].Rows[0];
			userStatus = dr["USER_STATUS"].ToString();
			existsAccept = dr["EXISTS_ACCEPT"].ToString();

			// セッションに保存
			sessionWoman.userWoman.userStatus = userStatus;
			sessionWoman.userWoman.existAcceptWithdrawalFlag = existsAccept;
		}

		// 退会処理完了かどうかを判定
		if (pWithdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE)) {
			if (userStatus.Equals(ViCommConst.USER_WOMAN_RESIGNED)) {
				// 出演者ステータスが退会の場合
				return true;
			}
			// 出演者ステータスが退会以外の場合
			return false;
		}
		// 退会申請中かどうかを判定
		else if (pWithdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_ACCEPT)) {
			if (userStatus.Equals(ViCommConst.USER_WOMAN_RESIGNED)) {
				// 出演者ステータスが退会の場合
				return false;
			}
			if (existsAccept.Equals(ViCommConst.FLAG_ON_STR)) {
				// 退会申請中の場合
				return true;
			}
			// 退会申請中以外の場合
			return false;
		}
		// 退会申請なし(未遂、継続含む)かどうかを判定
		if (userStatus.Equals(ViCommConst.USER_WOMAN_RESIGNED)) {
			// 出演者ステータスが退会の場合
			return false;
		}
		if (existsAccept.Equals(ViCommConst.FLAG_ON_STR)) {
			// 退会申請中の場合
			return false;
		}
		return true;
	}

	/// <summary>
	/// 退会申請が許可された状態かを判定
	/// </summary>
	/// <returns></returns>
	private bool IsApprovalWithdrawal() {
		// 清算申請中
		if (sessionWoman.userWoman.waitPaymentFlag == ViCommConst.FLAG_ON) {
			return false;
		}

		// 清算履歴を取得
		string sPaymentDate = GetLastPaymentDate();
		DateTime dtPaymentDate = new DateTime();
		if (string.IsNullOrEmpty(sPaymentDate)) {
			PaymentHistory oPaymentHistory = new PaymentHistory();
			bool bExist = oPaymentHistory.GetLastPaymentDate(sessionWoman.userWoman.userSeq,out dtPaymentDate);
			if (!bExist) {
				// 清算履歴が存在しない場合
				return true;
			}
		} else {
			dtPaymentDate = DateTime.Parse(sPaymentDate);
		}

		DateTime dtLastMonth = DateTime.Now.AddMonths(-1);

		// 最終精算日から１ヶ月以上経過
		if (dtPaymentDate <= dtLastMonth) {
			return true;
		}
		return false;
	}
}
