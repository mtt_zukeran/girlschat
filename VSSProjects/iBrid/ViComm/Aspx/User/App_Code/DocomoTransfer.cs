﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: ドコモケータイ送金
--	Progaram ID		: DocomoTransfer
--
--  Creation Date	: 2011.09.27
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class DocomoTransfer : DbSession {
	public DocomoTransfer() {
	}

	public string GetDocomoTransferTel(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT												").AppendLine();
		oSqlBuilder.Append(" 	DOCOMO_MONEY_TRANSFER_TEL						").AppendLine();
		oSqlBuilder.Append(" FROM												").AppendLine();
		oSqlBuilder.Append(" 	T_DOCOMO_TRANSFER								").AppendLine();
		oSqlBuilder.Append(" WHERE												").AppendLine();
		oSqlBuilder.Append(" 	T_DOCOMO_TRANSFER.SITE_CD		= :SITE_CD	AND	").AppendLine();
		oSqlBuilder.Append(" 	T_DOCOMO_TRANSFER.USED_FLAG		= :USED_FLAG	").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USED_FLAG",ViCommConst.FLAG_ON_STR);

				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
	}

	public string GetDocomoTransferNm(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT												").AppendLine();
		oSqlBuilder.Append(" 	DOCOMO_MONEY_TRANSFER_NM						").AppendLine();
		oSqlBuilder.Append(" FROM												").AppendLine();
		oSqlBuilder.Append(" 	T_DOCOMO_TRANSFER								").AppendLine();
		oSqlBuilder.Append(" WHERE												").AppendLine();
		oSqlBuilder.Append(" 	T_DOCOMO_TRANSFER.SITE_CD		= :SITE_CD	AND	").AppendLine();
		oSqlBuilder.Append(" 	T_DOCOMO_TRANSFER.USED_FLAG		= :USED_FLAG	").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USED_FLAG",ViCommConst.FLAG_ON_STR);

				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
	}
}
