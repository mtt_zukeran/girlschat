﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using iBridCommLib;

[System.Serializable]
public class UserDefPointHistory:DbSession {

	/// <summary>
	/// 退会時のポイント付与日時（最新1件）を取得する
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <returns></returns>
	public string GetWithdrawalAddPointDate(string pSiteCd,string pUserSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sPointDate = string.Empty;

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	TO_CHAR(MAX(ADD_POINT_DATE), 'YYYY-MM-DD HH24:MI:SS') AS ADD_POINT_DATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER_DEF_POINT_HISTORY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("	AND USER_SEQ = :USER_SEQ");
		oSqlBuilder.AppendLine("	AND (");
		oSqlBuilder.AppendLine("		(");
		oSqlBuilder.AppendLine("			ADD_POINT_ID = '_SYSTEM_POINT'");
		oSqlBuilder.AppendLine("			AND REMARKS LIKE 'ADMIN:退会%'");
		oSqlBuilder.AppendLine("		) OR (");
		oSqlBuilder.AppendLine("			ADD_POINT_ID IN ('WITHDRAWAL_300','WITHDRAWAL_500','WITHDRAWAL_1000')");
		oSqlBuilder.AppendLine("		)");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	SITE_CD, USER_SEQ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sPointDate = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ADD_POINT_DATE"]);
		}

		return sPointDate;
	}
}
