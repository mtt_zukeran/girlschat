﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 個人広告
--	Progaram ID		: PersonalAd
--
--  Creation Date	: 2010.12.11
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class PersonalAd:DbSession {

	public PersonalAd() {
	}

	public string GetFriendIntroCd(string pPersonalAdCd) {
		DataSet ds;
		DataRow dr;
		string sFriendIntroCd = string.Empty;

		try {
			conn = DbConnect("PersonalAd.GetFriendIntroCd");

			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	SEX_CD				,");
			sSql.AppendLine("	MAN_FRIEND_INTRO_CD ,");
			sSql.AppendLine("	CAST_FRIEND_INTRO_CD ");
			sSql.AppendLine("FROM	");
			sSql.AppendLine("	VW_PERSONAL_AD01	");
			sSql.AppendLine("WHERE	");
			sSql.AppendLine("	PERSONAL_AD_CD	= :PERSONAL_AD_CD	");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("PERSONAL_AD_CD",pPersonalAdCd);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						if (dr["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
							sFriendIntroCd = dr["MAN_FRIEND_INTRO_CD"].ToString();
						} else {
							sFriendIntroCd = dr["CAST_FRIEND_INTRO_CD"].ToString();
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sFriendIntroCd;
	}

	public string GetPersonalAdCd(string pUserSeq) {
		DataSet ds;
		DataRow dr;
		string sAdCd = string.Empty;

		try {
			conn = DbConnect("PersonalAd.GetPersonalAdCd");

			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	PERSONAL_AD_CD		");
			sSql.AppendLine("FROM	");
			sSql.AppendLine("	T_PERSONAL_AD	");
			sSql.AppendLine("WHERE	");
			sSql.AppendLine("	USER_SEQ = :USER_SEQ	");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sAdCd = dr["PERSONAL_AD_CD"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sAdCd;
	}

	public string IssuePersonalAd(string pSiteCd,string pUserSeq,string pUserCharNo,string pPersonalAdCd) {
		string sResult = "";
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ISSUE_PERSONAL_AD");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPERSONAL_AD_CD",DbSession.DbType.VARCHAR2,pPersonalAdCd);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("PRESULT");
		}
		return sResult;
	}
}

