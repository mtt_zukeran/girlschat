﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 曜日別ランダム表示広告
--	Progaram ID		: RandomAdDayOfWeek
--
--  Creation Date	: 2010.03.29
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System.Data;

[System.Serializable]
public class RandomAdDayOfWeek:DbSession {

	public RandomAdDayOfWeek() {
	}

	public DataSet GetOne(string pSiteCd, string pSexCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_RANDOM_AD_DAYOFWEEK WHERE SITE_CD =:SITE_CD AND SEX_CD =:SEX_CD AND DAY_CD = TO_CHAR(SYSDATE,'D')";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SEX_CD", pSexCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
