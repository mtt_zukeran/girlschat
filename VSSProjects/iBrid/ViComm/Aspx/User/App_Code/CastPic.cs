﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者写真

--	Progaram ID		: CastPic
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System.Data;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Configuration;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Collections.Generic;
using System.Collections.Specialized;

[Serializable]
public class CastPicSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public List<string> PicTypeList = new List<string>();
	public string ProfilePicFlag;
	public string ObjNaFlag;
	public string ObjNotApproveFlag;
	public string ObjNotPublishFlag;
	public bool IsAppendObjUsedHistory = false;

	public CastPicSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastPicSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[System.Serializable]
public class CastPic:CastBase {

	public CastPic()
		: base() {
	}
	public CastPic(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,SeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,"","",pCondition,false,false,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,"","",new SeekCondition(),false,false,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),false,false,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),false,false,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,false,false,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pOnlyUsed,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),false,pOnlyUsed,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pOnlyUsed,SeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,false,pOnlyUsed,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,false,false,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pHidePicViewFlag,bool pOnlyUsed,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),pHidePicViewFlag,pOnlyUsed,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pHidePicViewFlag,bool pOnlyUsed,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),pHidePicViewFlag,pOnlyUsed,string.Empty,pRecPerPage,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pHidePicViewFlag,bool pOnlyUsed,string pPfNotApproved,int pRecPerPage,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),pHidePicViewFlag,pOnlyUsed,pPfNotApproved,pRecPerPage,out pRecCount);
	}

	private int GetPageCountBase(FlexibleQueryType pFlexibleQueryType,string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,bool pIncludeNotPublish,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,bool pHidePicViewFlag,bool pOnlyUsed,string pPfNotApproved,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = CreateFlexibleQueryTarget(pFlexibleQueryType,pCondition,pPicType);

		string sSuffix = "VW";
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastPic"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			sSuffix = "MV";
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT								").AppendLine();
		oSqlBuilder.Append("	COUNT(P.PIC_SEQ) AS ROW_COUNT		").AppendLine();
		oSqlBuilder.Append(" FROM								").AppendLine();
		oSqlBuilder.Append(string.Format("	{0}_CAST_PIC00 P",sSuffix)).AppendLine();

		if (!string.IsNullOrEmpty(pCondition.pickupId)) {
			oSqlBuilder.Append("	, VW_PICKUP_OBJS00	").AppendLine();
		}
		// いいねした画像のみ検索
		if (pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oSqlBuilder.AppendLine("	, T_CAST_PIC_LIKE PL");
		}

		if (pCondition.hasExtend) {
			for (int i = 0;i < pCondition.attrValue.Count;i++) {
				if ((!pCondition.attrValue[i].Equals(string.Empty)) && (!pCondition.attrValue[i].Equals("*"))) {
					oSqlBuilder.AppendFormat(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1).AppendLine();
				}
			}
		}

		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,string.Empty,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,pHidePicViewFlag,pOnlyUsed,pPfNotApproved,false,ref sWhere);

		oSqlBuilder.Append(sWhere);

		try {
			conn = DbConnect("CastPic.GetPageCountByLoginId");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}

		if (pFlexibleQueryType == FlexibleQueryType.None && oFlexibleQueryTarget.HasTarget) {
			decimal dExcludeRecCount = decimal.Zero;
			GetPageCountBase(FlexibleQueryType.InScope,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,pHidePicViewFlag,pOnlyUsed,pPfNotApproved,pRecPerPage,out dExcludeRecCount);
			iPages = CalcExcludeCount(ref pRecCount,dExcludeRecCount,pRecPerPage);
		}

		return iPages;
	}

	public DataSet GetPageCollectionBySeq(string pPicSeq) {
		return GetPageCollectionBase(pPicSeq,"","","",string.Empty,false,"","","","",new SeekCondition(),false,false,string.Empty,0,0);
	}
	public DataSet GetPageCollectionBySeq(string pPicSeq,string pPicType) {
		return GetPageCollectionBase(pPicSeq,"","","",string.Empty,false,pPicType,"","","",new SeekCondition(),false,false,string.Empty,0,0);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,SeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,"","",pCondition,false,false,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,"","",new SeekCondition(),false,false,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),false,false,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),false,false,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,false,false,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pOnlyUsed,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),false,pOnlyUsed,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pOnlyUsed,SeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,false,pOnlyUsed,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,false,false,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,bool pIncludeNotApproveFlag,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pHidePicViewFlag,bool pOnlyUsed,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),pHidePicViewFlag,pOnlyUsed,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pHidePicViewFlag,bool pOnlyUsed,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),pHidePicViewFlag,pOnlyUsed,string.Empty,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,bool pHidePicViewFlag,bool pOnlyUsed,string pPfNotApproved,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pApproveStatus,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,new SeekCondition(),pHidePicViewFlag,pOnlyUsed,pPfNotApproved,pPageNo,pRecPerPage);
	}

	public void GetPrevNextPicSeq(
		string pPicSeq,
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		bool pIncludeNotApproveFlag,
		string pPicType,
		string pActCategorySeq,
		string pCastPicAttrTypeSeq,
		string pCastPicAttrSeq,
		bool pOnlyUsed,
		out string pPrevPicSeq,
		out string pNextPicSeq) {
		GetPrevNextPicSeq(pPicSeq,pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pOnlyUsed,new SeekCondition(),out pPrevPicSeq,out pNextPicSeq);
	}
	public void GetPrevNextPicSeq(
		string pPicSeq,
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		bool pIncludeNotApproveFlag,
		string pPicType,
		string pActCategorySeq,
		string pCastPicAttrTypeSeq,
		string pCastPicAttrSeq,
		bool pOnlyUsed,
		SeekCondition pCondition,
		out string pPrevPicSeq,
		out string pNextPicSeq) {

		pPrevPicSeq = "";
		pNextPicSeq = "";

		try {
			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastPic"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			conn = DbConnect("CastPic.GetPrevNextPicSeq");
			DataSet ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM(");
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		PIC_SEQ,").AppendLine();
			sSql.Append("		LAG(PIC_SEQ)	OVER (ORDER BY DISPLAY_POSITION,PIC_SEQ DESC) AS PREV_PIC_SEQ,").AppendLine();
			sSql.Append("		LEAD(PIC_SEQ)	OVER (ORDER BY DISPLAY_POSITION,PIC_SEQ DESC) AS NEXT_PIC_SEQ");
			sSql.Append(string.Format("	FROM {0}_CAST_PIC00 P ",sSuffix)).AppendLine();

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope,pCondition,pPicType),"",pSiteCd,pLoginId,pUserCharNo,pIncludeNotApproveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,false,pOnlyUsed,string.Empty,false,ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(") WHERE PIC_SEQ =:PIC_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("PIC_SEQ",pPicSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC00");
				}
			}
			if (ds.Tables[0].Rows.Count != 0) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (dr["PIC_SEQ"].ToString().Equals(pPicSeq)) {
						pPrevPicSeq = dr["PREV_PIC_SEQ"].ToString();
						pNextPicSeq = dr["NEXT_PIC_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetPageCollectionBase(string pPicSeq,string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,bool pIncludeNotPublish,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,bool pHidePicViewFlag,bool pOnlyUsed,string pPfNotApproved,int pPageNo,int pRecPerPage) {
		DataSet ds;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.None,pCondition,pPicType);

		try {
			conn = DbConnect("CastPic.GetPageCollectionByLoginId");
			ds = new DataSet();

			string sOrder;
			GetOrderBy(!string.IsNullOrEmpty(pCondition.pickupId),pCondition.directRandom == ViCommConst.FLAG_ON,pCondition.sortType,pLoginId,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,out sOrder);

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastPic"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder sInnerSql = new StringBuilder();

			sInnerSql.Append("SELECT * FROM ");
			sInnerSql.Append("(SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT 											").AppendLine();
			sInnerSql.Append("			P.*, 										").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			D.GOOD_POINT AS GOOD_POINT_DAILY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			W.GOOD_POINT AS GOOD_POINT_WEEKLY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			M.GOOD_POINT AS GOOD_POINT_MONTHLY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
				sInnerSql.AppendLine("NVL(LW.LIKE_COUNT,0) AS LIKE_COUNT_WEEK,");
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
				sInnerSql.AppendLine("NVL(LM.LIKE_COUNT,0) AS LIKE_COUNT_MONTH,");
			}
			sInnerSql.Append("			NVL(T.REVIEW_COUNT,0) AS REVIEW_COUNT,		").AppendLine();
			sInnerSql.Append("			NVL(T.COMMENT_COUNT,0) AS COMMENT_COUNT,	").AppendLine();
			sInnerSql.Append("			NVL(T.GOOD_STAR_TOTAL,0) AS GOOD_STAR_TOTAL,").AppendLine();

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sInnerSql.AppendLine("		1 AS BOOKMARK_FLAG	");
				} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
				} else {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						sInnerSql.AppendLine("CASE WHEN EXISTS(");
						sInnerSql.AppendLine("SELECT 1 FROM T_OBJ_BOOKMARK BKM WHERE");
						sInnerSql.AppendLine(" BKM.SITE_CD = P.SITE_CD AND");
						sInnerSql.AppendLine(" BKM.OBJ_SEQ = P.PIC_SEQ AND");
						sInnerSql.AppendFormat(" BKM.USER_SEQ = {0} AND",sSelfUserSeq).AppendLine();
						sInnerSql.AppendFormat(" BKM.USER_CHAR_NO = {0}",sSelfUserCharNo).AppendLine();
						sInnerSql.AppendLine(") THEN 1 ELSE 0 END AS BOOKMARK_FLAG");
					} else {
						sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
					}
				}
			} else {
				sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
			}

			sInnerSql.Append("		FROM (											").AppendLine();
			sInnerSql.Append("	SELECT " + CastInfoFields() + ",");
			if (!string.IsNullOrEmpty(pCondition.pickupId)) {
				sInnerSql.Append("	VW_PICKUP_OBJS00.PICKUP_TITLE				,	").AppendLine();
				sInnerSql.Append("	VW_PICKUP_OBJS00.COMMENT_PICKUP			AS PICKUP_OBJ_COMMENT	,	").AppendLine();
				sInnerSql.Append("	VW_PICKUP_OBJS00.PICKUP_START_PUB_DAY		,	").AppendLine();
				sInnerSql.Append("	VW_PICKUP_OBJS00.PICKUP_END_PUB_DAY			,	").AppendLine();
			}
			sInnerSql.Append("		P.DISPLAY_POSITION				,").AppendLine();
			sInnerSql.Append("		P.PIC_SEQ						,").AppendLine();
			sInnerSql.Append("		P.UNIQUE_VALUE					,").AppendLine();
			sInnerSql.Append("		P.UPLOAD_DATE					,").AppendLine();
			sInnerSql.Append("		P.PIC_TYPE						,").AppendLine();
			sInnerSql.Append("		P.PIC_TITLE						,").AppendLine();
			sInnerSql.Append("		P.PIC_DOC						,").AppendLine();
			sInnerSql.Append("		P.OBJ_NOT_APPROVE_FLAG			,").AppendLine();
			sInnerSql.Append("		P.OBJ_NOT_PUBLISH_FLAG			,").AppendLine();
			sInnerSql.Append("		P.CAST_PIC_ATTR_TYPE_SEQ		,").AppendLine();
			sInnerSql.Append("		P.CAST_PIC_ATTR_SEQ				,").AppendLine();
			sInnerSql.Append("		P.PLANNING_TYPE					,").AppendLine();
			sInnerSql.Append("		P.PROFILE_PIC_FLAG AS CUR_PIC_IS_PROFILE,").AppendLine();
			sInnerSql.Append("		P.READING_COUNT					,").AppendLine();
			sInnerSql.Append("		P.OBJ_NA_FLAG					,").AppendLine();
			sInnerSql.Append("		P.OBJ_RANKING_FLAG				,").AppendLine();
			sInnerSql.Append("		P.LIKE_COUNT					,").AppendLine();
			sInnerSql.Append("		P.COMMENT_COUNT AS CAST_PIC_COMMENT_COUNT").AppendLine();
			sInnerSql.Append(string.Format("	FROM {0}_CAST_PIC00 P ",sSuffix)).AppendLine();

			if (!string.IsNullOrEmpty(pCondition.pickupId)) {
				sInnerSql.Append("	, VW_PICKUP_OBJS00	").AppendLine();
			}
			// いいねした画像のみ検索
			if (pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sInnerSql.AppendLine("	, T_CAST_PIC_LIKE PL");
			}

			if (pCondition != null && pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						sInnerSql.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
					}
				}
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pPicSeq,pSiteCd,pLoginId,pUserCharNo,pApproveStatus,pIncludeNotPublish,pPicType,pActCategorySeq,pCastPicAttrTypeSeq,pCastPicAttrSeq,pCondition,pHidePicViewFlag,pOnlyUsed,pPfNotApproved,true,ref sWhere);
			sInnerSql.Append(sWhere).AppendLine();

			sInnerSql.Append("			) P,							").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_DAILY D,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_WEEKLY W,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_MONTHLY M,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
				sInnerSql.AppendLine("T_CAST_PIC_LIKE_WEEK LW,");
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
				sInnerSql.AppendLine("T_CAST_PIC_LIKE_MONTH LM,");
			}
			sInnerSql.Append("			T_OBJ_REVIEW_TOTAL	T			").AppendLine();
			sInnerSql.Append("		WHERE								").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			P.SITE_CD	= D.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.PIC_SEQ	= D.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			P.SITE_CD	= W.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.PIC_SEQ	= W.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			P.SITE_CD	= M.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.PIC_SEQ	= M.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
				sInnerSql.AppendLine("P.SITE_CD = LW.SITE_CD (+) AND");
				sInnerSql.AppendLine("P.PIC_SEQ = LW.PIC_SEQ (+) AND");
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
				sInnerSql.AppendLine("P.SITE_CD = LM.SITE_CD (+) AND");
				sInnerSql.AppendLine("P.PIC_SEQ = LM.PIC_SEQ (+) AND");
			}
			sInnerSql.Append("			P.SITE_CD	= T.SITE_CD	(+) AND	").AppendLine();
			sInnerSql.Append("			P.PIC_SEQ	= T.OBJ_SEQ	(+)		").AppendLine();

			sInnerSql.Append(sOrder).AppendLine();
			if (pPicSeq.Equals(string.Empty)) {
				sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sInnerSql.Append(")INNER ) ");
			}

			SessionObjs oObj = this.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			StringBuilder sJoinTable = new StringBuilder();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_PIC_ATTR_TYPE.SITE_CD						(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_PIC_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ		(+)	AND").AppendLine();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD				(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_PIC_ATTR_TYPE_SEQ	= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ	(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_PIC_ATTR_SEQ			= T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ		(+)	AND");

			StringBuilder sJoinField = new StringBuilder();
			sJoinField.Append("TO_CHAR(T1.UPLOAD_DATE,'YYYY/MM/DD HH24:MI')							AS UPLOAD_DATE_RAW							,").AppendLine();
			sJoinField.Append("GET_PHOTO_IMG_PATH				(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ)	AS OBJ_PHOTO_IMG_PATH						,").AppendLine();
			sJoinField.Append("GET_SMALL_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ)	AS OBJ_SMALL_PHOTO_IMG_PATH					,").AppendLine();
			sJoinField.Append("GET_BLUR_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ)	AS OBJ_BLUR_PHOTO_IMG_PATH					,").AppendLine();
			sJoinField.Append("GET_SMALL_BLUR_PHOTO_IMG_PATH	(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ)	AS OBJ_SMALL_BLUR_PHOTO_IMG_PATH			,").AppendLine();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開中', '非公開'), '申請中')	AS UNAUTH_MARK		,").AppendLine();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開', '非公開'), '認証待ち')	AS UNAUTH_MARK_ADMIN,").AppendLine();
			sJoinField.Append("T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_NM																		,").AppendLine();
			sJoinField.Append("T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_NM																		,").AppendLine();

			sSql = string.Format(sSql,sInnerSql.ToString(),"T_CAST_PIC_ATTR_TYPE,T_CAST_PIC_ATTR_TYPE_VALUE,",sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				if (pPicSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
				}

				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					//da.Fill(ds,"VW_CAST_PIC00");
					this.Fill(da,ds,"VW_CAST_PIC00");
				}
			}
		} finally {
			conn.Close();
		}
		ds = this.Exclude(ds,pRecPerPage,oFlexibleQueryTarget);
		AppendAttr(ds);
		
		if (ds.Tables[0].Rows.Count > 0) {
			if (ds.Tables[0].Rows[0]["PIC_TYPE"].ToString().Equals(ViCommConst.ATTACHED_BBS.ToString())) {
				AppendBbsPic(ds);
			} else {
				AppendProfilePic(ds);
			}
		}

		return ds;
	}

	private void GetOrderBy(bool pIsPickup,bool pDirectRandom,string sSortType,string pLoginId,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,out string pOrder) {
		if (sSortType.Equals("*"))
			sSortType = string.Empty;

		switch (sSortType) {
			case ViCommConst.SortType.LAST_LOGIN_DATE:	//最終ﾛｸﾞｲﾝ日時順 
				pOrder = "ORDER BY P.LAST_LOGIN_DATE DESC, P.DISPLAY_POSITION, P.PIC_SEQ DESC";
				break;
			case ViCommConst.SortType.ONLINE_STATUS:	//オンライン優先 
				pOrder = "ORDER BY P.MONITOR_TALK_TYPE DESC,DECODE(P.CHARACTER_ONLINE_STATUS,2,5,1,4,P.CHARACTER_ONLINE_STATUS) DESC,P.LAST_ACTION_DATE DESC, P.DISPLAY_POSITION, P.PIC_SEQ DESC";
				break;
			case ViCommConst.SortType.PICKUP_START_PUB_DAY:	//ﾋﾟｯｸｱｯﾌﾟ開始日
				pOrder = "ORDER BY P.PICKUP_START_PUB_DAY DESC, P.PRIORITY ASC,P.DISPLAY_POSITION,P.PIC_SEQ DESC ";
				break;
			case ViCommConst.SortType.NEW:				//新着順 
				pOrder = "ORDER BY P.PIC_SEQ DESC";
				break;
			case ViCommConst.SortType.LOGINED:			//ﾛｸﾞｲﾝ中
			case ViCommConst.SortType.WAITING:			//待機中
				pOrder = "ORDER BY P.LAST_ACTION_DATE DESC, P.PIC_SEQ DESC";
				break;
			case ViCommConst.SortType.READING_COUNT:	//閲覧回数順

				pOrder = "ORDER BY P.READING_COUNT DESC, P.PIC_SEQ DESC";
				break;
			case ViCommConst.SortType.RANDOM:			//ランダム
				pOrder = "ORDER BY DBMS_RANDOM.RANDOM";
				break;
			default:
				if (pIsPickup) {
					if (pDirectRandom) {
						pOrder = "ORDER BY DBMS_RANDOM.RANDOM,P.PRIORITY ASC ";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
						pOrder = "ORDER BY GOOD_POINT_DAILY DESC NULLS LAST,P.PIC_SEQ DESC";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
						pOrder = "ORDER BY GOOD_POINT_WEEKLY DESC NULLS LAST,P.PIC_SEQ DESC";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
						pOrder = "ORDER BY GOOD_POINT_MONTHLY DESC NULLS LAST,P.PIC_SEQ DESC";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
						pOrder = "ORDER BY P.SITE_CD,LIKE_COUNT_WEEK DESC,P.UPLOAD_DATE DESC NULLS LAST";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
						pOrder = "ORDER BY P.SITE_CD,LIKE_COUNT_MONTH DESC,P.UPLOAD_DATE DESC NULLS LAST";
					} else {
						pOrder = "ORDER BY P.PRIORITY ASC,P.DISPLAY_POSITION,P.PIC_SEQ DESC ";
					}
				} else {
					string sOrderSpec = string.Empty;
					if (!pLoginId.Equals(string.Empty)) {
						sOrderSpec = sOrderSpec + "P.LOGIN_ID,";
					}
					if (!pCastPicAttrTypeSeq.Equals(string.Empty)) {
						sOrderSpec = sOrderSpec + "P.CAST_PIC_ATTR_TYPE_SEQ,";
					}
					if (!pCastPicAttrSeq.Equals(string.Empty)) {
						sOrderSpec = sOrderSpec + "P.CAST_PIC_ATTR_SEQ,";
					}
					if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
						sOrderSpec = sOrderSpec + "GOOD_POINT_DAILY DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
						sOrderSpec = sOrderSpec + "GOOD_POINT_WEEKLY DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
						sOrderSpec = sOrderSpec + "GOOD_POINT_MONTHLY DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
						sOrderSpec = sOrderSpec + "LIKE_COUNT_WEEK DESC,P.UPLOAD_DATE DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
						sOrderSpec = sOrderSpec + "LIKE_COUNT_MONTH DESC,P.UPLOAD_DATE DESC NULLS LAST,";
					}
					pOrder = string.Format("ORDER BY P.SITE_CD,P.NA_FLAG,P.OBJ_NA_FLAG,PIC_TYPE,{0}P.DISPLAY_POSITION,P.PIC_SEQ DESC ",sOrderSpec);
				}
				break;
		}
	}

	private string CastInfoFields() {
		string sField = CastBasicField();

		sField = sField + ",P.ENABLED_BLOG_FLAG ";
		sField = sField + ",P.BLOG_SEQ ";
		sField = sField + ",P.BLOG_TITLE ";
		sField = sField + ",P.BLOG_DOC ";

		return sField;
	}

	private OracleParameter[] CreateWhere(FlexibleQueryTargetInfo pFlexibleQueryTarget,string pPicSeq,string pSiteCd,string pLoginId,string pUserCharNo,string pApproveStatus,bool pIncludeNotPublish,string pPicType,string pActCategorySeq,string pCastPicAttrTypeSeq,string pCastPicAttrSeq,SeekCondition pCondition,bool pHidePicViewFlag,bool pOnlyUsed,string pPfNotApproved,bool bAllInfo,ref string pWhere) {
		if (pCastPicAttrSeq.Equals("*")) {
			pCastPicAttrSeq = string.Empty;
		}

		ArrayList list = new ArrayList();
		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		if (!pPicSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.PIC_SEQ = :PIC_SEQ",ref pWhere);
			list.Add(new OracleParameter("PIC_SEQ",pPicSeq));
		} else {
			if (pSiteCd.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
				list.Add(new OracleParameter("SITE_CD",pSiteCd));
			}

			if (pPfNotApproved.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("P.NA_FLAG IN( :NA_FLAG1,:NA_FLAG2 ) ",ref pWhere);
				list.Add(new OracleParameter("NA_FLAG1",ViCommConst.NA_CHAR_NONE));
				list.Add(new OracleParameter("NA_FLAG2",ViCommConst.NA_CHAR_PF_NOT_APPROVED));
			} else {
				SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhere);
				list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
			}


			if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR) && pIncludeNotPublish == false) {
				SysPrograms.SqlAppendWhere("P.OBJ_NA_FLAG = :OBJ_NA_FLAG",ref pWhere);
				list.Add(new OracleParameter("OBJ_NA_FLAG",ViCommConst.FLAG_OFF));
			} else {
				if (!string.IsNullOrEmpty(pApproveStatus)) {
					SysPrograms.SqlAppendWhere("P.OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pApproveStatus));
				}
				if (pIncludeNotPublish == false) {
					SysPrograms.SqlAppendWhere("P.OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));
				}
			}

			if (pPicType.Equals("") == false) {
				if (pHidePicViewFlag) {
					SysPrograms.SqlAppendWhere("P.PIC_TYPE IN (:PIC_TYPE1,:PIC_TYPE2)",ref pWhere);
					list.Add(new OracleParameter("PIC_TYPE1",pPicType));
					list.Add(new OracleParameter("PIC_TYPE2",ViCommConst.ATTACHED_HIDE.ToString()));
				} else {
					SysPrograms.SqlAppendWhere("P.PIC_TYPE = :PIC_TYPE",ref pWhere);
					list.Add(new OracleParameter("PIC_TYPE",pPicType));
				}
			}

			if (pLoginId.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.LOGIN_ID = :LOGIN_ID",ref pWhere);
				SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginId));
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}

			if (!string.IsNullOrEmpty(pCastPicAttrTypeSeq)) {
				SysPrograms.SqlAppendWhere("P.CAST_PIC_ATTR_TYPE_SEQ = :CAST_PIC_ATTR_TYPE_SEQ",ref pWhere);
				list.Add(new OracleParameter("CAST_PIC_ATTR_TYPE_SEQ",pCastPicAttrTypeSeq));
			}

			if (!string.IsNullOrEmpty(pCastPicAttrSeq)) {
				SysPrograms.SqlAppendWhere("P.CAST_PIC_ATTR_SEQ = :CAST_PIC_ATTR_SEQ",ref pWhere);
				list.Add(new OracleParameter("CAST_PIC_ATTR_SEQ",pCastPicAttrSeq));
			}

			if (!string.IsNullOrEmpty(pCondition.screenId)) {
				if (!string.IsNullOrEmpty(pCastPicAttrSeq) && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
					using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
						if (oCastPicAttrTypeValue.GetOne(pSiteCd,pCastPicAttrSeq)) {
							SysPrograms.SqlAppendWhere("P.PLANNING_TYPE = :PLANNING_TYPE",ref pWhere);
							list.Add(new OracleParameter("PLANNING_TYPE",oCastPicAttrTypeValue.planningType));
						}
					}
				} else {
					SysPrograms.SqlAppendWhere("P.PLANNING_TYPE > 0",ref pWhere);
				}
			} else if (string.IsNullOrEmpty(pCondition.pickupId)) {
				SysPrograms.SqlAppendWhere("P.PLANNING_TYPE = 0",ref pWhere);
			}

			if (pActCategorySeq.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ",ref pWhere);
				list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pActCategorySeq));
			}

			// 拡張検索(一覧下部の検索フォームからの検索)の条件追加
			if (pCondition.hasExtend) {
				if (pCondition.onlineStatus == ViCommConst.SeekOnlineStatus.WAITING) {
					pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS = :EXT_ONLINE_STATUS_WAITING ";
					list.Add(new OracleParameter("EXT_ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				} else if (pCondition.onlineStatus == ViCommConst.SeekOnlineStatus.LOGINED) {
					pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS = :EXT_ONLINE_STATUS_LOGINED ";
					list.Add(new OracleParameter("EXT_ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				}
				if (!string.IsNullOrEmpty(pCondition.handleNm)) {
					pWhere = pWhere + " AND P.HANDLE_NM LIKE '%' || :EXT_HANDLE_NM || '%' ";
					list.Add(new OracleParameter("EXT_HANDLE_NM",pCondition.handleNm));
				}
				if (!string.IsNullOrEmpty(pCondition.ageFrom)) {
					pWhere = pWhere + " AND P.AGE >= :EXT_AGE_FROM ";
					list.Add(new OracleParameter("EXT_AGE_FROM",pCondition.ageFrom));
				}
				if (!string.IsNullOrEmpty(pCondition.ageTo)) {
					pWhere = pWhere + " AND P.AGE <= :EXT_AGE_TO ";
					list.Add(new OracleParameter("EXT_AGE_TO",pCondition.ageTo));
				}

				if (pCondition.newCastDay != 0) {
					pWhere = pWhere + " AND SYSDATE <= TO_DATE(P.START_PERFORM_DAY, 'YYYY/MM/DD') + :NEW_CAST_DAY ";
					list.Add(new OracleParameter("NEW_CAST_DAY",pCondition.newCastDay));
				}

				if (pCondition.attrValue.Count > 0) {
					this.CreateAttrQuery(pCondition,ref pWhere,ref list);
				}
			}

			if (!string.IsNullOrEmpty(pCondition.pickupId)) {
				// VW_PICKUP_OBJS00 × P
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.OBJ_SEQ		= P.PIC_SEQ ";
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.SITE_CD		= :SITE_CD	";
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.PICKUP_ID		= :PICKUP_ID ";
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.PICKUP_FLAG	= :PICKUP_FLAG ";
				list.Add(new OracleParameter(":SITE_CD",pSiteCd));
				list.Add(new OracleParameter(":PICKUP_ID",pCondition.pickupId));
				list.Add(new OracleParameter(":PICKUP_FLAG",ViCommConst.FLAG_ON));

				if (!string.IsNullOrEmpty(pCondition.pickupStartPubDay)) {
					pWhere = pWhere + " AND VW_PICKUP_OBJS00.PICKUP_START_PUB_DAY	<= TO_DATE(:PICKUP_START_PUB_DAY,'YYYYMMDD') ";
					list.Add(new OracleParameter(":PICKUP_START_PUB_DAY",pCondition.pickupStartPubDay));
				}
			}

			if (pOnlyUsed) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.PIC_SEQ				");
				oSubQueryBuilder.AppendLine(" ) ");
				pWhere = pWhere + oSubQueryBuilder.ToString();

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));

			} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.PIC_SEQ				");
				oSubQueryBuilder.AppendLine(" ) ");
				pWhere = pWhere + oSubQueryBuilder.ToString();

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX));

			}

			if (!string.IsNullOrEmpty(pCondition.rankType)) {
				SysPrograms.SqlAppendWhere("P.OBJ_RANKING_FLAG = 1",ref pWhere);
			}

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						StringBuilder oSubQueryBuilder = new StringBuilder();
						oSubQueryBuilder.AppendLine(" AND EXISTS(										");
						oSubQueryBuilder.AppendLine("	SELECT											");
						oSubQueryBuilder.AppendLine("		1											");
						oSubQueryBuilder.AppendLine("	FROM											");
						oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
						oSubQueryBuilder.AppendLine("	WHERE											");
						oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
						oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.PIC_SEQ			AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
						oSubQueryBuilder.AppendLine(" )													");
						pWhere = pWhere + oSubQueryBuilder.ToString();

						list.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
						list.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
					}
				}
			}

			if (pCondition.withoutProf.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("P.PROFILE_PIC_FLAG = 0",ref pWhere);
			}

			// いいねした画像のみ検索
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)
				&& pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)
			) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;

				SysPrograms.SqlAppendWhere("P.SITE_CD = PL.SITE_CD",ref pWhere);
				SysPrograms.SqlAppendWhere("P.PIC_SEQ = PL.PIC_SEQ",ref pWhere);
				SysPrograms.SqlAppendWhere(":MAN_USER_SEQ = PL.MAN_USER_SEQ",ref pWhere);
				list.Add(new OracleParameter(":MAN_USER_SEQ",oSessionMan.userMan.userSeq));
			}
		}

		if (pCondition.enabledBlog.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" P.ENABLED_BLOG_FLAG = :ENABLED_BLOG_FLAG  ",ref pWhere);
			list.Add(new OracleParameter(":ENABLED_BLOG_FLAG",ViCommConst.FLAG_ON));
		}

		if (!string.IsNullOrEmpty(pCondition.lastTxMailDays)) {
			SysPrograms.SqlAppendWhere("P.LAST_TX_MAIL_DATE >= SYSDATE - :LAST_TX_MAIL_DAYS",ref pWhere);
			list.Add(new OracleParameter("LAST_TX_MAIL_DAYS",pCondition.lastTxMailDays));
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}

		if (pPicType.Equals(ViCommConst.ATTACHED_BBS.ToString()) || pPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				pWhere += SetFlexibleQuery(pFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list);
			}
		}

		switch (pCondition.sortType) {
			case ViCommConst.SortType.LOGINED:
				pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING) ";
				list.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;
			case ViCommConst.SortType.WAITING:
				pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING) ";
				list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;

			default:
				break;
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public string GetPictureFileNm(string pPicSeq) {
		string sFileNm = string.Empty;
		using (DataSet oCastPicDataSet = this.GetPageCollectionBySeq(pPicSeq)) {
			if (oCastPicDataSet.Tables[0].Rows.Count != 0) {
				sFileNm = oCastPicDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"].ToString();
			}
		}
		return sFileNm;
	}

	private void AppendAttr(DataSet pDS) {
		SessionObjs oObj = this.sessionObj;
		string sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
						"WHERE " +
							"SITE_CD		= :SITE_CD		AND " +
							"USER_SEQ		= :USER_SEQ		AND	" +
							"USER_CHAR_NO	= :USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();

			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				try {
					using (cmd = CreateSelectCommand(sSql,conn)) {
						cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
						cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
						cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
						using (da = new OracleDataAdapter(cmd)) {
							da.Fill(dsSub,"T_ATTR_VALUE");
						}
					}
				} finally {
					conn.Close();
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	private void AppendBbsPic(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_DATE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_FLAG",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_USED_SPECIAL_FREE_FLAG",Type.GetType("System.String"));

		string sSiteCd = string.Empty;
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			sSiteCd = oSessionMan.site.siteCd;
			sUserSeq = oSessionMan.userMan.userSeq;
			sUserCharNo = oSessionMan.userMan.userCharNo;
		}

		if (string.IsNullOrEmpty(sSiteCd) || string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sUserCharNo)) {
			return;
		}

		string sWhereClause = String.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> sObjSeqList = new List<string>();

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			sObjSeqList.Add(iBridUtil.GetStringValue(dr["PIC_SEQ"]));

			dr["OBJ_DOWNLOAD_FLAG"] = "0";
			dr["OBJ_DOWNLOAD_DATE"] = "";
			dr["SELF_USED_SPECIAL_FREE_FLAG"] = "0";
		}

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		SysPrograms.SqlAppendWhere("OBJ_TYPE = :OBJ_TYPE",ref sWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_TYPE",ViCommConst.ATTACH_PIC_INDEX.ToString()));

		SysPrograms.SqlAppendWhere(string.Format("OBJ_SEQ IN ({0})",string.Join(",",sObjSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	OBJ_SEQ,");
		oSqlBuilder.AppendLine("	USED_DATE,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG2");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_OBJ_USED_HISTORY");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["PIC_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["OBJ_SEQ"]);

			pDR["OBJ_DOWNLOAD_FLAG"] = "1";
			pDR["OBJ_DOWNLOAD_DATE"] = subDR["USED_DATE"].ToString();

			if (subDR["SPECIAL_FREE_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR) || subDR["SPECIAL_FREE_FLAG2"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				pDR["SELF_USED_SPECIAL_FREE_FLAG"] = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	private void AppendProfilePic(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_DATE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_FLAG",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_USED_SPECIAL_FREE_FLAG",Type.GetType("System.String"));

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["OBJ_DOWNLOAD_FLAG"] = "0";
			dr["OBJ_DOWNLOAD_DATE"] = "";
			dr["SELF_USED_SPECIAL_FREE_FLAG"] = "0";
		}
	}

	public int GetNewlyArrived(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType,int pHour) {
		return this.GetNewlyArrived(pSiteCd,pUserSeq,pUserCharNo,pPicType,pHour,string.Empty);
	}
	public int GetNewlyArrived(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType,int pHour,string pAttr) {
		DataSet ds;
		int iFlag = ViCommConst.FLAG_OFF;
		try {
			conn = DbConnect("CastPic.GetNewlyArrived");
			ds = new DataSet();

			string sSql = "SELECT " +
							" COUNT(*) CNT " +
							"FROM " +
							" T_CAST_PIC " +
							"WHERE " +
							" SITE_CD		=  :SITE_CD			AND " +
							" USER_SEQ		=  :USER_SEQ		AND " +
							" USER_CHAR_NO	=  :USER_CHAR_NO	AND " +
							" PIC_TYPE		=  :PIC_TYPE		AND " +
							" OBJ_NA_FLAG	=  :OBJ_NA_FLAG		AND " +
							" UPLOAD_DATE	>= :UPLOAD_DATE		";
			if (!string.IsNullOrEmpty(pAttr)) {
				sSql += "AND  CAST_PIC_ATTR_SEQ	=  :CAST_PIC_ATTR_SEQ	";
			}

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PIC_TYPE",pPicType);
				cmd.Parameters.Add("OBJ_NA_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("UPLOAD_DATE",OracleDbType.Date,DateTime.Now.AddHours(pHour * -1),ParameterDirection.Input);
				if (!string.IsNullOrEmpty(pAttr)) {
					cmd.Parameters.Add("CAST_PIC_ATTR_SEQ",pAttr);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_PIC");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow drSub = ds.Tables[0].Rows[0];
						iFlag = int.Parse(drSub["CNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return iFlag;
	}

	public void GetChargeInfoByPicSeq(string pSiteCd,string pPicSeq,
		out int pUseIndividualChargeFlag,
		out int pViewerChargePoint,
		out int pUseIndividualPayFlag,
		out int pViewePayAmt
	) {
		DataSet ds;
		DataRow dr;

		pUseIndividualChargeFlag = 0;
		pViewerChargePoint = 0;
		pUseIndividualPayFlag = 0;
		pViewePayAmt = 0;

		string sValue = string.Empty;
		try {
			conn = DbConnect("CastPic.GetChargeInfoByPicSeq");
			string sSql = "SELECT USE_INDIVIDUAL_PAY_FLAG,VIEW_PAY_AMT,USE_INDIVIDUAL_CHARGE_FLAG,VIEWER_CHARGE_POINT FROM VW_CAST_PIC01 " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND PIC_SEQ = :PIC_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PIC_SEQ",pPicSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC01");

					if (ds.Tables["VW_CAST_PIC01"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_PIC01"].Rows[0];
						pUseIndividualChargeFlag = int.Parse(dr["USE_INDIVIDUAL_CHARGE_FLAG"].ToString());
						pViewerChargePoint = int.Parse(dr["VIEWER_CHARGE_POINT"].ToString());
						pUseIndividualPayFlag = int.Parse(dr["USE_INDIVIDUAL_PAY_FLAG"].ToString());
						pViewePayAmt = int.Parse(dr["VIEW_PAY_AMT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
	}

	public string GetValueByPicSeq(string pSiteCd,string pPicSeq,string pValue) {
		DataSet ds;
		DataRow dr;

		string sValue = string.Empty;
		try {
			conn = DbConnect("CastPic.GetValueByPicSeq");

			string sSql = "SELECT PIC_TITLE,PIC_DOC,UPLOAD_DATE,OBJ_PHOTO_IMG_PATH FROM VW_CAST_PIC01 " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND PIC_SEQ = :PIC_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PIC_SEQ",pPicSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_PIC");

					if (ds.Tables["T_CAST_PIC"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_PIC"].Rows[0];
						sValue = dr[pValue].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sValue;
	}

	private void CreateAttrQuery(SeekCondition pCondition,ref string pWhere,ref ArrayList pList) {
		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				pWhere = pWhere + string.Format(" AND AT{0}.SITE_CD				= P.SITE_CD				",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_SEQ			= P.USER_SEQ			",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO		",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0}",i + 1);
				pList.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						pWhere += " AND (";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									pWhere += " OR ";
								}
								pWhere = pWhere + string.Format(" (AT{0}.CAST_ATTR_INPUT_VALUE	LIKE '%' || :CAST_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						pWhere += ")";
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');
					if (sValues.Length > 1) {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	IN (		",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0)
								pWhere = pWhere + " , ";
							pWhere = pWhere + string.Format(" :CAST_ATTR_SEQ{0}_{1}		",i + 1,j);

							pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						pWhere = pWhere + " ) ";
					} else {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	= :CAST_ATTR_SEQ{0}		",i + 1);
						pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					pWhere = pWhere + string.Format(" AND AT{0}.GROUPING_CD		= :GROUPING_CD{0}	",i + 1);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}
	}



	public DataSet GetPicAttrTypeCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pPicType,bool pRecentNotLogin,bool pOnlyUsed,SeekCondition pCondition) {
		DataSet ds,dsSorted;
		try {
			string sSuffix = "VW";
			SessionObjs oObj = this.sessionObj;

			if (oObj.sexCd.Equals(ViCommConst.MAN)) {
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastPic"]).Equals("1")) {
					sSuffix = "MV";
				}
			}
			conn = DbConnect("CastPic.GetPicAttrTypeCount");

			StringBuilder oString = new StringBuilder();
			System.Collections.Generic.List<OracleParameter> list = new System.Collections.Generic.List<OracleParameter>();


			oString.Append("SELECT																	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ	AS PRIORITY			,	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ	AS OBJ_ATTR_TYPE_SEQ,	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_NM	AS OBJ_ATTR_TYPE_NM	,	").AppendLine();
			oString.Append("	NVL(COUNT_TABLE.CNT,0)	CNT											").AppendLine();
			oString.Append("FROM																	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE,												").AppendLine();
			oString.Append("	(SELECT																").AppendLine();
			oString.Append("		CAST_PIC_ATTR_TYPE_SEQ	,										").AppendLine();
			oString.Append("		COUNT(*) CNT													").AppendLine();
			oString.Append("	FROM																").AppendLine();
			oString.Append(string.Format("{0}_CAST_PIC00 P",sSuffix)).AppendLine();
			if (pCondition != null && pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						oString.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
					}
				}
			}
			// いいねした画像のみ検索
			if (pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				oString.AppendLine("	, T_CAST_PIC_LIKE PL");
			}
			oString.Append("	WHERE												").AppendLine();
			oString.Append("		P.SITE_CD					=:SITE_CD	AND		").AppendLine();
			oString.Append("		P.NA_FLAG					=:NA_FLAG			").AppendLine();
			if (!pUserSeq.Equals(string.Empty)) {
				oString.Append("AND P.USER_SEQ	=:USER_SEQ							").AppendLine();
				oString.Append("AND P.USER_CHAR_NO=:USER_CHAR_NO					").AppendLine();
			}
			if (!pActCategorySeq.Equals(string.Empty)) {
				oString.Append("AND P.ACT_CATEGORY_SEQ	=:ACT_CATEGORY_SEQ			").AppendLine();
			}
			if (pRecentNotLogin) {
				oString.Append("AND	P.LAST_LOGIN_DATE	<:LAST_LOGIN_DATE			").AppendLine();
			}
			oString.Append("		AND	P.PIC_TYPE		=:PIC_TYPE					").AppendLine();
			oString.Append("		AND	P.OBJ_NA_FLAG	=:OBJ_NA_FLAG				").AppendLine();
			if (pOnlyUsed) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.PIC_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));

			} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.PIC_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));
			}

			if (!string.IsNullOrEmpty(pCondition.rankType)) {
				oString.AppendLine(" AND P.OBJ_RANKING_FLAG = 1");
			}

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						StringBuilder oSubQueryBuilder = new StringBuilder();
						oSubQueryBuilder.AppendLine(" AND EXISTS(										");
						oSubQueryBuilder.AppendLine("	SELECT											");
						oSubQueryBuilder.AppendLine("		1											");
						oSubQueryBuilder.AppendLine("	FROM											");
						oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
						oSubQueryBuilder.AppendLine("	WHERE											");
						oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
						oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.PIC_SEQ			AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
						oSubQueryBuilder.AppendLine(" )													");
						oString.AppendLine(oSubQueryBuilder.ToString());

						list.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
						list.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
					}
				}
			}

			if (pCondition.hasExtend) {
				oString.AppendLine(SetExtednCondition(pCondition,ref list));
			}

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
					string sUserSeq = string.Empty;
					string sUserCharNo = string.Empty;
					if (this.sessionObj != null) {
						if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
							SessionMan oSessionMan = (SessionMan)this.sessionObj;
							sUserSeq = oSessionMan.userMan.userSeq;
							sUserCharNo = oSessionMan.userMan.userCharNo;
						}
					}
					if (!sUserSeq.Equals(string.Empty)) {
						oString.AppendLine(" AND NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)");
						list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
						list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
					}
				}
			}
			// いいねした画像のみ検索
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)
				&& pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)
			) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;

				oString.AppendLine(" AND P.SITE_CD = PL.SITE_CD");
				oString.AppendLine(" AND P.PIC_SEQ = PL.PIC_SEQ");
				oString.AppendLine(" AND :MAN_USER_SEQ = PL.MAN_USER_SEQ");
				list.Add(new OracleParameter(":MAN_USER_SEQ",oSessionMan.userMan.userSeq));
			}
			// やきもち防止設定
			if (pCondition.containJealousyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
					SessionMan oSessionMan = (SessionMan)this.sessionObj;
					FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope,pCondition,pPicType);
					ArrayList pWhereList = new ArrayList();
					string sJealousyWhere = SetFlexibleQuery(oFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref pWhereList);
					list.AddRange((OracleParameter[])pWhereList.ToArray(typeof(OracleParameter)));
					oString.AppendLine(sJealousyWhere);
				}
			}

			oString.Append("	GROUP BY															").AppendLine();
			oString.Append("		P.CAST_PIC_ATTR_TYPE_SEQ)	COUNT_TABLE							").AppendLine();
			oString.Append("WHERE																	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE.SITE_CD				= :SITE_CD								AND	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE.CAST_PIC_ATTR_TYPE_SEQ	= COUNT_TABLE.CAST_PIC_ATTR_TYPE_SEQ (+)	").AppendLine();

			using (cmd = CreateSelectCommand(oString.ToString(),conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);

				if (!pUserSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}
				if (!pActCategorySeq.Equals(string.Empty)) {
					cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);
				}
				if (pRecentNotLogin) {
					cmd.Parameters.Add("LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Now.AddDays(ViCommConst.FREE_BBS_VIEW_NOT_LOGIN_DAYS * -1),ParameterDirection.Input);
				}
				cmd.Parameters.Add("PIC_TYPE",pPicType);
				cmd.Parameters.Add("OBJ_NA_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.AddRange(list.ToArray());

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC00");
				}
				dsSorted = ds.Clone();
				DataRow[] dr = ds.Tables[0].Select(string.Empty,"PRIORITY");

				for (int i = 0;i < dr.Length;++i) {
					dsSorted.Tables[0].ImportRow(dr[i]);
				}

			}
		} finally {
			conn.Close();
		}
		return dsSorted;
	}


	public DataSet GetPicAttrCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,string pPicType,bool pRecentNotLogin,bool pOnlyUsed,SeekCondition pCondition) {
		DataSet ds,dsSorted;
		try {
			string sSuffix = "VW";
			SessionObjs oObj = this.sessionObj;

			if (oObj.sexCd.Equals(ViCommConst.MAN)) {
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastPic"]).Equals("1")) {
					sSuffix = "MV";
				}
			}

			conn = DbConnect("CastPic.GetPicAttrCount");

			StringBuilder oString = new StringBuilder();
			System.Collections.Generic.List<OracleParameter> list = new System.Collections.Generic.List<OracleParameter>();

			oString.Append("SELECT																		").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.PRIORITY					AS PRIORITY			,").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ	AS OBJ_ATTR_TYPE_SEQ,").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ		AS OBJ_ATTR_SEQ		,").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_NM			AS OBJ_ATTR_NM		,").AppendLine();
			oString.Append("	NVL(COUNT_TABLE.CNT,0)	CNT												").AppendLine();
			oString.Append("FROM																		").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE	,											").AppendLine();
			oString.Append("	(SELECT																	").AppendLine();
			oString.Append("		CAST_PIC_ATTR_TYPE_SEQ	,											").AppendLine();
			oString.Append("		CAST_PIC_ATTR_SEQ		,											").AppendLine();
			oString.Append("		COUNT(*) CNT														").AppendLine();
			oString.Append("	FROM																	").AppendLine();
			oString.Append(string.Format("{0}_CAST_PIC00 P",sSuffix)).AppendLine();
			if (pCondition != null && pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						oString.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
					}
				}
			}
			// いいねした画像のみ検索
			if (pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				oString.AppendLine("	, T_CAST_PIC_LIKE PL");
			}
			oString.Append("	WHERE												").AppendLine();
			oString.Append("		P.SITE_CD					=:SITE_CD	AND		").AppendLine();
			oString.Append("		P.NA_FLAG					=:NA_FLAG			").AppendLine();
			if (!pUserSeq.Equals(string.Empty)) {
				oString.Append("AND P.USER_SEQ	=:USER_SEQ							").AppendLine();
				oString.Append("AND P.USER_CHAR_NO=:USER_CHAR_NO					").AppendLine();
			}
			if (!pActCategorySeq.Equals(string.Empty)) {
				oString.Append("AND P.ACT_CATEGORY_SEQ	=:ACT_CATEGORY_SEQ			").AppendLine();
			}
			if (pRecentNotLogin) {
				oString.Append("AND	P.LAST_LOGIN_DATE	<:LAST_LOGIN_DATE			").AppendLine();
			}
			oString.Append("		AND	P.PIC_TYPE		=:PIC_TYPE					").AppendLine();
			oString.Append("		AND	P.OBJ_NA_FLAG	=:OBJ_NA_FLAG				").AppendLine();
			if (pOnlyUsed) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.PIC_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));

			} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.PIC_SEQ	");
				oSubQueryBuilder.AppendLine(" ) ");
				oString.AppendLine(oSubQueryBuilder.ToString());

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));

			}

			if (!string.IsNullOrEmpty(pCondition.rankType)) {
				oString.AppendLine(" AND P.OBJ_RANKING_FLAG = 1");
			}

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						StringBuilder oSubQueryBuilder = new StringBuilder();
						oSubQueryBuilder.AppendLine(" AND EXISTS(										");
						oSubQueryBuilder.AppendLine("	SELECT											");
						oSubQueryBuilder.AppendLine("		1											");
						oSubQueryBuilder.AppendLine("	FROM											");
						oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
						oSubQueryBuilder.AppendLine("	WHERE											");
						oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
						oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.PIC_SEQ			AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
						oSubQueryBuilder.AppendLine(" )													");
						oString.AppendLine(oSubQueryBuilder.ToString());

						list.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
						list.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
					}
				}
			}

			if (pCondition.hasExtend) {
				oString.AppendLine(SetExtednCondition(pCondition,ref list));
			}

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
					string sUserSeq = string.Empty;
					string sUserCharNo = string.Empty;
					if (this.sessionObj != null) {
						if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
							SessionMan oSessionMan = (SessionMan)this.sessionObj;
							sUserSeq = oSessionMan.userMan.userSeq;
							sUserCharNo = oSessionMan.userMan.userCharNo;
						}
					}
					if (!sUserSeq.Equals(string.Empty)) {
						oString.AppendLine(" AND NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)");
						list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
						list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
					}
				}
			}
			// いいねした画像のみ検索
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)
				&& pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)
			) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;

				oString.AppendLine(" AND P.SITE_CD = PL.SITE_CD");
				oString.AppendLine(" AND P.PIC_SEQ = PL.PIC_SEQ");
				oString.AppendLine(" AND :MAN_USER_SEQ = PL.MAN_USER_SEQ");
				list.Add(new OracleParameter(":MAN_USER_SEQ",oSessionMan.userMan.userSeq));
			}
			// やきもち防止設定
			if (pCondition.containJealousyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
					SessionMan oSessionMan = (SessionMan)this.sessionObj;
					FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope,pCondition,pPicType);
					ArrayList pWhereList = new ArrayList();
					string sJealousyWhere = SetFlexibleQuery(oFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref pWhereList);
					list.AddRange((OracleParameter[])pWhereList.ToArray(typeof(OracleParameter)));
					oString.AppendLine(sJealousyWhere);
				}
			}

			oString.Append("	GROUP BY																").AppendLine();
			oString.Append("		P.CAST_PIC_ATTR_TYPE_SEQ,P.CAST_PIC_ATTR_SEQ) COUNT_TABLE			").AppendLine();
			oString.Append("WHERE																		").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.SITE_CD					= :SITE_CD									AND	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_TYPE_SEQ	= COUNT_TABLE.CAST_PIC_ATTR_TYPE_SEQ	(+)	AND	").AppendLine();
			oString.Append("	T_CAST_PIC_ATTR_TYPE_VALUE.CAST_PIC_ATTR_SEQ		= COUNT_TABLE.CAST_PIC_ATTR_SEQ			(+)		").AppendLine();

			using (cmd = CreateSelectCommand(oString.ToString(),conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);
				if (!pUserSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}
				if (!pActCategorySeq.Equals(string.Empty)) {
					cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);
				}
				if (pRecentNotLogin) {
					cmd.Parameters.Add("LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Now.AddDays(ViCommConst.FREE_BBS_VIEW_NOT_LOGIN_DAYS * -1),ParameterDirection.Input);
				}
				cmd.Parameters.Add("PIC_TYPE",pPicType);
				cmd.Parameters.Add("OBJ_NA_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.AddRange(list.ToArray());

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC00");
				}
				dsSorted = ds.Clone();
				DataRow[] dr = ds.Tables[0].Select(string.Empty,"PRIORITY");

				for (int i = 0;i < dr.Length;++i) {
					dsSorted.Tables[0].ImportRow(dr[i]);
				}
			}
		} finally {
			conn.Close();
		}
		return dsSorted;
	}

	private FlexibleQueryTargetInfo CreateFlexibleQueryTarget(FlexibleQueryType pFlexibleQueryType,SeekCondition pSeekCondition,string pPicType) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(pFlexibleQueryType);
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			if (pPicType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
				oFlexibleQueryTarget.JealousyBbs = true;
			} else if (pPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
				oFlexibleQueryTarget.JealousyProfilePic = true;
			}
			switch (pSeekCondition.onlineStatus) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					oFlexibleQueryTarget.JealousyWaitingStatus = true;
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
				case ViCommConst.SeekOnlineStatus.LOGINED:
					oFlexibleQueryTarget.JealousyLoginStatus = true;
					break;
			}
		}
		return oFlexibleQueryTarget;
	}

	public int GetPicCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT													");
		oSqlBuilder.AppendLine("	COUNT(T_CAST_PIC.PIC_SEQ)							");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	T_CAST_PIC											");
		oSqlBuilder.AppendLine(" WHERE													");
		oSqlBuilder.AppendLine("	T_CAST_PIC.SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_CAST_PIC.USER_SEQ			= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_CAST_PIC.USER_CHAR_NO		= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_CAST_PIC.PIC_TYPE			= :PIC_TYPE			AND	");
		oSqlBuilder.AppendLine("	T_CAST_PIC.OBJ_NA_FLAG		= :OBJ_NA_FLAG		AND	");
		oSqlBuilder.AppendLine("	T_CAST_PIC.PLANNING_TYPE	= 0						");

		System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PIC_TYPE",pPicType));
		oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",ViCommConst.FLAG_OFF));

		return ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	public int GetPicCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType,string pAttr,string pObjNaFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT												");
		oSqlBuilder.AppendLine("	COUNT(T_CAST_PIC.PIC_SEQ)						");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	T_CAST_PIC										");
		oSqlBuilder.AppendLine(" WHERE												");
		oSqlBuilder.AppendLine("	T_CAST_PIC.SITE_CD		= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_CAST_PIC.USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_CAST_PIC.USER_CHAR_NO	= :USER_CHAR_NO		");

		System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		if (!string.IsNullOrEmpty(pPicType)) {
			oSqlBuilder.AppendLine("	AND	T_CAST_PIC.PIC_TYPE	= :PIC_TYPE");
			oParamList.Add(new OracleParameter(":PIC_TYPE",pPicType));
		}
		if (!string.IsNullOrEmpty(pAttr)) {
			oSqlBuilder.AppendLine("	AND	T_CAST_PIC.CAST_PIC_ATTR_SEQ	= :CAST_PIC_ATTR_SEQ");
			oParamList.Add(new OracleParameter(":CAST_PIC_ATTR_SEQ",pAttr));
		}
		if (!string.IsNullOrEmpty(pObjNaFlag)) {
			oSqlBuilder.AppendLine("	AND	T_CAST_PIC.OBJ_NA_FLAG	= :OBJ_NA_FLAG");
			oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",pObjNaFlag));
		}

		return ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	private string SetExtednCondition(SeekCondition pCondition,ref System.Collections.Generic.List<OracleParameter> pParamList) {
		StringBuilder oSubQueryBuilder = new StringBuilder();
		// 拡張検索(一覧下部の検索フォームからの検索)の条件追加
		if (pCondition.hasExtend) {
			if (pCondition.onlineStatus == ViCommConst.SeekOnlineStatus.WAITING) {
				oSubQueryBuilder.AppendLine(" AND P.CHARACTER_ONLINE_STATUS = :EXT_ONLINE_STATUS_WAITING ");
				pParamList.Add(new OracleParameter("EXT_ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
			}
			if (!string.IsNullOrEmpty(pCondition.handleNm)) {
				oSubQueryBuilder.AppendLine(" AND P.HANDLE_NM LIKE '%' || :EXT_HANDLE_NM || '%' ");
				pParamList.Add(new OracleParameter("EXT_HANDLE_NM",pCondition.handleNm));
			}
			if (!string.IsNullOrEmpty(pCondition.ageFrom)) {
				oSubQueryBuilder.AppendLine(" AND P.AGE >= :EXT_AGE_FROM ");
				pParamList.Add(new OracleParameter("EXT_AGE_FROM",pCondition.ageFrom));
			}
			if (!string.IsNullOrEmpty(pCondition.ageTo)) {
				oSubQueryBuilder.AppendLine(" AND P.AGE <= :EXT_AGE_TO ");
				pParamList.Add(new OracleParameter("EXT_AGE_TO",pCondition.ageTo));
			}

			if (pCondition.newCastDay != 0) {
				oSubQueryBuilder.AppendLine(" AND SYSDATE <= TO_DATE(P.START_PERFORM_DAY, 'YYYY/MM/DD') + :NEW_CAST_DAY ");
				pParamList.Add(new OracleParameter("NEW_CAST_DAY",pCondition.newCastDay));
			}

			if (pCondition.attrValue.Count > 0) {
				string sWhere = string.Empty;
				this.CreateAttrQuery(pCondition,ref sWhere,ref pParamList);
				oSubQueryBuilder.Append(sWhere);
			}
		}
		return oSubQueryBuilder.ToString();
	}
	private void CreateAttrQuery(SeekCondition pCondition,ref string pWhere,ref System.Collections.Generic.List<OracleParameter> pList) {
		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				pWhere = pWhere + string.Format(" AND AT{0}.SITE_CD				= P.SITE_CD				",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_SEQ			= P.USER_SEQ			",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO		",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0}",i + 1);
				pList.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						pWhere += " AND (";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									pWhere += " OR ";
								}
								pWhere = pWhere + string.Format(" (AT{0}.CAST_ATTR_INPUT_VALUE	LIKE '%' || :CAST_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						pWhere += ")";
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');
					if (sValues.Length > 1) {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	IN (		",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0)
								pWhere = pWhere + " , ";
							pWhere = pWhere + string.Format(" :CAST_ATTR_SEQ{0}_{1}		",i + 1,j);

							pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						pWhere = pWhere + " ) ";
					} else {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	= :CAST_ATTR_SEQ{0}		",i + 1);
						pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					pWhere = pWhere + string.Format(" AND AT{0}.GROUPING_CD		= :GROUPING_CD{0}	",i + 1);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}
	}
	public DataSet GetOneByCastPicSeq(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		DataSet oDataSet = null;
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	PROFILE_PIC_FLAG						");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_CAST_PIC								");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	PIC_SEQ			= :PIC_SEQ				");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PIC_SEQ",pPicSeq));
		
		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public DataSet GetPageCollectionNew(CastPicSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhereNew(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	PIC_TYPE,");
		oSqlBuilder.AppendLine("	OBJ_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	OBJ_SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	OBJ_BLUR_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	OBJ_SMALL_BLUR_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	CRYPT_VALUE,");
		oSqlBuilder.AppendLine("	PROFILE_PIC_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_PIC01");
		oSqlBuilder.AppendLine(sWhereClause);
		sSortExpression = "ORDER BY UPLOAD_DATE DESC NULLS LAST";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		if (pCondition.IsAppendObjUsedHistory) {
			AppendObjUsedHistory(oDataSet);
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhereNew(CastPicSeekCondition pCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (pCondition.PicTypeList.Count > 0) {
			SysPrograms.SqlAppendWhere(string.Format("PIC_TYPE IN({0})",string.Join(",",pCondition.PicTypeList.ToArray())),ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.ProfilePicFlag)) {
			SysPrograms.SqlAppendWhere("PROFILE_PIC_FLAG = :PROFILE_PIC_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PROFILE_PIC_FLAG",pCondition.ProfilePicFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjNaFlag)) {
			SysPrograms.SqlAppendWhere("OBJ_NA_FLAG = :OBJ_NA_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",pCondition.ObjNaFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjNotApproveFlag)) {
			SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",pCondition.ObjNotApproveFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjNotPublishFlag)) {
			SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",pCondition.ObjNotPublishFlag));
		}

		return oParamList.ToArray();
	}

	private void AppendObjUsedHistory(DataSet pDataSet) {
		if (pDataSet.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn("OBJ_DOWNLOAD_FLAG",System.Type.GetType("System.String"));
		pDataSet.Tables[0].Columns.Add(col);

		col = new DataColumn("OBJ_DOWNLOAD_DATE",System.Type.GetType("System.String"));
		pDataSet.Tables[0].Columns.Add(col);

		col = new DataColumn("SELF_USED_SPECIAL_FREE_FLAG",System.Type.GetType("System.String"));
		pDataSet.Tables[0].Columns.Add(col);

		string sSiteCd = string.Empty;
		string sObjType = ViCommConst.ATTACH_PIC_INDEX.ToString();
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		List<string> sObjSeqList = new List<string>();

		foreach (DataRow dr in pDataSet.Tables[0].Rows) {
			sSiteCd = iBridUtil.GetStringValue(dr["SITE_CD"]);
			sObjSeqList.Add(iBridUtil.GetStringValue(dr["PIC_SEQ"]));
			dr["OBJ_DOWNLOAD_FLAG"] = ViCommConst.FLAG_OFF_STR;
			dr["OBJ_DOWNLOAD_DATE"] = string.Empty;
			dr["SELF_USED_SPECIAL_FREE_FLAG"] = ViCommConst.FLAG_OFF_STR;
		}

		if (this.sessionObj != null) {
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				sUserSeq = oSessionMan.userMan.userSeq;
				sUserCharNo = oSessionMan.userMan.userCharNo;
			}
		}

		if (string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sUserCharNo)) {
			return;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		SysPrograms.SqlAppendWhere("OBJ_TYPE = :OBJ_TYPE",ref sWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_TYPE",sObjType));

		SysPrograms.SqlAppendWhere(string.Format("OBJ_SEQ IN({0})",string.Join(",",sObjSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	OBJ_SEQ,");
		oSqlBuilder.AppendLine("	USED_DATE,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG2");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_OBJ_USED_HISTORY");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet dsUsed = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDataSet.Tables[0].PrimaryKey = new DataColumn[] { pDataSet.Tables[0].Columns["PIC_SEQ"] };

		foreach (DataRow drUsed in dsUsed.Tables[0].Rows) {
			DataRow drPic = pDataSet.Tables[0].Rows.Find(drUsed["OBJ_SEQ"]);
			drPic["OBJ_DOWNLOAD_FLAG"] = ViCommConst.FLAG_ON_STR;
			drPic["OBJ_DOWNLOAD_DATE"] = iBridUtil.GetStringValue(drUsed["USED_DATE"]);

			if (iBridUtil.GetStringValue(drUsed["SPECIAL_FREE_FLAG"]).Equals(ViCommConst.FLAG_ON_STR) || iBridUtil.GetStringValue(drUsed["SPECIAL_FREE_FLAG2"]).Equals(ViCommConst.FLAG_ON_STR)) {
				drPic["SELF_USED_SPECIAL_FREE_FLAG"] = ViCommConst.FLAG_ON_STR;
			}
		}
	}
	
	public DataSet GetOneByPicSeq(string pSiteCd,string pPicSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	USER_SEQ			,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_CAST_PIC				");
		oSqlBuilder.AppendLine("WHERE						");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD	AND	");
		oSqlBuilder.AppendLine("	PIC_SEQ	= :PIC_SEQ		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":PIC_SEQ",pPicSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}

