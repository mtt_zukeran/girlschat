﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;


[System.Serializable]
public class ManBase:DbSession {
	private SessionObjs _sessionObj = null;
	protected SessionObjs sessionObj {
		get {
			if (_sessionObj == null) {
				_sessionObj = SessionObjs.Current;
			}
			return _sessionObj;
		}
	}
	
	public ManBase() : this(SessionObjs.Current) { }
	public ManBase(SessionObjs pSessionObj) { 
		this._sessionObj = pSessionObj;
	}

	protected void InnnerCondition(ref ArrayList pParmList) {
		SessionObjs oObj = sessionObj;
		string sUserSeq,sUserCharNo;
		if (oObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan sessionMan = (SessionMan)sessionObj;
			sUserSeq = sessionMan.userMan.userSeq;
			sUserCharNo = sessionMan.userMan.userCharNo;
		} else {
			SessionWoman sessionWoman = (SessionWoman)sessionObj;
			sUserSeq = sessionWoman.userWoman.userSeq;
			sUserCharNo = sessionWoman.userWoman.curCharNo;
		}
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ1",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO1",sUserCharNo));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ2",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO2",sUserCharNo));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ3",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO3",sUserCharNo));
	}

	protected void JoinCondition(ref ArrayList pParmList) {
		SessionObjs oObj = sessionObj;
		string sUserSeq,sUserCharNo;
		if (oObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan sessionMan = (SessionMan)sessionObj;
			sUserSeq = sessionMan.userMan.userSeq;
			sUserCharNo = sessionMan.userMan.userCharNo;
		} else {
			SessionWoman sessionWoman = (SessionWoman)sessionObj;
			sUserSeq = sessionWoman.userWoman.userSeq;
			sUserCharNo = sessionWoman.userWoman.curCharNo;
		}
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ4",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO4",sUserCharNo));
		pParmList.Add(new OracleParameter("PARTNER_USER_SEQ5",sUserSeq));
		pParmList.Add(new OracleParameter("PARTNER_USER_CHAR_NO5",sUserCharNo));
		pParmList.Add(new OracleParameter("CODE_TYPE1","73"));
		pParmList.Add(new OracleParameter("CODE_TYPE2","50"));
		pParmList.Add(new OracleParameter("CODE_TYPE3","52"));
		pParmList.Add(new OracleParameter("CODE_TYPE4","15"));
		pParmList.Add(new OracleParameter("CODE_TYPE5","66"));
	}

	protected string ManBasicField() {
		StringBuilder sField = new StringBuilder();
		sField.Append("P.SITE_CD					,").AppendLine();
		sField.Append("P.USER_SEQ					,").AppendLine();
		sField.Append("P.USER_CHAR_NO				,").AppendLine();
		sField.Append("P.LOGIN_ID					,").AppendLine();
		sField.Append("P.LOGIN_PASSWORD				,").AppendLine();
		sField.Append("P.BAL_POINT					,").AppendLine();
		sField.Append("P.CHARACTER_ONLINE_STATUS	,").AppendLine();
		sField.Append("P.USER_STATUS				,").AppendLine();
		sField.Append("P.BIRTHDAY					,").AppendLine();
		sField.Append("P.LAST_LOGIN_DATE			,").AppendLine();
//		sField.Append("P.TEL						,").AppendLine();
		sField.Append("P.HANDLE_NM					,").AppendLine();
		sField.Append("P.AGE						,").AppendLine();
		sField.Append("P.CAST_MAIL_RX_TYPE			,").AppendLine();
		sField.Append("P.INFO_MAIL_RX_TYPE			,").AppendLine();
		sField.Append("P.TOTAL_RX_MAIL_COUNT		,").AppendLine();
		sField.Append("P.TOTAL_TX_MAIL_COUNT		,").AppendLine();
		sField.Append("P.TOTAL_TALK_COUNT			,").AppendLine();
		sField.Append("P.MOBILE_CARRIER_CD			,").AppendLine();
		sField.Append("P.TERMINAL_UNIQUE_ID			,").AppendLine();
		sField.Append("P.MOBILE_TERMINAL_NM			,").AppendLine();
		sField.Append("P.TOTAL_RECEIPT_AMT			,").AppendLine();
		sField.Append("P.REC_SEQ					,").AppendLine();
		sField.Append("P.PIC_SEQ					,").AppendLine();
//		sField.Append("P.OBJ_NOT_APPROVE_FLAG		,").AppendLine();
		sField.Append("P.IVP_ACCEPT_SEQ				,").AppendLine();
		sField.Append("P.USER_RANK					,").AppendLine();
		sField.Append("P.REGIST_DATE				").AppendLine();
		return sField.ToString();
	}

	protected virtual DataSet ExecuteTemplateQuery(string pInnerSql, List<OracleParameter> pParamList, string pJoinTables, string pJoinConditions, string pOuterFields)
	{
		// =================================================
		//  TEMPLATE 適用
		// =================================================
		ArrayList oTemplateParamList = new ArrayList();
		JoinCondition(ref oTemplateParamList);
		InnnerCondition(ref oTemplateParamList);
		pParamList.AddRange((OracleParameter[])oTemplateParamList.ToArray(typeof(OracleParameter)));

		string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "MAN";
		string sSql = sessionObj.sqlSyntax[sTemplate].ToString();
		sSql = string.Format(sSql, pInnerSql, pJoinTables, pJoinConditions, pOuterFields);

		return ExecuteSelectQueryBase(sSql, pParamList.ToArray());
	}

	protected void AppendManAttr(DataSet pDS) {
		string sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_USER_MAN_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD	AND " +
						"USER_SEQ		= :USER_SEQ AND " +
						"USER_CHAR_NO	= :USER_CHAR_NO ";

		string sSql2 = "SELECT WAITING_COMMENT FROM T_RECORDING WHERE REC_SEQ = :REC_SEQ ";

		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("MAN_ATTR_VALUE{0:D2}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		bool bInit = false;
		if (pDS.Tables[0].Columns.Contains("UNIQUE_VALUE") == false) {
			col = new DataColumn("UNIQUE_VALUE",System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
			bInit = true;
		}

		if (pDS.Tables[0].Columns.Contains("WAITING_COMMENT") == false) {
			col = new DataColumn("WAITING_COMMENT",System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		string sOnlineStatus;
		foreach (DataRow dr in pDS.Tables[0].Rows) {
			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("MAN_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"VW_USER_MAN_ATTR_VALUE01");
					}
				}
				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("MAN_ATTR_VALUE{0}",drSub["ITEM_NO"].ToString())] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
			if (iBridUtil.GetStringValue(dr["WAITING_COMMENT"]).Equals(string.Empty)) {
				sOnlineStatus = iBridUtil.GetStringValue(dr["CHARACTER_ONLINE_STATUS"]);
				if (sOnlineStatus.Equals(ViCommConst.USER_WAITING.ToString()) || sOnlineStatus.Equals(ViCommConst.USER_TALKING.ToString())) {
					using (DataSet dsSub = new DataSet()) {
						using (cmd = CreateSelectCommand(sSql2,conn)) {
							cmd.Parameters.Add("REC_SEQ",iBridUtil.GetStringValue(dr["REC_SEQ"]));
							using (da = new OracleDataAdapter(cmd)) {
								da.Fill(dsSub,"T_RECORDING");
							}
						}

						if (dsSub.Tables[0].Rows.Count != 0) {
							dr["WAITING_COMMENT"] = iBridUtil.GetStringValue(dsSub.Tables[0].Rows[0]["WAITING_COMMENT"]);
						}
					}
				}
			}

			if (bInit) {
				dr["UNIQUE_VALUE"] = "";
			}

		}
	}

	protected void CreateAttrQuery(SeekCondition pCondition,ref string pWhere,ref ArrayList pList) {
		string sQuery = string.Empty;
		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.SITE_CD			= P.SITE_CD					",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_SEQ			= P.USER_SEQ				",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO			",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ{0}	",i + 1),ref pWhere);
				pList.Add(new OracleParameter(string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						sQuery = " ( ";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									sQuery += " OR ";
								}
								sQuery += string.Format(" (AT{0}.MAN_ATTR_INPUT_VALUE	LIKE '%' || :MAN_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("MAN_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						sQuery += ")";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');

					if (sValues.Length > 1) {
						sQuery = string.Format(" AT{0}.MAN_ATTR_SEQ IN ( ",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0) {
								sQuery += " , ";
							}
							sQuery += string.Format(" :MAN_ATTR_SEQ{0}_{1}	",i + 1,j);
							pList.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						sQuery += " ) ";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					} else {
						if (pCondition.notEqualFlag.Count > i && pCondition.notEqualFlag[i].Equals("1")) {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_SEQ <> :MAN_ATTR_SEQ{0} ",i + 1),ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_SEQ = :MAN_ATTR_SEQ{0} ",i + 1),ref pWhere);
						}
						pList.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					SysPrograms.SqlAppendWhere(string.Format(" AT{0}.GROUPING_CD	= :GROUPING_CD{0}	",i + 1),ref pWhere);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}
	}
}
