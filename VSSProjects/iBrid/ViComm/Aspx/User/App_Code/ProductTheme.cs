﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductTheme : ProductBase {
	public ProductTheme() : base() { }
	public ProductTheme(SessionObjs pSessionObj) : base(pSessionObj) { }
	
	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(ProductSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT	");
		oSqlBuilder.AppendLine(base.CreateCountFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oWhereParams);

				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			this.conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionBySeq(string pProductSeq) {
		ProductSeekCondition oCondtion = new ProductSeekCondition();
		oCondtion.ProductSeq = pProductSeq;
		return this.GetPageCollectionBase(oCondtion, 0, 0);
	}
	public DataSet GetPageCollection(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion, pPageNo, pRecPerPage);
	}
	private DataSet GetPageCollectionBase(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {

		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.Append(base.CreateFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where		
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresionBase(pCondtion);

		return ExecutePageCollection(oSqlBuilder.ToString(), pCondtion, oWhereParams, sSortExpression, iStartIndex, pRecPerPage);
	}
	
	private string CreateFrom(ProductSeekCondition pCondition){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine(base.CreateFromBase(pCondition));
		return oSqlBuilder.ToString();
	}

	private OracleParameter[] CreateWhere(ProductSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 商品共通のWHERE条件を作成
		oParamList.AddRange(base.CreateWhereBase(pCondition, ref pWhereClause));
		
		//// 公開条件：コンテンツ写真が登録されていること
		//StringBuilder oSubSqlBuilder = new StringBuilder();
		//oSubSqlBuilder.AppendLine(" EXISTS(								");
		//oSubSqlBuilder.AppendLine("		SELECT 1 FROM T_PRODUCT_THEME	");
		//oSubSqlBuilder.AppendLine("		WHERE							");
		//oSubSqlBuilder.AppendLine("			T_PRODUCT_THEME.SITE_CD				= VW_PRODUCT00.SITE_CD			AND ");
		//oSubSqlBuilder.AppendLine("			T_PRODUCT_THEME.PRODUCT_AGENT_CD	= VW_PRODUCT00.PRODUCT_AGENT_CD	AND ");
		//oSubSqlBuilder.AppendLine("			T_PRODUCT_THEME.PRODUCT_SEQ			= VW_PRODUCT00.PRODUCT_SEQ			");
		//oSubSqlBuilder.AppendLine(" )");
		//SysPrograms.SqlAppendWhere(oSubSqlBuilder.ToString(), ref pWhereClause);

		return oParamList.ToArray();
	}
	#endregion ========================================================================================================

	protected override string GetProductType(ProductSeekCondition pCondition) {
		return ViCommConst.ProductType.THEME;
	}
}
