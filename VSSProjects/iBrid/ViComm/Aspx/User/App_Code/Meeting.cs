﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 待ち合せ
--	Progaram ID		: Meeting
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;

[System.Serializable]
public class Meeting:DbSession {

	public string siteCd;
	public string userSeq;
	public string userCharNo;
	public string requestId;
	public string menuId;
	public string chargeType;

	public Meeting() {
		siteCd = "";
		userSeq = "";
		userCharNo = "";
		requestId = "";
		menuId = "";
		chargeType = "";
	}

	public bool GetOne(string pMeetingKey) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("Meeting.GetOne");

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	SITE_CD				,").AppendLine();
			sSql.Append("	USER_SEQ			,").AppendLine();
			sSql.Append("	USER_CHAR_NO		,").AppendLine();
			sSql.Append("	PARTNER_USER_SEQ	,").AppendLine();
			sSql.Append("	PARTNER_USER_CHAR_NO,").AppendLine();
			sSql.Append("	MENU_ID				,").AppendLine();
			sSql.Append("	REQUEST_ID			,").AppendLine();
			sSql.Append("	CHARGE_TYPE			").AppendLine();
			sSql.Append("FROM					").AppendLine();
			sSql.Append("	T_MEETING			").AppendLine();
			sSql.Append("WHERE					").AppendLine();
			sSql.Append("	MEETING_KEY = :MEETING_KEY ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("MEETING_KEY",pMeetingKey);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MEETING");

					if (ds.Tables["T_MEETING"].Rows.Count != 0) {
						dr = ds.Tables["T_MEETING"].Rows[0];
						siteCd = dr["SITE_CD"].ToString();
						userSeq = dr["USER_SEQ"].ToString();
						userCharNo = dr["USER_CHAR_NO"].ToString();
						requestId = dr["REQUEST_ID"].ToString();
						menuId = dr["MENU_ID"].ToString();
						chargeType = dr["CHARGE_TYPE"].ToString();

						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
