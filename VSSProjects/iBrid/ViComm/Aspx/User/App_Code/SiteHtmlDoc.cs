﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サイト構成HTML文章
--	Progaram ID		: SiteHtmlDoc
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class SiteHtmlDoc:DbSession {
	public string htmlDoc1;
	public string htmlDoc2;
	public string htmlDoc3;
	public string htmlDoc4;
	public string htmlDocTitle;
	public string endPubDay;
	public string naFlag;

	public SiteHtmlDoc() {
	}

	public bool GetOne(string pSiteCd,string pHtmlDocType,string pPageAgentType,string pStartPubDay,string[] pSexCdArray,out string pHtmlDoc) {
		DataSet ds;
		DataRow dr;
		pHtmlDoc = "";
		bool bExist = false;
		try {
			conn = DbConnect("SiteHtmlDoc.GetOne");

			string sSql = "SELECT * FROM VW_SITE_HTML_DOC03 WHERE SITE_CD = :SITE_CD AND HTML_DOC_TYPE = :HTML_DOC_TYPE AND USER_AGENT_TYPE = :USER_AGENT_TYPE AND START_PUB_DAY = :START_PUB_DAY ";
	
			if (pSexCdArray.Length > 0) {
				sSql += " AND SEX_CD IN (";
				for(int i=0;i<pSexCdArray.Length;i++){
					if(i>0){
						sSql += ",";
					}
					sSql += ":SEX_CD_" + i.ToString();
				}

				sSql += " )";
			}

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("HTML_DOC_TYPE",pHtmlDocType);
				cmd.Parameters.Add("USER_AGENT_TYPE",pPageAgentType);
				cmd.Parameters.Add("START_PUB_DAY",pStartPubDay);
				if (pSexCdArray.Length > 0) {
					for (int i = 0;i < pSexCdArray.Length;i++) {
						cmd.Parameters.Add( ":SEX_CD_" + i.ToString(), pSexCdArray[i]);
					}
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE_HTML_DOC03");

					if (ds.Tables["VW_SITE_HTML_DOC03"].Rows.Count != 0) {
						dr = ds.Tables["VW_SITE_HTML_DOC03"].Rows[0];
						htmlDocTitle = dr["HTML_DOC_TITLE"].ToString();
						htmlDoc1 = dr["HTML_DOC1"].ToString();
						htmlDoc2 = dr["HTML_DOC2"].ToString();
						htmlDoc3 = dr["HTML_DOC3"].ToString();
						htmlDoc4 = dr["HTML_DOC4"].ToString();

						pHtmlDoc = dr["HTML_DOC1"].ToString() +
								dr["HTML_DOC2"].ToString() +
								dr["HTML_DOC3"].ToString() +
								dr["HTML_DOC4"].ToString() +
								dr["HTML_DOC5"].ToString() +
								dr["HTML_DOC6"].ToString() +
								dr["HTML_DOC7"].ToString() +
								dr["HTML_DOC8"].ToString() +
								dr["HTML_DOC9"].ToString() +
								dr["HTML_DOC10"].ToString() +
								dr["HTML_DOC11"].ToString() +
								dr["HTML_DOC12"].ToString() +
								dr["HTML_DOC13"].ToString() +
								dr["HTML_DOC14"].ToString() +
								dr["HTML_DOC15"].ToString() +
								dr["HTML_DOC16"].ToString() +
								dr["HTML_DOC17"].ToString() +
								dr["HTML_DOC18"].ToString() +
								dr["HTML_DOC19"].ToString() +
								dr["HTML_DOC20"].ToString();

						endPubDay = dr["END_PUB_DAY"].ToString();
						naFlag = dr["NA_FLAG"].ToString();

						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public int GetPageCount(string pSiteCd,string pHtmlDocType,int pRecPerPage) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try {
			conn = DbConnect("SiteHtmlDoc.GetPageCount");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SITE_HTML_DOC03 ";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pHtmlDocType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}

		return iPages;
	}


	public DataSet GetPageCollection(string pSiteCd,string pHtmlDocType,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try {
			conn = DbConnect("SiteHtmlDoc.GetPageCollection");
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,HTML_DOC_TYPE, START_PUB_DAY DESC ";

			string sSql = "SELECT START_PUB_DAY,HTML_DOC1,HTML_DOC2,HTML_DOC3,HTML_DOC4 " +
							"FROM(SELECT VW_SITE_HTML_DOC03.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_SITE_HTML_DOC03 ";

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pHtmlDocType,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE_HTML_DOC03");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pHtmlDocType,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = " WHERE SITE_CD = :SITE_CD AND  HTML_DOC_TYPE = :HTML_DOC_TYPE ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("HTML_DOC_TYPE",pHtmlDocType));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public string GetDocSeq(string pSiteCd,string pHtmlDocType) {
		DataSet ds;
		DataRow dr;

		string sSeq = "";
		try {
			conn = DbConnect();
			string sSql = "SELECT HTML_DOC_SEQ FROM T_SITE_HTML_MANAGE WHERE SITE_CD = :SITE_CD AND HTML_DOC_GROUP =:HTML_DOC_GROUP AND HTML_DOC_TYPE = :HTML_DOC_TYPE";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("HTML_DOC_GROUP",ViComm.ViCommConst.DOC_GRP_NORMAL);
				cmd.Parameters.Add("HTML_DOC_TYPE",pHtmlDocType);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE_HTML_MANAGE");
					if (ds.Tables["T_SITE_HTML_MANAGE"].Rows.Count != 0) {
						dr = ds.Tables["T_SITE_HTML_MANAGE"].Rows[0];
						sSeq = dr["HTML_DOC_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sSeq;
	}

	public bool GetDocHtmlInfo(string pSiteCd,string pHtmlDocType,string pCarrier,bool pDesignDebug,
				out string pPageTitle,out string pPageKeyword,out string pPageDescription,out int pTableIdx,
				ref WebFace pWebFace,out bool pExistWebFace,out string pUserAgentType,out string[] pCssFileNmArray,out string[] pJavascriptFileNmArray,out string pCanonical,out int pProtectImageFlag,out int pEmojiToolFlag) {
		DataSet ds;
		bool bFound = false;
		pPageTitle = string.Empty;
		pPageKeyword = string.Empty;
		pPageDescription = string.Empty;
		pUserAgentType = string.Empty;
		pCssFileNmArray = new string[] { };
		pJavascriptFileNmArray = new string[] { };
		pCanonical = string.Empty;
		pProtectImageFlag = ViCommConst.FLAG_ON;
		pEmojiToolFlag = ViCommConst.FLAG_ON;
		pTableIdx = SysConst.NOTHING;
		pExistWebFace = false;
		string sWebFaceSeq = string.Empty;
		string sSexCd = string.Empty;

		try {
			conn = DbConnect();
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT PAGE_TITLE,PAGE_KEYWORD,PAGE_DESCRIPTION,TABLE_INDEX,WEB_FACE_SEQ,SEX_CD,USER_AGENT_TYPE,CSS_FILE_NM1,CSS_FILE_NM2,CSS_FILE_NM3,CSS_FILE_NM4,CSS_FILE_NM5,JAVASCRIPT_FILE_NM1,JAVASCRIPT_FILE_NM2,JAVASCRIPT_FILE_NM3,JAVASCRIPT_FILE_NM4,JAVASCRIPT_FILE_NM5,CANONICAL,PROTECT_IMAGE_FLAG,EMOJI_TOOL_FLAG FROM T_SITE_HTML_MANAGE").AppendLine();
			sSql.Append("WHERE SITE_CD = :SITE_CD AND HTML_DOC_GROUP =:HTML_DOC_GROUP AND HTML_DOC_TYPE = :HTML_DOC_TYPE").AppendLine();
			sSql.Append("ORDER BY SITE_CD,HTML_DOC_GROUP,HTML_DOC_TYPE,USER_AGENT_TYPE DESC").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("HTML_DOC_GROUP",ViComm.ViCommConst.DOC_GRP_NORMAL);
				cmd.Parameters.Add("HTML_DOC_TYPE",pHtmlDocType);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE_HTML_MANAGE");
					string sUserAgentType;

					foreach (DataRow dr in ds.Tables[0].Rows) {
						sUserAgentType = dr["USER_AGENT_TYPE"].ToString();
						bool bEnableSmartPhoneDesign = pDesignDebug;
						if (!bEnableSmartPhoneDesign) {
							if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesign"]).Equals("1")) {
								if(!iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesignSiteCd"]).Equals(string.Empty)) {
									string[] oEnableSmartPhoneDesignSiteCd = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesignSiteCd"]).Split(',');
									foreach (string sTempSiteCd in oEnableSmartPhoneDesignSiteCd) {
										if (pSiteCd.Equals(sTempSiteCd)) {
											bEnableSmartPhoneDesign = true;
											break;
										}
									}
								} else {
									bEnableSmartPhoneDesign = true;
								}
							}
						}
						if (bEnableSmartPhoneDesign) {
							if (pCarrier.Equals(ViCommConst.ANDROID) || (pCarrier.Equals(ViCommConst.CARRIER_OTHERS) && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableDisplaySPDesignToPCUser"]).Equals(ViCommConst.FLAG_ON_STR))) {
								if (sUserAgentType.Equals(ViCommConst.DEVICE_ANDROID) || sUserAgentType.Equals(ViCommConst.DEVICE_SMART_PHONE) || sUserAgentType.Equals(ViCommConst.DEVICE_3G)) {
									bFound = true;
								}
							} else if (pCarrier.Equals(ViCommConst.IPHONE)) {
								if (sUserAgentType.Equals(ViCommConst.DEVICE_IPHONE) || sUserAgentType.Equals(ViCommConst.DEVICE_SMART_PHONE) || sUserAgentType.Equals(ViCommConst.DEVICE_3G)) {
									bFound = true;
								}
							} else {
								if (sUserAgentType.Equals(ViCommConst.DEVICE_3G)) {
									bFound = true;
								}
							}
						} else {
							if (sUserAgentType.Equals(ViCommConst.DEVICE_3G)) {
								bFound = true;
							}
						}

						if (bFound) {
							pPageTitle = dr["PAGE_TITLE"].ToString();
							pPageKeyword = dr["PAGE_KEYWORD"].ToString();
							pPageDescription = dr["PAGE_DESCRIPTION"].ToString();
							pTableIdx = int.Parse(dr["TABLE_INDEX"].ToString());
							pUserAgentType = sUserAgentType;

							List<string> oCssFileNmList = new List<string>();
							List<string> oJavascriptFileNmList = new List<string>();
							for (int i = 0; i < 5; i++) {
								string sCssFileNm = iBridUtil.GetStringValue(dr[string.Format("CSS_FILE_NM{0}",i + 1)]);
								if (!string.IsNullOrEmpty(sCssFileNm)) {
									oCssFileNmList.Add(sCssFileNm);
								}

								string sJavascriptFileNm = iBridUtil.GetStringValue(dr[string.Format("JAVASCRIPT_FILE_NM{0}",i + 1)]);
								if (!string.IsNullOrEmpty(sJavascriptFileNm)) {
									oJavascriptFileNmList.Add(sJavascriptFileNm);
								}
							}
							pCssFileNmArray = oCssFileNmList.ToArray();
							pJavascriptFileNmArray = oJavascriptFileNmList.ToArray();
							pCanonical = dr["CANONICAL"].ToString();
							pProtectImageFlag = int.Parse(dr["PROTECT_IMAGE_FLAG"].ToString());
							pEmojiToolFlag = int.Parse(dr["EMOJI_TOOL_FLAG"].ToString());

							sWebFaceSeq = dr["WEB_FACE_SEQ"].ToString();
							sSexCd = dr["SEX_CD"].ToString();
							break;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}

		if (sWebFaceSeq.Equals(string.Empty) == false) {
			pExistWebFace = pWebFace.GetOne(sWebFaceSeq,sSexCd);
		}

		return bFound;
	}
}
