﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: システム設定

--	Progaram ID		: Sys
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Sys:DbSession {
	public string twilioAccountSid;
	public string twilioAuthToken;
	public string twilioTelNo;
	public string twilioFreedialTelNo;
	public string twilioWhiteplanTelNo;
	public string twilioSmsTelNo;

	public Sys() {
	}

	public bool GetValue(string pItem,out string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		try {
			conn = DbConnect();
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	SIP_IVP_LOC_CD					,");
			sSql.AppendLine("	SIP_IVP_SITE_CD					,");
			sSql.AppendLine("	SIP_REGIST_URL					,");
			sSql.AppendLine("	IVP_ADMIN_URL					,");
			sSql.AppendLine("	IVP_REQUEST_URL					,");
			sSql.AppendLine("	IVP_STARTUP_CAMERA_URL			,");
			sSql.AppendLine("	LIVE_KEY						,");
			sSql.AppendLine("	SIP_DOMAIN						,");
			sSql.AppendLine("	VCS_REQUEST_URL					,");
			sSql.AppendLine("	IVP_ISSUE_LIVE_KEY_URL			,");
			sSql.AppendLine("	WHITE_PLAN_NA_TIME				,");
			sSql.AppendLine("	DECOMAIL_SERVER_TYPE			,");
			sSql.AppendLine("	CROSMILE_SIPURI_GET_IF_URL		,");
			sSql.AppendLine("	CROSMILE_SIPURI_EXIST_IF_URL	,");
			sSql.AppendLine("	XSM_VOICE_PHONE_LINE_FLAG	");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("	T_SYS ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SYS");
					if (ds.Tables["T_SYS"].Rows.Count != 0) {
						dr = ds.Tables["T_SYS"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsAvailableWhitePlan() {
		string sNATime;
		DateTime dtFromNATime,dtToNATime;

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_WHITE_PLAN) == false) {
				return false;
			}
		}

		if (GetValue("WHITE_PLAN_NA_TIME",out sNATime)) {
			if (sNATime.Equals(string.Empty) == false) {

				sNATime = sNATime + ":00";

				if (sNATime.CompareTo("01:00:00") == 0) {
					dtFromNATime = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd") + " " + sNATime);
					dtToNATime = DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy/MM/dd") + " " + "01:00:00");

				} else if (sNATime.CompareTo("01:00:00") > 0) {
					dtFromNATime = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd") + " " + sNATime);
					dtToNATime = DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy/MM/dd") + " " + "01:00:00");
					if (DateTime.Now.ToString("yyyy/MM/dd").CompareTo(dtToNATime.ToString("yyyy/MM/dd")) == 0) {
						dtFromNATime = dtFromNATime.AddDays(-1);
						dtToNATime = dtToNATime.AddDays(-1);
					}
				} else {
					dtFromNATime = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd") + " " + "01:00:00");
					dtToNATime = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd") + " " + sNATime);
				}

				if (DateTime.Now >= dtFromNATime && DateTime.Now <= dtToNATime) {
					return false;
				} else {
					return true;
				}
			}
		}
		return false;
	}

	public bool GetOne() {
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	TWILIO_ACCOUNT_SID,");
		oSqlBuilder.AppendLine("	TWILIO_AUTH_TOKEN,");
		oSqlBuilder.AppendLine("	TWILIO_TEL_NO,");
		oSqlBuilder.AppendLine("	TWILIO_FREEDIAL_TEL_NO,");
		oSqlBuilder.AppendLine("	TWILIO_WHITEPLAN_TEL_NO,");
		oSqlBuilder.AppendLine("	TWILIO_SMS_TEL_NO");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_SYS");

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			twilioAccountSid = iBridUtil.GetStringValue(dr["TWILIO_ACCOUNT_SID"]);
			twilioAuthToken = iBridUtil.GetStringValue(dr["TWILIO_AUTH_TOKEN"]);
			twilioTelNo = iBridUtil.GetStringValue(dr["TWILIO_TEL_NO"]);
			twilioFreedialTelNo = iBridUtil.GetStringValue(dr["TWILIO_FREEDIAL_TEL_NO"]);
			twilioWhiteplanTelNo = iBridUtil.GetStringValue(dr["TWILIO_WHITEPLAN_TEL_NO"]);
			twilioSmsTelNo = iBridUtil.GetStringValue(dr["TWILIO_SMS_TEL_NO"]);
			bExist = true;
		}

		return bExist;
	}
}
