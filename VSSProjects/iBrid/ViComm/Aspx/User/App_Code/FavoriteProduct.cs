﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using MobileLib;
using iBridCommLib;
using ViComm;


[System.Serializable]
public class FavoriteProduct:DbSession {
	public FavoriteProduct() {
	}
	
	
	public void FavoriteProductMainte(string pSiteCd,string pProductAgentCd,int pProductSeq,int pUserSeq,string pUserCharNo,string pFavoritComment,int pDeleteFlag) {
		using(DbSession db = new DbSession()){
			db.PrepareProcedure("FAVORITE_PRODUCT_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PPRODUCT_AGENT_CD",DbSession.DbType.VARCHAR2,pProductAgentCd);
			db.ProcedureInParm("PPRODUCT_SEQ",DbSession.DbType.NUMBER,pProductSeq);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PFAVORIT_COMMENT",DbSession.DbType.VARCHAR2,pFavoritComment);
			db.ProcedureInParm("PDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
