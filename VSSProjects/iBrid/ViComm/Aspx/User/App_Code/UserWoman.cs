﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Mobile
--	Title			: 女性会員

--	Progaram ID		: UserWoman
--
--  Creation Date	: 2009.07.13
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;

[System.Serializable]
public class UserWomanAttr {
	public string attrTypeSeq;
	public string attrTypeNm;
	public string attrSeq;
	public string attrNm;
	public string attrImputValue;
	//public bool freeImputFlag;
	public string inputType;
	public string proirity;
	public string rowCount;
	public string itemNo;
	public string itemCd;
	public bool profileReqItemFlag;

	public UserWomanAttr() {
		attrTypeSeq = "";
		attrTypeNm = "";
		attrSeq = "";
		attrNm = "";
		attrImputValue = "";
		//freeImputFlag = false;
		inputType = "";
		proirity = "";
		rowCount = "";
		itemNo = "";
		itemCd = string.Empty;
		profileReqItemFlag = false;
	}
}

[System.Serializable]
public class CastPayment {
	public string chargeKey;
	public int chargeAmt;
	public int chargeUnit;
	public int chargePoint;

	public CastPayment() {
		chargeAmt = 0;
		chargeUnit = 0;
		chargePoint = 0;
	}

	public CastPayment(int pChargeAmt,int pChargeUnit,int pChargePoint) {
		chargeAmt = pChargeAmt;
		chargeUnit = pChargeUnit;
		chargePoint = pChargePoint;
	}
}

[System.Serializable]
public class UserWomanEx {
	public int enabledBlogFlag;
	public string buyPointMailRxType;
	public string waitingPointLastAddDay;
	public string waitingPointLastReleaseDay;
	public string blogSeq;
	public string blogTitle;
	public string blogDoc;
	public int enabledRichinoFlag;
	public int quickResFlag;//自動退室時には更新がされないので注意。 
	public DateTime quickResOutSchTime;
	public string siteUseStatus;
	public int notReadLikedFlag;
	public int wantedTargetRefuseFlag;
	public int mailRequestRefuseFlag;
	public string unconfirmMarkingCount;  // 足あとの未確認件数
	public string mailResBonusStatus; // メール返信ボーナス開催状況
	public string mailResBonusSeq; // メール返信ボーナス期間SEQ
	public string mailResBonusLimitSec1; // 1通目のメール返信期限秒数
	public string mailResBonusLimitSec2; // 2通目以降のメール返信期限秒数
	public string mailResBonusUnreceivedDays; // 男性会員からのメール未受信日数
	public string mailResBonusMaxNum; // メール返信ボーナス最大返信数

	public UserWomanEx() {
		enabledBlogFlag = 0;
		buyPointMailRxType = string.Empty;
		waitingPointLastAddDay = string.Empty;
		waitingPointLastReleaseDay = string.Empty;
		buyPointMailRxType = string.Empty;
		blogSeq = string.Empty;
		blogTitle = string.Empty;
		blogDoc = string.Empty;
		enabledRichinoFlag = 0;
		quickResFlag = 0;
		siteUseStatus = string.Empty;
		notReadLikedFlag = 0;
		wantedTargetRefuseFlag = 0;
		mailRequestRefuseFlag = 0;
		unconfirmMarkingCount = string.Empty;
		mailResBonusStatus = string.Empty;
		mailResBonusSeq = string.Empty;
		mailResBonusLimitSec1 = string.Empty;
		mailResBonusLimitSec2 = string.Empty;
		mailResBonusUnreceivedDays = string.Empty;
		mailResBonusMaxNum = string.Empty;
	}
}

[System.Serializable]
public class UserWomanCharacter:Object {
	public string siteCd;
	public string userCharNo;
	public string actCategorySeq;
	public int actCategoryIdx;
	public string cryptValue;
	public string handleNm;
	public string handleKanaNm;
	public string commentList;
	public string commentDetail;
	public string waitingComment;
	public int priority;
	public string startPerformDay;
	public Int64 okPlayMask;
	public int unreadMailCount;
	public string photoImgPath;
	public string smallPhotoImgPath;
	public string manMailRxType;
	public string infoMailRxType;
	public int talkEndMailRxFlag;
	public int mobileMailRxTimeUseFlag;
	public string mobileMailRxStartTime;
	public string mobileMailRxEndTime;
	public int mobileMailRxTimeUseFlag2;
	public string mobileMailRxStartTime2;
	public string mobileMailRxEndTime2;
	public string friendIntroCd;
	public Man manCharacter;
	public List<UserWomanAttr> attrList;
	public Dictionary<string,CastPayment> castPaymentList;
	public int castToManBatchMailLimit;
	public int batchMailCount;
	public int inviteMailLimit;
	public int inviteMailCount;
	public int registIncompleteFlag;
	public string birthday;
	public string age;
	public string picSeq;
	public string adCd;
	public string adGroupCd;
	public int noReportAffiliateFlag;
	public int handleNmResettingFlag;
	public int profileResettingFlag;
	public int castToManBatchMailNaFlag;
	public string mobileOnlineStatusNm;
	public string mobileConnectTypeNm;
	public string castNewSign;
	public int characterOnlineStatus;
	public int bulkImpNonUseFlag;
	public string standbyTimeType;
	public int userDefineMask;
	public long pickupMask;
	public string diaryTitle;
	public string omikujiNo;
	public int getOmikujiPoint;
	public UserWomanEx characterEx;
	public GameCharacter gameCharacter;
	public int displayMailAttackLoginFlag;
	public int displayMailAttackNewFlag;
	public string mailResBonus2MainFlag;
	public string mailResBonus2SubFlag;

	public UserWomanCharacter() {
		siteCd = string.Empty;
		userCharNo = string.Empty;
		actCategorySeq = string.Empty;
		actCategoryIdx = 0;
		cryptValue = string.Empty;
		handleNm = string.Empty;
		handleKanaNm = string.Empty;
		commentList = string.Empty;
		commentDetail = string.Empty;
		waitingComment = string.Empty;
		priority = 0;
		startPerformDay = string.Empty;
		okPlayMask = 0;
		unreadMailCount = 0;
		photoImgPath = string.Empty;
		smallPhotoImgPath = string.Empty;
		manMailRxType = string.Empty;
		infoMailRxType = string.Empty;
		talkEndMailRxFlag = 0;
		mobileMailRxTimeUseFlag = 0;
		mobileMailRxStartTime = string.Empty;
		mobileMailRxEndTime = string.Empty;
		mobileMailRxTimeUseFlag2 = 0;
		mobileMailRxStartTime2 = string.Empty;
		mobileMailRxEndTime2 = string.Empty;
		friendIntroCd = string.Empty;
		manCharacter = new Man();
		attrList = new List<UserWomanAttr>();
		castPaymentList = new Dictionary<string,CastPayment>();
		castToManBatchMailLimit = 0;
		batchMailCount = 0;
		inviteMailLimit = 0;
		inviteMailCount = 0;
		registIncompleteFlag = 0;
		birthday = string.Empty;
		age = string.Empty;
		picSeq = string.Empty;
		adCd = string.Empty;
		adGroupCd = string.Empty;
		noReportAffiliateFlag = 0;
		handleNmResettingFlag = 0;
		profileResettingFlag = 0;
		castToManBatchMailNaFlag = 0;
		characterOnlineStatus = 0;
		mobileOnlineStatusNm = "";
		mobileConnectTypeNm = "";
		castNewSign = "";
		bulkImpNonUseFlag = 0;
		standbyTimeType = string.Empty;
		userDefineMask = 0;
		pickupMask = 0;
		omikujiNo = string.Empty;
		getOmikujiPoint = 0;
		diaryTitle = string.Empty;
		characterEx = new UserWomanEx();
		gameCharacter = new GameCharacter();
		displayMailAttackLoginFlag = 0;
		displayMailAttackNewFlag = 0;
		mailResBonus2MainFlag = string.Empty;
		mailResBonus2SubFlag = string.Empty;
	}
}

[System.Serializable]
public class MailInfo:Object {
	public string rxUserSeq;
	public string rxUserCharNo;
	public string rxUserNm;
	public string mailTitle;
	public string mailDoc;
	public string mailTemplateNo;
	public string queryString;
	public string returnMailSeq;
	public string objTempId;
	public string layOutType;
	public bool isReturnMail;
	public bool isSent;
	public string draftMailSeq;
	public int chatMailFlag;
	public string rxLoginId;
	public int mailAttackLoginFlag;
	public int mailAttackNewFlag;
	public string mailResBonus2Flag;

	public MailInfo() {
		rxUserSeq = string.Empty;
		rxUserCharNo = string.Empty;
		rxUserNm = string.Empty;
		mailTitle = string.Empty;
		mailDoc = string.Empty;
		mailTemplateNo = string.Empty;
		queryString = string.Empty;
		returnMailSeq = string.Empty;
		objTempId = string.Empty;
		layOutType = string.Empty;
		isReturnMail = false;
		isSent = false;
		draftMailSeq = string.Empty;
		chatMailFlag = 0;
		rxLoginId = string.Empty;
		mailAttackLoginFlag = 0;
		mailAttackNewFlag = 0;
		mailResBonus2Flag = string.Empty;
	}
}

[System.Serializable]
public class UserWoman:DbSession {
	public string userSeq;
	public string castNm;
	public string loginId;
	public string loginPassword;
	public string previousLoginId;
	public string previousLoginPassword;
	public string emailAddr;
	public int nonExistEmailAddrFlag;
	public string tel;
	public string uri;
	public string crosmileUri;
	public string crosmileRegistKey;
	public int useFreeDialFlag;
	public int useCrosmileFlag;
	public string productionSeq;
	public string connectType;
	public int monitorEnableFlag;
	//	public int onlineStatus;
	public int dummyTalkFlag;
	public DateTime startDate;
	public DateTime endDate;
	public Dictionary<string,UserWomanCharacter> characterList;
	public UserWomanCharacter CurCharacter {
		get {
			if (characterList != null && characterList.ContainsKey(curKey)) {
				return characterList[curKey];
			}
			return null;
		}
	}
	public Dictionary<string,bool> loginCharacter;
	public Dictionary<string,MailInfo> mailData;
	public BbsInfo bbsInfo;
	public int mailDataSeq;
	public string curCharNo;
	public string curKey;
	public string tempRegistId;
	public string bankNm;
	public string bankCd;
	public string bankOfficeNm;
	public string bankOfficeKanaNm;
	public string bankOfficeCd;
	public string bankAccountType;
	public string bankAccountType2;
	public string bankAccountNo;
	public string bankAccountHolderNm;
	public Dictionary<string,int> bonusAmtList;
	public Dictionary<string,int> bonusPointList;
	public int totalPaymentAmt;
	public int totalPaymentPoint;
	public string loginErrMsgCd;		//ログイン時のエラーメッセージコード 
	public string terminalUniqueId;
	public string iModeId;
	public DateTime registDate;
	public string performanceFindMonth;
	public string performanceFindFrom;
	public string performanceFindTo;
	public string talkRequestFindType;
	public int bankAccountInvalidFlag;
	public int pointPrice;
	public int selfMinimumPayment;
	public int autoMinimumPayment;
	public DateTime lastPaymentDate;
	public int paymentFlag;
	public string ObjTempId;
	public int waitPaymentFlag;
	public string rankNm;
	public string userRankCd;
	public int nonUsedEasyLoginFlag;
	public string registCarrierCd;
	public int paymentTimingType;
	public int useVoiceappFlag;
	public int existDeviceTokenFlag;
	/// <summary>出演者ステータス</summary>
	public string userStatus = string.Empty;
	/// <summary>退会申請中フラグ</summary>
	public string existAcceptWithdrawalFlag = string.Empty;
	/// <summary>最終退会引き留め処理日時</summary>
	public string lastWithdrawalDetainDate = string.Empty;

	public UserWoman() {
		userSeq = string.Empty;
		castNm = string.Empty;
		loginId = string.Empty;
		loginPassword = string.Empty;
		previousLoginId = string.Empty;
		previousLoginPassword = string.Empty;
		emailAddr = string.Empty;
		nonExistEmailAddrFlag = 0;
		tel = string.Empty;
		uri = string.Empty;
		crosmileUri = string.Empty;
		crosmileRegistKey = string.Empty;
		useFreeDialFlag = 0;
		useCrosmileFlag = 0;
		connectType = string.Empty;
		productionSeq = string.Empty;
		monitorEnableFlag = 0;
		dummyTalkFlag = 0;
		characterList = new Dictionary<string,UserWomanCharacter>();
		loginCharacter = new Dictionary<string,bool>();
		mailData = new Dictionary<string,MailInfo>();
		bbsInfo = new BbsInfo();
		mailDataSeq = 0;
		curCharNo = string.Empty;
		curKey = string.Empty;
		tempRegistId = string.Empty;
		bankNm = string.Empty;
		bankCd = string.Empty;
		bankOfficeNm = string.Empty;
		bankOfficeKanaNm = string.Empty;
		bankOfficeCd = string.Empty;
		bankAccountType = string.Empty;
		bankAccountType2 = string.Empty;
		bankAccountNo = string.Empty;
		bankAccountHolderNm = string.Empty;
		bonusAmtList = new Dictionary<string,int>();
		bonusPointList = new Dictionary<string,int>();
		totalPaymentAmt = 0;
		totalPaymentPoint = 0;
		loginErrMsgCd = string.Empty;
		terminalUniqueId = string.Empty;
		iModeId = string.Empty;
		performanceFindMonth = string.Empty;
		bankAccountInvalidFlag = 0;
		pointPrice = 1;
		selfMinimumPayment = 0;
		paymentFlag = 0;
		ObjTempId = string.Empty;
		waitPaymentFlag = 0;
		rankNm = string.Empty;
		userRankCd = string.Empty;
		nonUsedEasyLoginFlag = 0;
		registCarrierCd = string.Empty;
		paymentTimingType = 0;
		useVoiceappFlag = 0;
		existDeviceTokenFlag = 0;
		userStatus = string.Empty;
		existAcceptWithdrawalFlag = string.Empty;
		lastWithdrawalDetainDate = string.Empty;
	}

	[System.Serializable]
	public class BbsInfo:Object {
		public string bbsThreadTitle;
		public string bbsThreadDoc;
		public string bbsThreadHandleNm;
		public string bbsResDoc;
		public string bbsResHandleNm;

		public BbsInfo() {
			bbsThreadTitle = string.Empty;
			bbsThreadDoc = string.Empty;
			bbsThreadHandleNm = string.Empty;
			bbsResDoc = string.Empty;
			bbsResHandleNm = string.Empty;
		}
	}

	public int NextMailDataSeq() {
		mailDataSeq += 1;
		return mailDataSeq;
	}

	public bool GetOneByTel(string pTel,string pPassword) {
		DataSet ds;
		bool bExist = false;
		string sLoginId = "";
		int iCnt = 0;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"LOGIN_ID,USER_STATUS " +
							"FROM " +
								"T_USER " +
							"WHERE " +
								"TEL =:TEL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("TEL",pTel);
				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_STATUS"].Equals(ViCommConst.USER_WOMAN_HOLD)) {
							sLoginId = dr["LOGIN_ID"].ToString();
							iCnt++;
						}
					}
				}
			}

			if (iCnt == 1) {
				bExist = true;
			} else if (iCnt == 0) {
				loginErrMsgCd = ViCommConst.ERR_STATUS_INVALID_ID_PW;
			} else {
				loginErrMsgCd = ViCommConst.ERR_STATUS_EXIST_SOME_TEL;
			}
		} finally {
			conn.Close();
		}

		if (bExist) {
			bExist = GetOne(sLoginId,pPassword);
		}
		return bExist;
	}

	public bool GetOne(string pLoginId,string pPassword) {
		return GetOneBase(pLoginId,pPassword,false);
	}

	public bool GetOneByPreviousId(string pLoginId,string pPassword) {
		return GetOneBase(pLoginId,pPassword,true);
	}

	public bool GetOneBase(string pLoginId,string pPassword,bool pUsePreviousLoginId) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	USER_SEQ					,").AppendLine();
			sSql.Append("	CAST_NM						,").AppendLine();
			sSql.Append("	TEL							,").AppendLine();
			sSql.Append("	URI							,").AppendLine();
			sSql.Append("	CROSMILE_URI				,").AppendLine();
			sSql.Append("	CROSMILE_REGIST_KEY			,").AppendLine();
			sSql.Append("	EMAIL_ADDR					,").AppendLine();
			sSql.Append("	NON_EXIST_MAIL_ADDR_FLAG	,").AppendLine();
			sSql.Append("	USER_STATUS					,").AppendLine();
			sSql.Append("	PRODUCTION_SEQ				,").AppendLine();
			sSql.Append("	MONITOR_ENABLE_FLAG			,").AppendLine();
			sSql.Append("	REGIST_CARRIER_CD			,").AppendLine();
			sSql.Append("	TERMINAL_UNIQUE_ID			,").AppendLine();
			sSql.Append("	IMODE_ID					,").AppendLine();
			sSql.Append("	BANK_NM						,").AppendLine();
			sSql.Append("	BANK_CD						,").AppendLine();
			sSql.Append("	BANK_OFFICE_NM				,").AppendLine();
			sSql.Append("	BANK_OFFICE_KANA_NM			,").AppendLine();
			sSql.Append("	BANK_OFFICE_CD				,").AppendLine();
			sSql.Append("	BANK_ACCOUNT_TYPE			,").AppendLine();
			sSql.Append("	BANK_ACCOUNT_TYPE2			,").AppendLine();
			sSql.Append("	BANK_ACCOUNT_NO				,").AppendLine();
			sSql.Append("	BANK_ACCOUNT_HOLDER_NM		,").AppendLine();
			sSql.Append("	PAYMENT_TIMING_TYPE			,").AppendLine();
			sSql.Append("	START_DATE					,").AppendLine();
			sSql.Append("	LOGOFF_DATE					,").AppendLine();
			sSql.Append("	END_DATE					,").AppendLine();
			sSql.Append("	LOGIN_ID					,").AppendLine();
			sSql.Append("	LOGIN_PASSWORD				,").AppendLine();
			sSql.Append("	PREVIOUS_LOGIN_ID			,").AppendLine();
			sSql.Append("	PREVIOUS_LOGIN_PASSWORD		,").AppendLine();
			sSql.Append("	DUMMY_TALK_FLAG				,").AppendLine();
			sSql.Append("	REGIST_DATE					,").AppendLine();
			sSql.Append("	USE_FREE_DIAL_FLAG			,").AppendLine();
			sSql.Append("	USE_CROSMILE_FLAG			,").AppendLine();
			sSql.Append("	BANK_ACCOUNT_INVALID_FLAG	,").AppendLine();
			sSql.Append("	POINT_PRICE					,").AppendLine();
			sSql.Append("	WAIT_PAYMENT_FLAG			,").AppendLine();
			sSql.Append("	USER_RANK					,").AppendLine();
			sSql.Append("	USER_RANK_NM				,").AppendLine();
			sSql.Append("	NOT_USED_EASY_LOGIN			,").AppendLine();
			sSql.Append("	USE_VOICEAPP_FLAG			").AppendLine();
			sSql.Append("FROM ").AppendLine();
			sSql.Append("	VW_CAST06 ").AppendLine();
			sSql.Append("WHERE ").AppendLine();
			if (!pUsePreviousLoginId) {
				sSql.Append("LOGIN_ID =:LOGIN_ID ").AppendLine();
			} else {
				sSql.Append("PREVIOUS_LOGIN_ID =:LOGIN_ID ").AppendLine();
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("LOGIN_ID",pLoginId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST06");
					if (ds.Tables["VW_CAST06"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST06"].Rows[0];
						string sUsedPassword = dr["LOGIN_PASSWORD"].ToString();
						if (pUsePreviousLoginId) {
							sUsedPassword = dr["PREVIOUS_LOGIN_PASSWORD"].ToString();
						}

						if ((sUsedPassword.Equals(pPassword)) && (dr["USER_STATUS"].Equals(ViCommConst.USER_WOMAN_NORMAL))) {
							userSeq = dr["USER_SEQ"].ToString();
							castNm = dr["CAST_NM"].ToString();
							tel = dr["TEL"].ToString();
							uri = dr["URI"].ToString();
							crosmileUri = dr["CROSMILE_URI"].ToString();
							crosmileRegistKey = dr["CROSMILE_REGIST_KEY"].ToString();
							emailAddr = dr["EMAIL_ADDR"].ToString();
							nonExistEmailAddrFlag = int.Parse(dr["NON_EXIST_MAIL_ADDR_FLAG"].ToString());
							loginId = dr["LOGIN_ID"].ToString();
							loginPassword = dr["LOGIN_PASSWORD"].ToString();
							previousLoginId = dr["PREVIOUS_LOGIN_ID"].ToString();
							previousLoginPassword = dr["PREVIOUS_LOGIN_PASSWORD"].ToString();
							productionSeq = dr["PRODUCTION_SEQ"].ToString();
							monitorEnableFlag = int.Parse(dr["MONITOR_ENABLE_FLAG"].ToString());
							dummyTalkFlag = int.Parse(dr["DUMMY_TALK_FLAG"].ToString());
							bankNm = dr["BANK_NM"].ToString();
							bankCd = dr["BANK_CD"].ToString();
							bankOfficeNm = dr["BANK_OFFICE_NM"].ToString();
							bankOfficeKanaNm = dr["BANK_OFFICE_KANA_NM"].ToString();
							bankOfficeCd = dr["BANK_OFFICE_CD"].ToString();
							bankAccountType = dr["BANK_ACCOUNT_TYPE"].ToString();
							bankAccountType2 = dr["BANK_ACCOUNT_TYPE2"].ToString();
							bankAccountNo = dr["BANK_ACCOUNT_NO"].ToString();
							bankAccountHolderNm = dr["BANK_ACCOUNT_HOLDER_NM"].ToString();
							useFreeDialFlag = int.Parse(dr["USE_FREE_DIAL_FLAG"].ToString());
							useCrosmileFlag = int.Parse(dr["USE_CROSMILE_FLAG"].ToString());
							bankAccountInvalidFlag = int.Parse(dr["BANK_ACCOUNT_INVALID_FLAG"].ToString());
							pointPrice = int.Parse(dr["POINT_PRICE"].ToString()) != 0 ? int.Parse(dr["POINT_PRICE"].ToString()) : 1;
							waitPaymentFlag = int.Parse(dr["WAIT_PAYMENT_FLAG"].ToString());
							userRankCd = dr["USER_RANK"].ToString();
							rankNm = dr["USER_RANK_NM"].ToString();
							nonUsedEasyLoginFlag = int.Parse(dr["NOT_USED_EASY_LOGIN"].ToString());

							if (!dr["START_DATE"].ToString().Equals("")) {
								DateTime.TryParse(dr["START_DATE"].ToString(),out startDate);
							}
							if (dr["END_DATE"].ToString().Equals("") && !dr["LOGOFF_DATE"].ToString().Equals("")) {
								DateTime.TryParse(dr["LOGOFF_DATE"].ToString(),out endDate);
							} else if (!dr["END_DATE"].ToString().Equals("")) {
								DateTime.TryParse(dr["END_DATE"].ToString(),out endDate);
							} else {
								endDate = DateTime.Now;
							}
							terminalUniqueId = dr["TERMINAL_UNIQUE_ID"].ToString();
							registCarrierCd = dr["REGIST_CARRIER_CD"].ToString();
							paymentTimingType = int.Parse(dr["PAYMENT_TIMING_TYPE"].ToString());
							useVoiceappFlag = int.Parse(dr["USE_VOICEAPP_FLAG"].ToString());

							iModeId = dr["IMODE_ID"].ToString();
							if (!dr["REGIST_DATE"].ToString().Equals("")) {
								DateTime.TryParse(dr["REGIST_DATE"].ToString(),out registDate);
							}
							loginErrMsgCd = "";

							bExist = true;
						} else if (sUsedPassword.Equals(pPassword) && dr["USER_STATUS"].Equals(ViCommConst.USER_WOMAN_AUTH_WAIT)) {
							loginErrMsgCd = ViCommConst.ERR_STATUS_CAST_LOGIN_AUTH_WAIT;
						} else if (sUsedPassword.Equals(pPassword) && dr["USER_STATUS"].Equals(ViCommConst.USER_WOMAN_STOP)) {
							loginErrMsgCd = ViCommConst.ERR_STATUS_CAST_LOGIN_STOP;
						} else if (sUsedPassword.Equals(pPassword) && dr["USER_STATUS"].Equals(ViCommConst.USER_WOMAN_RESIGNED)) {
							loginErrMsgCd = ViCommConst.ERR_STATUS_CAST_LOGIN_RESIGNED;
							loginId = dr["LOGIN_ID"].ToString();
							loginPassword = dr["LOGIN_PASSWORD"].ToString();
						} else if (sUsedPassword.Equals(pPassword) && dr["USER_STATUS"].Equals(ViCommConst.USER_WOMAN_BAN)) {
							loginErrMsgCd = ViCommConst.ERR_STATUS_CAST_LOGIN_BAN;
						} else {
							loginErrMsgCd = ViCommConst.ERR_STATUS_INVALID_ID_PW;
						}
					} else {
						loginErrMsgCd = ViCommConst.ERR_STATUS_INVALID_ID_PW;
					}
				}
			}
		} finally {
			conn.Close();
		}
		characterList.Clear();

		if (bExist) {
			DataSet dsCharacter = GetCharacterList("");
			UserWomanCharacter oCharacter;
			foreach (DataRow drSub in dsCharacter.Tables[0].Rows) {
				oCharacter = new UserWomanCharacter();
				characterList.Add(drSub["SITE_CD"].ToString() + drSub["USER_CHAR_NO"].ToString(),oCharacter);
				SetupCharacterInfo(drSub);
			}
			using (ManageCompany oManageCompany = new ManageCompany()) {
				string sValue;
				oManageCompany.GetValue("CAST_SELF_MINIMUM_PAYMENT",out sValue);
				selfMinimumPayment = int.Parse(sValue);

				string sAutoMinPayment;
				oManageCompany.GetValue("CAST_AUTO_MINIMUM_PAYMENT",out sAutoMinPayment);
				autoMinimumPayment = int.Parse(sAutoMinPayment);

				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_KICK_BACK_INTRO_SYSTEM)) {
					using (PersonalAd oPersonalAd = new PersonalAd()) {
						SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];
						userObjs.personalAdCd = oPersonalAd.GetPersonalAdCd(userSeq);
					}
				}
			}

			using (PaymentHistory oPaymentHistory = new PaymentHistory()) {
				if (oPaymentHistory.GetLastPaymentDate(userSeq,out lastPaymentDate)) {
					paymentFlag = ViCommConst.FLAG_ON;
				}
			}
		}

		return bExist;
	}

	public DataSet GetCharacterList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT ").AppendLine();
			sSql.Append("VW_CAST01.SITE_CD									SITE_CD							,").AppendLine();
			sSql.Append("VW_CAST01.USER_SEQ									USER_SEQ						,").AppendLine();
			sSql.Append("VW_CAST01.USER_CHAR_NO								USER_CHAR_NO					,").AppendLine();
			sSql.Append("VW_CAST01.CRYPT_VALUE								CRYPT_VALUE						,").AppendLine();
			sSql.Append("VW_CAST01.CAST_NM									CAST_NM							,").AppendLine();
			sSql.Append("VW_CAST01.HANDLE_NM								HANDLE_NM						,").AppendLine();
			sSql.Append("VW_CAST01.HANDLE_KANA_NM							HANDLE_KANA_NM					,").AppendLine();
			sSql.Append("VW_CAST01.SITE_NM									SITE_NM							,").AppendLine();
			sSql.Append("VW_CAST01.PHOTO_IMG_PATH							PHOTO_IMG_PATH					,").AppendLine();
			sSql.Append("VW_CAST01.SMALL_PHOTO_IMG_PATH						SMALL_PHOTO_IMG_PATH			,").AppendLine();
			sSql.Append("VW_CAST01.PIC_SEQ									PIC_SEQ							,").AppendLine();
			sSql.Append("VW_CAST01.ACT_CATEGORY_SEQ							ACT_CATEGORY_SEQ				,").AppendLine();
			sSql.Append("VW_CAST01.ACT_CATEGORY_NM							ACT_CATEGORY_NM					,").AppendLine();
			sSql.Append("VW_CAST01.ACT_CATEGORY_IDX							ACT_CATEGORY_IDX				,").AppendLine();
			sSql.Append("VW_CAST01.COMMENT_LIST								COMMENT_LIST					,").AppendLine();
			sSql.Append("VW_CAST01.COMMENT_DETAIL							COMMENT_DETAIL					,").AppendLine();
			sSql.Append("VW_CAST01.WAITING_COMMENT							WAITING_COMMENT					,").AppendLine();
			sSql.Append("VW_CAST01.OK_PLAY_MASK								OK_PLAY_MASK					,").AppendLine();
			sSql.Append("VW_CAST01.MAN_MAIL_RX_TYPE							MAN_MAIL_RX_TYPE				,").AppendLine();
			sSql.Append("VW_CAST01.INFO_MAIL_RX_TYPE						INFO_MAIL_RX_TYPE				,").AppendLine();
			sSql.Append("VW_CAST01.TALK_END_MAIL_RX_FLAG					TALK_END_MAIL_RX_FLAG			,").AppendLine();
			sSql.Append("VW_CAST01.MOBILE_MAIL_RX_TIME_USE_FLAG				MOBILE_MAIL_RX_TIME_USE_FLAG	,").AppendLine();
			sSql.Append("VW_CAST01.MOBILE_MAIL_RX_START_TIME				MOBILE_MAIL_RX_START_TIME		,").AppendLine();
			sSql.Append("VW_CAST01.MOBILE_MAIL_RX_END_TIME					MOBILE_MAIL_RX_END_TIME			,").AppendLine();
			sSql.Append("VW_CAST01.MOBILE_MAIL_RX_TIME_USE_FLAG2			MOBILE_MAIL_RX_TIME_USE_FLAG2	,").AppendLine();
			sSql.Append("VW_CAST01.MOBILE_MAIL_RX_START_TIME2				MOBILE_MAIL_RX_START_TIME2		,").AppendLine();
			sSql.Append("VW_CAST01.MOBILE_MAIL_RX_END_TIME2					MOBILE_MAIL_RX_END_TIME2		,").AppendLine();
			sSql.Append("VW_CAST01.NA_MARK									NA_MARK							,").AppendLine();
			sSql.Append("VW_CAST01.FRIEND_INTRO_CD							FRIEND_INTRO_CD					,").AppendLine();
			sSql.Append("VW_CAST01.CAST_TO_MAN_BATCH_MAIL_NA_FLAG			CAST_TO_MAN_BATCH_MAIL_NA_FLAG	,").AppendLine();
			sSql.Append("VW_CAST01.CAST_TO_MAN_BATCH_MAIL_LIMIT				CAST_TO_MAN_BATCH_MAIL_LIMIT	,").AppendLine();
			sSql.Append("VW_CAST01.BATCH_MAIL_COUNT							BATCH_MAIL_COUNT				,").AppendLine();
			sSql.Append("VW_CAST01.INVITE_MAIL_LIMIT						INVITE_MAIL_LIMIT				,").AppendLine();
			sSql.Append("VW_CAST01.INVITE_MAIL_COUNT						INVITE_MAIL_COUNT				,").AppendLine();
			sSql.Append("VW_CAST01.CONNECT_TYPE								CONNECT_TYPE					,").AppendLine();
			sSql.Append("VW_CAST01.BIRTHDAY									BIRTHDAY						,").AppendLine();
			sSql.Append("VW_CAST01.AGE										AGE								,").AppendLine();
			sSql.Append("VW_CAST01.NA_FLAG									NA_FLAG							,").AppendLine();
			sSql.Append("VW_CAST01.AD_CD									AD_CD							,").AppendLine();
			sSql.Append("VW_CAST01.NO_REPORT_AFFILIATE_FLAG					NO_REPORT_AFFILIATE_FLAG		,").AppendLine();
			sSql.Append("VW_CAST01.HANDLE_NM_RESETTING_FLAG					HANDLE_NM_RESETTING_FLAG		,").AppendLine();
			sSql.Append("VW_CAST01.PROFILE_RESETTING_FLAG					PROFILE_RESETTING_FLAG			,").AppendLine();
			sSql.Append("VW_CAST01.CHARACTER_ONLINE_STATUS					CHARACTER_ONLINE_STATUS			,").AppendLine();
			sSql.Append("VW_CAST01.BULK_IMP_NON_USE_FLAG					BULK_IMP_NON_USE_FLAG			,").AppendLine();
			sSql.Append("VW_CAST01.START_PERFORM_DAY						START_PERFORM_DAY				,").AppendLine();
			sSql.Append("VW_CAST01.STANDBY_TIME_TYPE						STANDBY_TIME_TYPE				,").AppendLine();
			sSql.Append("VW_CAST01.USER_DEFINE_MASK							USER_DEFINE_MASK				,").AppendLine();
			sSql.Append("VW_CAST01.PICKUP_MASK								PICKUP_MASK						,").AppendLine();
			sSql.Append("VW_CAST01.DIARY_TITLE                              DIARY_TITLE                     ,").AppendLine();
			sSql.Append("NVL(EX.ENABLED_BLOG_FLAG,0)						ENABLED_BLOG_FLAG				,").AppendLine();
			sSql.Append("EX.BUY_POINT_MAIL_RX_TYPE							BUY_POINT_MAIL_RX_TYPE			,").AppendLine();
			sSql.Append("EX.WAITING_POINT_LAST_ADD_DAY						WAITING_POINT_LAST_ADD_DAY		,").AppendLine();
			sSql.Append("EX.WAITING_POINT_LAST_RELEASE_DAY					WAITING_POINT_LAST_RELEASE_DAY	,").AppendLine();
			sSql.Append("EX.BLOG_SEQ										BLOG_SEQ						,").AppendLine();
			sSql.Append("EX.BLOG_TITLE										BLOG_TITLE						,").AppendLine();
			sSql.Append("EX.BLOG_DOC										BLOG_DOC						,").AppendLine();
			sSql.Append("EX.ENABLED_RICHINO_FLAG							ENABLED_RICHINO_FLAG			,").AppendLine();
			sSql.Append("EX.NO_UPDATE_RICHINO_VIEW_FLAG						NO_UPDATE_RICHINO_VIEW_FLAG		,").AppendLine();
			sSql.Append("EX.SITE_USE_STATUS									SITE_USE_STATUS					,").AppendLine();
			sSql.Append("EX.NOT_READ_LIKED_FLAG								NOT_READ_LIKED_FLAG				,").AppendLine();
			sSql.Append("EX.WANTED_TARGET_REFUSE_FLAG						WANTED_TARGET_REFUSE_FLAG		,").AppendLine();
			sSql.Append("EX.MAIL_REQUEST_REFUSE_FLAG						MAIL_REQUEST_REFUSE_FLAG		,").AppendLine();
			sSql.Append("VW_CAST01.QUICK_RES_FLAG							QUICK_RES_FLAG					,").AppendLine();
			sSql.Append("VW_CAST01.QUICK_RES_OUT_SCH_TIME					QUICK_RES_OUT_SCH_TIME			,").AppendLine();
			sSql.Append("VW_CAST01.DEVICE_TOKEN								DEVICE_TOKEN					,").AppendLine();
			sSql.Append("CASE").AppendLine();
			sSql.Append("	WHEN VW_CAST01.CHARACTER_ONLINE_STATUS = 0 THEN	").AppendLine();
			sSql.Append("		T_SITE_MANAGEMENT.OFFLINE_GUIDANCE			").AppendLine();
			sSql.Append("	WHEN VW_CAST01.CHARACTER_ONLINE_STATUS = 1 THEN	").AppendLine();
			sSql.Append("		T_SITE_MANAGEMENT.LOGINED_GUIDANCE			").AppendLine();
			sSql.Append("	WHEN VW_CAST01.CHARACTER_ONLINE_STATUS = 2 THEN	").AppendLine();
			sSql.Append("		T_SITE_MANAGEMENT.WAITING_GUIDANCE			").AppendLine();
			sSql.Append("	WHEN VW_CAST01.CHARACTER_ONLINE_STATUS = 3 THEN	").AppendLine();
			sSql.Append("		T_SITE_MANAGEMENT.TALKING_WSHOT_GUIDANCE	").AppendLine();
			sSql.Append("END AS MOBILE_ONLINE_STATUS_NM,").AppendLine();
			sSql.Append("CASE").AppendLine();
			sSql.Append("	WHEN (VW_CAST01.CONNECT_TYPE = '1') THEN			").AppendLine();
			sSql.Append("		T_SITE_MANAGEMENT.CONNECT_TYPE_VIDEO_GUIDANCE	").AppendLine();
			sSql.Append("	WHEN (VW_CAST01.CONNECT_TYPE = '2') THEN			").AppendLine();
			sSql.Append("		T_SITE_MANAGEMENT.CONNECT_TYPE_VOICE_GUIDANCE	").AppendLine();
			sSql.Append("	WHEN (VW_CAST01.CONNECT_TYPE = '3') THEN			").AppendLine();
			sSql.Append("		T_SITE_MANAGEMENT.CONNECT_TYPE_BOTH_GUIDANCE	").AppendLine();
			sSql.Append("END AS MOBILE_CONNECT_TYPE_NM,").AppendLine();
			sSql.Append("CASE").AppendLine();
			sSql.Append("	WHEN (VW_CAST01.START_PERFORM_DAY >= TO_CHAR(SYSDATE - T_SITE_MANAGEMENT.NEW_FACE_DAYS,'YYYY/MM/DD')) THEN T_SITE_MANAGEMENT.NEW_FACE_MARK").AppendLine();
			sSql.Append("ELSE NULL").AppendLine();
			sSql.Append("END AS NEW_CAST_SIGN").AppendLine();
			sSql.Append("FROM").AppendLine();
			sSql.Append("VW_CAST01,T_SITE_MANAGEMENT ").AppendLine();
			sSql.Append(",VW_CAST_CHARACTER_EX01 EX ").AppendLine();
			sSql.Append("WHERE ").AppendLine();

			if (pSiteCd.Equals("") == false) {
				sSql.Append("VW_CAST01.SITE_CD = :SITE_CD AND").AppendLine();
			}

			sSql.Append("VW_CAST01.USER_SEQ		= :USER_SEQ					AND ").AppendLine();
			sSql.Append("VW_CAST01.SITE_NA_FLAG	= :SITE_NA_FLAG				AND ").AppendLine();
			sSql.Append("VW_CAST01.NA_FLAG		IN( :NA_FLAG1,:NA_FLAG2,:NA_FLAG3)	AND	").AppendLine();
			sSql.Append("VW_CAST01.SITE_CD		= T_SITE_MANAGEMENT.SITE_CD ").AppendLine();
			sSql.Append(" AND	VW_CAST01.SITE_CD		= EX.SITE_CD		(+)	AND ").AppendLine();
			sSql.Append("		VW_CAST01.USER_SEQ		= EX.USER_SEQ		(+)	AND ").AppendLine();
			sSql.Append("		VW_CAST01.USER_CHAR_NO	= EX.USER_CHAR_NO	(+)		").AppendLine();
			sSql.Append("ORDER BY ").AppendLine();
			sSql.Append("VW_CAST01.SITE_PRIORITY,VW_CAST01.SITE_CD,VW_CAST01.USER_SEQ,VW_CAST01.USER_CHAR_NO").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				if (pSiteCd.Equals("") == false) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}
				cmd.Parameters.Add("USER_SEQ",userSeq);
				cmd.Parameters.Add("SITE_NA_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("NA_FLAG1",ViCommConst.NaFlag.OK);
				cmd.Parameters.Add("NA_FLAG2",ViCommConst.NaFlag.NO_AUTH);
				cmd.Parameters.Add("NA_FLAG3",ViCommConst.NaFlag.IMPORT);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public int GetOnlineStatus(string pUserSeq,string pUserCharNo) {
		DataSet ds;

		if (string.IsNullOrEmpty(pUserCharNo)) {
			pUserCharNo = ViCommConst.MAIN_CHAR_NO;
		}

		int iCharacterOnlineStatus = ViCommConst.USER_OFFLINE;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"SITE_CD,CHARACTER_ONLINE_STATUS " +
							"FROM " +
								"T_CAST_CHARACTER " +
							"WHERE " +
								"USER_SEQ =:USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_CHARACTER");
					if (ds.Tables["T_CAST_CHARACTER"].Rows.Count != 0) {
						foreach (DataRow dr in ds.Tables[0].Rows) {
							SessionWoman sessionWoman = (SessionWoman)(HttpContext.Current.Session["objs"]);
							if (sessionWoman.userWoman.characterList.ContainsKey(dr["SITE_CD"].ToString() + pUserCharNo)) {
								sessionWoman.userWoman.characterList[dr["SITE_CD"].ToString() + pUserCharNo].characterOnlineStatus = int.Parse(dr["CHARACTER_ONLINE_STATUS"].ToString());
							}
							if (iCharacterOnlineStatus < int.Parse(dr["CHARACTER_ONLINE_STATUS"].ToString())) {
								iCharacterOnlineStatus = int.Parse(dr["CHARACTER_ONLINE_STATUS"].ToString());
							}
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iCharacterOnlineStatus;
	}

	public void UpdateCharacterInfo(string pSiteCd) {
		DataSet dsCharacter = GetCharacterList(pSiteCd);
		foreach (DataRow drSub in dsCharacter.Tables[0].Rows) {
			SetupCharacterInfo(drSub);
		}
	}

	private void SetupCharacterInfo(DataRow pDataRow) {
		SessionWoman sessionWoman = (SessionWoman)(HttpContext.Current.Session["objs"]);
		UserWomanCharacter oCharacter = (UserWomanCharacter)sessionWoman.userWoman.characterList[pDataRow["SITE_CD"].ToString() + pDataRow["USER_CHAR_NO"].ToString()];
		if (oCharacter != null) {
			oCharacter.siteCd = pDataRow["SITE_CD"].ToString();
			oCharacter.userCharNo = pDataRow["USER_CHAR_NO"].ToString();
			oCharacter.actCategorySeq = pDataRow["ACT_CATEGORY_SEQ"].ToString();
			oCharacter.handleNm = pDataRow["HANDLE_NM"].ToString();
			oCharacter.handleKanaNm = pDataRow["HANDLE_KANA_NM"].ToString();
			oCharacter.commentList = pDataRow["COMMENT_LIST"].ToString();
			oCharacter.commentDetail = pDataRow["COMMENT_DETAIL"].ToString();
			oCharacter.waitingComment = pDataRow["WAITING_COMMENT"].ToString();
			oCharacter.okPlayMask = Int64.Parse(pDataRow["OK_PLAY_MASK"].ToString());
			oCharacter.photoImgPath = pDataRow["PHOTO_IMG_PATH"].ToString();
			oCharacter.smallPhotoImgPath = pDataRow["SMALL_PHOTO_IMG_PATH"].ToString();
			oCharacter.manMailRxType = pDataRow["MAN_MAIL_RX_TYPE"].ToString();
			oCharacter.infoMailRxType = pDataRow["INFO_MAIL_RX_TYPE"].ToString();
			oCharacter.talkEndMailRxFlag = int.Parse(pDataRow["TALK_END_MAIL_RX_FLAG"].ToString());
			oCharacter.mobileMailRxTimeUseFlag = int.Parse(pDataRow["MOBILE_MAIL_RX_TIME_USE_FLAG"].ToString());
			oCharacter.mobileMailRxStartTime = pDataRow["MOBILE_MAIL_RX_START_TIME"].ToString();
			oCharacter.mobileMailRxEndTime = pDataRow["MOBILE_MAIL_RX_END_TIME"].ToString();
			oCharacter.mobileMailRxTimeUseFlag2 = int.Parse(pDataRow["MOBILE_MAIL_RX_TIME_USE_FLAG2"].ToString());
			oCharacter.mobileMailRxStartTime2 = pDataRow["MOBILE_MAIL_RX_START_TIME2"].ToString();
			oCharacter.mobileMailRxEndTime2 = pDataRow["MOBILE_MAIL_RX_END_TIME2"].ToString();
			oCharacter.friendIntroCd = pDataRow["FRIEND_INTRO_CD"].ToString();
			oCharacter.castToManBatchMailNaFlag = int.Parse(pDataRow["CAST_TO_MAN_BATCH_MAIL_NA_FLAG"].ToString());
			oCharacter.castToManBatchMailLimit = int.Parse(pDataRow["CAST_TO_MAN_BATCH_MAIL_LIMIT"].ToString());
			oCharacter.batchMailCount = int.Parse(pDataRow["BATCH_MAIL_COUNT"].ToString());
			oCharacter.inviteMailLimit = int.Parse(pDataRow["INVITE_MAIL_LIMIT"].ToString());
			oCharacter.inviteMailCount = int.Parse(pDataRow["INVITE_MAIL_COUNT"].ToString());
			oCharacter.actCategoryIdx = int.Parse(pDataRow["ACT_CATEGORY_IDX"].ToString());
			oCharacter.cryptValue = pDataRow["CRYPT_VALUE"].ToString();
			oCharacter.birthday = pDataRow["BIRTHDAY"].ToString();
			oCharacter.age = pDataRow["AGE"].ToString();
			oCharacter.picSeq = pDataRow["PIC_SEQ"].ToString();
			oCharacter.adCd = pDataRow["AD_CD"].ToString();
			oCharacter.noReportAffiliateFlag = int.Parse(pDataRow["NO_REPORT_AFFILIATE_FLAG"].ToString());
			oCharacter.handleNmResettingFlag = int.Parse(pDataRow["HANDLE_NM_RESETTING_FLAG"].ToString());
			oCharacter.profileResettingFlag = int.Parse(pDataRow["PROFILE_RESETTING_FLAG"].ToString());
			oCharacter.characterOnlineStatus = int.Parse(pDataRow["CHARACTER_ONLINE_STATUS"].ToString());
			oCharacter.mobileConnectTypeNm = pDataRow["MOBILE_CONNECT_TYPE_NM"].ToString();
			oCharacter.mobileOnlineStatusNm = pDataRow["MOBILE_ONLINE_STATUS_NM"].ToString();
			oCharacter.castNewSign = pDataRow["NEW_CAST_SIGN"].ToString();
			oCharacter.standbyTimeType = pDataRow["STANDBY_TIME_TYPE"].ToString();
			oCharacter.startPerformDay = pDataRow["START_PERFORM_DAY"].ToString();
			oCharacter.bulkImpNonUseFlag = int.Parse(pDataRow["BULK_IMP_NON_USE_FLAG"].ToString());
			oCharacter.registIncompleteFlag = (int.Parse(pDataRow["NA_FLAG"].ToString()) == ViCommConst.NaFlag.NO_AUTH) ? 1 : 0;
			oCharacter.userDefineMask = int.Parse(pDataRow["USER_DEFINE_MASK"].ToString());
			oCharacter.pickupMask = long.Parse(pDataRow["PICKUP_MASK"].ToString());
			oCharacter.diaryTitle = pDataRow["DIARY_TITLE"].ToString();

			if (!string.IsNullOrEmpty(oCharacter.siteCd) && !string.IsNullOrEmpty(oCharacter.adCd)) {
				string sAdGroupCd = string.Empty;
				int iGroupType = 0;

				using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
					oSiteAdGroup.GetAdGroupCd(oCharacter.siteCd,oCharacter.adCd,out sAdGroupCd,out iGroupType);
				}

				oCharacter.adGroupCd = sAdGroupCd;
			}

			// Extend
			oCharacter.characterEx.enabledBlogFlag = int.Parse(pDataRow["ENABLED_BLOG_FLAG"].ToString());
			oCharacter.characterEx.buyPointMailRxType = pDataRow["BUY_POINT_MAIL_RX_TYPE"].ToString();
			oCharacter.characterEx.waitingPointLastAddDay = pDataRow["WAITING_POINT_LAST_ADD_DAY"].ToString();
			oCharacter.characterEx.waitingPointLastReleaseDay = pDataRow["WAITING_POINT_LAST_RELEASE_DAY"].ToString();
			oCharacter.characterEx.blogSeq = pDataRow["BLOG_SEQ"].ToString();
			oCharacter.characterEx.blogTitle = pDataRow["BLOG_TITLE"].ToString();
			oCharacter.characterEx.blogDoc = pDataRow["BLOG_DOC"].ToString();
			oCharacter.characterEx.enabledRichinoFlag = int.Parse(pDataRow["ENABLED_RICHINO_FLAG"].ToString());
			oCharacter.characterEx.quickResFlag = int.Parse(pDataRow["QUICK_RES_FLAG"].ToString());
			if (!pDataRow["QUICK_RES_OUT_SCH_TIME"].ToString().Equals("")) {
				DateTime.TryParse(pDataRow["QUICK_RES_OUT_SCH_TIME"].ToString(),out oCharacter.characterEx.quickResOutSchTime);
			}
			if (oCharacter.characterEx.enabledRichinoFlag == 0 && int.Parse(pDataRow["NO_UPDATE_RICHINO_VIEW_FLAG"].ToString()) == 0) {
				oCharacter.characterEx.enabledRichinoFlag = UpdateEnabledRichino(oCharacter.siteCd,sessionWoman.userWoman.userSeq,oCharacter.userCharNo);
			}
			oCharacter.characterEx.siteUseStatus = pDataRow["SITE_USE_STATUS"].ToString();
			oCharacter.characterEx.notReadLikedFlag = int.Parse(pDataRow["NOT_READ_LIKED_FLAG"].ToString());
			oCharacter.characterEx.wantedTargetRefuseFlag = int.Parse(pDataRow["WANTED_TARGET_REFUSE_FLAG"].ToString());
			oCharacter.characterEx.mailRequestRefuseFlag = int.Parse(pDataRow["MAIL_REQUEST_REFUSE_FLAG"].ToString());

			if (oCharacter.userCharNo.Equals(ViCommConst.MAIN_CHAR_NO)) {
				sessionWoman.userWoman.connectType = pDataRow["CONNECT_TYPE"].ToString();
			}

			sessionWoman.categoryList.Clear();
			sessionWoman.categoryList.Add(null);
			for (int i = 1;i <= 3;i++) {
				sessionWoman.categoryList.Add(new ActCategory(oCharacter.siteCd,i));
			}
			
			sessionWoman.userWoman.existDeviceTokenFlag = !string.IsNullOrEmpty(iBridUtil.GetStringValue(pDataRow["DEVICE_TOKEN"])) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;


			using (MailBox oMailLog = new MailBox()) {
				oCharacter.unreadMailCount = (int)(oMailLog.GetMailCount(oCharacter.siteCd,sessionWoman.userWoman.userSeq,oCharacter.userCharNo,ViCommConst.OPERATOR,ViCommConst.MAIL_BOX_USER,true));
			}

			GetAttrValue(oCharacter);

			DataSet ds;
			oCharacter.castPaymentList.Clear();
			//チャージポイント 
			using (TimeOperation oTimeOperation = new TimeOperation()) {
				ds = oTimeOperation.GetAllSum(oCharacter.siteCd,sessionWoman.userWoman.userSeq,oCharacter.userCharNo);
				DataRow dr;
				dr = ds.Tables[0].Rows[0];

				if (dr["PUB_TV_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_PUBLIC,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_PUBLIC,new CastPayment(int.Parse(dr["PUB_TV_TALK_PAY_AMT"].ToString()),int.Parse(dr["PUB_TV_TALK_MIN"].ToString()),int.Parse(dr["PUB_TV_TALK_PAY_POINT"].ToString())));
				}
				if (dr["PRV_TV_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_WSHOT,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_WSHOT,new CastPayment(int.Parse(dr["PRV_TV_TALK_PAY_AMT"].ToString()),int.Parse(dr["PRV_TV_TALK_MIN"].ToString()),int.Parse(dr["PRV_TV_TALK_PAY_POINT"].ToString())));
				}
				if (dr["PRV_VOICE_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_VOICE_WSHOT,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_VOICE_WSHOT,new CastPayment(int.Parse(dr["PRV_VOICE_TALK_PAY_AMT"].ToString()),int.Parse(dr["PRV_VOICE_TALK_MIN"].ToString()),int.Parse(dr["PRV_VOICE_TALK_PAY_POINT"].ToString())));
				}
				if (dr["PUB_VOICE_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_VOICE_PUBLIC,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TALK_VOICE_PUBLIC,new CastPayment(int.Parse(dr["PUB_VOICE_TALK_PAY_AMT"].ToString()),int.Parse(dr["PUB_VOICE_TALK_MIN"].ToString()),int.Parse(dr["PUB_VOICE_TALK_PAY_POINT"].ToString())));
				}
				oCharacter.castPaymentList.Add(ViCommConst.CHARGE_VIEW_LIVE,new CastPayment(0,0,0));//現在データなし 

				if (dr["VIEW_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_VIEW_TALK,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_VIEW_TALK,new CastPayment(int.Parse(dr["VIEW_TALK_PAY_AMT"].ToString()),int.Parse(dr["VIEW_TALK_MIN"].ToString()),int.Parse(dr["VIEW_TALK_PAY_POINT"].ToString())));
				}
				if (dr["VIEW_BROADCAST_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_VIEW_ONLINE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_VIEW_ONLINE,new CastPayment(int.Parse(dr["VIEW_BROADCAST_PAY_AMT"].ToString()),int.Parse(dr["VIEW_BROADCAST_MIN"].ToString()),int.Parse(dr["VIEW_BROADCAST_PAY_POINT"].ToString())));
				}
				oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PLAY_MOVIE,new CastPayment(0,0,0));//現在データなし 

				if (dr["PLAY_PROFILE_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PLAY_PF_VOICE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PLAY_PF_VOICE,new CastPayment(int.Parse(dr["PLAY_PROFILE_PAY_AMT"].ToString()),int.Parse(dr["PLAY_PROFILE_MIN"].ToString()),int.Parse(dr["PLAY_PROFILE_PAY_POINT"].ToString())));
				}
				if (dr["REC_PROFILE_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_REC_PF_VOICE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_REC_PF_VOICE,new CastPayment(int.Parse(dr["REC_PROFILE_PAY_AMT"].ToString()),int.Parse(dr["REC_PROFILE_MIN"].ToString()),int.Parse(dr["REC_PROFILE_PAY_POINT"].ToString())));
				}
				if (dr["WIRETAP_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_WIRETAPPING,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_WIRETAPPING,new CastPayment(int.Parse(dr["WIRETAP_PAY_AMT"].ToString()),int.Parse(dr["WIRETAP_MIN"].ToString()),int.Parse(dr["WIRETAP_PAY_POINT"].ToString())));
				}
				if (dr["GPF_TALK_VOICE_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_GPF_TALK_VOICE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_GPF_TALK_VOICE,new CastPayment(int.Parse(dr["GPF_TALK_VOICE_PAY_AMT"].ToString()),int.Parse(dr["GPF_TALK_VOICE_MIN"].ToString()),int.Parse(dr["GPF_TALK_VOICE_PAY_POINT"].ToString())));
				}
				if (dr["PLAY_PV_MSG_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PLAY_PV_MSG,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PLAY_PV_MSG,new CastPayment(int.Parse(dr["PLAY_PV_MSG_PAY_AMT"].ToString()),int.Parse(dr["PLAY_PV_MSG_MIN"].ToString()),int.Parse(dr["PLAY_PV_MSG_PAY_POINT"].ToString())));
				}
				if (dr["USER_MAIL_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_RX_MAIL,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_RX_MAIL,new CastPayment(int.Parse(dr["USER_MAIL_PAY_AMT"].ToString()),int.Parse(dr["USER_MAIL_COUNT"].ToString()),int.Parse(dr["USER_MAIL_PAY_POINT"].ToString())));
				}
				if (dr["OPEN_PIC_MAIL_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_MAIL_WITH_PIC,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_MAIL_WITH_PIC,new CastPayment(int.Parse(dr["OPEN_PIC_MAIL_PAY_AMT"].ToString()),int.Parse(dr["OPEN_PIC_MAIL_COUNT"].ToString()),int.Parse(dr["OPEN_PIC_MAIL_PAY_POINT"].ToString())));
				}
				if (dr["OPEN_MOVIE_MAIL_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_MAIL_WITH_MOVIE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_MAIL_WITH_MOVIE,new CastPayment(int.Parse(dr["OPEN_MOVIE_MAIL_PAY_AMT"].ToString()),int.Parse(dr["OPEN_MOVIE_MAIL_COUNT"].ToString()),int.Parse(dr["OPEN_MOVIE_MAIL_PAY_POINT"].ToString())));
				}

				if (dr["OPEN_PIC_BBS_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_BBS_PIC,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_BBS_PIC,new CastPayment(int.Parse(dr["OPEN_PIC_BBS_PAY_AMT"].ToString()),int.Parse(dr["OPEN_PIC_BBS_COUNT"].ToString()),int.Parse(dr["OPEN_PIC_BBS_PAY_POINT"].ToString())));
				}
				if (dr["OPEN_MOVIE_BBS_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_BBS_MOVIE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_BBS_MOVIE,new CastPayment(int.Parse(dr["OPEN_MOVIE_BBS_PAY_AMT"].ToString()),int.Parse(dr["OPEN_MOVIE_BBS_COUNT"].ToString()),int.Parse(dr["OPEN_MOVIE_BBS_PAY_POINT"].ToString())));
				}
				if (dr["MOVIE_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_DOWNLOAD_PF_MOVIE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_DOWNLOAD_PF_MOVIE,new CastPayment(int.Parse(dr["MOVIE_PAY_AMT"].ToString()),int.Parse(dr["MOVIE_MIN"].ToString()),int.Parse(dr["MOVIE_PAY_POINT"].ToString())));
				}
				if (dr["USER_MAIL_PIC_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_RX_MAIL_WITH_PIC,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_RX_MAIL_WITH_PIC,new CastPayment(int.Parse(dr["USER_MAIL_PIC_PAY_AMT"].ToString()),int.Parse(dr["USER_MAIL_PIC_COUNT"].ToString()),int.Parse(dr["USER_MAIL_PIC_PAY_POINT"].ToString())));
				}
				if (dr["USER_MAIL_MOVIE_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_RX_MAIL_WITH_MOVIE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_RX_MAIL_WITH_MOVIE,new CastPayment(int.Parse(dr["USER_MAIL_MOVIE_PAY_AMT"].ToString()),int.Parse(dr["USER_MAIL_MOVIE_COUNT"].ToString()),int.Parse(dr["USER_MAIL_MOVIE_PAY_POINT"].ToString())));
				}
				if (dr["FRIEND_INTRO_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_FRIEND_INTRO,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_FRIEND_INTRO,new CastPayment(int.Parse(dr["FRIEND_INTRO_PAY_AMT"].ToString()),int.Parse(dr["FRIEND_INTRO_COUNT"].ToString()),int.Parse(dr["FRIEND_INTRO_PAY_POINT"].ToString())));
				}
				if (dr["PRV_MESSAGE_RX_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PRV_MESSAGE_RX,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PRV_MESSAGE_RX,new CastPayment(int.Parse(dr["PRV_MESSAGE_RX_PAY_AMT"].ToString()),int.Parse(dr["PRV_MESSAGE_RX_COUNT"].ToString()),int.Parse(dr["PRV_MESSAGE_RX_PAY_POINT"].ToString())));
				}
				if (dr["PRV_PROFILE_RX_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PRV_PROFILE_RX,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PRV_PROFILE_RX,new CastPayment(int.Parse(dr["PRV_PROFILE_RX_PAY_AMT"].ToString()),int.Parse(dr["PRV_PROFILE_RX_COUNT"].ToString()),int.Parse(dr["PRV_PROFILE_RX_PAY_POINT"].ToString())));
				}

				if (dr["DOWNLOAD_MOVIE_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_DOWNLOAD_MOVIE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_DOWNLOAD_MOVIE,new CastPayment(int.Parse(dr["DOWNLOAD_MOVIE_PAY_AMT"].ToString()),int.Parse(dr["DOWNLOAD_MOVIE_COUNT"].ToString()),int.Parse(dr["DOWNLOAD_MOVIE_PAY_POINT"].ToString())));
				}

				if (dr["TIME_CALL_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TIME_CALL,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TIME_CALL,new CastPayment(int.Parse(dr["TIME_CALL_PAY_AMT"].ToString()),int.Parse(dr["TIME_CALL_COUNT"].ToString()),int.Parse(dr["TIME_CALL_PAY_POINT"].ToString())));
				}

				if (dr["DOWNLOAD_VOICE_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_DOWNLOAD_VOICE,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_DOWNLOAD_VOICE,new CastPayment(int.Parse(dr["DOWNLOAD_VOICE_PAY_AMT"].ToString()),int.Parse(dr["DOWNLOAD_VOICE_COUNT"].ToString()),int.Parse(dr["DOWNLOAD_VOICE_PAY_POINT"].ToString())));
				}

				if (dr["SOCIAL_GAME_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_SOCIAL_GAME,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_SOCIAL_GAME,new CastPayment(int.Parse(dr["SOCIAL_GAME_PAY_AMT"].ToString()),int.Parse(dr["SOCIAL_GAME_COUNT"].ToString()),int.Parse(dr["SOCIAL_GAME_PAY_POINT"].ToString())));
				}

				if (dr["YAKYUKEN_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_YAKYUKEN,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_YAKYUKEN,new CastPayment(int.Parse(dr["YAKYUKEN_PAY_AMT"].ToString()),int.Parse(dr["YAKYUKEN_COUNT"].ToString()),int.Parse(dr["YAKYUKEN_PAY_POINT"].ToString())));
				}

				if (dr["PRESENT_MAIL_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PRESENT_MAIL,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PRESENT_MAIL,new CastPayment(int.Parse(dr["PRESENT_MAIL_PAY_AMT"].ToString()),int.Parse(dr["PRESENT_MAIL_COUNT"].ToString()),int.Parse(dr["PRESENT_MAIL_PAY_POINT"].ToString())));
				}

				oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TX_MAIL,new CastPayment(0,0,0));				//現在データなし 
				oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TX_MAIL_WITH_PIC,new CastPayment(0,0,0));		//現在データなし 
				oCharacter.castPaymentList.Add(ViCommConst.CHARGE_TX_MAIL_WITH_MOVIE,new CastPayment(0,0,0));		//現在データなし 
				oCharacter.castPaymentList.Add(ViCommConst.CHARGE_PLAY_ROOM_SHOW,new CastPayment(0,0,0));			//現在データなし 
				oCharacter.castPaymentList.Add(ViCommConst.CHARGE_REC_PF_MOVIE,new CastPayment(0,0,0));			//現在データなし  

				if (dr["CAST_PUB_TV_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_PUBLIC,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_PUBLIC,new CastPayment(int.Parse(dr["CAST_PUB_TV_TALK_PAY_AMT"].ToString()),int.Parse(dr["CAST_PUB_TV_TALK_MIN"].ToString()),int.Parse(dr["CAST_PUB_TV_TALK_PAY_POINT"].ToString())));
				}
				if (dr["CAST_PRV_TV_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_WSHOT,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_WSHOT,new CastPayment(int.Parse(dr["CAST_PRV_TV_TALK_PAY_AMT"].ToString()),int.Parse(dr["CAST_PRV_TV_TALK_MIN"].ToString()),int.Parse(dr["CAST_PRV_TV_TALK_PAY_POINT"].ToString())));
				}
				if (dr["CAST_PRV_VOICE_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT,new CastPayment(int.Parse(dr["CAST_PRV_VOICE_TALK_PAY_AMT"].ToString()),int.Parse(dr["CAST_PRV_VOICE_TALK_MIN"].ToString()),int.Parse(dr["CAST_PRV_VOICE_TALK_PAY_POINT"].ToString())));
				}
				if (dr["CAST_PUB_VOICE_TALK_PAY_AMT"].ToString() == "") {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC,new CastPayment(0,0,0));
				} else {
					oCharacter.castPaymentList.Add(ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC,new CastPayment(int.Parse(dr["CAST_PUB_VOICE_TALK_PAY_AMT"].ToString()),int.Parse(dr["CAST_PUB_VOICE_TALK_MIN"].ToString()),int.Parse(dr["CAST_PUB_VOICE_TALK_PAY_POINT"].ToString())));
				}
			}
			using (BonusLog oBonusLog = new BonusLog()) {
				bonusAmtList.Clear();
				bonusPointList.Clear();
				int iAdminAmt,iAdminPoint,iFavoritAmt,iFavoritPoint,iRefuseAmt,iRefusePoint,iBlogAmt,iBlogPoint,iOmikujiPoint,iOmikujiAmt,iFanClubAmt,iFanClubPoint;
				oBonusLog.GetPaymentSum(sessionWoman.userWoman.userSeq,ViCommConst.BONUS_TYPE_ADMIN,out iAdminAmt,out iAdminPoint);
				oBonusLog.GetPaymentSum(sessionWoman.userWoman.userSeq,ViCommConst.BONUS_TYPE_FAVORIT,out iFavoritAmt,out iFavoritPoint);
				oBonusLog.GetPaymentSum(sessionWoman.userWoman.userSeq,ViCommConst.BONUS_TYPE_REFUSE,out iRefuseAmt,out iRefusePoint);
				oBonusLog.GetPaymentSum(sessionWoman.userWoman.userSeq,ViCommConst.BONUS_TYPE_BLOG,out iBlogAmt,out iBlogPoint);
				oBonusLog.GetPaymentSum(sessionWoman.userWoman.userSeq,ViCommConst.BONUS_TYPE_OMIKUJI,out iOmikujiAmt,out iOmikujiPoint);
				oBonusLog.GetPaymentSum(sessionWoman.userWoman.userSeq,ViCommConst.BONUS_TYPE_FANCLUB,out iFanClubAmt,out iFanClubPoint);
				bonusAmtList.Add(ViCommConst.BONUS_TYPE_ADMIN,iAdminAmt);
				bonusAmtList.Add(ViCommConst.BONUS_TYPE_FAVORIT,iFavoritAmt);
				bonusAmtList.Add(ViCommConst.BONUS_TYPE_REFUSE,iRefuseAmt);
				bonusAmtList.Add(ViCommConst.BONUS_TYPE_BLOG,iBlogAmt);
				bonusAmtList.Add(ViCommConst.BONUS_TYPE_OMIKUJI,iOmikujiAmt);
				bonusAmtList.Add(ViCommConst.BONUS_TYPE_FANCLUB,iFanClubAmt);
				bonusPointList.Add(ViCommConst.BONUS_TYPE_ADMIN,iAdminPoint);
				bonusPointList.Add(ViCommConst.BONUS_TYPE_FAVORIT,iFavoritPoint);
				bonusPointList.Add(ViCommConst.BONUS_TYPE_REFUSE,iRefusePoint);
				bonusPointList.Add(ViCommConst.BONUS_TYPE_BLOG,iBlogPoint);
				bonusPointList.Add(ViCommConst.BONUS_TYPE_OMIKUJI,iOmikujiPoint);
				bonusPointList.Add(ViCommConst.BONUS_TYPE_FANCLUB,iFanClubPoint);
			}
			using (TimeOperation oTimeOperation = new TimeOperation()) {
				oTimeOperation.GetTotalPayment(string.Empty,sessionWoman.userWoman.userSeq,out sessionWoman.userWoman.totalPaymentAmt,out sessionWoman.userWoman.totalPaymentPoint);
			}
		}
	}

	private bool GetAttrValue(UserWomanCharacter pCharacter) {
		DataSet ds;
		UserWomanAttr attr;
		pCharacter.attrList.Clear();

		using (CastAttrType oAttrType = new CastAttrType()) {
			ds = oAttrType.GetList(pCharacter.siteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				attr = new UserWomanAttr();
				attr.attrTypeSeq = dr["CAST_ATTR_TYPE_SEQ"].ToString();
				attr.attrTypeNm = dr["CAST_ATTR_TYPE_NM"].ToString();
				attr.inputType = dr["INPUT_TYPE"].ToString();
				attr.proirity = dr["PRIORITY"].ToString();
				attr.rowCount = dr["ROW_COUNT"].ToString();
				attr.itemNo = dr["ITEM_NO"].ToString();
				attr.profileReqItemFlag = dr["PROFILE_REQ_ITEM_FLAG"].ToString().Equals("1");
				pCharacter.attrList.Add(attr);
			}
		}

		UserWomanAttr oWomanAttr;
		using (CastAttrValue oAttrValue = new CastAttrValue()) {
			ds = oAttrValue.GetList(pCharacter.siteCd,userSeq,pCharacter.userCharNo);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				for (int i = 0;i < pCharacter.attrList.Count;i++) {
					oWomanAttr = pCharacter.attrList[i];
					if (oWomanAttr.attrTypeSeq.Equals(dr["CAST_ATTR_TYPE_SEQ"].ToString())) {
						oWomanAttr.attrImputValue = dr["CAST_ATTR_INPUT_VALUE"].ToString();
						oWomanAttr.attrSeq = dr["CAST_ATTR_SEQ"].ToString();
						oWomanAttr.attrNm = dr["CAST_ATTR_NM"].ToString();
						oWomanAttr.itemCd = dr["ITEM_CD"].ToString();
					}
				}
			}
		}
		return (ds.Tables[0].Rows.Count > 0);
	}

	public void StartWaitting(DateTime pWaittingDate,string pConnectType,bool pMonitorEnableFlag,string pMobileTerminalNm,int pDummyTalkFlag,string pWaitingComment,string pChangeRank,string pStandByTimeType,bool pAdminFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("START_WAITTING");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PWAITTING_DATE",DbSession.DbType.DATE,pWaittingDate);
			db.ProcedureInParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2,pConnectType);
			db.ProcedureInParm("PMONITOR_ENABLE_FLAG",DbSession.DbType.NUMBER,pMonitorEnableFlag);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureInParm("PDUMMY_TALK_FLAG",DbSession.DbType.NUMBER,pDummyTalkFlag);
			db.ProcedureInParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2,pWaitingComment);
			db.ProcedureInParm("PCHANGE_RANK",DbSession.DbType.VARCHAR2,pChangeRank);
			db.ProcedureInParm("PSTANDBY_TIME_TYPE",DbSession.DbType.VARCHAR2,pStandByTimeType);
			db.ProcedureInParm("PADMIN_FLAG",DbSession.DbType.VARCHAR2,pAdminFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			db.ProcedureInParm("PLOGIN_BY_TEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PLOGIN_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateWaitingComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pWaitingComment) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_WAITING_COMMENT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2,pWaitingComment);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void ModifyUser(
		string pUserSeq,
		string pUserCharNo,
		string pHandleNm,
		string pTel,
		string pCommentList,
		string pCommentDetail,
		Int64 pOkPlayList,
		string pBirthday,
		string[] pAttrTypeSeq,
		string[] pAttrSeq,
		string[] pAttrInputValue,
		int pMaxAttrCount,
		out string pResult
	) {
		SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];
		string sCommentList = "";
		string sCommentDetail = "";
		using (DbSession db = new DbSession()) {
			string sHandleNm = Mobile.EmojiToCommTag(userObjs.carrier,pHandleNm);
			if (pCommentList != null) {
				sCommentList = Mobile.EmojiToCommTag(userObjs.carrier,pCommentList);
			}
			if (pCommentDetail != null) {
				sCommentDetail = Mobile.EmojiToCommTag(userObjs.carrier,pCommentDetail);
			}
			string[] sAttrInputValue = new string[pMaxAttrCount];

			for (int i = 0;i < pMaxAttrCount;i++) {
				if (pAttrInputValue[i] != null) {
					sAttrInputValue[i] = Mobile.EmojiToCommTag(userObjs.carrier,pAttrInputValue[i]);
				}
			}
			db.PrepareProcedure("MODIFY_USER_WOMAN");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sHandleNm);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2,sCommentList);
			db.ProcedureInParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2,sCommentDetail);
			db.ProcedureInParm("POK_PLAY_LIST",DbSession.DbType.NUMBER,pOkPlayList);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,userObjs.carrier);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,userObjs.mobileUserAgent);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,userObjs.currentIModeId);
			db.ProcedureInArrayParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrTypeSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,pMaxAttrCount,sAttrInputValue);
			db.ProcedureInParm("PMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,pMaxAttrCount);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("PRESULT");

		}
		GetOne(loginId,loginPassword);
		UpdateCharacterInfo(userObjs.site.siteCd);
	}

	public string GetOkPlayList(string pSiteCd,Int64 pOkPlayMask) {
		string sList = "";
		string sSql = "SELECT OK_PLAY,OK_PLAY_NM FROM T_OK_PLAY_LIST " +
					"WHERE " +
						"SITE_CD	= :SITE_CD " +
					"ORDER BY SITE_CD,OK_PLAY";
		try {
			conn = DbConnect();
			using (DataSet ds = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(ds,"T_OK_PLAY_LIST");
					}
				}
				Int64 iMask;
				foreach (DataRow dr in ds.Tables[0].Rows) {
					iMask = Int64.Parse(dr["OK_PLAY"].ToString());
					if ((iMask & pOkPlayMask) != 0) {
						sList = sList + dr["OK_PLAY_NM"].ToString() + ",";
					}
				}
			}
		} finally {
			conn.Close();
		}
		if (sList.Length != 0) {
			sList = sList.Substring(0,sList.Length - 1);
		}
		return sList;
	}

	public void SetupRxMail(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pManMailRxType,
		string pInfoMailRxType,
		int pTalkEndMailRxFlag,
		string pMobileMailRxStartTime,
		string pMobileMailRxEndTime,
		int pClearMailAddrNgFlag
	) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_CAST_RX_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PMAN_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pManMailRxType);
			db.ProcedureInParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pInfoMailRxType);
			db.ProcedureInParm("PTALK_END_MAIL_RX_FLAG",DbSession.DbType.NUMBER,pTalkEndMailRxFlag);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxStartTime);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxEndTime);
			db.ProcedureInParm("PCLEAR_MAIL_ADDR_NG_FLAG",DbSession.DbType.NUMBER,pClearMailAddrNgFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		UpdateCharacterInfo(pSiteCd);
	}

	public void SetupRxMailDouble(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pManMailRxType,
		string pInfoMailRxType,
		int pTalkEndMailRxFlag,
		int pMobileMailRxTimeUseFlag,
		string pMobileMailRxStartTime,
		string pMobileMailRxEndTime,
		int pMobileMailRxTimeUseFlag2,
		string pMobileMailRxStartTime2,
		string pMobileMailRxEndTime2,
		int pClearMailAddrNgFlag
	) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_CAST_RX_MAIL_DOUBLE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PMAN_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pManMailRxType);
			db.ProcedureInParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pInfoMailRxType);
			db.ProcedureInParm("PTALK_END_MAIL_RX_FLAG",DbSession.DbType.NUMBER,pTalkEndMailRxFlag);
			db.ProcedureInParm("PMOBILE_MAIL_RX_TIME_USE_FLAG",DbSession.DbType.NUMBER,pMobileMailRxTimeUseFlag);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxStartTime);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2,pMobileMailRxEndTime);
			db.ProcedureInParm("PMOBILE_MAIL_RX_TIME_USE_FLAG2",DbSession.DbType.NUMBER,pMobileMailRxTimeUseFlag2);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME2",DbSession.DbType.VARCHAR2,pMobileMailRxStartTime2);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME2",DbSession.DbType.VARCHAR2,pMobileMailRxEndTime2);
			db.ProcedureInParm("PCLEAR_MAIL_ADDR_NG_FLAG",DbSession.DbType.NUMBER,pClearMailAddrNgFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		UpdateCharacterInfo(pSiteCd);
	}

	public void SetupRxMailEx(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pBuPointMailRxType
	) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_CAST_RX_MAIL_EX");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PBUF_POINT_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,pBuPointMailRxType);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		UpdateCharacterInfo(pSiteCd);
	}

	public void UpdateProfilePicture(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPicSeq,
		int pDelFlag,
		int pProfilePicFlag,
		string pPicType,
		int pNotApproveFlag,
		int pNotPublishFlag,
		string pPicTitle,
		string pPicDoc,
		string pCastPicAttrTypeSeq,
		string pCastPicAttrSeq,
		int? pObjRankingFlag
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("pPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("pPIC_TYPE",DbSession.DbType.VARCHAR2,pPicType);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,pNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,pNotPublishFlag);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,pPicTitle);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,pPicDoc);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pCastPicAttrTypeSeq);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,pCastPicAttrSeq);
			db.ProcedureInParm("pOBJ_RANKING_FLAG",DbSession.DbType.NUMBER,pObjRankingFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
		UpdateCharacterInfo(pSiteCd);
	}

	public void ModifyBank(
		string pUserSeq,
		string pBankNm,
		string pBankCd,
		string pBankOfficeNm,
		string pBankOfficeKanaNm,
		string pBankOfficeCd,
		string pBankAccountType,
		string pBankAccountType2,
		string pBankAccountHolderNm,
		string pBankAccountNo
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_USER_WOMAN_BANK");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PBANK_NM",DbSession.DbType.VARCHAR2,pBankNm);
			db.ProcedureInParm("PBANK_CD",DbSession.DbType.VARCHAR2,pBankCd);
			db.ProcedureInParm("PBANK_OFFICE_NM",DbSession.DbType.VARCHAR2,pBankOfficeNm);
			db.ProcedureInParm("PBANK_OFFICE_KANA_NM",DbSession.DbType.VARCHAR2,pBankOfficeKanaNm);
			db.ProcedureInParm("PBANK_OFFICE_CD",DbSession.DbType.VARCHAR2,pBankOfficeCd);
			db.ProcedureInParm("PBANK_ACCOUNT_TYPE",DbSession.DbType.VARCHAR2,pBankAccountType);
			db.ProcedureInParm("PBANK_ACCOUNT_TYPE2",DbSession.DbType.VARCHAR2,pBankAccountType2);
			db.ProcedureInParm("PBANK_ACCOUNT_HOLDER_NM",DbSession.DbType.VARCHAR2,pBankAccountHolderNm);
			db.ProcedureInParm("PBANK_ACCOUNT_NO",DbSession.DbType.VARCHAR2,pBankAccountNo);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetOne(loginId,loginPassword);
	}

	public void SetLastActionDate(
		string pUserSeq
	) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_LAST_ACTION_DATE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,String.Empty);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,String.Empty);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void SetLastLoginDate(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pGuid
	) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_LAST_LOGIN_DATE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pGUID",DbSession.DbType.VARCHAR2,pGuid);
			db.ProcedureInParm("pGCAPP_VERSION",DbSession.DbType.VARCHAR2,SessionObjs.Current.gcappVersion);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void SetupTermId(
		string pUserSeq,
		string pTermId,
		string pImodeId
	) {
		if (SessionObjs.Current.adminFlg)
			return;
		SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_TERM_ID");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTermId);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,pImodeId);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetOne(loginId,loginPassword);
	}

	public bool UsedTermId(
		string pTermId,
		string pImodeId,
		out string pLoginId,
		out string pPassword
	) {
		DataSet ds;
		DataRow dr;
		pLoginId = "";
		pPassword = "";

		bool bExist = false;
		try {
			conn = DbConnect("UserWoman.UsedTermId");

			string sSql = "SELECT " +
							"LOGIN_ID			," +
							"LOGIN_PASSWORD		" +
						"FROM " +
							"T_USER ";

			string sWhere = "";
			string sOrder = " ORDER BY USER_STATUS ";
			ArrayList list = new ArrayList();
			if (!pTermId.Equals("")) {
				SysPrograms.SqlAppendWhere("TERMINAL_UNIQUE_ID = :TERMINAL_UNIQUE_ID",ref sWhere);
				list.Add(new OracleParameter("TERMINAL_UNIQUE_ID",pTermId));
			}
			if (!pImodeId.Equals("")) {
				SysPrograms.SqlAppendWhere("IMODE_ID = :IMODE_ID",ref sWhere);
				list.Add(new OracleParameter("IMODE_ID",pImodeId));
			}
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref sWhere);
			list.Add(new OracleParameter("SEX_CD",ViCommConst.OPERATOR));

			SysPrograms.SqlAppendWhere("NOT_USED_EASY_LOGIN = :NOT_USED_EASY_LOGIN",ref sWhere);
			list.Add(new OracleParameter("NOT_USED_EASY_LOGIN",ViCommConst.FLAG_OFF));

			OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
			sSql += sWhere + sOrder;
			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count > 0) {
						dr = ds.Tables["T_USER"].Rows[0];
						pLoginId = dr["LOGIN_ID"].ToString();
						pPassword = dr["LOGIN_PASSWORD"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void UpdateBatchMailCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		int pMailCount
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_BATCH_MAIL_COUNT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PMAIL_COUNT",DbSession.DbType.NUMBER,pMailCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		UpdateCharacterInfo(pSiteCd);
	}

	public void UpdateInviteMailCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		int pMailCount
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_INVITE_MAIL_COUNT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PMAIL_COUNT",DbSession.DbType.NUMBER,pMailCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		UpdateCharacterInfo(pSiteCd);
	}

	public void SetupFreeDial(
		string pUserSeq,
		bool pUseFreeDialFalg
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_FREE_DIAL");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSE_FREE_DIAL_FLAG",DbSession.DbType.NUMBER,pUseFreeDialFalg);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		if (pUseFreeDialFalg) {
			useFreeDialFlag = 1;
		} else {
			useFreeDialFlag = 0;
		}
	}

	public void SetupCrosmile(
		string pUserSeq,
		bool pUseCrosmileFalg
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_CROSMILE");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSE_CROSMILE_FLAG",DbSession.DbType.NUMBER,pUseCrosmileFalg);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		if (pUseCrosmileFalg) {
			useCrosmileFlag = ViCommConst.FLAG_ON;
		} else {
			useCrosmileFlag = ViCommConst.FLAG_OFF;
		}
	}

	public string SetupVoiceapp(string pUserSeq,int pUseVoiceappFlag) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_VOICEAPP");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,int.Parse(pUserSeq));
			db.ProcedureInParm("pUSE_VOICEAPP_FLAG",DbSession.DbType.NUMBER,pUseVoiceappFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		if (sResult.Equals("0")) {
			useVoiceappFlag = pUseVoiceappFlag;
		}

		return sResult;
	}

	public bool IsTelephoneRegistered(string pTelephoneNo) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect("UseWoman.GetOneByLoginId");
			ds = new DataSet();

			string sSql = "SELECT TEL,USER_STATUS " +
							"FROM " +
								"T_USER " +
							"WHERE " +
								"TEL = :TEL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("TEL",pTelephoneNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
				}
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_HOLD)) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void ResignedReturn(
	string pLoginMode,
	string pLoginId,
	string pLoginPassword,
	out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RESIGNED_RETURN_WOMAN");
			db.ProcedureInParm("PLOGIN_MODE",DbSession.DbType.VARCHAR2,pLoginMode);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,pLoginId);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}
	}

	public void WithdrawalMainte(ref string pWithDrawalSeq,string pReason) {
		string userSeq = string.Empty;
		string sReasonStatus = string.Empty;
		string sReasonCd = string.Empty;
		string sReasonDoc = string.Empty;

		SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];
		userSeq = Convert.ToString(userObjs.userWoman.userSeq);

		if (userSeq == string.Empty) {
			return;
		}
		if (pWithDrawalSeq.Equals(string.Empty)) {
			sReasonStatus = ViCommConst.WITHDRAWAL_STATUS_CHOICE;
			sReasonCd = pReason;
		} else {
			sReasonStatus = ViCommConst.WITHDRAWAL_STATUS_ACCEPT;
			sReasonDoc = pReason;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WITHDRAWAL_MAINTE");
			db.ProcedureBothParm("PWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithDrawalSeq);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("PWITHDRAWAL_STATUS",DbSession.DbType.VARCHAR2,sReasonStatus);
			db.ProcedureInParm("PWITHDRAWAL_REASON_CD",DbSession.DbType.VARCHAR2,sReasonCd);
			db.ProcedureInParm("PWITHDRAWAL_REASON_DOC",DbSession.DbType.VARCHAR2,sReasonDoc);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pWithDrawalSeq = db.GetStringValue("PWITHDRAWAL_SEQ");
		}
	}

	public void SecessionUser(out string pResult) {
		string userSeq = string.Empty;
		SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];
		userSeq = Convert.ToString(userObjs.userWoman.userSeq);

		if (userSeq == string.Empty) {
			pResult = "";
			return;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SECESSION_USER_WOMAN");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}
	}
	public void PaymentApplication(
		string pUserSeq
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PAYMENT_APPLICATION");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetOne(loginId,loginPassword);
	}

	public void ModifyUserTel(string pUserSeq,string pTel,string pSiteCd,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_USER_TEL");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}
		GetOne(loginId,loginPassword);
	}

	public void ModifyUserHandleNm(string pUserSeq,string pUserCharNo,string pHandleNm,out string pResult) {
		SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];
		using (DbSession db = new DbSession()) {
			string sHandleNm = Mobile.EmojiToCommTag(userObjs.carrier,pHandleNm);

			db.PrepareProcedure("MODIFY_USER_WOMAN_HANDLE_NM");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sHandleNm);
			db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,userObjs.carrier);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,userObjs.mobileUserAgent);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,userObjs.currentIModeId);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");
		}

		UpdateCharacterInfo(userObjs.site.siteCd);
	}

	public bool CheckCastHandleNmDupli(string sSiteCd,string pUserSeq,string pUserCharNo,string pHandleNm) {
		bool bDupliFlag = false;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_CAST_HANDLE_NM_DUPLI");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureOutParm("pDUPLI_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			bDupliFlag = iBridUtil.GetStringValue(db.GetStringValue("pDUPLI_FLAG")).Equals(ViCommConst.FLAG_ON_STR);
		}

		return bDupliFlag;
	}

	public int UpdateEnabledRichino(string sSiteCd,string pUserSeq,string pUserCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_ENABLED_RICHINO");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("PENABLED_RICHINO_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			return db.GetIntValue("PENABLED_RICHINO_FLAG");
		}
	}


	public void UpdateDiaryTitle(string pSiteCd,string pUserSeq,string pUserCharNo,string pDiaryTitle,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_DIARY_TITLE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PDIARY_TITLE",DbSession.DbType.VARCHAR2,pDiaryTitle);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("PRESULT");

			UpdateCharacterInfo(pSiteCd);
		}
	}

	public void ModifyCastCommentDetail(string pSiteCd,string pUserSeq,string pUserCharNo,string pCommentDetail,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_CAST_COMMENT_DETAIL");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCOMMENT_DETAIL",DbSession.DbType.VARCHAR2,pCommentDetail);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}

		UpdateCharacterInfo(pSiteCd);
	}

	public void RegistWantedTargetRefuse(string pSiteCd,string pUserSeq,string pUserCharNo,int pWantedTargetRefuseFlag,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_WANTED_TARGET_REFUSE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pWANTED_TARGET_REFUSE_FLAG",DbSession.DbType.NUMBER,pWantedTargetRefuseFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void RegistMailRequestRefuse(string pSiteCd,string pUserSeq,string pUserCharNo,int pMailRequestRefuseFlag,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_MAIL_REQUEST_REFUSE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMAIL_REQUEST_REFUSE_FLAG",DbSession.DbType.NUMBER,pMailRequestRefuseFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	/// <summary>
	/// 退会申請レコードを手動対応に変更
	/// </summary>
	/// <param name="pWithDrawalSeq"></param>
	public void UpdateManualWithdrawal(string pWithDrawalSeq) {
		string userSeq = string.Empty;

		SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];
		userSeq = Convert.ToString(userObjs.userWoman.userSeq);

		if (userSeq.Equals(string.Empty)) {
			return;
		}
		if (pWithDrawalSeq.Equals(string.Empty)) {
			return;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MANUAL_WITHDRAWAL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,userSeq);
			db.ProcedureInParm("PWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithDrawalSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}


