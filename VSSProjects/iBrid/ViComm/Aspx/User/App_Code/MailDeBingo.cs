﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeビンゴ設定
--	Progaram ID		: MailDeBingo
--
--  Creation Date	: 2011.07.06
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System;
using System.Text;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class MailDeBingo:DbSession {
    public MailDeBingo() {
    }

	public DataSet GetCurrentOpeningInfo(string pSiteCd, string pSexCd, DateTime pTargetTime)
	{
        DataSet ds = new DataSet();
        StringBuilder oBuilder = new StringBuilder();

		oBuilder.Append("SELECT																	 ").AppendLine();
		oBuilder.Append("   P1.CNT										AS	BINGO_NEXT_COUNT	,").AppendLine();
		oBuilder.Append("	CASE																 ").AppendLine();
		oBuilder.Append("		WHEN															 ").AppendLine();
		oBuilder.Append("			EXISTS(														 ").AppendLine();
		oBuilder.Append("				SELECT													 ").AppendLine();
		oBuilder.Append("					*													 ").AppendLine();
		oBuilder.Append("				FROM													 ").AppendLine();
		oBuilder.Append("					T_MAIL_DE_BINGO										 ").AppendLine();
		oBuilder.Append("				WHERE													 ").AppendLine();
		oBuilder.Append("					SITE_CD				 = :SITE_CD				AND		 ").AppendLine();
		oBuilder.Append("					SEX_CD				 = :SEX_CD				AND		 ").AppendLine();
		oBuilder.Append("					BINGO_START_DATE	<= :BINGO_TARGET_DATE	AND		 ").AppendLine();
		oBuilder.Append("					BINGO_END_DATE		>= :BINGO_TARGET_DATE			 ").AppendLine();
		oBuilder.Append("			)															 ").AppendLine();
		oBuilder.Append("		THEN															 ").AppendLine();
		oBuilder.Append("			1															 ").AppendLine();
		oBuilder.Append("		ELSE															 ").AppendLine();
		oBuilder.Append("			0															 ").AppendLine();
		oBuilder.Append("	END											AS	IS_OPENING			 ").AppendLine();
		oBuilder.Append("FROM																	 ").AppendLine();
		oBuilder.Append("	(																	 ").AppendLine();
		oBuilder.Append("	SELECT																 ").AppendLine();
		oBuilder.Append("		COUNT(*)								AS	CNT					 ").AppendLine();
		oBuilder.Append("	FROM																 ").AppendLine();
		oBuilder.Append("		T_MAIL_DE_BINGO													 ").AppendLine();
		oBuilder.Append("	WHERE																 ").AppendLine();
		oBuilder.Append("		SITE_CD				 = :SITE_CD 		               AND		 ").AppendLine();
		oBuilder.Append("		SEX_CD				 = :SEX_CD			               AND		 ").AppendLine();
		oBuilder.Append("		BINGO_START_DATE	 > :BINGO_TARGET_DATE						 ").AppendLine();
		oBuilder.Append(") P1																	 ").AppendLine();


        try {
            conn = DbConnect("MailDeBingo.GetOpeningInfo");
            using (cmd = CreateSelectCommand(oBuilder.ToString(), conn)) {
                cmd.BindByName = true;
                cmd.Parameters.Add(":SITE_CD", pSiteCd);
                cmd.Parameters.Add(":SEX_CD", pSexCd);
				cmd.Parameters.Add(":BINGO_TARGET_DATE", OracleDbType.Date, pTargetTime, ParameterDirection.Input);

                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(ds);
                }
            
            }
        } finally {
            conn.Close();
        }

        return ds;
    }

	public string GetCurrentJackpot(string pSiteCd, string pSexCd, DateTime pTargetTime) {

		StringBuilder oBuilder = new StringBuilder();
		string sJackpot = string.Empty;

		oBuilder.Append("SELECT													 ").AppendLine();
		oBuilder.Append("	JACKPOT												 ").AppendLine();
		oBuilder.Append("FROM													 ").AppendLine();
		oBuilder.Append("	T_MAIL_DE_BINGO										 ").AppendLine();
		oBuilder.Append("WHERE													 ").AppendLine();
		oBuilder.Append("	SITE_CD				 = :SITE_CD				AND		 ").AppendLine();
		oBuilder.Append("	SEX_CD				 = :SEX_CD				AND		 ").AppendLine();
		oBuilder.Append("	BINGO_START_DATE	<= :BINGO_TARGET_DATE	AND		 ").AppendLine();
		oBuilder.Append("	BINGO_END_DATE		>= :BINGO_TARGET_DATE			 ").AppendLine();

		try {
			DataSet ds = new DataSet();
			conn = DbConnect("MailDeBingo.GetCurrentJackpot");
			using (cmd = CreateSelectCommand(oBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":SEX_CD", pSexCd);
				cmd.Parameters.Add(":BINGO_TARGET_DATE", OracleDbType.Date, pTargetTime, ParameterDirection.Input);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count > 0) {
						sJackpot = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["JACKPOT"]);
					}
				}

			}
		} finally {
			conn.Close();
		}

		return sJackpot;
	}

	public string GetCurrentEntryCount(string pSiteCd,string pUserSeq,string pUserCharNo, string pSexCd, DateTime pTargetTime) {

		StringBuilder oBuilder = new StringBuilder();
		string sEntryCount = "0";

		oBuilder.Append("SELECT															 ").AppendLine();
		oBuilder.Append("	COUNT(*)								AS	ENTRY_COUNT		 ").AppendLine();
		oBuilder.Append("FROM															 ").AppendLine();
		oBuilder.Append("	T_BINGO_ENTRY												,").AppendLine();
		oBuilder.Append("	(															 ").AppendLine();
		oBuilder.Append("		SELECT													 ").AppendLine();
		oBuilder.Append("			SITE_CD												,").AppendLine();
		oBuilder.Append("			BINGO_TERM_SEQ										 ").AppendLine();
		oBuilder.Append("		FROM													 ").AppendLine();
		oBuilder.Append("			T_MAIL_DE_BINGO										 ").AppendLine();
		oBuilder.Append("		WHERE													 ").AppendLine();
		oBuilder.Append("			SITE_CD				 = :SITE_CD				AND		 ").AppendLine();
		oBuilder.Append("			SEX_CD				 = :SEX_CD				AND		 ").AppendLine();
		oBuilder.Append("			BINGO_START_DATE	<= :BINGO_TARGET_DATE	AND		 ").AppendLine();
		oBuilder.Append("			BINGO_END_DATE		>= :BINGO_TARGET_DATE			 ").AppendLine();
		oBuilder.Append(")T1															 ").AppendLine();
		oBuilder.Append("WHERE															 ").AppendLine();
		oBuilder.Append("T_BINGO_ENTRY.SITE_CD			= T1.SITE_CD			AND		 ").AppendLine();
		oBuilder.Append("T_BINGO_ENTRY.BINGO_TERM_SEQ	= T1.BINGO_TERM_SEQ		AND		 ").AppendLine();
		oBuilder.Append("T_BINGO_ENTRY.USER_SEQ			= :USER_SEQ				AND		 ").AppendLine();
		oBuilder.Append("T_BINGO_ENTRY.USER_CHAR_NO		= :USER_CHAR_NO					 ").AppendLine();


		try
		{
			DataSet ds = new DataSet();
			conn = DbConnect("MailDeBingo.GetCurrentJackpot");
			using (cmd = CreateSelectCommand(oBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":SEX_CD", pSexCd);
				cmd.Parameters.Add(":BINGO_TARGET_DATE", OracleDbType.Date, pTargetTime, ParameterDirection.Input);
				cmd.Parameters.Add(":USER_SEQ", pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO", pUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count > 0) {
						sEntryCount = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["ENTRY_COUNT"]);
					}
				}

			}
		}
		finally
		{
			conn.Close();
		}

		return sEntryCount;
	}

	public string GetCurrentTermSeq(string pSiteCd,string pSexCd,DateTime pTargetTime) {

		StringBuilder oBuilder = new StringBuilder();
		string sBingoTermSeq = string.Empty;

		oBuilder.Append("SELECT													 ").AppendLine();
		oBuilder.Append("	BINGO_TERM_SEQ										 ").AppendLine();
		oBuilder.Append("FROM													 ").AppendLine();
		oBuilder.Append("	T_MAIL_DE_BINGO										 ").AppendLine();
		oBuilder.Append("WHERE													 ").AppendLine();
		oBuilder.Append("	SITE_CD				 = :SITE_CD				AND		 ").AppendLine();
		oBuilder.Append("	SEX_CD				 = :SEX_CD				AND		 ").AppendLine();
		oBuilder.Append("	BINGO_START_DATE	<= :BINGO_TARGET_DATE	AND		 ").AppendLine();
		oBuilder.Append("	BINGO_END_DATE		>= :BINGO_TARGET_DATE			 ").AppendLine();

		try {
			DataSet ds = new DataSet();
			conn = DbConnect("MailDeBingo.GetCurrentTermSeq");
			using (cmd = CreateSelectCommand(oBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				cmd.Parameters.Add(":BINGO_TARGET_DATE",OracleDbType.Date,pTargetTime,ParameterDirection.Input);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count > 0) {
						sBingoTermSeq = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["BINGO_TERM_SEQ"]);
					}
				}

			}
		} finally {
			conn.Close();
		}

		return sBingoTermSeq;
	}

	public string DealJackpot(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,out string pBingoStatus,out string pBingoElectionAmt, out string pBingoElectionPoint, out string pBingoCardNo) {
		string sResult = string.Empty;
		pBingoStatus = string.Empty;
		pBingoElectionPoint = string.Empty;
		pBingoElectionAmt = string.Empty;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DEAL_JACKPOT");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			oDbSession.ProcedureOutParm("pBINGO_ELECTION_AMT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBINGO_ELECTION_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBINGO_CARD_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBINGO_STATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			pBingoStatus = oDbSession.GetStringValue("pBINGO_STATUS");
			pBingoElectionAmt = oDbSession.GetStringValue("pBINGO_ELECTION_AMT");
			pBingoElectionPoint = oDbSession.GetStringValue("pBINGO_ELECTION_POINT");
			pBingoCardNo = oDbSession.GetStringValue("pBINGO_CARD_NO");
		}
		return sResult;
	}

	public string GetBingoStartDate(string pSiteCd,string pSexCd) {
		StringBuilder oBuilder = new StringBuilder();
		string sDate = string.Empty;

		oBuilder.AppendLine("SELECT CASE																																						");
		oBuilder.AppendLine("	WHEN OPEN_COUNT > 0 THEN																																		");
		oBuilder.AppendLine("		(SELECT BINGO_START_DATE FROM T_MAIL_DE_BINGO WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD AND BINGO_START_DATE <= SYSDATE AND SYSDATE <= BINGO_END_DATE)	");
		oBuilder.AppendLine("	WHEN OPEN_COUNT = 0 AND NEXT_OPEN_COUNT = 0 AND PAST_COUNT > 0 THEN																								");
		oBuilder.AppendLine("		(SELECT MAX(BINGO_START_DATE) FROM T_MAIL_DE_BINGO WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD AND BINGO_END_DATE < SYSDATE)								");
		oBuilder.AppendLine("	WHEN OPEN_COUNT = 0 AND NEXT_OPEN_COUNT > 0 THEN																												");
		oBuilder.AppendLine("		(SELECT MIN(BINGO_START_DATE) FROM T_MAIL_DE_BINGO WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD AND SYSDATE < BINGO_START_DATE)								");
		oBuilder.AppendLine("	END START_DATE																																					");
		oBuilder.AppendLine("FROM																																								");
		oBuilder.AppendLine("	(SELECT																																							");
		oBuilder.AppendLine("		SUM(CASE WHEN BINGO_START_DATE <= SYSDATE AND SYSDATE <= BINGO_END_DATE THEN 1 ELSE 0 END) OPEN_COUNT,														");
		oBuilder.AppendLine("		SUM(CASE WHEN BINGO_END_DATE < SYSDATE THEN 1 ELSE 0 END) PAST_COUNT,																						");
		oBuilder.AppendLine("		SUM(CASE WHEN SYSDATE < BINGO_START_DATE THEN 1 ELSE 0 END) NEXT_OPEN_COUNT																					");
		oBuilder.AppendLine("	FROM																																							");
		oBuilder.AppendLine("		T_MAIL_DE_BINGO																																				");
		oBuilder.AppendLine("	WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD)																													");

		try {
			DataSet ds = new DataSet();
			conn = DbConnect("MailDeBingo.GetCurrentTermSeq");
			using (cmd = CreateSelectCommand(oBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count > 0) {
						sDate = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["START_DATE"]);
					}
					return sDate;
				}
			}
		}
		finally {
			conn.Close();
		}
	}


	public string GetBingoEndDate(string pSiteCd,string pSexCd) {
		StringBuilder oBuilder = new StringBuilder();
		string sDate = string.Empty;

		oBuilder.AppendLine("SELECT CASE																																						");
		oBuilder.AppendLine("	WHEN OPEN_COUNT > 0 THEN																																		");
		oBuilder.AppendLine("		(SELECT BINGO_END_DATE FROM T_MAIL_DE_BINGO WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD AND BINGO_START_DATE <= SYSDATE AND SYSDATE <= BINGO_END_DATE)	");
		oBuilder.AppendLine("	WHEN OPEN_COUNT = 0 AND NEXT_OPEN_COUNT = 0 AND PAST_COUNT > 0 THEN																								");
		oBuilder.AppendLine("		(SELECT MAX(BINGO_END_DATE) FROM T_MAIL_DE_BINGO WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD AND BINGO_END_DATE < SYSDATE)								");
		oBuilder.AppendLine("	WHEN OPEN_COUNT = 0 AND NEXT_OPEN_COUNT > 0 THEN																												");
		oBuilder.AppendLine("		(SELECT MIN(BINGO_END_DATE) FROM T_MAIL_DE_BINGO WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD AND SYSDATE < BINGO_START_DATE)								");
		oBuilder.AppendLine("	END START_DATE																																					");
		oBuilder.AppendLine("FROM																																								");
		oBuilder.AppendLine("	(SELECT																																							");
		oBuilder.AppendLine("		SUM(CASE WHEN BINGO_START_DATE <= SYSDATE AND SYSDATE <= BINGO_END_DATE THEN 1 ELSE 0 END) OPEN_COUNT,														");
		oBuilder.AppendLine("		SUM(CASE WHEN BINGO_END_DATE < SYSDATE THEN 1 ELSE 0 END) PAST_COUNT,																						");
		oBuilder.AppendLine("		SUM(CASE WHEN SYSDATE < BINGO_START_DATE THEN 1 ELSE 0 END) NEXT_OPEN_COUNT																					");
		oBuilder.AppendLine("	FROM																																							");
		oBuilder.AppendLine("		T_MAIL_DE_BINGO																																				");
		oBuilder.AppendLine("	WHERE SITE_CD = :SITE_CD AND SEX_CD= :SEX_CD)																													");

		try {
			DataSet ds = new DataSet();
			conn = DbConnect("MailDeBingo.GetCurrentTermSeq");
			using (cmd = CreateSelectCommand(oBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count > 0) {
						sDate = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["START_DATE"]);
					}
					return sDate;
				}
			}
		}
		finally {
			conn.Close();
		}
	}
}
