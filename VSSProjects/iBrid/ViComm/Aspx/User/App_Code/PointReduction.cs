﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ポイント還元

--	Progaram ID		: PointReduction
--
--  Creation Date	: 2011.09.29
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class PointReduction : DbSession {
	public PointReduction() {
	}

	public int? GetCurrentReductionPoint(string pSiteCd, int? pReductionReceiptSumAmt) {
		if (pReductionReceiptSumAmt == null) { return null; }

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT											 ");
		oSqlBuilder.AppendLine("	PR.SITE_CD									,");
		oSqlBuilder.AppendLine("	PR.RECEIPT_SUM_AMT							,");
		oSqlBuilder.AppendLine("	PR.REDUCTION_POINT							 ");
		oSqlBuilder.AppendLine("FROM											 ");
		oSqlBuilder.AppendLine("	T_POINT_REDUCTION		PR					 ");
		oSqlBuilder.AppendLine("WHERE											 ");
		oSqlBuilder.AppendLine("	PR.SITE_CD			 = :SITE_CD			AND	 ");
		oSqlBuilder.AppendLine("	PR.RECEIPT_SUM_AMT	<= :RECEIPT_SUM_AMT		 ");
		oSqlBuilder.AppendLine("ORDER BY										 ");
		oSqlBuilder.AppendLine("	PR.SITE_CD,PR.RECEIPT_SUM_AMT			DESC ");

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;

				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter(":RECEIPT_SUM_AMT", pReductionReceiptSumAmt));

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}

		if (oDataSet.Tables[0].Rows.Count <= 0) { return 0; }

		return int.Parse(oDataSet.Tables[0].Rows[0]["REDUCTION_POINT"].ToString());
	}

	public DataSet GetNextReductionInfo(string pSiteCd, int? pReductionReceiptSumAmt) {
		if (pReductionReceiptSumAmt == null) { return null; }

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT											 ");
		oSqlBuilder.AppendLine("	PR.SITE_CD									,");
		oSqlBuilder.AppendLine("	PR.RECEIPT_SUM_AMT							,");
		oSqlBuilder.AppendLine("	PR.REDUCTION_POINT							 ");
		oSqlBuilder.AppendLine("FROM											 ");
		oSqlBuilder.AppendLine("	T_POINT_REDUCTION		PR					 ");
		oSqlBuilder.AppendLine("WHERE											 ");
		oSqlBuilder.AppendLine("	PR.SITE_CD			= :SITE_CD			AND	 ");
		oSqlBuilder.AppendLine("	PR.RECEIPT_SUM_AMT	> :RECEIPT_SUM_AMT		 ");
		oSqlBuilder.AppendLine("ORDER BY										 ");
		oSqlBuilder.AppendLine("	PR.SITE_CD,PR.RECEIPT_SUM_AMT			ASC ");

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;

				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter(":RECEIPT_SUM_AMT", pReductionReceiptSumAmt));

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}

		if (oDataSet.Tables[0].Rows.Count <= 0) { return null; }

		return oDataSet;
	}
}
