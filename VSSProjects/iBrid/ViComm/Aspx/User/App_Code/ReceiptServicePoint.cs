﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User

--	Title			: 入金サービスポイント
--	Progaram ID		: ReceiptSericePoint
--  Creation Date	: 2011.04.25
--  Creater			: iBrid
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class ReceiptServicePoint : DbSession {
	public ReceiptServicePoint() {	}

	public bool IsExists(string pSiteCd, string pSettleType) {
		List<OracleParameter> oParameterList = new List<OracleParameter>();
		oParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT	COUNT(*)	").AppendLine();
		oSqlBuilder.Append("FROM	").AppendLine();
		oSqlBuilder.Append("	T_RECEIPT_SERVICE_POINT").AppendLine();
		oSqlBuilder.Append("WHERE	").AppendLine();
		oSqlBuilder.Append("	SYSDATE BETWEEN APPLICATION_START_DATE AND APPLICATION_END_DATE	AND").AppendLine();
		oSqlBuilder.Append("	CAMPAIN_APPLICATION_FLAG	= '1'			AND").AppendLine();
		oSqlBuilder.Append("	SITE_CD						= :SITE_CD		");

		if (!string.IsNullOrEmpty(pSettleType)) {
			oSqlBuilder.AppendLine("AND");
			oSqlBuilder.AppendLine("SETTLE_TYPE					= :SETTLE_TYPE	");
			oParameterList.Add(new OracleParameter("SETTLE_TYPE", pSettleType));
		}

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.Parameters.AddRange(oParameterList.ToArray());
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}

		return iPageCount > 0;
	}
}
