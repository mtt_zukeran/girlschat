﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾞﾛｸﾞ投稿
--	Progaram ID		: BlogArticle
--
--  Creation Date	: 2011.04.06
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

#region □■□ Condition □■□ =======================================================================================
[Serializable]
public class BlogArticleSeekCondition : SeekConditionBase{

	public string SiteCd {
		get { return this.Query["site"]; }
		set { this.Query["site"] = value; }
	}
	public string UserSeq {
		get { return this.Query["user_seq"]; }
		set { this.Query["user_seq"] = value; }
	}
	public string UserCharNo {
		get { return this.Query["user_char_no"]; }
		set { this.Query["user_char_no"] = value; }
	}
	public string BlogSeq {
		get { return this.Query["blog_seq"]; }
		set { this.Query["blog_seq"] = value; }
	}
	public string BlogArticleSeq {
		get { return this.Query["blog_article_seq"]; }
		set { this.Query["blog_article_seq"] = value; }
	}
	public string BlogArticleStatus {
		get { return this.Query["blog_article_status"]; }
		set { this.Query["blog_article_status"] = value; }
	}
	public string Random {
		get { return this.Query["random"]; }
		set { this.Query["random"] = value; }
	}
	
	public BlogArticleSeekCondition():this(new NameValueCollection()) {}
	public BlogArticleSeekCondition(NameValueCollection pQuery):base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["blog_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_seq"]));
		this.Query["blog_article_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_article_seq"]));
		this.Query["blog_article_status"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_article_status"]));
		this.Query["random"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["random"]));
	}
}
#endregion ============================================================================================================

public class BlogArticle : CastBase{

	public BlogArticle() : base() { }
	public BlogArticle(SessionObjs pSessionObj) : base(pSessionObj) { }

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(BlogArticleSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendFormat("	{0} P			", GetMainTableName()).AppendLine();
		if (pCondtion.SeekMode == ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP) {
			oSqlBuilder.AppendLine("	,T_BLOG_ARTICLE_PICKUP S1 ");
		}

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder, oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(BlogArticleSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		if (!string.IsNullOrEmpty(pCondtion.BlogArticleSeq)) {
			pPageNo = 1;
			pRecPerPage = 1;
		}
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	" + CastBasicField() + ",   ");
		if (!string.IsNullOrEmpty(pCondtion.BlogArticleSeq) && (!string.IsNullOrEmpty(pCondtion.BlogSeq) || !string.IsNullOrEmpty(pCondtion.UserSeq))) {
			oSqlBuilder.AppendFormat("	LEAD(P.BLOG_ARTICLE_SEQ,1)	OVER ({0}) AS NEXT_SEQ		,	", sSortExpression).AppendLine();
			oSqlBuilder.AppendFormat("	LAG (P.BLOG_ARTICLE_SEQ,1)	OVER ({0}) AS PREV_SEQ		,	", sSortExpression).AppendLine();
			oSqlBuilder.AppendFormat("	RANK()						OVER ({0}) AS RNUM			,	", sSortExpression).AppendLine();
		}
		oSqlBuilder.AppendLine("	P.BLOG_SEQ				,	");
		oSqlBuilder.AppendLine("	P.BLOG_TITLE			,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_SEQ		,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_TITLE	,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_BODY1	,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_BODY2	,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_BODY3	,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_BODY4	,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_BODY5	,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_STATUS	,	");
		oSqlBuilder.AppendLine("	P.ATTACHED_PIC_FLAG		,	");
		oSqlBuilder.AppendLine("	P.ATTACHED_MOVIE_FLAG	,	");
		oSqlBuilder.AppendLine("	P.REGIST_DATE			,	");
		oSqlBuilder.AppendLine("	P.OBJ_SEQ				,	");
		oSqlBuilder.AppendLine("	P.READING_COUNT			,	");
		oSqlBuilder.AppendLine("	0 AS COMMENT_COUNT		,	");
		oSqlBuilder.AppendLine("	P.BLOG_LIKE_COUNT		,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_LIKE_COUNT	");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendFormat("	{0} P			", GetMainTableName()).AppendLine();
		if(pCondtion.SeekMode == ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP){
			oSqlBuilder.AppendLine("	,T_BLOG_ARTICLE_PICKUP S1 ");
		}
		
		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion, ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);

		string sExecSql = oSqlBuilder.ToString();

		if (!string.IsNullOrEmpty(pCondtion.BlogArticleSeq) && (!string.IsNullOrEmpty(pCondtion.BlogSeq) || !string.IsNullOrEmpty(pCondtion.UserSeq))) {
			oParamList.AddRange(WrapSeqQuery(sExecSql, pCondtion, out sExecSql));
		}else{
			oParamList.AddRange(ViCommPrograms.CreatePagingSql(sExecSql, iStartIndex, pRecPerPage, out sExecSql));
		}

		// =================================================
		//  TEMPLATE 適用
		// =================================================
		StringBuilder oJoinTables = new StringBuilder();
		oJoinTables.AppendLine(" T_BLOG_OBJS	T_BLOG_OBJS		,");
		oJoinTables.AppendLine(" T_CODE_DTL		T_CODE_DTL_12	,");

		StringBuilder oJoinCondition = new StringBuilder();
		//
		oJoinCondition.AppendLine(" T1.SITE_CD				= T_BLOG_OBJS.SITE_CD		(+)	AND ");
		oJoinCondition.AppendLine(" T1.USER_SEQ				= T_BLOG_OBJS.USER_SEQ		(+)	AND ");
		oJoinCondition.AppendLine(" T1.USER_CHAR_NO			= T_BLOG_OBJS.USER_CHAR_NO	(+)	AND ");
		oJoinCondition.AppendLine(" T1.OBJ_SEQ				= T_BLOG_OBJS.OBJ_SEQ		(+)	AND ");		
		//
		oJoinCondition.AppendLine(" :CODE_TYPE_12			= T_CODE_DTL_12.CODE_TYPE		AND	");
		oJoinCondition.AppendLine(" T1.BLOG_ARTICLE_STATUS	= T_CODE_DTL_12.CODE			AND ");
		oParamList.Add(new OracleParameter(":CODE_TYPE_12","12"));

		StringBuilder oOuterFields = new StringBuilder();
		// PIC
		oOuterFields.AppendLine("	T1.OBJ_SEQ															AS PIQ_SEQ							, ");
		oOuterFields.AppendLine("	GET_PHOTO_IMG_PATH				(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_PHOTO_IMG_PATH				, ");
		oOuterFields.AppendLine("	GET_SMALL_PHOTO_IMG_PATH		(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_SMALL_PHOTO_IMG_PATH			, ");
		oOuterFields.AppendLine("	GET_BLUR_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_BLUR_PHOTO_IMG_PATH			, ");
		oOuterFields.AppendLine("	GET_SMALL_BLUR_PHOTO_IMG_PATH	(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_SMALL_BLUR_PHOTO_IMG_PATH	, ");
		// MOVIE
		oOuterFields.AppendLine("	T1.OBJ_SEQ															AS MOVIE_SEQ						, ");
		oOuterFields.AppendLine("	T1.OBJ_SEQ															AS THUMBNAIL_PIC_SEQ				, ");
		oOuterFields.AppendLine("	GET_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)				AS THUMBNAIL_IMG_PATH				, ");
		// OTHER
		oOuterFields.AppendLine("	T_CODE_DTL_12.CODE_NM				AS BLOG_ARTICLE_STATUS_NM				, ");
		oOuterFields.AppendLine("	DECODE(T1.ATTACHED_PIC_FLAG,1,1,2)  AS BLOG_FILE_TYPE 						, ");

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql, oParamList, oJoinTables.ToString(), oJoinCondition.ToString(), oOuterFields.ToString());

		FakeOnlineStatus(oDataSet);
		base.AppendCastAttr(oDataSet);

		AppendCommentCount(oDataSet);

		return oDataSet;
		
	}
		
	private OracleParameter[] CreateWhere(BlogArticleSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (pCondition.SeekMode == ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD			= S1.SITE_CD			", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_SEQ			= S1.USER_SEQ			", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= S1.USER_CHAR_NO		", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" P.BLOG_SEQ 		= S1.BLOG_SEQ			", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" P.BLOG_ARTICLE_SEQ	= S1.BLOG_ARTICLE_SEQ	", ref pWhereClause);
		}


		SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD", ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" P.USER_STATUS = :USER_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_STATUS",ViCommConst.USER_WOMAN_NORMAL));

		SysPrograms.SqlAppendWhere(" P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		if (!string.IsNullOrEmpty(pCondition.BlogArticleSeq) && (string.IsNullOrEmpty(pCondition.BlogSeq) && string.IsNullOrEmpty(pCondition.UserSeq))) {
			SysPrograms.SqlAppendWhere("P.BLOG_ARTICLE_SEQ = :BLOG_ARTICLE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_ARTICLE_SEQ",pCondition.BlogArticleSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= :USER_CHAR_NO", ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ", pCondition.UserSeq));
			oParamList.Add(new OracleParameter(":USER_CHAR_NO", pCondition.UserCharNo));
		}
		if (!string.IsNullOrEmpty(pCondition.BlogSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_SEQ		= :BLOG_SEQ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_SEQ", pCondition.BlogSeq));
		}
		if (!string.IsNullOrEmpty(pCondition.BlogArticleStatus)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_ARTICLE_STATUS	= :BLOG_ARTICLE_STATUS", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_ARTICLE_STATUS", pCondition.BlogArticleStatus));
		}

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(FlexibleQueryType.NotInScope);
			oFlexibleQueryTarget.JealousyBlog=true;
			
			ArrayList oTmpList = new ArrayList();
			pWhereClause += SetFlexibleQuery(oFlexibleQueryTarget, oSessionMan.site.siteCd, oSessionMan.userMan.userSeq, oSessionMan.userMan.userCharNo, ref oTmpList);
			foreach(OracleParameter oParam in oTmpList){
				oParamList.Add(oParam);
			}

			SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_SEQ",oSessionMan.userMan.userSeq));
			oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",oSessionMan.userMan.userCharNo));
		}

		SysPrograms.SqlAppendWhere(" P.DEL_FLAG	= :DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DEL_FLAG",ViCommConst.FLAG_OFF_STR));
	
		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(BlogArticleSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		
		if(pCondition.Random.Equals(ViCommConst.FLAG_ON_STR)){
			sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";
		}else{
			if(pCondition.SeekMode == ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP){
				sSortExpression = " ORDER BY S1.PRIORITY ASC ";
			}else{
				sSortExpression += " ORDER BY ";
				sSortExpression += " SITE_CD ";
				if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
					sSortExpression += " , USER_SEQ , USER_CHAR_NO ";
				}
				if (!string.IsNullOrEmpty(pCondition.BlogSeq)) {
					sSortExpression += " , BLOG_SEQ ";
				}
				if (!string.IsNullOrEmpty(pCondition.BlogArticleStatus)) {
					sSortExpression += " , BLOG_ARTICLE_STATUS ";
				}									
				
				if (string.IsNullOrEmpty(pCondition.Sort)) {
					sSortExpression += " , BLOG_ARTICLE_SEQ DESC ";
				} else {
					sSortExpression += string.Format(" , {0} ", pCondition.Sort);
				}
			}
		}
		return sSortExpression;
	}
	
	public OracleParameter[] WrapSeqQuery(string pSql,BlogArticleSeekCondition pCondtion,out string pSeqQuery){
		if(string.IsNullOrEmpty(pCondtion.BlogArticleSeq) || (string.IsNullOrEmpty(pCondtion.BlogSeq) && string.IsNullOrEmpty(pCondtion.UserSeq))){
			pSeqQuery = pSql;
			return new OracleParameter[]{};
		}
	
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine(" SELECT	/*+ FIRST_ROWS(1) */	");
		oSqlBuilder.AppendLine("	SEQ.*						");	    
		oSqlBuilder.AppendLine(" FROM (							");
		oSqlBuilder.AppendFormat(" {0} ",pSql).AppendLine();
		oSqlBuilder.AppendLine(" ) SEQ							");
		oSqlBuilder.AppendLine(" WHERE							");
		oSqlBuilder.AppendLine("	SEQ.BLOG_ARTICLE_SEQ = :BLOG_ARTICLE_SEQ AND ");
		oSqlBuilder.AppendLine("	ROWNUM <= 1									 ");

		oParamList.Add(new OracleParameter(":BLOG_ARTICLE_SEQ", pCondtion.BlogArticleSeq));
		
		pSeqQuery = oSqlBuilder.ToString();
		return oParamList.ToArray();
	}

	private void AppendCommentCount(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			BlogCommentSeekCondition oCondition = new BlogCommentSeekCondition();
			oCondition.SiteCd = dr["SITE_CD"].ToString();
			oCondition.BlogArticleSeq = dr["BLOG_ARTICLE_SEQ"].ToString();
			decimal dRecCount = 0;

			using (BlogComment oBlogComment = new BlogComment(sessionObj)) {
				oBlogComment.GetPageCount(oCondition,1,out dRecCount);
			}

			dr["COMMENT_COUNT"] = dRecCount.ToString();
		}
	}

	#endregion ========================================================================================================
	
	public string GetMainTableName(){
		string sPrefix = "VW";
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBlogArticle"]).Equals(ViCommConst.FLAG_ON_STR)
			&& sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			sPrefix = "MV";
		}
		return string.Format("{0}_BLOG_ARTICLE00",sPrefix);
	}


	public void AppendObj(string pSiteCd, string pUserSeq, string pUserCharNo, string pBlogSeq, string pBlogArticleSeq, string pObjSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADD_BLOG_ARTICLE_OBJ");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, pUserCharNo);
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.NUMBER, pBlogSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.NUMBER, pBlogArticleSeq);
			oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.VARCHAR2, pObjSeq);
			oDbSession.ProcedureOutParm("pRESULT", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	public void PreferPublish(string pSiteCd, string pUserSeq, string pUserCharNo, string pBlogSeq, string pBlogArticleSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BLOG_ARTICLE_STATUS_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, pUserCharNo);
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.NUMBER, pBlogSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.NUMBER, pBlogArticleSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_STATUS", DbSession.DbType.VARCHAR2, ViCommConst.BlogArticleStatus.PRIVATE);
			oDbSession.ProcedureInParm("pSWKIP_CHK_REV_NO", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	public void UpdateReadingCount(string pSiteCd, string pUserSeq, string pUserCharNo, string pBlogSeq, string pBlogArticleSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("UPDATE_BLOG_ARTICLE_COUNT");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, pUserCharNo);
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.VARCHAR2, pBlogSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.VARCHAR2, pBlogArticleSeq);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}
	
	public void PublishBlogArticle(string pSiteCd,string pUserSeq,string pUserCharNo,string pBlogSeq,string pBlogArticleSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BLOG_ARTICLE_STATUS_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pBLOG_SEQ",DbSession.DbType.VARCHAR2,pBlogSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ",DbSession.DbType.VARCHAR2,pBlogArticleSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_STATUS",DbSession.DbType.VARCHAR2,ViCommConst.BlogArticleStatus.PUBLIC);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	public void DeleteBlogArticle(string pSiteCd,string pUserSeq,string pUserCharNo,string pBlogArticleSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_BLOG_ARTICLE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ",DbSession.DbType.VARCHAR2,pBlogArticleSeq);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}
}
