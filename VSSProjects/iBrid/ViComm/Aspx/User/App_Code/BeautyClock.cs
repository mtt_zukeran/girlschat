﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[Serializable]
public class BeautyClockSeekCondition:SeekConditionBase {

	public string SiteCd {
		get { return this.Query["site_cd"];}
		set { this.Query["site_cd"] = value;}
	}

	public string AssignedTime {
		get { return this.Query["assigned_time"];}
		set {
				if (value.Equals("just_now")) {
					this.Query["assigned_time"] = ViCommPrograms.GetAssignedNowTime();
				} else if(value.Contains(":")) {
					this.Query["assigned_time"] = ViCommPrograms.ConvertTimeToAssigned(value);
				} else {
					this.Query["assigned_time"] = value;
				}
			}
	}

	public BeautyClockSeekCondition() : this(new NameValueCollection()) {}
	public BeautyClockSeekCondition(NameValueCollection pQuery) : base(pQuery){
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));

		//AssignedTimeのみ、setアクセサーを起動したい為、プロパティに入力
		this.AssignedTime = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["assigned_time"]));
	}

}

[System.Serializable]
public class BeautyClock : CastBase {
	public BeautyClock():base() {}
	public BeautyClock(SessionObjs pSessionObjs):base(pSessionObjs) {}
	public int GetPageCount(BeautyClockSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine(" COUNT(P.PIC_SEQ)	");
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where		
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oWhereParams);

				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			this.conn.Close();
		}
		return iPageCount;
	}
	public DataSet GetPageCollection(BeautyClockSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}
	private DataSet GetPageCollectionBase(BeautyClockSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT " + CastBasicField() + "																		, ");
		oSqlBuilder.AppendLine(" 	P.ASSIGNED_TIME																					, ");
		oSqlBuilder.AppendLine(" 	P.PIC_SEQ																						, ");
		oSqlBuilder.AppendLine(" 	P.PUBLISH_FLAG																					, ");
		oSqlBuilder.AppendLine(" 	P.BEAUTY_CLOCK_REGIST_DATE																		, ");
		oSqlBuilder.AppendLine(" 	P.BEAUTY_CLOCK_UPDATE_DATE																		, ");
		oSqlBuilder.AppendLine(" 	GET_BEAUTY_CLOCK_IMG_PATH(		P.SITE_CD,P.ASSIGNED_TIME,P.PIC_SEQ) AS BEAUTY_IMG_PATH			, ");
		oSqlBuilder.AppendLine(" 	GET_BEAUTY_CLOCK_IMG_PATH_S(	P.SITE_CD,P.ASSIGNED_TIME,P.PIC_SEQ) AS BEAUTY_IMG_S_PATH		, ");
		oSqlBuilder.AppendLine(" 	P.BLOG_SEQ																					      ");
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where		
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = oSqlBuilder.ToString();
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(sExecSql,sSortExpression,iStartIndex,pRecPerPage,out sExecSql);

		OracleParameter[] oJoinParams = null;
		if(pCondtion.UseTemplate){
			SessionObjs oObj = base.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			StringBuilder sTables = new StringBuilder();
			StringBuilder sJoinTable = new StringBuilder();
			StringBuilder sJoinField = new StringBuilder();

			sExecSql = string.Format(sSql,sExecSql,sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			ArrayList list = new ArrayList();
			JoinCondition(ref list);
			InnnerCondition(ref list);
			oJoinParams = (OracleParameter[])list.ToArray(typeof(OracleParameter));
		}

		
		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(sExecSql,this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oWhereParams);
				this.cmd.Parameters.AddRange(oPagingParams);
				
				if(oJoinParams != null){
					this.cmd.Parameters.AddRange(oJoinParams);
				}

				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.Fill(da,oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}

		FakeOnlineStatus(oDataSet);
		AppendCastAttr(oDataSet);
		return oDataSet;
	}

	private string CreateFrom(BeautyClockSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" FROM							  ");
		oSqlBuilder.AppendLine(" 	VW_BEAUTY_CLOCK00		P	  ");
		return oSqlBuilder.ToString();
	}
	private OracleParameter[] CreateWhere(BeautyClockSeekCondition pCondition,ref string pWhereClause){
		if(pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		SysPrograms.SqlAppendWhere(" P.PUBLISH_FLAG			=	:PUBLISH_FLAG					",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON));

		SysPrograms.SqlAppendWhere(" P.SITE_CD				= :SITE_CD							",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));


		if (!string.IsNullOrEmpty(pCondition.AssignedTime)) {
			SysPrograms.SqlAppendWhere(" P.ASSIGNED_TIME	= :ASSIGNED_TIME					",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ASSIGNED_TIME",pCondition.AssignedTime));
		}		
		return oParamList.ToArray();
	}
	protected string CreateOrderExpresion(BeautyClockSeekCondition pCondition){
		string sSortExpression = string.Empty;
		if(string.IsNullOrEmpty(pCondition.Sort)){
			sSortExpression = " ORDER BY P.ASSIGNED_TIME ASC,P.BEAUTY_CLOCK_UPDATE_DATE DESC,P.PIC_SEQ DESC ";
		} else {
			sSortExpression = string.Format("ORDER BY {0} ",pCondition.Sort);
		}
		return sSortExpression;
	}
}
