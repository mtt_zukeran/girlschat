﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サイト 
--	Progaram ID		: Site
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Generic;
using iBridCommLib;

[System.Serializable]
public class Site:DbSession {
	public string siteCd;
	public string siteNm;
	public string webPhisicalDir;
	public string url;
	public string colorBack;
	public string colorChar;
	public string colorIndex;
	public string colorLink;
	public string colorALink;
	public string colorVLink;
	public string colorLine;
	public int sizeChar;
	public int sizeLine;
	public string headerHtmlDoc;
	public string footerHtmlDoc;
	public string hostNm;
	public string subHostNm;
	public string mailHost;
	public string infoEmailAddr;
	public string supportEmailAddr;
	public string reportEmailAddr;
	public string castRegistReportEmailAddr;
	public string supportTel;
	public string ivpLocCd;
	public string ivpSiteCd;
	public string VcsWeLocCd;
	public string VcsTenantCd;
	public string lightChConnectType;
	public string siteType;
	public int useOtherSysInfoFlag;
	public string userInfoUrl;
	public string parentWebNm;
	public string parentWebTopPageUrl;
	public string parentWebLoginUrl;
	public string parentWebMyPageUrl;
	public string PcRedirectUrl;
	public int enablePrivteTalkMenuFlag;
	public int displayMovieUploadDays;
	public int findManElapasedDays;
	public int txMailRefMarking;
	public int newFaceDays;
	public string newFaceMark;
	public int manNewFaceDays;
	public int voiceNewDays;
	public int profileMovieNewHours;
	public int bbsNewHours;
	public int supportChgObjCatFlag;
	public int picBbsNewHours;
	public int movieBbsNewHours;
	public int RxMailNewHours;
	public string pageTitle;
	public string pageKeyword;
	public string pageDescription;
	public string pageTitleOriginal;
	public string pageKeywordOriginal;
	public string pageDescriptionOriginal;
	public string[] cssArrayDocomo;
	public string[] cssArraySoftbank;
	public string[] cssArrayAu;
	public string[] cssArrayiPhone;
	public string[] cssArrayAndroid;

	public string cssDocomoOriginal;
	public string cssSoftbankOriginal;
	public string cssAuOriginal;
	public string cssiPhoneOriginal;
	public string cssAndroidOriginal;

	public int topPageSeekMode;
	public int castCanSelectMonitorFlag;
	public int castCanSelectConnectType;
	public string manHandleDefaultNm;

	public int supportPremiumTalkFlag;
	public int supportKddiTvTelFlag;
	public int supportChgMonitorToTalkFlag;
	public int supportVoiceMonitorFlag;
	public int castMultiCtlSiteFlag;
	public int bbsDupChargeFlag;
	public int attachedMailDupChargeFlag;
	public int useAttachedMailFlag;
	public int registHandleNmInputFlag;
	public int registBirthdayInputFlag;
	public int castRegistNeedAgreeFlag;
	public int profilePageingFlag;
	public string profilePreviousGuidance;
	public string profileNextGuidance;
	public int profilePageingFlag2;
	public string profilePreviousGuidance2;
	public string profileNextGuidance2;
	public string previousAccessKey;
	public string nextAccessKey;
	public string charNoItemNm;
	public bool jobOfferSiteFlag;
	public string jobOfferSiteHostNm;
	public string jobOfferStartDoc;
	public string jobOfferSiteType;
	public int pointPrice;
	public float pointTax;
	public int usedChargeSettleFlag;
	public int NonAdRegistReqAgeCertFlag;
	public string userTopId;
	public string nonUserTopId;
	public int mobileManDetailListCount;
	public int mobileManListCount;
	public int mobileWomanDetailListCount;
	public int mobileWomanListCount;
	public int bbsPicAttrFlag;
	public int bbsMovieAttrFlag;
	public int zeroSettleServicePoint;
	public int mailSearchEnabled;
	public int mailSearchLimit;
	private Hashtable siteSupplement;
	public int castWriteBbsLimit = -1;
	public int castWriteIntervalMin = 0;
	public int manWriteBbsLimit = -1;
	public int manWriteIntervalMin = 0;
	public int payManWriteBbsLimit = -1;
	public int payManWriteIntervalMin = 0;
	public int loginMailNotSendFlag;
	public int loginMail2NotSendFlag;
	public int logoutMailNotSendFlag;
	public int manTalkEndSendMailFlag;
	public int castTalkEndSendMailFlag;
	public int reBatchMailNaHour;
	public string googleVerification;
	public string whitePlanNo;
	public string impCastFirstLoginAct;
	public string impManFirstLoginAct;
	public int dummyTalkTxLoginMailFlag;
	public int manAutoLoginResignedFlag;
	public int castAutoLoginResignedFlag;
	public int siteHtmlDocSexCheckFlag;
	public string ManRegistFuncLimitAdCd;

	//EX
	public string buyPointMailCommDays;
	public string loveListProfileDays;
	public int bbsResSearchLimit;
	//public string docomoMoneyTransferTel;
	//public string docomoMoneyTransferNm;
	public int wantedApplicantDelDays;
	

	public Site() {
		siteSupplement = new Hashtable();
		jobOfferSiteFlag = false;
	}

	public bool GetOneByHost(string pHostNm) {
		DataSet ds;
		bool bExist = false;

		string[] sHostNm = pHostNm.Split('.');
		string sKey = pHostNm;
		try {
			conn = DbConnect("Site.GetOneByHost");

			for (int i = 0;i < 2;i++) {
				using (cmd = CreateSelectCommand("SELECT * FROM VW_SITE01 WHERE HOST_NM =:HOST_NM",conn))
				using (ds = new DataSet()) {
					cmd.Parameters.Add("HOST_NM",sKey);
					if ((bExist = SetData(ds)) == true) {
						break;
					}
				}
				sKey = "";
				for (int j = i + 1;j < sHostNm.Length;j++) {
					sKey += sHostNm[j] + ".";
				}
				sKey = sKey.Substring(0,sKey.Length - 1);
			}
		} finally {
			conn.Close();
		}
		SetSupplement();
		return bExist;
	}

	public bool GetOne(string pSiteCd) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect("Site.GetOne");

			using (cmd = CreateSelectCommand("SELECT * FROM VW_SITE01 WHERE SITE_CD = :SITE_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				bExist = SetData(ds);
			}
		} finally {
			conn.Close();
		}
		SetSupplement();

		return bExist;
	}

	public bool GetOneByJobOfferHost(string pHost) {
		DataSet ds;
		jobOfferSiteFlag = false;
		try {
			conn = DbConnect("Site.GetOneByJobOfferHost");

			using (cmd = CreateSelectCommand("SELECT * FROM VW_SITE01 WHERE JOB_OFFER_SITE_HOST_NM = :JOB_OFFER_SITE_HOST_NM",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("JOB_OFFER_SITE_HOST_NM",pHost);
				jobOfferSiteFlag = SetData(ds);
			}
		} finally {
			conn.Close();
		}
		SetSupplement();

		return jobOfferSiteFlag;
	}

	public bool GetCurrent() {
		return GetOne(siteCd);
	}

	public bool GetCastMultiCtlSite() {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect("Site.GetCastMultiCtlSite");

			for (int i = 0;i < 2;i++) {
				using (cmd = CreateSelectCommand("SELECT * FROM VW_SITE01 WHERE CAST_MULTI_CTL_SITE_FLAG =:CAST_MULTI_CTL_SITE_FLAG",conn))
				using (ds = new DataSet()) {
					cmd.Parameters.Add("CAST_MULTI_CTL_SITE_FLAG",ViComm.ViCommConst.FLAG_ON);
					if ((bExist = SetData(ds)) == true) {
						break;
					}
				}
			}
		} finally {
			conn.Close();
		}
		SetSupplement();
		return bExist;
	}


	private bool SetData(DataSet ds) {
		DataRow dr;
		bool bRet = false;
		try {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_SITE01");
				if (ds.Tables["VW_SITE01"].Rows.Count != 0) {
					dr = ds.Tables["VW_SITE01"].Rows[0];
					siteCd = dr["SITE_CD"].ToString();
					siteNm = dr["SITE_NM"].ToString();
					webPhisicalDir = dr["WEB_PHISICAL_DIR"].ToString();
					url = dr["URL"].ToString();
					if (SessionObjs.CurrentSexCd.Equals(ViComm.ViCommConst.MAN)) {
						colorBack = dr["COLOR_BACK"].ToString();
						colorChar = dr["COLOR_CHAR"].ToString();
						colorIndex = dr["COLOR_INDEX"].ToString();
						colorLink = dr["COLOR_LINK"].ToString();
						colorALink = dr["COLOR_ALINK"].ToString();
						colorVLink = dr["COLOR_VLINK"].ToString();
						colorLine = dr["COLOR_LINE"].ToString();
						sizeChar = int.Parse(dr["SIZE_CHAR"].ToString());
						sizeLine = int.Parse(dr["SIZE_LINE"].ToString());
					} else {
						colorBack = dr["COLOR_BACK_WOMAN"].ToString();
						colorChar = dr["COLOR_CHAR_WOMAN"].ToString();
						colorIndex = dr["COLOR_INDEX_WOMAN"].ToString();
						colorLink = dr["COLOR_LINK_WOMAN"].ToString();
						colorALink = dr["COLOR_ALINK_WOMAN"].ToString();
						colorVLink = dr["COLOR_VLINK_WOMAN"].ToString();
						colorLine = dr["COLOR_LINE_WOMAN"].ToString();
						sizeChar = int.Parse(dr["SIZE_CHAR_WOMAN"].ToString());
						sizeLine = int.Parse(dr["SIZE_LINE_WOMAN"].ToString());
					}
					hostNm = dr["HOST_NM"].ToString();
					subHostNm = dr["SUB_HOST_NM"].ToString();
					mailHost = dr["MAIL_HOST"].ToString();
					infoEmailAddr = dr["INFO_EMAIL_ADDR"].ToString();
					supportEmailAddr = dr["SUPPORT_EMAIL_ADDR"].ToString();
					reportEmailAddr = dr["REPORT_EMAIL_ADDR"].ToString();
					castRegistReportEmailAddr = dr["CAST_REGIST_REPORT_EMAIL_ADDR"].ToString();
					supportTel = dr["SUPPORT_TEL"].ToString();
					ivpLocCd = dr["IVP_LOC_CD"].ToString();
					ivpSiteCd = dr["IVP_SITE_CD"].ToString();
					VcsWeLocCd = dr["VCS_WEB_LOC_CD"].ToString();
					VcsTenantCd = dr["VCS_TENANT_CD"].ToString();
					lightChConnectType = dr["LIGHT_CH_CONNECT_TYPE"].ToString();
					siteType = dr["SITE_TYPE"].ToString();
					useOtherSysInfoFlag = int.Parse(dr["USE_OTHER_SYS_INFO_FLAG"].ToString());
					PcRedirectUrl = dr["PC_REDIRECT_URL"].ToString();
					enablePrivteTalkMenuFlag = int.Parse(dr["ENABLE_PRIVATE_TALK_MENU_FLAG"].ToString());
					displayMovieUploadDays = int.Parse(dr["DISPLAY_MOVIE_UPLOAD_DAYS"].ToString());
					findManElapasedDays = int.Parse(dr["FIND_MAN_ELAPASED_DAYS"].ToString());
					txMailRefMarking = int.Parse(dr["TX_MAIL_REF_MARKING"].ToString());
					newFaceDays = int.Parse(dr["NEW_FACE_DAYS"].ToString());
					newFaceMark = dr["NEW_FACE_MARK"].ToString();
					manNewFaceDays = int.Parse(dr["MAN_NEW_FACE_DAYS"].ToString());
					voiceNewDays = int.Parse(dr["VOICE_NEW_DAYS"].ToString());
					profileMovieNewHours = int.Parse(dr["PROFILE_MOVIE_NEW_HOURS"].ToString());
					bbsNewHours = int.Parse(dr["BBS_NEW_HOURS"].ToString());
					picBbsNewHours = int.Parse(dr["PIC_BBS_NEW_HOURS"].ToString());
					movieBbsNewHours = int.Parse(dr["MOVIE_BBS_NEW_HOURS"].ToString());
					RxMailNewHours = int.Parse(dr["RX_MAIL_NEW_HOURS"].ToString());
					supportChgObjCatFlag = int.Parse(dr["SUPPORT_CHG_OBJ_CAT_FLAG"].ToString());
					pageTitle = dr["PAGE_TITLE"].ToString();
					pageKeyword = dr["PAGE_KEYWORD"].ToString();
					pageDescription = dr["PAGE_DESCRIPTION"].ToString();
					pageTitleOriginal = dr["PAGE_TITLE"].ToString();
					pageKeywordOriginal = dr["PAGE_KEYWORD"].ToString();
					pageDescriptionOriginal = dr["PAGE_DESCRIPTION"].ToString();
					cssDocomoOriginal = dr["CSS_DOCOMO"].ToString();
					cssAuOriginal = dr["CSS_AU"].ToString();
					cssSoftbankOriginal = dr["CSS_SOFTBANK"].ToString();
					cssiPhoneOriginal = dr["CSS_IPHONE"].ToString();
					cssAndroidOriginal = dr["CSS_ANDROID"].ToString();
					cssArrayDocomo = new string[] { cssDocomoOriginal };
					cssArrayAu = new string[] { cssAuOriginal };
					cssArraySoftbank = new string[] { cssSoftbankOriginal };
					cssArrayiPhone = new string[] { cssiPhoneOriginal };
					cssArrayAndroid = new string[] { cssAndroidOriginal };
					headerHtmlDoc = dr["HEADER_HTML_DOC"].ToString();
					footerHtmlDoc = dr["FOOTER_HTML_DOC"].ToString();
					supportPremiumTalkFlag = int.Parse(dr["SUPPORT_PREMIUM_TALK_FLAG"].ToString());
					supportKddiTvTelFlag = int.Parse(dr["SUPPORT_KDDI_TV_TEL_FLAG"].ToString());
					supportChgMonitorToTalkFlag = int.Parse(dr["SUPPORT_CHG_MONITOR_TO_TALK"].ToString());
					supportVoiceMonitorFlag = int.Parse(dr["SUPPORT_VOICE_MONITOR_FLAG"].ToString());
					castMultiCtlSiteFlag = int.Parse(dr["CAST_MULTI_CTL_SITE_FLAG"].ToString());
					bbsDupChargeFlag = int.Parse(dr["BBS_DUP_CHARGE_FLAG"].ToString());
					attachedMailDupChargeFlag = int.Parse(dr["ATTACHED_MAIL_DUP_CHARGE_FLAG"].ToString());
					useAttachedMailFlag = int.Parse(dr["USE_ATTACHED_MAIL_FLAG"].ToString());
					registHandleNmInputFlag = int.Parse(dr["REGIST_HANDLE_NM_INPUT_FLAG"].ToString());
					registBirthdayInputFlag = int.Parse(dr["REGIST_BIRTHDAY_INPUT_FLAG"].ToString());
					castRegistNeedAgreeFlag = int.Parse(dr["CAST_REGIST_NEED_AGREE_FLAG"].ToString());
					userInfoUrl = dr["USER_INFO_URL"].ToString();
					parentWebNm = dr["PARENT_WEB_NM"].ToString();
					parentWebTopPageUrl = dr["PARENT_WEB_TOP_PAGE_URL"].ToString();
					parentWebLoginUrl = dr["PARENT_WEB_LOGIN_URL"].ToString();
					parentWebMyPageUrl = dr["PARENT_WEB_MY_PAGE_URL"].ToString();
					profilePageingFlag = int.Parse(dr["PROFILE_PAGEING_FLAG"].ToString());
					profilePreviousGuidance = dr["PROFILE_PREVIOUS_GUIDANCE"].ToString();
					profileNextGuidance = dr["PROFILE_NEXT_GUIDANCE"].ToString();
					profilePageingFlag2 = int.Parse(dr["PROFILE_PAGEING_FLAG2"].ToString());
					profilePreviousGuidance2 = dr["PROFILE_PREVIOUS_GUIDANCE2"].ToString();
					profileNextGuidance2 = dr["PROFILE_NEXT_GUIDANCE2"].ToString();
					previousAccessKey = dr["PREVIOUS_ACCESS_KEY"].ToString();
					nextAccessKey = dr["NEXT_ACCESS_KEY"].ToString();
					topPageSeekMode = int.Parse(dr["TOP_PAGE_SEEK_MODE"].ToString());
					castCanSelectMonitorFlag = int.Parse(dr["CAST_CAN_SELECT_MONITOR_FLAG"].ToString());
					castCanSelectConnectType = int.Parse(dr["CAST_CAN_SELECT_CONNECT_TYPE"].ToString());
					charNoItemNm = dr["CHAR_NO_ITEM_NM"].ToString();
					if (charNoItemNm.Equals("")) {
						charNoItemNm = "nocrypt";
					}
					jobOfferSiteHostNm = dr["JOB_OFFER_SITE_HOST_NM"].ToString();
					jobOfferStartDoc = dr["JOB_OFFER_START_DOC"].ToString();
					jobOfferSiteType = dr["JOB_OFFER_SITE_TYPE"].ToString();
					pointPrice = int.Parse(dr["POINT_PRICE"].ToString());
					pointTax = float.Parse(dr["POINT_TAX"].ToString());
					usedChargeSettleFlag = int.Parse(dr["USED_CHARGE_SETTLE_FLAG"].ToString());
					NonAdRegistReqAgeCertFlag = int.Parse(dr["NON_AD_REGIST_REQ_AGECERT_FLAG"].ToString());
					userTopId = dr["USER_TOP_ID"].ToString();
					nonUserTopId = dr["NON_USER_TOP_ID"].ToString();
					mobileManDetailListCount = int.Parse(dr["MOBILE_MAN_DETAIL_LIST_COUNT"].ToString());
					mobileManListCount = int.Parse(dr["MOBILE_MAN_LIST_COUNT"].ToString());
					mobileWomanDetailListCount = int.Parse(dr["MOBILE_WOMAN_DETAIL_LIST_COUNT"].ToString());
					mobileWomanListCount = int.Parse(dr["MOBILE_WOMAN_LIST_COUNT"].ToString());
					bbsPicAttrFlag = int.Parse(dr["BBS_PIC_ATTR_FLAG"].ToString());
					bbsMovieAttrFlag = int.Parse(dr["BBS_MOVIE_ATTR_FLAG"].ToString());
					zeroSettleServicePoint = int.Parse(dr["ZERO_SETTLE_SERVICE_POINT"].ToString());
					googleVerification = dr["GOOGLE_VERIFICATION"].ToString();
					mailSearchEnabled = int.Parse(dr["MAIL_SEARCH_ENABLED"].ToString());
					mailSearchLimit = int.Parse(dr["MAIL_SEARCH_LIMIT"].ToString());
					castWriteBbsLimit = int.Parse(dr["CAST_WRITE_BBS_LIMIT"].ToString());
					castWriteIntervalMin = int.Parse(dr["CAST_WRITE_BBS_INTERVAL_MIN"].ToString());
					manWriteBbsLimit = int.Parse(dr["MAN_WRITE_BBS_LIMIT"].ToString());
					manWriteIntervalMin = int.Parse(dr["MAN_WRITE_BBS_INTERVAL_MIN"].ToString());
					payManWriteBbsLimit = int.Parse(dr["PAY_MAN_WRITE_BBS_LIMIT"].ToString());
					payManWriteIntervalMin = int.Parse(dr["PAY_MAN_WRITE_BBS_INTERVAL_MIN"].ToString());
					loginMailNotSendFlag = int.Parse(dr["LOGIN_MAIL_NOT_SEND_FLAG"].ToString());
					loginMail2NotSendFlag = int.Parse(dr["LOGIN_MAIL2_NOT_SEND_FLAG"].ToString());
					logoutMailNotSendFlag = int.Parse(dr["LOGOUT_MAIL_NOT_SEND_FLAG"].ToString());
					manTalkEndSendMailFlag = int.Parse(dr["MAN_TALK_END_SEND_MAIL_FLAG"].ToString());
					castTalkEndSendMailFlag = int.Parse(dr["CAST_TALK_END_SEND_MAIL_FLAG"].ToString());
					manHandleDefaultNm = dr["MAN_DEFAULT_HANDLE_NM"].ToString();
					reBatchMailNaHour = int.Parse(dr["RE_BATCH_MAIL_NA_HOUR"].ToString());
					whitePlanNo = dr["WHITE_PLAN_NO"].ToString();
					impCastFirstLoginAct = dr["IMP_CAST_FIRST_LOGIN_ACT"].ToString();
					impManFirstLoginAct = dr["IMP_MAN_FIRST_LOGIN_ACT"].ToString();
					dummyTalkTxLoginMailFlag = int.Parse(dr["DUMMY_TALK_TX_LOGIN_MAIL_FLAG"].ToString());
					manAutoLoginResignedFlag = int.Parse(dr["MAN_AUTO_LOGIN_RESIGNED_FLAG"].ToString());
					castAutoLoginResignedFlag = int.Parse(dr["CAST_AUTO_LOGIN_RESIGNED_FLAG"].ToString());
					siteHtmlDocSexCheckFlag = int.Parse(dr["SITE_HTML_DOC_SEX_CHECK_FLAG"].ToString());
					ManRegistFuncLimitAdCd = dr["MAN_REGIST_FUNC_LIMIT_AD_CD"].ToString();

					//EX
					buyPointMailCommDays = iBridUtil.GetStringValue(dr["BUY_POINT_MAIL_COMM_DAYS"]);
					loveListProfileDays = iBridUtil.GetStringValue(dr["LOVE_LIST_PROFILE_DAYS"]);
					int.TryParse(iBridUtil.GetStringValue(dr["BBS_RES_SEARCH_LIMIT"]),out bbsResSearchLimit);
					//docomoMoneyTransferTel = iBridUtil.GetStringValue(dr["DOCOMO_MONEY_TRANSFER_TEL"]);
					//docomoMoneyTransferNm = iBridUtil.GetStringValue(dr["DOCOMO_MONEY_TRANSFER_NM"]);
					int.TryParse(iBridUtil.GetStringValue(dr["WANTED_APPLICANT_DEL_DAYS"]),out wantedApplicantDelDays);
					bRet = true;
				}
			}
		} finally {
			conn.Close();
		}
		return bRet;
	}


	public void SetSupplement() {
		DataSet ds;
		siteSupplement.Clear();
		try {
			conn = DbConnect("Site.SetSupplement");
			using (cmd = CreateSelectCommand("SELECT SUPPLEMENT_CD,SUPPLEMENT_VALUE FROM T_SITE_SUPPLEMENT WHERE SITE_CD = :SITE_CD",conn))
			using (ds = new DataSet())
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",siteCd);
				da.Fill(ds,"T_SITE_SUPPLEMENT");
				foreach (DataRow dr in ds.Tables[0].Rows) {
					siteSupplement.Add(dr["SUPPLEMENT_CD"],dr["SUPPLEMENT_VALUE"]);
				}
			}
		} finally {
			conn.Close();
		}
	}


	public string GetSupplement(string pCode) {
		return iBridUtil.GetStringValue(siteSupplement[pCode]);
	}

	public bool GetValue(string pSiteCd,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"T_SITE.SITE_CD				," +
								"T_SITE.SITE_NM				," +
								"T_SITE.MAILER_FTP_ID		," +
								"T_SITE.MAILER_FTP_PW		," +
								"T_SITE.MAIL_SEARCH_LIMIT	," +
								"T_SITE.TX_HI_MAILER_IP		," +
								"T_SITE.WEB_PHISICAL_DIR	," +
								"T_SITE.HOST_NM				," +
								"T_SITE.MOVIE_SITE_URL		," +
								"T_SITE_MANAGEMENT_EX.DOCOMO_MONEY_TRANSFER_TEL	," +
								"T_SITE_MANAGEMENT_EX.DOCOMO_MONEY_TRANSFER_NM	," +
								"T_SITE_MANAGEMENT_EX.CAST_FAVORIT_POINT ," +
								"T_SITE_MANAGEMENT_EX.WANTED_APPLICANT_DEL_DAYS, " +
								"T_SITE_MANAGEMENT_EX.FANCLUB_FEE_POINT ," +
								"T_SITE_MANAGEMENT_EX.REGIST_CAST_AGE_MIN	," +
								"T_SITE_MANAGEMENT_EX.FIRST_SETTLE_ENABLE_DAYS	" +
							"FROM " +
								"T_SITE ," +
								"T_SITE_MANAGEMENT_EX " +
							"WHERE " +
								"T_SITE.SITE_CD = :SITE_CD AND T_SITE.SITE_CD = T_SITE_MANAGEMENT_EX.SITE_CD (+)";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE");
					if (ds.Tables["T_SITE"].Rows.Count != 0) {
						dr = ds.Tables["T_SITE"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool GetValues(string pSiteCd,string pFields,ref string[] pValue) {
		DataSet ds;
		bool bExist = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX.BBS_OBJ_FREE_VIEW_START_DATE	,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX.BBS_OBJ_FREE_VIEW_END_DATE			");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("T_SITE													,	");
		oSqlBuilder.AppendLine("T_SITE_MANAGEMENT_EX										");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("T_SITE.SITE_CD = :SITE_CD								AND	");
		oSqlBuilder.AppendLine("T_SITE.SITE_CD = T_SITE_MANAGEMENT_EX.SITE_CD		(+)		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		
		ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (ds.Tables[0].Rows.Count > 0) {
			bExist = true;

			string[] sFields = pFields.Split(',');
			pValue = new string[sFields.Length];

			for (int i = 0;i < sFields.Length;i++) {
				pValue[i] = ds.Tables[0].Rows[0][sFields[i]].ToString();
			}
		}
		
		return bExist;
	}
}
