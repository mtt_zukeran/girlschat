﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理会社設定

--	Progaram ID		: ManageCompany
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class ManageCompany:DbSession {

	public int manUsedEasyLoginFlag;
	public int womanUsedEasyLoginFlag;
	public int manGetTermIdFlag;
	public int womanGetTermIdFlag;

	public ManageCompany() {
		manUsedEasyLoginFlag = 0;
		womanUsedEasyLoginFlag = 0;
		manGetTermIdFlag = 0;
		womanGetTermIdFlag = 0;
	}

	public bool GetOne() {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"MAN_GET_TERM_ID_FLAG		," +
								"WOMAN_GET_TERM_ID_FLAG		," +
								"MAN_USED_EASY_LOGIN		," +
								"WOMAN_USED_EASY_LOGIN		" +
							"FROM " +
								"T_MANAGE_COMPANY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGE_COMPANY");
					if (ds.Tables["T_MANAGE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_MANAGE_COMPANY"].Rows[0];

						manUsedEasyLoginFlag = int.Parse(dr["MAN_USED_EASY_LOGIN"].ToString());
						womanUsedEasyLoginFlag = int.Parse(dr["WOMAN_USED_EASY_LOGIN"].ToString());
						manGetTermIdFlag = int.Parse(dr["MAN_GET_TERM_ID_FLAG"].ToString());
						womanGetTermIdFlag = int.Parse(dr["WOMAN_GET_TERM_ID_FLAG"].ToString());

						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public bool GetValue(string pItem,out string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"AUTO_GET_TEL_INFO_FLAG 	," +
								"CAST_SELF_MINIMUM_PAYMENT	," +
								"CAST_AUTO_MINIMUM_PAYMENT	," +
								"SETTLE_COMPANY_MASK		," +
								"COMPANY_MASK				," +
								"NOT_PAY_EXPIRE_MONTH		," +
								"MAN_GET_TERM_ID_FLAG		," +
								"WOMAN_GET_TERM_ID_FLAG		," +
								"MAN_USED_EASY_LOGIN		," +
								"WOMAN_USED_EASY_LOGIN		" +
							"FROM " +
								"T_MANAGE_COMPANY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGE_COMPANY");
					if (ds.Tables["T_MANAGE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_MANAGE_COMPANY"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsAvailableService(ulong pService) {
		return IsAvailableService(pService,string.Empty);
	}
	public bool IsAvailableService(ulong pService,int pNo) {
		return IsAvailableService(pService,pNo.ToString());
	}
	private bool IsAvailableService(ulong pService,string pNo) {
		DataSet ds;
		DataRow dr;
		bool bOk = false;
		string sColumnsNm = string.Format("RELEASE_PROGRAM_MASK{0}",pNo);
		try {
			conn = DbConnect();
			ulong uMask = 0;
			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT	");
			oSql.AppendLine(sColumnsNm);
			oSql.AppendLine("FROM	");
			oSql.AppendLine("	T_MANAGE_COMPANY	");

			using (cmd = CreateSelectCommand(oSql.ToString(),conn))
			using (ds = new DataSet()) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGE_COMPANY");

					if (ds.Tables["T_MANAGE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_MANAGE_COMPANY"].Rows[0];
						uMask = ulong.Parse(iBridUtil.GetStringValue(dr[sColumnsNm]));
						bOk = ((pService & uMask) > 0L);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bOk;
	}

	[System.ThreadStatic]
	private static int? _companyMask = null;
	public static bool IsCompany(int pMask) {

		if (_companyMask == null) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				string sMask = string.Empty;
				if (oManageCompany.GetValue("COMPANY_MASK",out sMask)) {
					_companyMask = int.Parse(sMask);
				}
			}
		}
		if ((_companyMask & pMask) > 0) {
			return true;
		}

		return false;
	}
}
