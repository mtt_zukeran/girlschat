﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サイト別決済方法
--	Progaram ID		: SiteSettle
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

[System.Serializable]
public class SiteSettle:DbSession {


	public string cpIdNo;
	public string cpPassword;
	public string settleUrl;
	public string continueSettleUrl;
	public string backUrl;
	public string subSettleUrl;
	public int notCreateSettleReqLogFlag;

	public SiteSettle() {
		cpIdNo = "";
		cpPassword = "";
		settleUrl = "";
		continueSettleUrl = "";
		backUrl = "";
		notCreateSettleReqLogFlag = 0;
	}

	public bool GetOne(string pSiteCd,string pSettleCompanyCd,string pSettleType) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("SiteSettle.GetOne");

			using (cmd = CreateSelectCommand("SELECT CP_ID_NO,CP_PASSWORD,SETTLE_URL,CONTINUE_SETTLE_URL,BACK_URL,SUB_SETTLE_URL,NOT_CREATE_SETTLE_REQ_LOG_FLAG FROM T_SITE_SETTLE WHERE SITE_CD = :SITE_CD AND SETTLE_COMPANY_CD =:SETTLE_COMPANY_CD AND SETTLE_TYPE =:SETTLE_TYPE  ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_COMPANY_CD",pSettleCompanyCd);
				cmd.Parameters.Add("SETTLE_TYPE",pSettleType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SETTLE_COMPANY");
					if (ds.Tables["T_SETTLE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_SETTLE_COMPANY"].Rows[0];
						cpIdNo = dr["CP_ID_NO"].ToString();
						cpPassword = dr["CP_PASSWORD"].ToString();
						settleUrl = dr["SETTLE_URL"].ToString();
						continueSettleUrl = dr["CONTINUE_SETTLE_URL"].ToString();
						backUrl = dr["BACK_URL"].ToString();
						subSettleUrl = dr["SUB_SETTLE_URL"].ToString();
						notCreateSettleReqLogFlag = int.Parse(dr["NOT_CREATE_SETTLE_REQ_LOG_FLAG"].ToString());
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
