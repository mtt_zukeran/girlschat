﻿using System;
using iBridCommLib;
using ViComm;
using System.Text;
using System.Data;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseLottery(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$GAME_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sValue);
				break;
			case "$GAME_LOTTERY_SEQ":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_SEQ",out sValue);
				break;
			case "$GAME_LOTTERY_ITEM_GET_FLAG":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_ITEM_GET_FLAG",out sValue);
				break;
			case "$RARE_ITEM_FLAG":
				this.parseWoman.SetFieldValue(pTag,"RARE_ITEM_FLAG",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
}