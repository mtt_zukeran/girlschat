﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップオブジェ コメント検索結果
--	Progaram ID		: PickupObjsComment
--  Creation Date	: 2013.05.28
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParsePickupObjsComment(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$MAN_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$PICKUP_OBJS_COMMENT_SEQ":
				this.parseWoman.SetFieldValue(pTag,"PICKUP_OBJS_COMMENT_SEQ",out sValue);
				break;
			case "$PICKUP_ID":
				this.parseWoman.SetFieldValue(pTag,"PICKUP_ID",out sValue);
				break;
			case "$OBJ_SEQ":
				this.parseWoman.SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;
			case "$COMMENT_TEXT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_TEXT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$COMMENT_DATE":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_DATE",out sValue);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$MAN_NG_SKULL_COUNT":
				this.parseWoman.SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			case "$MAN_LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}