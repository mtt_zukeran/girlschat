﻿using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
    private string ParseCastRegistedSiteList(string pTag,string pArgument,out bool pParsed) {
        pParsed = true;
        string sValue = "";

        switch (pTag) {
            case "$DIV_SELECT_CHARACTER":
                SetNoneDisplay(!CanSelectCharacter(pTag,pArgument));
                break;
            case "$DIV_HAVE_MULTI_CHARACTER":
                SetNoneDisplay(!IsHaveMultiCharacter(pTag));
                break;
            case "$DIV_HAVE_NO_MULTI_CHARACTER":
                SetNoneDisplay(IsHaveMultiCharacter(pTag));
                break;
            case "$DIV_NEED_SETUP_CHARACTER":
                SetNoneDisplay(!IsNeedSetupCharacter(pTag));
                break;
            case "$DIV_NO_NEED_SETUP_CHARACTER":
                SetNoneDisplay(IsNeedSetupCharacter(pTag));
                break;
            case "$REGISTED_SITE_CD":
                SetFieldValue(pTag,"SITE_CD",out sValue);
                break;
            case "$REGISTED_SITE_CHAR_NO":
                SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
                break;
            case "$REGISTED_SITE_COMMENT_LIST":
                sValue = GetSiteCommentList(pTag,pArgument);
                break;
            case "$REGISTED_SITE_HANDLE_NM":
                SetFieldValue(pTag,"HANDLE_NM",out sValue);
                break;
            case "$REGISTED_SITE_MAN_MAIL_UNREAD_COUNT":
                sValue = GetSiteMailUnreadCount(pTag,pArgument);
                break;
            case "$REGISTED_SITE_NM":
                SetFieldValue(pTag,"SITE_NM",out sValue);
                break;
            case "$REGISTED_SITE_NEED_PROFILE_FLAG":
                if (IsNeedSetupCharacter(pTag)) {
                    sValue = ViCommConst.FLAG_ON_STR;
                } else {
                    sValue = ViCommConst.FLAG_OFF_STR;
                }
                break;
            case "$REGISTED_SITE_PROFILE_PIC_OK_FLAG":
                SetFieldValue(pTag,"PIC_SEQ",out sValue);
                sValue = sValue.Equals("0") ? "0" : "1";
                break;
            default:
                pParsed = false;
                break;
        }
        return sValue;
    }


    private string GetSiteCommentList(string pTag,string pArgument) {
        string sText = pArgument + ViCommConst.MAIN_CHAR_NO;
        if (sessionWoman.userWoman.characterList.ContainsKey(sText)) {
            return parseContainer.Parse(sessionWoman.userWoman.characterList[sText].commentList);
        } else {
            return "";
        }
    }
    private string GetSiteMailUnreadCount(string pTag,string pArgument) {
        string sText = pArgument + ViCommConst.MAIN_CHAR_NO;
        if (sessionWoman.userWoman.characterList.ContainsKey(sText)) {
            return sessionWoman.userWoman.characterList[sText].unreadMailCount.ToString();
        } else {
            return "0";
        }
    }

    private bool CanSelectCharacter(string pTag,string pArgument) {
        bool bExist = false;
        string sText = pArgument + ViCommConst.MAIN_CHAR_NO;
        if (sessionWoman.userWoman.characterList.ContainsKey(sText)) {
            if (sessionWoman.userWoman.characterList[sText].characterOnlineStatus == ViCommConst.USER_WAITING) {
                bExist = IsHaveMultiCharacter(pTag);
            }
        }
        return bExist;
    }

    private bool IsHaveMultiCharacter(string pTag) {
        string sValue;
        if (GetValue(pTag,"SITE_CD",out sValue)) {
            using (Cast oCast = new Cast()) {
                return oCast.IsHaveMultiCharacter(sValue,sessionWoman.userWoman.userSeq);
            }
        } else {
            return false;
        }
    }

    private bool IsNeedSetupCharacter(string pTag) {
        string sValue;
        if (GetValue(pTag,"NA_FLAG",out sValue)) {
			if (sValue.Equals(ViCommConst.NA_CHAR_NONE.ToString()) || sValue.Equals(ViCommConst.NA_CHAR_ADMIN.ToString())) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
