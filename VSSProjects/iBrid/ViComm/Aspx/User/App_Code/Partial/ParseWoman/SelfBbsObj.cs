﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseSelfBbsObj(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		string sObjKind = "";
		string sObjSeq = string.Empty;

		switch (pTag) {
			case "$SELF_OBJ_ATTR_NM":
				sValue = GetSelfObjAttrNm(pTag);
				break;
			case "$SELF_OBJ_ATTR_TYPE_NM":
				sValue = GetSelfObjAttrTypeNm(pTag);
				break;
			case "$SELF_OBJ_AUTH_MARK":
				SetFieldValue(pTag,"UNAUTH_MARK",out sValue);
				break;
			case "$SELF_OBJ_UNAUTH_FLAG":
				SetFieldValue(pTag,"OBJ_NOT_APPROVE_FLAG",out sValue);
				break;
			case "$SELF_OBJ_DOC":
				sValue = GetDoc(pTag,"OBJ_DOC");
				break;
			case "$SELF_OBJ_TITLE":
				SetFieldValue(pTag,"OBJ_TITLE",out sValue);
				break;
			case "$SELF_OBJ_UPLOAD_DATE":
				SetFieldValue(pTag,"OBJ_UPLOAD_DATE",out sValue);
				break;
			case "$SELF_OBJ_VIEW_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$DIV_IS_PIC":
				if (GetValue(pTag,"OBJ_KIND",out sObjKind)) {
					SetNoneDisplay(!(sObjKind.Equals(ViCommConst.BbsObjType.PIC)));
				}
				break;
			case "$DIV_IS_MOVIE":
				if (GetValue(pTag,"OBJ_KIND",out sObjKind)) {
					SetNoneDisplay(!(sObjKind.Equals(ViCommConst.BbsObjType.MOVIE)));
				}
				break;
			case "$QUERY_VIEW_SELF_BBS_OBJ":
				sValue = GetQueryViewSelfBbsObj(pTag);
				break;
			case "$SELF_OBJ_PLANNING_TYPE":
				SetFieldValue(pTag,"PLANNING_TYPE",out sValue);
				break;
			case "$SELF_OBJ_DELETABLE_FLAG":
				string sAttrSeq = string.Empty;
				if (GetValue(pTag,"CAST_OBJ_ATTR_SEQ",out sAttrSeq)) {
					using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
						sValue = oCastPicAttrTypeValue.IsDeletable(sessionWoman.site.siteCd,sAttrSeq) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
					}
				}
				break;
			// ピックアップ（特集）
			case "$DIV_IS_PICKUP_OBJ":
				if (GetValue(pTag,"OBJ_SEQ",out sObjSeq)) {
					// ピックアップ（特集）の対象である場合は表示、それ以外は非表示
					PickupObjs oPickupObjs = new PickupObjs();
					DataSet ds = oPickupObjs.GetPickupInfoByObjSeq(sessionWoman.site.siteCd,sObjSeq,true);
					bool bFlag = (ds == null || ds.Tables[0].Rows.Count <= 0);
					SetNoneDisplay(bFlag);
				} else {
					// 非表示
					SetNoneDisplay(true);
				}
				break;
			case "$PICKUP_NM":
				if (GetValue(pTag,"OBJ_SEQ",out sObjSeq)) {
					// ピックアップ（特集）名
					PickupObjs oPickupObjs = new PickupObjs();
					DataSet ds = oPickupObjs.GetPickupInfoByObjSeq(sessionWoman.site.siteCd,sObjSeq,true);
					sValue = oPickupObjs.ConcatPickupTitle(ds,"<br/>");
				}
				break;

			//PICのみ使用可能-------------------------------------------------------------------
			case "$SELF_PIC_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag,"OBJ_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_SMALL_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag, "OBJ_SMALL_BLUR_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_PIC_SEQ":
				SetFieldValue(pTag,"PIC_SEQ",out sValue);
				break;

			//MOVIEのみ使用可能-------------------------------------------------------------------
			case "$HREF_SELF_MOVIE_DOWNLOAD":
				sValue = GetHRefSelfMovieDwonload(pTag,pArgument);
				break;
			case "$SELF_MOVIE_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;

			case "$OBJ_SEQ":
				SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;

			//レビュー
			case "$REVIEW_COUNT":
				SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = GetGoodStarAvrX10(pTag);
				break;
			case "$OBJ_RANKING_FLAG":
				SetFieldValue(pTag,"OBJ_RANKING_FLAG",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetSelfObjAttrNm(string pTag) {
		string sObjAttrNm = "";
		string[] sValues;
		if (GetValues(pTag,"CAST_OBJ_ATTR_SEQ,CAST_OBJ_ATTR_NM",out sValues)) {
			if (sValues[0].Equals(ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString())) {
				sObjAttrNm = "指定なし";
			} else {
				sObjAttrNm = parseContainer.Parse(sValues[1]);
			}
		}
		return sObjAttrNm;
	}

	private string GetSelfObjAttrTypeNm(string pTag) {
		string sObjAttrTypeNm = "";
		string[] sValues;
		if (GetValues(pTag,"CAST_OBJ_ATTR_TYPE_SEQ,CAST_OBJ_ATTR_TYPE_NM",out sValues)) {
			if (sValues[0].ToString().Equals(ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString())) {
				sObjAttrTypeNm = "指定なし";
			} else {
				sObjAttrTypeNm = parseContainer.Parse(sValues[1]);
			}
		}
		return sObjAttrTypeNm;
	}

	private string GetQueryViewSelfBbsObj(string pTag) {
		string sValue;
		SetFieldValue(pTag,"OBJ_KIND",out sValue);
		if (sValue.Equals(ViCommConst.BbsObjType.PIC)) {
			return GetQueryViewSelfBbsPic(pTag);
		} else if (sValue.Equals(ViCommConst.BbsObjType.MOVIE)) {
			return GetQueryViewSelfBbsMovie(pTag);
		} else {
			return string.Empty;
		}
	}

	public string GetGoodStarAvr(string pTag) {
		double dGoodStarAvr = 0;
		string sGoodStarTotal,sReviewCount;
		float fGoodStarTotal,fReviewCount;

		SetFieldValue(pTag,"GOOD_STAR_TOTAL",out sGoodStarTotal);
		SetFieldValue(pTag,"REVIEW_COUNT",out sReviewCount);

		if (float.TryParse(sGoodStarTotal,out fGoodStarTotal) && float.TryParse(sReviewCount,out fReviewCount)) {
			if (fGoodStarTotal > 0 && fReviewCount > 0) {
				dGoodStarAvr = fGoodStarTotal / fReviewCount;
				dGoodStarAvr = iBridUtil.RoundOff(dGoodStarAvr,1);
			}
		}

		return dGoodStarAvr.ToString();
	}

	public string GetGoodStarAvrX10(string pTag) {
		float fResult = 0;
		float fAvr;
		if (float.TryParse(GetGoodStarAvr(pTag),out fAvr)) {
			fResult = fAvr * 10;
		}
		return fResult.ToString();
	}
}

