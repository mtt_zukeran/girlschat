﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者画像検索結果
--	Progaram ID		: CastPic
--  Creation Date	: 2013.05.28
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseCastPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_PF_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_PIC_ATTR_TYPE_NM":
				this.parseWoman.SetFieldValue(pTag,"CAST_PIC_ATTR_TYPE_NM",out sValue);
				break;
			case "$CAST_PIC_ATTR_NM":
				this.parseWoman.SetFieldValue(pTag,"CAST_PIC_ATTR_NM",out sValue);
				break;
			case "$CAST_PIC_BLUR_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"OBJ_BLUR_PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_PIC_DOC":
				this.parseWoman.SetFieldValue(pTag,"PIC_DOC",out sValue);
				break;
			case "$CAST_PIC_LARGE_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				break;
			case "$OBJ_SMALL_BLUR_PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"OBJ_SMALL_BLUR_PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_PIC_TITLE":
				this.parseWoman.SetFieldValue(pTag,"PIC_TITLE",out sValue);
				break;
			case "$CAST_PIC_UPLOAD_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.GetQueryCastProfileCastPic(pTag);
				break;
			case "$CAST_COMMENT_DETAIL":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_DETAIL",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_LIST":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_PIC_READING_COUNT":
				this.parseWoman.SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;

			//レビュー
			case "$REVIEW_COUNT":
				SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = parseWoman.GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = parseWoman.GetGoodStarAvrX10(pTag);
				break;

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetCastItem(string pTag,string pArgument) {
		string sValue;
		this.parseWoman.SetFieldValue(pTag,"CAST_ATTR_VALUE" + pArgument,out sValue);
		return sValue;
	}

	public string GetQueryCastProfileCastPic(string pTag) {
		string[] sValues;
		if (this.parseWoman.GetValues(pTag,"LOGIN_ID",out sValues)) {
			return string.Format("loginid={0}&direct=1",sValues[0]);
		} else {
			return string.Empty;
		}
	}
}
