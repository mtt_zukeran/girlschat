﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやきコメント検索結果
--	Progaram ID		: ManTweetComment
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseManTweetComment(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_TWEET_COMMENT_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_TWEET_COMMENT_SEQ",out sValue);
				break;
			case "$CAST_TWEET_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_TWEET_SEQ",out sValue);
				break;
			case "$COMMENT_TEXT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_TEXT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$COMMENT_DATE":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_DATE",out sValue);
				break;
			case "$MAN_TWEET_USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"MAN_TWEET_USER_SEQ",out sValue);
				break;
			case "$MAN_TWEET_USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"MAN_TWEET_USER_CHAR_NO",out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.GetQueryCastProfile(pTag);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	public string GetQueryCastProfile(string pTag) {
		string[] sValues;
		if (parseWoman.GetValues(pTag,"ACT_CATEGORY_IDX,LOGIN_ID,RNUM,CRYPT_VALUE",out sValues)) {
			return string.Format("seekmode={0}&category={1}&loginid={2}&userrecno={3}&{4}={5}",
							   parseWoman.sessionWoman.seekMode,sValues[0],sValues[1],sValues[2],parseWoman.sessionWoman.site.charNoItemNm,sValues[3]
					);
		} else {
			return "";
		}
	}
}