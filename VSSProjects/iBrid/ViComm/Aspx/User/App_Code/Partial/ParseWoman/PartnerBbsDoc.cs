﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;

partial class ParseWoman {
	private string ParseManBbsDoc(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$MAN_AGE":
				SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_BBS_READ_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$MAN_BBS_TITLE":
				SetFieldValue(pTag,"BBS_TITLE",out sValue);
				break;
			case "$MAN_BBS_WRITE_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$MAN_BBS_WRITE_DOC":
				sValue = GetDoc(pTag,"BBS_DOC");
				break;
			case "$MAN_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_IMG":
				sValue = GetManFavoritImg(pTag,pArgument);
				break;
			case "$MAN_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				sValue = ManHandleNmEmptyFill(sValue);
				break;
			case "$MAN_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_IMG_MARK":
				sValue = GetManImgMark(pTag,pArgument);
				break;
			case "$MAN_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_ITEM":
				sValue = GetManItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$MAN_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$MAN_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$MAN_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$QUERY_VIEW_MAN_BBS_DOC":
				sValue = GetQueryViewManBbsDoc(pTag);
				break;
			case "$MAN_TOTAL_TALK_COUNT":
				GetValue("$MAN_TOTAL_TALK_COUNT",ViCommConst.DATASET_MAN,"TOTAL_TALK_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_TX_MAIL_COUNT":
				GetValue("$MAN_TOTAL_TX_MAIL_COUNT",ViCommConst.DATASET_MAN,"TOTAL_TX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_RX_MAIL_COUNT":
				GetValue("$MAN_TOTAL_RX_MAIL_COUNT",ViCommConst.DATASET_MAN,"TOTAL_RX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_LAST_RECEIPT_DATE": {
					sValue = this.GetManLastReceiptDate(pTag);
				}
				break;
			case "$MAN_LAST_RECEIPT_PAST_DAYS": {
					DateTime dtLastPeceipt;
					if (DateTime.TryParse(this.GetManLastReceiptDate(pTag),out dtLastPeceipt)) {
						sValue = ((TimeSpan)(DateTime.Today - dtLastPeceipt.Date)).Days.ToString();
					} else {
						sValue = string.Empty;
					}
				}
				break;
			case "$MAN_NG_SKULL_COUNT":
				SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			case "$GET_MAN_HANDLE_NM_BY_LOGIN_ID":
				sValue = GetManHandleNmByQueryLoginId(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryViewManBbsDoc(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,BBS_SEQ",out sValues)) {
			return string.Format("pageno={0}&loginid={1}&bbsseq={2}",
						sValues[0],
						iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["loginid"]),
						sValues[1]);
		} else {
			return "";
		}
	}
}
