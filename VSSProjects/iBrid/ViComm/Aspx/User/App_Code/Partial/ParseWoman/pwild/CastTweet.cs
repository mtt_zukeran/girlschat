﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルつぶやき検索結果
--	Progaram ID		: CastTweet
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseCastTweet(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_TWEET_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_TWEET_SEQ",out sValue);
				break;
			case "$TWEET_TEXT":
				this.parseWoman.SetFieldValue(pTag,"TWEET_TEXT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$TWEET_DATE":
				this.parseWoman.SetFieldValue(pTag,"TWEET_DATE",out sValue);
				break;
			case "$LIKE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$CAST_TWEET_PIC_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_TWEET_PIC_SEQ",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_TWEET_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"CAST_TWEET_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_TWEET_SMALL_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"CAST_TWEET_SMALL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_TWEET_PIC_UPLOAD_ADDR":
				sValue = this.GetCastTweetPicUploadAddr(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
	
	private string GetCastTweetPicUploadAddr(string pTag,string pArgument) {
		string sAddr = string.Empty;
		string[] sValues;
		if (this.parseWoman.GetValues(pTag,"LOGIN_ID,USER_CHAR_NO,CAST_TWEET_SEQ",out sValues)) {
			sAddr = string.Format("{0}{1}{2}{3}_{4}@{5}",
				ViCommConst.TWEET_URL_HEADER2,
				parseWoman.sessionWoman.site.siteCd,
				sValues[0],
				sValues[1],
				sValues[2],
				parseWoman.sessionWoman.site.mailHost
			);
		}
		
		return sAddr;
	}

	private string GetCastTweet(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		CastTweetSeekCondition oCondition = new CastTweetSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.SelfUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.SelfUserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.OnlyFanClubFlag = ViCommConst.FLAG_OFF_STR;
		
		using (CastTweet oCastTweet = new CastTweet(parseWoman.sessionObjs)) {
			oTmpDataSet = oCastTweet.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_CAST_TWEET);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
}
