﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやき検索結果
--	Progaram ID		: ManTweet
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseManTweet(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$MAN_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_NEW_FLAG":
				this.parseWoman.SetFieldValue(pTag,"NEW_MAN_FLAG",out sValue);
				break;
			case "$MAN_RICHINO_RANK":
				this.parseWoman.SetFieldValue(pTag,"RICHINO_RANK",out sValue);
				break;
			case "$MAN_TWEET_SEQ":
				this.parseWoman.SetFieldValue(pTag,"MAN_TWEET_SEQ",out sValue);
				break;
			case "$TWEET_TEXT":
				this.parseWoman.SetFieldValue(pTag,"TWEET_TEXT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$TWEET_DATE":
				this.parseWoman.SetFieldValue(pTag,"TWEET_DATE",out sValue);
				break;
			case "$LIKE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$MAN_TWEET_PIC_SEQ":
				this.parseWoman.SetFieldValue(pTag,"MAN_TWEET_PIC_SEQ",out sValue);
				break;
			case "$MAN_TWEET_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"MAN_TWEET_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_TWEET_SMALL_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"MAN_TWEET_SMALL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_MAN_TWEET_LIKED_FLAG":
				this.parseWoman.SetFieldValue(pTag,"SELF_MAN_TWEET_LIKED_FLAG",out sValue);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$SELF_MAN_TWEET_READ_FLAG":
				this.parseWoman.SetFieldValue(pTag,"READ_FLAG",out sValue);
				break;
			case "$MAN_TWEET_UNAUTH_PIC_FLAG":
				this.parseWoman.SetFieldValue(pTag,"MAN_TWEET_UNAUTH_PIC_FLAG",out sValue);
				break;
			case "$MAN_NG_SKULL_COUNT":
				this.parseWoman.SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			case "$MAN_LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetManTweet(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManTweetExSeekCondition oCondition = new ManTweetExSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.AppendLikeCount = true;
		oCondition.AppendCommentCount = true;
		oCondition.AppendCastLikedFlag = true;
		oCondition.AppendCastReadFlag = true;

		using (ManTweetEx oManTweetEx = new ManTweetEx()) {
			oTmpDataSet = oManTweetEx.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_MAN_TWEET);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
}
