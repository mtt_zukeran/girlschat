﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやきｺﾒﾝﾄﾗﾝｷﾝｸﾞ設定 検索結果
--	Progaram ID		: ManTweetCommentTerm
--  Creation Date	: 2013.03.27
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseManTweetCommentTerm(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$TERM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"TERM_SEQ",out sValue);
				break;
			case "$START_DATE":
				this.parseWoman.SetFieldValue(pTag,"START_DATE",out sValue);
				break;
			case "$END_DATE":
				this.parseWoman.SetFieldValue(pTag,"END_DATE",out sValue);
				break;
			case "$LAST_TERM_FLAG":
				this.parseWoman.SetFieldValue(pTag,"LAST_TERM_FLAG",out sValue);
				break;
			case "$NEXT_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"NEXT_END_DATE",out sValue);
				break;
			case "$SELF_COMMENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"SELF_COMMENT_COUNT",out sValue);
				break;
			case "$SELF_RANK_NO":
				this.parseWoman.SetFieldValue(pTag,"SELF_RANK_NO",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}