﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;
using ViComm.Extension.Pwild;

partial class ParseWoman {
	private string ParseMan(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$MAN_AGE":
				SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_BAL_POINT":
				SetFieldValue(pTag,"BAL_POINT",out sValue);
				break;
			case "$MAN_BIRTHDAY":
				SetFieldValue(pTag,"BIRTHDAY",out sValue);
				break;
			case "$MAN_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_ME_COMMENT":
				SetFieldValue(pTag,"FAVORIT_COMMENT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$MAN_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_IMG":
				sValue = GetManFavoritImg(pTag,pArgument);
				break;
			case "$MAN_FAVORIT_REGIST_DATE":
				SetFieldValue(pTag,"FAVORIT_REGIST_DATE",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				sValue = ManHandleNmEmptyFill(sValue);
				break;
			case "$MAN_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_IMG_MARK":
				sValue = GetManImgMark(pTag,pArgument);
				break;
			case "$MAN_ITEM":
				sValue = GetManItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$MAN_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_LAST_LOGIN_DATE":
				SetFieldValue(pTag,"LAST_LOGIN_DATE",out sValue);
				break;
			case "$MAN_LAST_TALK_DATE":
				SetFieldValue(pTag,"TALK_START_DATE",out sValue);
				break;
			case "$MAN_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$MAN_MARKING_ME_DATE":
				SetFieldValue(pTag,"MARKING_DATE",out sValue);
				break;
			case "$MAN_NEW_FLAG":
				SetFieldValue(pTag,"NEW_MAN_FLAG",out sValue);
				break;
			case "$MAN_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$MAN_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$MAN_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$MAN_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$MAN_TOTAL_RECEIPT_AMT":
				SetFieldValue(pTag,"TOTAL_RECEIPT_AMT",out sValue);
				break;
			case "$MAN_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$MAN_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$MAN_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$MAN_REFUSE_ME_COMMENT":
				SetFieldValue(pTag,"REFUSE_COMMENT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$MAN_REGIST_DATE":
				SetFieldValue(pTag,"REGIST_DATE",out sValue);
				break;
			case "$MAN_TOTAL_TALK_COUNT":
				SetFieldValue(pTag,"TOTAL_TALK_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_TX_MAIL_COUNT":
				SetFieldValue(pTag,"TOTAL_TX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_RX_MAIL_COUNT":
				SetFieldValue(pTag,"TOTAL_RX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_TALK_COUNT_BY_SELF":
				SetFieldValue(pTag,"PARTNER_TOTAL_TALK_COUNT",out sValue);
				break;
			case "$MAN_TO_SELF_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_TX_MAIL_COUNT",out sValue);	// 男性基準のためtxを設定 
				break;
			case "$MAN_TV_TALK_OK_FLAG":
				sValue = IsEqual(pTag,IsTvTalkEnable(pTag) && !IsVoiceTalkEnable(pTag));
				break;
			case "$MAN_VOICE_TALK_OK_FLAG":
				sValue = IsEqual(pTag,!IsTvTalkEnable(pTag) && IsVoiceTalkEnable(pTag));
				break;
			case "$MAN_VOICE_TV_TALK_OK_FLAG":
				sValue = IsEqual(pTag,IsTvTalkEnable(pTag) && IsVoiceTalkEnable(pTag));
				break;
			case "$MAN_WAITING_COMMENT":
				SetFieldValue(pTag,"WAITING_COMMENT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$MAN_WAITING_END_SCH_DATE":
				SetFieldValue(pTag,"FOWARD_TERM_SCH_DATE",out sValue);
				break;
			case "$MAN_WAITING_START_DATE":
				SetFieldValue(pTag,"RECORDING_DATE",out sValue);
				break;
			case "$MAN_WAITING_TYPE":
				//SetFieldValue(pTag,"WAITING_TYPE_NM",out sValue);
				SetFieldValue(pTag,"MAN_PF_WAITING_TYPE_NM",out sValue);
				break;
			case "$MAN_JEALOUSY_LOGIN":
				SetFieldValue(pTag,"LOGIN_VIEW_STATUS",out sValue);
				break;
			case "$MAN_JEALOUSY_WAITING":
				SetFieldValue(pTag,"WAITING_VIEW_STATUS",out sValue);
				break;
			case "$MAN_JEALOUSY_BBS":
				SetFieldValue(pTag,"BBS_NON_PUBLISH_FLAG",out sValue);
				break;
			case "$MAN_JEALOUSY_RANKING":
				SetFieldValue(pTag,"RANKING_NON_PUBLISH_FLAG",out sValue);
				break;
			case "$MAN_JEALOUSY_BATCH":
				SetFieldValue(pTag,"NOT_SEND_BATCH_MAIL_FLAG",out sValue);
				break;
			case "$MAN_JEALOUSY_BLOG":
				SetFieldValue(pTag,"BLOG_NON_PUBLISH_FLAG",out sValue);
				break;
			case "$MAN_JEALOUSY_DIARY":
				SetFieldValue(pTag,"DIARY_NON_PUBLISH_FLAG",out sValue);
				break;
			case "$MAN_JEALOUSY_PROFILE_PIC":
				SetFieldValue(pTag,"PROFILE_PIC_NON_PUBLISH_FLAG",out sValue);
				break;
			case "$MAN_JEALOUSY_COMMENT_DETAIL":
				SetFieldValue(pTag,"USE_COMMENT_DETAIL_FLAG",out sValue);
				break;
			case "$MAN_MEMO_REVIEWS":
				SetFieldValue(pTag,"MEMO_REVIEWS",out sValue);
				break;
			case "$MAN_MEMO_DOC":
				SetFieldValue(pTag,"MEMO_DOC",out sValue);
				sValue = sValue.Replace("\r\n","<br />");
				break;
			case "$MAN_MEMO_UPDATE_DATE":
				SetFieldValue(pTag,"MEMO_UPDATE_DATE",out sValue);
				break;
			case "$MAN_BBS_WRITE_FLAG":
				sValue = GetBbsWriteFlag(pTag);
				break;
			case "$MAN_BBS_WRITE_NEW_FLAG":
				sValue = GetManBbsNewFlag(pTag).ToString();
				break;
			case "$SELF_TO_MAN_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_RX_MAIL_COUNT",out sValue);	// 男性基準のためrxを設定 
				break;
			case "$SELF_TO_MAN_LAST_MAIL_DATE":
				SetFieldValue(pTag,"PARTNER_LAST_RX_MAIL_DATE",out sValue);	// 男性基準のためrxを設定 
				break;
			case "$SELF_FAVORIT_MAN_MEMO":
				sValue = GetFavoritMemo(pTag);
				break;
			case "$SELF_REFUSE_MAN_REGIST_DATE":
				SetFieldValue(pTag,"REFUSE_REGIST_DATE",out sValue);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceWShot(pTag,pArgument);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$MAN_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$MAN_RICHINO_RANK":
				SetFieldValue(pTag,"RICHINO_RANK",out sValue);
				break;
			case "$MAN_LAST_RECEIPT_DATE": {
					sValue = this.GetManLastReceiptDate(pTag);
				}
				break;
			case "$MAN_LAST_RECEIPT_PAST_DAYS": {
					DateTime dtLastPeceipt;
					if (DateTime.TryParse(this.GetManLastReceiptDate(pTag),out dtLastPeceipt)) {
						sValue = ((TimeSpan)(DateTime.Today - dtLastPeceipt.Date)).Days.ToString();
					} else {
						sValue = string.Empty;
					}
				}
				break;
			case "$MAN_TALK_COUNT": {
					sValue = GetTalkCount(pTag); 
					
				}
				break;
			case "$MAN_DIARY_LIKE_DATE":
				SetFieldValue(pTag,"LIKE_DATE",out sValue);
				break;
			case "$MAN_FEATURE_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.DOCOMO,ViCommConst.KDDI,ViCommConst.SOFTBANK);
				break;
			case "$MAN_SMART_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID,ViCommConst.IPHONE);
				break;
			case "$MAN_ANDROID_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID);
				break;
			case "$MAN_IPHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.IPHONE);
				break;
			case "$MAN_NG_SKULL_COUNT":
				SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			case "$LIST_RNUM":
				SetFieldValue(pTag,"RNUM",out sValue);
				break;
			//足あとリスト
			case "$MARKING_REFERER_DISPLAY_NM":
				SetFieldValue(pTag,"MARKING_REFERER_DISPLAY_NM",out sValue);
				break;
			//お気に入りリスト
			case "$OFFLINE_LAST_UPDATE_DATE":
				SetFieldValue(pTag,"OFFLINE_LAST_UPDATE_DATE",out sValue);
				break;
			case "$SELF_LAST_ACTION_DATE":
				SetFieldValue(pTag,"SELF_LAST_ACTION_DATE",out sValue);
				break;
			//お気に入りグループ
			case "$FAVORIT_GROUP_NM":
				SetFieldValue(pTag,"FAVORIT_GROUP_NM",out sValue);
				break;
			case "$USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			//ファンクラブ
			case "$MEMBER_RANK":
				SetFieldValue(pTag,"MEMBER_RANK",out sValue);
				break;
			case "$MY_FANCLUB_MEMBER_RANK":
				string sUserSeq;
				if (GetValue(pTag,"USER_SEQ",out sUserSeq)) {
					sValue = GetMyFanClubMemberRank(sUserSeq);
				}
				break;
			//アタックリスト
			case "$SELF_COMMUNICATION_PARTS":
				sValue = this.CreateCommunicationList(pTag,pArgument);
				break;
			case "$SELF_COMMUNICATION_LIST_PARTS":
				sValue = this.CreateCommunicationList(pTag,pArgument);
				break;
			//会員ニュース
			case "$MAN_NEWS_DATE":
				SetFieldValue(pTag,"NEWS_DATE",out sValue);
				break;
			case "$MAN_NEWS_TYPE":
				SetFieldValue(pTag,"NEWS_TYPE",out sValue);
				break;
			//イイネ
			case "$LIKE_DATE":
				SetFieldValue(pTag,"LIKE_DATE",out sValue);
				break;
			//出演者動画コメント
			case "$CAST_MOVIE_COMMENT_SEQ":
				SetFieldValue(pTag,"CAST_MOVIE_COMMENT_SEQ",out sValue);
				break;
			case "$CAST_MOVIE_COMMENT_DOC":
				SetFieldValue(pTag,"COMMENT_DOC",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_MOVIE_COMMENT_DATE":
				SetFieldValue(pTag,"COMMENT_DATE",out sValue);
				break;
			case "$CAST_MOVIE_TYPE":
				SetFieldValue(pTag,"CAST_MOVIE_TYPE",out sValue);
				break;
			case "$CAST_MOVIE_UPLOAD_DATE":
				SetFieldValue(pTag,"CAST_MOVIE_UPLOAD_DATE",out sValue);
				break;
			case "$CAST_MOVIE_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;
			//出演者画像コメント
			case "$CAST_PIC_COMMENT_SEQ":
				SetFieldValue(pTag,"CAST_PIC_COMMENT_SEQ",out sValue);
				break;
			case "$CAST_PIC_COMMENT_DOC":
				SetFieldValue(pTag,"COMMENT_DOC",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_PIC_COMMENT_DATE":
				SetFieldValue(pTag,"COMMENT_DATE",out sValue);
				break;
			case "$CAST_PIC_TYPE":
				SetFieldValue(pTag,"CAST_PIC_TYPE",out sValue);
				break;
			case "$CAST_PIC_UPLOAD_DATE":
				SetFieldValue(pTag,"CAST_PIC_UPLOAD_DATE",out sValue);
				break;
			case "$CAST_PIC_SEQ":
				SetFieldValue(pTag,"PIC_SEQ",out sValue);
				break;
			//メールおねだり
			case "$MAIL_REQUEST_DATE":
				SetFieldValue(pTag,"MAIL_REQUEST_DATE",out sValue);
				break;
			case "$SELF_RESERVED_MAIL_REQUEST_COUNT":
				SetFieldValue(pTag,"RESERVED_MAIL_REQUEST_COUNT",out sValue);
				break;
			//LINE風メール
			case "$CHAT_MAIL_PARTS":
				sValue = this.GetChatMailPartsByUser(pTag,pArgument);
				break;
			//メールアタックイベント(一定期間ログインしていない男性)
			case "$MAN_TOTAL_RECEIPT_COUNT":
				SetFieldValue(pTag,"TOTAL_RECEIPT_COUNT",out sValue);
				break;
			case "$MAN_MAIL_ATTACK_LOGIN_RX_COUNT":
				SetFieldValue(pTag,"RX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_MAIL_ATTACK_LOGIN_TX_COUNT_24H":
				SetFieldValue(pTag,"TX_MAIL_COUNT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	public bool IsTvTalkEnable(string pTag) {
		return IsTalkEnable(pTag,ViCommConst.WAIT_TYPE_VOICE);
	}

	public bool IsVoiceTalkEnable(string pTag) {
		return IsTalkEnable(pTag,ViCommConst.WAIT_TYPE_VIDEO);
	}

	public bool IsTalkEnable(string pTag,string pNAWaiting) {
		string[] sValues;
		if (GetValues(pTag,"MAN_PF_WAITING_TYPE,CHARACTER_ONLINE_STATUS,MAN_PF_FOWARD_TERM_SCH_DATE,MAN_PF_RECORDING_DATE,MAN_PF_FOWARD_TERM_SCH_DATE",out sValues)) {
			string sWaittingType = sValues[0];
			string sOnlineStatus = sValues[1];
			if (sWaittingType.Equals("") ||
				sWaittingType.Equals(pNAWaiting) ||
				sOnlineStatus.Equals(ViCommConst.USER_TALKING.ToString()) ||
				sValues[2].Equals("")) {
				return false;
			}
			DateTime dtStartDate,dtTermDate;
			DateTime.TryParse(sValues[3],out dtStartDate);
			DateTime.TryParse(sValues[4],out dtTermDate);

			if ((dtStartDate <= DateTime.Now) && (dtTermDate >= DateTime.Now)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private string GetQueryManProfile(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"LOGIN_ID,RNUM",out sValues)) {
			return string.Format("seekmode={0}&loginid={1}&userrecno={2}&site={3}",
								sessionWoman.seekMode,
								sValues[0],
								sValues[1],
								sessionWoman.site.siteCd
								) +
								sessionWoman.seekCondition.GetConditionQuery() + sessionWoman.AddUniqueQuery(GetDataRow(this.currentTableIdx),this.sessionWoman.requestQuery);
		} else {
			return "";
		}
	}

	private string GetTvTalkUrl(string pTag,string pChargeType,string pText) {
		string sMenuId = "";

		string sLabel = "",sAccessKey = "";
		string[] sValue = pText.Split(',');
		if (sValue.Length >= 1) {
			sLabel = sValue[0];
		}
		if (sValue.Length >= 2) {
			sAccessKey = string.Format(" accesskey=\"{0}\" ",sValue[1]);
		}

		if (pChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_PUBLIC)) {
			sMenuId = ViCommConst.MENU_VIDEO_PTALK;
		} else {
			sMenuId = ViCommConst.MENU_VIDEO_WTALK;
		}

		string[] sValues;
		string sUrl = "";
		if (GetValues(pTag,"USER_SEQ,RNUM",out sValues)) {
			sUrl = string.Format("<a href=\"RequestIVP.aspx?category={0}&request={1}&menuid={2}&userseq={3}&chgtype={4}&userrecno={5}&{6}={7}\" {9}>{8}</a>",
								sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].actCategoryIdx,
								ViCommConst.REQUEST_TALK,
								sMenuId,
								sValues[0],
								pChargeType,
								sValues[1],
								sessionWoman.site.charNoItemNm,
								sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].cryptValue,
								sLabel,
								sAccessKey);
		}
		return sUrl;
	}

	private string GetVoiceTalkUrl(string pTag,string pChargeType,string pText) {
		string sUrl = "",sMenuId;
		string sLabel = "",sAccessKey = "";
		string[] sValue = pText.Split(',');
		if (sValue.Length >= 1) {
			sLabel = sValue[0];
		}
		if (sValue.Length >= 2) {
			sAccessKey = string.Format(" accesskey=\"{0}\" ",sValue[1]);
		}

		if (pChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC)) {
			sMenuId = ViCommConst.MENU_VOICE_PTALK;
		} else {
			sMenuId = ViCommConst.MENU_VOICE_WTALK;
		}

		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,RNUM",out sValues)) {
			sUrl = string.Format("<a href=\"RequestIVP.aspx?category={0}&request={1}&menuid={2}&userseq={3}&chgtype={4}&userrecno={5}&{6}={7}\" {9}>{8}</a>",
								sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].actCategoryIdx,
								ViCommConst.REQUEST_TALK,
								sMenuId,
								sValues[0],
								pChargeType,
								sValues[1],
								sessionWoman.site.charNoItemNm,
								sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].cryptValue,
								sLabel,
								sAccessKey);
		}
		return sUrl;
	}

	public string GetManItem(string pTag,string pArgument) {
		string sValue;
		SetFieldValue(pTag,"MAN_ATTR_VALUE" + pArgument,out sValue);
		return sValue;
	}

	public string GetHrefTvWShot(string pTag,string pArgument) {
		if (IsTvTalkEnable(pTag)) {
			return GetTvTalkUrl(pTag,ViCommConst.CHARGE_CAST_TALK_WSHOT,pArgument);
		} else {
			string sLabel,sAccessKey;
			GetLableAndAccessKey(pArgument,out sLabel,out sAccessKey);
			return string.Format("<a>{0}</a>",sLabel);
		}
	}

	public string GetHrefVoiceWShot(string pTag,string pArgument) {
		if (IsVoiceTalkEnable(pTag)) {
			return GetVoiceTalkUrl(pTag,ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT,pArgument);
		} else {
			string sLabel,sAccessKey;
			GetLableAndAccessKey(pArgument,out sLabel,out sAccessKey);
			return string.Format("<a>{0}</a>",sLabel);
		}
	}

	private string GetFavoritMemo(string pTag) {
		string sMemo = "";
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (Favorit oFavorit = new Favorit()) {
				sMemo = oFavorit.GetFavoritComment(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,ViCommConst.MAIN_CHAR_NO,sValues[0],sValues[1]);
			}
		}
		return sMemo;
	}

	private string GetBbsWriteFlag(string pTag) {
		string[] sValues;
		decimal dRecCount = 0;
		if (GetValues(pTag,"LOGIN_ID,USER_CHAR_NO",out sValues)) {
			using (UserManBbs oUserManBbs = new UserManBbs()) {
				oUserManBbs.GetPageCount(sessionWoman.site.siteCd,sValues[0],sValues[1],1,out dRecCount);
			}
		}
		if (dRecCount > 0) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private int GetManBbsNewFlag(string pTag) {
		string[] sValues;
		int iFlag = ViCommConst.FLAG_OFF;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (UserManBbs oBbs = new UserManBbs()) {
				iFlag = oBbs.GetNewlyArrived(sessionWoman.site.siteCd,sValues[0],sValues[1],sessionWoman.site.bbsNewHours);
			}
		}
		return iFlag;
	}

	private string GetTalkCount(string pTag) {
		int iRecCount = 0;
		string sValue = "0", sUserSeq = "", sUserCharNo = "";
		if (GetValue(pTag, "USER_SEQ", out sUserSeq) && GetValue(pTag, "USER_CHAR_NO", out sUserCharNo)) {
			if (sessionWoman.logined) {
				using (TalkHistory oTalkHis = new TalkHistory()) {
					iRecCount = oTalkHis.GetTalkCount(
									sessionWoman.site.siteCd,
									sUserSeq,
									sUserCharNo,
									sessionWoman.userWoman.userSeq,
									sessionWoman.userWoman.CurCharacter.userCharNo
								);
					sValue = iRecCount.ToString();
				}
			}
		}
		return sValue;
	}

	private string GetContainsOfCarrier(string pTag,params string[] pCarriers) {
		if (sessionWoman.batchMailFlag) {
			switch (pTag) {
				case "$MAN_FEATURE_PHONE_FLAG":
					return sessionWoman.batchMailSeekDevice.Equals(ViCommConst.DEVICE_3G) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				case "$MAN_SMART_PHONE_FLAG":
					return sessionWoman.batchMailSeekDevice.Equals(ViCommConst.DEVICE_SMART_PHONE) || sessionWoman.batchMailSeekDevice.Equals(ViCommConst.DEVICE_ANDROID) || sessionWoman.batchMailSeekDevice.Equals(ViCommConst.DEVICE_IPHONE)
						? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				case "$MAN_ANDROID_FLAG":
					return sessionWoman.batchMailSeekDevice.Equals(ViCommConst.DEVICE_ANDROID) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				case "$MAN_IPHONE_FLAG":
					return sessionWoman.batchMailSeekDevice.Equals(ViCommConst.DEVICE_IPHONE) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				default:
					return ViCommConst.FLAG_OFF_STR;
			}
		}
		string sMobileCarrierCd = string.Empty;
		if (sessionObjs.tableIndex.Equals(ViCommConst.DATASET_MAIL)) {
			string sUserSeq;
			if (GetValue(pTag,"PARTNER_USER_SEQ",out sUserSeq)) {
				using (User oUser = new User()) {
					if (oUser.GetOne(sUserSeq,ViCommConst.MAN)) {
						sMobileCarrierCd = oUser.mobileCarrierCd;
					}
				}
			}
		} else {
			GetValue(pTag,"MOBILE_CARRIER_CD",out sMobileCarrierCd);
		}
		
		return Array.Exists<string>(pCarriers,delegate(string carrier) { return sMobileCarrierCd.Equals(carrier); }) ?
			ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}

	private string GetSelfCommunication(string pTag,string pArgument) {
		string[] sValues;
		DataSet oDataSet = null;
		DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		if (GetValues(pTag,"USER_SEQ,LIKE_ME_FLAG",out sValues)) {
			using (Communication oCommunication = new Communication()) {
				oDataSet = oCommunication.GetTypeAndDateList(
					sessionWoman.site.siteCd,
					sValues[0],
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sValues[1]
				);
			}
		}

		return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_COMMUNICATION);
	}

	private string GetManFavoritImg(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"LIKE_ME_FLAG,FAVORIT_FLAG",out sValues)) {
			string sLikeMeFlag = sValues[0];
			string sFavoritFlag = sValues[1];

			if (sLikeMeFlag.Equals(ViCommConst.FLAG_ON_STR) && sFavoritFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = "<img src=\"../Image/" + sessionWoman.site.siteCd + "/wo_both.gif\" />";
			} else if (sLikeMeFlag.Equals(ViCommConst.FLAG_ON_STR) && sFavoritFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				sValue = "<img src=\"../Image/" + sessionWoman.site.siteCd + "/wo_m_to_w.gif\" />";
			} else if (sLikeMeFlag.Equals(ViCommConst.FLAG_OFF_STR) && sFavoritFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = "<img src=\"../Image/" + sessionWoman.site.siteCd + "/wo_w_to_m.gif\" />";
			}
		}

		return sValue;
	}

	private string GetManImgMark(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sSmallPhotoImgPath;

		if (GetValue(pTag,"SMALL_PHOTO_IMG_PATH",out sSmallPhotoImgPath)) {
			if (!sSmallPhotoImgPath.Equals("image/A001/photo_men1.gif")) {
				sValue = string.Format("<img src=\"../Image/{0}/fp/ico/ico_photo_03.gif\" />",sessionWoman.site.siteCd);
			}
		}

		return sValue;
	}

	private string CreateCommunicationList(string pTag,string pArgument) {
		string[] sValues;
		DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);
		
		DataSet dsSorted = new DataSet();

		if (GetValues(pTag,"LAST_TALK_DATE,LAST_TX_MAIL_DATE,REGIST_FAVORITE_DATE_BY_MAN,LAST_MARKING_DATE_BY_MAN,LAST_MAIL_READ_DATE_BY_MAN,LIKE_ME_FLAG",out sValues)) {
			DataSet dsTemp = new DataSet();
			DataTable dt = new DataTable();
			dt.Columns.Add("COMM_TYPE",Type.GetType("System.String"));
			dt.Columns.Add("COMM_DATE",Type.GetType("System.String"));
			dsTemp.Tables.Add(dt);

			if (!string.IsNullOrEmpty(sValues[0])) {
				DataRow dr01 = dsTemp.Tables[0].Rows.Add();
				dr01["COMM_TYPE"] = "TALK";
				dr01["COMM_DATE"] = sValues[0];
			}

			if (!string.IsNullOrEmpty(sValues[1])) {
				DataRow dr02 = dsTemp.Tables[0].Rows.Add();
				dr02["COMM_TYPE"] = "TX_MAIL";
				dr02["COMM_DATE"] = sValues[1];
			}

			if (!string.IsNullOrEmpty(sValues[2]) && sValues[5].Equals(ViCommConst.FLAG_ON_STR)) {
				DataRow dr03 = dsTemp.Tables[0].Rows.Add();
				dr03["COMM_TYPE"] = "FAVORITE_BY_MAN";
				dr03["COMM_DATE"] = sValues[2];
			}

			if (!string.IsNullOrEmpty(sValues[3])) {
				DataRow dr04 = dsTemp.Tables[0].Rows.Add();
				dr04["COMM_TYPE"] = "MARKING_BY_MAN";
				dr04["COMM_DATE"] = sValues[3];
			}

			if (!string.IsNullOrEmpty(sValues[4])) {
				DataRow dr05 = dsTemp.Tables[0].Rows.Add();
				dr05["COMM_TYPE"] = "MAIL_READ_BY_MAN";
				dr05["COMM_DATE"] = sValues[4];
			}

			DataRow[] oRows = dsTemp.Tables[0].Select(string.Empty,"COMM_DATE DESC");
			dsSorted = dsTemp.Clone();

			foreach (DataRow dr in oRows) {
				dsSorted.Tables[0].ImportRow(dr);
			}
		}

		return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,dsSorted,PwViCommConst.DATASET_COMMUNICATION);
	}
}
