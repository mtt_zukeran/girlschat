﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseBbsViewHistory(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		string sObjKind = "";

		switch (pTag) {
			case "$MAN_AGE":
				SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_BAL_POINT":
				SetFieldValue(pTag,"BAL_POINT",out sValue);
				break;
			case "$MAN_BIRTHDAY":
				SetFieldValue(pTag,"BIRTHDAY",out sValue);
				break;
			case "$MAN_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_IMG":
				sValue = GetManFavoritImg(pTag,pArgument);
				break;
			case "$MAN_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				sValue = ManHandleNmEmptyFill(sValue);
				break;
			case "$MAN_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				break;
			case "$MAN_ITEM":
				sValue = GetManItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$MAN_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				break;
			case "$MAN_LAST_LOGIN_DATE":
				SetFieldValue(pTag,"LAST_LOGIN_DATE",out sValue);
				break;
			case "$MAN_LAST_RECEIPT_DATE": {
					sValue = this.GetManLastReceiptDate(pTag);
				}
				break;
			case "$MAN_LAST_RECEIPT_PAST_DAYS": {
					DateTime dtLastPeceipt;
					if (DateTime.TryParse(this.GetManLastReceiptDate(pTag),out dtLastPeceipt)) {
						sValue = ((TimeSpan)(DateTime.Today - dtLastPeceipt.Date)).Days.ToString();
					} else {
						sValue = string.Empty;
					}
				}
				break;
			case "$MAN_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$MAN_NEW_FLAG":
				SetFieldValue(pTag,"NEW_MAN_FLAG",out sValue);
				break;
			case "$MAN_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$MAN_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$MAN_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$MAN_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$MAN_TOTAL_RECEIPT_AMT":
				SetFieldValue(pTag,"TOTAL_RECEIPT_AMT",out sValue);
				break;
			case "$MAN_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$MAN_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$MAN_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$MAN_REGIST_DATE":
				SetFieldValue(pTag,"REGIST_DATE",out sValue);
				break;
			case "$MAN_TOTAL_TALK_COUNT":
				SetFieldValue(pTag,"TOTAL_TALK_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_TX_MAIL_COUNT":
				SetFieldValue(pTag,"TOTAL_TX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_RX_MAIL_COUNT":
				SetFieldValue(pTag,"TOTAL_RX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_TALK_COUNT_BY_SELF":
				SetFieldValue(pTag,"PARTNER_TOTAL_TALK_COUNT",out sValue);
				break;
			case "$SELF_TO_MAN_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_RX_MAIL_COUNT",out sValue);	// 男性基準のためrxを設定
				break;
			case "$MAN_TO_SELF_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_TX_MAIL_COUNT",out sValue);	// 男性基準のためtxを設定
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$MAN_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$MAN_BBS_VIEW_DATE":
				SetFieldValue(pTag,"USED_DATE",out sValue);
				break;
			case "$SELF_OBJ_DOC":
				sValue = GetDoc(pTag,"OBJ_DOC");
				break;
			case "$SELF_OBJ_TITLE":
				SetFieldValue(pTag,"OBJ_TITLE",out sValue);
				break;
			case "$SELF_OBJ_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$SELF_OBJ_VIEW_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$DIV_IS_PIC":
				if (GetValue(pTag,"OBJ_KIND",out sObjKind)) {
					SetNoneDisplay(!(sObjKind.Equals(ViCommConst.BbsObjType.PIC)));
				}
				break;
			case "$DIV_IS_MOVIE":
				if (GetValue(pTag,"OBJ_KIND",out sObjKind)) {
					SetNoneDisplay(!(sObjKind.Equals(ViCommConst.BbsObjType.MOVIE)));
				}
				break;
			case "$SELF_OBJ_SEQ":
				SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;
			//PICのみ使用可能-------------------------------------------------------------------
			case "$SELF_PIC_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				break;
			case "$SELF_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				break;

			//MOVIEのみ使用可能-------------------------------------------------------------------
			case "$HREF_SELF_MOVIE_DOWNLOAD":
				sValue = GetHRefSelfMovieDwonload(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
}

