﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 登録後トップ検索結果
--	Progaram ID		: UserTop
--  Creation Date	: 2015.05.12
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

partial class PwParseWoman {
	private string ParseUserTop(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$LATEST_ADMIN_REPORT_TITLE":
				this.parseWoman.SetFieldValue(pTag,"LATEST_ADMIN_REPORT_TITLE",out sValue);
				break;
			case "$DIV_IS_SELF_LOGIN_PLAN_EXIST":
				this.parseWoman.SetNoneDisplay(!this.IsNotNullField(pTag,"SELF_RECENT_LOGIN_PLAN_START_TIME"));
				break;
			case "$SELF_RECENT_LOGIN_PLAN_START_TIME":
				this.parseWoman.SetFieldValue(pTag,"SELF_RECENT_LOGIN_PLAN_START_TIME",out sValue);
				break;
			case "$SELF_RECENT_LOGIN_PLAN_START_TIME_FORMAT":
				sValue = this.GetDateTimeFormat01(pTag,"SELF_RECENT_LOGIN_PLAN_START_TIME");
				break;
			case "$DIV_IS_SELF_WAIT_SCHEDULE_EXIST":
				this.parseWoman.SetNoneDisplay(!this.IsNotNullField(pTag,"SELF_RECENT_WAIT_SCHEDULE_START_TIME"));
				break;
			case "$SELF_RECENT_WAIT_SCHEDULE_START_TIME":
				this.parseWoman.SetFieldValue(pTag,"SELF_RECENT_WAIT_SCHEDULE_START_TIME",out sValue);
				break;
			case "$SELF_RECENT_WAIT_SCHEDULE_START_TIME_FORMAT":
				sValue = this.GetDateTimeFormat01(pTag,"SELF_RECENT_WAIT_SCHEDULE_START_TIME");
				break;
			case "$DIV_IS_SELF_MAIL_REQUEST_EXIST":
				this.parseWoman.SetNoneDisplay(!this.IsNotZeroField(pTag,"SELF_MAIL_REQUEST_COUNT"));
				break;
			case "$SELF_MAIL_REQUEST_COUNT":
				this.parseWoman.SetFieldValue(pTag,"SELF_MAIL_REQUEST_COUNT",out sValue);
				break;
			case "$DIV_IS_SELF_FAVORIT_NEWS01_EXIST":
				this.parseWoman.SetNoneDisplay(!this.IsNotNullField(pTag,"SELF_FAVORIT_NEWS01_DATE_FORMAT"));
				break;
			case "$DIV_IS_SELF_FAVORIT_NEWS02_EXIST":
				this.parseWoman.SetNoneDisplay(!this.IsNotNullField(pTag,"SELF_FAVORIT_NEWS02_DATE_FORMAT"));
				break;
			case "$DIV_IS_SELF_FAVORIT_NEWS03_EXIST":
				this.parseWoman.SetNoneDisplay(!this.IsNotNullField(pTag,"SELF_FAVORIT_NEWS03_DATE_FORMAT"));
				break;
			case "$DIV_IS_SELF_FAVORIT_NEWS04_EXIST":
				this.parseWoman.SetNoneDisplay(!this.IsNotNullField(pTag,"SELF_FAVORIT_NEWS04_DATE_FORMAT"));
				break;
			case "$SELF_FAVORIT_NEWS01_DATE_FORMAT":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS01_DATE_FORMAT",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS02_DATE_FORMAT":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS02_DATE_FORMAT",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS03_DATE_FORMAT":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS03_DATE_FORMAT",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS01_URL":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS01_URL",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS02_URL":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS02_URL",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS03_URL":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS03_URL",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS01_TEXT":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS01_TEXT",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS02_TEXT":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS02_TEXT",out sValue);
				break;
			case "$SELF_FAVORIT_NEWS03_TEXT":
				this.parseWoman.SetFieldValue(pTag,"SELF_FAVORIT_NEWS03_TEXT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private bool IsNotNullField(string pTag,string pField) {
		string sValue;

		if (this.parseWoman.SetFieldValue(pTag,pField,out sValue)) {
			if (!string.IsNullOrEmpty(sValue)) {
				return true;
			}
		}

		return false;
	}

	private bool IsNotZeroField(string pTag,string pField) {
		string sValue;

		if (this.parseWoman.SetFieldValue(pTag,pField,out sValue)) {
			if (!sValue.Equals("0")) {
				return true;
			}
		}

		return false;
	}

	private string GetDateTimeFormat01(string pTag,string pField) {
		string sTime = string.Empty;
		string sValue = string.Empty;

		if (this.parseWoman.SetFieldValue(pTag,pField,out sValue)) {
			DateTime dt;
			if (DateTime.TryParse(sValue,out dt)) {
				string[] sWeek = { "日","月","火","水","木","金","土" };
				int iDay = int.Parse(dt.DayOfWeek.ToString("d"));
				sTime = string.Format("{0}({1}) {2}",dt.ToString("M/dd"),sWeek[iDay],dt.ToString("H:mm"));
			}
		}

		return sTime;
	}
}
