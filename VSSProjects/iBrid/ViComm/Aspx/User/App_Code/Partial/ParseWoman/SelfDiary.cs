﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseSelfDiary(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$QUERY_VIEW_SELF_PERSONAL_DIARY":
				sValue = GetQueryViewSelfPersonalDiary(pTag);
				break;
			case "$SELF_DIARY_DAY":
				SetFieldValue(pTag,"REPORT_DAY",out sValue);
				break;
			case "$SELF_DIARY_WRITE_DATE":
				SetFieldValue(pTag,"DIARY_CREATE_DATE",out sValue);
				break;
			case "$SELF_DIARY_DOC":
				sValue = GetDiaryDoc(pTag);
				break;
			case "$SELF_DIARY_IMG_PATH":
				sValue = GetSelfDiaryImage(pTag);
				break;
			case "$SELF_SMALL_DIARY_IMG_PATH":
				sValue = GetSelfSmallDiaryImage(pTag);
				break;
			case "$SELF_DIARY_HEADER_TITLE": {
					using (Cast oCast = new Cast()) {
						oCast.GetCastInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
						sValue = oCast.diaryHeaderTitle;
					}
				}
				break;
			case "$SELF_DIARY_TITLE":
				SetFieldValue(pTag,"DIARY_TITLE",out sValue);
				break;
			case "$SELF_DIARY_READ_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$SELF_DIARY_LIKE_COUNT":
				SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$SELF_DIARY_STOCK_PIC_SEND_ADDR":
				sValue = GetDiaryPictureUploadAddr(pTag,pArgument);
				break;
			case "$SELF_DIARY_MAILER_PIC_SEND_ADDR":
				sValue = GetDiaryPictureUploadAddrByDisplayDoc(pTag,pArgument);
				break;
			case "$SELF_DIARY_PUBLISH_FLAG": {
					string sFlag = string.Empty;
					SetFieldValue(pTag,"ADMIN_DEL_FLAG",out sFlag);
					sValue = ViCommConst.FLAG_ON_STR.Equals(sFlag) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR;
				}
				break;
			case "$SELF_DIARY_PIC_DELETE_FLAG": {
					string[] sValues;
					if (GetValues(pTag,"ATTACHED_OBJ_TYPE,PIC_SEQ",out sValues)) {
						if (ViCommConst.DiaryObjType.PIC.Equals(sValues[0]) && string.IsNullOrEmpty(sValues[1])) {
							sValue = ViCommConst.FLAG_ON_STR;
						}
					}
					sValue = ViCommConst.FLAG_OFF_STR;
				}
				break;
			case "$QUERY_UPDATE_DIARY":
				sValue = GetQueryUpdateDiary(pTag);
				break;
			case "$DIARY_HAS_PIC_FLAG":
				sValue = GetDiaryHasPicFlag(pTag);
				break;
			case "$DIARY_UPLOAD_KEY": {
					string[] sValues;
					if (GetValues(pTag,"REPORT_DAY,CAST_DIARY_SUB_SEQ",out sValues)) {
						sValue = string.Format("{0}{1}",sValues[0].Replace("/",string.Empty),sValues[1]);
					}
				}
				break;
			case "$QUERY_DIARY_LIKE":
				sValue = GetQueryListCastDiaryLike(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryListCastDiaryLike(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"REPORT_DAY,CAST_DIARY_SUB_SEQ,SCREEN_ID",out sValues)) {
			return string.Format("reportday={0}&subseq={1}&scrid={2}",
				sValues[0],sValues[1],sValues[2]);
		} else {
			return "";
		}
	}

	private string GetDiaryPictureUploadAddr(string pTag, string pArgument) {
		string[] sValues;
		if (GetValues(pTag,"LOGIN_ID,USER_CHAR_NO,REPORT_DAY,CAST_DIARY_SUB_SEQ",out sValues)) {

			string sMailto = parseContainer.Parse(pArgument);
			string sUrlHeader = string.Empty;
			if (string.IsNullOrEmpty(sMailto)) {
				sUrlHeader = ViCommConst.DIARY_URL_HEADER;
			} else {
				sUrlHeader = string.Format("{0}:{1}", sMailto, ViCommConst.DIARY_URL_HEADER2);
			}
			return sUrlHeader +
							   sessionWoman.site.siteCd + 
							   sValues[0] + sValues[1] + "_" +
							   sValues[2].Substring(0,4) +
							   sValues[2].Substring(5,2) +
							   sValues[2].Substring(8,2) +
							   sValues[3] +
								"@" + sessionWoman.site.mailHost;
		} else {
			return "";
		}
	}

	private string GetDiaryPictureUploadAddrByDisplayDoc(string pTag, string pArgument) {

		string sMailto = parseContainer.Parse(pArgument);
		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailto)) {
			sUrlHeader = ViCommConst.DIARY_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}", sMailto, ViCommConst.DIARY_URL_HEADER2);
		}
		return sUrlHeader +
						   sessionWoman.site.siteCd + 
						   sessionWoman.userWoman.loginId +
						   sessionWoman.userWoman.curCharNo + "_" +
						   iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["uploadkey"]) +
							"@" + sessionWoman.site.mailHost;
	}

	private string GetQueryUpdateDiary(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"REPORT_DAY,CAST_DIARY_SUB_SEQ,RNUM",out sValues)) {
			return string.Format("mode=2&reportday={0}&subseq={1}&pageno={2}&haspic={3}",
				sValues[0],sValues[1],sValues[2],iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["haspic"]));
		} else {
			return "";
		}
	}

	private string GetQueryViewSelfPersonalDiary(string pTag) {
		string sValue;
		if (GetValue(pTag,"RNUM",out sValue)) {
			return string.Format("pageno={0}",sValue);
		} else {
			return "";
		}
	}

	private string GetSelfDiaryImage(string pTag) {
		string sValue;
		if (GetValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue)) {
			sValue = "../" + sValue;
			return sValue;
		} else {
			return "";
		}
	}

	private string GetSelfSmallDiaryImage(string pTag) {
		string sValue;
		if(GetValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue)) {
			sValue = "../" + sValue;
			return sValue;
		} else {
			return string.Empty;
		}
	}

	private string GetDiaryDoc(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "";
		if (GetValue(pTag,"HTML_DOC1",out sDoc1) && GetValue(pTag,"HTML_DOC2",out sDoc2) && GetValue(pTag,"HTML_DOC3",out sDoc3) && GetValue(pTag,"HTML_DOC4",out sDoc4)) {
			sValue = parseContainer.Parse(Regex.Replace(sDoc1 + sDoc2 + sDoc3 + sDoc4,"\r\n","<br />"));
		}
		return sValue;
	}
	
	private string GetDiaryHasPicFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ATTACHED_OBJ_TYPE,PIC_SEQ",out sValues)) {
			if (ViCommConst.DiaryObjType.PIC.Equals(sValues[0]) && !string.IsNullOrEmpty(sValues[1])) {
				return ViCommConst.FLAG_ON_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}

	private string GetOneCastDiary(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this,pTag,pArgument);
		SeekCondition oCondition = new SeekCondition();

		bool bDiarySwitchScreenFlag = false;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bDiarySwitchScreenFlag = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DIARY_SWITCH_SCREEN,2);
		}

		using (CastDiary oCastDiary = new CastDiary(sessionObjs)) {
			oTmpDataSet = oCastDiary.GetOneVwByReportDay(this.sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,iBridUtil.GetStringValue(oPartsArgs.Query["diaryday"]),iBridUtil.GetStringValue(oPartsArgs.Query["subseq"]));
			
		}
		
		sValue = this.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.ViCommConst.DATASET_SELF_DIARY);

		return sValue;
	}
}
