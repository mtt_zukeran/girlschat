﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class PwParseWoman {
	private string ParseTreasure(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		switch (pTag) {
			case "$CAST_TREASURE_NM":
				this.parseWoman.SetFieldValue(pTag,"CAST_TREASURE_NM",out sValue);
				break;
			case "$CAST_TREASURE_USED_TRAP_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USED_TRAP_COUNT",out sValue);
				break;
			case "$CAST_TREASURE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_TREASURE_SEQ",out sValue);
				break;
			case "$CAST_TREASURE_POSSESSION_COUNT":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_COUNT",out sValue);
				break;
			case "$PIC_TITLE":
				this.parseWoman.SetFieldValue(pTag,"PIC_TITLE",out sValue);
				break;
			case "$PIC_DOC":
				this.parseWoman.SetFieldValue(pTag,"PIC_DOC",out sValue);
				break;
			case "$OBJ_SMALL_PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_GAME_PIC_ATTR_NM":
				this.parseWoman.SetFieldValue(pTag,"CAST_GAME_PIC_ATTR_NM",out sValue);
				break;
			case "$SELF_OBJ_UPLOAD_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$CAST_GAME_PIC_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_GAME_PIC_SEQ",out sValue);
				break;
			case "$CAST_USER_MOVIE_TITLE":
				this.parseWoman.SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$GAME_UNAUTH_MARK":
				this.parseWoman.SetFieldValue(pTag,"UNAUTH_MARK",out sValue);
				break;
			case "$GAME_TOTAL_MOSAIC_ERASE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_MOSAIC_ERASE_COUNT",out sValue);
				break;
			case "$OBJ_PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$TOTAL_MAN_POSSESSION_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_MAN_POSSESSION_COUNT",out sValue);
				break;
			case "$CAST_TREASURE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TREASURE_COUNT",out sValue);
				break;
			case "$GAME_STAGE_GROUP_NM":
				this.parseWoman.SetFieldValue(pTag,"STAGE_GROUP_NM",out sValue);
				break;
			case "$GAME_COMPLETE_DATE":
				this.parseWoman.SetFieldValue(pTag,"COMPLETE_DATE",out sValue);
				break;
			case "$GAME_STAGE_GROUP_TYPE":
				this.parseWoman.SetFieldValue(pTag,"STAGE_GROUP_TYPE",out sValue);
				break;
			case "$CAST_TREASURE_USED_DOUBLE_TRAP_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USED_DOUBLE_TRAP_COUNT",out sValue);
				break;
			case "$CAST_TREASURE_TOTAL_USED_TRAP_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_USED_TRAP_COUNT",out sValue);
				break;
			case "$CAST_GAME_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$MAN_TREASURE_DISPLAY_DAY":
				this.parseWoman.SetFieldValue(pTag,"DISPLAY_DAY",out sValue);
				break;
			case "$GAME_TREASURE_COMPLETE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"TREASURE_COMPLETE_FLAG",out sValue);
				break;
			case "$GAME_TREASURE_POSSESSION_FLAG":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_FLAG",out sValue);
				break;
			case "$GAME_TREASURE_CAN_ROB_FLAG":
				this.parseWoman.SetFieldValue(pTag,"CAN_ROB_FLAG",out sValue);
				break;
			case "$POSSESSION_FLAG":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_FLAG",out sValue);
				break;
			case "$TUTORIAL_BATTLE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"TUTORIAL_BATTLE_FLAG",out sValue);
				break;
			case "$CAST_TREASURE_TYPE":
				this.parseWoman.SetFieldValue(pTag,"CAST_TREASURE_TYPE",out sValue);
				break;
			case "$MAN_TREASURE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"MAN_TREASURE_COUNT",out sValue);
				break;
			case "$USE_MOSAIC_ERASE_PERSON_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USE_MOSAIC_ERASE_PERSON_COUNT",out sValue);
				break;
			case "$POSSESSION_PIC_PERSON_COUNT":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_PIC_PERSON_COUNT",out sValue);
				break;
			case "$POSSESSION_PIC_COUNT":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_PIC_COUNT",out sValue);
				break;
			case "$MOVIE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"MOVIE_COUNT",out sValue);
				break;
			case "$USE_MOVIE_TICKET_PERSON_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USE_MOVIE_TICKET_PERSON_COUNT",out sValue);
				break;
			case "$USE_MOVIE_TICKET_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USE_MOVIE_TICKET_COUNT",out sValue);
				break;
			case "$BONUS_GET_FLAG":
				this.parseWoman.SetFieldValue(pTag,"BONUS_GET_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;

		}
		return sValue;
	}

	private string GetCastTreasureData(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		CastTreasureSeekCondition oCondition = new CastTreasureSeekCondition(pPartsArgs.Query);
		if (string.IsNullOrEmpty(oCondition.CastTreasureSeq)) {
			return string.Empty;
		}
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		DataSet oTmpDataSet = null;
		CastTreasure oCastTreasure = new CastTreasure();
		oTmpDataSet = oCastTreasure.GetOneByCastTreasureSeqPossession(oCondition);

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		return this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
	}

	private string GetManTreasureData(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		ManTreasureManagementSeekCondition oCondition = new ManTreasureManagementSeekCondition(pPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		DataSet oTmpDataSet = null;
		using (ManTreasureManagement oManTreasureManagement = new ManTreasureManagement()) {
			oTmpDataSet = oManTreasureManagement.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		return this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
	}

	private string GetTotalPossessionCastTreasureCount(string pTag,string pArgument) {
		string sValue = string.Empty;

		CastTreasureSeekCondition oCondition = new CastTreasureSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (CastTreasure oCastTreasure = new CastTreasure()) {
			sValue = oCastTreasure.GetTotalPossessionCastTreasureCount(oCondition);
		}

		return sValue;
	}

	private string GetGetCastTreasureLog(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		string sGetTreasureLogSeq = oPartsArgs.Query["get_treasure_log_seq"].ToString();

		using (GetCastTreasureLog oGetCastTreasureLog = new GetCastTreasureLog()) {
			oTmpDataSet = oGetCastTreasureLog.GetOne(
				this.parseWoman.sessionWoman.site.siteCd,
				sGetTreasureLogSeq
			);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);

		return sValue;
	}

	private string GetStageGroupNm(string pTag,string pArgument) {

		string sValue = null;

		using (StageGroup oStageGroup = new StageGroup()) {
			sValue = oStageGroup.GetStageGroupNm(this.parseWoman.sessionWoman.site.siteCd,pArgument,ViCommConst.OPERATOR);
		}

		return sValue;
	}

	private string GetStageGroupPossessionCastTreasure(string pTag,string pArgument) {

		string sValue = null;

		using (CastTreasure oCastTreasure = new CastTreasure()) {
			sValue = oCastTreasure.StageGroupPossessionCastTreasure(this.parseWoman.sessionWoman.site.siteCd,this.parseWoman.sessionWoman.userWoman.userSeq,this.parseWoman.sessionWoman.userWoman.curCharNo,pArgument);
		}

		return sValue;
	}

	private string GetRemainderPossessionCastTreasureCount(string pTag,string pArgument) {
		
		int iValue = 0;

		iValue = PwViCommConst.WOMAN_TREASURE_ATTR_NUM - int.Parse(pArgument);

		return iValue.ToString();
	}

	private string GetStageGroupType(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;

		string sGetTreasureLogSeq = pArgument;

		using (GetCastTreasureLog oGetCastTreasureLog = new GetCastTreasureLog()) {
			oDataSet = oGetCastTreasureLog.GetOne(
				this.parseWoman.sessionWoman.site.siteCd,
				sGetTreasureLogSeq
			);
		}

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			sValue = dr["STAGE_GROUP_TYPE"].ToString();
		}

		return sValue;
	}

	private string GetSoonCompCastTreasure(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		StageGroupSeekCondition oCondition = new StageGroupSeekCondition(pPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		
		DataSet oTmpDataSet = null;
		using (StageGroup oStageGroup = new StageGroup()) {
			oTmpDataSet = oStageGroup.GetSoonCompCastTreasure(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		return this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
	}
	
	private string GetCastTreasureCompStatus(string pTag,string pArgument) {
		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		
		PossessionCastTreasureSeekCondition oCondition = new PossessionCastTreasureSeekCondition(pPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		DataSet oTmpDataSet = null;
		using (PossessionCastTreasure oPossessionCastTreasure = new PossessionCastTreasure()) {
			oTmpDataSet = oPossessionCastTreasure.GetCastTreasureCompStatus(oCondition);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_TREASURE);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
	
	private bool CheckGameCastBonusTerm(string pTag,string pArgument) {
		bool bOK = false;
		
		GameCastBonusTermSeekCondition oCondition = new GameCastBonusTermSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		
		DataSet oTmpDataSet = null;
		using (GameCastBonusTerm oGameCastBonusTerm = new GameCastBonusTerm()) {
			oTmpDataSet = oGameCastBonusTerm.CheckGameCastBonusTerm(oCondition);
		}
		
		if(oTmpDataSet.Tables[0].Rows.Count == 0) {
			return false;
		}
		
		string sBonusGetTermFlag = oTmpDataSet.Tables[0].Rows[0]["BONUS_GET_TERM_FLAG"].ToString();
		
		if(sBonusGetTermFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			bOK = true;
		}
		
		return bOK;
	}
	
	private string GetSelfGameTreasureData(string pTag,string pArgument) {
		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		DataSet oTmpDataSet = null;
		using(ManTreasure oManTreasure = new ManTreasure()) {
			oTmpDataSet = oManTreasure.GetOneGameTreasureData(sSiteCd,sUserSeq,sUserCharNo);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_TREASURE);

		return sValue;
	}
}
