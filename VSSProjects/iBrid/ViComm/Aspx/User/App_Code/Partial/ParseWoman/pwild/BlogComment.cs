﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class PwParseWoman {
	private string ParseBlogComment(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		switch (pTag) {
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$COMMENT_DOC":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_DOC",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$BLOG_ARTICLE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"BLOG_ARTICLE_SEQ",out sValue);
				break;
			case "$BLOG_COMMENT_SEQ":
				this.parseWoman.SetFieldValue(pTag,"BLOG_COMMENT_SEQ",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$MAN_NG_SKULL_COUNT":
				this.parseWoman.SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			case "$MAN_LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$BLOG_ARTICLE_REGIST_DATE":
				this.parseWoman.SetFieldValue(pTag,"BLOG_ARTICLE_REGIST_DATE",out sValue);
				break;
			default:
				pParsed = false;
				break;

		}
		return sValue;
	}
}