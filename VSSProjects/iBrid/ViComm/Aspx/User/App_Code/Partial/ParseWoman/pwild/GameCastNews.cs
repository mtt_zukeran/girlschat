﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ゲーム出演者新着情報 検索結果
--	Progaram ID		: GameCastNews
--  Creation Date	: 2013.09.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

partial class PwParseWoman {
	private string ParseGameCastNews(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$NEWS_TYPE":
				this.parseWoman.SetFieldValue(pTag,"NEWS_TYPE",out sValue);
				break;
			case "$NEWS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"NEWS_COUNT",out sValue);
				break;
			case "$UPDATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}