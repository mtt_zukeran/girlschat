﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseBbsThread(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			#region -----THREAD-----
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_THREAD_NO":
				SetFieldValue(pTag,"BBS_THREAD_SEQ",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				sValue = GetThreadHandleNm(pTag);
				break;
			case "$CAST_THREAD_TITLE":
				SetFieldValue(pTag,"BBS_THREAD_TITLE",out sValue);
				break;
			case "$CAST_THREAD_DOC":
				sValue = GetBbsThreadDoc(pTag);
				break;
			case "$CAST_THREAD_LAST_RES_DATE":
				SetFieldValue(pTag,"LAST_RES_WRITE_DATE",out sValue);
				break;
			case "$CAST_THREAD_RES_COUNT":
				SetFieldValue(pTag,"LAST_THREAD_SUB_SEQ",out sValue);
				break;
			case "$CAST_THREAD_WRITE_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$CAST_THREAD_PROF_LINK_NG":
				sValue = GetThreadProfNotLinkFlag(pTag);
				break;
			case "$CAST_THREAD_TODAY_ACCESS_COUNT":
				SetFieldValue(pTag,"TODAY_ACCESS_COUNT",out sValue);
				break;
			case "$QUERY_BBS_RES_LIST":
				sValue = GetQueryResList(pTag);
				break;
			case "$QUERY_BBS_THREAD":
				sValue = GetQueryBbsThread(pTag);
				break;
			#endregion
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetThreadHandleNm(string pTag) {
		string sValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"BBS_THREAD_HANDLE_NM,HANDLE_NM",out sValues)) {
			if (sValues[0].Equals(string.Empty)) {
				sValue = sValues[1];
			} else {
				sValue = sValues[0];
			}
		}
		return sValue;
	}

	private string GetThreadProfNotLinkFlag(string pTag) {
		string sValue = ViCommConst.FLAG_ON_STR;
		string[] sValues;
		if (GetValues(pTag,"BBS_THREAD_HANDLE_NM",out sValues)) {
			if (sValues[0].Equals(string.Empty)) {
				sValue = ViCommConst.FLAG_OFF_STR;
			} else {
				sValue = ViCommConst.FLAG_ON_STR;
			}
		}
		return sValue;
	}

	private string GetBbsThreadDoc(string pTag) {
		string sValue = string.Empty,sTempValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"BBS_THREAD_DOC1,BBS_THREAD_DOC2,BBS_THREAD_DOC3,BBS_THREAD_DOC4,BBS_THREAD_SEQ",out sValues)) {
			sTempValue = parseContainer.Parse(Regex.Replace(sValues[0] + sValues[1] + sValues[2] + sValues[3],"\r\n","<br />"));
			Regex regAnchor = new Regex(@"(＞＞(\d+))|(\[(\d+)\])|((\d+)＞(\d+))",RegexOptions.Compiled);
			sValue = sTempValue;

			Dictionary<string,string> oKeyCheck = new Dictionary<string,string>();

			foreach (Match match in regAnchor.Matches(sTempValue)) {
				using (BbsRes oBbsRes = new BbsRes()) {
					if (!match.Groups[2].Value.Equals(string.Empty)) {
						sValue = Microsoft.VisualBasic.Strings.Replace(sValue,match.Groups[1].Value,GetAnchor(sValues[4],match.Groups[2].Value,string.Format(">>{0}",match.Groups[2].Value),pTag),1,1,Microsoft.VisualBasic.CompareMethod.Binary);
						//sValue = sValue.Replace(string.Format("＞＞{0}",match.Groups[2].Value),GetAnchor(sValues[4],match.Groups[2].Value,string.Format(">>{0}",match.Groups[2].Value),pTag));
					} else if (!match.Groups[4].Value.Equals(string.Empty)) {
						string s = string.Format("[{0}]",match.Groups[4].Value);
						if (!oKeyCheck.ContainsKey(s)) {
							sValue = sValue.Replace(s,GetAnchor(match.Groups[4].Value,string.Empty,s,pTag));
							oKeyCheck.Add(s,s);
						}
					} else {
						sValue = Microsoft.VisualBasic.Strings.Replace(sValue,match.Groups[5].Value,GetAnchor(match.Groups[6].Value,match.Groups[7].Value,string.Format("{0}>{1}",match.Groups[6].Value,match.Groups[7].Value),pTag),1,1,Microsoft.VisualBasic.CompareMethod.Binary);
						//sValue = sValue.Replace(string.Format("{0}＞{1}",match.Groups[6].Value,match.Groups[7].Value),GetAnchor(match.Groups[6].Value,match.Groups[7].Value,string.Format("{0}>{1}",match.Groups[6].Value,match.Groups[7].Value),pTag));
					}
				}
			}
		}
		return sValue;
	}

	private string GetQueryResList(string pTag) {
		string sSeq = string.Empty;
		string sValue = string.Empty;
		if (GetValue(pTag,"BBS_THREAD_SEQ",out sSeq)) {
			sValue = string.Format("bbsthreadseq={0}",sSeq);
		}
		return sValue;
	}

	private string GetListBbsThread(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		string sBbsThreadSeq = string.Empty;
		string sBbsAdminType = ViCommConst.BbsThreadSearchAdminType.ALL;
		
		if (pPartsArgs.Query.GetValues("info") != null) {
			sBbsAdminType = ViCommConst.BbsThreadSearchAdminType.ADMIN_FLAG_IS_TRUE;
		} else {
			sBbsThreadSeq = iBridUtil.GetStringValue(sessionObjs.requestQuery["bbsthreadseq"]);
		}
		DataSet oTmpDataSet = null;
		using (BbsThread oBbsThread = new BbsThread(sessionObjs)) {
			oTmpDataSet = oBbsThread.GetPageCollection(sessionWoman.site.siteCd,sBbsThreadSeq,sBbsAdminType,ViCommConst.BbsThreadSortType.NEW_RES,string.Empty,1,pPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_EX_BBS_THREAD);
	}

	private string GetQueryBbsThread(string pTag) {
		string sValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO,BBS_THREAD_SEQ",out sValues)){ 
			sValue = string.Format("bbs_thread_user_seq={0}&bbs_thread_user_char_no={1}&bbsthreadseq={2}",sValues[0],sValues[1],sValues[2]);
		}
		return sValue;
	}
}
