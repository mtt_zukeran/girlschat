﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using ViComm;

partial class ParseWoman {
	private string ParseBbsRes(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				sValue = GetResHandleNm(pTag);
				break;
			case "$CAST_THREAD_NO":
				SetFieldValue(pTag,"BBS_THREAD_SEQ",out sValue);
				break;
			case "$CAST_RES_NO":
				SetFieldValue(pTag,"BBS_THREAD_SUB_SEQ",out sValue);
				break;
			case "$CAST_RES_WRITE_DATE":
				SetFieldValue(pTag,"RES_CREATE_DATE",out sValue);
				break;
			case "$CAST_RES_PROF_LINK_NG":
				sValue = GetResProfNotLinkFlag(pTag);
				break;
			case "$CAST_RES_DOC":
				sValue = GetBbsResDoc(pTag);
				break;
			case "$CAST_POST_RES_NO":
				SetFieldValue(pTag,"POST_THREAD_SUB_SEQ",out sValue);
				break;
			case "$CAST_POST_RES_HANDLE_NM":
				sValue = GetPostResHandleNm(pTag);
				break;
			case "$CAST_POST_RES_PROF_LINK_NG":
				sValue = GetPostResProfNotLinkFlag(pTag);
				break;
			case "$CAST_POST_RES_LOGIN_ID":
				SetFieldValue(pTag,"BBS_POST_RES_USER_LOGIN_ID",out sValue);
				break;
			case "$CAST_THREAD_TITLE":
				SetFieldValue(pTag,"BBS_THREAD_TITLE",out sValue);
				break;
			case "$QUERY_BBS_RES":
				sValue = GetQueryBbsRes(pTag);
				break;
			case "$QUERY_BBS_THREAD":
				sValue = GetQueryBbsThread(pTag);
				break;
			case "$CAST_POST_RES_ANCHOR_LINK":
				sValue = GetCastPostResAnchor(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetResHandleNm(string pTag) {
		string sValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"BBS_RES_HANDLE_NM,HANDLE_NM,ANONYMOUS_FLAG",out sValues)) {
			if (sValues[2].Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = "匿名";
			} else {
				if (sValues[0].Equals(string.Empty)) {
					sValue = sValues[1];
				} else {
					sValue = sValues[0];
				}
			}
		}
		return sValue;
	}

	private string GetPostResHandleNm(string pTag) {
		string sValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"BBS_POST_RES_HANDLE_NM,BBS_POST_RES_USER_HANDLE_NM,BBS_POST_RES_ANONYMOUS_FLAG",out sValues)) {
			if (sValues[2].Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = "匿名";
			} else {
				if (sValues[0].Equals(string.Empty)) {
					sValue = sValues[1];
				} else {
					sValue = sValues[0];
				}
			}
		}
		return sValue;
	}

	private string GetResProfNotLinkFlag(string pTag) {
		string sValue = ViCommConst.FLAG_ON_STR;
		string[] sValues;
		if (GetValues(pTag,"BBS_RES_HANDLE_NM,ANONYMOUS_FLAG",out sValues)) {
			if (sValues[1].Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = ViCommConst.FLAG_ON_STR;
			} else {
				if (sValues[0].Equals(string.Empty)) {
					sValue = ViCommConst.FLAG_OFF_STR;
				} else {
					sValue = ViCommConst.FLAG_ON_STR;
				}
			}
		}
		return sValue;
	}

	private string GetPostResProfNotLinkFlag(string pTag) {
		string sValue = ViCommConst.FLAG_ON_STR;
		string[] sValues;
		if (GetValues(pTag,"BBS_POST_RES_HANDLE_NM,BBS_POST_RES_ANONYMOUS_FLAG",out sValues)) {
			if (sValues[1].Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = ViCommConst.FLAG_ON_STR;
			} else {
				if (sValues[0].Equals(string.Empty)) {
					sValue = ViCommConst.FLAG_OFF_STR;
				} else {
					sValue = ViCommConst.FLAG_ON_STR;
				}
			}
		}
		return sValue;
	}

	private string GetBbsResDoc(string pTag) {
		string sValue = string.Empty,sTempValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"BBS_RES_DOC1,BBS_RES_DOC2,BBS_RES_DOC3,BBS_RES_DOC4,BBS_THREAD_SEQ",out sValues)) {
			sTempValue = parseContainer.Parse(Regex.Replace(sValues[0] + sValues[1] + sValues[2] + sValues[3],"\r\n","<br />"));
			Regex regAnchor = new Regex(@"(＞＞(\d+))|(\[(\d+)\])|((\d+)＞(\d+))",RegexOptions.Compiled);
			sValue = sTempValue;

			Dictionary<string,string> oKeyCheck = new Dictionary<string,string>();

			foreach (Match match in regAnchor.Matches(sTempValue)) {
				using (BbsRes oBbsRes = new BbsRes()) {
					if (!match.Groups[2].Value.Equals(string.Empty)) {
						sValue = Microsoft.VisualBasic.Strings.Replace(sValue,match.Groups[1].Value,GetAnchor(sValues[4],match.Groups[2].Value,string.Format(">>{0}",match.Groups[2].Value),pTag),1,1,Microsoft.VisualBasic.CompareMethod.Binary);
						//sValue = sValue.Replace(string.Format("＞＞{0}",match.Groups[2].Value),GetAnchor(sValues[4],match.Groups[2].Value,string.Format(">>{0}",match.Groups[2].Value),pTag));
					} else if (!match.Groups[4].Value.Equals(string.Empty)) {
						string s = string.Format("[{0}]",match.Groups[4].Value);
						if (!oKeyCheck.ContainsKey(s)) {
							sValue = sValue.Replace(s,GetAnchor(match.Groups[4].Value,string.Empty,s,pTag));
							oKeyCheck.Add(s,s);
						}
					} else {
						sValue = Microsoft.VisualBasic.Strings.Replace(sValue,match.Groups[5].Value,GetAnchor(match.Groups[6].Value,match.Groups[7].Value,string.Format("{0}>{1}",match.Groups[6].Value,match.Groups[7].Value),pTag),1,1,Microsoft.VisualBasic.CompareMethod.Binary);
						//sValue = sValue.Replace(string.Format("{0}＞{1}",match.Groups[6].Value,match.Groups[7].Value),GetAnchor(match.Groups[6].Value,match.Groups[7].Value,string.Format("{0}>{1}",match.Groups[6].Value,match.Groups[7].Value),pTag));
					}
				}
			}
		}
		return sValue;
	}

	private string GetQueryBbsRes(string pTag) {
		string sValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"BBS_THREAD_USER_SEQ,BBS_THREAD_USER_CHAR_NO,BBS_THREAD_SEQ,BBS_THREAD_SUB_SEQ",out sValues)) {
			sValue = string.Format("bbs_thread_user_seq={0}&bbs_thread_user_char_no={1}&bbsthreadseq={2}&subseq={3}",sValues[0],sValues[1],sValues[2],sValues[3]);
		}
		return sValue;
	}

	private string GetCastPostResAnchor(string pTag) {
		string sValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"BBS_THREAD_SEQ,POST_THREAD_SUB_SEQ",out sValues)) {
			sValue = GetAnchor(sValues[0],sValues[1],string.Format(">>{0}",sValues[1]),pTag);
		}
		return sValue;
	}
}
