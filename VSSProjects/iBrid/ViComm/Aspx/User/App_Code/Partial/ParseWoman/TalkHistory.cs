﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseTalkHistory(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CALL_REQUEST_CONNECT_TYPE":
				sValue = GetCallRequestConnectType(pTag);
				break;
			case "$CALL_RESULT":
				sValue = GetCallResult(pTag);
				break;
			case "$CALL_RESULT_DETAIL":
				sValue = GetCallResultDetail(pTag);
				break;
			case "$INCOMMING_AND_OUTGOING_FLAG":
				sValue = IsIncommingAndOutgoing();
				break;
			case "$INCOMMING_FLAG":
				sValue = IsIncomming();
				break;
			case "$OUTGOING_FLAG":
				sValue = IsOutgoing();
				break;
			case "$MAN_AGE":
				SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_BAL_POINT":
				SetFieldValue(pTag,"BAL_POINT",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				sValue = GetManHandleNm(pTag,pArgument);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$TALK_FINDING_ALL_FLAG":
				sValue = GetFindingAllFlag();
				break;
			case "$TALK_FINDING_GROUPING_MAN_FLAG":
				sValue = GetFindingGroupingManFlag();
				break;
			case "$TALK_FINDING_SUCCESS_ONLY_FLAG":
				sValue = GetFindingSuccessOnlyFlag();
				break;
			case "$TALK_REQUEST_DATE":
				sValue = GetTalkRequestDate(pTag);
				break;
			case "$TALK_REQUEST_SEC":
				sValue = GetTalkRequestSec(pTag);
				break;
			case "$MAN_TOTAL_TALK_COUNT_BY_SELF":
				SetFieldValue(pTag,"PARTNER_TOTAL_TALK_COUNT",out sValue);
				break;
			case "$SELF_TO_MAN_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_RX_MAIL_COUNT",out sValue);	// 男性基準のためrxを設定 
				break;
			case "$MAN_TO_SELF_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_TX_MAIL_COUNT",out sValue);	// 男性基準のためtxを設定 
				break;
			case "$TALK_PAYMENT_AMT":
				sValue = GetTalkPayment(pTag,false);
				break;
			case "$TALK_PAYMENT_POINT":
				sValue = GetTalkPayment(pTag,true);
				break;
			case "$MAN_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$MAN_FAVORIT_IMG":
				sValue = GetManFavoritImg(pTag,pArgument);
				break;
			case "$MAN_TOTAL_RECEIPT_AMT":
				SetFieldValue(pTag,"TOTAL_RECEIPT_AMT",out sValue);
				break;
			case "$MAN_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$MAN_LAST_RECEIPT_DATE": {
					sValue = this.GetManLastReceiptDate(pTag);
				}
				break;
			case "$MAN_LAST_RECEIPT_PAST_DAYS": {
					DateTime dtLastPeceipt;
					if (DateTime.TryParse(this.GetManLastReceiptDate(pTag),out dtLastPeceipt)) {
						sValue = ((TimeSpan)(DateTime.Today - dtLastPeceipt.Date)).Days.ToString();
					} else {
						sValue = string.Empty;
					}
				}
				break;	
			case "$MAN_NG_SKULL_COUNT":
				SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			case "$MAN_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetCallRequestConnectType(string pTag) {
		string sValue = string.Empty;
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {

			return string.Empty;
		}
		SetFieldValue(pTag,"CONNECT_TYPE_NM",out sValue);
		return sValue;
	}

	private string GetCallResult(string pTag) {
		string sValue = string.Empty;
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {

			return string.Empty;
		}
		SetFieldValue(pTag,"CALL_RESULT_NM",out sValue);
		return sValue;
	}

	private string GetCallResultDetail(string pTag) {
		string sValue = string.Empty;
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {

			return string.Empty;
		}
		SetFieldValue(pTag,"CALL_RESULT_DETAIL",out sValue);
		return sValue;
	}

	private string GetTalkRequestDate(string pTag) {
		string sValue = string.Empty;
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {

			return string.Empty;
		}

		if (GetValue(pTag,"REQUEST_DATE",out sValue)) {
			if (sValue.Equals("")) {
				GetValue(pTag,"ACTION_DATE",out sValue);
			}
		}

		return sValue;
	}

	private string GetTalkRequestSec(string pTag) {
		int talkSec = 0;
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) ||
			sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {

			return string.Empty;
		}
		TimeSpan tsTalkSec;
		string sTalkStartDate,sTalkEndDate;
		if (GetValue(pTag,"TALK_START_DATE",out sTalkStartDate) && GetValue(pTag,"TALK_END_DATE",out sTalkEndDate)) {
			DateTime oTalkStartDate,oTalkEndDate;
			if (DateTime.TryParse(sTalkStartDate,out oTalkStartDate) && DateTime.TryParse(sTalkEndDate,out oTalkEndDate)) {
				tsTalkSec = (DateTime)oTalkEndDate - (DateTime)oTalkStartDate;
				talkSec = (int)tsTalkSec.TotalSeconds;
			}
		}
		return talkSec.ToString();
	}

	private string GetFindingAllFlag() {
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_INCOMMING) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_OUTGOING)) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private string GetFindingGroupingManFlag() {
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private string GetFindingSuccessOnlyFlag() {
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_INCOMMING) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_OUTGOING)) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private string IsIncomming() {
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_INCOMMING) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_INCOMMING)) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private string IsOutgoing() {
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_OUTGOING) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_OUTGOING)) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private string IsIncommingAndOutgoing() {
		if (sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) || sessionWoman.userWoman.talkRequestFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS)) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private string GetTalkPayment(string pTag,bool pPointFlag) {
		int iPayment;
		string sValue;
		if (GetValue(pTag,"TALK_SEQ",out sValue) == false) {
			return string.Empty;
		}
		using (UsedLog oUsedLog = new UsedLog()) {
			if(pPointFlag){
			int.TryParse(oUsedLog.GetPaymentPointByTalkSeq(sValue),out iPayment);
			}else{
			int.TryParse(oUsedLog.GetPaymentAmtByTalkSeq(sValue),out iPayment);
			}
		}
			return iPayment.ToString();
	}

	private string GetManHandleNm(string pTag,string pArgument) {
		string sHandleNm;

		if (SetFieldValue(pTag,"HANDLE_NM",out sHandleNm)) {
			return sHandleNm;
		} else {
			return GetManHandleNmByQueryLoginId(pTag);
		}
	}
}
