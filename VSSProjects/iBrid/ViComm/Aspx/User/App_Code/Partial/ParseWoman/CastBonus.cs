﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseCastBonus(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$BONUS_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$BONUS_POINT":
				SetFieldValue(pTag,"BONUS_POINT",out sValue);
				break;
			case "$BONUS_AMT":
				SetFieldValue(pTag,"BONUS_AMT",out sValue);
				break;
			case "$BONUS_REMARKS":
				SetFieldValue(pTag,"REMARKS",out sValue);
				break;
			case "$DIV_PAID_BONUS":
				SetNoneDisplay(!IsBonusPayd(pTag));
				break;
			case "$DIV_NO_PAID_BONUS":
				SetNoneDisplay(IsBonusPayd(pTag));
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private bool IsBonusPayd(string pTag) {
		string sValue = "";
		if (GetValue(pTag,"PAYMENT_FLAG",out sValue)) {
			return sValue.Equals("1");
		} else {
			return false;
		}
	}
}
