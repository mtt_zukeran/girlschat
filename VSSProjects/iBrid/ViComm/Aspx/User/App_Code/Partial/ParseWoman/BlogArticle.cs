﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

partial class ParseWoman {	
	private string ParseBlogArticle(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		string[] oValues;
		string sSiteCd = string.Empty;
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		string sObjSeq = string.Empty;
		string sBlogFileType = string.Empty;
		string sBlogSeq = string.Empty;
		string sBlogArticleSeq = string.Empty;
		string sBlogArticleStatus = string.Empty;

		if (GetDataRow(currentTableIdx) != null) {
			if (!GetValues(pTag, "SITE_CD,USER_SEQ,USER_CHAR_NO,OBJ_SEQ,BLOG_FILE_TYPE,BLOG_SEQ,BLOG_ARTICLE_SEQ,BLOG_ARTICLE_STATUS", out oValues)) {
				pParsed = false;
				return string.Empty;
			}
			sSiteCd = oValues[0];
			sUserSeq = oValues[1];
			sUserCharNo = oValues[2];
			sObjSeq = oValues[3];
			sBlogFileType = oValues[4];
			sBlogSeq = oValues[5];
			sBlogArticleSeq = oValues[6];
			sBlogArticleStatus = oValues[7];
		}
		
		switch (pTag) {
			case "$DIV_IS_PIC":
				SetNoneDisplay(!(sBlogFileType.Equals(ViCommConst.BlogFileType.PIC)));
				break;
			case "$DIV_IS_MOVIE":
				SetNoneDisplay(!(sBlogFileType.Equals(ViCommConst.BlogFileType.MOVIE)));
				break;
			case "$DIV_IS_DRAFT":
				SetNoneDisplay(!(sBlogArticleStatus.Equals(ViCommConst.BlogArticleStatus.DRAFT)));
				break;
			case "$DIV_IS_NOT_DRAFT":
				SetNoneDisplay((sBlogArticleStatus.Equals(ViCommConst.BlogArticleStatus.DRAFT)));
				break;
			case "$BLOG_OBJ_SEQ":
				SetFieldValue(pTag, "OBJ_SEQ", out sValue);
				break;
			case "$BLOG_ARTICLE_SEQ":
				SetFieldValue(pTag, "BLOG_ARTICLE_SEQ", out sValue);
				break;
			case "$BLOG_ARTICLE_TITLE":
				SetFieldValue(pTag, "BLOG_ARTICLE_TITLE", out sValue);
				break;
			case "$BLOG_ARTICLE_DOC":
				sValue = GetBlogArticleDoc(pTag, pArgument,sSiteCd,sUserSeq,sUserCharNo,sBlogSeq,sBlogArticleSeq);
				break;
			case "$BLOG_ARTICLE_DOC_LENGTH":
				sValue = GetBlogArticleDocLength(pTag, pArgument, sSiteCd, sUserSeq, sUserCharNo, sBlogSeq, sBlogArticleSeq);
				break;
			case "$BLOG_ARTICLE_DOC_TAG_COUNT":
				sValue = BlogHelper.RegexBlogArticleTag.Matches(GetBlogArticleDocRaw(pTag, pArgument, sSiteCd, sUserSeq, sUserCharNo, sBlogSeq, sBlogArticleSeq)).Count.ToString();
				break;
			case "$BLOG_ARTICLE_REGIST_DATE":
				SetFieldValue(pTag, "REGIST_DATE", out sValue);
				break;
			case "$BLOG_ARTICLE_STATUS":
				SetFieldValue(pTag, "BLOG_ARTICLE_STATUS", out sValue);
				break;
			case "$BLOG_ARTICLE_STATUS_NM":
				SetFieldValue(pTag, "BLOG_ARTICLE_STATUS_NM", out sValue);
				break;
			case "$QUERY_BLOG_ARTICLE":
				sValue = GetQueryBlogArticle(sBlogArticleSeq);
				break;
			case "$QUERY_BLOG_OBJ":
				sValue = GetQueryBlogObj(sObjSeq);
				break;
			case "$BLOG_TITLE":
				SetFieldValue(pTag, "BLOG_TITLE", out sValue);
				break;
			case "$BLOG_SEQ":
				SetFieldValue(pTag, "BLOG_SEQ", out sValue);
				break;
			case "$READING_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$BLOG_ARTICLE_LIKE_COUNT":
				SetFieldValue(pTag,"BLOG_ARTICLE_LIKE_COUNT",out sValue);
				break;
			case "$BLOG_LIKE_COUNT":
				SetFieldValue(pTag,"BLOG_LIKE_COUNT",out sValue);
				break;
			// 写真用 =====================================================================
			case "$BLOG_PIC_IMG_PATH":
				SetFieldValue(pTag, "OBJ_SMALL_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$BLOG_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag, "OBJ_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$BLOG_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag, "OBJ_BLUR_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			// 動画用 =====================================================================
			case "$BLOG_MOVIE_IMG_PATH":
				SetFieldValue(pTag, "THUMBNAIL_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$HREF_BLOG_MOVIE_DOWNLOAD":
				sValue = GetHRefBlogMovieDwonload(pTag, pArgument,sSiteCd,sUserSeq,sUserCharNo,sObjSeq);
				break;
			
			default:
				pParsed = false;
				break;
		}
		
		return sValue;
	}

	protected string GetBlogArticleDocRaw(string pTag, string pArgument, string pSiteCd, string pUserSeq, string pUserCharNo, string pBlogSeq, string pBlogArticleSeq) {

		string[] oValues;
		if (!GetValues(pTag, "BLOG_ARTICLE_BODY1,BLOG_ARTICLE_BODY2,BLOG_ARTICLE_BODY3,BLOG_ARTICLE_BODY4,BLOG_ARTICLE_BODY5", out oValues)) {
			return string.Empty;
		}

		StringBuilder oDocBuilder = new StringBuilder();
		for (int i = 0; i < 5; i++) {
			oDocBuilder.Append(oValues[i]);
		}
		return oDocBuilder.ToString().Trim();

	}

	


}
