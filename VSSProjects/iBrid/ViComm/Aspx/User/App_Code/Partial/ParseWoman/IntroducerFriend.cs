﻿using System;
using System.Text;
using System.Data;
using iBridCommLib;
using ViComm;
using System.Collections.Specialized;
using System.Globalization;

partial class ParseWoman {
	private string ParseIntroducerFriend(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {

			case "$FRIEND_LOGIN_ID":
				SetFieldValue(pTag, "LOGIN_ID", out sValue);
				break;

			case "$FRIEND_HANDLE_NM":
				SetFieldValue(pTag, "HANDLE_NM", out sValue);
				break;
			
			case "$FRIEND_IMG_PATH":
				SetFieldValue(pTag, "SMALL_PHOTO_IMG_PATH", out sValue);
				break;

			case "$FRIEND_AGE":
				SetFieldValue(pTag, "AGE", out sValue);
				break;

			case "$FRIEND_REGIST_DATE":
				SetFieldValue(pTag, "REGIST_DATE", out sValue);
				break;

			case "$FRIEND_TOTAL_RECEIPT_AMT":
				SetFieldValue(pTag, "TOTAL_RECEIPT_AMT", out sValue);
				break;

			case "$FRIEND_IS_MAN_FLAG": {
					string sSexCd = string.Empty;
					SetFieldValue(pTag, "SEX_CD", out sSexCd);
					sValue = (sSexCd.Equals(ViCommConst.MAN)) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				}
				break;

			case "$FRIEND_IS_CAST_FLAG": {
					string sSexCd = string.Empty;
					SetFieldValue(pTag, "SEX_CD", out sSexCd);
					sValue = (sSexCd.Equals(ViCommConst.OPERATOR)) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				}
				break;

			case "$FRIEND_HAS_PROFILE_PIC": {
					string sPicSeq = string.Empty;
					SetFieldValue(pTag, "PIC_SEQ", out sPicSeq);
					sValue = (!string.IsNullOrEmpty(sPicSeq)) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				}
				break;

			case "$FRIEND_REGIST_SERVICE_POINT_FLAG":
				SetFieldValue(pTag,"REGIST_SERVICE_POINT_FLAG",out sValue);
				break;
			
			case "$NOT_CHECK_MAN_INTRO_COUNT":
				SetFieldValue(pTag,"NOT_CHECK_MAN_INTRO_COUNT",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
