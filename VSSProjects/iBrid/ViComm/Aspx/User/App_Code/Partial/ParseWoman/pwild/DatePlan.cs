﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ソーシャルゲーム デートプラン検索結果
--	Progaram ID		: Request
--  Creation Date	: 2012.10.08
--  Creater			: PW Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseDatePlan(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$DATE_PLAN_NM":
				this.parseWoman.SetFieldValue(pTag,"DATE_PLAN_NM",out sValue);
				break;
			case "$DATE_PLAN_CHARGE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"CHARGE_FLAG",out sValue);
				break;
			case "$PAYMENT":
				this.parseWoman.SetFieldValue(pTag,"PAYMENT",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$GAME_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$FRIENDLY_RANK":
				this.parseWoman.SetFieldValue(pTag,"FRIENDLY_RANK",out sValue);
				break;
			case "$GAME_MAN_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}