﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ検索結果
--	Progaram ID		: MailLottery
--  Creation Date	: 2015.01.05
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseMailLottery(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$POSSESSION_COUNT":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_COUNT",out sValue);
				break;
			case "$NEED_SECOND_LOTTERY_COUNT":
				this.parseWoman.SetFieldValue(pTag,"NEED_SECOND_LOTTERY_COUNT",out sValue);
				break;
			case "$CAN_GET_SECOND_LOTTERY_COUNT":
				this.parseWoman.SetFieldValue(pTag,"CAN_GET_SECOND_LOTTERY_COUNT",out sValue);
				break;
			case "$MAIL_LOTTERY_RATE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"MAIL_LOTTERY_RATE_SEQ",out sValue);
				break;
			case "$MAIL_LOTTERY_RATE_NM":
				this.parseWoman.SetFieldValue(pTag,"MAIL_LOTTERY_RATE_NM",out sValue);
				break;
			case "$REWARD_POINT":
				this.parseWoman.SetFieldValue(pTag,"REWARD_POINT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
