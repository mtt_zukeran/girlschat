﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい検索結果
--	Progaram ID		: Request
--  Creation Date	: 2012.07.03
--  Creater			: PW K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseRequest(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$REQUEST_SEQ":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_SEQ",out sValue);
				break;
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$TITLE":
				this.parseWoman.SetFieldValue(pTag,"TITLE",out sValue);
				break;
			case "$TEXT":
				this.parseWoman.SetFieldValue(pTag,"TEXT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$ADMIN_COMMENT":
				this.parseWoman.SetFieldValue(pTag,"ADMIN_COMMENT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$GOOD_COUNT":
				this.parseWoman.SetFieldValue(pTag,"GOOD_COUNT",out sValue);
				break;
			case "$BAD_COUNT":
				this.parseWoman.SetFieldValue(pTag,"BAD_COUNT",out sValue);
				break;
			case "$OVERLAP_COUNT":
				this.parseWoman.SetFieldValue(pTag,"OVERLAP_COUNT",out sValue);
				break;
			case "$VALUE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"VALUE_COUNT",out sValue);
				break;
			case "$VALUE_COMMENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"VALUE_COMMENT_COUNT",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$UPDATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			case "$LAST_VALUE_DATE":
				this.parseWoman.SetFieldValue(pTag,"LAST_VALUE_DATE",out sValue);
				break;
			case "$DELETE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"DELETE_FLAG",out sValue);
				break;
			case "$REQUEST_CATEGORY_SEQ":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_CATEGORY_SEQ",out sValue);
				break;
			case "$REQUEST_CATEGORY_NM":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_CATEGORY_NM",out sValue);
				break;
			case "$REQUEST_PROGRESS_SEQ":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_PROGRESS_SEQ",out sValue);
				break;
			case "$REQUEST_PROGRESS_NM":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_PROGRESS_NM",out sValue);
				break;
			case "$REQUEST_PROGRESS_COLOR":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_PROGRESS_COLOR",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$SELF_REQUEST_VALUE_EXIST_FLAG":
				sValue = this.GetSelfRequestValueExistFlag(pTag);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetSelfRequestValueExistFlag(string pTag) {
		string sRequestSeq;
		decimal dRecCount;

		this.parseWoman.SetFieldValue(pTag,"REQUEST_SEQ",out sRequestSeq);

		RequestValueSeekCondition oCondition = new RequestValueSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.RequestSeq = sRequestSeq;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;

		using (RequestValue oRequestValue = new RequestValue()) {
			oRequestValue.GetPageCount(oCondition,1,out dRecCount);
		}

		return (dRecCount > 0) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}
}