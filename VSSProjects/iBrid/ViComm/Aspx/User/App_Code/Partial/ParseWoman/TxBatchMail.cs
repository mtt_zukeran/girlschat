﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseTxBatchMail(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		string sObjType = string.Empty;

		switch (pTag) {
			case "$BATCH_MAIL_COUNT":
				SetFieldValue(pTag,"MAIL_COUNT",out sValue);
				break;
			case "$BATCH_MAIL_DATE":
				SetFieldValue(pTag,"REQUEST_TX_DATE",out sValue);
				break;
			case "$BATCH_MAIL_DOC":
				sValue = GetBatchMailDoc(pTag);
				sValue = MailHelper.ConvertVarForDisplay(sValue);
				break;
			case "$BATCH_MAIL_SEARCH_CONDITION":
				sValue = GetBatchMailSearchCondition(pTag,pArgument);
				break;
			case "$BATCH_MAIL_TITLE":
				SetFieldValue(pTag,"MAIL_TITLE",out sValue);
				sValue = MailHelper.ConvertVarForDisplay(sValue);
				break;
			case "$DIV_IS_MAIL_WITH_MOVIE":
				if (GetValue(pTag,"ATTACHED_OBJ_TYPE",out sObjType)) {
					SetNoneDisplay(!(sObjType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())));
				}
				break;
			case "$DIV_IS_MAIL_WITH_PIC":
				if (GetValue(pTag,"ATTACHED_OBJ_TYPE",out sObjType)) {
					SetNoneDisplay(!(sObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())));
				}
				break;
			case "$QUERY_VIEW_BATCH_MAIL":
				sValue = GetQueryViewBatchMail(pTag);
				break;
			case "$HREF_SELF_MOVIE_DOWNLOAD":
				sValue = GetHrefSelfMovieDownload(pTag,pArgument);
				break;
			case "$MAIL_PIC_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				break;
			#region ---------- 一括未送信メール管理 ----------
			case "$BATCH_MAIL_TEMP_ID":
				SetFieldValue(pTag,"OBJ_TEMP_ID",out sValue);
				break;
			case "$BATCH_DELETE_CHECK_BOX":
				sValue = GetCancelCheckBoxTag(pTag);
				break;
			#endregion
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetBatchMailDoc(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "",sDoc5 = "";
		if (GetValue(pTag,"ORIGINAL_DOC1",out sDoc1) && GetValue(pTag,"ORIGINAL_DOC2",out sDoc2) && GetValue(pTag,"ORIGINAL_DOC3",out sDoc3) && GetValue(pTag,"ORIGINAL_DOC4",out sDoc4) && GetValue(pTag,"ORIGINAL_DOC5",out sDoc5)) {
			sValue = parseContainer.Parse(Regex.Replace(sDoc1 + sDoc2 + sDoc3 + sDoc4 + sDoc5,"\r\n","<br />"));
		}
		return sValue;
	}

	private string GetBatchMailSearchCondition(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');
		string sNameFormat = ParseArg(oArgs,0,"[{0}]");
		string sValueFormat = ParseArg(oArgs,1,"{0}");


		// REQUEST_TX_MAIL_SEQから検索条件(T_BATCH_MAIL_SEARCH_CND)を取得 
		string sValue;
		if (GetValue(pTag,"REQUEST_TX_MAIL_SEQ",out sValue)) {
			string mailSeq = sValue;
			DataSet ds;
			using (BatchMailSearchCnd oSearchCnd = new BatchMailSearchCnd()) {
				ds = oSearchCnd.GetList(mailSeq);
			}
			if (ds.Tables[0].Rows.Count > 0) {
				StringBuilder sb = new StringBuilder();
				foreach (DataRow row in ds.Tables[0].Rows) {
					sb.AppendFormat("{0}<br />{1}<br />",string.Format(sNameFormat,row["CND_NM"]),string.Format(sValueFormat,row["CND_VALUE"]));
				}
				return sb.ToString();
			}
		}
		return "";
	}

	private string GetQueryViewBatchMail(string pTag) {
		string sValue;
		if (GetValue(pTag,"RNUM",out sValue)) {
			return string.Format("pageno={0}",sValue);
		} else {
			return "";
		}
	}

	private string GetCancelCheckBoxTag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"REQUEST_TX_MAIL_SEQ,MAIL_COUNT",out sValues)) {
			return string.Format("<input type=\"hidden\" name=\"checkbox{0}\" value=\"{1}\" />",sValues[0],string.Format("{0},{1}",sValues[0],sValues[1]));
		} else {
			return "";
		}
	}
}
