﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseGameCharacter(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$PARTNER_USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"PARTNER_USER_SEQ",out sValue);
				break;
			case "$PARTNER_USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"PARTNER_USER_CHAR_NO",out sValue);
				break;
			case "$GAME_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_CHARACTER_TYPE":
				this.parseWoman.SetFieldValue(pTag,"GAME_CHARACTER_TYPE",out sValue);
				break;
			case "$GAME_CHARACTER_LEVEL":
				this.parseWoman.SetFieldValue(pTag,"GAME_CHARACTER_LEVEL",out sValue);
				break;
			case "$FELLOW_COUNT":
				this.parseWoman.SetFieldValue(pTag,"FELLOW_COUNT",out sValue);
				break;
			case "$WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"WIN_COUNT",out sValue);
				break;
			case "$LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"LOSS_COUNT",out sValue);
				break;
			case "$TOTAL_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_WIN_COUNT",out sValue);
				break;
			case "$TOTAL_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_LOSS_COUNT",out sValue);
				break;
			case "$TOTAL_PARTY_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_PARTY_WIN_COUNT",out sValue);
				break;
			case "$TOTAL_PARTY_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_PARTY_LOSS_COUNT",out sValue);
				break;
			case "$PARTY_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTY_WIN_COUNT",out sValue);
				break;
			case "$PARTY_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTY_LOSS_COUNT",out sValue);
				break;
			case "$ATTACK_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"ATTACK_WIN_COUNT",out sValue);
				break;
			case "$ATTACK_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"ATTACK_LOSS_COUNT",out sValue);
				break;
			case "$PARTY_ATTACK_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTY_ATTACK_WIN_COUNT",out sValue);
				break;
			case "$PARTY_ATTACK_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTY_ATTACK_LOSS_COUNT",out sValue);
				break;
			case "$DEFENCE_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"DEFENCE_WIN_COUNT",out sValue);
				break;
			case "$DEFENCE_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"DEFENCE_LOSS_COUNT",out sValue);
				break;
			case "$PARTY_DEFENCE_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTY_DEFENCE_WIN_COUNT",out sValue);
				break;
			case "$PARTY_DEFENCE_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTY_DEFENCE_LOSS_COUNT",out sValue);
				break;
			case "$GET_POINT":
				this.parseWoman.SetFieldValue(pTag,"GET_POINT",out sValue);
				break;
			case "$LOSS_POINT":
				this.parseWoman.SetFieldValue(pTag,"LOSS_POINT",out sValue);
				break;
			case "$TREASURE_GET_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TREASURE_GET_COUNT",out sValue);
				break;
			case "$TREASURE_LOSS_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TREASURE_LOSS_COUNT",out sValue);
				break;
			case "$FRIENDLY_RANK":
				this.parseWoman.SetFieldValue(pTag,"FRIENDLY_RANK",out sValue);
				break;
			case "$ATTACK_FORCE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"ATTACK_FORCE_COUNT",out sValue);
				break;
			case "$MAX_ATTACK_FORCE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"MAX_ATTACK_FORCE_COUNT",out sValue);
				break;
			case "$DEFENCE_FORCE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"DEFENCE_FORCE_COUNT",out sValue);
				break;
			case "$MAX_DEFENCE_FORCE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"MAX_DEFENCE_FORCE_COUNT",out sValue);
				break;
			case "$LAST_HUG_DAY":
				this.parseWoman.SetFieldValue(pTag,"LAST_HUG_DAY",out sValue);
				break;
			case "$LAST_HUG_DATE":
				this.parseWoman.SetFieldValue(pTag,"LAST_HUG_DATE",out sValue);
				break;
			case "$APPLICATION_DATE":
				this.parseWoman.SetFieldValue(pTag,"APPLICATION_DATE",out sValue);
				break;
			case "$FELLOW_APPLICATION_STATUS":
				this.parseWoman.SetFieldValue(pTag,"FELLOW_APPLICATION_STATUS",out sValue);
				break;
			case "$GAME_LIST_ATTACK_FORCE_COUNT":
				sValue = this.CreateListAttackForceCount(pArgument);
				break;
			case "$SUPPORT_USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"SUPPORT_USER_SEQ",out sValue);
				break;
			case "$SUPPORT_USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"SUPPORT_USER_CHAR_NO",out sValue);
				break;
			case "$SUPPORT_BATTLE_LOG_SEQ":
				this.parseWoman.SetFieldValue(pTag,"BATTLE_LOG_SEQ",out sValue);
				break;
			case "$SUPPORT_REQUEST_SUBSEQ":
				this.parseWoman.SetFieldValue(pTag,"SUPPORT_REQUEST_SUBSEQ",out sValue);
				break;
			case "$AIM_TREASURE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"AIM_TREASURE_SEQ",out sValue);
				break;
			case "$ATTACK_POWER":
				this.parseWoman.SetFieldValue(pTag,"ATTACK_POWER",out sValue);
				break;
			case "$USE_ATTACK_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USE_ATTACK_COUNT",out sValue);
				break;
			case "$USE_DEFENCE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USE_DEFENCE_COUNT",out sValue);
				break;
			case "$MOVE_GAME_POINT":
				this.parseWoman.SetFieldValue(pTag,"MOVE_GAME_POINT",out sValue);
				break;
			case "$GAME_LOTTERY_COMPLETE_DATE":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_COMPLETE_DATE",out sValue);
				break;
			case "$NA_FLAG":
				this.parseWoman.SetFieldValue(pTag,"NA_FLAG",out sValue);
				break;
			case "$PARTNER_NA_FLAG":
				this.parseWoman.SetFieldValue(pTag,"PARTNER_NA_FLAG",out sValue);
				break;
			case "$SITE_USE_STATUS":
				this.parseWoman.SetFieldValue(pTag,"SITE_USE_STATUS",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$GAME_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sValue);
				break;
			case "$GAME_ITEM_NM":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_NM",out sValue);
				break;
			case "$GAME_BATTLE_LOG_SEQ":
				this.parseWoman.SetFieldValue(pTag,"BATTLE_LOG_SEQ",out sValue);
				break;
			case "$GAME_ATTACK_WIN_FLAG":
				this.parseWoman.SetFieldValue(pTag,"ATTACK_WIN_FLAG",out sValue);
				break;
			case "$GAME_ATTACK_LOSE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"ATTACK_LOSE_FLAG",out sValue);
				break;
			case "$GAME_DEFENCE_WIN_FLAG":
				this.parseWoman.SetFieldValue(pTag,"DEFENCE_WIN_FLAG",out sValue);
				break;
			case "$GAME_DEFENCE_LOSE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"DEFENCE_LOSE_FLAG",out sValue);
				break;
			case "$GAME_SUPPORT_SUCCESS_FLAG":
				this.parseWoman.SetFieldValue(pTag,"SUPPORT_SUCCESS_FLAG",out sValue);
				break;
			case "$GAME_SUPPORT_FAIL_FLAG":
				this.parseWoman.SetFieldValue(pTag,"SUPPORT_FAIL_FLAG",out sValue);
				break;
			case "$GAME_SUPPORT_REQUEST_FLAG":
				this.parseWoman.SetFieldValue(pTag,"SUPPORT_REQUEST_FLAG",out sValue);
				break;
			case "$GAME_CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$GAME_NEWS_TYPE":
				this.parseWoman.SetFieldValue(pTag,"NEWS_TYPE",out sValue);
				break;
			case "$GAME_NEWS_ITEM_NM":
				this.parseWoman.SetFieldValue(pTag,"NEWS_ITEM_NM",out sValue);
				break;
			case "$GAME_CAST_TREASURE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_TREASURE_SEQ",out sValue);
				break;
			case "$GAME_BATTLE_WIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"BATTLE_WIN_COUNT",out sValue);
				break;
			case "$GAME_EXP":
				this.parseWoman.SetFieldValue(pTag,"EXP",out sValue);
				break;
			case "$GAME_RANK":
				this.parseWoman.SetFieldValue(pTag,"GAME_RANK",out sValue);
				break;
			case "$GAME_MAN_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_ONLINE_STATUS":
				this.parseWoman.SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$MAN_TOTAL_TALK_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_TALK_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_TX_MAIL_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_TX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_TOTAL_RX_MAIL_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_RX_MAIL_COUNT",out sValue);
				break;
			case "$MAN_LARGE_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$GAME_FAVORITE":
				this.parseWoman.SetFieldValue(pTag,"GAME_FAVORITE",out sValue);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$LAST_PARTNER_HUG_DATE":
				this.parseWoman.SetFieldValue(pTag,"LAST_PARTNER_HUG_DATE",out sValue);
				break;
			case "$GAME_AGE":
				this.parseWoman.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_FIND_RE_QUERY":
				sValue = GetGameFindReQuery();
				break;
			case "$GAME_UPDATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			case "$GAME_TREASURE_PIC_COUNT":
				sValue = GetManTreasurePicCount();
				break;
			case "$GAME_PAYMENT":
				this.parseWoman.SetFieldValue(pTag,"PAYMENT",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				this.parseWoman.SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$GAME_USED_DATE":
				this.parseWoman.SetFieldValue(pTag,"USED_DATE",out sValue);
				break;
			case "$GAME_CHARACTER_REGIST_DATE":
				this.parseWoman.SetFieldValue(pTag,"GAME_CHARACTER_REGIST_DATE",out sValue);
				break;
			case "$GAME_LIST_TUTORIAL_ATTACK_FORCE_COUNT":
				sValue = this.CreateListTutorialAttackForceCount(pArgument);
				break;
			case "$GAME_CAST_TREASURE_NM":
				this.parseWoman.SetFieldValue(pTag,"CAST_TREASURE_NM",out sValue);
				break;
			case "$DISTINCTION_CD":
				this.parseWoman.SetFieldValue(pTag,"DISTINCTION_CD",out sValue);
				break;
			case "$GAME_SUPPORT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"SUPPORT_COUNT",out sValue);
				break;
			case "$GAME_PARTNER_SUPPORT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTNER_SUPPORT_COUNT",out sValue);
				break;
			case "$GAME_HUG_COUNT":
				this.parseWoman.SetFieldValue(pTag,"HUG_COUNT",out sValue);
				break;
			case "$GAME_PARTNER_HUG_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTNER_HUG_COUNT",out sValue);
				break;
			case "$GAME_PRESENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PRESENT_COUNT",out sValue);
				break;
			case "$GAME_PARTNER_PRESENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PARTNER_PRESENT_COUNT",out sValue);
				break;
			case "$ENEMY_USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"ENEMY_USER_SEQ",out sValue);
				break;
			case "$ENEMY_USER_CHAR_NO":
				this.parseWoman.SetFieldValue(pTag,"ENEMY_USER_CHAR_NO",out sValue);
				break;
			case "$ENEMY_GAME_HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"ENEMY_GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_PARTY_MEMBER_FLAG":
				this.parseWoman.SetFieldValue(pTag,"PARTY_MEMBER_FLAG",out sValue);
				break;
			case "$DIV_IS_SUPPORT_REQUEST_END": {
					string sSupportRequestEndFlag = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"SUPPORT_REQUEST_END_FLAG",out sSupportRequestEndFlag);
					this.parseWoman.SetNoneDisplay(!sSupportRequestEndFlag.Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$DIV_IS_NOT_SUPPORT_REQUEST_END": {
					string sSupportRequestEndFlag = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"SUPPORT_REQUEST_END_FLAG",out sSupportRequestEndFlag);
					this.parseWoman.SetNoneDisplay(sSupportRequestEndFlag.Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$GAME_OBJ_TYPE":
				this.parseWoman.SetFieldValue(pTag,"OBJ_TYPE",out sValue);
				break;
			case "$GAME_OBJ_TITLE":
				this.parseWoman.SetFieldValue(pTag,"OBJ_TITLE",out sValue);
				break;
			case "$GAME_OBJ_SEQ":
				this.parseWoman.SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;
			case "$PRESENT_GAME_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"PRESENT_GAME_ITEM_SEQ",out sValue);
				break;
			case "$SMALL_PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$GAME_SMALL_PHOTO_IMG_PATH":
				sValue = GetGameSmallPhotoImgPath(pTag,pArgument);
				break;
			case "$GAME_RNUM":
				this.parseWoman.SetFieldValue(pTag,"RNUM",out sValue);
				break;
			case "$DEFENCE_POWER":
				this.parseWoman.SetFieldValue(pTag,"DEFENCE_POWER",out sValue);
				break;
			case "$ENDURANCE_NM":
				this.parseWoman.SetFieldValue(pTag,"ENDURANCE_NM",out sValue);
				break;
			case "$ITEM_COUNT":
				this.parseWoman.SetFieldValue(pTag,"ITEM_COUNT",out sValue);
				break;
			case "$MAN_WAITING_TYPE":
				this.parseWoman.SetFieldValue(pTag,"MAN_PF_WAITING_TYPE_NM",out sValue);
				break;
			case "$MAN_STATUS_WAITING_FLAG":
				sValue = this.parseWoman.IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$MAN_TV_TALK_OK_FLAG":
				sValue = this.parseWoman.IsEqual(pTag,this.parseWoman.IsTvTalkEnable(pTag) && !this.parseWoman.IsVoiceTalkEnable(pTag));
				break;
			case "$MAN_VOICE_TALK_OK_FLAG":
				sValue = this.parseWoman.IsEqual(pTag,!this.parseWoman.IsTvTalkEnable(pTag) && this.parseWoman.IsVoiceTalkEnable(pTag));
				break;
			case "$MAN_VOICE_TV_TALK_OK_FLAG":
				sValue = this.parseWoman.IsEqual(pTag,this.parseWoman.IsTvTalkEnable(pTag) && this.parseWoman.IsVoiceTalkEnable(pTag));
				break;
			case "$HREF_TV_WSHOT":
				sValue = this.parseWoman.GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = this.parseWoman.GetHrefVoiceWShot(pTag,pArgument);
				break;
			case "$GAME_MAX_ATTACK_FORCE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"MAX_ATTACK_FORCE_COUNT",out sValue);
				break;
			case "$REC_NO_PER_PAGE":
				this.parseWoman.SetFieldValue(pTag,"REC_NO_PER_PAGE",out sValue);
				break;
			case "$GAME_INFORMATION_TYPE":
				this.parseWoman.SetFieldValue(pTag,"INFORMATION_TYPE",out sValue);
				break;
			case "$GAME_STAGE_GROUP_TYPE":
				this.parseWoman.SetFieldValue(pTag,"STAGE_GROUP_TYPE",out sValue);
				break;
			case "$TREASURE_COMPLETE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"TREASURE_COMPLETE_FLAG",out sValue);
				break;
			case "$BONUS_GET_FLAG":
				this.parseWoman.SetFieldValue(pTag,"BONUS_GET_FLAG",out sValue);
				break;
			case "$USE_MOSAIC_ERASE_POINT":
				this.parseWoman.SetFieldValue(pTag,"USE_MOSAIC_ERASE_POINT",out sValue);
				break;
			case "$USE_MOVIE_TICKET_POINT":
				this.parseWoman.SetFieldValue(pTag,"USE_MOVIE_TICKET_POINT",out sValue);
				break;
			case "$GET_DATE_POINT":
				this.parseWoman.SetFieldValue(pTag,"GET_DATE_POINT",out sValue);
				break;
			case "$GET_PRESENT_GAME_ITEM_POINT":
				this.parseWoman.SetFieldValue(pTag,"GET_PRESENT_GAME_ITEM_POINT",out sValue);
				break;
			case "$GET_COMPLETE_BONUS_POINT":
				this.parseWoman.SetFieldValue(pTag,"GET_COMPLETE_BONUS_POINT",out sValue);
				break;
			case "$GAME_TOTAL_GET_POINT":
				this.parseWoman.SetFieldValue(pTag,"GAME_TOTAL_GET_POINT",out sValue);
				break;
			case "$USE_MOSAIC_ERASE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USE_MOSAIC_ERASE_COUNT",out sValue);
				break;
			case "$TOTAL_PIC_POSSESSION_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOTAL_PIC_POSSESSION_COUNT",out sValue);
				break;
			case "$PRESENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"PRESENT_COUNT",out sValue);
				break;
			case "$USE_MOVIE_TICKET_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USE_MOVIE_TICKET_COUNT",out sValue);
				break;
			case "$USE_MOVIE_TICKET_COUNT_CHARGE":
				this.parseWoman.SetFieldValue(pTag,"USE_MOVIE_TICKET_COUNT_CHARGE",out sValue);
				break;
			case "$USE_MOVIE_TICKET_COUNT_FREE":
				this.parseWoman.SetFieldValue(pTag,"USE_MOVIE_TICKET_COUNT_FREE",out sValue);
				break;
			case "$CHARGE_DATE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"CHARGE_DATE_COUNT",out sValue);
				break;
			case "$FREE_DATE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"FREE_DATE_COUNT",out sValue);
				break;
			case "$MOVIE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;
			case "$MOVIE_TITLE":
				this.parseWoman.SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$MOVIE_DOC":
				this.parseWoman.SetFieldValue(pTag,"MOVIE_DOC",out sValue);
				break;
			case "$CAST_GAME_PIC_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_GAME_PIC_SEQ",out sValue);
				break;
			case "$PIC_TITLE":
				this.parseWoman.SetFieldValue(pTag,"PIC_TITLE",out sValue);
				break;
			case "$PIC_DOC":
				this.parseWoman.SetFieldValue(pTag,"PIC_DOC",out sValue);
				break;
			case "$GAME_PAYMENT_TYPE":
				this.parseWoman.SetFieldValue(pTag,"GAME_PAYMENT_TYPE",out sValue);
				break;
			case "$RANK":
				this.parseWoman.SetFieldValue(pTag,"RANK",out sValue);
				break;
			case "$GAME_PIC_NON_PUBLISH_DATE":
				this.parseWoman.SetFieldValue(pTag,"GAME_PIC_NON_PUBLISH_DATE",out sValue);
				break;
			case "$ACTION_TYPE":
				this.parseWoman.SetFieldValue(pTag,"ACTION_TYPE",out sValue);
				break;
			case "$MAN_LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;

			#region □■□■□ プレゼント履歴 □■□■□
			case "$PRESENT_HISTORY_SEQ":
				this.parseWoman.SetFieldValue(pTag,"PRESENT_HISTORY_SEQ",out sValue);
				break;
			case "$PRESENT_HISTORY_GET_FLAG":
				this.parseWoman.SetFieldValue(pTag,"GET_FLAG",out sValue);
				break;
			case "$PRESENT_POINT":
				this.parseWoman.SetFieldValue(pTag,"PRESENT_POINT",out sValue);
				break;
			case "$GAME_ITEM_CATEGORY_TYPE":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_CATEGORY_TYPE",out sValue);
				break;
			#endregion
			
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string CreateListAttackForceCount(string pArgument) {
		string sValue = string.Empty;

		string sForceCountLimit = pArgument;
		int iForceCountLimit;
		int.TryParse(sForceCountLimit,out iForceCountLimit);
		double dForceCount = iForceCountLimit;

		if (iForceCountLimit > 0) {
			StringBuilder sValueBuilder = new StringBuilder();
			sValueBuilder.AppendLine("<select name=\"attack_force_count\">");

			if (iForceCountLimit <= 10) {
				for (int i = iForceCountLimit;i >= 1;i--) {
					sValueBuilder.AppendLine(string.Format("<option value=\"{0}\">{0}人</option>",i.ToString()));
				}
			} else {
				for (int i = 1;i <= 10;i++) {
					sValueBuilder.AppendLine(string.Format("<option value=\"{0}\">{0}人</option>",dForceCount.ToString()));
					dForceCount = iForceCountLimit - Math.Floor((iForceCountLimit * 0.1 * i));
				}
			}

			sValueBuilder.AppendLine("</select>");

			sValue = sValueBuilder.ToString();
		}
		
		return sValue;
	}

	private string GetListPartyBattleMember(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		ManGameCharacterFellowSeekCondition oCondition = new ManGameCharacterFellowSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
		oCondition.PartyMemberFlag = ViCommConst.FLAG_ON_STR;

		ManGameCharacterFellow oManGameCharacterFellow = new ManGameCharacterFellow(this.parseWoman.sessionObjs);
		oDataSet = oManGameCharacterFellow.GetPartyMember(oCondition);

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetGameFellowData(string pTag,string pArgument) {
		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameFellowSeekCondition oCondition = new GameFellowSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.FellowApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;

		DataSet oTmpDataSet = null;
		using (GameFellow oGameFellow = new GameFellow()) {
			oTmpDataSet = oGameFellow.GetOneByUserSeqForCast(oCondition);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		
		return sValue;
	}

	private string GetTotalFriendlyPoint(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] arrText = pArgument.Split(',');

		if (arrText.Length == 2) {
			FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.UserSeq = arrText[0];
			oCondition.UserCharNo = arrText[1];
			oCondition.PartnerUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
			oCondition.PartnerUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
			using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
				sValue = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
			}
		}

		return sValue;
	}

	private string GetFellowLog(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		NewsSeekCondition oCondition = new NewsSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		DataSet oDataSet = null;
		using (News oNews = new News()) {
			oDataSet = oNews.GetPageCollectionManFellowLog(oCondition,1,pPartsArgs.NeedCount);
		}
		if (oDataSet == null) {
			return string.Empty;
		}
		return this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
	}

	private string GetQueryManProfile(string pTag) {
		string[] sValues;
		if (this.parseWoman.GetValues(pTag,"LOGIN_ID,RNUM",out sValues)) {
			return string.Format(
				"seekmode={0}&loginid={1}&userrecno={2}&site={3}",
				this.parseWoman.sessionWoman.seekMode,
				sValues[0],
				sValues[1],
				this.parseWoman.sessionWoman.site.siteCd
			) + this.parseWoman.sessionWoman.seekCondition.GetConditionQuery() + this.parseWoman.sessionWoman.AddUniqueQuery(this.parseWoman.GetDataRow(this.parseWoman.currentTableIdx),this.parseWoman.sessionWoman.requestQuery);
		} else {
			return "";
		}
	}

	private string GetListGameCharacterFellowNoHug(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManGameCharacterFellowMemberSeekCondition oCondition = new ManGameCharacterFellowMemberSeekCondition(pPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
		oCondition.ExceptStatusOfflineFlag = ViCommConst.FLAG_ON_STR;
		oCondition.LastHugDay = DateTime.Now.ToString("yyyy/MM/dd");
		DataSet oDataSet = null;
		using (ManGameCharacterFellowMember oManGameCharacterFellowMember = new ManGameCharacterFellowMember()) {
			oDataSet = oManGameCharacterFellowMember.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}
		if (oDataSet == null) {
			return string.Empty;
		}
		return this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
	}

	private string GetFriendlyPoint(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] arrText = pArgument.Split(',');

		if (arrText.Length == 2) {
			FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
			oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
			oCondition.PartnerUserSeq = arrText[0];
			oCondition.PartnerUserCharNo = arrText[1];

			using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
				sValue = oFriendlyPoint.GetFriendlyPoint(oCondition);
			}
		}

		return sValue;
	}

	private string GetManGameCharacterOuter(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		UserManGameCharacterSeekCondition oCondition = new UserManGameCharacterSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SelfUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.SelfUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (UserManGameCharacter oUserManGameCharacter = new UserManGameCharacter()) {
			oDataSet = oUserManGameCharacter.GetOneOuter(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetGameFindReQuery() {
		string sRequestQuery = string.Empty;
		string sValue = string.Empty;
		foreach (string sKey in this.parseWoman.sessionWoman.requestQuery.QueryString) {
			if (string.IsNullOrEmpty(sKey)) {
				continue;
			}
			if (sKey.StartsWith("sort")) {
				continue;
			}
			if (sKey.Equals("pageno")) {
				continue;
			}
			if (sRequestQuery.Equals(string.Empty)) {
				sRequestQuery = sKey + "=" + HttpUtility.UrlDecode(this.parseWoman.sessionWoman.requestQuery.QueryString[sKey]);
			} else {
				sRequestQuery = "&" + sKey + "=" + HttpUtility.UrlDecode(this.parseWoman.sessionWoman.requestQuery.QueryString[sKey]);
			}
			sValue += sRequestQuery;
		}
		return sValue;
	}

	private string GetManTreasurePicCount() {
		string sValue = string.Empty;
		
		CastGameCharacterSeekCondition oCondition = new CastGameCharacterSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			sValue = oCastGameCharacter.GetManTreasurePicCount(oCondition);
		}

		return sValue;
	}

	private string GetPaymentRank(string pTag,string pArgument) {
		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameCharacterPerformanceSeekCondition oCondition = new GameCharacterPerformanceSeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_PAYMENT;
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		DataSet oTmpDataSet = null;
		using (GameCharacterPerformance oGameCharacterPerformance = new GameCharacterPerformance()) {
			oTmpDataSet = oGameCharacterPerformance.GetMyRankData(oCondition);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetPaymentRankList(string pTag,string pArgument) {
		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameCharacterPerformanceSeekCondition oCondition = new GameCharacterPerformanceSeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_PAYMENT;
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.RankType = PwViCommConst.RankType.PAYMENT;

		DataSet oTmpDataSet = null;
		using (GameCharacterPerformance oGameCharacterPerformance = new GameCharacterPerformance()) {
			oTmpDataSet = oGameCharacterPerformance.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetMosaicRankData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		
		string sSiteCd = parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter(parseWoman.sessionObjs)) {
			oTmpDataSet = oCastGameCharacter.GetMosaicRank(sSiteCd,sUserSeq,sUserCharNo,pPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		if (oTmpDataSet.Tables[0].Rows.Count < pPartsArgs.NeedCount) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}
	
	private string GetSelfMovie(string pTag,string pArgument) {

		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		CastGameCharacterMovieManagementSeekCondition oCondition = new CastGameCharacterMovieManagementSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		if (string.IsNullOrEmpty(oCondition.PublishFlag)) {
			oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;
		}

		DataSet oTmpDataSet = null;
		using (CastGameCharacterMovieManagement oCastGameCharacterMovieManagement = new CastGameCharacterMovieManagement()) {
			oTmpDataSet = oCastGameCharacterMovieManagement.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_SELF_MOVIE);

		return sValue;
	}

	private string GetFavoriteLog(string pTag,string pArgument) {

		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		
		NewsSeekCondition oCondition = new NewsSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		DataSet oDataSet = null;
		using (News oNews = new News()) {
			oDataSet = oNews.GetPageCollectionManFavoriteLog(oCondition,1,pPartsArgs.NeedCount);
		}
		if (oDataSet == null) {
			return string.Empty;
		}
		
		sValue = this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetManGameCharacterNew(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;

		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManGameCharacterFindSeekCondition oCondition = new ManGameCharacterFindSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.Sort = PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_NEW;

		using (ManGameCharacterFind oManGameCharacterFind = new ManGameCharacterFind(parseWoman.sessionObjs)) {
			oDataSet = oManGameCharacterFind.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetGamePartnerHug(string pTag,string pArgument) {

		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		ManGameCommunicationPartnerHugSeekCondition oCondition = new ManGameCommunicationPartnerHugSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		DataSet oDataSet = null;
		using (ManGameCommunicationPartnerHug oManGameCommunicationPartnerHug = new ManGameCommunicationPartnerHug()) {
			oDataSet = oManGameCommunicationPartnerHug.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string CreateListTutorialAttackForceCount(string pArgument) {
		string sValue = string.Empty;

		string sForceCountLimit = pArgument;
		int iForceCountLimit;
		int.TryParse(sForceCountLimit,out iForceCountLimit);

		if (iForceCountLimit > 0) {
			StringBuilder sValueBuilder = new StringBuilder();
			sValueBuilder.AppendLine("<select name=\"attack_force_count\">");

			sValueBuilder.AppendLine(string.Format("<option value=\"{0}\">{0}人</option>",iForceCountLimit.ToString()));

			sValueBuilder.AppendLine("</select>");

			sValue = sValueBuilder.ToString();
		}

		return sValue;
	}

	private string GetBattleLog(string pTag,string pArgument) {

		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		BattleLogSeekCondition oCondition = new BattleLogSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		DataSet oDataSet = null;
		using (BattleLog oBattleLog = new BattleLog()) {
			oDataSet = oBattleLog.GetPageCollectionWoman(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetGameObjUsedHistoryData(string pTag,string pArgument) {
		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameObjUsedHistorySeekCondition oCondition = new GameObjUsedHistorySeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.ObjCategory = ViCommConst.ATTACHED_SOCIAL_GAME.ToString();
		
		DataSet oDataSet = null;
		using (GameObjUsedHistory oGameObjUsedHistory = new GameObjUsedHistory()) {
			oDataSet = oGameObjUsedHistory.GetObjUsedHistoryPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		if (oDataSet == null)
			return string.Empty;

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetWomanSweetHeartCount(string pTag,string pArgument) {
		string sValue = string.Empty;

		using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
			sValue = oFriendlyPoint.GetWomanSweetHeartCount(this.parseWoman.sessionWoman.site.siteCd,this.parseWoman.sessionWoman.userWoman.userSeq,this.parseWoman.sessionWoman.userWoman.curCharNo);
		}

		return sValue;
	}

	protected string GetGameFellowCountAllStatus(string pTag,string pArgument) {
		string sValue = string.Empty;

		GameFellowSeekCondition oCondition = new GameFellowSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (GameFellow oGameFellow = new GameFellow()) {
			sValue = oGameFellow.GetFellowCount(oCondition);
		}

		return sValue;
	}

	private string GetListManGameCharacter(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManGameCharacterViewSeekCondition oCondition = new ManGameCharacterViewSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		using (ManGameCharacterView oManGameCharacterView = new ManGameCharacterView(parseWoman.sessionObjs)) {
			oTmpDataSet = oManGameCharacterView.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}
		
		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		
		return sValue;
	}
	
	private bool CheckExistGameFavorite(string pTag,string pArgument) {
		bool bOk = false;
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (GameFavorite oGameFavorite = new GameFavorite()) {
			bOk = oGameFavorite.CheckExistGameFavorite(sSiteCd,sUserSeq,sUserCharNo);
		}

		return bOk;
	}

	private bool CheckExistManSweetHeart(string pTag,string pArgument) {
		string sValue = "0";

		string[] arrText = pArgument.Split(',');
		string sSiteCd = string.Empty;
		string sPartnerUserSeq = string.Empty;
		string sPartnerUserCharNo = string.Empty;

		if (arrText.Length == 2) {
			FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
			sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
			sPartnerUserSeq = arrText[0];
			sPartnerUserCharNo = arrText[1];

			using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
				sValue = oFriendlyPoint.GetSweetHeartCount(sSiteCd,sPartnerUserSeq,sPartnerUserCharNo);
			}
		}
		
		if(int.Parse(sValue) > 0) {
			return true;
		} else {
			return false;
		}
	}

	private string GetOneNewHug(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManGameCommunicationPartnerHugSeekCondition oCondition = new ManGameCommunicationPartnerHugSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (ManGameCommunicationPartnerHug oManGameCommunicationPartnerHug = new ManGameCommunicationPartnerHug()) {
			oTmpDataSet = oManGameCommunicationPartnerHug.GetOneNewHug(oCondition);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetOneSelfPartnerPresentUnchecked(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		GameCharacterPresentHistorySeekCondition oCondition = new GameCharacterPresentHistorySeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = 0;
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.PartnerUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.PartnerUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (GameCharacterPresentHistory oGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
			oTmpDataSet = oGameCharacterPresentHistory.GetOnePresentHistory(oCondition);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetOneFellowApplication(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManGameCharacterFellowSeekCondition oCondition = new ManGameCharacterFellowSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION;
		oCondition.Sort = PwViCommConst.ManGameCharacterFellowSort.APPLICATION_DATE_DESC;
		
		using (ManGameCharacterFellow oManGameCharacterFellow = new ManGameCharacterFellow()) {
			oTmpDataSet = oManGameCharacterFellow.GetOneFellowApplication(oCondition);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetOneSupportRequest(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (BattleLog oBattleLog = new BattleLog()) {
			oTmpDataSet = oBattleLog.GetOneSupportRequest(oCondition);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetInformation(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		GameInformationSeekCondition oCondition = new GameInformationSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (GameInformation oGameInformation = new GameInformation()) {
			oTmpDataSet = oGameInformation.GetPageCollectionForCast(oCondition,1,1);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetSweetHeartData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		ManGameCharacterFindSeekCondition oCondition = new ManGameCharacterFindSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.Relation = "1";
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(oPartsArgs.Query["sort"]))) {
			oCondition.Sort = PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_RANDOM;
		}

		using (ManGameCharacterFind oManGameCharacterFind = new ManGameCharacterFind(this.parseWoman.sessionObjs)) {
			oDataSet = oManGameCharacterFind.GetPageCollection(oCondition,1,1);
		}

		if (oDataSet.Tables[0].Rows.Count == 0) {
			oDataSet = new DataSet();

			DataTable oDt = new DataTable();
			DataRow oDr;
			oDataSet.Tables.Add(oDt);
			oDt.Columns.Add("USER_SEQ",Type.GetType("System.String"));
			oDr = oDt.NewRow();
			oDr["USER_SEQ"] = string.Empty;
			oDt.Rows.Add(oDr);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}
	
	private string GetGamePaymentLog(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		DailyGamePaymentLogSeekCondition oCondition = new DailyGamePaymentLogSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		using (DailyGamePaymentLog oDailyGamePaymentLog = new DailyGamePaymentLog()) {
			oTmpDataSet = oDailyGamePaymentLog.GetOne(oCondition);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}
		
		return sValue;
	}
	
	private string GetGameGetPaymentNews(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		
		GamePaymentLogSeekCondition oCondition = new GamePaymentLogSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SelfUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.SelfUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (GamePaymentLog oGamePaymentLog = new GamePaymentLog(parseWoman.sessionObjs)) {
			oTmpDataSet = oGamePaymentLog.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetSelfGameGetPaymentNewsCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		GamePaymentLogSeekCondition oCondition = new GamePaymentLogSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SelfUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.SelfUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (GamePaymentLog oGamePaymentLog = new GamePaymentLog(parseWoman.sessionObjs)) {
			oGamePaymentLog.GetPageCount(oCondition,1,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private string GetGameSmallPhotoImgPath(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] sValues;
		string sSiteUseStatus = string.Empty;

		if (this.parseWoman.GetValues(pTag,"SMALL_PHOTO_IMG_PATH,GAME_CHARACTER_TYPE",out sValues)) {
			if (this.parseWoman.sessionWoman.userWoman.CurCharacter != null) {
				sSiteUseStatus = this.parseWoman.sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus;
			}

			if (sSiteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
				if (!sValues[0].Equals("image/A001/photo_men1.gif")) {
					sValue = "../" + sValues[0];
				} else {
					sValue = string.Format("../Image/{0}/game/man_type_a{1}.gif",parseWoman.sessionWoman.site.siteCd,sValues[1]);
				}
			} else {
				sValue = string.Format("../Image/{0}/game/man_type_a{1}.gif",parseWoman.sessionWoman.site.siteCd,sValues[1]);
			}
		}

		return sValue;
	}
}
