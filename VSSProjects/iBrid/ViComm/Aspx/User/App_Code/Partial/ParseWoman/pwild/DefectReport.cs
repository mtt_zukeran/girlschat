﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 不具合報告検索結果
--	Progaram ID		: DefectReport
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseDefectReport(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$DEFECT_REPORT_SEQ":
				this.parseWoman.SetFieldValue(pTag,"DEFECT_REPORT_SEQ",out sValue);
				break;
			case "$TITLE":
				this.parseWoman.SetFieldValue(pTag,"TITLE",out sValue);
				break;
			case "$TEXT":
				this.parseWoman.SetFieldValue(pTag,"TEXT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$ADMIN_COMMENT":
				this.parseWoman.SetFieldValue(pTag,"ADMIN_COMMENT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$OVERLAP_COUNT":
				this.parseWoman.SetFieldValue(pTag,"OVERLAP_COUNT",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$UPDATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			case "$DELETE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"DELETE_FLAG",out sValue);
				break;
			case "$DEFECT_REPORT_CATEGORY_NM":
				this.parseWoman.SetFieldValue(pTag,"DEFECT_REPORT_CATEGORY_NM",out sValue);
				break;
			case "$DEFECT_REPORT_PROGRESS_NM":
				this.parseWoman.SetFieldValue(pTag,"DEFECT_REPORT_PROGRESS_NM",out sValue);
				break;
			case "$DEFECT_REPORT_PROGRESS_COLOR":
				this.parseWoman.SetFieldValue(pTag,"DEFECT_REPORT_PROGRESS_COLOR",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}