﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳画像検索結果
--	Progaram ID		: YakyukenPic
--  Creation Date	: 2013.04.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseYakyukenPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$YAKYUKEN_PIC_SEQ":
				this.parseWoman.SetFieldValue(pTag,"YAKYUKEN_PIC_SEQ",out sValue);
				break;
			case "$YAKYUKEN_GAME_SEQ":
				this.parseWoman.SetFieldValue(pTag,"YAKYUKEN_GAME_SEQ",out sValue);
				break;
			case "$YAKYUKEN_TYPE":
				this.parseWoman.SetFieldValue(pTag,"YAKYUKEN_TYPE",out sValue);
				break;
			case "$COMMENT_TEXT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_TEXT",out sValue);
				break;
			case "$AUTH_FLAG":
				this.parseWoman.SetFieldValue(pTag,"AUTH_FLAG",out sValue);
				break;
			case "$SHOW_FACE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"SHOW_FACE_FLAG",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$AUTH_DATE":
				this.parseWoman.SetFieldValue(pTag,"AUTH_DATE",out sValue);
				break;
			case "$VIEW_COUNT":
				this.parseWoman.SetFieldValue(pTag,"VIEW_COUNT",out sValue);
				break;
			case "$PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SMALL_PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			//野球拳進捗
			case "$SELF_LAST_WIN_YAKYUKEN_TYPE":
				this.parseWoman.SetFieldValue(pTag,"LAST_WIN_YAKYUKEN_TYPE",out sValue);
				break;
			case "$SELF_CLEAR_FLAG":
				this.parseWoman.SetFieldValue(pTag,"CLEAR_FLAG",out sValue);
				break;
			case "$SELF_CLEAR_YAKYUKEN_TYPE":
				this.parseWoman.SetFieldValue(pTag,"CLEAR_YAKYUKEN_TYPE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
