﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseCast(string pTag, string pArgument, out bool pParsed) {
			pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag, "LOGIN_ID", out sValue);
				break;
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag, "HANDLE_NM", out sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			//新人のアイドル
			case "$START_PERFORM_DAY":
				SetFieldValue(pTag,"START_PERFORM_DAY",out sValue);
				break;
			//投票順位
			case "$CAST_VOTE_RANK":
				SetFieldValue(pTag,"VOTE_RANK",out sValue);
				break;
			case "$CAST_VOTE_COUNT":
				SetFieldValue(pTag,"VOTE_COUNT",out sValue);
				break;
			case "$FIRST_PERIOD_START_DATE":
				SetFieldValue(pTag,"FIRST_PERIOD_START_DATE",out sValue);
				break;
			case "$FIRST_PERIOD_END_DATE":
				SetFieldValue(pTag,"FIRST_PERIOD_END_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_START_DATE":
				SetFieldValue(pTag,"SECOND_PERIOD_START_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_END_DATE":
				SetFieldValue(pTag,"SECOND_PERIOD_END_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_FLAG":
				SetFieldValue(pTag,"SECOND_PERIOD_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
}
