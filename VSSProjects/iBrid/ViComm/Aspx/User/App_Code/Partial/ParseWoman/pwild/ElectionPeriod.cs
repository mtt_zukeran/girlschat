﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドル選挙機関設定検索結果
--	Progaram ID		: ElectionPeriod
--  Creation Date	: 2013.12.13
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseElectionPeriod(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$ENTRY_START_DATE":
				this.parseWoman.SetFieldValue(pTag,"ENTRY_START_DATE",out sValue);
				break;
			case "$ENTRY_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"ENTRY_END_DATE",out sValue);
				break;
			case "$FIRST_PERIOD_START_DATE":
				this.parseWoman.SetFieldValue(pTag,"FIRST_PERIOD_START_DATE",out sValue);
				break;
			case "$FIRST_PERIOD_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"FIRST_PERIOD_END_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_START_DATE":
				this.parseWoman.SetFieldValue(pTag,"SECOND_PERIOD_START_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"SECOND_PERIOD_END_DATE",out sValue);
				break;
			case "$DIV_IS_ELECTION_ENTRY": {
				string sElectionEntry = string.Empty;
				this.parseWoman.SetFieldValue(pTag,"ELECTION_ENTRY_FLAG",out sElectionEntry);
				this.parseWoman.SetNoneDisplay(!sElectionEntry.Equals(ViCommConst.FLAG_ON_STR));
				break;
			}
			case "$DIV_IS_NOT_ELECTION_ENTRY": {
				string sElectionEntry = string.Empty;
				this.parseWoman.SetFieldValue(pTag,"ELECTION_ENTRY_FLAG",out sElectionEntry);
				this.parseWoman.SetNoneDisplay(sElectionEntry.Equals(ViCommConst.FLAG_ON_STR));
				break;
			}
			case "$ELECTION_ENTRY_FLAG":
				this.parseWoman.SetFieldValue(pTag,"ELECTION_ENTRY_FLAG",out sValue);
				break;
			case "$RETIRE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"RETIRE_FLAG",out sValue);
				break;
			case "$REFUSE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
