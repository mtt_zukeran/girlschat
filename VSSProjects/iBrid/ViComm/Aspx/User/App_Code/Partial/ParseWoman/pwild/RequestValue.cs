﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい評価検索結果
--	Progaram ID		: Request
--  Creation Date	: 2012.07.03
--  Creater			: PW K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseRequestValue(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$REQUEST_VALUE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_VALUE_SEQ",out sValue);
				break;
			case "$REQUEST_SEQ":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_SEQ",out sValue);
				break;
			case "$USER_SEQ":
				this.parseWoman.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$VALUE_NO":
				this.parseWoman.SetFieldValue(pTag,"VALUE_NO",out sValue);
				break;
			case "$VALUE_NM": {
					string sValueNo;
					this.parseWoman.SetFieldValue(pTag,"VALUE_NO",out sValueNo);
					sValue = this.GetRequestValueNm(sValueNo);
					break;
				}
			case "$COMMENT_TEXT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_TEXT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$UPDATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			case "$DELETE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"DELETE_FLAG",out sValue);
				break;
			case "$REQUEST_PROGRESS_SEQ":
				this.parseWoman.SetFieldValue(pTag,"REQUEST_PROGRESS_SEQ",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseWoman.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$LOGIN_ID":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$COMMENT_PUBLIC_STATUS":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_PUBLIC_STATUS",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
