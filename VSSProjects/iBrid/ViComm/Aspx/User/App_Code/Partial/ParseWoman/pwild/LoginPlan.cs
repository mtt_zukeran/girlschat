﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者ログイン予定検索結果
--	Progaram ID		: LoginPlan
--  Creation Date	: 2014.12.22
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseLoginPlan(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$CAST_LOGIN_PLAN_SEQ":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_PLAN_SEQ",out sValue);
				break;
			case "$CAST_LOGIN_START_TIME":
				this.parseWoman.SetFieldValue(pTag,"LOGIN_START_TIME",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
