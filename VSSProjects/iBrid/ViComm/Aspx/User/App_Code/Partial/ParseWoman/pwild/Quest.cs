﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ソーシャルゲーム クエスト検索結果
--	Progaram ID		: Request
--  Creation Date	: 2012.07.11
--  Creater			: PW Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseQuest(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$QUEST_SEQ":
				this.parseWoman.SetFieldValue(pTag,"QUEST_SEQ",out sValue);
				break;
			case "$LEVEL_QUEST_SEQ":
				this.parseWoman.SetFieldValue(pTag,"LEVEL_QUEST_SEQ",out sValue);
				break;
			case "$QUEST_LEVEL":
				this.parseWoman.SetFieldValue(pTag,"QUEST_LEVEL",out sValue);
				break;
			case "$LEVEL_QUEST_SUB_SEQ":
				this.parseWoman.SetFieldValue(pTag,"LEVEL_QUEST_SUB_SEQ",out sValue);
				break;
			case "$LITTLE_QUEST_SEQ":
				this.parseWoman.SetFieldValue(pTag,"LITTLE_QUEST_SEQ",out sValue);
				break;
			case "$LITTLE_QUEST_NM":
				this.parseWoman.SetFieldValue(pTag,"LITTLE_QUEST_NM",out sValue);
				break;
			case "$OTHER_QUEST_SEQ":
				this.parseWoman.SetFieldValue(pTag,"OTHER_QUEST_SEQ",out sValue);
				break;
			case "$OTHER_QUEST_NM":
				this.parseWoman.SetFieldValue(pTag,"OTHER_QUEST_NM",out sValue);
				break;
			case "$QUEST_TYPE":
				this.parseWoman.SetFieldValue(pTag,"QUEST_TYPE",out sValue);
				break;
			case "$REMARKS":
				this.parseWoman.SetFieldValue(pTag,"REMARKS",out sValue);
				break;
			case "$QUEST_STATUS":
				this.parseWoman.SetFieldValue(pTag,"QUEST_STATUS",out sValue);
				break;
			case "$LAST_ENTRY_OK_FLAG":
				this.parseWoman.SetFieldValue(pTag,"LAST_ENTRY_OK_FLAG",out sValue);
				break;
			case "$TIME_LIMIT_D":
				this.parseWoman.SetFieldValue(pTag,"TIME_LIMIT_D",out sValue);
				break;
			case "$TIME_LIMIT_H":
				this.parseWoman.SetFieldValue(pTag,"TIME_LIMIT_H",out sValue);
				break;
			case "$TIME_LIMIT_M":
				this.parseWoman.SetFieldValue(pTag,"TIME_LIMIT_M",out sValue);
				break;
			case "$ENTRY_LOG_SEQ":
				this.parseWoman.SetFieldValue(pTag,"ENTRY_LOG_SEQ",out sValue);
				break;
			case "$QUEST_EX_STATUS":
				this.parseWoman.SetFieldValue(pTag,"QUEST_EX_STATUS",out sValue);
				break;
			case "$COUNT_UNIT":
				this.parseWoman.SetFieldValue(pTag,"COUNT_UNIT",out sValue);
				break;
			case "$REST_COUNT":
				this.parseWoman.SetFieldValue(pTag,"REST_COUNT",out sValue);
				break;
			case "$NOW_ENTRY_FLAG":
				this.parseWoman.SetFieldValue(pTag,"NOW_ENTRY_FLAG",out sValue);
				break;
			case "$END_DATE":
				this.parseWoman.SetFieldValue(pTag,"END_DATE",out sValue);
				break;
			case "$GET_QUEST_LEVEL_BY_LEVEL_QUEST_SEQ":
				sValue = this.GetQuestLevelByLevelQuestSeq(pTag,pArgument);
				break;
			case "$GET_NOW_QUEST_LEVEL":
				sValue = this.GetNowQuestLevel(pTag,pArgument);
				break;
			case "$QUEST_EX_REMARKS":
				this.parseWoman.SetFieldValue(pTag,"QUEST_EX_REMARKS",out sValue);
				break;

			#region □■□■□ PARTS定義 □■□■□
			case "$GAME_LITTLE_QUEST_LIST_PARTS":
				sValue = this.GetLittleQuestList(pTag,pArgument);
				break;
			case "$GAME_QUEST_TRIAL_STATUS_PARTS":
				sValue = this.GetQuestTrialStatus(pTag,pArgument);
				break;
			#endregion

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetLittleQuestList(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		LittleQuestSeekCondition oCondition = new LittleQuestSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oTmpDataSet = null;

		using (LittleQuest oLittleQuest = new LittleQuest()) {
			oTmpDataSet = oLittleQuest.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_QUEST);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetQuestRewardList(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		QuestRewardSeekCondition oCondition = new QuestRewardSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oTmpDataSet = null;

		using (QuestReward oQuestReward = new QuestReward()) {
			oTmpDataSet = oQuestReward.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetQuestInformation(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		DataSet oTmpDataSet = null;
		string sQuestType = iBridUtil.GetStringValue(oPartsArgs.Query["quest_type"]);

		if (sQuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
			LittleQuestSeekCondition oCondition = new LittleQuestSeekCondition(oPartsArgs.Query);
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.SexCd = ViCommConst.OPERATOR;

			using (LittleQuest oLittleQuest = new LittleQuest()) {
				oTmpDataSet = oLittleQuest.GetOneForInformation(oCondition);
			}
		} else if (sQuestType.Equals(PwViCommConst.GameQuestType.LEVEL_QUEST) || sQuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
			LevelQuestSeekCondition oCondition = new LevelQuestSeekCondition(oPartsArgs.Query);
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.SexCd = ViCommConst.OPERATOR;

			using (LevelQuest oLevelQuest = new LevelQuest()) {
				oTmpDataSet = oLevelQuest.GetOneForInformation(oCondition);
			}
		} else if (sQuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
			OtherQuestSeekCondition oCondition = new OtherQuestSeekCondition(oPartsArgs.Query);
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.SexCd = ViCommConst.OPERATOR;

			using (OtherQuest oOtherQuest = new OtherQuest()) {
				oTmpDataSet = oOtherQuest.GetOneForInformation(oCondition);
			}
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_QUEST);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetQuestClearInfo(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		QuestEntryLogSeekCondition oCondition = new QuestEntryLogSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oTmpDataSet = null;

		using (QuestEntryLog oQuestEntryLog = new QuestEntryLog()) {
			oTmpDataSet = oQuestEntryLog.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_QUEST);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetEntryQuestInfo(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		QuestEntryLogSeekCondition oCondition = new QuestEntryLogSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		
		DataSet oTmpDataSet = null;

		using (QuestEntryLog oQuestEntryLog = new QuestEntryLog()) {
			oTmpDataSet = oQuestEntryLog.GetPageCollectionEntryQuest(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_QUEST_ENTRY_LOG);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetQuestTrialStatus(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		QuestTrialSeekCondition oCondition = new QuestTrialSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		DataSet oTmpDataSet = null;

		using (QuestTrial oQuestTrial = new QuestTrial()) {
			oTmpDataSet = oQuestTrial.GetPageCollectionQuestTrialStatus(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_QUEST);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetQuestLevelByLevelQuestSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] strArr = pArgument.Split(',');

		if (strArr.Length < 2) {
			return sValue;
		}

		string sQuestSeq = strArr[0];
		string sLevelQuestSeq = strArr[1];

		LevelQuestSeekCondition oCondition = new LevelQuestSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.QuestSeq = sQuestSeq;
		oCondition.LevelQuestSeq = sLevelQuestSeq;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oTmpDataSet = null;
		using (LevelQuest oLevelQuest = new LevelQuest()) {
			oTmpDataSet = oLevelQuest.GetOneForInformation(oCondition);
		}

		if(oTmpDataSet.Tables[0].Rows.Count > 0) {
			sValue = oTmpDataSet.Tables[0].Rows[0]["QUEST_LEVEL"].ToString();
		}

		return sValue;
	}

	private string GetNowQuestLevel(string pTag,string pArgument) {
		string sValue = string.Empty;

		if (string.IsNullOrEmpty(pArgument)) {
			return sValue;
		}

		LevelQuestSeekCondition oCondition = new LevelQuestSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.QuestSeq = pArgument;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oTmpDataSet = null;
		using (LevelQuest oLevelQuest = new LevelQuest()) {
			oTmpDataSet = oLevelQuest.GetOne(oCondition);
		}

		if (oTmpDataSet.Tables[0].Rows.Count > 0) {
			sValue = oTmpDataSet.Tables[0].Rows[0]["QUEST_LEVEL"].ToString();
		}

		return sValue;
	}

	private string GetNowPublishQuestSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;

		QuestSeekCondition oCondition = new QuestSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;

		using (Quest oQuest = new Quest()) {
			oTmpDataSet = oQuest.GetPageCollection(oCondition,1,1);
		}

		if (oTmpDataSet.Tables[0].Rows.Count > 0) {
			sValue = oTmpDataSet.Tables[0].Rows[0]["QUEST_SEQ"].ToString();
		}

		return sValue;
	}

	private bool CheckQuestPublish(string pTag,string pArgument) {
		DataSet oTmpDataSet = null;

		if (string.IsNullOrEmpty(pArgument)) {
			return false;
		}

		QuestSeekCondition oCondition = new QuestSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.QuestSeq = pArgument;
		oCondition.SexCd = ViCommConst.OPERATOR;

		using (Quest oQuest = new Quest()) {
			oTmpDataSet = oQuest.GetPageCollection(oCondition,1,1);
		}

		if (oTmpDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}