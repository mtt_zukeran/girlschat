﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseCastPay(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$MONTHLY_CORNER_PAY_AMT":
				sValue = GetPay(pTag,pArgument,false,false);
				break;
			case "$MONTHLY_CORNER_PAY_POINT":
				sValue = GetPay(pTag,pArgument,false,true);
				break;
			case "$MONTHLY_CORNER_PAY_UNIT":
				sValue = GetPay(pTag,pArgument,true,false);
				break;
			case "$MONTHLY_CORNER_BONUS_AMT":
				sValue = GetBonus(pTag,pArgument,false);
				break;
			case "$MONTHLY_CORNER_BONUS_POINT":
				sValue = GetBonus(pTag,pArgument,true);
				break;
			case "$PERFORMANCE_FIND_MONTH":
				sValue = sessionWoman.userWoman.performanceFindMonth;
				break;
			case "$PERFORMANCE_FIND_FROM":
				sValue = sessionWoman.userWoman.performanceFindFrom;
				break;
			case "$PERFORMANCE_FIND_TO":
				sValue = sessionWoman.userWoman.performanceFindTo;
				break;
			case "$REPORT_DAY":
				SetFieldValue(pTag,"REPORT_DAY",out sValue);
				break;

			#region ▼△▼ 支払履歴用変数 ▼△▼
			case "$PAYMENT_DATE":
				SetFieldValue(pTag,"PAYMENT_DATE",out sValue);
				break;
			case "$PAYMENT_CLOSE_DATE":
				SetFieldValue(pTag,"PAYMENT_CLOSE_DATE",out sValue);
				break;
			case "$PAYMENT_AMT":
				sValue = GetPayment(pTag,false);
				break;
			case "$PAYMENT_POINT":
				sValue = GetPayment(pTag,true);
				break;
			#endregion

			#region ▼△▼ 昨日の報酬ランキング ▼△▼
			case "$USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$ALL_PAY_AMT":
				SetFieldValue(pTag,"ALL_PAY_AMT",out sValue);
				break;
			case "$ALL_PAY_AMT_RANK":
				SetFieldValue(pTag,"ALL_PAY_AMT_RANK",out sValue);
				break;
			#endregion

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetPay(string pTag,string pChargeNo,bool pUnitFlag,bool pPointFlag) {
		string sPerformance = "0";
		switch (pChargeNo) {
			case ViCommConst.CHARGE_TALK_PUBLIC:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PUB_TV_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PUB_TV_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PUB_TV_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_TALK_WSHOT:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PRV_TV_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PRV_TV_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PRV_TV_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_TALK_VOICE_WSHOT:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PRV_VOICE_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PRV_VOICE_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PRV_VOICE_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_TALK_VOICE_PUBLIC:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PUB_VOICE_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PUB_VOICE_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PUB_VOICE_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_VIEW_LIVE:
				sPerformance = "0";
				break;
			case ViCommConst.CHARGE_VIEW_TALK:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"VIEW_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"VIEW_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"VIEW_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_VIEW_ONLINE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"VIEW_BROADCAST_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"VIEW_BROADCAST_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"VIEW_BROADCAST_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_PLAY_MOVIE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"MOVIE_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"MOVIE_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"MOVIE_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_RX_MAIL:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"USER_MAIL_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"USER_MAIL_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"USER_MAIL_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_MAIL_WITH_PIC:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"OPEN_PIC_MAIL_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"OPEN_PIC_MAIL_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"OPEN_PIC_MAIL_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_MAIL_WITH_MOVIE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"OPEN_MOVIE_MAIL_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"OPEN_MOVIE_MAIL_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"OPEN_MOVIE_MAIL_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_TX_MAIL:
				sPerformance = "0";
				break;
			case ViCommConst.CHARGE_TX_MAIL_WITH_PIC:
				sPerformance = "0";
				break;
			case ViCommConst.CHARGE_TX_MAIL_WITH_MOVIE:
				sPerformance = "0";
				break;
			case ViCommConst.CHARGE_BBS_MOVIE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"OPEN_MOVIE_BBS_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"OPEN_MOVIE_BBS_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"OPEN_MOVIE_BBS_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_BBS_PIC:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"OPEN_PIC_BBS_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"OPEN_PIC_BBS_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"OPEN_PIC_BBS_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_DOWNLOAD_PF_MOVIE:
				sPerformance = "0";
				break;
			case ViCommConst.CHARGE_RX_MAIL_WITH_PIC:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"USER_MAIL_PIC_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"USER_MAIL_PIC_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"USER_MAIL_PIC_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_RX_MAIL_WITH_MOVIE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"USER_MAIL_MOVIE_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"USER_MAIL_MOVIE_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"USER_MAIL_MOVIE_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_FRIEND_INTRO:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"FRIEND_INTRO_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"FRIEND_INTRO_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"FRIEND_INTRO_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_PLAY_ROOM_SHOW:
				sPerformance = "0";
				break;
			case ViCommConst.CHARGE_REC_PF_MOVIE:
				sPerformance = "0";
				break;
			case ViCommConst.CHARGE_GPF_TALK_VOICE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"GPF_TALK_VOICE_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"GPF_TALK_VOICE_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"GPF_TALK_VOICE_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_PLAY_PF_VOICE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PLAY_PROFILE_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PLAY_PROFILE_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PLAY_PROFILE_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_REC_PF_VOICE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"REC_PROFILE_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"REC_PROFILE_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"REC_PROFILE_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_PRV_MESSAGE_RX:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PRV_MESSAGE_RX_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PRV_MESSAGE_RX_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PRV_MESSAGE_RX_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_PRV_PROFILE_RX:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PRV_PROFILE_RX_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PRV_PROFILE_RX_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PRV_PROFILE_RX_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_WIRETAPPING:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"WIRETAP_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"WIRETAP_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"WIRETAP_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_PLAY_PV_MSG:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PLAY_PV_MSG_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PLAY_PV_MSG_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PLAY_PV_MSG_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_DOWNLOAD_MOVIE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"DOWNLOAD_MOVIE_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"DOWNLOAD_MOVIE_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"DOWNLOAD_MOVIE_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_TIME_CALL:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"TIME_CALL_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"TIME_CALL_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"TIME_CALL_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_DOWNLOAD_VOICE:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"DOWNLOAD_VOICE_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"DOWNLOAD_VOICE_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"DOWNLOAD_VOICE_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_SOCIAL_GAME:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"SOCIAL_GAME_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"SOCIAL_GAME_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"SOCIAL_GAME_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_YAKYUKEN:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"YAKYUKEN_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"YAKYUKEN_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"YAKYUKEN_COUNT",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_CAST_TALK_PUBLIC:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"CAST_PUB_TV_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"CAST_PUB_TV_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"CAST_PUB_TV_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_CAST_TALK_WSHOT:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"CAST_PRV_TV_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"CAST_PRV_TV_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"CAST_PRV_TV_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"CAST_PRV_VOICE_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"CAST_PRV_VOICE_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"CAST_PRV_VOICE_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"CAST_PUB_VOICE_TALK_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"CAST_PUB_VOICE_TALK_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"CAST_PUB_VOICE_TALK_MIN",out sPerformance);
				}
				break;
			case ViCommConst.CHARGE_PRESENT_MAIL:
				if (!pUnitFlag) {
					if (pPointFlag) {
						SetFieldValue(pTag,"PRESENT_MAIL_PAY_POINT",out sPerformance);
					} else {
						SetFieldValue(pTag,"PRESENT_MAIL_PAY_AMT",out sPerformance);
					}
				} else {
					SetFieldValue(pTag,"PRESENT_MAIL_COUNT",out sPerformance);
				}
				break;
		}
		return sPerformance;
	}

	private string GetBonus(string pTag,string pBonusPointType,bool pPointFlag) {
		int iBonus = 0;
		string[] sValues;
		if (GetValues(pTag,"BONUS_AMT_ADMIN,BONUS_AMT_FAVORIT,BONUS_AMT_REFUSE,BONUS_AMT_BLOG,BONUS_AMT_OMIKUJI,BONUS_POINT_ADMIN,BONUS_POINT_FAVORIT,BONUS_POINT_REFUSE,BONUS_POINT_BLOG,BONUS_POINT_OMIKUJI,BONUS_AMT_FANCLUB,BONUS_POINT_FANCLUB",out sValues)) {
			if (!pPointFlag) {
				if (pBonusPointType.Equals(string.Empty)) {
					iBonus = int.Parse(sValues[0]) + int.Parse(sValues[1]) + int.Parse(sValues[2]) + int.Parse(sValues[3]) + int.Parse(sValues[4]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_ADMIN)) {
					iBonus = int.Parse(sValues[0]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_FAVORIT)) {
					iBonus = int.Parse(sValues[1]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_REFUSE)) {
					iBonus = int.Parse(sValues[2]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_BLOG)) {
					iBonus = int.Parse(sValues[3]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_OMIKUJI)) {
					iBonus = int.Parse(sValues[4]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_FANCLUB)) {
					iBonus = int.Parse(sValues[10]);
				}
			} else {
				if (pBonusPointType.Equals(string.Empty)) {
					iBonus = int.Parse(sValues[5]) + int.Parse(sValues[6]) + int.Parse(sValues[7]) + int.Parse(sValues[8]) + int.Parse(sValues[9]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_ADMIN)) {
					iBonus = int.Parse(sValues[5]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_FAVORIT)) {
					iBonus = int.Parse(sValues[6]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_REFUSE)) {
					iBonus = int.Parse(sValues[7]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_BLOG)) {
					iBonus = int.Parse(sValues[8]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_OMIKUJI)) {
					iBonus = int.Parse(sValues[9]);
				} else if (pBonusPointType.Equals(ViCommConst.BONUS_TYPE_FANCLUB)) {
					iBonus = int.Parse(sValues[11]);
				}
			}
		}

		return iBonus.ToString();
	}

	private string GetPayment(string pTag,bool pPointFlag) {
		string sPayment = "";
		if (pPointFlag) {
			SetFieldValue(pTag,"PAYMENT_POINT",out sPayment);
		} else {
			SetFieldValue(pTag,"PAYMENT_AMT",out sPayment);
		}
		return sPayment;
	}

	private string GetListCastTodayPay(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);
		DataSet oTmpDataSet = null;

		string sSiteCd = string.Empty;
		string sUserCharNo = string.Empty;

		//Z001で開いた時は、全サイト、全サブキャラクターの合計報酬を表示する
		if (sessionWoman.site.siteCd.Equals("Z001")) {
			sSiteCd = string.Empty;
			sUserCharNo = string.Empty;
		} else {
			sSiteCd = sessionWoman.site.siteCd;
			sUserCharNo = sessionWoman.userWoman.curCharNo;
		}

		using (TimeOperation oTimeOperation = new TimeOperation(sessionObjs)) {
			oTmpDataSet = oTimeOperation.GetPerformance(sSiteCd,
														sessionWoman.userWoman.userSeq,
														sUserCharNo,
														DateTime.Now.Year.ToString("00"),
														DateTime.Now.Month.ToString("00"),
														DateTime.Now.Day.ToString("00"),
														DateTime.Now.Year.ToString("00"),
														DateTime.Now.Month.ToString("00"),
														DateTime.Now.Day.ToString("00"),
														false);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_CAST_PAY);
	}
}
