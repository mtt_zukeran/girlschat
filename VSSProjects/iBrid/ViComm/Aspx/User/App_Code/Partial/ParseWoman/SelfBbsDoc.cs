﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;

partial class ParseWoman {
	private string ParseSelfBbsDoc(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$QUERY_VIEW_SELF_BBS_DOC":
				sValue = GetQueryViewSelfBbsDoc(pTag);
				break;
			case "$SELF_BBS_READ_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$SELF_BBS_TITLE":
				SetFieldValue(pTag,"BBS_TITLE",out sValue);
				break;
			case "$SELF_BBS_WRITE_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$SELF_BBS_WRITE_DOC":
				sValue = GetDoc(pTag,"BBS_DOC");
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryViewSelfBbsDoc(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,BBS_SEQ",out sValues)) {
			return string.Format("pageno={0}&loginid={1}&bbsseq={2}",
									sValues[0],
									iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["loginid"]),
									sValues[1]
									);
		} else {
			return "";
		}
	}
}
