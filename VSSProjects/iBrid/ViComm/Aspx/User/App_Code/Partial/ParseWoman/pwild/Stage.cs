﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseStage(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$GAME_STAGE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"STAGE_SEQ",out sValue);
				break;
			case "$GAME_STAGE_NM":
				this.parseWoman.SetFieldValue(pTag,"STAGE_NM",out sValue);
				break;
			case "$GAME_STAGE_HONOR":
				this.parseWoman.SetFieldValue(pTag,"HONOR",out sValue);
				break;
			case "$GAME_STAGE_GROUP_TYPE":
				this.parseWoman.SetFieldValue(pTag,"STAGE_GROUP_TYPE",out sValue);
				break;
			case "$GAME_STAGE_GROUP_NM":
				this.parseWoman.SetFieldValue(pTag,"STAGE_GROUP_NM",out sValue);
				break;
			case "$GAME_STAGE_CLEAR_FLAG":
				this.parseWoman.SetFieldValue(pTag,"STAGE_CLEAR_FLAG",out sValue);
				break;
			case "$GAME_STAGE_MAN_LIMIT_STAGE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"MAN_LIMIT_STAGE_SEQ",out sValue);
				break;
			case "$GAME_STAGE_CAST_LIMIT_STAGE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_LIMIT_STAGE_SEQ",out sValue);
				break;
			case "$GAME_TOWN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOWN_COUNT",out sValue);
				break;
			case "$GAME_TOWN_CLEAR_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TOWN_CLEAR_COUNT",out sValue);
				break;
			case "$GAME_STAGE_BOSS_NM":
				this.parseWoman.SetFieldValue(pTag,"BOSS_NM",out sValue);
				break;
			case "$GAME_STAGE_BOSS_DESCRIPTION":
				this.parseWoman.SetFieldValue(pTag,"BOSS_DESCRIPTION",out sValue);
				break;
			case "$GAME_STAGE_EXP":
				this.parseWoman.SetFieldValue(pTag,"EXP",out sValue);
				break;
			case "$GAME_NEXT_STAGE_NM":
				this.parseWoman.SetFieldValue(pTag,"NEXT_STAGE_NM",out sValue);
				break;
			case "$GAME_NEXT_STAGE_GROUP_TYPE":
				this.parseWoman.SetFieldValue(pTag,"NEXT_STAGE_GROUP_TYPE",out sValue);
				break;
			case "$GAME_NEXT_STAGE_GROUP_NM":
				this.parseWoman.SetFieldValue(pTag,"NEXT_STAGE_GROUP_NM",out sValue);
				break;
			case "$GAME_GET_ITEM_SEQ_CSV":
				this.parseWoman.SetFieldValue(pTag,"GET_ITEM_SEQ_CSV",out sValue);
				break;
			case "$GAME_GET_ITEM_NM_CSV":
				this.parseWoman.SetFieldValue(pTag,"GET_ITEM_NM_CSV",out sValue);
				break;
			case "$GAME_GET_ITEM_COUNT_CSV":
				this.parseWoman.SetFieldValue(pTag,"GET_ITEM_COUNT_CSV",out sValue);
				break;
			case "$GAME_GET_ITEM_NM_STR":
				sValue = this.CreateGetItemNm(pTag,pArgument);
				break;
			case "$GAME_PREV_STAGE_GROUP_TYPE_FOR_LINK":
				sValue = this.GetPrevStageGroupType(pTag,pArgument);
				break;
			case "$GAME_NEXT_STAGE_GROUP_TYPE_FOR_LINK":
				sValue = this.GetNextStageGroupType(pTag,pArgument);
				break;
			case "$CAST_CLEAR_FROM_REGIST_DAYS":
				this.parseWoman.SetFieldValue(pTag,"CAST_CLEAR_FROM_REGIST_DAYS",out sValue);
				break;
			case "$CAST_STAGE_CLEAR_POINT":
				this.parseWoman.SetFieldValue(pTag,"CAST_STAGE_CLEAR_POINT",out sValue);

				if (!string.IsNullOrEmpty(sValue)) {
					int iValue = int.Parse(sValue);
					sValue = String.Format("{0:#,0}",iValue);
				}
				break;
			case "$GAME_TOWN_CLEAR_PERCENT": {
				string[] sValues;
				if (this.parseWoman.GetValues(pTag,"TOWN_COUNT,TOWN_CLEAR_COUNT",out sValues)) {

					double dTownCount = 0;
					double dTownClearCount = 0;

					double.TryParse(sValues[0],out dTownCount);
					double.TryParse(sValues[1],out dTownClearCount);

					if (dTownCount == 0) {
						return string.Empty;
					}

					sValue = iBridUtil.RoundOff(dTownClearCount / dTownCount * 100,0).ToString();
				}
				break;
			}

			#region □■□■□ PARTS定義 □■□■□
			case "$GAME_STAGE_CLEAR_GET_ITEM_PARTS":
				sValue = this.GetGameStageClearGetItem(pTag,pArgument);
				break;
			#endregion
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string CreateGetItemNm(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sGameGetItemNmCsv = string.Empty;
		string sNmSeparateString = string.Empty;

		this.parseWoman.SetFieldValue(pTag,"GET_ITEM_NM_CSV",out sGameGetItemNmCsv);
		string[] GameGetItemNmArr = sGameGetItemNmCsv.Split(',');

		if (string.IsNullOrEmpty(pArgument)) {
			sNmSeparateString = "と";
		} else {
			sNmSeparateString = pArgument;
		}

		sValue = string.Join(sNmSeparateString,GameGetItemNmArr);

		return sValue;
	}

	private string GetListTown(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		TownSeekCondition oCondition = new TownSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		DataSet oDataSet;
		using (Town oTown = new Town()) {
			oDataSet = oTown.GetPageCollectionItem(oCondition,oPartsArgs.NeedCount);
		}
		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_TOWN);
	}

	private string GetPrevStageGroupType(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iGameStageGroupType;
		int iGamePrevStageGroupType;
		int.TryParse(pArgument,out iGameStageGroupType);

		if (iGameStageGroupType > 1) {
			iGamePrevStageGroupType = iGameStageGroupType - 1;
		} else {
			iGamePrevStageGroupType = iGameStageGroupType;
		}

		return iGamePrevStageGroupType.ToString();
	}

	private string GetNextStageGroupType(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iGameStageGroupType;
		int iGameNextStageGroupType;
		int.TryParse(pArgument,out iGameStageGroupType);

		iGameNextStageGroupType = iGameStageGroupType + 1;

		return iGameNextStageGroupType.ToString();
	}

	private string GetOneBossData(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		StageSeekCondition oCondition = new StageSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.StageClearFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.NextStageMovedFlag = ViCommConst.FLAG_OFF_STR;

		DataSet oDataSet;

		using (Stage oGameStage = new Stage()) {
			oDataSet = oGameStage.GetOneBossData(oCondition);
		}
		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_TOWN);
	}

	private string GetStageClearPoint(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		DataSet oDataSet = null;
		using (StageClearPoint oStageClearPoint = new StageClearPoint()) {
			oDataSet = oStageClearPoint.GetStageClearPoint(this.parseWoman.sessionWoman.site.siteCd,ViCommConst.OPERATOR);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_STAGE);
	}

	private string GetGameStageClearGetItem(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		StageSeekCondition oCondition = new StageSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oDataSet = null;

		using (Stage oStage = new Stage()) {
			oDataSet = oStage.GetStageClearItem(oCondition);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_ITEM);
	}
}