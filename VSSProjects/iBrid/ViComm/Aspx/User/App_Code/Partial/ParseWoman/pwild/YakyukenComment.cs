﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳コメント検索結果
--	Progaram ID		: YakyukenComment
--  Creation Date	: 2013.04.29
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseYakyukenComment(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$YAKYUKEN_COMMENT_SEQ":
				this.parseWoman.SetFieldValue(pTag,"YAKYUKEN_COMMENT_SEQ",out sValue);
				break;
			case "$COMMENT_TEXT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_TEXT",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}