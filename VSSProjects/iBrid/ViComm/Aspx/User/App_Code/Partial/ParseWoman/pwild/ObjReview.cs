﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝レビュー検索結果
--	Progaram ID		: ObjReview
--  Creation Date	: 2012.04.30
--  Creater			: PW K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseObjReview(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$OBJ_REVIEW_HISTORY_SEQ":
				this.parseWoman.SetFieldValue(pTag,"OBJ_REVIEW_HISTORY_SEQ",out sValue);
				break;
			case "$OBJ_SEQ":
				this.parseWoman.SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;
			case "$GOOD_STAR":
				this.parseWoman.SetFieldValue(pTag,"GOOD_STAR",out sValue);
				break;
			case "$GOOD_POINT":
				this.parseWoman.SetFieldValue(pTag,"GOOD_POINT",out sValue);
				break;
			case "$COMMENT_DOC":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_DOC",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$COMMENT_DEL_FLAG":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_DEL_FLAG",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			//T_OBJ_REVIEW_TOTAL
			case "$REVIEW_COUNT":
				this.parseWoman.SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = this.parseWoman.GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = this.parseWoman.GetGoodStarAvrX10(pTag);
				break;
			case "$GOOD_1_COUNT":
				this.parseWoman.SetFieldValue(pTag,"GOOD_1_COUNT",out sValue);
				break;
			case "$GOOD_2_COUNT":
				this.parseWoman.SetFieldValue(pTag,"GOOD_2_COUNT",out sValue);
				break;
			case "$GOOD_3_COUNT":
				this.parseWoman.SetFieldValue(pTag,"GOOD_3_COUNT",out sValue);
				break;
			case "$GOOD_4_COUNT":
				this.parseWoman.SetFieldValue(pTag,"GOOD_4_COUNT",out sValue);
				break;
			case "$GOOD_5_COUNT":
				this.parseWoman.SetFieldValue(pTag,"GOOD_5_COUNT",out sValue);
				break;
			case "$GOOD_1_PERCENT":
				sValue = this.GetGoodPercent(pTag,"GOOD_1_COUNT");
				break;
			case "$GOOD_2_PERCENT":
				sValue = this.GetGoodPercent(pTag,"GOOD_2_COUNT");
				break;
			case "$GOOD_3_PERCENT":
				sValue = this.GetGoodPercent(pTag,"GOOD_3_COUNT");
				break;
			case "$GOOD_4_PERCENT":
				sValue = this.GetGoodPercent(pTag,"GOOD_4_COUNT");
				break;
			case "$GOOD_5_PERCENT":
				sValue = this.GetGoodPercent(pTag,"GOOD_5_COUNT");
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetGoodPercent(string pTag,string pTagNm) {
		string sGoodCount,sReviewCount;
		float fGoodCount,fReviewCount;
		double dGoodPercent = 0;

		this.parseWoman.SetFieldValue(pTag,pTagNm,out sGoodCount);
		this.parseWoman.SetFieldValue(pTag,"REVIEW_COUNT",out sReviewCount);

		if (float.TryParse(sGoodCount,out fGoodCount) && float.TryParse(sReviewCount,out fReviewCount)) {
			if (fGoodCount > 0 && fReviewCount > 0) {
				dGoodPercent = fGoodCount / fReviewCount * 100;
				dGoodPercent = iBridUtil.RoundOff(dGoodPercent,0);
			}
		}

		return dGoodPercent.ToString();
	}
}
