﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやきｺﾒﾝﾄﾗﾝｷﾝｸﾞ 検索結果
--	Progaram ID		: ManTweetCommentCnt
--  Creation Date	: 2013.03.27
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseManTweetCommentCnt(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$RANK_NO":
				this.parseWoman.SetFieldValue(pTag,"RANK_NO",out sValue);
				break;
			case "$COMMENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$TERM_START_DATE":
				this.parseWoman.SetFieldValue(pTag,"TERM_START_DATE",out sValue);
				break;
			case "$TERM_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"TERM_END_DATE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}