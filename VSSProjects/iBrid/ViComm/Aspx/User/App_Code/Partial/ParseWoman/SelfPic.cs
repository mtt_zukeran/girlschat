﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseSelfPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		string sObjSeq = string.Empty;

		switch (pTag) {
			case "$HREF_SELF_MAIL_SEND_IMG":
				sValue = GetHRefSelfMailSendImg(pTag,pArgument);
				break;
			case "$HREF_SELF_VIEW_STOCK_PIC":
				sValue = GetHRefSelfViewStockPic(pTag,pArgument);
				break;
			case "$UNAUTH_SELF_VIEW_STOCK_PIC":
				sValue = GetUnauthSelfViewStockPic(pTag,pArgument);
				break;
			case "$HREF_SELF_IMG_PATH":
				sValue = GetHRefSelfImgPath(pTag,pArgument);
				break;
			case "$QUERY_VIEW_SELF_BBS_PIC":
				sValue = GetQueryViewSelfBbsPic(pTag);
				break;
			case "$QUERY_VIEW_SELF_GALLERY_PIC":
				sValue = GetQueryViewSelfGalleryPic(pTag);
				break;
			case "$SELF_PIC_ATTR_NM":
				sValue = GetSelfPicAttrNm(pTag);
				break;
			case "$SELF_PIC_ATTR_TYPE_NM":
				sValue = GetSelfPicAttrTypeNm(pTag);
				break;
			case "$SELF_PIC_AUTH_MARK":
				SetFieldValue(pTag,"UNAUTH_MARK",out sValue);
				break;
			case "$SELF_PIC_DOC":
				sValue = GetDoc(pTag,"PIC_DOC");
				break;
			case "$SELF_PIC_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag,"OBJ_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_SMALL_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_PIC_TITLE":
				SetFieldValue(pTag,"PIC_TITLE",out sValue);
				break;
			case "$SELF_PIC_UNAUTH_FLAG":
				SetFieldValue(pTag,"OBJ_NOT_APPROVE_FLAG",out sValue);
				break;
			case "$SELF_PIC_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$SELF_PIC_VIEW_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$SELF_PIC_PROFILE_FLAG":
				SetFieldValue(pTag,"CUR_PIC_IS_PROFILE",out sValue);
				break;
			case "$SELF_PIC_CLOSED_FLAG":
				sValue = IsHidePic(pTag);
				break;
			case "$SELF_PIC_SEQ":
				SetFieldValue(pTag,"PIC_SEQ",out sValue);
				break;
			case "$SELF_PIC_PLANNING_TYPE":
				SetFieldValue(pTag,"PLANNING_TYPE",out sValue);
				break;
			case "$SELF_PIC_DELETABLE_FLAG":
				string sAttrSeq = string.Empty;
				if (GetValue(pTag,"CAST_PIC_ATTR_SEQ",out sAttrSeq)) {
					using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
						sValue = oCastPicAttrTypeValue.IsDeletable(sessionWoman.site.siteCd,sAttrSeq) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
					}
				}
				break;

			case "$OBJ_SEQ":
				SetFieldValue(pTag,"PIC_SEQ",out sValue);
				break;

			//レビュー
			case "$REVIEW_COUNT":
				SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = GetGoodStarAvrX10(pTag);
				break;
			case "$OBJ_RANKING_FLAG":
				SetFieldValue(pTag,"OBJ_RANKING_FLAG",out sValue);
				break;
			//イイネ
			case "$SELF_PIC_LIKE_COUNT":
				SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			//コメント
			case "$SELF_PIC_COMMENT_COUNT":
				SetFieldValue(pTag,"CAST_PIC_COMMENT_COUNT",out sValue);
				break;
			// ピックアップ（特集）
			case "$DIV_IS_PICKUP_OBJ":
				if (GetValue(pTag,"PIC_SEQ",out sObjSeq)) {
					// ピックアップ（特集）の対象である場合は表示、それ以外は非表示
					PickupObjs oPickupObjs = new PickupObjs();
					DataSet ds = oPickupObjs.GetPickupInfoByObjSeq(sessionWoman.site.siteCd,sObjSeq,true);
					bool bFlag = (ds == null || ds.Tables[0].Rows.Count <= 0);
					SetNoneDisplay(bFlag);
				} else {
					// 非表示
					SetNoneDisplay(true);
				}
				break;
			case "$PICKUP_NM":
				if (GetValue(pTag,"PIC_SEQ",out sObjSeq)) {
					// ピックアップ（特集）名
					PickupObjs oPickupObjs = new PickupObjs();
					DataSet ds = oPickupObjs.GetPickupInfoByObjSeq(sessionWoman.site.siteCd,sObjSeq,true);
					sValue = oPickupObjs.ConcatPickupTitle(ds,"<br/>");
				}
				break;

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string IsHidePic(string pTag) {
		string sValue = string.Empty;

		if (GetValue(pTag,"PIC_TYPE",out sValue)) {
			if (sValue.Equals(ViCommConst.ATTACHED_HIDE.ToString())) {
				return ViCommConst.FLAG_ON_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}
	private string GetQueryViewSelfBbsPic(string pTag) {
		string sValue = string.Empty;

		if (GetValue(pTag,"RNUM",out sValue)) {
			return string.Format("pageno={0}&attrtypeseq={1}&attrseq={2}",
										sValue,
										iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrtypeseq"]),
										iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrseq"]));
		} else {
			return string.Empty;
		}
	}

	private string GetQueryViewSelfGalleryPic(string pTag) {
		string[] sValue;
		if (GetValues(pTag,"RNUM,PIC_SEQ,CUR_PIC_IS_PROFILE,OBJ_NOT_PUBLISH_FLAG",out sValue)) {
			return string.Format("pageno={0}&picseq={1}&profilepicflag={2}&closed={3}&site={4}",
								sValue[0],
								sValue[1],
								sValue[2],
								sValue[3],
								sessionWoman.site.siteCd
								);
		} else {
			return "";
		}
	}

	private string GetHRefSelfImgPath(string pTag,string pArgument) {
		string[] sArgument = pArgument.Split(',');
		int iIdx = int.Parse(sArgument[0]);
		string sWidth = sArgument[1];
		string sHerf = "";
		string sPhoto = "";
		if (sessionWoman.datasetRecCount >= iIdx) {
			sHerf = sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,
				string.Format("ViewGalleryPicConf.aspx?pageno={0}&picseq={1}&profilepicflag={2}&site={3}",
							dataTable[this.currentTableIdx].Rows[iIdx - 1]["RNUM"].ToString(),
							dataTable[this.currentTableIdx].Rows[iIdx - 1]["PIC_SEQ"].ToString(),
							dataTable[this.currentTableIdx].Rows[iIdx - 1]["CUR_PIC_IS_PROFILE"].ToString(),
							sessionWoman.site.siteCd
				)
			);
			string ret = string.Empty;
			sPhoto = dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
			return string.Format("<a href=\"{0}\"><img src=\"{1}\" width=\"{2}\"; alt=\"\" hspace=\"2\" vspace=\"2\" /></a>",sHerf,sPhoto,sWidth);
		} else {
			sPhoto = string.Format("image/{0}/nopic_s.gif",sessionWoman.site.siteCd);
			return string.Format("<img src=\"{0}\" width=\"{1}\"; alt=\"\"  hspace=\"2\" vspace=\"2\" />",sPhoto,sWidth);
		}
	}

	private string GetHRefSelfMailSendImg(string pTag,string pArgument) {
		string[] sArgument = pArgument.Split(',');
		int iIdx = int.Parse(sArgument[0]);
		string sWidth = sArgument[1];
		string sHerf = "";
		string sPhoto = "";
		if (sessionWoman.datasetRecCount >= iIdx) {
			sHerf = sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,
							string.Format("TxMailConfirm.aspx?picseq={0}&data={1}&pageno={2}&site={3}{4}&mailtype={5}&charno={6}",
								dataTable[this.currentTableIdx].Rows[iIdx - 1]["PIC_SEQ"].ToString(),
								iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"]),
								iIdx,
								sessionWoman.site.siteCd,
								iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["batch"]) == "1" ? "&batch=1" : "",
								ViCommConst.ATTACH_PIC_INDEX.ToString(),
								sessionWoman.userWoman.curCharNo
							)
			);

			sPhoto = dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
			return string.Format("<a href=\"{0}\"><img src=\"{1}\" width=\"{2}\"; alt=\"\" hspace=\"2\" vspace=\"2\" /></a>",sHerf,sPhoto,sWidth);
		} else {
			sPhoto = string.Format("image/{0}/nopic_s.gif",sessionWoman.site.siteCd);
			return string.Format("<img src=\"{0}\" width=\"{1}\"; alt=\"\"  hspace=\"2\" vspace=\"2\" />",sPhoto,sWidth);
		}
	}

	private string GetHRefSelfViewStockPic(string pTag,string pArgument) {
		string[] sArgument = pArgument.Split(',');
		int iIdx = int.Parse(sArgument[0]);
		string sWidth = sArgument[1];
		string sHerf = "";
		string sPhoto = "";
		if (sessionWoman.datasetRecCount >= iIdx) {
			sHerf = sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,
						string.Format("ViewMailPicConf.aspx?pageno={0}&picseq={1}&site={2}",
							iIdx,
							dataTable[this.currentTableIdx].Rows[iIdx - 1]["PIC_SEQ"].ToString(),
							sessionWoman.site.siteCd
						)
			);

			sPhoto = dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
			return string.Format("<a href=\"{0}\"><img src=\"{1}\" width=\"{2}\"; alt=\"\" hspace=\"2\" vspace=\"2\" /></a>",sHerf,sPhoto,sWidth);
		} else {
			sPhoto = string.Format("image/{0}/nopic_s.gif",sessionWoman.site.siteCd);
			return string.Format("<img src=\"{0}\" width=\"{1}\"; alt=\"\"  hspace=\"2\" vspace=\"2\" />",sPhoto,sWidth);
		}
	}

	private string GetUnauthSelfViewStockPic(string pTag,string pArgument) {
		int iIdx = int.Parse(pArgument);
		if (sessionWoman.datasetRecCount >= iIdx) {
			return dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_NOT_APPROVE_FLAG"].ToString();
		} else {
			return string.Empty;
		}
	}

	private string GetSelfPicAttrNm(string pTag) {
		string sPicAttrNm = "";
		string[] sValues;
		if (GetValues(pTag,"CAST_PIC_ATTR_SEQ,CAST_PIC_ATTR_NM",out sValues)) {
			if (sValues[0].Equals(ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString())) {
				sPicAttrNm = "指定なし";
			} else {
				sPicAttrNm = parseContainer.Parse(sValues[1]);
			}
		}
		return sPicAttrNm;
	}

	private string GetSelfPicAttrTypeNm(string pTag) {
		string sPicAttrTypeNm = "";
		string[] sValues;
		if (GetValues(pTag,"CAST_PIC_ATTR_TYPE_SEQ,CAST_PIC_ATTR_TYPE_NM",out sValues)) {
			if (sValues[0].ToString().Equals(ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString())) {
				sPicAttrTypeNm = "指定なし";
			} else {
				sPicAttrTypeNm = parseContainer.Parse(sValues[1]);
			}
		}
		return sPicAttrTypeNm;
	}
}
