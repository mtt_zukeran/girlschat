﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ギフト券検索結果
--	Progaram ID		: GiftCode
--  Creation Date	: 2017.04.11
 * 
--  Creater			: M&TT Zukeran
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class ParseWoman {
	private string ParseGiftCode(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			// ギフト券の種類(NUMBER)
			case "$GIFT_TYPE":
				SetFieldValue(pTag,"GIFT_CODE_TYPE",out sValue);
				break;
			// ギフト券の名称
			case "$GIFT_NM":
				SetFieldValue(pTag,"GIFT_CODE_TYPE_NM",out sValue);
				break;
			// ギフト券の金額
			case "$GIFT_AMOUNT":
				SetFieldValue(pTag,"AMOUNT",out sValue);
				break;
			// ギフト券の画像ディレクトリ
			case "$GIFT_IMAGE_DIR_PATH":
				SetFieldValue(pTag,"IMAGE_DIR_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			// ギフト券の交換手数料
			case "$GIFT_PAYMENT_FEE":
				SetFieldValue(pTag,"PAYMENT_FEE",out sValue);
				break;
			// ギフト券の交換予定日
			case "$GIFT_PAYMENT_DUE_DATE":
				string sDays = string.Empty;
				if (SetFieldValue(pTag,"PAYMENT_DELAY_DAYS",out sDays)) {
					if (sDays.Equals("0")) {
						sValue = "リアルタイム";
					} else {
						DateTime oDateTime = new DateTime();
						oDateTime.AddDays(double.Parse(sDays));
						sValue = oDateTime.ToString("yyyy/MM/dd");
					}
				}
				break;
			// ギフト券の在庫数(未割当枚数)
			case "$GIFT_REMAINING":
				SetFieldValue(pTag,"GIFT_REMAINING",out sValue);
				break;
			// ギフト券の枚数選択用リスト
			case "$GIFT_REMAINING_LIST_PARTS":
				sValue = this.GetRemainingListParts(pTag,pArgument);
				break;
			// ギフト券の交換年
			case "$GIFT_PAYMENT_YEAR":
				sValue = iBridUtil.GetStringValue(sessionWoman.requestQuery["year"]);
				if (string.IsNullOrEmpty(sValue)) {
					sValue = DateTime.Today.Year.ToString();
				}
				break;
			// ギフト券の交換日
			case "$GIFT_PAYMENT_DATE":
				SetFieldValue(pTag,"PAYMENT_DATE_SHORT",out sValue);
				break;
			// ギフト券の有効期限
			case "$GIFT_EXPIRATION_DATE":
				SetFieldValue(pTag,"EXPIRATION_DATE",out sValue);
				break;
			// ギフト券のレコードID
			case "$GIFT_RECORD_ID":
				SetFieldValue(pTag,"GIFT_ROWID",out sValue);
				break;
			// ギフトコード
			case "$GIFT_CODE":
				SetFieldValue(pTag,"GIFT_CODE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	/// <summary>
	/// ギフト券の枚数選択用リスト
	/// </summary>
	/// <param name="pTag"></param>
	/// <param name="pArgument"></param>
	/// <returns></returns>
	private string GetRemainingListParts(string pTag,string pArgument) {
		StringBuilder oTag = new StringBuilder();

		// 交換最大枚数
		int iMaxListItem = 10;
		// ギフト券の枚数
		int iRemaining = 0;
		// リスト表示件数
		int iViewNum = 0;

		// 未割当のギフトコードの残枚数
		string sValue = string.Empty;
		SetFieldValue(pTag,"GIFT_REMAINING",out sValue);
		iRemaining = int.Parse(sValue);

		Cast oCast = new Cast();
		DataSet oDSCast = oCast.GetNotPayment(sessionWoman.userWoman.userSeq);
		if (oDSCast.Tables[0].Rows.Count > 0) {
			// 未清算ポイント、金額
			int iNotPaymentAmt = int.Parse(oDSCast.Tables[0].Rows[0]["NOT_PAYMENT_AMT"].ToString());
			// ギフト券の単価
			string sGiftAmt = string.Empty;
			SetFieldValue(pTag,"AMOUNT",out sGiftAmt);
			int iGiftAmt = int.Parse(sGiftAmt);

			if (iNotPaymentAmt > 0 && iGiftAmt > 0) {
				// 清算報酬額による交換制限 (報酬金額不足)
				decimal dValue = iNotPaymentAmt / iGiftAmt;
				int iValue = int.Parse(Math.Floor(double.Parse(dValue.ToString())).ToString());
				if (iValue < iRemaining) {
					iViewNum = iValue;
				}
			}
		}

		// 購入可能な数より在庫が少ない場合
		if (iViewNum > iRemaining) {
			iViewNum = iRemaining;
		}

		if (iViewNum > 0) {
			// リストの表示最大件数より多い場合
			if (iViewNum > iMaxListItem) {
				iViewNum = iMaxListItem;
			}
			oTag.AppendLine("<select name='gift_payment_num'>");
			for (int i = 1;i <= iViewNum;i++) {
				oTag.AppendLine(string.Format(
					"<option value='{0}'>{0}</option>"
					,i
				));
			}
			oTag.AppendLine("</select>");
		}
		return oTag.ToString();
	}
}
