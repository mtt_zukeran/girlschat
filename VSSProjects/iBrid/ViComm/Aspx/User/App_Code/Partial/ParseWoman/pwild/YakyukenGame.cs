﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳ゲーム検索結果
--	Progaram ID		: YakyukenGame
--  Creation Date	: 2013.04.29
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseYakyukenGame(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$YAKYUKEN_GAME_SEQ":
				this.parseWoman.SetFieldValue(pTag,"YAKYUKEN_GAME_SEQ",out sValue);
				break;
			case "$PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SMALL_PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$COMPLETE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"COMPLETE_FLAG",out sValue);
				break;
			case "$SHOW_FACE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"SHOW_FACE_FLAG",out sValue);
				break;
			case "$AUTH_PIC_COUNT":
				this.parseWoman.SetFieldValue(pTag,"AUTH_PIC_COUNT",out sValue);
				break;
			case "$VIEW_COUNT":
				this.parseWoman.SetFieldValue(pTag,"VIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				this.parseWoman.SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$COMPLETE_DATE":
				this.parseWoman.SetFieldValue(pTag,"COMPLETE_DATE",out sValue);
				break;
			//女性勝利コメント
			case "$SELF_RANDOM_YAKYUKEN_COMMENT":
				sValue = GetSelfRandomYakyukenComment(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetSelfRandomYakyukenComment(string pTag,string pArgument) {
		string sCommentText = string.Empty;

		YakyukenCommentSeekCondition oCondition = new YakyukenCommentSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.RandomFlag = ViCommConst.FLAG_ON_STR;

		using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
			DataSet ds = oYakyukenComment.GetPageCollection(oCondition,1,1);

			if (ds.Tables[0].Rows.Count > 0) {
				sCommentText = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["COMMENT_TEXT"]);
			}
		}

		return sCommentText;
	}
}

