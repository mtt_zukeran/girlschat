﻿using System;
using System.Text;
using System.Data;
using iBridCommLib;
using ViComm;


partial class ParseWoman {
	private string ParseGame(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$HREF_GAME": {
					sValue = GetHrefGame(pTag);
				}
				break;

			case "$GAME_TYPE": {
					SetFieldValue(pTag,"GAME_TYPE",out sValue);
				}
				break;
			case "$GAME_HIGH_SCORE": {
					SetFieldValue(pTag,"GAME_HIGH_SCORE",out sValue);
				}
				break;
			case "$GAME_HIGH_RANK": {
					SetFieldValue(pTag,"GAME_HIGH_RANK",out sValue);
				}
				break;
			case "$GAME_LATEST_RANK": {
					SetFieldValue(pTag,"GAME_LATEST_RANK",out sValue);
				}
				break;
			case "$GAME_LATEST_SCORE": {
					SetFieldValue(pTag,"GAME_LATEST_SCORE",out sValue);
				}
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetHrefGame(string pTag) {
		string sHrefGame = string.Empty;

		string[] oValues = null;
		GetValues(pTag,"GAME_TYPE,SEX_CD",out oValues);

		string sGameType = oValues[0];
		string sSexCd = oValues[1];

		return FlashLiteHelper.GetFlashGameUrl(sessionObjs,sGameType,sSexCd,sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
	}
}
