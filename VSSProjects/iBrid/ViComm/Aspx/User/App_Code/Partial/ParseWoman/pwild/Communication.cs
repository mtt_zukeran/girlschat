﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: コミュニケーション検索結果
--	Progaram ID		: Communication
--  Creation Date	: 2013.04.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseCommunication(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$COMM_TYPE":
				this.parseWoman.SetFieldValue(pTag,"COMM_TYPE",out sValue);
				break;
			case "$COMM_DATE":
				this.parseWoman.SetFieldValue(pTag,"COMM_DATE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
