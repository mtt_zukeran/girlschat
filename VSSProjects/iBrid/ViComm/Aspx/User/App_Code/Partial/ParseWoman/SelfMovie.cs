﻿using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseSelfMovie(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		string sObjSeq = string.Empty;

		switch (pTag) {
			case "$HREF_SELF_MOVIE_DOWNLOAD":
				sValue = GetHRefSelfMovieDwonload(pTag,pArgument);
				break;
			case "$SELF_MOVIE_ATTR_NM":
				sValue = GetSelfMovieAttrNm(pTag);
				break;
			case "$SELF_MOVIE_ATTR_TYPE_NM":
				sValue = GetSelfMovieAttrTypeNm(pTag);
				break;
			case "$SELF_MOVIE_AUTH_MARK":
				SetFieldValue(pTag,"UNAUTH_MARK",out sValue);
				break;
			case "$SELF_MOVIE_UNAUTH_FLAG":
				SetFieldValue(pTag,"OBJ_NOT_APPROVE_FLAG",out sValue);
				break;
			case "$SELF_MOVIE_DOC":
				sValue = GetDoc(pTag,"MOVIE_DOC");
				break;
			case "$SELF_MOVIE_TITLE":
				SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$SELF_MOVIE_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$SELF_MOVIE_LIKE_COUNT":
				SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$SELF_MOVIE_COMMENT_COUNT":
				SetFieldValue(pTag,"CAST_MOVIE_COMMENT_COUNT",out sValue);
				break;
			case "$SELF_MOVIE_VIEW_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$SELF_MOVIE_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;
			case "$SELF_MOVIE_PLANNING_TYPE":
				SetFieldValue(pTag,"PLANNING_TYPE",out sValue);
				break;
			case "$QUERY_VIEW_BBS_MOVIE":
				sValue = GetQueryViewSelfBbsMovie(pTag);
				break;
			case "$QUERY_VIEW_SELF_STOCK_MOVIE":
				sValue = GetQueryViewSelfStockMovie(pTag);
				break;
			case "$SELF_MOVIE_DELETABLE_FLAG":
				string sAttrSeq = string.Empty;
				if (GetValue(pTag,"CAST_MOVIE_ATTR_SEQ",out sAttrSeq)) {
					using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
						sValue = oCastMovieAttrTypeValue.IsDeletable(sessionWoman.site.siteCd,sAttrSeq) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
					}
				}
				break;

			case "$OBJ_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;

			//レビュー
			case "$REVIEW_COUNT":
				SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = GetGoodStarAvrX10(pTag);
				break;
			case "$OBJ_RANKING_FLAG":
				SetFieldValue(pTag,"OBJ_RANKING_FLAG",out sValue);
				break;
			// ピックアップ（特集）
			case "$DIV_IS_PICKUP_OBJ":
				if (GetValue(pTag,"MOVIE_SEQ",out sObjSeq)) {
					// ピックアップ（特集）の対象である場合は表示、それ以外は非表示
					PickupObjs oPickupObjs = new PickupObjs();
					DataSet ds = oPickupObjs.GetPickupInfoByObjSeq(sessionWoman.site.siteCd,sObjSeq,true);
					bool bFlag = (ds == null || ds.Tables[0].Rows.Count <= 0);
					SetNoneDisplay(bFlag);
				} else {
					// 非表示
					SetNoneDisplay(true);
				}
				break;
			case "$PICKUP_NM":
				if (GetValue(pTag,"MOVIE_SEQ",out sObjSeq)) {
					// ピックアップ（特集）名
					PickupObjs oPickupObjs = new PickupObjs();
					DataSet ds = oPickupObjs.GetPickupInfoByObjSeq(sessionWoman.site.siteCd,sObjSeq,true);
					sValue = oPickupObjs.ConcatPickupTitle(ds,"<br/>");
				}
				break;

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryViewSelfBbsMovie(string pTag) {
		string sValue = string.Empty;

		if (GetValue(pTag,"RNUM",out sValue)) {
			return string.Format("pageno={0}&attrtypeseq={1}&attrseq={2}",
										sValue,
										iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrtypeseq"]),
										iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["attrseq"]));
		} else {
			return string.Empty;
		}
	}
	
	private string GetHRefSelfMovieDwonload(string pTag,string pArgument) {
		string sValue = "";
		if (GetValue(pTag,"MOVIE_SEQ",out sValue)) {
			return ParseDownloadUrl(
								"1",
								sessionWoman.userWoman.userSeq,
								sessionWoman.userWoman.curCharNo,
								sessionWoman.userWoman.loginId,
								sValue,
								pArgument,
								"",
								false);
		} else {
			return "";
		}
	}

	private string GetSelfMovieAttrNm(string pTag) {
		string sMovieAttrNm = "";
		string[] sValues;
		if (GetValues(pTag,"CAST_MOVIE_ATTR_SEQ,CAST_MOVIE_ATTR_NM",out sValues)) {
			if (sValues[0].Equals(ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString())) {
				sMovieAttrNm = "指定なし";
			} else {
				sMovieAttrNm = parseContainer.Parse(sValues[1]);
			}
		}
		return sMovieAttrNm;
	}

	private string GetSelfMovieAttrTypeNm(string pTag) {
		string sMovieAttrTypeNm = "";
		string[] sValues;
		if (GetValues(pTag,"CAST_MOVIE_ATTR_TYPE_SEQ,CAST_MOVIE_ATTR_TYPE_NM",out sValues)) {
			if (sValues[0].ToString().Equals(ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString())) {
				sMovieAttrTypeNm = "指定なし";
			} else {
				sMovieAttrTypeNm = parseContainer.Parse(sValues[1]);
			}
		}
		return sMovieAttrTypeNm;
	}

	private string GetQueryViewSelfStockMovie(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,MOVIE_SEQ",out sValues)) {
			return string.Format("pageno={0}&movieseq={1}&data={2}{3}&mailtype={4}",
					sValues[0],
					sValues[1],
					iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["data"]),
					iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["batch"]) == "1" ? "&batch=1" : "",
					ViCommConst.ATTACH_MOVIE_INDEX.ToString()
					);
		} else {
			return "";
		}
	}
}
