﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者待機スケジュール検索結果
--	Progaram ID		: WaitSchedule
--  Creation Date	: 2014.11.19
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseWaitSchedule(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$CAST_WAIT_SCHEDULE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"WAIT_SCHEDULE_SEQ",out sValue);
				break;
			case "$CAST_WAIT_START_TIME":
				this.parseWoman.SetFieldValue(pTag,"WAIT_START_TIME",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}
