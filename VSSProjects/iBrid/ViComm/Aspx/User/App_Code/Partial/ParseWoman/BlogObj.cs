﻿using System;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseBlogObj(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		string[] oValues;
		string sSiteCd = string.Empty;
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		string sObjSeq = string.Empty;
		string sBlogFileType = string.Empty;
		string sBlogArticleSeq = string.Empty;

		if (GetDataRow(currentTableIdx) != null) {
			if (!GetValues(pTag, "SITE_CD,USER_SEQ,USER_CHAR_NO,OBJ_SEQ,BLOG_FILE_TYPE,BLOG_ARTICLE_SEQ", out oValues)) {
				pParsed = false;
				return string.Empty;
			}
			sSiteCd = oValues[0];
			sUserSeq = oValues[1];
			sUserCharNo = oValues[2];
			sObjSeq = oValues[3];
			sBlogFileType = oValues[4];
			sBlogArticleSeq = oValues[5];
		}
		

		switch (pTag) {
			case "$DIV_IS_PIC":
				SetNoneDisplay(!(sBlogFileType.Equals(ViCommConst.BlogFileType.PIC)));
				break;
			case "$DIV_IS_MOVIE":
				SetNoneDisplay(!(sBlogFileType.Equals(ViCommConst.BlogFileType.MOVIE)));
				break;	
			case "$APPEND_BLOG_ARTICLE_SEQ":
				sValue =iBridUtil.GetStringValue(sessionWoman.requestQuery["append_blog_article_seq"]);
				break;
			case "$BLOG_OBJ_SEQ":
				sValue = sObjSeq;
				break;
			case "$BLOG_ARTICLE_SEQ":
				SetFieldValue(pTag, "BLOG_ARTICLE_SEQ", out sValue);
				break;
			case "$BLOG_ARTICLE_TITLE":
				SetFieldValue(pTag, "BLOG_ARTICLE_TITLE", out sValue);
				break;
			case "$BLOG_OBJ_UPLOAD_DATE":
				SetFieldValue(pTag, "REGIST_DATE", out sValue);
				break;
			case "$QUERY_BLOG_ARTICLE":
				sValue = GetQueryBlogArticle(sBlogArticleSeq);
				break;
			case "$QUERY_BLOG_OBJ":
				sValue = GetQueryBlogObj(sObjSeq);
				break;
			case "$READING_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			// 写真用 =====================================================================
			case "$BLOG_PIC_IMG_PATH":
				SetFieldValue(pTag, "OBJ_SMALL_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$BLOG_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag, "OBJ_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$BLOG_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag, "OBJ_BLUR_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			// 動画用 =====================================================================
			case "$BLOG_MOVIE_IMG_PATH":
				SetFieldValue(pTag, "THUMBNAIL_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$HREF_BLOG_MOVIE_DOWNLOAD":
				sValue = GetHRefBlogMovieDwonload(pTag, pArgument, sSiteCd, sUserSeq, sUserCharNo, sObjSeq);
				break;
				
		
			default:
				pParsed = false;
				break;
		}
		
		return sValue;
	}

}
