﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseMail(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		string sObjType = "";
		string sMailType = "";
		string sPresentMailItemSeq = string.Empty;
		string sPresentReturnFlag = string.Empty;

		switch (pTag) {
			case "$DIV_IS_MAIL_WITH_MOVIE":
				if (GetValue(pTag,"ATTACHED_OBJ_TYPE",out sObjType)) {
					SetNoneDisplay(!(sObjType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())));
				}
				break;
			case "$DIV_IS_MAIL_WITH_PIC":
				if (GetValue(pTag,"ATTACHED_OBJ_TYPE",out sObjType)) {
					SetNoneDisplay(!(sObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())));
				}
				break;
			case "$DIV_IS_RX_MAIL":
				if (GetValue(pTag,"TXRX_TYPE",out sMailType)) {
					SetNoneDisplay(!(sMailType.Equals(ViCommConst.RX)));
				}
				break;
			case "$DIV_IS_TX_MAIL":
				if (GetValue(pTag,"TXRX_TYPE",out sMailType)) {
					SetNoneDisplay(!(sMailType.Equals(ViCommConst.TX)));
				}
				break;
			case "$HREF_MAN_MOVIE_DOWNLOAD":
				sValue = GetHrefManMovieDownload(pTag,pArgument);
				break;
			case "$HREF_SELF_MOVIE_DOWNLOAD":
				sValue = GetHrefSelfMovieDownload(pTag,pArgument);
				break;
			case "$MAIL_ATTACHED_OPEN_FLAG":
				SetFieldValue(pTag,"ATTACHED_OBJ_OPEN_FLAG",out sValue);
				break;
			case "$MAIL_CREATE_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$MAIL_DELETE_CHECK_BOX":
				sValue = GetCheckBoxTag(pTag,"MAIL_SEQ");
				break;
			case "$MAIL_DOC":
				sValue = GetMailDoc(pTag);
				break;
			case "$MAIL_DEL_PROTECT_FLAG":
				SetFieldValue(pTag,"DEL_PROTECT_FLAG",out sValue);
				break;
			case "$MAIL_NEW_FLAG":
				sValue = GetMailNewFlag(pTag).ToString();
				break;
			case "$MAIL_PIC_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAIL_READ_FLAG":
				SetFieldValue(pTag,"READ_FLAG",out sValue);
				break;
			case "$MAIL_RETURN_FLAG":
				SetFieldValue(pTag,"RETURN_MAIL_FLAG",out sValue);
				break;
			case "$MAIL_SERVICE_POINT":
				SetFieldValue(pTag,"SERVICE_POINT",out sValue);
				break;
			case "$MAIL_SUBJECT":
				SetFieldValue(pTag,"MAIL_TITLE",out sValue);
				break;
			case "$MAN_AGE":
				SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				sValue = ManHandleNmEmptyFill(sValue);
				break;
			case "$MAN_ITEM":
				sValue = GetManItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$MAN_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_IMG_MARK":
				sValue = GetManImgMark(pTag,pArgument);
				break;
			case "$MAN_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$MAN_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$MAN_STATUS_IS_NORMAL_FLAG":
				sValue = ManStatusIsNormal(pTag);
				break;
			case "$MAN_LAST_RECEIPT_DATE": {
					sValue = this.GetManLastReceiptDate(pTag);
				}
				break;
			case "$MAN_LAST_RECEIPT_PAST_DAYS": {
					DateTime dtLastPeceipt;
					if (DateTime.TryParse(this.GetManLastReceiptDate(pTag),out dtLastPeceipt)) {
						sValue = ((TimeSpan)(DateTime.Today - dtLastPeceipt.Date)).Days.ToString();
					} else {
						sValue = string.Empty;
					}
				}
				break;
			case "$QUERY_MAIL_INFO_ALL":
				sValue = string.Format("mailtype={0}",ViCommConst.DELETE_MAIL_INFO);
				break;
			case "$QUERY_MAIL_INFO_READ_ONLY":
				sValue = string.Format("mailtype={0}&readonly={1}",ViCommConst.DELETE_MAIL_INFO,ViCommConst.FLAG_ON.ToString());
				break;
			case "$QUERY_MAIL_RX_ALL":
				sValue = string.Format("mailtype={0}",ViCommConst.DELETE_MAIL_RX);
				break;
			case "$QUERY_MAIL_RX_READ_ONLY":
				sValue = string.Format("mailtype={0}&readonly={1}",ViCommConst.DELETE_MAIL_RX,ViCommConst.FLAG_ON.ToString());
				break;
			case "$QUERY_MAIL_TX_ALL":
				sValue = string.Format("mailtype={0}",ViCommConst.DELETE_MAIL_TX);
				break;
			case "$QUERY_RETURN_MAIL":
				sValue = GetQueryReturnMail(pTag);
				break;
			case "$QUERY_VIEW_MAIL":
				sValue = GetQueryViewMail(pTag);
				break;
			case "$QUERY_MAN_PROFILE":
				sValue = GetQueryManProfile(pTag);
				break;
			case "$MAIL_HAS_PIC_FLAG":
				sValue = GetMailHasPicFlag(pTag);
				break;
			case "$MAIL_HAS_MOVIE_FLAG":
				sValue = GetMailHasMovieFlag(pTag);
				break;
			case "$MAN_FEATURE_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.DOCOMO,ViCommConst.KDDI,ViCommConst.SOFTBANK);
				break;
			case "$MAN_SMART_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID,ViCommConst.IPHONE);
				break;
			case "$MAN_ANDROID_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID);
				break;
			case "$MAN_IPHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.IPHONE);
				break;
			case "$MAN_NG_SKULL_COUNT":
				SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			case "$LIST_RNUM":
				SetFieldValue(pTag,"RNUM",out sValue);
				break;
			case "$DRAFT_MAIL_SEQ":
				SetFieldValue(pTag,"DRAFT_MAIL_SEQ",out sValue);
				break;
			case "$DRAFT_MAIL_DOC":
				sValue = GetDraftMailDoc(pTag);
				break;
			case "$LAST_UPDATE_DATE":
				SetFieldValue(pTag,"LAST_UPDATE_DATE",out sValue);
				break;
			case "$QUICK_RES_MAIL_FLAG":
				SetFieldValue(pTag,"QUICK_RES_MAIL_FLAG",out sValue);
				break;
			case "$TX_USER_SEQ":
				SetFieldValue(pTag,"TX_USER_SEQ",out sValue);
				break;
			case "$TX_USER_CHAR_NO":
				SetFieldValue(pTag,"TX_USER_CHAR_NO",out sValue);
				break;
			case "$MAIL_SEQ":
				SetFieldValue(pTag,"MAIL_SEQ",out sValue);
				break;
			case "$PRESENT_MAIL_ITEM_SEQ":
				SetFieldValue(pTag,"PRESENT_MAIL_ITEM_SEQ",out sValue);
				break;
			case "$DIV_IS_PRESENT_MAIL":
				if (GetValue(pTag,"PRESENT_MAIL_ITEM_SEQ",out sPresentMailItemSeq)) {
					SetNoneDisplay(string.IsNullOrEmpty(sPresentMailItemSeq));
				}
				break;
			case "$DIV_IS_NOT_PRESENT_MAIL":
				if (GetValue(pTag,"PRESENT_MAIL_ITEM_SEQ",out sPresentMailItemSeq)) {
					SetNoneDisplay(!string.IsNullOrEmpty(sPresentMailItemSeq));
				}
				break;
			case "$DIV_IS_PRESENT_RETURN_MAIL":
				if (GetValue(pTag,"PRESENT_RETURN_FLAG",out sPresentReturnFlag)) {
					SetNoneDisplay(!sPresentReturnFlag.Equals(ViCommConst.FLAG_ON_STR));
				}
				break;
			case "$DIV_IS_NOT_PRESENT_RETURN_MAIL":
				if (GetValue(pTag,"PRESENT_RETURN_FLAG",out sPresentReturnFlag)) {
					SetNoneDisplay(sPresentReturnFlag.Equals(ViCommConst.FLAG_ON_STR));
				}
				break;
			//ファンクラブ
			case "$MY_FANCLUB_MEMBER_RANK":
				string sUserSeq;
				if (GetValue(pTag,"PARTNER_USER_SEQ",out sUserSeq)) {
					sValue = GetMyFanClubMemberRank(sUserSeq);
				}
				break;
			case "$CHAT_MAIL_PARTS":
				sValue = this.GetChatMailParts(pTag,pArgument);
				break;
			// 3分以内メール返信ボーナス
			case "$MAIL_RES_BONUS_MAIN_FLAG":
				if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus)) {
					SetFieldValue(pTag,"IS_MAIL_RES_BONUS_1",out sValue);
				}
				break;
			// 10分以内メール返信ボーナス
			case "$MAIL_RES_BONUS_SUB_FLAG":
				if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus)) {
					SetFieldValue(pTag,"IS_MAIL_RES_BONUS_2",out sValue);
				}
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetMailDoc(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "",sDoc5 = "";
		string[] sMailData;
		if (GetValue(pTag,"MAIL_DOC1",out sDoc1) && GetValue(pTag,"MAIL_DOC2",out sDoc2) && GetValue(pTag,"MAIL_DOC3",out sDoc3) && GetValue(pTag,"MAIL_DOC4",out sDoc4) && GetValue(pTag,"MAIL_DOC5",out sDoc5)) {
			sValue = parseContainer.Parse(sDoc1 + sDoc2 + sDoc3 + sDoc4 + sDoc5);
		}
		if (string.IsNullOrEmpty(sValue)) {
			string sRequestTxMailSeq;
			if (GetValue(pTag,"REQUEST_TX_MAIL_SEQ",out sRequestTxMailSeq)) {
				using (MailLog oMailLog = new MailLog()) {
					sValue = oMailLog.GetOriginalMailDoc(sRequestTxMailSeq);
				}
			}
		}
		string sTxSexCd = string.Empty;
		string sMailType = string.Empty;
		if (GetValues(pTag,"TX_SEX_CD,MAIL_TYPE",out sMailData)) {
			sTxSexCd = sMailData[0];
			sMailType = sMailData[1];

			if (sTxSexCd.Equals(ViCommConst.OPERATOR)) {
				if (sMailType.Equals(ViCommConst.MAIL_TP_INVITE_TALK) || sMailType.Equals(ViCommConst.MAIL_TP_MEETING_TV) || sMailData[1].Equals(ViCommConst.MAIL_TP_MEETING_VOICE)) {
					Regex regex = new Regex(@"(href=)(.*?start.aspx.*?)(>)",RegexOptions.IgnoreCase);
					//Regex regex = new Regex(@"(href=)(.*?)(>)",RegexOptions.IgnoreCase);
					sValue = regex.Replace(sValue,"$3");
				}
				if (sMailType.Equals(ViCommConst.MAIL_TP_MEETING_TV) || sMailData[1].Equals(ViCommConst.MAIL_TP_MEETING_VOICE)) {
					Regex regex = new Regex(@"(href=)(.*?RequestIVP.aspx.*?&meeting=.*?)(>)",RegexOptions.IgnoreCase);
					//Regex regex = new Regex(@"(href=)(.*?)(>)",RegexOptions.IgnoreCase);
					sValue = regex.Replace(sValue,"$3");
				}
			}
		}

		string sTextMailFlag = string.Empty;
		if (GetValue(pTag,"TEXT_MAIL_FLAG",out sTextMailFlag)) {
			if (!sTextMailFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				return sValue;
			}
		}
		return Regex.Replace(sValue,"\r\n","<br />");
		//
	}

	private string GetHrefManMovieDownload(string pTag,string pArgument) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,TX_USER_SEQ,TX_USER_CHAR_NO,PARTNER_LOGIN_ID,MOVIE_SEQ",out sValues)) {
			return ParseManDownloadUrl(sValues[0],
										sValues[1],
										sValues[2],
										sValues[3],
										sValues[4],
										pArgument);
		} else {
			return "";
		}
	}

	private string GetHrefSelfMovieDownload(string pTag,string pArgument) {
		string[] sValues;
		if (GetValues(pTag,"MOVIE_SEQ",out sValues)) {
			return ParseDownloadUrl(
					"1",
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sessionWoman.userWoman.loginId,
					sValues[0],
					pArgument,
					"",
					false);
		} else {
			return "";
		}
	}

	private string GetQueryReturnMail(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"TX_USER_SEQ,MAIL_SEQ",out sValues)) {
			return string.Format("direct=1&txuserseq={0}&mailseq={1}",sValues[0],sValues[1]);
		} else {
			return "";
		}
	}

	private string GetQueryViewMail(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,MAIL_SEQ",out sValues)) {
			return string.Format("pageno={0}&loginid={1}&mailseq={2}&seekmode={3}",
				sValues[0],
				iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["loginid"]),
				sValues[1],
				sessionWoman.seekMode);
		} else {
			return "";
		}
	}

	private string ManStatusIsNormal(string pTag) {
		string sValue = "";
		if (GetValue(pTag,"PARTNER_USER_STATUS",out sValue)) {
			if (sValue.Equals(ViCommConst.USER_MAN_HOLD) || sValue.Equals(ViCommConst.USER_MAN_RESIGNED) || sValue.Equals(ViCommConst.USER_MAN_STOP) || sValue.Equals(ViCommConst.USER_MAN_BLACK)) {
				sValue = ViCommConst.FLAG_OFF_STR;
			} else {
				sValue = ViCommConst.FLAG_ON_STR;
			}
		}
		return sValue;
	}

	private string GetMailHasPicFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ATTACHED_OBJ_TYPE,PIC_SEQ",out sValues)) {
			if (ViCommConst.ATTACH_PIC_INDEX.ToString().Equals(sValues[0]) && !string.IsNullOrEmpty(sValues[1])) {
				return ViCommConst.FLAG_ON_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}

	private string GetMailHasMovieFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ATTACHED_OBJ_TYPE,MOVIE_SEQ",out sValues)) {
			if (ViCommConst.ATTACH_MOVIE_INDEX.ToString().Equals(sValues[0]) && !string.IsNullOrEmpty(sValues[1])) {
				return ViCommConst.FLAG_ON_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}

	private string GetDraftMailDoc(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "",sDoc5 = "";
		if (GetValue(pTag,"MAIL_DOC1",out sDoc1) && GetValue(pTag,"MAIL_DOC2",out sDoc2) && GetValue(pTag,"MAIL_DOC3",out sDoc3) && GetValue(pTag,"MAIL_DOC4",out sDoc4) && GetValue(pTag,"MAIL_DOC5",out sDoc5)) {
			sValue = parseContainer.Parse(sDoc1 + sDoc2 + sDoc3 + sDoc4 + sDoc5);
		}
		
		return Regex.Replace(sValue,"\r\n","<br />");
	}
	
	private string GetYesterdayMailCount(string pTag) {
		string sValue = string.Empty;
		
		using (MailLog oMailLog = new MailLog()) {
			sValue = oMailLog.GetYesterdayTxMailCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
		}
		
		return sValue;
	}

	private string GetChatMailParts(string pTag,string pArgument) {
		string sReturn = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"TX_SITE_CD,TX_USER_SEQ,TX_USER_CHAR_NO",out sValues)) {
			DataSet oDataSet = new DataSet();
			ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this,pTag,pArgument);

			MailLogSeekCondition oCondition = new MailLogSeekCondition();
			oCondition.CastSiteCd = sessionWoman.site.siteCd;
			oCondition.CastUserSeq = sessionWoman.userWoman.userSeq;
			oCondition.CastCharNo = sessionWoman.userWoman.curCharNo;
			oCondition.ManUserSiteCd = sValues[0];
			oCondition.ManUserSeq = sValues[1];
			oCondition.ManUserCharNo = sValues[2];

			int iDispCount;
			if (!int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["dispcount"]),out iDispCount)) {
				iDispCount = oPartsArgs.NeedCount;
			}

			int iStartIndex = 0;
			int iListNo;
			if (int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["listno"]),out iListNo)) {
				iStartIndex = (iListNo - 1) * iDispCount;
			}

			using (MailLog oMailLog = new MailLog()) {
				oDataSet = oMailLog.GetCastChatMail(oCondition,iStartIndex,oPartsArgs.NeedCount);
			}

			if (!sessionWoman.adminFlg) {
				List<string> oMailSeqList = new List<string>();

				foreach (DataRow dr in oDataSet.Tables[0].Rows) {
					if (iBridUtil.GetStringValue(dr["TXRX_TYPE"]).Equals(ViCommConst.RX) && iBridUtil.GetStringValue(dr["READ_FLAG"]).Equals(ViCommConst.FLAG_OFF_STR)) {
						oMailSeqList.Add(iBridUtil.GetStringValue(dr["MAIL_SEQ"]));
					}
				}

				if (oMailSeqList.Count > 0) {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailReadFlagBatch(oMailSeqList.ToArray());
					}
				}
			}

			sReturn = parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
		}

		return sReturn;
	}

	private string GetChatMailPartsByUser(string pTag,string pArgument) {
		string sReturn = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"SITE_CD,USER_SEQ,USER_CHAR_NO",out sValues)) {
			DataSet oDataSet = new DataSet();
			ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this,pTag,pArgument);

			MailLogSeekCondition oCondition = new MailLogSeekCondition();
			oCondition.CastSiteCd = sessionWoman.site.siteCd;
			oCondition.CastUserSeq = sessionWoman.userWoman.userSeq;
			oCondition.CastCharNo = sessionWoman.userWoman.curCharNo;
			oCondition.ManUserSiteCd = sValues[0];
			oCondition.ManUserSeq = sValues[1];
			oCondition.ManUserCharNo = sValues[2];

			int iDispCount;
			if (!int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["dispcount"]),out iDispCount)) {
				iDispCount = oPartsArgs.NeedCount;
			}

			int iStartIndex = 0;
			int iListNo;
			if (int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["listno"]),out iListNo)) {
				iStartIndex = (iListNo - 1) * iDispCount;
			}

			using (MailLog oMailLog = new MailLog()) {
				oDataSet = oMailLog.GetCastChatMail(oCondition,iStartIndex,oPartsArgs.NeedCount);
			}

			if (!sessionWoman.adminFlg) {
				List<string> oMailSeqList = new List<string>();

				foreach (DataRow dr in oDataSet.Tables[0].Rows) {
					if (iBridUtil.GetStringValue(dr["TXRX_TYPE"]).Equals(ViCommConst.RX) && iBridUtil.GetStringValue(dr["READ_FLAG"]).Equals(ViCommConst.FLAG_OFF_STR)) {
						oMailSeqList.Add(iBridUtil.GetStringValue(dr["MAIL_SEQ"]));
					}
				}

				if (oMailSeqList.Count > 0) {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailReadFlagBatch(oMailSeqList.ToArray());
					}
				}
			}

			sReturn = parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
		}

		return sReturn;
	}
}
