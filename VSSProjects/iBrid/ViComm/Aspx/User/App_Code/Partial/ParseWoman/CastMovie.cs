﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画検索結果
--	Progaram ID		: CastMovie
--  Creation Date	: 2014.06.05
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseCastMovie(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_MOVIE_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;
			case "$CAST_MOVIE_TITLE":
				SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$CAST_MOVIE_DOC":
				SetFieldValue(pTag,"MOVIE_DOC",out sValue);
				break;
			case "$CAST_MOVIE_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$CAST_MOVIE_LIKE_COUNT":
				SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$HREF_CAST_MOVIE_DOWNLOAD_FREE":
				sValue = GetHrefMovieDownload(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetHrefMovieDownload(string pTag,string pArgument) {
		string sUrl = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO,LOGIN_ID,MOVIE_SEQ",out sValues)) {
			sUrl = ParseDownloadUrl(
				"1",
				sValues[0],
				sValues[1],
				sValues[2],
				sValues[3],
				pArgument,
				"",
				false
			);
		}

		return sUrl;
	}
}
