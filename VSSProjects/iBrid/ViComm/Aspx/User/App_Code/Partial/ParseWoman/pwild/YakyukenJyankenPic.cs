﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳ジャンケン画像検索結果
--	Progaram ID		: YakyukenJyankenPic
--  Creation Date	: 2013.04.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseYakyukenJyankenPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$JYANKEN_TYPE":
				this.parseWoman.SetFieldValue(pTag,"JYANKEN_TYPE",out sValue);
				break;
			case "$PIC_SEQ":
				this.parseWoman.SetFieldValue(pTag,"PIC_SEQ",out sValue);
				break;
			case "$AUTH_FLAG":
				this.parseWoman.SetFieldValue(pTag,"AUTH_FLAG",out sValue);
				break;
			case "$PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SMALL_PHOTO_IMG_PATH":
				this.parseWoman.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$YAKYUKEN_JYANKEN_PIC_UPLOAD_ADDR":
				sValue = this.GetYakyukenJyankenPicUploadAddr(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetYakyukenJyankenPicUploadAddr(string pTag,string pArgument) {
		string sAddr = string.Empty;
		string[] sValues;

		if (this.parseWoman.GetValues(pTag,"JYANKEN_TYPE",out sValues)) {
			sAddr = string.Format("{0}{1}{2}{3}_{4}_{5}@{6}",
				ViCommConst.JYANKEN_URL_HEADER2,
				parseWoman.sessionWoman.site.siteCd,
				parseWoman.sessionWoman.userWoman.loginId,
				parseWoman.sessionWoman.userWoman.curCharNo,
				parseWoman.sessionWoman.userWoman.userSeq,
				sValues[0],
				parseWoman.sessionWoman.site.mailHost
			);
		}

		return sAddr;
	}
}