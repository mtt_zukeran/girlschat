﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者新着いいね通知検索結果
--	Progaram ID		: CastLikeNotice
--  Creation Date	: 2016.10.05
 * 
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class ParseWoman {
	private string ParseCastLikeNotice(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$CAST_LIKE_NOTICE_SEQ":
				SetFieldValue(pTag,"CAST_LIKE_NOTICE_SEQ",out sValue);
				break;
			case "$CONTENTS_TYPE":
				SetFieldValue(pTag,"CONTENTS_TYPE",out sValue);
				break;
			case "$OBJ_SEQ":
				SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;
			case "$CAST_DIARY_REPORT_DAY":
				SetFieldValue(pTag,"CAST_DIARY_REPORT_DAY",out sValue);
				break;
			case "$CAST_DIARY_SUB_SEQ":
				SetFieldValue(pTag,"CAST_DIARY_SUB_SEQ",out sValue);
				break;
			case "$UNCONFIRMED_COUNT":
				SetFieldValue(pTag,"UNCONFIRMED_COUNT",out sValue);
				break;
			case "$UNCONFIRMED_COUNT_OTHER":
				SetFieldValue(pTag,"UNCONFIRMED_COUNT_OTHER",out sValue);
				break;
			case "$LAST_LIKE_DATE":
				SetFieldValue(pTag,"LAST_LIKE_DATE",out sValue);
				break;
			case "$CONFIRMED_FLAG":
				SetFieldValue(pTag,"CONFIRMED_FLAG",out sValue);
				break;
			case "$SELF_PIC_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_MOVIE_IMG_PATH":
				SetFieldValue(pTag,"THUMBNAIL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_TITLE":
				SetFieldValue(pTag,"PIC_TITLE",out sValue);
				break;
			case "$CAST_MOVIE_TITLE":
				SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				SetFieldValue(pTag,"MAN_HANDLE_NM",out sValue);
				break;
			case "$HREF_CAST_MOVIE_DOWNLOAD_FREE":
				sValue = this.GetHRefSelfMovieDwonload(pTag,pArgument);
				break;
			case "$SELF_OBJ_LIKE_COUNT_OTHER":
				SetFieldValue(pTag,"LIKE_COUNT_OTHER",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}