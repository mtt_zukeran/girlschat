﻿using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

partial class ParseWoman {
	private string ParseCastRanking(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_COMMENT_LIST":
				SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$CAST_RANKING_RNUM":
				sValue = (sessionWoman.startRNum + parseContainer.currentRecPos[ViCommConst.DATASET_RANKING]).ToString();
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag,"CONNECT_TYPE_NM",out sValue);
				break;
			case "$CAST_SUMMARY_COUNT":
				SetFieldValue(pTag,"SUMMARY_COUNT",out sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_RANKING_END_DATE":
				sValue = GetRankingEndDate(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetCastItem(string pTag,string pArgument) {
		string sValue;
		SetFieldValue(pTag,"CAST_ATTR_VALUE" + pArgument,out sValue);
		return sValue;
	}

	private string GetAge(string pTag,string pArgument) {
		string sText = "";
		string sValue = "0";

		sText = parseContainer.Parse(pArgument);

		if (sText.Length == 10) {
			sValue = ViCommPrograms.Age(sText).ToString();
		}
		if (sValue.Equals("0")) {
			sValue = "秘密";
		}
		return sValue;
	}

	private string GetRankingEndDate(string pTag) {
		string sValue = string.Empty;
		string sRankingEndDate = string.Empty;

		if (GetValue(pTag,"RANKING_CTL_SEQ",out sValue)) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				sRankingEndDate = oRankingCtl.GetSummaryEndDate(sValue);
			}
		}
		return sRankingEndDate;
	}
}
