﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ お宝動画カテゴリ検索結果
--	Progaram ID		: Request
--  Creation Date	: 2013.01.04
--  Creater			: PW Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseCastMovieAttrType(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$CAST_MOVIE_ATTR_TYPE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_MOVIE_ATTR_TYPE_SEQ",out sValue);
				break;
			case "$CAST_MOVIE_ATTR_TYPE_NM":
				this.parseWoman.SetFieldValue(pTag,"CAST_MOVIE_ATTR_TYPE_NM",out sValue);
				break;
			case "$CAST_MOVIE_ATTR_SEQ":
				this.parseWoman.SetFieldValue(pTag,"CAST_MOVIE_ATTR_SEQ",out sValue);
				break;
			case "$CAST_MOVIE_ATTR_NM":
				this.parseWoman.SetFieldValue(pTag,"CAST_MOVIE_ATTR_NM",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	public string GetCastMovieAttrTypeValueList(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		CastMovieAttrTypeValueSeekCondition oCondition = new CastMovieAttrTypeValueSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;

		DataSet oTmpDataSet = null;

		using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
			oTmpDataSet = oCastMovieAttrTypeValue.GetListByCastMovieAttrTypeValueSeq(oCondition);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_CAST_MOVIE_ATTR_TYPE);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
}