﻿using System;
using iBridCommLib;
using ViComm;
using System.Text;
using System.Data;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParseGameItem(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$GAME_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sValue);
				break;
			case "$GAME_ITEM_NM"://
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_NM",out sValue);
				break;
			case "$GAME_ITEM_ATTACK_POWER":
				this.parseWoman.SetFieldValue(pTag,"ATTACK_POWER",out sValue);
				break;
			case "$GAME_ITEM_DEFENCE_POWER":
				this.parseWoman.SetFieldValue(pTag,"DEFENCE_POWER",out sValue);
				break;
			case "$GAME_ITEM_ENDURANCE":
				this.parseWoman.SetFieldValue(pTag,"ENDURANCE",out sValue);
				break;
			case "$GAME_ITEM_ENDURANCE_NM":
				this.parseWoman.SetFieldValue(pTag,"ENDURANCE_NM",out sValue);
				break;
			case "$GAME_ITEM_DESCRIPTION":
				this.parseWoman.SetFieldValue(pTag,"DESCRIPTION",out sValue);
				break;
			case "$GAME_ITEM_REMARKS":
				this.parseWoman.SetFieldValue(pTag,"REMARKS",out sValue);
				break;
			case "$GAME_ITEM_GET_CD":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_GET_CD",out sValue);
				break;
			case "$GAME_ITEM_PRICE":
				this.parseWoman.SetFieldValue(pTag,"PRICE",out sValue);
				break;
			case "$GAME_ITEM_FIXED_PRICE":
				this.parseWoman.SetFieldValue(pTag,"FIXED_PRICE",out sValue);
				break;
			case "$GAME_ITEM_PRESENT_FLAG":
				this.parseWoman.SetFieldValue(pTag,"PRESENT_FLAG",out sValue);
				break;
			case "$GAME_ITEM_PRESENT_GAME_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"PRESENT_GAME_ITEM_SEQ",out sValue);
				break;
			case "$GAME_ITEM_STAGE_SEQ":
				this.parseWoman.SetFieldValue(pTag,"STAGE_SEQ",out sValue);
				break;
			case "$GAME_ITEM_CATEGORY_TYPE":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_CATEGORY_TYPE",out sValue);
				break;
			case "$GAME_ITEM_CATEGORY_GROUP_TYPE":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_CATEGORY_GROUP_TYPE",out sValue);
				break;
			case "$GAME_ITEM_PUBLISH_FLAG":
				this.parseWoman.SetFieldValue(pTag,"PUBLISH_FLAG",out sValue);
				break;
			case "$GAME_ITEM_PUBLISH_DATE":
				this.parseWoman.SetFieldValue(pTag,"PUBLISH_DATE",out sValue);
				break;
			case "$GAME_ITEM_CREATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$GAME_ITEM_UPDATE_DATE":
				this.parseWoman.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			case "$GAME_ITEM_POSSESSION_COUNT":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_COUNT",out sValue);
				break;
			case "$GAME_ITEM_SALE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"SALE_FLAG",out sValue);
				break;
			case "$GAME_ITEM_TOTAL_PRICE":
				this.getTotalPrice(pTag,out sValue);
				break;
			case "$GAME_SALE_COMMENT":
				this.parseWoman.SetFieldValue(pTag,"SALE_COMMENT",out sValue);
				break;
			case "$GAME_SALE_NOTICE_COMMENT":
				this.parseWoman.SetFieldValue(pTag,"SALE_NOTICE_COMMENT",out sValue);
				break;
			case "$GAME_SALE_START_DATE":
				this.parseWoman.SetFieldValue(pTag,"SALE_START_DATE",out sValue);
				//sValue = this.GetSaleDateConvert(sValue);
				break;
			case "$GAME_SALE_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"SALE_END_DATE",out sValue);
				//sValue = this.GetSaleDateConvert(sValue);
				break;
			case "$GAME_LOTTERY_SEQ":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_SEQ",out sValue);
				break;
			case "$GAME_LOTTERY_COMPLETE_GET_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"COMPLETE_GET_ITEM_SEQ",out sValue);
				break;
			case "$GAME_LOTTERY_FIRST_BUY_FREE_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"FIRST_BUY_FREE_END_DATE",out sValue);
				break;
			case "$GAME_LOTTERY_PUBLISH_START_DATE":
				this.parseWoman.SetFieldValue(pTag,"PUBLISH_START_DATE",out sValue);
				break;
			case "$GAME_LOTTERY_PUBLISH_END_DATE":
				this.parseWoman.SetFieldValue(pTag,"PUBLISH_END_DATE",out sValue);
				break;
			case "$GAME_LOTTERY_PRICE":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_PRICE",out sValue);
				break;
			case "$GAME_LOTTERY_NM":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_NM",out sValue);
				break;
			case "$GAME_LOTTERY_COMPLETE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"COMPLETE_FLAG",out sValue);
				break;
			case "$GAME_NOW_LOTTERY_FLAG":
				this.parseWoman.SetFieldValue(pTag,"NOW_LOTTERY_FLAG",out sValue);
				break;
			case "$GAME_NOW_FIRST_BUY_FREE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"NOW_FIRST_BUY_FREE_FLAG",out sValue);
				break;
			case "$GAME_LOTTERY_ITEM_COUNT":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_ITEM_COUNT",out sValue);
				break;
			case "$GAME_REST_LOTTERY_COMPLETE_COUNT":
				this.parseWoman.SetFieldValue(pTag,"REST_LOTTERY_COMPLETE_COUNT",out sValue);
				break;
			case "$GAME_COMP_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"COMP_ITEM_SEQ",out sValue);
				break;
			case "$GAME_COMP_ITEM_NM":
				this.parseWoman.SetFieldValue(pTag,"COMP_ITEM_NM",out sValue);
				break;
			case "$GAME_COMP_ITEM_ATTACK_POWER":
				this.parseWoman.SetFieldValue(pTag,"COMP_ITEM_ATTACK_POWER",out sValue);
				break;
			case "$GAME_COMP_ITEM_DEFENCE_POWER":
				this.parseWoman.SetFieldValue(pTag,"COMP_ITEM_DEFENCE_POWER",out sValue);
				break;
			case "$GAME_COMP_ITEM_ENDURANCE":
				this.parseWoman.SetFieldValue(pTag,"COMP_ITEM_ENDURANCE",out sValue);
				break;
			case "$GAME_COMP_ITEM_ENDURANCE_NM":
				this.parseWoman.SetFieldValue(pTag,"COMP_ITEM_ENDURANCE_NM",out sValue);
				break;
			case "$GAME_ITEM_USED_COUNT":
				this.parseWoman.SetFieldValue(pTag,"USED_COUNT",out sValue);
				break;
			case "$GAME_LOTTERY_ITEM_GET_FLAG":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_ITEM_GET_FLAG",out sValue);
				break;
			case "$NEED_LOGIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"NEED_LOGIN_COUNT",out sValue);
				break;
			case "$REST_NEED_LOGIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"REST_NEED_LOGIN_COUNT",out sValue);
				break;
			case "$SELF_LOGIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"SELF_LOGIN_COUNT",out sValue);
				break;
			case "$NEXT_NEED_LOGIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"NEXT_NEED_COUNT",out sValue);
				break;
			case "$REST_NEXT_NEED_LOGIN_COUNT":
				this.parseWoman.SetFieldValue(pTag,"REST_NEXT_LOGIN_COUNT",out sValue);
				break;
			case "$MONTHLY_BONUS_GET_FLAG":
				this.parseWoman.SetFieldValue(pTag,"MONTHLY_BONUS_GET_FLAG",out sValue);
				break;
			case "$TOTAL_LOGIN_BONUS_GAME_POINT":
				sValue = this.GetLoginBonusDayGamePoint(pTag,pArgument);
				break;
			case "$GET_LOGIN_BONUS_ITEM_NM":
				sValue = this.GetGameLoginBonusItemNm(pTag,pArgument);
				break;
			case "$GAME_ITEM_COUNT":
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_COUNT",out sValue);
				break;
			case "$GAME_ITEM_DOUBLE_TRAP_FLAG":
				string sItemCategory = string.Empty;
				this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_CATEGORY_TYPE",out sItemCategory);
				sValue = sItemCategory.Equals("8") ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;
			case "$POSSESSION_GAME_ITEM_QUANTITY_LIST_BOX":
				sValue = this.GetPossessionGameItemQuantityListBox(pTag,pArgument);
				break;
			case "$DIV_IS_CAN_BUY_GAME_ITEM": {
					string sGameItemStageLevel = string.Empty;
					string sGameItemGetCd = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"STAGE_LEVEL",out sGameItemStageLevel);
					this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_GET_CD",out sGameItemGetCd);
					this.parseWoman.SetNoneDisplay(!this.CheckCanBuyGameItem(sGameItemStageLevel,sGameItemGetCd));
					break;
				}
			case "$DIV_IS_CAN_NOT_BUY_GAME_ITEM": {
					string sGameItemStageLevel = string.Empty;
					string sGameItemGetCd = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"STAGE_LEVEL",out sGameItemStageLevel);
					this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_GET_CD",out sGameItemGetCd);
					this.parseWoman.SetNoneDisplay(this.CheckCanBuyGameItem(sGameItemStageLevel,sGameItemGetCd));
					break;
				}
			case "$WANTED_ITEM_SEQ":
				this.parseWoman.SetFieldValue(pTag,"WANTED_ITEM_SEQ",out sValue);
				break;
			case "$GAME_ITEM_DISCOUNT_RATE":
				this.parseWoman.SetFieldValue(pTag,"DISCOUNT_RATE",out sValue);
				break;
			case "$GAME_USED_ITEM_SHORTAGE": {
					this.parseWoman.SetFieldValue(pTag,"USED_ITEM_SHORTAGE",out sValue);
					break;
				}
			case "$GAME_USED_ITEM_TOTAL_PRICE": {
					this.parseWoman.SetFieldValue(pTag,"USED_ITEM_TOTAL_PRICE",out sValue);
					break;
				}
			case "$DIV_IS_EXIST_PRESENT_GAME_ITEM": {
					string sGameItemSeq = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sGameItemSeq);
					this.parseWoman.SetNoneDisplay(!this.CheckExistPresentGameItem(sGameItemSeq));
					break;
				}
			case "$DIV_IS_NOT_EXIST_PRESENT_GAME_ITEM": {
					string sGameItemSeq = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sGameItemSeq);
					this.parseWoman.SetNoneDisplay(this.CheckExistPresentGameItem(sGameItemSeq));
					break;
				}
			case "$DIV_IS_WANTED_ITEM": {
					string sGameItemSeq = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sGameItemSeq);
					this.parseWoman.SetNoneDisplay(!this.CheckExistWantedGameItem(sGameItemSeq));
					break;
				}
			case "$DIV_IS_NOT_WANTED_ITEM": {
					string sGameItemSeq = string.Empty;
					this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sGameItemSeq);
					this.parseWoman.SetNoneDisplay(this.CheckExistWantedGameItem(sGameItemSeq));
					break;
				}
			case "$GAME_CLEAR_TOWN_GET_ITEM_NO":
				this.parseWoman.SetFieldValue(pTag,"CLEAR_TOWN_GET_ITEM_NO",out sValue);
				break;
			case "$GET_GAME_ITEM_POSSESSION_COUNT":
				sValue = this.GetGameItemPossessionCount(pTag,pArgument);
				break;
			case "$REC_NO_PER_PAGE":
				this.parseWoman.SetFieldValue(pTag,"REC_NO_PER_PAGE",out sValue);
				break;
			case "$GAME_LOTTERY_PRICE_1":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_PRICE_1",out sValue);
				break;
			case "$GAME_LOTTERY_PRICE_2":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_PRICE_2",out sValue);
				break;
			case "$GAME_LOTTERY_PRICE_3":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_PRICE_3",out sValue);
				break;
			case "$GAME_LOTTERY_COUNT_1":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_COUNT_1",out sValue);
				break;
			case "$GAME_LOTTERY_COUNT_2":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_COUNT_2",out sValue);
				break;
			case "$GAME_LOTTERY_COUNT_3":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_COUNT_3",out sValue);
				break;
			case "$GAME_LOTTERY_SET_FLG_1":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_SET_FLG_1",out sValue);
				break;
			case "$GAME_LOTTERY_SET_FLG_2":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_SET_FLG_2",out sValue);
				break;
			case "$GAME_LOTTERY_SET_FLG_3":
				this.parseWoman.SetFieldValue(pTag,"LOTTERY_SET_FLG_3",out sValue);
				break;
			case "$GAME_PREMIUM_COUNT_1":
				this.parseWoman.SetFieldValue(pTag,"PREMIUM_COUNT_1",out sValue);
				break;
			case "$GAME_PREMIUM_COUNT_2":
				this.parseWoman.SetFieldValue(pTag,"PREMIUM_COUNT_2",out sValue);
				break;
			case "$GAME_PREMIUM_COUNT_3":
				this.parseWoman.SetFieldValue(pTag,"PREMIUM_COUNT_3",out sValue);
				break;
			case "$GAME_FIRST_BUY_FREE_FLAG":
				this.parseWoman.SetFieldValue(pTag,"FIRST_BUY_FREE_FLAG",out sValue);
				break;
			case "$GAME_PM_ITEM_FLAG":
				this.parseWoman.SetFieldValue(pTag,"PM_ITEM_FLAG",out sValue);
				break;
			case "$GAME_TICKET_NM":
				this.parseWoman.SetFieldValue(pTag,"TICKET_NM",out sValue);
				break;
			case "$GAME_TICKET_COUNT":
				this.parseWoman.SetFieldValue(pTag,"TICKET_COUNT",out sValue);
				break;
			case "$GAME_POINT":
				this.parseWoman.SetFieldValue(pTag,"GAME_POINT",out sValue);
				break;
			case "$POSSESSION_TICKET_COUNT":
				this.parseWoman.SetFieldValue(pTag,"POSSESSION_TICKET_COUNT",out sValue);
				break;
			case "$RANK":
				this.parseWoman.SetFieldValue(pTag,"RANK",out sValue);
				break;
				
			#region □■□■□ 友達紹介獲得アイテム □■□■□
			case "$GAME_INTRO_SEQ":
				this.parseWoman.SetFieldValue(pTag,"GAME_INTRO_SEQ",out sValue);
				break;
			case "$ITEM_COUNT":
				this.parseWoman.SetFieldValue(pTag,"ITEM_COUNT",out sValue);
				break;
			case "$INTRO_MIN":
				this.parseWoman.SetFieldValue(pTag,"INTRO_MIN",out sValue);
				break;
			case "$INTRO_RANK":
				this.parseWoman.SetFieldValue(pTag,"INTRO_RANK",out sValue);
				break;
			case "$RARE_ITEM_FLAG":
				this.parseWoman.SetFieldValue(pTag,"RARE_ITEM_FLAG",out sValue);
				break;
			#endregion
			
			#region □■□■□ PARTS定義 □■□■□
			case "$GAME_SALE_INFO_PARTS":
				sValue = this.GetItemSaleInfo(pTag,pArgument);
				break;
			case "$GAME_PICKUP_LOTTERY_GET_ITEM_PARTS":
				sValue = this.GetPickUpLotteryGetItem(pTag,pArgument);
				break;
			case "$GAME_LOTTERY_COMP_USER_PARTS":
				sValue = this.GetLotteryCompUser(pTag,pArgument);
				break;
			case "$GAME_LOTTERY_GET_ITEM_PARTS":
				sValue = this.GetLotteryGetItem(pTag,pArgument);
				break;
			case "$GAME_LOGIN_RECORD_PARTS":
				sValue = this.GetGameLoginRecord(pTag,pArgument);
				break;
			case "$GET_DAILY_LOGIN_BONUS_PARTS":
				sValue = this.GetGameLoginBonus(pTag,pArgument);
				break;
			case "$BUY_GAME_ITEM_QUANTITY_LIST_BOX":
				sValue = this.GetGameItemQuantityListBox(pTag,pArgument);
				break;
			case "$GAME_CLEAR_TOWN_USED_ITEM_BUY_PLURAL_PARTS":
				sValue = this.GetClearTownUsedItemBuyPlural(pTag,pArgument);
				break;
			case "$GAME_LOTTERY_GET_RARE_ITEM_PARTS":
				sValue = this.GetLotteryGetRareItem(pTag,pArgument);
				break;
			case "$GAME_PICKUP_LOTTERY_GET_ITEM_NOT_COMP_PARTS":
				sValue = this.GetPickUpLotteryGetItemNotComp(pTag,pArgument);
				break;
			#endregion

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private void getTotalPrice(string pTag,out string sValue) {
		string cost;
		if (this.parseWoman.SetFieldValue(pTag,"PRICE",out cost)) {
			string buyNum = this.parseWoman.sessionWoman.requestQuery.Params["buy_num"];
			if (!String.IsNullOrEmpty(buyNum)) {
				int iCost;
				int iBuyNum;
				int.TryParse(cost,out iCost);
				int.TryParse(buyNum,out iBuyNum);
				int iValue = iCost * iBuyNum;
				sValue = iValue.ToString();
				return;
			} else {
				sValue = cost;
				return;
			}
		}

		sValue = null;
		return;
	}

	private string GetGameItemQuantityListBox(string pTag,string pArgument) {
		StringBuilder sValueBuilder = new StringBuilder("");
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		if (arrText.Length > 1) {
			int iPrice = 0;
			int iPoint = 0;
			StringBuilder sRNumBuilder = new StringBuilder();

			int iMaxQuantity = 0;

			int.TryParse(arrText[0],out iPrice);
			int.TryParse(arrText[1],out iPoint);
			if(arrText.Length > 2) {
				sRNumBuilder.Append("_" + arrText[2]);
			}
			
			string sRNum = sRNumBuilder.ToString();

			double dTmp = iPoint / iPrice;
			int.TryParse(Math.Ceiling(dTmp).ToString(),out iMaxQuantity);

			PwViCommConst oPwViCommConst = new PwViCommConst();

			if (iMaxQuantity > 0) {
				sValueBuilder.AppendLine("<select name=\"buyItemNum" + sRNum + "\">");

				foreach (int Quantity in oPwViCommConst.BUY_ITEM_QUANTITY_LIST) {
					if (iMaxQuantity >= Quantity) {
						sValueBuilder.AppendLine("<option value=" + Quantity + ">" + Quantity + "</option>");
					}
				}

				sValueBuilder.AppendLine("</select>");
			}
			sValue = sValueBuilder.ToString();
			return sValue;
		} else {
			return "";
		}
	}

	private string GetItemSaleInfo(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameItemSaleSeekCondition oCondition = new GameItemSaleSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oTmpDataSet = null;

		using (GameItemSale oGameItemSale = new GameItemSale()) {
			oTmpDataSet = oGameItemSale.GetSaleInfo(oCondition);
		}

		string sValue = string.Empty;
		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		
		return sValue;
	}
	
	private string GetRecoveryItemData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		GameItemSeekCondition oCondition = new GameItemSeekCondition();
		oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.GameItemCategoryGroupType = PwViCommConst.GameItemCatregoryGroup.RESTORE;
		oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
		
		using (GameItem oGameItem = new GameItem()) {
			oTmpDataSet = oGameItem.GetPageCollection(oCondition,1,1);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
	
	private string GetPickUpLotteryGetItem(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.PickupFlag = ViCommConst.FLAG_ON_STR;
		oCondition.RareItemFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;

		using (LotteryGetItem oLotteryGetItem = new LotteryGetItem()) {
			oTmpDataSet = oLotteryGetItem.GetList(oCondition);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);

		return sValue;
	}

	private string GetLotteryCompUser(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		LotterySeekCondition oCondition = new LotterySeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;

		using (Lottery oLottery = new Lottery()) {
			oTmpDataSet = oLottery.GetOneCompUserData(oCondition);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetLotteryGetItem(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;

		using (LotteryGetItem oLotteryGetItem = new LotteryGetItem()) {
			oTmpDataSet = oLotteryGetItem.GetListAddGetFlag(oCondition,1,oPartsArgs.NeedCount);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);

		return sValue;
	}

	private string GetGameLoginRecord(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameLoginBonusMonthlySeekCondition oCondition = new GameLoginBonusMonthlySeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DateTime dt = DateTime.Now;
		oCondition.Month = string.Format("{0}/{1}",dt.ToString("yyyy"),dt.ToString("MM"));

		DataSet oTmpDataSet = null;

		using (GameLoginBonusMonthly oGameLoginBonusMonthly = new GameLoginBonusMonthly()) {
			oTmpDataSet = oGameLoginBonusMonthly.GetLoginRecord(oCondition);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetGameLoginBonus(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameLoginBonusDailySeekCondition oCondition = new GameLoginBonusDailySeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.DateNowFlag = ViCommConst.FLAG_ON_STR;
		oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;

		DataSet oTmpDataSet = null;

		using (GameLoginBonusDaily oGameLoginBonusDaily = new GameLoginBonusDaily()) {
			oTmpDataSet = oGameLoginBonusDaily.GetDailyLoginBonusData(oCondition);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetGameLoginBonusItemNm(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sGameItemNmCsv = string.Empty;
		string sGameItemCountCsv = string.Empty;
		string sNmSeparateString = string.Empty;
		string sCountString = string.Empty;

		this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_NM_CSV",out sGameItemNmCsv);
		this.parseWoman.SetFieldValue(pTag,"GAME_ITEM_COUNT_CSV",out sGameItemCountCsv);

		string[] arrText = pArgument.Split(',');
		string[] GameItemNmArr = sGameItemNmCsv.Split(',');
		string[] GameItemCountArr = sGameItemCountCsv.Split(',');

		if (arrText.Length > 1) {
			sNmSeparateString = arrText[0];
			sCountString = arrText[1];
		} else {
			sNmSeparateString = "と";
			sCountString = "個";
		}

		List<string> GetItemCountList = new List<string>();

		for (int i = 0;i < GameItemNmArr.Length;i++) {
			GetItemCountList.Add(GameItemNmArr[i] + GameItemCountArr[i] + sCountString);
		}

		sValue = string.Join(sNmSeparateString,GetItemCountList.ToArray());

		return sValue;
	}
	
	private string GetGameItemDataList(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		GameItemSeekCondition oCondition = new GameItemSeekCondition();

		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		if (oPartsArgs.Query.Get("item_seq_list").Equals(string.Empty)) {
			return string.Empty;
		}
		
		string[] ItemSeqArr = oPartsArgs.Query["item_seq_list"].ToString().Split('_');

		using (GameItem oGameItem = new GameItem()) {
			oTmpDataSet = oGameItem.GetGameItemDataList(oCondition,ItemSeqArr);
		}

		if (oPartsArgs.Query.Get("item_count_list") != null) {
			string[] ItemCountArr = oPartsArgs.Query["item_count_list"].ToString().Split('_');

			if (ItemCountArr.Length == oTmpDataSet.Tables[0].Rows.Count) {
				oTmpDataSet.Tables[0].Columns.Add(new DataColumn("GAME_ITEM_COUNT",System.Type.GetType("System.String")));
				for (int i = 0;i < ItemCountArr.Length;i++) {
					oTmpDataSet.Tables[0].Rows[i]["GAME_ITEM_COUNT"] = ItemCountArr[i];
				}
			}
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);

		return sValue;
	}

	private string GetListPossessionGameItem(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);
		oCondition.TrapFlg = ViCommConst.FLAG_ON_STR;
		System.Data.DataSet oTmpDataSet = null;
		using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			oTmpDataSet = oPossessionGameItem.GetPossessionItem(oCondition);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		string iPrevSeekMode = this.parseWoman.sessionObjs.seekMode;
		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			this.parseWoman.sessionObjs.seekMode = oCondition.SeekMode.ToString();
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekMode = iPrevSeekMode;
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetPossessionGameItemQuantityListBox(string pTag,string pArgument) {
		StringBuilder sValueBuilder = new StringBuilder("");
		string sValue = string.Empty;

		string[] arrText = pArgument.Split(',');

		if (arrText.Length > 0) {
			int iPossessionCount = 0;
			int.TryParse(pArgument,out iPossessionCount);

			int iMaxQuantity = 0;

			int.TryParse(iPossessionCount.ToString(),out iMaxQuantity);

			PwViCommConst oPwViCommConst = new PwViCommConst();

			if (iMaxQuantity > 0) {
				sValueBuilder.AppendLine("<select name=\"possessionItemNum\">");

				foreach (int Quantity in oPwViCommConst.BUY_ITEM_QUANTITY_LIST) {
					if (iMaxQuantity >= Quantity) {
						sValueBuilder.AppendLine("<option value=" + Quantity + ">" + Quantity + "</option>");
					} else {
						break;
					}
				}

				sValueBuilder.AppendLine("</select>");
			}
			sValue = sValueBuilder.ToString();

		}
		return sValue;
	}

	private bool CheckCanBuyGameItem(string sGameItemStageLevel,string sGameItemGetCd) {
		int iGameItemStageLevel;

		int.TryParse(sGameItemStageLevel,out iGameItemStageLevel);

		if (
			iGameItemStageLevel <= parseWoman.sessionWoman.userWoman.CurCharacter.gameCharacter.stageLevel &&
			!string.IsNullOrEmpty(sGameItemGetCd)
		) {
			return true;
		} else {
			return false;
		}
	}

	private string GetCompCastTreasureGetItem(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		CastTreasureSeekCondition oCondition = new CastTreasureSeekCondition(oPartsArgs.Query);
		
		if (string.IsNullOrEmpty(oCondition.StageGroupType)) {
			return string.Empty;
		}
		
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;

		DataSet oTmpDataSet = null;
		CastTreasure oCastTreasure = new CastTreasure();
		oTmpDataSet = oCastTreasure.GetCompCastTreasureGetItem(oCondition);

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_ITEM);
	}

	private string GetLoginBonusDayGamePoint(string pTag,string pArgument) {
		string sValue = string.Empty;

		using (GameLoginBonusDaily oGameLoginBonusDaily = new GameLoginBonusDaily()) {
			sValue = oGameLoginBonusDaily.GetLoginBonusDayGamePoint(this.parseWoman.sessionWoman.site.siteCd,ViCommConst.OPERATOR);
		}

		return sValue;
	}

	private string GetGameLotteryNotCompFreeFlag(string pTag,string pArgument) {
		string sValue = string.Empty;

		if (string.IsNullOrEmpty(pArgument)) {
			return string.Empty;
		}

		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		string sSexCd = ViCommConst.OPERATOR;
		string sLotterySeq = pArgument;

		using (Lottery oLottery = new Lottery()) {
			sValue = oLottery.GetLotteryNotCompFreeFlag(sSiteCd,sUserSeq,sUserCharNo,sSexCd,sLotterySeq);
		}

		return sValue;
	}

	private string GetClearTownUsedItemBuyPlural(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		ClearTownUsedItemSeekCondition oCondition = new ClearTownUsedItemSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
		oCondition.TotalFlag = ViCommConst.FLAG_ON_STR;

		using (ClearTownUsedItem oClearTownUsedItem = new ClearTownUsedItem()) {
			oTmpDataSet = oClearTownUsedItem.GetPageCollectionUsedItemShortage(oCondition);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);

		return sValue;
	}

	private bool CheckExistPresentGameItem(string sGameItemSeq) {
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		//男性側のｱｲﾃﾑとして検索
		string sSexCd = ViCommConst.MAN;

		DataSet ds;

		using (GameItem oGameItem = new GameItem()) {
			ds = oGameItem.GetOnePresentGameItemByPresentGameItemSeq(sSiteCd,sSexCd,sGameItemSeq);
		}

		if (ds.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}

	private bool CheckExistWantedGameItem(string sGameItemSeq) {
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		DataSet ds;

		using (WantedItem oWantedItem = new WantedItem()) {
			ds = oWantedItem.GetOneByGameItemSeq(sSiteCd,sUserSeq,sUserCharNo,sGameItemSeq);
		}

		if (ds.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}

	private string GetGameItemPossessionCount(string pTag,string pArgument) {
		if (string.IsNullOrEmpty(pArgument)) {
			return string.Empty;
		}

		string sValue = string.Empty;

		PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.ItemSeq = pArgument;

		using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			sValue = oPossessionGameItem.GetGameItemPossessionCount(oCondition);
		}

		return sValue;
	}

	private string GetSelfOnceDayPresentDoneFlag(string pTag,string pArgument) {
		string sValue = string.Empty;

		using (OnceDayPresent oOnceDayPresent = new OnceDayPresent()) {
			sValue = oOnceDayPresent.GetOnceDayPresentDoneFlag(
				this.parseWoman.sessionWoman.site.siteCd,
				this.parseWoman.sessionWoman.userWoman.userSeq,
				this.parseWoman.sessionWoman.userWoman.curCharNo
			);
		}

		return sValue;
	}

	private bool CheckExistPossessionGameItem(string pTag,string pArgument) {
		bool bOk = false;
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			bOk = oPossessionGameItem.CheckExistPossessionItem(sSiteCd,sUserSeq,sUserCharNo);
		}

		return bOk;
	}

	private string GetItemDataByCategory(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		GameItemSeekCondition oCondition = new GameItemSeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;

		using (GameItem oGameItem = new GameItem()) {
			oTmpDataSet = oGameItem.GetGameItemByCategoryType(oCondition,1,1);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetGameTreasureCompItem(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		CastTreasureSeekCondition oCondition = new CastTreasureSeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;

		using (CastTreasure oCastTreasure = new CastTreasure()) {
			oTmpDataSet = oCastTreasure.GetCompCastTreasureGetItem(oCondition);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetGameBattleCompItem(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		CastTreasureSeekCondition oCondition = new CastTreasureSeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (CastTreasure oCastTreasure = new CastTreasure()) {
			oTmpDataSet = oCastTreasure.GetCompCastTreasureGetItemByBattleLogSeq(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseWoman.sessionObjs.seekConditionEx;
		try {
			this.parseWoman.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		} finally {
			this.parseWoman.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetGameIntroItemByIntroCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sSexCd = ViCommConst.OPERATOR;

		using (GameIntroItem oGameIntroItem = new GameIntroItem()) {
			oTmpDataSet = oGameIntroItem.GetItemListByIntroCount(sSiteCd,sSexCd,1,oPartsArgs.NeedCount);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);
		
		return sValue;
	}

	private string GetLotteryGetRareItem(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
		oCondition.RareItemFlag = ViCommConst.FLAG_ON_STR;

		using (LotteryGetItem oLotteryGetItem = new LotteryGetItem()) {
			oTmpDataSet = oLotteryGetItem.GetListAddGetFlag(oCondition,1,oPartsArgs.NeedCount);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_LOTTERY);

		return sValue;
	}

	private string GetPickUpLotteryGetItemNotComp(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.PickupFlag = ViCommConst.FLAG_ON_STR;

		using (LotteryGetItem oLotteryGetItem = new LotteryGetItem()) {
			oTmpDataSet = oLotteryGetItem.GetListNotComp(oCondition);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_ITEM);

		return sValue;
	}
	
	private string GetSelfFreePresentRestCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iValue = 0;

		using (OnceDayPresent oOnceDayPresent = new OnceDayPresent()) {
			iValue = oOnceDayPresent.GetRestFreePresentDoneCount(
				this.parseWoman.sessionWoman.site.siteCd,
				this.parseWoman.sessionWoman.userWoman.userSeq,
				this.parseWoman.sessionWoman.userWoman.curCharNo
			);
			
			sValue = iValue.ToString();
		}

		return sValue;
	}
}