﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseWoman {
	private string ParsePartTimeJobPlan(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		string lQuery = string.Empty;

		switch (pTag) {
			case "$PART_TIME_JOB_SEQ":
				this.parseWoman.SetFieldValue(pTag,"PART_TIME_JOB_SEQ",out sValue);
				break;
			case "$PART_TIME_JOB_NM":
				this.parseWoman.SetFieldValue(pTag,"PART_TIME_JOB_NM",out sValue);
				break;
			case "$PART_TIME_JOB_INCOME":
				this.parseWoman.SetFieldValue(pTag,"INCOME",out sValue);
				break;
			case "$PART_TIME_JOB_WAITING_MIN":
				this.parseWoman.SetFieldValue(pTag,"WAITING_MIN",out sValue);
				break;
			case "$REC_NO_PER_PAGE":
				this.parseWoman.SetFieldValue(pTag,"REC_NO_PER_PAGE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetListPartTimeJobPlan(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		PartTimeJobPlanSeekCondition oCondition = new PartTimeJobPlanSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		DataSet oDataSet = null;
		using (PartTimeJobPlan oPartTimeJobPlan = new PartTimeJobPlan()) {
			oDataSet = oPartTimeJobPlan.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}
		if (oDataSet == null) {
			return string.Empty;
		}
		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_PART_TIME_JOB_PLAN);
	}
}
