﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseAdminReport(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$ADMIN_REPORT_DATE":
				SetFieldValue(pTag,"ADMIN_REPORT_DATE",out sValue);
				break;
			case "$ADMIN_REPORT_DOC":
				sValue = GetHtmlDocFull(pTag);
				break;
			case "$ADMIN_REPORT_TITLE":
				SetFieldValue(pTag,"DOC_TITLE",out sValue);
				break;
			case "$QUERY_VIEW_ADMIN_REPORT":
				sValue = GetQueryViewAdminReport(pTag);
				break;
			default:
				pParsed = false;
				break;

		}
		return sValue;
	}

	private string GetQueryViewAdminReport(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,DOC_SEQ",out sValues)) {
			return string.Format("pageno={0}&docseq={1}",sValues[0],sValues[1]);
		} else {
			return "";
		}
	}

}
