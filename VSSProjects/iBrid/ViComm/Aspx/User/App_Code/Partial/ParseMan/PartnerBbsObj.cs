﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private static readonly Regex regexCount = new Regex(@"\((\d*?)\)",RegexOptions.Compiled);

	private string ParseCastBbsObj(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		string sObjKind = "";

		switch (pTag) {
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_IMG":
				sValue = GetCastFavoritImg(pTag,pArgument);
				break;
			case "$CAST_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$CAST_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_MONITOR_TALK_OK_FLAG":
				sValue = CanMonitorTalk(pTag);
				break;
			case "$CAST_MONITOR_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,IsMonitorVoiceEnable(pTag));
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$CAST_OBJ_DOC":
				SetFieldValue(pTag,"OBJ_DOC",out sValue);
				break;
			case "$CAST_OBJ_TITLE":
				SetFieldValue(pTag,"OBJ_TITLE",out sValue);
				break;
			case "$CAST_OBJ_UPLOAD_DATE":
				SetFieldValue(pTag,"OBJ_UPLOAD_DATE",out sValue);
				break;
			case "$CAST_OBJ_ATTR_TYPE_NM":
				SetFieldValue(pTag,"CAST_OBJ_ATTR_TYPE_NM",out sValue);
				break;
			case "$CAST_OBJ_ATTR_NM":
				SetFieldValue(pTag,"CAST_OBJ_ATTR_NM",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_FLAG":
				SetFieldValue(pTag,"PROFILE_MOVIE_FLAG",out sValue);
				break;
			case "$CAST_PROFILE_PIC_COUNT":
				SetFieldValue(pTag,"PROFILE_PIC_COUNT",out sValue);
				break;
			case "$CAST_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$CAST_BBS_PIC_FLAG":
				SetFieldValue(pTag,"BBS_PIC_FLAG",out sValue);
				break;
			case "$CAST_BBS_MOVIE_FLAG":
				SetFieldValue(pTag,"BBS_MOVIE_FLAG",out sValue);
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag, "MOBILE_CONNECT_TYPE_NM", out sValue);
				break;
			case "$CAST_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag, "FAVORIT_FLAG", out sValue);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				SetFieldValue(pTag,"MONITOR_ENABLE_FLAG",out sValue);
				break;
			case "$DIV_IS_PIC":
				if (GetValue(pTag,"OBJ_KIND",out sObjKind)) {
					SetNoneDisplay(!(sObjKind.Equals(ViCommConst.BbsObjType.PIC)));
				}
				break;
			case "$DIV_IS_MOVIE":
				if (GetValue(pTag,"OBJ_KIND",out sObjKind)) {
					SetNoneDisplay(!(sObjKind.Equals(ViCommConst.BbsObjType.MOVIE)));
				}
				break;
			case "$DIV_IS_BBS_DOC":
				if (GetValue(pTag,"OBJ_KIND",out sObjKind)) {
					SetNoneDisplay(!(sObjKind.Equals(ViCommConst.BbsObjType.BBSDOC)));
				}
				break;
			case "$DIV_IS_PICKUP":
			case "$CAST_IS_PICKUP":
				sValue = IsPickup(pTag,pArgument).ToString();
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$QUERY_VIEW_BBS_OBJ":
				sValue = GetQueryViewBbsObj(pTag);
				break;
			case "$HREF_MONITOR_TALK":
			case "$HREF_MONITOR_TALK2":
				sValue = GetHrefMonitorTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_VOICE":
				sValue = GetHrefMonitorVoice(pTag,pArgument);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$CAST_PROFILE_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag,ViCommConst.ATTACHED_PROFILE.ToString());
				break;
			case "$CAST_BBS_PIC_COUNT":
				sValue = this.GetPicCount(pTag,ViCommConst.ATTACHED_BBS.ToString());
				break;
			case "$CAST_BBS_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag,ViCommConst.ATTACHED_BBS.ToString());
				break;
			case "$CAST_BBS_ATTR_PIC_COUNT":
				sValue = this.GetPicCountByAttr(pTag,pArgument);
				break;
			case "$CAST_BBS_ATTR_MOVIE_COUNT":
				sValue = this.GetMovieCountByAttr(pTag,pArgument);
				break;

			//PICのみ使用可能-------------------------------------------------------------------
			case "$CAST_PIC_SMALL_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag,"OBJ_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_SMALL_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag, "OBJ_SMALL_BLUR_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$OBJ_SMALL_BLUR_PHOTO_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_DOWNLOAD_FLAG":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_FLAG",out sValue);
				break;

			//MOVIEのみ使用可能-------------------------------------------------------------------
			case "$CAST_MOVIE_IMG_PATH":
				SetFieldValue(pTag,"THUMBNAIL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_MOVIE_CHARGE_POINT":
				SetFieldValue(pTag,"CHARGE_POINT",out sValue);
				break;
			case "$CAST_MOVIE_DOWNLOAD_DATE":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_DATE",out sValue);
				break;
			case "$CAST_MOVIE_DOWNLOAD_FLAG":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_FLAG",out sValue);
				break;
			case "$CAST_MOVIE_NEW_FLAG":
				sValue = GetNewMovieFlag(pTag).ToString();
				break;
			case "$HREF_CAST_MOVIE_DOWNLOAD":
				if (sessionMan.logined) {
					sValue = GetHrefMovieDownload(pTag,pArgument);
				} else {
					using (ManageCompany oManageCompany = new ManageCompany()) {
						if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
							sValue = string.Format("<a href=\"RegistUserRequestByTermId.aspx\">{0}</a>",pArgument);
						} else {
							sValue = string.Format("<a href=\"RegistUserRequestByTermIdCast.aspx\">{0}</a>",pArgument);
						}
					}
				}
				break;
			case "$QUERY_CAST_MOVIE_DOWNLOAD_CHECK":
				sValue = this.GetQueryCastMovieDownloadCheck(pTag,pArgument);
				break;
			case "$CAST_COMMENT_DETAIL":
				SetFieldValue(pTag,"COMMENT_DETAIL",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_LIST":
				SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$SELECT_OBJ_COUNT":
				Match rgxMatch = regexCount.Match(iBridUtil.GetStringValue(sessionMan.requestQuery.Params["lstGenreSelect"]));
				sValue = rgxMatch.Value;
				break;
			case "$OBJ_SEQ":
				SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;

			// 閲覧履歴
			case "$SELF_USED_FLAG":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_FLAG",out sValue);
				break;
			case "$SELF_USED_DATE":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_DATE",out sValue);
				break;
			case "$SELF_USED_SPECIAL_FREE_FLAG":
				SetFieldValue(pTag,"SELF_USED_SPECIAL_FREE_FLAG",out sValue);
				break;

			// レビュー
			case "$SELF_REVIEW_FLAG":
				sValue = this.GetSelfBbsObjReviewFlag(pTag,pArgument);
				break;
			case "$REVIEW_COUNT":
				SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = GetGoodStarAvrX10(pTag);
				break;

			// お宝ブックマーク
			case "$BOOKMARK_FLAG":
				SetFieldValue(pTag,"BOOKMARK_FLAG",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryViewBbsObj(string pTag) {
		string sValue;
		string sQuery;
		SetFieldValue(pTag,"OBJ_KIND",out sValue);
		if (sValue.Equals(ViCommConst.BbsObjType.PIC)) {
			sQuery = GetQueryViewBbsPic(pTag);
		} else if (sValue.Equals(ViCommConst.BbsObjType.MOVIE)) {
			sQuery = GetQueryViewBbsMovie(pTag);
		} else if (sValue.Equals(ViCommConst.BbsObjType.BBSDOC)) {
			sQuery = GetQueryViewBbsDoc(pTag);
		} else {
			sQuery = string.Empty;
		}
		if (iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["allbbs"]).Equals("1")) {
			sQuery += "&allbbs=1";
		}
		return sQuery;
	}

	public string GetGoodStarAvr(string pTag) {
		double dGoodStarAvr = 0;
		string sGoodStarTotal,sReviewCount;
		float fGoodStarTotal,fReviewCount;

		SetFieldValue(pTag,"GOOD_STAR_TOTAL",out sGoodStarTotal);
		SetFieldValue(pTag,"REVIEW_COUNT",out sReviewCount);

		if (float.TryParse(sGoodStarTotal,out fGoodStarTotal) && float.TryParse(sReviewCount,out fReviewCount)) {
			if (fGoodStarTotal > 0 && fReviewCount > 0) {
				dGoodStarAvr = fGoodStarTotal / fReviewCount;
				dGoodStarAvr = iBridUtil.RoundOff(dGoodStarAvr,1);
			}
		}

		return dGoodStarAvr.ToString();
	}

	public string GetGoodStarAvrX10(string pTag) {
		float fResult = 0;
		float fAvr;
		if (float.TryParse(GetGoodStarAvr(pTag),out fAvr)) {
			fResult = fAvr * 10;
		}
		return fResult.ToString();
	}

	public bool IsExistSelfObjReviewHistory(string pObjSeq) {
		bool bExist = false;

		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			int iReviewCount = oObjReviewHistory.GetRecCount(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				pObjSeq
			);

			if (iReviewCount > 0) {
				bExist = true;
			}
		}

		return bExist;
	}

	private string GetSelfBbsObjReviewFlag(string pTag,string pArgument) {
		string sValue = ViCommConst.FLAG_OFF_STR;
		string sObjSeq;

		if (sessionMan.logined) {
			if (GetValue(pTag,"OBJ_SEQ",out sObjSeq)) {
				if (IsExistSelfObjReviewHistory(sObjSeq)) {
					sValue = ViCommConst.FLAG_ON_STR;
				}
			}
		}

		return sValue;
	}
}

