﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 選挙ランキング検索結果
--	Progaram ID		: ElectionEntry
--  Creation Date	: 2013.12.17
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

partial class PwParseMan {
	private string ParseElectionEntry(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_VOTE_RANK":
				this.parseMan.SetFieldValue(pTag,"VOTE_RANK",out sValue);
				break;
			case "$CAST_VOTE_COUNT":
				this.parseMan.SetFieldValue(pTag,"VOTE_COUNT",out sValue);
				break;
			case "$FIRST_PERIOD_START_DATE":
				this.parseMan.SetFieldValue(pTag,"FIRST_PERIOD_START_DATE",out sValue);
				break;
			case "$FIRST_PERIOD_END_DATE":
				this.parseMan.SetFieldValue(pTag,"FIRST_PERIOD_END_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_START_DATE":
				this.parseMan.SetFieldValue(pTag,"SECOND_PERIOD_START_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_END_DATE":
				this.parseMan.SetFieldValue(pTag,"SECOND_PERIOD_END_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_FLAG":
				this.parseMan.SetFieldValue(pTag,"SECOND_PERIOD_FLAG",out sValue);
				break;
			case "$DIV_IS_ELECTION_VOTABLE":
				this.parseMan.SetNoneDisplay(!this.CheckElectionVotable(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_ELECTION_VOTABLE":
				this.parseMan.SetNoneDisplay(this.CheckElectionVotable(pTag,pArgument));
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private bool CheckElectionVotable(string pTag,string pArgument) {
		bool bValue = false;
		string[] sValues;
		if (this.parseMan.GetValues(pTag,"FIRST_PERIOD_START_DATE,FIRST_PERIOD_END_DATE,SECOND_PERIOD_START_DATE,SECOND_PERIOD_END_DATE,SECOND_PERIOD_FLAG",out sValues)) {
			DateTime oFirstPeriodStartDate = DateTime.Parse(sValues[0]);
			DateTime oFirstPeriodEndDate = DateTime.Parse(sValues[1]);
			DateTime oSecondPeriodStartDate = DateTime.Parse(sValues[2]);
			DateTime oSecondPeriodEndDate = DateTime.Parse(sValues[3]);
			string sSecondPeriodFlag = sValues[4];

			if (oFirstPeriodStartDate <= DateTime.Now && oFirstPeriodEndDate >= DateTime.Now) {
				bValue = true;
			} else if (oSecondPeriodStartDate <= DateTime.Now && oSecondPeriodEndDate >= DateTime.Now && sSecondPeriodFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				bValue = true;
			} else {
				bValue = false;
			}
		}
		return bValue;
	}
}