﻿using System;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseCastVoice(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_SALES_VOICE_CHARGE_POINT":
				SetFieldValue(pTag,"CHARGE_POINT",out sValue);
				break;
			case "$CAST_SALES_VOICE_DOWNLOAD_FLAG":
				SetFieldValue(pTag,"DOWNLOAD_FLAG",out sValue);
				break;
			case "$CAST_SALES_VOICE_NEW_FLAG":
				sValue = GetNewVoiceFlag(pTag);
				break;
			case "$CAST_SALES_VOICE_TITLE":
				SetFieldValue(pTag,"VOICE_TITLE",out sValue);
				break;
			case "$DIV_IS_PICKUP":
			case "$CAST_IS_PICKUP":
				sValue = IsPickup(pTag, pArgument).ToString();
				break;								
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$QUERY_SALES_VOICE":
				sValue = GetQuerySalesVoice(pTag);
				break;
			case "$QUERY_VIEW_SALES_VOICE":
				sValue = GetQueryViewSalesVoice(pTag);
				break;
			case "$HREF_CAST_VOICE_DOWNLOAD": 
				sValue = GetHrefCastVoiceDownload(pTag,pArgument);
				break;			
			default:
				pParsed = false;
				break;
		}
		/*
			case "$HREF_CONFIRM_SALE_VOICE_PURCHASE": {
					string sCastVoiceSeq = dataRow[ViCommConst.MAN_IDX_CAST_SALE_VOICE]["VOICE_SEQ"].ToString();

					UrlBuilder obuUrlBuilder = new UrlBuilder("ConfirmSaleVoice.aspx");
					obuUrlBuilder.Parameters.Add("voiceseq",sCastVoiceSeq);
					obuUrlBuilder.Parameters.Add("action","purchase");
					string sHref = obuUrlBuilder.ToString();

					sResult = string.Format("<a href=\"{0}\">{1}</a>",sHref,sText);
				}
				break; 
		 */
		return sValue;
	}

	private string GetNewVoiceFlag(string pTag) {
		DateTime dtValue;
		string sValue = ViCommConst.FLAG_OFF.ToString();
		if (GetValue(pTag,"PUBLISH_DATE",out dtValue)) {
			string sCastVoiceIsNew = ViCommConst.FLAG_OFF.ToString();
			DateTime? oCastVoicePublishDate = dtValue as DateTime?;
			if (oCastVoicePublishDate != null) {
				DateTime oVoiceNewBoundaryDate = DateTime.Now.Date.AddDays(-1 * sessionMan.site.voiceNewDays);
				sCastVoiceIsNew = (oVoiceNewBoundaryDate.CompareTo(oCastVoicePublishDate.Value.Date) <= 0) ? ViCommConst.FLAG_ON.ToString() : ViCommConst.FLAG_OFF.ToString();
			}
		}
		return sValue;
	}
	
	private string GetQuerySalesVoice(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"VOICE_SEQ",out sValues)) {
			return string.Format("voiceseq={0}",sValues[0]);
		} else {
			return "";
		}
	}

	private string GetQueryViewSalesVoice(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"LOGIN_ID,CRYPT_VALUE",out sValues)) {
			return string.Format("loginid={0}&{1}={2}",sValues[0],sessionMan.site.charNoItemNm,sValues[1]);
		} else {
			return "";
		}
	}

	private string GetHrefCastVoiceDownload(string pTag, string pLabel) {
		
		string sVoiceSeq = null;
		string sSiteCd = null;
		string sLoginId = null;

		bool bHasValue = true;
		bHasValue = bHasValue && GetValue(pTag, "VOICE_SEQ", out sVoiceSeq);
		bHasValue = bHasValue && GetValue(pTag, "SITE_CD", out sSiteCd);
		bHasValue = bHasValue && GetValue(pTag, "LOGIN_ID", out sLoginId);
		if(!bHasValue) return string.Empty;
		
		VoiceHelper oVoiceHelper = new VoiceHelper();
		if(!oVoiceHelper.IsAlreadyUsed(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.ATTACHED_VOICE.ToString(),sVoiceSeq)){
			// 購入済出ない場合は購入確認画面のへのリンクを生成する
			UrlBuilder oConfirmUrlBuilder = new UrlBuilder("ConfirmSaleVoice.aspx");
			oConfirmUrlBuilder.Parameters.Add("voiceseq", sVoiceSeq);
			return string.Format("<a href=\"{0}\">{1}</a>", oConfirmUrlBuilder.ToString(), pLabel);
		}else{
			return oVoiceHelper.GetDownloadTag(this.sessionMan,sSiteCd,sLoginId,sVoiceSeq,pLabel);
		}
	}

}
