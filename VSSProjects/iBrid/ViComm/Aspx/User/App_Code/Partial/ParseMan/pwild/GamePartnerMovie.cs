﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class ParseMan {
    private string ParseGamePartnerMovie(string pTag, string pArgument, out bool pParsed) {
        pParsed = true;
        string sValue = string.Empty;

        switch (pTag) {
			case "$SITE_CD":
				SetFieldValue(pTag,"SITE_CD",out sValue);
				break;
			case "$CAST_USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$CAST_USER_CHAR_NO":
				SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_GAME_HANDLE_NM":
				SetFieldValue(pTag,"GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_ITEM_PRICE":
				sValue = GetGameItemValue("PRICE");
				break;
			case "$FRIENDLY_RANK":
				SetFieldValue(pTag,"FRIENDLY_RANK",out sValue);
				break;
			case "$CAST_USER_MOVIE_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;
			case "$GAME_ITEM_POSSESSION_COUNT":
				SetFieldValue(pTag,"GAME_ITEM_POSSESSION_COUNT",out sValue);
				break;
            default:
                pParsed = false;
                break;
        }

        return sValue;
    }

	private string GetGameItemValue(string pItemDistinction) {
		string sValue = string.Empty;

		using (GameItem oGameItem = new GameItem()) {
			sValue = oGameItem.GetGameItem(sessionMan.userMan.siteCd,sessionMan.sexCd,PwViCommConst.GameItemCategory.ITEM_CATEGORY_TICKET,pItemDistinction);
		}
		
		return sValue;
	}
}