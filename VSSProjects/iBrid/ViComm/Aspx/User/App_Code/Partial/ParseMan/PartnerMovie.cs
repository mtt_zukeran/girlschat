﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseCastMovie(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_BBS_OBJ_FLAG":
				sValue = GetBbsObjFlag(pTag);
				break;
			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_IMG":
				sValue = GetCastFavoritImg(pTag,pArgument);
				break;
			case "$CAST_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$CAST_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_MONITOR_TALK_OK_FLAG":
				sValue = CanMonitorTalk(pTag);
				break;
			case "$CAST_MONITOR_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,IsMonitorVoiceEnable(pTag));
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag,"MOBILE_CONNECT_TYPE_NM",out sValue);
				break;
			case "$CAST_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_LAST_ACTION_DATE":
			case "$CAST_LAST_LOGIN_DATE":
				SetFieldValue(pTag,"LAST_ACTION_DATE",out sValue);
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;			
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_MOVIE_ATTR_TYPE_NM":
				SetFieldValue(pTag,"CAST_MOVIE_ATTR_TYPE_NM",out sValue);
				break;
			case "$CAST_MOVIE_ATTR_NM":
				SetFieldValue(pTag,"CAST_MOVIE_ATTR_NM",out sValue);
				break;
			case "$CAST_MOVIE_CHARGE_POINT":
				SetFieldValue(pTag,"CHARGE_POINT",out sValue);
				break;
			case "$CAST_MOVIE_DOC":
				SetFieldValue(pTag,"MOVIE_DOC",out sValue);
				break;
			case "$CAST_MOVIE_DOWNLOAD_DATE":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_DATE",out sValue);
				break;
			case "$CAST_MOVIE_DOWNLOAD_FLAG":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_FLAG",out sValue);
				break;
			case "$CAST_MOVIE_IMG_PATH":
				SetFieldValue(pTag,"THUMBNAIL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_MOVIE_NEW_FLAG":
				sValue = GetNewMovieFlag(pTag).ToString();
				break;
			case "$CAST_MOVIE_PLAY_TIME":
				SetFieldValue(pTag,"PLAY_TIME",out sValue);
				break;
			case "$CAST_MOVIE_SAMPLE_FLAG":
				sValue = IsNotEqual(pTag,"SAMPLE_MOVIE_SEQ","");
				break;
			case "$CAST_MOVIE_SERIES":
				SetFieldValue(pTag,"MOVIE_SERIES_NM",out sValue);
				break;
			case "$CAST_MOVIE_TITLE":
				SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$CAST_MOVIE_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_FLAG":
				SetFieldValue(pTag,"PROFILE_MOVIE_FLAG",out sValue);
				break;
			case "$CAST_PROFILE_PIC_COUNT":
				SetFieldValue(pTag,"PROFILE_PIC_COUNT",out sValue);
				break;
			case "$CAST_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$CAST_BBS_PIC_FLAG":
				SetFieldValue(pTag,"BBS_PIC_FLAG",out sValue);
				break;
			case "$CAST_BBS_MOVIE_FLAG":
				SetFieldValue(pTag,"BBS_MOVIE_FLAG",out sValue);
				break;
			case "$CAST_PICKUP_START_PUB_DAY":
				SetFieldValue(pTag, "PICKUP_START_PUB_DAY", out sValue);
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				SetFieldValue(pTag,"MONITOR_ENABLE_FLAG",out sValue);
				break;
			case "$CAST_BBS_ATTR_PIC_COUNT":
				sValue = this.GetPicCountByAttr(pTag,pArgument);
				break;
			case "$CAST_BBS_ATTR_MOVIE_COUNT":
				sValue = this.GetMovieCountByAttr(pTag,pArgument);
				break;
			case "$DIV_IS_PICKUP":
			case "$CAST_IS_PICKUP":
				sValue = IsPickup(pTag, pArgument).ToString();
				break;								
			case "$HREF_CAST_MOVIE_DOWNLOAD":
				if (sessionMan.logined) {
					sValue = GetHrefMovieDownload(pTag,pArgument);
				} else {
					using (ManageCompany oManageCompany = new ManageCompany()) {
						if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
							sValue = string.Format("<a href=\"RegistUserRequestByTermId.aspx\">{0}</a>",pArgument.Split(new char[]{','})[0]);
						}
						else {
							sValue = string.Format("<a href=\"RegistUserRequestByTermIdCast.aspx\">{0}</a>", pArgument.Split(new char[] { ',' })[0]);
						}
					}
				}
				break;
			case "$HREF_CAST_MOVIE_DOWNLOAD_FREE":
				sValue = GetHrefMovieDownload(pTag,pArgument);
				break;
			case "$HREF_SALES_SAMPLE_MOVIE_DOWNLOAD":
				sValue = GetHrefSalesSampleMovieDownload(pTag,pArgument);
				break;
			case "$HREF_MONITOR_TALK":
			case "$HREF_MONITOR_TALK2":
				sValue = GetHrefMonitorTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_VOICE":
				sValue = GetHrefMonitorVoice(pTag,pArgument);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$QUERY_PURCHASE_MOVIE":
				sValue = GetQueryPurchaseMoive();
				break;
			case "$QUERY_SALES_MOVIE_SERIES":
				sValue = GetQuerySaleMovieSeries(pTag);
				break;
			case "$QUERY_SORT_ASC_MOVIE":
				sValue = string.Format("sort={0}&movieseries={1}","1",sessionMan.requestQuery["movieseries"]);
				break;
			case "$QUERY_SORT_DESC_MOVIE":
				sValue = string.Format("sort={0}&movieseries={1}","2",sessionMan.requestQuery["movieseries"]);
				break;
			case "$QUERY_VIEW_BBS_MOVIE":
				sValue = GetQueryViewBbsMovie(pTag);
				break;
			case "$QUERY_VIEW_GAME_MOVIE":
				sValue = GetQueryViewGameMovie(pTag);
				break;
			case "$QUERY_VIEW_SALES_MOVIE":
				sValue = GetQueryViewSaleMovie(pTag);
				break;
			case "$QUERY_VIEW_SALES_MOVIE_DOWNLOAD":
				sValue = GetQueryViewSalesMovie(pTag,"d");
				break;
			case "$QUERY_VIEW_SALES_MOVIE_STREAMING":
				sValue = GetQueryViewSalesMovie(pTag, "s");
				break;
			case "$QUERY_CAST_MOVIE_DOWNLOAD_CHECK":
				sValue = this.GetQueryCastMovieDownloadCheck(pTag,pArgument);
				break;
			case "$CAST_COMMENT_DETAIL":
				SetFieldValue(pTag,"COMMENT_DETAIL",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_LIST":
				SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_MOVIE_READING_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_SEQ":
				SetFieldValue(pTag,"PROFILE_MOVIE_SEQ",out sValue);
				break;
			case "$CAST_USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$CAST_CHAR_NO":
				SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			// PICKUPのみ
			case "$CAST_PICKUP_OBJ_COMMENT":
				SetFieldValue(pTag,"PICKUP_OBJ_COMMENT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_PROFILE_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag, ViCommConst.ATTACHED_PROFILE.ToString());
				break;
			case "$CAST_BBS_PIC_COUNT":
				sValue = this.GetPicCount(pTag, ViCommConst.ATTACHED_BBS.ToString());
				break;
			case "$CAST_BBS_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag, ViCommConst.ATTACHED_BBS.ToString());
				break;

			case "$BLOG_SEQ":
				SetFieldValue(pTag, "BLOG_SEQ", out sValue);
				break;
			case "$OBJ_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;

			// 閲覧履歴
			case "$SELF_USED_FLAG":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_FLAG",out sValue);
				break;
			case "$SELF_USED_DATE":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_DATE",out sValue);
				break;
			case "$SELF_USED_SPECIAL_FREE_FLAG":
				SetFieldValue(pTag,"SELF_USED_SPECIAL_FREE_FLAG",out sValue);
				break;

			// レビュー
			case "$SELF_REVIEW_FLAG":
				sValue = this.GetSelfBbsMovieReviewFlag(pTag,pArgument);
				break;
			case "$REVIEW_COUNT":
				SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = GetGoodStarAvrX10(pTag);
				break;

			// お宝ブックマーク
			case "$BOOKMARK_FLAG":
				SetFieldValue(pTag,"BOOKMARK_FLAG",out sValue);
				break;

			// ファンクラブ
			case "$SELF_FANCLUB_MEMBER_RANK":
				string[] sValues;
				if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubMemberRank(sValues[0],sValues[1]);
				}
				break;
			// イイネ
			case "$CAST_MOVIE_LIKE_COUNT":
				SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$SELF_CAST_MOVIE_LIKED":
				sValue = GetSelfCastMovieLiked(pTag,pArgument);
				break;
			// 自分を除くいいね数
			case "$CAST_MOVIE_LIKE_COUNT_OTHER":
				if (ViCommConst.FLAG_OFF_STR.Equals(GetSelfCastMovieLiked(pTag,pArgument))) {
					// 自分がいいねしていない動画
					SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				} else {
					// 自分がいいねした動画
					string sCnt = string.Empty;
					GetValue(pTag,"LIKE_COUNT",out sCnt);
					sValue = string.Format("{0}",int.Parse(sCnt) - 1);
				}
				break;
			// コメント
			case "$CAST_MOVIE_COMMENT_COUNT":
				SetFieldValue(pTag,"CAST_MOVIE_COMMENT_COUNT",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetHrefSalesSampleMovieDownload(string pTag,string pLabel) {
		string[] sValues;
		if (GetValues(pTag,"SAMPLE_MOVIE_SEQ,RNUM,USER_SEQ,USER_CHAR_NO,LOGIN_ID,SAMPLE_MOVIE_SEQ,CRYPT_VALUE",out sValues) == false) {
			return string.Join("",sValues);
		}
		if (!sValues[0].Equals("")) {
			return ParseDownloadUrl(sValues[1], sValues[2], sValues[3], sValues[4], sValues[5], pLabel, sValues[6], true);
		} else {
			return "";
		}
	}

	private string GetQuerySaleMovieSeries(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"MOVIE_SERIES",out sValues)) {
			return string.Format("movieseries={0}",sValues[0]);
		} else {
			return "";
		}
	}

	private string GetQueryViewBbsMovie(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,LOGIN_ID,CRYPT_VALUE,MOVIE_SEQ",out sValues)) {
			return string.Format("userrecno={0}&loginid={1}&category={2}&{3}={4}&attrtypeseq={5}&attrseq={6}&seekmode={7}&objseq={8}&used={9}&unused={10}&ranktype={11}&bkm={12}",
										sValues[0],sValues[1],sessionMan.currentCategoryIndex,sessionMan.site.charNoItemNm,sValues[2],
										iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["attrtypeseq"]),
										iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["attrseq"]),
										sessionMan.seekMode,
										sValues[3],
										iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["used"]),
										iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["unused"]),
										iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["ranktype"]),
										iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["bkm"]));
		} else {
			return "";
		}
	}

	private string GetQueryViewGameMovie(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,LOGIN_ID,CRYPT_VALUE,MOVIE_SEQ",out sValues)) {
			return string.Format("userrecno={0}&loginid={1}&category={2}&{3}={4}&seekmode={5}&objseq={6}",
										sValues[0],
										sValues[1],
										sessionMan.currentCategoryIndex,
										sessionMan.site.charNoItemNm,
										sValues[2],
										sessionMan.seekMode,
										sValues[3]);
		} else {
			return "";
		}
	}

	private string GetQueryViewSaleMovie(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM",out sValues)) {
			return string.Format("userrecno={0}&loginid={1}&category={2}&{3}={4}&sort={5}&movieseries={6}&seekmode={7}",
									sValues[0],
									sessionMan.requestQuery.QueryString["loginid"],
									sessionMan.currentCategoryIndex,
									sessionMan.site.charNoItemNm,
									sessionMan.requestQuery.QueryString[sessionMan.site.charNoItemNm],
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["sort"]),
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["movieseries"]),
									sessionMan.seekMode);
		} else {
			return "";
		}
	}

	private string GetQueryPurchaseMoive() {
		return string.Format("action=purchase&direct=1&filenm={0}&castseq={1}&movieseq={2}&usetype={3}",
							iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["filenm"]),
							iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["castseq"]),
							iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["movieseq"]),
							iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["usetype"]));
	}

	private string GetQueryViewSalesMovie(string pTag,string pUseType) {
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,MOVIE_SEQ",out sValues)) {
			return string.Format("castseq={0}&movieseq={1}&usetype={2}&direct=1",sValues[0],sValues[1],pUseType);
		} else {
			return "";
		}
	}

	private string GetHrefMovieDownload(string pTag,string pArgument) {
		string[] sValues;
		string sMovieSeq;
		string sErrNoDisplayFlag = ViCommConst.FLAG_OFF_STR;
		if (GetValues(pTag,"RNUM,USER_SEQ,USER_CHAR_NO,LOGIN_ID,PROFILE_MOVIE_SEQ,CRYPT_VALUE",out sValues) == false) {
			return "";
		}
		sMovieSeq = sValues[4];
		
		string[] oArgsArray = pArgument.Split(',');
		string sLabel = oArgsArray[0];
		string sDownloadPage = string.Empty;
		if (oArgsArray.Length > 1) {
			sDownloadPage = oArgsArray[1];
		}
		if (oArgsArray.Length > 2) {
			sErrNoDisplayFlag = oArgsArray[2];
		}

		// DataSetにMOVIE_TYPEが存在する場合、Profile Movie以外と判定する 
		// 本来はHREF_QUERYを分割するべき 
		if (IsExistField(pTag,"MOVIE_TYPE")) {
			if (!GetValue(pTag,"MOVIE_SEQ",out sMovieSeq)){
				return "";
			}
		}
		string sUrl = ParseDownloadUrl(sValues[0], sValues[1], sValues[2], sValues[3], sMovieSeq, sLabel, string.Format("{0}={1}", sessionMan.site.charNoItemNm, sValues[5]), false, sDownloadPage);

		if (sErrNoDisplayFlag.Equals(ViCommConst.FLAG_ON_STR) && sUrl.Contains("(<font color=\"#ff0000\">")) {
			return "";
		} else {
			return sUrl;
		}
	}

	private int GetNewMovieFlag(string pTag) {
		DateTime dtCreate;
		if (GetValue(pTag,"UPLOAD_DATE",out dtCreate)) {
			if (DateTime.Now.AddHours(sessionMan.site.profileMovieNewHours * -1) <= dtCreate) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	private string GetQueryCastMovieDownloadCheck(string pTag,string pArgument){
		string[] oValues;

		if (!GetValues(pTag, "USER_SEQ,MOVIE_SEQ", out oValues)) {
			return string.Empty;
		}

		return string.Format("{0}&castseq={1}&movieseq={2}&check=1", GetReQuery(pTag, pArgument), oValues[0], oValues[1]);
	}

	private string GetSelfCastMovieLiked(string pTag,string pArgument) {
		string sValue = ViCommConst.FLAG_OFF_STR;
		string sMovieSeq = string.Empty;

		if (sessionMan.logined) {
			if (GetValue(pTag,"MOVIE_SEQ",out sMovieSeq)) {
				using (CastMovieLike oCastMovieLike = new CastMovieLike()) {
					sValue = oCastMovieLike.IsExist(sessionMan.site.siteCd,sMovieSeq,sessionMan.userMan.userSeq);
				}
			}
		}

		return sValue;
	}

	private string GetSelfBbsMovieReviewFlag(string pTag,string pArgument) {
		string sValue = ViCommConst.FLAG_OFF_STR;
		string sObjSeq;

		if (sessionMan.logined) {
			if (GetValue(pTag,"MOVIE_SEQ",out sObjSeq)) {
				if (IsExistSelfObjReviewHistory(sObjSeq)) {
					sValue = ViCommConst.FLAG_ON_STR;
				}
			}
		}

		return sValue;
	}
}
