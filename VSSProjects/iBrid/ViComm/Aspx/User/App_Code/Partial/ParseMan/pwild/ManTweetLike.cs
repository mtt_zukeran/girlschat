﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやきイイネ検索結果
--	Progaram ID		: ManTweetLike
--  Creation Date	: 2013.04.26
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

partial class PwParseMan {
	private string ParseManTweetLike(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_TWEET_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"MAN_TWEET_USER_SEQ",out sValue);
				break;
			case "$MAN_TWEET_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"MAN_TWEET_USER_CHAR_NO",out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.parseMan.GetQueryCastProfile(pTag);
				break;
			case "$CAST_LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}