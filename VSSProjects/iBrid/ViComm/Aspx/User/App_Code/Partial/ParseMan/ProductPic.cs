﻿using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

partial class ParseMan {

	private string ParseProductPic(string pTag, string pArgument, out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			// ｼﾞｬﾝﾙｾﾚｸﾄﾎﾞｯｸｽ
			case "$COMBO_PRODUCT_GENRE":
				// p1: 商品種別 p2: ｶﾃｺﾞﾘ表示ﾌﾗｸﾞ(1=>ｶﾃｺﾞﾘ-ｼﾞｬﾝﾙ)
				sValue = GetComboProductPicGenre(pTag, pArgument);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetComboProductPicGenre(string pTag, string pArgument) {
		// 引数解析


		string[] oArgArr = pArgument.Split(',');

		string sAdultFlag = ParseArg(oArgArr, 0, "");
		int iCateFlg = ParseArg(oArgArr, 1, 0);

		string sProductType = ProductHelper.GetPicProductType(sAdultFlag);
		if (sProductType.Equals("") == true) {
			return "";
		}

		//コンボボックス作成
		StringBuilder oBuilder = new StringBuilder();
		using (ProductGenre oProdGenRel = new ProductGenre()) {
			using (DataSet ds = oProdGenRel.GetListByProductType(sessionMan.site.siteCd, sProductType)) {

				oBuilder.Append("<select name='narrow_genre'>");
				oBuilder.Append("<option value='$DECODE(:,$QUERY_PARM_DECODE(narrow_genre);,@selected@);:' selected>");
				oBuilder.Append("--ｼﾞｬﾝﾙ--");
				oBuilder.Append("</option>");

				foreach (DataRow dr in ds.Tables[0].Rows) {
					string sComboNm = "";
					string sGenreCodes = string.Format("{0}-{1}", dr["PRODUCT_GENRE_CATEGORY_CD"].ToString(), dr["PRODUCT_GENRE_CD"].ToString());
					if (iCateFlg == ViCommConst.FLAG_ON) {
						sComboNm = string.Format("{0}-{1}", dr["PRODUCT_GENRE_CATEGORY_NM"].ToString(), dr["PRODUCT_GENRE_NM"].ToString());
					} else {
						sComboNm = dr["PRODUCT_GENRE_NM"].ToString();
					}

					oBuilder.AppendFormat("<option value='$DECODE({0},$QUERY_PARM_DECODE(narrow_genre);,@selected@);{0}'>", sGenreCodes);
					oBuilder.Append(sComboNm);
					oBuilder.Append("</option>");

				}
				oBuilder.Append("</select>");

			}
		}


		return parseContainer.Parse(oBuilder.ToString());

	}

	
	
}
