﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せ 報酬
--	Progaram ID		: WantedBounty
--  Creation Date	: 2013.06.13
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseWantedBounty(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$WANTED_STAMP_COMPLETE_COUNT":
				this.parseMan.SetFieldValue(pTag,"COMPLETE_COUNT",out sValue);
				break;
			case "$WANTED_TOTAL_COMP_CNT":
				this.parseMan.SetFieldValue(pTag,"TOTAL_COMP_CNT",out sValue);
				break;
			case "$WANTED_MONTHLY_BOUNTY_POINT":
				this.parseMan.SetFieldValue(pTag,"WANTED_MONTHLY_BOUNTY_POINT",out sValue);
				break;
			case "$WANTED_MONTHLY_BOUNTY_POINT_PER_SHEET":
				this.parseMan.SetFieldValue(pTag,"BOUNTY_POINT_PER_SHEET",out sValue);
				break;
			case "$SELF_WANTED_MONTHLY_BOUNTY_POINT":
				this.parseMan.SetFieldValue(pTag,"SELF_BOUNTY_POINT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}