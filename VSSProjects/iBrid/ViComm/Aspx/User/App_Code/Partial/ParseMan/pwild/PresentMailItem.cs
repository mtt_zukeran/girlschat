﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プレゼントメールアイテム検索結果
--	Progaram ID		: PresentMailItem
--  Creation Date	: 2014.12.03
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

partial class PwParseMan {
	private string ParsePresentMailItem(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$PRESENT_MAIL_ITEM_SEQ":
				this.parseMan.SetFieldValue(pTag,"PRESENT_MAIL_ITEM_SEQ",out sValue);
				break;
			case "$PRESENT_MAIL_ITEM_NM":
				this.parseMan.SetFieldValue(pTag,"PRESENT_MAIL_ITEM_NM",out sValue);
				break;
			case "$CHARGE_POINT":
				this.parseMan.SetFieldValue(pTag,"CHARGE_POINT",out sValue);
				break;
			case "$PRESENT_MAIL_TERM_SEQ":
				this.parseMan.SetFieldValue(pTag,"PRESENT_MAIL_TERM_SEQ",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}