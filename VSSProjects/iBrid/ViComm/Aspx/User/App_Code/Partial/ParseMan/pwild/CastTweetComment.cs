﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルつぶやきｺﾒﾝﾄ検索結果
--	Progaram ID		: CastTweet
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

partial class PwParseMan {
	private string ParseCastTweetComment(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$MAN_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_TWEET_COMMENT_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_COMMENT_SEQ",out sValue);
				break;
			case "$CAST_TWEET_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_SEQ",out sValue);
				break;
			case "$COMMENT_TEXT":
				this.parseMan.SetFieldValue(pTag,"COMMENT_TEXT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$COMMENT_DATE":
				this.parseMan.SetFieldValue(pTag,"COMMENT_DATE",out sValue);
				break;
			case "$CAST_TWEET_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_USER_SEQ",out sValue);
				break;
			case "$CAST_TWEET_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_USER_CHAR_NO",out sValue);
				break;
			case "$MAN_NG_SKULL_COUNT":
				this.parseMan.SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}