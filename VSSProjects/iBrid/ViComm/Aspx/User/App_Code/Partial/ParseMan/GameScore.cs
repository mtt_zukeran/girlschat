﻿using System;
using System.Text;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

partial class ParseMan {

	private string ParseGameScore(string pTag, string pArgument, out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$GAME_SCORE": {
					SetFieldValue(pTag, "GAME_SCORE", out sValue);
				}
				break;
			case "$GAME_RANK": {
					SetFieldValue(pTag, "GAME_RANK", out sValue);
				}
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
	private string GetListGameScore(string pTag, string pArgument) {

		string[] oArgArr = pArgument.Split(',');
		string sHtmlTemplate = oArgArr[0];	// パースしない

		int iNeedCount = ParseArg(oArgArr, 1, 3);
		
		NameValueCollection oQuery = new NameValueCollection();
		for (int i = 2; i < oArgArr.Length; i++) {
			
			string[] sQueryItem = ParseArg(new string[]{oArgArr[i]},0,string.Empty).Split(new char[] { '=' }, 2);
			string sKey = sQueryItem[0];
			string sValue = string.Empty;
			if (sQueryItem.Length == 2) {
				sValue = sQueryItem[1];
			}
			oQuery.Add(sKey, sValue);
		}

		GameScoreSeekCondition oCondition = new GameScoreSeekCondition(oQuery);
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_GAME_SCORE;
		oCondition.SiteCd = sessionMan.site.siteCd;		
		oCondition.SexCd = sessionMan.sexCd;

		DataSet oTmpDataSet = null;
		using (GameScore oGameScore = new GameScore()) {
			oTmpDataSet = oGameScore.GetPageCollection(oCondition, 1, iNeedCount);
		}


		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(sHtmlTemplate, oTmpDataSet, ViCommConst.DATASET_EX_GAME_SCORE);
	}

}
