﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseTalkMovie(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_COMMENT_LIST":
				SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_TALK_MOVIE_DOC":
				SetFieldValue(pTag,"MOVIE_DOC",out sValue);
				break;
			case "$CAST_TALK_MOVIE_PART_TITLE":
				SetFieldValue(pTag,"PART_TITLE",out sValue);
				break;
			case "$CAST_TALK_MOVIE_SEQ":
				SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;
			case "$CAST_TALK_MOVIE_TITLE":
				SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$CAST_TALK_MOVIE_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$QUERY_PERSONAL_TALK_MOVIE":
				sValue = GetQueryPersonalTalkMovie(pTag);
				break;
			case "$QUERY_TALK_MOVIE":
				sValue = GetQueryTalkMovie(pTag);
				break;
			case "$QUERY_TALK_MOVIE_PART":
				sValue = GetQueryTalkMoviePart(pTag);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
	private string GetQueryTalkMovie(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ACT_CATEGORY_IDX,LOGIN_ID,RNUM,MOVIE_SEQ,CRYPT_VALUE",out sValues)) {
			return string.Format("seekmode={0}&category={1}&loginid={2}&userrecno={3}&movieseq={4}&{5}={6}",
									sessionMan.seekMode,sValues[0],sValues[1],sValues[2],sValues[3],sessionMan.site.charNoItemNm,sValues[4]);
		} else {
			return "";
		}
	}

	private string GetQueryTalkMoviePart(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ACT_CATEGORY_IDX,USER_SEQ,MOVIE_SEQ,MOVIE_PART_NO,CRYPT_VALUE",out sValues)) {
			return string.Format("category={0}&request={1}&castseq={2}&chgtype={3}&movieseq={4}&moviepart={5}&{6}={7}",
									sValues[0],ViCommConst.REQUEST_DIALIN_PLAY,sValues[0],ViCommConst.CHARGE_PLAY_MOVIE,sValues[1],sValues[2],sessionMan.site.charNoItemNm,sValues[3]);
		} else {
			return "";
		}
	}

	private string GetQueryPersonalTalkMovie(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ACT_CATEGORY_IDX,LOGIN_ID,RNUM,MOVIE_SEQ,CRYPT_VALUE",out sValues)) {
			return string.Format("seekmode={0}&category={1}&loginid={2}&userrecno={3}&movieseq={4}&{5}={6}",
								sessionMan.seekMode,sValues[0],sValues[1],sValues[2],sValues[3],sessionMan.site.charNoItemNm,sValues[4]);
		} else {
			return "";
		}
	}
}
