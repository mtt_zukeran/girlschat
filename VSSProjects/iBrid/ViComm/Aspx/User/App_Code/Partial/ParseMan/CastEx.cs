﻿using System;
using iBridCommLib;
using ViComm;
using System.Text;
using System.Globalization;
using System.Data;
using ViComm.Extension.Pwild;

partial class ParseMan {
	private string ParseCastEx(string pTag, string pArgument, out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		string[] sValues;

		switch (pTag) {
			case "$BLOG_SEQ":
				SetFieldValue(pTag, "BLOG_SEQ", out sValue);
				break;
			case "$BLOG_TITLE":
				SetFieldValue(pTag, "BLOG_TITLE", out sValue);
				break;
			case "$BLOG_DOC":
				SetFieldValue(pTag, "BLOG_DOC", out sValue);
				break;
			case "$DIV_IS_INVESTIGATE_TARGET_CAST": {
					SetNoneDisplay(!IsInvestigateTargetCast(pTag));
				}
				break;
			case "$DIV_IS_NOT_INVESTIGATE_TARGET_CAST": {
					SetNoneDisplay(IsInvestigateTargetCast(pTag));
				}
				break;
			case "$DIV_IS_INVESTIGATE_CAUGHT": {
					SetNoneDisplay(!IsInvestigateCaught(pTag));
				}
				break;
			case "$DIV_IS_NOT_INVESTIGATE_CAUGHT": {
					SetNoneDisplay(IsInvestigateCaught(pTag));
				}
				break;
			case "$RANK_NO":
				SetFieldValue(pTag,"RANK_NO",out sValue);
				break;
			case "$ROWNUM":
				SetFieldValue(pTag,"RNUM",out sValue);
				break;
			case "$PAYMENT_POINT":
				SetFieldValue(pTag,"PAYMENT_POINT",out sValue);
				break;
			case "$SELF_FANCLUB_ADMISSION_ENABLE":
				if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubAdmissionEnable(sValues[0],sValues[1]);
				}
				break;
			case "$SELF_FANCLUB_MEMBER_RANK":
				if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubMemberRank(sValues[0],sValues[1]);
				}
				break;
			case "$CAST_NEWS_TYPE":
				SetFieldValue(pTag,"NEWS_TYPE",out sValue);
				break;
			case "$CAST_NEWS_DATE":
				SetFieldValue(pTag,"NEWS_DATE",out sValue);
				break;
			case "$RANK":
				SetFieldValue(pTag,"RANK",out sValue);
				break;
			case "$SELF_FANCLUB_MEMBER_RANK_WITHOUT_SP":
				if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubMemberRankWithoutSpPremium(sValues[0],sValues[1]);
				}
				break;
			case "$FAVORIT_REGIST_DATE":
				SetFieldValue(pTag,"REGIST_DATE",out sValue);
				break;
			//ファンクラブ
			case "$NEXT_SETTLE_DATE":
				SetFieldValue(pTag,"NEXT_SETTLE_DATE",out sValue);
				break;
			case "$EXPIRE_DATE":
				SetFieldValue(pTag,"EXPIRE_DATE",out sValue);
				break;
			case "$MEMBER_RANK":
				SetFieldValue(pTag,"MEMBER_RANK",out sValue);
				break;
			case "$MEMBER_RANK_EXPIRE_DATE":
				SetFieldValue(pTag,"MEMBER_RANK_EXPIRE_DATE",out sValue);
				break;
			case "$COIN_COUNT":
				SetFieldValue(pTag,"COIN_COUNT",out sValue);
				break;
			case "$NEED_COIN_COUNT_NOW_RANK":
				sValue = this.GetNeedCoinCountNowRank(pTag,pArgument);
				break;
			case "$NEED_COIN_COUNT_NEXT_RANK":
				sValue = this.GetNeedCoinCountNextRank(pTag,pArgument);
				break;
			case "$SETTLE_NG_FLAG":
				SetFieldValue(pTag,"SETTLE_NG_FLAG",out sValue);
				break;
			case "$PAYMENT_TYPE":
				SetFieldValue(pTag,"PAYMENT_TYPE",out sValue);
				break;
			//新人のアイドル
			case "$START_PERFORM_DAY":
				SetFieldValue(pTag,"START_PERFORM_DAY",out sValue);
				break;
			//野球拳
			case "$CAST_YAKYUKEN_GAME_COUNT":
				sValue = GetCastYakyukenGameCount(pTag,pArgument);
				break;
			//この娘を探せ
			case "$STAMP_SHEET_NO":
				SetFieldValue(pTag,"STAMP_SHEET_NO",out sValue);
				break;
			case "$STAMP_SHEET_AVAILABLE_FLAG":
				SetFieldValue(pTag,"STAMP_SHEET_AVAILABLE_FLAG",out sValue);
				break;
			case "$NOT_DISPLAY_FLAG":
				SetFieldValue(pTag,"NOT_DISPLAY_FLAG",out sValue);
				break;
			//投票順位
			case "$CAST_VOTE_RANK":
				SetFieldValue(pTag,"VOTE_RANK",out sValue);
				break;
			case "$CAST_VOTE_COUNT":
				SetFieldValue(pTag,"VOTE_COUNT",out sValue);
				break;
			//投票設定
			case "$ELECTION_PERIOD_SEQ":
				SetFieldValue(pTag,"ELECTION_PERIOD_SEQ",out sValue);
				break;
			case "$ELECTION_PERIOD_NM":
				SetFieldValue(pTag,"ELECTION_PERIOD_NM",out sValue);
				break;
			case "$VOTE_END_DATE":
				SetFieldValue(pTag,"VOTE_END_DATE",out sValue);
				break;
			case "$VOTE_TICKET_POINT":
				SetFieldValue(pTag,"TICKET_POINT",out sValue);
				break;
			case "$VOTE_FIX_RANK_FLAG":
				SetFieldValue(pTag,"FIX_RANK_FLAG",out sValue);
				break;
			case "$VOTE_FIX_RANK_DATE":
				SetFieldValue(pTag,"FIX_RANK_DATE",out sValue);
				break;
			case "$FIRST_PERIOD_START_DATE":
				SetFieldValue(pTag,"FIRST_PERIOD_START_DATE",out sValue);
				break;
			case "$FIRST_PERIOD_END_DATE":
				SetFieldValue(pTag,"FIRST_PERIOD_END_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_START_DATE":
				SetFieldValue(pTag,"SECOND_PERIOD_START_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_END_DATE":
				SetFieldValue(pTag,"SECOND_PERIOD_END_DATE",out sValue);
				break;
			case "$SECOND_PERIOD_FLAG":
				SetFieldValue(pTag,"SECOND_PERIOD_FLAG",out sValue);
				break;
			case "$DIV_IS_ELECTION_VOTABLE":
				SetNoneDisplay(!this.CheckElectionVotable(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_ELECTION_VOTABLE":
				SetNoneDisplay(this.CheckElectionVotable(pTag,pArgument));
				break;
			case "$CAST_BLOG_ARTICLE_COUNT":
				sValue = this.GetCastBlogArticleCount(pTag);
				break;
			case "$CAST_POST_NEWS_PARTS":
				sValue = this.GetCastPostNews(pTag,pArgument);
				break;
			case "$CAST_GALLERY_BBS_PIC_PARTS":
				sValue = this.GetCastGalleryBbsPic(pTag,pArgument);
				break;
			case "$CAST_PROFILE_PIC_EXIST_FLAG":
				sValue = this.GetCastProfilePicExistFlag(pTag);
				break;
			//メールおねだり
			case "$REST_MAIL_REQUEST_COUNT":
				SetFieldValue(pTag,"REST_MAIL_REQUEST_COUNT",out sValue);
				break;
			case "$MAIL_REQUEST_LIMIT_COUNT":
				SetFieldValue(pTag,"MAIL_REQUEST_LIMIT_COUNT",out sValue);
				break;
			case "$MAIL_REQUEST_CONFIRM_STATUS":
				SetFieldValue(pTag,"MAIL_REQUEST_CONFIRM_STATUS",out sValue);
				break;
			//無料動画
			case "$CAST_MOVIE_IMG_PATH":
				SetFieldValue(pTag,"THUMBNAIL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			//LINE風メール
			case "$CHAT_MAIL_PARTS":
				sValue = this.GetChatMailPartsByCast(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetNeedCoinCountNowRank(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"COIN_COUNT,MEMBER_RANK",out sValues)) {
			int iNeedCount = 0;
			int iCoinCount = int.Parse(sValues[0]);

			switch (sValues[1]) {
				case "2":
					iNeedCount = 100 - iCoinCount;
					break;
				case "3":
					iNeedCount = 200 - iCoinCount;
					break;
				case "4":
					iNeedCount = 350 - iCoinCount;
					break;
				default:
					break;
			}

			sValue = iNeedCount.ToString();
		}

		return sValue;
	}

	private string GetNeedCoinCountNextRank(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"COIN_COUNT,MEMBER_RANK",out sValues)) {
			int iNeedCount = 0;
			int iCoinCount = int.Parse(sValues[0]);

			switch (sValues[1]) {
				case "1":
					iNeedCount = 100 - iCoinCount;
					break;
				case "2":
					iNeedCount = 200 - iCoinCount;
					break;
				case "3":
					iNeedCount = 350 - iCoinCount;
					break;
				default:
					break;
			}

			sValue = iNeedCount.ToString();
		}

		return sValue;
	}

	private string GetCastYakyukenGameCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		string[] sValues;

		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			YakyukenGameCastSeekCondition oCondition = new YakyukenGameCastSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.CastUserSeq = sValues[0];
			oCondition.CastCharNo = sValues[1];

			using (YakyukenGameCast oYakyukenGameCast = new YakyukenGameCast()) {
				oYakyukenGameCast.GetPageCount(oCondition,1,out dRecCount);
			}
		}

		return dRecCount.ToString();
	}

	private bool CheckElectionVotable(string pTag,string pArgument) {
		bool bValue = false;
		string[] sValues;
		if (GetValues(pTag,"FIRST_PERIOD_START_DATE,FIRST_PERIOD_END_DATE,SECOND_PERIOD_START_DATE,SECOND_PERIOD_END_DATE,SECOND_PERIOD_FLAG",out sValues)) {
			DateTime oFirstPeriodStartDate = DateTime.Parse(sValues[0]);
			DateTime oFirstPeriodEndDate = DateTime.Parse(sValues[1]);
			DateTime oSecondPeriodStartDate = DateTime.Parse(sValues[2]);
			DateTime oSecondPeriodEndDate = DateTime.Parse(sValues[3]);
			string sSecondPeriodFlag = sValues[4];
			
			if (oFirstPeriodStartDate <= DateTime.Now && oFirstPeriodEndDate >= DateTime.Now) {
				bValue = true;
			} else if (oSecondPeriodStartDate <= DateTime.Now && oSecondPeriodEndDate >= DateTime.Now && sSecondPeriodFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				bValue = true;
			} else {
				bValue = false;
			}
		}
		return bValue;
	}

	private string GetCastBlogArticleCount(string pTag) {
		decimal dRecCount = 0;
		string[] sValues;

		if (sessionObjs.IsImpersonated) {
			if (!EnabledBlog()) {
				return "0";
			}
		}

		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO,ENABLED_BLOG_FLAG,BLOG_NON_PUBLISH_FLAG",out sValues)) {
			string sUserSeq = sValues[0];
			string sUserCharNo = sValues[1];
			string sEnabledBlogFlag = sValues[2];
			string sBlogNonPublishFlag = sValues[3];

			if (sEnabledBlogFlag.Equals(ViCommConst.FLAG_ON_STR) && sBlogNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				BlogArticleSeekCondition oCondition = new BlogArticleSeekCondition();
				oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE;
				oCondition.SiteCd = sessionMan.site.siteCd;
				oCondition.UserSeq = sUserSeq;
				oCondition.UserCharNo = sUserCharNo;
				oCondition.BlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;

				using (BlogArticle oBlogArticle = new BlogArticle(sessionObjs)) {
					oBlogArticle.GetPageCount(oCondition,1,out dRecCount);
				}
			}
		}

		return dRecCount.ToString();
	}

	private string GetCastPostNews(string pTag,string pArgument) {
		string[] sValues;
		DataSet dsTemp = null;
		DataSet dsCastNews = null;

		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO,BBS_NON_PUBLISH_FLAG,BLOG_NON_PUBLISH_FLAG,DIARY_NON_PUBLISH_FLAG,PROFILE_PIC_NON_PUBLISH_FLAG",out sValues)) {
			string sUserSeq = sValues[0];
			string sUserCharNo = sValues[1];
			string sBbsNonPublishFlag = sValues[2];
			string sBlogNonPublishFlag = sValues[3];
			string sDiaryNonPublishFlag = sValues[4];
			string sProfilePicNonPublishFlag = sValues[5];

			DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);
			CastPostNewsSeekCondition oCondition = new CastPostNewsSeekCondition(oPartsArgs.Query);
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sUserSeq;
			oCondition.UserCharNo = sUserCharNo;

			using (CastPostNews oCastPostNews = new CastPostNews()) {
				dsTemp = oCastPostNews.GetPageCollection(oCondition,1,999);
			}

			dsCastNews = dsTemp.Clone();

			foreach (DataRow dr in dsTemp.Tables[0].Rows) {
				string sNewsType = iBridUtil.GetStringValue(dr["NEWS_TYPE"]);

				switch (sNewsType) {
					case PwViCommConst.CastNewsType.NEWS_TYPE_BBS_PIC:
					case PwViCommConst.CastNewsType.NEWS_TYPE_BBS_MOV:
					case PwViCommConst.CastNewsType.NEWS_TYPE_BBS:
						if (sBbsNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
							dsCastNews.Tables[0].ImportRow(dr);
						}
						break;
					case PwViCommConst.CastNewsType.NEWS_TYPE_BLOG:
						if (sBlogNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
							dsCastNews.Tables[0].ImportRow(dr);
						}
						break;
					case PwViCommConst.CastNewsType.NEWS_TYPE_FREE_PIC:
						if (sProfilePicNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
							dsCastNews.Tables[0].ImportRow(dr);
						}
						break;
					case PwViCommConst.CastNewsType.NEWS_TYPE_DIARY:
						if (sDiaryNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
							dsCastNews.Tables[0].ImportRow(dr);
						}
						break;
					default:
						dsCastNews.Tables[0].ImportRow(dr);
						break;
				}

				if (dsCastNews.Tables[0].Rows.Count >= oPartsArgs.NeedCount) {
					break;
				}
			}

			return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,dsCastNews,ViCommConst.DATASET_CAST);
		}

		return string.Empty;
	}

	private string GetCastGalleryBbsPic(string pTag,string pArgument) {
		string[] sValues;
		DataSet dsTemp = null;
		DataSet dsCastPic = null;

		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO,BBS_NON_PUBLISH_FLAG,PROFILE_PIC_NON_PUBLISH_FLAG",out sValues)) {
			string sUserSeq = sValues[0];
			string sUserCharNo = sValues[1];
			string sBbsNonPublishFlag = sValues[2];
			string sProfilePicNonPublishFlag = sValues[3];

			DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);
			CastPicSeekCondition oCondition = new CastPicSeekCondition(oPartsArgs.Query);
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sUserSeq;
			oCondition.UserCharNo = sUserCharNo;
			oCondition.PicTypeList.Add(ViCommConst.ATTACHED_PROFILE.ToString());
			oCondition.PicTypeList.Add(ViCommConst.ATTACHED_BBS.ToString());
			oCondition.ProfilePicFlag = ViCommConst.FLAG_OFF_STR;
			oCondition.ObjNaFlag = ViCommConst.FLAG_OFF_STR;
			oCondition.ObjNotApproveFlag = ViCommConst.FLAG_OFF_STR;
			oCondition.ObjNotPublishFlag = ViCommConst.FLAG_OFF_STR;
			oCondition.IsAppendObjUsedHistory = true;

			using (CastPic oCastPic = new CastPic(sessionObjs)) {
				dsTemp = oCastPic.GetPageCollectionNew(oCondition,1,99);
			}

			dsCastPic = dsTemp.Clone();

			foreach (DataRow dr in dsTemp.Tables[0].Rows) {
				string sPicType = iBridUtil.GetStringValue(dr["PIC_TYPE"]);

				if (sPicType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
					if (sBbsNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						dsCastPic.Tables[0].ImportRow(dr);
					}
				} else if (sPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
					if (sProfilePicNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						dsCastPic.Tables[0].ImportRow(dr);
					}
				}

				if (dsCastPic.Tables[0].Rows.Count >= oPartsArgs.NeedCount) {
					break;
				}
			}

			return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,dsCastPic,ViCommConst.DATASET_PARTNER_PIC);
		}

		return string.Empty;
	}

	private string GetCastProfilePicExistFlag(string pTag) {
		string[] sValues;

		if (GetValues(pTag,"PROFILE_PIC_FLAG,PROFILE_PIC_NON_PUBLISH_FLAG",out sValues)) {
			string sProfilePicFlag = sValues[0];
			string sProfilePicNonPublishFlag = sValues[1];

			if (sProfilePicFlag.Equals(ViCommConst.FLAG_ON_STR) && !sProfilePicNonPublishFlag.Equals("2")) {
				return ViCommConst.FLAG_ON_STR;
			} else {
				return ViCommConst.FLAG_OFF_STR;
			}
		}

		return string.Empty;
	}

	private bool IsInvestigateTargetCast(string pTag) {
		bool bValue = false;
		using (InvestigateTarget oInvestigateTarget = new InvestigateTarget()) {
			string[] sValues;
			if (GetValues(pTag,"SITE_CD,USER_SEQ,USER_CHAR_NO",out sValues)) {
				InvestigateTargetSeekCondition oCondition = new InvestigateTargetSeekCondition();
				oCondition.SiteCd = sValues[0];
				oCondition.UserSeq = sValues[1];
				oCondition.UserCharNo = sValues[2];
				oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
				bValue = oInvestigateTarget.IsExistInvestigateTarget(oCondition,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo);
			}
		}

		return bValue;
	}
}
