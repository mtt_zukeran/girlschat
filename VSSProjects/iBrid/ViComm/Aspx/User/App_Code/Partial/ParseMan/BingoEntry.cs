﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseBingoEntry(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$BINGO_APPLICATION_DATE":
				SetFieldValue(pTag,"BINGO_APPLICATION_DATE",out sValue);
				break;
			case "$BINGO_ELECTION_AMT":
				SetFieldValue(pTag,"BINGO_ELECTION_AMT",out sValue);
				break;
			case "$BINGO_ELECTION_POINT":
				SetFieldValue(pTag,"BINGO_ELECTION_POINT",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$BINGO_BALL_FLAG":
				SetFieldValue(pTag,"BINGO_BALL_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
}
