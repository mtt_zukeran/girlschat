﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログコメント検索結果
--	Progaram ID		: BlogComment
--  Creation Date	: 2012.12.29
--  Creater			: PW Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

partial class PwParseMan {
	private string ParseBlogComment(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$COMMENT_DOC":
				this.parseMan.SetFieldValue(pTag,"COMMENT_DOC",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CREATE_DATE":
				this.parseMan.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$BLOG_ARTICLE_SEQ":
				this.parseMan.SetFieldValue(pTag,"BLOG_ARTICLE_SEQ",out sValue);
				break;
			case "$MAN_NG_SKULL_COUNT":
				this.parseMan.SetFieldValue(pTag,"NG_SKULL_COUNT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}