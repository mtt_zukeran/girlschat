﻿using System;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseAspAd(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		NameValueCollection oQeuryList = new NameValueCollection(sessionMan.requestQuery.QueryString);

		switch (pTag) {
			case "$ASP_AD_START_DAY":
				SetFieldValue(pTag,"APPLY_START_DAY",out sValue);
				sValue = sValue.Substring(5,5);
				break;
			case "$ASP_AD_CD":
				SetFieldValue(pTag,"ASP_AD_CD",out sValue);
				break;
			case "$ASP_AD_NM":
				SetFieldValue(pTag,"AD_NM",out sValue);
				break;
			case "$ASP_AD_SETTLE_COMPANY_CD":
				SetFieldValue(pTag,"SETTLE_COMPANY_CD",out sValue);
				break;
			case "$ASP_AD_DESCRIPTION":
				SetFieldValue(pTag,"AD_DESCRIPTION",out sValue);
				break;
			case "$ASP_AD_DESCRIPTION_SUB":
				SetFieldValue(pTag,"AD_DESCRIPTION_SUB",out sValue);
				break;
			case "$ASP_AD_MONTHLY_FEE":
				SetFieldValue(pTag,"MONTHLY_FEE",out sValue);
				break;
			case "$ASP_AD_AFFILIATE_POINT":
				SetFieldValue(pTag,"AFFILIATE_POINT",out sValue);
				break;
			case "$ASP_AD_TITLE":
				sValue = sessionMan.userMan.aspAdTitle;
				break;
			case "$RE_QUERY_ASP_AD_EX_SETTLED":
				sValue = GetRequeryAspAdSettle(oQeuryList,"1");
				break;
			case "$RE_QUERY_ASP_AD_IN_SETTLED":
				sValue = GetRequeryAspAdSettle(oQeuryList,"0");
				break;
			case "$RE_QUERY_ASP_AD_ORDER_BY_NEW":
				sValue = GetRequeryAspAdOrder(oQeuryList,"1");
				break;
			case "$RE_QUERY_ASP_AD_ORDER_BY_POINT":
				sValue = GetRequeryAspAdOrder(oQeuryList,"0");
				break;
			case "$HREF_ASP_AD_SETTLE_COMPANY":
				sValue = GetAdSettleCompany(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetRequeryAspAdSettle(NameValueCollection oQeuryList,string pExcludeSettled) {
		oQeuryList.Remove("excludesettled");
		oQeuryList.Remove("pageno");
		return ConstructQueryString(oQeuryList) + string.Format("&excludesettled={0}",pExcludeSettled);
	}

	private string GetRequeryAspAdOrder(NameValueCollection oQeuryList,string pOrder) {
		oQeuryList.Remove("neworder");
		oQeuryList.Remove("pageno");
		return ConstructQueryString(oQeuryList) + string.Format("&neworder={0}",pOrder);
	}

	private string GetAdSettleCompany(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ASP_AD_CD,SETTLE_COMPANY_CD",out sValues)) {
			return string.Format("PaymentPointAffiliate.aspx?aspadcd={0}&aspsettlecom={1}",sValues[0],sValues[1]);
		} else {
			return "";
		}
	}
}
