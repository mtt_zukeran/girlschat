﻿using System;
using System.Text;
using System.Data;
using iBridCommLib;
using ViComm;
using System.Collections.Specialized;
using System.Globalization;


partial class ParseMan {
	private string ParseWantedSchedule(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;


		switch (pTag) {
			case "$EXECUTION_DAY":
				GetValue(pTag,"EXECUTION_DAY",out sValue);
				break;
			case "$GOT_INVESTIGATE_ACQUIRED_POINT": {
				sValue = GetGotAcquiredPoint(pTag,pArgument);
			}
				break;

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private bool IsInvestigateEnter(string pTag) {
		bool bValue = false;
		using (InvestigateEntrant oInvestigateEntrant = new InvestigateEntrant()) {
			InvestigateEntrantSeekCondition oCondition = new InvestigateEntrantSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sessionMan.userMan.userSeq;
			oCondition.UserCharNo = sessionMan.userMan.userCharNo;
			oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
			System.Data.DataSet ds = oInvestigateEntrant.GetOne(oCondition);
			bValue = (ds.Tables[0].Rows.Count > 0);
		}

		return bValue;
	}

	public bool IsInvestigateAcquiredPoint(string pTag) {
		bool bValue = false;
		using (InvestigateEntrant oInvestigateEntrant = new InvestigateEntrant()) {
			InvestigateEntrantSeekCondition oCondition = new InvestigateEntrantSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sessionMan.userMan.userSeq;
			oCondition.UserCharNo = sessionMan.userMan.userCharNo;
			oCondition.ExecutionDay = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
			DataSet ds = oInvestigateEntrant.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				if (ds.Tables[0].Rows[0]["CAUGHT_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
					bValue = (ds.Tables[0].Rows[0]["POINT_ACQUIRED_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF_STR));
				}
			}
		}

		return bValue;
	}

	private bool IsInvestigateCaught(string pTag) {

		bool bValue = false;
		using (InvestigateEntrant oInvestigateEntrant = new InvestigateEntrant()) {
			InvestigateEntrantSeekCondition oCondition = new InvestigateEntrantSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sessionMan.userMan.userSeq;
			oCondition.UserCharNo = sessionMan.userMan.userCharNo;
			oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
			System.Data.DataSet ds = oInvestigateEntrant.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				bValue = ds.Tables[0].Rows[0]["CAUGHT_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR);
			}
		}

		return bValue;
	}
	
	private string GetGotAcquiredPoint(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		using (InvestigateEntrant oInvestigateEntrant = new InvestigateEntrant()) {
			InvestigateEntrantSeekCondition oCondition = new InvestigateEntrantSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sessionMan.userMan.userSeq;
			oCondition.UserCharNo = sessionMan.userMan.userCharNo;
			oCondition.ExecutionDay = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
			sValue = oInvestigateEntrant.GetGotAcquiredPoint(oCondition);
		}

		return sValue;
	}
}