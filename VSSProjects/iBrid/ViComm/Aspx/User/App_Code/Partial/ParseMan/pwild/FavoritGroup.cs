﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入りグループ検索結果
--	Progaram ID		: FavoritGroup
--  Creation Date	: 2012.10.02
--  Creater			: PW K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

partial class PwParseMan {
	private string ParseFavoritGroup(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$FAVORIT_GROUP_SEQ":
				this.parseMan.SetFieldValue(pTag,"FAVORIT_GROUP_SEQ",out sValue);
				break;
			case "$FAVORIT_GROUP_NM":
				this.parseMan.SetFieldValue(pTag,"FAVORIT_GROUP_NM",out sValue);
				break;
			case "$USER_COUNT":
				this.parseMan.SetFieldValue(pTag,"USER_COUNT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}