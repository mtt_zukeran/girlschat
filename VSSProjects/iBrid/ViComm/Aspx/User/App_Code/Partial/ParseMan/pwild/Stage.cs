﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseStage(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$GAME_STAGE_SEQ":
				this.parseMan.SetFieldValue(pTag,"STAGE_SEQ",out sValue);
				break;
			case "$GAME_STAGE_NM":
				this.parseMan.SetFieldValue(pTag,"STAGE_NM",out sValue);
				break;
			case "$GAME_STAGE_HONOR":
				this.parseMan.SetFieldValue(pTag,"HONOR",out sValue);
				break;
			case "$GAME_STAGE_GROUP_TYPE":
				this.parseMan.SetFieldValue(pTag,"STAGE_GROUP_TYPE",out sValue);
				break;
			case "$GAME_STAGE_GROUP_NM":
				this.parseMan.SetFieldValue(pTag,"STAGE_GROUP_NM",out sValue);
				break;
			case "$GAME_STAGE_CLEAR_FLAG":
				this.parseMan.SetFieldValue(pTag,"STAGE_CLEAR_FLAG",out sValue);
				break;
			case "$GAME_STAGE_MAN_LIMIT_STAGE_SEQ":
				this.parseMan.SetFieldValue(pTag,"MAN_LIMIT_STAGE_SEQ",out sValue);
				break;
			case "$GAME_TOWN_COUNT":
				this.parseMan.SetFieldValue(pTag,"TOWN_COUNT",out sValue);
				break;
			case "$GAME_TOWN_CLEAR_COUNT":
				this.parseMan.SetFieldValue(pTag,"TOWN_CLEAR_COUNT",out sValue);
				break;
			case "$GAME_STAGE_BOSS_NM":
				this.parseMan.SetFieldValue(pTag,"BOSS_NM",out sValue);
				break;
			case "$GAME_STAGE_BOSS_DESCRIPTION":
				this.parseMan.SetFieldValue(pTag,"BOSS_DESCRIPTION",out sValue);
				break;
			case "$GAME_STAGE_EXP":
				this.parseMan.SetFieldValue(pTag,"EXP",out sValue);
				break;
			case "$GAME_NEXT_STAGE_NM":
				this.parseMan.SetFieldValue(pTag,"NEXT_STAGE_NM",out sValue);
				break;
			case "$GAME_NEXT_STAGE_GROUP_TYPE":
				this.parseMan.SetFieldValue(pTag,"NEXT_STAGE_GROUP_TYPE",out sValue);
				break;
			case "$GAME_NEXT_STAGE_GROUP_NM":
				this.parseMan.SetFieldValue(pTag,"NEXT_STAGE_GROUP_NM",out sValue);
				break;
			case "$GAME_GET_ITEM_SEQ_CSV":
				this.parseMan.SetFieldValue(pTag,"GET_ITEM_SEQ_CSV",out sValue);
				break;
			case "$GAME_GET_ITEM_NM_CSV":
				this.parseMan.SetFieldValue(pTag,"GET_ITEM_NM_CSV",out sValue);
				break;
			case "$GAME_GET_ITEM_COUNT_CSV":
				this.parseMan.SetFieldValue(pTag,"GET_ITEM_COUNT_CSV",out sValue);
				break;
			case "$GAME_GET_ITEM_NM_STR":
				sValue = this.CreateGetItemNm(pTag,pArgument);
				break;
			case "$GAME_PREV_STAGE_GROUP_TYPE_FOR_LINK":
				sValue = this.GetPrevStageGroupType(pTag,pArgument);
				break;
			case "$GAME_NEXT_STAGE_GROUP_TYPE_FOR_LINK":
				sValue = this.GetNextStageGroupType(pTag,pArgument);
				break;
			case "$GAME_BEFORE_STAGE_LEVEL":
				this.parseMan.SetFieldValue(pTag,"STAGE_LEVEL",out sValue);
				int iStageLevelBefore = 0;
				iStageLevelBefore = int.Parse(sValue) - 1;

				if (iStageLevelBefore < 0) {
					iStageLevelBefore = 0;
				}

				sValue = iStageLevelBefore.ToString();
				
				break;
			case "$GAME_NEXT_STAGE_LEVEL":
				this.parseMan.SetFieldValue(pTag,"STAGE_LEVEL",out sValue);
				int iStageLevelNext = 0;
				iStageLevelNext = int.Parse(sValue) + 1;

				if (iStageLevelNext < 0) {
					iStageLevelNext = 0;
				}

				sValue = iStageLevelNext.ToString();

				break;
			case "$SELF_GAME_STAGE_HONOR_BEFORE_2":
				sValue = GetHonorBefore(pTag,pArgument);
				break;
			case "$MAN_CLEAR_FROM_REGIST_DAYS":
				this.parseMan.SetFieldValue(pTag,"MAN_CLEAR_FROM_REGIST_DAYS",out sValue);
				break;
			case "$MAN_STAGE_CLEAR_POINT":
				this.parseMan.SetFieldValue(pTag,"MAN_STAGE_CLEAR_POINT",out sValue);
				
				if(!string.IsNullOrEmpty(sValue)){
					int iValue = int.Parse(sValue);
					sValue = String.Format("{0:#,0}",iValue);
				}
				break;
			case "$GAME_TOWN_CLEAR_PERCENT":{
				string[] sValues;
				if (this.parseMan.GetValues(pTag,"TOWN_COUNT,TOWN_CLEAR_COUNT",out sValues)) {

					double dTownCount = 0;
					double dTownClearCount = 0;

					double.TryParse(sValues[0],out dTownCount);
					double.TryParse(sValues[1],out dTownClearCount);

					if (dTownCount == 0) {
						return string.Empty;
					}

					sValue = iBridUtil.RoundOff(dTownClearCount / dTownCount * 100,0).ToString();
				}
				break;
			}

			#region □■□■□ PARTS定義 □■□■□
			case "$GAME_STAGE_CLEAR_GET_ITEM_PARTS":
				sValue = this.GetGameStageClearGetItem(pTag,pArgument);
				break;
			#endregion

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetListTown(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		TownSeekCondition oCondition = new TownSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.SexCd = this.parseMan.sessionMan.sexCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		DataSet oDataSet;
		using (Town oTown = new Town()) {
			oDataSet = oTown.GetPageCollectionItem(oCondition,oPartsArgs.NeedCount);
		}
		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_TOWN);
	}

	private string CreateGetItemNm(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sGameGetItemNmCsv = string.Empty;
		string sNmSeparateString = string.Empty;

		this.parseMan.SetFieldValue(pTag,"GET_ITEM_NM_CSV",out sGameGetItemNmCsv);
		string[] GameGetItemNmArr = sGameGetItemNmCsv.Split(',');

		if (string.IsNullOrEmpty(pArgument)) {
			sNmSeparateString = "と";
		} else {
			sNmSeparateString = pArgument;
		}

		sValue = string.Join(sNmSeparateString,GameGetItemNmArr);

		return sValue;
	}
	
	private string GetPrevStageGroupType(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iGameStageGroupType;
		int iGamePrevStageGroupType;
		int.TryParse(pArgument,out iGameStageGroupType);
		
		if(iGameStageGroupType > 1) {
			iGamePrevStageGroupType = iGameStageGroupType - 1;
		} else {
			iGamePrevStageGroupType = iGameStageGroupType;
		}
		
		return iGamePrevStageGroupType.ToString();
	}
	
	private string GetNextStageGroupType(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iGameStageGroupType;
		int iGameNextStageGroupType;
		int.TryParse(pArgument,out iGameStageGroupType);
		
		iGameNextStageGroupType = iGameStageGroupType + 1;
		
		return iGameNextStageGroupType.ToString();
	}

	private string GetHonorBefore(string pTag,string pArgument) {
		
		string sValue;
		int iStageLevel = 0;

		
		if (string.IsNullOrEmpty(pArgument)) {
			iStageLevel = int.Parse(this.getGameCharacter().stageLevel.ToString()) - 1;
		}
		
		iStageLevel = int.Parse(pArgument);
	
		using (Stage oStage = new Stage()) {
			sValue = oStage.GetHonor(parseMan.sessionMan.site.siteCd,iStageLevel,this.parseMan.sessionMan.sexCd);
		}
		
		return sValue;
	}

	private string GetOneBossData(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		StageSeekCondition oCondition = new StageSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.SexCd = this.parseMan.sessionMan.sexCd;
		oCondition.StageClearFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.NextStageMovedFlag = ViCommConst.FLAG_OFF_STR;

		DataSet oDataSet;
		
		using (Stage oGameStage = new Stage()) {
			oDataSet = oGameStage.GetOneBossData(oCondition);
		}
		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_TOWN);
	}

	private string GetStageClearPoint(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		DataSet oDataSet = null;
		using (StageClearPoint oStageClearPoint = new StageClearPoint()) {
			oDataSet = oStageClearPoint.GetStageClearPoint(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.sexCd);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_STAGE);
	}
	
	private string GetGameStageClearGetItem(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		StageSeekCondition oCondition = new StageSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.SexCd = this.parseMan.sessionMan.sexCd;

		DataSet oDataSet = null;

		using (Stage oStage = new Stage()) {
			oDataSet = oStage.GetStageClearItem(oCondition);
		}
		
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_ITEM);
	}
	
	private string GetGameTutorialStageSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;

		StageSeekCondition oCondition = new StageSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.SexCd = this.parseMan.sessionMan.sexCd;
		
		using (Stage oStage = new Stage()) {
			oDataSet = oStage.GetGameTutorialStageSeq(oCondition);
		}
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["STAGE_SEQ"].ToString();
		}
		
		return sValue;
	}
}