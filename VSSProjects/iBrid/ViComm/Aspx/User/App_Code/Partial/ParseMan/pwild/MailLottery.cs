﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ検索結果
--	Progaram ID		: MailLottery
--  Creation Date	: 2015.03.18
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

partial class PwParseMan {
	private string ParseMailLottery(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$POSSESSION_COUNT":
				this.parseMan.SetFieldValue(pTag,"POSSESSION_COUNT",out sValue);
				break;
			case "$NEED_SECOND_LOTTERY_COUNT":
				this.parseMan.SetFieldValue(pTag,"NEED_SECOND_LOTTERY_COUNT",out sValue);
				break;
			case "$CAN_GET_SECOND_LOTTERY_COUNT":
				this.parseMan.SetFieldValue(pTag,"CAN_GET_SECOND_LOTTERY_COUNT",out sValue);
				break;
			case "$MAIL_LOTTERY_RATE_SEQ":
				this.parseMan.SetFieldValue(pTag,"MAIL_LOTTERY_RATE_SEQ",out sValue);
				break;
			case "$MAIL_LOTTERY_RATE_NM":
				this.parseMan.SetFieldValue(pTag,"MAIL_LOTTERY_RATE_NM",out sValue);
				break;
			case "$REWARD_POINT":
				this.parseMan.SetFieldValue(pTag,"REWARD_POINT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}