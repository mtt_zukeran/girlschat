﻿using System;
using iBridCommLib;
using ViComm;
using System.Text;

partial class ParseMan {
	private string ParseManRanking(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {

			case "$MAN_SUMMARY_COUNT":
				SetFieldValue(pTag,"SUMMARY_COUNT",out sValue);
				break;
			case "$MAN_AGE":
				SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
}