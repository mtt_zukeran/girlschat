﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseCastPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_BBS_OBJ_FLAG":
				sValue = GetBbsObjFlag(pTag);
				break;
			case "$CAST_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$CAST_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_MONITOR_TALK_OK_FLAG":
				sValue = CanMonitorTalk(pTag);
				break;
			case "$CAST_MONITOR_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,IsMonitorVoiceEnable(pTag));
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag,"MOBILE_CONNECT_TYPE_NM",out sValue);
				break;
			case "$CAST_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_GALLERY_PATH":
				sValue = GetGalleryImage(pTag,pArgument);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LAST_ACTION_DATE":
			case "$CAST_LAST_LOGIN_DATE":
				SetFieldValue(pTag,"LAST_ACTION_DATE",out sValue);
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$CAST_PF_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_ATTR_TYPE_NM":
				SetFieldValue(pTag,"CAST_PIC_ATTR_TYPE_NM",out sValue);
				break;
			case "$CAST_PIC_ATTR_NM":
				SetFieldValue(pTag,"CAST_PIC_ATTR_NM",out sValue);
				break;
			case "$CAST_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag,"OBJ_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_BLUR_IMG_BACKGROUND_URL":
				SetFieldValue(pTag,"OBJ_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = string.Format("background:url(../{0});",sValue);
				break;
			case "$CAST_PIC_DOC":
				SetFieldValue(pTag,"PIC_DOC",out sValue);
				break;
			case "$CAST_PIC_DOWNLOAD_DATE":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_DATE",out sValue);
				break;
			case "$CAST_PIC_DOWNLOAD_FLAG":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_FLAG",out sValue);
				break;
			case "$CAST_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_LARGE_IMG_BACKGROUND_URL":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = string.Format("background:url(../{0});",sValue);
				break;
			case "$CAST_PROFILE_MOVIE_FLAG":
				SetFieldValue(pTag,"PROFILE_MOVIE_FLAG",out sValue);
				break;
			case "$CAST_PROFILE_PIC_COUNT":
				SetFieldValue(pTag,"PROFILE_PIC_COUNT",out sValue);
				break;
			case "$CAST_BBS_PIC_FLAG":
				SetFieldValue(pTag,"BBS_PIC_FLAG",out sValue);
				break;
			case "$CAST_BBS_MOVIE_FLAG":
				SetFieldValue(pTag,"BBS_MOVIE_FLAG",out sValue);
				break;
			case "$OBJ_SMALL_BLUR_PHOTO_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PIC_TITLE":
				SetFieldValue(pTag,"PIC_TITLE",out sValue);
				break;
			case "$CAST_PIC_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$CAST_PICKUP_START_PUB_DAY":
				SetFieldValue(pTag, "PICKUP_START_PUB_DAY", out sValue);
				break;								
			case "$CAST_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				SetFieldValue(pTag,"MONITOR_ENABLE_FLAG",out sValue);
				break;
			case "$DIV_IS_PICKUP":
			case "$CAST_IS_PICKUP":
				sValue = IsPickup(pTag, pArgument).ToString();
				break;								
			case "$HREF_CAST_IMG_PATH":
				sValue = GetHRefCastImg(pTag,pArgument);
				break;
			case "$HREF_MONITOR_TALK":
				sValue = GetHrefMonitorTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_VOICE":
				sValue = GetHrefMonitorVoice(pTag,pArgument);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$QUERY_VIEW_BBS_PIC":
				sValue = GetQueryViewBbsPic(pTag);
				break;
			case "$QUERY_VIEW_PROFILE_PIC":
				sValue = GetQueryViewProfilePic(pTag);
				break;
			case "$CAST_COMMENT_DETAIL":
				SetFieldValue(pTag,"COMMENT_DETAIL",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_LIST":
				SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_PIC_READING_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$CAST_PIC_ATTR_TYPE_SEQ":
				SetFieldValue(pTag,"CAST_PIC_ATTR_TYPE_SEQ",out sValue);
				break;
			case "$CAST_PIC_ATTR_SEQ":
				SetFieldValue(pTag,"CAST_PIC_ATTR_SEQ",out sValue);
				break;
			case "$CAST_USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$CAST_CHAR_NO":
				SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			// PICKUPのみ
			case "$CAST_PICKUP_OBJ_COMMENT":
				SetFieldValue(pTag,"PICKUP_OBJ_COMMENT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_PROFILE_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag,ViCommConst.ATTACHED_PROFILE.ToString());
				break;
			case "$CAST_BBS_PIC_COUNT":
				sValue = this.GetPicCount(pTag,ViCommConst.ATTACHED_BBS.ToString());
				break;
			case "$CAST_BBS_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag,ViCommConst.ATTACHED_BBS.ToString());
				break;
			case "$CAST_BBS_ATTR_PIC_COUNT":
				sValue = this.GetPicCountByAttr(pTag,pArgument);
				break;
			case "$CAST_BBS_ATTR_MOVIE_COUNT":
				sValue = this.GetMovieCountByAttr(pTag,pArgument);
				break;
	
			case "$BLOG_SEQ":
				SetFieldValue(pTag, "BLOG_SEQ", out sValue);
				break;

			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_IMG":
				sValue = GetCastFavoritImg(pTag,pArgument);
				break;
			case "$OBJ_SEQ":
				SetFieldValue(pTag,"PIC_SEQ",out sValue);
				break;
			case "$TODAY_VOTED_FLAG":
				SetFieldValue(pTag,"TODAY_VOTED_FLAG",out sValue);
				break;
			case "$PIC_TYPE":
				SetFieldValue(pTag,"PIC_TYPE",out sValue);
				break;

			// 閲覧履歴
			case "$SELF_USED_FLAG":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_FLAG",out sValue);
				break;
			case "$SELF_USED_DATE":
				SetFieldValue(pTag,"OBJ_DOWNLOAD_DATE",out sValue);
				break;
			case "$SELF_USED_SPECIAL_FREE_FLAG":
				SetFieldValue(pTag,"SELF_USED_SPECIAL_FREE_FLAG",out sValue);
				break;

			// レビュー
			case "$SELF_REVIEW_FLAG":
				sValue = this.GetSelfBbsPicReviewFlag(pTag,pArgument);
				break;
			case "$REVIEW_COUNT":
				SetFieldValue(pTag,"REVIEW_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$GOOD_STAR_AVR":
				sValue = GetGoodStarAvr(pTag);
				break;
			case "$GOOD_STAR_AVR_X10":
				sValue = GetGoodStarAvrX10(pTag);
				break;

			// お宝ブックマーク
			case "$BOOKMARK_FLAG":
				SetFieldValue(pTag,"BOOKMARK_FLAG",out sValue);
				break;

			// ファンクラブ
			case "$SELF_FANCLUB_MEMBER_RANK":
				string[] sValues;
				if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubMemberRank(sValues[0],sValues[1]);
				}
				break;
			// イイネ
			case "$CAST_PIC_LIKE_COUNT":
				SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$SELF_CAST_PIC_LIKED":
				sValue = GetSelfCastPicLiked(pTag,pArgument);
				break;
			// 自分を除くいいね数
			case "$CAST_PIC_LIKE_COUNT_OTHER":
				if (ViCommConst.FLAG_OFF_STR.Equals(GetSelfCastPicLiked(pTag,pArgument))) {
					// 自分がいいねしていない画像
					SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				} else {
					// 自分がいいねした画像
					string sCnt = string.Empty;
					GetValue(pTag,"LIKE_COUNT",out sCnt);
					sValue = string.Format("{0}",int.Parse(sCnt) - 1);
				}
				break;
			// コメント
			case "$CAST_PIC_COMMENT_COUNT":
				SetFieldValue(pTag,"CAST_PIC_COMMENT_COUNT",out sValue);
				break;
			case "$STRIP_PIC_SEQ":
				sValue = GetStripPicSeq(pTag,pArgument);
				break;
			case "$STRIP_BBS_MOVIE_SEQ":
				sValue = GetStripBbsMovieSeq(pTag,pArgument);
				break;
			case "$STRIP_SMALL_PIC_IMG_PATH":
				sValue = GetStripSmallPicImgPath(pTag,pArgument);
				break;
			case "$STRIP_LARGE_PIC_IMG_PATH":
				sValue = GetStripLargePicImgPath(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetGalleryImage(string pTag,string pArgument) {
		int iIdx = int.Parse(pArgument);
		if (GetDataRow(currentTableIdx).Table.Columns.Contains("OBJ_PHOTO_IMG_PATH")) {
			if (sessionMan.datasetRecCount >= iIdx) {
				return dataTable[currentTableIdx].Rows[iIdx - 1]["OBJ_PHOTO_IMG_PATH"].ToString();
			} else {
				return string.Format("image/{0}/nopic_s.gif",sessionMan.site.siteCd);
			}
		} else {
			parseErrorList.Add(string.Format("Unknow Column Tag:{0} Field:{1}",pTag,"OBJ_PHOTO_IMG_PATH"));
			return "";
		}
	}

	private string GetHRefCastImg(string pTag,string pArgument) {
		int iIdx;
		string sWidth,sAccessKey;
		if (ParseImageArguement(pTag,pArgument,out iIdx,out sWidth,out sAccessKey) == false) {
			return "";
		}
		string[] sValues;
		if (GetValues(pTag,"ACT_CATEGORY_IDX,LOGIN_ID,RNUM,CRYPT_VALUE",out sValues) == false) {
			return "";
		}

		string sHerf = "";
		string sPhoto = "";
		if (sessionMan.datasetRecCount >= iIdx) {
			sHerf = sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,
							string.Format("ViewPic.aspx?seekmode={0}&category={1}&loginid={2}&userrecno={3}&picrecno={4}&{5}={6}",
								sessionMan.seekMode,sValues[0],sValues[1],sValues[2],iIdx,sessionMan.site.charNoItemNm,sValues[3]));

			sPhoto = dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
			return string.Format("<a href=\"{0}\" {3}><img src=\"{1}\" width=\"{2}\"; alt=\"\" hspace=\"2\" vspace=\"2\" /></a>",sHerf,sPhoto,sWidth,sAccessKey);
		} else {
			sPhoto = string.Format("image/{0}/nopic_s.gif",sessionMan.site.siteCd);
			return string.Format("<img src=\"{0}\" width=\"{1}\"; alt=\"\"  hspace=\"2\" vspace=\"2\" />",sPhoto,sWidth);
		}
	}

	private string GetQueryViewProfilePic(string pTag) {
		string[] sValues;
		string sPicSeq;

		if (GetValues(pTag,"RNUM,LOGIN_ID,CRYPT_VALUE,PROFILE_PIC_SEQ",out sValues)) {
			sPicSeq = sValues[3];

			// DataSetにPROFILE_PIC_SEQが存在する場合、Profile PIC以外と判定する 
			// 本来はHREF_QUERYを分割するべき 
			if (IsExistField(pTag,"PIC_TYPE")) {
				if (!GetValue(pTag,"PIC_SEQ",out sPicSeq)) {
					return "";
				}
			}
			return string.Format("userrecno={0}&loginid={1}&category={2}&{3}={4}&picseq={5}&seekmode={6}",
									sValues[0],sValues[1],sessionMan.currentCategoryIndex,sessionMan.site.charNoItemNm,sValues[2],sPicSeq,sessionMan.seekMode);
		} else {
			return "";
		}
	}

	private string GetQueryViewBbsPic(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,LOGIN_ID,CRYPT_VALUE,PIC_SEQ",out sValues)) {
			return string.Format("userrecno={0}&loginid={1}&category={2}&{3}={4}&attrtypeseq={5}&attrseq={6}&seekmode={7}&objseq={8}&used={9}&unused={10}&ranktype={11}&bkm={12}",
									sValues[0],sValues[1],sessionMan.currentCategoryIndex,sessionMan.site.charNoItemNm,sValues[2],
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["attrtypeseq"]),
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["attrseq"]),
									sessionMan.seekMode,
									sValues[3],
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["used"]),
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["unused"]),
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["ranktype"]),
									iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["bkm"]));
		} else {
			return "";
		}
	}

	private string GetSelfCastPicLiked(string pTag,string pArgument) {
		string sValue = ViCommConst.FLAG_OFF_STR;
		string sPicSeq = string.Empty;

		if (sessionMan.logined) {
			if (GetValue(pTag,"PIC_SEQ",out sPicSeq)) {
				using (CastPicLike oCastPicLike = new CastPicLike()) {
					sValue = oCastPicLike.IsExist(sessionMan.site.siteCd,sPicSeq,sessionMan.userMan.userSeq);
				}
			}
		}

		return sValue;
	}

	private string GetSelfBbsPicReviewFlag(string pTag,string pArgument) {
		string sValue = ViCommConst.FLAG_OFF_STR;
		string sObjSeq;

		if (sessionMan.logined) {
			if (GetValue(pTag,"PIC_SEQ",out sObjSeq)) {
				if (IsExistSelfObjReviewHistory(sObjSeq)) {
					sValue = ViCommConst.FLAG_ON_STR;
				}
			}
		}

		return sValue;
	}

	private string GetStripPicSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sPickupObjComment;

		if (GetValue(pTag,"PICKUP_OBJ_COMMENT",out sPickupObjComment)) {
			string[] oPieces = sPickupObjComment.Split(new string[] { ":" },StringSplitOptions.None);

			if (oPieces.Length >= 1) {
				sValue = oPieces[0];
			}
		}

		return sValue;
	}

	private string GetStripBbsMovieSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sPickupObjComment;

		if (GetValue(pTag,"PICKUP_OBJ_COMMENT",out sPickupObjComment)) {
			string[] oPieces = sPickupObjComment.Split(new string[] { ":" },StringSplitOptions.None);

			if (oPieces.Length >= 2) {
				sValue = oPieces[1];
			}
		}

		return sValue;
	}

	private string GetStripSmallPicImgPath(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sPicSeq = GetStripPicSeq(pTag,pArgument);
		string sLoginId;

		if (!string.IsNullOrEmpty(sPicSeq)) {
			sPicSeq = sPicSeq.PadLeft(15,'0');

			if (GetValue(pTag,"LOGIN_ID",out sLoginId)) {
				sValue = string.Format("../data/{0}/operator/{1}/{2}_s.jpg",sessionMan.site.siteCd,sLoginId,sPicSeq);
			}
		}

		return sValue;
	}

	private string GetStripLargePicImgPath(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sPicSeq = GetStripPicSeq(pTag,pArgument);
		string sLoginId;

		if (!string.IsNullOrEmpty(sPicSeq)) {
			sPicSeq = sPicSeq.PadLeft(15,'0');

			if (GetValue(pTag,"LOGIN_ID",out sLoginId)) {
				sValue = string.Format("../data/{0}/operator/{1}/{2}.jpg",sessionMan.site.siteCd,sLoginId,sPicSeq);
			}
		}

		return sValue;
	}
}
