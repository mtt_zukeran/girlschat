﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 不具合報告検索結果
--	Progaram ID		: DefectReport
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseDefectReport(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$DEFECT_REPORT_SEQ":
				this.parseMan.SetFieldValue(pTag,"DEFECT_REPORT_SEQ",out sValue);
				break;
			case "$TITLE":
				this.parseMan.SetFieldValue(pTag,"TITLE",out sValue);
				break;
			case "$TEXT":
				this.parseMan.SetFieldValue(pTag,"TEXT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$ADMIN_COMMENT":
				this.parseMan.SetFieldValue(pTag,"ADMIN_COMMENT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$OVERLAP_COUNT":
				this.parseMan.SetFieldValue(pTag,"OVERLAP_COUNT",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseMan.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$UPDATE_DATE":
				this.parseMan.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			case "$DELETE_FLAG":
				this.parseMan.SetFieldValue(pTag,"DELETE_FLAG",out sValue);
				break;
			case "$DEFECT_REPORT_CATEGORY_NM":
				this.parseMan.SetFieldValue(pTag,"DEFECT_REPORT_CATEGORY_NM",out sValue);
				break;
			case "$DEFECT_REPORT_PROGRESS_NM":
				this.parseMan.SetFieldValue(pTag,"DEFECT_REPORT_PROGRESS_NM",out sValue);
				break;
			case "$DEFECT_REPORT_PROGRESS_COLOR":
				this.parseMan.SetFieldValue(pTag,"DEFECT_REPORT_PROGRESS_COLOR",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}