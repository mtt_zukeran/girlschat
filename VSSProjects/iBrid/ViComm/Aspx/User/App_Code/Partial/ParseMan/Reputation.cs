﻿partial class ParseMan {
	private string ParseReputation(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$QUERY_VIEW_REPUTATION":
				sValue = GetQueryViewReputation(pTag);
				break;
			case "$REPUTATION_DELETE_CHECK_BOX":
				sValue = GetCheckBoxTag(pTag,"MESSAGE_SEQ");
				break;
			case "$REPUTATION_DOC":
				sValue = GetDoc(pTag,"MESSAGE_DOC");
				break;
			case "$REPUTATION_CAST_HANDLE_NM":
				SetFieldValue(pTag,"PARTNER_HANDLE_NM",out sValue);
				break;
			case "$REPUTATION_CAST_LOGIN_ID":
				SetFieldValue(pTag,"PARTNER_LOGIN_ID",out sValue);
				break;
			case "$REPUTATION_WRITE_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$REPUTATION_WRITER_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryViewReputation(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,MESSAGE_SEQ",out sValues)) {
			return string.Format("userrecno={0}&category={1}&messageseq={2}",sValues[0],sessionMan.currentCategoryIndex,sValues[1]);
		} else {
			return "";
		}
	}

}
