﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;
using System.Text;
using ViComm.Extension.Pwild;
using System.Collections.Generic;
using System.Collections.Specialized;

partial class PwParseMan {
	public string ParseGameCharacter(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$PARTNER_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"PARTNER_USER_SEQ",out sValue);
				break;
			case "$PARTNER_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"PARTNER_USER_CHAR_NO",out sValue);
				break;
			case "$GAME_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_CHARACTER_TYPE":
				this.parseMan.SetFieldValue(pTag,"GAME_CHARACTER_TYPE",out sValue);
				break;
			case "$GAME_CHARACTER_LEVEL":
				this.parseMan.SetFieldValue(pTag,"GAME_CHARACTER_LEVEL",out sValue);
				break;
			case "$FELLOW_COUNT":
				this.parseMan.SetFieldValue(pTag,"FELLOW_COUNT",out sValue);
				break;
			case "$WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"WIN_COUNT",out sValue);
				break;
			case "$LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"LOSS_COUNT",out sValue);
				break;
			case "$TOTAL_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"TOTAL_WIN_COUNT",out sValue);
				break;
			case "$TOTAL_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"TOTAL_LOSS_COUNT",out sValue);
				break;
			case "$TOTAL_PARTY_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"TOTAL_PARTY_WIN_COUNT",out sValue);
				break;
			case "$TOTAL_PARTY_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"TOTAL_PARTY_LOSS_COUNT",out sValue);
				break;
			case "$PARTY_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTY_WIN_COUNT",out sValue);
				break;
			case "$PARTY_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTY_LOSS_COUNT",out sValue);
				break;
			case "$ATTACK_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"ATTACK_WIN_COUNT",out sValue);
				break;
			case "$ATTACK_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"ATTACK_LOSS_COUNT",out sValue);
				break;
			case "$PARTY_ATTACK_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTY_ATTACK_WIN_COUNT",out sValue);
				break;
			case "$PARTY_ATTACK_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTY_ATTACK_LOSS_COUNT",out sValue);
				break;
			case "$DEFENCE_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"DEFENCE_WIN_COUNT",out sValue);
				break;
			case "$DEFENCE_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"DEFENCE_LOSS_COUNT",out sValue);
				break;
			case "$PARTY_DEFENCE_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTY_DEFENCE_WIN_COUNT",out sValue);
				break;
			case "$PARTY_DEFENCE_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTY_DEFENCE_LOSS_COUNT",out sValue);
				break;
			case "$GET_POINT":
				this.parseMan.SetFieldValue(pTag,"GET_POINT",out sValue);
				break;
			case "$LOSS_POINT":
				this.parseMan.SetFieldValue(pTag,"LOSS_POINT",out sValue);
				break;
			case "$TREASURE_GET_COUNT":
				this.parseMan.SetFieldValue(pTag,"TREASURE_GET_COUNT",out sValue);
				break;
			case "$TREASURE_LOSS_COUNT":
				this.parseMan.SetFieldValue(pTag,"TREASURE_LOSS_COUNT",out sValue);
				break;
			case "$SMALL_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$GAME_SMALL_PHOTO_IMG_PATH":
				sValue = GetGameSmallPhotoImgPath(pTag,pArgument);
				break;
			case "$CAST_ONLINE_STATUS":
				this.parseMan.SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$GAME_TREASURE_PIC_COUNT":
				this.parseMan.SetFieldValue(pTag,"GAME_TREASURE_PIC_COUNT",out sValue);
				break;
			case "$GAME_TREASURE_MOVIE_COUNT":
				this.parseMan.SetFieldValue(pTag,"GAME_TREASURE_MOVIE_COUNT",out sValue);
				break;
			case "$FRIENDLY_RANK":
				this.parseMan.SetFieldValue(pTag,"FRIENDLY_RANK",out sValue);
				break;
			case "$ATTACK_FORCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"ATTACK_FORCE_COUNT",out sValue);
				break;
			case "$MAX_ATTACK_FORCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"MAX_ATTACK_FORCE_COUNT",out sValue);
				break;
			case "$DEFENCE_FORCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"DEFENCE_FORCE_COUNT",out sValue);
				break;
			case "$MAX_DEFENCE_FORCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"MAX_DEFENCE_FORCE_COUNT",out sValue);
				break;
			case "$CAST_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_USER_SEQ",out sValue);
				break;
			case "$LAST_HUG_DAY":
				this.parseMan.SetFieldValue(pTag,"LAST_HUG_DAY",out sValue);
				break;
			case "$LAST_HUG_DATE":
				this.parseMan.SetFieldValue(pTag,"LAST_HUG_DATE",out sValue);
				break;
			case "$APPLICATION_DATE":
				this.parseMan.SetFieldValue(pTag,"APPLICATION_DATE",out sValue);
				break;
			case "$FELLOW_APPLICATION_STATUS":
				this.parseMan.SetFieldValue(pTag,"FELLOW_APPLICATION_STATUS",out sValue);
				break;
			case "$CAST_ITEM":
				this.parseMan.SetFieldValue(pTag,"CAST_ATTR_VALUE" + pArgument,out sValue);
				break;
			case "$ACTION_TYPE":
				this.parseMan.SetFieldValue(pTag,"ACTION_TYPE",out sValue);
				break;
			#region □■□■□ 通話・メールリンク □■□■□
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = this.parseMan.IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = this.parseMan.IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = this.parseMan.IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = this.parseMan.IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = this.parseMan.IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_MONITOR_TALK_OK_FLAG":
				sValue = this.parseMan.CanMonitorTalk(pTag);
				break;
			case "$CAST_MONITOR_VOICE_OK_FLAG":
				sValue = this.parseMan.IsEqual(pTag,this.parseMan.IsMonitorVoiceEnable(pTag));
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				this.parseMan.SetFieldValue(pTag,"MONITOR_ENABLE_FLAG",out sValue);
				break;
			case "$HREF_TV_TALK":
				sValue = this.parseMan.GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = this.parseMan.GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = this.parseMan.GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = this.parseMan.GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_TALK":
			case "$HREF_MONITOR_TALK2":
				sValue = this.parseMan.GetHrefMonitorTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_VOICE":
				sValue = this.parseMan.GetHrefMonitorVoice(pTag,pArgument);
				break;
			#endregion
			case "$GAME_FAVORITE":
				this.parseMan.SetFieldValue(pTag,"GAME_FAVORITE",out sValue);
				break;
			case "$GAME_PIC_UPLOAD_COUNT":
				this.parseMan.SetFieldValue(pTag,"GAME_PIC_UPLOAD_COUNT",out sValue);
				break;
			case "$GAME_MOVIE_UPLOAD_COUNT":
				this.parseMan.SetFieldValue(pTag,"GAME_MOVIE_UPLOAD_COUNT",out sValue);
				break;
			case "$GAME_FIND_RE_QUERY":
				sValue = GetGameFindReQuery();
				break;
			case "$CAST_GAME_PIC_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_GAME_PIC_SEQ",out sValue);
				break;
			case "$OBJ_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"CAST_USER_CHAR_NO",out sValue);
				break;
			case "$CAST_GAME_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"CAST_GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_LIST_ATTACK_FORCE_COUNT":
				sValue = this.CreateListAttackForceCount(pArgument);
				break;
			case "$SUPPORT_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"SUPPORT_USER_SEQ",out sValue);
				break;
			case "$SUPPORT_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"SUPPORT_USER_CHAR_NO",out sValue);
				break;
			case "$SUPPORT_BATTLE_LOG_SEQ":
				this.parseMan.SetFieldValue(pTag,"BATTLE_LOG_SEQ",out sValue);
				break;
			case "$SUPPORT_REQUEST_SUBSEQ":
				this.parseMan.SetFieldValue(pTag,"SUPPORT_REQUEST_SUBSEQ",out sValue);
				break;
			case "$AIM_TREASURE_SEQ":
				this.parseMan.SetFieldValue(pTag,"AIM_TREASURE_SEQ",out sValue);
				break;
			case "$ATTACK_POWER":
				this.parseMan.SetFieldValue(pTag,"ATTACK_POWER",out sValue);
				break;
			case "$USE_ATTACK_COUNT":
				this.parseMan.SetFieldValue(pTag,"USE_ATTACK_COUNT",out sValue);
				break;
			case "$USE_DEFENCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"USE_DEFENCE_COUNT",out sValue);
				break;
			case "$MOVE_GAME_POINT":
				this.parseMan.SetFieldValue(pTag,"MOVE_GAME_POINT",out sValue);
				break;
			case "$GAME_LOTTERY_COMPLETE_DATE":
				this.parseMan.SetFieldValue(pTag,"LOTTERY_COMPLETE_DATE",out sValue);
				break;
			case "$NA_FLAG":
				this.parseMan.SetFieldValue(pTag,"NA_FLAG",out sValue);
				break;
			case "$PARTNER_NA_FLAG":
				this.parseMan.SetFieldValue(pTag,"PARTNER_NA_FLAG",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$SITE_USE_STATUS":
				this.parseMan.SetFieldValue(pTag,"SITE_USE_STATUS",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseMan.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$GAME_ITEM_SEQ":
				this.parseMan.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sValue);
				break;
			case "$GAME_ITEM_NM":
				this.parseMan.SetFieldValue(pTag,"GAME_ITEM_NM",out sValue);
				break;
			case "$GAME_BATTLE_LOG_SEQ":
				this.parseMan.SetFieldValue(pTag,"BATTLE_LOG_SEQ",out sValue);
				break;
			case "$GAME_ATTACK_WIN_FLAG":
				this.parseMan.SetFieldValue(pTag,"ATTACK_WIN_FLAG",out sValue);
				break;
			case "$GAME_ATTACK_LOSE_FLAG":
				this.parseMan.SetFieldValue(pTag,"ATTACK_LOSE_FLAG",out sValue);
				break;
			case "$GAME_DEFENCE_WIN_FLAG":
				this.parseMan.SetFieldValue(pTag,"DEFENCE_WIN_FLAG",out sValue);
				break;
			case "$GAME_DEFENCE_LOSE_FLAG":
				this.parseMan.SetFieldValue(pTag,"DEFENCE_LOSE_FLAG",out sValue);
				break;
			case "$GAME_SUPPORT_SUCCESS_FLAG":
				this.parseMan.SetFieldValue(pTag,"SUPPORT_SUCCESS_FLAG",out sValue);
				break;
			case "$GAME_SUPPORT_FAIL_FLAG":
				this.parseMan.SetFieldValue(pTag,"SUPPORT_FAIL_FLAG",out sValue);
				break;
			case "$GAME_SUPPORT_REQUEST_FLAG":
				this.parseMan.SetFieldValue(pTag,"SUPPORT_REQUEST_FLAG",out sValue);
				break;
			case "$GAME_CREATE_DATE":
				this.parseMan.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$GAME_NEWS_TYPE":
				this.parseMan.SetFieldValue(pTag,"NEWS_TYPE",out sValue);
				break;
			case "$GAME_NEWS_ITEM_NM":
				this.parseMan.SetFieldValue(pTag,"NEWS_ITEM_NM",out sValue);
				break;
			case "$GAME_BATTLE_WIN_COUNT":
				this.parseMan.SetFieldValue(pTag,"BATTLE_WIN_COUNT",out sValue);
				break;
			case "$FRIENDLY_POINT":
				this.parseMan.SetFieldValue(pTag,"FRIENDLY_POINT",out sValue);
				break;
			case "$GAME_RANK":
				this.parseMan.SetFieldValue(pTag,"GAME_RANK",out sValue);
				break;
			case "$COMPLETE_DATE":
				this.parseMan.SetFieldValue(pTag,"COMPLETE_DATE",out sValue);
				break;
			case "$LAST_PARTNER_HUG_DATE":
				this.parseMan.SetFieldValue(pTag,"LAST_PARTNER_HUG_DATE",out sValue);
				break;
			case "$GAME_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$DISTINCTION_CD":
				this.parseMan.SetFieldValue(pTag,"DISTINCTION_CD",out sValue);
				break;
			case "$GAME_LIST_TUTORIAL_ATTACK_FORCE_COUNT":
				sValue = this.CreateListTutorialAttackForceCount(pArgument);
				break;
			case "$GAME_SUPPORT_COUNT":
				this.parseMan.SetFieldValue(pTag,"SUPPORT_COUNT",out sValue);
				break;
			case "$GAME_PARTNER_SUPPORT_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTNER_SUPPORT_COUNT",out sValue);
				break;
			case "$GAME_HUG_COUNT":
				this.parseMan.SetFieldValue(pTag,"HUG_COUNT",out sValue);
				break;
			case "$GAME_PARTNER_HUG_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTNER_HUG_COUNT",out sValue);
				break;
			case "$GAME_PRESENT_COUNT":
				this.parseMan.SetFieldValue(pTag,"PRESENT_COUNT",out sValue);
				break;
			case "$GAME_PARTNER_PRESENT_COUNT":
				this.parseMan.SetFieldValue(pTag,"PARTNER_PRESENT_COUNT",out sValue);
				break;
			case "$ENEMY_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"ENEMY_USER_SEQ",out sValue);
				break;
			case "$ENEMY_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"ENEMY_USER_CHAR_NO",out sValue);
				break;
			case "$ENEMY_GAME_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"ENEMY_GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_TUTORIAL_STATUS":
				this.parseMan.SetFieldValue(pTag,"TUTORIAL_STATUS",out sValue);
				break;
			case "$DIV_IS_SUPPORT_REQUEST_END": {
					string sSupportRequestEndFlag = string.Empty;
					this.parseMan.SetFieldValue(pTag,"SUPPORT_REQUEST_END_FLAG",out sSupportRequestEndFlag);
					this.parseMan.SetNoneDisplay(!sSupportRequestEndFlag.Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$DIV_IS_NOT_SUPPORT_REQUEST_END": {
					string sSupportRequestEndFlag = string.Empty;
					this.parseMan.SetFieldValue(pTag,"SUPPORT_REQUEST_END_FLAG",out sSupportRequestEndFlag);
					this.parseMan.SetNoneDisplay(sSupportRequestEndFlag.Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$GAME_PARTY_MEMBER_FLAG":
				this.parseMan.SetFieldValue(pTag,"PARTY_MEMBER_FLAG",out sValue);
				break;
			case "$CAST_LARGE_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$PRESENT_GAME_ITEM_SEQ":
				this.parseMan.SetFieldValue(pTag,"PRESENT_GAME_ITEM_SEQ",out sValue);
				break;
			case "$GAME_RNUM":
				this.parseMan.SetFieldValue(pTag,"RNUM",out sValue);
				break;
			case "$DEFENCE_POWER":
				this.parseMan.SetFieldValue(pTag,"DEFENCE_POWER",out sValue);
				break;
			case "$ENDURANCE_NM":
				this.parseMan.SetFieldValue(pTag,"ENDURANCE_NM",out sValue);
				break;
			case "$ITEM_COUNT":
				this.parseMan.SetFieldValue(pTag,"ITEM_COUNT",out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.parseMan.GetQueryCastProfile(pTag);
				break;
			case "$GAME_MAX_ATTACK_FORCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"MAX_ATTACK_FORCE_COUNT",out sValue);
				break;
			case "$REC_NO_PER_PAGE":
				this.parseMan.SetFieldValue(pTag,"REC_NO_PER_PAGE",out sValue);
				break;
			case "$GAME_INFORMATION_TYPE":
				this.parseMan.SetFieldValue(pTag,"INFORMATION_TYPE",out sValue);
				break;
			case "$TREASURE_COMPLETE_FLAG":
				this.parseMan.SetFieldValue(pTag,"TREASURE_COMPLETE_FLAG",out sValue);
				break;
			case "$BONUS_GET_FLAG":
				this.parseMan.SetFieldValue(pTag,"BONUS_GET_FLAG",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"CAST_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$TUTORIAL_FLAG":
				this.parseMan.SetFieldValue(pTag,"TUTORIAL_FLAG",out sValue);
				break;
			case "$MOST_SWEET_HEART_FLAG":
				this.parseMan.SetFieldValue(pTag,"MOST_SWEET_HEART_FLAG",out sValue);
				break;
			case "$DIV_IS_EXIST_MOST_SWEET_HEART":
				this.parseMan.SetNoneDisplay(!CheckExistMostSweetHeart(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_EXIST_MOST_SWEET_HEART":
				this.parseMan.SetNoneDisplay(CheckExistMostSweetHeart(pTag,pArgument));
				break;
			#region □■□■□ プレゼント履歴 □■□■□
			case "$PRESENT_HISTORY_SEQ":
				this.parseMan.SetFieldValue(pTag,"PRESENT_HISTORY_SEQ",out sValue);
				break;
			case "$PRESENT_HISTORY_GET_FLAG":
				this.parseMan.SetFieldValue(pTag,"GET_FLAG",out sValue);
				break;
			#endregion

			#region □■□■□ バトル履歴 □■□■□
			case "$MAN_TREASURE_DISPLAY_DAY":
				this.parseMan.SetFieldValue(pTag,"DISPLAY_DAY",out sValue);
				break;
			case "$CAST_GAME_PIC_ATTR_NM":
				this.parseMan.SetFieldValue(pTag,"CAST_GAME_PIC_ATTR_NM",out sValue);
				break;
			#endregion

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetListPartyBattleMember(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		CastGameCharacterFellowMemberSeekCondition oCondition = new CastGameCharacterFellowMemberSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.FellowApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
		oCondition.PartyMemberFlag = ViCommConst.FLAG_ON_STR;

		DataSet oDataSet = null;
		CastGameCharacterFellowMember oCastGameCharacterFelloMember = new CastGameCharacterFellowMember(this.parseMan.sessionObjs);
		oDataSet = oCastGameCharacterFelloMember.GetListPartyBattleMember(oCondition);

		if (oDataSet == null) {
			return string.Empty;
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
	}

	private string GetListGameCharacter(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		CastGameCharacterSeekCondition oCondition = new CastGameCharacterSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.SeekMode = ulong.Parse(this.parseMan.sessionObjs.seekMode);
		oCondition.ManUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.ManUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		DataSet oTmpDataSet = null;
		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter(this.parseMan.sessionObjs)) {
			oTmpDataSet = oCastGameCharacter.GetOneByUserSeq(oCondition);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		string iPrevSeekMode = this.parseMan.sessionObjs.seekMode;
		SeekConditionBase oPrevSeekConditionBase = this.parseMan.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			this.parseMan.sessionObjs.seekMode = oCondition.SeekMode.ToString();
			this.parseMan.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		} finally {
			this.parseMan.sessionObjs.seekMode = iPrevSeekMode;
			this.parseMan.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetCastGameCharacterOuter(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		CastGameCharacterSeekCondition oCondition = new CastGameCharacterSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.ManUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.ManUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter(this.parseMan.sessionObjs)) {
			oDataSet = oCastGameCharacter.GetOneOuter(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetListGameCharacterFellowNoHug(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		CastGameCharacterFellowMemberSeekCondition oCondition = new CastGameCharacterFellowMemberSeekCondition(pPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.FellowApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
		oCondition.LastHugDay = DateTime.Now.ToString("yyyy/MM/dd");
		DataSet oDataSet = null;
		using (CastGameCharacterFellowMember oCastGameCharacterFellowMember = new CastGameCharacterFellowMember(this.parseMan.sessionObjs)) {
			oDataSet = oCastGameCharacterFellowMember.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}
		if (oDataSet == null) {
			return string.Empty;
		}
		return this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
	}

	private string GetGameFindReQuery() {
		string sRequestQuery = string.Empty;
		string sValue = string.Empty;
		foreach (string sKey in this.parseMan.sessionMan.requestQuery.QueryString) {
			if (string.IsNullOrEmpty(sKey)) {
				continue;
			}
			if (sKey.StartsWith("sort")) {
				continue;
			}
			if (sKey.Equals("pageno")) {
				continue;
			}
			if (sRequestQuery.Equals(string.Empty)) {
				sRequestQuery = sKey + "=" + HttpUtility.UrlDecode(this.parseMan.sessionMan.requestQuery.QueryString[sKey]);
			} else {
				sRequestQuery = "&" + sKey + "=" + HttpUtility.UrlDecode(this.parseMan.sessionMan.requestQuery.QueryString[sKey]);
			}
			sValue += sRequestQuery;
		}
		return sValue;
	}

	private string CreateListAttackForceCount(string pArgument) {
		string sValue = string.Empty;

		string sForceCountLimit = pArgument;
		int iForceCountLimit;
		int.TryParse(sForceCountLimit,out iForceCountLimit);
		double dForceCount = iForceCountLimit;

		if (iForceCountLimit > 0) {
			StringBuilder sValueBuilder = new StringBuilder();
			sValueBuilder.AppendLine("<select name=\"attack_force_count\">");

			if (iForceCountLimit <= 10) {
				for (int i = iForceCountLimit;i >= 1;i--) {
					sValueBuilder.AppendLine(string.Format("<option value=\"{0}\">{0}人</option>",i.ToString()));
				}
			} else {
				for (int i = 1;i <= 10;i++) {
					sValueBuilder.AppendLine(string.Format("<option value=\"{0}\">{0}人</option>",dForceCount.ToString()));
					dForceCount = iForceCountLimit - Math.Floor((iForceCountLimit * 0.1 * i));
				}
			}

			sValueBuilder.AppendLine("</select>");

			sValue = sValueBuilder.ToString();
		}

		return sValue;
	}

	private string GetFriendlyPoint(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] arrText = pArgument.Split(',');

		if (arrText.Length == 2) {
			FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
			oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
			oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
			oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
			oCondition.PartnerUserSeq = arrText[0];
			oCondition.PartnerUserCharNo = arrText[1];

			using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
				sValue = oFriendlyPoint.GetFriendlyPoint(oCondition);
			}
		}

		return sValue;
	}

	private string GetTotalFriendlyPoint(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] arrText = pArgument.Split(',');

		if (arrText.Length == 2) {
			FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
			oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
			oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
			oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
			oCondition.PartnerUserSeq = arrText[0];
			oCondition.PartnerUserCharNo = arrText[1];

			using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
				sValue = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
			}
		}

		return sValue;
	}

	private string GetFellowLog(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		NewsSeekCondition oCondition = new NewsSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		DataSet oDataSet = null;
		using (News oNews = new News()) {
			oDataSet = oNews.GetPageCollectionFellowLog(oCondition,1,pPartsArgs.NeedCount);
		}
		if (oDataSet == null) {
			return string.Empty;
		}
		return this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
	}

	private string GetFavoriteLog(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		NewsSeekCondition oCondition = new NewsSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		DataSet oDataSet = null;
		using (News oNews = new News()) {
			oDataSet = oNews.GetPageCollectionFavoriteLog(oCondition,1,pPartsArgs.NeedCount);
		}
		if (oDataSet == null) {
			return string.Empty;
		}
		return this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
	}

	private string GetGamePartnerHug(string pTag,string pArgument) {

		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		CastGameCommunicationPartnerHugSeekCondition oCondition = new CastGameCommunicationPartnerHugSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		DataSet oDataSet = null;
		using (CastGameCommunicationPartnerHug oCastGameCommunicationPartnerHug = new CastGameCommunicationPartnerHug()) {
			oDataSet = oCastGameCommunicationPartnerHug.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetSweetHeartCount(string pTag,string pArgument) {
		string sValue = string.Empty;

		using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
			sValue = oFriendlyPoint.GetSweetHeartCount(this.parseMan.sessionMan.site.siteCd,this.parseMan.sessionMan.userMan.userSeq,this.parseMan.sessionMan.userMan.userCharNo);
		}

		return sValue;
	}

	private string GetMosaicRankData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		
		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = parseMan.sessionMan.userMan.userCharNo;

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter(parseMan.sessionObjs)) {
			oTmpDataSet = oCastGameCharacter.GetMosaicRank(sSiteCd,sUserSeq,sUserCharNo,pPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		if (oTmpDataSet.Tables[0].Rows.Count < pPartsArgs.NeedCount) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string GetCastToday(string pTag,string pArgument) {

		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		CastGameCharacterTodaySeekCondition oCondition = new CastGameCharacterTodaySeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.SelfUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.SelfUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		DataSet oDataSet = null;
		using (CastGameCharacterToday oCastGameCharacterToday = new CastGameCharacterToday()) {
			oDataSet = oCastGameCharacterToday.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		if (oDataSet.Tables[0].Rows.Count < pPartsArgs.NeedCount) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private string CreateListTutorialAttackForceCount(string pArgument) {
		string sValue = string.Empty;

		string sForceCountLimit = pArgument;
		int iForceCountLimit;
		int.TryParse(sForceCountLimit,out iForceCountLimit);

		if (iForceCountLimit > 0) {
			StringBuilder sValueBuilder = new StringBuilder();
			sValueBuilder.AppendLine("<select name=\"attack_force_count\">");

			sValueBuilder.AppendLine(string.Format("<option value=\"{0}\">{0}人</option>",iForceCountLimit.ToString()));

			sValueBuilder.AppendLine("</select>");

			sValue = sValueBuilder.ToString();
		}

		return sValue;
	}

	private string GetBattleLog(string pTag,string pArgument) {

		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		BattleLogSeekCondition oCondition = new BattleLogSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		DataSet oDataSet = null;
		using (BattleLog oBattleLog = new BattleLog()) {
			oDataSet = oBattleLog.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);

		return sValue;
	}

	private bool CheckExistGameDatePartner(string pTag,string pArgument) {
		if (this.parseMan.sessionMan.userMan.gameCharacter.fellowCount > 0) {
			return true;
		}

		CastGameCharacterFavoriteSeekCondition oCondition = new CastGameCharacterFavoriteSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.SiteUseStatus = ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME;

		using (CastGameCharacterFavorite oCastGameCharacterFavorite = new CastGameCharacterFavorite()) {
			int iCount = oCastGameCharacterFavorite.GetGameFavoriteCount(oCondition);

			if (iCount > 0) {
				return true;
			}
		}

		return false;
	}

	protected string GetGameFellowCountAllStatus(string pTag,string pArgument) {
		string sValue = string.Empty;

		GameFellowSeekCondition oCondition = new GameFellowSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (GameFellow oGameFellow = new GameFellow()) {
			sValue = oGameFellow.GetFellowCount(oCondition);
		}

		return sValue;
	}

	private bool CheckExistGameFavorite(string pTag,string pArgument) {
		bool bOk = false;
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (GameFavorite oGameFavorite = new GameFavorite()) {
			bOk = oGameFavorite.CheckExistGameFavorite(sSiteCd,sUserSeq,sUserCharNo);
		}

		return bOk;
	}

	private string GetCastGameCharacterOuterTutorialGetTreasure(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oLogDataSet = null;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oLogDataSet = oGetManTreasureLog.GetOneForTutorial(this.parseMan.sessionMan.userMan.siteCd,this.parseMan.sessionMan.userMan.userSeq,this.parseMan.sessionMan.userMan.userCharNo);
		}

		if (oLogDataSet == null) {
			return string.Empty;
		}

		CastGameCharacterSeekCondition oCondition = new CastGameCharacterSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = oLogDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
		oCondition.UserCharNo = oLogDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		oCondition.ManUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.ManUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter(this.parseMan.sessionObjs)) {
			oDataSet = oCastGameCharacter.GetOneOuter(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetOneNewHug(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		CastGameCommunicationPartnerHugSeekCondition oCondition = new CastGameCommunicationPartnerHugSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (CastGameCommunicationPartnerHug oCastGameCommunicationPartnerHug = new CastGameCommunicationPartnerHug()) {
			oDataSet = oCastGameCommunicationPartnerHug.GetOneNewHug(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetOneSelfPartnerPresentUnchecked(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		GameCharacterPresentHistorySeekCondition oCondition = new GameCharacterPresentHistorySeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY;
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.PartnerUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.PartnerUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (GameCharacterPresentHistory oGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
			oDataSet = oGameCharacterPresentHistory.GetOnePresentHistory(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetOneFellowApplication(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		CastGameCharacterFellowSeekCondition oCondition = new CastGameCharacterFellowSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION;
		oCondition.Sort = PwViCommConst.CastGameCharacterFellowSort.APPLICATION_DATE_DESC;

		using (CastGameCharacterFellow oCastGameCharacterFellow = new CastGameCharacterFellow()) {
			oDataSet = oCastGameCharacterFellow.GetOneFellowApplication(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetOneSupportRequest(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (BattleLog oBattleLog = new BattleLog()) {
			oDataSet = oBattleLog.GetOneSupportRequest(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetInformation(string pTag,string pArgument) {

		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		GameInformationSeekCondition oCondition = new GameInformationSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (GameInformation oGameInformation = new GameInformation()) {
			oDataSet = oGameInformation.GetPageCollection(oCondition,1,1);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetSweetHeartData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		CastGameCharacterFindSeekCondition oCondition = new CastGameCharacterFindSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.Relation = "1";
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(oPartsArgs.Query["sort"]))) {
			oCondition.Sort = PwViCommConst.CastGameCharacterFindSort.MOST_SWEET_HEART_NEXT_RANDOM;
		}

		using (CastGameCharacterFind oCastGameCharacterFind = new CastGameCharacterFind(this.parseMan.sessionObjs)) {
			oDataSet = oCastGameCharacterFind.GetPageCollection(oCondition,1,1);
		}

		if (oDataSet.Tables[0].Rows.Count == 0) {
			oDataSet = new DataSet();

			DataTable oDt = new DataTable();
			DataRow oDr;
			oDataSet.Tables.Add(oDt);
			oDt.Columns.Add("USER_SEQ",Type.GetType("System.String"));
			oDr = oDt.NewRow();
			oDr["USER_SEQ"] = string.Empty;
			oDt.Rows.Add(oDr);
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}

	private string GetHerSweetHeartData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.Rank = "1";

		using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
			oDataSet = oFriendlyPoint.GetHerSweetHeartData(oCondition,1,1);
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_PARTNER_GAME_CHARACTER);
		return sValue;
	}
	
	private string GetCastFindCount(string pTag,string pArgument) {
		decimal totalRowCount;

		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseMan,oArgArr,0);

		CastGameCharacterFindSeekCondition oCondition = new CastGameCharacterFindSeekCondition(oQuery);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		if (oCondition.NewCast.Equals(ViCommConst.FLAG_ON_STR)) {
			oCondition.NewCastDay = this.parseMan.sessionMan.site.newFaceDays.ToString();
		}
		if (string.IsNullOrEmpty(oCondition.Sort)) {
			oCondition.Sort = PwViCommConst.CastGameCharacterFindSort.LAST_LOGIN_DATE_DESC;
		}
		using (CastGameCharacterFind oCastGameCharacterFind = new CastGameCharacterFind()) {
			oCastGameCharacterFind.GetPageCount(oCondition,1,out totalRowCount);
		}
		
		return totalRowCount.ToString();
	}
	
	private bool CheckExistMostSweetHeart(string pTag,string pArgument) {
		DataSet oDataSet = null;

		MostSweetHeartSeekCondition oCondition = new MostSweetHeartSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.SelfUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.SelfUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (MostSweetHeart oMostSweetHeart = new MostSweetHeart()) {
			oDataSet = oMostSweetHeart.GetOne(oCondition);
		}
		
		return oDataSet.Tables[0].Rows.Count > 0;
	}

	private string GetCastHandleNm(string pTag, string pArgument) {
		string sValue = string.Empty;
		
		string[] sArr = pArgument.Split(',');
		
		if (sArr.Length < 2) {
			return string.Empty;
		}
		
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = sArr[0];
		string sUserCharNo = sArr[1];
		
		DataSet oDataSet = null;
		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			oDataSet = oCastGameCharacter.GetCastGameCharacterData(sSiteCd,sUserSeq,sUserCharNo);
		}
		
		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		
		sValue = oDataSet.Tables[0].Rows[0]["HANDLE_NM"].ToString();
		
		return sValue;
	}

	private string GetGameSmallPhotoImgPath(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] sValues;

		if (this.parseMan.GetValues(pTag,"SMALL_PHOTO_IMG_PATH,GAME_CHARACTER_TYPE",out sValues)) {
			if (!sValues[0].Equals("image/A001/nopic_s.gif")) {
				sValue = "../" + sValues[0];
			} else {
				sValue = string.Format("../Image/{0}/game/wo_type_a{1}.gif",parseMan.sessionMan.site.siteCd,sValues[1]);
			}
		}

		return sValue;
	}
}