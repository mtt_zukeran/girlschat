﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseDatePlan(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		string lQuery = string.Empty;

		switch (pTag) {
			case "$DATE_PLAN_SEQ":
				this.parseMan.SetFieldValue(pTag,"DATE_PLAN_SEQ",out sValue);
				break;
			case "$DATE_PLAN_NM":
				this.parseMan.SetFieldValue(pTag,"DATE_PLAN_NM",out sValue);
				break;
			case "$DATE_PLAN_CHARGE_FLAG":
				this.parseMan.SetFieldValue(pTag,"CHARGE_FLAG",out sValue);
				break;
			case "$DATE_PLAN_PRICE":
				this.parseMan.SetFieldValue(pTag,"PRICE",out sValue);
				break;
			case "$DATE_PLAN_FRIENDLY_POINT":
				this.parseMan.SetFieldValue(pTag,"FRIENDLY_POINT",out sValue);
				break;
			case "$DATE_PLAN_WAITING_MIN":
				this.parseMan.SetFieldValue(pTag,"WAITING_MIN",out sValue);
				break;
			case "$REC_NO_PER_PAGE":
				this.parseMan.SetFieldValue(pTag,"REC_NO_PER_PAGE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}