﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseTown(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$GAME_STAGE_SEQ":
				this.parseMan.SetFieldValue(pTag,"STAGE_SEQ",out sValue);
				break;
			case "$GAME_STAGE_NM":
				this.parseMan.SetFieldValue(pTag,"STAGE_NM",out sValue);
				break;
			case "$GAME_TOWN_SEQ":
				this.parseMan.SetFieldValue(pTag,"TOWN_SEQ",out sValue);
				break;
			case "$GAME_TOWN_NM":
				this.parseMan.SetFieldValue(pTag,"TOWN_NM",out sValue);
				break;
			case "$GAME_TOWN_NM_EXTRA":
				this.parseMan.SetFieldValue(pTag,"TOWN_NM_EXTRA",out sValue);
				break;
			case "$GAME_TOWN_EXP":
				this.parseMan.SetFieldValue(pTag,"EXP",out sValue);
				break;
			case "$GAME_TOWN_LOSS_FORCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"LOSS_FORCE_COUNT",out sValue);
				break;
			case "$GAME_TOWN_INCOME_MIN":
				this.parseMan.SetFieldValue(pTag,"INCOME_MIN",out sValue);
				break;
			case "$GAME_TOWN_INCOME_MAX":
				this.parseMan.SetFieldValue(pTag,"INCOME_MAX",out sValue);
				break;
			case "$GAME_TOWN_TARGET_COUNT":
				this.parseMan.SetFieldValue(pTag,"TARGET_COUNT",out sValue);
				break;
			case "$GAME_TOWN_EXEC_COUNT":
				this.parseMan.SetFieldValue(pTag,"EXEC_COUNT",out sValue);
				break;
			case "$GAME_TOWN_CLEAR_FLAG":
				this.parseMan.SetFieldValue(pTag,"TOWN_CLEAR_FLAG",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_SEQ01":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_SEQ01",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_SEQ02":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_SEQ02",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_SEQ03":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_SEQ03",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_COUNT01":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_COUNT01",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_COUNT02":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_COUNT02",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_COUNT03":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_COUNT03",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_POSSESSION_FLAG":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_POSSESSION_FLAG",out sValue);
				break;
			case "$GAME_TOWN_USED_ITEM_NUM":
				this.parseMan.SetFieldValue(pTag,"USED_ITEM_NUM",out sValue);
				break;
			case "$CLEAR_TOWN_USED_ITEM_PARTS":
				sValue = this.GetClearTownUsedItem(pTag,pArgument);
				break;
			case "$NOT_POSSESSION_ITEM_SUM_PRICE":
				sValue = this.GetNotPossessionItemSumPrice(pTag,pArgument);
				break;
			case "$GAME_BOSS_NM":
				this.parseMan.SetFieldValue(pTag,"BOSS_NM",out sValue);
				break;
			case "$TOWN_CLEAR_GET_ITEM_FLAG":
				this.parseMan.SetFieldValue(pTag,"TOWN_CLEAR_GET_ITEM_FLAG",out sValue);
				break;
			case "$TOWN_CLEAR_GET_ITEM_SEQ":
				this.parseMan.SetFieldValue(pTag,"TOWN_CLEAR_GET_ITEM_SEQ",out sValue);
				break;
			case "$GAME_CLEAR_TOWN_GET_ITEM_NO":
				this.parseMan.SetFieldValue(pTag,"CLEAR_TOWN_GET_ITEM_NO",out sValue);
				break;
			case "$GAME_STAGE_GROUP_TYPE":
				this.parseMan.SetFieldValue(pTag,"STAGE_GROUP_TYPE",out sValue);
				break;
			case "$GAME_STAGE_GROUP_NM":
				this.parseMan.SetFieldValue(pTag,"STAGE_GROUP_NM",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetClearTownUsedItem(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		DataSet oDataSet;
		string sStageSeq = oPartsArgs.Query.Get("stage_seq");
		string sTownSeq = oPartsArgs.Query.Get("town_seq");

		if (string.IsNullOrEmpty(sStageSeq) || string.IsNullOrEmpty(sTownSeq)) {
			return string.Empty;
		}

		using (ClearTownUsedItem oClearTownUsedItem = new ClearTownUsedItem()) {
			oDataSet = oClearTownUsedItem.GetList(
				this.parseMan.sessionMan.userMan.siteCd,
				this.parseMan.sessionMan.sexCd,
				this.parseMan.sessionMan.userMan.userSeq,
				this.parseMan.sessionMan.userMan.userCharNo,
				sStageSeq,
				sTownSeq
			);
		}

		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_ITEM);
	}

	private string GetNotPossessionItemSumPrice(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		string[] arrText = pArgument.Split(',');

		if (arrText.Length == 2) {
			string sStageSeq = arrText[0];
			string sTownSeq = arrText[1];
			int iPrice = 0;

			using (ClearTownUsedItem oClearTownUsedItem = new ClearTownUsedItem()) {
				oDataSet = oClearTownUsedItem.GetList(
					this.parseMan.sessionMan.userMan.siteCd,
					this.parseMan.sessionMan.sexCd,
					this.parseMan.sessionMan.userMan.userSeq,
					this.parseMan.sessionMan.userMan.userCharNo,
					sStageSeq,
					sTownSeq
				);
			}

			foreach (DataRow dr in oDataSet.Tables[0].Rows) {
				if (int.Parse(dr["USED_COUNT"].ToString()) > int.Parse(dr["POSSESSION_COUNT"].ToString())) {
					iPrice = iPrice + int.Parse(dr["PRICE"].ToString()) * (int.Parse(dr["USED_COUNT"].ToString()) - int.Parse(dr["POSSESSION_COUNT"].ToString()));
				}
			}

			sValue = iPrice.ToString();
		}

		return sValue;
	}

	private string GetNextStage(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		TownSeekCondition oCondition = new TownSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.SexCd = this.parseMan.sessionMan.sexCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		
		DataSet oDataSet;

		using (Town oTown = new Town()) {
			oDataSet = oTown.GetNextTown(oCondition);
		}

		if (oDataSet == null) {
			return string.Empty;
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_TOWN);
	}

	private string GetClearTownSeqData(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		string sStageSeq = oPartsArgs.Query.Get("stage_seq");
		string sItemSeq = oPartsArgs.Query.Get("item_seq");
		string sClearTownGetItemNo = oPartsArgs.Query.Get("clear_town_get_item_no");

		DataSet oDataSet;

		using (GameItem oGameItem = new GameItem()) {
			oDataSet = oGameItem.GetClearTownSeqData(this.parseMan.sessionMan.userMan.siteCd,sItemSeq,sStageSeq);
		}

		if (oDataSet == null) {
			return string.Empty;
		}
		DataColumn col = new DataColumn("CLEAR_TOWN_GET_ITEM_NO",System.Type.GetType("System.String"));
		oDataSet.Tables[0].Columns.Add(col);
		oDataSet.Tables[0].Rows[0]["CLEAR_TOWN_GET_ITEM_NO"] = sClearTownGetItemNo;

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_TOWN);
	}
	
	private string GetGameTutorialTownSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		TownSeekCondition oCondition = new TownSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.SexCd = this.parseMan.sessionMan.sexCd;
		
		using(Town oTown = new Town()) {
			sValue = oTown.GetGameTutorialTownSeq(oCondition);
		}
		
		return sValue;
	}
}
