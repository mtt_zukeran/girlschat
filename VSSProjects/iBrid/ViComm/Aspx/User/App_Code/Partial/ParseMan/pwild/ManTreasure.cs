﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class PwParseMan {
	private string ParseManTreasure(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		switch (pTag) {
			case "$PIC_TITLE":
				this.parseMan.SetFieldValue(pTag,"PIC_TITLE",out sValue);
				break;
			case "$PIC_DOC":
				this.parseMan.SetFieldValue(pTag,"PIC_DOC",out sValue);
				break;
			case "$MAN_TREASURE_USED_TRAP_COUNT":
				this.parseMan.SetFieldValue(pTag,"USED_TRAP_COUNT",out sValue);
				break;
			case "$CAST_GAME_PIC_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_GAME_PIC_SEQ",out sValue);
				break;
			case "$CAST_GAME_PIC_ATTR_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_GAME_PIC_ATTR_SEQ",out sValue);
				break;
			case "$CAST_GAME_PIC_ATTR_NM":
				this.parseMan.SetFieldValue(pTag,"CAST_GAME_PIC_ATTR_NM",out sValue);
				break;
			case "$MAN_TREASURE_POSSESSION_COUNT_BY_ATTR":
				this.parseMan.SetFieldValue(pTag,"POSSESSION_COUNT_SUMMARY",out sValue);
				break;
			case "$OBJ_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$OBJ_BLUR_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"OBJ_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$OBJ_SMALL_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$OBJ_SMALL_BLUR_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"OBJ_SMALL_BLUR_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$DIV_IS_MOSAIC_ERASE": {
					string sMosaicEraseFlag = string.Empty;
					this.parseMan.SetFieldValue(pTag,"MOSAIC_ERASE_FLAG",out sMosaicEraseFlag);
					this.parseMan.SetNoneDisplay(!sMosaicEraseFlag.Equals(ViCommConst.FLAG_ON_STR));
				}
				break;
			case "$DIV_IS_NOT_MOSAIC_ERASE": {
					string sMosaicEraseFlag = string.Empty;
					this.parseMan.SetFieldValue(pTag,"MOSAIC_ERASE_FLAG",out sMosaicEraseFlag);
					this.parseMan.SetNoneDisplay(!sMosaicEraseFlag.Equals(ViCommConst.FLAG_OFF_STR));
				}
				break;
			case "$MAN_TREASURE_NOT_POSSESSION_ATTR_COUNT": {
					sValue = this.GetNotPossessionAttrCount(pTag,pArgument);
				}
				break;
			case "$PREV_MAN_TREASURE_DISPLAY_DAY": {
					sValue = this.GetManTreasureDisplayDay(pTag,-1);
				}
				break;
			case "$NEXT_MAN_TREASURE_DISPLAY_DAY": {
					sValue = this.GetManTreasureDisplayDay(pTag,1);
				}
				break;

			case "$CAST_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$CAST_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_GAME_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$MAN_TREASURE_POSSESSION_COUNT":
				this.parseMan.SetFieldValue(pTag,"POSSESSION_COUNT",out sValue);
				break;
			case "$MAN_TREASURE_DISPLAY_DAY":
				sValue = this.GetManTreasureDisplayDay(pTag,0);
				break;
			case "$MAN_TREASURE_REST_ATTR_COUNT_BY_DAY":
				this.parseMan.SetFieldValue(pTag,"REST_ATTR_COUNT_BY_DAY",out sValue);
				break;
			case "$ITEM_COUNT":
				this.parseMan.SetFieldValue(pTag,"ITEM_COUNT",out sValue);
				break;
			case "$ATTACK_POWER":
				this.parseMan.SetFieldValue(pTag,"ATTACK_POWER",out sValue);
				break;
			case "$DEFENCE_POWER":
				this.parseMan.SetFieldValue(pTag,"DEFENCE_POWER",out sValue);
				break;
			case "$GAME_ITEM_NM":
				this.parseMan.SetFieldValue(pTag,"GAME_ITEM_NM",out sValue);
				break;
			case "$GAME_ITEM_SEQ":
				this.parseMan.SetFieldValue(pTag,"GAME_ITEM_SEQ",out sValue);
				break;
			case "$COMPLETE_COUNT":
				this.parseMan.SetFieldValue(pTag,"COMPLETE_COUNT",out sValue);
				break;
			case "$CAST_USER_MOVIE_SEQ":
				this.parseMan.SetFieldValue(pTag,"MOVIE_SEQ",out sValue);
				break;
			case "$CAST_USER_MOVIE_DOC":
				this.parseMan.SetFieldValue(pTag,"MOVIE_DOC",out sValue);
				break;
			case "$CAST_USER_UPLOAD_DATE":
				this.parseMan.SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$FRIENDLY_RANK":
				this.parseMan.SetFieldValue(pTag,"FRIENDLY_RANK",out sValue);
				break;
			case "$COMP_CALENDAR_PARTS":
				sValue = this.GetListCompCalendar(pTag,pArgument);
				break;
			case "$COMP_COUNT_MAN_TREASURE_GET_ITEM_PARTS":
				sValue = this.GetListCompCountGetItem(pTag,pArgument);
				break;
			case "$COMP_MAN_TEASURE_GET_ITEM_PARTS":
				sValue = this.GetListCompManTerasureGetItem(pTag,pArgument);
				break;
			case "$CAST_NA_FLAG":
				this.parseMan.SetFieldValue(pTag,"CAST_NA_FLAG",out sValue);
				break;
			case "$GAME_ITEM_MOSAIC_ERASE_PRICE":
				sValue = GetGameItemValue("PRICE");
				break;
			case "$DISPLAY_DAY":
				this.parseMan.SetFieldValue(pTag,"DISPLAY_DAY",out sValue);
				break;
			case "$CAST_TREASURE_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_TREASURE_SEQ",out sValue);
				break;
			case "$CAST_TREASURE_NM":
				this.parseMan.SetFieldValue(pTag,"CAST_TREASURE_NM",out sValue);
				break;
			case "$GAME_TREASURE_COMPLETE_FLAG":
				this.parseMan.SetFieldValue(pTag,"TREASURE_COMPLETE_FLAG",out sValue);
				break;
			case "$DIV_IS_COMPLETE":
				this.parseMan.SetNoneDisplay(!CheckManTreasureComplete(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_COMPLETE":
				this.parseMan.SetNoneDisplay(CheckManTreasureComplete(pTag,pArgument));
				break;
			case "$GAME_ITEM_GET_FLAG":
				this.parseMan.SetFieldValue(pTag,"ITEM_GET_FLAG",out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.parseMan.GetQueryCastProfile(pTag);
				break;
			case "$PRIORITY":
				this.parseMan.SetFieldValue(pTag,"PRIORITY",out sValue);
				break;
			case "$POSSESSION_FLAG":
				this.parseMan.SetFieldValue(pTag,"POSSESSION_FLAG",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"CAST_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$SMALL_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$GAME_SMALL_PHOTO_IMG_PATH":
				sValue = GetGameSmallPhotoImgPath(pTag,pArgument);
				break;
			case "$PARTNER_USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"PARTNER_USER_SEQ",out sValue);
				break;
			case "$PARTNER_USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"PARTNER_USER_CHAR_NO",out sValue);
				break;
			case "$PARTNER_GAME_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"PARTNER_GAME_HANDLE_NM",out sValue);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GAME_CHARACTER_LEVEL":
				this.parseMan.SetFieldValue(pTag,"GAME_CHARACTER_LEVEL",out sValue);
				break;
			case "$GAME_CHARACTER_TYPE":
				this.parseMan.SetFieldValue(pTag,"GAME_CHARACTER_TYPE",out sValue);
				break;
			case "$FELLOW_COUNT":
				this.parseMan.SetFieldValue(pTag,"FELLOW_COUNT",out sValue);
				break;
			case "$MAX_ATTACK_FORCE_COUNT":
				this.parseMan.SetFieldValue(pTag,"MAX_ATTACK_FORCE_COUNT",out sValue);
				break;
			case "$TREASURE_TUTORIAL_MISSION_FLAG":
				this.parseMan.SetFieldValue(pTag,"TREASURE_TUTORIAL_MISSION_FLAG",out sValue);
				break;
			case "$TREASURE_TUTORIAL_BATTLE_FLAG":
				this.parseMan.SetFieldValue(pTag,"TREASURE_TUTORIAL_BATTLE_FLAG",out sValue);
				break;
			case "$TUTORIAL_FLAG":
				this.parseMan.SetFieldValue(pTag,"TUTORIAL_FLAG",out sValue);
				break;
			case "$TUTORIAL_BATTLE_FLAG":
				this.parseMan.SetFieldValue(pTag,"TUTORIAL_BATTLE_FLAG",out sValue);
				break;
			case "$CAST_LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			//コンプカレンダー
			case "$SELF_COMPLETE_COUNT":
				this.parseMan.SetFieldValue(pTag,"SELF_COMPLETE_COUNT",out sValue);
				break;
			default:
				pParsed = false;
				break;

		}
		return sValue;
	}

	private string GetNotPossessionAttrCount(string pTag,string pArgument) {

		int iValue = 0;

		if (this.parseMan.dataTable[this.parseMan.currentTableIdx].Rows.Count > 0) {
			iValue = int.Parse(PwViCommConst.MAN_TREASURE_ATTR_NUM);
			foreach (DataRow dr in this.parseMan.dataTable[this.parseMan.currentTableIdx].Rows) {
				if (!dr["POSSESSION_COUNT_SUMMARY"].ToString().Equals("0")) {
					iValue -= 1;
				}
			}
		}
		if (iValue < 0) {
			iValue = 0;
		}
		
		return iValue.ToString();
	}

	private string GetListCompManTerasureGetItem(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		CompManTreasureGetItemSeekCondition oCondition = new CompManTreasureGetItemSeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_TREASURE_ATTR;
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.DisplayDay = this.parseMan.sessionMan.requestQuery.QueryString["display_day"];
		if (string.IsNullOrEmpty(oCondition.DisplayDay)) {
			oCondition.DisplayDay = DateTime.Now.ToString("yyyy/MM/dd");
		}else if (DateTime.Parse(oCondition.DisplayDay) > DateTime.Today) {
			oCondition.DisplayDay = DateTime.Now.ToString("yyyy/MM/dd");
		}
		DataSet oTmpDataSet = null;
		using (CompManTreasureGetItem oCompManTreasureGetItem = new CompManTreasureGetItem()) {
			oTmpDataSet = oCompManTreasureGetItem.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}
		if (oTmpDataSet == null) {
			return string.Empty;
		}
		string iPrevSeekMode = this.parseMan.sessionObjs.seekMode;
		SeekConditionBase oPrevSeekConditionBase = this.parseMan.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			this.parseMan.sessionObjs.seekMode = oCondition.SeekMode.ToString();
			this.parseMan.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
		} finally {
			this.parseMan.sessionObjs.seekMode = iPrevSeekMode;
			this.parseMan.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}

	private string GetManTreasureDisplayDay(string pTag,int pAddDay) {
		string sDisplayDay = string.Empty;
		ManTreasureAttrSeekCondition oManTreasureAttrCondition = this.parseMan.sessionMan.seekConditionEx as ManTreasureAttrSeekCondition;

		if (oManTreasureAttrCondition != null) {
			sDisplayDay = oManTreasureAttrCondition.DisplayDay;
		} else {
			this.parseMan.SetFieldValue(pTag,"DISPLAY_DAY",out sDisplayDay);
		}

		if (!string.IsNullOrEmpty(sDisplayDay)) {
			sDisplayDay = DateTime.Parse(sDisplayDay).AddDays(pAddDay).ToString("yyyy/MM/dd");
		}

		return sDisplayDay;
	}

	private string GetListCompCalendar(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		ManTreasureCompCalendarSeekCondition oCondition = new ManTreasureCompCalendarSeekCondition(oPartsArgs.Query);
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_COMP_CALENDAR;
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.TreasureCompleteFlag = ViCommConst.FLAG_OFF_STR;
		if (string.IsNullOrEmpty(oCondition.Sort)) {
			oCondition.Sort = PwViCommConst.ManTreasureCompCalendarSort.REST_ATTR_COUNT_ASC;
		}
		DataSet oTmpDataSet = null;
		using (ManTreasureCompCalendar oManTreasureCompCalendar = new ManTreasureCompCalendar()) {
			oTmpDataSet = oManTreasureCompCalendar.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}
		if (oTmpDataSet == null) {
			return string.Empty;
		}
		string iPrevSeekMode = this.parseMan.sessionObjs.seekMode;
		SeekConditionBase oPrevSeekConditionBase = this.parseMan.sessionObjs.seekConditionEx;
		string sValue = string.Empty;
		try {
			this.parseMan.sessionObjs.seekMode = oCondition.SeekMode.ToString();
			this.parseMan.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
		} finally {
			this.parseMan.sessionObjs.seekMode = iPrevSeekMode;
			this.parseMan.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}
		return sValue;
	}

	private string GetListCompCountGetItem(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		ManTreasureCompCountGetItemSeekCondition oCondition = new ManTreasureCompCountGetItemSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (ManTreasureCompCountGetItem oManTreasureCompCountGetItem = new ManTreasureCompCountGetItem()) {
			oTmpDataSet = oManTreasureCompCountGetItem.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}
		if (oTmpDataSet == null) {
			return string.Empty;
		}
		string iPrevSeekMode = this.parseMan.sessionObjs.seekMode;
		SeekConditionBase oPrevSeekConditionBase = this.parseMan.sessionObjs.seekConditionEx;
		try {
			this.parseMan.sessionObjs.seekMode = PwViCommConst.INQUIRY_GAME_COMP_CALENDAR.ToString();
			this.parseMan.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
		} finally {
			this.parseMan.sessionObjs.seekMode = iPrevSeekMode;
			this.parseMan.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}
		return sValue;
	}

	private string GetGetManTreasureLog(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		string sGetTreasureLogSeq = oPartsArgs.Query["get_treasure_log_seq"].ToString();

		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oTmpDataSet = oGetManTreasureLog.GetOne(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				this.parseMan.sessionMan.userMan.userCharNo,
				sGetTreasureLogSeq
			);
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);

		return sValue;		
	}

	private string GetListManTreasure(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument pPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		ManTreasureSeekCondition oCondition = new ManTreasureSeekCondition(pPartsArgs.Query);
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_TREASURE;
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		DataSet oTmpDataSet = null;
		ManTreasure oManTreasure = new ManTreasure();
		oTmpDataSet = oManTreasure.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);

		if (oTmpDataSet == null)
			return string.Empty;

		return this.parseMan.parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
	}

	private string GetTotalPossessionManTreasureCount(string pTag,string pArgument) {
		string sValue = string.Empty;

		ManTreasureSeekCondition oCondition = new ManTreasureSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		
		using (ManTreasure oManTreasure = new ManTreasure()) {
			sValue = oManTreasure.GetTotalPossessionManTreasureCount(oCondition);
		}

		return sValue;
	}

	private string GetListAllCompCalendar(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		DateTime dtGameStartDate;

		using (SocialGame oSocialGame = new SocialGame()) {
			dtGameStartDate = oSocialGame.GetGameStartDate(this.parseMan.sessionMan.userMan.siteCd);
		}

		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		ManTreasureCompCalendarSeekCondition oCondition = new ManTreasureCompCalendarSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.TreasureCompleteFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.StartDisplayDay = dtGameStartDate.ToString("yyyy/MM/dd");
		oCondition.EndDisplayDay = DateTime.Now.ToString("yyyy/MM/dd");

		if (string.IsNullOrEmpty(oCondition.Sort)) {
			oCondition.Sort = PwViCommConst.ManTreasureCompCalendarSort.REST_ATTR_COUNT_ASC;
		}

		using (ManTreasureCompCalendar oManTreasureCompCalendar = new ManTreasureCompCalendar()) {
			oTmpDataSet = oManTreasureCompCalendar.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);

		return sValue;
	}

	private string GetGameItemValue(string pItemDistinction) {
		string sValue = string.Empty;

		using (GameItem oGameItem = new GameItem()) {
			sValue = oGameItem.GetGameItem(this.parseMan.sessionMan.userMan.siteCd,this.parseMan.sessionMan.sexCd,PwViCommConst.GameItemCategory.ITEM_CATEGOAY_TOOL,pItemDistinction);
		}

		return sValue;
	}
	private string GetCastTreasureData(string pTag,string pArgument) {

		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		CastTreasureSeekCondition oCondition = new CastTreasureSeekCondition(oPartsArgs.Query);
		if (string.IsNullOrEmpty(oCondition.CastTreasureSeq)) {
			return string.Empty;
		}
		oCondition.SiteCd = this.parseMan.sessionMan.userMan.siteCd;

		DataSet oTmpDataSet = null;
		CastTreasure oCastTreasure = new CastTreasure();
		oTmpDataSet = oCastTreasure.GetOneByCastTreasureSeq(oCondition);

		if (oTmpDataSet == null) {
			return string.Empty;
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);
	}
	
	private bool CheckManTreasureComplete(string pTag,string pArgument) {
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		string sDisplayDay = pArgument;
		
		DataSet oDS = null;
		
		using (ManTreasure oManTreasure = new ManTreasure()) {
			oDS = oManTreasure.GetManTreasureCompleteLogByDisplayDay(sSiteCd,sUserSeq,sUserCharNo,sDisplayDay);
		}
		
		if(oDS.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	private string GetGetManTreasureLogTutorial(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oTmpDataSet = oGetManTreasureLog.GetOneForTutorial(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				this.parseMan.sessionMan.userMan.userCharNo
			);
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_GAME_TREASURE);

		return sValue;
	}
	
	private string GetManTreasureCompStatus(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		PossessionManTreasureSeekCondition oCondition = new PossessionManTreasureSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		
		using (PossessionManTreasure oPossessionManTreasure = new PossessionManTreasure()) {
			oTmpDataSet = oPossessionManTreasure.GetManTreasureCompStatus(oCondition);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseMan.sessionObjs.seekConditionEx;
		try {
			this.parseMan.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_GAME_TREASURE);
		} finally {
			this.parseMan.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
	
	private string GetCastGamePicAttrNm(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		
		using (ManTreasureAttr oManTreasureAttr = new ManTreasureAttr()) {
			sValue = oManTreasureAttr.GetCastGamePicAttrNmBySeq(sSiteCd,pArgument);
		}
		
		return sValue;
	}

	private string GetGameTutorialBattleTreasureAttrSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sDisplayDay = DateTime.Now.ToString("yyyy/MM/dd");
		
		using (ManTreasure oManTreasure = new ManTreasure()) {
			oDataSet = oManTreasure.GetOneTutorialBattle(sSiteCd,sDisplayDay);
		}
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["CAST_GAME_PIC_ATTR_SEQ"].ToString();
		}
		
		return sValue;
	}
}
