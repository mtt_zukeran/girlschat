﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルつぶやき検索結果
--	Progaram ID		: CastTweet
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

partial class PwParseMan {
	private string ParseCastTweet(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_TWEET_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_SEQ",out sValue);
				break;
			case "$TWEET_TEXT":
				this.parseMan.SetFieldValue(pTag,"TWEET_TEXT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$TWEET_DATE":
				this.parseMan.SetFieldValue(pTag,"TWEET_DATE",out sValue);
				break;
			case "$LIKE_COUNT":
				this.parseMan.SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				this.parseMan.SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$CAST_TWEET_PIC_SEQ":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_PIC_SEQ",out sValue);
				break;
			case "$CAST_TWEET_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_TWEET_SMALL_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"CAST_TWEET_SMALL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SELF_CAST_TWEET_LIKED_FLAG":
				sValue = this.GetSelfCastTweetLikedFlag(pTag,pArgument);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.parseMan.GetQueryCastProfile(pTag);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				this.parseMan.SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$SELF_FANCLUB_MEMBER_RANK": {
				string[] sValues;
				if (this.parseMan.GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
					sValue = this.parseMan.GetSelfFanClubMemberRank(sValues[0],sValues[1]);
				}
				break;
			}
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetSelfCastTweetLikedFlag(string pTag,string pArgument) {
		string sLikedFlag = ViCommConst.FLAG_OFF_STR;
		string[] sValues;
		if (parseMan.GetValues(pTag,"CAST_TWEET_SEQ",out sValues)) {
			decimal dRecCount;
			CastTweetLikeSeekCondition oCondition = new CastTweetLikeSeekCondition();
			oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
			oCondition.UserSeq = parseMan.sessionMan.userMan.userSeq;
			oCondition.UserCharNo = parseMan.sessionMan.userMan.userCharNo;
			oCondition.CastTweetSeq = sValues[0];
			using (CastTweetLike oCastTweetLike = new CastTweetLike(parseMan.sessionObjs)) {
				oCastTweetLike.GetPageCount(oCondition,1,out dRecCount);
			}

			if (dRecCount >= 1) {
				sLikedFlag = ViCommConst.FLAG_ON_STR;
			}
		}

		return sLikedFlag;
	}

	private string GetCastTweet(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		CastTweetSeekCondition oCondition = new CastTweetSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
		oCondition.SelfUserSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.SelfUserCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.OnlyFanClubFlag = ViCommConst.FLAG_OFF_STR;

		using (CastTweet oCastTweet = new CastTweet(parseMan.sessionObjs)) {
			oTmpDataSet = oCastTweet.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseMan.sessionObjs.seekConditionEx;
		try {
			this.parseMan.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_CAST_TWEET);
		} finally {
			this.parseMan.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
}