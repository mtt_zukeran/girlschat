﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseAttrType(string pTag,string pArgument,out bool pParsed) {
		string sValue = "";
		pParsed = true;

		switch (pTag) {
			case "$CAST_FIND_ATTR_NM":
				SetFieldValue(pTag,"CAST_ATTR_NM",out sValue);
				break;
			case "$CAST_FIND_ATTR_TYPE_NM":
				sValue = GetFindAttrTypeNm();
				break;
			case "$CAST_FIND_GROUP_NM":
				SetFieldValue(pTag,"GROUPING_NM",out sValue);
				break;
			case "$QUERY_SELECT_ATTR_TYPE":
				sValue = GetQuerySelectAttrType(pTag);
				break;
			case "$QUERY_FIND_GROUP_RESULT":
				sValue = GetQueryFindGroupResult(pTag);
				break;
			case "$QUERY_FIND_ATTR_TYPE_RESULT":
				sValue = GetQueryFindAttrTypeResult(pTag);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryFindAttrTypeResult(string pTag) {
		string sQuery = "";
		string[] sValues;
		if (GetValues(pTag,"CAST_ATTR_SEQ,CAST_ATTR_TYPE_SEQ",out sValues)) {
			sQuery = string.Format("category={0}&online={1}&item01={2}&typeseq01={3}",
							iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["category"]),
							iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["online"]),
							sValues[0],
							sValues[1]
							);
		}
		return sQuery;
	}

	private string GetQuerySelectAttrType(string pTag) {
		string sQuery = "";
		string[] sValues;
		if (GetValues(pTag,"ITEM_NO,GROUPING_CD",out sValues)) {
			sQuery = string.Format("category={0}&online={1}&itemno={2}&group={3}",
						iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["category"]),
						iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["online"]),
							sValues[0],
							sValues[1]
							);
		}
		return sQuery;
	}

	private string GetQueryFindGroupResult(string pTag) {
		string sQuery = "";
		string[] sValues;
		if (GetValues(pTag,"GROUPING_CD,CAST_ATTR_TYPE_SEQ",out sValues)) {
			sQuery = string.Format("category={0}&online={1}&item01={2}&typeseq01={3}&group01={4}",
						iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["category"]),
						iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["online"]),
						sValues[0],
						sValues[1],
						ViCommConst.FLAG_ON
						);
		}
		return sQuery;
	}

	private string GetFindAttrTypeNm() {
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			oCastAttrType.GetOneByItemNo(sessionMan.site.siteCd,iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["itemno"]));
			return oCastAttrType.attrTypeNm;
		}
	}


}
