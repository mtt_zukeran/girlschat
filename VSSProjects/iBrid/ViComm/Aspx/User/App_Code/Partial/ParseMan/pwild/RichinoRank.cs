﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: リッチーノランク検索結果
--	Progaram ID		: RichinoRank
--  Creation Date	: 2016.06.02
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseRichinoRank(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$SELF_REST_RANK_KEEP_AMT":
				this.parseMan.SetFieldValue(pTag,"REST_RICHINO_RANK_KEEP_AMT",out sValue);
				break;
			case "$SELF_RANK_KEEP_CLOSE_DATE":
				this.parseMan.SetFieldValue(pTag,"RANK_KEEP_CLOSE_DATE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}