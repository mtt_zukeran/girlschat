﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

partial class ParseMan {	
	private string ParseBlog(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		
		switch (pTag) {
			case "$BLOG_SEQ":
				SetFieldValue(pTag,"BLOG_SEQ",out sValue);
				break;
			case "$BLOG_TITLE":
				SetFieldValue(pTag, "BLOG_TITLE", out sValue);
				break;
			case "$BLOG_DOC":
				SetFieldValue(pTag, "BLOG_DOC", out sValue);
				sValue = sValue.Replace(Environment.NewLine, "<br />");
				break;
			case "$BLOG_LIKE_COUNT":
				SetFieldValue(pTag,"BLOG_LIKE_COUNT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		
		return sValue;
	}
}
