﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳画像検索結果
--	Progaram ID		: YakyukenPic
--  Creation Date	: 2013.05.03
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseYakyukenPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.parseMan.GetQueryCastProfile(pTag);
				break;
			case "$CAST_LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			//野球拳画像
			case "$YAKYUKEN_PIC_SEQ":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_PIC_SEQ",out sValue);
				break;
			case "$YAKYUKEN_GAME_SEQ":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_GAME_SEQ",out sValue);
				break;
			case "$YAKYUKEN_TYPE":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_TYPE",out sValue);
				break;
			case "$COMMENT_TEXT":
				this.parseMan.SetFieldValue(pTag,"COMMENT_TEXT",out sValue);
				break;
			case "$YAKYUKEN_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$YAKYUKEN_SMALL_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			//野球拳進捗
			case "$SELF_LAST_WIN_YAKYUKEN_TYPE":
				this.parseMan.SetFieldValue(pTag,"LAST_WIN_YAKYUKEN_TYPE",out sValue);
				break;
			case "$SELF_CLEAR_FLAG":
				this.parseMan.SetFieldValue(pTag,"CLEAR_FLAG",out sValue);
				break;
			case "$SELF_CLEAR_YAKYUKEN_TYPE":
				this.parseMan.SetFieldValue(pTag,"CLEAR_YAKYUKEN_TYPE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}