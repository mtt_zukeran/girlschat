﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseSelfPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$HREF_SELF_MAIL_SEND_IMG":
				sValue = GetHRefMailSendImg(pTag,pArgument);
				break;
			case "$HREF_VIEW_STOCK_PIC":
				sValue = GetHRefViewStockImg(pTag,pArgument);
				break;
			case "$UNAUTH_SELF_VIEW_STOCK_PIC":
				sValue = GetUnauthSelfViewStockPic(pTag,pArgument);
				break;
			case "$STOCK_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				break;
			case "$SELF_PIC_UNAUTH_FLAG":
				SetFieldValue(pTag,"OBJ_NOT_APPROVE_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetHRefMailSendImg(string pTag,string pText) {
		int iIdx;
		string sWidth,sAccessKey;
		if (ParseImageArguement(pTag,pText,out iIdx,out sWidth,out sAccessKey) == false) {
			return "";
		}
		string sHerf = "";
		string sPhoto = "";
		if (sessionMan.datasetRecCount >= iIdx) {
			sHerf = sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,
							string.Format("TxMailConfirm.aspx?picseq={0}&data={1}&pageno={2}&mailtype={3}",
								dataTable[this.currentTableIdx].Rows[iIdx - 1]["PIC_SEQ"].ToString(),
								iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"]),
								iIdx,
								iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["mailtype"]))

			);
			sPhoto = dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
			return string.Format("<a href=\"{0}\"><img src=\"{1}\" width=\"{2}\"; alt=\"\" hspace=\"2\" vspace=\"2\" /></a>",sHerf,sPhoto,sWidth);
		} else {
			sPhoto = string.Format("image/{0}/photo_men1.gif",sessionMan.site.siteCd);
			return string.Format("<img src=\"{0}\" width=\"{1}\"; alt=\"\"  hspace=\"2\" vspace=\"2\" />",sPhoto,sWidth);
		}
	}

	private string GetHRefViewStockImg(string pTag,string pText) {
		int iIdx;
		string sWidth,sAccessKey;
		if (ParseImageArguement(pTag,pText,out iIdx,out sWidth,out sAccessKey) == false) {
			return "";
		}
		string sHerf = "";
		string sPhoto = "";
		if (sessionMan.datasetRecCount >= iIdx) {
			sHerf = sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,
						string.Format("ViewMailPicConf.aspx?pageno={0}&picseq={1}",
							iIdx,
							dataTable[this.currentTableIdx].Rows[iIdx - 1]["PIC_SEQ"].ToString()
						)
			);
			sPhoto = dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
			return string.Format("<a href=\"{0}\"><img src=\"{1}\" width=\"{2}\"; alt=\"\" hspace=\"2\" vspace=\"2\" /></a>",sHerf,sPhoto,sWidth);
		} else {
			sPhoto = string.Format("image/{0}/photo_men1.gif",sessionMan.site.siteCd);
			return string.Format("<img src=\"{0}\" width=\"{1}\"; alt=\"\"  hspace=\"2\" vspace=\"2\" />",sPhoto,sWidth);
		}
	}

	private string GetUnauthSelfViewStockPic(string pTag,string pArgument) {
		int iIdx = int.Parse(pArgument);
		if (sessionMan.datasetRecCount >= iIdx) {
			return dataTable[this.currentTableIdx].Rows[iIdx - 1]["OBJ_NOT_APPROVE_FLAG"].ToString();
		} else {
			return string.Empty;
		}
	}
}
