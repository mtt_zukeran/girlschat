﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ソーシャルゲーム クエストエントリー履歴検索結果
--	Progaram ID		: QuestEntryLog
--  Creation Date	: 2012.10.16
--  Creater			: PW Y.Ikemiya
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseQuestEntryLog(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$QUEST_SEQ":
				this.parseMan.SetFieldValue(pTag,"QUEST_SEQ",out sValue);
				break;
			case "$LEVEL_QUEST_SEQ":
				this.parseMan.SetFieldValue(pTag,"LEVEL_QUEST_SEQ",out sValue);
				break;
			case "$QUEST_LEVEL":
				this.parseMan.SetFieldValue(pTag,"QUEST_LEVEL",out sValue);
				break;
			case "$LEVEL_QUEST_SUB_SEQ":
				this.parseMan.SetFieldValue(pTag,"LEVEL_QUEST_SUB_SEQ",out sValue);
				break;
			case "$LITTLE_QUEST_SEQ":
				this.parseMan.SetFieldValue(pTag,"LITTLE_QUEST_SEQ",out sValue);
				break;
			case "$LITTLE_QUEST_NM":
				this.parseMan.SetFieldValue(pTag,"LITTLE_QUEST_NM",out sValue);
				break;
			case "$OTHER_QUEST_SEQ":
				this.parseMan.SetFieldValue(pTag,"OTHER_QUEST_SEQ",out sValue);
				break;
			case "$OTHER_QUEST_NM":
				this.parseMan.SetFieldValue(pTag,"OTHER_QUEST_NM",out sValue);
				break;
			case "$QUEST_TYPE":
				this.parseMan.SetFieldValue(pTag,"QUEST_TYPE",out sValue);
				break;
			case "$REMARKS":
				this.parseMan.SetFieldValue(pTag,"REMARKS",out sValue);
				break;
			case "$QUEST_STATUS":
				this.parseMan.SetFieldValue(pTag,"QUEST_STATUS",out sValue);
				break;
			case "$LAST_ENTRY_OK_FLAG":
				this.parseMan.SetFieldValue(pTag,"LAST_ENTRY_OK_FLAG",out sValue);
				break;
			case "$TIME_LIMIT_D":
				this.parseMan.SetFieldValue(pTag,"TIME_LIMIT_D",out sValue);
				break;
			case "$TIME_LIMIT_H":
				this.parseMan.SetFieldValue(pTag,"TIME_LIMIT_H",out sValue);
				break;
			case "$TIME_LIMIT_M":
				this.parseMan.SetFieldValue(pTag,"TIME_LIMIT_M",out sValue);
				break;
			case "$ENTRY_LOG_SEQ":
				this.parseMan.SetFieldValue(pTag,"ENTRY_LOG_SEQ",out sValue);
				break;
			case "$QUEST_EX_STATUS":
				this.parseMan.SetFieldValue(pTag,"QUEST_EX_STATUS",out sValue);
				break;
			case "$COUNT_UNIT":
				this.parseMan.SetFieldValue(pTag,"COUNT_UNIT",out sValue);
				break;
			case "$REST_COUNT":
				this.parseMan.SetFieldValue(pTag,"REST_COUNT",out sValue);
				break;
			case "$NOW_ENTRY_FLAG":
				this.parseMan.SetFieldValue(pTag,"NOW_ENTRY_FLAG",out sValue);
				break;
			case "$END_DATE":
				this.parseMan.SetFieldValue(pTag,"END_DATE",out sValue);
				break;
			case "$GET_QUEST_LEVEL_BY_LEVEL_QUEST_SEQ":
				sValue = this.GetQuestLevelByLevelQuestSeq(pTag,pArgument);
				break;
			case "$GET_NOW_QUEST_LEVEL":
				sValue = this.GetNowQuestLevel(pTag,pArgument);
				break;
			case "$QUEST_EX_REMARKS":
				this.parseMan.SetFieldValue(pTag,"QUEST_EX_REMARKS",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}