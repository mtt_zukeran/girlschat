﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・友達紹介記録検索結果
--	Progaram ID		: GameIntroLog
--
--  Creation Date	: 2012.02.08
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseGameIntroLog(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$GAME_INTRO_LOG_SEQ":
				this.parseMan.SetFieldValue(pTag,"GAME_INTRO_LOG_SEQ",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseMan.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$GAME_INTRO_SEQ":
				this.parseMan.SetFieldValue(pTag,"GAME_INTRO_SEQ",out sValue);
				break;
			case "$INTRO_COMMENT":
				this.parseMan.SetFieldValue(pTag,"INTRO_COMMENT",out sValue);
				break;
			case "$GAME_INTRO_ITEM_PARTS":
				sValue = this.GetGameIntroItem(pTag,pArgument);
				break;
			case "$REC_NO_PER_PAGE":
				this.parseMan.SetFieldValue(pTag,"REC_NO_PER_PAGE",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetGameIntroItem(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		DataSet oDataSet;
		string sGameIntroSeq = oPartsArgs.Query.Get("game_intro_seq");

		if (string.IsNullOrEmpty(sGameIntroSeq)) {
			return string.Empty;
		}

		using (GameIntroItem oGameIntroItem = new GameIntroItem()) {
			oDataSet = oGameIntroItem.GetList(
				this.parseMan.sessionMan.userMan.siteCd,
				sGameIntroSeq
			);
		}

		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_ITEM);
	}
}
