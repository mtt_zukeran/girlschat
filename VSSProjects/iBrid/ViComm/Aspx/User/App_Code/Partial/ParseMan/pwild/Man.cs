﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員検索結果
--	Progaram ID		: Man
--  Creation Date	: 2013.11.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseMan(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$MAN_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$MAN_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$MAN_LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$LIST_RNUM":
				this.parseMan.SetFieldValue(pTag,"RNUM",out sValue);
				break;

			// お宝deビンゴ
			case "$MAN_BBS_BINGO_PRIZE_AMT":
				this.parseMan.SetFieldValue(pTag,"PRIZE_AMT",out sValue);
				break;
			case "$MAN_BBS_BINGO_PRIZE_POINT":
				this.parseMan.SetFieldValue(pTag,"PRIZE_POINT",out sValue);
				break;
			case "$MAN_BBS_BINGO_PRIZE_DATE":
				this.parseMan.SetFieldValue(pTag,"PRIZE_DATE",out sValue);
				break;
			case "$MAN_BBS_BINGO_BALL_FLAG":
				this.parseMan.SetFieldValue(pTag,"BINGO_BALL_FLAG",out sValue);
				break;

			// 出演者動画コメント
			case "$CAST_MOVIE_COMMENT_DOC":
				this.parseMan.SetFieldValue(pTag,"COMMENT_DOC",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_MOVIE_COMMENT_DATE":
				this.parseMan.SetFieldValue(pTag,"COMMENT_DATE",out sValue);
				break;

			// 出演者画像コメント
			case "$CAST_PIC_COMMENT_DOC":
				this.parseMan.SetFieldValue(pTag,"COMMENT_DOC",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_PIC_COMMENT_DATE":
				this.parseMan.SetFieldValue(pTag,"COMMENT_DATE",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}