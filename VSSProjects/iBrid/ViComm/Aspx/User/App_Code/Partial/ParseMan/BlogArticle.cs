﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

partial class ParseMan {	
	private string ParseBlogArticle(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		
		if(GetDataRow(currentTableIdx) == null){
			pParsed = false;
			return string.Empty;
		}

		string[] oValues;
		string sSiteCd = string.Empty;
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		string sObjSeq = string.Empty;
		string sBlogFileType = string.Empty;
		string sBlogSeq = string.Empty;
		string sBlogArticleSeq = string.Empty;
		string sBlogArticleStatus = string.Empty;

		if (GetDataRow(currentTableIdx) != null) {
			if (!GetValues(pTag, "SITE_CD,USER_SEQ,USER_CHAR_NO,OBJ_SEQ,BLOG_FILE_TYPE,BLOG_SEQ,BLOG_ARTICLE_SEQ,BLOG_ARTICLE_STATUS", out oValues)) {
				pParsed = true;
				return string.Empty;
			}
			sSiteCd = oValues[0];
			sUserSeq = oValues[1];
			sUserCharNo = oValues[2];
			sObjSeq = oValues[3];
			sBlogFileType = oValues[4];
			sBlogSeq = oValues[5];
			sBlogArticleSeq = oValues[6];
			sBlogArticleStatus = oValues[7];
		}
		
		switch (pTag) {
			case "$DIV_IS_PIC":
				SetNoneDisplay(!(sBlogFileType.Equals(ViCommConst.BlogFileType.PIC)));
				break;
			case "$DIV_IS_MOVIE":
				SetNoneDisplay(!(sBlogFileType.Equals(ViCommConst.BlogFileType.MOVIE)));
				break;
			case "$DIV_IS_DRAFT":
				SetNoneDisplay(!(sBlogArticleStatus.Equals(ViCommConst.BlogArticleStatus.DRAFT)));
				break;
			case "$DIV_IS_NOT_DRAFT":
				SetNoneDisplay((sBlogArticleStatus.Equals(ViCommConst.BlogArticleStatus.DRAFT)));
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag, "HANDLE_NM", out sValue);
				break;
			case "$BLOG_OBJ_SEQ":
				SetFieldValue(pTag, "OBJ_SEQ", out sValue);
				break;
			case "$BLOG_ARTICLE_SEQ":
				SetFieldValue(pTag, "BLOG_ARTICLE_SEQ", out sValue);
				break;
			case "$BLOG_ARTICLE_TITLE":
				SetFieldValue(pTag, "BLOG_ARTICLE_TITLE", out sValue);
				break;
			case "$BLOG_ARTICLE_DOC":
				sValue = GetBlogArticleDoc(pTag, pArgument, sSiteCd, sUserSeq, sUserCharNo, sBlogSeq, sBlogArticleSeq);
				break;
			case "$BLOG_ARTICLE_REGIST_DATE":
				SetFieldValue(pTag, "REGIST_DATE", out sValue);
				break;
			case "$BLOG_ARTICLE_STATUS":
				SetFieldValue(pTag, "BLOG_ARTICLE_STATUS", out sValue);
				break;
			case "$BLOG_ARTICLE_STATUS_NM":
				SetFieldValue(pTag, "BLOG_ARTICLE_STATUS_NM", out sValue);
				break;
			case "$BLOG_TITLE":
				SetFieldValue(pTag, "BLOG_TITLE", out sValue);
				break;
			case "$QUERY_BLOG_ARTICLE":
				sValue = GetQueryBlogArticle(sBlogArticleSeq);
				break;
			case "$QUERY_BLOG_OBJ":
				sValue = GetQueryBlogObj(sObjSeq);
				break;
			case "$READING_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$CAST_USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$CAST_CHAR_NO":
				SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$SELF_FANCLUB_MEMBER_RANK": {
				string[] sValues;
				if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubMemberRank(sValues[0],sValues[1]);
				}
				break;
			}
			case "$SELF_BLOG_ARTICLE_LIKE_FLAG":
				sValue = GetSelfBlogArticleLikeFlag(pTag,pArgument);
				break;
			case "$BLOG_ARTICLE_LIKE_COUNT":
				SetFieldValue(pTag,"BLOG_ARTICLE_LIKE_COUNT",out sValue);
				break;
			case "$BLOG_LIKE_COUNT":
				SetFieldValue(pTag,"BLOG_LIKE_COUNT",out sValue);
				break;
			// 写真用 =====================================================================
			case "$BLOG_PIC_IMG_PATH":
				SetFieldValue(pTag, "OBJ_SMALL_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$BLOG_PIC_LARGE_IMG_PATH":
				SetFieldValue(pTag, "OBJ_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$BLOG_PIC_BLUR_IMG_PATH":
				SetFieldValue(pTag, "OBJ_BLUR_PHOTO_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			// 動画用 =====================================================================
			case "$BLOG_MOVIE_IMG_PATH":
				SetFieldValue(pTag, "THUMBNAIL_IMG_PATH", out sValue);
				sValue = "../" + sValue;
				break;
			case "$HREF_BLOG_MOVIE_DOWNLOAD":
				sValue = GetHRefBlogMovieDwonload(pTag, pArgument,sSiteCd,sUserSeq,sUserCharNo,sObjSeq);
				break;
			
			default:
				pParsed = false;
				break;
		}
		
		return sValue;
	}

	private string GetSelfBlogArticleLikeFlag(string pTag,string pArgument) {
		string sLikedFlag = ViCommConst.FLAG_OFF_STR;
		string sValue;
		if (GetValue(pTag,"BLOG_ARTICLE_SEQ",out sValue)) {
			decimal dRecCount;
			BlogArticleLikeSeekCondition oCondition = new BlogArticleLikeSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sessionMan.userMan.userSeq;
			oCondition.BlogArticleSeq = sValue;
			using (BlogArticleLike oBlogArticleLike = new BlogArticleLike(sessionObjs)) {
				oBlogArticleLike.GetPageCount(oCondition,1,out dRecCount);
			}

			if (dRecCount >= 1) {
				sLikedFlag = ViCommConst.FLAG_ON_STR;
			}
		}

		return sLikedFlag;
	}

}
