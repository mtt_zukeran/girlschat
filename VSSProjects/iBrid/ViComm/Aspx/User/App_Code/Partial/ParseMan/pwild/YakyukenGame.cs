﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳ゲーム検索結果
--	Progaram ID		: YakyukenGame
--  Creation Date	: 2013.05.02
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseYakyukenGame(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.parseMan.GetQueryCastProfile(pTag);
				break;
			case "$CAST_LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			//野球拳ゲーム
			case "$YAKYUKEN_GAME_SEQ":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_GAME_SEQ",out sValue);
				break;
			case "$SHOW_FACE_FLAG":
				this.parseMan.SetFieldValue(pTag,"SHOW_FACE_FLAG",out sValue);
				break;
			case "$COMPLETE_DATE":
				this.parseMan.SetFieldValue(pTag,"COMPLETE_DATE",out sValue);
				break;
			case "$YAKYUKEN_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$YAKYUKEN_SMALL_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"YAKYUKEN_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			//野球拳進捗
			case "$SELF_LAST_WIN_YAKYUKEN_TYPE":
				this.parseMan.SetFieldValue(pTag,"LAST_WIN_YAKYUKEN_TYPE",out sValue);
				break;
			case "$SELF_CLEAR_FLAG":
				this.parseMan.SetFieldValue(pTag,"CLEAR_FLAG",out sValue);
				break;
			//女性勝利コメント
			case "$RANDOM_YAKYUKEN_COMMENT":
				sValue = GetRandomYakyukenComment(pTag,pArgument);
				break;
			//ジャンケン画像
			case "$CAST_YAKYUKEN_JYANKEN_PIC_PARTS":
				sValue = GetCastYakyukenJyankenPic(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetRandomYakyukenComment(string pTag,string pArgument) {
		string sCommentText = string.Empty;
		string[] sValues;

		if (parseMan.GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			YakyukenCommentSeekCondition oCondition = new YakyukenCommentSeekCondition();
			oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
			oCondition.CastUserSeq = sValues[0];
			oCondition.CastCharNo = sValues[1];
			oCondition.RandomFlag = ViCommConst.FLAG_ON_STR;

			using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
				DataSet ds = oYakyukenComment.GetPageCollection(oCondition,1,1);

				if (ds.Tables[0].Rows.Count > 0) {
					sCommentText = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["COMMENT_TEXT"]);
				}
			}
		}

		return sCommentText;
	}

	private string GetCastYakyukenJyankenPic(string pTag,string pArgument) {
		string sResult = string.Empty;
		string[] sValues;

		if (parseMan.GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			DataSet oDataSet = null;
			ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
			YakyukenJyankenPicSeekCondition oCondition = new YakyukenJyankenPicSeekCondition(oPartsArgs.Query);
			oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
			oCondition.CastUserSeq = sValues[0];
			oCondition.CastCharNo = sValues[1];
			oCondition.AuthFlag = ViCommConst.FLAG_ON_STR;

			using (YakyukenJyankenPic oYakyukenJyankenPic = new YakyukenJyankenPic()) {
				oDataSet = oYakyukenJyankenPic.GetList(oCondition);
			}

			sResult = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_YAKYUKEN_JYANKEN_PIC);
		}

		return sResult;
	}
}