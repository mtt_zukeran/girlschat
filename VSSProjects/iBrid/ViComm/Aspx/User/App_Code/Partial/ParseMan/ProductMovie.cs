﻿using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private const int DEFAULT_HREF_DOWNLOAD_PRODUCT_MOVIE_COLUMN = 3;
	
	private string ParseProductMovie(string pTag, string pArgument, out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			// 商品動画のﾀﾞｳﾝﾛｰﾄﾞﾘﾝｸを表示する。p1:列数,p2:商品動画種別,p3:セパレータデザイン
			case "$HREF_DOWNLOAD_PRODUCT_MOVIE":
				sValue = this.GetHrefDownloadProductMovie(pTag, pArgument);
				break;		
			case "$PRODUCT_MOVIE_PLAY_TIME":
				SetFieldValue(pTag,"PLAY_TIME",out sValue);
				string sTmpValue = sValue;
				if (sTmpValue.Split(':').Length < 2) {
					sTmpValue = "00:00" + sTmpValue;
				}
				if (sTmpValue.Split(':').Length < 3) {
					sTmpValue = "00:" + sTmpValue;
				} 
				TimeSpan tmpMoviePlayTime;
				if(TimeSpan.TryParse(sTmpValue,out tmpMoviePlayTime)){
					sValue = tmpMoviePlayTime.ToString();
				}			
				break;
			// ｼﾞｬﾝﾙｾﾚｸﾄﾎﾞｯｸｽ
			case "$COMBO_PRODUCT_GENRE":
				// p1: 商品種別 p2: ｶﾃｺﾞﾘ表示ﾌﾗｸﾞ(1=>ｶﾃｺﾞﾘ-ｼﾞｬﾝﾙ)
				sValue = GetComboProductMovieGenre(pTag, pArgument);
				break;
				
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}


	private string GetHrefDownloadProductMovie(string pTag, string pArgument){
		string[] sArgs = pArgument.Split(',');
		int iColumn;
		
		string sUseType = ViCommConst.TARGET_USE_TYPE_DOWNLOAD;
		
		if(!int.TryParse(sArgs[0],out iColumn)){
			iColumn = DEFAULT_HREF_DOWNLOAD_PRODUCT_MOVIE_COLUMN;
		}
		
		string sProductMovieType = ParseArg(sArgs,1,ViCommConst.ProductMovieType.CONTENTS);
		
		string sSepareta = ParseArg(sArgs,2,string.Empty);
		if (!sSepareta.Equals(string.Empty)) {
			sSepareta += "<br />";
		}
		StringBuilder oValueBuilder = new StringBuilder();
		string[] sTmpValues = null;

		if (!GetValues(pTag, "LINK_ID,PRODUCT_TYPE,PRODUCT_ID,SITE_CD,PRODUCT_AGENT_CD,PRODUCT_SEQ", out sTmpValues)) {
			return string.Empty;
		}
		string sObjSeq = string.Empty;
		string sLinkId = sTmpValues[0];
		string sProductType = sTmpValues[1];
		string sProductId = sTmpValues[2];
		string sSiteCd = sTmpValues[3];
		string sProductAgentCd = sTmpValues[4];
		string sProductSeq = sTmpValues[5];
		bool bIsMaqia = string.IsNullOrEmpty(sLinkId);
		
		if(sProductMovieType.Equals(ViCommConst.ProductMovieType.SAMPLE)){
			using(ProductMovie oProductMovie = new ProductMovie(sessionMan)){
				using(DataSet oSampleDataSet = oProductMovie.GetListByType(sSiteCd,sProductAgentCd,sProductSeq,ViCommConst.ProductMovieType.SAMPLE)){
					if( oSampleDataSet.Tables[0].Rows.Count>0){
						sObjSeq = iBridUtil.GetStringValue(oSampleDataSet.Tables[0].Rows[0]["OBJ_SEQ"]);
					}else{
						return string.Empty;
					}
				}
			}
		}else{
			sTmpValues = new string[]{};
			if (!GetValues(pTag, "OBJ_SEQ", out sTmpValues)) {
				return string.Empty;
			}
			sObjSeq = sTmpValues[0];
		}
		
		using(ProductMovieDtl oProductMovieDtl = new ProductMovieDtl()){
			using(DataSet oDataSet = oProductMovieDtl.GetList(sObjSeq,sessionMan.carrier)){
				int iNum = 1;
				
				DataRow	oPrevRow = null;
				foreach(DataRow oProdMovDtlRow in oDataSet.Tables[0].Rows){
				
					string sFileNm = iBridUtil.GetStringValue(oProdMovDtlRow["FILE_NM"]);
					string sSizeType = iBridUtil.GetStringValue(oProdMovDtlRow["CP_SIZE_TYPE"]);
					if(oPrevRow == null || !(
						oPrevRow["SITE_CD"].Equals(oProdMovDtlRow["SITE_CD"]) &&
						oPrevRow["PRODUCT_AGENT_CD"].Equals(oProdMovDtlRow["PRODUCT_AGENT_CD"]) &&
						oPrevRow["PRODUCT_SEQ"].Equals(oProdMovDtlRow["PRODUCT_SEQ"]) &&
						oPrevRow["OBJ_SEQ"].Equals(oProdMovDtlRow["OBJ_SEQ"]) &&
						oPrevRow["CP_SIZE_TYPE"].Equals(oProdMovDtlRow["CP_SIZE_TYPE"]) &&
						oPrevRow["CP_FILE_FORMAT"].Equals(oProdMovDtlRow["CP_FILE_FORMAT"]) &&
						oPrevRow["CP_TARGET_CARRIER"].Equals(oProdMovDtlRow["CP_TARGET_CARRIER"]))){
			
						iNum = 1;
						oValueBuilder.AppendLine("<br />");
						oValueBuilder.AppendFormat("■{0}■", oProdMovDtlRow["CP_SIZE_TYPE"]).AppendLine();
						oValueBuilder.AppendLine("<br />");
					}else{
						if (((iNum - 1)  % iColumn) == 0) {
							oValueBuilder.AppendFormat("{0}", sSepareta).AppendLine();
						}
					}
					
					
					
					string sUrl = string.Empty;
					if(bIsMaqia){
						sUseType = iBridUtil.GetStringValue(oProdMovDtlRow["CP_SIZE_TYPE"]).Substring(4,1);
						// MAQIA管理のコンテンツの場合
						sUrl = ProductHelper.GenerateDownloadUrl(sProductId, sProductType, sObjSeq, sFileNm, sUseType);
					}else{
						// ===============================
						// ASP動画
						// ===============================
						string sDownloadUrl = iBridUtil.GetStringValue(oProdMovDtlRow["DOWNLOAD_URL"]);
						sUrl = ProductHelper.GenerateDownloadUrl4CP(sDownloadUrl,sSiteCd,sProductAgentCd,sProductSeq,sessionMan.userMan.userSeq);
					}
					oValueBuilder.AppendFormat(this.GetDownloadTag(sObjSeq, iNum, sUrl, sessionMan.carrier, sSizeType,sUseType));
					oValueBuilder.AppendLine();

					if ((iNum % iColumn) == 0) {
						oValueBuilder.AppendLine("<br />");					
					}

					iNum += 1;
					
					oPrevRow = oProdMovDtlRow;
				}
			}
		}

		return oValueBuilder.ToString();
	}
	

	

	private string GetDownloadTag(string pObjSeq, int pNum, string pUrl, string pCarrier,string pSizeType,string pUseType) {
		string sFileNm = iBridUtil.addZero(pObjSeq,ViCommConst.OBJECT_NM_LENGTH);
		string sNum = string.Format("{0:000}", pNum);
		
		StringBuilder oDownloadTagBuilder = new StringBuilder();
		
		// ================================
		//  for Docomo
		// ================================
		switch(pCarrier){
			case ViCommConst.DOCOMO:
				oDownloadTagBuilder.AppendFormat("<OBJECT declare id=\"obj_{1}_{0}\" data=\"{2}\" type=\"video/3gpp\">", sNum, pSizeType, pUrl);
				oDownloadTagBuilder.AppendFormat("</OBJECT><A href=\"#obj_{1}_{0}\">{0}</A>", sNum, pSizeType);
				break;
			case ViCommConst.KDDI:
				//string sCopyright = pUseType.Equals(ViCommConst.TARGET_USE_TYPE_STREAMING)?"yes":"no";
				string sCopyright = "no";

				oDownloadTagBuilder.AppendFormat("<object data=\"{0}&audummy=1\" type=\"video/3gpp2\" copyright=\"{1}\" standby=\"{2}\" >", pUrl, sCopyright, sNum);
				oDownloadTagBuilder.AppendFormat("<param name=\"disposition\" value=\"devmpzz\" valuetype=\"data\"/>");
				oDownloadTagBuilder.AppendFormat("</object>");			
				break;
			case ViCommConst.ANDROID:
				oDownloadTagBuilder.AppendFormat("<A href=\"{0}\">{1}</A>",pUrl,sNum);
				break;
			case ViCommConst.IPHONE:
				if(ViCommConst.Browsers.IPHONE_OLD.Equals(sessionObjs.browserId)){
					oDownloadTagBuilder.AppendFormat("<embed src=\"{0}\" href=\"{0}\" type=\"video/3gpp\" target=\"myself\" autoplay/>",pUrl);
				}else{
					oDownloadTagBuilder.AppendFormat("<embed src=\"{0}\" href=\"{0}\" type=\"video/mp4\" target=\"myself\" autoplay/>",pUrl);
				}
				break;
			default:
				oDownloadTagBuilder.AppendFormat("<A href=\"{0}\">{1}</A>", pUrl, sNum);
				break;
		}
		return oDownloadTagBuilder.ToString();
	}


	private string GetComboProductMovieGenre(string pTag, string pArgument) {
		// 引数解析


		string[] oArgArr = pArgument.Split(',');

		string sAdultFlag = ParseArg(oArgArr, 0, "");
		int iCateFlg = ParseArg(oArgArr, 1, 0);
		
		string sProductType = ProductHelper.GetMovieProductType(sAdultFlag);
		if (sProductType.Equals("") == true) {
			return "";
		}

		//コンボボックス作成
		StringBuilder oBuilder = new StringBuilder();
		using (ProductGenre oProdGenRel = new ProductGenre()) {
			using (DataSet ds = oProdGenRel.GetListByProductType(sessionMan.site.siteCd, sProductType)) {

				oBuilder.Append("<select name='narrow_genre'>");
				oBuilder.Append("<option value='$DECODE(:,$QUERY_PARM_DECODE(narrow_genre);,@selected@);:' selected>");
				oBuilder.Append("--ｼﾞｬﾝﾙ--");
				oBuilder.Append("</option>");

				foreach (DataRow dr in ds.Tables[0].Rows) {
					string sComboNm = "";
					string sGenreCodes = string.Format("{0}-{1}", dr["PRODUCT_GENRE_CATEGORY_CD"].ToString(), dr["PRODUCT_GENRE_CD"].ToString());
					if (iCateFlg == ViCommConst.FLAG_ON) {
						sComboNm = string.Format("{0}-{1}", dr["PRODUCT_GENRE_CATEGORY_NM"].ToString(), dr["PRODUCT_GENRE_NM"].ToString());
					} else {
						sComboNm = dr["PRODUCT_GENRE_NM"].ToString();
					}

					oBuilder.AppendFormat("<option value='$DECODE({0},$QUERY_PARM_DECODE(narrow_genre);,@selected@);{0}'>", sGenreCodes);
					oBuilder.Append(sComboNm);
					oBuilder.Append("</option>");

				}
				oBuilder.Append("</select>");

			}
		}


		return parseContainer.Parse(oBuilder.ToString());

	}

	
}
