﻿using System;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseGetAspAd(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		NameValueCollection oQeuryList = new NameValueCollection(sessionMan.requestQuery.QueryString);

		switch (pTag) {
			case "$ASP_AD_CD":
				SetFieldValue(pTag,"AD_ID",out sValue);
				break;
			case "$ASP_AD_NM":
				SetFieldValue(pTag,"AD_NM",out sValue);
				break;
			case "$ASP_DESCRIPTION":
				SetFieldValue(pTag,"AD_COMMENT",out sValue);
				break;
			case "$ASP_MONTHLY_FEE":
				SetFieldValue(pTag,"MONTH_PRICE_COMMENT",out sValue);
				break;
			case "$ASP_END_DATE":
				SetFieldValue(pTag,"END_DATE",out sValue);
				break;
			case "$ASP_URL":
				SetFieldValue(pTag,"AD_URL",out sValue);
				break;
			case "$ASP_AD_TEXT":
				SetFieldValue(pTag,"AD_TEXT",out sValue);
				break;
			case "$ASP_AD_IMAGE":
				SetFieldValue(pTag,"AD_IMAGE",out sValue);
				break;
			case "$ASP_CLICK_UNIT_PRICE":
				SetFieldValue(pTag,"CLICK_UNIT_PRICE",out sValue);
				break;
			case "$ASP_ACTION_UNIT_PRICE":
				SetFieldValue(pTag,"ACTION_UNIT_PRICE",out sValue);
				break;
			case "$ASP_ACTION_TARIFF":
				SetFieldValue(pTag,"ACTION_TARIFF",out sValue);
				break;
			case "$ASP_AD_AFFILIATE_POINT":
				SetFieldValue(pTag,"GET_POINT",out sValue);
				break;
			case "$ASP_REGISTED_FLAG":
				SetFieldValue(pTag,"REGISTED_AD_FLAG",out sValue);
				break;
			case "$ASP_AD_OWNER_ID":
				SetFieldValue(pTag,"AD_OWNER_ID",out sValue);
				break;
			case "$ASP_COUNT_COMMENT":
				SetFieldValue(pTag,"COUNT_COMMENT",out sValue);
				break;
			case "$RE_QUERY_ASP_AD_EX_SETTLED":
				sValue = GetRequeryAspAdRcFlag(oQeuryList,"1");
				break;
			case "$RE_QUERY_ASP_AD_IN_SETTLED":
				sValue = GetRequeryAspAdRcFlag(oQeuryList,"0");
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
	
	private string GetRequeryAspAdRcFlag(NameValueCollection oQeuryList,string pRcFlag) {
		oQeuryList.Remove("rc_flg");
		oQeuryList.Remove("pageno");
		return ConstructQueryString(oQeuryList) + string.Format("&rc_flg={0}",pRcFlag);
	}
}
