﻿using System;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseCastBbsDoc(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$CAST_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag,"MOBILE_CONNECT_TYPE_NM",out sValue);
				break;
			case "$CAST_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_PROFILE_PIC_COUNT":
				SetFieldValue(pTag,"PROFILE_PIC_COUNT",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_FLAG":
				sValue = IsNotEqual(pTag,"PROFILE_MOVIE_FLAG",ViCommConst.FLAG_OFF);
				break;
			case "$CAST_BBS_OBJ_FLAG":
				sValue = GetBbsObjFlag(pTag);
				break;
			case "$CAST_BBS_ATTACHED_NEW_FLAG":
				sValue = GetCastBbsPicNewFlag(pTag).ToString();
				if (sValue.Equals("0")) {
					sValue = GetCastMovieNewFlag(pTag,ViCommConst.ATTACHED_BBS.ToString(),sessionMan.site.movieBbsNewHours).ToString();
				}
				break;
			case "$CAST_BBS_WRITE_NEW_FLAG":
				sValue = GetNewBbsFlag(pTag).ToString();
				break;
			case "$CAST_COMMENT_DETAIL":
				SetFieldValue(pTag,"COMMENT_DETAIL",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_MONITOR_TALK_OK_FLAG":
				sValue = CanMonitorTalk(pTag);
				break;
			case "$CAST_MONITOR_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,IsMonitorVoiceEnable(pTag));
				break;
			case "$CAST_BBS_READ_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$CAST_BBS_WRITE_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$CAST_BBS_WRITE_DOC":
				sValue = GetDoc(pTag,"BBS_DOC");
				break;
			case "$CAST_BBS_WRITE_TITLE":
				SetFieldValue(pTag,"BBS_TITLE",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$DIV_IS_PICKUP":
			case "$CAST_IS_PICKUP":
				sValue = IsPickup(pTag, pArgument).ToString();
				break;
			case "$CAST_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$QUERY_VIEW_BBS_DOC":
				sValue = GetQueryViewBbsDoc(pTag);
				break;
			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_IMG":
				sValue = GetCastFavoritImg(pTag,pArgument);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$CAST_COMMENT_LIST":
				SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				SetFieldValue(pTag,"MONITOR_ENABLE_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
	private string GetQueryViewBbsDoc(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,CRYPT_VALUE,BBS_SEQ",out sValues)) {
			return string.Format("userrecno={0}&loginid={1}&category={2}&{3}={4}&bbsseq={5}&seekmode={6}",
						sValues[0],iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["loginid"]),sessionMan.currentCategoryIndex,
						sessionMan.site.charNoItemNm,sValues[1],sValues[2],sessionMan.seekMode);
		} else {
			return "";
		}
	}

	private int GetNewBbsFlag(string pTag) {
		DateTime dtCreate;
	
		if (GetValue(pTag,"CREATE_DATE",out dtCreate)) {
			if (DateTime.Now.AddHours(sessionMan.site.bbsNewHours * -1) <= dtCreate) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

}
