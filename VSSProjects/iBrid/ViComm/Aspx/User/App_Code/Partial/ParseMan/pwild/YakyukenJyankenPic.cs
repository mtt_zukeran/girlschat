﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳ジャンケン画像検索結果
--	Progaram ID		: YakyukenJyankenPic
--  Creation Date	: 2013.05.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseYakyukenJyankenPic(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$JYANKEN_TYPE":
				this.parseMan.SetFieldValue(pTag,"JYANKEN_TYPE",out sValue);
				break;
			case "$PIC_SEQ":
				this.parseMan.SetFieldValue(pTag,"PIC_SEQ",out sValue);
				break;
			case "$PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$SMALL_PHOTO_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}