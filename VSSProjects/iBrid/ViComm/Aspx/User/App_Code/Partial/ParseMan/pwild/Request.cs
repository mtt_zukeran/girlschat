﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい検索結果
--	Progaram ID		: Request
--  Creation Date	: 2012.06.27
--  Creater			: PW K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseRequest(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$REQUEST_SEQ":
				this.parseMan.SetFieldValue(pTag,"REQUEST_SEQ",out sValue);
				break;
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$TITLE":
				this.parseMan.SetFieldValue(pTag,"TITLE",out sValue);
				break;
			case "$TEXT":
				this.parseMan.SetFieldValue(pTag,"TEXT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$ADMIN_COMMENT":
				this.parseMan.SetFieldValue(pTag,"ADMIN_COMMENT",out sValue);
				sValue = sValue.Replace(Environment.NewLine,"<br />");
				break;
			case "$GOOD_COUNT":
				this.parseMan.SetFieldValue(pTag,"GOOD_COUNT",out sValue);
				break;
			case "$BAD_COUNT":
				this.parseMan.SetFieldValue(pTag,"BAD_COUNT",out sValue);
				break;
			case "$OVERLAP_COUNT":
				this.parseMan.SetFieldValue(pTag,"OVERLAP_COUNT",out sValue);
				break;
			case "$VALUE_COUNT":
				this.parseMan.SetFieldValue(pTag,"VALUE_COUNT",out sValue);
				break;
			case "$VALUE_COMMENT_COUNT":
				this.parseMan.SetFieldValue(pTag,"VALUE_COMMENT_COUNT",out sValue);
				break;
			case "$CREATE_DATE":
				this.parseMan.SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$UPDATE_DATE":
				this.parseMan.SetFieldValue(pTag,"UPDATE_DATE",out sValue);
				break;
			case "$LAST_VALUE_DATE":
				this.parseMan.SetFieldValue(pTag,"LAST_VALUE_DATE",out sValue);
				break;
			case "$DELETE_FLAG":
				this.parseMan.SetFieldValue(pTag,"DELETE_FLAG",out sValue);
				break;
			case "$REQUEST_CATEGORY_SEQ":
				this.parseMan.SetFieldValue(pTag,"REQUEST_CATEGORY_SEQ",out sValue);
				break;
			case "$REQUEST_CATEGORY_NM":
				this.parseMan.SetFieldValue(pTag,"REQUEST_CATEGORY_NM",out sValue);
				break;
			case "$REQUEST_PROGRESS_SEQ":
				this.parseMan.SetFieldValue(pTag,"REQUEST_PROGRESS_SEQ",out sValue);
				break;
			case "$REQUEST_PROGRESS_NM":
				this.parseMan.SetFieldValue(pTag,"REQUEST_PROGRESS_NM",out sValue);
				break;
			case "$REQUEST_PROGRESS_COLOR":
				this.parseMan.SetFieldValue(pTag,"REQUEST_PROGRESS_COLOR",out sValue);
				break;
			case "$HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$SELF_REQUEST_VALUE_EXIST_FLAG":
				sValue = this.GetSelfRequestValueExistFlag(pTag);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetSelfRequestValueExistFlag(string pTag) {
		string sRequestSeq;
		decimal dRecCount;

		this.parseMan.SetFieldValue(pTag,"REQUEST_SEQ",out sRequestSeq);

		RequestValueSeekCondition oCondition = new RequestValueSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.SexCd = ViCommConst.MAN;
		oCondition.RequestSeq = sRequestSeq;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;

		using (RequestValue oRequestValue = new RequestValue()) {
			oRequestValue.GetPageCount(oCondition,1,out dRecCount);
		}

		return (dRecCount > 0) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}
}