﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseSelfMovie(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$HREF_SELF_MOVIE_DOWNLOAD":
				sValue = GetHrefSelfProfileMovieDownload(pTag,pArgument);
				break;
			case "$QUERY_VIEW_SELF_STOCK_MOVIE":
				sValue = GetQueryViewSelfStockMovie(pTag);
				break;
			case "$STOCK_MOVIE_TITLE":
				SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$STOCK_MOVIE_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$SELF_MOVIE_UNAUTH_FLAG":
				SetFieldValue(pTag,"OBJ_NOT_APPROVE_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
	private string GetHrefSelfProfileMovieDownload(string pTag,string pLabel) {
		string[] sValues;
		if (GetValues(pTag,"MOVIE_SEQ",out sValues) == false) {
			return string.Join("",sValues);
		}
		return ParseManDownloadUrl("1",sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sessionMan.userMan.loginId,sValues[0],pLabel);
	}

	private string GetQueryViewSelfStockMovie(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"MOVIE_SEQ,RNUM",out sValues)) {
			return string.Format("movieseq={0}&data={1}&pageno={2}&mailtype={3}",sValues[0],iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"]),sValues[1],iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["mailtype"]));
		} else {
			return "";
		}
	}
}
