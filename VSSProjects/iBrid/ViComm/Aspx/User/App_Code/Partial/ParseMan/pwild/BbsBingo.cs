﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝deビンゴ検索結果
--	Progaram ID		: BbsBingo
--  Creation Date	: 2013.10.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseBbsBingo(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			//開催設定
			case "$BBS_BINGO_TERM_SEQ":
				this.parseMan.SetFieldValue(pTag,"TERM_SEQ",out sValue);
				break;
			case "$BBS_BINGO_TERM_START_DATE":
				this.parseMan.SetFieldValue(pTag,"START_DATE",out sValue);
				break;
			case "$BBS_BINGO_TERM_END_DATE":
				this.parseMan.SetFieldValue(pTag,"END_DATE",out sValue);
				break;
			case "$BBS_BINGO_TERM_JACKPOT":
				this.parseMan.SetFieldValue(pTag,"JACKPOT",out sValue);
				break;
			case "$BBS_BINGO_TERM_ENTRY_COUNT":
				this.parseMan.SetFieldValue(pTag,"ENTRY_COUNT",out sValue);
				break;
			case "$BBS_BINGO_TERM_BINGO_BALL_FLAG":
				this.parseMan.SetFieldValue(pTag,"BINGO_BALL_FLAG",out sValue);
				break;
			//カード
			case "$GET_BBS_BINGO_CARD_TAG":
				sValue = this.GetBbsBingoCardTag(pTag,pArgument);
				break;
			//履歴
			case "$BBS_BINGO_LOG_BINGO_NO":
				this.parseMan.SetFieldValue(pTag,"BINGO_NO",out sValue);
				break;
			case "$BBS_BINGO_LOG_NEW_NO_FLAG":
				this.parseMan.SetFieldValue(pTag,"NEW_NO_FLAG",out sValue);
				break;
			case "$BBS_BINGO_LOG_CARD_NO_FLAG":
				this.parseMan.SetFieldValue(pTag,"CARD_NO_FLAG",out sValue);
				break;
			case "$BBS_BINGO_LOG_COMPLETE_FLAG":
				this.parseMan.SetFieldValue(pTag,"COMPLETE_FLAG",out sValue);
				break;
			case "$BBS_BINGO_LOG_BINGO_BALL_FLAG":
				this.parseMan.SetFieldValue(pTag,"BINGO_BALL_FLAG",out sValue);
				break;
			case "$BBS_BINGO_LOG_RESULT":
				this.parseMan.SetFieldValue(pTag,"RESULT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetBbsBingoCardTag(string pTag,string pArgument) {
		StringBuilder oBingoCardTag = new StringBuilder();
		string[] sCardColorList = new string[] { "#FFCC00","#009900","#0099FF","#9933FF","#FF0099","#FF6600","#006633","#FF00FF","#0066FF","#FF3300" };
		int iCardNo;
		string[] sCardMask;
		string[] sHitMask;
		string sResult;

		using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
			oBbsBingoTerm.GetBbsBingoCard(
				parseMan.sessionMan.site.siteCd,
				parseMan.sessionMan.userMan.userSeq,
				out iCardNo,
				out sCardMask,
				out sHitMask,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.GetBbsBingoCardResult.RESULT_OK)) {
			int iCardColorNum = (iCardNo-1) % sCardColorList.Length;
			string sCardColorCd = sCardColorList[iCardColorNum];
			string sCardTitle = parseMan.parseContainer.Parse(pArgument);

			oBingoCardTag.AppendFormat("<table bgcolor=\"{0}\">",sCardColorCd);
			oBingoCardTag.AppendFormat("<tr><td colspan=\"5\" align=\"center\"><font size=\"3\" color=\"#FFFFFF\">BINGO CARD {0:D2}</font></td></tr>",iCardNo);

			for (int i = 0;i < 5;i++) {
				oBingoCardTag.Append("<tr>");

				for (int j = 0;j < 5;j++) {
					int iIdx = (i * 5) + j;
					string sBingoNoImg = string.Empty;

					if (sCardMask[iIdx] == "0") {
						sBingoNoImg = "b_heart.gif";
					} else {
						if (sHitMask[iIdx].Equals(ViCommConst.FLAG_ON_STR)) {
							sBingoNoImg = string.Format("{0}_{1}.gif",iCardColorNum+1,sCardMask[iIdx]);
						} else {
							sBingoNoImg = string.Format("{0}.gif",sCardMask[iIdx]);
						}
					}
					oBingoCardTag.AppendFormat("<td><img src=\"../Image/{0}/bingo/{1}\" /></td>",parseMan.sessionMan.site.siteCd,sBingoNoImg);
				}
				oBingoCardTag.Append("</tr>");
			}
			oBingoCardTag.Append("</table>");			
		}

		return oBingoCardTag.ToString();
	}
}