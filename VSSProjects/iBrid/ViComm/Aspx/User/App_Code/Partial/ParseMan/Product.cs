﻿using System;
using System.Text;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseProduct(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		
		switch (pTag) {
			#region □ 商品写真種別	□ --------------------------
			case "$PIC_PACKAGE_A":
				sValue = ViCommConst.ProductPicType.PACKAGE_A;
				break;
			case "$PIC_PACKAGE_B":
				sValue = ViCommConst.ProductPicType.PACKAGE_B;
				break;
			case "$PIC_SAMPLE":
				sValue = ViCommConst.ProductPicType.SAMPLE;
				break;
			case "$PIC_CONTENTS":
				sValue = ViCommConst.ProductPicType.CONTENTS;
				break;
			#endregion --------------------------------------	
			#region □ 商品動画種別	□ --------------------------
			case "$MOVIE_CONTENTS":
				sValue = ViCommConst.ProductMovieType.CONTENTS;
				break;
			case "$MOVIE_SAMPLE":
				sValue = ViCommConst.ProductMovieType.SAMPLE;
				break;
			#endregion --------------------------------------		
					
// CAST_CHARACTER
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag, "HANDLE_NM", out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;				
// PRODUCT		
			case "$QUERY_PRODUCT":
				sValue = GetQueryProduct(pTag);
				break;
			case "$PRODUCT_NM":
				SetFieldValue(pTag, "PRODUCT_NM", out sValue);
				break;
			case "$PRODUCT_SEQ":
				SetFieldValue(pTag, "PRODUCT_SEQ", out sValue);
				break;
			case "$PRODUCT_DISCRIPTION":
				sValue = GetProductDescription(pTag,pArgument);
				break;
			case "$PART_NUMBER":
				SetFieldValue(pTag, "PART_NUMBER", out sValue);
				break;
			case "$PRODUCT_PUBLISH_START_DATE":
				SetFieldValue(pTag, "PUBLISH_START_DATE", out sValue);
				break;
			case "$PRODUCT_MAKER_SUMMARY":
				SetFieldValue(pTag, "PRODUCT_MAKER_SUMMARY", out sValue);
				break;
			case "$PRODUCT_SERIES_SUMMARY":
				SetFieldValue(pTag, "PRODUCT_SERIES_SUMMARY", out sValue);
				break;
			case "$PRODUCT_CAST_SUMMARY":
				SetFieldValue(pTag, "CAST_SUMMARY", out sValue);
				break;
			case "$PRODUCT_RELEASE_DATE":
				SetFieldValue(pTag, "RELEASE_DATE", out sValue);
				break;
			case "$PRODUCT_GROSS_PRICE":
				SetFieldValue(pTag, "GROSS_PRICE", out sValue);
				break;
			case "$PRODUCT_CHARGE_POINT":
				SetFieldValue(pTag, "CHARGE_POINT", out sValue);
				break;
			case "$PRODUCT_APPLY_CHARGE_POINT":
				SetFieldValue(pTag, "APPLY_CHARGE_POINT", out sValue);
				break;
			case "$PRODUCT_SPECIAL_CHARGE_POINT":
				SetFieldValue(pTag, "SPECIAL_CHARGE_POINT", out sValue);
				break;
			case "$DIV_IS_FAVORITE": {
					string sFlag;
					GetValue(pTag, "FAVORITE_FLAG", out sFlag);
					SetNoneDisplay(ViCommConst.FLAG_OFF_STR.Equals(sFlag));
				}
				break;
			case "$DIV_IS_NOT_FAVORITE": {
					string sFlag;
					GetValue(pTag, "FAVORITE_FLAG", out sFlag);
					SetNoneDisplay(ViCommConst.FLAG_ON_STR.Equals(sFlag));
				}
				break;
			case "$DIV_IS_BOUGHT":
				SetFieldValue(pTag, "BOUGHT_FLAG", out sValue);
				break;				
			case "$PRODUCT_IMG_PATH":
				sValue = GetProductImgPath(pTag, pArgument);
				break;
			// 写真				
			case "$LIST_PRODUCT_PIC":
				// p1: 写真種別,p2:写真ｻｲｽﾞ(ﾃﾞﾌｫﾙﾄ 小),p3:列数(ﾃﾞﾌｫﾙﾄ 3),p4:写真幅(ﾃﾞﾌｫﾙﾄ 未指定),p5:href写真ｻｲｽﾞ(ﾃﾞﾌｫﾙﾄ未指定)
				sValue = GetListProductPic(pTag, pArgument);
				break;
			// ｼﾞｬﾝﾙ				
			case "$LIST_GENRE":
				// p1: セパレータ,p2:ﾘﾝｸ有無フラグ(1=>ﾘﾝｸにする0=>ﾘﾝｸにしない)
				sValue = GetProductGenre(pTag,pArgument);
				break;
			// お気に入り登録日				
			case "$FAVORITE_REGIST_DATE":
				SetFieldValue(pTag,"FAVORITE_REGIST_DATE",out sValue);
				break;
			// 購入日時				
			case "$BUY_DATE":
				SetFieldValue(pTag,"BUY_DATE",out sValue);
				break;

			default:
				pParsed = false;
				break;
		}
		
		return sValue;
	}

	private string GetQueryProduct(string pTag) {
		string[] sValues;
		
		if (GetValues(pTag, "PRODUCT_ID,PRODUCT_TYPE", out sValues)) {
			string sAdultFlag = ProductHelper.GetAdultFlag(sValues[1]);
			return string.Format("seekmode={0}&product_id={1}&adult={2}",
							   sessionMan.seekMode,sValues[0],sAdultFlag);// + sessionMan.seekCondition.GetConditionQuery() + sessionMan.AddUniqueQuery(dataRow[this.currentTableIdx], this.sessionMan.requestQuery);
		} else {
			return string.Empty;
		}
	}
	

	private string GetProductImgPath(string pTag, string pArgument) {
		// 引数解析
		string[] oArgArr = pArgument.Split(',');
		ViCommConst.PicSizeType iPicSize = (ViCommConst.PicSizeType)ParseArg(oArgArr, 0, (int)ViCommConst.PicSizeType.SMALL);
		
		string sValue = string.Empty;
		SetFieldValue(pTag,ProductHelper.ConvertPicSizeType2DataColumnNm(iPicSize),out sValue);
		return sValue;
	}
	
	/// <summary>
	/// 画像の一覧を表示するためのHTMLを出力する
	/// </summary>
	/// <param name="pTag">タグ</param>
	/// <param name="pArgument">タグ引数</param>
	/// <returns>画像の一覧を表示するためのHTML</returns>
	private string GetListProductPic(string pTag, string pArgument) {
		// 引数解析
		string[] oArgArr = pArgument.Split(',');

		string sPicType = ParseArg(oArgArr, 0, string.Empty);
		ViCommConst.PicSizeType iPicSize = (ViCommConst.PicSizeType)ParseArg(oArgArr, 1, (int)ViCommConst.PicSizeType.SMALL);
		int iColumn = ParseArg(oArgArr, 2, int.MaxValue);
		string sWidth = ParseArg(oArgArr, 3, string.Empty);
		ViCommConst.PicSizeType iLinkPicSize = (ViCommConst.PicSizeType)ParseArg(oArgArr, 4, (int)ViCommConst.PicSizeType.WITHOUT);
		
		if(sPicType.Equals(string.Empty)){
			throw new ArgumentException(string.Format("args:{0}",pArgument));
		}

		// 主情報のキー取得
		string[] sTmpValues = null;
		if (!GetValues(pTag, "SITE_CD,PRODUCT_AGENT_CD,PRODUCT_SEQ", out sTmpValues)) {
			return string.Empty;
		}
		string sSiteCd = sTmpValues[0];
		string sProductAgentCd = sTmpValues[1];
		string sProductSeq = sTmpValues[2];

		// HTML作成
		StringBuilder oValueBuilder = new StringBuilder();
		using(ProductPic oProductPic = new ProductPic(sessionMan)){
			using (DataSet oProductPicDs = oProductPic.GetListByType(sSiteCd, sProductAgentCd, sProductSeq, sPicType)) {
				int iNum = 1;
				foreach(DataRow oProductPicRow in oProductPicDs.Tables[0].Rows){

					string sPicPath = iBridUtil.GetStringValue(oProductPicRow[ProductHelper.ConvertPicSizeType2DataColumnNm(iPicSize)]);
					
					string sLinkPicPath = string.Empty;
					if(iLinkPicSize != ViCommConst.PicSizeType.WITHOUT ){
						sLinkPicPath = iBridUtil.GetStringValue(oProductPicRow[ProductHelper.ConvertPicSizeType2DataColumnNm(iLinkPicSize)]);
					}

					string sWidthAttr = string.Empty;
					if(!sWidth.Equals(string.Empty)){
						sWidthAttr = string.Format("width='{0}'",sWidth);
					}

					if (!sLinkPicPath.Equals(string.Empty)) {
						oValueBuilder.AppendFormat("<a href='{0}'>", sLinkPicPath);
					}
					oValueBuilder.AppendFormat("<img src='{0}' {1} />", sPicPath, sWidthAttr);
					if (!sLinkPicPath.Equals(string.Empty)) {
						oValueBuilder.AppendFormat("</a>");
					}

					if ((iNum % iColumn) == 0) {
						oValueBuilder.AppendLine("<br />");
					}

					iNum += 1;

				}
			}
		}
		
		return oValueBuilder.ToString();
	}

	private string GetProductGenre(string pTag, string pArgument){
		
		// 引数解析
		string[] oArgArr = pArgument.Split(',');

		string sSepareta = oArgArr[0];
		if(sSepareta.Equals("") == true){sSepareta = "/";}
		int iLinkFlg = ParseArg(oArgArr,1,0);

		//キー取得
		
		string[] sTmpValues = null;
		if (!GetValues(pTag,"SITE_CD,PRODUCT_AGENT_CD,PRODUCT_SEQ,PRODUCT_TYPE",out sTmpValues)) {
			return string.Empty;
		}
		string sSiteCd = sTmpValues[0];
		string sProductAgentCd = sTmpValues[1];
		string sProductSeq = sTmpValues[2];
		string sProductType = sTmpValues[3].ToString();
		string sBaseUrl = "";
		
		if(ViCommConst.ProductType.IsMovie(sProductType)){
			sBaseUrl = "ListProdMov.aspx";
		} else if (ViCommConst.ProductType.IsPic(sProductType)) {
			sBaseUrl = "ListProdPic.aspx";
		} else if (ViCommConst.ProductType.IsAuction(sProductType)){
			sBaseUrl = "ListProdAuction.aspx";
		} else if (ViCommConst.ProductType.IsTheme(sProductType)) {
			sBaseUrl = "ListProdTheme.aspx";
		}

		string sAdultFlag = ProductHelper.GetAdultFlag(sProductType);
		
		StringBuilder oBuilder = new StringBuilder();
		
		using(ProductGenreRel oProdGenRel = new ProductGenreRel()) {
		
			using(DataSet ds = oProdGenRel.GetList(sSiteCd,sProductAgentCd,sProductSeq)) {
			
				string sUseSepareta = "";
				foreach(DataRow dr in ds.Tables[0].Rows) {
				
					string sGenleNm = dr["PRODUCT_GENRE_NM"].ToString();
					
					if(iLinkFlg == ViCommConst.FLAG_ON) {
						oBuilder.AppendFormat("{0}",sUseSepareta);
						oBuilder.AppendFormat("<a href='./{0}?genre={1}-{2}&adult={3}'>",sBaseUrl,dr["PRODUCT_GENRE_CATEGORY_CD"].ToString(),dr["PRODUCT_GENRE_CD"].ToString(),sAdultFlag);
						oBuilder.AppendFormat("{0}",sGenleNm);
						oBuilder.AppendFormat("</a>");
					}
					else {					
					
						oBuilder.AppendFormat("{0}{1}",sUseSepareta,sGenleNm);
					}
					
					sUseSepareta = sSepareta;
				}
					
			}
		}
		
		return oBuilder.ToString();
	}
	
	private string GetProductDescription(string pTag, string pArgument)
	{
		string[] oArgArr = pArgument.Split(',');

		int iWithNewLine2BrFlag = ParseArg(oArgArr,0,0);
		
		string sValue = string.Empty;
		SetFieldValue(pTag, "PRODUCT_DISCRIPTION", out sValue);

		if (iWithNewLine2BrFlag == 1) {
			sValue = sValue.Replace("\r\n", "<br />");
		}
				
		return sValue;
	}



}
