﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: クレジット自動決済検索結果
--	Progaram ID		: CreditAutoSettle
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

partial class PwParseMan {
	private string ParseCreditAutoSettle(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$CREDIT_AUTO_SETTLE_SEQ":
				this.parseMan.SetFieldValue(pTag,"CREDIT_AUTO_SETTLE_SEQ",out sValue);
				break;
			case "$CREDIT_AUTO_SETTLE_AMT":
				this.parseMan.SetFieldValue(pTag,"SETTLE_AMT",out sValue);
				break;
			case "$CREDIT_AUTO_SETTLE_POINT":
				this.parseMan.SetFieldValue(pTag,"SETTLE_POINT",out sValue);
				break;
			case "$CREDIT_AUTO_SETTLE_EXIST_FLAG":
				this.parseMan.SetFieldValue(pTag,"EXIST_FLAG",out sValue);
				break;
			case "$CREDIT_AUTO_SETTLE_NEXT_SETTLE_DATE":
				this.parseMan.SetFieldValue(pTag,"NEXT_SETTLE_DATE",out sValue);
				break;
			case "$CREDIT_AUTO_SETTLE_COUNT":
				this.parseMan.SetFieldValue(pTag,"SETTLE_COUNT",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}