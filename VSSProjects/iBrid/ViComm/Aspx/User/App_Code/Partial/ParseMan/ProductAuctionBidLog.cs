﻿using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseProductAuctionBidLog(string pTag, string pArgument, out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		switch (pTag) {
			case "$PRODUCT_AUCTION_BID_AMT":
				SetFieldValue(pTag, "BID_AMT", out sValue);
				break;
			case "$PRODUCT_AUCTION_BID_DATE":
				SetFieldValue(pTag, "BID_DATE", out sValue);
				break;
			case "$PRODUCT_AUCTION_BIDDER_HANDLE_NM":
				SetFieldValue(pTag, "BIDDER_HANDLE_NM", out sValue);
				break;								
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}


}