﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者 待機スケジュール
--	Progaram ID		: WaitSchedule
--  Creation Date	: 2014.11.19
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

partial class PwParseMan {
	private string ParseWaitSchedule(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$CAST_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_LOGIN_ID":
				this.parseMan.SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				this.parseMan.SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_WAIT_START_TIME":
				this.parseMan.SetFieldValue(pTag,"WAIT_START_TIME",out sValue);
				break;
			case "$CAST_ITEM":
				sValue = this.parseMan.GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = this.parseMan.GetQueryCastProfile(pTag);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}