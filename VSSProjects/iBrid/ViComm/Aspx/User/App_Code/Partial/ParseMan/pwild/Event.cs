﻿using System;
using System.Text;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

partial class PwParseMan {
	private string ParseEvent(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$EVENT_NM":
				this.parseMan.SetFieldValue(pTag,"EVENT_NM",out sValue);
				break;
			case "$PREFECTURE_NM":
				this.parseMan.SetFieldValue(pTag,"PREFECTURE_NM",out sValue);
				break;
			case "$EVENT_AREA_NM":
				this.parseMan.SetFieldValue(pTag,"EVENT_AREA_NM",out sValue);
				break;
			case "$EVENT_START_DATE":
				this.parseMan.SetFieldValue(pTag,"EVENT_START_DATE",out sValue);
				break;
			case "$EVENT_END_DATE":
				this.parseMan.SetFieldValue(pTag,"EVENT_END_DATE",out sValue);
				break;
			case "$EVENT_DETAIL":
				this.parseMan.SetFieldValue(pTag,"EVENT_DETAIL",out sValue);
				sValue = sValue.Replace("\r\n","<br />");
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}
}