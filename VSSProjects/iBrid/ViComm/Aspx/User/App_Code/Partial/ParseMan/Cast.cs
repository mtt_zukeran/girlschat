﻿using System;
using iBridCommLib;
using ViComm;
using System.Text;

partial class ParseMan {
	private string ParseCast(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_ATTR_TYPE_NM":
				sValue = GetAttrTypeNm(pTag);
				break;
			case "$CAST_BIRTHDAY":
				SetFieldValue(pTag,"CHARACTER_BIRTHDAY",out sValue);
				break;
			case "$CAST_BBS_MOVIE_FLAG":
				SetFieldValue(pTag,"BBS_MOVIE_FLAG",out sValue);
				break;
			case "$CAST_BBS_MOVIE_NEW_FLAG":
				sValue = GetCastMovieNewFlag(pTag,ViCommConst.ATTACHED_BBS.ToString(),sessionMan.site.movieBbsNewHours).ToString();
				break;
			case "$CAST_BBS_ATTACHED_NEW_FLAG":
				sValue = GetCastBbsPicNewFlag(pTag).ToString();
				if (sValue.Equals("0")) {
					sValue = GetCastMovieNewFlag(pTag,ViCommConst.ATTACHED_BBS.ToString(),sessionMan.site.movieBbsNewHours).ToString();
				}
				break;
			case "$CAST_BBS_LAST_ACTION_TYPE":
				//SetFieldValue(pTag,"BBS_LAST_ACTION_TYPE",out sValue);
				sValue = this.GetBbsLastActionType(pTag);
				break;
			case "$CAST_BBS_LAST_ACTION_DATE":
				//SetFieldValue(pTag,"BBS_LAST_ACTION_DATE",out sValue);
				sValue = this.GetBbsLastActionDate(pTag);
				break;
			case "$CAST_NEWLY_BBS_ACTION_DATE":
				//SetFieldValue(pTag,"BBS_LAST_ACTION_DATE",out sValue);
				sValue = this.GetBbsNewlyActionDate(pTag);
				break;
			case "$CAST_NEWLY_BBS_ACTION_TYPE":
				sValue = this.GetBbsNewlyActionType(pTag);
				break;
			case "$CAST_BBS_PIC_FLAG":
				SetFieldValue(pTag,"BBS_PIC_FLAG",out sValue);
				break;
			case "$CAST_BBS_PIC_NEW_FLAG":
				sValue = GetCastBbsPicNewFlag(pTag).ToString();
				break;
			case "$CAST_BBS_WRITE_FLAG":
				SetFieldValue(pTag,"BBS_FLAG",out sValue);
				break;
			case "$CAST_BBS_WRITE_NEW_FLAG":
				sValue = GetCastBbsNewFlag(pTag).ToString();
				break;
			case "$CAST_BBS_WRITE_LAST_DATE":
				SetFieldValue(pTag,"BBS_LAST_DATE",out sValue);
				break;
			case "$CAST_BBS_OBJ_FLAG":
				sValue = GetBbsObjFlag(pTag);
				break;
			case "$CAST_BBS_ALL_OBJ_FLAG":
				sValue = GetBbsAllObjFlag(pTag);
				break;
			case "$CAST_PROFILE_PIC_FLAG":
				SetFieldValue(pTag,"PROFILE_PIC_FLAG",out sValue);
				break;
			case "$CAST_TOAL_TALK_COUNT_BY_SELF":
				SetFieldValue(pTag,"PARTNER_TOTAL_TALK_COUNT",out sValue);
				break;
			case "$CAST_TO_SELF_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_RX_MAIL_COUNT",out sValue);	// 男性基準のためrxを設定 
				break;
			case "$SELF_TO_CAST_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_TX_MAIL_COUNT",out sValue);	// 男性基準のためtxを設定 
				break;
			case "$CAST_CHARGE_TALK_VOICE_WSHOT":
				sValue = GetChargePoint(pTag,ViCommConst.CHARGE_TALK_WSHOT);
				break;
			case "$CAST_CHARGE_TX_MAIL":
				sValue = GetChargePoint(pTag,ViCommConst.CHARGE_TX_MAIL);
				break;
			case "$CAST_CHAT_TV_OK_FLAG":
				sValue = IsEqual(pTag,IsTvTalkEnable(pTag));
				break;
			case "$CAST_COMMENT_ADMIN":
				SetFieldValue(pTag,"COMMENT_ADMIN",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_DETAIL":
				SetFieldValue(pTag,"COMMENT_DETAIL",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_LIST":
				sValue = GetCastCommentList(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_PICKUP":
				SetFieldValue(pTag,"COMMENT_PICKUP",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_RECOMMEND":
				SetFieldValue(pTag,"COMMENT_RECOMMEND",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_DIARY_FLAG":
			case "$CAST_DIARY_COUNT":
				sValue = this.GetDiaryFlag(pTag);
				break;
			case "$CAST_DIARY_HEADER_TITLE":
				sValue = GetDiaryHeaderTitle(pTag);
				break;
			case "$CAST_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_COUNT":
				sValue = GetFavoritMeCount(pTag);
				break;
			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_IMG":
				sValue = GetCastFavoritImg(pTag,pArgument);
				break;
			case "$CAST_FAVORIT_LOGIN_MAIL_NOT_RX_FLAG":
				sValue = GetFavoriteLoginMailNotRxFlag(pTag);
				break;
			case "$CAST_FAVORIT_LOGIN_MAIL_RADIOBUTTON":
				sValue = CreateRadioButtonTag(pTag,pArgument);
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				SetFieldValue(pTag,"MONITOR_ENABLE_FLAG",out sValue);
				break;
			case "$CAST_HANDLE_KANA_NM":
				SetFieldValue(pTag,"HANDLE_KANA_NM",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_IMG_BACKGROUND_URL":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = string.Format("background:url(../{0});",sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_ITEM_CD":
				sValue = GetCastItemCd(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_JANLE_NM":
				SetFieldValue(pTag,"ACT_CATEGORY_NM",out sValue);
				break;
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_LARGE_IMG_BACKGROUND_URL":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = string.Format("background:url(../{0});",sValue);
				break;
			case "$CAST_LAST_ACTION_DATE":
			case "$CAST_LAST_LOGIN_DATE":
				SetFieldValue(pTag,"LAST_ACTION_DATE",out sValue);
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$CAST_LAST_TALK_DATE":
				SetFieldValue(pTag,"LAST_TALK_DATE",out sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_MAIL_COUNT_NOW":
				sValue = GetMailCountNow(pTag);
				break;
			case "$CAST_MAIL_NA_FLAG":
				sValue = IsEqual(pTag,"MAIL_NA_FLAG",ViCommConst.FLAG_ON);
				break;
			case "$CAST_MARKING_ME_DATE":
				SetFieldValue(pTag,"MARKING_DATE",out sValue);
				break;
			case "$SELF_MARKING_DATE":
				SetFieldValue(pTag,"MARKING_DATE",out sValue);
				break;
			case "$CAST_MONITOR_PERSON_COUNT":
				sValue = GetMonitorPersonCount(pTag);
				break;
			case "$CAST_MONITOR_ROOM_OK_FLAG":
				sValue = IsEqual(pTag,"MONITOR_TALK_TYPE",ViCommConst.MONITOR_ROOM);
				break;
			case "$CAST_MONITOR_TALK_OK_FLAG":
				sValue = CanMonitorTalk(pTag);
				break;
			case "$CAST_MONITOR_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,IsMonitorVoiceEnable(pTag));
				break;
			case "$CAST_MONITOR_OK_FLAG":
				sValue = GetCastMonitorOkFlag(pTag);
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$CAST_OK_LIST":
				sValue = GetOkList(pTag);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_PICKUP_HEADER_DOC":
				SetFieldValue(pTag,"PICKUP_HEADER_DOC",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_DOC":
				SetFieldValue(pTag,"MOVIE_DOC",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_FLAG":
				sValue = IsNotEqual(pTag,"PROFILE_MOVIE_FLAG",ViCommConst.FLAG_OFF);
				break;
			case "$CAST_PROFILE_MOVIE_NEW_FLAG":
				sValue = GetCastMovieNewFlag(pTag,ViCommConst.ATTACHED_PROFILE.ToString(),sessionMan.site.profileMovieNewHours).ToString();
				break;
			case "$CAST_PROFILE_MOVIE_TITLE":
				SetFieldValue(pTag,"MOVIE_TITLE",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_UPLOAD_DATE":
				SetFieldValue(pTag,"UPLOAD_DATE",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_SEQ":
				SetFieldValue(pTag,"PROFILE_MOVIE_SEQ",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_LIKE_COUNT":
				SetFieldValue(pTag,"MOVIE_LIKE_COUNT",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_COMMENT_COUNT":
				SetFieldValue(pTag,"MOVIE_COMMENT_COUNT",out sValue);
				break;
			case "$CAST_PROFILE_PIC_COUNT":
				SetFieldValue(pTag,"PROFILE_PIC_COUNT",out sValue);
				break;
			case "$CAST_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$CAST_RANKING_START_DATE":
				SetFieldValue(pTag,"CREATE_START_DATE",out sValue);
				break;
			case "$CAST_RANKING_END_DATE":
				SetFieldValue(pTag,"CREATE_END_DATE",out sValue);
				break;
			case "$CAST_SALES_MOVIE_COUNT":
				SetFieldValue(pTag,"SALE_MOVIE_COUNT",out sValue);
				break;
			case "$CAST_SALES_VOICE_COUNT":
				SetFieldValue(pTag,"SALE_VOICE_COUNT",out sValue);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$CAST_SEND_ME_MAIL_COUNT":
				sValue = GetMailCount(ViCommConst.RX,pTag);
				break;
			case "$CAST_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$CAST_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_TALK_COUNT":
				sValue = GetTalkCount(pTag);
				break;
			case "$CAST_TALK_MOVIE_FLAG":
				SetFieldValue(pTag,"TALK_MOVIE_FLAG",out sValue);
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag,"MOBILE_CONNECT_TYPE_NM",out sValue);
				break;
			case "$CAST_WAITING_COMMENT":
				SetFieldValue(pTag,"WAITING_COMMENT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_WSHOT_TV_OK_FLAG":
				sValue = CanTvWShot(pTag);
				break;
			case "$CAST_WSHOT_VOICE_OK_FLAG":
				sValue = CanVoiceTalk(pTag);
				break;
			case "$CAST_MEMO_REVIEWS":
				SetFieldValue(pTag,"MEMO_REVIEWS",out sValue);
				break;
			case "$CAST_MEMO_DOC":
				SetFieldValue(pTag,"MEMO_DOC",out sValue);
				sValue = sValue.Replace("\r\n","<br />");
				break;
			case "$CAST_MEMO_UPDATE_DATE":
				SetFieldValue(pTag,"MEMO_UPDATE_DATE",out sValue);
				break;
			case "$CAST_PROFILE_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag,ViCommConst.ATTACHED_PROFILE.ToString());
				break;
			case "$CAST_BBS_PIC_COUNT":
				sValue = this.GetPicCount(pTag,ViCommConst.ATTACHED_BBS.ToString());
				break;
			case "$CAST_BBS_MOVIE_COUNT":
				sValue = this.GetMovieCount(pTag,ViCommConst.ATTACHED_BBS.ToString());
				break;
			case "$CAST_BBS_COUNT":
				sValue = this.GetBbsWriteCount(pTag);
				break;
			case "$CAST_BBS_ATTR_PIC_COUNT":
				sValue = this.GetPicCountByAttr(pTag,pArgument);
				break;
			case "$CAST_BBS_ATTR_PIC_NEW_FLAG":
				sValue = GetCastAttrBbsPicNewFlag(pTag,pArgument).ToString();
				break;
			case "$CAST_BBS_ATTR_MOVIE_COUNT":
				sValue = this.GetMovieCountByAttr(pTag,pArgument);
				break;
			case "$CAST_BBS_ATTR_MOVIE_NEW_FLAG":
				sValue = GetCastAttrMovieNewFlag(pTag,ViCommConst.ATTACHED_BBS.ToString(),sessionMan.site.movieBbsNewHours,pArgument).ToString();
				break;
			case "$DIV_IS_NOW_CALLING":
				SetNoneDisplay(IsNowCall(pTag) == false);
				break;
			case "$DIV_IS_PICKUP":
			case "$CAST_IS_PICKUP":
				sValue = IsPickup(pTag,pArgument).ToString();
				break;
			case "$DIV_IS_QUICK_RESPONSE":
				SetNoneDisplay(IsQuickResponse(pTag) == false);
				break;
			case "$DIV_IS_NOT_QUICK_RESPONSE":
				SetNoneDisplay(IsQuickResponse(pTag) == true);
				break;
			case "$SELF_TO_CAST_USE_POINT":
				sValue = GetUsePoint(pTag,pArgument).ToString();
				break;
			case "$SELF_FAVORIT_CAST_MEMO":
				sValue = GetFavoritMemo(pTag);
				break;
			case "$SELF_REFUSE_CAST_MEMO":
				sValue = GetRefuseMemo(pTag);
				break;
			case "$SELF_REFUSE_CAST_REGIST_DATE":
				SetFieldValue(pTag,"REFUSE_REGIST_DATE",out sValue);
				break;
			case "$SEFL_TALK_TO_CAST_AVE_MIN":
				sValue = GetAvailableMin(pTag,pArgument).ToString();
				break;
			case "$HREF_MONITOR_ROOM":
				sValue = GetHrefMonitorRoom(pTag,pArgument);
				break;
			case "$HREF_MONITOR_TALK":
			case "$HREF_MONITOR_TALK2":
				sValue = GetHrefMonitorTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_VOICE":
				sValue = GetHrefMonitorVoice(pTag,pArgument);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VCS_PLAY_PF":
				sValue = GetHrefVcsPlayProfile(pTag,pArgument);
				break;
			case "$HREF_VCS_REC_PF":
				sValue = GetVcsMenuUrl(pTag,ViCommConst.VCS_MENU_DIRECT_REC_PF,ViCommConst.CHARGE_REC_PF_VOICE,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$QUERY_CANCEL_FORWARD":
				sValue = GetQueryCancelForward(pTag);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$REGIST_MAIL_ADDR_BY_CAST":
				sValue = GetRegistMailCast();
				break;
			case "$QUERY_VIEW_PROFILE_PIC":
				sValue = GetQueryViewProfilePic(pTag);
				break;
			case "$DIV_IS_EXT_SEARCH_RESULT":
				sValue = (this.sessionMan.seekCondition.hasExtend) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;
			case "$CAST_PROFILE_PIC_PERTS":
				sValue = GetListPersonalProfPic(pTag,pArgument);
				break;
			case "$QUERY_DIRECT_PROFILE":
				sValue = GetQueryDirectProfile(pTag);
				break;
			case "$CAST_FEATURE_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.DOCOMO,ViCommConst.KDDI,ViCommConst.SOFTBANK);
				break;
			case "$CAST_SMART_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID,ViCommConst.IPHONE);
				break;
			case "$CAST_ANDROID_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID);
				break;
			case "$CAST_IPHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.IPHONE);
				break;
			case "$HREF_CAST_MOVIE_DOWNLOAD_FREE":
				sValue = GetHrefMovieDownload(pTag,pArgument);
				break;
			case "$CAST_BBS_PIC_UNUSED_COUNT":
				sValue = GetBbsPicUnusedCount(pTag,pArgument).ToString();
				break;
			case "$CAST_BBS_MOVIE_UNUSED_COUNT":
				sValue = GetBbsMovieUnusedCount(pTag,pArgument).ToString();
				break;
			//お気に入りグループ
			case "$FAVORIT_GROUP_NM":
				SetFieldValue(pTag,"FAVORIT_GROUP_NM",out sValue);
				break;
			case "$USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			//イイネ
			case "$SELF_CAST_PROFILE_MOVIE_LIKED":
				sValue = GetSelfCastProfileMovieLiked(pTag,pArgument);
				break;
			default:
				sValue = ParseCastEx(pTag,pArgument,out pParsed);
				break;
		}
		return sValue;
	}

	private string CreateRadioButtonTag(string pTag,string pArgument) {
		string sFlagValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (Favorit oFavorit = new Favorit()) {
				sFlagValue = oFavorit.GetLoginMailNotRxFlag(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sValues[0],sValues[1]).ToString();
			}
		}

		StringBuilder oStringBuilder = new StringBuilder();
		int iCounter = 0;
		foreach (string sItem in pArgument.Split(',')) {
			oStringBuilder.AppendFormat("<input value=\"{0}\" ",iCounter);
			if (iCounter.ToString().Equals(sFlagValue)) {
				oStringBuilder.Append("checked=\"checked\" ");
			}
			oStringBuilder.AppendFormat("type=\"radio\" name=\"rdoNotRx\" />{0}<br />",sItem);

			iCounter++;
		}
		return oStringBuilder.ToString();
	}

	private string GetAge(string pTag,string pArgument) {
		string sText = "";
		string sValue = "0";

		sText = parseContainer.Parse(pArgument);

		if (sText.Length == 10) {
			sValue = ViCommPrograms.Age(sText).ToString();
		}
		if (sValue.Equals("0")) {
			sValue = "秘密";
		}
		return sValue;
	}

	private string GetAttrTypeNm(string pTag) {
		string sValue = "";
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			oCastAttrType.GetOneByItemNo(sessionMan.site.siteCd,iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["itemno"]));
			sValue = oCastAttrType.attrTypeNm;
		}
		return sValue;
	}

	private string GetChargePoint(string pTag,string pChargeType) {
		string sValue,sCategoryIdx = "",sUserRank = "";
		if (GetValue(pTag,"ACT_CATEGORY_IDX",out sCategoryIdx) == false || GetValue(pTag,"USER_RANK",out sUserRank) == false) {
			return "";
		}
		using (UserCharge oUserCharge = new UserCharge()) {
			if (sessionMan.logined) {
				sValue = oUserCharge.GetChargePoint(
							sessionMan.site.siteCd,
							sessionMan.userMan.userRankCd,
							pChargeType,
							sCategoryIdx,
							sUserRank,
							sessionMan.userMan.userStatus,
							sessionMan.userMan.useFreeDialFlag,
							sessionMan.userMan.useWhitePlanFlag).ToString();
			} else {
				sValue = oUserCharge.GetChargePoint(
							sessionMan.site.siteCd,
							ViCommConst.USER_RANK_A,
							pChargeType,
							sCategoryIdx,
							sUserRank,
							ViCommConst.USER_MAN_NORMAL,
							ViCommConst.FLAG_OFF,
							ViCommConst.FLAG_OFF).ToString();
			}
		}
		return sValue;
	}

	private string GetMonitorPersonCount(string pTag) {
		string sValue = "0",sMonitorTalkType = "",sConnectType = "",sMonitorPersons = "";
		if (GetValue(pTag,"MONITOR_TALK_TYPE",out sMonitorTalkType) == false || GetValue(pTag,"CONNECT_TYPE",out sConnectType) == false || GetValue(pTag,"MONITOR_PERSONS",out sMonitorPersons) == false) {
			sValue = sMonitorTalkType + "<br>" + sConnectType + "<br>";
		} else {
			int iMonitorTalkType;
			int.TryParse(sMonitorTalkType,out iMonitorTalkType);
			if (iMonitorTalkType > 0 && !sConnectType.Equals(ViCommConst.WAIT_TYPE_VOICE)) {
				return string.Format("{0}名覗き中",sMonitorPersons);
			} else {
				return "";
			}
		}
		return sValue;
	}

	private string GetOkList(string pTag) {
		string sValue = "";
		if (GetValue(pTag,"OK_PLAY_MASK",out sValue)) {
			sValue = sessionMan.userMan.castCharacter.GetOkPlayList(sessionMan.site.siteCd,Int64.Parse(sValue));
		}
		return sValue;
	}

	private string GetMailCount(string pTxRxType,string pTag) {
		int iRecCount = 0;
		string sUserSeq = "",sUserCharNo = "";
		if (GetValue(pTag,"USER_SEQ",out sUserSeq) && GetValue(pTag,"USER_CHAR_NO",out sUserCharNo)) {
			if (sessionMan.logined) {
				using (MailBox oMailLog = new MailBox()) {
					iRecCount = oMailLog.GetMailCount(
									pTxRxType,
									sessionMan.site.siteCd,
									sessionMan.userMan.userSeq,
									sessionMan.userMan.userCharNo,
									sUserSeq,
									sUserCharNo
								);
				}

			}
		}
		return iRecCount.ToString();
	}

	private string GetMailCountNow(string pTag) {
		int iTxRecCount = 0;	//男性が送信
		int iRxRecCount = 0;	//男性が受信
		string sUserSeq = "",sUserCharNo = "";
		if (GetValue(pTag,"USER_SEQ",out sUserSeq) && GetValue(pTag,"USER_CHAR_NO",out sUserCharNo)) {
			if (sessionMan.logined) {
				using (MailBox oMailLog = new MailBox()) {
					iRxRecCount = oMailLog.GetMailCount(
									ViCommConst.RX,
									sessionMan.site.siteCd,
									sessionMan.userMan.userSeq,
									sessionMan.userMan.userCharNo,
									sUserSeq,
									sUserCharNo
								);
					iTxRecCount = oMailLog.GetMailCount(
									ViCommConst.TX,
									sessionMan.site.siteCd,
									sUserSeq,
									sUserCharNo,
									sessionMan.userMan.userSeq,
									sessionMan.userMan.userCharNo
								);
				}
			}
		}
		return (iRxRecCount + iTxRecCount).ToString();
	}
	private string GetTalkCount(string pTag) {
		int iRecCount = 0;
		string sValue = "0",sUserSeq = "",sUserCharNo = "";
		if (GetValue(pTag,"USER_SEQ",out sUserSeq) && GetValue(pTag,"USER_CHAR_NO",out sUserCharNo)) {
			if (sessionMan.logined) {
				using (TalkHistory oTalkHis = new TalkHistory()) {
					iRecCount = oTalkHis.GetTalkCount(
									sessionMan.site.siteCd,
									sessionMan.userMan.userSeq,
									sessionMan.userMan.userCharNo,
									sUserSeq,
									sUserCharNo
								);
					sValue = iRecCount.ToString();
				}
			}
		}
		return sValue;
	}

	private string CanTvWShot(string pTag) {
		string sValue = ViCommConst.FLAG_OFF.ToString();
		if (IsTvTalkEnable(pTag) && sessionMan.site.enablePrivteTalkMenuFlag != 0) {
			sValue = IsEqual(pTag,"WSHOT_NA_FLAG",ViCommConst.FLAG_OFF);
		}
		return sValue;
	}

	private string CanVoiceTalk(string pTag) {
		string sValue = ViCommConst.FLAG_OFF.ToString();
		if (IsVoiceTalkEnable(pTag)) {
			sValue = IsEqual(pTag,"WSHOT_NA_FLAG",ViCommConst.FLAG_OFF);
		}
		return sValue;
	}

	private string GetHrefMonitorRoom(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		string[] sValues;
		if (GetValues(pTag,"MONITOR_TALK_TYPE",out sValues) == false) {
			return string.Join("",sValues);
		}

		GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
		if (sValues[0].Equals(ViCommConst.MONITOR_ROOM)) {
			return GetMonitorUrl(pTag,ViCommConst.CHARGE_VIEW_ONLINE,sLabel,"",0,false);
		} else {
			return "";
		}
	}

	public string GetHrefMonitorTalk(string pTag,string pLabel) {
		int iRefresh = 0;
		string sLabel,sAccessKey;
		string[] sValues;
		if (GetValues(pTag,"MONITOR_TALK_TYPE",out sValues) == false) {
			return string.Join("",sValues);
		}
		if (pTag.StartsWith("$HREF_MONITOR_TALK2")) {
			iRefresh = 1;
		}
		GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);

		if ((sessionMan.carrier.Equals(ViCommConst.KDDI)) && (sessionMan.site.supportKddiTvTelFlag == 0)) {
			return string.Format("<a>{0}</a>",sLabel);
		} else {
			if (IsMonitorTalkEnable(pTag)) {
				return GetMonitorUrl(pTag,ViCommConst.CHARGE_VIEW_TALK,pLabel,"",iRefresh,false);
			} else {
				if (iRefresh == 1 || sValues[0].ToString().Equals(ViCommConst.MONITOR_ROOM)) {
					sLabel = "";
				}
				return string.Format("<a>{0}</a>",sLabel);
			}
		}
	}

	public string GetHrefMonitorVoice(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
		if (IsMonitorVoiceEnable(pTag)) {
			return GetMonitorUrl(pTag,ViCommConst.CHARGE_WIRETAPPING,pLabel,"",0,true);
		} else {
			return string.Format("<a>{0}</a>",sLabel);
		}
	}

	public string GetHrefTvTalk(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		if (IsTvTalkEnable(pTag)) {
			return GetTvTalkUrl(pTag,ViCommConst.CHARGE_TALK_PUBLIC,0,pLabel);
		} else {
			GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
			return string.Format("<a>{0}</a>",sLabel);
		}
	}

	public string GetHrefTvWShot(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		string[] sValues;
		if (GetValues(pTag,"WSHOT_NA_FLAG",out sValues) == false) {
			return string.Join("",sValues);
		}

		if (sessionMan.site.enablePrivteTalkMenuFlag != 0) {
			if (IsTvTalkEnable(pTag) && sValues[0].Equals("0")) {
				return GetTvTalkUrl(pTag,ViCommConst.CHARGE_TALK_WSHOT,1,pLabel);
			} else {
				GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
				return string.Format("<a>{0}</a>",sLabel);
			}
		} else {
			return "";
		}
	}

	private string GetHrefVcsPlayProfile(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		string[] sValues;
		if (GetValues(pTag,"REC_SEQ",out sValues) == false) {
			return string.Join("",sValues);
		}
		if (sValues[0].Equals("") == false) {
			return GetVcsMenuUrl(pTag,ViCommConst.VCS_MENU_DIRECT_PLAY_PF,ViCommConst.CHARGE_PLAY_PF_VOICE,pLabel);
		} else {
			GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
			return string.Format("<a>{0}</a>",sLabel);
		}
	}

	private string GetVcsMenuUrl(string pTag,string pVcsMenuId,string pChargeType,string pText) {
		string sLabel,sAccessKey;
		GetLableAndAccessKey(pText,out sLabel,out sAccessKey);

		string[] sValues;
		if (GetValues(pTag,"ACT_CATEGORY_IDX,USER_SEQ,RNUM,CRYPT_VALUE",out sValues) == false) {
			return "";
		}

		return string.Format("<a href=\"RequestIVP.aspx?category={0}&request={1}&menuid={2}&castseq={3}&chgtype={4}&userrecno={5}&{6}={7}\" {9}>{8}</a>",
								sValues[0],ViCommConst.REQUEST_VCS_MENU,pVcsMenuId,sValues[1],pChargeType,sValues[2],sessionMan.site.charNoItemNm,sValues[3],sLabel,sAccessKey);
	}

	public string GetHrefVoiceChat(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		if (IsVoiceTalkEnable(pTag)) {
			return GetVoiceTalkUrl(pTag,ViCommConst.CHARGE_TALK_VOICE_PUBLIC,pLabel);
		} else {
			GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
			return string.Format("<a>{0}</a>",sLabel);
		}
	}

	public string GetHrefVoiceTalk(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		string[] sValues;
		if (GetValues(pTag,"WSHOT_NA_FLAG",out sValues) == false) {
			return string.Join("",sValues);
		}
		if (IsVoiceTalkEnable(pTag) && sValues[0].Equals("0")) {
			return GetVoiceTalkUrl(pTag,ViCommConst.CHARGE_TALK_VOICE_WSHOT,pLabel);
		} else {
			GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
			return string.Format("<a>{0}</a>",sLabel);
		}
	}


	private string GetQueryCancelForward(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,CRYPT_VALUE",out sValues)) {
			return string.Format("puserseq={0}&{1}={2}",sValues[0],sessionMan.site.charNoItemNm,sValues[1]);
		} else {
			return "";
		}
	}

	private string GetRegistMailCast() {
		string sCodeType = ViCommConst.REGIST_FROM_NONE;
		string sCode = "";

		if (!sessionMan.affiliaterCd.Equals("")) {
			sCodeType = sessionMan.affiliateCompany;
			sCode = sessionMan.affiliaterCd;
		} else if (!sessionMan.adCd.Equals("")) {
			sCodeType = ViCommConst.REGIST_FROM_AD;
			sCode = sessionMan.adCd;
		}

		return string.Format("regm{0}{1:d2}{2}{3}{4}{5}@{6}",
					sessionMan.site.siteCd,
					GetDataRow(ViCommConst.DATASET_CAST)["LOGIN_ID"].ToString().Length + 2,
					GetDataRow(ViCommConst.DATASET_CAST)["LOGIN_ID"].ToString() + GetDataRow(ViCommConst.DATASET_CAST)["USER_CHAR_NO"].ToString(),
					sessionMan.carrier,
					sCodeType,
					sCode,
					sessionMan.site.mailHost);
	}

	private bool IsTvTalkEnable(string pTag) {
		bool bRet = false;
		string[] sValues;
		if (GetValues(pTag,"CHARACTER_ONLINE_STATUS,CONNECT_TYPE,MONITOR_TALK_TYPE",out sValues) == false) {
			return bRet;
		}

		int iStat = int.Parse(sValues[0]);
		string sWaitType = sValues[1];
		string sMonitorTalkType = sValues[2];

		if ((sessionMan.carrier.Equals(ViCommConst.KDDI)) && (sessionMan.site.supportKddiTvTelFlag == 0)) {
			return false;
		}

		if ((iStat == ViCommConst.USER_WAITING) && (!sWaitType.ToString().Equals(ViCommConst.WAIT_TYPE_VOICE))) {
			bRet = true;
		} else if ((sMonitorTalkType.Equals(ViCommConst.MONITOR_ROOM)) && (sessionMan.site.supportChgMonitorToTalkFlag == 1)) {
			bRet = true;
		}
		return bRet;
	}

	private bool IsVoiceTalkEnable(string pTag) {
		bool bRet = false;

		string[] sValues;
		if (GetValues(pTag,"CHARACTER_ONLINE_STATUS,CONNECT_TYPE,MONITOR_TALK_TYPE",out sValues) == false) {
			return bRet;
		}

		int iStat = int.Parse(sValues[0]);
		string sWaitType = sValues[1];
		string sMonitorTalkType = sValues[2];

		if ((iStat == ViCommConst.USER_WAITING) && (!sWaitType.ToString().Equals(ViCommConst.WAIT_TYPE_VIDEO))) {
			bRet = true;
		} else if ((sMonitorTalkType.Equals(ViCommConst.MONITOR_ROOM)) && (sessionMan.site.supportChgMonitorToTalkFlag == 1)) {
			bRet = true;
		}
		return bRet;
	}

	private bool IsMonitorTalkEnable(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"TALK_LOCK_FLAG,TALK_LOCK_DATE,MONITOR_TALK_TYPE,CONNECT_TYPE,MAN_USER_SITE_CD",out sValues) == false) {
			return false;
		}

		bool bRet = true;
		string sTalkLockFlag = sValues[0];
		string sTalkLockDateTime = sValues[1];
		string sMonitorTalkType = sValues[2];
		string sConnectType = sValues[3];
		string sCurTalkerManSiteCd = sValues[4];
		DateTime dtLockDate;

		if (sTalkLockFlag.Equals("1")) {
			if (!sTalkLockDateTime.Equals("")) {
				if (GetValue(pTag,"TALK_LOCK_DATE",out dtLockDate) && dtLockDate.AddSeconds(90) > DateTime.Now) {
					bRet = false;
				}
			}
		}

		if (!sMonitorTalkType.Equals(ViCommConst.MONITOR_TALK)) {
			bRet = false;
		}

		if (sConnectType.Equals(ViCommConst.WAIT_TYPE_VOICE)) {
			bRet = false;
		}

		if (bRet) {
			if (sCurTalkerManSiteCd.Equals("M003")) {
				bRet = sessionMan.site.siteCd.Equals("M003");
			} else {
				bRet = !(sessionMan.site.siteCd.Equals("M003"));
			}
		}
		return bRet;
	}

	public bool IsMonitorVoiceEnable(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"TALK_LOCK_FLAG,TALK_LOCK_DATE,MONITOR_TALK_TYPE",out sValues) == false) {
			return false;
		}

		bool bRet = true;
		string sTalkLockFlag = sValues[0];
		string sTalkLockDateTime = sValues[1];
		string sMonitorTalkType = sValues[2];
		DateTime dtLockDate;

		if (sTalkLockFlag.Equals("1")) {
			if (!sTalkLockDateTime.Equals("")) {
				if (GetValue(pTag,"TALK_LOCK_DATE",out dtLockDate) && dtLockDate.AddSeconds(90) > DateTime.Now) {
					bRet = false;
				}
			}
		}

		if (!sMonitorTalkType.Equals(ViCommConst.MONITOR_VOICE)) {
			bRet = false;
		}
		return bRet;
	}
	private string GetTvTalkUrl(string pTag,string pChargeType,int pPrivateFlag,string pText) {
		string[] sValues;
		if (GetValues(pTag,"MONITOR_TALK_TYPE,ACT_CATEGORY_IDX,USER_SEQ,RNUM,CRYPT_VALUE",out sValues) == false) {
			return "";
		}

		string sMenuId = "";
		string sActionDTMF = "";
		string sLabel,sAccessKey;
		GetLableAndAccessKey(pText,out sLabel,out sAccessKey);

		if (pPrivateFlag != 0) {
			sMenuId = ViCommConst.MENU_VIDEO_WTALK;
			sActionDTMF = ViCommConst.DTMF_VIDEO_WTALK;
		} else {
			sMenuId = ViCommConst.MENU_VIDEO_PTALK;
			sActionDTMF = ViCommConst.DTMF_VIDEO_PTALK;
		}

		string sUrl = "";
		if (sValues[0].Equals(ViCommConst.MONITOR_ROOM) == false) {
			sUrl = string.Format("<a href=\"RequestIVP.aspx?category={0}&request={1}&menuid={2}&castseq={3}&chgtype={4}&userrecno={5}&{6}={7}\" {9}>{8}</a>",
								sValues[1],ViCommConst.REQUEST_TALK,sMenuId,sValues[2],pChargeType,sValues[3],sessionMan.site.charNoItemNm,sValues[4],sLabel,sAccessKey);
		} else {
			sUrl = GetMonitorUrl(pTag,pChargeType,pText,sActionDTMF,0,false);
		}
		return sUrl;
	}

	private string GetVoiceTalkUrl(string pTag,string pChargeType,string pText) {
		string[] sValues;
		if (GetValues(pTag,"MONITOR_TALK_TYPE,ACT_CATEGORY_IDX,USER_SEQ,RNUM,CRYPT_VALUE",out sValues) == false) {
			return "";
		}

		string sUrl,sMenuId,sLabel,sAccessKey;
		GetLableAndAccessKey(pText,out sLabel,out sAccessKey);

		if (pChargeType.Equals(ViCommConst.CHARGE_TALK_VOICE_PUBLIC)) {
			sMenuId = ViCommConst.MENU_VOICE_PTALK;
		} else {
			sMenuId = ViCommConst.MENU_VOICE_WTALK;
		}

		if (sValues[0].Equals("2") == false) {
			sUrl = string.Format("<a href=\"RequestIVP.aspx?category={0}&request={1}&menuid={2}&castseq={3}&chgtype={4}&userrecno={5}&{6}={7}\" {9}>{8}</a>",
								sValues[1],ViCommConst.REQUEST_TALK,sMenuId,sValues[2],pChargeType,sValues[3],sessionMan.site.charNoItemNm,sValues[4],sLabel,sAccessKey);
		} else {
			if (pChargeType.Equals(ViCommConst.CHARGE_TALK_VOICE_PUBLIC)) {
				sUrl = GetMonitorUrl(pTag,pChargeType,pText,ViCommConst.DTMF_VOICE_PTALK,0,true);
			} else {
				sUrl = GetMonitorUrl(pTag,pChargeType,pText,ViCommConst.DTMF_VOICE_WTALK,0,true);
			}
		}
		return sUrl;
	}

	private string GetMonitorUrl(string pTag,string pChargeType,string pText,string pMenuId,int pRefresh,bool bVoice) {
		string[] sValues;
		if (GetValues(pTag,"MONITOR_TALK_TYPE,ACT_CATEGORY_IDX,USER_SEQ,LOGIN_ID,RNUM,CRYPT_VALUE",out sValues) == false) {
			return "";
		}
		string sRequest;

		if (sValues[0].Equals("2")) {
			sRequest = ViCommConst.REQUEST_VIEW_BROADCASTING;
		} else {
			if (bVoice) {
				sRequest = ViCommConst.REQUEST_MONITOR_VOICE;
			} else {
				sRequest = ViCommConst.REQUEST_VIEW_TALK;
			}
		}

		string sLabel,sAccessKey;
		GetLableAndAccessKey(pText,out sLabel,out sAccessKey);

		return string.Format("<a href=\"RequestIVP.aspx?category={0}&request={1}&castseq={2}&chgtype={3}&menuid={4}&refresh={5}&loginid={6}&userrecno={7}&{8}={9}\" {11}>{10}</a>",
										sValues[1],sRequest,sValues[2],pChargeType,pMenuId,pRefresh,sValues[3],sValues[4],sessionMan.site.charNoItemNm,sValues[5],sLabel,sAccessKey);
	}

	private string GetVcsNoPersonMenuUrl(string pVcsMenuId,string pChargeType,string pText) {
		string sLabel,sAccessKey;
		GetLableAndAccessKey(pText,out sLabel,out sAccessKey);

		return string.Format("<a href=\"RequestIVP.aspx?category={0}&request={1}&menuid={2}&castseq={3}&chgtype={4}&userrecno={5}\" {7}>{6}</a>",
								1,ViCommConst.REQUEST_VCS_MENU,pVcsMenuId,"",pChargeType,0,sLabel,sAccessKey);
	}

	private bool IsQuickResponse(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"QUICK_RES_FLAG,QUICK_RES_IN_TIME,QUICK_RES_OUT_SCH_TIME",out sValues) == false) {
			return false;
		}

		bool bRet = false;

		if (sValues[0].Equals("1")) {
			DateTime dtInTime = DateTime.Parse(sValues[1]);
			DateTime dtOutSchTime = DateTime.Parse(sValues[2]);
			if (dtInTime < DateTime.Now && dtOutSchTime > DateTime.Now) {
				bRet = true;
			}
		}
		return bRet;
	}

	private bool IsNowCall(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"TALK_LOCK_FLAG,TALK_LOCK_DATE",out sValues) == false) {
			return false;
		}

		bool bRet = false;
		string sTalkLockFlag = sValues[0];
		string sTalkLockDateTime = sValues[1];
		DateTime dtLockDate;

		if (sTalkLockFlag.Equals("1")) {
			if (!sTalkLockDateTime.Equals("")) {
				if (GetValue(pTag,"TALK_LOCK_DATE",out dtLockDate) && dtLockDate.AddSeconds(90) < DateTime.Now) {
					bRet = true;
				}
			}
		}
		return bRet;
	}

	public string CanMonitorTalk(string pTag) {
		string sValue;
		if ((sessionMan.carrier.Equals(ViCommConst.KDDI)) && (sessionMan.site.supportKddiTvTelFlag == 0)) {
			sValue = ViCommConst.FLAG_OFF.ToString();
		} else {
			sValue = IsEqual(pTag,IsMonitorTalkEnable(pTag));
		}
		return sValue;
	}

	public string GetCastItem(string pTag,string pArgument) {
		string sValue;
		SetFieldValue(pTag,"CAST_ATTR_VALUE" + pArgument,out sValue);
		return sValue;
	}
	public string GetCastItemCd(string pTag,string pArgument) {
		string sValue;
		SetFieldValue(pTag,"CAST_ATTR_VALUE_CD" + pArgument,out sValue);
		return sValue;
	}

	public string GetQueryCastProfile(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ACT_CATEGORY_IDX,LOGIN_ID,RNUM,CRYPT_VALUE",out sValues)) {
			return string.Format("seekmode={0}&category={1}&loginid={2}&userrecno={3}&{4}={5}",
							   sessionMan.seekMode,sValues[0],sValues[1],sValues[2],sessionMan.site.charNoItemNm,sValues[3]
					) + sessionMan.seekCondition.GetConditionQuery() + sessionMan.AddUniqueQuery(GetDataRow(this.currentTableIdx),this.sessionMan.requestQuery);
		} else {
			return "";
		}
	}

	private string GetQueryDirectProfile(string pTag) {
		string[] sValues;
		bool bGet = false;
		switch (this.currentTableIdx) {
			case ViCommConst.DATASET_MAIL:
				bGet = GetValues(pTag,"PARTNER_LOGIN_ID,CRYPT_VALUE",out sValues);
				break;
			default:
				bGet = GetValues(pTag,"LOGIN_ID,CRYPT_VALUE",out sValues);
				break;
		}

		if (bGet) {
			return string.Format("direct=1&loginid={0}&{1}={2}&userrecno=1",sValues[0],sessionMan.site.charNoItemNm,sValues[1]);
		} else {
			return "";
		}
	}


	private string GetFavoritMeCount(string pTag) {
		string sValue = "",sUserSeq = "",sUserCharNo = "";
		if (GetValue(pTag,"USER_SEQ",out sUserSeq) && GetValue(pTag,"USER_CHAR_NO",out sUserCharNo)) {
			int iCnt = sessionMan.userMan.castCharacter.GetFavoritCount(sessionMan.site.siteCd,sUserSeq,sUserCharNo);
			sValue = iCnt.ToString();
		}
		return sValue;
	}

	private int GetCastBbsNewFlag(string pTag) {
		string[] sValues;
		int iFlag = ViCommConst.FLAG_OFF;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (CastBbs oBbs = new CastBbs()) {
				iFlag = oBbs.GetNewlyArrived(sessionMan.site.siteCd,sValues[0],sValues[1],sessionMan.site.bbsNewHours);
			}
		}
		return iFlag;
	}

	private int GetCastMovieNewFlag(string pTag,string pMovieType,int pNewHours) {
		string[] sValues;
		int iFlag = ViCommConst.FLAG_OFF;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (CastMovie oMovie = new CastMovie()) {
				iFlag = oMovie.GetNewlyArrived(sessionMan.site.siteCd,sValues[0],sValues[1],pMovieType,pNewHours);
			}
		}
		return iFlag;
	}

	private int GetCastAttrMovieNewFlag(string pTag,string pMovieType,int pNewHours,string pAttr) {
		string[] sValues;
		int iFlag = ViCommConst.FLAG_OFF;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues) && !string.IsNullOrEmpty(pAttr)) {
			using (CastMovie oMovie = new CastMovie()) {
				iFlag = oMovie.GetNewlyArrived(sessionMan.site.siteCd,sValues[0],sValues[1],pMovieType,pNewHours,pAttr);
			}
		}
		return iFlag;
	}

	private int GetCastBbsPicNewFlag(string pTag) {
		string[] sValues;
		int iFlag = ViCommConst.FLAG_OFF;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (CastPic oPic = new CastPic()) {
				iFlag = oPic.GetNewlyArrived(sessionMan.site.siteCd,sValues[0],sValues[1],ViCommConst.ATTACHED_BBS.ToString(),sessionMan.site.picBbsNewHours);
			}
		}
		return iFlag;
	}

	private int GetCastAttrBbsPicNewFlag(string pTag,string pAttr) {
		string[] sValues;
		int iFlag = ViCommConst.FLAG_OFF;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues) && !string.IsNullOrEmpty(pAttr)) {
			using (CastPic oPic = new CastPic()) {
				iFlag = oPic.GetNewlyArrived(sessionMan.site.siteCd,sValues[0],sValues[1],ViCommConst.ATTACHED_BBS.ToString(),sessionMan.site.picBbsNewHours,pAttr);
			}
		}
		return iFlag;
	}

	private string GetBbsLastActionType(string pTag) {
		string[] sValues;
		string sValue;
		string sType = "0";
		if (GetValue(pTag,"BBS_LAST_ACTION_TYPE",out sValue)) {
			if (string.IsNullOrEmpty(sValue)) {
				if (GetValues(pTag,"BBS_NON_PUBLISH_FLAG,USER_SEQ,USER_CHAR_NO",out sValues)) {
					if (ViCommConst.FLAG_OFF_STR.Equals(sValues[0])) {
						using (CastBbs oCastBbs = new CastBbs()) {
							if (oCastBbs.GetWriteCount(sessionMan.site.siteCd,sValues[1],sValues[2]) > 0) {
								sType = ViCommConst.BbsObjType.BBSDOC;
							}
						}
					}
				}
			} else {
				sType = sValue;
			}
		}
		return sType;
	}

	private string GetBbsNewlyActionType(string pTag) {
		string[] sValues;
		string sType = "0";
		if (GetValues(pTag,"BBS_LAST_ACTION_TYPE,BBS_LAST_ACTION_DATE",out sValues)) {
			DateTime dtLastAction;
			if (DateTime.TryParse(sValues[1],out dtLastAction)) {
				switch (sValues[0]) {
					case "1":		// 掲示板書込
						if (dtLastAction.AddHours(sessionMan.site.bbsNewHours) >= DateTime.Now) {
							sType = sValues[0];
						}
						break;
					case "2":		// 掲示板写真
						if (dtLastAction.AddHours(sessionMan.site.picBbsNewHours) >= DateTime.Now) {
							sType = sValues[0];
						}
						break;
					case "3":		// 掲示板動画
						if (dtLastAction.AddHours(sessionMan.site.movieBbsNewHours) >= DateTime.Now) {
							sType = sValues[0];
						}
						break;
				}
			} else {
				if (GetValues(pTag,"BBS_NON_PUBLISH_FLAG,USER_SEQ,USER_CHAR_NO",out sValues)) {
					if (ViCommConst.FLAG_OFF_STR.Equals(sValues[0])) {
						using (CastBbs oCastBbs = new CastBbs()) {
							if (oCastBbs.GetWriteCount(sessionMan.site.siteCd,sValues[1],sValues[2]) > 0) {
								DateTime? dtLastWrite = oCastBbs.GetLastWriteDate(sessionMan.site.siteCd,sValues[1],sValues[2]);
								if (dtLastWrite.HasValue && dtLastWrite.Value.AddHours(sessionMan.site.bbsNewHours) >= DateTime.Now) {
									sType = ViCommConst.BbsObjType.BBSDOC;
								}
							}
						}
					}
				}
			}
		}
		return sType;
	}

	private string GetBbsLastActionDate(string pTag) {
		string[] sValues;
		string sValue;
		if (GetValue(pTag,"BBS_LAST_ACTION_DATE",out sValue)) {
			DateTime dtLastAction;
			if (DateTime.TryParse(sValue,out dtLastAction)) {
				return dtLastAction.ToString("yyyy/MM/dd HH:mm:ss");
			} else {
				if (GetValues(pTag,"BBS_NON_PUBLISH_FLAG,USER_SEQ,USER_CHAR_NO",out sValues)) {
					if (ViCommConst.FLAG_OFF_STR.Equals(sValues[0])) {
						using (CastBbs oCastBbs = new CastBbs()) {
							if (oCastBbs.GetWriteCount(sessionMan.site.siteCd,sValues[1],sValues[2]) > 0) {
								DateTime? dtLastWrite = oCastBbs.GetLastWriteDate(sessionMan.site.siteCd,sValues[1],sValues[2]);
								if (dtLastWrite.HasValue) {
									return dtLastWrite.Value.ToString("yyyy/MM/dd HH:mm:ss");
								}
							}
						}
					}
				}
			}
		}
		return string.Empty;
	}

	private string GetBbsNewlyActionDate(string pTag) {
		string[] sValues;
		string sValue = string.Empty;
		if (GetValues(pTag,"BBS_LAST_ACTION_TYPE,BBS_LAST_ACTION_DATE",out sValues)) {
			DateTime dtLastAction;
			if (DateTime.TryParse(sValues[1],out dtLastAction)) {
				switch (sValues[0]) {
					case "1":		// 掲示板書込
						if (dtLastAction.AddHours(sessionMan.site.bbsNewHours) >= DateTime.Now) {
							sValue = dtLastAction.ToString("yyyy/MM/dd HH:mm:ss");
						}
						break;
					case "2":		// 掲示板写真
						if (dtLastAction.AddHours(sessionMan.site.picBbsNewHours) >= DateTime.Now) {
							sValue = dtLastAction.ToString("yyyy/MM/dd HH:mm:ss");
						}
						break;
					case "3":		// 掲示板動画
						if (dtLastAction.AddHours(sessionMan.site.movieBbsNewHours) >= DateTime.Now) {
							sValue = dtLastAction.ToString("yyyy/MM/dd HH:mm:ss");
						}
						break;
				}
			} else {
				if (GetValues(pTag,"BBS_NON_PUBLISH_FLAG,USER_SEQ,USER_CHAR_NO",out sValues)) {
					if (ViCommConst.FLAG_OFF_STR.Equals(sValues[0])) {
						using (CastBbs oCastBbs = new CastBbs()) {
							if (oCastBbs.GetWriteCount(sessionMan.site.siteCd,sValues[1],sValues[2]) > 0) {
								DateTime? dtLastWrite = oCastBbs.GetLastWriteDate(sessionMan.site.siteCd,sValues[1],sValues[2]);
								if (dtLastWrite.HasValue && dtLastWrite.Value.AddHours(sessionMan.site.bbsNewHours) >= DateTime.Now) {
									sValue = dtLastWrite.Value.ToString("yyyy/MM/dd HH:mm:ss");
								}
							}
						}
					}
				}
			}
		}
		return sValue;
	}

	private string GetFavoritMemo(string pTag) {
		string sMemo = "";
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (Favorit oFavorit = new Favorit()) {
				sMemo = oFavorit.GetFavoritComment(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sValues[0],sValues[1]);
			}
		}
		return sMemo;
	}

	private string GetRefuseMemo(string pTag) {
		string sMemo = "";
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (Refuse oRefuse = new Refuse()) {
				if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sValues[0],sValues[1])) {
					sMemo = oRefuse.refuseComment;
				}
			}
		}
		return sMemo;
	}

	private string GetBbsObjFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"BBS_PIC_FLAG,BBS_MOVIE_FLAG",out sValues) == false) {
			return ViCommConst.FLAG_OFF.ToString();
		}
		if (sValues[0].Equals("1") || sValues[1].Equals("1")) {
			return ViCommConst.FLAG_ON.ToString();
		} else {
			return ViCommConst.FLAG_OFF.ToString();
		}
	}

	private string GetBbsAllObjFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"BBS_PIC_FLAG,BBS_MOVIE_FLAG,BBS_FLAG",out sValues) == false) {
			return ViCommConst.FLAG_OFF.ToString();
		}
		if (sValues[0].Equals("1") || sValues[1].Equals("1") || sValues[2].Equals("1")) {
			return ViCommConst.FLAG_ON.ToString();
		} else {
			return ViCommConst.FLAG_OFF.ToString();
		}
	}

	private int GetAvailableMin(string pTag,string pArgument) {
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,ACT_CATEGORY_SEQ",out sValues) == false) {
			return 0;
		}

		int iUnitSec;
		int iPoint;
		double dwSec,dwMin = 0;
		sessionMan.userMan.GetCornerCharge(sessionMan.site.siteCd,sValues[0],pArgument,sValues[1],out iUnitSec,out iPoint);
		if (iUnitSec > 0) {
			double dw = (sessionMan.userMan.balPoint + sessionMan.userMan.limitPoint + sessionMan.userMan.servicePoint) / iPoint;
			dwSec = System.Math.Floor(dw) * iUnitSec;
			dwMin = System.Math.Floor(dwSec / 60);
		}

		return (int)dwMin;
	}

	private int IsPickup(string pTag,String pArgument) {
		int isPickup = ViCommConst.FLAG_OFF;

		string[] sArguments = pArgument.Split(',');
		string sPickupId = sArguments[0];
		string isPickupFlag = ViCommConst.FLAG_OFF_STR;
		if (sArguments.Length > 1) {
			isPickupFlag = sArguments[1];
		}
		string sPickupType = string.Empty;
		using (Pickup oPickup = new Pickup()) {
			if (oPickup.GetOne(sessionMan.site.siteCd,sPickupId)) {
				sPickupType = oPickup.pickupType;
				string sPickupMask = string.Empty;
				if (SetFieldValue(pTag,"PICKUP_MASK",out sPickupMask)) {
					long lPickupMask = 0;
					if (long.TryParse(sPickupMask,out lPickupMask)) {
						isPickup = ((lPickupMask & oPickup.pickupMask) > 0) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
					}
				}
			}
		}

		string[] sValues;
		if (!GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			return ViCommConst.FLAG_OFF;
		}

		switch (sPickupType) {
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				if ((isPickup == ViCommConst.FLAG_ON) && isPickupFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					using (PickupCharacter oPickupCharacter = new PickupCharacter()) {
						if (oPickupCharacter.GetOne(sessionMan.site.siteCd,sValues[0],sValues[1],sPickupId)) {
							if (oPickupCharacter.pickupFlag &&
								oPickupCharacter.pickupStartPubDay.CompareTo(DateTime.Now) <= 0 &&
								DateTime.Now.CompareTo(oPickupCharacter.pickupEndPubDay) < 0) {
								isPickup = ViCommConst.FLAG_ON;
							} else {
								isPickup = ViCommConst.FLAG_OFF;
							}
						}
					}
				}
				break;
			case ViCommConst.PicupTypes.BBS_MOVIE:
			case ViCommConst.PicupTypes.BBS_PIC:
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.PROFILE_PIC:
				using (PickupObjs oPickupObjs = new PickupObjs()) {
					if (oPickupObjs.IsExist(sessionMan.site.siteCd,sValues[0],sValues[1],sPickupId,sPickupType)) {
						isPickup = ViCommConst.FLAG_ON;
						if (isPickupFlag.Equals(ViCommConst.FLAG_ON_STR) && (!oPickupObjs.pickupFlag || !oPickupObjs.publishFlag)) {
							isPickup = ViCommConst.FLAG_OFF;
						}
					} else {
						isPickup = ViCommConst.FLAG_OFF;
					}
				}
				break;
			default:
				break;
		}

		return isPickup;
	}

	private string GetFavoriteLoginMailNotRxFlag(string pTag) {
		string sFlagValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (Favorit oFavorit = new Favorit()) {
				sFlagValue = oFavorit.GetLoginMailNotRxFlag(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sValues[0],sValues[1]).ToString();
			}
		}

		return sFlagValue;
	}

	private string GetListPersonalProfPic(string pTag,string pArgument) {
		DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.withoutProf = iBridUtil.GetStringValue(oPartsArgs.Query["withoutprof"]);

		System.Data.DataSet oDataSet = null;
		string[] sValues;
		if (GetValues(pTag,"LOGIN_ID,USER_CHAR_NO",out sValues)) {
			using (CastPic oCastPic = new CastPic(sessionObjs)) {
				oDataSet = oCastPic.GetPageCollection(sessionMan.site.siteCd,sValues[0],sValues[1],false,ViCommConst.ATTACHED_PROFILE.ToString(),string.Empty,sAttrTypeSeq,sAttrSeq,oCondition,1,oPartsArgs.NeedCount);
			}
		} else {
			return string.Empty;
		}

		if (oDataSet == null)
			return string.Empty;
		return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_PARTNER_PIC);

	}

	private string GetDiaryHeaderTitle(string pTag) {
		string sValue = string.Empty;
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (Cast oCast = new Cast()) {
				if (oCast.GetCastInfo(sessionMan.site.siteCd,sValues[0],sValues[1])) {
					sValue = oCast.diaryHeaderTitle;
				}
			}
		}
		return sValue;
	}

	private string GetContainsOfCarrier(string pTag,params string[] pCarriers) {
		string sValue;
		string sFieldNm = string.Format("{0}USER_SEQ",sessionObjs.tableIndex.Equals(ViCommConst.DATASET_MAIL) ? "PARTNER_" : String.Empty);
		if (GetValue(pTag,sFieldNm,out sValue)) {
			using (User oUser = new User()) {
				if (oUser.GetOne(sValue,ViCommConst.OPERATOR)) {
					return Array.Exists<string>(pCarriers,delegate(string carrier) {
						return oUser.mobileCarrierCd.Equals(carrier);
					}) ?
						ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				}
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}
	
	private int GetBbsPicUnusedCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		string[] sValues;
		
		if (GetValues(pTag,"LOGIN_ID,USER_CHAR_NO",out sValues)) {
			SeekCondition oCondition = new SeekCondition();
			oCondition.unusedFlag = ViCommConst.FLAG_ON_STR;
			
			string sSiteCd = this.sessionMan.site.siteCd;
			string sLoginId = sValues[0];
			string sUserCharNo = sValues[1];
			
			using (CastPic oCastPic = new CastPic(sessionObjs)) {
				oCastPic.GetPageCount(
					sSiteCd,
					sLoginId,
					sUserCharNo,
					false,
					ViCommConst.ATTACHED_BBS.ToString(),
					"",
					string.Empty,
					string.Empty,
					false,
					oCondition,
					1,
					out dRecCount
				);
			}
		}
		
		return (int)dRecCount;
	}

	private int GetBbsMovieUnusedCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		string[] sValues;

		if (GetValues(pTag,"LOGIN_ID,USER_CHAR_NO",out sValues)) {
			SeekCondition oCondition = new SeekCondition();
			oCondition.unusedFlag = ViCommConst.FLAG_ON_STR;

			string sSiteCd = this.sessionMan.site.siteCd;
			string sLoginId = sValues[0];
			string sUserCharNo = sValues[1];

			using (CastMovie oCastMovie = new CastMovie(sessionObjs)) {
				oCastMovie.GetPageCount(
					sSiteCd,
					sLoginId,
					sUserCharNo,
					"",
					false,
					ViCommConst.ATTACHED_BBS.ToString(),
					"",
					string.Empty,
					string.Empty,
					false,
					oCondition,
					1,
					true,
					out dRecCount
				);
			}
		}

		return (int)dRecCount;
	}

	private string GetCastCommentList(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sUseCommentDetailFlag = string.Empty;

		SetFieldValue(pTag,"COMMENT_LIST",out sValue);

		if (GetValue(pTag,"USE_COMMENT_DETAIL_FLAG",out sUseCommentDetailFlag)) {
			if (sUseCommentDetailFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				string sCommentDetail;

				SetFieldValue(pTag,"COMMENT_DETAIL",out sCommentDetail);

				if (!string.IsNullOrEmpty(sCommentDetail)) {
					sValue = sCommentDetail;
				}
			}
		}

		return sValue;
	}

	private string GetSelfCastProfileMovieLiked(string pTag,string pArgument) {
		string sValue = ViCommConst.FLAG_OFF_STR;
		string sProfileMovieSeq = string.Empty;

		if (sessionMan.logined) {
			if (GetValue(pTag,"PROFILE_MOVIE_SEQ",out sProfileMovieSeq)) {
				using(CastMovieLike oCastMovieLike = new CastMovieLike()) {
					sValue = oCastMovieLike.IsExist(sessionMan.site.siteCd,sProfileMovieSeq,sessionMan.userMan.userSeq);
				}
			}
		}

		return sValue;
	}

	private string GetCastMonitorOkFlag(string pTag) {
		string sMonitorTalkFlag = CanMonitorTalk(pTag);
		string sMonitorVoiceFlag = IsEqual(pTag,IsMonitorVoiceEnable(pTag));

		if (sMonitorTalkFlag.Equals(ViCommConst.FLAG_ON_STR) || sMonitorVoiceFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private string GetCastFavoritImg(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"LIKE_ME_FLAG,FAVORIT_FLAG",out sValues)) {
			string sLikeMeFlag = sValues[0];
			string sFavoritFlag = sValues[1];

			if (sLikeMeFlag.Equals(ViCommConst.FLAG_ON_STR) && sFavoritFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = "<img src=\"../Image/" + sessionMan.site.siteCd + "/men_both.gif\" />";
			} else if (sLikeMeFlag.Equals(ViCommConst.FLAG_ON_STR) && sFavoritFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				sValue = "<img src=\"../Image/" + sessionMan.site.siteCd + "/men_w_to_m.gif\" />";
			} else if (sLikeMeFlag.Equals(ViCommConst.FLAG_OFF_STR) && sFavoritFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sValue = "<img src=\"../Image/" + sessionMan.site.siteCd + "/men_m_to_w.gif\" />";
			}
		}

		return sValue;
	}
}
