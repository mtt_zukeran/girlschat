﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseMail(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";
		string sObjType;
		string sMailType = "";
		string sPresentMailItemSeq = string.Empty;
		string sPresentReturnFlag = string.Empty;

		switch (pTag) {
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$CAST_STATUS_IS_NORMAL_FLAG":
				sValue = CastStatusIsNormal(pTag);
				break;
			case "$DIV_IS_MAIL_WITH_MOVIE":
				if (GetValue(pTag,"ATTACHED_OBJ_TYPE",out sObjType)) {
					SetNoneDisplay(!(sObjType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())));
				}
				break;
			case "$DIV_IS_MAIL_WITH_PIC":
				if (GetValue(pTag,"ATTACHED_OBJ_TYPE",out sObjType)) {
					SetNoneDisplay(!(sObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())));
				}
				break;
			case "$DIV_IS_RX_MAIL":
				if (GetValue(pTag,"TXRX_TYPE",out sMailType)) {
					SetNoneDisplay(!(sMailType.Equals(ViCommConst.RX)));
				}
				break;
			case "$DIV_IS_TX_MAIL":
				if (GetValue(pTag,"TXRX_TYPE",out sMailType)) {
					SetNoneDisplay(!(sMailType.Equals(ViCommConst.TX)));
				}
				break;
			case "$HREF_CAST_MOVIE_DOWNLOAD":
				sValue = GetHrefMailMovieDownload(pTag,pArgument);
				break;
			case "$HREF_SELF_MOVIE_DOWNLOAD":
				sValue = GetHrefSelfMovieDownload(pTag,pArgument);
				break;
			case "$MAIL_ATTACHED_OPEN_FLAG":
				SetFieldValue(pTag,"ATTACHED_OBJ_OPEN_FLAG",out sValue);
				break;
			case "$MAIL_DEL_PROTECT_FLAG":
				SetFieldValue(pTag,"DEL_PROTECT_FLAG",out sValue);
				break;
			case "$MAIL_CREATE_DATE":
				SetFieldValue(pTag,"CREATE_DATE",out sValue);
				break;
			case "$MAIL_DELETE_CHECK_BOX":
				sValue = GetCheckBoxTag(pTag,"MAIL_SEQ");
				break;
			case "$MAIL_DOC":
				sValue = GetMailDoc(pTag);
				break;
			case "$MAIL_MOVIE_TITLE":
				sValue = GetMailMovieTitle(pTag);
				break;
			case "$MAIL_NEW_FLAG":
				sValue = GetMailNewFlag(pTag).ToString();
				break;
			case "$MAIL_OBJ_SEQ":
				SetFieldValue(pTag,"OBJ_SEQ",out sValue);
				break;
			case "$MAIL_PIC_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAIL_READ_FLAG":
				SetFieldValue(pTag,"READ_FLAG",out sValue);
				break;
			case "$MAIL_RETURN_FLAG":
				SetFieldValue(pTag,"RETURN_MAIL_FLAG",out sValue);
				break;
			case "$MAIL_SUBJECT":
				SetFieldValue(pTag,"MAIL_TITLE",out sValue);
				break;
			case "$MAIL_SERVICE_POINT":
				SetFieldValue(pTag,"SERVICE_POINT",out sValue);
				break;
			case "$QUERY_DIRECT_PROFILE":
				sValue = GetQueryDirectProfile(pTag);
				break;
			case "$QUERY_MAIL_INFO_ALL":
				sValue = string.Format("mailtype={0}",ViCommConst.DELETE_MAIL_INFO);
				break;
			case "$QUERY_MAIL_INFO_READ_ONLY":
				sValue = string.Format("mailtype={0}&readonly={1}",ViCommConst.DELETE_MAIL_INFO,ViCommConst.FLAG_ON.ToString());
				break;
			case "$QUERY_MAIL_RX_ALL":
				sValue = string.Format("mailtype={0}",ViCommConst.DELETE_MAIL_RX);
				break;
			case "$QUERY_MAIL_RX_READ_ONLY":
				sValue = string.Format("mailtype={0}&readonly={1}",ViCommConst.DELETE_MAIL_RX,ViCommConst.FLAG_ON.ToString());
				break;
			case "$QUERY_MAIL_TX_ALL":
				sValue = string.Format("mailtype={0}",ViCommConst.DELETE_MAIL_TX);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$QUERY_RETURN_MAIL":
				sValue = GetQueryReturnMail(pTag);
				break;
			case "$QUERY_VIEW_MAIL":
				sValue = GetQueryViewMail(pTag);
				break;
			case "$QUERY_CAST_MOVIE_DOWNLOAD_CHECK":
				sValue = this.GetQueryMailMovieDownloadCheck(pTag,pArgument);
				break;
			case "$QUERY_BLOG_ARTICLE":
				sValue = GetQueryBlogArticle(pTag,pArgument);
				break;
			//ｸｲｯｸﾚｽ違反用ﾌｫｰﾑを表示するしないﾌﾗｸﾞ　完全にPWILD仕様 
			case "$DISP_QUICK_RES_TX_VIOLATION_FORM":
				sValue = GetDispQuickResTxViolationForm(pTag);
				break;
			case "$MAIL_HAS_PIC_FLAG":
				sValue = GetMailHasPicFlag(pTag);
				break;
			case "$MAIL_HAS_MOVIE_FLAG":
				sValue = GetMailHasMovieFlag(pTag);
				break;
			case "$CAST_FEATURE_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.DOCOMO,ViCommConst.KDDI,ViCommConst.SOFTBANK);
				break;
			case "$CAST_SMART_PHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID,ViCommConst.IPHONE);
				break;
			case "$CAST_ANDROID_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.ANDROID);
				break;
			case "$CAST_IPHONE_FLAG":
				sValue = GetContainsOfCarrier(pTag,ViCommConst.IPHONE);
				break;
			case "$SELF_FANCLUB_MEMBER_RANK":
				string[] sValues;
				if (GetValues(pTag,"PARTNER_USER_SEQ,PARTNER_USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubMemberRank(sValues[0],sValues[1]);
				}
				break;
			case "$SELF_FANCLUB_MEMBER_RANK_WITHOUT_SP":
				if (GetValues(pTag,"PARTNER_USER_SEQ,PARTNER_USER_CHAR_NO",out sValues)) {
					sValue = GetSelfFanClubMemberRankWithoutSpPremium(sValues[0],sValues[1]);
				}
				break;
			case "$LIST_RNUM":
				SetFieldValue(pTag,"RNUM",out sValue);
				break;
			case "$DRAFT_MAIL_SEQ":
				SetFieldValue(pTag,"DRAFT_MAIL_SEQ",out sValue);
				break;
			case "$DRAFT_MAIL_DOC":
				sValue = GetDraftMailDoc(pTag);
				break;
			case "$LAST_UPDATE_DATE":
				SetFieldValue(pTag,"LAST_UPDATE_DATE",out sValue);
				break;
			case "$TX_USER_SEQ":
				SetFieldValue(pTag,"TX_USER_SEQ",out sValue);
				break;
			case "$TX_USER_CHAR_NO":
				SetFieldValue(pTag,"TX_USER_CHAR_NO",out sValue);
				break;
			case "$QUICK_RES_MAIL_FLAG":
				SetFieldValue(pTag,"QUICK_RES_MAIL_FLAG",out sValue);
				break;
			case "$CAST_TO_SELF_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_RX_MAIL_COUNT",out sValue);	// 男性基準のためrxを設定 
				break;
			case "$SELF_TO_CAST_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_TX_MAIL_COUNT",out sValue);	// 男性基準のためtxを設定 
				break;
			case "$MAIL_SEQ":
				SetFieldValue(pTag,"MAIL_SEQ",out sValue);
				break;
			case "$PRESENT_MAIL_ITEM_SEQ":
				SetFieldValue(pTag,"PRESENT_MAIL_ITEM_SEQ",out sValue);
				break;
			case "$DIV_IS_PRESENT_MAIL":
				if (GetValue(pTag,"PRESENT_MAIL_ITEM_SEQ",out sPresentMailItemSeq)) {
					SetNoneDisplay(string.IsNullOrEmpty(sPresentMailItemSeq));
				}
				break;
			case "$DIV_IS_NOT_PRESENT_MAIL":
				if (GetValue(pTag,"PRESENT_MAIL_ITEM_SEQ",out sPresentMailItemSeq)) {
					SetNoneDisplay(!string.IsNullOrEmpty(sPresentMailItemSeq));
				}
				break;
			case "$DIV_IS_PRESENT_RETURN_MAIL":
				if (GetValue(pTag,"PRESENT_RETURN_FLAG",out sPresentReturnFlag)) {
					SetNoneDisplay(!sPresentReturnFlag.Equals(ViCommConst.FLAG_ON_STR));
				}
				break;
			case "$DIV_IS_NOT_PRESENT_RETURN_MAIL":
				if (GetValue(pTag,"PRESENT_RETURN_FLAG",out sPresentReturnFlag)) {
					SetNoneDisplay(sPresentReturnFlag.Equals(ViCommConst.FLAG_ON_STR));
				}
				break;
			case "$CHAT_MAIL_PARTS":
				sValue = this.GetChatMailParts(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryBlogArticle(string pTag,string pArgument) {
		string[] oValues = null;
		string sBlogSeq = string.Empty;

		if (GetValues(pTag,"SITE_CD,USER_SEQ,USER_CHAR_NO,OBJ_SEQ",out oValues)) {
			BlogSeekCondition oCondition = new BlogSeekCondition();
			oCondition.SiteCd = oValues[0];
			oCondition.UserSeq = oValues[1];
			oCondition.UserCharNo = oValues[2];

			using (Blog oBlog = new Blog(sessionObjs)) {
				using (DataSet oBlogDataSet = oBlog.GetPageCollection(oCondition,1,1)) {
					if (oBlogDataSet.Tables[0].Rows.Count > 0) {
						sBlogSeq = iBridUtil.GetStringValue(oBlogDataSet.Tables[0].Rows[0]["BLOG_SEQ"]);
					}
				}

			}

			return string.Format("blog_seq={0}&blog_article_seq={1}",sBlogSeq,oValues[3]);
		} else {
			return string.Empty;
		}
	}

	private string GetHrefMailMovieDownload(string pTag,string pLabel) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,TX_USER_SEQ,TX_USER_CHAR_NO,PARTNER_LOGIN_ID,MOVIE_SEQ,CRYPT_VALUE",out sValues) == false) {
			return string.Join("",sValues);
		}
		return ParseDownloadUrl(sValues[0],sValues[1],sValues[2],sValues[3],sValues[4],pLabel,string.Format("{0}={1}",sessionMan.site.charNoItemNm,sValues[5]),false);
	}


	private string GetHrefSelfMovieDownload(string pTag,string pArgument) {
		string[] sValues;
		if (GetValues(pTag,"MOVIE_SEQ",out sValues)) {
			return ParseManDownloadUrl(
					"1",
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sessionMan.userMan.loginId,
					sValues[0],
					pArgument
					);
		} else {
			return "";
		}
	}

	private string GetMailDoc(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "",sDoc5 = "";
		if (GetValue(pTag,"MAIL_DOC1",out sDoc1) && GetValue(pTag,"MAIL_DOC2",out sDoc2) && GetValue(pTag,"MAIL_DOC3",out sDoc3) && GetValue(pTag,"MAIL_DOC4",out sDoc4) && GetValue(pTag,"MAIL_DOC5",out sDoc5)) {
			sValue = parseContainer.Parse(sDoc1 + sDoc2 + sDoc3 + sDoc4 + sDoc5);
		}
		if (string.IsNullOrEmpty(sValue)) {
			string sRequestTxMailSeq;
			if (GetValue(pTag,"REQUEST_TX_MAIL_SEQ",out sRequestTxMailSeq)) {
				using (MailLog oMailLog = new MailLog()) {
					sValue = oMailLog.GetOriginalMailDoc(sRequestTxMailSeq);
				}
			}
		}

		string sMailType = string.Empty;
		string sTextMailFlag = string.Empty;
		if (GetValue(pTag,"MAIL_TYPE",out sMailType) && GetValue(pTag,"TEXT_MAIL_FLAG",out sTextMailFlag)) {
			if (!sTextMailFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				return sValue;
			}
		}

		return Regex.Replace(sValue,"\r\n","<br />");
	}

	private string GetMailMovieTitle(string pTag) {
		string sValue = "";
		if (GetValue(pTag,"MOVIE_SEQ",out sValue)) {
			using (CastMovie oCastMovie = new CastMovie()) {
				sValue = parseContainer.Parse(oCastMovie.GetMovieTitleByMovieSeq(sValue,sessionMan.site.siteCd));
			}
		}
		return sValue;
	}

	private string GetQueryViewMail(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM,CRYPT_VALUE,MAIL_SEQ",out sValues)) {
			return string.Format("pageno={0}&loginid={1}&{2}={3}&mailseq={4}&seekmode={5}",
									sValues[0],iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["loginid"]),sessionMan.site.charNoItemNm,sValues[1],sValues[2],sessionMan.seekMode);
		} else {
			return "";
		}
	}

	private string GetQueryReturnMail(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"MAIL_SEQ",out sValues)) {
			return string.Format("direct=1&mailseq={0}",sValues[0]);
		} else {
			return "";
		}
	}

	private string GetCheckBoxTag(string pTag,string pFieldNm) {
		string[] sValues;
		if (GetValues(pTag,pFieldNm,out sValues)) {
			return string.Format("<input type=\"checkbox\" name=\"checkbox{0}\" value=\"{1}\" />選択",sValues[0],sValues[0]);
		} else {
			return "";
		}
	}

	private string CastStatusIsNormal(string pTag) {
		string sValue = "";
		if (GetValue(pTag,"PARTNER_USER_STATUS",out sValue)) {
			if (sValue.Equals(ViCommConst.USER_WOMAN_NORMAL)) {
				sValue = ViCommConst.FLAG_ON_STR;
			} else {
				sValue = ViCommConst.FLAG_OFF_STR;
			}
		}
		return sValue;
	}
	private string GetQueryMailMovieDownloadCheck(string pTag,string pArgument) {
		string[] oValues;

		if (!GetValues(pTag,"TX_USER_SEQ,MOVIE_SEQ",out oValues)) {
			return string.Empty;
		}

		return string.Format("{0}&castseq={1}&movieseq={2}&check=1",GetReQuery(pTag,pArgument),oValues[0],oValues[1]);
	}

	private string GetDispQuickResTxViolationForm(string pTag) {
		//ﾒｰﾙ送信後、10分～3時間の間　かつ　ﾒｰﾙが返信されていない場合に1を返す。

		string[] sValues;
		string sDisp = ViCommConst.FLAG_OFF_STR;

		if (GetValues(pTag,"RETURN_MAIL_FLAG,MAIL_SEQ,CREATE_DATE",out sValues)) {
			if (sValues[0].Equals(ViCommConst.FLAG_OFF_STR)) {
				DateTime dtCreateDate;
				dtCreateDate = DateTime.Parse(sValues[2]);
				if (dtCreateDate.AddMinutes(10) < DateTime.Now && DateTime.Now < dtCreateDate.AddHours(3)) {
					using (MailBox oMailBox = new MailBox()) {
						if (oMailBox.IsQuickResMail(sValues[1])) {
							sDisp = ViCommConst.FLAG_ON_STR;
						}
					}
				}
			}
		}

		return sDisp;
	}

	private string GetMailHasPicFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ATTACHED_OBJ_TYPE,PIC_SEQ",out sValues)) {
			if (ViCommConst.ATTACH_PIC_INDEX.ToString().Equals(sValues[0]) && !string.IsNullOrEmpty(sValues[1])) {
				return ViCommConst.FLAG_ON_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}

	private string GetMailHasMovieFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ATTACHED_OBJ_TYPE,MOVIE_SEQ",out sValues)) {
			if (ViCommConst.ATTACH_MOVIE_INDEX.ToString().Equals(sValues[0]) && !string.IsNullOrEmpty(sValues[1])) {
				return ViCommConst.FLAG_ON_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}

	private string GetDraftMailDoc(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "",sDoc5 = "";
		if (GetValue(pTag,"MAIL_DOC1",out sDoc1) && GetValue(pTag,"MAIL_DOC2",out sDoc2) && GetValue(pTag,"MAIL_DOC3",out sDoc3) && GetValue(pTag,"MAIL_DOC4",out sDoc4) && GetValue(pTag,"MAIL_DOC5",out sDoc5)) {
			sValue = parseContainer.Parse(sDoc1 + sDoc2 + sDoc3 + sDoc4 + sDoc5);
		}

		return Regex.Replace(sValue,"\r\n","<br />");
	}

	private string GetChatMailParts(string pTag,string pArgument) {
		string sReturn = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"TX_SITE_CD,TX_USER_SEQ,TX_USER_CHAR_NO",out sValues)) {
			DataSet oDataSet = new DataSet();
			ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this,pTag,pArgument);

			MailLogSeekCondition oCondition = new MailLogSeekCondition();
			oCondition.CastSiteCd = sValues[0];
			oCondition.CastUserSeq = sValues[1];
			oCondition.CastCharNo = sValues[2];
			oCondition.ManUserSiteCd = sessionMan.site.siteCd;
			oCondition.ManUserSeq = sessionMan.userMan.userSeq;
			oCondition.ManUserCharNo = sessionMan.userMan.userCharNo;
			
			int iDispCount;
			if (!int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["dispcount"]),out iDispCount)) {
				iDispCount = oPartsArgs.NeedCount;
			}
			
			int iStartIndex = 0;
			int iListNo;
			if (int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["listno"]),out iListNo)) {
				iStartIndex = (iListNo - 1) * iDispCount;
			}

			using (MailLog oMailLog = new MailLog()) {
				oDataSet = oMailLog.GetManChatMail(oCondition,iStartIndex,oPartsArgs.NeedCount);
			}

			if (!sessionMan.adminFlg) {
				List<string> oMailSeqList = new List<string>();

				foreach (DataRow dr in oDataSet.Tables[0].Rows) {
					if (iBridUtil.GetStringValue(dr["TXRX_TYPE"]).Equals(ViCommConst.RX) && iBridUtil.GetStringValue(dr["READ_FLAG"]).Equals(ViCommConst.FLAG_OFF_STR)) {
						oMailSeqList.Add(iBridUtil.GetStringValue(dr["MAIL_SEQ"]));
					}
				}

				if (oMailSeqList.Count > 0) {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailReadFlagBatch(oMailSeqList.ToArray());
					}
				}
			}

			sReturn = parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
		}

		return sReturn;
	}

	private string GetChatMailPartsByCast(string pTag,string pArgument) {
		string sReturn = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"SITE_CD,USER_SEQ,USER_CHAR_NO",out sValues)) {
			DataSet oDataSet = new DataSet();
			ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this,pTag,pArgument);

			MailLogSeekCondition oCondition = new MailLogSeekCondition();
			oCondition.CastSiteCd = sValues[0];
			oCondition.CastUserSeq = sValues[1];
			oCondition.CastCharNo = sValues[2];
			oCondition.ManUserSiteCd = sessionMan.site.siteCd;
			oCondition.ManUserSeq = sessionMan.userMan.userSeq;
			oCondition.ManUserCharNo = sessionMan.userMan.userCharNo;

			int iDispCount;
			if (!int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["dispcount"]),out iDispCount)) {
				iDispCount = oPartsArgs.NeedCount;
			}

			int iStartIndex = 0;
			int iListNo;
			if (int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["listno"]),out iListNo)) {
				iStartIndex = (iListNo - 1) * iDispCount;
			}

			using (MailLog oMailLog = new MailLog()) {
				oDataSet = oMailLog.GetManChatMail(oCondition,iStartIndex,oPartsArgs.NeedCount);
			}

			if (!sessionMan.adminFlg) {
				List<string> oMailSeqList = new List<string>();

				foreach (DataRow dr in oDataSet.Tables[0].Rows) {
					if (iBridUtil.GetStringValue(dr["TXRX_TYPE"]).Equals(ViCommConst.RX) && iBridUtil.GetStringValue(dr["READ_FLAG"]).Equals(ViCommConst.FLAG_OFF_STR)) {
						oMailSeqList.Add(iBridUtil.GetStringValue(dr["MAIL_SEQ"]));
					}
				}

				if (oMailSeqList.Count > 0) {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailReadFlagBatch(oMailSeqList.ToArray());
					}
				}
			}

			sReturn = parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
		}

		return sReturn;
	}
}
