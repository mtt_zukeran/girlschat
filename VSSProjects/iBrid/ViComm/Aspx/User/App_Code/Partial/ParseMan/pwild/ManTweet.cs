﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやき検索結果
--	Progaram ID		: ManTweet
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

partial class PwParseMan {
	private string ParseManTweet(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$USER_SEQ":
				this.parseMan.SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$USER_CHAR_NO":
				this.parseMan.SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$MAN_TWEET_SEQ":
				this.parseMan.SetFieldValue(pTag,"MAN_TWEET_SEQ",out sValue);
				break;
			case "$MAN_HANDLE_NM":
				this.parseMan.SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$MAN_AGE":
				this.parseMan.SetFieldValue(pTag,"AGE",out sValue);
				break;
			case "$TWEET_TEXT":
				this.parseMan.SetFieldValue(pTag,"TWEET_TEXT",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$TWEET_DATE":
				this.parseMan.SetFieldValue(pTag,"TWEET_DATE",out sValue);
				break;
			case "$LIKE_COUNT":
				this.parseMan.SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$COMMENT_COUNT":
				this.parseMan.SetFieldValue(pTag,"COMMENT_COUNT",out sValue);
				break;
			case "$MAN_TWEET_PIC_SEQ":
				this.parseMan.SetFieldValue(pTag,"MAN_TWEET_PIC_SEQ",out sValue);
				break;
			case "$MAN_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_TWEET_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"MAN_TWEET_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_TWEET_SMALL_IMG_PATH":
				this.parseMan.SetFieldValue(pTag,"MAN_TWEET_SMALL_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$MAN_TWEET_PIC_UPLOAD_ADDR":
				sValue = this.GetManTweetPicUploadAddr(pTag,pArgument);
				break;
			case "$MAN_TWEET_UNAUTH_PIC_FLAG":
				this.parseMan.SetFieldValue(pTag,"MAN_TWEET_UNAUTH_PIC_FLAG",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetManTweetPicUploadAddr(string pTag,string pArgument) {
		string sAddr = string.Empty;
		string[] sValues;
		if (parseMan.GetValues(pTag,"LOGIN_ID,USER_CHAR_NO,MAN_TWEET_SEQ",out sValues)) {
			sAddr = string.Format("{0}{1}{2}{3}_{4}@{5}",
				ViCommConst.TWEET_URL_HEADER2,
				parseMan.sessionMan.site.siteCd,
				sValues[0],
				sValues[1],
				sValues[2],
				parseMan.sessionMan.site.mailHost
			);
		}

		return sAddr;
	}

	private string GetManTweet(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		ManTweetExSeekCondition oCondition = new ManTweetExSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.AppendLikeCount = true;
		oCondition.AppendCommentCount = true;

		using (ManTweetEx oManTweetEx = new ManTweetEx()) {
			oTmpDataSet = oManTweetEx.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		SeekConditionBase oPrevSeekConditionBase = this.parseMan.sessionObjs.seekConditionEx;
		try {
			this.parseMan.sessionObjs.seekConditionEx = oCondition;
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViComm.Extension.Pwild.PwViCommConst.DATASET_MAN_TWEET);
		} finally {
			this.parseMan.sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}

		return sValue;
	}
}