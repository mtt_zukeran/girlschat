﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseCastDiary(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_USER_SEQ":
				SetFieldValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$CAST_USER_CHAR_NO":
				SetFieldValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_DIARY_DAY":
				SetFieldValue(pTag,"REPORT_DAY",out sValue);
				break;
			case "$CAST_DIARY_WRITE_DATE":
				SetFieldValue(pTag,"DIARY_CREATE_DATE",out sValue);
				break;
			case "$CAST_DIARY_DOC":
				sValue = GetDiaryHtmlDocFull(pTag);
				break;
			case "$CAST_DIARY_FLAG":
				sValue = this.GetDiaryFlag(pTag);
				break;
			case "$CAST_DIARY_IMG_PATH":
				SetFieldValue(pTag,"OBJ_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_SMALL_DIARY_IMG_PATH":
				SetFieldValue(pTag,"OBJ_SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_DIARY_HEADER_TITLE":
				SetFieldValue(pTag,"DIARY_HEADER_TITLE",out sValue);
				break;
			case "$CAST_DIARY_TITLE":
				SetFieldValue(pTag,"DIARY_TITLE",out sValue);
				break;
			case "$CAST_DIARY_READ_COUNT":
				SetFieldValue(pTag,"READING_COUNT",out sValue);
				break;
			case "$CAST_DIARY_LIKE_COUNT":
				SetFieldValue(pTag,"LIKE_COUNT",out sValue);
				break;
			case "$CAST_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_IMG":
				sValue = GetCastFavoritImg(pTag,pArgument);
				break;
			case "$CAST_REFUSE_BY_SELF_FLAG":
				SetFieldValue(pTag,"REFUSE_FLAG",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_LARGE_IMG_PATH":
				SetFieldValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$CAST_RANK":
				SetFieldValue(pTag,"USER_RANK",out sValue);
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$DIV_IS_PICKUP":
			case "$CAST_IS_PICKUP":
				sValue = IsPickup(pTag, pArgument).ToString();
				break;								
			case "$HREF_MONITOR_ROOM":
				sValue = GetHrefMonitorRoom(pTag,pArgument);
				break;
			case "$HREF_MONITOR_TALK":
			case "$HREF_MONITOR_TALK2":
				sValue = GetHrefMonitorTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_VOICE":
				sValue = GetHrefMonitorVoice(pTag,pArgument);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$QUERY_VIEW_DIARY":
				sValue = GetQueryViewDiary(pTag);
				break;
			case "$QUERY_VIEW_PERSONAL_DIARY":
				sValue = GetQueryViewPersonalDiary(pTag);
				break;
			case "$DIARY_HAS_PIC_FLAG":
				sValue = GetDiaryHasPicFlag(pTag);
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				SetFieldValue(pTag,"MONITOR_ENABLE_FLAG",out sValue);
				break;
			case "$QUERY_DIARY_LIKE":
				sValue = GetQueryListCastDiaryLike(pTag);
				break;
			case "$LINK_LIST_PERSONAL_CAST_DIARY":
				sValue = GetLinkListPersonalCastDiary(pTag,pArgument);
				break;
			case "$LINK_VIEW_CAST_DIARY":
				sValue = GetLinkViewCastDiary(pTag,pArgument);
				break;
			case "$LINK_VIEW_CAST_DIARY01":
				sValue = GetLinkViewCastDiary01(pTag,pArgument);
				break;
			case "$LINK_VIEW_CAST_DIARY_LIKE":
				sValue = GetLinkViewCastDiaryLike(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryViewDiary(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ACT_CATEGORY_IDX,LOGIN_ID,RNUM,REPORT_DAY,CRYPT_VALUE,CAST_DIARY_SUB_SEQ",out sValues)) {
			// ReplaceはCrawler対応 
			return string.Format("seekmode={0}&category={1}&loginid={2}&userrecno={3}&diaryday={4}&{5}={6}&subseq={7}",
									sessionMan.seekMode,sValues[0],sValues[1],sValues[2],sValues[3].Replace("/","-"),sessionMan.site.charNoItemNm,sValues[4],sValues[5]);
		} else {
			return "";
		}
	}

	private string GetQueryViewPersonalDiary(string pTag) {
		string[] sValues;
		string sCastRNum;

		if (GetValue(pTag,ViCommConst.DATASET_CAST,"RNUM",out sCastRNum)) {
			if (GetValues(pTag,"ACT_CATEGORY_IDX,LOGIN_ID,RNUM,CRYPT_VALUE",out sValues)) {
				return string.Format("seekmode={0}&category={1}&loginid={2}&userrecno={3}&diaryrecno={4}&{5}={6}",
										sessionMan.seekMode,sValues[0],sValues[1],sCastRNum,sValues[2],sessionMan.site.charNoItemNm,sValues[3]);
			} else {
				return "";
			}
		} else {
			return sCastRNum;
		}
	}

	private string GetDiaryHtmlDocFull(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "";
		if (GetValue(pTag,"HTML_DOC1",out sDoc1) && GetValue(pTag,"HTML_DOC2",out sDoc2) && GetValue(pTag,"HTML_DOC3",out sDoc3) && GetValue(pTag,"HTML_DOC4",out sDoc4)) {
			sValue = parseContainer.Parse(Regex.Replace(sDoc1 + sDoc2 + sDoc3 + sDoc4,"\r\n","<br />"));
		}
		return sValue;
	}

	private string GetDiaryHasPicFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"ATTACHED_OBJ_TYPE,PIC_SEQ",out sValues)) {
			if (ViCommConst.DiaryObjType.PIC.Equals(sValues[0]) && !string.IsNullOrEmpty(sValues[1])) {
				return ViCommConst.FLAG_ON_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}

	private string GetDiaryFlag(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"DIARY_COUNT,DIARY_NON_PUBLISH_FLAG",out sValues)) {
			if (ViCommConst.FLAG_ON_STR.Equals(sValues[1])) {
				return ViCommConst.FLAG_OFF_STR;
			}

			return sValues[0];
		}
		return string.Empty;
	}

	private string GetQueryListCastDiaryLike(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"REPORT_DAY,CAST_DIARY_SUB_SEQ,SCREEN_ID",out sValues)) {
			return string.Format("reportday={0}&subseq={1}&scrid={2}",
				sValues[0],sValues[1],sValues[2]);
		} else {
			return "";
		}
	}

	private string GetLinkListPersonalCastDiary(string pTag,string pArgument) {
		string sUrl = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"USER_SEQ",out sValues)) {
			UrlBuilder oUrl = new UrlBuilder("ListPersonalCastDiary.aspx");
			oUrl.AddParameter("userseq",iBridUtil.GetStringValue(sValues[0]));
			sUrl = oUrl.ToString();
		}

		return sUrl;
	}

	private string GetLinkViewCastDiary(string pTag,string pArgument) {
		string sUrl = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"USER_SEQ,REPORT_DAY,CAST_DIARY_SUB_SEQ",out sValues)) {
			UrlBuilder oUrl = new UrlBuilder("ViewCastDiary.aspx");
			oUrl.AddParameter("userseq",iBridUtil.GetStringValue(sValues[0]));
			oUrl.AddParameter("reportday",HttpUtility.UrlEncode(iBridUtil.GetStringValue(sValues[1]),Encoding.GetEncoding("Shift_JIS")));
			oUrl.AddParameter("subseq",iBridUtil.GetStringValue(sValues[2]));
			sUrl = oUrl.ToString();
		}

		return sUrl;
	}

	private string GetLinkViewCastDiary01(string pTag,string pArgument) {
		string sUrl = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"USER_SEQ,REPORT_DAY,CAST_DIARY_SUB_SEQ",out sValues)) {
			UrlBuilder oUrl = new UrlBuilder("ViewCastDiary.aspx");
			oUrl.AddParameter("userseq",iBridUtil.GetStringValue(sValues[0]));
			oUrl.AddParameter("reportday",HttpUtility.UrlEncode(iBridUtil.GetStringValue(sValues[1]),Encoding.GetEncoding("Shift_JIS")));
			oUrl.AddParameter("subseq",iBridUtil.GetStringValue(sValues[2]));
			oUrl.AddParameter("scrid","01");
			sUrl = oUrl.ToString();
		}

		return sUrl;
	}

	private string GetLinkViewCastDiaryLike(string pTag,string pArgument) {
		string sUrl = string.Empty;
		string[] sValues;

		if (GetValues(pTag,"USER_SEQ,REPORT_DAY,CAST_DIARY_SUB_SEQ",out sValues)) {
			UrlBuilder oUrl = new UrlBuilder("ViewCastDiary.aspx");
			oUrl.AddParameter("userseq",iBridUtil.GetStringValue(sValues[0]));
			oUrl.AddParameter("reportday",HttpUtility.UrlEncode(iBridUtil.GetStringValue(sValues[1]),Encoding.GetEncoding("Shift_JIS")));
			oUrl.AddParameter("subseq",iBridUtil.GetStringValue(sValues[2]));
			oUrl.AddParameter("like","1");
			sUrl = oUrl.ToString();
		}

		return sUrl;
	}
}
