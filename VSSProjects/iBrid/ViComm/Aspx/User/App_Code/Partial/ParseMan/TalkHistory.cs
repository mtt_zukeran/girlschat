﻿using System;
using System.Data;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseTalkHistory(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = "";

		switch (pTag) {
			case "$CAST_AGE":
				if (!string.IsNullOrEmpty(pArgument)) {
					sValue = GetAge(pTag,pArgument);
				} else {
					SetFieldValue(pTag,"AGE",out sValue);
				}
				break;
			case "$CAST_LAST_LOGIN_MIN":
				sValue = GetLastLoginMin(pTag);
				break;
			case "$CAST_NEW_SIGN":
				SetFieldValue(pTag,"NEW_CAST_SIGN",out sValue);
				break;
			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_IMG_PATH":
				SetFieldValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED.ToString());
				break;
			case "$CAST_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag,"MOBILE_CONNECT_TYPE_NM",out sValue);
				break;
			case "$CAST_TALK_COUNT":
				sValue = GetTalkCount(pTag);
				break;
			case "$QUERY_TALK_HIS_RNUM":
				sValue = GetQueryRNum(pTag);
				break;
			case "$QUERY_TALK_SEQ":
				sValue = GetQueryTalkSeq(pTag);
				break;
			case "$QUERY_TALK_HISTORY_GROUP_DEL":
				sValue = GetQueryTalkHistoryGroupDel(pTag);
				break;
			case "$TALK_HIS_MEMO":
				SetFieldValue(pTag,"MEMO",out sValue);
				break;
			case "$TALK_HIS_START_DATE":
				SetFieldValue(pTag,"TALK_START_DATE",out sValue);
				break;
			case "$TALK_HIS_COUNT":
				SetFieldValue(pTag,"TALK_COUNT",out sValue);
				break;
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$CAST_TOAL_TALK_COUNT_BY_SELF":
				SetFieldValue(pTag,"PARTNER_TOTAL_TALK_COUNT",out sValue);
				break;
			case "$CAST_TO_SELF_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_TX_MAIL_COUNT",out sValue);	// 男性基準のためrxを設定 
				break;
			case "$SELF_TO_CAST_MAIL_COUNT":
				SetFieldValue(pTag,"PARTNER_TOTAL_RX_MAIL_COUNT",out sValue);	// 男性基準のためtxを設定 
				break;
			case "$CAST_COMMENT_ADMIN":
				SetFieldValue(pTag,"COMMENT_ADMIN",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_DETAIL":
				SetFieldValue(pTag,"COMMENT_DETAIL",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_COMMENT_LIST":
				SetFieldValue(pTag,"COMMENT_LIST",out sValue);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_WSHOT_TV_OK_FLAG":
				sValue = CanTvWShot(pTag);
				break;
			case "$CAST_WSHOT_VOICE_OK_FLAG":
				sValue = CanVoiceTalk(pTag);
				break;
			case "$CAST_CHAT_TV_OK_FLAG":
				sValue = IsEqual(pTag,IsTvTalkEnable(pTag));
				break;
			case "$CAST_MONITOR_ROOM_OK_FLAG":
				sValue = IsEqual(pTag,"MONITOR_TALK_TYPE",ViCommConst.MONITOR_ROOM);
				break;
			case "$CAST_MONITOR_TALK_OK_FLAG":
				sValue = CanMonitorTalk(pTag);
				break;
			case "$CAST_MONITOR_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,IsMonitorVoiceEnable(pTag));
				break;
			case "$TALK_END_REASON":
				sValue = GetEndReason(pTag,pArgument);
				break;
			case "$HREF_MONITOR_ROOM":
				sValue = GetHrefMonitorRoom(pTag,pArgument);
				break;
			case "$HREF_MONITOR_TALK":
			case "$HREF_MONITOR_TALK2":
				sValue = GetHrefMonitorTalk(pTag,pArgument);
				break;
			case "$HREF_MONITOR_VOICE":
				sValue = GetHrefMonitorVoice(pTag,pArgument);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag,pArgument);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$CAST_FAVORIT_BY_SELF_FLAG":
				SetFieldValue(pTag,"FAVORIT_FLAG",out sValue);
				break;
			case "$CAST_FAVORIT_ME_FLAG":
				SetFieldValue(pTag,"LIKE_ME_FLAG",out sValue);
                break;
			case "$CAST_FAVORIT_IMG":
				sValue = GetCastFavoritImg(pTag,pArgument);
				break;
            case "$CAST_MAIL_COUNT_NOW":
                sValue = GetMailCountNow(pTag);
                break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	private string GetQueryRNum(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"RNUM",out sValues)) {
			return string.Format("pageno={0}",sValues[0]);
		} else {
			return "";
		}
	}

	private string GetQueryTalkSeq(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"TALK_SEQ",out sValues)) {
			return string.Format("talkseq={0}",sValues[0]);
		} else {
			return "";
		}
	}

	private string GetQueryTalkHistoryGroupDel(string pTag) {
		string[] sValues;
		if (GetValues(pTag,"TALK_SEQ",out sValues)) {
			return string.Format("talkseq={0}&all=1",sValues[0]);
		} else {
			return "";
		}
	}

	private string GetEndReason(string pTag,string pArgument) {
		string[] sValues;
		if (GetValues(pTag,"CALL_RESULT,CHARGE_TYPE",out sValues)) {
			string sCallResult = sValues[0];
			string sChargeType = sValues[1];

			if (sCallResult.Equals(ViCommConst.CALL_RESULT_POINT_DISC)) {
				return ViCommConst.CALL_RESULT_EASY_POINT_DISCONN;
			} else if (sCallResult.Equals(ViCommConst.CALL_RESULT_REQUESTER_DISCONN)) {
				if (sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_PUBLIC) ||
					sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_WSHOT) ||
					sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC) ||
					sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT)) {
					return ViCommConst.CALL_RESULT_EASY_CAST_DISCONN;
				} else {
					return ViCommConst.CALL_RESULT_EASY_MAN_DISCONN;
				}
			} else if (sCallResult.Equals(ViCommConst.CALL_RESULT_PARTNER_DISCONN)) {
				if (sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_PUBLIC) ||
						sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_WSHOT) ||
						sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC) ||
						sChargeType.Equals(ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT)) {
					return ViCommConst.CALL_RESULT_EASY_MAN_DISCONN;
				} else {
					return ViCommConst.CALL_RESULT_EASY_CAST_DISCONN;
				}
			} else if (sCallResult.Equals(ViCommConst.CALL_RESULT_FAIL_SYS_ERROR)) {
				return ViCommConst.CALL_RESULT_EASY_SYS_DISCONN;
			} else {
				return string.Empty;
			}
		} else {
			return string.Empty;
		}
	}
}
