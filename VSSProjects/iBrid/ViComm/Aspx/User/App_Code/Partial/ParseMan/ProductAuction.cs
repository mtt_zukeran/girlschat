﻿using System;
using System.Data;
using System.Text;
using iBridCommLib;
using ViComm;

partial class ParseMan {
	private string ParseProductAuction(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		switch (pTag) {
			#region □ 商品動画種別	□ --------------------------
			case "$AUCTION_STATUS_NORMAL":
				sValue = ViCommConst.AuctionStatus.Normal;
				break;
			case "$AUCTION_STATUS_WINBID":
				sValue = ViCommConst.AuctionStatus.WinBid;
				break;
			case "$AUCTION_STATUS_DECLINATION_BID":
				sValue = ViCommConst.AuctionStatus.DeclinationBid;
				break;
			case "$AUCTION_STATUS_NONBID_END":
				sValue = ViCommConst.AuctionStatus.NonbidEnd;
				break;
			case "$AUCTION_STATUS_END":
				sValue = ViCommConst.AuctionStatus.End;
				break;
			#endregion --------------------------------------	
			case "$PRODUCT_AUCTION_START_DATE":
				SetFieldValue(pTag, "AUCTION_START_DATE", out sValue);
				break;
			case "$PRODUCT_BLIND_END_DATE":
				SetFieldValue(pTag, "BLIND_END_DATE", out sValue);
				break;
			case "$PRODUCT_AUCTION_END_DATE":
				SetFieldValue(pTag, "AUCTION_END_DATE", out sValue);
				break;
			case "$PRODUCT_RESERVE_AMT":
				SetFieldValue(pTag, "RESERVE_AMT", out sValue);
				break;
			case "$PRODUCT_MINIMUM_BID_AMT":
				SetFieldValue(pTag, "MINIMUM_BID_AMT", out sValue);
				break;
			case "$PRODUCT_ENTRY_POINT":
				SetFieldValue(pTag, "ENTRY_POINT", out sValue);
				break;
			case "$PRODUCT_BID_POINT":
				SetFieldValue(pTag, "BID_POINT", out sValue);
				break;
			case "$PRODUCT_BID_COUNT":
				SetFieldValue(pTag, "BID_COUNT", out sValue);
				break;
			case "$PRODUCT_LAST_BID_DATE":
				SetFieldValue(pTag, "LAST_BID_DATE", out sValue);
				break;
			case "$PRODUCT_AUCTION_STATUS":
				SetFieldValue(pTag, "AUCTION_STATUS", out sValue);
				break;
			case "$PRODUCT_BLIND_ENTRY_POINT":
				SetFieldValue(pTag, "BLIND_ENTRY_POINT", out sValue);
				break;
			case "$PRODUCT_BLIND_BID_POINT":
				SetFieldValue(pTag, "BLIND_BID_POINT", out sValue);
				break;
			case "$PRODUCT_MAX_BID_AMT":
				SetFieldValue(pTag, "MAX_BID_AMT", out sValue);
				break;
			case "$PRODUCT_MAX_BIDDER_HANDLE_NM":
				SetFieldValue(pTag, "MAX_BIDDER_HANDLE_NM", out sValue);
				break;
			case "$PRODUCT_AUTO_EXTENSION_FLAG":
				SetFieldValue(pTag, "AUTO_EXTENSION_FLAG", out sValue);
				break;
			case "$DIV_IS_MAX_BIDDER":
				SetNoneDisplay(!this.GetIsMaxBidder(pTag));
				break;
			case "$DIV_IS_NOT_MAX_BIDDER":
				SetNoneDisplay(this.GetIsMaxBidder(pTag));
				break;
			case "$DIV_IS_ENTERED_AUCTION":
				SetNoneDisplay(!this.GetIsEnteredAuction(pTag));
				break;
			case "$DIV_IS_NOT_ENTERED_AUCTION":
				SetNoneDisplay(this.GetIsEnteredAuction(pTag));
				break;
				
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}
	
	private bool GetIsMaxBidder(string pTag){
		string[] sValues = null;
		if (!GetValues(pTag, "MAX_BIDDER_SITE_CD,MAX_BIDDER_USER_SEQ,MAX_BIDDER_USER_CHAR_NO", out sValues)) {
			return false;
		}
		string sMaxBidderSiteCd = sValues[0];
		string sMaxBidderUserSeq = sValues[1];
		string sMaxBidderUserCharNo = sValues[2];
		
		return (sessionObjs.site.siteCd.Equals(sMaxBidderSiteCd) && sessionMan.userMan.userSeq.Equals(sMaxBidderUserSeq) && sessionMan.userMan.userCharNo.Equals(sMaxBidderUserCharNo));	
	}
	private bool GetIsEnteredAuction(string pTag) {
		string[] sValues = null;
		if (!GetValues(pTag, "AUCTION_ENTRY_FLAG", out sValues)) {
			return false;
		}
		string sAuctionEntryFlag = sValues[0];

		return sAuctionEntryFlag.Equals(ViCommConst.FLAG_ON_STR);
	}

}