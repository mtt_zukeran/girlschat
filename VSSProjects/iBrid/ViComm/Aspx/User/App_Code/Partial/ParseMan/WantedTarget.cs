﻿using System;
using System.Text;
using System.Data;
using iBridCommLib;
using ViComm;
using System.Collections.Specialized;
using System.Globalization;

partial class ParseMan {
	private string ParseWantedTarget(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$WANTED_TARGET_SMALL_IMG_PATH":
				GetValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$WANTED_TARGET_LARGE_IMG_PATH":
				GetValue(pTag,"PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
				
			case "$QUERY_CAST_PROFILE":
				sValue = GetQueryCastProfile(pTag);
				break;
			case "$CAST_HANDLE_NM":
				GetValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_USER_SEQ":
				GetValue(pTag,"USER_SEQ",out sValue);
				break;
			case "$CAST_CHAR_NO":
				GetValue(pTag,"USER_CHAR_NO",out sValue);
				break;
			case "$INVESTIGATE_TARGET_SEQ":
				GetValue(pTag,"INVESTIGATE_TARGET_SEQ",out sValue);
				break;
			case "$EXECUTION_DAY":
				GetValue(pTag,"EXECUTION_DAY",out sValue);
				break;
			case "$DIV_IS_ENABLED_INVESTIGATE_HINT1":
				SetNoneDisplay(!IsEnabledInvestigateHint1(pTag));
				break;
			case "$DIV_IS_NOT_ENABLED_INVESTIGATE_HINT1":
				SetNoneDisplay(IsEnabledInvestigateHint1(pTag));
				break;
			case "$DIV_IS_ENABLED_INVESTIGATE_HINT2":
				SetNoneDisplay(!IsEnabledInvestigateHint2(pTag));
				break;
			case "$DIV_IS_NOT_ENABLED_INVESTIGATE_HINT2":
				SetNoneDisplay(IsEnabledInvestigateHint2(pTag));
				break;
			case "$DIV_IS_ENABLED_INVESTIGATE_HINT3":
				SetNoneDisplay(!IsEnabledInvestigateHint3(pTag));
				break;
			case "$DIV_IS_NOT_ENABLED_INVESTIGATE_HINT3":
				SetNoneDisplay(IsEnabledInvestigateHint3(pTag));
				break;
			case "$INVESTIGATE_HINT_TYPE1":
				GetValue(pTag,"INVESTIGATE_HINT_TYPE1",out sValue);
				break;
				
			case "$INVESTIGATE_HINT_TYPE2":
				GetValue(pTag,"INVESTIGATE_HINT_TYPE2",out sValue);
				break;
			case "$INVESTIGATE_HINT_TYPE3":
				GetValue(pTag,"INVESTIGATE_HINT_TYPE3",out sValue);
				break;
			case "$INVESTIGATE_HINT_DISPLAY_VALUE1":
				GetValue(pTag,"INVESTIGATE_HINT_DISPLAY_VALUE1",out sValue);
				break;
			case "$INVESTIGATE_HINT_DISPLAY_VALUE2":
				GetValue(pTag,"INVESTIGATE_HINT_DISPLAY_VALUE2",out sValue);
				break;
			case "$INVESTIGATE_HINT_DISPLAY_VALUE3":
				GetValue(pTag,"INVESTIGATE_HINT_DISPLAY_VALUE3",out sValue);
				break;
			default:
				pParsed = false;
				break;
		}

		return sValue;
	}

	private string GetListInvestigateTarget(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this,pTag,pArgument);

		InvestigateTargetSeekCondition oCondition = new InvestigateTargetSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = sessionMan.site.siteCd;

		if (string.IsNullOrEmpty(oCondition.ExecutionDay)) {
			oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
		}
		
		using (InvestigateTarget oInvestigateTarget = new InvestigateTarget()) {
			oDataSet = oInvestigateTarget.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_EX_WANTED_TARGET);
	}

	private bool IsEnabledInvestigateHint1(string pTag) {
		string sTime;
		GetValue(pTag,"FIRST_HINT_ANNOUNCE_TIME",out sTime);
		return CheckEnabledInvestigateHint(pTag,sTime);
	}
	private bool IsEnabledInvestigateHint2(string pTag) {
		string sTime;
		GetValue(pTag,"SECOND_HINT_ANNOUNCE_TIME",out sTime);
		return CheckEnabledInvestigateHint(pTag,sTime);
	}
	private bool IsEnabledInvestigateHint3(string pTag) {
		string sTime;
		GetValue(pTag,"THIRD_HINT_ANNOUNCE_TIME",out sTime);
		return CheckEnabledInvestigateHint(pTag,sTime);
	}

	private bool CheckEnabledInvestigateHint(string pTag,string pHintStartTime) {
		bool bValue = false;
		DateTime dtNow = DateTime.Now;

		if (!string.IsNullOrEmpty(pHintStartTime)) {
			long lTaregtTime = DateTime.Parse(string.Format("{0} {1}",dtNow.ToString("yyyy/MM/dd"),"00:00:00")).ToFileTime();
			long lDiffStartTime = DateTime.Parse(pHintStartTime).ToFileTime() - DateTime.Parse("00:00:00").ToFileTime() - 1;
			bValue = (dtNow.ToFileTime() < lTaregtTime || (lTaregtTime + lDiffStartTime) < dtNow.ToFileTime());
		}
		return bValue;

	}
}
