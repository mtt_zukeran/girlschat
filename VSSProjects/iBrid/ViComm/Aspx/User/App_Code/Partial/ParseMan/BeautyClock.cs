﻿using System;
using System.Text;
using System.Data;
using iBridCommLib;
using ViComm;
using System.Collections.Specialized;

partial class ParseMan {
	private string ParseBeautyClock(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$BEAUTY_CLOCK_ASSIGNED_TIME":
				sValue = GetAssignedTime(pTag);
				break;
			case "$BEAUTY_CLOCK_ASSIGNED_NOW_TIME":
				sValue = ViCommPrograms.GetAssignedNowTime();
				break;
				
			case "$BEAUTY_CLOCK_TIME":
				sValue = GetAssignedTime(pTag);
				sValue = ViCommPrograms.ConvertAssignedToTime(sValue);
				break;
			case "$BEAUTY_CLOCK_NEXT_TIME":
				sValue = GetAssignedTime(pTag,1);
				sValue = ViCommPrograms.ConvertAssignedToTime(sValue);
				break;
			case "$BEAUTY_CLOCK_PREV_TIME":
				sValue = GetAssignedTime(pTag,-1);
				sValue = ViCommPrograms.ConvertAssignedToTime(sValue);
				break;

			case "$DIV_IS_ENABLE_TIME": {
					string sTime = pArgument;
					if (sTime.Contains(":")) {
						sTime = ViCommPrograms.ConvertTimeToAssigned(sTime);
					}
					SetNoneDisplay(!this.IsEnableTime(sTime));
					break;
				}
			case "$DIV_IS_NOT_ENABLE_TIME": {
					string sTime = pArgument;
					if (sTime.Contains(":")) {
						sTime = ViCommPrograms.ConvertTimeToAssigned(sTime);
					}
					SetNoneDisplay(this.IsEnableTime(sTime));
					break;
				}

			case "$DIV_IS_MIN_TIME": {
					string sTime = GetAssignedTime(pTag);
					SetNoneDisplay(!(sTime.Equals(ViCommConst.BeautyClock.MIN_ASSIGNED_TIME.ToString())));
					break;
				}
			case "$DIV_IS_NOT_MIN_TIME": {
					string sTime = GetAssignedTime(pTag);
					SetNoneDisplay((sTime.Equals(ViCommConst.BeautyClock.MIN_ASSIGNED_TIME.ToString())));
					break;
				}

			case "$DIV_IS_MAX_TIME": {
					string sTime = GetAssignedTime(pTag);
					SetNoneDisplay(!(sTime.Equals(ViCommConst.BeautyClock.MAX_ASSIGNED_TIME.ToString())));
					break;
				}
			case "$DIV_IS_NOT_MAX_TIME": {
					string sTime = GetAssignedTime(pTag);
					SetNoneDisplay((sTime.Equals(ViCommConst.BeautyClock.MAX_ASSIGNED_TIME.ToString())));
					break;
				}
				
			case "$BEAUTY_CLOCK_IMG_PATH":			
				sValue = GetBeautyClockImgPath(pTag,pArgument);
				break;
			case "$BEAUTY_CLOCK_LARGE_IMG_PATH":
				sValue = GetBeautyClockImgPath(pTag,"1");
				break;
			case "$BEAUTY_CLOCK_SMALL_IMG_PATH":
				sValue = GetBeautyClockImgPath(pTag,"2");
				break;

			case "$QUERY_VIEW_BEAUTY_CLOCK":
				sValue = GetQueryViewBeautyClock(pTag,pArgument);
				break;
			case "$QUERY_NEXT_BEAUTY_CLOCK":
				sValue = GetQueryViewBeautyClock(pTag,pArgument,1);
				break;
			case "$QUERY_PREV_BEAUTY_CLOCK":
				sValue = GetQueryViewBeautyClock(pTag,pArgument,-1);
				break;

			case "$CAST_HANDLE_NM":
				SetFieldValue(pTag,"HANDLE_NM",out sValue);
				break;
			case "$CAST_LOGIN_ID":
				SetFieldValue(pTag,"LOGIN_ID",out sValue);
				break;
			case "$CAST_ITEM":
				sValue = GetCastItem(pTag,pArgument);
				sValue = ReplaceBr(sValue);
				break;
			case "$CAST_ONLINE_STATUS":
				SetFieldValue(pTag,"MOBILE_ONLINE_STATUS_NM",out sValue);
				break;
			case "$CAST_SMALL_PHOTO_IMG_PATH":
				GetValue(pTag,"SMALL_PHOTO_IMG_PATH",out sValue);
				sValue = "../" + sValue;
				break;
			case "$CAST_AGE":
				GetValue(pTag,"AGE",out sValue);
				break;
			case "$BLOG_SEQ":
				GetValue(pTag,"BLOG_SEQ",out sValue);
				break;
			case "$CAST_STATUS_LOGINED_FLAG":
				sValue = IsEqual(pTag, "CHARACTER_ONLINE_STATUS", ViCommConst.USER_LOGINED.ToString());
				break;
			case "$CAST_STATUS_OFFLINE_FLAG":
				sValue = IsEqual(pTag, "CHARACTER_ONLINE_STATUS", ViCommConst.USER_OFFLINE.ToString());
				break;
			case "$CAST_STATUS_TALKING_FLAG":
				sValue = IsEqual(pTag, "CHARACTER_ONLINE_STATUS", ViCommConst.USER_TALKING.ToString());
				break;
			case "$CAST_STATUS_WAITING_FLAG":
				sValue = IsEqual(pTag,"CHARACTER_ONLINE_STATUS",ViCommConst.USER_WAITING.ToString());
				break;
			case "$CAST_WAIT_TYPE":
				SetFieldValue(pTag,"MOBILE_CONNECT_TYPE_NM",out sValue);
				break;
			case "$CAST_VOICE_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_BOTH);
				break;
			case "$CAST_VIDEO_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VIDEO);
				break;
			case "$CAST_VOICE_OK_FLAG":
				sValue = IsEqual(pTag,"CONNECT_TYPE",ViCommConst.WAIT_TYPE_VOICE);
				break;
			case "$HREF_VOICE_WSHOT":
				sValue = GetHrefVoiceTalk(pTag,pArgument);
				break;
			case "$HREF_TV_WSHOT":
				sValue = GetHrefTvWShot(pTag,pArgument);
				break;
			case "$CAST_ENABLE_PARTY_TALK":
				SetFieldValue(pTag, "MONITOR_ENABLE_FLAG", out sValue);
				break;
			case "$HREF_VOICE_CHAT":
				sValue = GetHrefVoiceChat(pTag, pArgument);
				break;
			case "$HREF_TV_TALK":
				sValue = GetHrefTvTalk(pTag, pArgument);
				break;
			default:
				pParsed = false;
				break;
		}
		
		return sValue;
	}

	private string GetListBeautyClock(string pTag,string pArgument) {

		string[] oArgArr = pArgument.Split(',');
		string sHtmlTemplate = oArgArr[0];	// パースしない


		int iNeedCount = ParseArg(oArgArr,1,1);
		
		NameValueCollection oQuery = new NameValueCollection();
		for (int i = 2;i < oArgArr.Length;i++) {
			string[] sQueryItem = oArgArr[i].Split(new char[] { '=' },2);
			string sKey = sQueryItem[0];
			string sValue = string.Empty;
			if (sQueryItem.Length == 2) {
				sValue = sQueryItem[1];
			}
			oQuery.Add(sKey,sValue);
		}

		BeautyClockSeekCondition oCondition = new BeautyClockSeekCondition(oQuery);
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BEAUTY_CLOCK;
		oCondition.UseTemplate = true;

		DataSet oTmpDataSet = null;
		int iTmpTableIndex = -1;
		using (BeautyClock oBeautyClock = new BeautyClock(sessionObjs)) {
			oTmpDataSet = oBeautyClock.GetPageCollection(oCondition,1,iNeedCount);
		}
		iTmpTableIndex = ViCommConst.DATASET_EX_BEAUTY_CLOCK;
		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(sHtmlTemplate,oTmpDataSet,iTmpTableIndex);
	}
	private string GetBeautyClockImgPath(string pTag,string pArgument) {
		string[] oArgArr = pArgument.Split(',');
		ViCommConst.PicSizeType iPicSize = (ViCommConst.PicSizeType)ParseArg(oArgArr,0,(int)ViCommConst.PicSizeType.SMALL);

		string sValue = string.Empty;
		SetFieldValue(pTag,ConvertPicSizeType(iPicSize),out sValue);
		if (!string.IsNullOrEmpty(sValue)) {
			sValue = "../" + sValue;
		}
		return sValue;
	}
	private string ConvertPicSizeType(ViCommConst.PicSizeType pPicSizeType) {
		switch (pPicSizeType) {
			case ViCommConst.PicSizeType.ORIGINAL:
				return "BEAUTY_IMG_PATH";
			case ViCommConst.PicSizeType.LARGE:
				return "BEAUTY_IMG_PATH";
			case ViCommConst.PicSizeType.SMALL:
				return "BEAUTY_IMG_S_PATH";
			default:
				return string.Empty;
		}
	}

	private string GetQueryViewBeautyClock(string pTag,string pArgument){
		return GetQueryViewBeautyClock(pTag,pArgument,0);
	}

	private string GetQueryViewBeautyClock(string pTag,string pArgument,int pDiffTime){		
		string sAssignedTime = string.Empty;
		if(pArgument.Equals(string.Empty)){
			sAssignedTime = GetAssignedTime(pTag,pDiffTime);
		}else if(pArgument.Equals("just_now")){
			sAssignedTime = pArgument;
		} else {
			sAssignedTime = ViCommPrograms.ConvertTimeToAssigned(pArgument);			
		}
		return string.Format("assigned_time={0}&seekmode={1}",sAssignedTime,ViCommConst.INQUIRY_EXTENSION_BEAUTY_CLOCK);
	}
	
	private string GetAssignedTime(string pTag){
		return GetAssignedTime(pTag,0);
	}

	private string GetAssignedTime(string pTag,int pDiffTime) {
		string sValue = string.Empty;
		if(SetFieldValue(pTag,"ASSIGNED_TIME",out sValue)){
			return string.Format("{0}",int.Parse(sValue)+pDiffTime);
		}
		return sValue;
	
	}

	private bool IsEnableTime(string pAssignedTime) {
		int iTime = int.Parse(pAssignedTime);
		return (ViCommConst.BeautyClock.MIN_ASSIGNED_TIME <= iTime && iTime <= ViCommConst.BeautyClock.MAX_ASSIGNED_TIME);
	}	
}
