﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品

--	Progaram ID		: Product
--
--  Creation Date	: 2010.12.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductAgent : DbSession {
	public ProductAgent() { }

	public DataSet GetOne(string pProductAgentCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT 													");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.PRODUCT_AGENT_CD		,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.PRODUCT_AGENT_NM        ,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.PRODUCT_AGENT_DOMAIN    ,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.PRODUCT_AGENT_UID       ,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.PRODUCT_AGENT_PWD       ,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.LAST_LINK_DATE          ,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.REVISION_NO             ,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.OUTER_AGENT_FLAG        ,               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.PRODUCT_TYPE	                        ");
		oSqlBuilder.AppendLine(" FROM                                                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT                                         ");
		oSqlBuilder.AppendLine(" WHERE                                                      ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_AGENT.PRODUCT_AGENT_CD = :PRODUCT_AGENT_CD    ");
		
		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();

			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);

				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.da.Fill(oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;

	}
}
