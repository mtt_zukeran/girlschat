﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾟﾛｸﾞﾗﾑ拡張
--	Progaram ID		: ProgramEx
--
--  Creation Date	: 2011.05.30
--  Creater			: K.Itoh
--
**************************************************************************/
using System.Collections;
using System;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using System.Web.UI;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


[System.Serializable]
public class BreadcrumbManage : DbSession {
	
	public string programRoot = null;
	public string programId = null;
	public string htmlDocSeq = null;	
	public string breadcrumbTitle = null;
	public bool topBreadcrumbFlag = false;
	public bool enableBreadcrumbFlag = false;
	

	public bool GetOne(string pProgramRoot, string pProgramId, string pHtmlDocSeq) {
		bool bHasData = false;
		try {
			conn = DbConnect();

			System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
			oSqlBuilder.Append(" SELECT                                    ").AppendLine();
			oSqlBuilder.Append("   P.PROGRAM_ROOT				,          ").AppendLine();
			oSqlBuilder.Append("   P.PROGRAM_ID					,          ").AppendLine();
			oSqlBuilder.Append("   P.HTML_DOC_SEQ				,          ").AppendLine();
			oSqlBuilder.Append("   P.BREADCRUMB_TITLE			,          ").AppendLine();
			oSqlBuilder.Append("   P.ENABLE_BREADCRUMB_FLAG		,          ").AppendLine();
			oSqlBuilder.Append("   P.TOP_BREADCRUMB_FLAG	               ").AppendLine();
			oSqlBuilder.Append(" FROM                                      ").AppendLine();
			oSqlBuilder.Append("   T_BREADCRUMB_MANAGE P                   ").AppendLine();
			oSqlBuilder.Append(" WHERE                                     ").AppendLine();
			oSqlBuilder.Append("   P.PROGRAM_ROOT			= :PROGRAM_ROOT				AND  ").AppendLine();
			oSqlBuilder.Append("   P.PROGRAM_ID				= :PROGRAM_ID				AND  ").AppendLine();
			oSqlBuilder.Append("   P.HTML_DOC_SEQ			= :HTML_DOC_SEQ				AND  ").AppendLine();
			oSqlBuilder.Append("   P.ENABLE_BREADCRUMB_FLAG = :ENABLE_BREADCRUMB_FLAG        ").AppendLine();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":PROGRAM_ROOT", pProgramRoot);
				cmd.Parameters.Add(":PROGRAM_ID", pProgramId);
				cmd.Parameters.Add(":HTML_DOC_SEQ", pHtmlDocSeq);
				cmd.Parameters.Add(":ENABLE_BREADCRUMB_FLAG", ViCommConst.FLAG_ON);

				using(da = new OracleDataAdapter(cmd)){
					using(DataSet oDataSet = new DataSet()){
						this.Fill(da,oDataSet);
						
						if(oDataSet.Tables[0].Rows.Count>0){
							bHasData = true;

							programRoot = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PROGRAM_ROOT"]);
							programId = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PROGRAM_ID"]);
							htmlDocSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["HTML_DOC_SEQ"]);
							breadcrumbTitle = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["BREADCRUMB_TITLE"]);
							topBreadcrumbFlag = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOP_BREADCRUMB_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
							enableBreadcrumbFlag = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENABLE_BREADCRUMB_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);	
						}						
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bHasData;
	}
	
	
}
