/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: 認証IP
--	Progaram ID		: AuthIP
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Collections.Specialized;
using iBridCommLib;

[System.Serializable]
public class AuthIP:DbSession {

	public AuthIP() {
	}

	public bool IsExist(string pIP,string pDomain,string pUserAgent,NameValueCollection pQuery,out int pPcRedirectFlag,out bool pIsCrawler) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pPcRedirectFlag = 0;
		pIsCrawler = false;
		try{
			conn = DbConnect("AuthIP.IsExist");

			using (cmd = CreateSelectCommand("SELECT REDIRECT_PC_PAGE_FLAG FROM T_AUTH_IP WHERE IP_ADDR =:IP_ADDR AND NA_MOBILE_PAGE_FLAG = 0",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("IP_ADDR",pIP);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AUTH_IP");
					if (ds.Tables["T_AUTH_IP"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						pPcRedirectFlag = int.Parse(dr["REDIRECT_PC_PAGE_FLAG"].ToString());
						bExist = true;
					}
				}
			}

			if (iBridUtil.GetStringValue(pQuery["crw"]).Equals("1")) {
				pIsCrawler = true;
				bExist = true;
			} else {
				string sCrawlers = ConfigurationManager.AppSettings["crawler"];
				if (!string.IsNullOrEmpty(sCrawlers)) {
					string[] sCrawler = sCrawlers.Split(',');
					for (int i = 0;i < sCrawler.Length;i++) {
						if (pUserAgent.IndexOf(sCrawler[i]) >= 0) {
							EventLogWriter.Debug(string.Format("Crawler Site:{0} IP:{1} Crawler:{2}",pDomain,pIP,sCrawler[i]));
							bExist = true;
							pIsCrawler = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
