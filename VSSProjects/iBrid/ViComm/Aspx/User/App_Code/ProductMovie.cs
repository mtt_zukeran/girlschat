﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductMovie : ProductBase {
	public ProductMovie() : base() { }
	public ProductMovie(SessionObjs pSessionObj) : base(pSessionObj) { }
	
	public DataSet GetListByType(string pSiteCd,string pProductAgentCd,string pProductSeq,string pProductMovieType){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT															");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.SITE_CD				,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PRODUCT_AGENT_CD    ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PRODUCT_SEQ         ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.OBJ_SEQ             ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PRODUCT_MOVIE_TYPE  ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.OUTER_MOVIE_FLAG    ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PLAY_TIME           ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.LINK_ID             ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.LINK_DATE           ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.UPLOAD_DATE         ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.UNEDITABLE_FLAG     ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.REVISION_NO         ,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.UPDATE_DATE                                 ");
		oSqlBuilder.AppendLine(" FROM                                                           ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE                                             ");
		oSqlBuilder.AppendLine(" WHERE                                                          ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PRODUCT_AGENT_CD    = :PRODUCT_AGENT_CD	AND ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PRODUCT_SEQ         = :PRODUCT_SEQ		AND ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PRODUCT_MOVIE_TYPE  = :PRODUCT_MOVIE_TYPE   ");
		
		DataSet oDataSet = new DataSet();
		try{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
				cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
				cmd.Parameters.Add(":PRODUCT_MOVIE_TYPE", pProductMovieType);
				
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(oDataSet);
				}
				
			}
		}finally{
			conn.Close();
		}
		return oDataSet;
	}
	
	public DataSet GetOne(string pMovieSeq){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT ");
		oSqlBuilder.AppendLine("	SITE_CD				, ");
		oSqlBuilder.AppendLine("	PRODUCT_AGENT_CD    , ");
		oSqlBuilder.AppendLine("	PRODUCT_SEQ         , ");
		oSqlBuilder.AppendLine("	OBJ_SEQ             , ");
		oSqlBuilder.AppendLine("	PRODUCT_MOVIE_TYPE  , ");
		oSqlBuilder.AppendLine("	OUTER_MOVIE_FLAG    , ");
		oSqlBuilder.AppendLine("	PLAY_TIME           , ");
		oSqlBuilder.AppendLine("	LINK_ID             , ");
		oSqlBuilder.AppendLine("	LINK_DATE           , ");
		oSqlBuilder.AppendLine("	UPLOAD_DATE         , ");
		oSqlBuilder.AppendLine("	UNEDITABLE_FLAG     , ");
		oSqlBuilder.AppendLine("	REVISION_NO         , ");
		oSqlBuilder.AppendLine("	UPDATE_DATE           ");
		oSqlBuilder.AppendLine(" FROM ");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE ");
		oSqlBuilder.AppendLine(" WHERE ");
		oSqlBuilder.AppendLine("	OBJ_SEQ=:OBJ_SEQ ");
		
		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();
			using(cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)){
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":OBJ_SEQ", pMovieSeq);
				using(da = new OracleDataAdapter(this.cmd)){
					da.Fill(oDataSet);
				}
			}
		}finally{
			this.conn.Close();
		}
		return oDataSet;
	}

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(ProductSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT	");
		oSqlBuilder.AppendLine(base.CreateCountFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		
		int iPageCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oWhereParams);
				
				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			this.conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionBySeq(string pProductSeq) {
		ProductSeekCondition oCondtion = new ProductSeekCondition();
		oCondtion.ProductSeq = pProductSeq;
		return this.GetPageCollectionBase(oCondtion,0,0);				
	}
	public DataSet GetPageCollection(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}	
	private DataSet GetPageCollectionBase(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage){

		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT																				");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.OBJ_SEQ				,                                           ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE.PLAY_TIME			,                                           ");
		oSqlBuilder.Append(base.CreateFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where		
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresionBase(pCondtion);

		return ExecutePageCollection(oSqlBuilder.ToString(), pCondtion,  oWhereParams, sSortExpression, iStartIndex, pRecPerPage);
	}
	

	
	
	private OracleParameter[] CreateWhere(ProductSeekCondition pCondition,ref string pWhereClause){
		if(pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		// 商品共通のWHERE条件を作成
		oParamList.AddRange(base.CreateWhereBase(pCondition,ref pWhereClause));

		SysPrograms.SqlAppendWhere(" T_PRODUCT_MOVIE.SITE_CD			= VW_PRODUCT00.SITE_CD				", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" T_PRODUCT_MOVIE.PRODUCT_AGENT_CD	= VW_PRODUCT00.PRODUCT_AGENT_CD	", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" T_PRODUCT_MOVIE.PRODUCT_SEQ		= VW_PRODUCT00.PRODUCT_SEQ			", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" T_PRODUCT_MOVIE.PRODUCT_MOVIE_TYPE	= :PRODUCT_MOVIE_TYPE			", ref pWhereClause);
		oParamList.Add(new OracleParameter(":PRODUCT_MOVIE_TYPE", ViCommConst.ProductMovieType.CONTENTS));
		
		return oParamList.ToArray();
	}

	private string CreateFrom(ProductSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_MOVIE			,	");
		oSqlBuilder.AppendLine(base.CreateFromBase(pCondition));
		return oSqlBuilder.ToString();
	}


	#endregion ========================================================================================================


	protected override string GetProductType(ProductSeekCondition pCondition) {
		return ProductHelper.GetMovieProductType(pCondition.AdultFlag);
	}
}
