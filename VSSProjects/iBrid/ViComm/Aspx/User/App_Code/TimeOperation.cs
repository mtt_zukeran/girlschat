﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者時間別稼動集計 
--	Progaram ID		: TimeOperation
--
--  Creation Date	: 2009.07.23
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using ViComm;

[System.Serializable]
public class TimeOperation:CastBase {

	public TimeOperation(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}
	public TimeOperation()
		: base() {
	}

	public DataSet GetAllSum(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;

		string sNotPayExpireMonth;
		int iNotPayExpireMonth;
		string sReportDayTime;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			oManageCompany.GetValue("NOT_PAY_EXPIRE_MONTH",out sNotPayExpireMonth);
		}
		int.TryParse(sNotPayExpireMonth,out iNotPayExpireMonth);
		if (iNotPayExpireMonth != 0) {
			sReportDayTime = DateTime.Now.AddMonths(iNotPayExpireMonth * -1).ToString("yyyyMM") + "0100";
		} else {
			sReportDayTime = "1900010100";
		}

		try {
			conn = DbConnect("TimeOperation.GetAllSum");

			string sSql = "SELECT " + GetField() +
							"FROM " +
								"T_TIME_OPERATION " +
							"WHERE " +
								"SITE_CD		= :SITE_CD		AND " +
								"USER_SEQ		= :USER_SEQ		AND " +
								"USER_CHAR_NO	= :USER_CHAR_NO	AND " +
								"PAYMENT_FLAG	= :PAYMENT_FLAG	AND " +
								"REPORT_DAY_TIME>=:REPORT_DAY_TIME ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PAYMENT_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("REPORT_DAY_TIME",sReportDayTime);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TIME_OPERATION");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void GetTotalPayment(string pSiteCd,string pUserSeq,out int pPaymentAmt,out int pPaymentPoint) {
		DataSet ds;
		DataRow dr;

		string sNotPayExpireMonth;
		int iNotPayExpireMonth;
		string sReportDayTime;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			oManageCompany.GetValue("NOT_PAY_EXPIRE_MONTH",out sNotPayExpireMonth);
		}
		int.TryParse(sNotPayExpireMonth,out iNotPayExpireMonth);
		if (iNotPayExpireMonth != 0) {
			sReportDayTime = DateTime.Now.AddMonths(iNotPayExpireMonth * -1).ToString("yyyyMM") + "0100";
		} else {
			sReportDayTime = "1900010100";
		}

		try {
			conn = DbConnect("TimeOperation.GetAllSum");
			StringBuilder oStringBuilder = new StringBuilder();
			oStringBuilder.AppendLine("SELECT	");
			oStringBuilder.AppendLine("		NVL(SUM(PRV_TV_TALK_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			PUB_TV_TALK_PAY_AMT 			+ 	");
			oStringBuilder.AppendLine("			PRV_VOICE_TALK_PAY_AMT 			+ 	");
			oStringBuilder.AppendLine("			PUB_VOICE_TALK_PAY_AMT 			+ 	");
			oStringBuilder.AppendLine("			VIEW_TALK_PAY_AMT				+ 	");
			oStringBuilder.AppendLine("			VIEW_BROADCAST_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			MOVIE_PAY_AMT					+ 	");
			oStringBuilder.AppendLine("			USER_MAIL_PAY_AMT				+ 	");
			oStringBuilder.AppendLine("			OPEN_PIC_MAIL_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			OPEN_MOVIE_MAIL_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			OPEN_PIC_BBS_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			OPEN_MOVIE_BBS_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			USER_MAIL_PIC_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			USER_MAIL_MOVIE_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			DOWNLOAD_MOVIE_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			TIME_CALL_PAY_AMT				+ 	");
			oStringBuilder.AppendLine("			DOWNLOAD_VOICE_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			SOCIAL_GAME_PAY_AMT				+ 	");
			oStringBuilder.AppendLine("			YAKYUKEN_PAY_AMT				+ 	");
			oStringBuilder.AppendLine("			PRESENT_MAIL_PAY_AMT			+	");
			oStringBuilder.AppendLine("			FRIEND_INTRO_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			PRV_MESSAGE_RX_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			PRV_PROFILE_RX_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			WIRETAP_PAY_AMT					+ 	");
			oStringBuilder.AppendLine("			GPF_TALK_VOICE_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			PLAY_PROFILE_PAY_AMT			+ 	");
			oStringBuilder.AppendLine("			REC_PROFILE_PAY_AMT				+ 	");
			oStringBuilder.AppendLine("			PLAY_PV_MSG_PAY_AMT				+ 	");
			oStringBuilder.AppendLine("			LIVE_PAY_AMT					+ 	");
			oStringBuilder.AppendLine("			CAST_PRV_TV_TALK_PAY_AMT		+ 	");
			oStringBuilder.AppendLine("			CAST_PUB_TV_TALK_PAY_AMT		+ 	");
			oStringBuilder.AppendLine("			CAST_PRV_VOICE_TALK_PAY_AMT		+ 	");
			oStringBuilder.AppendLine("			CAST_PUB_VOICE_TALK_PAY_AMT	),0) TOTAL_PAYMENT_AMT, 	");
			oStringBuilder.AppendLine("		NVL(SUM(PRV_TV_TALK_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			PUB_TV_TALK_PAY_POINT 			+ 	");
			oStringBuilder.AppendLine("			PRV_VOICE_TALK_PAY_POINT 		+ 	");
			oStringBuilder.AppendLine("			PUB_VOICE_TALK_PAY_POINT 		+ 	");
			oStringBuilder.AppendLine("			VIEW_TALK_PAY_POINT				+ 	");
			oStringBuilder.AppendLine("			VIEW_BROADCAST_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			MOVIE_PAY_POINT					+ 	");
			oStringBuilder.AppendLine("			USER_MAIL_PAY_POINT				+ 	");
			oStringBuilder.AppendLine("			OPEN_PIC_MAIL_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			OPEN_MOVIE_MAIL_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			OPEN_PIC_BBS_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			OPEN_MOVIE_BBS_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			USER_MAIL_PIC_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			USER_MAIL_MOVIE_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			DOWNLOAD_MOVIE_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			TIME_CALL_PAY_POINT				+ 	");
			oStringBuilder.AppendLine("			DOWNLOAD_VOICE_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			SOCIAL_GAME_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			YAKYUKEN_PAY_POINT				+ 	");
			oStringBuilder.AppendLine("			PRESENT_MAIL_PAY_POINT			+	");
			oStringBuilder.AppendLine("			FRIEND_INTRO_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			PRV_MESSAGE_RX_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			PRV_PROFILE_RX_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			WIRETAP_PAY_POINT				+ 	");
			oStringBuilder.AppendLine("			GPF_TALK_VOICE_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			PLAY_PROFILE_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			REC_PROFILE_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			PLAY_PV_MSG_PAY_POINT			+ 	");
			oStringBuilder.AppendLine("			LIVE_PAY_POINT					+ 	");
			oStringBuilder.AppendLine("			CAST_PRV_TV_TALK_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			CAST_PUB_TV_TALK_PAY_POINT		+ 	");
			oStringBuilder.AppendLine("			CAST_PRV_VOICE_TALK_PAY_POINT	+ 	");
			oStringBuilder.AppendLine("			CAST_PUB_VOICE_TALK_PAY_POINT	),0) TOTAL_PAYMENT_POINT 	");
			oStringBuilder.AppendLine("FROM 	");
			oStringBuilder.AppendLine("		T_TIME_OPERATION 	");
			oStringBuilder.AppendLine("WHERE 	");
			if (!pSiteCd.Equals(string.Empty)) {
				oStringBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND	");
			}
			oStringBuilder.AppendLine("		USER_SEQ		= :USER_SEQ		AND ");
			oStringBuilder.AppendLine("		PAYMENT_FLAG	= :PAYMENT_FLAG	AND ");
			oStringBuilder.AppendLine("		REPORT_DAY_TIME>= :REPORT_DAY_TIME ");

			using (cmd = CreateSelectCommand(oStringBuilder.ToString(),conn))
			using (ds = new DataSet()) {

				if (!pSiteCd.Equals(string.Empty)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("PAYMENT_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("REPORT_DAY_TIME",sReportDayTime);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TIME_OPERATION");
				}
			}
		} finally {
			conn.Close();
		}
		dr = ds.Tables["T_TIME_OPERATION"].Rows[0];
		int.TryParse(dr["TOTAL_PAYMENT_AMT"].ToString(),out pPaymentAmt);
		int.TryParse(dr["TOTAL_PAYMENT_POINT"].ToString(),out pPaymentPoint);
	}

	public DataSet GetPerformance(string pSiteCd,string pUserSeq,string pUserCharNo,string pFromYYYY,string pFromMM,string pFromDD,string pToYYYY,string pToMM,string pToDD,bool pNotPaymentOnlyFlag) {
		DataSet ds;

		string sNotPayExpireMonth;
		int iNotPayExpireMonth;
		string sReportDayTime;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			oManageCompany.GetValue("NOT_PAY_EXPIRE_MONTH",out sNotPayExpireMonth);
		}
		int.TryParse(sNotPayExpireMonth,out iNotPayExpireMonth);
		if (iNotPayExpireMonth != 0) {
			sReportDayTime = DateTime.Now.AddMonths(iNotPayExpireMonth * -1).ToString("yyyyMM") + "0100";
		} else {
			sReportDayTime = "1900010100";
		}

		string sReportDayTimeFrom = pFromYYYY + pFromMM + pFromDD + "00";
		string sReportDayTimeTo = pToYYYY + pToMM + pToDD + "23";
		if (pNotPaymentOnlyFlag) {
			if (string.Compare(sReportDayTime,sReportDayTimeFrom) > 0) {
				sReportDayTimeFrom = sReportDayTime;
			}
		}

		try {
			conn = DbConnect("TimeOperation.GetAllSum");

			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine(GetField());
			sSql.AppendLine("FROM	");
			sSql.AppendLine("	T_TIME_OPERATION	");
			sSql.AppendLine("WHERE	");
			if (!pSiteCd.Equals(string.Empty)) {
				sSql.AppendLine("SITE_CD		= :SITE_CD				AND	");
			}
			sSql.AppendLine("	USER_SEQ		= :USER_SEQ				AND	");
			if (!pUserCharNo.Equals(string.Empty)) {
				sSql.AppendLine("USER_CHAR_NO	= :USER_CHAR_NO			AND	");
			}
			if (pNotPaymentOnlyFlag) {
				sSql.AppendLine("PAYMENT_FLAG	= :PAYMENT_FLAG			AND	");
			}
			sSql.AppendLine("	REPORT_DAY_TIME	>=:REPORT_DAY_TIME_FROM	AND	");
			sSql.AppendLine("	REPORT_DAY_TIME <=:REPORT_DAY_TIME_TO		");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				if (!pSiteCd.Equals(string.Empty)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}
				cmd.Parameters.Add("USER_SEQ2",pUserSeq);
				if (!pUserCharNo.Equals(string.Empty)) {
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}
				if (pNotPaymentOnlyFlag) {
					cmd.Parameters.Add("PAYMENT_FLAG",ViCommConst.FLAG_OFF);
				}
				cmd.Parameters.Add("REPORT_DAY_TIME_FROM",sReportDayTimeFrom);
				cmd.Parameters.Add("REPORT_DAY_TIME_TO",sReportDayTimeTo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TIME_OPERATION");
				}
			}
		} finally {
			conn.Close();
		}
		AppendBonusAmt(ds,pUserSeq,sReportDayTimeFrom,sReportDayTimeTo,pNotPaymentOnlyFlag);
		return ds;
	}

	private void AppendBonusAmt(DataSet pDS,string pUserSeq,string pReportDayTimeFrom,string pReportDayTimeTo,bool pNotPaymentOnlyFlag) {
		DataSet ds;
		DataColumn col;
		col = new DataColumn("BONUS_AMT_ADMIN",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_FAVORIT",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_REFUSE",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_BLOG",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_OMIKUJI",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_FANCLUB",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_ADMIN",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_FAVORIT",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_REFUSE",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_BLOG",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_OMIKUJI",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_FANCLUB",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		pDS.Tables[0].Rows[0]["BONUS_AMT_ADMIN"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_AMT_FAVORIT"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_AMT_REFUSE"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_AMT_BLOG"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_AMT_OMIKUJI"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_AMT_FANCLUB"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_POINT_ADMIN"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_POINT_FAVORIT"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_POINT_REFUSE"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_POINT_BLOG"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_POINT_OMIKUJI"] = "0";
		pDS.Tables[0].Rows[0]["BONUS_POINT_FANCLUB"] = "0";

		StringBuilder sSql = new StringBuilder();
		sSql.AppendLine("SELECT	");
		sSql.AppendLine("	NVL(SUM(BONUS_AMT),0)	AS BONUS_AMT	,	");
		sSql.AppendLine("	NVL(SUM(BONUS_POINT),0) AS BONUS_POINT	,	");
		sSql.AppendLine("	BONUS_POINT_TYPE							");
		sSql.AppendLine("FROM	");
		sSql.AppendLine("	T_BONUS_LOG	");
		sSql.AppendLine("WHERE	");
		sSql.AppendLine("	USER_SEQ		= :USER_SEQ		AND	");
		if (pNotPaymentOnlyFlag) {
			sSql.AppendLine("PAYMENT_FLAG	= :PAYMENT_FLAG	AND	");
		} else {
			sSql.AppendLine("CONVERT_NOT_PAYMENT_FLAG	= :CONVERT_NOT_PAYMENT_FLAG	AND	");
		}
		sSql.AppendLine("	REPORT_DAY_TIME	>=:REPORT_DAY_TIME_FROM	AND	");
		sSql.AppendLine("	REPORT_DAY_TIME <=:REPORT_DAY_TIME_TO		");
		sSql.AppendLine("GROUP BY ");
		sSql.AppendLine("	BONUS_POINT_TYPE			");

		try {
			conn = DbConnect("TimeOperation.AppendBonusAmt");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				if (pNotPaymentOnlyFlag) {
					cmd.Parameters.Add("PAYMENT_FLAG",ViCommConst.FLAG_OFF);
				} else {
					cmd.Parameters.Add("CONVERT_NOT_PAYMENT_FLAG",ViCommConst.FLAG_OFF);
				}
				cmd.Parameters.Add("REPORT_DAY_TIME_FROM",pReportDayTimeFrom);
				cmd.Parameters.Add("REPORT_DAY_TIME_TO",pReportDayTimeTo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BONUS_LOG");
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_ADMIN)) {
							pDS.Tables[0].Rows[0]["BONUS_AMT_ADMIN"] = dr["BONUS_AMT"].ToString();
							pDS.Tables[0].Rows[0]["BONUS_POINT_ADMIN"] = dr["BONUS_POINT"].ToString();
						} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_FAVORIT)) {
							pDS.Tables[0].Rows[0]["BONUS_AMT_FAVORIT"] = dr["BONUS_AMT"].ToString();
							pDS.Tables[0].Rows[0]["BONUS_POINT_FAVORIT"] = dr["BONUS_POINT"].ToString();
						} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_REFUSE)) {
							pDS.Tables[0].Rows[0]["BONUS_AMT_REFUSE"] = dr["BONUS_AMT"].ToString();
							pDS.Tables[0].Rows[0]["BONUS_POINT_REFUSE"] = dr["BONUS_POINT"].ToString();
						} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_BLOG)) {
							pDS.Tables[0].Rows[0]["BONUS_AMT_BLOG"] = dr["BONUS_AMT"].ToString();
							pDS.Tables[0].Rows[0]["BONUS_POINT_BLOG"] = dr["BONUS_POINT"].ToString();
						} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_OMIKUJI)) {
							pDS.Tables[0].Rows[0]["BONUS_AMT_OMIKUJI"] = dr["BONUS_AMT"].ToString();
							pDS.Tables[0].Rows[0]["BONUS_POINT_OMIKUJI"] = dr["BONUS_POINT"].ToString();
						} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_FANCLUB)) {
							pDS.Tables[0].Rows[0]["BONUS_AMT_FANCLUB"] = dr["BONUS_AMT"].ToString();
							pDS.Tables[0].Rows[0]["BONUS_POINT_FANCLUB"] = dr["BONUS_POINT"].ToString();
						}
					}
				}
			}
		} finally {
			conn.Close();
		}

	}

	private string GetField() {
		StringBuilder oStringBuilder = new StringBuilder();
		oStringBuilder.AppendLine("NVL(SUM(PRV_TV_TALK_PAY_AMT),0)				PRV_TV_TALK_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_TV_TALK_PAY_POINT),0)			PRV_TV_TALK_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_TV_TALK_MIN),0)					PRV_TV_TALK_MIN 				, ");
		oStringBuilder.AppendLine("NVL(SUM(PUB_TV_TALK_PAY_AMT),0)				PUB_TV_TALK_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(PUB_TV_TALK_PAY_POINT),0)			PUB_TV_TALK_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PUB_TV_TALK_MIN),0)					PUB_TV_TALK_MIN					, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_VOICE_TALK_PAY_AMT),0)			PRV_VOICE_TALK_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_VOICE_TALK_PAY_POINT),0)			PRV_VOICE_TALK_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_VOICE_TALK_MIN),0)				PRV_VOICE_TALK_MIN				, ");
		oStringBuilder.AppendLine("NVL(SUM(PUB_VOICE_TALK_PAY_AMT),0)			PUB_VOICE_TALK_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PUB_VOICE_TALK_PAY_POINT),0)			PUB_VOICE_TALK_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(PUB_VOICE_TALK_MIN),0)				PUB_VOICE_TALK_MIN				, ");
		oStringBuilder.AppendLine("NVL(SUM(VIEW_TALK_PAY_AMT),0)				VIEW_TALK_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(VIEW_TALK_PAY_POINT),0)				VIEW_TALK_PAY_POINT				, ");
		oStringBuilder.AppendLine("NVL(SUM(VIEW_TALK_MIN),0)					VIEW_TALK_MIN					, ");
		oStringBuilder.AppendLine("NVL(SUM(VIEW_BROADCAST_PAY_AMT),0)			VIEW_BROADCAST_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(VIEW_BROADCAST_PAY_POINT),0)			VIEW_BROADCAST_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(VIEW_BROADCAST_MIN),0)				VIEW_BROADCAST_MIN				, ");
		oStringBuilder.AppendLine("NVL(SUM(LIVE_PAY_AMT),0)						LIVE_PAY_AMT					, ");
		oStringBuilder.AppendLine("NVL(SUM(LIVE_PAY_POINT),0)					LIVE_PAY_POINT					, ");
		oStringBuilder.AppendLine("NVL(SUM(LIVE_MIN),0)							LIVE_MIN						, ");
		oStringBuilder.AppendLine("NVL(SUM(MOVIE_PAY_AMT),0)					MOVIE_PAY_AMT					, ");
		oStringBuilder.AppendLine("NVL(SUM(MOVIE_PAY_POINT),0)					MOVIE_PAY_POINT					, ");
		oStringBuilder.AppendLine("NVL(SUM(MOVIE_MIN),0)						MOVIE_MIN						, ");
		oStringBuilder.AppendLine("NVL(SUM(PLAY_PROFILE_PAY_AMT),0)				PLAY_PROFILE_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PLAY_PROFILE_PAY_POINT),0)			PLAY_PROFILE_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PLAY_PROFILE_MIN),0)					PLAY_PROFILE_MIN				, ");
		oStringBuilder.AppendLine("NVL(SUM(REC_PROFILE_PAY_AMT),0)				REC_PROFILE_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(REC_PROFILE_PAY_POINT),0)			REC_PROFILE_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(REC_PROFILE_MIN),0)					REC_PROFILE_MIN					, ");
		oStringBuilder.AppendLine("NVL(SUM(WIRETAP_PAY_AMT),0)					WIRETAP_PAY_AMT					, ");
		oStringBuilder.AppendLine("NVL(SUM(WIRETAP_PAY_POINT),0)				WIRETAP_PAY_POINT				, ");
		oStringBuilder.AppendLine("NVL(SUM(WIRETAP_MIN),0)						WIRETAP_MIN						, ");
		oStringBuilder.AppendLine("NVL(SUM(GPF_TALK_VOICE_PAY_AMT),0)			GPF_TALK_VOICE_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(GPF_TALK_VOICE_PAY_POINT),0)			GPF_TALK_VOICE_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(GPF_TALK_VOICE_MIN),0)				GPF_TALK_VOICE_MIN				, ");
		oStringBuilder.AppendLine("NVL(SUM(PLAY_PV_MSG_PAY_AMT),0)				PLAY_PV_MSG_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(PLAY_PV_MSG_PAY_POINT),0)			PLAY_PV_MSG_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PLAY_PV_MSG_MIN),0)					PLAY_PV_MSG_MIN					, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PRV_TV_TALK_PAY_AMT),0)			CAST_PRV_TV_TALK_PAY_AMT 		, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PRV_TV_TALK_PAY_POINT),0)		CAST_PRV_TV_TALK_PAY_POINT 		, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PRV_TV_TALK_MIN),0)				CAST_PRV_TV_TALK_MIN 			, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PUB_TV_TALK_PAY_AMT),0)			CAST_PUB_TV_TALK_PAY_AMT		, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PUB_TV_TALK_PAY_POINT),0)		CAST_PUB_TV_TALK_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PUB_TV_TALK_MIN),0)				CAST_PUB_TV_TALK_MIN			, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PRV_VOICE_TALK_PAY_AMT),0)		CAST_PRV_VOICE_TALK_PAY_AMT		, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PRV_VOICE_TALK_PAY_POINT),0)	CAST_PRV_VOICE_TALK_PAY_POINT	, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PRV_VOICE_TALK_MIN),0)			CAST_PRV_VOICE_TALK_MIN			, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PUB_VOICE_TALK_PAY_AMT),0)		CAST_PUB_VOICE_TALK_PAY_AMT		, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PUB_VOICE_TALK_PAY_POINT),0)	CAST_PUB_VOICE_TALK_PAY_POINT	, ");
		oStringBuilder.AppendLine("NVL(SUM(CAST_PUB_VOICE_TALK_MIN),0)			CAST_PUB_VOICE_TALK_MIN			, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_PAY_AMT),0)				USER_MAIL_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_PAY_POINT),0)				USER_MAIL_PAY_POINT				, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_COUNT),0)					USER_MAIL_COUNT					, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_PIC_MAIL_PAY_AMT),0)			OPEN_PIC_MAIL_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_PIC_MAIL_PAY_POINT),0)			OPEN_PIC_MAIL_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_PIC_MAIL_COUNT),0)				OPEN_PIC_MAIL_COUNT				, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_MOVIE_MAIL_PAY_AMT),0)			OPEN_MOVIE_MAIL_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_MOVIE_MAIL_PAY_POINT),0)		OPEN_MOVIE_MAIL_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_MOVIE_MAIL_COUNT),0)			OPEN_MOVIE_MAIL_COUNT			, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_PIC_BBS_PAY_AMT),0)				OPEN_PIC_BBS_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_PIC_BBS_PAY_POINT),0)			OPEN_PIC_BBS_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_PIC_BBS_COUNT),0)				OPEN_PIC_BBS_COUNT				, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_MOVIE_BBS_PAY_AMT),0)			OPEN_MOVIE_BBS_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_MOVIE_BBS_PAY_POINT),0)			OPEN_MOVIE_BBS_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(OPEN_MOVIE_BBS_COUNT),0)				OPEN_MOVIE_BBS_COUNT			, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_PIC_PAY_AMT),0)			USER_MAIL_PIC_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_PIC_PAY_POINT),0)			USER_MAIL_PIC_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_PIC_COUNT),0)				USER_MAIL_PIC_COUNT				, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_MOVIE_PAY_AMT),0)			USER_MAIL_MOVIE_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_MOVIE_PAY_POINT),0)		USER_MAIL_MOVIE_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(USER_MAIL_MOVIE_COUNT),0)			USER_MAIL_MOVIE_COUNT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_MESSAGE_RX_PAY_AMT),0)			PRV_MESSAGE_RX_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_MESSAGE_RX_PAY_POINT),0)			PRV_MESSAGE_RX_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_MESSAGE_RX_COUNT),0)				PRV_MESSAGE_RX_COUNT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_PROFILE_RX_PAY_AMT),0)			PRV_PROFILE_RX_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_PROFILE_RX_PAY_POINT),0)			PRV_PROFILE_RX_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(PRV_PROFILE_RX_COUNT),0)				PRV_PROFILE_RX_COUNT			, ");
		oStringBuilder.AppendLine("NVL(SUM(DOWNLOAD_MOVIE_PAY_AMT),0)			DOWNLOAD_MOVIE_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(DOWNLOAD_MOVIE_PAY_POINT),0)			DOWNLOAD_MOVIE_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(DOWNLOAD_MOVIE_COUNT),0)				DOWNLOAD_MOVIE_COUNT			, ");
		oStringBuilder.AppendLine("NVL(SUM(TIME_CALL_PAY_AMT),0)				TIME_CALL_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(TIME_CALL_PAY_POINT),0)				TIME_CALL_PAY_POINT				, ");
		oStringBuilder.AppendLine("NVL(SUM(TIME_CALL_COUNT),0)					TIME_CALL_COUNT					, ");
		oStringBuilder.AppendLine("NVL(SUM(DOWNLOAD_VOICE_PAY_AMT),0)			DOWNLOAD_VOICE_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(DOWNLOAD_VOICE_PAY_POINT),0)			DOWNLOAD_VOICE_PAY_POINT		, ");
		oStringBuilder.AppendLine("NVL(SUM(DOWNLOAD_VOICE_COUNT),0)				DOWNLOAD_VOICE_COUNT			, ");
		oStringBuilder.AppendLine("NVL(SUM(SOCIAL_GAME_PAY_AMT),0)				SOCIAL_GAME_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(SOCIAL_GAME_PAY_POINT),0)			SOCIAL_GAME_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(SOCIAL_GAME_COUNT),0)				SOCIAL_GAME_COUNT				, ");
		oStringBuilder.AppendLine("NVL(SUM(YAKYUKEN_PAY_AMT),0)					YAKYUKEN_PAY_AMT				, ");
		oStringBuilder.AppendLine("NVL(SUM(YAKYUKEN_PAY_POINT),0)				YAKYUKEN_PAY_POINT				, ");
		oStringBuilder.AppendLine("NVL(SUM(YAKYUKEN_COUNT),0)					YAKYUKEN_COUNT					, ");
		oStringBuilder.AppendLine("NVL(SUM(PRESENT_MAIL_PAY_AMT),0)				PRESENT_MAIL_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRESENT_MAIL_PAY_POINT),0)			PRESENT_MAIL_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(PRESENT_MAIL_COUNT),0)				PRESENT_MAIL_COUNT				, ");
		oStringBuilder.AppendLine("NVL(SUM(FRIEND_INTRO_PAY_AMT),0)				FRIEND_INTRO_PAY_AMT			, ");
		oStringBuilder.AppendLine("NVL(SUM(FRIEND_INTRO_PAY_POINT),0)			FRIEND_INTRO_PAY_POINT			, ");
		oStringBuilder.AppendLine("NVL(SUM(FRIEND_INTRO_COUNT),0)				FRIEND_INTRO_COUNT	 			");

		return oStringBuilder.ToString();
	}
	
	public DataSet GetPerformanceDaily(string pSiteCd,string pUserSeq,string pUserCharNo,string pDisplayDayFrom,string pDisplayDayTo) {
		DataSet ds = null;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	" + GetField() + "																				,	");
		oSqlBuilder.AppendLine("	TO_DATE(D.DAYS,'YYYY/MM/DD') AS REPORT_DAY														,	");
		oSqlBuilder.AppendLine("	D.DAYS AS REPORT_DAY_STRING																			");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			TO_CHAR(STARTDATE + ROWNUM -1, 'YYYYMMDD') AS DAYS											");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					TRUNC(TO_DATE(:REPORT_DAY_FROM)) AS STARTDATE									,	");
		oSqlBuilder.AppendLine("					TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE(:REPORT_DAY_FROM)), 'DD')) AS DAYS				");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					DUAL																				");
		oSqlBuilder.AppendLine("			)																						,	");
		oSqlBuilder.AppendLine("			T_CODE_DTL																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			ROWNUM <= DAYS																				");
		oSqlBuilder.AppendLine("	) D																								,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			T.*																						,	");
		oSqlBuilder.AppendLine("			SUBSTR(REPORT_DAY_TIME,0,8) AS REPORT_DAY													");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_TIME_OPERATION T																			");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD																AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ																AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO															AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY_TIME	>= :REPORT_DAY_FROM || '00'												AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY_TIME	<= :REPORT_DAY_TO || '23'													");
		oSqlBuilder.AppendLine("	) T																									");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	D.DAYS	= T.REPORT_DAY																			(+)	");
		oSqlBuilder.AppendLine("GROUP BY D.DAYS																							");
		oSqlBuilder.AppendLine("ORDER BY D.DAYS ASC																						");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY_FROM",pDisplayDayFrom));
		oParamList.Add(new OracleParameter(":REPORT_DAY_TO",pDisplayDayTo));
		
		ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		AppendBonusAmtDaily(ds,pUserSeq,pDisplayDayFrom,pDisplayDayTo);
		
		return ds;
	}
	
	private void AppendBonusAmtDaily(DataSet pDS,string pUserSeq,string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataColumn col;
		col = new DataColumn("BONUS_AMT_ADMIN",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_FAVORIT",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_REFUSE",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_BLOG",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_OMIKUJI",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_AMT_FANCLUB",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_ADMIN",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_FAVORIT",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_REFUSE",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_BLOG",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_OMIKUJI",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("BONUS_POINT_FANCLUB",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		int i = 0;

		foreach (DataRow oDr in pDS.Tables[0].Rows) {
			pDS.Tables[0].Rows[i]["BONUS_AMT_ADMIN"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_AMT_FAVORIT"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_AMT_REFUSE"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_AMT_BLOG"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_AMT_OMIKUJI"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_AMT_FANCLUB"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_POINT_ADMIN"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_POINT_FAVORIT"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_POINT_REFUSE"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_POINT_BLOG"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_POINT_OMIKUJI"] = "0";
			pDS.Tables[0].Rows[i]["BONUS_POINT_FANCLUB"] = "0";
			i++;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	NVL(SUM(B.BONUS_AMT),0)	AS BONUS_AMT															,	");
		oSqlBuilder.AppendLine("	NVL(SUM(B.BONUS_POINT),0) AS BONUS_POINT														,	");
		oSqlBuilder.AppendLine("	B.BONUS_POINT_TYPE																				,	");
		oSqlBuilder.AppendLine("	TO_DATE(D.DAYS,'YYYY/MM/DD') AS REPORT_DAY														,	");
		oSqlBuilder.AppendLine("	D.DAYS AS REPORT_DAY_STRING																			");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			TO_CHAR(STARTDATE + ROWNUM -1, 'YYYYMMDD') AS DAYS											");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					TRUNC(TO_DATE(:REPORT_DAY_FROM)) AS STARTDATE									,	");
		oSqlBuilder.AppendLine("					TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE(:REPORT_DAY_FROM)), 'DD')) AS DAYS				");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					DUAL																				");
		oSqlBuilder.AppendLine("			)																						,	");
		oSqlBuilder.AppendLine("			T_CODE_DTL																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			ROWNUM <= DAYS																				");
		oSqlBuilder.AppendLine("	) D																								,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			BONUS_AMT																				,	");
		oSqlBuilder.AppendLine("			BONUS_POINT																				,	");
		oSqlBuilder.AppendLine("			BONUS_POINT_TYPE																		,	");
		oSqlBuilder.AppendLine("			SUBSTR(REPORT_DAY_TIME,0,8) AS REPORT_DAY													");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_BONUS_LOG																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ																AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY_TIME	>=:REPORT_DAY_FROM || '00'												AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY_TIME <=:REPORT_DAY_TO || '23'													");
		oSqlBuilder.AppendLine("	) B																									");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	D.DAYS	= B.REPORT_DAY																			(+)	");
		oSqlBuilder.AppendLine("GROUP BY D.DAYS,B.BONUS_POINT_TYPE																		");
		oSqlBuilder.AppendLine("ORDER BY D.DAYS ASC																						");

		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":REPORT_DAY_FROM",pReportDayFrom));
		oParamList.Add(new OracleParameter(":REPORT_DAY_TO",pReportDayTo));

		ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		DataRow[] oDR;
		
		foreach (DataRow dr in ds.Tables[0].Rows) {
			oDR = pDS.Tables[0].Select(string.Format("REPORT_DAY_STRING = {0}",dr["REPORT_DAY_STRING"].ToString()));
			
			if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_ADMIN)) {
				oDR[0]["BONUS_AMT_ADMIN"] = dr["BONUS_AMT"].ToString();
				oDR[0]["BONUS_POINT_ADMIN"] = dr["BONUS_POINT"].ToString();
			} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_FAVORIT)) {
				oDR[0]["BONUS_AMT_FAVORIT"] = dr["BONUS_AMT"].ToString();
				oDR[0]["BONUS_POINT_FAVORIT"] = dr["BONUS_POINT"].ToString();
			} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_REFUSE)) {
				oDR[0]["BONUS_AMT_REFUSE"] = dr["BONUS_AMT"].ToString();
				oDR[0]["BONUS_POINT_REFUSE"] = dr["BONUS_POINT"].ToString();
			} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_BLOG)) {
				oDR[0]["BONUS_AMT_BLOG"] = dr["BONUS_AMT"].ToString();
				oDR[0]["BONUS_POINT_BLOG"] = dr["BONUS_POINT"].ToString();
			} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_OMIKUJI)) {
				oDR[0]["BONUS_AMT_OMIKUJI"] = dr["BONUS_AMT"].ToString();
				oDR[0]["BONUS_POINT_OMIKUJI"] = dr["BONUS_POINT"].ToString();
			} else if (dr["BONUS_POINT_TYPE"].ToString().Equals(ViCommConst.BONUS_TYPE_FANCLUB)) {
				oDR[0]["BONUS_AMT_FANCLUB"] = dr["BONUS_AMT"].ToString();
				oDR[0]["BONUS_POINT_FANCLUB"] = dr["BONUS_POINT"].ToString();
			}
		}
	}
}