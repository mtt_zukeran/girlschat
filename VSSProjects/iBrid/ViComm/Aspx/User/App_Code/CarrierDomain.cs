﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャリアメールのドメイン
--	Progaram ID		: CarrierDomain
--
--  Creation Date	: 2013.02.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

public class CarrierDomain:DbSession {

	public CarrierDomain()
		: base() {
	}

	public bool CheckCarrierDomain(string pDomain) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	1											");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_CARRIER_DOMAIN							");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	CARRIER_DOMAIN		= :DOMAIN				");

		oParamList.Add(new OracleParameter(":DOMAIN",pDomain));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
