﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板レス
--	Progaram ID		: BbsRes
--  Creation Date	: 2011.04.11
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class BbsRes:CastBase {
	public BbsRes() {
	}

	public int GetPageCount(string pSiteCd,string pBbsThreadSeq,string pBbsThreadSubSeq,string pSearchKey,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("BbsRes.GetPageCount");
			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_BBS_RES00 P ";

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pBbsThreadSeq,pBbsThreadSubSeq,pSearchKey,false,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}

		if (!string.IsNullOrEmpty(pSearchKey)) {
			if (pRecCount > sessionObj.site.bbsResSearchLimit) {
			
				UrlBuilder oErrorPageUrlBuilder = new UrlBuilder("DisplayDoc.aspx");
				oErrorPageUrlBuilder.Parameters.Add("doc",ViCommConst.ERR_FRAME_OVER_BBS_RES_SEARCH_LIMIT);
				HttpContext.Current.Response.Redirect(sessionObj.GetNavigateUrl(oErrorPageUrlBuilder.ToString()),true);
			}
		}
		return iPages;
	}

	private void GetOrder(string pOrderType,string pSearchKey,out string pOrder) {
		if (pSearchKey.Equals(string.Empty)) {
			if (pOrderType.Equals(ViCommConst.BbsResSortType.RES_WRITE_DATE_ASC)) {
				pOrder = " ORDER BY SITE_CD,BBS_THREAD_SUB_SEQ ";
			} else {
				pOrder = " ORDER BY SITE_CD,BBS_THREAD_SUB_SEQ DESC ";
			}
		} else {
			pOrder = " ORDER BY SITE_CD,RES_CREATE_DATE DESC ";
		}
	}

	public int GetRNum(
		string pSiteCd,
		string pBbsThreadSeq,
		string pAnchorNo,
		string pOrderType
	) {
		DataSet ds = GetPageCollectionBase(pSiteCd,pBbsThreadSeq,string.Empty,pAnchorNo,pOrderType,string.Empty,1,1);
		if (ds.Tables[0].Rows.Count > 0) {
			return int.Parse(ds.Tables[0].Rows[0]["RNUM"].ToString());
		} else {
			return 0;
		}
	}
	public DataSet GetPageCollection(string pSiteCd,string pBbsThreadSeq,string pBbsThreadSubSeq,string pOrderType,string pSearchKey,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase(pSiteCd,pBbsThreadSeq,pBbsThreadSubSeq,string.Empty,pOrderType,pSearchKey,pPageNo,pRecPerPage);
	}
	private DataSet GetPageCollectionBase(string pSiteCd,string pBbsThreadSeq,string pBbsThreadSubSeq,string pAnchorNo,string pOrderType,string pSearchKey,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try {
			conn = DbConnect("BbsThread.GetPageCollection");
			ds = new DataSet();

			string sOrder;
			GetOrder(pOrderType,pSearchKey,out sOrder);

			StringBuilder sInnerSql = new StringBuilder();
			sInnerSql.Append("SELECT  * FROM( ");
			sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT " + CastBasicField() + ",").AppendLine();
			sInnerSql.Append("			BBS_THREAD_USER_SEQ		,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_USER_CHAR_NO	,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_SEQ			,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_HANDLE_NM	,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_TITLE		,").AppendLine();
			sInnerSql.Append("			ADMIN_THREAD_FLAG		,").AppendLine();
			sInnerSql.Append("			LAST_RES_WRITE_DATE		,").AppendLine();
			sInnerSql.Append("			LAST_THREAD_SUB_SEQ		,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC1			,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC2			,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC3			,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC4			,").AppendLine();
			sInnerSql.Append("			THREAD_DEL_FLAG			,").AppendLine();
			sInnerSql.Append("			THREAD_CREATE_DATE		,").AppendLine();
			sInnerSql.Append("			THREAD_UPDATE_DATE		,").AppendLine();
			sInnerSql.Append("			TODAY_ACCESS_COUNT		,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_SUB_SEQ		,").AppendLine();
			sInnerSql.Append("			BBS_RES_HANDLE_NM		,").AppendLine();
			sInnerSql.Append("			BBS_RES_USER_SEQ		,").AppendLine();
			sInnerSql.Append("			BBS_RES_USER_CHAR_NO	,").AppendLine();
			sInnerSql.Append("			POST_BBS_THREAD_SEQ		,").AppendLine();
			sInnerSql.Append("			POST_THREAD_SUB_SEQ		,").AppendLine();
			sInnerSql.Append("			RES_DEL_FLAG			,").AppendLine();
			sInnerSql.Append("			RES_CREATE_DATE			,").AppendLine();
			sInnerSql.Append("			RES_UPDATE_DATE			,").AppendLine();
			sInnerSql.Append("			BBS_RES_DOC1			,").AppendLine();
			sInnerSql.Append("			BBS_RES_DOC2			,").AppendLine();
			sInnerSql.Append("			BBS_RES_DOC3			,").AppendLine();
			sInnerSql.Append("			BBS_RES_DOC4			,").AppendLine();
			sInnerSql.Append("			ANONYMOUS_FLAG			").AppendLine();
			sInnerSql.Append("		FROM VW_BBS_RES00 P ");

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pBbsThreadSeq,pBbsThreadSubSeq,pSearchKey,true,ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			if (pAnchorNo.Equals(string.Empty)) {
				sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sInnerSql.Append(")INNER ");
				sInnerSql.Append(")WHERE BBS_THREAD_SUB_SEQ = :BBS_THREAD_SUB_SEQ ");
			}

			SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			StringBuilder sTables = new StringBuilder();
			sTables.Append("T_BBS_RES							,").AppendLine();//返信元のレス 
			sTables.Append("VW_CAST_CHARACTER00					,").AppendLine();//返信元のユーザー 

			StringBuilder sJoinTable = new StringBuilder();
			sJoinTable.Append("T1.SITE_CD						= T_BBS_RES.SITE_CD					(+) AND ").AppendLine();
			sJoinTable.Append("T1.POST_BBS_THREAD_SEQ			= T_BBS_RES.BBS_THREAD_SEQ			(+)	AND	").AppendLine();
			sJoinTable.Append("T1.POST_THREAD_SUB_SEQ			= T_BBS_RES.BBS_THREAD_SUB_SEQ		(+)	AND	").AppendLine();
			sJoinTable.Append("T_BBS_RES.SITE_CD				= VW_CAST_CHARACTER00.SITE_CD		(+)	AND	").AppendLine();
			sJoinTable.Append("T_BBS_RES.BBS_RES_USER_SEQ		= VW_CAST_CHARACTER00.USER_SEQ		(+)	AND	").AppendLine();
			sJoinTable.Append("T_BBS_RES.BBS_RES_USER_CHAR_NO	= VW_CAST_CHARACTER00.USER_CHAR_NO	(+)	AND	").AppendLine();

			StringBuilder sJoinField = new StringBuilder();
			sJoinField.Append("VW_CAST_CHARACTER00.LOGIN_ID		AS	BBS_POST_RES_USER_LOGIN_ID	,").AppendLine();
			sJoinField.Append("VW_CAST_CHARACTER00.HANDLE_NM	AS	BBS_POST_RES_USER_HANDLE_NM	,").AppendLine();
			sJoinField.Append("T_BBS_RES.BBS_RES_HANDLE_NM		AS	BBS_POST_RES_HANDLE_NM		,").AppendLine();
			sJoinField.Append("T_BBS_RES.ANONYMOUS_FLAG			AS	BBS_POST_RES_ANONYMOUS_FLAG	,").AppendLine();
			sJoinField.Append("T_BBS_RES.DEL_FLAG				AS	BBS_POST_RES_DEL_FLAG		,").AppendLine();

			sSql = string.Format(sSql,sInnerSql,sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			string sHint = string.Empty;
			if (oObj.sqlHint.ContainsKey("CAST")) {
				sHint = oObj.sqlHint["CAST"].ToString();
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				if (pAnchorNo.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				} else {
					cmd.Parameters.Add("BBS_THREAD_SUB_SEQ",pAnchorNo);
				}
				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_BBS_RES00");

				}
			}
		} finally {
			conn.Close();
		}
		AppendAttr(ds);
		return ds;
	}

	private void AppendAttr(DataSet pDS) {
		SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
		string sNewDays = oObj.site.newFaceDays.ToString();
		string sNewMark = oObj.site.newFaceMark;
		string sSql;
		if (sNewDays.Equals("")) {
			sNewDays = "0";
		}
		string sNewDay = DateTime.Now.AddDays(-1 * int.Parse(sNewDays)).ToString("yyyy/MM/dd");

		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD		AND " +
						"USER_SEQ		= :USER_SEQ		AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();

			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pBbsThreadSeq,string pBbsThreadSubSeq,string pSearchKey,bool bAllInfo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pBbsThreadSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.BBS_THREAD_SEQ = :BBS_THREAD_SEQ",ref pWhere);
			list.Add(new OracleParameter("BBS_THREAD_SEQ",pBbsThreadSeq));
		}

		if (!pBbsThreadSubSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.BBS_THREAD_SUB_SEQ = :BBS_THREAD_SUB_SEQ",ref pWhere);
			list.Add(new OracleParameter("BBS_THREAD_SUB_SEQ",pBbsThreadSubSeq));
		}

		if (sessionObj.sexCd.Equals(ViCommConst.WOMAN)) {
			SessionWoman sessionWoman = (SessionWoman)sessionObj;
			string sThreadTypeSql = " P.THREAD_TYPE IN (:THREAD_TYPE1 ";
			list.Add(new OracleParameter("THREAD_TYPE1",ViCommConst.BbsThreadType.NORMAL));
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.enabledRichinoFlag == ViCommConst.FLAG_ON) {
				sThreadTypeSql += ",:THREAD_TYPE2 ";
				list.Add(new OracleParameter("THREAD_TYPE2",ViCommConst.BbsThreadType.RICHINO));
			}
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.enabledBlogFlag == ViCommConst.FLAG_ON) {
				sThreadTypeSql += ",:THREAD_TYPE3 ";
				list.Add(new OracleParameter("THREAD_TYPE3",ViCommConst.BbsThreadType.BLOG));
			}
			sThreadTypeSql += " ) ";
			SysPrograms.SqlAppendWhere(sThreadTypeSql,ref pWhere);
		}

		SysPrograms.SqlAppendWhere("P.THREAD_DEL_FLAG = :THREAD_DEL_FLAG",ref pWhere);
		list.Add(new OracleParameter("THREAD_DEL_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere("P.RES_DEL_FLAG = :RES_DEL_FLAG",ref pWhere);
		list.Add(new OracleParameter("RES_DEL_FLAG",ViCommConst.FLAG_OFF));

		if (!pSearchKey.Equals(string.Empty)) {

			StringBuilder oSql = new StringBuilder();

			using (ManageCompany oManageCompany = new ManageCompany())　{
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_BBS_SEARCH_MULTIPLE_ENTRIES,2)) {
					pSearchKey = pSearchKey.Replace("　"," ");
					string[] sArray = pSearchKey.Split(' ');
					string sInnerWhere;
					List<string> oInnerList = new List<string>();

					for (int i = 1;i <= 4;i++) {
						sInnerWhere = string.Empty;
						for (int j = 1;j <= sArray.Length;j++) {
							SysPrograms.SqlAppendWhere(string.Format("P.BBS_RES_DOC{0} LIKE '%' || :BBS_RES_DOC{0}_{1} || '%'",i.ToString(),j.ToString()),ref sInnerWhere);
							list.Add(new OracleParameter(string.Format("BBS_RES_DOC{0}_{1}",i.ToString(),j.ToString()),sArray[j - 1]));
						}
						sInnerWhere = sInnerWhere.Replace("WHERE","");
						oInnerList.Add(string.Format("({0})",sInnerWhere));
					}
					string[] oSqlArray = oInnerList.ToArray();
					SysPrograms.SqlAppendWhere(string.Format(" ( {0} ) ",string.Join(" OR ",oSqlArray)),ref pWhere);
				} else {
					oSql.AppendLine(" (	P.BBS_RES_DOC1	 LIKE '%' || :BBS_RES_DOC1	|| '%'	OR	");
					oSql.AppendLine("	P.BBS_RES_DOC2	 LIKE '%' || :BBS_RES_DOC2	|| '%'	OR	");
					oSql.AppendLine("	P.BBS_RES_DOC3	 LIKE '%' || :BBS_RES_DOC3	|| '%'	OR	");
					oSql.AppendLine("	P.BBS_RES_DOC4	 LIKE '%' || :BBS_RES_DOC4	|| '%'	)");
					SysPrograms.SqlAppendWhere(oSql.ToString(),ref pWhere);
					list.Add(new OracleParameter("BBS_RES_DOC1",pSearchKey));
					list.Add(new OracleParameter("BBS_RES_DOC2",pSearchKey));
					list.Add(new OracleParameter("BBS_RES_DOC3",pSearchKey));
					list.Add(new OracleParameter("BBS_RES_DOC4",pSearchKey));
				}
			}
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void WriteBbsRes(string pSiteCd,string pBbsThreadSeq,string pBbsResUserSeq,string pBbsResUserCharNo,string pBbsResDoc,string pPostBbsThreadSeq,string pPostThreadSubSeq,string pAnonymousFlag,string pBbsResHandleNm,out string pNewBbsThreadSubSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("WRITE_BBS_RES");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pBBS_THREAD_SEQ",DbSession.DbType.VARCHAR2,pBbsThreadSeq);
			oDbSession.ProcedureInParm("pBBS_RES_USER_SEQ",DbSession.DbType.VARCHAR2,pBbsResUserSeq);
			oDbSession.ProcedureInParm("pBBS_RES_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pBbsResUserCharNo);
			oDbSession.ProcedureInParm("pBBS_RES_HANDLE_NM",DbSession.DbType.VARCHAR2,pBbsResHandleNm);
			oDbSession.ProcedureInParm("pBBS_RES_DOC",DbSession.DbType.VARCHAR2,pBbsResDoc);
			oDbSession.ProcedureInParm("pPOST_BBS_THREAD_SEQ",DbSession.DbType.VARCHAR2,pPostBbsThreadSeq);
			oDbSession.ProcedureInParm("pPOST_THREAD_SUB_SEQ",DbSession.DbType.VARCHAR2,pPostThreadSubSeq);
			oDbSession.ProcedureInParm("pANONYMOUS_FLAG",DbSession.DbType.NUMBER,pAnonymousFlag);
			oDbSession.ProcedureOutParm("pNEW_BBS_THREAD_SUB_SEQ",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			pNewBbsThreadSubSeq = oDbSession.GetStringValue("pNEW_BBS_THREAD_SUB_SEQ");
		}
	}

	public bool IsExist(string pSiteCd,string pBbsThreadSeq,string pBbsThreadSubSeq) {
		//レスが会員から見た時に存在しているかチェック
		DataSet ds;
		bool bExist = false;

		if (string.IsNullOrEmpty(pSiteCd) || string.IsNullOrEmpty(pBbsThreadSeq) || string.IsNullOrEmpty(pBbsThreadSubSeq)) {
			return false;
		}

		int iBbsThreadSeq;
		int iBbsThreadSubSeq;

		if (!int.TryParse(pBbsThreadSeq,out iBbsThreadSeq) || !int.TryParse(pBbsThreadSubSeq,out iBbsThreadSubSeq)) {
			return false;
		}

		try {
			conn = DbConnect("BbsRes.IsExist");

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine(" SELECT												");
			oSqlBuilder.AppendLine(" 	BBS_THREAD_SEQ									");
			oSqlBuilder.AppendLine(" FROM												");
			oSqlBuilder.AppendLine(" 	VW_BBS_RES01									");
			oSqlBuilder.AppendLine(" WHERE												");
			oSqlBuilder.AppendLine(" 	SITE_CD				= :SITE_CD				AND	");
			oSqlBuilder.AppendLine("	BBS_THREAD_SEQ		= :BBS_THREAD_SEQ		AND	");
			oSqlBuilder.AppendLine("	THREAD_DEL_FLAG		= :THREAD_DEL_FLAG		AND	");
			oSqlBuilder.AppendLine("	BBS_THREAD_SUB_SEQ	= :BBS_THREAD_SUB_SEQ	AND	");
			oSqlBuilder.AppendLine("	RES_DEL_FLAG		= :RES_DEL_FLAG				");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("BBS_THREAD_SEQ",pBbsThreadSeq);
				cmd.Parameters.Add("THREAD_DEL_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("BBS_THREAD_SUB_SEQ",pBbsThreadSubSeq);
				cmd.Parameters.Add("RES_DEL_FLAG",ViCommConst.FLAG_OFF);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_BBS_RES01");
					if (ds.Tables["VW_BBS_RES01"].Rows.Count != 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
