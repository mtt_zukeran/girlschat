﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: クレジット自動決済設定データクラス
--	Progaram ID		: CreditAutoSettleStatus
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class CreditAutoSettleStatusSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;

	public string CreditAutoSettleSeq {
		get {
			return this.Query["settleseq"];
		}
		set {
			this.Query["settleseq"] = value;
		}
	}

	public CreditAutoSettleStatusSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CreditAutoSettleStatusSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["settleseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["settleseq"]));
	}
}

public class CreditAutoSettleStatus:DbSession {
	public CreditAutoSettleStatus() {
	}

	public DataSet GetPageCollection(CreditAutoSettleStatusSeekCondition pCondtion) {
		string sWhereClause = string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	P.SITE_CD							,	");
		oSqlBuilder.AppendLine("	P.CREDIT_AUTO_SETTLE_SEQ			,	");
		oSqlBuilder.AppendLine("	P.NEXT_SETTLE_DATE					,	");
		oSqlBuilder.AppendLine("	P.SETTLE_COUNT						,	");
		oSqlBuilder.AppendLine("	CAS.SETTLE_AMT						,	");
		oSqlBuilder.AppendLine("	CAS.SETTLE_POINT						");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_CREDIT_AUTO_SETTLE_STATUS P		,	");
		oSqlBuilder.AppendLine("	T_CREDIT_AUTO_SETTLE CAS				");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY P.SITE_CD,CAS.SETTLE_AMT,P.CREDIT_AUTO_SETTLE_SEQ");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CreditAutoSettleStatusSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();
		SysPrograms.SqlAppendWhere("P.SITE_CD = CAS.SITE_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.CREDIT_AUTO_SETTLE_SEQ = CAS.CREDIT_AUTO_SETTLE_SEQ",ref pWhereClause);
		

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SelfUserSeq)) {
			SysPrograms.SqlAppendWhere("P.USER_SEQ = :SELF_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CreditAutoSettleSeq)) {
			SysPrograms.SqlAppendWhere("P.CREDIT_AUTO_SETTLE_SEQ = :CREDIT_AUTO_SETTLE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CREDIT_AUTO_SETTLE_SEQ",pCondition.CreditAutoSettleSeq));
		}

		SysPrograms.SqlAppendWhere("P.UNAVAILABLE_FLAG = :UNAVAILABLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":UNAVAILABLE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("P.RESIGN_FLAG = :RESIGN_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":RESIGN_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	public void RegistCreditAutoSettle(string pSiteCd,string pUserSeq,string pCreditAutoSettleSeq,string pSettleTermDays,out int pSettleAmt,out string pSid,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_CREDIT_AUTO_SETTLE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCREDIT_AUTO_SETTLE_SEQ",DbSession.DbType.VARCHAR2,pCreditAutoSettleSeq);
			db.ProcedureInParm("pSETTLE_TERM_DAYS",DbSession.DbType.VARCHAR2,pSettleTermDays);
			db.ProcedureOutParm("pSETTLE_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pSettleAmt = db.GetIntValue("pSETTLE_AMT");
			pSid = db.GetStringValue("pSID");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void CancelCreditAutoSettle(string pSiteCd,string pUserSeq,string pCreditAutoSettleSeq,string pRemarks,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CANCEL_CREDIT_AUTO_SETTLE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCREDIT_AUTO_SETTLE_SEQ",DbSession.DbType.VARCHAR2,pCreditAutoSettleSeq);
			db.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,pRemarks);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public bool IsExistAvailable(string pSiteCd,string pUserSeq) {
		bool bExist = false;
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	1												");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_CREDIT_AUTO_SETTLE_STATUS						");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	UNAVAILABLE_FLAG	= :UNAVAILABLE_FLAG		AND	");
		oSqlBuilder.AppendLine("	RESIGN_FLAG			= :RESIGN_FLAG				");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":UNAVAILABLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":RESIGN_FLAG",ViCommConst.FLAG_OFF_STR));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			bExist = true;
		}
		
		return bExist;
	}
}
