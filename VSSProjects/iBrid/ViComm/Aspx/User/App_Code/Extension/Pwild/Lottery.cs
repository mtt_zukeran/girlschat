﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ ｶﾞﾁｬ

--	Progaram ID		: Lottery
--
--  Creation Date	: 2011.09.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class LotterySeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string SexCd;
	public string LotteryPublishFlag;
	public string GameItemPublishFlag;
	public string NaFlag;
	public string RecentLotteryFlag;
	
	public string LotterySeq {
		get {
			return this.Query["lottery_seq"];
		}
		set {
			this.Query["lottery_seq"] = value;
		}
	}

	public LotterySeekCondition()
		: this(new NameValueCollection()) {
	}

	public LotterySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["lottery_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["lottery_seq"]));
	}
}

#endregion ============================================================================================================

public class Lottery:DbSession {
	public Lottery() {
	}

	public DataSet GetOne(LotterySeekCondition pCondition) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	*														");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			LOTTERY.LOTTERY_SEQ							,	");
		oSqlBuilder.AppendLine("			LOTTERY.COMPLETE_GET_ITEM_SEQ				,	");
		oSqlBuilder.AppendLine("			LOTTERY.PUBLISH_START_DATE					,	");
		oSqlBuilder.AppendLine("			LOTTERY.PUBLISH_END_DATE					,	");
		oSqlBuilder.AppendLine("			LOTTERY.LOTTERY_NM							,	");
		oSqlBuilder.AppendLine("			LOTTERY.GAME_ITEM_NM						,	");
		oSqlBuilder.AppendLine("			LOTTERY.ATTACK_POWER						,	");
		oSqlBuilder.AppendLine("			LOTTERY.DEFENCE_POWER						,	");
		oSqlBuilder.AppendLine("			LOTTERY.ENDURANCE							,	");
		oSqlBuilder.AppendLine("			LOTTERY.ENDURANCE_NM						,	");
		oSqlBuilder.AppendLine("			LOTTERY.TICKET_NM							,	");
		oSqlBuilder.AppendLine("			CASE											");
		oSqlBuilder.AppendLine("				WHEN COMP.USER_SEQ IS NOT NULL				");
		oSqlBuilder.AppendLine("				THEN 1										");
		oSqlBuilder.AppendLine("				ELSE 0										");
		oSqlBuilder.AppendLine("			END AS COMPLETE_FLAG						,	");
		oSqlBuilder.AppendLine("			CASE											");
		oSqlBuilder.AppendLine("				WHEN SYSDATE BETWEEN LOTTERY.PUBLISH_START_DATE AND LOTTERY.PUBLISH_END_DATE		");
		oSqlBuilder.AppendLine("				THEN 1										");
		oSqlBuilder.AppendLine("				ELSE 0										");
		oSqlBuilder.AppendLine("			END AS NOW_LOTTERY_FLAG						,	");
		oSqlBuilder.AppendLine("			NVL(PLT.POSSESSION_COUNT,0)	AS POSSESSION_TICKET_COUNT	");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			VW_PW_LOTTERY01 LOTTERY						,	");
		oSqlBuilder.AppendLine("			(												");
		oSqlBuilder.AppendLine("				SELECT										");
		oSqlBuilder.AppendLine("					SITE_CD								,	");
		oSqlBuilder.AppendLine("					LOTTERY_SEQ							,	");
		oSqlBuilder.AppendLine("					USER_SEQ								");
		oSqlBuilder.AppendLine("				FROM										");
		oSqlBuilder.AppendLine("					T_LOTTERY_COMPLETE_LOG					");
		oSqlBuilder.AppendLine("				WHERE										");
		oSqlBuilder.AppendLine("					SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("					USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO	= :USER_CHAR_NO			");
		oSqlBuilder.AppendLine("			) COMP										,	");
		oSqlBuilder.AppendLine("			(												");
		oSqlBuilder.AppendLine("				SELECT										");
		oSqlBuilder.AppendLine("					SITE_CD								,	");
		oSqlBuilder.AppendLine("					LOTTERY_SEQ							,	");
		oSqlBuilder.AppendLine("					POSSESSION_COUNT						");
		oSqlBuilder.AppendLine("				FROM										");
		oSqlBuilder.AppendLine("					T_POSSESSION_LOTTERY_TICKET				");
		oSqlBuilder.AppendLine("				WHERE										");
		oSqlBuilder.AppendLine("					SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("					USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO	= :USER_CHAR_NO			");
		oSqlBuilder.AppendLine("			) PLT											");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			LOTTERY.SITE_CD						= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			LOTTERY.SEX_CD						= :SEX_CD					AND	");
		if(pCondition.RecentLotteryFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oSqlBuilder.AppendLine("			LOTTERY.PUBLISH_END_DATE			>	SYSDATE					AND	");
		}
		if(!string.IsNullOrEmpty(pCondition.LotterySeq)) {
			oSqlBuilder.AppendLine("			LOTTERY.LOTTERY_SEQ					= :LOTTERY_SEQ				AND	");
			oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));
		}
		oSqlBuilder.AppendLine("			LOTTERY.LOTTERY_PUBLISH_FLAG		= :LOTTERY_PUBLISH_FLAG		AND	");
		oSqlBuilder.AppendLine("			LOTTERY.GAME_ITEM_PUBLISH_FLAG		= :GAME_ITEM_PUBLISH_FLAG	AND	");
		oSqlBuilder.AppendLine("			LOTTERY.SITE_CD						= COMP.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("			LOTTERY.LOTTERY_SEQ					= COMP.LOTTERY_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("			LOTTERY.SITE_CD						= PLT.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("			LOTTERY.LOTTERY_SEQ					= PLT.LOTTERY_SEQ		(+)		");
		oSqlBuilder.AppendLine("		ORDER BY PUBLISH_START_DATE ASC											");
		oSqlBuilder.AppendLine("	)																			");
		oSqlBuilder.AppendLine("WHERE						");
		oSqlBuilder.AppendLine("	ROWNUM = 1				");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":LOTTERY_PUBLISH_FLAG",pCondition.LotteryPublishFlag));
		oParamList.Add(new OracleParameter(":GAME_ITEM_PUBLISH_FLAG",pCondition.GameItemPublishFlag));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		AppendItemCount(oDataSet,pCondition);
		
		return oDataSet;
	}

	public DataSet GetOneNotComp(LotterySeekCondition pCondition) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ							,	");
		oSqlBuilder.AppendLine("	FIRST_BUY_FREE_END_DATE				,	");
		oSqlBuilder.AppendLine("	PUBLISH_START_DATE					,	");
		oSqlBuilder.AppendLine("	PUBLISH_END_DATE					,	");
		oSqlBuilder.AppendLine("	PRICE	AS LOTTERY_PRICE			,	");
		oSqlBuilder.AppendLine("	PRICE_1	AS LOTTERY_PRICE_1			,	");
		oSqlBuilder.AppendLine("	PRICE_2	AS LOTTERY_PRICE_2			,	");
		oSqlBuilder.AppendLine("	PRICE_3	AS LOTTERY_PRICE_3			,	");
		oSqlBuilder.AppendLine("	LOTTERY_NM							,	");
		oSqlBuilder.AppendLine("	LOTTERY_COUNT_1						,	");
		oSqlBuilder.AppendLine("	LOTTERY_COUNT_2						,	");
		oSqlBuilder.AppendLine("	LOTTERY_COUNT_3						,	");
		oSqlBuilder.AppendLine("	LOTTERY_SET_FLG_1					,	");
		oSqlBuilder.AppendLine("	LOTTERY_SET_FLG_2					,	");
		oSqlBuilder.AppendLine("	LOTTERY_SET_FLG_3					,	");
		oSqlBuilder.AppendLine("	PREMIUM_COUNT_1   					,	");
		oSqlBuilder.AppendLine("	PREMIUM_COUNT_2   					,	");
		oSqlBuilder.AppendLine("	PREMIUM_COUNT_3   					,	");
		oSqlBuilder.AppendLine("			CASE											");
		oSqlBuilder.AppendLine("				WHEN SYSDATE BETWEEN PUBLISH_START_DATE AND PUBLISH_END_DATE				");
		oSqlBuilder.AppendLine("				THEN 1										");
		oSqlBuilder.AppendLine("				ELSE 0										");
		oSqlBuilder.AppendLine("			END AS NOW_LOTTERY_FLAG						,	");
		oSqlBuilder.AppendLine("			CASE											");
		oSqlBuilder.AppendLine("				WHEN SYSDATE BETWEEN FIRST_BUY_FREE_START_DATE AND FIRST_BUY_FREE_END_DATE	");
		oSqlBuilder.AppendLine("				THEN 1										");
		oSqlBuilder.AppendLine("				ELSE 0										");
		oSqlBuilder.AppendLine("			END AS NOW_FIRST_BUY_FREE_FLAG					");
		oSqlBuilder.AppendLine(" FROM														");
		oSqlBuilder.AppendLine("			T_LOTTERY_NOT_COMP								");
		oSqlBuilder.AppendLine(" WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD	    AND	");
		oSqlBuilder.AppendLine("	SEX_CD				= :SEX_CD	    AND	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ			= :LOTTERY_SEQ	AND	");
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));
		oSqlBuilder.AppendLine("	PUBLISH_END_DATE	>	SYSDATE			");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
				
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendFirstBuyFreeFlag(oDataSet,pCondition);

		return oDataSet;
	}
	
	public void AppendItemCount(DataSet pDS,LotterySeekCondition pCondition) {
		if(pDS.Tables[0].Rows.Count < 1) {
			return;
		}
		
		DataRow pDR = pDS.Tables[0].Rows[0];

		DataColumn col;
		col = new DataColumn(string.Format("LOTTERY_ITEM_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("REST_LOTTERY_COMPLETE_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)	AS LOTTERY_ITEM_COUNT	");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM					");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ		= :LOTTERY_SEQ		");
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pDR["LOTTERY_SEQ"]));

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		DataRow subDR = subDS.Tables[0].Rows[0];
		pDR["LOTTERY_ITEM_COUNT"] = subDR["LOTTERY_ITEM_COUNT"];

		oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	NVL(COUNT(1),0)		AS REST_LOTTERY_COMPLETE_COUNT		");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM										");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ	= :LOTTERY_SEQ							AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS												");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			1												");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			T_LOTTERY_ITEM_GET_LOG							");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD									= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ								= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO							= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			LOTTERY_SEQ								= :LOTTERY_SEQ							AND	");
		oSqlBuilder.AppendLine("			T_LOTTERY_ITEM_GET_LOG.GAME_ITEM_SEQ	= T_LOTTERY_GET_ITEM.GAME_ITEM_SEQ			");
		oSqlBuilder.AppendLine("	)											");

		oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pDR["LOTTERY_SEQ"]));

		subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		subDR = subDS.Tables[0].Rows[0];

		pDR["REST_LOTTERY_COMPLETE_COUNT"] = subDR["REST_LOTTERY_COMPLETE_COUNT"];
	}

	public void AppendFirstBuyFreeFlag(DataSet pDS,LotterySeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count < 1) {
			return;
		}

		DataRow pDR = pDS.Tables[0].Rows[0];

		DataColumn col;
		col = new DataColumn(string.Format("FIRST_BUY_FREE_FLAG"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();	

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	COUNT(*)	AS SET_LOTTERY_NOT_COMP_LOG_COUNT	");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_SET_LOTTERY_NOT_COMP_LOG						");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND		");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ			AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO		AND		");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ			= :LOTTERY_SEQ		AND		");
		oSqlBuilder.AppendLine("	LOTTERY_SET_FLAG	= :LOTTERY_SET_FLAG			");
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pDR["LOTTERY_SEQ"]));
		oParamList.Add(new OracleParameter(":LOTTERY_SET_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		DataRow subDR = subDS.Tables[0].Rows[0];

		if (int.Parse(subDR["SET_LOTTERY_NOT_COMP_LOG_COUNT"].ToString()) > 0 ) {
			pDR["FIRST_BUY_FREE_FLAG"] = ViCommConst.FLAG_OFF_STR;	
		}else {
			pDR["FIRST_BUY_FREE_FLAG"] = ViCommConst.FLAG_ON_STR;	
		}
		
	}
	
	public DataSet GetOneCompUserData(LotterySeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT			");
		oSqlBuilder.AppendLine("	*			");
		oSqlBuilder.AppendLine("FROM			");
		oSqlBuilder.AppendLine("	(			");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			USER_SEQ										,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("			COMPLETE_DATE		AS LOTTERY_COMPLETE_DATE	,	");
		oSqlBuilder.AppendLine("			GAME_HANDLE_NM									,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_TYPE								,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_LEVEL							,	");
		oSqlBuilder.AppendLine("			PHOTO_IMG_PATH									,	");
		oSqlBuilder.AppendLine("			SMALL_PHOTO_IMG_PATH								");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			VW_PW_LOTTERY_COMPLETE_LOG01						");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			SEX_CD			= :SEX_CD						AND	");
		oSqlBuilder.AppendLine("			LOTTERY_SEQ		= :LOTTERY_SEQ					AND	");
		oSqlBuilder.AppendLine("			(NA_FLAG		IN (:NA_FLAG,:NA_FLAG_2))		AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		oSqlBuilder.AppendLine("		ORDER BY COMPLETE_DATE DESC							");
		oSqlBuilder.AppendLine("	)			");
		oSqlBuilder.AppendLine("WHERE ROWNUM = 1");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public void GetLottery(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		string pLotterySeq,
		out string pGetGameItemSeq,
		out string pCompleteGetItemSeq,
		out int pLotteryCompleteFlag,
		out int pLotteryCompLeftCount,
		out string pResult
		) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_LOTTERY");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pLOTTERY_SEQ",DbSession.DbType.VARCHAR2,pLotterySeq);
			db.ProcedureOutParm("pGET_GAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCOMPLETE_GET_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLOTTERY_COMPLETE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pLOTTERY_COMP_LEFT_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pGetGameItemSeq = db.GetStringValue("pGET_GAME_ITEM_SEQ");
			pCompleteGetItemSeq = db.GetStringValue("pCOMPLETE_GET_ITEM_SEQ");
			pLotteryCompleteFlag = db.GetIntValue("pLOTTERY_COMPLETE_FLAG");
			pLotteryCompLeftCount = db.GetIntValue("pLOTTERY_COMP_LEFT_COUNT");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void GetLotteryNotComp(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		int pLotterySeq,
		int pLotteryPattern,
		out int pSetLotteryItemGetLogSeq,
		out int pFirstBuyFreeFlag,
		out string pResult
		) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_LOTTERY_NOT_COMP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pLOTTERY_SEQ",DbSession.DbType.NUMBER,pLotterySeq);
			db.ProcedureInParm("pLOTTERY_PATTERN",DbSession.DbType.NUMBER,pLotteryPattern);
			db.ProcedureOutParm("pSET_LOTTERY_ITEM_GET_LOG_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pFIRST_BUY_FREE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pSetLotteryItemGetLogSeq = db.GetIntValue("pSET_LOTTERY_ITEM_GET_LOG_SEQ");
			pFirstBuyFreeFlag = db.GetIntValue("pFIRST_BUY_FREE_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	/*public string GetLotteryFreeFlag(string sSiteCd,string sUserSeq,string sUserCharNo,string sSexCd,string sLotterySeq) {
		string sLotteryFreeFlag = "0";
		
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	CASE														");
		oSqlBuilder.AppendLine("		WHEN COUNT(*) > 0 THEN 1								");
		oSqlBuilder.AppendLine("		ELSE 0													");
		oSqlBuilder.AppendLine("	END AS LOTTERY_FREE_FLAG									");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_LOTTERY LOTTERY											");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	NOT EXISTS													");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			*													");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_LOTTERY_ITEM_GET_LOG LOG							");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			LOG.SITE_CD			= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			LOG.USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			LOG.USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			LOG.LOTTERY_SEQ		= :LOTTERY_SEQ				AND	");
		oSqlBuilder.AppendLine("			LOG.SITE_CD			= LOTTERY.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			LOG.LOTTERY_SEQ		= LOTTERY.LOTTERY_SEQ			");
		oSqlBuilder.AppendLine("	)	AND														");
		oSqlBuilder.AppendLine("	SITE_CD						= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ					= :LOTTERY_SEQ				AND	");
		oSqlBuilder.AppendLine("	SEX_CD						= :SEX_CD					AND	");
		oSqlBuilder.AppendLine("	FIRST_BUY_FREE_START_DATE	<= SYSDATE					AND	");
		oSqlBuilder.AppendLine("	FIRST_BUY_FREE_END_DATE		>= SYSDATE					AND	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG				= :PUBLISH_FLAG					");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",sSexCd));
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",sLotterySeq));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			sLotteryFreeFlag = oDataSet.Tables[0].Rows[0]["LOTTERY_FREE_FLAG"].ToString();
		}

		return sLotteryFreeFlag;
	}*/

	public string GetLotteryNotCompFreeFlag(string sSiteCd,string sUserSeq,string sUserCharNo,string sSexCd,string sLotterySeq) {
		string sLotteryFreeFlag = "0";

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	CASE														");
		oSqlBuilder.AppendLine("		WHEN COUNT(*) > 0 THEN 1								");
		oSqlBuilder.AppendLine("		ELSE 0													");
		oSqlBuilder.AppendLine("	END AS LOTTERY_FREE_FLAG									");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_LOTTERY_NOT_COMP LOTTERY									");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	NOT EXISTS													");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			*													");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_SET_LOTTERY_NOT_COMP_LOG LOG						");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			LOG.SITE_CD			 = :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			LOG.USER_SEQ		 = :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			LOG.USER_CHAR_NO	 = :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			LOG.LOTTERY_SEQ		 = :LOTTERY_SEQ				AND	");
		oSqlBuilder.AppendLine("			LOG.LOTTERY_SET_FLAG = :LOTTERY_SET_FLAG		AND	");
		oSqlBuilder.AppendLine("			LOG.SITE_CD			 = LOTTERY.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			LOG.LOTTERY_SEQ		 = LOTTERY.LOTTERY_SEQ			");
		oSqlBuilder.AppendLine("	)	AND														");
		oSqlBuilder.AppendLine("	SITE_CD						 = :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ					 = :LOTTERY_SEQ				AND	");
		oSqlBuilder.AppendLine("	SEX_CD						 = :SEX_CD					AND	");
		oSqlBuilder.AppendLine("	FIRST_BUY_FREE_START_DATE	 <= SYSDATE					AND	");
		oSqlBuilder.AppendLine("	FIRST_BUY_FREE_END_DATE		 >= SYSDATE						");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",sSexCd));
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",sLotterySeq));
		oParamList.Add(new OracleParameter(":LOTTERY_SET_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sLotteryFreeFlag = oDataSet.Tables[0].Rows[0]["LOTTERY_FREE_FLAG"].ToString();
		}

		return sLotteryFreeFlag;
	}
}
