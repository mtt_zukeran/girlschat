﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: プロフアクセスログ登録
--	Progaram ID		: ProfAccessLog
--
--  Creation Date	: 2015.09.30
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class ProfAccessLog:DbSession {
	public ProfAccessLog()
		: base() {
	}

	public void RegistProfAccessLog(string pSiteCd,string pUserSeq,string pCastUserSeq,string pCastCharNo,string pRefererAspxNm) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_PROF_ACCESS_LOG");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pREFERER_ASPX_NM",DbSession.DbType.VARCHAR2,pRefererAspxNm);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}