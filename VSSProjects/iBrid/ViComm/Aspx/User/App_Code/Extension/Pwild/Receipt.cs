﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 入金記録--	Progaram ID		: Receipt
--  Creation Date	: 2015.07.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using iBridCommLib;

public class Receipt:DbSession {
	public Receipt() {
	}

	public string GetLastSettleType(string pSiteCd,string pUserSeq) {
		string sSettleType = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SETTLE_TYPE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		SETTLE_TYPE");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_RECEIPT");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("		USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("		SETTLE_TYPE != 'N'");
		oSqlBuilder.AppendLine("	ORDER BY CREATE_DATE DESC)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM = 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			sSettleType = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["SETTLE_TYPE"]);
		}

		return sSettleType;
	}

	/// <summary>
	/// 指定日時以降の課金額を取得する
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <returns></returns>
	public int GetReceiptAmtFromCreateDate(string pSiteCd,string pUserSeq,string pCreateDate) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		int iReceiptAmt = 0;

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	NVL(SUM(RECEIPT_AMT),0) AS RECEIPT_AMT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_RECEIPT");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("	AND USER_SEQ = :USER_SEQ");
		oSqlBuilder.AppendLine("	AND CREATE_DATE > TO_DATE(:CREATE_DATE, 'YYYY-MM-DD HH24:MI:SS')");
		oSqlBuilder.AppendLine("	AND RECEIPT_CANCEL_FLAG = 0");
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	SITE_CD, USER_SEQ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":CREATE_DATE",pCreateDate));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			int.TryParse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["RECEIPT_AMT"]),out iReceiptAmt);
		}

		return iReceiptAmt;
	}
}
