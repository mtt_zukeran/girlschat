﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 出演者登録後トップ
--	Progaram ID		: CastUserTop
--  Creation Date	: 2015.05.12
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class CastUserTopSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string CastUserSeq;
	public string CastCharNo;

	public CastUserTopSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastUserTopSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class CastUserTop:DbSession {
	public CastUserTop() {
	}

	public DataSet GetDataSet(CastUserTopSeekCondition pCondition) {
		DataSet ds = new DataSet();
		ds.Tables.Add(new DataTable());
		ds.Tables[0].Rows.Add();

		AppendLatestAdminReport(ds,pCondition);
		AppendSelfRecentLoginPlan(ds,pCondition);
		AppendSelfRecentWaitSchedule(ds,pCondition);
		AppendSelfMailRequestCount(ds,pCondition);
		AppendSelfFavoritNews(ds,pCondition);
		AppendQuickResponseRoomInCount(ds,pCondition);
		AppendManLoginedCount(ds,pCondition);
		AppendManOnlineCount(ds,pCondition);
		AppendSelfManMailUnreadCount(ds,pCondition);

		return ds;
	}

	private void AppendLatestAdminReport(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("LATEST_ADMIN_REPORT_TITLE",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd)) {
			return;
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	INNER.*");
		oSql.AppendLine("FROM (");
		oSql.AppendLine("	SELECT");
		oSql.AppendLine("		DOC_TITLE");
		oSql.AppendLine("	FROM");
		oSql.AppendLine("		T_ADMIN_REPORT");
		oSql.AppendLine("	WHERE");
		oSql.AppendLine("		SITE_CD = :SITE_CD AND");
		oSql.AppendLine("		SEX_CD = :SEX_CD AND");
		oSql.AppendLine("		TO_DATE(START_PUB_DAY||' '||REPORT_HOUR_MIN,'YYYY/MM/DD HH24:MI') < SYSDATE");
		oSql.AppendLine(" ORDER BY START_PUB_DAY DESC, REPORT_HOUR_MIN DESC");
		oSql.AppendLine("	) INNER");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	ROWNUM <= 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",ViCommConst.OPERATOR));

		DataSet ds = ExecuteSelectQueryBase(oSql,oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			pDS.Tables[0].Rows[0]["LATEST_ADMIN_REPORT_TITLE"] = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["DOC_TITLE"]);
		}
	}

	private void AppendSelfRecentLoginPlan(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("SELF_RECENT_LOGIN_PLAN_START_TIME",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		string sMinLoginStartTime;

		if (DateTime.Now.Minute < 30) {
			sMinLoginStartTime = DateTime.Now.ToString("yyyy/MM/dd HH:00");
		} else {
			sMinLoginStartTime = DateTime.Now.ToString("yyyy/MM/dd HH:30");
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	INNER.*");
		oSql.AppendLine("FROM (");
		oSql.AppendLine("	SELECT");
		oSql.AppendLine("		LOGIN_START_TIME");
		oSql.AppendLine("	FROM");
		oSql.AppendLine("		T_LOGIN_PLAN");
		oSql.AppendLine("	WHERE");
		oSql.AppendLine("		SITE_CD = :SITE_CD AND");
		oSql.AppendLine("		USER_SEQ = :USER_SEQ AND");
		oSql.AppendLine("		USER_CHAR_NO = :USER_CHAR_NO AND");
		oSql.AppendLine("		LOGIN_START_TIME >= :MIN_LOGIN_START_TIME");
		oSql.AppendLine("	ORDER BY LOGIN_START_TIME ASC");
		oSql.AppendLine("	) INNER");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	ROWNUM <= 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.CastCharNo));
		oParamList.Add(new OracleParameter(":MIN_LOGIN_START_TIME",DateTime.Parse(sMinLoginStartTime)));

		DataSet ds = ExecuteSelectQueryBase(oSql,oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			pDS.Tables[0].Rows[0]["SELF_RECENT_LOGIN_PLAN_START_TIME"] = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["LOGIN_START_TIME"]);
		}
	}

	private void AppendSelfRecentWaitSchedule(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("SELF_RECENT_WAIT_SCHEDULE_START_TIME",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		string sMinWaitStartTime;

		if (DateTime.Now.Minute < 30) {
			sMinWaitStartTime = DateTime.Now.ToString("yyyy/MM/dd HH:00");
		} else {
			sMinWaitStartTime = DateTime.Now.ToString("yyyy/MM/dd HH:30");
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	INNER.*");
		oSql.AppendLine("FROM (");
		oSql.AppendLine("	SELECT");
		oSql.AppendLine("		WAIT_START_TIME");
		oSql.AppendLine("	FROM");
		oSql.AppendLine("		T_WAIT_SCHEDULE");
		oSql.AppendLine("	WHERE");
		oSql.AppendLine("		SITE_CD = :SITE_CD AND");
		oSql.AppendLine("		USER_SEQ = :USER_SEQ AND");
		oSql.AppendLine("		USER_CHAR_NO = :USER_CHAR_NO AND");
		oSql.AppendLine("		WAIT_START_TIME >= :MIN_WAIT_START_TIME");
		oSql.AppendLine("	ORDER BY WAIT_START_TIME ASC");
		oSql.AppendLine("	) INNER");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	ROWNUM <= 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.CastCharNo));
		oParamList.Add(new OracleParameter(":MIN_WAIT_START_TIME",DateTime.Parse(sMinWaitStartTime)));

		DataSet ds = ExecuteSelectQueryBase(oSql,oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			pDS.Tables[0].Rows[0]["SELF_RECENT_WAIT_SCHEDULE_START_TIME"] = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["WAIT_START_TIME"]);
		}
	}

	private void AppendSelfMailRequestCount(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("SELF_MAIL_REQUEST_COUNT",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	COUNT(*)");
		oSql.AppendLine("FROM");
		oSql.AppendLine("	(SELECT");
		oSql.AppendLine("		SITE_CD,");
		oSql.AppendLine("		USER_SEQ,");
		oSql.AppendLine("		CAST_USER_SEQ,");
		oSql.AppendLine("		CAST_CHAR_NO");
		oSql.AppendLine("	FROM");
		oSql.AppendLine("		T_MAIL_REQUEST_LOG");
		oSql.AppendLine("	WHERE");
		oSql.AppendLine("		SITE_CD = :SITE_CD AND");
		oSql.AppendLine("		CAST_USER_SEQ = :CAST_USER_SEQ AND");
		oSql.AppendLine("		CAST_CHAR_NO = :CAST_CHAR_NO AND");
		oSql.AppendLine("		NOT_DISPLAY_FLAG = 0");
		oSql.AppendLine("	) P");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	NOT EXISTS(SELECT 1 FROM T_USER_MAN_CHARACTER WHERE");
		oSql.AppendLine("		SITE_CD = P.SITE_CD AND");
		oSql.AppendLine("		USER_SEQ = P.USER_SEQ AND");
		oSql.AppendLine("		USER_CHAR_NO = '00' AND");
		oSql.AppendLine("		NA_FLAG != 0) AND");
		oSql.AppendLine("	NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE");
		oSql.AppendLine("		SITE_CD = P.SITE_CD AND");
		oSql.AppendLine("		USER_SEQ = P.USER_SEQ AND");
		oSql.AppendLine("		USER_CHAR_NO = '00' AND");
		oSql.AppendLine("		PARTNER_USER_SEQ = P.CAST_USER_SEQ AND");
		oSql.AppendLine("		PARTNER_USER_CHAR_NO = P.CAST_CHAR_NO) AND");
		oSql.AppendLine("	NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE");
		oSql.AppendLine("		SITE_CD = P.SITE_CD AND");
		oSql.AppendLine("		USER_SEQ = P.CAST_USER_SEQ AND");
		oSql.AppendLine("		USER_CHAR_NO = P.CAST_CHAR_NO AND");
		oSql.AppendLine("		PARTNER_USER_SEQ = P.USER_SEQ AND");
		oSql.AppendLine("		PARTNER_USER_CHAR_NO = '00')");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));

		int iRecCount = ExecuteSelectCountQueryBase(oSql,oParamList.ToArray());

		pDS.Tables[0].Rows[0]["SELF_MAIL_REQUEST_COUNT"] = iRecCount.ToString();
	}

	private void AppendSelfFavoritNews(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS01_DATE_FORMAT",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS01_URL",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS01_TEXT",Type.GetType("System.String"));

		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS02_DATE_FORMAT",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS02_URL",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS02_TEXT",Type.GetType("System.String"));

		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS03_DATE_FORMAT",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS03_URL",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS03_TEXT",Type.GetType("System.String"));

		pDS.Tables[0].Columns.Add("SELF_FAVORIT_NEWS04_DATE_FORMAT",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	ROWNUM,");
		oSql.AppendLine("	INNER.*");
		oSql.AppendLine("FROM (");

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	FAVORIT.SITE_CD,");
		oSql.AppendLine("	FAVORIT.PARTNER_USER_SEQ AS USER_SEQ,");
		oSql.AppendLine("	FAVORIT.PARTNER_USER_CHAR_NO AS USER_CHAR_NO,");
		oSql.AppendLine("	T_MAN_NEWS.NEWS_TYPE,");
		oSql.AppendLine("	T_MAN_NEWS.NEWS_DATE,");
		oSql.AppendLine("	T_USER_MAN_CHARACTER.HANDLE_NM,");
		oSql.AppendLine("	T_USER.LOGIN_ID");
		oSql.AppendLine("FROM");
		oSql.AppendLine("	(SELECT");
		oSql.AppendLine("		SITE_CD,");
		oSql.AppendLine("		PARTNER_USER_SEQ,");
		oSql.AppendLine("		PARTNER_USER_CHAR_NO");
		oSql.AppendLine("	FROM");
		oSql.AppendLine("		T_FAVORIT");
		oSql.AppendLine("	WHERE");
		oSql.AppendLine("		SITE_CD = :SITE_CD AND");
		oSql.AppendLine("		USER_SEQ = :CAST_USER_SEQ AND");
		oSql.AppendLine("		USER_CHAR_NO = :CAST_CHAR_NO AND");
		oSql.AppendLine("		NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE");
		oSql.AppendLine("			SITE_CD = T_FAVORIT.SITE_CD AND");
		oSql.AppendLine("			USER_SEQ = T_FAVORIT.PARTNER_USER_SEQ AND");
		oSql.AppendLine("			USER_CHAR_NO = T_FAVORIT.PARTNER_USER_CHAR_NO AND");
		oSql.AppendLine("			PARTNER_USER_SEQ = T_FAVORIT.USER_SEQ AND");
		oSql.AppendLine("			PARTNER_USER_CHAR_NO = T_FAVORIT.USER_CHAR_NO)");
		oSql.AppendLine("	) FAVORIT");
		oSql.AppendLine("	INNER JOIN T_USER_MAN_CHARACTER");
		oSql.AppendLine("		ON (FAVORIT.SITE_CD = T_USER_MAN_CHARACTER.SITE_CD AND");
		oSql.AppendLine("			FAVORIT.PARTNER_USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ AND");
		oSql.AppendLine("			FAVORIT.PARTNER_USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO AND");
		oSql.AppendLine("			0 = T_USER_MAN_CHARACTER.NA_FLAG)");
		oSql.AppendLine("	INNER JOIN T_USER");
		oSql.AppendLine("		ON (FAVORIT.PARTNER_USER_SEQ = T_USER.USER_SEQ)");
		oSql.AppendLine("	INNER JOIN T_MAN_NEWS");
		oSql.AppendLine("		ON (FAVORIT.SITE_CD = T_MAN_NEWS.SITE_CD AND");
		oSql.AppendLine("			FAVORIT.PARTNER_USER_SEQ = T_MAN_NEWS.MAN_USER_SEQ AND");
		oSql.AppendLine("			FAVORIT.PARTNER_USER_CHAR_NO = '00')");
		oSql.AppendLine("ORDER BY T_MAN_NEWS.NEWS_DATE DESC");

		oSql.AppendLine("	) INNER");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	ROWNUM <= 4");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));

		DataSet ds = ExecuteSelectQueryBase(oSql,oParamList.ToArray());

		for (int i = 1;i <= ds.Tables[0].Rows.Count;i++) {
			DataRow dr = ds.Tables[0].Rows[i-1];
			DateTime dtNewsDate;
			int iNewsType;

			if (DateTime.TryParse(iBridUtil.GetStringValue(dr["NEWS_DATE"]),out dtNewsDate)) {
				if (pDS.Tables[0].Columns.Contains(string.Format("SELF_FAVORIT_NEWS{0:D2}_DATE_FORMAT",i))) {
					if (dtNewsDate.ToString("yyyy/MM/dd").Equals(DateTime.Now.ToString("yyyy/MM/dd"))) {
						pDS.Tables[0].Rows[0][string.Format("SELF_FAVORIT_NEWS{0:D2}_DATE_FORMAT",i)] = dtNewsDate.ToString("HH:mm");
					} else {
						pDS.Tables[0].Rows[0][string.Format("SELF_FAVORIT_NEWS{0:D2}_DATE_FORMAT",i)] = dtNewsDate.ToString("M/d");
					}
				}
			}

			if (int.TryParse(iBridUtil.GetStringValue(dr["NEWS_TYPE"]),out iNewsType)) {
				if (pDS.Tables[0].Columns.Contains(string.Format("SELF_FAVORIT_NEWS{0:D2}_URL",i))) {
					if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(dr["LOGIN_ID"])) && !string.IsNullOrEmpty(iBridUtil.GetStringValue(dr["USER_SEQ"]))) {
						pDS.Tables[0].Rows[0][string.Format("SELF_FAVORIT_NEWS{0:D2}_URL",i)] = this.GetManNewsUrl(iNewsType,iBridUtil.GetStringValue(dr["LOGIN_ID"]),iBridUtil.GetStringValue(dr["USER_SEQ"]));
					}
				}

				if (pDS.Tables[0].Columns.Contains(string.Format("SELF_FAVORIT_NEWS{0:D2}_TEXT",i))) {
					if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(dr["HANDLE_NM"]))) {
						pDS.Tables[0].Rows[0][string.Format("SELF_FAVORIT_NEWS{0:D2}_TEXT",i)] = this.GetManNewsText(iNewsType,iBridUtil.GetStringValue(dr["HANDLE_NM"]));
					}
				}
			}
		}
	}

	private string GetManNewsUrl(int pNewsType,string pLoginId,string pUserSeq) {
		string sUrl = string.Empty;

		switch (pNewsType) {
			case PwViCommConst.ManNewsType.LOGIN:
			case PwViCommConst.ManNewsType.START_WAITING:
			case PwViCommConst.ManNewsType.START_TEL:
			case PwViCommConst.ManNewsType.UPDATE_PROFILE:
			case PwViCommConst.ManNewsType.UPDATE_PROFILE_PIC:
				sUrl = string.Format("Profile.aspx?loginid={0}&direct=1",pLoginId);
				break;
			case PwViCommConst.ManNewsType.WRITE_BBS:
				sUrl = string.Format("ListUserBbs.aspx?loginid={0}",pLoginId);
				break;
			case PwViCommConst.ManNewsType.WRITE_MAN_TWEET:
				sUrl = string.Format("ListPersonalManTweet.aspx?userseq={0}&usercharno=00",pUserSeq);
				break;
			default:
				break;
		}

		return sUrl;
	}

	private string GetManNewsText(int pNewsType,string pHandleNm) {
		string sText = string.Empty;

		switch (pNewsType) {
			case PwViCommConst.ManNewsType.LOGIN:
				sText = string.Format("<font color=\"#115fff\">{0}</font><font color=\"#252525\">ｻﾝがﾛｸﾞｲﾝしました</font>",pHandleNm);
				break;
			case PwViCommConst.ManNewsType.START_WAITING:
				sText = string.Format("<font color=\"#115fff\">{0}</font><font color=\"#252525\">ｻﾝが待機中になりました</font>",pHandleNm);
				break;
			case PwViCommConst.ManNewsType.START_TEL:
				sText = string.Format("<font color=\"#115fff\">{0}</font><font color=\"#252525\">ｻﾝが通話中になりました</font>",pHandleNm);
				break;
			case PwViCommConst.ManNewsType.UPDATE_PROFILE:
				sText = string.Format("<font color=\"#115fff\">{0}</font><font color=\"#252525\">ｻﾝがﾌﾟﾛﾌｨｰﾙを更新しました</font>",pHandleNm);
				break;
			case PwViCommConst.ManNewsType.UPDATE_PROFILE_PIC:
				sText = string.Format("<font color=\"#115fff\">{0}</font><font color=\"#252525\">ｻﾝがﾌﾟﾛﾌ画像を更新しました</font>",pHandleNm);
				break;
			case PwViCommConst.ManNewsType.WRITE_BBS:
				sText = string.Format("<font color=\"#115fff\">{0}</font><font color=\"#252525\">ｻﾝが掲示板に書き込みました</font>",pHandleNm);
				break;
			case PwViCommConst.ManNewsType.WRITE_MAN_TWEET:
				sText = string.Format("<font color=\"#115fff\">{0}</font><font color=\"#252525\">ｻﾝがつぶやきを投稿しました</font>",pHandleNm);
				break;
			default:
				break;
		}

		return sText;
	}

	private void AppendQuickResponseRoomInCount(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("QUICK_RESPONSE_ROOM_IN_COUNT",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd)) {
			return;
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	COUNT(*)");
		oSql.AppendLine("FROM");
		oSql.AppendLine("	T_CAST_CHARACTER");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	SITE_CD = :SITE_CD AND");
		oSql.AppendLine("	QUICK_RES_FLAG = 1 AND");
		oSql.AppendLine("	QUICK_RES_IN_TIME <= SYSDATE AND");
		oSql.AppendLine("	QUICK_RES_OUT_SCH_TIME >= SYSDATE AND");
		oSql.AppendLine("	NA_FLAG = 0");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		int iRecCount = ExecuteSelectCountQueryBase(oSql,oParamList.ToArray());

		pDS.Tables[0].Rows[0]["QUICK_RESPONSE_ROOM_IN_COUNT"] = iRecCount.ToString();
	}

	private void AppendManLoginedCount(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("MAN_LOGINED_COUNT",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	COUNT(*)");
		oSql.AppendLine("FROM");
		oSql.AppendLine("	T_USER_MAN_CHARACTER");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	SITE_CD = :SITE_CD AND");
		oSql.AppendLine("	CHARACTER_ONLINE_STATUS IN (1,2,3) AND");
		oSql.AppendLine("	NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE");
		oSql.AppendLine("		T_REFUSE.SITE_CD = T_USER_MAN_CHARACTER.SITE_CD AND");
		oSql.AppendLine("		T_REFUSE.USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ AND");
		oSql.AppendLine("		T_REFUSE.USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO AND");
		oSql.AppendLine("		T_REFUSE.PARTNER_USER_SEQ = :CAST_USER_SEQ AND");
		oSql.AppendLine("		T_REFUSE.PARTNER_USER_CHAR_NO = :CAST_CHAR_NO) AND");
		oSql.AppendLine("	NA_FLAG = 0");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));

		int iRecCount = ExecuteSelectCountQueryBase(oSql,oParamList.ToArray());

		pDS.Tables[0].Rows[0]["MAN_LOGINED_COUNT"] = iRecCount.ToString();
	}

	private void AppendManOnlineCount(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("MAN_ONLINE_COUNT",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	COUNT(*)");
		oSql.AppendLine("FROM");
		oSql.AppendLine("	T_USER_MAN_CHARACTER");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	SITE_CD = :SITE_CD AND");
		oSql.AppendLine("	CHARACTER_ONLINE_STATUS IN (2,3) AND");
		oSql.AppendLine("	NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE");
		oSql.AppendLine("		T_REFUSE.SITE_CD = T_USER_MAN_CHARACTER.SITE_CD AND");
		oSql.AppendLine("		T_REFUSE.USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ AND");
		oSql.AppendLine("		T_REFUSE.USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO AND");
		oSql.AppendLine("		T_REFUSE.PARTNER_USER_SEQ = :CAST_USER_SEQ AND");
		oSql.AppendLine("		T_REFUSE.PARTNER_USER_CHAR_NO = :CAST_CHAR_NO) AND");
		oSql.AppendLine("	NA_FLAG = 0");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));

		int iRecCount = ExecuteSelectCountQueryBase(oSql,oParamList.ToArray());

		pDS.Tables[0].Rows[0]["MAN_ONLINE_COUNT"] = iRecCount.ToString();
	}

	private void AppendSelfManMailUnreadCount(DataSet pDS,CastUserTopSeekCondition pCondition) {
		pDS.Tables[0].Columns.Add("SELF_MAN_MAIL_UNREAD_COUNT",Type.GetType("System.String"));

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT");
		oSql.AppendLine("	COUNT(*)");
		oSql.AppendLine("FROM");
		oSql.AppendLine("	T_MAIL_LOG");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	RX_SITE_CD = :SITE_CD AND");
		oSql.AppendLine("	RX_USER_SEQ = :CAST_USER_SEQ AND");
		oSql.AppendLine("	RX_USER_CHAR_NO = :CAST_CHAR_NO AND");
		oSql.AppendLine("	RX_MAIL_BOX_TYPE = '1' AND");
		oSql.AppendLine("	WAIT_TX_FLAG = 0 AND");
		oSql.AppendLine("	RX_DEL_FLAG = 0 AND");
		oSql.AppendLine("	READ_FLAG = 0");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));

		int iRecCount = ExecuteSelectCountQueryBase(oSql,oParamList.ToArray());

		pDS.Tables[0].Rows[0]["SELF_MAN_MAIL_UNREAD_COUNT"] = iRecCount.ToString();
	}
}
