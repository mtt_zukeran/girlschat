﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 野球拳ジャンケン画像
--	Progaram ID		: YakyukenJyankenPic
--
--  Creation Date	: 2013.04.30
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class YakyukenJyankenPicSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string CastUserSeq;
	public string CastCharNo;

	public string AuthFlag {
		get {
			return this.Query["auth"];
		}
		set {
			this.Query["auth"] = value;
		}
	}

	public string JyakenType {
		get {
			return this.Query["jtype"];
		}
		set {
			this.Query["jtype"] = value;
		}
	}

	public YakyukenJyankenPicSeekCondition()
		: this(new NameValueCollection()) {
	}

	public YakyukenJyankenPicSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["jtype"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["jtype"]));
		this.Query["auth"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auth"]));
	}
}

public class YakyukenJyankenPic:DbSession {
	public YakyukenJyankenPic() {
	}

	public DataSet GetList(YakyukenJyankenPicSeekCondition pCondtion) {
		DataSet ds = CreateInitData(pCondtion);
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	JYANKEN_TYPE,");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	AUTH_FLAG,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	AUTH_DATE,");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_JYANKEN_PIC01");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet dsTmp = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		foreach (DataRow drTmp in dsTmp.Tables[0].Rows) {
			DataRow dr = ds.Tables[0].Rows.Find(drTmp["JYANKEN_TYPE"]);
			dr["SITE_CD"] = drTmp["SITE_CD"];
			dr["CAST_USER_SEQ"] = drTmp["CAST_USER_SEQ"];
			dr["CAST_CHAR_NO"] = drTmp["CAST_CHAR_NO"];
			dr["PIC_SEQ"] = drTmp["PIC_SEQ"];
			dr["AUTH_FLAG"] = drTmp["AUTH_FLAG"];
			dr["CREATE_DATE"] = drTmp["CREATE_DATE"];
			dr["AUTH_DATE"] = drTmp["AUTH_DATE"];
			dr["PHOTO_IMG_PATH"] = drTmp["PHOTO_IMG_PATH"];
			dr["SMALL_PHOTO_IMG_PATH"] = drTmp["SMALL_PHOTO_IMG_PATH"];
		}

		return ds;
	}

	private DataSet CreateInitData(YakyukenJyankenPicSeekCondition pCondtion) {
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();

		dt.Columns.Add("SITE_CD",Type.GetType("System.String"));
		dt.Columns.Add("CAST_USER_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("CAST_CHAR_NO",Type.GetType("System.String"));
		dt.Columns.Add("JYANKEN_TYPE",Type.GetType("System.String"));
		dt.Columns.Add("PIC_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("AUTH_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("CREATE_DATE",Type.GetType("System.String"));
		dt.Columns.Add("AUTH_DATE",Type.GetType("System.String"));
		dt.Columns.Add("PHOTO_IMG_PATH",Type.GetType("System.String"));
		dt.Columns.Add("SMALL_PHOTO_IMG_PATH",Type.GetType("System.String"));
		ds.Tables.Add(dt);

		if (!string.IsNullOrEmpty(pCondtion.JyakenType)) {
			DataRow dr = ds.Tables[0].Rows.Add();
			dr["JYANKEN_TYPE"] = pCondtion.JyakenType;
			dr["SITE_CD"] = pCondtion.SiteCd;
			dr["CAST_USER_SEQ"] = pCondtion.CastUserSeq;
			dr["CAST_CHAR_NO"] = pCondtion.CastCharNo;
		} else {
			for (int i = 1;i <= 3;i++) {
				DataRow dr = ds.Tables[0].Rows.Add();
				dr["JYANKEN_TYPE"] = i.ToString();
				dr["SITE_CD"] = pCondtion.SiteCd;
				dr["CAST_USER_SEQ"] = pCondtion.CastUserSeq;
				dr["CAST_CHAR_NO"] = pCondtion.CastCharNo;
			}
		}

		ds.Tables[0].PrimaryKey = new DataColumn[] { dt.Columns["JYANKEN_TYPE"] };

		return ds;
	}

	private OracleParameter[] CreateWhere(YakyukenJyankenPicSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.JyakenType)) {
			SysPrograms.SqlAppendWhere("JYANKEN_TYPE = :JYANKEN_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":JYANKEN_TYPE",pCondition.JyakenType));
		}

		if (!string.IsNullOrEmpty(pCondition.AuthFlag)) {
			SysPrograms.SqlAppendWhere("AUTH_FLAG = :AUTH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AUTH_FLAG",pCondition.AuthFlag));
		}

		return oParamList.ToArray();
	}
}
