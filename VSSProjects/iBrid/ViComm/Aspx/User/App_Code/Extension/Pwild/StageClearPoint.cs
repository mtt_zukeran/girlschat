﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｽﾃｰｼﾞｸﾘｱﾎﾟｲﾝﾄ
--	Progaram ID		: StageClearPoint
--
--  Creation Date	: 2012.02.09
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class StageClearPoint:DbSession {

	public DataSet GetStageClearPoint(string pSiteCd,string SexCd) {

		StringBuilder oSqlBuilder = new StringBuilder();

		if (SexCd == ViCommConst.MAN)
		{
			oSqlBuilder.AppendLine("SELECT														");
			oSqlBuilder.AppendLine("	MAN_CLEAR_FROM_REGIST_DAYS	,							");
			oSqlBuilder.AppendLine("	MAN_STAGE_CLEAR_POINT		,							");
			oSqlBuilder.AppendLine("	STAGE_NM												");
			oSqlBuilder.AppendLine(" FROM							   							");
			oSqlBuilder.AppendLine("	VW_PW_MAN_STAGE_CLEAR_POINT								");
			oSqlBuilder.AppendLine(" WHERE														");
			oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD									");
		
		} else {
			oSqlBuilder.AppendLine("SELECT														");
			oSqlBuilder.AppendLine("	CAST_CLEAR_FROM_REGIST_DAYS	,							");
			oSqlBuilder.AppendLine("	CAST_STAGE_CLEAR_POINT		,							");
			oSqlBuilder.AppendLine("	STAGE_NM												");
			oSqlBuilder.AppendLine(" FROM							   							");
			oSqlBuilder.AppendLine("	VW_PW_CAST_STAGE_CLEAR_POINT							");
			oSqlBuilder.AppendLine(" WHERE														");
			oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD									");
		}
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;

	}
}

