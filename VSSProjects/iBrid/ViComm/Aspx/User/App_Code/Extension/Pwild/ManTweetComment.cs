﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: アイドルテレビ・会員つぶやきコメント
--	Progaram ID		: ManTweetComment
--
--  Creation Date	: 2013.01.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class ManTweetCommentSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public string ManTweetSeq {
		get {
			return this.Query["tweetseq"];
		}
		set {
			this.Query["tweetseq"] = value;
		}
	}

	public string ManTweetCommentSeq {
		get {
			return this.Query["commentseq"];
		}
		set {
			this.Query["commentseq"] = value;
		}
	}

	public string TweetUserSeq {
		get {
			return this.Query["tweetuserseq"];
		}
		set {
			this.Query["tweetuserseq"] = value;
		}
	}

	public string TweetUserCharNo {
		get {
			return this.Query["tweetusercharno"];
		}
		set {
			this.Query["tweetusercharno"] = value;
		}
	}

	public ManTweetCommentSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTweetCommentSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["tweetseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tweetseq"]));
		this.Query["commentseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["commentseq"]));
		this.Query["tweetuserseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tweetuserseq"]));
		this.Query["tweetusercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tweetusercharno"]));
	}
}

#endregion ============================================================================================================

public class ManTweetComment:CastBase {
	public ManTweetComment()
		: base() {
	}
	public ManTweetComment(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ManTweetCommentSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_MAN_TWEET_COMMENT00 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManTweetCommentSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "					,	");
		oSqlBuilder.AppendLine("	P.MAN_TWEET_COMMENT_SEQ						,	");
		oSqlBuilder.AppendLine("	P.MAN_TWEET_SEQ								,	");
		oSqlBuilder.AppendLine("	P.COMMENT_TEXT								,	");
		oSqlBuilder.AppendLine("	P.COMMENT_DATE								,	");
		oSqlBuilder.AppendLine("	P.MAN_TWEET_USER_SEQ						,	");
		oSqlBuilder.AppendLine("	P.MAN_TWEET_USER_CHAR_NO						");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_MAN_TWEET_COMMENT00 P						");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.COMMENT_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManTweetCommentSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		ArrayList oParamList = new ArrayList();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ManTweetSeq)) {
			SysPrograms.SqlAppendWhere(" P.MAN_TWEET_SEQ		= :MAN_TWEET_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAN_TWEET_SEQ",pCondition.ManTweetSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.ManTweetCommentSeq)) {
			SysPrograms.SqlAppendWhere(" P.MAN_TWEET_COMMENT_SEQ		= :MAN_TWEET_COMMENT_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAN_TWEET_COMMENT_SEQ",pCondition.ManTweetCommentSeq));
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" P.ADMIN_DEL_FLAG		= :ADMIN_DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":ADMIN_DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.DEL_FLAG		= :DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		return (OracleParameter[])oParamList.ToArray(typeof(OracleParameter));
	}

	public void WriteManTweetComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pManTweetSeq,string pCommentText,int pTxMailFlag,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_MAN_TWEET_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,pManTweetSeq);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureInParm("pTX_MAIL_FLAG",DbSession.DbType.NUMBER,pTxMailFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void DeleteManTweetComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pManTweetCommentSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_MAN_TWEET_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMAN_TWEET_COMMENT_SEQ",DbSession.DbType.VARCHAR2,pManTweetCommentSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public decimal GetManTweetCommentCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pManTweetSeq) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	COUNT(*)								");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT						");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	MAN_TWEET_SEQ	= :MAN_TWEET_SEQ		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":MAN_TWEET_SEQ",pManTweetSeq));
		
		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}
}