﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性用お宝コンプリート時獲得アイテム


--	Progaram ID		: Access
--
--  Creation Date	: 2011.07.27
--  Creater			: iBrid
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using Oracle.DataAccess.Client;

[Serializable]
public class CompManTreasureGetItemSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	public string DisplayDay {
		get {
			return this.Query["display_day"];
		}
		set {
			this.Query["display_day"] = value;
		}
	}
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	public string GetTreasureLogSeq {
		get {
			return this.Query["get_treasure_log_seq"];
		}
		set {
			this.Query["get_treasure_log_seq"] = value;
		}
	}
	public CompManTreasureGetItemSeekCondition() : this(new NameValueCollection()) {
	}
	public CompManTreasureGetItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["display_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["display_day"]));
		this.Query["get_treasure_log_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["get_treasure_log_seq"]));
	}
}
[System.Serializable]
public class CompManTreasureGetItem:DbSession {
	public CompManTreasureGetItem() {
	}

	public int GetPageCount(CompManTreasureGetItemSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	T_COMP_MAN_TREASURE_GET_ITEM 	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}
	public DataSet GetPageCollection(CompManTreasureGetItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}
	public DataSet GetPageCollectionBase(CompManTreasureGetItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT									 ");
		oSqlBuilder.AppendLine("	SITE_CD								,");
		oSqlBuilder.AppendLine("	ITEM_COUNT							,");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ						,");
		oSqlBuilder.AppendLine("	ATTACK_POWER						,");
		oSqlBuilder.AppendLine("	DEFENCE_POWER						,");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM						,");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_GROUP_TYPE		 ");
		oSqlBuilder.AppendLine("FROM									 ");
		oSqlBuilder.AppendLine("	VW_PW_CMP_MAN_TRSR_GET_ITEM01		 ");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));


		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		return oDataSet;

	}

	public OracleParameter[] CreateWhere(CompManTreasureGetItemSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null)
			throw new ArgumentNullException();

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.DisplayDay)) {
			SysPrograms.SqlAppendWhere(" DISPLAY_DAY = :DISPLAY_DAY ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DISPLAY_DAY",pCondition.DisplayDay));
		}

		return oParamList.ToArray();
	}

	public string CreateOrderExpresion(CompManTreasureGetItemSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.SeekMode) {
			default:
				sSortExpression = " ORDER BY SITE_CD ASC ";
				break;
		}
		return sSortExpression;
	}

	public DataSet GetCompGetItemByGetTreasureLogSeq(CompManTreasureGetItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_SEQ															");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	T_COMP_MAN_TREASURE_GET_ITEM GI											,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			MT.SITE_CD														,	");
		oSqlBuilder.AppendLine("			MT.DISPLAY_DAY														");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_MAN_TREASURE MT												,	");
		oSqlBuilder.AppendLine("			T_GET_MAN_TREASURE_LOG GMTL											");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			MT.SITE_CD					= GMTL.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			MT.CAST_GAME_PIC_SEQ		= GMTL.CAST_GAME_PIC_SEQ			AND	");
		oSqlBuilder.AppendLine("			MT.SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			GMTL.USER_SEQ				= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			GMTL.USER_CHAR_NO			= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			GMTL.GET_TREASURE_LOG_SEQ	= :GET_TREASURE_LOG_SEQ				");
		oSqlBuilder.AppendLine("	) DD																		");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	GI.SITE_CD		= DD.SITE_CD											AND	");
		oSqlBuilder.AppendLine("	GI.DISPLAY_DAY	= DD.DISPLAY_DAY										AND	");
		oSqlBuilder.AppendLine("	GI.SITE_CD		= :SITE_CD													");

		// where
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":GET_TREASURE_LOG_SEQ",pCondition.GetTreasureLogSeq));
		
		// order by
		sSortExpression = "ORDER BY GI.GAME_ITEM_SEQ ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));


		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		return oDataSet;
	}

}
