﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: プレゼントメール期間
--	Progaram ID		: PresentMailTerm
--
--  Creation Date	: 2014.12.02
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class PresentMailTerm:DbSession {
	public PresentMailTerm() {
	}
	
	public bool CheckCurrentPresentMailTerm(string pSiteCd) {
		bool bExists = false;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	1															");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_PRESENT_MAIL_TERM											");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	SYSDATE BETWEEN	TERM_START_DATE AND TERM_END_DATE			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			bExists = true;
		}

		return bExists;
	}
}
