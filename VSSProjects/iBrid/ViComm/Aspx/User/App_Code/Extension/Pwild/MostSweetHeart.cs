﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class MostSweetHeartSeekCondition :SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string SelfUserSeq {
		get {
			return this.Query["self_user_seq"];
		}
		set {
			this.Query["self_user_seq"] = value;
		}
	}

	public string SelfUserCharNo {
		get {
			return this.Query["self_user_char_no"];
		}
		set {
			this.Query["self_user_char_no"] = value;
		}
	}
	
	public MostSweetHeartSeekCondition()
		: this(new NameValueCollection()) {
	}

	public MostSweetHeartSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["self_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_seq"]));
		this.Query["self_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_char_no"]));
	}
}

public class MostSweetHeart:DbSession {
	public MostSweetHeart() {
	}
	
	public void RegistMostSweetHeart(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pOutFlag,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_MOST_SWEET_HEART");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pOUT_FLAG",DbSession.DbType.VARCHAR2,pOutFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public DataSet GetOne(MostSweetHeartSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	USER_SEQ										,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHARACTER00 P					,	");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			SITE_CD									,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ						,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_MOST_SWEET_HEART 							");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :SELF_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	) MSH											,	");
		oSqlBuilder.AppendLine(" 	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine(" 			SITE_CD									,	");
		oSqlBuilder.AppendLine(" 			PARTNER_USER_SEQ						,	");
		oSqlBuilder.AppendLine(" 			PARTNER_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("	 		MV_FRIENDLY_POINT00							");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :SELF_USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			RANK			= :RANK						");
		oSqlBuilder.AppendLine(" 	) FP												");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	P.SITE_CD		= MSH.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ		= MSH.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO	= MSH.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD		= FP.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ		= FP.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO	= FP.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG		IN(:NA_FLAG,:NA_FLAG2)			AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG	= :STAFF_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":RANK",PwViCommConst.GameFriendlyRank.LOVER.ToString()));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}
