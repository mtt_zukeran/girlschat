﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class BattleLogSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	
	public string BattleLogSeq {
		get {
			return this.Query["battle_log_seq"];
		}
		set {
			this.Query["battle_log_seq"] = value;
		}
	}

	public string BattleType {
		get {
			return this.Query["battle_type"];
		}
		set {
			this.Query["battle_type"] = value;
		}
	}
	
	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}
	
	public string AttackWinFlag;
	public string DefenceWinFlag;

	public BattleLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public BattleLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["battle_log_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["battle_log_seq"]));
		this.Query["battle_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["battle_type"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
	}
}
#endregion ============================================================================================================

public class BattleLog:DbSession {
	public BattleLog() {
	}

	public int GetPageCount(BattleLogSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	COUNT(*)													");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	(															");
		
		//直接対決
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ								,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ									,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE										,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG									,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG								,	");
		oSqlBuilder.AppendLine("			DISTINCTION_CD									,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT									,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ								,	");
		oSqlBuilder.AppendLine("			CREATE_DATE											");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG02									");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE			!= :SUPPORT_BATTLE_TYPE			");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ			");
		}
		oSqlBuilder.AppendLine("		UNION														");
		//仲間が応援
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			AS ENEMY_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		AS ENEMY_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE											,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG									,	");
		oSqlBuilder.AppendLine("			3	AS DISTINCTION_CD								,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT										,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ									,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05										");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE				= :SUPPORT_BATTLE_TYPE			");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("		UNION														");
		//仲間を応援
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ			AS PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO		AS PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			AS ENEMY_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		AS ENEMY_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE											,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG									,	");
		oSqlBuilder.AppendLine("			4	AS DISTINCTION_CD								,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT										,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ									,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05										");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE		= :SUPPORT_BATTLE_TYPE					");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("		UNION														");
		//応援ﾊﾞﾄﾙを仕掛けられた
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE											,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG									,	");
		oSqlBuilder.AppendLine("			5	AS DISTINCTION_CD								,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT										,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ									,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05										");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE				= :SUPPORT_BATTLE_TYPE			");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("		UNION														");
		//仲間からの応援依頼
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			NULL						AS ENEMY_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			NULL						AS ENEMY_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,	");
		oSqlBuilder.AppendLine("			NULL AS BATTLE_TYPE									,	");
		oSqlBuilder.AppendLine("			0 AS ATTACK_WIN_FLAG								,	");
		oSqlBuilder.AppendLine("			0 AS DEFENCE_WIN_FLAG								,	");
		oSqlBuilder.AppendLine("			6 AS DISTINCTION_CD									,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT										,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ									,	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_DATE		AS CREATE_DATE				");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01									");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO					");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("	) LOG														,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER											,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER ENEMY											");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ		= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO	= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= ENEMY.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_SEQ			= ENEMY.USER_SEQ				(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_CHAR_NO		= ENEMY.USER_CHAR_NO			(+)	AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_MISSION_FLAG,0)				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_MISSION_TREASURE_FLAG,0)		= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_BATTLE_FLAG,0)				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_BATTLE_TREASURE_FLAG,0)		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			*																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_REFUSE														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD					= LOG.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= LOG.PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= LOG.PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("	)																		");
		
		if (pCondition.BattleType.Equals(PwViCommConst.GameBattleType.SUPPORT)) {
			oSqlBuilder.AppendLine(" AND	DISTINCTION_CD IN(3,4,5,6)");
		} else if (pCondition.BattleType.Equals(PwViCommConst.GameBattleType.SINGLE)) {
			oSqlBuilder.AppendLine(" AND	DISTINCTION_CD IN(1,2)");
		}

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SUPPORT_BATTLE_TYPE",PwViCommConst.GameBattleType.SUPPORT));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));
		}

		OracleParameter[] oWhereParams = oParamList.ToArray();

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		if (pRecCount > 20) {
			pRecCount = 20;
		}

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(BattleLogSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	public DataSet GetPageCollectionBase(BattleLogSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_SEQ								,	");//ﾊﾞﾄﾙ相手(応援された場合にのみ使用)
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_CHAR_NO							,	");//ﾊﾞﾄﾙ相手(応援された場合にのみ使用)
		oSqlBuilder.AppendLine("	LOG.BATTLE_LOG_SEQ								,	");
		oSqlBuilder.AppendLine("	LOG.BATTLE_TYPE									,	");
		oSqlBuilder.AppendLine("	LOG.ATTACK_WIN_FLAG								,	");
		oSqlBuilder.AppendLine("	LOG.DEFENCE_WIN_FLAG							,	");
		oSqlBuilder.AppendLine("	LOG.DISTINCTION_CD								,	");
		oSqlBuilder.AppendLine("	CASE												");
		oSqlBuilder.AppendLine("		WHEN LOG.MOVE_GAME_POINT		< 0				");
		oSqlBuilder.AppendLine("		THEN LOG.MOVE_GAME_POINT		* -1			");
		oSqlBuilder.AppendLine("		ELSE LOG.MOVE_GAME_POINT						");
		oSqlBuilder.AppendLine("	END MOVE_GAME_POINT								,	");
		oSqlBuilder.AppendLine("	LOG.AIM_TREASURE_SEQ							,	");
		oSqlBuilder.AppendLine("	LOG.CREATE_DATE									,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	ENEMY.GAME_HANDLE_NM AS ENEMY_GAME_HANDLE_NM	,	");
		oSqlBuilder.AppendLine("	LOG.SUPPORT_REQUEST_END_FLAG					,	");
		oSqlBuilder.AppendLine("	LOG.TREASURE_COMPLETE_FLAG						,	");
		oSqlBuilder.AppendLine("	LOG.BONUS_GET_FLAG								,	");
		oSqlBuilder.AppendLine("	LOG.DISPLAY_DAY									,	");
		oSqlBuilder.AppendLine("	LOG.STAGE_GROUP_TYPE							,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.HANDLE_NM							");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		//直接対決
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ										,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_SEQ				,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			DISTINCTION_CD											,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			TREASURE_COMPLETE_FLAG									,	");
		oSqlBuilder.AppendLine("			BONUS_GET_FLAG											,	");
		oSqlBuilder.AppendLine("			DISPLAY_DAY												,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG02											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE			!= :SUPPORT_BATTLE_TYPE					");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ			");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//仲間が応援
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			AS ENEMY_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		AS ENEMY_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			3							AS DISTINCTION_CD			,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			TREASURE_COMPLETE_FLAG									,	");
		oSqlBuilder.AppendLine("			BONUS_GET_FLAG											,	");
		oSqlBuilder.AppendLine("			DISPLAY_DAY												,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE				= :SUPPORT_BATTLE_TYPE				");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ					");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//仲間を応援
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ			AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO		AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			AS ENEMY_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		AS ENEMY_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			4							AS DISTINCTION_CD			,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			0 AS TREASURE_COMPLETE_FLAG								,	");
		oSqlBuilder.AppendLine("			0 AS BONUS_GET_FLAG										,	");
		oSqlBuilder.AppendLine("			NULL AS DISPLAY_DAY										,	");
		oSqlBuilder.AppendLine("			NULL AS STAGE_GROUP_TYPE									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE		= :SUPPORT_BATTLE_TYPE						");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ					");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//応援ﾊﾞﾄﾙを仕掛けられた
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		AS PARTNER_USER_SEQ				,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	AS PARTNER_USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_SEQ				,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			5						AS DISTINCTION_CD				,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			0 AS TREASURE_COMPLETE_FLAG								,	");
		oSqlBuilder.AppendLine("			0 AS BONUS_GET_FLAG										,	");
		oSqlBuilder.AppendLine("			NULL AS DISPLAY_DAY										,	");
		oSqlBuilder.AppendLine("			NULL AS STAGE_GROUP_TYPE									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE				= :SUPPORT_BATTLE_TYPE				");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//仲間からの応援依頼
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			NULL						AS ENEMY_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			NULL						AS ENEMY_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,	");
		oSqlBuilder.AppendLine("			NULL AS BATTLE_TYPE									,	");
		oSqlBuilder.AppendLine("			0							AS ATTACK_WIN_FLAG		,	");
		oSqlBuilder.AppendLine("			0							AS DEFENCE_WIN_FLAG		,	");
		oSqlBuilder.AppendLine("			6							AS DISTINCTION_CD		,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT										,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ									,	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_DATE		AS CREATE_DATE			,	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_END_FLAG							,	");
		oSqlBuilder.AppendLine("			0 AS TREASURE_COMPLETE_FLAG							,	");
		oSqlBuilder.AppendLine("			0 AS BONUS_GET_FLAG									,	");
		oSqlBuilder.AppendLine("			NULL AS DISPLAY_DAY									,	");
		oSqlBuilder.AppendLine("			NULL AS STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01									");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO					");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("	) LOG														,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER											,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER ENEMY										,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER												");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ		= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO	= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= T_CAST_CHARACTER.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ		= T_CAST_CHARACTER.USER_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO	= T_CAST_CHARACTER.USER_CHAR_NO		(+)	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= ENEMY.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_SEQ			= ENEMY.USER_SEQ				(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_CHAR_NO		= ENEMY.USER_CHAR_NO			(+)	AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_MISSION_FLAG,0)				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_MISSION_TREASURE_FLAG,0)		= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_BATTLE_FLAG,0)				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_BATTLE_TREASURE_FLAG,0)		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			*																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_REFUSE														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD					= LOG.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= LOG.PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= LOG.PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("	)																		");
		if(pCondition.BattleType.Equals(PwViCommConst.GameBattleType.SUPPORT)) {
			oSqlBuilder.AppendLine(" AND	LOG.DISTINCTION_CD IN(3,4,5,6)");	
		} else if(pCondition.BattleType.Equals(PwViCommConst.GameBattleType.SINGLE)) {
			oSqlBuilder.AppendLine(" AND	LOG.DISTINCTION_CD IN(1,2)");
		}

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SUPPORT_BATTLE_TYPE",PwViCommConst.GameBattleType.SUPPORT));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));
		}

		//Order
		string sSortExpression = "ORDER BY BATTLE_LOG_SEQ DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionWoman(BattleLogSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_SEQ								,	");//ﾊﾞﾄﾙ相手(応援された場合にのみ使用)
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_CHAR_NO							,	");//ﾊﾞﾄﾙ相手(応援された場合にのみ使用)
		oSqlBuilder.AppendLine("	LOG.BATTLE_LOG_SEQ								,	");
		oSqlBuilder.AppendLine("	LOG.BATTLE_TYPE									,	");
		oSqlBuilder.AppendLine("	LOG.ATTACK_WIN_FLAG								,	");
		oSqlBuilder.AppendLine("	LOG.DEFENCE_WIN_FLAG							,	");
		oSqlBuilder.AppendLine("	LOG.DISTINCTION_CD								,	");
		oSqlBuilder.AppendLine("	CASE												");
		oSqlBuilder.AppendLine("		WHEN LOG.MOVE_GAME_POINT		< 0				");
		oSqlBuilder.AppendLine("		THEN LOG.MOVE_GAME_POINT		* -1			");
		oSqlBuilder.AppendLine("		ELSE LOG.MOVE_GAME_POINT						");
		oSqlBuilder.AppendLine("	END MOVE_GAME_POINT								,	");
		oSqlBuilder.AppendLine("	LOG.AIM_TREASURE_SEQ							,	");
		oSqlBuilder.AppendLine("	LOG.CREATE_DATE									,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	ENEMY.GAME_HANDLE_NM AS ENEMY_GAME_HANDLE_NM	,	");
		oSqlBuilder.AppendLine("	LOG.SUPPORT_REQUEST_END_FLAG					,	");
		oSqlBuilder.AppendLine("	LOG.TREASURE_COMPLETE_FLAG						,	");
		oSqlBuilder.AppendLine("	LOG.BONUS_GET_FLAG								,	");
		oSqlBuilder.AppendLine("	LOG.DISPLAY_DAY									,	");
		oSqlBuilder.AppendLine("	LOG.STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		//直接対決
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ										,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_SEQ				,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			DISTINCTION_CD											,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			TREASURE_COMPLETE_FLAG									,	");
		oSqlBuilder.AppendLine("			BONUS_GET_FLAG											,	");
		oSqlBuilder.AppendLine("			DISPLAY_DAY												,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG02											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE			!= :SUPPORT_BATTLE_TYPE					");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ			");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//仲間が応援
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			AS ENEMY_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		AS ENEMY_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			3							AS DISTINCTION_CD			,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			TREASURE_COMPLETE_FLAG									,	");
		oSqlBuilder.AppendLine("			BONUS_GET_FLAG											,	");
		oSqlBuilder.AppendLine("			DISPLAY_DAY												,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE				= :SUPPORT_BATTLE_TYPE				");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ					");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//仲間を応援
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ			AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO		AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			AS ENEMY_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		AS ENEMY_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			4							AS DISTINCTION_CD			,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			0 AS TREASURE_COMPLETE_FLAG								,	");
		oSqlBuilder.AppendLine("			0 AS BONUS_GET_FLAG										,	");
		oSqlBuilder.AppendLine("			NULL AS DISPLAY_DAY										,	");
		oSqlBuilder.AppendLine("			NULL AS STAGE_GROUP_TYPE									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE		= :SUPPORT_BATTLE_TYPE						");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ					");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//応援ﾊﾞﾄﾙを仕掛けられた
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		AS PARTNER_USER_SEQ				,	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	AS PARTNER_USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_SEQ				,	");
		oSqlBuilder.AppendLine("			NULL					AS ENEMY_USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE												,	");
		oSqlBuilder.AppendLine("			ATTACK_WIN_FLAG											,	");
		oSqlBuilder.AppendLine("			DEFENCE_WIN_FLAG										,	");
		oSqlBuilder.AppendLine("			5						AS DISTINCTION_CD				,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT											,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("			CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			NULL					AS SUPPORT_REQUEST_END_FLAG		,	");
		oSqlBuilder.AppendLine("			0 AS TREASURE_COMPLETE_FLAG								,	");
		oSqlBuilder.AppendLine("			0 AS BONUS_GET_FLAG										,	");
		oSqlBuilder.AppendLine("			NULL AS DISPLAY_DAY										,	");
		oSqlBuilder.AppendLine("			NULL AS STAGE_GROUP_TYPE									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG05											");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			BATTLE_TYPE				= :SUPPORT_BATTLE_TYPE				");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("		UNION															");
		//仲間からの応援依頼
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			NULL						AS ENEMY_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			NULL						AS ENEMY_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,	");
		oSqlBuilder.AppendLine("			NULL AS BATTLE_TYPE									,	");
		oSqlBuilder.AppendLine("			0							AS ATTACK_WIN_FLAG		,	");
		oSqlBuilder.AppendLine("			0							AS DEFENCE_WIN_FLAG		,	");
		oSqlBuilder.AppendLine("			6							AS DISTINCTION_CD		,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT										,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ									,	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_DATE		AS CREATE_DATE			,	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_END_FLAG							,	");
		oSqlBuilder.AppendLine("			0 AS TREASURE_COMPLETE_FLAG							,	");
		oSqlBuilder.AppendLine("			0 AS BONUS_GET_FLAG									,	");
		oSqlBuilder.AppendLine("			NULL AS DISPLAY_DAY									,	");
		oSqlBuilder.AppendLine("			NULL AS STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01									");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO					");
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oSqlBuilder.AppendLine("		AND BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ				");
		}
		oSqlBuilder.AppendLine("	) LOG														,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER											,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER ENEMY											");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ		= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO	= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= ENEMY.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_SEQ			= ENEMY.USER_SEQ				(+)	AND	");
		oSqlBuilder.AppendLine("	LOG.ENEMY_USER_CHAR_NO		= ENEMY.USER_CHAR_NO			(+)	AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_MISSION_FLAG,0)				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_MISSION_TREASURE_FLAG,0)		= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_BATTLE_FLAG,0)				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	NVL(ENEMY.TUTORIAL_BATTLE_TREASURE_FLAG,0)		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			*																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_REFUSE														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD					= LOG.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= LOG.PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= LOG.PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("	)																		");
		if (pCondition.BattleType.Equals(PwViCommConst.GameBattleType.SUPPORT)) {
			oSqlBuilder.AppendLine(" AND	LOG.DISTINCTION_CD IN(3,4,5,6)");
		} else if (pCondition.BattleType.Equals(PwViCommConst.GameBattleType.SINGLE)) {
			oSqlBuilder.AppendLine(" AND	LOG.DISTINCTION_CD IN(1,2)");
		}

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SUPPORT_BATTLE_TYPE",PwViCommConst.GameBattleType.SUPPORT));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		if (!string.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));
		}

		//Order
		string sSortExpression = "ORDER BY BATTLE_LOG_SEQ DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOne(BattleLogSeekCondition pCondition){
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT																						");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ																	,	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO																,	");
		oSqlBuilder.AppendLine("	BL.ATTACK_POWER																		,	");
		oSqlBuilder.AppendLine("	BL.USE_ATTACK_COUNT																	,	");
		oSqlBuilder.AppendLine("	BL.USE_DEFENCE_COUNT																,	");
		oSqlBuilder.AppendLine("	BL.MOVE_GAME_POINT																	,	");
		oSqlBuilder.AppendLine("	BL.CAST_GAME_PIC_SEQ																,	");
		oSqlBuilder.AppendLine("	BL.OBJ_PHOTO_IMG_PATH																,	");
		oSqlBuilder.AppendLine("	BL.DISPLAY_DAY																		,	");
		oSqlBuilder.AppendLine("	BL.CAST_GAME_PIC_ATTR_NM															,	");
		oSqlBuilder.AppendLine("	BL.GAME_HANDLE_NM																	,	");
		oSqlBuilder.AppendLine("	BL.GAME_CHARACTER_TYPE																,	");
		oSqlBuilder.AppendLine("	BL.CAST_USER_SEQ																	,	");
		oSqlBuilder.AppendLine("	BL.CAST_USER_CHAR_NO																,	");
		oSqlBuilder.AppendLine("	BL.CAST_GAME_HANDLE_NM																,	");
		oSqlBuilder.AppendLine("	BL.HANDLE_NM																		,	");
		oSqlBuilder.AppendLine("	BL.AGE																				,	");
		oSqlBuilder.AppendLine("	FP.FRIENDLY_POINT																	,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_WIN_COUNT + GC.DEFENCE_WIN_COUNT,0)				AS WIN_COUNT		,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_LOSS_COUNT + GC.DEFENCE_LOSS_COUNT,0)				AS LOSS_COUNT		,	");
		oSqlBuilder.AppendLine("	NVL(GC.PARTY_ATTACK_WIN_COUNT + GC.PARTY_DEFENCE_WIN_COUNT,0)	AS PARTY_WIN_COUNT	,	");
		oSqlBuilder.AppendLine("	NVL(GC.PARTY_ATTACK_LOSS_COUNT + GC.PARTY_DEFENCE_LOSS_COUNT,0)	AS PARTY_LOSS_COUNT		");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	(									");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			USER_SEQ				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			ATTACK_POWER			,	");
		oSqlBuilder.AppendLine("			USE_ATTACK_COUNT		,	");
		oSqlBuilder.AppendLine("			USE_DEFENCE_COUNT		,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT			,	");
		oSqlBuilder.AppendLine("			CAST_GAME_PIC_SEQ		,	");
		oSqlBuilder.AppendLine("			OBJ_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("			DISPLAY_DAY				,	");
		oSqlBuilder.AppendLine("			CAST_GAME_PIC_ATTR_NM	,	");
		oSqlBuilder.AppendLine("			GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_TYPE		,	");
		oSqlBuilder.AppendLine("			CAST_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			CAST_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			CAST_GAME_HANDLE_NM		,	");
		oSqlBuilder.AppendLine("			HANDLE_NM				,	");
		oSqlBuilder.AppendLine("			AGE							");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG01			");

		// where		
		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) BL							,	");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00 FP			,	");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION GC				");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	BL.SITE_CD				= FP.SITE_CD				(+)	AND	");
		oSqlBuilder.AppendLine("	BL.USER_SEQ				= FP.USER_SEQ				(+)	AND	");
		oSqlBuilder.AppendLine("	BL.USER_CHAR_NO			= FP.USER_CHAR_NO			(+)	AND	");
		oSqlBuilder.AppendLine("	BL.CAST_USER_SEQ		= FP.PARTNER_USER_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("	BL.CAST_USER_CHAR_NO	= FP.PARTNER_USER_CHAR_NO	(+)	AND	");
		oSqlBuilder.AppendLine("	BL.SITE_CD				= GC.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	BL.USER_SEQ				= GC.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	BL.USER_CHAR_NO			= GC.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ		= GC.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO	= GC.PARTNER_USER_CHAR_NO			");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneSupport(BattleLogSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ													,	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO												,	");
		oSqlBuilder.AppendLine("	BL.GAME_CHARACTER_TYPE												,	");
		oSqlBuilder.AppendLine("	BL.ATTACK_POWER														,	");
		oSqlBuilder.AppendLine("	BL.USE_ATTACK_COUNT													,	");
		oSqlBuilder.AppendLine("	BL.USE_DEFENCE_COUNT												,	");
		oSqlBuilder.AppendLine("	BL.MOVE_GAME_POINT													,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_WIN_COUNT + GC.DEFENCE_WIN_COUNT,0)	AS WIN_COUNT	,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_LOSS_COUNT + GC.DEFENCE_LOSS_COUNT,0)	AS LOSS_COUNT		");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(									");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			USER_SEQ				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_TYPE		,	");
		oSqlBuilder.AppendLine("			ATTACK_POWER			,	");
		oSqlBuilder.AppendLine("			USE_ATTACK_COUNT		,	");
		oSqlBuilder.AppendLine("			USE_DEFENCE_COUNT		,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT				");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG01			");

		// where		
		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) BL,					");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION GC	");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	BL.SITE_CD				= GC.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	BL.USER_SEQ				= GC.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	BL.USER_CHAR_NO			= GC.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ		= GC.PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO	= GC.PARTNER_USER_CHAR_NO		");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneWoman(BattleLogSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																						");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ																	,	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO																,	");
		oSqlBuilder.AppendLine("	BL.ATTACK_POWER																		,	");
		oSqlBuilder.AppendLine("	BL.USE_ATTACK_COUNT																	,	");
		oSqlBuilder.AppendLine("	BL.USE_DEFENCE_COUNT																,	");
		oSqlBuilder.AppendLine("	BL.MOVE_GAME_POINT																	,	");
		oSqlBuilder.AppendLine("	BL.CAST_TREASURE_SEQ																,	");
		oSqlBuilder.AppendLine("	BL.CAST_TREASURE_NM																	,	");
		oSqlBuilder.AppendLine("	BL.GAME_HANDLE_NM																	,	");
		oSqlBuilder.AppendLine("	BL.GAME_CHARACTER_TYPE																,	");
		oSqlBuilder.AppendLine("	BL.STAGE_GROUP_TYPE																	,	");
		oSqlBuilder.AppendLine("	BL.BONUS_GET_FLAG																	,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_WIN_COUNT + GC.DEFENCE_WIN_COUNT,0)				AS WIN_COUNT		,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_LOSS_COUNT + GC.DEFENCE_LOSS_COUNT,0)				AS LOSS_COUNT		,	");
		oSqlBuilder.AppendLine("	NVL(GC.PARTY_ATTACK_WIN_COUNT + GC.PARTY_DEFENCE_WIN_COUNT,0)	AS PARTY_WIN_COUNT	,	");
		oSqlBuilder.AppendLine("	NVL(GC.PARTY_ATTACK_LOSS_COUNT + GC.PARTY_DEFENCE_LOSS_COUNT,0)	AS PARTY_LOSS_COUNT		");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	(									");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			USER_SEQ				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			ATTACK_POWER			,	");
		oSqlBuilder.AppendLine("			USE_ATTACK_COUNT		,	");
		oSqlBuilder.AppendLine("			USE_DEFENCE_COUNT		,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT			,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ		,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_NM		,	");
		oSqlBuilder.AppendLine("			GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_TYPE		,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE		,	");
		oSqlBuilder.AppendLine("			BONUS_GET_FLAG				");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG03			");

		// where		
		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) BL							,	");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION GC				");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	BL.SITE_CD				= GC.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	BL.USER_SEQ				= GC.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	BL.USER_CHAR_NO			= GC.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ		= GC.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO	= GC.PARTNER_USER_CHAR_NO			");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneSupportWoman(BattleLogSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ													,	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO												,	");
		oSqlBuilder.AppendLine("	BL.GAME_CHARACTER_TYPE												,	");
		oSqlBuilder.AppendLine("	BL.ATTACK_POWER														,	");
		oSqlBuilder.AppendLine("	BL.USE_ATTACK_COUNT													,	");
		oSqlBuilder.AppendLine("	BL.USE_DEFENCE_COUNT												,	");
		oSqlBuilder.AppendLine("	BL.MOVE_GAME_POINT													,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_WIN_COUNT + GC.DEFENCE_WIN_COUNT,0)	AS WIN_COUNT	,	");
		oSqlBuilder.AppendLine("	NVL(GC.ATTACK_LOSS_COUNT + GC.DEFENCE_LOSS_COUNT,0)	AS LOSS_COUNT		");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(									");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			USER_SEQ				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_TYPE		,	");
		oSqlBuilder.AppendLine("			ATTACK_POWER			,	");
		oSqlBuilder.AppendLine("			USE_ATTACK_COUNT		,	");
		oSqlBuilder.AppendLine("			USE_DEFENCE_COUNT		,	");
		oSqlBuilder.AppendLine("			MOVE_GAME_POINT				");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			VW_PW_BATTLE_LOG03			");

		// where		
		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) BL,					");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION GC	");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	BL.SITE_CD				= GC.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	BL.USER_SEQ				= GC.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	BL.USER_CHAR_NO			= GC.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_SEQ		= GC.PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	BL.PARTNER_USER_CHAR_NO	= GC.PARTNER_USER_CHAR_NO		");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(BattleLogSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
			
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		if(!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}
		
		if(!String.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}
		
		if(!String.IsNullOrEmpty(pCondition.BattleLogSeq)) {
			SysPrograms.SqlAppendWhere(" BATTLE_LOG_SEQ = :BATTLE_LOG_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));
		}

		if (!String.IsNullOrEmpty(pCondition.BattleType)) {
			SysPrograms.SqlAppendWhere(" BATTLE_TYPE = :BATTLE_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BATTLE_TYPE",pCondition.BattleType));
		}

		if (!String.IsNullOrEmpty(pCondition.AttackWinFlag)) {
			SysPrograms.SqlAppendWhere(" ATTACK_WIN_FLAG = :ATTACK_WIN_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ATTACK_WIN_FLAG",pCondition.AttackWinFlag));
		}

		if (!String.IsNullOrEmpty(pCondition.DefenceWinFlag)) {
			SysPrograms.SqlAppendWhere(" DEFENCE_WIN_FLAG = :DEFENCE_WIN_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DEFENCE_WIN_FLAG",pCondition.DefenceWinFlag));
		}

		return oParamList.ToArray();
	}
	
	public bool CheckRecentBattleLog(BattleLogSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	BATTLE_LOG_SEQ		");
		oSqlBuilder.AppendLine(" FROM					");
		oSqlBuilder.AppendLine("(						");
		oSqlBuilder.AppendLine("	SELECT				");
		oSqlBuilder.AppendLine("		BATTLE_LOG_SEQ	");
		oSqlBuilder.AppendLine("	FROM				");
		oSqlBuilder.AppendLine("		T_BATTLE_LOG	");

		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!String.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!String.IsNullOrEmpty(pCondition.BattleType)) {
			SysPrograms.SqlAppendWhere(" BATTLE_TYPE = :BATTLE_TYPE",ref sWhereClause);
			oParamList.Add(new OracleParameter(":BATTLE_TYPE",pCondition.BattleType));
		}

		if (!String.IsNullOrEmpty(pCondition.AttackWinFlag)) {
			SysPrograms.SqlAppendWhere(" ATTACK_WIN_FLAG = :ATTACK_WIN_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":ATTACK_WIN_FLAG",pCondition.AttackWinFlag));
		}

		if (!String.IsNullOrEmpty(pCondition.DefenceWinFlag)) {
			SysPrograms.SqlAppendWhere(" DEFENCE_WIN_FLAG = :DEFENCE_WIN_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":DEFENCE_WIN_FLAG",pCondition.DefenceWinFlag));
		}

		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	ORDER BY CREATE_DATE DESC	");
		oSqlBuilder.AppendLine(")								");
		oSqlBuilder.AppendLine(" WHERE							");
		oSqlBuilder.AppendLine("	ROWNUM = 1					");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			DataRow dr = oDataSet.Tables[0].Rows[0];

			if(pCondition.BattleLogSeq.Equals(dr["BATTLE_LOG_SEQ"].ToString())) {
				return true;
			}
		}
		
		return false;
	}
	
	public DataSet GetOneSimpleData(BattleLogSeekCondition pCondition)
	{
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ					,	");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine("	PARTNER_GAME_CHARACTER_TYPE			,	");
		oSqlBuilder.AppendLine("	BATTLE_TYPE							,	");
		oSqlBuilder.AppendLine("	ATTACK_WIN_FLAG						,	");
		oSqlBuilder.AppendLine("	DEFENCE_WIN_FLAG						");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_PW_BATTLE_LOG04						");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	BATTLE_LOG_SEQ	= :BATTLE_LOG_SEQ		");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneSupportRequest(BattleLogSeekCondition pCondition) {

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ											,	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO										,	");
		oSqlBuilder.AppendLine("	LOG.BATTLE_LOG_SEQ												,	");
		oSqlBuilder.AppendLine("	LOG.CREATE_DATE													,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.GAME_HANDLE_NM										");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD												,		");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ		,		");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO	,		");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,		");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_DATE		AS CREATE_DATE					");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01										");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_END_FLAG	= :SUPPORT_REQUEST_END_FLAG		");
		oSqlBuilder.AppendLine("		ORDER BY BATTLE_LOG_SEQ DESC									");
		
		oSqlBuilder.AppendLine("	) LOG															,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER													");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ		= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO	= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	ROWNUM <= 1	  														");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SUPPORT_REQUEST_END_FLAG",ViCommConst.FLAG_OFF));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneSupportRequestAddCast(BattleLogSeekCondition pCondition) {

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ											,	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO										,	");
		oSqlBuilder.AppendLine("	LOG.BATTLE_LOG_SEQ												,	");
		oSqlBuilder.AppendLine("	LOG.CREATE_DATE													,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.GAME_HANDLE_NM									,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.HANDLE_NM											");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD												,		");
		oSqlBuilder.AppendLine("			USER_SEQ					AS PARTNER_USER_SEQ		,		");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				AS PARTNER_USER_CHAR_NO	,		");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ										,		");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_DATE		AS CREATE_DATE					");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01										");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_END_FLAG	= :SUPPORT_REQUEST_END_FLAG		");
		oSqlBuilder.AppendLine("		ORDER BY BATTLE_LOG_SEQ DESC									");

		oSqlBuilder.AppendLine("	) LOG															,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER												,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER													");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ		= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO	= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	LOG.SITE_CD					= T_CAST_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_SEQ		= T_CAST_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	LOG.PARTNER_USER_CHAR_NO	= T_CAST_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	ROWNUM <= 1	  														");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SUPPORT_REQUEST_END_FLAG",ViCommConst.FLAG_OFF));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	public DataSet GetOneForTutorial(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	*															");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			INNER.*											,	");
		oSqlBuilder.AppendLine("			ROWNUM AS RNUM										");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			(													");
		oSqlBuilder.AppendLine("				SELECT											");
		oSqlBuilder.AppendLine("					BATTLE_LOG_SEQ								");
		oSqlBuilder.AppendLine("				FROM											");
		oSqlBuilder.AppendLine("					T_BATTLE_LOG								");
		oSqlBuilder.AppendLine("				WHERE											");
		oSqlBuilder.AppendLine("					SITE_CD = :SITE_CD AND						");
		oSqlBuilder.AppendLine("					USER_SEQ = :USER_SEQ AND					");
		oSqlBuilder.AppendLine("					USER_CHAR_NO = :USER_CHAR_NO				");
		oSqlBuilder.AppendLine("				ORDER BY CREATE_DATE DESC						");
		oSqlBuilder.AppendLine("			) INNER												");
		oSqlBuilder.AppendLine("	)															");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	RNUM = 1													");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
