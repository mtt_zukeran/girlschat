﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 会員つぶやきコメントランキング設定
--	Progaram ID		: ManTweetCommentTerm
--
--  Creation Date	: 2013.03.27
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManTweetCommentTermSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string Finished;

	public ManTweetCommentTermSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTweetCommentTermSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class ManTweetCommentTerm:DbSession {
	public ManTweetCommentTerm() {
	}

	public DataSet GetNowData(string pSiteCd,string pCastUserSeq,string pCastCharNo) {
		DataSet oDataSet = new DataSet();
		DataTable dt = new DataTable();
		dt.Columns.Add("TERM_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("START_DATE",Type.GetType("System.String"));
		dt.Columns.Add("END_DATE",Type.GetType("System.String"));
		dt.Columns.Add("LAST_TERM_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("NEXT_END_DATE",Type.GetType("System.String"));
		dt.Columns.Add("SELF_COMMENT_COUNT",Type.GetType("System.String"));
		dt.Columns.Add("SELF_RANK_NO",Type.GetType("System.String"));
		oDataSet.Tables.Add(dt);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_NOW_MAN_TWEET_COMMENT_TERM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("pTERM_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTART_DATE",DbSession.DbType.DATE);
			db.ProcedureOutParm("pEND_DATE",DbSession.DbType.DATE);
			db.ProcedureOutParm("pLAST_TERM_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pNEXT_END_DATE",DbSession.DbType.DATE);
			db.ProcedureOutParm("pSELF_COMMENT_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSELF_RANK_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			if (db.GetStringValue("pRESULT").Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				DataRow dr = oDataSet.Tables[0].NewRow();
				dr["TERM_SEQ"] = db.GetStringValue("pTERM_SEQ");
				dr["START_DATE"] = iBridUtil.GetStringValue(db.GetDateTimeValue("pSTART_DATE"));
				dr["END_DATE"] = iBridUtil.GetStringValue(db.GetDateTimeValue("pEND_DATE"));
				dr["LAST_TERM_FLAG"] = db.GetStringValue("pLAST_TERM_FLAG");
				dr["NEXT_END_DATE"] = iBridUtil.GetStringValue(db.GetDateTimeValue("pNEXT_END_DATE"));
				dr["SELF_COMMENT_COUNT"] = db.GetStringValue("pSELF_COMMENT_COUNT");
				dr["SELF_RANK_NO"] = db.GetStringValue("pSELF_RANK_NO");
				oDataSet.Tables[0].Rows.Add(dr);
			}
		}

		return oDataSet;
	}

	public DataSet GetPageCollection(ManTweetCommentTermSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	TERM_SEQ,");
		oSqlBuilder.AppendLine("	END_DATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT_TERM");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY END_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public string GetLastTermSeq(string pSiteCd) {
		string sTermSeq = string.Empty;

		ManTweetCommentTermSeekCondition oCondition = new ManTweetCommentTermSeekCondition();
		oCondition.SiteCd = pSiteCd;
		oCondition.Finished = ViCommConst.FLAG_ON_STR;
		DataSet oDataSet = GetPageCollection(oCondition,1,1);

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sTermSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TERM_SEQ"]);
		}

		return sTermSeq;
	}

	private OracleParameter[] CreateWhere(ManTweetCommentTermSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (pCondition.Finished.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("END_DATE < SYSDATE",ref pWhereClause);
		}

		return oParamList.ToArray();
	}
}
