﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: お宝deビンゴエントリー
--	Progaram ID		: BbsBingoEntry
--  Creation Date	: 2013.10.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class BbsBingoEntrySeekCondition:SeekConditionBase {
	public string SiteCd;
	public string PrizeFlag;

	public string TermSeq {
		get {
			return this.Query["termseq"];
		}
		set {
			this.Query["termseq"] = value;
		}
	}

	public BbsBingoEntrySeekCondition()
		: this(new NameValueCollection()) {
	}

	public BbsBingoEntrySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["termseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["termseq"]));
	}
}

public class BbsBingoEntry:DbSession {
	public BbsBingoEntry() {
	}

	public int GetPageCount(BbsBingoEntrySeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		int iPageCount = 0;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_BBS_BINGO_ENTRY01");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);

		return iPageCount;
	}

	public DataSet GetPageCollection(BbsBingoEntrySeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	PRIZE_DATE,");
		oSqlBuilder.AppendLine("	PRIZE_POINT,");
		oSqlBuilder.AppendLine("	PRIZE_AMT,");
		oSqlBuilder.AppendLine("	BINGO_BALL_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_BBS_BINGO_ENTRY01");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = CreateOrderExpresion(pCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(BbsBingoEntrySeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.TermSeq)) {
			SysPrograms.SqlAppendWhere("TERM_SEQ = :TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TERM_SEQ",pCondition.TermSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PrizeFlag)) {
			SysPrograms.SqlAppendWhere("PRIZE_FLAG = :PRIZE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRIZE_FLAG",pCondition.PrizeFlag));
		}

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(BbsBingoEntrySeekCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = "ORDER BY SITE_CD,TERM_SEQ,PRIZE_DATE DESC NULLS LAST,ENTRY_DATE DESC";
		return sSortExpression;
	}

	public DataSet GetSelf(string pSiteCd,string pTermSeq,string pUserSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	TERM_SEQ,");
		oSqlBuilder.AppendLine("	ENTRY_SEQ,");
		oSqlBuilder.AppendLine("	CARD_NO,");
		oSqlBuilder.AppendLine("	COMPLETE_FLAG,");
		oSqlBuilder.AppendLine("	BINGO_BALL_FLAG,");
		oSqlBuilder.AppendLine("	PRIZE_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_BBS_BINGO_ENTRY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	TERM_SEQ = :TERM_SEQ AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	EXISTS (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			1");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER_EX");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			SITE_CD = T_BBS_BINGO_ENTRY.SITE_CD AND");
		oSqlBuilder.AppendLine("			USER_SEQ = T_BBS_BINGO_ENTRY.USER_SEQ AND");
		oSqlBuilder.AppendLine("			BBS_BINGO_ENTRY_SEQ = T_BBS_BINGO_ENTRY.ENTRY_SEQ");
		oSqlBuilder.AppendLine("	)");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":TERM_SEQ",pTermSeq));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public void EntryBbsBingo(
		string pSiteCd,
		string pUserSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ENTRY_BBS_BINGO");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
