﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ブログ記事イイネ
--	Progaram ID		: BlogArticleLike
--
--  Creation Date	: 2014.03.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class BlogArticleLikeSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string CastUserSeq;
	public string CastCharNo;

	public string BlogArticleSeq {
		get {
			return this.Query["blogarticleseq"];
		}
		set {
			this.Query["blogarticleseq"] = value;
		}
	}

	public BlogArticleLikeSeekCondition()
		: this(new NameValueCollection()) {
	}

	public BlogArticleLikeSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["blogarticleseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blogarticleseq"]));
	}
}

#endregion ============================================================================================================

public class BlogArticleLike:ManBase {
	public BlogArticleLike()
		: base() {
	}
	public BlogArticleLike(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(BlogArticleLikeSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_BLOG_ARTICLE_LIKE00 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(BlogArticleLikeSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "						,	");
		oSqlBuilder.AppendLine("	P.LIKE_DATE									,	");
		oSqlBuilder.AppendLine("	P.RICHINO_RANK									");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_BLOG_ARTICLE_LIKE00 P						");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.LIKE_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(BlogArticleLikeSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.BlogArticleSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_ARTICLE_SEQ		= :BLOG_ARTICLE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_ARTICLE_SEQ",pCondition.BlogArticleSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_USER_SEQ		= :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere(" P.CAST_CHAR_NO		= :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		
		if (!string.IsNullOrEmpty(pCondition.CastUserSeq) && !string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND PARTNER_USER_SEQ = :CAST_USER_SEQ AND PARTNER_USER_CHAR_NO = :CAST_CHAR_NO)",ref pWhereClause);
		}

		return oParamList.ToArray();
	}
	
	public void RegistBlogArticleLike(string pSiteCd,string pUserSeq,string pBlogArticleSeq,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_BLOG_ARTICLE_LIKE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pBLOG_ARTICLE_SEQ",DbSession.DbType.VARCHAR2,pBlogArticleSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}