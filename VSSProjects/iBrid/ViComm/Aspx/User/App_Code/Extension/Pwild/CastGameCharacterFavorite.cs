﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰお気に入り

--	Progaram ID		: CastGameCharacterFavorite
--
--  Creation Date	: 2011.08.05
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterFavoriteSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string Online {
		get {
			return this.Query["online"];
		}
		set {
			this.Query["online"] = value;
		}
	}
	
	public string TreasurePic {
		get {
			return this.Query["treasure_pic"];
		}
		set {
			this.Query["treasure_pic"] = value;
		}
	}
	
	public string TreasureMovie {
		get {
			return this.Query["treasure_movie"];
		}
		set {
			this.Query["treasure_movie"] = value;
		}
	}
	
	public string Agefrom {
		get {
			return this.Query["agefrom"];
		}
		set {
			this.Query["agefrom"] = value;
		}
	}
	
	public string Ageto {
		get {
			return this.Query["ageto"];
		}
		set {
			this.Query["ageto"] = value;
		}
	}

	public string Handle {
		get {
			return this.Query["handle"];
		}
		set {
			this.Query["handle"] = value;
		}
	}
	
	public string SiteUseStatus;

	public CastGameCharacterFavoriteSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterFavoriteSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_seq"]));
		this.Query["online"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["online"]));
		this.Query["treasure_pic"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["treasure_pic"]));
		this.Query["treasure_movie"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["treasure_movie"]));
		this.Query["agefrom"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["agefrom"]));
		this.Query["ageto"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ageto"]));
		this.Query["handle"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["handle"]));
	}
}

[System.Serializable]
public class CastGameCharacterFavorite:CastBase {

	public CastGameCharacterFavorite()
		: base() {
	}

	public CastGameCharacterFavorite(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastGameCharacterFavoriteSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHAR_FAVORIT00 P	,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER_EX EX					");
		// where
		string sWhereClause = string.Empty;
		SysPrograms.SqlAppendWhere(" P.SITE_CD					= EX.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ					= EX.USER_SEQ",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO				= EX.USER_CHAR_NO",ref sWhereClause);

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterFavoriteSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "					,	");
		oSqlBuilder.AppendLine("	P.SELF_USER_SEQ								,	");
		oSqlBuilder.AppendLine("	P.SELF_USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL						,	");
		oSqlBuilder.AppendLine("	P.GAME_FAVORIT_REGIST_DATE					,	");
		oSqlBuilder.AppendLine("	P.EXP										,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("	P.VIEW_LAST_LOGIN_DATE						,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.RANK	AS FRIENDLY_RANK		,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.FRIENDLY_POINT					");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		" + CastBasicField() + "	,				");
		oSqlBuilder.AppendLine("		P.SELF_USER_SEQ				,				");
		oSqlBuilder.AppendLine("		P.SELF_USER_CHAR_NO			,				");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM			,				");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL		,				");
		oSqlBuilder.AppendLine("		P.GAME_FAVORIT_REGIST_DATE	,				");
		oSqlBuilder.AppendLine("		P.EXP						,				");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE		,				");
		oSqlBuilder.AppendLine("		CASE												");
		oSqlBuilder.AppendLine("			WHEN											");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE IS NOT NULL AND	");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE < SYSDATE		");
		oSqlBuilder.AppendLine("			THEN											");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE					");
		oSqlBuilder.AppendLine("			ELSE											");
		oSqlBuilder.AppendLine("				P.LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("		END VIEW_LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("	FROM													");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHAR_FAVORIT00 P				,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER_EX EX							,	");
		oSqlBuilder.AppendLine("		(																");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				SITE_CD												,	");
		oSqlBuilder.AppendLine("				USER_SEQ											,	");
		oSqlBuilder.AppendLine("				USER_CHAR_NO										,	");
		oSqlBuilder.AppendLine("				OFFLINE_LAST_UPDATE_DATE								");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_FAVORIT												");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_SEQ		= :SELF_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("				WAITING_VIEW_STATUS		= :WAITING_VIEW_STATUS		AND	");
		oSqlBuilder.AppendLine("				LOGIN_VIEW_STATUS		= :LOGIN_VIEW_STATUS			");
		oSqlBuilder.AppendLine("		) F																");

		// where
		SysPrograms.SqlAppendWhere(" P.SITE_CD					= EX.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ					= EX.USER_SEQ",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO				= EX.USER_CHAR_NO",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.SITE_CD					= F.SITE_CD			(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ					= F.USER_SEQ		(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO				= F.USER_CHAR_NO	(+)",ref sWhereClause);
		oParamList.Add(new OracleParameter(":WAITING_VIEW_STATUS","1"));
		oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS","1"));
		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine(" 	) P,											");
		oSqlBuilder.AppendLine(" 	(SELECT											");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.SITE_CD				,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.PARTNER_USER_SEQ	,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.PARTNER_USER_CHAR_NO,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.RANK				,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.FRIENDLY_POINT			");
		oSqlBuilder.AppendLine(" 	FROM											");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00							");

		// where		
		this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend);
		oSqlBuilder.AppendLine(sWhereClauseFriend);

		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT								");
		oSqlBuilder.AppendLine(" WHERE																		");
		oSqlBuilder.AppendLine(" 	P.SITE_CD				= FRIENDLY_POINT.SITE_CD				(+) AND	");
		oSqlBuilder.AppendLine(" 	P.USER_SEQ				= FRIENDLY_POINT.PARTNER_USER_SEQ		(+) AND ");
		oSqlBuilder.AppendLine(" 	P.USER_CHAR_NO			= FRIENDLY_POINT.PARTNER_USER_CHAR_NO	(+)		");

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		AppendCastAttr(oDataSet);
		AppendManTreasure(oDataSet,pCondtion);
		AppendCastMovie(oDataSet,pCondtion);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastGameCharacterFavoriteSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.SELF_USER_SEQ		= :SELF_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.SELF_USER_CHAR_NO		= :SELF_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.Online) && pCondition.Online != ViCommConst.FLAG_OFF_STR) {

			switch (int.Parse(pCondition.Online)) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) ",ref pWhereClause);
					oParamList.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					oParamList.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND WAITING_VIEW_STATUS != :WAITING_VIEW_STATUS)",ref pWhereClause);
					oParamList.Add(new OracleParameter(":WAITING_VIEW_STATUS","0"));
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhereClause);
					oParamList.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
					oParamList.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					oParamList.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));

					SysPrograms.SqlAppendWhere("P.LAST_ACTION_DATE >= :LAST_ACTION_DATE ",ref pWhereClause);
					oParamList.Add(new OracleParameter("LAST_ACTION_DATE",OracleDbType.Date,DateTime.Now.AddDays(-1),ParameterDirection.Input));

					SysPrograms.SqlAppendWhere("P.PRIORITY > :PRIORITY ",ref pWhereClause);
					oParamList.Add(new OracleParameter("PRIORITY",ViCommConst.FLAG_OFF));
					
					SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND LOGIN_VIEW_STATUS = :LOGIN_VIEW_STATUS AND OFFLINE_LAST_UPDATE_DATE < SYSDATE)",ref pWhereClause);
					oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS","1"));
					break;
			}
		}

		if (!string.IsNullOrEmpty(pCondition.TreasurePic)) {
			SysPrograms.SqlAppendWhere(" EXISTS (SELECT * FROM T_MAN_TREASURE MT WHERE SITE_CD = P.SITE_CD AND " +
			 " USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PUBLISH_FLAG = :PUBLISH_FLAG   AND " +
			 "NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = MT.SITE_CD AND USER_SEQ = MT.USER_SEQ AND USER_CHAR_NO = MT.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND GAME_PIC_NON_PUBLISH_DATE < MT.UPLOAD_DATE))",ref pWhereClause);
			 oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		}

		if (!string.IsNullOrEmpty(pCondition.TreasureMovie)) {
			SysPrograms.SqlAppendWhere(" EXISTS (SELECT * FROM T_CAST_MOVIE WHERE SITE_CD = P.SITE_CD AND " +
			 " USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND MOVIE_TYPE = :GAME_MOVIE_TYPE )   ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));
			
			SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND BBS_NON_PUBLISH_FLAG = :BBS_NON_PUBLISH_FLAG)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		}

		if (!string.IsNullOrEmpty(pCondition.Agefrom)) {
			SysPrograms.SqlAppendWhere(" P.AGE >= :AGE_FROM",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AGE_FROM",pCondition.Agefrom));
		}

		if (!string.IsNullOrEmpty(pCondition.Ageto)) {
			SysPrograms.SqlAppendWhere(" P.AGE <= :AGE_TO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AGE_TO",pCondition.Ageto));

		}
		
		if (!string.IsNullOrEmpty(pCondition.Handle)) {
			SysPrograms.SqlAppendWhere(" P.HANDLE_NM LIKE '%' || :HANDLE_NM || '%' ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":HANDLE_NM",pCondition.Handle));
		}
		
		
		if(!string.IsNullOrEmpty(pCondition.SiteUseStatus)) {
			SysPrograms.SqlAppendWhere(" EX.SITE_USE_STATUS		= :SITE_USE_STATUS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_USE_STATUS",pCondition.SiteUseStatus));
		}
		
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("(P.NA_FLAG IN (:NA_FLAG,:NA_FLAG_2))",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		
		return oParamList.ToArray();
	}

	private void CreateWhereFriend(CastGameCharacterFavoriteSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.SITE_CD = :SITE_CD",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.USER_SEQ = :SELF_USER_SEQ",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.USER_CHAR_NO = :SELF_USER_CHAR_NO",ref pWhereClause);
		}
	}

	private string CreateOrderExpresion(CastGameCharacterFavoriteSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.CastGameCharacterFavoriteSort.LAST_LOGIN_DATE_DESC:
				sSortExpression = "ORDER BY VIEW_LAST_LOGIN_DATE DESC NULLS LAST";
				break;
			case PwViCommConst.CastGameCharacterFavoriteSort.GAME_CHARACTER_LEVEL_DESC:
				sSortExpression = "ORDER BY GAME_CHARACTER_LEVEL DESC, EXP DESC";
				break;
			case PwViCommConst.CastGameCharacterFavoriteSort.FRIENDLY_RANK_ASC:
				sSortExpression = "ORDER BY FRIENDLY_RANK, FRIENDLY_POINT DESC, VIEW_LAST_LOGIN_DATE DESC NULLS LAST";
				break;
			case PwViCommConst.CastGameCharacterFavoriteSort.GAME_FAVORITE_REGIST_DATE_DESC:
				sSortExpression = "ORDER BY GAME_FAVORIT_REGIST_DATE DESC, VIEW_LAST_LOGIN_DATE DESC NULLS LAST";
				break;
			case PwViCommConst.CastGameCharacterFavoriteSort.RANDOM:
				sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";
				break;
			default:
				sSortExpression = "ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}

	private void AppendManTreasure(DataSet pDS,CastGameCharacterFavoriteSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("GAME_TREASURE_PIC_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											");
			oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_PIC_COUNT			");
			oSqlBuilder.AppendLine(" FROM							   				");
			oSqlBuilder.AppendLine("	T_MAN_TREASURE P							");
			oSqlBuilder.AppendLine(" WHERE											");
			oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND			");
			oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND			");
			oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO	AND			");
			oSqlBuilder.AppendLine("	PUBLISH_FLAG	= :PUBLISH_FLAG	AND			");
			oSqlBuilder.AppendLine("	NOT EXISTS														");
			oSqlBuilder.AppendLine("	(																");
			oSqlBuilder.AppendLine("		SELECT														");
			oSqlBuilder.AppendLine("			*														");
			oSqlBuilder.AppendLine("		FROM														");
			oSqlBuilder.AppendLine("			T_FAVORIT												");
			oSqlBuilder.AppendLine("		WHERE														");
			oSqlBuilder.AppendLine("			SITE_CD						= P.SITE_CD				AND	");
			oSqlBuilder.AppendLine("			USER_SEQ					= P.USER_SEQ			AND	");
			oSqlBuilder.AppendLine("			USER_CHAR_NO				= P.USER_CHAR_NO		AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= :SELF_USER_SEQ		AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= :SELF_USER_CHAR_NO	AND	");
			oSqlBuilder.AppendLine("			GAME_PIC_NON_PUBLISH_DATE	< P.UPLOAD_DATE				");
			oSqlBuilder.AppendLine("	)																");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					cmd.Parameters.Add(":SELF_USER_SEQ",pCondition.UserSeq);
					cmd.Parameters.Add(":SELF_USER_CHAR_NO",pCondition.UserCharNo);
					cmd.Parameters.Add(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["GAME_TREASURE_PIC_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_TREASURE_PIC_COUNT"];
				}
			}
		}
	}

	private void AppendCastMovie(DataSet pDS,CastGameCharacterFavoriteSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("GAME_TREASURE_MOVIE_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT													");
			oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_MOVIE_COUNT				");
			oSqlBuilder.AppendLine(" FROM													");
			oSqlBuilder.AppendLine("	T_CAST_MOVIE P										");
			oSqlBuilder.AppendLine(" WHERE													");
			oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND	");
			oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND	");
			oSqlBuilder.AppendLine("	MOVIE_TYPE				= :MOVIE_TYPE			AND	");
			oSqlBuilder.AppendLine("	OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPROVE_FLAG	AND	");
			oSqlBuilder.AppendLine("	OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG	AND	");
			oSqlBuilder.AppendLine("	NOT EXISTS														");
			oSqlBuilder.AppendLine("	(																");
			oSqlBuilder.AppendLine("		SELECT														");
			oSqlBuilder.AppendLine("			*														");
			oSqlBuilder.AppendLine("		FROM														");
			oSqlBuilder.AppendLine("			T_FAVORIT												");
			oSqlBuilder.AppendLine("		WHERE														");
			oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD					AND	");
			oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ				AND	");
			oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO			AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ			AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		AND	");
			oSqlBuilder.AppendLine("			BBS_NON_PUBLISH_FLAG	= :BBS_NON_PUBLISH_FLAG			");
			oSqlBuilder.AppendLine("	)																");
			

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					cmd.Parameters.Add(":SELF_USER_SEQ",pCondition.UserSeq);
					cmd.Parameters.Add(":SELF_USER_CHAR_NO",pCondition.UserCharNo);
					cmd.Parameters.Add(":MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME);
					cmd.Parameters.Add(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR);
					cmd.Parameters.Add(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR);
					cmd.Parameters.Add(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["GAME_TREASURE_MOVIE_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_TREASURE_MOVIE_COUNT"];
				}
			}
		}
	}
	
	public int GetGameFavoriteCount(CastGameCharacterFavoriteSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	COUNT(*) AS FAVORITE_COUNT					");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHAR_FAVORIT00 P		,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER_EX EX						");
		// where
		SysPrograms.SqlAppendWhere(" P.SITE_CD					= EX.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ					= EX.USER_SEQ",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO				= EX.USER_CHAR_NO",ref sWhereClause);
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		int iCount;

		int.TryParse(oDataSet.Tables[0].Rows[0]["FAVORITE_COUNT"].ToString(),out iCount);
		
		return iCount;
	}
}

