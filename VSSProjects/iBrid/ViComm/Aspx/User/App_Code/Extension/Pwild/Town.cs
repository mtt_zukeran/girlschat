﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾅﾝﾊﾟｽﾎﾟｯﾄ
--	Progaram ID		: Town
--
--  Creation Date	: 2011.07.25
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

[Serializable]
public class TownSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public string StageSeq {
		get {
			return this.Query["stage_seq"];
		}
		set {
			this.Query["stage_seq"] = value;
		}
	}

	public string TownSeq {
		get {
			return this.Query["town_seq"];
		}
		set {
			this.Query["town_seq"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string TownClearFlag {
		get {
			return this.Query["town_clear_flag"];
		}
		set {
			this.Query["town_clear_flag"] = value;
		}
	}

	public TownSeekCondition()
		: this(new NameValueCollection()) {
	}
	
	public TownSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["stage_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_seq"]));
		this.Query["town_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["town_seq"]));
		this.Query["town_clear_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["town_clear_flag"]));
	}
}

[System.Serializable]
public class Town:DbSession {

	public Town() {
	}
	
	public int GetPageCount(TownSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	T_TOWN		");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}
	
	public DataSet GetPageCollection(TownSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseProgress = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ				,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_SEQ				,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_NM				,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_NM_EXTRA			,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_LEVEL				,	");
		oSqlBuilder.AppendLine("	TOWN.EXP					,	");
		oSqlBuilder.AppendLine("	TOWN.LOSS_FORCE_COUNT		,	");
		oSqlBuilder.AppendLine("	TOWN.INCOME_MIN				,	");
		oSqlBuilder.AppendLine("	TOWN.INCOME_MAX				,	");
		oSqlBuilder.AppendLine("	TOWN.TARGET_COUNT			,	");
		oSqlBuilder.AppendLine("	PROGRESS.EXEC_COUNT			,	");
		oSqlBuilder.AppendLine("	PROGRESS.TOWN_CLEAR_FLAG	,	");
		oSqlBuilder.AppendLine("	STAGE.STAGE_NM				,	");
		oSqlBuilder.AppendLine("	STAGE.STAGE_GROUP_TYPE		,	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_NM			,	");
		oSqlBuilder.AppendLine("	CASE							");
		oSqlBuilder.AppendLine("		WHEN TOWN_LEVEL = (SELECT MAX(TOWN_LEVEL) FROM T_TOWN WHERE SITE_CD = :SITE_CD AND STAGE_SEQ = :STAGE_SEQ AND SEX_CD = :SEX_CD) THEN 1");
		oSqlBuilder.AppendLine("		ELSE 0						");
		oSqlBuilder.AppendLine("	END AS LAST_TOWN_FLAG			");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	(SELECT							");
		oSqlBuilder.AppendLine("		SITE_CD				,		");
		oSqlBuilder.AppendLine("		STAGE_SEQ			,		");
		oSqlBuilder.AppendLine("		TOWN_SEQ			,		");
		oSqlBuilder.AppendLine("		TOWN_NM				,		");
		oSqlBuilder.AppendLine("		TOWN_NM_EXTRA		,		");
		oSqlBuilder.AppendLine("		TOWN_LEVEL			,		");
		oSqlBuilder.AppendLine("		EXP					,		");
		oSqlBuilder.AppendLine("		LOSS_FORCE_COUNT	,		");
		oSqlBuilder.AppendLine("		INCOME_MIN			,		");
		oSqlBuilder.AppendLine("		INCOME_MAX			,		");
		oSqlBuilder.AppendLine("		TARGET_COUNT				");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		T_TOWN						");
		// where
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) TOWN,							");
		oSqlBuilder.AppendLine("	(SELECT							");
		oSqlBuilder.AppendLine("		SITE_CD			,			");
		oSqlBuilder.AppendLine("		STAGE_SEQ		,			");
		oSqlBuilder.AppendLine("		TOWN_SEQ		,			");
		oSqlBuilder.AppendLine("		EXEC_COUNT		,			");
		oSqlBuilder.AppendLine("		TOWN_CLEAR_FLAG				");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		T_TOWN_PROGRESS				");
		// where
		oParamList.AddRange(this.CreateWhereProgress(pCondtion,ref sWhereClauseProgress));
		oSqlBuilder.AppendLine(sWhereClauseProgress);
		oSqlBuilder.AppendLine("	) PROGRESS,						");
		oSqlBuilder.AppendLine("	T_STAGE STAGE,					");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP SG				");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	TOWN.SITE_CD			= STAGE.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ			= STAGE.STAGE_SEQ		AND	");
		oSqlBuilder.AppendLine("	STAGE.SITE_CD			= SG.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	STAGE.STAGE_GROUP_TYPE	= SG.STAGE_GROUP_TYPE	AND	");
		oSqlBuilder.AppendLine("	STAGE.SEX_CD			= SG.SEX_CD				AND	");
		oSqlBuilder.AppendLine("	TOWN.SITE_CD = PROGRESS.SITE_CD		(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ = PROGRESS.STAGE_SEQ (+) AND	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_SEQ = PROGRESS.TOWN_SEQ	(+)		");

		if (!string.IsNullOrEmpty(pCondtion.TownClearFlag)) {
			oSqlBuilder.AppendLine(" AND NVL(PROGRESS.TOWN_CLEAR_FLAG,0) = :TOWN_CLEAR_FLAG");
			oParamList.Add(new OracleParameter(":TOWN_CLEAR_FLAG",pCondtion.TownClearFlag));
		}

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionItem(TownSeekCondition pCondtion,int pNeedCount) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseProgress = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ						,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_SEQ						,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_NM						,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_NM_EXTRA					,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_LEVEL						,	");
		oSqlBuilder.AppendLine("	TOWN.EXP							,	");
		oSqlBuilder.AppendLine("	TOWN.LOSS_FORCE_COUNT				,	");
		oSqlBuilder.AppendLine("	TOWN.INCOME_MIN						,	");
		oSqlBuilder.AppendLine("	TOWN.INCOME_MAX						,	");
		oSqlBuilder.AppendLine("	TOWN.TARGET_COUNT					,	");
		oSqlBuilder.AppendLine("	NVL(PROGRESS.EXEC_COUNT,0) AS EXEC_COUNT			,	");
		oSqlBuilder.AppendLine("	NVL(PROGRESS.TOWN_CLEAR_FLAG,0)	AS TOWN_CLEAR_FLAG	,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_SEQ AS USED_ITEM_SEQ	,	");
		oSqlBuilder.AppendLine("	ITEM.ITEM_COUNT AS USED_ITEM_COUNT	,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_GET_CD 				,	");
		oSqlBuilder.AppendLine("	0 AS USED_ITEM_POSSESSION_COUNT			");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine(" (								");
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	SITE_CD				,		");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,		");
		oSqlBuilder.AppendLine("	TOWN_SEQ			,		");
		oSqlBuilder.AppendLine("	TOWN_NM				,		");
		oSqlBuilder.AppendLine("	TOWN_NM_EXTRA		,		");
		oSqlBuilder.AppendLine("	TOWN_LEVEL			,		");
		oSqlBuilder.AppendLine("	EXP					,		");
		oSqlBuilder.AppendLine("	LOSS_FORCE_COUNT	,		");
		oSqlBuilder.AppendLine("	INCOME_MIN			,		");
		oSqlBuilder.AppendLine("	INCOME_MAX			,		");
		oSqlBuilder.AppendLine("	TARGET_COUNT				");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	T_TOWN						");	
		// where
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" )	TOWN,						");

		oSqlBuilder.AppendLine(" (						");
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD			,	");
		oSqlBuilder.AppendLine("	STAGE_SEQ		,	");
		oSqlBuilder.AppendLine("	TOWN_SEQ		,	");
		oSqlBuilder.AppendLine("	EXEC_COUNT		,	");
		oSqlBuilder.AppendLine("	TOWN_CLEAR_FLAG		");
		oSqlBuilder.AppendLine(" FROM					");
		oSqlBuilder.AppendLine("	T_TOWN_PROGRESS		");
		// where
		oParamList.AddRange(this.CreateWhereProgress(pCondtion,ref sWhereClauseProgress));
		oSqlBuilder.AppendLine(sWhereClauseProgress);
		oSqlBuilder.AppendLine(" )	PROGRESS,			");

		oSqlBuilder.AppendLine(" (								");
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	SITE_CD				,		");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,		");
		oSqlBuilder.AppendLine("	TOWN_SEQ			,		");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ		,		");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_CD	,		");
		oSqlBuilder.AppendLine("	ITEM_COUNT					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CLR_TWN_USD_ITM01");
		oSqlBuilder.AppendLine(" WHERE							");
		oSqlBuilder.AppendFormat(" (	GAME_ITEM_GET_CD = {0}	OR	",PwViCommConst.GameItemGetCd.NOT_CHARGE).AppendLine();
		oSqlBuilder.AppendLine("		GAME_ITEM_GET_CD	IS NULL	)");
		oSqlBuilder.AppendLine(" )	ITEM						");

		oSqlBuilder.AppendLine(" WHERE						");
		oSqlBuilder.AppendLine("	TOWN.SITE_CD		= PROGRESS.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ		= PROGRESS.STAGE_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_SEQ		= PROGRESS.TOWN_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.SITE_CD		= ITEM.SITE_CD				(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ		= ITEM.STAGE_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_SEQ		= ITEM.TOWN_SEQ				(+)		");
		
		if(!string.IsNullOrEmpty(pCondtion.TownClearFlag)) {
			oSqlBuilder.AppendLine(" AND NVL(PROGRESS.TOWN_CLEAR_FLAG,0) = :TOWN_CLEAR_FLAG");
			oParamList.Add(new OracleParameter(":TOWN_CLEAR_FLAG",pCondtion.TownClearFlag));
		}

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendPossession(ds,pCondtion);

		DataSet oDataSet = new DataSet();
		DataTable oDataTable = oDataSet.Tables.Add();
		oDataTable.Columns.Add("STAGE_SEQ",Type.GetType("System.String"));
		oDataTable.Columns.Add("TOWN_SEQ",Type.GetType("System.String"));
		oDataTable.Columns.Add("TOWN_NM",Type.GetType("System.String"));
		oDataTable.Columns.Add("TOWN_NM_EXTRA",Type.GetType("System.String"));
		oDataTable.Columns.Add("EXP",Type.GetType("System.String"));
		oDataTable.Columns.Add("LOSS_FORCE_COUNT",Type.GetType("System.String"));
		oDataTable.Columns.Add("INCOME_MIN",Type.GetType("System.String"));
		oDataTable.Columns.Add("INCOME_MAX",Type.GetType("System.String"));
		oDataTable.Columns.Add("TARGET_COUNT",Type.GetType("System.String"));
		oDataTable.Columns.Add("EXEC_COUNT",Type.GetType("System.String"));
		oDataTable.Columns.Add("TOWN_CLEAR_FLAG",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_SEQ01",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_SEQ02",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_SEQ03",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_COUNT01",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_COUNT02",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_COUNT03",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_POSSESSION_FLAG",Type.GetType("System.String"));
		oDataTable.Columns.Add("USED_ITEM_NUM",Type.GetType("System.String"));
		oDataTable.Columns.Add("TOWN_CLEAR_GET_ITEM_FLAG",Type.GetType("System.String"));
		oDataTable.Columns.Add("TOWN_CLEAR_GET_ITEM_SEQ",Type.GetType("System.String"));
		
		String sTownSeq = String.Empty;
		DataRow oDataRow = oDataTable.NewRow();
		int iItem = 0;
		int iRec = 0;
		
		for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
			DataRow dr = ds.Tables[0].Rows[i];
			
			if (!sTownSeq.Equals(dr["TOWN_SEQ"].ToString())) {
				iRec++;
				
				if(iRec > pNeedCount) {
					break;
				}
				
				iItem = 0;
				oDataRow = oDataTable.Rows.Add();
				oDataRow["STAGE_SEQ"] = dr["STAGE_SEQ"];
				oDataRow["TOWN_SEQ"] = dr["TOWN_SEQ"];
				oDataRow["TOWN_NM"] = dr["TOWN_NM"];
				oDataRow["TOWN_NM_EXTRA"] = dr["TOWN_NM_EXTRA"];
				oDataRow["EXP"] = dr["EXP"];
				oDataRow["LOSS_FORCE_COUNT"] = dr["LOSS_FORCE_COUNT"];
				oDataRow["INCOME_MIN"] = dr["INCOME_MIN"];
				oDataRow["INCOME_MAX"] = dr["INCOME_MAX"];
				oDataRow["TARGET_COUNT"] = dr["TARGET_COUNT"];
				oDataRow["EXEC_COUNT"] = dr["EXEC_COUNT"];
				oDataRow["TOWN_CLEAR_FLAG"] = dr["TOWN_CLEAR_FLAG"];
				oDataRow["USED_ITEM_POSSESSION_FLAG"] = ViCommConst.FLAG_ON_STR;
				oDataRow["TOWN_CLEAR_GET_ITEM_FLAG"] = ViCommConst.FLAG_OFF_STR;
				
				if (dr["USED_ITEM_SEQ"].ToString().Equals("")) {
					oDataRow["USED_ITEM_NUM"] = "0";
				} else {
					iItem += 1;
					oDataRow["USED_ITEM_NUM"] = iItem.ToString();
					oDataRow["USED_ITEM_SEQ01"] = dr["USED_ITEM_SEQ"];
					oDataRow["USED_ITEM_COUNT01"] = dr["USED_ITEM_COUNT"];

					if (dr["USED_ITEM_POSSESSION_COUNT"].ToString().Equals("")) {
						oDataRow["USED_ITEM_POSSESSION_FLAG"] = ViCommConst.FLAG_OFF_STR;
						if (string.IsNullOrEmpty(dr["GAME_ITEM_GET_CD"].ToString())) {
							oDataRow["TOWN_CLEAR_GET_ITEM_FLAG"] = ViCommConst.FLAG_ON_STR;
							oDataRow["TOWN_CLEAR_GET_ITEM_SEQ"] = dr["USED_ITEM_SEQ"];
						}
						
					} else if (int.Parse(dr["USED_ITEM_COUNT"].ToString()) > int.Parse(dr["USED_ITEM_POSSESSION_COUNT"].ToString())) {
						oDataRow["USED_ITEM_POSSESSION_FLAG"] = ViCommConst.FLAG_OFF_STR;
						if (string.IsNullOrEmpty(dr["GAME_ITEM_GET_CD"].ToString())) {
							oDataRow["TOWN_CLEAR_GET_ITEM_FLAG"] = ViCommConst.FLAG_ON_STR;
							oDataRow["TOWN_CLEAR_GET_ITEM_SEQ"] = dr["USED_ITEM_SEQ"];
						}
					}
				}
			} else {
				iItem += 1;
				oDataRow["USED_ITEM_NUM"] = iItem.ToString();
				oDataRow[String.Format("USED_ITEM_SEQ{0:d2}",iItem)] = dr["USED_ITEM_SEQ"];
				oDataRow[String.Format("USED_ITEM_COUNT{0:d2}",iItem)] = dr["USED_ITEM_COUNT"];

				if (dr["USED_ITEM_POSSESSION_COUNT"].ToString().Equals("")) {
					oDataRow["USED_ITEM_POSSESSION_FLAG"] = ViCommConst.FLAG_OFF_STR;
					if (string.IsNullOrEmpty(dr["GAME_ITEM_GET_CD"].ToString())) {
						oDataRow["TOWN_CLEAR_GET_ITEM_FLAG"] = ViCommConst.FLAG_ON_STR;
						if (string.IsNullOrEmpty(oDataRow["TOWN_CLEAR_GET_ITEM_SEQ"].ToString())) {
							oDataRow["TOWN_CLEAR_GET_ITEM_SEQ"] = dr["USED_ITEM_SEQ"];
						}
					}
					
				} else if (int.Parse(dr["USED_ITEM_COUNT"].ToString()) > int.Parse(dr["USED_ITEM_POSSESSION_COUNT"].ToString())) {
					oDataRow["USED_ITEM_POSSESSION_FLAG"] = ViCommConst.FLAG_OFF_STR;
					if (string.IsNullOrEmpty(dr["GAME_ITEM_GET_CD"].ToString())) {
						oDataRow["TOWN_CLEAR_GET_ITEM_FLAG"] = ViCommConst.FLAG_ON_STR;
						if (string.IsNullOrEmpty(oDataRow["TOWN_CLEAR_GET_ITEM_SEQ"].ToString())) {
							oDataRow["TOWN_CLEAR_GET_ITEM_SEQ"] = dr["USED_ITEM_SEQ"];
						}
					}
				}
			}
			
			sTownSeq = dr["TOWN_SEQ"].ToString();
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(TownSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.StageSeq)) {
			SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_SEQ",pCondition.StageSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.TownSeq)) {
			SysPrograms.SqlAppendWhere(" TOWN_SEQ = :TOWN_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TOWN_SEQ",pCondition.TownSeq));
		}

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereNextTown(TownSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.StageSeq)) {
			SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_SEQ",pCondition.StageSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.TownSeq)) {
			SysPrograms.SqlAppendWhere(" TOWN_SEQ <> :TOWN_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TOWN_SEQ",pCondition.TownSeq));
		}

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereStage(TownSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.StageSeq)) {
			SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_SEQ",pCondition.StageSeq));
		}

		return oParamList.ToArray();
	}


	private OracleParameter[] CreateWhereProgress(TownSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();
		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(TownSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case (PwViCommConst.TownSort.HI):
				sSortExpression = "ORDER BY TOWN_LEVEL DESC";
				break;
			case (PwViCommConst.TownSort.LOW):
				sSortExpression = "ORDER BY TOWN_LEVEL ASC";
				break;
			default:
				sSortExpression = "ORDER BY TOWN_LEVEL ASC";
				break;
		}

		return sSortExpression;
	}

	private void AppendPossession(DataSet pDS,TownSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		List<string> sUsedItemSeqList = new List<string>();

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			if (!pDR["USED_ITEM_SEQ"].ToString().Equals("")) {
				if(!sUsedItemSeqList.Contains(pDR["USED_ITEM_SEQ"].ToString())) {
				 	sUsedItemSeqList.Add(pDR["USED_ITEM_SEQ"].ToString());
				}
			}
		}

		if (sUsedItemSeqList.Count == 0) {
			return;
		}

		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	SITE_CD			,		");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	,		");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT		");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	T_POSSESSION_GAME_ITEM	");

		// where
		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		SysPrograms.SqlAppendWhere(string.Format(" GAME_ITEM_SEQ IN ({0})",string.Join(",",sUsedItemSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		subDS.Tables[0].PrimaryKey = new DataColumn[] { subDS.Tables[0].Columns["GAME_ITEM_SEQ"] };

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			if (subDS.Tables[0].Rows.Find(pDR["USED_ITEM_SEQ"]) != null) {
				DataRow subDR = subDS.Tables[0].Rows.Find(pDR["USED_ITEM_SEQ"]);
				pDR["USED_ITEM_POSSESSION_COUNT"] = subDR["POSSESSION_COUNT"];
			}
		}
	}

	public bool CheckTownCrear(string sSiteCd,string sUserSeq,string sUserCharNo,string sSexCd,string sStageSeq) {
		string sTownCount;
		string sTownClearFlagCount;
		
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(TOWN_SEQ)	TOWN_COUNT	");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	T_TOWN						");

		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SEX_CD",sSexCd));

		SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":STAGE_SEQ",sStageSeq));

		oSqlBuilder.AppendLine(sWhereClause);

		DataSet dsSub1 = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		DataRow drSub1 = dsSub1.Tables[0].Rows[0];

		sTownCount = drSub1["TOWN_COUNT"].ToString();

		sWhereClause = string.Empty;
		oParamList = new List<OracleParameter>();
		oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(TOWN_SEQ)	TOWN_CLEAR_COUNT	");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_TOWN_PROGRESS						");

		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));

		SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":STAGE_SEQ",sStageSeq));

		SysPrograms.SqlAppendWhere(" TOWN_CLEAR_FLAG = :TOWN_CLEAR_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TOWN_CLEAR_FLAG",ViCommConst.FLAG_ON_STR));

		oSqlBuilder.AppendLine(sWhereClause);

		DataSet dsSub2 = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		DataRow drSub2 = dsSub2.Tables[0].Rows[0];

		sTownClearFlagCount = drSub2["TOWN_CLEAR_COUNT"].ToString();

		if (int.Parse(sTownCount) > int.Parse(sTownClearFlagCount)) {
			return false;
		} else {
			return true;
		}
	}

	public void ClearMission(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pStageSeq,
		string pTownSeq,
		int? pItemAbsoluteGetFlag,
		int? pTreasureAbsoluteGetFlag,
		out int pLevelUpFLag,
		out int pTownFirstClearFlag,
		out int pAddForceCount,
		out string pGetTreasureLogSeq,
		out string[] pGetGameItemSeq,
		out string[] pGetGameItemCount,
		out int pGetRecordCount,
		out string[] pBreakGameItemSeq,
		out int pBreakRecordCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CLEAR_MISSION");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.VARCHAR2,pStageSeq);
			db.ProcedureInParm("pTOWN_SEQ",DbSession.DbType.VARCHAR2,pTownSeq);
			db.ProcedureInParm("pITEM_ABSOLUTE_GET_FLAG",DbSession.DbType.NUMBER,pItemAbsoluteGetFlag);
			db.ProcedureInParm("pTREASURE_ABSOLUTE_GET_FLAG",DbSession.DbType.NUMBER,pTreasureAbsoluteGetFlag);
			db.ProcedureOutParm("pLEVEL_UP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pTOWN_FIRST_CLEAR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pADD_FORCE_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pGET_TREASURE_LOG_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pGET_GAME_ITEM_SEQ",DbSession.DbType.ARRAY_VARCHAR2);
			db.ProcedureOutArrayParm("pGET_GAME_ITEM_COUNT",DbSession.DbType.ARRAY_VARCHAR2);
			db.ProcedureOutParm("pGET_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pBREAK_GAME_ITEM_SEQ",DbSession.DbType.ARRAY_VARCHAR2);
			db.ProcedureOutParm("pBREAK_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pLevelUpFLag = db.GetIntValue("pLEVEL_UP_FLAG");
			pTownFirstClearFlag = db.GetIntValue("pTOWN_FIRST_CLEAR_FLAG");
			pAddForceCount = db.GetIntValue("pADD_FORCE_COUNT");
			pGetTreasureLogSeq = db.GetStringValue("pGET_TREASURE_LOG_SEQ");

			pGetGameItemSeq = new string[db.GetArrySize("pGET_GAME_ITEM_SEQ")];
			pGetGameItemCount = new string[db.GetArrySize("pGET_GAME_ITEM_COUNT")];
			pBreakGameItemSeq = new string[db.GetArrySize("pBREAK_GAME_ITEM_SEQ")];
			
			for (int i = 0;i < db.GetArrySize("pGET_GAME_ITEM_SEQ");i++) {
				pGetGameItemSeq[i] = db.GetArryStringValue("pGET_GAME_ITEM_SEQ",i);
			}

			for (int i = 0; i < db.GetArrySize("pGET_GAME_ITEM_COUNT"); i++) {
				pGetGameItemCount[i] = db.GetArryStringValue("pGET_GAME_ITEM_COUNT",i);
			}
			
			pGetRecordCount = db.GetIntValue("pGET_RECORD_COUNT");

			for (int i = 0;i < db.GetArrySize("pBREAK_GAME_ITEM_SEQ");i++) {
				pBreakGameItemSeq[i] = db.GetArryStringValue("pBREAK_GAME_ITEM_SEQ",i);
			}

			pBreakRecordCount = db.GetIntValue("pBREAK_RECORD_COUNT");
			pResult = db.GetStringValue("pRESULT");
		}			
	}

	public void GetTreasure(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pGetTreasureLogSeq,
		out int pTreasureCompleteFlag,
		out int pBonusGetFlag,
		out string pResult
	) {
		int pGetCompCountItemFlag;
		string[] pGetCompCountItemSeq;
		string[] pGetCompCountItemCount;
		
		GetTreasure(
			pSiteCd,
			pUserSeq,
			pUserCharNo,
			pGetTreasureLogSeq,
			out pTreasureCompleteFlag,
			out pBonusGetFlag,
			out pGetCompCountItemFlag,
			out pGetCompCountItemSeq,
			out pGetCompCountItemCount,
			out pResult
		);
	}

	public void GetTreasure(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pGetTreasureLogSeq,
		out int pTreasureCompleteFlag,
		out int pBonusGetFlag,
		out int pGetCompCountItemFlag,
		out string[] pGetCompCountItemSeq,
		out string[] pGetCompCountItemCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_TREASURE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pGET_TREASURE_LOG_SEQ",DbSession.DbType.VARCHAR2,pGetTreasureLogSeq);
			db.ProcedureOutParm("pTREASURE_COMPLETE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pBONUS_GET_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pGET_COMP_COUNT_ITEM_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pGET_COMP_COUNT_ITEM_SEQ",DbSession.DbType.ARRAY_VARCHAR2);
			db.ProcedureOutArrayParm("pGET_COMP_COUNT_ITEM_COUNT",DbSession.DbType.ARRAY_VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pTreasureCompleteFlag = db.GetIntValue("pTREASURE_COMPLETE_FLAG");
			pBonusGetFlag = db.GetIntValue("pBONUS_GET_FLAG");
			pGetCompCountItemFlag = db.GetIntValue("pGET_COMP_COUNT_ITEM_FLAG");
			
			pGetCompCountItemSeq = new string[db.GetArrySize("pGET_COMP_COUNT_ITEM_SEQ")];
			pGetCompCountItemCount = new string[db.GetArrySize("pGET_COMP_COUNT_ITEM_SEQ")];

			for (int i = 0;i < db.GetArrySize("pGET_COMP_COUNT_ITEM_SEQ");i++) {
				pGetCompCountItemSeq[i] = db.GetArryStringValue("pGET_COMP_COUNT_ITEM_SEQ",i);
				pGetCompCountItemCount[i] = db.GetArryStringValue("pGET_COMP_COUNT_ITEM_COUNT",i);
			}
			
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public string GetGameTownIncomeHugBonus(string siteCd,string pGameTownIncome) {
		
		string sHugBonusRate = null;
		int iValue = 0;
		
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(pGameTownIncome);
		if (!rgxMatch.Success) {
			return iValue.ToString();
		}

		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sHugBonusRate = ds.Tables[0].Rows[0]["HUG_BONUS_RATE"].ToString();
			}
		}

		Regex rgx2 = new Regex("^[0-9]+$");
		Match rgx2Match = rgx2.Match(sHugBonusRate);
		if (!rgx2Match.Success) {
			return iValue.ToString();
		}

		iValue = int.Parse(pGameTownIncome) * (int.Parse(sHugBonusRate) / 100);

		return iValue.ToString();
	}

	public DataSet GetNextTown(TownSeekCondition pCondition) {


		string sWhereClause = string.Empty;
		string sWhereClauseProgress = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ						,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_SEQ						,	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_NM						,	");
		oSqlBuilder.AppendLine("	PROGRESS.TOWN_CLEAR_FLAG				");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine(" (								");
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	SITE_CD				,		");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,		");
		oSqlBuilder.AppendLine("	TOWN_SEQ			,		");
		oSqlBuilder.AppendLine("	TOWN_NM				,		");
		oSqlBuilder.AppendLine("	TOWN_NM_EXTRA				");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	T_TOWN						");
		// where
		oParamList.AddRange(this.CreateWhereNextTown(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" )	TOWN,						");

		oSqlBuilder.AppendLine(" (						");
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD			,	");
		oSqlBuilder.AppendLine("	STAGE_SEQ		,	");
		oSqlBuilder.AppendLine("	TOWN_SEQ		,	");
		oSqlBuilder.AppendLine("	TOWN_CLEAR_FLAG		");
		oSqlBuilder.AppendLine(" FROM					");
		oSqlBuilder.AppendLine("	T_TOWN_PROGRESS		");
		// where
		oParamList.AddRange(this.CreateWhereProgress(pCondition,ref sWhereClauseProgress));
		oSqlBuilder.AppendLine(sWhereClauseProgress);
		oSqlBuilder.AppendLine(" )	PROGRESS			");

		oSqlBuilder.AppendLine(" WHERE					");
		oSqlBuilder.AppendLine("	TOWN.SITE_CD				= PROGRESS.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.STAGE_SEQ				= PROGRESS.STAGE_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("	TOWN.TOWN_SEQ				= PROGRESS.TOWN_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	(PROGRESS.TOWN_CLEAR_FLAG   = 0             			    OR	");
		oSqlBuilder.AppendLine("	 PROGRESS.TOWN_CLEAR_FLAG IS NULL             			  ) AND	");
		oSqlBuilder.AppendLine("	ROWNUM <= 1  													");
		oSqlBuilder.AppendLine(" ORDER BY TOWN.TOWN_SEQ												");

		DataSet oTempDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oTempDataSet.Tables[0].Rows.Count == 0) {
		
			sWhereClause = string.Empty;
			oSqlBuilder = new StringBuilder();
			oParamList = new List<OracleParameter>();
			
			oSqlBuilder.AppendLine("SELECT							");
			oSqlBuilder.AppendLine("	STAGE_SEQ	,				");
			oSqlBuilder.AppendLine("	STAGE_NM	,				");
			oSqlBuilder.AppendLine("	BOSS_NM						");
			oSqlBuilder.AppendLine(" FROM							");
			oSqlBuilder.AppendLine("	T_STAGE						");

			// where
			oParamList.AddRange(this.CreateWhereStage(pCondition,ref sWhereClause));
			oSqlBuilder.AppendLine(sWhereClause);

			oTempDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		}
		
		return oTempDataSet;
	}
	
	public string GetGameTutorialTownSeq(TownSeekCondition oCondition) {
		string sValue = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
			oSqlBuilder.AppendLine("SELECT															");
			oSqlBuilder.AppendLine("	*															");
			oSqlBuilder.AppendLine("FROM															");
			oSqlBuilder.AppendLine("	(															");
			oSqlBuilder.AppendLine("		SELECT													");
			oSqlBuilder.AppendLine("			INNER.*											,	");
			oSqlBuilder.AppendLine("			ROWNUM AS RNUM										");
			oSqlBuilder.AppendLine("		FROM													");
			oSqlBuilder.AppendLine("			(													");
			oSqlBuilder.AppendLine("				SELECT											");
			oSqlBuilder.AppendLine("					TOWN_SEQ									");
			oSqlBuilder.AppendLine("				FROM											");
			oSqlBuilder.AppendLine("					T_TOWN										");
			oSqlBuilder.AppendLine("				WHERE											");
			oSqlBuilder.AppendLine("					SITE_CD		= :SITE_CD					AND	");
			oSqlBuilder.AppendLine("					SEX_CD		= :SEX_CD					AND	");
			oSqlBuilder.AppendLine("					STAGE_SEQ	=								");
			oSqlBuilder.AppendLine("					(											");
			oSqlBuilder.AppendLine("						SELECT									");
			oSqlBuilder.AppendLine("							STAGE_SEQ							");
			oSqlBuilder.AppendLine("						FROM									");
			oSqlBuilder.AppendLine("							T_STAGE								");
			oSqlBuilder.AppendLine("						WHERE									");
			oSqlBuilder.AppendLine("							SITE_CD		= :SITE_CD			AND	");
			oSqlBuilder.AppendLine("							SEX_CD		= :SEX_CD			AND	");
			oSqlBuilder.AppendLine("							STAGE_LEVEL	=						");
			oSqlBuilder.AppendLine("							(									");
			oSqlBuilder.AppendLine("								SELECT							");
			oSqlBuilder.AppendLine("									MIN(STAGE_LEVEL)			");
			oSqlBuilder.AppendLine("								FROM							");
			oSqlBuilder.AppendLine("									T_STAGE						");
			oSqlBuilder.AppendLine("								WHERE							");
			oSqlBuilder.AppendLine("									SITE_CD	= :SITE_CD		AND	");
			oSqlBuilder.AppendLine("									SEX_CD	= :SEX_CD			");
			oSqlBuilder.AppendLine("							)									");
			oSqlBuilder.AppendLine("					)											");
			oSqlBuilder.AppendLine("				ORDER BY TOWN_LEVEL ASC							");
			oSqlBuilder.AppendLine("			) INNER												");
			oSqlBuilder.AppendLine("	)															");
			oSqlBuilder.AppendLine("WHERE															");
			oSqlBuilder.AppendLine("	RNUM = 1													");
			
			oParamList.Add(new OracleParameter(":SITE_CD",oCondition.SiteCd));
			oParamList.Add(new OracleParameter(":SEX_CD",oCondition.SexCd));

			DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
			
			if(oDataSet.Tables[0].Rows.Count > 0) {
				sValue = oDataSet.Tables[0].Rows[0]["TOWN_SEQ"].ToString();
			}
			
			return sValue;
	}
}
