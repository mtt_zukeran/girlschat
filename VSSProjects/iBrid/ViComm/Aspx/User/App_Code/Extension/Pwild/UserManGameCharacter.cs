﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 男性ﾕｰｻﾞｰ
--	Progaram ID		: UserManGameCharacter
--
--  Creation Date	: 2011.09.28
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class UserManGameCharacterSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	
	public string SelfUserSeq;
	
	public string SelfUserCharNo;

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public UserManGameCharacterSeekCondition() : this(new NameValueCollection()) {
	}
	public UserManGameCharacterSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["sort"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sort"]));
	}

}

public class UserManGameCharacter:DbSession {
	public UserManGameCharacter() {
	}

	public void GetSeqForMission(string pSiteCd,string pUserSeq,string pUserCharNo,string pAllFlag,out string pPartnerUserSeq,out string pPartnerUserCharNo) {
		pPartnerUserSeq = string.Empty;
		pPartnerUserCharNo = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("TUTORIAL_MISSION_FLAG = :TUTORIAL_MISSION_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("TUTORIAL_MISSION_TREASURE_FLAG = :TUTORIAL_MISSION_TREASURE_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("TUTORIAL_BATTLE_FLAG = :TUTORIAL_BATTLE_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("TUTORIAL_BATTLE_TREASURE_FLAG = :TUTORIAL_BATTLE_TREASURE_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND PARTNER_USER_SEQ = P.USER_SEQ AND PARTNER_USER_CHAR_NO = P.USER_CHAR_NO)",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pUserCharNo));

		if (!pAllFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("LAST_LOGIN_DATE > SYSDATE-1",ref sWhereClause);
		}

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	*									");
		oSqlBuilder.AppendLine("FROM (									");
		oSqlBuilder.AppendLine("	SELECT								");
		oSqlBuilder.AppendLine("		USER_SEQ					,	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_MAN_GAME_CHARACTER01 P	");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	ORDER BY DBMS_RANDOM.RANDOM			");
		oSqlBuilder.AppendLine(") INNER									");
		oSqlBuilder.AppendLine("WHERE ROWNUM <= 1						");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			pPartnerUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
			pPartnerUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		}
	}

	public DataSet GetOneOuter(UserManGameCharacterSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	INNER.*											,	");
		oSqlBuilder.AppendLine("	ROWNUM AS RNUM										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(SELECT												");
		oSqlBuilder.AppendLine("		UMC.HANDLE_NM								,	");
		oSqlBuilder.AppendLine("		UMC.USER_SEQ								,	");
		oSqlBuilder.AppendLine("		UMC.USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine("		LOGIN_ID									,	");
		oSqlBuilder.AppendLine("		UMC.TOTAL_TALK_COUNT						,	");
		oSqlBuilder.AppendLine("		UMC.TOTAL_RX_MAIL_COUNT						,	");
		oSqlBuilder.AppendLine("		UMC.TOTAL_TX_MAIL_COUNT						,	");
		oSqlBuilder.AppendLine("		UMC.AGE										,	");
		oSqlBuilder.AppendLine("		UMC.MOBILE_ONLINE_STATUS_NM					,	");
		oSqlBuilder.AppendLine("		UMC.CHARACTER_ONLINE_STATUS					,	");
		oSqlBuilder.AppendLine("		UMC.SMALL_PHOTO_IMG_PATH					,	");
		oSqlBuilder.AppendLine("		EX.SITE_USE_STATUS							,	");
		oSqlBuilder.AppendLine("		GC.GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("		GC.GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("		GC.GAME_CHARACTER_LEVEL						,	");
		oSqlBuilder.AppendLine("		REC.RECORDING_DATE AS MAN_PF_RECORDING_DATE					,	");
		oSqlBuilder.AppendLine("		REC.FOWARD_TERM_SCH_DATE AS MAN_PF_FOWARD_TERM_SCH_DATE		,	");
		oSqlBuilder.AppendLine("		REC.WAITING_TYPE AS MAN_PF_WAITING_TYPE							");
		oSqlBuilder.AppendLine("	FROM												");
		oSqlBuilder.AppendLine("		(SELECT											");
		oSqlBuilder.AppendLine("			P.SITE_CD								,	");
		oSqlBuilder.AppendLine("			P.USER_SEQ								,	");
		oSqlBuilder.AppendLine("			P.USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine("			P.HANDLE_NM								,	");
		oSqlBuilder.AppendLine("			P.LOGIN_ID								,	");
		oSqlBuilder.AppendLine("			P.TOTAL_TALK_COUNT						,	");
		oSqlBuilder.AppendLine("			P.TOTAL_RX_MAIL_COUNT					,	");
		oSqlBuilder.AppendLine("			P.TOTAL_TX_MAIL_COUNT					,	");
		oSqlBuilder.AppendLine("			P.AGE									,	");
		oSqlBuilder.AppendLine("			CASE										");
		oSqlBuilder.AppendLine("				WHEN P.CHARACTER_ONLINE_STATUS = 0		");
		oSqlBuilder.AppendLine("				THEN TSM.OFFLINE_GUIDANCE				");
		oSqlBuilder.AppendLine("				WHEN P.CHARACTER_ONLINE_STATUS = 1		");
		oSqlBuilder.AppendLine("				THEN TSM.LOGINED_GUIDANCE				");
		oSqlBuilder.AppendLine("				WHEN P.CHARACTER_ONLINE_STATUS = 2		");
		oSqlBuilder.AppendLine("				THEN TSM.WAITING_GUIDANCE				");
		oSqlBuilder.AppendLine("				WHEN P.CHARACTER_ONLINE_STATUS = 3		");
		oSqlBuilder.AppendLine("				THEN TSM.TALKING_WSHOT_GUIDANCE			");
		oSqlBuilder.AppendLine("			END AS MOBILE_ONLINE_STATUS_NM			,	");
		oSqlBuilder.AppendLine("			P.CHARACTER_ONLINE_STATUS				,	");
		oSqlBuilder.AppendLine("			P.REC_SEQ								,	");
		oSqlBuilder.AppendLine("			GET_MAN_SMALL_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,P.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			VW_USER_MAN_CHARACTER00 P				,	");
		oSqlBuilder.AppendLine("			T_SITE_MANAGEMENT TSM						");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			P.SITE_CD		= TSM.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			P.SITE_CD		= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			P.USER_SEQ		= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			P.USER_CHAR_NO	= :USER_CHAR_NO				");
		oSqlBuilder.AppendLine("		) UMC										,	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER_EX EX					,	");
		oSqlBuilder.AppendLine("		T_GAME_CHARACTER GC							,	");
		oSqlBuilder.AppendLine("		T_RECORDING REC									");
		oSqlBuilder.AppendLine("	WHERE												");
		oSqlBuilder.AppendLine("		UMC.SITE_CD			= EX.SITE_CD			AND	");
		oSqlBuilder.AppendLine("		UMC.USER_SEQ		= EX.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("		UMC.USER_CHAR_NO	= EX.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("		UMC.SITE_CD			= GC.SITE_CD		(+) AND	");
		oSqlBuilder.AppendLine("		UMC.USER_SEQ		= GC.USER_SEQ		(+) AND	");
		oSqlBuilder.AppendLine("		UMC.USER_CHAR_NO	= GC.USER_CHAR_NO	(+)	AND	");
		oSqlBuilder.AppendLine("		UMC.REC_SEQ			= REC.REC_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("		NOT EXISTS													");
		oSqlBuilder.AppendLine("		(															");
		oSqlBuilder.AppendLine("			SELECT													");
		oSqlBuilder.AppendLine("				*													");
		oSqlBuilder.AppendLine("			FROM													");
		oSqlBuilder.AppendLine("				T_REFUSE											");
		oSqlBuilder.AppendLine("			WHERE													");
		oSqlBuilder.AppendLine("				SITE_CD					= UMC.SITE_CD			AND	");
		oSqlBuilder.AppendLine("				USER_SEQ				= UMC.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("				USER_CHAR_NO			= UMC.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("		)															");
		oSqlBuilder.AppendLine("	) INNER												");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendFriendlyPoint(oDataSet,pCondition);

		return oDataSet;
	}
	
	private void AppendFriendlyPoint(DataSet pDS,UserManGameCharacterSeekCondition pCondition) {
		if(pDS.Tables[0].Rows.Count < 1) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("FRIENDLY_RANK"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	RANK													");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"]);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",pCondition.SelfUserSeq);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",pCondition.SelfUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["FRIENDLY_RANK"] = dsSub.Tables[0].Rows[0]["RANK"];
				} else {
					dr["FRIENDLY_RANK"] = null;
				}
			}
		}
		
	}
}
