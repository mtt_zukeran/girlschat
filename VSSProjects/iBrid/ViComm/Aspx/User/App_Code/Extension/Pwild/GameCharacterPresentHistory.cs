﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｹﾞｰﾑｷｬﾗｸﾀｰﾌﾟﾚｾﾞﾝﾄ履歴
--	Progaram ID		: GameCharacterPresentHistory
--
--  Creation Date	: 2011.09.16
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class GameCharacterPresentHistorySeekCondition:SeekConditionBase {
	public string SiteCd;
	public string PartnerUserSeq;
	public string PartnerUserCharNo;

	public string PresentHistorySeq {
		get {
			return this.Query["present_history_seq"];
		}
		set {
			this.Query["present_history_seq"] = value;
		}
	}

	public string GetFlag {
		get {
			return this.Query["get_flag"];
		}
		set {
			this.Query["get_flag"] = value;
		}
	}

	public GameCharacterPresentHistorySeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameCharacterPresentHistorySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["present_history_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["present_history_seq"]));
		this.Query["get_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["get_flag"]));
	}
}

public class GameCharacterPresentHistory:DbSession {

	public int GetPageCount(GameCharacterPresentHistorySeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("		" + CreateTable(pCondtion) + "	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" AND ROWNUM <= 20					");
		
		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameCharacterPresentHistorySeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	" + CreateField(pCondtion) +	"	,	");
		oSqlBuilder.AppendLine("	0 AS REC_NO_PER_PAGE					");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	(SELECT									");
		oSqlBuilder.AppendLine("		" + CreateField(pCondtion) + "		");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("		" + CreateTable(pCondtion) + "		");
		
		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	)	WHERE ROWNUM <= 20  				");
		
		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		int iRecNoPerPage = 0;
		if(oDataSet.Tables[0].Rows.Count > 0) {
			foreach(DataRow oDR in oDataSet.Tables[0].Rows) {
				iRecNoPerPage = (int.Parse(oDR["RNUM"].ToString()) - iStartIndex);
				oDR["REC_NO_PER_PAGE"] = iRecNoPerPage.ToString();
			}
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(GameCharacterPresentHistorySeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		SysPrograms.SqlAppendWhere(" PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));

		SysPrograms.SqlAppendWhere(" PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));

		if (!string.IsNullOrEmpty(pCondition.PresentHistorySeq)) {
			SysPrograms.SqlAppendWhere(" PRESENT_HISTORY_SEQ = :PRESENT_HISTORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRESENT_HISTORY_SEQ",pCondition.PresentHistorySeq));			
		}

		if (!string.IsNullOrEmpty(pCondition.GetFlag)) {
			SysPrograms.SqlAppendWhere(" GET_FLAG = :GET_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GET_FLAG",pCondition.GetFlag));
		}

		SysPrograms.SqlAppendWhere("(NA_FLAG IN (:NA_FLAG,:NA_FLAG_2))",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO)",ref pWhereClause);
		
		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(GameCharacterPresentHistorySeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY CREATE_DATE DESC ";
		
		return sSortExpression;
	}

	private string CreateField(GameCharacterPresentHistorySeekCondition pCondition) {
		StringBuilder sSql = new StringBuilder();

		if (pCondition.SeekMode == PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY) {
			sSql.AppendLine("USER_SEQ				,	");
			sSql.AppendLine("USER_CHAR_NO			,	");
			sSql.AppendLine("GAME_HANDLE_NM			,	");
			sSql.AppendLine("GAME_CHARACTER_TYPE	,	");
			sSql.AppendLine("HANDLE_NM				,	");
			sSql.AppendLine("AGE					,	");
			sSql.AppendLine("CREATE_DATE			,	");
			sSql.AppendLine("PRESENT_HISTORY_SEQ	,	");
			sSql.AppendLine("GET_FLAG				,	");
			sSql.AppendLine("ITEM_COUNT				,	");
			sSql.AppendLine("PRESENT_GAME_ITEM_SEQ	,	");
			sSql.AppendLine("GAME_ITEM_NM			,	");
			sSql.AppendLine("ATTACK_POWER			,	");
			sSql.AppendLine("DEFENCE_POWER			,	");
			sSql.AppendLine("ENDURANCE_NM			,	");
			sSql.AppendLine("SMALL_PHOTO_IMG_PATH	,	");
			sSql.AppendLine("ACT_CATEGORY_IDX		,	");
			sSql.AppendLine("LOGIN_ID				,	");
			sSql.AppendLine("CRYPT_VALUE				");
		} else {
			sSql.AppendLine("USER_SEQ				,	");
			sSql.AppendLine("USER_CHAR_NO			,	");
			sSql.AppendLine("GAME_HANDLE_NM			,	");
			sSql.AppendLine("GAME_CHARACTER_TYPE	,	");
			sSql.AppendLine("NULL AS HANDLE_NM		,	");
			sSql.AppendLine("AGE					,	");
			sSql.AppendLine("CREATE_DATE			,	");
			sSql.AppendLine("PRESENT_HISTORY_SEQ	,	");
			sSql.AppendLine("GET_FLAG				,	");
			sSql.AppendLine("ITEM_COUNT				,	");
			sSql.AppendLine("PRESENT_GAME_ITEM_SEQ	,	");
			sSql.AppendLine("GAME_ITEM_NM			,	");
			sSql.AppendLine("ATTACK_POWER			,	");
			sSql.AppendLine("DEFENCE_POWER			,	");
			sSql.AppendLine("ENDURANCE_NM			,	");
			sSql.AppendLine("LOGIN_ID				,	");
			sSql.AppendLine("SITE_USE_STATUS		,	");
			sSql.AppendLine("SMALL_PHOTO_IMG_PATH	,	");
			sSql.AppendLine("PRESENT_POINT			,	");
			sSql.AppendLine("GAME_ITEM_CATEGORY_TYPE	");
		}

		return sSql.ToString();
	}

	private string CreateTable(GameCharacterPresentHistorySeekCondition pCondition) {
		StringBuilder sSql = new StringBuilder();

		if (pCondition.SeekMode == PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY) {
			sSql.AppendLine("VW_PW_CAST_GAME_PRE_HISTORY01 P");	
		} else {
			sSql.AppendLine("VW_PW_MAN_GAME_PRE_HISTORY01 P");
		}

		return sSql.ToString();
	}

	public int GetPartnerPresentUncheckedCount(GameCharacterPresentHistorySeekCondition pCondtion) {
		int iCount = 0;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	 GET_FLAG								");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	(SELECT									");
		oSqlBuilder.AppendLine("		GET_FLAG							");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("		" + CreateTable(pCondtion) + "		");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	)	WHERE ROWNUM <= 20  				");

		string sExecSql = string.Empty;

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		iCount = GetPresentCount(oDataSet);
		
		return iCount;
	}

	private int GetPresentCount(DataSet pDS) {

		int iCount = 0;
		
		if (pDS.Tables[0].Rows.Count <= 0) {
			return iCount;
		}
		
		foreach (DataRow dr in pDS.Tables[0].Rows) {
			
			if (dr["GET_FLAG"].ToString() == ViCommConst.FLAG_OFF_STR) {
				iCount ++ ;
			}

		}

		return iCount;
	}
	
	public string GetPresentGameItem(string pSiteCd,string pPresentHistorySeq) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_PRESENT_GAME_ITEM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pPRESENT_HISTORY_SEQ",DbSession.DbType.VARCHAR2,pPresentHistorySeq);
			db.ProcedureOutParm("pPRESENT_POINT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public DataSet GetOnePresentHistory(GameCharacterPresentHistorySeekCondition pCondtion) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	" + CreateField(pCondtion) + "			");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	(SELECT									");
		oSqlBuilder.AppendLine("		" + CreateField(pCondtion) + "		");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("		" + CreateTable(pCondtion) + "		");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	)	WHERE ROWNUM <= 20	  				");


		DataSet oTmpDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		DataSet oDataSet = new DataSet();
		oDataSet = new DataSet();
		oDataSet = oTmpDataSet.Clone();

		AppendPresentHistory(oDataSet,oTmpDataSet);
		
		return oDataSet;
	}

	private void AppendPresentHistory(DataSet pDS,DataSet pTmpDS) {

		DataRow drsub = null;
		
		if (pTmpDS.Tables[0].Rows.Count <= 0) {
			return;
		}
		
		foreach (DataRow dr in pTmpDS.Tables[0].Rows) {
			
			if (dr["GET_FLAG"].ToString() == ViCommConst.FLAG_OFF_STR) {

				drsub = null;
				drsub = pDS.Tables[0].Rows.Add();

				drsub["GAME_HANDLE_NM"] = dr["GAME_HANDLE_NM"].ToString();
				drsub["HANDLE_NM"] = dr["HANDLE_NM"].ToString();
				drsub["CREATE_DATE"] = dr["CREATE_DATE"].ToString();
				return;
			}
		}
	}
}

