﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サイト構成拡張
--	Progaram ID		: SiteManagementEx
--  Creation Date	: 2015.05.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class SiteManagementEx:DbSession {
	public string latestGcappVeriOS = string.Empty;
	public string latestGcappVerAndroid = string.Empty;
	public string loginReminderSmsDoc = string.Empty;

	public SiteManagementEx() {
	}

	public bool GetOne(string pSiteCd) {
		bool bExist = false;

		if (string.IsNullOrEmpty(pSiteCd)) {
			return bExist;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	LATEST_GCAPP_VER_IOS,");
		oSqlBuilder.AppendLine("	LATEST_GCAPP_VER_ANDROID,");
		oSqlBuilder.AppendLine("	LOGIN_REMINDER_SMS_DOC");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			DataRow dr = oDataSet.Tables[0].Rows[0];
			latestGcappVeriOS = iBridUtil.GetStringValue(dr["LATEST_GCAPP_VER_IOS"]);
			latestGcappVerAndroid = iBridUtil.GetStringValue(dr["LATEST_GCAPP_VER_ANDROID"]);
			loginReminderSmsDoc = iBridUtil.GetStringValue(dr["LOGIN_REMINDER_SMS_DOC"]);
			bExist = true;
		}

		return bExist;
	}
}
