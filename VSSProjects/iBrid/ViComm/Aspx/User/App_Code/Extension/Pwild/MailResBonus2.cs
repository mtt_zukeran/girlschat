﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メール返信ボーナス2
--	Progaram ID		: MailResBonus2
--
--  Creation Date	: 2016.09.05
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class MailResBonus2:DbSession {
	public MailResBonus2() {
	}

	/// <summary>
	/// イベント情報を取得する
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <returns></returns>
	public DataRow GetData(
		string pSiteCd
	) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oParamList;

		DataSet ds;
		DataRow dr = null;
		try {
			conn = DbConnect();

			oSqlBuilder.AppendLine("SELECT");
			oSqlBuilder.AppendLine("	SITE_CD,");
			oSqlBuilder.AppendLine("	MAIL_RES_BONUS_SEQ,");
			oSqlBuilder.AppendLine("	RES_LIMIT_SEC1,");
			oSqlBuilder.AppendLine("	RES_LIMIT_SEC2,");
			oSqlBuilder.AppendLine("	UNRECEIVED_DAYS,");
			oSqlBuilder.AppendLine("	RES_BONUS_MAX_NUM,");
			oSqlBuilder.AppendLine("	START_DATE,");
			oSqlBuilder.AppendLine("	END_DATE,");
			oSqlBuilder.AppendLine("	UPDATE_DATE,");
			oSqlBuilder.AppendLine("	REVISION_NO");
			oSqlBuilder.AppendLine("FROM");
			oSqlBuilder.AppendLine("	T_MAIL_RES_BONUS2");

			oParamList = CreateWhere(pSiteCd,ref sWhereClause);
			oSqlBuilder.AppendLine(sWhereClause);

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParamList.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParamList[i]);
				}
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
				}
			}
		} finally {
			conn.Close();
		}

		return dr;
	}

	/// <summary>
	/// 検索条件を生成
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhereClause"></param>
	/// <returns></returns>
	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("START_DATE <= SYSDATE",ref pWhereClause);
		SysPrograms.SqlAppendWhere("END_DATE >= SYSDATE",ref pWhereClause);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// 3分以内メール返信ボーナスのイベント情報を設定
	/// </summary>
	/// <param name="userWoman"></param>
	/// <returns>イベント期間内かどうか(0:期間外、1:期間内)</returns>
	public string SetEventData(ref UserWoman userWoman) {
		DataRow dr = GetData(userWoman.CurCharacter.siteCd);

		// イベント期間外
		if (dr == null) {
			return ViCommConst.FLAG_OFF_STR;
		}

		// 1通目の返信期限秒数、2通目以降の返信期限秒数、メール未受信日数、メール返信ボーナス最大返信数を設定
		userWoman.characterList[userWoman.curKey].characterEx.mailResBonusSeq = dr["MAIL_RES_BONUS_SEQ"].ToString();
		userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec1 = dr["RES_LIMIT_SEC1"].ToString();
		userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec2 = dr["RES_LIMIT_SEC2"].ToString();
		userWoman.characterList[userWoman.curKey].characterEx.mailResBonusMaxNum = dr["RES_BONUS_MAX_NUM"].ToString();
		userWoman.characterList[userWoman.curKey].characterEx.mailResBonusUnreceivedDays = dr["UNRECEIVED_DAYS"].ToString();

		return ViCommConst.FLAG_ON_STR;
	}
}
