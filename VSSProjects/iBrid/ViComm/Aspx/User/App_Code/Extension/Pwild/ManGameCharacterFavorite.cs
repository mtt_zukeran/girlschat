﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性ｹﾞｰﾑｷｬﾗｸﾀｰお気に入り
--	Progaram ID		: ManGameCharacterFavorite
--
--  Creation Date	: 2011.09.26
--  Creater			: PW A.Taba
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

[Serializable]
public class ManGameCharacterFavoriteSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq{
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public ManGameCharacterFavoriteSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManGameCharacterFavoriteSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_seq"]));
		this.Query["game_item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_item_seq"]));
	}
}

[System.Serializable]
public class ManGameCharacterFavorite:ManBase {

	public int GetPageCount(ManGameCharacterFavoriteSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHAR_FAVORIT00 P	");

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManGameCharacterFavoriteSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause,ref sWhereClauseFriend));

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	P.*						,	");
		oSqlBuilder.AppendLine(" 	FP.RANK	AS FRIENDLY_RANK,	");
		oSqlBuilder.AppendLine(" 	FP.FRIENDLY_POINT			");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		" + ManBasicField() + "		,				");
		oSqlBuilder.AppendLine("		P.PARTNER_USER_SEQ			,				");
		oSqlBuilder.AppendLine("		P.PARTNER_USER_CHAR_NO		,				");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM			,				");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE		,				");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL		,				");
		oSqlBuilder.AppendLine("		P.GAME_FAVORIT_REGIST_DATE	,				");
		oSqlBuilder.AppendLine("		P.EXP						,				");
		oSqlBuilder.AppendLine("		P.SITE_USE_STATUS			,				");
		oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_FLAG					,	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_TREASURE_FLAG		,	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_FLAG					,	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_TREASURE_FLAG				");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		VW_PW_MAN_GAME_CHAR_FAVORIT00 P	");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" 	) P,								");
		oSqlBuilder.AppendLine(" 	(SELECT								");
		oSqlBuilder.AppendLine(" 		FP.SITE_CD			,			");
		oSqlBuilder.AppendLine(" 		FP.USER_SEQ			,			");
		oSqlBuilder.AppendLine(" 		FP.USER_CHAR_NO		,			");
		oSqlBuilder.AppendLine(" 		FP.RANK				,			");
		oSqlBuilder.AppendLine(" 		FP.FRIENDLY_POINT				");
		oSqlBuilder.AppendLine(" 	FROM								");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00	FP			");
		oSqlBuilder.AppendLine(sWhereClauseFriend);
		oSqlBuilder.AppendLine(" 	) FP								");
		oSqlBuilder.AppendLine(" WHERE									");
		oSqlBuilder.AppendLine(" 	P.SITE_CD				= FP.SITE_CD		(+) AND	");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_SEQ		= FP.USER_SEQ		(+) AND ");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_CHAR_NO	= FP.USER_CHAR_NO	(+)		");

		string sSortExpression = this.CreateOrderExpresion(pCondtion);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManGameCharacterFavoriteSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("P.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		SysPrograms.SqlAppendWhere("P.NA_FLAG IN (:NA_FLAG1,:NA_FLAG2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG1",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhere(ManGameCharacterFavoriteSeekCondition pCondition,ref string pWhereClause,ref string pWhereClauseFriend) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref pWhereClause));

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("FP.SITE_CD = :SITE_CD",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("FP.PARTNER_USER_SEQ = :USER_SEQ",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("FP.PARTNER_USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClauseFriend);
		}

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(ManGameCharacterFavoriteSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.ManGameCharacterFavoriteSort.GAME_CHARACTER_LEVEL_DESC:
				sSortExpression = " ORDER BY P.GAME_CHARACTER_LEVEL DESC, P.EXP DESC, P.PARTNER_USER_SEQ";
				break;
			case PwViCommConst.ManGameCharacterFavoriteSort.LAST_LOGIN_DATE_DESC:
				sSortExpression = " ORDER BY P.LAST_LOGIN_DATE DESC";
				break;
			case PwViCommConst.ManGameCharacterFavoriteSort.GAME_FAVORITE_REGIST_DATE_DESC:
				sSortExpression = " ORDER BY P.GAME_FAVORIT_REGIST_DATE DESC, P.LAST_LOGIN_DATE DESC";
				break;
			case PwViCommConst.ManGameCharacterFavoriteSort.FRIENDLY_RANK_ASC:
				sSortExpression = " ORDER BY FRIENDLY_RANK, FP.FRIENDLY_POINT DESC";
				break;
			default:
				sSortExpression = " ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}
	
	public int GetFavoritViewStatusOfflineCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	COUNT(*)										");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_FAVORIT										");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	LOGIN_VIEW_STATUS	= :LOGIN_VIEW_STATUS		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS",ViCommConst.FLAG_ON_STR));

		int iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		
		return iRecCount;
	}

	public int GetPageCountJealousy(ManGameCharacterFavoriteSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine(" SELECT														");
		oSqlBuilder.AppendLine("	COUNT(*)												");
		oSqlBuilder.AppendLine(" FROM														");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P						,	");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			SITE_CD										,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			GAME_PIC_NON_PUBLISH_DATE						");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			T_FAVORIT										");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :SELF_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :SELF_USER_CHAR_NO			");
		oSqlBuilder.AppendLine("	) F														");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	P.SITE_CD			= F.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ			= F.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO		= F.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG			IN (:NA_FLAG,:NA_FLAG2)			AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG		= :STAFF_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		if (!string.IsNullOrEmpty(pCondtion.UserSeq)) {
			oSqlBuilder.AppendLine(" AND	P.USER_SEQ	= :USER_SEQ	");
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));
			
		}
		
		if (!string.IsNullOrEmpty(pCondtion.UserCharNo)) {
			oSqlBuilder.AppendLine(" AND	P.USER_CHAR_NO	= :USER_CHAR_NO");
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		}

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollectionJealousy(ManGameCharacterFavoriteSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine(" SELECT															");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "									,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE									,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL									,	");
		oSqlBuilder.AppendLine("	F.GAME_PIC_NON_PUBLISH_DATE								,	");
		oSqlBuilder.AppendLine(" 	FP.RANK	AS FRIENDLY_RANK								,	");
		oSqlBuilder.AppendLine(" 	FP.FRIENDLY_POINT											");
		oSqlBuilder.AppendLine(" FROM															");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P							,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ								,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine("			GAME_PIC_NON_PUBLISH_DATE						,	");
		oSqlBuilder.AppendLine("			REGIST_DATE											");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_FAVORIT											");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :SELF_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :SELF_USER_CHAR_NO				");
		oSqlBuilder.AppendLine("	) F														,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine(" 		SELECT													");
		oSqlBuilder.AppendLine(" 			SITE_CD											,	");
		oSqlBuilder.AppendLine(" 			USER_SEQ										,	");
		oSqlBuilder.AppendLine(" 			USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine(" 			RANK											,	");
		oSqlBuilder.AppendLine(" 			FRIENDLY_POINT										");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("	 		MV_FRIENDLY_POINT00									");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	) FP														");
		oSqlBuilder.AppendLine(" WHERE															");
		oSqlBuilder.AppendLine("	P.SITE_CD			= F.SITE_CD							AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ			= F.PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO		= F.PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD			= FP.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ			= FP.USER_SEQ					(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO		= FP.USER_CHAR_NO				(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG			IN (:NA_FLAG,:NA_FLAG2)				AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG		= :STAFF_FLAG						AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		if (!string.IsNullOrEmpty(pCondtion.UserSeq)) {
			oSqlBuilder.AppendLine(" AND	P.USER_SEQ	= :USER_SEQ	");
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));

		}

		if (!string.IsNullOrEmpty(pCondtion.UserCharNo)) {
			oSqlBuilder.AppendLine(" AND	P.USER_CHAR_NO	= :USER_CHAR_NO");
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		}

		string sSortExpression = " ORDER BY F.REGIST_DATE DESC, P.LAST_LOGIN_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}
}

