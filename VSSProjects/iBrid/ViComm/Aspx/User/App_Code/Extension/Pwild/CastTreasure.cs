﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 女性お宝
--	Progaram ID		: CastTreasure
--
--  Creation Date	: 2011.09.22
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;

[Serializable]
public class CastTreasureSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	public string CastTreasureSeq {
		get {
			return this.Query["cast_treasure_seq"];
		}
		set {
			this.Query["cast_treasure_seq"] = value;
		}
	}
	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}
	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}
	public string SortOrder {
		get {
			return this.Query["sort_order"];
		}
		set {
			this.Query["sort_order"] = value;
		}
	}
	public string StageGroupType {
		get {
			return this.Query["stage_group_type"];
		}
		set {
			this.Query["stage_group_type"] = value;
		}
	}
	public string BattleLogSeq {
		get {
			return this.Query["battle_log_seq"];
		}
		set {
			this.Query["battle_log_seq"] = value;
		}
	}

	public CastTreasureSeekCondition() : this(new NameValueCollection()) {
	}
	public CastTreasureSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["cast_treasure_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_treasure_seq"]));
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["sort_order"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sort_order"]));
		this.Query["stage_group_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_group_type"]));
		this.Query["battle_log_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["battle_log_seq"]));
	}
}

[System.Serializable]
public class CastTreasure:DbSession {
	public CastTreasure() {
	}

	public int GetPageCount(CastTreasureSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE		");

		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhereCount(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastTreasureSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT																				");
		oSqlBuilder.AppendLine("	CT.SITE_CD																	,	");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_SEQ														,	");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_NM															,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.POSSESSION_COUNT,0) AS POSSESSION_COUNT						,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.USED_TRAP_COUNT,0) AS USED_TRAP_COUNT						,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.USED_DOUBLE_TRAP_COUNT,0) AS USED_DOUBLE_TRAP_COUNT			,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.TOTAL_USED_TRAP_COUNT,0) AS TOTAL_USED_TRAP_COUNT			,	");
		oSqlBuilder.AppendLine("	CT.TUTORIAL_BATTLE_FLAG														,	");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_TYPE														,	");
		oSqlBuilder.AppendLine("	CT.STAGE_GROUP_TYPE															,	");
		oSqlBuilder.AppendLine("	CASE																			");
		oSqlBuilder.AppendLine("		WHEN																		");
		oSqlBuilder.AppendLine("			EXISTS																	");
		oSqlBuilder.AppendLine("			(																		");
		oSqlBuilder.AppendLine("				SELECT																");
		oSqlBuilder.AppendLine("					*																");
		oSqlBuilder.AppendLine("				FROM																");
		oSqlBuilder.AppendLine("					T_GAME_CAST_BONUS_GET_LOG										");
		oSqlBuilder.AppendLine("				WHERE																");
		oSqlBuilder.AppendLine("					SITE_CD				= CT.SITE_CD							AND	");
		oSqlBuilder.AppendLine("					USER_SEQ			= POSSESSION.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO		= POSSESSION.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("					STAGE_GROUP_TYPE	= CT.STAGE_GROUP_TYPE						");
		oSqlBuilder.AppendLine("			)																		");
		oSqlBuilder.AppendLine("		THEN 1																		");
		oSqlBuilder.AppendLine("		ELSE 0																		");
		oSqlBuilder.AppendLine("	END AS BONUS_GET_FLAG															");
		oSqlBuilder.AppendLine(" FROM																				");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			SITE_CD																,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ													,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_NM													,	");
		oSqlBuilder.AppendLine("			TUTORIAL_BATTLE_FLAG												,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE													,	");
		oSqlBuilder.AppendLine("			RANK() OVER(PARTITION BY STAGE_GROUP_TYPE ORDER BY CAST_TREASURE_SEQ ASC) AS CAST_TREASURE_TYPE");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE															");
		oSqlBuilder.AppendLine("	) CT																		,	");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			SITE_CD																,	");
		oSqlBuilder.AppendLine("			USER_SEQ															,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO														,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ													,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT													,	");
		oSqlBuilder.AppendLine("			USED_TRAP_COUNT														,	");
		oSqlBuilder.AppendLine("			USED_DOUBLE_TRAP_COUNT												,	");
		oSqlBuilder.AppendLine("			USED_TRAP_COUNT + USED_DOUBLE_TRAP_COUNT AS TOTAL_USED_TRAP_COUNT		");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE												");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD		AND										");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ		AND										");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO											");
		oSqlBuilder.AppendLine("	) POSSESSION																	");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
				
		sSortExpression = this.CreateOrderExpresion();

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneByCastTreasureSeq(CastTreasureSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ				,	");
		oSqlBuilder.AppendLine("	CAST_TREASURE_NM					");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE						");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ	= :CAST_TREASURE_SEQ		");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":CAST_TREASURE_SEQ",pCondition.CastTreasureSeq));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneByCastTreasureSeqPossession(CastTreasureSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	T.CAST_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("	T.CAST_TREASURE_NM										,	");
		oSqlBuilder.AppendLine("	NVL(P.POSSESSION_COUNT,0) AS POSSESSION_COUNT			,	");
		oSqlBuilder.AppendLine("	CT_TYPE.CAST_TREASURE_TYPE									");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE T										,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ								,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT									");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE							");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ	= :CAST_TREASURE_SEQ		AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	) P														,	");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			SITE_CD																,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ													,	");
		oSqlBuilder.AppendLine("			RANK() OVER(ORDER BY CAST_TREASURE_SEQ ASC) AS CAST_TREASURE_TYPE		");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE CT														");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD		 = :SITE_CD												AND	");
		oSqlBuilder.AppendLine("			EXISTS																	");
		oSqlBuilder.AppendLine("			(																		");
		oSqlBuilder.AppendLine("				SELECT																");
		oSqlBuilder.AppendLine("					*																");
		oSqlBuilder.AppendLine("				FROM																");
		oSqlBuilder.AppendLine("					T_CAST_TREASURE													");
		oSqlBuilder.AppendLine("				WHERE																");
		oSqlBuilder.AppendLine("					CAST_TREASURE_SEQ	= :CAST_TREASURE_SEQ					AND	");
		oSqlBuilder.AppendLine("					SITE_CD				= CT.SITE_CD							AND	");
		oSqlBuilder.AppendLine("					STAGE_GROUP_TYPE	= CT.STAGE_GROUP_TYPE						");
		oSqlBuilder.AppendLine("			)																		");
		oSqlBuilder.AppendLine("	) CT_TYPE																		");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	T.SITE_CD				= CT_TYPE.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	T.CAST_TREASURE_SEQ		= CT_TYPE.CAST_TREASURE_SEQ		AND	");
		oSqlBuilder.AppendLine("	T.SITE_CD				= P.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	T.CAST_TREASURE_SEQ		= P.CAST_TREASURE_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("	T.SITE_CD				= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	T.CAST_TREASURE_SEQ		= :CAST_TREASURE_SEQ				");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":CAST_TREASURE_SEQ",pCondition.CastTreasureSeq));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastTreasureSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		SysPrograms.SqlAppendWhere("CT.SITE_CD	            = POSSESSION.SITE_CD	(+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere("CT.CAST_TREASURE_SEQ	= POSSESSION.CAST_TREASURE_SEQ	(+)",ref pWhereClause);
		
		if (!string.IsNullOrEmpty(pCondition.StageGroupType)) {
			SysPrograms.SqlAppendWhere(" CT.STAGE_GROUP_TYPE		= :STAGE_GROUP_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_GROUP_TYPE",pCondition.StageGroupType));
		}

		if (!string.IsNullOrEmpty(pCondition.CastTreasureSeq)) {
			SysPrograms.SqlAppendWhere(" CT.CAST_TREASURE_SEQ		= :CAST_TREASURE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_TREASURE_SEQ",pCondition.CastTreasureSeq));
		}
		
		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereCount(CastTreasureSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.StageGroupType)) {
			SysPrograms.SqlAppendWhere(" STAGE_GROUP_TYPE		= :STAGE_GROUP_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_GROUP_TYPE",pCondition.StageGroupType));
		}

		return oParamList.ToArray();
	}
	
	protected string CreateOrderExpresion() {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY CAST_TREASURE_SEQ";

		return sSortExpression;
	}

	public String GetTotalPossessionCastTreasureCount(CastTreasureSeekCondition pCondition) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	SUM(POSSESSION_COUNT)	POSSESSION_COUNT				");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_POSSESSION_CAST_TREASURE								");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO					");
		oSqlBuilder.AppendLine(" GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO						");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["POSSESSION_COUNT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public String StageGroupPossessionCastTreasure(string pSiteCd,string pUserSeq,string pUserCharNo,string pStageGroupType) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	COUNT(*) AS POSSESSION_COUNT				");
		oSqlBuilder.AppendLine(" FROM							   				");
		oSqlBuilder.AppendLine("	T_POSSESSION_CAST_TREASURE					");
		oSqlBuilder.AppendLine(" WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE		");
		
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;

				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add(":STAGE_GROUP_TYPE",pStageGroupType);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["POSSESSION_COUNT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public DataSet GetCompCastTreasureGetItem(CastTreasureSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ					,				");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE				,				");
		oSqlBuilder.AppendLine("	ITEM_COUNT	AS GAME_ITEM_COUNT	,				");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM					,				");
		oSqlBuilder.AppendLine("	ATTACK_POWER					,				");
		oSqlBuilder.AppendLine("	DEFENCE_POWER					,				");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_GROUP_TYPE					");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	VW_PW_COMP_CAST_TRE_GET_ITEM					");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":STAGE_GROUP_TYPE",pCondition.StageGroupType));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetCastTreasureSeqData(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE				,						");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ										");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE											");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG		");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_ON_STR));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;

	}

	public DataSet GetCompCastTreasureGetItemByBattleLogSeq(CastTreasureSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_SEQ													");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	T_COMP_CAST_TREASURE_GET_ITEM GI								,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			CT.SITE_CD												,	");
		oSqlBuilder.AppendLine("			CT.STAGE_GROUP_TYPE											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE CT										,	");
		oSqlBuilder.AppendLine("			T_BATTLE_LOG BL												");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			CT.SITE_CD				= BL.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			CT.CAST_TREASURE_SEQ	= BL.AIM_TREASURE_SEQ			AND	");
		oSqlBuilder.AppendLine("			CT.SITE_CD				= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			BL.USER_SEQ				= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			BL.USER_CHAR_NO			= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			BL.BATTLE_LOG_SEQ		= :BATTLE_LOG_SEQ					");
		oSqlBuilder.AppendLine("	) SGT																");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	GI.SITE_CD				= SGT.SITE_CD							AND	");
		oSqlBuilder.AppendLine("	GI.STAGE_GROUP_TYPE		= SGT.STAGE_GROUP_TYPE					AND	");
		oSqlBuilder.AppendLine("	GI.SITE_CD				= :SITE_CD									");

		// where
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));

		sSortExpression = "ORDER BY GI.GAME_ITEM_SEQ ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
}
