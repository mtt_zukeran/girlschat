﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 画像レビュー履歴
--	Progaram ID		: ObjReviewHistoryPic
--  Creation Date	: 2013.09.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ObjReviewHistoryMovieSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string CastUserSeq;
	public string CastUserCharNo;
	public string CommentFlag;

	public string ObjSeq {
		get {
			return this.Query["objseq"];
		}
		set {
			this.Query["objseq"] = value;
		}
	}

	public string ObjReviewHistorySeq {
		get {
			return this.Query["history_seq"];
		}
		set {
			this.Query["history_seq"] = value;
		}
	}

	public ObjReviewHistoryMovieSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ObjReviewHistoryMovieSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["objseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["objseq"]));
		this.Query["history_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["history_seq"]));
	}
}

public class ObjReviewHistoryMovie:DbSession {

	public ObjReviewHistoryMovie() {
	}

	public int GetPageCount(ObjReviewHistoryMovieSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_PW_OBJ_REVIEW_HISTORY04	");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ObjReviewHistoryMovieSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	*							");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_PW_OBJ_REVIEW_HISTORY04	");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ObjReviewHistoryMovieSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_USER_CHAR_NO = :CAST_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_CHAR_NO",pCondition.CastUserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjSeq)) {
			SysPrograms.SqlAppendWhere("OBJ_SEQ = :OBJ_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_SEQ",pCondition.ObjSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjReviewHistorySeq)) {
			SysPrograms.SqlAppendWhere("OBJ_REVIEW_HISTORY_SEQ = :OBJ_REVIEW_HISTORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_REVIEW_HISTORY_SEQ",pCondition.ObjReviewHistorySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CommentFlag)) {
			if (pCondition.CommentFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("COMMENT_DOC IS NOT NULL",ref pWhereClause);
				SysPrograms.SqlAppendWhere("COMMENT_DEL_FLAG = 0",ref pWhereClause);
			} else if (pCondition.CommentFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				SysPrograms.SqlAppendWhere("COMMENT_DOC IS NULL",ref pWhereClause);
			}
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(ObjReviewHistoryMovieSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY CREATE_DATE DESC";

		return sSortExpression;
	}
}