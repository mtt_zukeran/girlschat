﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 野球拳画像（CastBase）
--	Progaram ID		: YakyukenPicCast
--
--  Creation Date	: 2013.05.03
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


[Serializable]
public class YakyukenPicCastSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string MaxYakyukenType;

	public string YakyukenGameSeq {
		get {
			return this.Query["ygameseq"];
		}
		set {
			this.Query["ygameseq"] = value;
		}
	}

	public string YakyukenType {
		get {
			return this.Query["ytype"];
		}
		set {
			this.Query["ytype"] = value;
		}
	}

	public YakyukenPicCastSeekCondition()
		: this(new NameValueCollection()) {
	}

	public YakyukenPicCastSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["ygameseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ygameseq"]));
		this.Query["ytype"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ytype"]));
	}
}

public class YakyukenPicCast:CastBase {
	public YakyukenPicCast() {
	}

	public int GetPageCount(YakyukenPicCastSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC00 P");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(YakyukenPicCastSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	" + CastBasicField() + ",");
		oSqlBuilder.AppendLine("	YAKYUKEN_PIC_SEQ,");
		oSqlBuilder.AppendLine("	YAKYUKEN_GAME_SEQ,");
		oSqlBuilder.AppendLine("	YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	COMMENT_TEXT,");
		oSqlBuilder.AppendLine("	YAKYUKEN_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	YAKYUKEN_SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC00 P");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY SITE_CD,YAKYUKEN_GAME_SEQ,YAKYUKEN_TYPE ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		AppendYakyukenProgress(oDataSet);

		return oDataSet;
	}

	private void AppendYakyukenProgress(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("LAST_WIN_YAKYUKEN_TYPE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("CLEAR_YAKYUKEN_TYPE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("CLEAR_FLAG",Type.GetType("System.String"));

		string sSiteCd = string.Empty;
		string sManUserSeq = string.Empty;

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			sSiteCd = oSessionMan.site.siteCd;
			sManUserSeq = oSessionMan.userMan.userSeq;
		}

		if (string.IsNullOrEmpty(sSiteCd) || string.IsNullOrEmpty(sManUserSeq)) {
			return;
		}

		string sWhereClause = String.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> sYakyukenGameSeqList = new List<string>();

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			sYakyukenGameSeqList.Add(iBridUtil.GetStringValue(dr["YAKYUKEN_GAME_SEQ"]));
		}

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere("MAN_USER_SEQ = :MAN_USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",sManUserSeq));

		SysPrograms.SqlAppendWhere(string.Format("YAKYUKEN_GAME_SEQ IN ({0})",string.Join(",",sYakyukenGameSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	YAKYUKEN_GAME_SEQ,");
		oSqlBuilder.AppendLine("	LAST_WIN_YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	CLEAR_YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	CLEAR_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_YAKYUKEN_PROGRESS");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["YAKYUKEN_GAME_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["YAKYUKEN_GAME_SEQ"]);
			pDR["LAST_WIN_YAKYUKEN_TYPE"] = subDR["LAST_WIN_YAKYUKEN_TYPE"];
			pDR["CLEAR_YAKYUKEN_TYPE"] = subDR["CLEAR_YAKYUKEN_TYPE"];
			pDR["CLEAR_FLAG"] = subDR["CLEAR_FLAG"];
		}
	}

	private OracleParameter[] CreateWhere(YakyukenPicCastSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenGameSeq)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_GAME_SEQ = :YAKYUKEN_GAME_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_GAME_SEQ",pCondition.YakyukenGameSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenType)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_TYPE = :YAKYUKEN_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_TYPE",pCondition.YakyukenType));
		}

		if (!string.IsNullOrEmpty(pCondition.MaxYakyukenType)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_TYPE <= :MAX_YAKYUKEN_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAX_YAKYUKEN_TYPE",pCondition.MaxYakyukenType));
		}

		SysPrograms.SqlAppendWhere("AUTH_FLAG = :AUTH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":AUTH_FLAG",ViCommConst.FLAG_ON_STR));

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}
}
