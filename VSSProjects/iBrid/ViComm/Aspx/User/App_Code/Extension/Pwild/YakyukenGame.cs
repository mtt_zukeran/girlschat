﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 野球拳ゲーム
--	Progaram ID		: YakyukenGame
--
--  Creation Date	: 2013.04.29
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class YakyukenGameSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string CastUserSeq;
	public string CastCharNo;
	public string CompleteFlag;

	public string YakyukenGameSeq {
		get {
			return this.Query["ygameseq"];
		}
		set {
			this.Query["ygameseq"] = value;
		}
	}

	public YakyukenGameSeekCondition()
		: this(new NameValueCollection()) {
	}

	public YakyukenGameSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["ygameseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ygameseq"]));
	}
}

public class YakyukenGame:DbSession {
	public YakyukenGame() {
	}

	public int GetPageCount(YakyukenGameSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_GAME01");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(YakyukenGameSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	YAKYUKEN_GAME_SEQ,");
		oSqlBuilder.AppendLine("	COMPLETE_FLAG,");
		oSqlBuilder.AppendLine("	SHOW_FACE_FLAG,");
		oSqlBuilder.AppendLine("	AUTH_PIC_COUNT,");
		oSqlBuilder.AppendLine("	VIEW_COUNT,");
		oSqlBuilder.AppendLine("	COMMENT_COUNT,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	COMPLETE_DATE,");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_GAME01");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY SITE_CD,CAST_USER_SEQ,CAST_CHAR_NO,CREATE_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(YakyukenGameSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CompleteFlag)) {
			SysPrograms.SqlAppendWhere("COMPLETE_FLAG = :COMPLETE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":COMPLETE_FLAG",pCondition.CompleteFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenGameSeq)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_GAME_SEQ = :YAKYUKEN_GAME_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_GAME_SEQ",pCondition.YakyukenGameSeq));
		}

		return oParamList.ToArray();
	}

	public void RegistYakyukenGame(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		out string pYakyukenGameSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_YAKYUKEN_GAME");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("pYAKYUKEN_GAME_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pYakyukenGameSeq = db.GetStringValue("pYAKYUKEN_GAME_SEQ");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
