﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;


using ViComm.Extension.Pwild;
using ViComm;
using MobileLib;
using iBridCommLib;

partial class SessionWoman {
	protected DataSet CreateListDataSetEx(HttpRequest pRequest,ulong pMode,out int pCurrentPage,out int pRowCountOfPage,out int pMaxPage) {

		DataSet ds = null;

		if (int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["pageno"]), out pCurrentPage) == false) {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]), out pCurrentPage);
			if (pCurrentPage == 0) {
				pCurrentPage = 1;
			}
		}

		if (iBridUtil.GetStringValue(pRequest.QueryString["list"]).Equals("1")) {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["lrows"]), out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileManListCount;
			}
		} else {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["drows"]), out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileManDetailListCount;
			}
		}
		
		pMaxPage = 1;

		switch (pMode) {
			case PwViCommConst.INQUIRY_USER_TOP: {
				pRowCountOfPage = 1;
				totalRowCount = 1;

				CastUserTopSeekCondition oCondition = new CastUserTopSeekCondition();
				oCondition.SiteCd = site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;

				using (CastUserTop oCastUserTop = new CastUserTop()) {
					ds = oCastUserTop.GetDataSet(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SHOP_LIST: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.StageLevel = userWoman.CurCharacter.gameCharacter.stageLevel.ToString();
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				if (
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
				) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemGetCd)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_VIEW: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SHOP_VIEW: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_PART_TIME_JOB_PLAN: {
				PartTimeJobPlanSeekCondition oCondition = new PartTimeJobPlanSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.StageLevel = this.userWoman.CurCharacter.gameCharacter.stageLevel.ToString();
				using (PartTimeJobPlan oPartTimeJobPlan = new PartTimeJobPlan()) {
					pMaxPage = oPartTimeJobPlan.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPartTimeJobPlan.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_LIST: {
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_ATTACK;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					pMaxPage = oPossessionGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_RECOVERY_FORCE_FULL: {
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE;
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					ds = oPossessionGameItem.GetPageCollection(oCondition,1,1);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SALE_LIST: {
				GameItemSaleSeekCondition oCondition = new GameItemSaleSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.StageLevel = userWoman.CurCharacter.gameCharacter.stageLevel.ToString();
				oCondition.DateNowFlag = ViCommConst.FLAG_ON_STR;
				oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				if (
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
				) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemGetCd)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItemSale oGameItemSale = new GameItemSale()) {
					pMaxPage = oGameItemSale.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItemSale.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SALE_NOTICE_LIST: {
				GameItemSaleSeekCondition oCondition = new GameItemSaleSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.StageLevel = userWoman.CurCharacter.gameCharacter.stageLevel.ToString();
				oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.NoticeDispFlag = ViCommConst.FLAG_ON_STR;
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				if (
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
				) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemGetCd)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItemSale oGameItemSale = new GameItemSale()) {
					pMaxPage = oGameItemSale.GetPageCountNotice(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItemSale.GetPageCollectionNotice(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_BATTLE: {
				GameCharacterPerformanceSeekCondition oCondition = new GameCharacterPerformanceSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.RankType = PwViCommConst.RankType.BATTLE_WIN;
				using (GameCharacterPerformance oGameCharacterPerformance = new GameCharacterPerformance()) {
					pMaxPage = oGameCharacterPerformance.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					if (oCondition.SelfRank == ViCommConst.FLAG_ON_STR) {
						oCondition.UserSeq = this.userWoman.userSeq;
						oCondition.UserCharNo = this.userWoman.curCharNo;
						int iMyPage = oGameCharacterPerformance.GetMyPageCount(oCondition,pRowCountOfPage);

						if (iMyPage > 0) {
							pCurrentPage = iMyPage;
						} else {
							pCurrentPage = pMaxPage;
						}
					}
					ds = oGameCharacterPerformance.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					oCondition.SelfRank = null;
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_PAYMENT: {
				GameCharacterPerformanceSeekCondition oCondition = new GameCharacterPerformanceSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.RankType = PwViCommConst.RankType.PAYMENT;
				using (GameCharacterPerformance oGameCharacterPerformance = new GameCharacterPerformance()) {
					pMaxPage = oGameCharacterPerformance.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);

					if (!string.IsNullOrEmpty(oCondition.LimitPage)) {
						if (pMaxPage > int.Parse(oCondition.LimitPage)) {
							pMaxPage = int.Parse(oCondition.LimitPage);
						}
					}

					if (oCondition.SelfRank == ViCommConst.FLAG_ON_STR) {
						oCondition.UserSeq = this.userWoman.userSeq;
						oCondition.UserCharNo = this.userWoman.curCharNo;
						int iMyPage = oGameCharacterPerformance.GetMyPageCount(oCondition,pRowCountOfPage);

						if (iMyPage > 0) {
							pCurrentPage = iMyPage;
						} else {
							pCurrentPage = pMaxPage;
						}
					}
					ds = oGameCharacterPerformance.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					oCondition.SelfRank = null;
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_MAN_PRESENT_HISTORY: {
				GameCharacterPresentHistorySeekCondition oCondition = new GameCharacterPresentHistorySeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.PartnerUserSeq = this.userWoman.userSeq;
				oCondition.PartnerUserCharNo = this.userWoman.curCharNo;
				using (GameCharacterPresentHistory oCastGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
					pMaxPage = oCastGameCharacterPresentHistory.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterPresentHistory.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_PARTNER_LIST: {
				pRowCountOfPage = PwViCommConst.PARTNER_POSSESSION_ITEM_PER_PAGE;
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_ATTACK;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					pMaxPage = oPossessionGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE_BATTLE_STAGE_GROUP_LIST: {
				PossessionCastTreasureSeekCondition oCondition = new PossessionCastTreasureSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				using (PossessionCastTreasure oPossessionCastTreasure = new PossessionCastTreasure()) {
					ds = oPossessionCastTreasure.GetPageCollection(oCondition);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE_BATTLE_LIST: {

				StageGroupSeekCondition oCondition = new StageGroupSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.StageSeq = this.userWoman.CurCharacter.gameCharacter.stageSeq;
				using (StageGroup oStageGroup = new StageGroup()) {
					pMaxPage = oStageGroup.GetPageCountCastTreasureStageGroupPartner(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oStageGroup.GetPageCollectionCastTreasureStageGroupPartner(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_TREASURE_LIST: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				if (!string.IsNullOrEmpty(userWoman.CurCharacter.gameCharacter.tutorialStatus)) {
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_ON_STR;
				} else {
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
				}

				if ((this.userWoman.CurCharacter.gameCharacter.maxAttackPower + this.userWoman.CurCharacter.gameCharacter.maxDefencePower) > 0) {
					int iBattlePowerPercentFrom = 0;
					int iBattlePowerPercentTo = 0;

					using (SocialGame oSocialGame = new SocialGame()) {
						oSocialGame.GetBattlePowerPercent(this.site.siteCd,out iBattlePowerPercentFrom,out iBattlePowerPercentTo);
					}

					oCondition.MaxPowerFrom = ((this.userWoman.CurCharacter.gameCharacter.maxAttackPower + this.userWoman.CurCharacter.gameCharacter.maxDefencePower) * iBattlePowerPercentFrom / 100).ToString();
					oCondition.MaxPowerTo = ((this.userWoman.CurCharacter.gameCharacter.maxAttackPower + this.userWoman.CurCharacter.gameCharacter.maxDefencePower) * iBattlePowerPercentTo / 100).ToString();
				}

				using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
					ds = oGameCharacterBattle.GetPageCollectionTreasureWoman(oCondition,pCurrentPage,pRowCountOfPage);

					if (ds.Tables[0].Rows.Count == 0) {
						oCondition.MaxPowerFrom = null;
						oCondition.MaxPowerTo = null;

						ds = oGameCharacterBattle.GetPageCollectionTreasureWoman(oCondition,pCurrentPage,pRowCountOfPage);
					}
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_LOSE_LIST: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				if (String.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameCharacterBattleLoseSort.LAST_LOSE_DATE;
				}
				using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
					pMaxPage = oGameCharacterBattle.GetPageCountLose(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacterBattle.GetPageCollectionLose(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_START:
			case PwViCommConst.INQUIRY_GAME_BATTLE_START_PARTY: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SelfStageGroupType = this.userWoman.CurCharacter.gameCharacter.stageGroupType;
				oCondition.SexCd = ViCommConst.OPERATOR;
				
				if(!string.IsNullOrEmpty(this.userWoman.CurCharacter.gameCharacter.tutorialStatus)) {
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_ON_STR;
				} else {
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
				}

				using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
					ds = oGameCharacterBattle.GetOneBattleStartWoman(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_START_SUPPORT: {
				GameCharacterBattleSupportSeekCondition oCondition = new GameCharacterBattleSupportSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SupportRequestFlag = ViCommConst.FLAG_ON_STR;
				oCondition.SexCd = ViCommConst.MAN;

				using (GameCharacterBattleSupport oGameCharacterBattleSupport = new GameCharacterBattleSupport()) {
					ds = oGameCharacterBattleSupport.GetOne(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_WIN: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SINGLE;
				oCondition.AttackWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneWoman(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_WIN_PARTY: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.PARTY;
				oCondition.AttackWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneWoman(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_WIN_SUPPORT: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SUPPORT;
				oCondition.AttackWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneSupportWoman(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SINGLE;
				oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneWoman(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE_PARTY: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.PARTY;
				oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneWoman(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE_SUPPORT: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SUPPORT;
				oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneSupport(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_SUPPORT_REQUEST: {
				SupportRequestSeekCondition oCondition = new SupportRequestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;

				using (SupportRequest oSupportRequest = new SupportRequest()) {
					ds = oSupportRequest.GetPageCollectionForCast(oCondition,pCurrentPage,pRowCountOfPage);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FAVORITE_LIST: {
				ManGameCharacterFavoriteSeekCondition oCondition = new ManGameCharacterFavoriteSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManGameCharacterFavoriteSort.GAME_FAVORITE_REGIST_DATE_DESC;
				}
				using (ManGameCharacterFavorite oManGameCharacterFavorite = new ManGameCharacterFavorite()) {
					pMaxPage = oManGameCharacterFavorite.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFavorite.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FAVORITE_PRESENT: {
				ManGameCharacterFavoriteSeekCondition oCondition = new ManGameCharacterFavoriteSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManGameCharacterFavoriteSort.FRIENDLY_RANK_ASC;
				}
				using (ManGameCharacterFavorite oManGameCharacterFavorite = new ManGameCharacterFavorite()) {
					pMaxPage = oManGameCharacterFavorite.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFavorite.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SHOP_PRESENT: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.StageLevel = userWoman.CurCharacter.gameCharacter.stageLevel.ToString();
				oCondition.PresentFlag = ViCommConst.FLAG_ON_STR;
				oCondition.GameItemCategoryGroupType = PwViCommConst.GameItemCatregoryGroup.EQUIP;
				if (String.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_PRESENT: {
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.PresentFlag = ViCommConst.FLAG_ON_STR;
				if (String.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					pMaxPage = oPossessionGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_STAGE_BOSS_BATTLE: {
				StageSeekCondition oCondition = new StageSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.StageClearFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.NextStageMovedFlag = ViCommConst.FLAG_OFF_STR;
				using (Stage oGameStage = new Stage()) {
					ds = oGameStage.GetOneBossData(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_STAGE_BOSS_BATTLE_RESULT: {
				StageSeekCondition oCondition = new StageSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				using (Stage oStage = new Stage()) {
					ds = oStage.GetOneStageClearData(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_PRESENT_LIST: {
				ManGameCharacterFellowSeekCondition oCondition = new ManGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManGameCharacterFellowSort.FRIENDLY_RANK_ASC;
				}
				using (ManGameCharacterFellow oManGameCharacterFellow = new ManGameCharacterFellow()) {
					pMaxPage = oManGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY: {
				LotterySeekCondition oCondition = new LotterySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.LotteryPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.RecentLotteryFlag = ViCommConst.FLAG_ON_STR;

				using (Lottery oLottery = new Lottery()) {
					ds = oLottery.GetOne(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_NOT_COMP: {
				LotterySeekCondition oCondition = new LotterySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				using (Lottery oLottery = new Lottery()) {
					ds = oLottery.GetOneNotComp(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT: {
				LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				using (LotteryGetItem oLotteryGameItem = new LotteryGetItem()) {
					ds = oLotteryGameItem.GetOneLotteryGetItem(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT_NOT_COMP: {
				LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				using (LotteryGetItem oLotteryGameItem = new LotteryGetItem()) {
					ds = oLotteryGameItem.GetOneLotteryGetItemNotComp(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_LIST: {
				LotterySeekCondition oCondition = new LotterySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.LotteryPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.RecentLotteryFlag = ViCommConst.FLAG_OFF_STR;
				using (Lottery oLottery = new Lottery()) {
					ds = oLottery.GetOne(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_VIEW: {
				LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;

				using (LotteryGetItem oLotteryGameItem = new LotteryGetItem()) {
					if (oCondition.CompItemFlg == ViCommConst.FLAG_ON_STR) {
						ds = oLotteryGameItem.GetOneLotteryCompGetItem(oCondition);
					} else {
						ds = oLotteryGameItem.GetOneLotteryGetItem(oCondition);
					}
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW: {
				pRowCountOfPage = 1;
				ManGameCharacterViewSeekCondition oCondition = new ManGameCharacterViewSeekCondition(pRequest.Params);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				using (ManGameCharacterView oManGameCharacterView = new ManGameCharacterView()) {
					pMaxPage = oManGameCharacterView.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterView.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOGIN_BONUS_MONTHLY_LIST: {
				GameLoginBonusMonthlySeekCondition oCondition = new GameLoginBonusMonthlySeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				DateTime dt = DateTime.Now;
				oCondition.Month = string.Format("{0}/{1}",dt.ToString("yyyy"),dt.ToString("MM"));
				oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;

				using (GameLoginBonusMonthly oGameLoginBonusMonthly = new GameLoginBonusMonthly()) {
					pMaxPage = oGameLoginBonusMonthly.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameLoginBonusMonthly.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_LIST: {
				ManGameCharacterFellowSeekCondition oCondition = new ManGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManGameCharacterFellowSort.APPLICATION_PERMIT_DATE_DESC;
				}
				using (ManGameCharacterFellow oManGameCharacterFellow = new ManGameCharacterFellow()) {
					pMaxPage = oManGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_APPLICATION_LIST: {
				ManGameCharacterFellowSeekCondition oCondition = new ManGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.APPLICATION;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManGameCharacterFellowSort.APPLICATION_DATE_DESC;
				}
				using (ManGameCharacterFellow oManGameCharacterFellow = new ManGameCharacterFellow()) {
					pMaxPage = oManGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_HAS_APPLICATION_LIST: {
				ManGameCharacterFellowSeekCondition oCondition = new ManGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManGameCharacterFellowSort.APPLICATION_DATE_DESC;
				}
				using (ManGameCharacterFellow oManGameCharacterFellow = new ManGameCharacterFellow()) {
					pMaxPage = oManGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_FELLOW_LOG: {
				NewsSeekCondition oCondition = new NewsSeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				using (News oNews = new News()) {
					pMaxPage = oNews.GetPageCountManFellowLog(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oNews.GetPageCollectionManFellowLog(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_COMMUNICATION_PARTNER_HUG: {
				ManGameCommunicationPartnerHugSeekCondition oCondition = new ManGameCommunicationPartnerHugSeekCondition(pRequest.Params);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				using (ManGameCommunicationPartnerHug oManGameCommunicationPartnerHug = new ManGameCommunicationPartnerHug()) {
					pMaxPage = oManGameCommunicationPartnerHug.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCommunicationPartnerHug.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_MEMBER_LIST: {
				ManGameCharacterFellowMemberSeekCondition oCondition = new ManGameCharacterFellowMemberSeekCondition(pRequest.Params);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManGameCharacterFellowMemberSort.FRIENDLY_RANK_ASC;
				}
				using (ManGameCharacterFellowMember oManGameCharacterFellowMember = new ManGameCharacterFellowMember()) {
					pMaxPage = oManGameCharacterFellowMember.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFellowMember.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_LIST: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.Level = userWoman.CurCharacter.gameCharacter.gameCharacterLevel.ToString();
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.ExceptSelf = ViCommConst.FLAG_ON_STR;
				oCondition.ExceptBattled = ViCommConst.FLAG_ON_STR;
				oCondition.Random = ViCommConst.FLAG_ON_STR;
				oCondition.NotTutorialFlg = ViCommConst.FLAG_ON_STR;
				if (oCondition.LevelRange.Equals("")) {
					oCondition.LevelRange = PwViCommConst.GameCharSeek.LEVEL_RANGE;
				}
				using (GameCharacterBattle oGameCharacter = new GameCharacterBattle()) {
					pMaxPage = oGameCharacter.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacter.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_VIEW: {
				pRowCountOfPage = 1;
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				using (GameCharacterBattle oGameCharacter = new GameCharacterBattle()) {
					pMaxPage = oGameCharacter.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacter.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}

			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_COMMUNICATION_VIEW: {
				pRowCountOfPage = 1;
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				using (GameCharacterBattle oGameCharacter = new GameCharacterBattle()) {
					pMaxPage = oGameCharacter.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacter.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_STAGE: {
				pRowCountOfPage = 1;
				StageSeekCondition oCondition = new StageSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.StageLevel = this.userWoman.CurCharacter.gameCharacter.stageLevel.ToString();
				if (!string.IsNullOrEmpty(pRequest.QueryString["stage_group_type"])) {
					using (Stage oStage = new Stage()) {
						pMaxPage = 1;
						ds = oStage.GetOneByStageGroupType(oCondition);
					}
				} else if (!string.IsNullOrEmpty(pRequest.QueryString["stage_seq"])) {
					using (Stage oStage = new Stage()) {
						pMaxPage = 1;
						ds = oStage.GetOneByStageSeq(oCondition);
					}
				} else {
					oCondition.StageSeq = this.userWoman.CurCharacter.gameCharacter.stageSeq;
					using (Stage oStage = new Stage()) {
						pMaxPage = 1;
						ds = oStage.GetOneByStageSeq(oCondition);
					}
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TOWN: {
				TownSeekCondition oCondition = new TownSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				using (Town oTown = new Town()) {
					pMaxPage = oTown.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oTown.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CLEAR_MISSION_RECOVERY_FORCE: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.GameItemCategoryGroupType = PwViCommConst.GameItemCatregoryGroup.RESTORE;
				oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;

				using (GameItem oGameItem = new GameItem()) {
					ds = oGameItem.GetPageCollection(oCondition,1,1);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FIND_LIST: {
				ManGameCharacterFindSeekCondition oCondition = new ManGameCharacterFindSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				if (iBridUtil.GetStringValue(pRequest.QueryString["newman"]).Equals(ViCommConst.FLAG_ON_STR)) {
					oCondition.NewManDay = site.manNewFaceDays.ToString();
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_LOGIN;
				}
				using (ManGameCharacterFind oManGameCharacterFind = new ManGameCharacterFind()) {
					pMaxPage = oManGameCharacterFind.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFind.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_NEW: {
				ManGameCharacterFindSeekCondition oCondition = new ManGameCharacterFindSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.Sort = PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_NEW;
				using (ManGameCharacterFind oManGameCharacterFind = new ManGameCharacterFind()) {
					pMaxPage = oManGameCharacterFind.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFind.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE_MANAGEMENT: {
				ManTreasureManagementSeekCondition oCondition = new ManTreasureManagementSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_NEW;
				}
				if (string.IsNullOrEmpty(oCondition.PublishFlag)) {
					oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;
				}
				using (ManTreasureManagement oManTreasureManagement = new ManTreasureManagement()) {
					pMaxPage = oManTreasureManagement.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTreasureManagement.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_MOVIE_LIST: {
				CastGameCharacterMovieManagementSeekCondition oCondition = new CastGameCharacterMovieManagementSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_NEW;
				}
				if (string.IsNullOrEmpty(oCondition.PublishFlag)) {
					oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;
				}
				using (CastGameCharacterMovieManagement oCastGameCharacterMovie = new CastGameCharacterMovieManagement()) {
					pMaxPage = oCastGameCharacterMovie.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterMovie.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_TREASURE_MOSAIC_ERASE: {
				ManGameCharacterTreasurePossessionSeekCondition oCondition = new ManGameCharacterTreasurePossessionSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = this.userWoman.userSeq;
				oCondition.CastCharNo = this.userWoman.curCharNo;
				oCondition.MosaicEraseFlag = ViCommConst.FLAG_ON_STR;
				using (ManGameCharacterTreasurePossession oManGameCharacterTreasurePossession = new ManGameCharacterTreasurePossession()) {
					pMaxPage = oManGameCharacterTreasurePossession.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterTreasurePossession.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_MOVIE_BUY: {
				GameObjUsedHistorySeekCondition oCondition = new GameObjUsedHistorySeekCondition(pRequest.QueryString);
				using (GameObjUsedHistory oGameObjUsedHistory = new GameObjUsedHistory()) {
					pMaxPage = oGameObjUsedHistory.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameObjUsedHistory.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_MOVIE_PLAY: {
				using (CastMovie oCastMovie = new CastMovie()) {
					ds = oCastMovie.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["cast_user_movie_seq"]),false);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_FAVORITE_LOG: {
				NewsSeekCondition oCondition = new NewsSeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;

				using (News oNews = new News()) {
					pMaxPage = oNews.GetPageCountManFavoriteLog(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oNews.GetPageCollectionManFavoriteLog(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_LOG: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;

				if (string.IsNullOrEmpty(oCondition.BattleType)) {
					oCondition.BattleType = PwViCommConst.GameBattleType.SINGLE;
				}

				using (BattleLog oBattleLog = new BattleLog()) {
					pMaxPage = oBattleLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBattleLog.GetPageCollectionWoman(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_COMMUNICATION: {
				GameCommunicationSeekCondition oCondition = new GameCommunicationSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;

				using (GameCommunication oGameCommunication = new GameCommunication()) {
					ds = oGameCommunication.GetOne(oCondition);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_WANTED_ITEM_PARTNER: {
				WantedItemSeekCondition oCondition = new WantedItemSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.PartnerFlg = ViCommConst.FLAG_ON_STR;
				
				using (WantedItem oWantedItem = new WantedItem()) {
					pMaxPage = 1;
					ds = oWantedItem.GetListWantedItemPartner(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_WANTED_ITEM: {
				WantedItemSeekCondition oCondition = new WantedItemSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				using (WantedItem oWantedItem = new WantedItem()) {
					pMaxPage = 1;
					ds = oWantedItem.GetListWantedItem(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_WANTED: {
				GameItemWantedSeekCondition oCondition = new GameItemWantedSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.StageLevel = this.userWoman.CurCharacter.gameCharacter.stageLevel.ToString();
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItemWanted oGameItemWanted = new GameItemWanted()) {
					pMaxPage = oGameItemWanted.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItemWanted.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_COMP_CALENDAR: {
				StageGroupSeekCondition oCondition = new StageGroupSeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.StageSeq = this.userWoman.CurCharacter.gameCharacter.stageSeq;
				using (StageGroup oStageGroup = new StageGroup()) {
					pMaxPage = oStageGroup.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oStageGroup.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE: {
				CastTreasureSeekCondition oCondition = new CastTreasureSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;

				using (CastTreasure oCastTreasure = new CastTreasure()) {
					pMaxPage = oCastTreasure.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastTreasure.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_INTRO_LOG: {
				GameIntroLogSeekCondition oCondition = new GameIntroLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.GetFlag = ViCommConst.FLAG_OFF_STR;
				using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
					pMaxPage = oGameIntroLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameIntroLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CLEAR_TOWN_USED_ITEM_SHORTAGE_LIST: {
				ClearTownUsedItemSeekCondition oCondition = new ClearTownUsedItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;

				using (ClearTownUsedItem oClearTownUsedItem = new ClearTownUsedItem()) {
					ds = oClearTownUsedItem.GetPageCollectionUsedItemShortage(oCondition);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_ONCE_DAY_PRESENT: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.PresentFlag = ViCommConst.FLAG_ON_STR;
				using (OnceDayPresent oOnceDayPresent = new OnceDayPresent()) {
					pMaxPage = oOnceDayPresent.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oOnceDayPresent.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY: {
				ObjReviewHistorySeekCondition oCondition = new ObjReviewHistorySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = this.userWoman.userSeq;
				oCondition.CastUserCharNo = this.userWoman.curCharNo;
				oCondition.CommentFlag = ViCommConst.FLAG_ON_STR;
				using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
					pMaxPage = oObjReviewHistory.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oObjReviewHistory.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_REQUEST: {
				PwRequestSeekCondition oCondition = new PwRequestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.PublicStatus = PwViCommConst.RequestPublicStatus.PUBLIC;
				oCondition.BlogFlag = (this.userWoman.CurCharacter.characterEx.enabledBlogFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
				oCondition.RichFlag = (this.userWoman.CurCharacter.characterEx.enabledRichinoFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.RequestSort.GOOD_COUNT_DESC;
				}
				using (PwRequest oPwRequest = new PwRequest()) {
					pMaxPage = oPwRequest.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPwRequest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_REQUEST_VALUE: {
				RequestValueSeekCondition oCondition = new RequestValueSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				using (RequestValue oRequestValue = new RequestValue()) {
					pMaxPage = oRequestValue.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oRequestValue.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YESTERDAY_CAST_REPORT: {
				YesterdayCastReportSeekCondition oCondition = new YesterdayCastReportSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				using (YesterdayCastReport oYesterdayCastReport = new YesterdayCastReport()) {
					pMaxPage = oYesterdayCastReport.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oYesterdayCastReport.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LEVEL_QUEST: {
				LevelQuestSeekCondition oCondition = new LevelQuestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;

				using (LevelQuest oLevelQuest = new LevelQuest()) {
					ds = oLevelQuest.GetOne(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_OTHER_QUEST: {
				OtherQuestSeekCondition oCondition = new OtherQuestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;

				using (OtherQuest oOtherQuest = new OtherQuest()) {
					pMaxPage = oOtherQuest.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oOtherQuest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_REGIST_QUEST_ENTRY: {
				string sQuestType = iBridUtil.GetStringValue(pRequest.QueryString["quest_type"]);

				if (sQuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
					LittleQuestSeekCondition oCondition = new LittleQuestSeekCondition(pRequest.QueryString);
					oCondition.SiteCd = this.site.siteCd;
					oCondition.UserSeq = this.userWoman.userSeq;
					oCondition.UserCharNo = this.userWoman.curCharNo;
					oCondition.SexCd = ViCommConst.OPERATOR;

					using (LittleQuest oLittleQuest = new LittleQuest()) {
						ds = oLittleQuest.GetPageCollection(oCondition,1,1);
					}
					this.seekConditionEx = oCondition;
				} else if (sQuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
					LevelQuestSeekCondition oCondition = new LevelQuestSeekCondition(pRequest.QueryString);
					oCondition.SiteCd = this.site.siteCd;
					oCondition.UserSeq = this.userWoman.userSeq;
					oCondition.UserCharNo = this.userWoman.curCharNo;
					oCondition.SexCd = ViCommConst.OPERATOR;

					using (LevelQuest oLevelQuest = new LevelQuest()) {
						ds = oLevelQuest.GetOne(oCondition);
					}
					this.seekConditionEx = oCondition;
				} else if (sQuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
					OtherQuestSeekCondition oCondition = new OtherQuestSeekCondition(pRequest.QueryString);
					oCondition.SiteCd = this.site.siteCd;
					oCondition.UserSeq = this.userWoman.userSeq;
					oCondition.UserCharNo = this.userWoman.curCharNo;
					oCondition.SexCd = ViCommConst.OPERATOR;

					using (OtherQuest oOtherQuest = new OtherQuest()) {
						pMaxPage = oOtherQuest.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
						ds = oOtherQuest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					}
					this.seekConditionEx = oCondition;
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_QUEST_REWARD_GET: {
				QuestEntryLogSeekCondition oCondition = new QuestEntryLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.GetRewardFlag = ViCommConst.FLAG_OFF_STR;

				using (QuestEntryLog oQuestEntryLog = new QuestEntryLog()) {
					ds = oQuestEntryLog.GetPageCollection(oCondition,1,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_PAYMENT_LOG_VIEW: {
				DailyGamePaymentLogSeekCondition oCondition = new DailyGamePaymentLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				
				if (string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["start_date"]))) {
					oCondition.StartDate = DateTime.Now.ToString("yyyy/MM/") + "01";
				}
				
				if (string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["end_date"]))) {
					oCondition.EndDate = DateTime.Now.ToString("yyyy/MM/") + DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month).ToString();
				}
				
				using (DailyGamePaymentLog oDailyGamePaymentLog = new DailyGamePaymentLog()) {
					ds = oDailyGamePaymentLog.GetOne(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_FAVORIT_GROUP: {
				FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
					pMaxPage = oFavoritGroup.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_DATE_LOG: {
				DateLogSeekCondition oCondition = new DateLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.PartnerUserSeq = this.userWoman.userSeq;
				oCondition.PartnerUserCharNo = this.userWoman.curCharNo;
				using (DateLog oDateLog = new DateLog()) {
					pMaxPage = oDateLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oDateLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_REGULAR: {
				ManGameCharacterCommunicationSeekCondition oCondition = new ManGameCharacterCommunicationSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = this.userWoman.userSeq;
				oCondition.SelfUserCharNo = this.userWoman.curCharNo;
				if (string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["sort"]))) {
					oCondition.Sort = PwViCommConst.GameManCommunicationSort.SORT_FRIENDLY_RANK;
				}
				
				using (ManGameCharacterCommunication oManGameCharacterCommunication = new ManGameCharacterCommunication()) {
					pMaxPage = oManGameCharacterCommunication.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterCommunication.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_PAYMENT_LOG_DETAIL: {
				GamePaymentLogSeekCondition oCondition = new GamePaymentLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = this.userWoman.userSeq;
				oCondition.SelfUserCharNo = this.userWoman.curCharNo;

				using (GamePaymentLog oGamePaymentLog = new GamePaymentLog()) {
					pMaxPage = oGamePaymentLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGamePaymentLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_JEALOUSY_MAINTE: {
				ManGameCharacterFavoriteSeekCondition oCondition = new ManGameCharacterFavoriteSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = this.userWoman.userSeq;
				oCondition.SelfUserCharNo = this.userWoman.curCharNo;
				
				using (ManGameCharacterFavorite oManGameCharacterFavorite = new ManGameCharacterFavorite()) {
					pMaxPage = oManGameCharacterFavorite.GetPageCountJealousy(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManGameCharacterFavorite.GetPageCollectionJealousy(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_USER_RX: {
				using (MailBox oMailBox = new MailBox()) {
					pMaxPage = oMailBox.GetPageCountUserRx(site.siteCd,userWoman.userSeq,userWoman.curCharNo,ViCommConst.OPERATOR,pRowCountOfPage,out totalRowCount);
					ds = oMailBox.GetPageCollectionUserRx(site.siteCd,userWoman.userSeq,userWoman.curCharNo,ViCommConst.OPERATOR,pCurrentPage,pRowCountOfPage);
				}
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_USER_TX: {
				using (MailBox oMailBox = new MailBox()) {
					pMaxPage = oMailBox.GetPageCountUserTx(site.siteCd,userWoman.userSeq,userWoman.curCharNo,ViCommConst.OPERATOR,pRowCountOfPage,out totalRowCount);
					ds = oMailBox.GetPageCollectionUserTx(site.siteCd,userWoman.userSeq,userWoman.curCharNo,ViCommConst.OPERATOR,pCurrentPage,pRowCountOfPage);
				}
				break;
			}
			case PwViCommConst.INQUIRY_BLOG_COMMENT: {
				BlogCommentSeekCondition oCondition = new BlogCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				
				if (string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["sort"]))) {
					oCondition.Sort = PwViCommConst.BlogCommentSort.CREATE_DATE_DESC;
				}
				
				using (BlogComment oBlogComment = new BlogComment()) {
					pMaxPage = oBlogComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBlogComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_BLOG_NG_USER: {
				BlogNgUserSeekCondition oCondition = new BlogNgUserSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				using (BlogNgUser oBlogNgUser = new BlogNgUser()) {
					pMaxPage = oBlogNgUser.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBlogNgUser.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_BLOG_PIC_NON_USED: {
					BlogObjSeekCondition oCondition = new BlogObjSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FileType = ViCommConst.BlogFileType.PIC;
				oCondition.UsedFlag = ViCommConst.FLAG_OFF_STR;
				using (BlogObj oBlogObj = new BlogObj()) {
					pMaxPage = oBlogObj.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBlogObj.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUILY_BLOG_MOVIE_NON_USED: {
				BlogObjSeekCondition oCondition = new BlogObjSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.FileType = ViCommConst.BlogFileType.MOVIE;
				oCondition.UsedFlag = ViCommConst.FLAG_OFF_STR;
				using (BlogObj oBlogObj = new BlogObj()) {
					pMaxPage = oBlogObj.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBlogObj.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
		case PwViCommConst.INQUIRY_SELF_CAST_TWEET: {
				CastTweetSeekCondition oCondition = new CastTweetSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userWoman.userSeq;
				oCondition.UserCharNo = this.userWoman.curCharNo;
				oCondition.SelfUserSeq = this.userWoman.userSeq;
				oCondition.SelfUserCharNo = this.userWoman.curCharNo;
				oCondition.OnlyFanClubFlag = ViCommConst.FLAG_OFF_STR;
				using (CastTweet oCastTweet = new CastTweet()) {
					pMaxPage = oCastTweet.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastTweet.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
		case PwViCommConst.INQUIRY_CAST_TWEET_COMMENT: {
				CastTweetCommentSeekCondition oCondition = new CastTweetCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				using (CastTweetComment oCastTweetComment = new CastTweetComment()) {
					pMaxPage = oCastTweetComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastTweetComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
		case PwViCommConst.INQUIRY_MAN_TWEET: {
				ManTweetExSeekCondition oCondition = new ManTweetExSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = this.userWoman.userSeq;
				oCondition.CastCharNo = this.userWoman.curCharNo;

				if (currentAspx.Equals("ListPersonalManTweet.aspx") || currentAspx.Equals("ViewManTweet.aspx")) {
					oCondition.AppendLikeCount = true;
					oCondition.AppendCommentCount = true;
					oCondition.AppendCastLikedFlag = true;
					oCondition.AppendCastReadFlag = true;
				}

				using (ManTweetEx oManTweetEx = new ManTweetEx()) {
					pMaxPage = oManTweetEx.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTweetEx.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
		case PwViCommConst.INQUIRY_MAN_TWEET_LIST: {
				ManTweetExSeekCondition oCondition = new ManTweetExSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = this.userWoman.userSeq;
				oCondition.CastCharNo = this.userWoman.curCharNo;
				oCondition.AppendLikeCount = true;
				oCondition.AppendCommentCount = true;
				oCondition.AppendCastLikedFlag = true;
				oCondition.AppendCastReadFlag = true;
				using (ManTweetEx oManTweetEx = new ManTweetEx()) {
					pMaxPage = oManTweetEx.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTweetEx.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}		
		case PwViCommConst.INQUIRY_MAN_TWEET_COMMENT: {
				ManTweetCommentSeekCondition oCondition = new ManTweetCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				using (ManTweetComment oManTweetComment = new ManTweetComment()) {
					pMaxPage = oManTweetComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTweetComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_FANCLUB_STATUS: {
				FanClubStatusSeekCondition oCondition = new FanClubStatusSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;
				oCondition.EnableFlag = ViCommConst.FLAG_ON_STR;
				using (ManFanClubStatus oManFanClubStatus = new ManFanClubStatus()) {
					pMaxPage = oManFanClubStatus.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManFanClubStatus.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAN_TWEET_COMMENT_TERM: {
				using (ManTweetCommentTerm oManTweetCommentTerm = new ManTweetCommentTerm()) {
					pMaxPage = 1;
					ds = oManTweetCommentTerm.GetNowData(this.site.siteCd,userWoman.userSeq,userWoman.curCharNo);
				}
				break;
			}
			case PwViCommConst.INQUIRY_MAN_TWEET_COMMENT_CNT: {
				ManTweetCommentCntSeekCondition oCondition = new ManTweetCommentCntSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				if (string.IsNullOrEmpty(oCondition.TermSeq)) {
					string sLastTermSeq = string.Empty;

					using (ManTweetCommentTerm oManTweetCommentTerm = new ManTweetCommentTerm()) {
						sLastTermSeq = oManTweetCommentTerm.GetLastTermSeq(this.site.siteCd);
					}

					if (!string.IsNullOrEmpty(sLastTermSeq)) {
						oCondition.TermSeq = sLastTermSeq;
					}
				}
				using (ManTweetCommentCnt oManTweetCommentCnt = new ManTweetCommentCnt()) {
					pMaxPage = 1;
					ds = oManTweetCommentCnt.GetListReward(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_ATTACK: {
				MailAttackSeekCondition oCondition = new MailAttackSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = userWoman.userSeq;
				oCondition.SelfUserCharNo = userWoman.curCharNo;
				oCondition.LastLoginDateFrom = DateTime.Now.AddMonths(-3).ToString();
				using (MailAttack oMailAttack = new MailAttack()) {
					pMaxPage = oMailAttack.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oMailAttack.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_ATTACK_SUB: {
				MailAttackSeekCondition oCondition = new MailAttackSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = userWoman.userSeq;
				oCondition.SelfUserCharNo = userWoman.curCharNo;
				oCondition.LastLoginDateTo = DateTime.Now.AddMonths(-3).ToString();
				oCondition.NonExistMailAddrFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.CastMailRxType = "2";//携帯電話で受信する
				using (MailAttack oMailAttack = new MailAttack()) {
					pMaxPage = oMailAttack.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oMailAttack.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YAKYUKEN_GAME: {
				YakyukenGameSeekCondition oCondition = new YakyukenGameSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;
				using (YakyukenGame oYakyukenGame = new YakyukenGame()) {
					pMaxPage = oYakyukenGame.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oYakyukenGame.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YAKYUKEN_COMMENT: {
				YakyukenCommentSeekCondition oCondition = new YakyukenCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;
				using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
					pMaxPage = oYakyukenComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oYakyukenComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YAKYUKEN_PIC: {
				YakyukenPicSeekCondition oCondition = new YakyukenPicSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;
				using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
					pMaxPage = 1;
					ds = oYakyukenPic.GetList(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YAKYUKEN_PIC_VIEW: {
				YakyukenPicSeekCondition oCondition = new YakyukenPicSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;
				using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
					pMaxPage = 1;
					ds = oYakyukenPic.GetPageCollection(oCondition,1,1);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YAKYUKEN_PIC_PREVIEW: {
				YakyukenPicSeekCondition oCondition = new YakyukenPicSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;

				using (YakyukenProgressCast oYakyukenProgressCast = new YakyukenProgressCast()) {
					DataSet oDataSet = oYakyukenProgressCast.GetOne(this.site.siteCd,oCondition.YakyukenGameSeq);

					if (oDataSet.Tables[0].Rows.Count > 0) {
						string sClearYakyukenType = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CLEAR_YAKYUKEN_TYPE"]);
						string sLastWinYakyukenType = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["LAST_WIN_YAKYUKEN_TYPE"]);
						oCondition.MaxYakyukenType = sClearYakyukenType;

						if (string.IsNullOrEmpty(oCondition.YakyukenType)) {
							if (sLastWinYakyukenType.Equals("6")) {
								oCondition.YakyukenType = "1";
							} else {
								oCondition.YakyukenType = sLastWinYakyukenType;
							}
						}
					} else {
						oCondition.MaxYakyukenType = "1";
					}
				}
				using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
					pMaxPage = 1;
					ds = oYakyukenPic.GetPageCollection(oCondition,1,1);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_TWEET_LIKE: {
				CastTweetLikeSeekCondition oCondition = new CastTweetLikeSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastTweetUserSeq = this.userWoman.userSeq;
				oCondition.CastTweetUserCharNo = this.userWoman.curCharNo;
				using (CastTweetLike oCastTweetLike = new CastTweetLike()) {
					pMaxPage = oCastTweetLike.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastTweetLike.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_PICKUP_OBJS_COMMENT: {
				PickupObjsCommentSeekCondition oCondition = new PickupObjsCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				using (PickupObjsComment oPickupObjsComment = new PickupObjsComment()) {
					pMaxPage = oPickupObjsComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPickupObjsComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_DRAFT_MAIL: {
				ManDraftMailSeekCondition oCondition = new ManDraftMailSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.TxUserSeq = this.userWoman.userSeq;
				oCondition.TxUserCharNo = this.userWoman.curCharNo;
				using (ManDraftMail oManDraftMail = new ManDraftMail()) {
					ds = oManDraftMail.GetPageCollection(oCondition,1,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAN_FAVORIT_NEWS: {
				ManNewsSeekCondition oCondition = new ManNewsSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.OnlyFavorit = ViCommConst.FLAG_ON_STR;
				using (ManNews oManNews = new ManNews()) {
					pMaxPage = oManNews.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManNews.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY_PIC: {
				ObjReviewHistoryPicSeekCondition oCondition = new ObjReviewHistoryPicSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = this.userWoman.userSeq;
				oCondition.CastUserCharNo = this.userWoman.curCharNo;
				oCondition.CommentFlag = ViCommConst.FLAG_ON_STR;
				using (ObjReviewHistoryPic oObjReviewHistoryPic = new ObjReviewHistoryPic()) {
					pMaxPage = oObjReviewHistoryPic.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oObjReviewHistoryPic.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY_MOVIE: {
				ObjReviewHistoryMovieSeekCondition oCondition = new ObjReviewHistoryMovieSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CastUserSeq = this.userWoman.userSeq;
				oCondition.CastUserCharNo = this.userWoman.curCharNo;
				oCondition.CommentFlag = ViCommConst.FLAG_ON_STR;
				using (ObjReviewHistoryMovie oObjReviewHistoryMovie = new ObjReviewHistoryMovie()) {
					pMaxPage = oObjReviewHistoryMovie.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oObjReviewHistoryMovie.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_ELECTION_PERIOD: {
				using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
					ds = oElectionPeriod.GetCurrentEntry(site.siteCd,userWoman.userSeq,userWoman.curCharNo);
				}
				break;
			}
			case PwViCommConst.INQUIRY_SELF_CAST_MOVIE_LIKE: {
				CastMovieLikeSeekCondition oCondition = new CastMovieLikeSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.CastMovieUserSeq = userWoman.userSeq;
				oCondition.CastMovieUserCharNo = userWoman.curCharNo;
				using (CastMovieLike oCastMovieLike = new CastMovieLike()) {
					pMaxPage = oCastMovieLike.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastMovieLike.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_SELF_CAST_PIC_LIKE: {
				CastPicLikeSeekCondition oCondition = new CastPicLikeSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.CastPicUserSeq = userWoman.userSeq;
				oCondition.CastPicUserCharNo = userWoman.curCharNo;
				using (CastPicLike oCastPicLike = new CastPicLike()) {
					pMaxPage = oCastPicLike.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastPicLike.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_SELF_CAST_MOVIE_COMMENT: {
				CastMovieCommentSeekCondition oCondition = new CastMovieCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.CastMovieUserSeq = userWoman.userSeq;
				oCondition.CastMovieUserCharNo = userWoman.curCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastMovieCommentSort.COMMENT_DATE_NEW;
				}
				using (CastMovieComment oCastMovieComment = new CastMovieComment()) {
					pMaxPage = oCastMovieComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastMovieComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_SELF_CAST_PIC_COMMENT: {
				CastPicCommentSeekCondition oCondition = new CastPicCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.CastPicUserSeq = userWoman.userSeq;
				oCondition.CastPicUserCharNo = userWoman.curCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastPicCommentSort.COMMENT_DATE_NEW;
				}
				using (CastPicComment oCastPicComment = new CastPicComment()) {
					pMaxPage = oCastPicComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastPicComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_BLOG_ARTICLE_LIKE: {
				BlogArticleLikeSeekCondition oCondition = new BlogArticleLikeSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;
				
				using (BlogArticleLike oBlogArticleLike = new BlogArticleLike()) {
					pMaxPage = oBlogArticleLike.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBlogArticleLike.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_MOVIE_VIEW: {
				string sMovieSeq = iBridUtil.GetStringValue(pRequest.QueryString["movieseq"]);
				ds = new DataSet();

				if (!string.IsNullOrEmpty(sMovieSeq)) {
					using (CastMovie oCastMovie = new CastMovie()) {
						ds = oCastMovie.GetPageCollectionBySeq(sMovieSeq,false);
					}
				}

				if (ds.Tables[0].Rows.Count > 0) {
					totalRowCount = 1;
				}
				break;
			}
			case PwViCommConst.INQUIRY_PERFORMANCE_DAILY: {
				
				ds = new DataSet();
				
				string sReportYear = iBridUtil.GetStringValue(pRequest.QueryString["year"]);
				string sReportMonth = iBridUtil.GetStringValue(pRequest.QueryString["month"]);
				string sDisplayDayFrom = string.Empty;
				string sDisplayDayTo = string.Empty;
				int iDaysInMonth;
				DateTime dtFirstDate;
				
				if (string.IsNullOrEmpty(sReportYear)) {
					sReportYear = DateTime.Now.ToString("yyyy");
				}
				
				if (string.IsNullOrEmpty(sReportMonth)) {
					sReportMonth = DateTime.Now.ToString("MM");
				}
				
				dtFirstDate = DateTime.Parse(string.Format("{0}/{1}/01",sReportYear,sReportMonth));
				sDisplayDayFrom = dtFirstDate.ToString("yyyyMMdd");
				iDaysInMonth = DateTime.DaysInMonth(dtFirstDate.Year,dtFirstDate.Month);
				sDisplayDayTo = string.Format("{0}{1}{2}",sReportYear,sReportMonth,iDaysInMonth.ToString());

				using (TimeOperation oTimeOperation = new TimeOperation()) {
					ds = oTimeOperation.GetPerformanceDaily(site.siteCd,userWoman.userSeq,userWoman.curCharNo,sDisplayDayFrom,sDisplayDayTo);
				}
				
				break;
			}
			case PwViCommConst.INQUIRY_WAIT_SCHEDULE: {
				WaitScheduleSeekCondition oCondition = new WaitScheduleSeekCondition();
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;

				using (WaitSchedule oWaitSchedule = new WaitSchedule()) {
					pMaxPage = oWaitSchedule.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oWaitSchedule.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_REQUEST_LOG: {
				MailRequestLogSeekCondition oCondition = new MailRequestLogSeekCondition();
				oCondition.SiteCd = site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastCharNo = userWoman.curCharNo;

				using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
					pMaxPage = oMailRequestLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oMailRequestLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_LOGIN_PLAN: {
					LoginPlanSeekCondition oCondition = new LoginPlanSeekCondition();
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;

				using (LoginPlan oLoginPlan = new LoginPlan()) {
					pMaxPage = oLoginPlan.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oLoginPlan.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_LOTTERY_TICKET_COUNT: {
				using (MailLotteryTicketCount oMailLotteryTicketCount = new MailLotteryTicketCount()) {
					ds = oMailLotteryTicketCount.GetOne(site.siteCd,userWoman.userSeq,userWoman.curCharNo,ViCommConst.OPERATOR);
				}
				break;
			}
			case PwViCommConst.INQUIRY_NEIGHBOR_USER: {
				ManNeighborUserSeekCondition oCondition = new ManNeighborUserSeekCondition();
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;

				oCondition.LastLoginDate = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd HH:mm:ss");

				List<UserWomanAttr> oAttr = ((UserWomanCharacter)userWoman.characterList[userWoman.curKey]).attrList;
				foreach (UserWomanAttr attr in oAttr) {
					if ("01" == attr.itemNo) {
						oCondition.PrefectureCd = attr.itemCd;
						break;
					}
				}

				using (ManNeighborUser oManNeighborUser = new ManNeighborUser()) {
					pMaxPage = oManNeighborUser.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManNeighborUser.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_DEFECT_REPORT: {
					DefectReportSeekCondition oCondition = new DefectReportSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.BlogFlag = (this.userWoman.CurCharacter.characterEx.enabledBlogFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
				oCondition.RichFlag = (this.userWoman.CurCharacter.characterEx.enabledRichinoFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
				oCondition.DisplayType = PwViCommConst.DefectReportDislayType.PUBLIC;

				using (DefectReport oDefectReport = new DefectReport()) {
					pMaxPage = oDefectReport.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oDefectReport.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_DEFECT_REPORT_PERSONAL: {
				DefectReportSeekCondition oCondition = new DefectReportSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;
				oCondition.SexCd = ViCommConst.OPERATOR;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.BlogFlag = (this.userWoman.CurCharacter.characterEx.enabledBlogFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
				oCondition.RichFlag = (this.userWoman.CurCharacter.characterEx.enabledRichinoFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
				oCondition.DisplayType = PwViCommConst.DefectReportDislayType.PERSONAL;

				using (DefectReport oDefectReport = new DefectReport()) {
					pMaxPage = oDefectReport.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oDefectReport.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_MAIL_PIC_STOCK: {
				CastMailPicStockSeekCondition oCondition = new CastMailPicStockSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;

				using (CastMailPicStock oCastMailPicStock = new CastMailPicStock()) {
					pMaxPage = oCastMailPicStock.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastMailPicStock.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_MAIL_MOVIE_STOCK: {
				CastMailMovieStockSeekCondition oCondition = new CastMailMovieStockSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userWoman.userSeq;
				oCondition.UserCharNo = userWoman.curCharNo;

				using (CastMailMovieStock oCastMailMovieStock = new CastMailMovieStock()) {
					pMaxPage = oCastMailMovieStock.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastMailMovieStock.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_ATTACK_LOGIN: {
				DataSet oDataSetTerm;
				using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
					oDataSetTerm = oMailAttackLogin.GetOneCurrent(site.siteCd);
				}
				
				if (oDataSetTerm.Tables[0].Rows.Count == 0) {
					ds = new DataSet();
					DataTable dt = new DataTable();
					ds.Tables.Add(dt);
					break;
				}
				
				ManMailAttackLoginSeekCondition oCondition = new ManMailAttackLoginSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userWoman.userSeq;
				oCondition.SelfCharNo = userWoman.curCharNo;
				oCondition.MailAttackLoginSeq = iBridUtil.GetStringValue(oDataSetTerm.Tables[0].Rows[0]["MAIL_ATTACK_LOGIN_SEQ"]);
				oCondition.NotLoginMaxDays = iBridUtil.GetStringValue(oDataSetTerm.Tables[0].Rows[0]["NOT_LOGIN_MAX_DAYS"]);
				oCondition.NotLoginMinDays = iBridUtil.GetStringValue(oDataSetTerm.Tables[0].Rows[0]["NOT_LOGIN_MIN_DAYS"]);

				using (ManMailAttackLogin oManMailAttackLogin = new ManMailAttackLogin()) {
					pMaxPage = oManMailAttackLogin.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManMailAttackLogin.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_ATTACK_NEW: {
				DataSet oDataSetTerm;
				using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
					oDataSetTerm = oMailAttackNew.GetOneCurrent(site.siteCd);
				}

				if (oDataSetTerm.Tables[0].Rows.Count == 0) {
					ds = new DataSet();
					DataTable dt = new DataTable();
					ds.Tables.Add(dt);
					break;
				}

				ManMailAttackNewSeekCondition oCondition = new ManMailAttackNewSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userWoman.userSeq;
				oCondition.SelfCharNo = userWoman.curCharNo;
				oCondition.MailAttackNewSeq = iBridUtil.GetStringValue(oDataSetTerm.Tables[0].Rows[0]["MAIL_ATTACK_NEW_SEQ"]);
				oCondition.AfterRegistDays = iBridUtil.GetStringValue(oDataSetTerm.Tables[0].Rows[0]["AFTER_REGIST_DAYS"]);

				using (ManMailAttackNew oManMailAttackNew = new ManMailAttackNew()) {
					pMaxPage = oManMailAttackNew.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManMailAttackNew.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAN_LOGINED_MAIL_SEND: {
				ManLoginedMailSendSeekCondition oCondition = new ManLoginedMailSendSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userWoman.userSeq;
				oCondition.SelfCharNo = userWoman.curCharNo;

				using (ManLoginedMailSend oManLoginedMailSend = new ManLoginedMailSend()) {
					pMaxPage = oManLoginedMailSend.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManLoginedMailSend.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			// アタックリスト（3分以内メール返信）
			case PwViCommConst.INQUIRY_MAIL_ATTACK_LIMIT2_MAIN: {
				// 3分以内メール返信ボーナス用の値を取得
				MailResBonus2 oMailResBonus2 = new MailResBonus2();
				userWoman.characterList[userWoman.curKey].characterEx.mailResBonusStatus = oMailResBonus2.SetEventData(ref userWoman);

				// イベント期間外
				if (ViCommConst.FLAG_OFF_STR.Equals(userWoman.characterList[userWoman.curKey].characterEx.mailResBonusStatus)) {
					ds = new DataSet();
					DataTable dt = new DataTable();
					ds.Tables.Add(dt);
					break;
				}

				// 検索条件のパラメータを設定
				ManMailAttackLimit2SeekCondition oCondition = new ManMailAttackLimit2SeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastUserCharNo = userWoman.curCharNo;
				oCondition.MailResBonusSeq = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusSeq;
				oCondition.ResLimitSec1 = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec1;
				oCondition.ResLimitSec2 = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec2;
				oCondition.ResBonusMaxNum = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusMaxNum;
				oCondition.UnreceivedDays = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusUnreceivedDays;
				oCondition.MainFlag = ViCommConst.FLAG_ON_STR;

				// リストを検索
				using (ManMailAttackLimit2 oMailAttackLimit2 = new ManMailAttackLimit2()) {
					pMaxPage = oMailAttackLimit2.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oMailAttackLimit2.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			// アタックリスト（10分以内メール返信）
			case PwViCommConst.INQUIRY_MAIL_ATTACK_LIMIT2_SUB: {
				// 3分以内メール返信ボーナス用の値を取得
				MailResBonus2 oMailResBonus2 = new MailResBonus2();
				userWoman.characterList[userWoman.curKey].characterEx.mailResBonusStatus = oMailResBonus2.SetEventData(ref userWoman);

				// イベント期間外
				if (ViCommConst.FLAG_OFF_STR.Equals(userWoman.characterList[userWoman.curKey].characterEx.mailResBonusStatus)) {
					ds = new DataSet();
					DataTable dt = new DataTable();
					ds.Tables.Add(dt);
					break;
				}

				// 検索条件のパラメータを設定
				ManMailAttackLimit2SeekCondition oCondition = new ManMailAttackLimit2SeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.CastUserSeq = userWoman.userSeq;
				oCondition.CastUserCharNo = userWoman.curCharNo;
				oCondition.MailResBonusSeq = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusSeq;
				oCondition.ResLimitSec1 = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec1;
				oCondition.ResLimitSec2 = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusLimitSec2;
				oCondition.ResBonusMaxNum = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusMaxNum;
				oCondition.UnreceivedDays = userWoman.characterList[userWoman.curKey].characterEx.mailResBonusUnreceivedDays;
				oCondition.MainFlag = ViCommConst.FLAG_OFF_STR;

				// リストを検索
				using (ManMailAttackLimit2 oMailAttackLimit2 = new ManMailAttackLimit2()) {
					pMaxPage = oMailAttackLimit2.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oMailAttackLimit2.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			// 出演者画像新着いいね一覧
			case PwViCommConst.INQUIRY_CAST_LIKE_NOTICE_PIC: {
				CastLikeNoticePicSeekCondition oCondition = new CastLikeNoticePicSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userWoman.userSeq;
				oCondition.SelfCharNo = userWoman.curCharNo;

				using (CastLikeNoticePic oCastLikeNoticePic = new CastLikeNoticePic()) {
					pMaxPage = oCastLikeNoticePic.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastLikeNoticePic.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			// 出演者動画新着いいね一覧
			case PwViCommConst.INQUIRY_CAST_LIKE_NOTICE_MOVIE: {
				CastLikeNoticeMovieSeekCondition oCondition = new CastLikeNoticeMovieSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userWoman.userSeq;
				oCondition.SelfCharNo = userWoman.curCharNo;

				using (CastLikeNoticeMovie oCastLikeNoticeMovie = new CastLikeNoticeMovie()) {
					pMaxPage = oCastLikeNoticeMovie.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastLikeNoticeMovie.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			// 清算報酬(ギフト券)一覧
			case PwViCommConst.INQUIRY_EXTENSION_PAYMENT_GIFT: {
				GiftCode oGiftCode = new GiftCode();
				ds = oGiftCode.GetGiftCodeTypeCollection();
				break;
			}
			// 清算報酬(ギフト券)枚数選択
			case PwViCommConst.INQUIRY_EXTENSION_PAYMENT_GIFT_SELECT:
			// 清算報酬(ギフト券)交換確認
			case PwViCommConst.INQUIRY_EXTENSION_PAYMENT_GIFT_CONFIRM:
			// 清算報酬(ギフト券)交換完了
			case PwViCommConst.INQUIRY_EXTENSION_PAYMENT_GIFT_COMPLETE:
			{
				// 交換するギフト券の種類と金額
				GiftCode.SearchCondition oCondition = new GiftCode.SearchCondition();
				oCondition.giftCodeType = SessionObjs.Current.requestQuery.QueryString["id"];
				if (string.IsNullOrEmpty(oCondition.giftCodeType)) {
					oCondition.giftCodeType = "0";
				}
				oCondition.amount = SessionObjs.Current.requestQuery.QueryString["amt"];
				if (string.IsNullOrEmpty(oCondition.amount)) {
					oCondition.amount = "0";
				}

				GiftCode oGiftCode = new GiftCode();
				ds = oGiftCode.GetGiftCodeType(oCondition);
				break;
			}
			// ギフト券交換履歴
			case PwViCommConst.INQUIRY_EXTENSION_PAYMENT_HISTORY_GIFT: {
				// 表示条件
				GiftCode.SearchCondition oCondition = new GiftCode.SearchCondition();
				oCondition.siteCd = SessionObjs.Current.site.siteCd;
				oCondition.userSeq = SessionObjs.Current.GetUserSeq();
				oCondition.paymentYear = SessionObjs.Current.requestQuery.QueryString["year"];
				if (string.IsNullOrEmpty(oCondition.paymentYear)) {
					oCondition.paymentYear = DateTime.Today.Year.ToString();
				}

				GiftCode oGiftCode = new GiftCode();
				ds = oGiftCode.GetPaymentHistoryGiftCdCollection(oCondition);
				break;
			}
			// ギフト券交換履歴詳細
			case PwViCommConst.INQUIRY_EXTENSION_PAYMENT_HISTORY_GIFT_DTL: {
				// 表示条件
				GiftCode.SearchCondition oCondition = new GiftCode.SearchCondition();
				oCondition.siteCd = SessionObjs.Current.site.siteCd;
				oCondition.userSeq = SessionObjs.Current.GetUserSeq();
				oCondition.giftCdRowId = SessionObjs.Current.requestQuery.QueryString["rid"];
				if (string.IsNullOrEmpty(oCondition.giftCdRowId)) {
					ds = new DataSet();
					DataTable dt = new DataTable();
					ds.Tables.Add(dt);
					break;
				}

				GiftCode oGiftCode = new GiftCode();
				ds = oGiftCode.GetPaymentHistoryGiftCdCollection(oCondition);
				break;
			}
		}

		return ds;
	}

	public string GetBlogCommentValue(string pFieldNm) {
		bool bExist;
		return GetRowValue(pFieldNm,PwViCommConst.DATASET_BLOG_COMMENT,out bExist);
	}
}
