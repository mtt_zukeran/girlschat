﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class Battle:DbSession {
	public Battle() {
	}

	public void BattleStart(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerUserSeq,
		string pPartnerUserCharNo,
		string pSupportUserSeq,
		string pSupportUserCharNo,
		string pSupportBattleLogSeq,
		string pSupportRequestSubSeq,
		string pBattleType,
		string pTreasureSeq,
		int pSurelyWinFlag,
		int pAttackForceCount,
		out int pLevelUpFlag,
		out int pAddForceCount,
		out int pTreasureCompleteFlag,
		out int pBonusGetFlag,
		out int pWinFlag,
		out int pComePoliceFlag,
		out int pTrapUsedFlag,
		out string pBattleLogSeq,
		out string[] pBreakGameItemSeq,
		out string pResult
	) {
		int pGetCompCountItemFlag;
		string[] pGetCompCountItemSeq;
		string[] pGetCompCountItemCount;
		
		BattleStart(
			pSiteCd,
			pUserSeq,
			pUserCharNo,
			pPartnerUserSeq,
			pPartnerUserCharNo,
			pSupportUserSeq,
			pSupportUserCharNo,
			pSupportBattleLogSeq,
			pSupportRequestSubSeq,
			pBattleType,
			pTreasureSeq,
			pSurelyWinFlag,
			pAttackForceCount,
			out pLevelUpFlag,
			out pAddForceCount,
			out pTreasureCompleteFlag,
			out pBonusGetFlag,
			out pWinFlag,
			out pComePoliceFlag,
			out pTrapUsedFlag,
			out pBattleLogSeq,
			out pBreakGameItemSeq,
			out pGetCompCountItemFlag,
			out pGetCompCountItemSeq,
			out pGetCompCountItemCount,
			out pResult
		);
	}

	public void BattleStart(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerUserSeq,
		string pPartnerUserCharNo,
		string pSupportUserSeq,
		string pSupportUserCharNo,
		string pSupportBattleLogSeq,
		string pSupportRequestSubSeq,
		string pBattleType,
		string pTreasureSeq,
		int pSurelyWinFlag,
		int pAttackForceCount,
		out int pLevelUpFlag,
		out int pAddForceCount,
		out int pTreasureCompleteFlag,
		out int pBonusGetFlag,
		out int pWinFlag,
		out int pComePoliceFlag,
		out int pTrapUsedFlag,
		out string pBattleLogSeq,
		out string[] pBreakGameItemSeq,
		out int pGetCompCountItemFlag,
		out string[] pGetCompCountItemSeq,
		out string[] pGetCompCountItemCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BATTLE_START");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pSUPPORT_USER_SEQ",DbSession.DbType.VARCHAR2,pSupportUserSeq);
			db.ProcedureInParm("pSUPPORT_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pSupportUserCharNo);
			db.ProcedureInParm("pSUPPORT_BATTLE_LOG_SEQ",DbSession.DbType.VARCHAR2,pSupportBattleLogSeq);
			db.ProcedureInParm("pSUPPORT_REQUEST_SUBSEQ",DbSession.DbType.VARCHAR2,pSupportRequestSubSeq);
			db.ProcedureInParm("pBATTLE_TYPE",DbSession.DbType.VARCHAR2,pBattleType);
			db.ProcedureInParm("pTREASURE_SEQ",DbSession.DbType.VARCHAR2,pTreasureSeq);
			db.ProcedureInParm("pSURELY_WIN_FLAG",DbSession.DbType.VARCHAR2,pSurelyWinFlag);
			db.ProcedureBothParm("pATTACK_FORCE_COUNT",DbSession.DbType.VARCHAR2,pAttackForceCount);
			db.ProcedureOutParm("pLEVEL_UP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pADD_FORCE_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pTREASURE_COMPLETE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pBONUS_GET_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pWIN_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCOME_POLICE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pTRAP_USED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pATTACK_FORCE_POWER",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pDEFENCE_FORCE_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pBATTLE_LOG_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pBREAK_GAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pBREAK_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pATTACK_FELLOW_USER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pATTACK_FELLOW_USER_CHAR_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pATTACK_FELLOW_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pDEFENCE_FELLOW_USER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pDEFENCE_FELLOW_USER_CHAR_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pDEFENCE_FELLOW_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pGET_COMP_COUNT_ITEM_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pGET_COMP_COUNT_ITEM_SEQ",DbSession.DbType.ARRAY_VARCHAR2);
			db.ProcedureOutArrayParm("pGET_COMP_COUNT_ITEM_COUNT",DbSession.DbType.ARRAY_VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pLevelUpFlag = db.GetIntValue("pLEVEL_UP_FLAG");
			pAddForceCount = db.GetIntValue("pADD_FORCE_COUNT");
			pTreasureCompleteFlag = db.GetIntValue("pTREASURE_COMPLETE_FLAG");
			pBonusGetFlag = db.GetIntValue("pBONUS_GET_FLAG");
			pWinFlag = db.GetIntValue("pWIN_FLAG");
			pComePoliceFlag = db.GetIntValue("pCOME_POLICE_FLAG");
			pTrapUsedFlag = db.GetIntValue("pTRAP_USED_FLAG");
			pBattleLogSeq = db.GetStringValue("pBATTLE_LOG_SEQ");
			int length = db.GetArrySize("pBREAK_GAME_ITEM_SEQ");

			pBreakGameItemSeq = new string[db.GetArrySize("pBREAK_GAME_ITEM_SEQ")];
			for (int i = 0;i < db.GetArrySize("pBREAK_GAME_ITEM_SEQ");i++) {
				pBreakGameItemSeq[i] = db.GetArryStringValue("pBREAK_GAME_ITEM_SEQ",i);
			}

			pGetCompCountItemFlag = db.GetIntValue("pGET_COMP_COUNT_ITEM_FLAG");

			pGetCompCountItemSeq = new string[db.GetArrySize("pGET_COMP_COUNT_ITEM_SEQ")];
			pGetCompCountItemCount = new string[db.GetArrySize("pGET_COMP_COUNT_ITEM_COUNT")];
			for (int i = 0;i < db.GetArrySize("pGET_COMP_COUNT_ITEM_SEQ");i++) {
				pGetCompCountItemSeq[i] = db.GetArryStringValue("pGET_COMP_COUNT_ITEM_SEQ",i);
				pGetCompCountItemCount[i] = db.GetArryStringValue("pGET_COMP_COUNT_ITEM_COUNT",i);
			}

			pResult = db.GetStringValue("pRESULT");
		}
	}
}

