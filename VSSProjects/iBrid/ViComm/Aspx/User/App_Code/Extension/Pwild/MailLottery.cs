﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メールdeガチャ設定
--	Progaram ID		: MailLottery
--
--  Creation Date	: 2015.01.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class MailLottery:DbSession {
	public MailLottery() {
	}

	public bool GetMailLotteryOpening(string pSiteCd,string pSexCd) {
		bool bExists = false;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	1														");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_MAIL_LOTTERY											");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	SEX_CD			= :SEX_CD							AND	");
		oSqlBuilder.AppendLine("	SYSDATE BETWEEN START_DATE AND END_DATE					");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			bExists = true;
		}

		return bExists;
	}

	public void GetMailLottery(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		string pMailSeq,
		string pPartnerMailSeq,
		int pCheckOnlyFlag,
		out string pMailLotteryRateSeq,
		out string pLoseFlag,
		out string pResult
		) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_MAIL_LOTTERY");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pMAIL_SEQ",DbSession.DbType.VARCHAR2,pMailSeq);
			db.ProcedureInParm("pPARTNER_MAIL_SEQ",DbSession.DbType.VARCHAR2,pPartnerMailSeq);
			db.ProcedureInParm("pCHECK_ONLY_FLAG",DbSession.DbType.NUMBER,pCheckOnlyFlag);
			db.ProcedureOutParm("pMAIL_LOTTERY_RATE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLOSE_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pMailLotteryRateSeq = db.GetStringValue("pMAIL_LOTTERY_RATE_SEQ");
			pLoseFlag = db.GetStringValue("pLOSE_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void GetMailLotterySecond(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		int pCheckOnlyFlag,
		out string pMailLotteryRateSeq,
		out string pMailLotteryRewardPoint,
		out string pResult
		) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_MAIL_LOTTERY_SECOND");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pCHECK_ONLY_FLAG",DbSession.DbType.NUMBER,pCheckOnlyFlag);
			db.ProcedureOutParm("pMAIL_LOTTERY_RATE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pMAIL_LOTTERY_REWARD_POINT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pMailLotteryRateSeq = db.GetStringValue("pMAIL_LOTTERY_RATE_SEQ");
			pMailLotteryRewardPoint = db.GetStringValue("pMAIL_LOTTERY_REWARD_POINT");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
