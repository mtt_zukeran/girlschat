﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: イベント開催地域
--	Progaram ID		: EventArea
--
--  Creation Date	: 2013.07.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class EventArea:DbSession {
	public EventArea() {
	}

	public DataSet GetList(string pPrefectureCd) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	EVENT_AREA_CD						,	");
		oSqlBuilder.AppendLine("	EVENT_AREA_NM							");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_EVENT_AREA							");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	PREFECTURE_CD	= :PREFECTURE_CD		");
		oSqlBuilder.AppendLine("ORDER BY EVENT_AREA_CD ASC					");
		
		oParamList.Add(new OracleParameter(":PREFECTURE_CD",pPrefectureCd));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}
