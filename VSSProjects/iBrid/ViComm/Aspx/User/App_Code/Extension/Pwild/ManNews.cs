﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員ニュース
--	Progaram ID		: ManNews
--
--  Creation Date	: 2013.09.09
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManNewsSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string OnlyFavorit;

	public ManNewsSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManNewsSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class ManNews:DbSession {
	private SessionObjs _sessionObj = null;
	protected SessionObjs sessionObj {
		get {
			if (_sessionObj == null) {
				_sessionObj = SessionObjs.Current;
			}
			return _sessionObj;
		}
	}

	public ManNews()
		: this(SessionObjs.Current) {
	}

	public ManNews(SessionObjs pSessionObj) {
		this._sessionObj = pSessionObj;
	}

	public int GetPageCount(ManNewsSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_NEWS01 P");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManNewsSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	NEWS_TYPE,");
		oSqlBuilder.AppendLine("	NEWS_DATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_NEWS01 P");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManNewsSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		string sCastUserSeq = string.Empty;
		string sCastCharNo = string.Empty;

		if (sessionObj.sexCd.Equals(ViCommConst.WOMAN)) {
			SessionWoman sessionWoman = (SessionWoman)sessionObj;
			sCastUserSeq = sessionWoman.userWoman.userSeq;
			sCastCharNo = sessionWoman.userWoman.curCharNo;
		}

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (pCondition.OnlyFavorit.Equals(ViCommConst.FLAG_ON_STR) && !sCastUserSeq.Equals(string.Empty) && !sCastCharNo.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :FAVORIT_CAST_USER_SEQ AND USER_CHAR_NO = :FAVORIT_CAST_CHAR_NO AND PARTNER_USER_SEQ = P.USER_SEQ AND PARTNER_USER_CHAR_NO = P.USER_CHAR_NO)",ref pWhereClause);
			oParamList.Add(new OracleParameter("FAVORIT_CAST_USER_SEQ",sCastUserSeq));
			oParamList.Add(new OracleParameter("FAVORIT_CAST_CHAR_NO",sCastCharNo));
		}

		if (!sCastUserSeq.Equals(string.Empty) && !sCastCharNo.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :REFUSE_CAST_USER_SEQ AND PARTNER_USER_CHAR_NO = :REFUSE_CAST_CHAR_NO)",ref pWhereClause);
			oParamList.Add(new OracleParameter("REFUSE_CAST_USER_SEQ",sCastUserSeq));
			oParamList.Add(new OracleParameter("REFUSE_CAST_CHAR_NO",sCastCharNo));
		}

		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(ManNewsSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY NEWS_DATE DESC";

		return sSortExpression;
	}

	public void LogManNews(
		string pSiteCd,
		string pManUserSeq,
		int pNewsType
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_MAN_NEWS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pNEWS_TYPE",DbSession.DbType.NUMBER,pNewsType);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
