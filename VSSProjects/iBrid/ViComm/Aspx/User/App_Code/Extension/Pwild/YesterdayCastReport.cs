﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者日別集計
--	Progaram ID		: YesterdayCastReport
--  Creation Date	: 2012.07.09
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class YesterdayCastReportSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public YesterdayCastReportSeekCondition()
		: this(new NameValueCollection()) {
	}

	public YesterdayCastReportSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class YesterdayCastReport:DbSession {
	public YesterdayCastReport() {
	}

	public int GetPageCount(YesterdayCastReportSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_YESTERDAY_CAST_REPORT	");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public string GetAllPayAmtRank(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sRank = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	ALL_PAY_AMT_RANK			");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	T_YESTERDAY_CAST_REPORT		");
		oSqlBuilder.AppendLine("WHERE							");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND		");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sRank = oDataSet.Tables[0].Rows[0]["ALL_PAY_AMT_RANK"].ToString();
		}

		return sRank;
	}

	public DataSet GetPageCollection(YesterdayCastReportSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	ALL_PAY_AMT,");
		oSqlBuilder.AppendLine("	ALL_PAY_AMT_RANK");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_YESTERDAY_CAST_REPORT");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(YesterdayCastReportSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(YesterdayCastReportSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY ALL_PAY_AMT_RANK ASC";

		return sSortExpression;
	}
}