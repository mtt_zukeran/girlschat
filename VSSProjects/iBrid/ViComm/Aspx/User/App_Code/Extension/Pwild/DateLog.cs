﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: デート履歴
--	Progaram ID		: DateLog
--
--  Creation Date	: 2012.10.08
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

[Serializable]
public class DateLogSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}
	
	public string ChargeFlag {
		get {
			return this.Query["charge_flag"];
		}
		set {
			this.Query["charge_flag"] = value;
		}
	}

	public DateLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public DateLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["charge_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["charge_flag"]));
	}
}

[System.Serializable]
public class DateLog:ManBase {
	public DateLog()
		: base() {
	}

	public DateLog(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(DateLogSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																				");
		oSqlBuilder.AppendLine("	COUNT(*)																		");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	VW_PW_DATE_LOG01 DL															,	");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P												,	");
		oSqlBuilder.AppendLine(" 	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine(" 			SITE_CD																,	");
		oSqlBuilder.AppendLine(" 			USER_SEQ															,	");
		oSqlBuilder.AppendLine(" 			USER_CHAR_NO														,	");
		oSqlBuilder.AppendLine(" 			RANK																,	");
		oSqlBuilder.AppendLine(" 			FRIENDLY_POINT															");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00														");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO							");
		oSqlBuilder.AppendLine(" 	) FP																			");
		oSqlBuilder.AppendLine("WHERE																				");
		oSqlBuilder.AppendLine("	DL.SITE_CD							= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("	DL.USER_SEQ							= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("	DL.USER_CHAR_NO						= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("	DL.SITE_CD							= FP.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("	DL.USER_SEQ							= FP.USER_SEQ						(+)	AND	");
		oSqlBuilder.AppendLine("	DL.USER_CHAR_NO						= FP.USER_CHAR_NO					(+)	AND	");
		oSqlBuilder.AppendLine("	DL.SITE_CD							= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	DL.PARTNER_USER_SEQ					= :PARTNER_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	DL.PARTNER_USER_CHAR_NO				= :PARTNER_USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG						= :STAFF_FLAG							AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG							IN(:NA_FLAG,:NA_FLAG2)					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																		");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			*																		");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_REFUSE																");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD									AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO							");
		oSqlBuilder.AppendLine("	)																				");

		if (!string.IsNullOrEmpty(pCondtion.ChargeFlag)) {
			oSqlBuilder.AppendLine("	AND DL.CHARGE_FLAG	= :CHARGE_FLAG");
			oParamList.Add(new OracleParameter("CHARGE_FLAG",pCondtion.ChargeFlag));
		}

		oParamList.Add(new OracleParameter("SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter("PARTNER_USER_SEQ",pCondtion.PartnerUserSeq));
		oParamList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pCondtion.PartnerUserCharNo));
		oParamList.Add(new OracleParameter("STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter("NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(DateLogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();


		oSqlBuilder.AppendLine("SELECT																				");
		oSqlBuilder.AppendLine("	DL.DATE_PLAN_NM																,	");
		oSqlBuilder.AppendLine("	DL.PAYMENT																	,	");
		oSqlBuilder.AppendLine("	DL.CREATE_DATE																,	");
		oSqlBuilder.AppendLine("	DL.CHARGE_FLAG																,	");
		oSqlBuilder.AppendLine("		" + ManBasicField() + "													,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM															,	");
		oSqlBuilder.AppendLine("	FP.RANK AS FRIENDLY_RANK													,	");
		oSqlBuilder.AppendLine("	FP.FRIENDLY_POINT																");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	VW_PW_DATE_LOG01 DL															,	");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P												,	");
		oSqlBuilder.AppendLine(" 	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine(" 			SITE_CD																,	");
		oSqlBuilder.AppendLine(" 			USER_SEQ															,	");
		oSqlBuilder.AppendLine(" 			USER_CHAR_NO														,	");
		oSqlBuilder.AppendLine(" 			RANK																,	");
		oSqlBuilder.AppendLine(" 			FRIENDLY_POINT															");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00														");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO							");
		oSqlBuilder.AppendLine(" 	) FP																			");
		oSqlBuilder.AppendLine("WHERE																				");
		oSqlBuilder.AppendLine("	DL.SITE_CD							= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("	DL.USER_SEQ							= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("	DL.USER_CHAR_NO						= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("	DL.SITE_CD							= FP.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("	DL.USER_SEQ							= FP.USER_SEQ						(+)	AND	");
		oSqlBuilder.AppendLine("	DL.USER_CHAR_NO						= FP.USER_CHAR_NO					(+)	AND	");
		oSqlBuilder.AppendLine("	DL.SITE_CD							= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	DL.PARTNER_USER_SEQ					= :PARTNER_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	DL.PARTNER_USER_CHAR_NO				= :PARTNER_USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG						= :STAFF_FLAG							AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG							IN(:NA_FLAG,:NA_FLAG2)					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																		");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			*																		");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_REFUSE																");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD									AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO							");
		oSqlBuilder.AppendLine("	)																				");
		
		if (!string.IsNullOrEmpty(pCondtion.ChargeFlag)) {
			oSqlBuilder.AppendLine("	AND DL.CHARGE_FLAG	= :CHARGE_FLAG");
			oParamList.Add(new OracleParameter("CHARGE_FLAG",pCondtion.ChargeFlag));
		}
		
		oParamList.Add(new OracleParameter("SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter("PARTNER_USER_SEQ",pCondtion.PartnerUserSeq));
		oParamList.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pCondtion.PartnerUserCharNo));
		oParamList.Add(new OracleParameter("STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter("NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		string sSortExpression = " ORDER BY DL.CREATE_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}
}

