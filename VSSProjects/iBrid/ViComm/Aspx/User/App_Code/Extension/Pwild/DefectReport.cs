﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 不具合報告
--	Progaram ID		: DefectReport
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class DefectReportSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string SexCd;
	public string DeleteFlag;
	public string BlogFlag;
	public string RichFlag;
	public string DisplayType;

	public string DefectReportSeq {
		get {
			return this.Query["reportseq"];
		}
		set {
			this.Query["reportseq"] = value;
		}
	}

	public string DefectReportCategorySeq {
		get {
			return this.Query["ctgseq"];
		}
		set {
			this.Query["ctgseq"] = value;
		}
	}

	public string DefectReportProgressSeq {
		get {
			return this.Query["prgseq"];
		}
		set {
			this.Query["prgseq"] = value;
		}
	}

	public string Keyword {
		get {
			return this.Query["kwd"];
		}
		set {
			this.Query["kwd"] = value;
		}
	}

	public DefectReportSeekCondition()
		: this(new NameValueCollection()) {
	}

	public DefectReportSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["reportseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["reportseq"]));
		this.Query["ctgseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ctgseq"]));
		this.Query["prgseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["prgseq"]));
		this.Query["kwd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["kwd"]));
	}
}

public class DefectReport:DbSession {
	public DefectReport() {
	}

	public int GetPageCount(DefectReportSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT	P				,	");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_CATEGORY DRC		");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(DefectReportSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(DefectReportSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sJoinTableNm = string.Empty;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		if (pCondtion.SexCd.Equals(ViCommConst.MAN)) {
			sJoinTableNm = "T_USER_MAN_CHARACTER";
		} else {
			sJoinTableNm = "T_CAST_CHARACTER";
		}

		oSqlBuilder.AppendLine("SELECT																				");
		oSqlBuilder.AppendLine("	P.SITE_CD																	,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ																	,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO																,	");
		oSqlBuilder.AppendLine("	P.SEX_CD																	,	");
		oSqlBuilder.AppendLine("	P.DEFECT_REPORT_SEQ															,	");
		oSqlBuilder.AppendLine("	P.TITLE																		,	");
		oSqlBuilder.AppendLine("	P.TEXT																		,	");
		oSqlBuilder.AppendLine("	P.ADMIN_COMMENT																,	");
		oSqlBuilder.AppendLine("	P.OVERLAP_COUNT																,	");
		oSqlBuilder.AppendLine("	P.CREATE_DATE																,	");
		oSqlBuilder.AppendLine("	P.DEFECT_REPORT_CATEGORY_NM													,	");
		oSqlBuilder.AppendLine("	DRP.DEFECT_REPORT_PROGRESS_NM												,	");
		oSqlBuilder.AppendLine("	DRP.DEFECT_REPORT_PROGRESS_COLOR											,	");
		oSqlBuilder.AppendLine("	C.HANDLE_NM																	,	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID																		");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			P.SITE_CD															,	");
		oSqlBuilder.AppendLine("			P.USER_SEQ															,	");
		oSqlBuilder.AppendLine("			P.USER_CHAR_NO														,	");
		oSqlBuilder.AppendLine("			P.SEX_CD															,	");
		oSqlBuilder.AppendLine("			P.DEFECT_REPORT_SEQ													,	");
		oSqlBuilder.AppendLine("			P.TITLE																,	");
		oSqlBuilder.AppendLine("			P.TEXT																,	");
		oSqlBuilder.AppendLine("			P.ADMIN_COMMENT														,	");
		oSqlBuilder.AppendLine("			P.DEFECT_REPORT_CATEGORY_SEQ										,	");
		oSqlBuilder.AppendLine("			P.DEFECT_REPORT_PROGRESS_SEQ										,	");
		oSqlBuilder.AppendLine("			P.CREATE_DATE														,	");
		oSqlBuilder.AppendLine("			P.OVERLAP_COUNT														,	");
		oSqlBuilder.AppendLine("			DRC.DEFECT_REPORT_CATEGORY_NM											");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_DEFECT_REPORT P													,	");
		oSqlBuilder.AppendLine("			T_DEFECT_REPORT_CATEGORY DRC											");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) P																			,	");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_PROGRESS DRP												,	");
		oSqlBuilder.AppendFormat("	{0} C																		,	",sJoinTableNm).AppendLine();
		oSqlBuilder.AppendLine("	T_USER U																		");
		oSqlBuilder.AppendLine("WHERE																				");
		oSqlBuilder.AppendLine("	P.SITE_CD							= DRP.SITE_CD							AND	");
		oSqlBuilder.AppendLine("	P.DEFECT_REPORT_PROGRESS_SEQ		= DRP.DEFECT_REPORT_PROGRESS_SEQ		AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD							= C.SITE_CD								AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= C.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= C.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= U.USER_SEQ								");
		

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(DefectReportSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.SITE_CD = DRC.SITE_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.DEFECT_REPORT_CATEGORY_SEQ = DRC.DEFECT_REPORT_CATEGORY_SEQ",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.SEX_CD = DRC.SEX_CD",ref pWhereClause);

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("P.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("P.SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.DeleteFlag)) {
			SysPrograms.SqlAppendWhere("P.DELETE_FLAG = :DELETE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DELETE_FLAG",pCondition.DeleteFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.DefectReportSeq)) {
			SysPrograms.SqlAppendWhere("P.DEFECT_REPORT_SEQ = :DEFECT_REPORT_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DEFECT_REPORT_SEQ",pCondition.DefectReportSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.DefectReportCategorySeq)) {
			SysPrograms.SqlAppendWhere("P.DEFECT_REPORT_CATEGORY_SEQ = :DEFECT_REPORT_CATEGORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DEFECT_REPORT_CATEGORY_SEQ",pCondition.DefectReportCategorySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.DefectReportProgressSeq)) {
			SysPrograms.SqlAppendWhere("P.DEFECT_REPORT_PROGRESS_SEQ = :DEFECT_REPORT_PROGRESS_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DEFECT_REPORT_PROGRESS_SEQ",pCondition.DefectReportProgressSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.Keyword)) {
			SysPrograms.SqlAppendWhere("(P.TITLE LIKE '%' || :KEYWORD01 || '%' OR P.TEXT LIKE '%' || :KEYWORD02 || '%' OR P.ADMIN_COMMENT LIKE '%' || :KEYWORD03 || '%')",ref pWhereClause);
			oParamList.Add(new OracleParameter(":KEYWORD01",pCondition.Keyword));
			oParamList.Add(new OracleParameter(":KEYWORD02",pCondition.Keyword));
			oParamList.Add(new OracleParameter(":KEYWORD03",pCondition.Keyword));
		}

		if (!string.IsNullOrEmpty(pCondition.DisplayType)) {
			switch (pCondition.DisplayType) {
				case PwViCommConst.DefectReportDislayType.PUBLIC:
					SysPrograms.SqlAppendWhere("P.PUBLIC_STATUS IN (:PUBLIC_STATUS_ACCEPT,:PUBLIC_STATUS_PUBLIC)",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PUBLIC_STATUS_ACCEPT",PwViCommConst.DefectReportPublicStatus.ACCEPT));
					oParamList.Add(new OracleParameter(":PUBLIC_STATUS_PUBLIC",PwViCommConst.DefectReportPublicStatus.PUBLIC));
					break;
				case PwViCommConst.DefectReportDislayType.PERSONAL:
					SysPrograms.SqlAppendWhere("P.PUBLIC_STATUS IN (:PUBLIC_STATUS_APPLY,:PUBLIC_STATUS_ACCEPT,:PUBLIC_STATUS_PUBLIC)",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PUBLIC_STATUS_APPLY",PwViCommConst.DefectReportPublicStatus.APPLY));
					oParamList.Add(new OracleParameter(":PUBLIC_STATUS_ACCEPT",PwViCommConst.DefectReportPublicStatus.ACCEPT));
					oParamList.Add(new OracleParameter(":PUBLIC_STATUS_PUBLIC",PwViCommConst.DefectReportPublicStatus.PUBLIC));
					break;
				default:
					break;
			}
		}

		if (!string.IsNullOrEmpty(pCondition.BlogFlag)) {
			SysPrograms.SqlAppendWhere("DRC.BLOG_FLAG = :BLOG_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_FLAG",pCondition.BlogFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.RichFlag)) {
			SysPrograms.SqlAppendWhere("DRC.RICH_FLAG = :RICH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":RICH_FLAG",pCondition.RichFlag));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(DefectReportSeekCondition pCondition) {
		return " ORDER BY P.CREATE_DATE DESC";
	}

	public DataSet GetDefectReportProgress(string pSiteCd) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_PROGRESS_SEQ	,	");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_PROGRESS_NM		");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_PROGRESS		");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetDefectReportCategory(string pSiteCd,string pSexCd,string pBlogFlag,string pRichFlag) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		if (!string.IsNullOrEmpty(pBlogFlag)) {
			SysPrograms.SqlAppendWhere("BLOG_FLAG = :BLOG_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_FLAG",pBlogFlag));
		}

		if (!string.IsNullOrEmpty(pRichFlag)) {
			SysPrograms.SqlAppendWhere("RICH_FLAG = :RICH_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":RICH_FLAG",pRichFlag));
		}

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_CATEGORY_SEQ	,	");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_CATEGORY_NM		");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_CATEGORY		");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public string WriteDefectReport(
		string pSiteCd,
		string pSexCd,
		string pUserSeq,
		string pUserCharNo,
		string pTitle,
		string pText,
		string pDefectReportCategorySeq,
		out string pDefectReportSeq
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_DEFECT_REPORT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pTITLE",DbSession.DbType.VARCHAR2,pTitle);
			db.ProcedureInParm("pTEXT",DbSession.DbType.VARCHAR2,pText);
			db.ProcedureInParm("pDEFECT_REPORT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,pDefectReportCategorySeq);
			db.ProcedureOutParm("pDEFECT_REPORT_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pDefectReportSeq = db.GetStringValue("pDEFECT_REPORT_SEQ");
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}
}
