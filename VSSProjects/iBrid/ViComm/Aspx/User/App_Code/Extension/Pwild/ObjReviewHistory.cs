﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝レビュー履歴
--	Progaram ID		: ObjReviewHistory
--  Creation Date	: 2012.04.21
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ObjReviewHistorySeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string CastUserSeq;
	public string CastUserCharNo;
	public string CommentFlag;

	public string ObjSeq {
		get {
			return this.Query["objseq"];
		}
		set {
			this.Query["objseq"] = value;
		}
	}

	public string ObjReviewHistorySeq {
		get {
			return this.Query["history_seq"];
		}
		set {
			this.Query["history_seq"] = value;
		}
	}

	public ObjReviewHistorySeekCondition()
		: this(new NameValueCollection()) {
	}

	public ObjReviewHistorySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["objseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["objseq"]));
		this.Query["history_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["history_seq"]));
	}
}

public class ObjReviewHistory:DbSession {

	public ObjReviewHistory() {
	}

	public int GetRecCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pObjSeq
	) {
		decimal dRecCount = 0;

		ObjReviewHistorySeekCondition oCondition = new ObjReviewHistorySeekCondition();
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.ObjSeq = pObjSeq;

		this.GetPageCount(oCondition,1,out dRecCount);

		return (int)dRecCount;
	}

	public int GetPageCount(ObjReviewHistorySeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_PW_OBJ_REVIEW_HISTORY01	");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ObjReviewHistorySeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(ObjReviewHistorySeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	*							");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_PW_OBJ_REVIEW_HISTORY01	");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ObjReviewHistorySeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_USER_CHAR_NO = :CAST_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_CHAR_NO",pCondition.CastUserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjSeq)) {
			SysPrograms.SqlAppendWhere("OBJ_SEQ = :OBJ_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_SEQ",pCondition.ObjSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjReviewHistorySeq)) {
			SysPrograms.SqlAppendWhere("OBJ_REVIEW_HISTORY_SEQ = :OBJ_REVIEW_HISTORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_REVIEW_HISTORY_SEQ",pCondition.ObjReviewHistorySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CommentFlag)) {
			if (pCondition.CommentFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("COMMENT_DOC IS NOT NULL",ref pWhereClause);
				SysPrograms.SqlAppendWhere("COMMENT_DEL_FLAG = 0",ref pWhereClause);
			} else if (pCondition.CommentFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				SysPrograms.SqlAppendWhere("COMMENT_DOC IS NULL",ref pWhereClause);
			}
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(ObjReviewHistorySeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY CREATE_DATE DESC";

		return sSortExpression;
	}

	public DataSet GetObjReviewTotal(string pSiteCd,string pObjSeq) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("OBJ_SEQ = :OBJ_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD			,	");
		oSqlBuilder.AppendLine("	OBJ_SEQ			,	");
		oSqlBuilder.AppendLine("	REVIEW_COUNT	,	");
		oSqlBuilder.AppendLine("	COMMENT_COUNT	,	");
		oSqlBuilder.AppendLine("	GOOD_STAR_TOTAL	,	");
		oSqlBuilder.AppendLine("	GOOD_POINT_TOTAL,	");
		oSqlBuilder.AppendLine("	GOOD_1_COUNT	,	");
		oSqlBuilder.AppendLine("	GOOD_2_COUNT	,	");
		oSqlBuilder.AppendLine("	GOOD_3_COUNT	,	");
		oSqlBuilder.AppendLine("	GOOD_4_COUNT	,	");
		oSqlBuilder.AppendLine("	GOOD_5_COUNT		");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_OBJ_REVIEW_TOTAL	");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		
		return oDataSet;
	}

	public DataSet GetObjReviewTotalForParts(string pSiteCd,string pObjSeq) {
		DataSet oDataSet = this.GetObjReviewTotal(pSiteCd,pObjSeq);

		if (oDataSet.Tables[0].Rows.Count == 0) {
			DataRow oNewRow = oDataSet.Tables[0].NewRow();
			oNewRow["SITE_CD"] = pSiteCd;
			oNewRow["OBJ_SEQ"] = pObjSeq;
			oNewRow["REVIEW_COUNT"] = "0";
			oNewRow["COMMENT_COUNT"] = "0";
			oNewRow["GOOD_STAR_TOTAL"] = "0";
			oNewRow["GOOD_POINT_TOTAL"] = "0";
			oNewRow["GOOD_1_COUNT"] = "0";
			oNewRow["GOOD_2_COUNT"] = "0";
			oNewRow["GOOD_3_COUNT"] = "0";
			oNewRow["GOOD_4_COUNT"] = "0";
			oNewRow["GOOD_5_COUNT"] = "0";
			oDataSet.Tables[0].Rows.Add(oNewRow);
		}

		return oDataSet;
	}

	public string WriteObjReview(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pObjSeq,
		int pGoodStar,
		string pCommentDoc,
		int pAddPoint
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_OBJ_REVIEW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,pObjSeq);
			db.ProcedureInParm("pGOOD_STAR",DbSession.DbType.NUMBER,pGoodStar);
			db.ProcedureInParm("pCOMMENT_DOC",DbSession.DbType.VARCHAR2,pCommentDoc);
			db.ProcedureInParm("pADD_POINT",DbSession.DbType.NUMBER,pAddPoint);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string DeleteObjReviewComment(
		string pSiteCd,
		string pObjReviewHistorySeq,
		string pUserSeq,
		string pUserCharNo
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_OBJ_REVIEW_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pOBJ_REVIEW_HISTORY_SEQ",DbSession.DbType.VARCHAR2,pObjReviewHistorySeq);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string DeleteObjReviewCommentPic(
		string pSiteCd,
		string pObjReviewHistorySeq,
		string pUserSeq,
		string pUserCharNo
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_OBJ_REVIEW_PIC");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pOBJ_REVIEW_HISTORY_SEQ",DbSession.DbType.VARCHAR2,pObjReviewHistorySeq);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string DeleteObjReviewCommentMovie(
		string pSiteCd,
		string pObjReviewHistorySeq,
		string pUserSeq,
		string pUserCharNo
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_OBJ_REVIEW_MOVIE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pOBJ_REVIEW_HISTORY_SEQ",DbSession.DbType.VARCHAR2,pObjReviewHistorySeq);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}
	
	public bool CheckExistsByObjSeq(string pSiteCd,string pUserSeq,string pUserCharNo,string pObjSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	1										");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_OBJ_REVIEW_HISTORY					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	OBJ_SEQ			= :OBJ_SEQ				");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
