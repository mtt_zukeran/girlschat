﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰﾊｸﾞされた一覧
--	Progaram ID		: CastGameCommunicationPartnerHug
--
--  Creation Date	: 2011.08.12
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCommunicationPartnerHugSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string LastHugLogCheckDate {
		get {
			return this.Query["last_hug_log_check_date"];
		}
		set {
			this.Query["last_hug_log_check_date"] = value;
		}
	}

	public CastGameCommunicationPartnerHugSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCommunicationPartnerHugSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[System.Serializable]
public class CastGameCommunicationPartnerHug:DbSession {

	public int GetPageCount(CastGameCommunicationPartnerHugSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHR_PTR_HUG01 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" AND ROWNUM <= 20					");
		
		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCommunicationPartnerHugSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFellow = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT																	");
		oSqlBuilder.AppendLine("	SITE_CD												,				");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM										,				");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE									,				");
		oSqlBuilder.AppendLine("	HANDLE_NM											,				");
		oSqlBuilder.AppendLine("	AGE													,				");
		oSqlBuilder.AppendLine("	LAST_PARTNER_HUG_DATE								,				");
		oSqlBuilder.AppendLine("	LAST_HUG_DATE										,				");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ									,				");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO								,				");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH								,				");
		oSqlBuilder.AppendLine("	ACT_CATEGORY_IDX									,				");
		oSqlBuilder.AppendLine("	LOGIN_ID											,				");
		oSqlBuilder.AppendLine("	CRYPT_VALUE											,				");
		oSqlBuilder.AppendLine("	USER_SEQ											,				");
		oSqlBuilder.AppendLine("	USER_CHAR_NO														");
		oSqlBuilder.AppendLine(" FROM																	");
		oSqlBuilder.AppendLine("	(SELECT																");
		oSqlBuilder.AppendLine("		SITE_CD											,				");
		oSqlBuilder.AppendLine("		GAME_HANDLE_NM									,				");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_TYPE								,				");
		oSqlBuilder.AppendLine("		HANDLE_NM										,				");
		oSqlBuilder.AppendLine("		AGE												,				");
		oSqlBuilder.AppendLine("		LAST_PARTNER_HUG_DATE							,				");
		oSqlBuilder.AppendLine("		LAST_HUG_DATE									,				");
		oSqlBuilder.AppendLine("		PARTNER_USER_SEQ								,				");
		oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO							,				");
		oSqlBuilder.AppendLine("		SMALL_PHOTO_IMG_PATH							,				");
		oSqlBuilder.AppendLine("		ACT_CATEGORY_IDX								,				");
		oSqlBuilder.AppendLine("		LOGIN_ID										,				");
		oSqlBuilder.AppendLine("		CRYPT_VALUE										,				");
		oSqlBuilder.AppendLine("		TUTORIAL_MISSION_FLAG							,				");
		oSqlBuilder.AppendLine("		TUTORIAL_MISSION_TREASURE_FLAG					,				");
		oSqlBuilder.AppendLine("		TUTORIAL_BATTLE_FLAG							,				");
		oSqlBuilder.AppendLine("		TUTORIAL_BATTLE_TREASURE_FLAG					,				");
		oSqlBuilder.AppendLine("		USER_SEQ										,				");
		oSqlBuilder.AppendLine("		USER_CHAR_NO													");
		oSqlBuilder.AppendLine("	FROM																");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHR_PTR_HUG01 P									");
		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	)	WHERE ROWNUM <= 20  											");
		
		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		
		oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT																	");
		oSqlBuilder.AppendLine("	COM.SITE_CD										,					");
		oSqlBuilder.AppendLine("	COM.GAME_HANDLE_NM								,					");
		oSqlBuilder.AppendLine("	COM.GAME_CHARACTER_TYPE							,					");
		oSqlBuilder.AppendLine("	COM.HANDLE_NM									,					");
		oSqlBuilder.AppendLine("	COM.AGE											,					");
		oSqlBuilder.AppendLine("	COM.LAST_PARTNER_HUG_DATE						,					");
		oSqlBuilder.AppendLine("	COM.LAST_HUG_DATE								,					");
		oSqlBuilder.AppendLine("	COM.PARTNER_USER_SEQ							,					");
		oSqlBuilder.AppendLine("	COM.PARTNER_USER_CHAR_NO						,					");
		oSqlBuilder.AppendLine("	CASE																");
		oSqlBuilder.AppendLine("		WHEN FAVORIT.PROFILE_PIC_NON_PUBLISH_FLAG = 2					");
		oSqlBuilder.AppendLine("		THEN GET_SMALL_PHOTO_IMG_PATH(COM.SITE_CD,COM.LOGIN_ID,NULL)	");
		oSqlBuilder.AppendLine("		ELSE COM.SMALL_PHOTO_IMG_PATH									");
		oSqlBuilder.AppendLine("	END AS SMALL_PHOTO_IMG_PATH						,					");
		oSqlBuilder.AppendLine("	COM.ACT_CATEGORY_IDX							,					");
		oSqlBuilder.AppendLine("	COM.LOGIN_ID									,					");
		oSqlBuilder.AppendLine("	COM.CRYPT_VALUE									,					");
		oSqlBuilder.AppendLine("	COM.RNUM										,					");
		oSqlBuilder.AppendLine("	FELLOW.FELLOW_APPLICATION_STATUS									");
		oSqlBuilder.AppendLine(" FROM(																	");
		oSqlBuilder.AppendFormat("{0}",sExecSql);
		oSqlBuilder.AppendLine(" 	) COM,																");
		oSqlBuilder.AppendLine(" 	(SELECT																");
		oSqlBuilder.AppendLine(" 		T_GAME_FELLOW.SITE_CD						,					");
		oSqlBuilder.AppendLine(" 		T_GAME_FELLOW.PARTNER_USER_SEQ				,					");
		oSqlBuilder.AppendLine(" 		T_GAME_FELLOW.PARTNER_USER_CHAR_NO			,					");
		oSqlBuilder.AppendLine(" 		T_GAME_FELLOW.FELLOW_APPLICATION_STATUS							");
		oSqlBuilder.AppendLine(" 	FROM																");
		oSqlBuilder.AppendLine(" 		T_GAME_FELLOW													");
		// where		
		this.CreateWhereFellow(pCondtion,ref sWhereClauseFellow);
		oSqlBuilder.AppendLine(sWhereClauseFellow);
		
		oSqlBuilder.AppendLine(" 	) FELLOW										,					");
		oSqlBuilder.AppendLine("	T_FAVORIT FAVORIT													");
		oSqlBuilder.AppendLine(" WHERE																	");
		oSqlBuilder.AppendLine(" 	COM.SITE_CD					= FELLOW.SITE_CD				(+) AND	");
		oSqlBuilder.AppendLine(" 	COM.PARTNER_USER_SEQ		= FELLOW.PARTNER_USER_SEQ		(+) AND	");
		oSqlBuilder.AppendLine(" 	COM.PARTNER_USER_CHAR_NO	= FELLOW.PARTNER_USER_CHAR_NO	(+)	AND	");
		oSqlBuilder.AppendLine("	COM.SITE_CD					= FAVORIT.SITE_CD				(+)	AND	");
		oSqlBuilder.AppendLine("	COM.PARTNER_USER_SEQ		= FAVORIT.USER_SEQ				(+)	AND	");
		oSqlBuilder.AppendLine("	COM.PARTNER_USER_CHAR_NO	= FAVORIT.USER_CHAR_NO			(+) AND	");
		oSqlBuilder.AppendLine("	COM.USER_SEQ				= FAVORIT.PARTNER_USER_SEQ		(+) AND	");
		oSqlBuilder.AppendLine("	COM.USER_CHAR_NO			= FAVORIT.PARTNER_USER_CHAR_NO	(+)		");
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneNewHug(CastGameCommunicationPartnerHugSeekCondition pCondtion) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT																	");
		oSqlBuilder.AppendLine("	SITE_CD												,				");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM										,				");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE									,				");
		oSqlBuilder.AppendLine("	HANDLE_NM											,				");
		oSqlBuilder.AppendLine("	AGE													,				");
		oSqlBuilder.AppendLine("	LAST_PARTNER_HUG_DATE								,				");
		oSqlBuilder.AppendLine("	LAST_HUG_DATE										,				");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ									,				");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO								,				");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH								,				");
		oSqlBuilder.AppendLine("	ACT_CATEGORY_IDX									,				");
		oSqlBuilder.AppendLine("	LOGIN_ID											,				");
		oSqlBuilder.AppendLine("	CRYPT_VALUE															");
		oSqlBuilder.AppendLine(" FROM																	");
		oSqlBuilder.AppendLine("	(SELECT																");
		oSqlBuilder.AppendLine("		SITE_CD											,				");
		oSqlBuilder.AppendLine("		GAME_HANDLE_NM									,				");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_TYPE								,				");
		oSqlBuilder.AppendLine("		HANDLE_NM										,				");
		oSqlBuilder.AppendLine("		AGE												,				");
		oSqlBuilder.AppendLine("		LAST_PARTNER_HUG_DATE							,				");
		oSqlBuilder.AppendLine("		LAST_HUG_DATE									,				");
		oSqlBuilder.AppendLine("		PARTNER_USER_SEQ								,				");
		oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO							,				");
		oSqlBuilder.AppendLine("		SMALL_PHOTO_IMG_PATH							,				");
		oSqlBuilder.AppendLine("		ACT_CATEGORY_IDX								,				");
		oSqlBuilder.AppendLine("		LOGIN_ID										,				");
		oSqlBuilder.AppendLine("		CRYPT_VALUE														");
		oSqlBuilder.AppendLine("	FROM																");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHR_PTR_HUG01 P									");
		// where		
		oParamList.AddRange(this.CreateWhereInf(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	)	WHERE ROWNUM <= 1	 											");
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastGameCommunicationPartnerHugSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
		
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" USER_SEQ		= :USER_SEQ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		

		SysPrograms.SqlAppendWhere(" USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		SysPrograms.SqlAppendWhere("(NA_FLAG IN (:NA_FLAG,:NA_FLAG_2))",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.PARTNER_USER_SEQ AND USER_CHAR_NO = P.PARTNER_USER_CHAR_NO AND PARTNER_USER_SEQ = :USER_SEQ AND PARTNER_USER_CHAR_NO = :USER_CHAR_NO)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" LAST_PARTNER_HUG_DATE IS NOT NULL",ref pWhereClause);
		
		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereInf(CastGameCommunicationPartnerHugSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		SysPrograms.SqlAppendWhere("(NA_FLAG IN (:NA_FLAG,:NA_FLAG_2))",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" PARTNER_HUG_COUNT > 0	",ref pWhereClause);
		
		if (!string.IsNullOrEmpty(pCondition.LastHugLogCheckDate)) {
			SysPrograms.SqlAppendWhere(" TO_CHAR(LAST_PARTNER_HUG_DATE,'YYYY/MM/DD HH24:MI:SS') > :LAST_HUG_LOG_CHECK_DATE ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LAST_HUG_LOG_CHECK_DATE",pCondition.LastHugLogCheckDate));
		}

		return oParamList.ToArray();
	}

	private void CreateWhereFellow(CastGameCommunicationPartnerHugSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" T_GAME_FELLOW.SITE_CD = :SITE_CD",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" T_GAME_FELLOW.USER_SEQ = :USER_SEQ",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" T_GAME_FELLOW.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
		}
	}

	protected string CreateOrderExpresion(CastGameCommunicationPartnerHugSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = " ORDER BY LAST_PARTNER_HUG_DATE DESC NULLS LAST";
		return sSortExpression;
	}
}

