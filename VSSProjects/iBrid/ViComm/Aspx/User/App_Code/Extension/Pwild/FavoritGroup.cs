﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: お気に入りグループ
--	Progaram ID		: FavoritGroup
--
--  Creation Date	: 2012.10.02
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class FavoritGroupSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string SexCd;
	public string FavoritGroupSeq {
		get {
			return this.Query["grpseq"];
		}
		set {
			this.Query["grpseq"] = value;
		}
	}

	public FavoritGroupSeekCondition()
		: this(new NameValueCollection()) {
	}

	public FavoritGroupSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["grpseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["grpseq"]));
	}
}

public class FavoritGroup:DbSession {
	public FavoritGroup() {
	}

	public int GetPageCount(FavoritGroupSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine("FROM				");
		oSqlBuilder.AppendLine("	T_FAVORIT_GROUP	");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(FavoritGroupSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sExecSql = string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD,			");
		oSqlBuilder.AppendLine("	USER_SEQ,			");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,		");
		oSqlBuilder.AppendLine("	FAVORIT_GROUP_SEQ,	");
		oSqlBuilder.AppendLine("	FAVORIT_GROUP_NM	");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_FAVORIT_GROUP		");
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	public DataSet GetPageCollectionWithUserCount(FavoritGroupSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		DataSet oDataSet = GetPageCollection(pCondition,pPageNo,pRecPerPage);
		return AppendUserCount(oDataSet,pCondition);
	}

	private DataSet AppendUserCount(DataSet pDataSet,FavoritGroupSeekCondition pCondition) {
		string sCharacterTbl = string.Empty;
		pDataSet.Tables[0].Columns.Add(new DataColumn("USER_COUNT",Type.GetType("System.String")));

		foreach (DataRow dr in pDataSet.Tables[0].Rows) {
			dr["USER_COUNT"] = 0;
		}

		if (pDataSet.Tables[0].Rows.Count == 0 || string.IsNullOrEmpty(pCondition.UserSeq) || string.IsNullOrEmpty(pCondition.UserCharNo) || string.IsNullOrEmpty(pCondition.SexCd)) {
			return pDataSet;
		}

		if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
			sCharacterTbl = "T_CAST_CHARACTER";
		} else {
			sCharacterTbl = "T_USER_MAN_CHARACTER";
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*) AS USER_COUNT,");
		oSqlBuilder.AppendLine("	F.FAVORIT_GROUP_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FAVORIT F,");
		oSqlBuilder.AppendFormat("	{0} C",sCharacterTbl).AppendLine();
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	F.SITE_CD		= :SITE_CD					AND");
		oSqlBuilder.AppendLine("	F.USER_SEQ		= :USER_SEQ					AND");
		oSqlBuilder.AppendLine("	F.USER_CHAR_NO	= :USER_CHAR_NO				AND");
		oSqlBuilder.AppendLine("	F.FAVORIT_GROUP_SEQ IS NOT NULL				AND");
		oSqlBuilder.AppendLine("	C.SITE_CD		= F.SITE_CD					AND");
		oSqlBuilder.AppendLine("	C.USER_SEQ		= F.PARTNER_USER_SEQ		AND");
		oSqlBuilder.AppendLine("	C.USER_CHAR_NO	= F.PARTNER_USER_CHAR_NO	AND");
		oSqlBuilder.AppendLine("	C.NA_FLAG		= :NA_FLAG					AND");
		oSqlBuilder.AppendLine("	NOT EXISTS(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			1");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_REFUSE R");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			R.SITE_CD				= F.SITE_CD					AND");
		oSqlBuilder.AppendLine("			R.USER_SEQ				= F.PARTNER_USER_SEQ		AND");
		oSqlBuilder.AppendLine("			R.USER_CHAR_NO			= F.PARTNER_USER_CHAR_NO	AND");
		oSqlBuilder.AppendLine("			R.PARTNER_USER_SEQ		= F.USER_SEQ				AND");
		oSqlBuilder.AppendLine("			R.PARTNER_USER_CHAR_NO	= F.USER_CHAR_NO");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	F.SITE_CD,");
		oSqlBuilder.AppendLine("	F.USER_SEQ,");
		oSqlBuilder.AppendLine("	F.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	F.FAVORIT_GROUP_SEQ");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		DataSet oTmpDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oTmpDataSet.Tables[0].Rows.Count != 0) {
			oTmpDataSet.Tables[0].PrimaryKey = new DataColumn[] { oTmpDataSet.Tables[0].Columns["FAVORIT_GROUP_SEQ"] };

			foreach (DataRow dr in pDataSet.Tables[0].Rows) {
				if (oTmpDataSet.Tables[0].Rows.Find(dr["FAVORIT_GROUP_SEQ"]) != null) {
					DataRow oTmpDataRow = oTmpDataSet.Tables[0].Rows.Find(dr["FAVORIT_GROUP_SEQ"]);
					dr["USER_COUNT"] = oTmpDataRow["USER_COUNT"];
				}
			}
		}

		return pDataSet;
	}

	private OracleParameter[] CreateWhere(FavoritGroupSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.FavoritGroupSeq)) {
			SysPrograms.SqlAppendWhere("FAVORIT_GROUP_SEQ = :FAVORIT_GROUP_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FAVORIT_GROUP_SEQ",pCondition.FavoritGroupSeq));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(FavoritGroupSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY FAVORIT_GROUP_SEQ ASC";

		return sSortExpression;
	}

	public string RegistFavoritGroup(string pSiteCd,string pUserSeq,string pUserCharNo,string pFavoritGroupNm) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_FAVORIT_GROUP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pFAVORIT_GROUP_NM",DbSession.DbType.VARCHAR2,pFavoritGroupNm);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string ModifyFavoritGroup(string pSiteCd,string pUserSeq,string pUserCharNo,string pFavoritGroupSeq,string pFavoritGroupNm) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_FAVORIT_GROUP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pFAVORIT_GROUP_SEQ",DbSession.DbType.VARCHAR2,pFavoritGroupSeq);
			db.ProcedureInParm("pFAVORIT_GROUP_NM",DbSession.DbType.VARCHAR2,pFavoritGroupNm);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string SetFavoritGroup(string pSiteCd,string pUserSeq,string pUserCharNo,string[] pPartnerUserSeq,string[] pPartnerUserCharNo,string pFavoritGroupSeq) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_FAVORIT_GROUP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInArrayParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInArrayParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pFAVORIT_GROUP_SEQ",DbSession.DbType.VARCHAR2,pFavoritGroupSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}
}
