﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: メールアタックイベント(登録後24時間以内の男性)
--	Progaram ID		: ManMailAttackNew
--  Creation Date	: 2016.05.06
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManMailAttackNewSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;
	public string SelfCharNo;
	public string MailAttackNewSeq;
	public string AfterRegistDays;

	public ManMailAttackNewSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManMailAttackNewSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[Serializable]
public class ManMailAttackNew:ManBase {
	public ManMailAttackNew() {
	}

	public int GetPageCount(ManMailAttackNewSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P	");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManMailAttackNewSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sInnerSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oInnerSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		sSortExpression = this.CreateOrderExpression(pCondition);

		oInnerSqlBuilder.AppendLine("SELECT															");
		oInnerSqlBuilder.AppendLine("	P.SITE_CD												,	");
		oInnerSqlBuilder.AppendLine("	P.USER_SEQ												,	");
		oInnerSqlBuilder.AppendLine("	P.USER_CHAR_NO											,	");
		oInnerSqlBuilder.AppendLine("	P.HANDLE_NM												,	");
		oInnerSqlBuilder.AppendLine("	P.AGE													,	");
		oInnerSqlBuilder.AppendLine("	P.LOGIN_ID												,	");
		oInnerSqlBuilder.AppendLine("	P.PIC_SEQ												,	");
		oInnerSqlBuilder.AppendLine("	P.LAST_LOGIN_DATE										,	");
		oInnerSqlBuilder.AppendLine("	P.TOTAL_RX_MAIL_COUNT									,	");
		oInnerSqlBuilder.AppendLine("	P.TOTAL_TX_MAIL_COUNT									,	");
		oInnerSqlBuilder.AppendLine("	P.CHARACTER_ONLINE_STATUS								,	");
		oInnerSqlBuilder.AppendLine("	P.REC_SEQ												,	");
		oInnerSqlBuilder.AppendLine("	P.REGIST_DATE												");
		oInnerSqlBuilder.AppendLine("FROM															");
		oInnerSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P									");
		oInnerSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oInnerSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sInnerSql));
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	T1.*																							,	");
		oSqlBuilder.AppendLine("	GET_MAN_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0) AS PHOTO_IMG_PATH					,	");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("	NVL(COMM.TOTAL_RX_MAIL_COUNT,0) AS PARTNER_TOTAL_RX_MAIL_COUNT									,	");
		oSqlBuilder.AppendLine("	CASE																								");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 0																");
		oSqlBuilder.AppendLine("		THEN SM.OFFLINE_GUIDANCE																		");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 1																");
		oSqlBuilder.AppendLine("		THEN SM.LOGINED_GUIDANCE																		");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 2																");
		oSqlBuilder.AppendLine("		THEN SM.WAITING_GUIDANCE																		");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 3																");
		oSqlBuilder.AppendLine("		THEN SM.TALKING_WSHOT_GUIDANCE																	");
		oSqlBuilder.AppendLine("	END AS MOBILE_ONLINE_STATUS_NM																		");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine(sInnerSql);
		oSqlBuilder.AppendLine("	) T1																							,	");
		oSqlBuilder.AppendLine("	T_COMMUNICATION COMM																			,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT SM																				");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	T1.SITE_CD		= SM.SITE_CD																	AND	");
		oSqlBuilder.AppendLine("	T1.SITE_CD		= COMM.SITE_CD																(+) AND	");
		oSqlBuilder.AppendLine("	T1.USER_SEQ		= COMM.USER_SEQ																(+)	AND	");
		oSqlBuilder.AppendLine("	T1.USER_CHAR_NO	= COMM.USER_CHAR_NO															(+)	AND	");
		oSqlBuilder.AppendLine("	:SELF_USER_SEQ	= COMM.PARTNER_USER_SEQ														(+)	AND	");
		oSqlBuilder.AppendLine("	:SELF_CHAR_NO	= COMM.PARTNER_USER_CHAR_NO													(+)		");
		oSqlBuilder.AppendLine("ORDER BY T1.RNUM																						");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendManAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManMailAttackNewSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_CHAR_NO",pCondition.SelfCharNo));

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere("P.REGIST_DATE >= SYSDATE - :AFTER_REGIST_DAYS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":AFTER_REGIST_DAYS",pCondition.AfterRegistDays));

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("(															");
		oWhereBuilder.AppendLine("	P.USER_RANK		= :USER_RANK						OR	");
		oWhereBuilder.AppendLine("	EXISTS													");
		oWhereBuilder.AppendLine("	(														");
		oWhereBuilder.AppendLine("		SELECT												");
		oWhereBuilder.AppendLine("			1												");
		oWhereBuilder.AppendLine("		FROM												");
		oWhereBuilder.AppendLine("			T_MAIL_ATTACK_NEW_TX_LOG						");
		oWhereBuilder.AppendLine("		WHERE												");
		oWhereBuilder.AppendLine("			SITE_CD				= P.SITE_CD				AND	");
		oWhereBuilder.AppendLine("			MAIL_ATTACK_NEW_SEQ	= :MAIL_ATTACK_NEW_SEQ	AND	");
		oWhereBuilder.AppendLine("			TX_USER_SEQ			= :SELF_USER_SEQ		AND	");
		oWhereBuilder.AppendLine("			TX_CHAR_NO			= :SELF_CHAR_NO			AND	");
		oWhereBuilder.AppendLine("			RX_USER_SEQ			= P.USER_SEQ			AND	");
		oWhereBuilder.AppendLine("			RX_CHAR_NO			= P.USER_CHAR_NO			");
		oWhereBuilder.AppendLine("	)														");
		oWhereBuilder.AppendLine(")															");
		SysPrograms.SqlAppendWhere(oWhereBuilder.ToString(),ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_RANK",ViCommConst.USER_RANK_A));
		oParamList.Add(new OracleParameter(":MAIL_ATTACK_NEW_SEQ",pCondition.MailAttackNewSeq));

		SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_CHAR_NO)",ref pWhereClause);

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	private string CreateOrderExpression(ManMailAttackNewSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case (PwViCommConst.MailAttackNewSort.REGIST_DATE_DESC): {
					sSortExpression = "ORDER BY P.REGIST_DATE DESC,P.USER_SEQ ASC";
					break;
				}
			case (PwViCommConst.MailAttackNewSort.TOTAL_RX_MAIL_COUNT_ASC): {
					sSortExpression = "ORDER BY P.TOTAL_RX_MAIL_COUNT ASC,P.USER_SEQ ASC";
					break;
				}
			case (PwViCommConst.MailAttackNewSort.LAST_LOGIN_DATE_DESC): {
					sSortExpression = "ORDER BY P.LAST_LOGIN_DATE DESC NULLS LAST,P.USER_SEQ ASC";
					break;
				}
			default:
				sSortExpression = "ORDER BY P.REGIST_DATE DESC,P.USER_SEQ ASC";
				break;
		}
		return sSortExpression;
	}
}
