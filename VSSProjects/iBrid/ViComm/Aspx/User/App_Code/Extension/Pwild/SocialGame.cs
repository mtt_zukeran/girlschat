﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・基本設定
--	Progaram ID		: SocialGame
--
--  Creation Date	: 2011.08.29
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class SocialGameSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public SocialGameSeekCondition()
		: this(new NameValueCollection()) {
	}

	public SocialGameSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
	}
}


[System.Serializable]
public class SocialGame:DbSession {
	public SocialGame() {
	}

	public DataSet GetOne(SocialGameSeekCondition pCondtion) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	DATE_LIMIT_COUNT				,	");
		oSqlBuilder.AppendLine("	PART_TIME_JOB_LIMIT_COUNT		,	");
		oSqlBuilder.AppendLine("	ADD_FELLOW_FORCE				,	");
		oSqlBuilder.AppendLine("	REMOVE_FELLOW_FORCE				,	");
		oSqlBuilder.AppendLine("	REMOVE_FELLOW_PARTNER_FORCE		,	");
		oSqlBuilder.AppendLine("	FORCE_RECOVERY_COOP_POINT		,	");
		oSqlBuilder.AppendLine("	PT_BATTLE_ATTACK_INTERVAL_MIN	,	");
		oSqlBuilder.AppendLine("	HUG_BONUS_RATE					,	");
		oSqlBuilder.AppendLine("	MAN_GAME_SERVICE_POINT			,	");
		oSqlBuilder.AppendLine("	CAST_GAME_SERVICE_POINT			,	");
		oSqlBuilder.AppendLine("	PT_BATTLE_ATTACK_LIMIT_COUNT	,	");
		oSqlBuilder.AppendLine("	GAME_START_DATE					,	");
		oSqlBuilder.AppendLine("	BATTLE_WIN_EXP					,	");
		oSqlBuilder.AppendLine("	BATTLE_POWER_PERCENT_FROM		,	");
		oSqlBuilder.AppendLine("	BATTLE_POWER_PERCENT_TO				");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	T_SOCIAL_GAME						");

		// where	
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DateTime GetGameStartDate(string pSiteCd) {
		DateTime dtGameStartDate = DateTime.Now;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = pSiteCd;

		DataSet ds = this.GetOne(oCondition);

		if (ds.Tables[0].Rows.Count > 0) {
			dtGameStartDate = DateTime.Parse(ds.Tables[0].Rows[0]["GAME_START_DATE"].ToString());
		}

		return dtGameStartDate;
	}

	public void GetBattlePowerPercent(string pSiteCd,out int pBattlePowerPercentFrom,out int pBattlePowerPercentTo) {
		pBattlePowerPercentFrom = 0;
		pBattlePowerPercentTo = 0;

		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = pSiteCd;
		DataSet ds = this.GetOne(oCondition);

		if (ds.Tables[0].Rows.Count > 0) {
			pBattlePowerPercentFrom = int.Parse(ds.Tables[0].Rows[0]["BATTLE_POWER_PERCENT_FROM"].ToString());
			pBattlePowerPercentTo = int.Parse(ds.Tables[0].Rows[0]["BATTLE_POWER_PERCENT_TO"].ToString());
		}
	}

	private OracleParameter[] CreateWhere(SocialGameSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		return oParamList.ToArray();
	}
}
