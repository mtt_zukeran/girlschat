﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class GameLoginBonusMonthlySeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string SexCd;
	public string Month;
	public string PublishFlag;

	public GameLoginBonusMonthlySeekCondition()
		: this(new NameValueCollection()) {
	}
	
	public GameLoginBonusMonthlySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

#endregion ============================================================================================================

public class GameLoginBonusMonthly:DbSession {
	public GameLoginBonusMonthly() {
	}

	public int GetPageCount(GameLoginBonusMonthlySeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM02 P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameLoginBonusMonthlySeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(GameLoginBonusMonthlySeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	P.SITE_CD					,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ				,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_NM				,	");
		oSqlBuilder.AppendLine("	P.NEED_LOGIN_COUNT				");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM02	P			");
		
		// where
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		string sSortExpression = string.Empty;
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		AppendAttr(oDataSet,pCondition);
		
		return oDataSet; 
	}

	private OracleParameter[] CreateWhere(GameLoginBonusMonthlySeekCondition pCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (pCondition == null)
			throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		if (!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD	= :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!String.IsNullOrEmpty(pCondition.Month)) {
			SysPrograms.SqlAppendWhere(" P.GAME_LOGIN_BONUS_MONTH = :GAME_LOGIN_BONUS_MONTH",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_LOGIN_BONUS_MONTH",pCondition.Month));
		}
		
		if(!String.IsNullOrEmpty(pCondition.PublishFlag)) {
			SysPrograms.SqlAppendWhere(" P.PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PUBLISH_FLAG",pCondition.PublishFlag));
		}

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(GameLoginBonusMonthlySeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			default:
				sSortExpression = " ORDER BY P.NEED_LOGIN_COUNT ASC ";
				break;
		}
		return sSortExpression;
	}
	
	private void AppendAttr(DataSet pDS,GameLoginBonusMonthlySeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("REST_NEED_LOGIN_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn(string.Format("MONTHLY_BONUS_GET_FLAG"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		
		int iRestNeedCount;
		int iNeedCount;
		
		string sGetMonthlyBonusFlag;
		
		int iSelfLoginCount = GetLoginCountMonthly(pCondition);
		
		foreach(DataRow dr in pDS.Tables[0].Rows) {
			iRestNeedCount = 0;
			iNeedCount = 0;
			iNeedCount = int.Parse(dr["NEED_LOGIN_COUNT"].ToString());
			
			sGetMonthlyBonusFlag = "0";
			
			if(iSelfLoginCount < iNeedCount) {
				iRestNeedCount = iNeedCount - iSelfLoginCount;
			}
			else if(iSelfLoginCount == iNeedCount) {
				sGetMonthlyBonusFlag = "1";
			}
			
			dr["REST_NEED_LOGIN_COUNT"] =iRestNeedCount.ToString();
			dr["MONTHLY_BONUS_GET_FLAG"] = sGetMonthlyBonusFlag;
		}
	}
	
	private int GetLoginCountMonthly(GameLoginBonusMonthlySeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)	AS SELF_LOGIN_COUNT		");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_GAME_LOGIN_BONUS_DAY_LOG	P		");

		//Where
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string pWhereClause = string.Empty;
		SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" P.USER_SEQ	= :USER_SEQ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));

		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= :USER_CHAR_NO",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		SysPrograms.SqlAppendWhere(" P.REPORT_DAY LIKE :REPORT_DAY",ref pWhereClause);
		oParamList.Add(new OracleParameter(":REPORT_DAY",string.Format("{0}%",pCondition.Month)));

		oSqlBuilder.AppendLine(pWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		DataRow subDR = subDS.Tables[0].Rows[0];

		int iSelfLoginCount = int.Parse(subDR["SELF_LOGIN_COUNT"].ToString());
		
		return iSelfLoginCount;
	}
	
	public DataSet GetLoginRecord(GameLoginBonusMonthlySeekCondition pCondition) {
		DataSet ds = new DataSet();

		int iSelfLoginCount = GetLoginCountMonthly(pCondition);
		
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	MIN(P.NEED_LOGIN_COUNT)	AS NEXT_NEED_COUNT		");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_GAME_LOGIN_BONUS_MONTHLY	P					");

		// where
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;

		if (!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD	= :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!String.IsNullOrEmpty(pCondition.Month)) {
			SysPrograms.SqlAppendWhere(" P.GAME_LOGIN_BONUS_MONTH = :GAME_LOGIN_BONUS_MONTH",ref sWhereClause);
			oParamList.Add(new OracleParameter(":GAME_LOGIN_BONUS_MONTH",pCondition.Month));
		}
		
		if (iSelfLoginCount > 0) {
			SysPrograms.SqlAppendWhere(" P.NEED_LOGIN_COUNT	> :SELF_LOGIN_COUNT",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SELF_LOGIN_COUNT",iSelfLoginCount));
		}
		
		oParamList.AddRange(oParamList.ToArray());
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		DataColumn col;
		col = new DataColumn(string.Format("SELF_LOGIN_COUNT"),System.Type.GetType("System.String"));
		oDataSet.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("REST_NEXT_LOGIN_COUNT"),System.Type.GetType("System.String"));
		oDataSet.Tables[0].Columns.Add(col);
		
		DataRow dr = oDataSet.Tables[0].Rows[0];
		dr["SELF_LOGIN_COUNT"] = iSelfLoginCount.ToString();

		if (!string.IsNullOrEmpty(dr["NEXT_NEED_COUNT"].ToString())) {
			int iRestNeedCount = (int.Parse(dr["NEXT_NEED_COUNT"].ToString()) - iSelfLoginCount);
		
			dr["REST_NEXT_LOGIN_COUNT"] = iRestNeedCount.ToString();
		}
		
		return 	oDataSet;
	}

	public void GetLoginBonus(
		string pSiteCd							,
		string pUserSeq							,
		string pUserCharNo						,
		out int pDailyBonusFlag					,
		out int pMonthlyBonusFlag				,
		out string pResult						
		) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_LOGIN_BONUS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pDAILY_BONUS_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pDAILY_GET_GAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pDAILY_GET_GAME_ITEM_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pDAILY_GET_GAME_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pMONTHLY_BONUS_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pMONTHLY_GET_GAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pMONTHLY_GET_GAME_ITEM_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			pDailyBonusFlag =db.GetIntValue("pDAILY_BONUS_FLAG");
			pMonthlyBonusFlag = db.GetIntValue("pMONTHLY_BONUS_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
