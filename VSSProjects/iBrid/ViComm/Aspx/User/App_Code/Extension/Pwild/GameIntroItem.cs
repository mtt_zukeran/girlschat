﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・友達紹介獲得アイテム
--	Progaram ID		: GameIntroItem
--
--  Creation Date	: 2012.02.08
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class GameIntroItem:DbSession {
	public GameIntroItem() {
	}
	
	public DataSet GetList(
		string sSiteCd,
		string sGameIntroSeq
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	SITE_CD					,				");
		oSqlBuilder.AppendLine("	GAME_INTRO_SEQ			,				");
		oSqlBuilder.AppendLine("	ITEM_COUNT				,				");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ			,				");
		oSqlBuilder.AppendLine("	SEX_CD					,				");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM			,				");
		oSqlBuilder.AppendLine("	ATTACK_POWER			,				");
		oSqlBuilder.AppendLine("	DEFENCE_POWER			,				");
		oSqlBuilder.AppendLine("	ENDURANCE				,				");
		oSqlBuilder.AppendLine("	DESCRIPTION				,				");
		oSqlBuilder.AppendLine("	REMARKS					,				");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_CD		,				");
		oSqlBuilder.AppendLine("	PRICE					,				");
		oSqlBuilder.AppendLine("	PRESENT_FLAG			,				");
		oSqlBuilder.AppendLine("	PRESENT_GAME_ITEM_SEQ	,				");
		oSqlBuilder.AppendLine("	STAGE_SEQ				,				");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE	,				");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG			,				");
		oSqlBuilder.AppendLine("	PUBLISH_DATE			,				");
		oSqlBuilder.AppendLine("	CREATE_DATE								");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_PW_GAME_INTRO_ITEM01					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	GAME_INTRO_SEQ	= :GAME_INTRO_SEQ		");
		oSqlBuilder.AppendLine("ORDER BY									");
		oSqlBuilder.AppendLine("	SITE_CD, GAME_ITEM_SEQ					");

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":GAME_INTRO_SEQ",sGameIntroSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}
	
	public string GetGameIntroItem(string pSiteCd,string pUserSeq,string pUserCharNo,string pGameIntroLogSeq) {
		string sResult;
		
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_GAME_INTRO_ITEM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pGAME_INTRO_LOG_SEQ",DbSession.DbType.VARCHAR2,pGameIntroLogSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}
		return sResult;
	}

	public DataSet GetItemListByIntroCount(string sSiteCd,string sSexCd,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	ITEM_COUNT												,	");
		oSqlBuilder.AppendLine("	INTRO_MIN												,	");
		oSqlBuilder.AppendLine("	INTRO_MAX												,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ											,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM											,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER											,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER											,	");
		oSqlBuilder.AppendLine("	ENDURANCE												,	");
		oSqlBuilder.AppendLine("	RANK() OVER(ORDER BY INTRO_MIN ASC,GAME_ITEM_SEQ ASC) AS INTRO_RANK		");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	VW_PW_GAME_INTRO_ITEM02										");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("	SEX_CD	= :SEX_CD										AND	");
		//回復アイテム・ワナを除外
		oSqlBuilder.AppendFormat("	GAME_ITEM_CATEGORY_GROUP_TYPE NOT IN({0},{1})		\n",PwViCommConst.GameItemCatregoryGroup.RESTORE,PwViCommConst.GameItemCatregoryGroup.TRAP);

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",sSexCd));

		// order by
		string sSortExpression = string.Empty;
		sSortExpression = "ORDER BY INTRO_MIN ASC,GAME_ITEM_SEQ ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
}
