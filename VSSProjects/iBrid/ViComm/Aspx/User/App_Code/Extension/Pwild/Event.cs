﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: イベント情報
--	Progaram ID		: Event
--
--  Creation Date	: 2013.07.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class EventSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string EventCategorySeq {
		get {
			return this.Query["eventcategory"];
		}
		set {
			this.Query["eventcategory"] = value;
		}
	}
	
	public string PrefectureCd {
		get {
			return this.Query["prefecture"];
		}
		set {
			this.Query["prefecture"] = value;
		}
	}
	
	public string EventAreaCd {
		get {
			return this.Query["area"];
		}
		set {
			this.Query["area"] = value;
		}
	}

	public string EventDate {
		get {
			return this.Query["eventdate"];
		}
		set {
			this.Query["eventdate"] = value;
		}
	}
	
	public string NotEndFlag {
		get {
			return this.Query["notend"];
		}
		set {
			this.Query["notend"] = value;
		}
	}

	public EventSeekCondition()
		: this(new NameValueCollection()) {
	}

	public EventSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["eventcategory"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["eventcategory"]));
		this.Query["prefecture"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["prefecture"]));
		this.Query["area"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["area"]));
		this.Query["eventdate"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["eventdate"]));
		this.Query["notend"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["notend"]));
	}
}

public class Event:DbSession {
	public Event() {
	}

	public int GetPageCount(EventSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine("FROM				");
		oSqlBuilder.AppendLine("	VW_EVENT01 P	");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(EventSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sExecSql = string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	P.EVENT_NM				,	");
		oSqlBuilder.AppendLine("	P.PREFECTURE_CD			,	");
		oSqlBuilder.AppendLine("	P.PREFECTURE_NM			,	");
		oSqlBuilder.AppendLine("	P.EVENT_AREA_CD			,	");
		oSqlBuilder.AppendLine("	P.EVENT_AREA_NM			,	");
		oSqlBuilder.AppendLine("	P.EVENT_DETAIL			,	");
		oSqlBuilder.AppendLine("	P.EVENT_START_DATE		,	");
		oSqlBuilder.AppendLine("	P.EVENT_END_DATE			");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_EVENT01 P				");
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpression(pCondtion);
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		return oDataSet;
	}

	private OracleParameter[] CreateWhere(EventSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.EventCategorySeq)) {
			SysPrograms.SqlAppendWhere("P.EVENT_CATEGORY_SEQ = :EVENT_CATEGORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":EVENT_CATEGORY_SEQ",pCondition.EventCategorySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PrefectureCd)) {
			SysPrograms.SqlAppendWhere("P.PREFECTURE_CD = :PREFECTURE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PREFECTURE_CD",pCondition.PrefectureCd));
		}

		if (!string.IsNullOrEmpty(pCondition.EventAreaCd)) {
			SysPrograms.SqlAppendWhere("P.EVENT_AREA_CD = :EVENT_AREA_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":EVENT_AREA_CD",pCondition.EventAreaCd));
		}

		if (!string.IsNullOrEmpty(pCondition.EventDate)) {
			SysPrograms.SqlAppendWhere("P.EVENT_END_DATE >= :EVENT_END_DATE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":EVENT_END_DATE",OracleDbType.Date,DateTime.Parse(pCondition.EventDate),ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pCondition.NotEndFlag)) {
			SysPrograms.SqlAppendWhere("P.EVENT_END_DATE >= :TODAYS_DATE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TODAYS_DATE",OracleDbType.Date,DateTime.Now.ToString("yyyy/MM/dd"),ParameterDirection.Input));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpression(EventSeekCondition pCondtion) {
		string sSortExpression = " ORDER BY P.EVENT_START_DATE ASC,EVENT_SEQ ASC";

		return sSortExpression;
	}
}
