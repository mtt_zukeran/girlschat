﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ仲間(ﾒﾝﾊﾞｰ選択)
--	Progaram ID		: ManGameCharacterFellowMember
--
--  Creation Date	: 2011.09.30
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class ManGameCharacterFellowMemberSeekCondition:SeekConditionBase {

	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string FriendApplicationStatus;
	public string LastHugDay;

	public string Type {
		get {
			return this.Query["type"];
		}
		set {
			this.Query["type"] = value;
		}
	}
	
	public string ExceptStatusOfflineFlag;

	public ManGameCharacterFellowMemberSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManGameCharacterFellowMemberSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["type"]));
	}
}

public class ManGameCharacterFellowMember:DbSession {

	public int GetPageCount(ManGameCharacterFellowMemberSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT							  ");
		oSqlBuilder.AppendLine("	COUNT(*)					  ");
		oSqlBuilder.AppendLine("FROM							  ");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHR_FLW_MBR01 P");

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManGameCharacterFellowMemberSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause,ref sWhereClauseFriend));

		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	P.SITE_CD					,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("	P.AGE						,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL		,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE		,	");
		oSqlBuilder.AppendLine("	P.EXP						,	");
		oSqlBuilder.AppendLine("	P.PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("	P.PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("	P.PARTY_MEMBER_FLAG			,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID					,	");
		oSqlBuilder.AppendLine("	P.SITE_USE_STATUS			,	");
		oSqlBuilder.AppendLine("	P.SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("	P.MAX_ATTACK_FORCE_COUNT	,	");
		oSqlBuilder.AppendLine(" 	FP.RANK	AS FRIENDLY_RANK	,	");
		oSqlBuilder.AppendLine(" 	FP.FRIENDLY_POINT			,	");
		oSqlBuilder.AppendLine("	CASE												");
		oSqlBuilder.AppendLine("		WHEN P.CHARACTER_ONLINE_STATUS = 0				");
		oSqlBuilder.AppendLine("		THEN TSM.OFFLINE_GUIDANCE						");
		oSqlBuilder.AppendLine("		WHEN P.CHARACTER_ONLINE_STATUS = 1				");
		oSqlBuilder.AppendLine("		THEN TSM.LOGINED_GUIDANCE						");
		oSqlBuilder.AppendLine("		WHEN P.CHARACTER_ONLINE_STATUS = 2				");
		oSqlBuilder.AppendLine("		THEN TSM.WAITING_GUIDANCE						");
		oSqlBuilder.AppendLine("		WHEN P.CHARACTER_ONLINE_STATUS = 3				");
		oSqlBuilder.AppendLine("		THEN TSM.TALKING_WSHOT_GUIDANCE					");
		oSqlBuilder.AppendLine("	END AS MOBILE_ONLINE_STATUS_NM						");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	(SELECT												");
		oSqlBuilder.AppendLine("		P.SITE_CD									,	");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("		P.AGE										,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL					    ,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("		P.EXP										,	");
		oSqlBuilder.AppendLine("		P.USER_SEQ									,	");
		oSqlBuilder.AppendLine("		P.USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine("		P.PARTNER_USER_SEQ						    ,	");
		oSqlBuilder.AppendLine("		P.PARTNER_USER_CHAR_NO					    ,	");
		oSqlBuilder.AppendLine("		P.PARTY_MEMBER_FLAG						    ,	");
		oSqlBuilder.AppendLine("		P.LOGIN_ID								    ,	");
		oSqlBuilder.AppendLine("		P.SITE_USE_STATUS							,	");
		oSqlBuilder.AppendLine("		P.SMALL_PHOTO_IMG_PATH					   ,	");
		oSqlBuilder.AppendLine("		P.CHARACTER_ONLINE_STATUS					,	");
		oSqlBuilder.AppendLine("		AF.MAX_FORCE_COUNT AS MAX_ATTACK_FORCE_COUNT	");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_MAN_GAME_CHR_FLW_MBR01 P ,");
		oSqlBuilder.AppendLine("		T_ATTACK_FORCE AF				");
		SysPrograms.SqlAppendWhere(" P.SITE_CD = AF.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.PARTNER_USER_SEQ = AF.USER_SEQ",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.PARTNER_USER_CHAR_NO = AF.USER_CHAR_NO",ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" 	) P,							");
		oSqlBuilder.AppendLine(" 	(SELECT							");
		oSqlBuilder.AppendLine(" 		SITE_CD					,	");
		oSqlBuilder.AppendLine(" 		USER_SEQ				,	");
		oSqlBuilder.AppendLine(" 		USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine(" 		RANK					,	");
		oSqlBuilder.AppendLine(" 		FRIENDLY_POINT				");
		oSqlBuilder.AppendLine(" 	FROM							");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00			");
		oSqlBuilder.AppendLine(sWhereClauseFriend);
		oSqlBuilder.AppendLine(" 	) FP						,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT TSM			");
		oSqlBuilder.AppendLine(" WHERE								");
		oSqlBuilder.AppendLine(" 	P.SITE_CD				= FP.SITE_CD		(+) AND	");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_SEQ		= FP.USER_SEQ		(+) AND	");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_CHAR_NO	= FP.USER_CHAR_NO	(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD				= TSM.SITE_CD				");

		string sSortExpression = this.CreateOrderExpresion(pCondtion);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManGameCharacterFellowMemberSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("P.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.FriendApplicationStatus)) {
			SysPrograms.SqlAppendWhere("P.FRIEND_APPLICATION_STATUS = :FRIEND_APPLICATION_STATUS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FRIEND_APPLICATION_STATUS",pCondition.FriendApplicationStatus));
		}

		if (!string.IsNullOrEmpty(pCondition.Type)) {
			SysPrograms.SqlAppendWhere("P.GAME_CHARACTER_TYPE = :GAME_CHARACTER_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_TYPE",pCondition.Type));
		}

		if (!string.IsNullOrEmpty(pCondition.LastHugDay)) {
			SysPrograms.SqlAppendWhere("(P.LAST_HUG_DAY <> :LAST_HUG_DAY OR P.LAST_HUG_DAY IS NULL)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LAST_HUG_DAY",pCondition.LastHugDay));
		}
		
		if (!string.IsNullOrEmpty(pCondition.ExceptStatusOfflineFlag) && pCondition.ExceptStatusOfflineFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = P.PARTNER_USER_SEQ AND PARTNER_USER_CHAR_NO = P.PARTNER_USER_CHAR_NO AND LOGIN_VIEW_STATUS = :LOGIN_VIEW_STATUS)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS","1"));
		}

		SysPrograms.SqlAppendWhere("P.NA_FLAG IN (:NA_FLAG1,:NA_FLAG2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG1",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhere(ManGameCharacterFellowMemberSeekCondition pCondition,ref string pWhereClause,ref string pWhereClauseFriend) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref pWhereClause));

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :USER_SEQ",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClauseFriend);
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(ManGameCharacterFellowMemberSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.ManGameCharacterFellowMemberSort.FRIENDLY_RANK_ASC:
				sSortExpression = "ORDER BY FRIENDLY_RANK, FRIENDLY_POINT DESC";
				break;
			case PwViCommConst.ManGameCharacterFellowMemberSort.GAME_CHARACTER_LEVEL_DESC:
				sSortExpression = "ORDER BY GAME_CHARACTER_LEVEL DESC, EXP DESC";
				break;
			default:
				sSortExpression = "ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}

	public DataSet GetTeamMemberSeqData(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pFriendApplicationStatus,
		string pType,
		string pPartyMemberFlag
	) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ				,								");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO											");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHR_FLW_MBR01									");
		oSqlBuilder.AppendLine("WHERE																");
		oSqlBuilder.AppendLine("	SITE_CD						= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	USER_SEQ					= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO				= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	FRIEND_APPLICATION_STATUS	= :FRIEND_APPLICATION_STATUS	AND	");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE			= :GAME_CHARACTER_TYPE			AND	");
		oSqlBuilder.AppendLine("	PARTY_MEMBER_FLAG			= :PARTY_MEMBER_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":FRIEND_APPLICATION_STATUS",pFriendApplicationStatus));
		oParamList.Add(new OracleParameter(":GAME_CHARACTER_TYPE",pType));
		oParamList.Add(new OracleParameter(":PARTY_MEMBER_FLAG",pPartyMemberFlag));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
}

