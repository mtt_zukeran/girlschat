﻿using System;
using System.IO;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Collections.Specialized;
using ViComm;
using ViComm.Extension.Pwild;

public partial class PwParseWoman:PwParseViComm {
	private ParseWoman parseWoman;
	public PwParseWoman(ParseWoman pParseWoman) :base(pParseWoman){
		this.parseWoman = pParseWoman;
	}
	
	protected override GameCharacter getGameCharacter() {
		if (this.parseWoman.sessionWoman.userWoman.CurCharacter != null) {
			return this.parseWoman.sessionWoman.userWoman.CurCharacter.gameCharacter;
		} else {
			return null;
		}
	}
	
	internal override string Parse(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$SELF_USER_SEQ":
				sValue = this.parseWoman.sessionWoman.userWoman.userSeq;
				break;
			case "$SELF_USER_CHAR_NO":
				sValue = this.parseWoman.sessionWoman.userWoman.curCharNo;
				break;
			case "$SETUP_GAME_TUTORIAL_STATUS":
				SetupGameTutorialStatus(pTag,pArgument);
				break;
			case "$GET_GAME_HANDLE_NM":
				sValue = this.getGameHandleName(pTag,pArgument);
				sValue = sValue + "ｻﾝ";
				break;
			case "$SELF_CAST_GAME_POINT":
				sValue = this.getGameCharacter().castGamePoint.ToString();
				break;
			case "$GAME_PART_TIME_JOB_PLAN_PARTS":
				sValue = this.GetListPartTimeJobPlan(pTag,pArgument);
				break;
			case "$GET_GAME_ITEM_NM":
				sValue = this.GetGameItemNm(pTag,pArgument);
				break;
			case "$SELF_GAME_HAS_APPLICATION_COUNT":
				sValue = this.GetSelfHasApplicationCount(pTag,pArgument);
				break;
			case "$SELF_GAME_PARTNER_HUG_UNCHECKED_COUNT":
				sValue = this.GetSelfPartnerHugUncheckedCount(pTag,pArgument);
				break;
			case "$GAME_SALE_STATUS":
				sValue = this.GetGameItemSaleStatus(pTag,pArgument);
				break;
			case "$GAME_TOTAL_FRIENDLY_POINT":
				sValue = this.GetTotalFriendlyPoint(pTag,pArgument);
				break;
			case "$GAME_FELLOW_LOG_PARTS":
				sValue = this.GetFellowLog(pTag,pArgument);
				break;
			case "$GAME_FRIENDLY_POINT":
				sValue = this.GetFriendlyPoint(pTag,pArgument);
				break;
			case "$GET_TOTAL_POSSESSION_CAST_TREASURE_COUNT":
				sValue = this.GetTotalPossessionCastTreasureCount(pTag,pArgument);
				break;
			case "$SELF_GAME_PARTNER_PRESENT_UNCHECKED_COUNT":
				sValue = this.GetSelfPartnerPresentUncheckedCount(pTag,pArgument);
				break;
			case "$SELF_GAME_TODAY_PART_TIME_JOB_COUNT":
				sValue = this.GetTodayPartTimeJobCount(pTag,pArgument);
				break;
			case "$GAME_PART_TIME_JOB_LIMIT_COUNT":
				sValue = this.GetPartTimeJobLimitCount(pTag,pArgument);
				break;
			case "$GET_STAGE_GROUP_NM":
				sValue = this.GetStageGroupNm(pTag,pArgument);
				break;
			case "$GET_STAGE_GROUP_POSSESSION_CAST_TREASURE":
				sValue = this.GetStageGroupPossessionCastTreasure(pTag,pArgument);
				break;
			case "$GET_REMAINDER_POSSESSION_CAST_TREASURE_COUNT":
				sValue = this.GetRemainderPossessionCastTreasureCount(pTag,pArgument);
				break;
			case "$GET_CAST_GAME_SERVICE_POINT":
				sValue = GetCastGameServicePoint();
				break;
			case "$GET_STAGE_GROUP_TYPE":
				sValue = this.GetStageGroupType(pTag,pArgument);
				break;
			case "$SELF_GAME_NOW_HUG_COUNT":
				sValue = GetNowHugCount();
				break;
			case "$SELF_GAME_STAGE_HONOR_BEFORE":
				sValue = GetHonor();
				break;
			case "$GET_GAME_STAGE_CLEAR_FLAG":
				sValue = GetStageClearFlag();
				break;
			case "$GAME_SWEET_HEART_COUNT":
				sValue = this.GetWomanSweetHeartCount(pTag,pArgument);
				break;
			case "$GET_GAME_NAFLAG":
				sValue = GetNaFlag(pTag,pArgument);
				break;
			case "$GET_SELF_GAME_NAFLAG":
				sValue = GetSelfNaFlag();
				break;
			case "$DIV_IS_PARTY_ATTACK_COUNT_OVER": {
					this.parseWoman.SetNoneDisplay(!this.CheckPartyAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$DIV_IS_ATTACK_COUNT_OVER": {
					this.parseWoman.SetNoneDisplay(!this.CheckAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$DIV_IS_NOT_PARTY_ATTACK_COUNT_OVER": {
					this.parseWoman.SetNoneDisplay(this.CheckPartyAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$GAME_LOTTERY_NOT_COMP_FREE_FLAG":
				sValue = GetGameLotteryNotCompFreeFlag(pTag,pArgument);
				break;
			case "$SELF_GAME_INTRO_LOG_COUNT":
				sValue = this.GetSelfGameIntroLogCount(pTag,pArgument);
				break;
			case "$SELF_SITE_USE_STATUS":
				if (this.parseWoman.sessionWoman.userWoman.CurCharacter != null) {
					sValue = this.parseWoman.sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus;
				}
				break;
			case "$SELF_NOT_READ_LIKED_FLAG":
				if (this.parseWoman.sessionWoman.userWoman.CurCharacter != null) {
					sValue = iBridUtil.GetStringValue(this.parseWoman.sessionWoman.userWoman.CurCharacter.characterEx.notReadLikedFlag);
				}
				break;
			case "$SELF_GAME_FELLOW_COUNT_ALL_STATUS":
				sValue = this.GetGameFellowCountAllStatus(pTag,pArgument);
				break;
			case "$SELF_GAME_FAVORITE_COUNT":
				sValue = this.GetSelfFavoriteCount(pTag,pArgument);
				break;
			case "$SELF_ONCE_DAY_PRESENT_DONE_FLAG":
				sValue = this.GetSelfOnceDayPresentDoneFlag(pTag,pArgument);
				break;
			case "$GET_GAME_CHARACTER_TYPE":
				sValue = this.getGameCharacterType(pTag,pArgument);
				break;
			case "$DIV_IS_SELF_POSSESSION_GAME_ITEM_EXIST":
				this.parseWoman.SetNoneDisplay(!this.CheckExistPossessionGameItem(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_SELF_POSSESSION_GAME_ITEM_EXIST":
				this.parseWoman.SetNoneDisplay(this.CheckExistPossessionGameItem(pTag,pArgument));
				break;
			case "$DIV_IS_SELF_GAME_FAVORITE_EXIST":
				this.parseWoman.SetNoneDisplay(!this.CheckExistGameFavorite(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_SELF_GAME_FAVORITE_EXIST":
				this.parseWoman.SetNoneDisplay(this.CheckExistGameFavorite(pTag,pArgument));
				break;
			case "$GAME_FORCE_RECOVERY_COOP_POINT":
				sValue = this.GetForceRecoveryCoopPoint(pTag,pArgument);
				break;
			case "$SELF_GAME_SUPPORTED_REQUEST_COUNT":
				sValue = this.GetSelfRequestedSupportCount(pTag,pArgument);
				break;
			case "$SELF_GAME_APPLICATION_COUNT":
				sValue = this.GetSelfApplicationCount(pTag,pArgument);
				break;
			case "$DIV_IS_GAME_MAN_SWEET_HEART_EXIST":
				this.parseWoman.SetNoneDisplay(!this.CheckExistManSweetHeart(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_GAME_MAN_SWEET_HEART_EXIST":
				this.parseWoman.SetNoneDisplay(this.CheckExistManSweetHeart(pTag,pArgument));
				break;
			case "$GAME_TOTAL_PAYMENT_MONTH":
				sValue = this.GetGameTotalPaymentMonth(pTag,pArgument);
				break;
			case "$GAME_TOTAL_MOVIE_COUNT":
				sValue = this.GetGameCastMovieCount(pTag,pArgument);
				break;
			case "$GAME_DATE_LIMIT_COUNT":
				sValue = this.GetDateLimitCount(pTag,pArgument);
				break;
			case "$SELF_YESTERDAY_PAY_AMT_RANK":
				sValue = this.GetSelfYesterdayPayAmtRank();
				break;
			case "$DIV_IS_GAME_CAST_BONUS_TERM":
				this.parseWoman.SetNoneDisplay(!this.CheckGameCastBonusTerm(pTag,pArgument));
				break;
			case "$DIV_IS_SELF_TALKING": {
				int iCharacterOnlineStatus;
				using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
					iCharacterOnlineStatus = oCastGameCharacter.GetOnlineStatusGame(parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
				}
				this.parseWoman.SetNoneDisplay(!(iCharacterOnlineStatus == ViCommConst.USER_TALKING));
				break;
			}
			case "$DIV_IS_NOT_SELF_TALKING": {
				int iCharacterOnlineStatus;
				using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
					iCharacterOnlineStatus = oCastGameCharacter.GetOnlineStatusGame(parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
				}
				this.parseWoman.SetNoneDisplay(iCharacterOnlineStatus == ViCommConst.USER_TALKING);
				break;
			}
			case "$DIV_IS_VIEW_STATUS_OFFLINE" : {
				int iCharacterOnlineStatus = this.GetViewOnlineStatus(pTag,pArgument);
				this.parseWoman.SetNoneDisplay(!(iCharacterOnlineStatus == ViCommConst.USER_OFFLINE));
				break;
			}
			case "$DIV_IS_NOT_VIEW_STATUS_OFFLINE": {
				int iCharacterOnlineStatus = this.GetViewOnlineStatus(pTag,pArgument);
				this.parseWoman.SetNoneDisplay(iCharacterOnlineStatus == ViCommConst.USER_OFFLINE);
				break;
			}
			case "$DIV_IS_VIEW_STATUS_OFFLINE_EXIST": {
				int iRecCount = 0;
				using(ManGameCharacterFavorite oManGameCharacterFavorite = new ManGameCharacterFavorite()) {
					iRecCount = oManGameCharacterFavorite.GetFavoritViewStatusOfflineCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
				}
				this.parseWoman.SetNoneDisplay(!(iRecCount > 0));
				break;
			}
			case "$DIV_IS_NOT_VIEW_STATUS_OFFLINE_EXIST": {
				int iRecCount = 0;
				using (ManGameCharacterFavorite oManGameCharacterFavorite = new ManGameCharacterFavorite()) {
					iRecCount = oManGameCharacterFavorite.GetFavoritViewStatusOfflineCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
				}
				this.parseWoman.SetNoneDisplay(iRecCount > 0);
				break;
			}
			case "$SELF_FREE_PRESENT_REST_COUNT": {
				sValue = GetSelfFreePresentRestCount(pTag,pArgument);
				break;
			}
			case "$GET_GAME_INFORMATION_COUNT":
				sValue = this.GetGameInformationCount(pTag,pArgument);
				break;
			case "$SELF_GAME_LOGIN_BONUS_TODAY_GET_FLAG":
				sValue = this.GetGameLoginBonusGetFlag(pTag,pArgument);
				break;
			case "$GET_PHOTO_FLASH_VARS":
				sValue = this.GetPhotoFlashVars(pTag,pArgument);
				break;
			case "$GET_PHOTO_SCRIPT":
				sValue = this.GetPhotoScript(pTag,pArgument);
				break;
			case "$GET_PHOTO_SCRIPT_SP":
				sValue = this.GetPhotoScriptSp(pTag,pArgument);
				break;
			case "$SELF_NG_COUNT":
				if (this.parseWoman.sessionWoman.IsImpersonated) {
					sValue = parseWoman.sessionWoman.originalManObj.userMan.ngCount.ToString();
				} else {
					sValue = "0";
				}
				break;
			case "$SELF_NG_SKULL_COUNT":
				if (this.parseWoman.sessionWoman.IsImpersonated) {
					sValue = parseWoman.sessionWoman.originalManObj.userMan.ngSkullCount.ToString();
				} else {
					sValue = "0";
				}
				break;
			case "$USER_MAIL_PARTS":
				sValue = this.GetUserMail(pTag,pArgument);
				break;
			case "$GET_TOTAL_TX_RX_MAIL_COUNT":
				sValue = this.GetTotalTxRxMailCount(pTag,pArgument);
				break;
			//お気に入りグループ
			case "$SELF_FAVORIT_GROUP_COUNT":
				sValue = this.GetSelfFavoritGroupCount();
				break;
			case "$GET_FAVORIT_GROUP_NM":
				sValue = this.GetFavoritGroupNm(pTag,pArgument);
				break;
			//ファンクラブ
			case "$SELF_FANCLUB_MEMBER_COUNT":
				sValue = this.GetSelfFanClubMemberCount();
				break;
			case "$MAN_FANCLUB_STATUS_PARTS":
				sValue = this.GetManFanClubStatus(pTag,pArgument);
				break;
			//男性つぶやき
			case "$SELF_MAN_TWEET_READ_COUNT":
				sValue = this.GetSelfManTweetReadCount(pTag,pArgument);
				break;
			case "$DIV_IS_SELF_MAN_TWEET_READ":
				this.parseWoman.SetNoneDisplay(!this.IsSelfManTweetRead(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_SELF_MAN_TWEET_READ":
				this.parseWoman.SetNoneDisplay(this.IsSelfManTweetRead(pTag,pArgument));
				break;
			//野球拳
			case "$SELF_COMPLETE_YAKYUKEN_GAME_COUNT":
				sValue = this.GetSelfCompleteYakyukenGameCount(pTag,pArgument);
				break;
			case "$SELF_YAKYUKEN_COMMENT_COUNT":
				sValue = this.GetSelfYakyukenCommentCount(pTag,pArgument);
				break;
			case "$YAKYUKEN_PIC_UPLOAD_ADDR":
				sValue = this.GetYakyukenPicUploadAddr(pTag,pArgument);
				break;
			case "$SELF_DEVICE_TOKEN":
				sValue = this.parseWoman.sessionWoman.deviceToken;
				break;
			case "$SELF_GAME_IMG_PATH":
				sValue = this.GetSelfGameImgPath(pTag,pArgument);
				break;
			case "$DIV_IS_MAIL_LOTTERY_OPEN":
				this.parseWoman.SetNoneDisplay(!this.GetMailLotteryOpening(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_MAIL_LOTTERY_OPEN":
				this.parseWoman.SetNoneDisplay(this.GetMailLotteryOpening(pTag,pArgument));
				break;
			case "$DIV_IS_CAN_GET_MAIL_LOTTERY":
				this.parseWoman.SetNoneDisplay(!this.CheckCanGetMailLottery(pTag,pArgument));
				break;

			case "$GCAPP_AUTH_LINK":
				sValue = GetGcappAuthLink(pTag,pArgument);
				break;
			case "$SELF_GCAPP_VERSION":
				sValue = GetSelfGcappVersion(pTag,pArgument);
				break;
			case "$DIV_IS_SELF_GCAPP_NEED_UPDATE":
				this.parseWoman.SetNoneDisplay(!this.IsSelfGcappNeedUpdate(pTag,pArgument));
				break;
			case "$DIV_IS_EXIST_JEALOUSY_OFFLINE":
				this.parseWoman.SetNoneDisplay(!this.IsExistJealousyOffline(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_EXIST_JEALOUSY_OFFLINE":
				this.parseWoman.SetNoneDisplay(this.IsExistJealousyOffline(pTag,pArgument));
				break;
			// お友達紹介
			case "$SELF_MAN_INTRO_COUNT":
				sValue = this.GetManIntroducerFriendCount(pTag,pArgument);
				break;
			case "$SELF_MAN_INTRO_RECEIPT_COUNT":
				sValue = this.GetManIntroducerFriendReceiptUserCount(pTag,pArgument);
				break;
			// 返信率100%の女の子
			case "$DIV_IS_SELF_QUICK_RESPONSE_ENTRY":
				this.parseWoman.SetNoneDisplay(!this.IsQuickResponseEntry(pTag,pArgument));
				break;
			// 新着いいね通知
			case "$GET_CAST_LIKE_NOTICE_UNCONFIRMED_COUNT":
				sValue = this.GetCastLikeNoticeUnconfirmedCount(pTag,pArgument);
				break;
			case "$GET_CAST_LIKE_NOTICE_UNCONFIRMED_COUNT_PIC":
				sValue = this.GetCastLikeNoticeUnconfirmedCountPic(pTag,pArgument);
				break;
			case "$GET_CAST_LIKE_NOTICE_UNCONFIRMED_COUNT_MOVIE":
				sValue = this.GetCastLikeNoticeUnconfirmedCountMovie(pTag,pArgument);
				break;
			case "$GET_CAST_LIKE_NOTICE_UNCONFIRMED_COUNT_PIC_MOV":
				sValue = this.GetCastLikeNoticeUnconfirmedCountPicMovie(pTag,pArgument);
				break;

			#region ---------- セッション開始後利用可能・システム関連 ----------
			case "$ADD_PRESENT_GAME_ITEM_STAFF_EXT":
				this.ExecutePresentGameItemStaffExt(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo,pArgument);
				break;
			case "$DIV_IS_ADDABLE_PRESENT_GAME_ITEM_STAFF_EXT":
				this.parseWoman.SetNoneDisplay(!this.CheckPresentGameItemStaffExt(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo,pArgument).Equals(PwViCommConst.PresentGameItemStaffExt.RESULT_OK));
				break;
			case "$DIV_IS_NOT_ADDABLE_PRESENT_GAME_ITEM_STAFF_EXT":
				this.parseWoman.SetNoneDisplay(this.CheckPresentGameItemStaffExt(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo,pArgument).Equals(PwViCommConst.PresentGameItemStaffExt.RESULT_OK));
				break;
			case "$SELF_GAME_FELLOW_COUNT_EX":
				sValue = GetSelfGameFellowCountEx(pTag,pArgument);
				break;
			case "$GAME_NOW_PUBLISH_QUEST_SEQ":
				sValue = this.GetNowPublishQuestSeq(pTag,pArgument);
				break;
			case "$DIV_IS_NOW_PUBLISHED_QUEST":
				this.parseWoman.SetNoneDisplay(!this.CheckQuestPublish(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_NOW_PUBLISHED_QUEST":
				this.parseWoman.SetNoneDisplay(this.CheckQuestPublish(pTag,pArgument));
				break;
			#endregion
			#region □■□■□ PARTS定義 □■□■□
			case "$CAST_TREASURE_PARTS":
				sValue = this.GetCastTreasureData(pTag,pArgument);
				break;
			case "$PARTY_BATTLE_MEMBER_PARTS":
				sValue = this.GetListPartyBattleMember(pTag,pArgument);
				break;
			case "$USE_RECOVERY_ITEM_LINK_PARTS":
				sValue = this.GetRecoveryItemData(pTag,pArgument);
				break;
			case "$GAME_FELLOW_DATA_PARTS":
				sValue = this.GetGameFellowData(pTag,pArgument);
				break;
			case "$MAN_GAME_CHARACTER_FELLOW_NO_HUG_PARTS":
				sValue = this.GetListGameCharacterFellowNoHug(pTag,pArgument);
				break;
			case "$MAN_GAME_CHARACTER_OUTER_PARTS":
				sValue = this.GetManGameCharacterOuter(pTag,pArgument);
				break;
			case "$GAME_ITEM_PARTS":
				sValue = this.GetGameItemDataList(pTag,pArgument);
				break;
			case "$POSSESSION_GAME_ITEM_PARTS":
				sValue = this.GetListPossessionGameItem(pTag,pArgument);
				break;
			case "$MAN_TREASURE_PARTS":
				sValue = this.GetManTreasureData(pTag,pArgument);
				break;
			case "$WOMAN_GAME_PAYMENT_RANK_PARTS":
				sValue = this.GetPaymentRank(pTag,pArgument);
				break;
			case "$WOMAN_GAME_PAYMENT_RANK_LIST_PARTS":
				sValue = this.GetPaymentRankList(pTag,pArgument);
				break;
			case "$GAME_MOSAIC_RANK_PARTS":
				sValue = this.GetMosaicRankData(pTag,pArgument);
				break;
			case "$GAME_SELF_MOVIE_PARTS":
				sValue = this.GetSelfMovie(pTag,pArgument);
				break;
			case "$GAME_FAVORITE_LOG_PARTS":
				sValue = this.GetFavoriteLog(pTag,pArgument);
				break;
			case "$GAME_MAN_CHARACTER_NEW_PARTS":
				sValue = this.GetManGameCharacterNew(pTag,pArgument);
				break;
			case "$GAME_PARTNER_HUG_PARTS":
				sValue = this.GetGamePartnerHug(pTag,pArgument);
				break;
			case "$GET_CAST_TREASURE_LOG_PARTS":
				sValue = this.GetGetCastTreasureLog(pTag,pArgument);
				break;
			case "$GAME_BATTLE_LOG_PARTS":
				sValue = this.GetBattleLog(pTag,pArgument);
				break;
			case "$GAME_COMP_CAST_TREASURE_GET_ITEM_LIST_PARTS":
				sValue = this.GetCompCastTreasureGetItem(pTag,pArgument);
				break;
			case "$NEXT_STAGE_PARTS":
				sValue = this.GetNextStage(pTag,pArgument);
				break;
			case "$GAME_OBJ_USED_HISTORY_PARTS":
				sValue = this.GetGameObjUsedHistoryData(pTag,pArgument);
				break;
			case "$GAME_SOON_COMP_CAST_TREASURE_PARTS":
				sValue = this.GetSoonCompCastTreasure(pTag,pArgument);
				break;
			case "$STAGE_CLEAR_POINT_PARTS":
				sValue = this.GetStageClearPoint(pTag,pArgument);
				break;
			case "$DIV_IS_NOT_ATTACK_COUNT_OVER": {
					this.parseWoman.SetNoneDisplay(this.CheckAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$GET_CLEAR_TOWN_SEQ_DATA_PARTS":
				sValue = this.GetClearTownSeqData(pTag,pArgument);
				break;
			case "$MAN_GAME_CHARACTER_PARTS":
				sValue = this.GetListManGameCharacter(pTag,pArgument);
				break;
			case "$SELF_BBS_OBJ_PARTS":
				sValue = this.GetListSelfBbsObj(pTag,pArgument);
				break;
			case "$OBJ_REVIEW_TOTAL_PARTS":
				sValue = this.GetObjReviewTotal(pTag,pArgument);
				break;
			case "$GAME_ITEM_BY_CATEGORY_PARTS":
				sValue = this.GetItemDataByCategory(pTag,pArgument);
				break;
			case "$GAME_TREASURE_COMP_ITEM_PARTS":
				sValue = this.GetGameTreasureCompItem(pTag,pArgument);
				break;
			case "$GAME_BATTLE_COMP_ITEM_PARTS":
				sValue = this.GetGameBattleCompItem(pTag,pArgument);
				break;
			case "$GAME_INTRO_ITEM_LIST_PARTS":
				sValue = this.GetGameIntroItemByIntroCount(pTag,pArgument);
				break;
			case "$GAME_NEW_HUG_PARTS":
				sValue = this.GetOneNewHug(pTag,pArgument);
				break;
			case "$GAME_PARTNER_PRESENT_GET_ONE_UNCHECKED_PARTS":
				sValue = this.GetOneSelfPartnerPresentUnchecked(pTag,pArgument);
				break;
			case "$GAME_GET_ONE_FELLOW_APPLICATION_PARTS":
				sValue = this.GetOneFellowApplication(pTag,pArgument);
				break;
			case "$GAME_GET_ONE_SUPPORT_REQUEST_PARTS":
				sValue = this.GetOneSupportRequest(pTag,pArgument);
				break;
			case "$GAME_INFORMATION_PARTS":
				sValue = this.GetInformation(pTag,pArgument);
				break;
			case "$REQUEST_PARTS":
				sValue = this.GetListRequest(pTag,pArgument);
				break;
			case "$GAME_QUEST_REWARD_LIST_PARTS":
				sValue = this.GetQuestRewardList(pTag,pArgument);
				break;
			case "$GAME_QUEST_INFORMATION_PARTS":
				sValue = this.GetQuestInformation(pTag,pArgument);
				break;
			case "$GAME_QUEST_CLEAR_INFO_PARTS":
				sValue = this.GetQuestClearInfo(pTag,pArgument);
				break;
			case "$GAME_QUEST_ENTRY_QUEST_INFO":
				sValue = this.GetEntryQuestInfo(pTag,pArgument);
				break;
			case "$SELF_CAST_TREASURE_PARTS":
				sValue = this.GetCastTreasureCompStatus(pTag,pArgument);
				break;
			case "$GAME_TOWN_PARTS":
				sValue = this.GetListTown(pTag,pArgument);
				break;
			case "$GAME_SWEET_HEART_PARTS":
				sValue = this.GetSweetHeartData(pTag,pArgument);
				break;
			case "$GAME_PAYMENT_LOG_PARTS":
				sValue = this.GetGamePaymentLog(pTag,pArgument);
				break;
			case "$SELF_GAME_TREASURE_DATA_PARTS":
				sValue = this.GetSelfGameTreasureData(pTag,pArgument);
				break;
			case "$SELF_GAME_GET_PAYMENT_NEWS_PARTS":
				sValue = this.GetGameGetPaymentNews(pTag,pArgument);
				break;
			case "$SELF_GAME_GET_PAYMENT_NEWS_COUNT":
				sValue = this.GetSelfGameGetPaymentNewsCount(pTag,pArgument);
				break;
			case "$CAST_PIC_ATTR_TYPE_VALUE_PARTS":
				sValue = this.GetCastPicAttrTypeValueList(pTag,pArgument);
				break;
			case "$CAST_MOVIE_ATTR_TYPE_VALUE_PARTS":
				sValue = this.GetCastMovieAttrTypeValueList(pTag,pArgument);
				break;
			case "$INFO_MAIL_PARTS" :
				sValue = this.GetInfoMail(pTag,pArgument);
				break;
			case "$CAST_TWEET_PARTS":
				sValue = this.GetCastTweet(pTag,pArgument);
				break;
			case "$MAN_TWEET_PARTS":
				sValue = this.GetManTweet(pTag,pArgument);
				break;
			case "$MAN_DATA_BY_USER_SEQ_PARTS":
				sValue = this.GetManDataByUserSeq(pTag,pArgument);
				break;
			case "$MAN_DATA_BY_LOGINID_PARTS":
				sValue = this.GetManDataByLoginId(pTag,pArgument);
				break;
			case "$MAN_TWEET_COMMENT_CNT_PARTS":
				sValue = this.GetManTweetCommentCnt(pTag,pArgument);
				break;
			case "$CAST_NEW_FACE_PARTS":
				sValue = this.GetCastNewFace(pTag,pArgument);
				break;
			case "$SELF_YAKYUKEN_JYANKEN_PIC_PARTS":
				sValue = this.GetSelfYakyukenJyankenPic(pTag,pArgument);
				break;
			case "$CAST_PIC_PARTS":
				sValue = this.GetListCastPic(pTag,pArgument);
				break;
			case "$SELF_MOVIE_PARTS":
				sValue = this.GetListSelfMovie(pTag,pArgument);
				break;
			case "$MAN_NEWS_PARTS":
				sValue = this.GetListManNews(pTag,pArgument);
				break;
			case "$SELF_GAME_CAST_NEWS_PARTS":
				sValue = this.GetSelfGameCastNews(pTag,pArgument);
				break;
			case "$SELF_ELECTION_RANK_PARTS":
				sValue = this.GetOneSelfRankElection(pTag,pArgument);
				break;
			case "$SELF_WAIT_SCHEDULE_PARTS":
				sValue = this.GetSelfWaitSchedule(pTag,pArgument);
				break;
			case "$SELF_RESERVED_MAIL_REQUEST_COUNT_PARTS":
				sValue = this.GetReservedMailRequestCount(pTag,pArgument);
				break;
			case "$SELF_LOGIN_PLAN_PARTS":
				sValue = this.GetSelfLoginPlan(pTag,pArgument);
				break;
			case "$MAIL_LOTTERY_RATE_PARTS":
				sValue = this.GetMailLotteryRate(pTag,pArgument);
				break;
			case "$GET_NOT_CHECK_USER_INTRO_COUNT_PARTS":
				sValue = this.GetNotCheckUserIntroCount(pTag,pArgument);
				break;
			#endregion

			default:
				switch (this.parseWoman.currentTableIdx) {
					case PwViCommConst.DATASET_USER_TOP:
						sValue = this.ParseUserTop(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_ITEM:
						sValue = this.ParseGameItem(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_PART_TIME_JOB_PLAN:
						sValue = this.ParsePartTimeJobPlan(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_PARTNER_GAME_CHARACTER:
						sValue = this.ParseGameCharacter(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_TREASURE:
						sValue = this.ParseTreasure(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_STAGE:
						sValue = this.ParseStage(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_TOWN:
						sValue = this.ParseTown(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_INTRO_LOG:
						sValue = this.ParseGameIntroLog(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_OBJ_REVIEW:
						sValue = this.ParseObjReview(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_REQUEST:
						sValue = this.ParseRequest(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_REQUEST_VALUE:
						sValue = this.ParseRequestValue(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_QUEST:
						sValue = this.ParseQuest(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_FAVORIT_GROUP:
						sValue = this.ParseFavoritGroup(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_DATE_PLAN:
						sValue = this.ParseDatePlan(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_LOTTERY:
						sValue = this.ParseLottery(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_QUEST_ENTRY_LOG:
						sValue = this.ParseQuestEntryLog(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_BLOG_COMMENT:
						sValue = this.ParseBlogComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CAST_PIC_ATTR_TYPE:
						sValue = this.ParseCastPicAttrType(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CAST_MOVIE_ATTR_TYPE:
						sValue = this.ParseCastMovieAttrType(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CAST_TWEET:
						sValue = this.ParseCastTweet(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CAST_TWEET_COMMENT:
						sValue = this.ParseCastTweetComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAN_TWEET:
						sValue = this.ParseManTweet(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAN_TWEET_COMMENT:
						sValue = this.ParseManTweetComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAN_TWEET_COMMENT_TERM:
						sValue = this.ParseManTweetCommentTerm(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAN_TWEET_COMMENT_CNT:
						sValue = this.ParseManTweetCommentCnt(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_COMMUNICATION:
						sValue = this.ParseCommunication(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_YAKYUKEN_GAME:
						sValue = this.ParseYakyukenGame(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_YAKYUKEN_COMMENT:
						sValue = this.ParseYakyukenComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_YAKYUKEN_PIC:
						sValue = this.ParseYakyukenPic(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_YAKYUKEN_JYANKEN_PIC:
						sValue = this.ParseYakyukenJyankenPic(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_PICKUP_OBJS_COMMENT:
						sValue = this.ParsePickupObjsComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CAST_PIC:
						sValue = this.ParseCastPic(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_NEWS:
						sValue = this.ParseGameCastNews(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_ELECTION_PERIOD:
						sValue = this.ParseElectionPeriod(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_WAIT_SCHEDULE:
						sValue = this.ParseWaitSchedule(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_LOGIN_PLAN:
						sValue = this.ParseLoginPlan(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAIL_LOTTERY:
						sValue = this.ParseMailLottery(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_DEFECT_REPORT:
						sValue = this.ParseDefectReport(pTag,pArgument,out pParsed);
						break;
					
					default:
						pParsed = false;
						break;
				}
				break;
		}

		if (!pParsed) {
			sValue = base.Parse(pTag,pArgument,out pParsed);
		}
		
		return sValue;
	}

	
	private void SetupGameTutorialStatus(string pTag,string pArgument) {
		SetupGameTutorialStatus(pTag,this.parseWoman.sessionWoman.site.siteCd,this.parseWoman.sessionWoman.userWoman.userSeq,this.parseWoman.sessionWoman.userWoman.curCharNo,pArgument);
	}

	public string getGameHandleName(string pTag,string pArgument) {
		string sValue = "";
		string[] arrText = pArgument.Split(',');

		if (arrText.Length > 1) {
			GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.PartnerUserSeq = arrText[0];
			oCondition.PartnerUserCharNo = arrText[1];
			oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);

			using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
				DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);
				sValue = ds.Tables[0].Rows[0]["GAME_HANDLE_NM"].ToString();
			}
		}

		return sValue;
	}

	private string GetGameItemNm(string pTag,string pArgument) {
		string sGameItemNm = string.Empty;
		if (!pArgument.Equals(string.Empty)) {
			sGameItemNm = GetGameItemNm(pTag,this.parseWoman.sessionWoman.site.siteCd,ViCommConst.OPERATOR,pArgument);
		}
		return sGameItemNm;
	}

	private string GetSelfHasApplicationCount(string pTag,string pArgument) {
		string sValue = GetSelfHasApplicationCount(
			pTag,
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo
		);
		return sValue;
	}

	private string GetSelfPartnerHugUncheckedCount(string pTag,string pArgument) {
		string sValue = GetSelfPartnerHugUncheckedCount(
			pTag,
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo,
			this.getGameCharacter().lastHugLogCheckDate
		);
		return sValue;
	}

	private string GetGameItemSaleStatus(string pTag,string pArgument) {
		string sValue = string.Empty;

		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sSexCd = ViCommConst.OPERATOR;

		DataSet ds = null;
		GameItemSale oGameItemSale = new GameItemSale();
		ds = oGameItemSale.GetCurrentSaleStatus(sSiteCd,sSexCd);

		DataRow dr = ds.Tables[0].Rows[0];

		sValue = dr["GAME_ITEM_SALE_STATE"].ToString();

		return sValue;
	}

	private string GetSelfPartnerPresentUncheckedCount(string pTag,string pArgument) {
		string sValue = GetSelfPartnerPresentUncheckedCount(
			pTag,
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo,
			0
		);
		return sValue;
	}

	private string GetTodayPartTimeJobCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		using (PartTimeJobPlan oPartTimeJobPlan = new PartTimeJobPlan()) {
			sValue = oPartTimeJobPlan.GetTodayPartTimeJobCount(
				this.parseWoman.sessionWoman.site.siteCd,
				this.parseWoman.sessionWoman.userWoman.userSeq,
				this.parseWoman.sessionWoman.userWoman.curCharNo
			);
		}
		return sValue;
	}

	private string GetPartTimeJobLimitCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["PART_TIME_JOB_LIMIT_COUNT"].ToString();
			}
		}
		return sValue;
	}

	private string GetCastGameServicePoint() {
		string sValue = string.Empty;
		int iValue = 0;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				iValue = int.Parse(ds.Tables[0].Rows[0]["CAST_GAME_SERVICE_POINT"].ToString());
				sValue = String.Format("{0:#,0}",iValue);
			}
		}
		return sValue;
	}

	private string GetNowHugCount() {
		string sValue;
		using (GameFellow oGameFellow = new GameFellow()) {
			sValue = oGameFellow.GetNowHugCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
		}
		return sValue;
	}

	private string GetHonor() {

		string sValue;
		int iStageLevel = 0;
		iStageLevel = int.Parse(this.getGameCharacter().stageLevel.ToString()) - 1;

		using (Stage oStage = new Stage()) {
			sValue = oStage.GetHonor(this.parseWoman.sessionWoman.site.siteCd,iStageLevel,ViCommConst.OPERATOR);
		}

		return sValue;

	}

	private string GetStageClearFlag() {

		string sValue;
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (Stage oStage = new Stage()) {
			sValue = oStage.GetStageClearFlag(sSiteCd,sUserSeq,sUserCharNo,this.getGameCharacter().stageSeq.ToString());
		}

		return sValue;

	}

	public string GetNaFlag(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string pPartnerUserSeq;
		string pPartnerUserCharNo;

		if (arrText.Length > 1) {
			pPartnerUserSeq = arrText[0];
			pPartnerUserCharNo = arrText[1];

			DataSet oDataSet;
			using (ManCharacter oManCharacter = new ManCharacter()) {
				oDataSet = oManCharacter.GetOne(this.parseWoman.sessionWoman.site.siteCd,pPartnerUserSeq,pPartnerUserCharNo);
			}

			if (oDataSet.Tables[0].Rows.Count > 0) {
				sValue = oDataSet.Tables[0].Rows[0]["NA_FLAG"].ToString();
			}
			return sValue;
		}

		return sValue;
	}

	public string GetSelfNaFlag() {
		string sValue = "";

		DataSet oDataSet;
		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			oDataSet = oCastGameCharacter.GetOne(this.parseWoman.sessionWoman.site.siteCd,this.parseWoman.sessionWoman.userWoman.userSeq,this.parseWoman.sessionWoman.userWoman.curCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["NA_FLAG"].ToString();
		}
		return sValue;
	}

	private string CheckPartyAttackCountOver(string pTag,string pArgument) {
		string sPartyAttackCountOverFlag = string.Empty;
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (DailyPartyBattleLog oDailyPartyBattleLog = new DailyPartyBattleLog()) {
			sPartyAttackCountOverFlag = oDailyPartyBattleLog.CheckPartyAttackCountOver(sSiteCd,sUserSeq,sUserCharNo);
		}

		return sPartyAttackCountOverFlag;
	}

	private string CheckAttackCountOver(string pTag,string pArgument) {
		string sAttackCountOverFlag = string.Empty;
		if (string.IsNullOrEmpty(pArgument)) {
			return sAttackCountOverFlag;
		}

		string[] textArr = pArgument.Split(',');
		if (textArr.Length < 2) {
			return sAttackCountOverFlag;
		}

		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		string sPartnerUserSeq = textArr[0];
		string sPartnerUserCharNo = textArr[1];

		using (DailyBattleLog oDailyBattleLog = new DailyBattleLog()) {
			sAttackCountOverFlag = oDailyBattleLog.CheckAttackCountOver(sSiteCd,sUserSeq,sUserCharNo,sPartnerUserSeq,sPartnerUserCharNo);
		}

		return sAttackCountOverFlag;
	}

	private string GetSelfGameIntroLogCount(string pTag,string pArgument) {
		string sValue = GetSelfGameIntroLogCount(
			pTag,
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo,
			pArgument
		);
		return sValue;
	}

	private string GetSelfFavoriteCount(string pTag,string pArgument) {
		return GetSelfFavoriteCount(
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo
		);
	}

	public string getGameCharacterType(string pTag,string pArgument) {
		string sValue = "";
		string[] arrText = pArgument.Split(',');

		if (arrText.Length > 1) {
			GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.PartnerUserSeq = arrText[0];
			oCondition.PartnerUserCharNo = arrText[1];
			oCondition.SeekMode = ulong.Parse(this.parseWoman.sessionObjs.seekMode);

			using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
				DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);
				sValue = ds.Tables[0].Rows[0]["GAME_CHARACTER_TYPE"].ToString();
			}
		}

		return sValue;
	}

	private string GetListSelfBbsObj(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);
		string sUnAuth = iBridUtil.GetStringValue(oPartsArgs.Query["unauth"]);
		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();

		using (CastBbsObj oCastBbsObj = new CastBbsObj(parseWoman.sessionObjs)) {
			if (!sObjSeq.Equals(string.Empty)) {
				oTmpDataSet = oCastBbsObj.GetPageCollectionBySeq(sObjSeq,false,false);
			} else {
				oTmpDataSet = oCastBbsObj.GetPageCollection(
					this.parseWoman.sessionWoman.site.siteCd,
					this.parseWoman.sessionWoman.userWoman.loginId,
					this.parseWoman.sessionWoman.userWoman.curCharNo,
					sUnAuth,
					false,
					string.Empty,
					sAttrTypeSeq,
					sAttrSeq,
					false,
					false,
					oCondition,
					1,
					oPartsArgs.NeedCount,
					false
				);
			}
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_SELF_BBS_OBJ);

		return sValue;
	}

	private string GetObjReviewTotal(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);

		if (string.IsNullOrEmpty(sObjSeq)) {
			return string.Empty;
		}

		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			oTmpDataSet = oObjReviewHistory.GetObjReviewTotalForParts(
				this.parseWoman.sessionWoman.site.siteCd,
				sObjSeq
			);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_OBJ_REVIEW);

		return sValue;
	}

	private string GetForceRecoveryCoopPoint(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["FORCE_RECOVERY_COOP_POINT"].ToString();
			}
		}
		return sValue;
	}

	private string GetSelfRequestedSupportCount(string pTag,string pArgument) {
		string sValue = GetSelfRequestedSupportCount(
			pTag,
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo
		);
		return sValue;
	}

	private string GetSelfApplicationCount(string pTag,string pArgument) {
		string sValue = GetSelfApplicationCount(
			pTag,
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo
		);
		return sValue;
	}

	private string GetGameTotalPaymentMonth(string pTag,string pArgument) {
		string sValue = "0";

		GameCharacterPerformanceSeekCondition oCondition = new GameCharacterPerformanceSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.Period = PwViCommConst.GameRankPeriod.MONTH;

		using (GameCharacterPerformance oGameCharacterPerformance = new GameCharacterPerformance()) {
			sValue = oGameCharacterPerformance.GetGamePayment(oCondition);
		}

		return sValue;
	}
	
	private string GetGameCastMovieCount(string pTag,string pArgument) {
		string sValue = "0";

		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		using (CastGameCharacterMovie oCastGameCharacterMovie = new CastGameCharacterMovie()) {
			sValue = oCastGameCharacterMovie.GetGameCastMovieCount(sSiteCd,sUserSeq,sUserCharNo);
		}
		
		return sValue;
	}

	private string GetDateLimitCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["DATE_LIMIT_COUNT"].ToString();
			}
		}
		return sValue;
	}

	private string GetListRequest(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		PwRequestSeekCondition oCondition = new PwRequestSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.PublicStatus = PwViCommConst.RequestPublicStatus.PUBLIC;
		DataSet oDataSet;
		using (PwRequest oPwRequest = new PwRequest()) {
			oDataSet = oPwRequest.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}
		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_REQUEST);
	}

	private string GetSelfYesterdayPayAmtRank() {
		string sRank = string.Empty;

		using (YesterdayCastReport oYesterdayCastReport = new YesterdayCastReport()) {
			sRank = oYesterdayCastReport.GetAllPayAmtRank(
				this.parseWoman.sessionWoman.site.siteCd,
				this.parseWoman.sessionWoman.userWoman.userSeq,
				this.parseWoman.sessionWoman.userWoman.curCharNo
			);
		}

		return sRank;
	}

	protected string GetSelfGameFellowCountEx(string pTag,string pArgument) {
		string sValue = string.Empty;
		sValue = GetSelfGameFellowCountEx(
			pTag,
			this.parseWoman.sessionWoman.site.siteCd,
			this.parseWoman.sessionWoman.userWoman.userSeq,
			this.parseWoman.sessionWoman.userWoman.curCharNo,
			pArgument
		);
		return sValue;
	}

	private string GetSelfFavoritGroupCount() {
		decimal dRecCount = 0;

		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oFavoritGroup.GetPageCount(oCondition,1,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private string GetFavoritGroupNm(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		string[] oArgArr = pArgument.Split(',');

		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseWoman,oArgArr,0);
		string sPartnerUserSeq = iBridUtil.GetStringValue(oQuery["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(oQuery["partner_user_char_no"]);
		string sFavoritGroupSeq = iBridUtil.GetStringValue(oQuery["grpseq"]);

		if (!string.IsNullOrEmpty(sPartnerUserSeq) && !string.IsNullOrEmpty(sPartnerUserCharNo)) {
			using (Favorit oFavorit = new Favorit()) {
				oDataSet = oFavorit.GetOne(
					this.parseWoman.sessionWoman.site.siteCd,
					this.parseWoman.sessionWoman.userWoman.userSeq,
					this.parseWoman.sessionWoman.userWoman.curCharNo,
					sPartnerUserSeq,
					sPartnerUserCharNo
				);
			}

			if (oDataSet.Tables[0].Rows.Count != 0) {
				sValue = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["FAVORIT_GROUP_NM"]);

				if (string.IsNullOrEmpty(sValue)) {
					sValue = "ｸﾞﾙｰﾌﾟ未設定";
				}
			}
		} else if (!string.IsNullOrEmpty(sFavoritGroupSeq)) {
			FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
			oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
			oCondition.FavoritGroupSeq = sFavoritGroupSeq;

			using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
				oDataSet = oFavoritGroup.GetPageCollection(oCondition,1,1);

				if (oDataSet.Tables[0].Rows.Count != 0) {
					sValue = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["FAVORIT_GROUP_NM"]);
				}
			}
		}

		return sValue;
	}
	
	private int GetViewOnlineStatus(string pTag,string pArgument) {
		int iCharacterOnlineStatus;
		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			iCharacterOnlineStatus = oCastGameCharacter.GetOnlineStatusGame(parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
		}

		if (iCharacterOnlineStatus == ViCommConst.USER_OFFLINE) {
			return iCharacterOnlineStatus;
		}
		
		string[] sArr = pArgument.Split(',');
		string pManUserSeq = string.Empty;
		string pManUserCharNo = string.Empty;
		if(sArr.Length > 1) {
			pManUserSeq = sArr[0];
			pManUserCharNo = sArr[1];
		}
		
		int iIsSeenOnlineStatus;
		using (Favorit oFavorit = new Favorit()) {
			iIsSeenOnlineStatus = oFavorit.GetIsSeenOnlineStatus(
										parseWoman.sessionWoman.site.siteCd,
										parseWoman.sessionWoman.userWoman.userSeq,
										parseWoman.sessionWoman.userWoman.curCharNo,
										pManUserSeq,
										pManUserCharNo,
										iCharacterOnlineStatus);
		}
		
		return iIsSeenOnlineStatus;
	}
	
	private string GetGameInformationCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		string sLastHugLogCheckDate = this.parseWoman.sessionWoman.userWoman.CurCharacter.gameCharacter.lastHugLogCheckDate;
		
		using (GameInformation oGameInformation = new GameInformation()) {
			int iValue = oGameInformation.GetInformationCountForCast(sSiteCd,sUserSeq,sUserCharNo,sLastHugLogCheckDate);
			sValue = iValue.ToString();
		}
		
		return sValue;
	}
	
	private string GetGameLoginBonusGetFlag(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		string sSiteCd = this.parseWoman.sessionWoman.site.siteCd;
		string sUserSesq = this.parseWoman.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		
		using (GameLoginBonusDaily oGameLoginBonusDaily = new GameLoginBonusDaily()) {
			sValue = oGameLoginBonusDaily.GetGameLoginBonusGetFlag(sSiteCd,sUserSesq,sUserCharNo);
		}
		
		return sValue;
	}

	private string GetInfoMail(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		string sMailSearchKey = iBridUtil.GetStringValue(oPartsArgs.Query["search_key"]);
		string sMailReadFlag = iBridUtil.GetStringValue(oPartsArgs.Query["read_flag"]);
		int? iMailReadFlag = null;
		if (!sMailReadFlag.Equals("")) {
			iMailReadFlag = int.Parse(sMailReadFlag);
		}
		using (MailBox oMailBox = new MailBox()) {
			oDataSet = oMailBox.GetPageCollection(
				parseWoman.sessionWoman.site.siteCd,
				parseWoman.sessionWoman.userWoman.userSeq,
				parseWoman.sessionWoman.userWoman.curCharNo,
				"",
				"",
				parseWoman.sessionWoman.sexCd,
				ViCommConst.MAIL_BOX_INFO,
				ViCommConst.RX,
				true,
				sMailSearchKey,
				iMailReadFlag,
				null,
				false,
				null,
				null,
				null,
				1,
				oPartsArgs.NeedCount
			);
		}
		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
	}

	private string GetPhotoFlashVars(string pTag,string pArgument) {
		string sValue = string.Empty;
		NameValueCollection oQuery = new NameValueCollection(parseWoman.sessionWoman.requestQuery.QueryString);
		string sPicSeq = iBridUtil.GetStringValue(oQuery["pic_seq"]);
		string sNextUrl = iBridUtil.GetStringValue(oQuery["next_url"]);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		DataSet oDataSet = null;

		string sImgUrl = string.Empty;
		string sComment = string.Empty;

		using (CastPic oCastPic = new CastPic(parseWoman.sessionObjs)) {
			oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			if (!oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString().Equals(parseWoman.sessionWoman.userWoman.userSeq) || !oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString().Equals(parseWoman.sessionWoman.userWoman.curCharNo)) {
				sImgUrl = System.Web.HttpUtility.UrlEncode(Path.Combine("../",PwViCommConst.NoPicJpgPath),enc);
			} else {
				sImgUrl = System.Web.HttpUtility.UrlEncode(Path.Combine("../",iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"])),enc);
				sComment = System.Web.HttpUtility.UrlEncode(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]));
			}
		}

		sNextUrl = System.Web.HttpUtility.UrlEncode(sNextUrl,enc);

		sValue = string.Format("IMGURL={0}&COMMENT={1}&nextURL={2}",sImgUrl,sComment,sNextUrl);

		return sValue;
	}

	private string GetPhotoScript(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseWoman,oArgArr,0);
		string sPicSeq = iBridUtil.GetStringValue(oQuery["pic_seq"]);
		DataSet oDataSet = null;

		if (string.IsNullOrEmpty(sPicSeq)) {
			return sValue;
		}

		string sWebPhisicalDir = parseWoman.sessionWoman.site.webPhisicalDir;
		string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.FLASH_DIR);
		string sScriptPath = Path.Combine(sFlashDir,"get_photo_ip.js");
		string sObjPhotoImgPath = string.Empty;
		string sObjPicDoc = string.Empty;

		using (CastPic oCastPic = new CastPic(parseWoman.sessionObjs)) {
			oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			if (!oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString().Equals(parseWoman.sessionWoman.userWoman.userSeq) || !oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString().Equals(parseWoman.sessionWoman.userWoman.curCharNo)) {
				return sValue;
			} else {
				sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]));
				sObjPicDoc = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]);
			}
		}

		if (string.IsNullOrEmpty(sObjPhotoImgPath)) {
			return sValue;
		}

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			using (StreamReader srJs = new StreamReader(sScriptPath)) {
				sValue = srJs.ReadToEnd();
			}

			//お宝画像読み込み
			byte[] bufObjPhotoImg = FileHelper.GetFileBuffer(sObjPhotoImgPath);
			string sObjPhotoImgBase64 = Convert.ToBase64String(bufObjPhotoImg);

			sValue = sValue.Replace("%IMAGE%",sObjPhotoImgBase64);
			sValue = sValue.Replace("%COMMENT%",sObjPicDoc);
		}

		return sValue;
	}
	
	private string GetManDataByUserSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		string sUserSeq = iBridUtil.GetStringValue(oPartsArgs.Query["userseq"]);
		if (string.IsNullOrEmpty(sUserSeq)) {
			return string.Empty;
		}
		using (Man oMan = new Man(parseWoman.sessionObjs)) {
			oDataSet = oMan.GetOne(parseWoman.sessionWoman.site.siteCd,sUserSeq,1);
		}
		
		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAN);
	}

	private string GetSelfFanClubMemberCount() {
		decimal dRecCount = 0;

		FanClubStatusSeekCondition oCondition = new FanClubStatusSeekCondition();
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = this.parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = this.parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.EnableFlag = ViCommConst.FLAG_ON_STR;

		using (ManFanClubStatus oManFanClubStatus = new ManFanClubStatus(parseWoman.sessionObjs)) {
			oManFanClubStatus.GetPageCount(oCondition,1,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private string GetManFanClubStatus(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		FanClubStatusSeekCondition oCondition = new FanClubStatusSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (ManFanClubStatus oManFanClubStatus = new ManFanClubStatus(parseWoman.sessionObjs)) {
			oDataSet = oManFanClubStatus.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAN);
	}

	private string GetSelfManTweetReadCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		ManTweetReadSeekCondition oCondition = new ManTweetReadSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (ManTweetRead oManTweetRead = new ManTweetRead()) {
			oManTweetRead.GetPageCount(oCondition,1,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private bool IsSelfManTweetRead(string pTag,string pArgument) {
		decimal dRecCount = 0;
		string[] oArgs = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseWoman,oArgs,0);
		string sManUserSeq = iBridUtil.GetStringValue(oQuery["manuserseq"]);

		if (string.IsNullOrEmpty(sManUserSeq)) {
			return false;
		}

		ManTweetReadSeekCondition oCondition = new ManTweetReadSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.ManUserSeq = sManUserSeq;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (ManTweetRead oManTweetRead = new ManTweetRead()) {
			oManTweetRead.GetPageCount(oCondition,1,out dRecCount);
		}

		if (dRecCount > 0) {
			return true;
		} else {
			return false;
		}
	}

	private string GetManTweetCommentCnt(string pTag,string pArgument) {
		string sValue = string.Empty;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManTweetCommentCntSeekCondition oCondition = new ManTweetCommentCntSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseWoman.sessionWoman.site.siteCd;

		using (ManTweetCommentCnt oManTweetCommentCnt = new ManTweetCommentCnt()) {
			DataSet oDataSet = oManTweetCommentCnt.GetListParts(oCondition);

			if (oDataSet.Tables[0].Rows.Count > 0) {
				sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_MAN_TWEET_COMMENT_CNT);
			}
		}

		return sValue;
	}

	private string GetCastNewFace(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.conditionFlag = ViCommConst.INQUIRY_CAST_NEW_FACE;
		oCondition.siteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.userSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.userCharNo = parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.newCastDay = parseWoman.sessionWoman.site.newFaceDays;
		oCondition.orderAsc = iBridUtil.GetStringValue(oPartsArgs.Query["orderasc"]);
		oCondition.orderDesc = iBridUtil.GetStringValue(oPartsArgs.Query["orderdesc"]);

		if (iBridUtil.GetStringValue(oPartsArgs.Query["random"]).Equals(ViCommConst.FLAG_ON_STR)) {
			oCondition.directRandom = ViCommConst.FLAG_ON;
		}

		using (Cast oCast = new Cast(parseWoman.sessionObjs)) {
			oDataSet = oCast.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetSelfCompleteYakyukenGameCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		YakyukenGameSeekCondition oCondition = new YakyukenGameSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;
		oCondition.CompleteFlag = ViCommConst.FLAG_ON_STR;

		using (YakyukenGame oYakyukenGame = new YakyukenGame()) {
			oYakyukenGame.GetPageCount(oCondition,1,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private string GetSelfYakyukenCommentCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		YakyukenCommentSeekCondition oCondition = new YakyukenCommentSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
			oYakyukenComment.GetPageCount(oCondition,1,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private string GetYakyukenPicUploadAddr(string pTag,string pArgument) {
		string sAddr = string.Empty;
		string sYakyukenPicTmpSeq = string.Empty;

		sYakyukenPicTmpSeq = parseWoman.sessionWoman.requestQuery.QueryString["ypictmpseq"];

		if (string.IsNullOrEmpty(sYakyukenPicTmpSeq)) {
			return string.Empty;
		}

		sAddr = string.Format("{0}{1}{2}{3}_{4}@{5}",
			ViCommConst.YAKYUKEN_URL_HEADER2,
			parseWoman.sessionWoman.site.siteCd,
			parseWoman.sessionWoman.userWoman.loginId,
			parseWoman.sessionWoman.userWoman.curCharNo,
			sYakyukenPicTmpSeq,
			parseWoman.sessionWoman.site.mailHost
		);

		return sAddr;
	}

	private string GetSelfYakyukenJyankenPic(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		YakyukenJyankenPicSeekCondition oCondition = new YakyukenJyankenPicSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (YakyukenJyankenPic oYakyukenJyankenPic = new YakyukenJyankenPic()) {
			oDataSet = oYakyukenJyankenPic.GetList(oCondition);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_YAKYUKEN_JYANKEN_PIC);
	}

	private string GetPhotoScriptSp(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sPicSeq = iBridUtil.GetStringValue(parseWoman.sessionWoman.requestQuery.QueryString["pic_seq"]);
		DataSet oDataSet = null;

		if (string.IsNullOrEmpty(sPicSeq)) {
			return sValue;
		}

		string sWebPhisicalDir = parseWoman.sessionWoman.site.webPhisicalDir;
		string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.FLASH_DIR);
		string sScriptPath = Path.Combine(sFlashDir,pArgument);
		string sObjPhotoImgPath = string.Empty;
		string sObjPicDoc = string.Empty;

		using (CastPic oCastPic = new CastPic(parseWoman.sessionObjs)) {
			oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			if (!oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString().Equals(parseWoman.sessionWoman.userWoman.userSeq) || !oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString().Equals(parseWoman.sessionWoman.userWoman.curCharNo)) {
				return sValue;
			} else {
				sObjPhotoImgPath = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]);
				sObjPicDoc = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]);
			}
		}

		if (string.IsNullOrEmpty(sObjPhotoImgPath)) {
			return sValue;
		}

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			using (StreamReader srJs = new StreamReader(sScriptPath)) {
				sValue = srJs.ReadToEnd();
			}

			sValue = sValue.Replace("%IMAGE%",sObjPhotoImgPath);
			sValue = sValue.Replace("%COMMENT%",sObjPicDoc);
			sValue = sValue.Replace("%NEXT_URL%",iBridUtil.GetStringValue(parseWoman.sessionWoman.requestQuery.QueryString["next_url"]));
		}

		return sValue;
	}

	private string GetListCastPic(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);
		string sPicType = iBridUtil.GetStringValue(oPartsArgs.Query["pictype"]);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();

		using (CastPic oCastPic = new CastPic(parseWoman.sessionObjs)) {
			oTmpDataSet = oCastPic.GetPageCollectionBySeq(sObjSeq,sPicType);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_CAST_PIC);

		return sValue;
	}

	private string GetListSelfMovie(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);

		using (CastMovie oCastMovie = new CastMovie(parseWoman.sessionObjs)) {
			if (!sObjSeq.Equals(string.Empty)) {
				oTmpDataSet = oCastMovie.GetPageCollectionBySeq(sObjSeq,false);
			}
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_SELF_MOVIE);

		return sValue;
	}

	private string GetListManNews(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		ManNewsSeekCondition oCondition = new ManNewsSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.OnlyFavorit = iBridUtil.GetStringValue(oPartsArgs.Query["onlyfavorit"]);

		using (ManNews oManNews = new ManNews(parseWoman.sessionObjs)) {
			oTmpDataSet = oManNews.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_MAN);

		return sValue;
	}

	private string GetSelfGameCastNews(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		GameCastNewsSeekCondition oCondition = new GameCastNewsSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (GameCastNews oGameCastNews = new GameCastNews()) {
			oDataSet = oGameCastNews.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_GAME_NEWS);

		return sValue;
	}

	private string GetOneSelfRankElection(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		DataSet dsElectionPeriod;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);

		using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
			dsElectionPeriod = oElectionPeriod.GetCurrent(this.parseWoman.sessionWoman.site.siteCd);
		}

		if (dsElectionPeriod.Tables[0].Rows.Count > 0) {
			ElectionEntrySeekCondition oCondition = new ElectionEntrySeekCondition(oPartsArgs.Query);
			using (ElectionEntry oElectionEntry = new ElectionEntry(parseWoman.sessionObjs)) {
				oCondition.SiteCd = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["SITE_CD"]);
				oCondition.ElectionPeriodSeq = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["ELECTION_PERIOD_SEQ"]);
				oCondition.FixRankFlag = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["FIX_RANK_FLAG"]);
				oCondition.ElectionPeriodStatus = string.Empty;

				DateTime oFirstPeriodEndDate = DateTime.Parse(dsElectionPeriod.Tables[0].Rows[0]["FIRST_PERIOD_END_DATE"].ToString());
				if (oFirstPeriodEndDate >= DateTime.Now) {
					oCondition.ElectionPeriodStatus = PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD;
				}

				oDataSet = oElectionEntry.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
			}
		} else {
			oDataSet = new DataSet();
			DataTable dt = new DataTable();
			oDataSet.Tables.Add(dt);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
		}

		return sValue;
	}

	private string GetUserMail(string pTag,string pArgument) {
		DataSet oTempDataSet = null;
		DataSet oDataSet = new DataSet();
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		string sPartnerLoginId = iBridUtil.GetStringValue(oPartsArgs.Query["loginid"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(oPartsArgs.Query["partner_user_char_no"]);
		string sTxRxType = iBridUtil.GetStringValue(oPartsArgs.Query["tx_rx_type"]);
		string sMailSeq = iBridUtil.GetStringValue(oPartsArgs.Query["mail_seq"]);
		string sMailSearchKey = iBridUtil.GetStringValue(oPartsArgs.Query["search_key"]);
		string sSortReverseFlag = iBridUtil.GetStringValue(oPartsArgs.Query["sort_reverse"]);
		string sUpdateReadFlag = iBridUtil.GetStringValue(oPartsArgs.Query["update_read"]);

		using (MailBox oMailBox = new MailBox()) {
			oTempDataSet = oMailBox.GetPageCollectionBase(
				parseWoman.sessionWoman.site.siteCd,
				parseWoman.sessionWoman.userWoman.userSeq,
				parseWoman.sessionWoman.userWoman.curCharNo,
				sPartnerLoginId,
				sPartnerUserCharNo,
				parseWoman.sessionWoman.sexCd,
				ViCommConst.MAIL_BOX_USER,
				sTxRxType,
				true,
				sMailSeq,
				sMailSearchKey,
				null,
				null,
				false,
				string.Empty,
				string.Empty,
				string.Empty,
				1,
				oPartsArgs.NeedCount
			);
		}

		if (!parseWoman.sessionWoman.adminFlg) {
			if (sUpdateReadFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				List<string> oMailSeqList = new List<string>();

				foreach (DataRow dr in oTempDataSet.Tables[0].Rows) {
					if (iBridUtil.GetStringValue(dr["TXRX_TYPE"]).Equals(ViCommConst.RX) && iBridUtil.GetStringValue(dr["READ_FLAG"]).Equals(ViCommConst.FLAG_OFF_STR)) {
						oMailSeqList.Add(iBridUtil.GetStringValue(dr["MAIL_SEQ"]));
					}
				}

				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailReadFlagBatch(oMailSeqList.ToArray());
				}
			}
		}

		if (sSortReverseFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			List<DataRow> oDataRowList = new List<DataRow>();
			foreach (DataRow oDR in oTempDataSet.Tables[0].Rows) {
				oDataRowList.Add(oDR);
			}
			DataRow[] oDataRowArray = oDataRowList.ToArray();

			Array.Reverse(oDataRowArray);
			DataTable oDataTable = oTempDataSet.Tables[0].Clone();
			DataRow oDataRowCopy;

			foreach (DataRow oDR in oDataRowArray) {
				oDataRowCopy = oDataTable.NewRow();
				oDataRowCopy.ItemArray = oDR.ItemArray;
				oDataTable.Rows.Add(oDataRowCopy);
			}

			oDataSet.Tables.Add(oDataTable);
		} else {
			oDataSet = oTempDataSet;
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
	}

	private string GetTotalTxRxMailCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iTotalTxMailCount = 0;
		int iTotalRxMailCount = 0;
		DataSet oDataSet = null;

		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseWoman,oArgArr,0);

		string sSiteCd = parseWoman.sessionWoman.site.siteCd;
		string sUserSeq = iBridUtil.GetStringValue(oQuery["partner_user_seq"]);
		string sUserCharNo = iBridUtil.GetStringValue(oQuery["partner_char_no"]);
		string sPartnerUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		string sPartnerCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		if (string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sUserCharNo)) {
			return sValue;
		}

		using (Communication oCommunication = new Communication()) {
			oDataSet = oCommunication.GetOneByUserSeq(sSiteCd,sUserSeq,sUserCharNo,sPartnerUserSeq,sPartnerCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			int.TryParse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_TX_MAIL_COUNT"]),out iTotalTxMailCount);
			int.TryParse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_RX_MAIL_COUNT"]),out iTotalRxMailCount);
		}

		int iTotalTxRxMailCount = iTotalTxMailCount + iTotalRxMailCount;

		sValue = iBridUtil.GetStringValue(iTotalTxRxMailCount);

		return sValue;
	}

	private string GetSelfGameImgPath(string pTag,string pArgument) {
		string sValue = string.Empty;

		if (!parseWoman.sessionWoman.userWoman.characterList[parseWoman.sessionWoman.userWoman.curKey].smallPhotoImgPath.Equals("image/A001/nopic_s.gif")) {
			sValue = "../" + parseWoman.sessionWoman.userWoman.characterList[parseWoman.sessionWoman.userWoman.curKey].smallPhotoImgPath;
		} else {
			sValue = string.Format("../Image/{0}/game/wo_type_a{1}.gif",parseWoman.sessionWoman.site.siteCd,getGameCharacter().gameCharacterType);
		}

		return sValue;
	}

	private string GetSelfWaitSchedule(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		WaitScheduleSeekCondition oCondition = new WaitScheduleSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		string sMin;
		if (DateTime.Now.Minute < 30) {
			sMin = "00";
		} else {
			sMin = "30";
		}
		oCondition.MinWaitStartTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),sMin);

		using (WaitSchedule oWaitSchedule = new WaitSchedule(parseWoman.sessionObjs)) {
			oDataSet = oWaitSchedule.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_WAIT_SCHEDULE);
	}

	private string GetReservedMailRequestCount(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		MailRequestLogSeekCondition oCondition = new MailRequestLogSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.CastUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
			oDataSet = oMailRequestLog.GetReservedMailRequestCount(oCondition);

			if (int.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["RESERVED_MAIL_REQUEST_COUNT"])) == 0) {
				oDataSet = new DataSet();
			}
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAN);
	}

	private string GetSelfLoginPlan(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		LoginPlanSeekCondition oCondition = new LoginPlanSeekCondition();
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		string sMin;
		if (DateTime.Now.Minute < 30) {
			sMin = "00";
		} else {
			sMin = "30";
		}
		oCondition.MinLoginStartTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),sMin);

		using (LoginPlan oLoginPlan = new LoginPlan(parseWoman.sessionObjs)) {
			oDataSet = oLoginPlan.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_LOGIN_PLAN);
	}
	
	private bool GetMailLotteryOpening(string pTag,string pArgument) {
		bool bOK = false;
		
		using (MailLottery oMailLottery = new MailLottery()) {
			bOK = oMailLottery.GetMailLotteryOpening(this.parseWoman.sessionWoman.site.siteCd,ViCommConst.OPERATOR);
		}
		
		return bOK;
	}

	private string GetMailLotteryRate(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		string sMailLotteryRateSeq = iBridUtil.GetStringValue(oPartsArgs.Query["lottery"]);
		string sSecondLotteryFlag = iBridUtil.GetStringValue(oPartsArgs.Query["second"]);
		
		if (string.IsNullOrEmpty(sSecondLotteryFlag)) {
			sSecondLotteryFlag = ViCommConst.FLAG_OFF_STR;
		}
		
		using (MailLotteryRate oMailLotteryRate = new MailLotteryRate()) {
			oDataSet = oMailLotteryRate.GetOne(this.parseWoman.sessionWoman.site.siteCd,sMailLotteryRateSeq,sSecondLotteryFlag);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_MAIL_LOTTERY);
	}

	private bool CheckCanGetMailLottery(string pTag,string pArgument) {
		string[] sArr = pArgument.Split(',');
		
		if (sArr.Length < 2) {
			return false;
		}
		
		string sMailSeq = sArr[0];
		string sPartnerMailSeq = sArr[1];
		string sMailLotteryRateSeq;
		string sLoseFlag;
		string sResult;
		
		using (MailLottery oMailLottery = new MailLottery()) {
			oMailLottery.GetMailLottery(
				this.parseWoman.sessionWoman.site.siteCd,
				this.parseWoman.sessionWoman.userWoman.userSeq,
				this.parseWoman.sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR,
				sMailSeq,
				sPartnerMailSeq,
				ViCommConst.FLAG_ON,
				out sMailLotteryRateSeq,
				out sLoseFlag,
				out sResult
			);
		}
		
		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			return true;
		} else {
			return false;
		}
	}

	private string GetManDataByLoginId(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		string sLoginId = iBridUtil.GetStringValue(oPartsArgs.Query["loginid"]);
		if (string.IsNullOrEmpty(sLoginId)) {
			return string.Empty;
		}
		using (Man oMan = new Man(parseWoman.sessionObjs)) {
			oDataSet = oMan.GetOneByLoginId(parseWoman.sessionWoman.site.siteCd,sLoginId,1);
		}

		return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAN);
	}

	private string GetGcappAuthLink(string pTag,string pArgument) {
		string sLink = string.Empty;

		if (parseWoman.sessionWoman.logined) {
			string sData = string.Format("uid={0}&pw={1}&sex={2}",parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.loginPassword,ViCommConst.OPERATOR);
			sData = AEScryptHelper.Encrypt(sData);
			sLink = string.Format("girlschatapp://browser/?data={0}",sData);
		}

		return sLink;
	}

	private string GetSelfGcappVersion(string pTag,string pArgument) {
		string sResult = string.Empty;

		if (parseWoman.sessionWoman.logined) {
			using (User oUser = new User()) {
				if (oUser.GetOne(parseWoman.sessionWoman.userWoman.userSeq,ViCommConst.OPERATOR)) {
					sResult = oUser.gcappLastLoginVersion;
				}
			}
		}

		return sResult;
	}

	private bool IsSelfGcappNeedUpdate(string pTag,string pArgument) {
		if (parseWoman.sessionWoman.logined) {
			string sSelfGcappVersion = string.Empty;
			string sLatestGcappVersion = string.Empty;

			using (User oUser = new User()) {
				if (oUser.GetOne(parseWoman.sessionWoman.userWoman.userSeq,ViCommConst.OPERATOR)) {
					sSelfGcappVersion = oUser.gcappLastLoginVersion;
				}
			}

			if (!string.IsNullOrEmpty(sSelfGcappVersion)) {
				using (SiteManagementEx oSiteManagementEx = new SiteManagementEx()) {
					if (oSiteManagementEx.GetOne(parseWoman.sessionWoman.site.siteCd)) {
						if (parseWoman.sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
							sLatestGcappVersion = oSiteManagementEx.latestGcappVeriOS;
						} else if (parseWoman.sessionWoman.carrier.Equals(ViCommConst.ANDROID)) {
							sLatestGcappVersion = oSiteManagementEx.latestGcappVerAndroid;
						}
					}
				}

				if (!string.IsNullOrEmpty(sLatestGcappVersion) && !sSelfGcappVersion.Equals(sLatestGcappVersion)) {
					return true;
				}
			}
		}

		return false;
	}

	private bool IsExistJealousyOffline(string pTag,string pArgument) {
		bool bExist = false;

		if (parseWoman.sessionWoman.logined) {
			int iCount = 0;

			using (Favorit oFavorit = new Favorit()) {
				iCount = oFavorit.GetJealousyOfflineCount(
					parseWoman.sessionWoman.site.siteCd,
					parseWoman.sessionWoman.userWoman.userSeq,
					parseWoman.sessionWoman.userWoman.curCharNo
				);
			}

			if (iCount > 0) {
				bExist = true;
			}
		}

		return bExist;
	}
	
	private string GetManIntroducerFriendCount(string pTag,string pArgument) {
		IntroducerFriendSeekCondition oCondition = new IntroducerFriendSeekCondition();
		oCondition.IntroducerFriendCd = parseWoman.sessionWoman.userWoman.CurCharacter.friendIntroCd;
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;
		
		decimal dRecCount;
		
		using (ManIntroducerFriend oManIntroducerFriend = new ManIntroducerFriend()) {
			oManIntroducerFriend.GetPageCount(oCondition,10,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private string GetManIntroducerFriendReceiptUserCount(string pTag,string pArgument) {
		IntroducerFriendSeekCondition oCondition = new IntroducerFriendSeekCondition();
		oCondition.IntroducerFriendCd = parseWoman.sessionWoman.userWoman.CurCharacter.friendIntroCd;
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		int iRecCount;

		using (ManIntroducerFriend oManIntroducerFriend = new ManIntroducerFriend()) {
			iRecCount = oManIntroducerFriend.GetReceiptUserCount(oCondition);
		}

		return iRecCount.ToString();
	}
	
	private string GetNotCheckUserIntroCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseWoman,pTag,pArgument);
		
		string sUserIntroCountCheckDate = string.Empty;
		DateTime dtUserIntroCountCheckDate;
		
		using (IntroCheck oIntroCheck = new IntroCheck()) {
			sUserIntroCountCheckDate = oIntroCheck.GetUserIntroCountCheckDate(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
		}

		if (string.IsNullOrEmpty(sUserIntroCountCheckDate)) {
			if(!string.IsNullOrEmpty(iBridUtil.GetStringValue(oPartsArgs.Query["check_date"]))) {
				sUserIntroCountCheckDate = iBridUtil.GetStringValue(oPartsArgs.Query["check_date"]);
			}
		}
		
		if (!DateTime.TryParse(sUserIntroCountCheckDate,out dtUserIntroCountCheckDate)) {
			dtUserIntroCountCheckDate = DateTime.Parse("2015/12/23 12:00:00");
		}
		
		IntroducerFriendSeekCondition oCondition = new IntroducerFriendSeekCondition();
		oCondition.IntroducerFriendCd = parseWoman.sessionWoman.userWoman.CurCharacter.friendIntroCd;
		oCondition.SiteCd = parseWoman.sessionWoman.site.siteCd;
		oCondition.UserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (ManIntroducerFriend oManIntroducerFriend = new ManIntroducerFriend()) {
			oDataSet = oManIntroducerFriend.GetNotCheckIntroCount(oCondition,dtUserIntroCountCheckDate);
		}

		if (oDataSet.Tables[0].Rows[0]["NOT_CHECK_MAN_INTRO_COUNT"].ToString().Equals("0")) {
			return string.Empty;
		} else {
			return this.parseWoman.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_EX_INTRODUCER_FRIEND);
		}
	}

	private bool IsQuickResponseEntry(string pTag,string pArgument) {
		bool bOK = false;

		string[] sValues = pArgument.Split(',');
		string sSiteCd = parseWoman.sessionWoman.site.siteCd;
		string sSelfUserSeq = parseWoman.sessionWoman.userWoman.userSeq;
		string sSelfCharNo = parseWoman.sessionWoman.userWoman.curCharNo;

		using (Cast oCast = new Cast()) {
			bOK = oCast.IsSelfQuickResponseEntry(sSiteCd,sSelfUserSeq,sSelfCharNo);
		}

		return bOK;
	}
	
	private string GetCastLikeNoticeUnconfirmedCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		using (CastLikeNotice oCastLikeNotice = new CastLikeNotice()) {
			int iValue = oCastLikeNotice.GetCastLikeNoticeUnconfirmedCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
			sValue = iValue.ToString();
		}
		
		return sValue;
	}
	
	private string GetCastLikeNoticeUnconfirmedCountPic(string pTag,string pArgument) {
		string sValue = string.Empty;

		using (CastLikeNoticePic oCastLikeNoticePic = new CastLikeNoticePic()) {
			int iValue = oCastLikeNoticePic.GetCastLikeNoticeUnconfirmedCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
			sValue = iValue.ToString();
		}

		return sValue;
	}

	private string GetCastLikeNoticeUnconfirmedCountMovie(string pTag,string pArgument) {
		string sValue = string.Empty;

		using (CastLikeNoticeMovie oCastLikeNoticeMovie = new CastLikeNoticeMovie()) {
			int iValue = oCastLikeNoticeMovie.GetCastLikeNoticeUnconfirmedCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
			sValue = iValue.ToString();
		}

		return sValue;
	}

	private string GetCastLikeNoticeUnconfirmedCountPicMovie(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		using (CastLikeNoticePic oCastLikeNoticePic = new CastLikeNoticePic())
		using (CastLikeNoticeMovie oCastLikeNoticeMovie = new CastLikeNoticeMovie()) {
			int iPicValue = oCastLikeNoticePic.GetCastLikeNoticeUnconfirmedCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
			int iMovieValue = oCastLikeNoticeMovie.GetCastLikeNoticeUnconfirmedCount(parseWoman.sessionWoman.site.siteCd,parseWoman.sessionWoman.userWoman.userSeq,parseWoman.sessionWoman.userWoman.curCharNo);
			int iTotal = iPicValue + iMovieValue;
			sValue = iTotal.ToString();
		}

		return sValue;
	}
}
