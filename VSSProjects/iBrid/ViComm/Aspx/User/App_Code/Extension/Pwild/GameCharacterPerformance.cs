﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ勝利数・親密ﾎﾟｲﾝﾄ集計
--	Progaram ID		: GameCharacterPerformance
--
--  Creation Date	: 2011.09.21
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class GameCharacterPerformanceSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public string Period {
		get {
			return this.Query["period"];
		}
		set {
			this.Query["period"] = value;
		}
	}

	public string LimitPage {
		get {
			return this.Query["limit_page"];
		}
		set {
			this.Query["limit_page"] = value;
		}
	}
	
	public string RankType {
		get {
			return this.Query["rank_type"];
		}
		set {
			this.Query["rank_type"] = value;
		}
	}

	public string SelfRank {
		get {
			return this.Query["self_rank"];
		}
		set {
			this.Query["self_rank"] = value;
		}
	}

	public GameCharacterPerformanceSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameCharacterPerformanceSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["period"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["period"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
		this.Query["limit_page"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["limit_page"]));
		this.Query["position"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["position"]));
		this.Query["self_rank"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_rank"]));
		
	}
}

[System.Serializable]
public class GameCharacterPerformance:DbSession {

	public int GetPageCount(GameCharacterPerformanceSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();

		if (pCondition.Period == PwViCommConst.GameRankPeriod.WEEK || pCondition.Period == PwViCommConst.GameRankPeriod.MONTH) {
			oSqlBuilder.AppendLine("SELECT														");
			oSqlBuilder.AppendLine("	COUNT(*)												");
			oSqlBuilder.AppendLine("FROM														");
			oSqlBuilder.AppendLine("(															");
			oSqlBuilder.AppendLine("	SELECT													");
			oSqlBuilder.AppendLine("		SUM(P.BATTLE_WIN_COUNT)	AS  BATTLE_WIN_COUNT	,	");
			oSqlBuilder.AppendLine("		SUM(P.FRIENDLY_POINT)		AS	FRIENDLY_POINT	,	");
			oSqlBuilder.AppendLine("		SUM(P.EXP)				AS	EXP					,	");
			oSqlBuilder.AppendLine("		SUM(P.PAYMENT)			AS	PAYMENT					");
			oSqlBuilder.AppendLine("	FROM													");
			oSqlBuilder.AppendLine("		VW_GAME_USER_PERFORMANCE01 P					,	");
			if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
				oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER C								");
			} else {
				oSqlBuilder.AppendLine("	T_CAST_CHARACTER C									");
			}
			
			// where
			string sWhereClause = string.Empty;

			SysPrograms.SqlAppendWhere(" P.SITE_CD		= C.SITE_CD",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= C.USER_SEQ",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= C.USER_CHAR_NO",ref sWhereClause);
			
			OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
			oSqlBuilder.AppendLine(sWhereClause);
			oSqlBuilder.AppendLine("	GROUP BY P.SITE_CD,P.USER_SEQ,P.USER_CHAR_NO	");
			oSqlBuilder.AppendLine(")											");
			
			if (!string.IsNullOrEmpty(pCondition.RankType)) {
				if (pCondition.RankType.Equals(PwViCommConst.RankType.BATTLE_WIN)) {
					oSqlBuilder.AppendLine(" WHERE BATTLE_WIN_COUNT > 0");
				} else if (pCondition.RankType.Equals(PwViCommConst.RankType.PAYMENT)) {
					oSqlBuilder.AppendLine(" WHERE PAYMENT > 0");
				}
			}
			
			pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		} else {
			oSqlBuilder.AppendLine(" SELECT								");
			oSqlBuilder.AppendLine("	COUNT(*)						");
			oSqlBuilder.AppendLine(" FROM								");
			oSqlBuilder.AppendLine("	VW_GAME_USER_PERFORMANCE01 P ,	");
			if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
				oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER C							");
			} else {
				oSqlBuilder.AppendLine("	T_CAST_CHARACTER C								");
			}

			// where
			string sWhereClause = string.Empty;

			SysPrograms.SqlAppendWhere(" P.SITE_CD		= C.SITE_CD",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= C.USER_SEQ",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= C.USER_CHAR_NO",ref sWhereClause);
			
			OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
			
			if (!string.IsNullOrEmpty(pCondition.RankType)) {
				if (pCondition.RankType.Equals(PwViCommConst.RankType.BATTLE_WIN)) {
					SysPrograms.SqlAppendWhere(" P.BATTLE_WIN_COUNT  > 0",ref sWhereClause);
				} else if (pCondition.RankType.Equals(PwViCommConst.RankType.PAYMENT)) {
					SysPrograms.SqlAppendWhere(" P.PAYMENT  > 0",ref sWhereClause);
				}
			}
			
			oSqlBuilder.AppendLine(sWhereClause);
			pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		}
		
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameCharacterPerformanceSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		if (pCondition.Period == PwViCommConst.GameRankPeriod.WEEK || pCondition.Period == PwViCommConst.GameRankPeriod.MONTH) {
			// select 週別,月別
			oSqlBuilder.AppendLine("SELECT														");
			oSqlBuilder.AppendLine("	SITE_CD												,	");
			oSqlBuilder.AppendLine("	USER_SEQ											,	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO										,	");
			oSqlBuilder.AppendLine("	BATTLE_WIN_COUNT									,	");
			oSqlBuilder.AppendLine("	FRIENDLY_POINT										,	");
			oSqlBuilder.AppendLine("	EXP													,	");
			oSqlBuilder.AppendLine("	PAYMENT													");
			oSqlBuilder.AppendLine("FROM														");
			oSqlBuilder.AppendLine("(															");
			oSqlBuilder.AppendLine("	SELECT													");
			oSqlBuilder.AppendLine("		P.SITE_CD										,	");
			oSqlBuilder.AppendLine("		P.USER_SEQ										,	");
			oSqlBuilder.AppendLine("		P.USER_CHAR_NO									,	");
			oSqlBuilder.AppendLine("		SUM(P.BATTLE_WIN_COUNT)	AS  BATTLE_WIN_COUNT	,	");
			oSqlBuilder.AppendLine("		SUM(P.FRIENDLY_POINT)		AS	FRIENDLY_POINT	,	");
			oSqlBuilder.AppendLine("		SUM(P.EXP)				AS	EXP					,	");
			oSqlBuilder.AppendLine("		SUM(P.PAYMENT)			AS	PAYMENT					");
			oSqlBuilder.AppendLine("	FROM													");
			oSqlBuilder.AppendLine("		VW_GAME_USER_PERFORMANCE01 P					,	");
			if(pCondition.SexCd.Equals(ViCommConst.MAN)) {
				oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER C							");
			} else {
				oSqlBuilder.AppendLine("	T_CAST_CHARACTER C								");
			}

			SysPrograms.SqlAppendWhere(" P.SITE_CD		= C.SITE_CD",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= C.USER_SEQ",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= C.USER_CHAR_NO",ref sWhereClause);
			
			oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
			oSqlBuilder.AppendLine(sWhereClause);
			oSqlBuilder.AppendLine("	GROUP BY P.SITE_CD,P.USER_SEQ,P.USER_CHAR_NO	");
			oSqlBuilder.AppendLine(") P");
			
			if (!string.IsNullOrEmpty(pCondition.RankType)) {
				if (pCondition.RankType.Equals(PwViCommConst.RankType.BATTLE_WIN)) {
					oSqlBuilder.AppendLine(" WHERE BATTLE_WIN_COUNT > 0");
				} else if (pCondition.RankType.Equals(PwViCommConst.RankType.PAYMENT)) {
					oSqlBuilder.AppendLine(" WHERE PAYMENT > 0");
				}
			}
			
			string sExecSql = string.Empty;
			// order by
			sSortExpression = this.CreateOrderExpresion(pCondition);
			oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

			oSqlBuilder = new StringBuilder();
			
			oSqlBuilder.AppendLine(" SELECT									");
			oSqlBuilder.AppendLine("	P.USER_SEQ						,	");
			oSqlBuilder.AppendLine("	P.USER_CHAR_NO					,	");
			oSqlBuilder.AppendLine("	CHARACTER.GAME_HANDLE_NM		,	");
			oSqlBuilder.AppendLine("	CHARACTER.GAME_CHARACTER_TYPE	,	");
			oSqlBuilder.AppendLine("	CHARACTER.GAME_CHARACTER_LEVEL	,	");
			oSqlBuilder.AppendLine("	P.BATTLE_WIN_COUNT				,	");
			oSqlBuilder.AppendLine("	P.FRIENDLY_POINT				,	");
			oSqlBuilder.AppendLine("	P.EXP							,	");
			oSqlBuilder.AppendLine("	P.PAYMENT							");
			oSqlBuilder.AppendLine(" FROM(									");
			oSqlBuilder.AppendFormat("{0}",sExecSql);
			oSqlBuilder.AppendLine(" 	) P		,							");
			oSqlBuilder.AppendLine(" 	(SELECT									");
			oSqlBuilder.AppendLine(" 		SITE_CD					,			");
			oSqlBuilder.AppendLine(" 		USER_SEQ				,			");
			oSqlBuilder.AppendLine(" 		USER_CHAR_NO			,			");
			oSqlBuilder.AppendLine(" 		GAME_HANDLE_NM			,			");
			oSqlBuilder.AppendLine(" 		GAME_CHARACTER_TYPE		,			");
			oSqlBuilder.AppendLine(" 		GAME_CHARACTER_LEVEL				");
			oSqlBuilder.AppendLine(" 	FROM									");
			oSqlBuilder.AppendLine(" 		T_GAME_CHARACTER					");
			oSqlBuilder.AppendLine(" 	) CHARACTER											");
			oSqlBuilder.AppendLine(" WHERE													");
			oSqlBuilder.AppendLine(" 	P.SITE_CD		= CHARACTER.SITE_CD			AND	");
			oSqlBuilder.AppendLine(" 	P.USER_SEQ		= CHARACTER.USER_SEQ		AND ");
			oSqlBuilder.AppendLine(" 	P.USER_CHAR_NO	= CHARACTER.USER_CHAR_NO		");
		} else {
			// select 日別
			oSqlBuilder.AppendLine("SELECT										");
			oSqlBuilder.AppendLine("	P.USER_SEQ				,				");
			oSqlBuilder.AppendLine("	P.USER_CHAR_NO			,				");
			oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM			,				");
			oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE		,				");
			oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL	,				");
			oSqlBuilder.AppendLine("	P.BATTLE_WIN_COUNT		,				");
			oSqlBuilder.AppendLine("	P.FRIENDLY_POINT			,				");
			oSqlBuilder.AppendLine("	P.EXP						,				");
			oSqlBuilder.AppendLine("	P.PAYMENT									");
			oSqlBuilder.AppendLine("FROM										");
			oSqlBuilder.AppendLine("	VW_PW_GAME_USER_PERFORMANCE01 P		,	");
			if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
				oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER C							");
			} else {
				oSqlBuilder.AppendLine("	T_CAST_CHARACTER C								");
			}

			SysPrograms.SqlAppendWhere(" P.SITE_CD		= C.SITE_CD",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= C.USER_SEQ",ref sWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= C.USER_CHAR_NO",ref sWhereClause);
			
			// where		
			oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
			
			if (!string.IsNullOrEmpty(pCondition.RankType)) {
				if (pCondition.RankType.Equals(PwViCommConst.RankType.BATTLE_WIN)) {
					SysPrograms.SqlAppendWhere(" BATTLE_WIN_COUNT  > 0",ref sWhereClause);
				} else if (pCondition.RankType.Equals(PwViCommConst.RankType.PAYMENT)) {
					SysPrograms.SqlAppendWhere(" PAYMENT  > 0",ref sWhereClause);
				}
			}
			
			oSqlBuilder.AppendLine(sWhereClause);

			string sExecSql = string.Empty;
			// order by
			sSortExpression = this.CreateOrderExpresion(pCondition);
			oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

			oSqlBuilder = new StringBuilder();

			// select
			oSqlBuilder.AppendLine("SELECT									");
			oSqlBuilder.AppendLine("	*									");
			oSqlBuilder.AppendLine(" FROM(									");
			oSqlBuilder.AppendFormat("{0}",sExecSql);
			oSqlBuilder.AppendLine(" 	) P									");
			
		}

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);
		oSqlBuilder.AppendLine(sSortExpression);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		AppendRank(oDataSet,iStartIndex);
		return oDataSet;
	}

	private void AppendRank(DataSet pDS,int pStartIndex) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		int iCount = pStartIndex;
		col = new DataColumn(string.Format("GAME_RANK"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			iCount++;
			dr["GAME_RANK"] = iCount;
		}
	}

	private OracleParameter[] CreateWhere(GameCharacterPerformanceSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
	
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		DateTime dDate = DateTime.Now;
		string sToday = DateTime.Now.ToString("yyyy/MM/dd");
		string sWeek = dDate.AddDays(-6).ToString("yyyy/MM/dd");
		string sMonth = DateTime.Now.ToString("yyyy/MM/") + "01";

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}
		
		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}
		
		if (!string.IsNullOrEmpty(pCondition.Period)) {
			if (pCondition.Period == PwViCommConst.GameRankPeriod.WEEK) {
				SysPrograms.SqlAppendWhere(" P.REPORT_DAY  >= :REPORT_DAY",ref pWhereClause);
				oParamList.Add(new OracleParameter(":REPORT_DAY",sWeek));
			} else if (pCondition.Period == PwViCommConst.GameRankPeriod.MONTH) {
				SysPrograms.SqlAppendWhere(" P.REPORT_DAY  >= :REPORT_DAY",ref pWhereClause);
				oParamList.Add(new OracleParameter(":REPORT_DAY",sMonth));
			}else {
				SysPrograms.SqlAppendWhere(" P.REPORT_DAY = :REPORT_DAY",ref pWhereClause);
				oParamList.Add(new OracleParameter(":REPORT_DAY",sToday));
			}
		} else {
			SysPrograms.SqlAppendWhere(" P.REPORT_DAY = :REPORT_DAY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_DAY",sToday));
		}

		SysPrograms.SqlAppendWhere(" P.STAFF_FLAG	= :STAFF_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" C.NA_FLAG	IN(:NA_FLAG,:NA_FLAG2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(GameCharacterPerformanceSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		if (pCondition.SeekMode == PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_BATTLE) {
			sSortExpression = string.Format("ORDER BY P.BATTLE_WIN_COUNT DESC , P.EXP DESC , P.USER_SEQ ");
		} else if (pCondition.SeekMode == PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_PAYMENT) {
			sSortExpression = string.Format("ORDER BY P.PAYMENT DESC , P.EXP DESC , P.USER_SEQ ");
		} else {
			sSortExpression = string.Format("ORDER BY P.USER_SEQ ");
		}

		return sSortExpression;
	}

	public int GetMyPageCount(GameCharacterPerformanceSeekCondition pCondition,int pRecPerPage) {

		decimal iRecCount = 0;
		string sSortExpression = string.Empty;
		DateTime dDate = DateTime.Now;
		string sToday = DateTime.Now.ToString("yyyy/MM/dd");
		string sWeek = dDate.AddDays(-6).ToString("yyyy/MM/dd");
		string sMonth = DateTime.Now.ToString("yyyy/MM/") + "01";
		
		StringBuilder oSqlBuilder = new StringBuilder();

		if (pCondition.Period == PwViCommConst.GameRankPeriod.WEEK || pCondition.Period == PwViCommConst.GameRankPeriod.MONTH) {
			// select 週別,月別
			oSqlBuilder.AppendLine("SELECT															");
			oSqlBuilder.AppendLine("	RANK_NO														");
			oSqlBuilder.AppendLine("FROM															");
			oSqlBuilder.AppendLine("(																");
			oSqlBuilder.AppendLine("	SELECT														");
			oSqlBuilder.AppendLine("		SITE_CD												,	");
			oSqlBuilder.AppendLine("		USER_SEQ											,	");
			oSqlBuilder.AppendLine("		USER_CHAR_NO										,	");
			oSqlBuilder.AppendFormat("		ROW_NUMBER() OVER ({0}) AS RANK_NO						\n",this.CreateOrderExpresion(pCondition));
			oSqlBuilder.AppendLine("	FROM														");
			oSqlBuilder.AppendLine("		(SELECT													");
			oSqlBuilder.AppendLine("			P.SITE_CD										,	");
			oSqlBuilder.AppendLine("			P.USER_SEQ										,	");
			oSqlBuilder.AppendLine("			P.USER_CHAR_NO									,	");
			oSqlBuilder.AppendLine("			SUM(P.BATTLE_WIN_COUNT)	AS  BATTLE_WIN_COUNT	,	");
			oSqlBuilder.AppendLine("			SUM(P.FRIENDLY_POINT)	AS	FRIENDLY_POINT		,	");
			oSqlBuilder.AppendLine("			SUM(P.EXP)				AS	EXP					,	");
			oSqlBuilder.AppendLine("			SUM(P.PAYMENT)			AS	PAYMENT					");
			oSqlBuilder.AppendLine("		FROM													");
			oSqlBuilder.AppendLine("			VW_GAME_USER_PERFORMANCE01 P					,	");
			if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
				oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER C							");
			} else {
				oSqlBuilder.AppendLine("		T_CAST_CHARACTER C								");
			}
			oSqlBuilder.AppendLine(" 		WHERE														");
			oSqlBuilder.AppendLine("			P.SITE_CD		= C.SITE_CD							AND	");
			oSqlBuilder.AppendLine("			P.USER_SEQ		= C.USER_SEQ						AND	");
			oSqlBuilder.AppendLine("			P.USER_CHAR_NO	= C.USER_CHAR_NO					AND	");
			oSqlBuilder.AppendLine("			P.SITE_CD		= :SITE_CD							AND	");
			oSqlBuilder.AppendLine("			P.SEX_CD		= :SEX_CD							AND	");
			oSqlBuilder.AppendLine("			P.REPORT_DAY	>= :REPORT_DAY						AND	");
			oSqlBuilder.AppendLine("			P.STAFF_FLAG	= :STAFF_FLAG						AND	");
			oSqlBuilder.AppendLine("			C.NA_FLAG	IN(:NA_FLAG,:NA_FLAG2)				AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		");
			oSqlBuilder.AppendLine("		GROUP BY P.SITE_CD,P.USER_SEQ,P.USER_CHAR_NO				");
			oSqlBuilder.AppendLine(" 		) P															");

			if (!string.IsNullOrEmpty(pCondition.RankType)) {
				if (pCondition.RankType.Equals(PwViCommConst.RankType.BATTLE_WIN)) {
					oSqlBuilder.AppendLine(" WHERE P.BATTLE_WIN_COUNT > 0");
				} else if (pCondition.RankType.Equals(PwViCommConst.RankType.PAYMENT)) {
					oSqlBuilder.AppendLine(" WHERE P.PAYMENT > 0");
				}
			}
			
			oSqlBuilder.AppendLine(")											");
			oSqlBuilder.AppendLine("WHERE										");
			oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			");
			
		} else {
			// select 日別
			oSqlBuilder.AppendLine("SELECT										");
			oSqlBuilder.AppendLine("	RANK_NO									");
			oSqlBuilder.AppendLine("FROM										");
			oSqlBuilder.AppendLine("(											");
			oSqlBuilder.AppendLine("	SELECT									");
			oSqlBuilder.AppendLine("		SITE_CD							,	");
			oSqlBuilder.AppendLine("		USER_SEQ						,	");
			oSqlBuilder.AppendLine("		USER_CHAR_NO					,	");
			oSqlBuilder.AppendFormat("		ROW_NUMBER() OVER ({0}) AS RANK_NO	\n",this.CreateOrderExpresion(pCondition));
			oSqlBuilder.AppendLine("	FROM									");
			oSqlBuilder.AppendLine("	(										");
			oSqlBuilder.AppendLine("		SELECT								");
			oSqlBuilder.AppendLine("			P.SITE_CD					,	");
			oSqlBuilder.AppendLine("			P.USER_SEQ					,	");
			oSqlBuilder.AppendLine("			P.USER_CHAR_NO				,	");
			oSqlBuilder.AppendLine("			P.BATTLE_WIN_COUNT			,	");
			oSqlBuilder.AppendLine("			P.FRIENDLY_POINT			,	");
			oSqlBuilder.AppendLine("			P.EXP						,	");
			oSqlBuilder.AppendLine("			P.PAYMENT						");
			oSqlBuilder.AppendLine("		FROM								");
			oSqlBuilder.AppendLine("			VW_GAME_USER_PERFORMANCE01 P	,	");
			if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
				oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER C			");
			} else {
				oSqlBuilder.AppendLine("		T_CAST_CHARACTER C				");
			}
			oSqlBuilder.AppendLine("		WHERE								");
			oSqlBuilder.AppendLine("			P.SITE_CD		= C.SITE_CD						AND	");
			oSqlBuilder.AppendLine("			P.USER_SEQ		= C.USER_SEQ					AND	");
			oSqlBuilder.AppendLine("			P.USER_CHAR_NO	= C.USER_CHAR_NO				AND	");
			oSqlBuilder.AppendLine("			P.SITE_CD			= :SITE_CD						AND	");
			oSqlBuilder.AppendLine("			P.SEX_CD			= :SEX_CD						AND	");
			oSqlBuilder.AppendLine("			P.REPORT_DAY		= :REPORT_DAY					AND	");
			oSqlBuilder.AppendLine("			P.STAFF_FLAG		= :STAFF_FLAG					AND	");
			oSqlBuilder.AppendLine("			C.NA_FLAG		IN(:NA_FLAG,:NA_FLAG2)			AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG	AND			");
			oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG	AND				");
			oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");

			if (!string.IsNullOrEmpty(pCondition.RankType)) {
				if (pCondition.RankType.Equals(PwViCommConst.RankType.BATTLE_WIN)) {
					oSqlBuilder.AppendLine(" AND BATTLE_WIN_COUNT > 0");
				} else if (pCondition.RankType.Equals(PwViCommConst.RankType.PAYMENT)) {
					oSqlBuilder.AppendLine(" AND PAYMENT > 0");
				}
			}
			
			oSqlBuilder.AppendLine("	) P										");
			
			if (!string.IsNullOrEmpty(pCondition.RankType)) {
				if (pCondition.RankType.Equals(PwViCommConst.RankType.BATTLE_WIN)) {
					oSqlBuilder.AppendLine(" WHERE BATTLE_WIN_COUNT > 0");
				} else if (pCondition.RankType.Equals(PwViCommConst.RankType.PAYMENT)) {
					oSqlBuilder.AppendLine(" WHERE PAYMENT > 0");
				}
			}
			
			oSqlBuilder.AppendLine(")");
			oSqlBuilder.AppendLine("WHERE");
			oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			");
		}
	
		using (DataSet ds = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":SEX_CD",pCondition.SexCd);
				cmd.Parameters.Add(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":NA_FLAG",ViCommConst.NaFlag.OK);
				cmd.Parameters.Add(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH);
				cmd.Parameters.Add(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				if (!string.IsNullOrEmpty(pCondition.Period)) {
					if (pCondition.Period == PwViCommConst.GameRankPeriod.WEEK) {
						cmd.Parameters.Add(":REPORT_DAY",sWeek);
					} else if (pCondition.Period == PwViCommConst.GameRankPeriod.MONTH) {
						cmd.Parameters.Add(":REPORT_DAY",sMonth);
					} else {
						cmd.Parameters.Add(":REPORT_DAY",sToday);
					}
				} else {
					cmd.Parameters.Add(":REPORT_DAY",sToday);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
			
			if(ds.Tables[0].Rows.Count > 0) {
					decimal.TryParse(ds.Tables[0].Rows[0]["RANK_NO"].ToString(),out iRecCount);
			}
		}

		return (int)Math.Ceiling(iRecCount / pRecPerPage);
	}

	public DataSet GetMyRankData(GameCharacterPerformanceSeekCondition pCondition) {
		
		string sSortExpression = string.Empty;
		DataSet oDataSet = new DataSet();
		DateTime dDate = DateTime.Now;
		string sToday = DateTime.Now.ToString("yyyy/MM/dd");
		string sWeek = dDate.AddDays(-6).ToString("yyyy/MM/dd");
		string sMonth = DateTime.Now.ToString("yyyy/MM/") + "01";

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		if (pCondition.Period == PwViCommConst.GameRankPeriod.WEEK || pCondition.Period == PwViCommConst.GameRankPeriod.MONTH) {
			// select 週別,月別
			oSqlBuilder.AppendLine("SELECT																");
			oSqlBuilder.AppendLine("	PAYMENT														,	");
			oSqlBuilder.AppendLine("	GAME_RANK														");
			oSqlBuilder.AppendLine("FROM																");
			oSqlBuilder.AppendLine("(																	");
			oSqlBuilder.AppendLine("	SELECT															");
			oSqlBuilder.AppendLine("		SITE_CD													,	");
			oSqlBuilder.AppendLine("		USER_SEQ												,	");
			oSqlBuilder.AppendLine("		USER_CHAR_NO											,	");
			oSqlBuilder.AppendLine("		PAYMENT													,	");
			oSqlBuilder.AppendFormat("		ROW_NUMBER() OVER ({0}) AS GAME_RANK					\n",this.CreateOrderExpresion(pCondition));
			oSqlBuilder.AppendLine("	FROM															");
			oSqlBuilder.AppendLine("	(																");
			oSqlBuilder.AppendLine("		SELECT														");
			oSqlBuilder.AppendLine("			P.SITE_CD											,	");
			oSqlBuilder.AppendLine("			P.USER_SEQ											,	");
			oSqlBuilder.AppendLine("			P.USER_CHAR_NO										,	");
			oSqlBuilder.AppendLine("			SUM(P.EXP)				AS	EXP						,	");
			oSqlBuilder.AppendLine("			SUM(P.PAYMENT)			AS	PAYMENT						");
			oSqlBuilder.AppendLine("		FROM														");
			oSqlBuilder.AppendLine("			VW_GAME_USER_PERFORMANCE01 P						,	");
			oSqlBuilder.AppendLine("			T_CAST_CHARACTER C										");
			oSqlBuilder.AppendLine(" 		WHERE														");
			oSqlBuilder.AppendLine("			P.SITE_CD		= C.SITE_CD							AND	");
			oSqlBuilder.AppendLine("			P.USER_SEQ		= C.USER_SEQ						AND	");
			oSqlBuilder.AppendLine("			P.USER_CHAR_NO	= C.USER_CHAR_NO					AND	");
			oSqlBuilder.AppendLine("			P.SITE_CD			= :SITE_CD						AND	");
			oSqlBuilder.AppendLine("			P.SEX_CD			= :SEX_CD						AND	");
			oSqlBuilder.AppendLine("			P.REPORT_DAY		>= :REPORT_DAY					AND	");
			oSqlBuilder.AppendLine("			P.STAFF_FLAG		= :STAFF_FLAG					AND	");
			oSqlBuilder.AppendLine("			C.NA_FLAG			IN(:NA_FLAG,:NA_FLAG2)			AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG					AND	");
			oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG			");
			oSqlBuilder.AppendLine("		GROUP BY P.SITE_CD,P.USER_SEQ,P.USER_CHAR_NO						");
			oSqlBuilder.AppendLine(" 	) P																");
			oSqlBuilder.AppendLine(")																	");
			oSqlBuilder.AppendLine("WHERE																");
			oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ								AND	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO								");
		} else {
			// select 日別
			oSqlBuilder.AppendLine("SELECT													");
			oSqlBuilder.AppendLine("	PAYMENT											,	");
			oSqlBuilder.AppendLine("	GAME_RANK											");
			oSqlBuilder.AppendLine("FROM													");
			oSqlBuilder.AppendLine("(														");
			oSqlBuilder.AppendLine("	SELECT												");
			oSqlBuilder.AppendLine("		P.SITE_CD									,	");
			oSqlBuilder.AppendLine("		P.USER_SEQ									,	");
			oSqlBuilder.AppendLine("		P.USER_CHAR_NO								,	");
			oSqlBuilder.AppendLine("		P.PAYMENT									,	");
			oSqlBuilder.AppendFormat("		P.ROW_NUMBER() OVER ({0}) AS GAME_RANK			\n",this.CreateOrderExpresion(pCondition));
			oSqlBuilder.AppendLine("	FROM												");
			oSqlBuilder.AppendLine("		VW_GAME_USER_PERFORMANCE01 P				,	");
			oSqlBuilder.AppendLine("		T_CAST_CHARACTER C							");
			oSqlBuilder.AppendLine("	WHERE												");
			oSqlBuilder.AppendLine("		P.SITE_CD		= C.SITE_CD					AND	");
			oSqlBuilder.AppendLine("		P.USER_SEQ		= C.USER_SEQ				AND	");
			oSqlBuilder.AppendLine("		P.USER_CHAR_NO	= C.USER_CHAR_NO			AND	");
			oSqlBuilder.AppendLine("		P.SITE_CD		= :SITE_CD					AND	");
			oSqlBuilder.AppendLine("		P.SEX_CD		= :SEX_CD					AND	");
			oSqlBuilder.AppendLine("		P.REPORT_DAY	= :REPORT_DAY				AND	");
			oSqlBuilder.AppendLine("		P.STAFF_FLAG	= :STAFF_FLAG				AND	");
			oSqlBuilder.AppendLine("		C.NA_FLAG		IN(:NA_FLAG,:NA_FLAG2)		AND	");
			oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
			oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
			oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
			oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		");
			oSqlBuilder.AppendLine(")														");
			oSqlBuilder.AppendLine("WHERE													");
			oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ						AND	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO						");
		}

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		if (!string.IsNullOrEmpty(pCondition.Period)) {
			if (pCondition.Period == PwViCommConst.GameRankPeriod.WEEK) {
				oParamList.Add(new OracleParameter(":REPORT_DAY",sWeek));
			} else if (pCondition.Period == PwViCommConst.GameRankPeriod.MONTH) {
				oParamList.Add(new OracleParameter(":REPORT_DAY",sMonth));
			} else {
				oParamList.Add(new OracleParameter(":REPORT_DAY",sToday));
			}
		} else {
			oParamList.Add(new OracleParameter(":REPORT_DAY",sToday));
		}
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter("TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));

		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count < 1) {
			oDataSet = new DataSet();
			DataTable dt = new DataTable();
			DataRow dr;
			
			oDataSet.Tables.Add(dt);

			dt.Columns.Add("PAYMENT",Type.GetType("System.String"));
			dt.Columns.Add("GAME_RANK",Type.GetType("System.String"));
			dr = dt.NewRow();
			
			dr["PAYMENT"] = "0";
			dr["GAME_RANK"] = string.Empty;
			dt.Rows.Add(dr);
		}
		
		return oDataSet;
	}
	
	public string GetGamePayment(GameCharacterPerformanceSeekCondition pCondition) {
		string sValue = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		DataSet oDataSet = new DataSet();
		DateTime dDate = DateTime.Now;
		string sMonth = DateTime.Now.ToString("yyyy/MM/") + "01";
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	SUM(PAYMENT) AS PAYMENT					");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_GAME_USER_PERFORMANCE01				");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	SEX_CD			= :SEX_CD				");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		
		if (pCondition.Period.Equals(PwViCommConst.GameRankPeriod.MONTH)) {
			oSqlBuilder.AppendLine(" AND REPORT_DAY >= :REPORT_DAY");
			oParamList.Add(new OracleParameter(":REPORT_DAY",sMonth));
		}

		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["PAYMENT"].ToString();
		}

		return sValue;
	}
}

