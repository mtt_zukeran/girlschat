﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: お気に入りニュース
--	Progaram ID		: CastFavoritNews
--
--  Creation Date	: 2014.12.24
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class CastFavoritNewsSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;

	public CastFavoritNewsSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastFavoritNewsSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class CastFavoritNews:DbSession {
	public CastFavoritNews() {
	}

	public DataSet GetPageCollection(CastFavoritNewsSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(CreateWhere(pCondition,ref sWhereClause));
		oParamList.Add(new OracleParameter(":FAVORIT_PARTNER_USER_SEQ",pCondition.SelfUserSeq));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	NEWS_TYPE,");
		oSqlBuilder.AppendLine("	NEWS_DATE,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	BLOG_SEQ,");
		oSqlBuilder.AppendLine("	NA_FLAG,");
		oSqlBuilder.AppendLine("	ACT_CATEGORY_IDX,");
		oSqlBuilder.AppendLine("	CRYPT_VALUE,");
		oSqlBuilder.AppendLine("	LOGIN_VIEW_STATUS,");
		oSqlBuilder.AppendLine("	WAITING_VIEW_STATUS,");
		oSqlBuilder.AppendLine("	BBS_NON_PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("	BLOG_NON_PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("	DIARY_NON_PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("	PROFILE_PIC_NON_PUBLISH_FLAG");
		oSqlBuilder.AppendLine("FROM (");
		oSqlBuilder.AppendLine("	SELECT");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.SITE_CD,");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_SEQ,");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.NEWS_TYPE,");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.NEWS_DATE,");
		oSqlBuilder.AppendLine("		T_USER.LOGIN_ID,");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.HANDLE_NM,");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.BLOG_SEQ,");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.NA_FLAG,");
		oSqlBuilder.AppendLine("		T_ACT_CATEGORY.PRIORITY AS ACT_CATEGORY_IDX,");
		oSqlBuilder.AppendLine("		NVL(T_EX_CHAR_NO.CRYPT_VALUE,T_CAST_NEWS.USER_CHAR_NO) AS CRYPT_VALUE,");
		oSqlBuilder.AppendLine("		NVL(T_FAVORIT.LOGIN_VIEW_STATUS,0) AS LOGIN_VIEW_STATUS,");
		oSqlBuilder.AppendLine("		NVL(T_FAVORIT.WAITING_VIEW_STATUS,0) AS WAITING_VIEW_STATUS,");
		oSqlBuilder.AppendLine("		NVL(T_FAVORIT.BBS_NON_PUBLISH_FLAG,0) AS BBS_NON_PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("		NVL(T_FAVORIT.BLOG_NON_PUBLISH_FLAG,0) AS BLOG_NON_PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("		NVL(T_FAVORIT.DIARY_NON_PUBLISH_FLAG,0) AS DIARY_NON_PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("		NVL(T_FAVORIT.PROFILE_PIC_NON_PUBLISH_FLAG,0) AS PROFILE_PIC_NON_PUBLISH_FLAG");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_CAST_NEWS,");
		oSqlBuilder.AppendLine("		T_USER,");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER,");
		oSqlBuilder.AppendLine("		T_ACT_CATEGORY,");
		oSqlBuilder.AppendLine("		T_EX_CHAR_NO,");
		oSqlBuilder.AppendLine("		T_FAVORIT");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_SEQ = T_USER.USER_SEQ AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.SITE_CD = T_CAST_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_SEQ = T_CAST_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.SITE_CD = T_ACT_CATEGORY.SITE_CD AND");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.ACT_CATEGORY_SEQ = T_ACT_CATEGORY.ACT_CATEGORY_SEQ AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.SITE_CD = T_EX_CHAR_NO.SITE_CD AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_CHAR_NO = T_EX_CHAR_NO.USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("		1 = T_EX_CHAR_NO.LATEST_FLAG AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.SITE_CD = T_FAVORIT.SITE_CD (+) AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_SEQ = T_FAVORIT.USER_SEQ (+) AND");
		oSqlBuilder.AppendLine("		T_CAST_NEWS.USER_CHAR_NO = T_FAVORIT.USER_CHAR_NO (+) AND");
		oSqlBuilder.AppendLine("		:FAVORIT_PARTNER_USER_SEQ = T_FAVORIT.PARTNER_USER_SEQ (+) AND");
		oSqlBuilder.AppendLine("		'00' = T_FAVORIT.PARTNER_USER_CHAR_NO (+)");
		oSqlBuilder.AppendLine("	) P");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY P.NEWS_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastFavoritNewsSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SelfUserSeq)) {
			StringBuilder oSubQueryBuilder = new StringBuilder();
			oSubQueryBuilder.AppendLine("EXISTS (");
			oSubQueryBuilder.AppendLine("	SELECT");
			oSubQueryBuilder.AppendLine("		1");
			oSubQueryBuilder.AppendLine("	FROM");
			oSubQueryBuilder.AppendLine("		T_FAVORIT");
			oSubQueryBuilder.AppendLine("	WHERE");
			oSubQueryBuilder.AppendLine("		SITE_CD = P.SITE_CD AND");
			oSubQueryBuilder.AppendLine("		USER_SEQ = :FAVORIT_USER_SEQ AND");
			oSubQueryBuilder.AppendLine("		USER_CHAR_NO = '00' AND");
			oSubQueryBuilder.AppendLine("		PARTNER_USER_SEQ = P.USER_SEQ AND");
			oSubQueryBuilder.AppendLine("		PARTNER_USER_CHAR_NO = P.USER_CHAR_NO");
			oSubQueryBuilder.AppendLine(")");

			SysPrograms.SqlAppendWhere(oSubQueryBuilder.ToString(),ref pWhereClause);

			oParamList.Add(new OracleParameter(":FAVORIT_USER_SEQ",pCondition.SelfUserSeq));
		}

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	public DataSet RemoveNonPublishData(DataSet pDataSet,int pNeedCount) {
		pDataSet.Tables[0].Columns.Add(new DataColumn("RANK_NO",Type.GetType("System.String")));
		DataSet ds = pDataSet.Clone();
		int iRankNo = 1;

		foreach (DataRow dr in pDataSet.Tables[0].Rows) {
			bool bImportFlag = false;
			string sNewsType = iBridUtil.GetStringValue(dr["NEWS_TYPE"]);
			string sLoginViewStatus = iBridUtil.GetStringValue(dr["LOGIN_VIEW_STATUS"]);
			string sWaitingViewStatus = iBridUtil.GetStringValue(dr["WAITING_VIEW_STATUS"]);
			string sBbsNonPublishFlag = iBridUtil.GetStringValue(dr["BBS_NON_PUBLISH_FLAG"]);
			string sBlogNonPublishFlag = iBridUtil.GetStringValue(dr["BLOG_NON_PUBLISH_FLAG"]);
			string sDiaryNonPublishFlag = iBridUtil.GetStringValue(dr["DIARY_NON_PUBLISH_FLAG"]);
			string sProfilePicNonPublishFlag = iBridUtil.GetStringValue(dr["PROFILE_PIC_NON_PUBLISH_FLAG"]);

			switch (sNewsType) {
				case PwViCommConst.CastNewsType.NEWS_TYPE_LOGIN:
					if (sLoginViewStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_STANDBY:
				case PwViCommConst.CastNewsType.NEWS_TYPE_TALK:
					if (sWaitingViewStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_BBS_PIC:
				case PwViCommConst.CastNewsType.NEWS_TYPE_BBS_MOV:
				case PwViCommConst.CastNewsType.NEWS_TYPE_BBS:
					if (sBbsNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_BLOG:
					if (sBlogNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_FREE_PIC:
					if (sProfilePicNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_DIARY:
					if (sDiaryNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				default:
					bImportFlag = true;
					break;
			}

			if (bImportFlag) {
				dr["RANK_NO"] = iRankNo.ToString();
				iRankNo++;
				ds.Tables[0].ImportRow(dr);
			}

			if (ds.Tables[0].Rows.Count >= pNeedCount) {
				break;
			}
		}

		return ds;
	}
}
