﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: ﾛｸﾞｲﾝ中の男性(メール送信済み)
--	Progaram ID		: ManLoginedMailSend
--  Creation Date	: 2016.05.10
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManLoginedMailSendSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;
	public string SelfCharNo;

	public string NoTxMailDays {
		get {
			return this.Query["notxmaildays"];
		}
		set {
			this.Query["notxmaildays"] = value;
		}
	}

	public ManLoginedMailSendSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManLoginedMailSendSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["notxmaildays"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["notxmaildays"]));
	}
}

[Serializable]
public class ManLoginedMailSend:ManBase {
	public ManLoginedMailSend() {
	}

	public int GetPageCount(ManLoginedMailSendSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P		,	");
		oSqlBuilder.AppendLine("	T_COMMUNICATION C					");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManLoginedMailSendSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sInnerSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oInnerSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		sSortExpression = this.CreateOrderExpression(pCondition);

		oInnerSqlBuilder.AppendLine("SELECT															");
		oInnerSqlBuilder.AppendLine("	P.SITE_CD												,	");
		oInnerSqlBuilder.AppendLine("	P.USER_SEQ												,	");
		oInnerSqlBuilder.AppendLine("	P.USER_CHAR_NO											,	");
		oInnerSqlBuilder.AppendLine("	P.HANDLE_NM												,	");
		oInnerSqlBuilder.AppendLine("	P.AGE													,	");
		oInnerSqlBuilder.AppendLine("	P.LOGIN_ID												,	");
		oInnerSqlBuilder.AppendLine("	P.PIC_SEQ												,	");
		oInnerSqlBuilder.AppendLine("	P.LAST_LOGIN_DATE										,	");
		oInnerSqlBuilder.AppendLine("	P.LAST_ACTION_DATE										,	");
		oInnerSqlBuilder.AppendLine("	P.CHARACTER_ONLINE_STATUS								,	");
		oInnerSqlBuilder.AppendLine("	P.REC_SEQ												,	");
		oInnerSqlBuilder.AppendLine("	P.REGIST_DATE											,	");
		oInnerSqlBuilder.AppendLine("	C.TOTAL_TALK_COUNT AS PARTNER_TOTAL_TALK_COUNT			,	");
		oInnerSqlBuilder.AppendLine("	C.TOTAL_TX_MAIL_COUNT AS PARTNER_TOTAL_TX_MAIL_COUNT	,	");
		oInnerSqlBuilder.AppendLine("	C.TOTAL_RX_MAIL_COUNT AS PARTNER_TOTAL_RX_MAIL_COUNT	,	");
		oInnerSqlBuilder.AppendLine("	C.LAST_RX_MAIL_DATE AS PARTNER_LAST_RX_MAIL_DATE			");
		oInnerSqlBuilder.AppendLine("FROM															");
		oInnerSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P								,	");
		oInnerSqlBuilder.AppendLine("	T_COMMUNICATION C											");
		oInnerSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oInnerSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sInnerSql));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	T1.*																							,	");
		oSqlBuilder.AppendLine("	GET_MAN_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0) AS PHOTO_IMG_PATH					,	");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("	CASE																								");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 0																");
		oSqlBuilder.AppendLine("		THEN SM.OFFLINE_GUIDANCE																		");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 1																");
		oSqlBuilder.AppendLine("		THEN SM.LOGINED_GUIDANCE																		");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 2																");
		oSqlBuilder.AppendLine("		THEN SM.WAITING_GUIDANCE																		");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 3																");
		oSqlBuilder.AppendLine("		THEN SM.TALKING_WSHOT_GUIDANCE																	");
		oSqlBuilder.AppendLine("	END AS MOBILE_ONLINE_STATUS_NM																	,	");
		oSqlBuilder.AppendLine("	CASE																								");
		oSqlBuilder.AppendLine("		WHEN (T1.REGIST_DATE >= TO_CHAR(SYSDATE - SM.MAN_NEW_FACE_DAYS,'YYYY/MM/DD'))					");
		oSqlBuilder.AppendLine("		THEN 1																							");
		oSqlBuilder.AppendLine("		ELSE 0																							");
		oSqlBuilder.AppendLine("	END AS NEW_MAN_FLAG																				,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			NVL(COUNT(USER_SEQ),0) AS CNT																");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_FAVORIT																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= T1.SITE_CD													AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= T1.USER_SEQ													AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= T1.USER_CHAR_NO												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO												");
		oSqlBuilder.AppendLine("	) AS LIKE_ME_FLAG																				,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			NVL(COUNT(USER_SEQ),0) AS CNT																");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_FAVORIT																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= T1.SITE_CD													AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :PARTNER_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :PARTNER_USER_CHAR_NO											AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= T1.USER_SEQ													AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= T1.USER_CHAR_NO													");
		oSqlBuilder.AppendLine("	) AS FAVORIT_FLAG																					");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine(sInnerSql);
		oSqlBuilder.AppendLine("	) T1																							,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT SM																				");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	T1.SITE_CD		= SM.SITE_CD																		");
		oSqlBuilder.AppendLine("ORDER BY T1.RNUM																						");
		
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.SelfCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendManAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManLoginedMailSendSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.SITE_CD = C.SITE_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_SEQ = C.USER_SEQ",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = C.USER_CHAR_NO",ref pWhereClause);
		SysPrograms.SqlAppendWhere("C.PARTNER_USER_SEQ = :SELF_USER_SEQ",ref pWhereClause);
		SysPrograms.SqlAppendWhere("C.PARTNER_USER_CHAR_NO = :SELF_CHAR_NO",ref pWhereClause);
		SysPrograms.SqlAppendWhere("C.TOTAL_RX_MAIL_COUNT > 0",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_CHAR_NO",pCondition.SelfCharNo));

		SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
		oParamList.Add(new OracleParameter(":ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
		oParamList.Add(new OracleParameter(":ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		
		if (!string.IsNullOrEmpty(pCondition.NoTxMailDays)) {
			SysPrograms.SqlAppendWhere("LAST_RX_MAIL_DATE > SYSDATE - :NOT_TX_MAIL_DAYS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NOT_TX_MAIL_DAYS",pCondition.NoTxMailDays));
		}

		SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :REFUSE_CAST_USER_SEQ AND PARTNER_USER_CHAR_NO = :REFUSE_CAST_CHAR_NO)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":REFUSE_CAST_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":REFUSE_CAST_CHAR_NO",pCondition.SelfCharNo));

		return oParamList.ToArray();
	}

	private string CreateOrderExpression(ManLoginedMailSendSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case (PwViCommConst.ManLoginedMailSend.LAST_ACTION_DATE_DESC): {
					sSortExpression = "ORDER BY P.SITE_CD,P.LAST_ACTION_DATE DESC ,P.USER_SEQ";
					break;
				}
			case (PwViCommConst.ManLoginedMailSend.LAST_RX_MAIL_DATE_ASC): {
					sSortExpression = "ORDER BY P.SITE_CD,C.LAST_RX_MAIL_DATE ASC ,P.USER_SEQ";
					break;
				}
			case (PwViCommConst.ManLoginedMailSend.LAST_RX_MAIL_DATE_DESC): {
					sSortExpression = "ORDER BY P.SITE_CD,C.LAST_RX_MAIL_DATE DESC ,P.USER_SEQ";
					break;
				}
			default:
				sSortExpression = "ORDER BY P.SITE_CD,C.LAST_RX_MAIL_DATE DESC ,P.USER_SEQ";
				break;
		}
		return sSortExpression;
	}
}