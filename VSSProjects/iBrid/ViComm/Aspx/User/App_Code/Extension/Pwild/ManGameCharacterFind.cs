﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性ｹﾞｰﾑｷｬﾗｸﾀｰ検索
--	Progaram ID		: ManGameCharacterFind
--
--  Creation Date	: 2011.10.4
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[Serializable]
public class ManGameCharacterFindSeekCondition:SeekConditionBase {

	public List<string> attrTypeSeq;
	public List<string> attrValue;
	public List<string> groupingFlag;
	public List<string> likeSearchFlag;
	public List<string> notEqualFlag;

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string ExtRelation {
		get {
			return this.Query["ext_relation"];
		}
		set {
			this.Query["ext_relation"] = value;
		}
	}

	public string RelationFrom {
		get {
			return this.Query["relation_from"];
		}
		set {
			this.Query["relation_from"] = value;
		}
	}
	
	public string Relation {
		get {
			return this.Query["relation"];
		}
		set {
			this.Query["relation"] = value;
		}
	}

	public string ExtStartLevel {
		get {
			return this.Query["ext_start_level"];
		}
		set {
			this.Query["ext_start_level"] = value;
		}
	}

	public string StartLevel {
		get {
			return this.Query["start_level"];
		}
		set {
			this.Query["start_level"] = value;
		}
	}

	public string ExtEndLevel {
		get {
			return this.Query["ext_end_level"];
		}
		set {
			this.Query["ext_end_level"] = value;
		}
	}

	public string EndLevel {
		get {
			return this.Query["end_level"];
		}
		set {
			this.Query["end_level"] = value;
		}
	}

	public string OnlineStatus {
		get {
			return this.Query["online"];
		}
		set {
			this.Query["online"] = value;
		}
	}

	public string Handle {
		get {
			return this.Query["handle"];
		}
		set {
			this.Query["handle"] = value;
		}
	}

	public string Agefrom {
		get {
			return this.Query["agefrom"];
		}
		set {
			this.Query["agefrom"] = value;
		}
	}

	public string Ageto {
		get {
			return this.Query["ageto"];
		}
		set {
			this.Query["ageto"] = value;
		}
	}

	public string NewManDay {
		get {
			return this.Query["newmanday"];
		}
		set {
			this.Query["newmanday"] = value;
		}
	}
	
	public string BirthDayFrom {
		get {
			return this.Query["birthfrom"];
		}
		set {
			this.Query["birthfrom"] = value;
		}
	}

	public string BirthDayTo {
		get {
			return this.Query["birthto"];
		}
		set {
			this.Query["birthto"] = value;
		}
	}

	public string GameType {
		get {
			return this.Query["game_type"];
		}
		set {
			this.Query["game_type"] = value;
		}
	}

	public ManGameCharacterFindSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManGameCharacterFindSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {

		attrTypeSeq = new List<string>();
		attrValue = new List<string>();
		groupingFlag = new List<string>();
		likeSearchFlag = new List<string>();
		notEqualFlag = new List<string>();

		this.Query["ext_relation"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_relation"]));
		this.Query["relation_from"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["relation_from"]));
		this.Query["relation"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["relation"]));
		this.Query["ext_start_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_start_level"]));
		this.Query["start_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["start_level"]));
		this.Query["ext_end_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_end_level"]));
		this.Query["end_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["end_level"]));
		this.Query["online"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["online"]));
		this.Query["handle"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["handle"]));
		this.Query["agefrom"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["agefrom"]));
		this.Query["ageto"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ageto"]));
		this.Query["birthfrom"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["birthfrom"]));
		this.Query["birthto"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["birthto"]));
		this.Query["game_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_type"]));
		this.Query["newmanday"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["newmanday"]));

		for (int i = 0;i < ViCommConst.MAX_ATTR_COUNT;i++) {
			this.attrTypeSeq.Add(iBridUtil.GetStringValue(pQuery[string.Format("typeseq{0:D2}",i + 1)]));
			this.attrValue.Add(iBridUtil.GetStringValue(pQuery[string.Format("item{0:D2}",i + 1)]));
			this.groupingFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("group{0:D2}",i + 1)]));
			this.likeSearchFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("like{0:D2}",i + 1)]));
			this.notEqualFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("noteq{0:D2}",i + 1)]));

			this.Query[string.Format("typeseq{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("typeseq{0:D2}",i + 1)]));
			this.Query[string.Format("item{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("item{0:D2}",i + 1)]));
			this.Query[string.Format("group{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("group{0:D2}",i + 1)]));
			this.Query[string.Format("like{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("like{0:D2}",i + 1)]));
			this.Query[string.Format("noteq{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("noteq{0:D2}",i + 1)]));
		}
	}
}

[System.Serializable]
public class ManGameCharacterFind:ManBase {
	public ManGameCharacterFind()
		: base() {
	}
	public ManGameCharacterFind(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ManGameCharacterFindSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine(" 	COUNT(*)								");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	(SELECT									");
		oSqlBuilder.AppendLine("		P.SITE_CD						,	");
		oSqlBuilder.AppendLine("		P.USER_SEQ						,	");
		oSqlBuilder.AppendLine("		P.USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	FROM									");
		oSqlBuilder.AppendLine("		VW_PW_MAN_GAME_CHARACTER00 P		");

		for (int i = 0;i < pCondtion.attrValue.Count;i++) {
			if ((!pCondtion.attrValue[i].Equals("")) && (!pCondtion.attrValue[i].Equals("*"))) {
				oSqlBuilder.Append(string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1));
			}
		}

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
	
		oSqlBuilder.AppendLine(" 	) P,												");
		oSqlBuilder.AppendLine(" 	(SELECT												");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.SITE_CD					,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.USER_SEQ				,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.USER_CHAR_NO				");
		oSqlBuilder.AppendLine(" 	FROM												");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00								");

		oParamList.AddRange(this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend));
		oSqlBuilder.AppendLine(sWhereClauseFriend);

		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT					");
		oSqlBuilder.AppendLine(" WHERE									");
		oSqlBuilder.AppendLine("	" + CreateJoinField(pCondtion) + "	");
		
		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManGameCharacterFindSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	P.*											,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.RANK	AS FRIENDLY_RANK		,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.FRIENDLY_POINT				,	");
		oSqlBuilder.AppendLine("	GC.LAST_HUG_DATE								");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		" + ManBasicField() + "					,	");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM						,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE					,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL					,	");
		oSqlBuilder.AppendLine("		P.EXP									,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_REGIST_DATE			,	");
		oSqlBuilder.AppendLine("		P.SITE_USE_STATUS							");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		VW_PW_MAN_GAME_CHARACTER00 P				");

		for (int i = 0;i < pCondtion.attrValue.Count;i++) {
			if ((!pCondtion.attrValue[i].Equals("")) && (!pCondtion.attrValue[i].Equals("*"))) {
				oSqlBuilder.Append(string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1));
			}
		}

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine(" 	) P,												");
		oSqlBuilder.AppendLine(" 	(SELECT												");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.SITE_CD					,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.USER_SEQ				,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.RANK					,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.FRIENDLY_POINT				");
		oSqlBuilder.AppendLine(" 	FROM												");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00								");

		// where		
		oParamList.AddRange(this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend));
		oSqlBuilder.AppendLine(sWhereClauseFriend);

		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT								,	");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			SITE_CD									,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ						,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO					,	");
		oSqlBuilder.AppendLine("			LAST_HUG_DATE								");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION						");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :SELF_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	) GC												");
		oSqlBuilder.AppendLine(" WHERE													");
		oSqlBuilder.AppendLine("	" + CreateJoinField(pCondtion) + "	");

		oSqlBuilder.AppendLine("	AND	P.SITE_CD				= GC.SITE_CD				(+)	AND	");
		oSqlBuilder.AppendLine("		P.USER_SEQ				= GC.PARTNER_USER_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("		P.USER_CHAR_NO			= GC.PARTNER_USER_CHAR_NO	(+)		");

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.UserCharNo));

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);
		
		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManGameCharacterFindSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.OnlineStatus)) {
			switch (int.Parse(pCondition.OnlineStatus)) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhereClause);
					oParamList.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					oParamList.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) ",ref pWhereClause);
					oParamList.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
					oParamList.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					oParamList.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					break;
			}
		
		}
		
		if (!string.IsNullOrEmpty(pCondition.NewManDay)) {
			DateTime Dt = DateTime.Today.AddDays(int.Parse(pCondition.NewManDay) * -1);
			SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE	",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REGIST_DATE",OracleDbType.Date,Dt,ParameterDirection.Input));
		}
		
		if (!string.IsNullOrEmpty(pCondition.ExtStartLevel) || !string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
			string sStartLevel = null;
			string sEndLevel = null;

			if (!string.IsNullOrEmpty(pCondition.ExtStartLevel) && !string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
				sStartLevel = pCondition.ExtStartLevel;
				sEndLevel = pCondition.ExtEndLevel;
			} else if (!string.IsNullOrEmpty(pCondition.ExtStartLevel) && string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
				sStartLevel = pCondition.ExtStartLevel;
				sEndLevel = "9999";
			} else if (string.IsNullOrEmpty(pCondition.ExtStartLevel) && !string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
				sStartLevel = "1";
				sEndLevel = pCondition.ExtEndLevel;
			}

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		>= :GAME_CHARACTER_LEVEL_START",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_START",sStartLevel));

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		<= :GAME_CHARACTER_LEVEL_END",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_END",sEndLevel));

		} else if (!string.IsNullOrEmpty(pCondition.StartLevel) || !string.IsNullOrEmpty(pCondition.EndLevel)) {
			string sStartLevel = null;
			string sEndLevel = null;

			if (!string.IsNullOrEmpty(pCondition.StartLevel) && !string.IsNullOrEmpty(pCondition.EndLevel)) {
				sStartLevel = pCondition.StartLevel;
				sEndLevel = pCondition.EndLevel;
			} else if (!string.IsNullOrEmpty(pCondition.StartLevel) && string.IsNullOrEmpty(pCondition.EndLevel)) {
				sStartLevel = pCondition.StartLevel;
				sEndLevel = "9999";
			} else if (string.IsNullOrEmpty(pCondition.StartLevel) && !string.IsNullOrEmpty(pCondition.EndLevel)) {
				sStartLevel = "1";
				sEndLevel = pCondition.EndLevel;
			}

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		>= :GAME_CHARACTER_LEVEL_START",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_START",sStartLevel));

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		<= :GAME_CHARACTER_LEVEL_END",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_END",sEndLevel));
		}

		if (!string.IsNullOrEmpty(pCondition.Handle)) {
			SysPrograms.SqlAppendWhere(" P.GAME_HANDLE_NM LIKE '%' || :GAME_HANDLE_NM || '%' ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_HANDLE_NM",pCondition.Handle));
		}

		if (!string.IsNullOrEmpty(pCondition.Agefrom)) {
			SysPrograms.SqlAppendWhere(" P.AGE >= :AGE_FROM",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AGE_FROM",pCondition.Agefrom));
		}

		if (!string.IsNullOrEmpty(pCondition.Ageto)) {
			SysPrograms.SqlAppendWhere(" P.AGE <= :AGE_TO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AGE_TO",pCondition.Ageto));

		}
		
		if (!string.IsNullOrEmpty(pCondition.BirthDayFrom)) {
			SysPrograms.SqlAppendWhere("P.BIRTHDAY_MMDD >= :BIRTHDAY_MMDD_FROM",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BIRTHDAY_MMDD_FROM",pCondition.BirthDayFrom));
		}

		if (!string.IsNullOrEmpty(pCondition.BirthDayTo)) {
			SysPrograms.SqlAppendWhere("P.BIRTHDAY_MMDD <= :BIRTHDAY_MMDD_TO ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BIRTHDAY_MMDD_TO",pCondition.BirthDayTo));
		}

		if (!string.IsNullOrEmpty(pCondition.GameType)) {
			SysPrograms.SqlAppendWhere("P.GAME_CHARACTER_TYPE = :GAME_CHARACTER_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_TYPE",pCondition.GameType));
		}

		SysPrograms.SqlAppendWhere(" P.STAFF_FLAG = :STAFF_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		SysPrograms.SqlAppendWhere(string.Format(" P.NA_FLAG IN({0},{1})",ViCommConst.NaFlag.OK,ViCommConst.NaFlag.NO_AUTH),ref pWhereClause);

		SysPrograms.SqlAppendWhere(" P.SITE_USE_STATUS != :SITE_USE_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_USE_STATUS",ViCommConst.SiteUseStatus.LIVE_CHAT_ONLY));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO)",ref pWhereClause);

		oParamList.AddRange(CreateAttrQuery(pCondition,ref pWhereClause));

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereFriend(ManGameCharacterFindSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.RelationFrom)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.RANK		>= :RANK_FROM",ref pWhereClause);
			oParamList.Add(new OracleParameter(":RANK_FROM",pCondition.RelationFrom));
		}
		
		if (!string.IsNullOrEmpty(pCondition.ExtRelation) || !string.IsNullOrEmpty(pCondition.Relation)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.RANK		<= :RANK",ref pWhereClause);
			if (!string.IsNullOrEmpty(pCondition.ExtRelation)) {
				oParamList.Add(new OracleParameter(":RANK",pCondition.ExtRelation));
			} else if (!string.IsNullOrEmpty(pCondition.Relation)) {
				oParamList.Add(new OracleParameter(":RANK",pCondition.Relation));
			}
		}

		return oParamList.ToArray();
	}
	
	protected string CreateJoinField(ManGameCharacterFindSeekCondition pCondition) {
		StringBuilder sSql = new StringBuilder();

		if (!string.IsNullOrEmpty(pCondition.ExtRelation) || !string.IsNullOrEmpty(pCondition.Relation)) {
			sSql.Append(" 	P.SITE_CD				= FRIENDLY_POINT.SITE_CD		AND	").AppendLine();
			sSql.Append(" 	P.USER_SEQ				= FRIENDLY_POINT.USER_SEQ		AND ").AppendLine();
			sSql.Append(" 	P.USER_CHAR_NO			= FRIENDLY_POINT.USER_CHAR_NO		").AppendLine();
		} else {
			sSql.Append(" 	P.SITE_CD				= FRIENDLY_POINT.SITE_CD		(+) AND	").AppendLine();
			sSql.Append(" 	P.USER_SEQ				= FRIENDLY_POINT.USER_SEQ		(+) AND ").AppendLine();
			sSql.Append(" 	P.USER_CHAR_NO			= FRIENDLY_POINT.USER_CHAR_NO	(+)		").AppendLine();
		}

		return sSql.ToString();
	}

	protected string CreateOrderExpresion(ManGameCharacterFindSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_NEW) {
			sSortExpression = " ORDER BY P.GAME_CHARACTER_REGIST_DATE DESC NULLS LAST";
		} else if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_FRIENDLY) {
			sSortExpression = " ORDER BY FRIENDLY_POINT.RANK, FRIENDLY_POINT.FRIENDLY_POINT DESC, P.USER_SEQ";
		} else if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_LEVEL) {
			sSortExpression = " ORDER BY P.GAME_CHARACTER_LEVEL DESC, P.EXP DESC, P.USER_SEQ";
		} else if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_LOGIN) {
			sSortExpression = " ORDER BY P.LAST_LOGIN_DATE DESC NULLS LAST";
		} else if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_RANDOM) {
			sSortExpression = " ORDER BY DBMS_RANDOM.RANDOM";
		} else {
			sSortExpression = " ORDER BY P.USER_SEQ";
		}

		return sSortExpression;
	}

	private OracleParameter[] CreateAttrQuery(ManGameCharacterFindSeekCondition pCondition,ref string pWhere) {
		string sQuery = string.Empty;
		ArrayList pList;
		pList = new ArrayList();

		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.SITE_CD			= P.SITE_CD					",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_SEQ			= P.USER_SEQ				",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO			",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ{0}	",i + 1),ref pWhere);
				pList.Add(new OracleParameter(string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						sQuery = " ( ";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									sQuery += " OR ";
								}
								sQuery += string.Format(" (AT{0}.MAN_ATTR_INPUT_VALUE	LIKE '%' || :MAN_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("MAN_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						sQuery += ")";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');

					if (sValues.Length > 1) {
						sQuery = string.Format(" AT{0}.MAN_ATTR_SEQ IN ( ",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0) {
								sQuery += " , ";
							}
							sQuery += string.Format(" :MAN_ATTR_SEQ{0}_{1}	",i + 1,j);
							pList.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						sQuery += " ) ";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					} else {
						if (pCondition.notEqualFlag.Count > i && pCondition.notEqualFlag[i].Equals("1")) {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_SEQ <> :MAN_ATTR_SEQ{0} ",i + 1),ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_SEQ = :MAN_ATTR_SEQ{0} ",i + 1),ref pWhere);
						}
						pList.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					SysPrograms.SqlAppendWhere(string.Format(" AT{0}.GROUPING_CD	= :GROUPING_CD{0}	",i + 1),ref pWhere);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}

		return (OracleParameter[])pList.ToArray(typeof(OracleParameter));
	}
}