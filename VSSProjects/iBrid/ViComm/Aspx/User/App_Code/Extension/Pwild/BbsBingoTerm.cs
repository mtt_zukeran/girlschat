﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: お宝deビンゴ開催設定
--	Progaram ID		: BbsBingoTerm
--  Creation Date	: 2013.10.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class BbsBingoTerm:DbSession {
	public BbsBingoTerm() {
	}

	public DataSet GetCurrent(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	TERM_SEQ,");
		oSqlBuilder.AppendLine("	START_DATE,");
		oSqlBuilder.AppendLine("	END_DATE,");
		oSqlBuilder.AppendLine("	JACKPOT,");
		oSqlBuilder.AppendLine("	ENTRY_COUNT,");
		oSqlBuilder.AppendLine("	LAST_PRIZE_DATE,");
		oSqlBuilder.AppendLine("	BINGO_BALL_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_BBS_BINGO_TERM");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	START_DATE <= SYSDATE AND");
		oSqlBuilder.AppendLine("	END_DATE >= SYSDATE");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetLatest(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		SITE_CD,");
		oSqlBuilder.AppendLine("		TERM_SEQ,");
		oSqlBuilder.AppendLine("		START_DATE,");
		oSqlBuilder.AppendLine("		END_DATE,");
		oSqlBuilder.AppendLine("		JACKPOT,");
		oSqlBuilder.AppendLine("		ENTRY_COUNT,");
		oSqlBuilder.AppendLine("		LAST_PRIZE_DATE,");
		oSqlBuilder.AppendLine("		BINGO_BALL_FLAG");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_BBS_BINGO_TERM");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("		START_DATE <= SYSDATE");
		oSqlBuilder.AppendLine("	ORDER BY");
		oSqlBuilder.AppendLine("		SITE_CD,");
		oSqlBuilder.AppendLine("		START_DATE DESC");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM = 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public string GetLatestTermSeq(string pSiteCd) {
		string sTermSeq = string.Empty;

		DataSet oDataSet = GetLatest(pSiteCd);

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sTermSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TERM_SEQ"]);
		}

		return sTermSeq;
	}

	public void GetBbsBingoCard(
		string pSiteCd,
		string pUserSeq,
		out int pCardNo,
		out string[] pCardMask,
		out string[] pHitMask,
		out string pResult
	) {
		pCardMask = new string[25];
		pHitMask = new string[25];

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_BBS_BINGO_CARD");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			db.ProcedureOutParm("pCARD_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pCARD_MASK",DbSession.DbType.VARCHAR2,25);
			db.ProcedureOutArrayParm("pHIT_MASK",DbSession.DbType.VARCHAR2,25);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pCardNo = db.GetIntValue("pCARD_NO");
			pResult = db.GetStringValue("pRESULT");

			if (pResult.Equals(PwViCommConst.GetBbsBingoCardResult.RESULT_OK)) {
				for (int i = 0;i < 25;i++) {
					pCardMask[i] = db.GetArryStringValue("pCARD_MASK",i);
					pHitMask[i] = db.GetArryStringValue("pHIT_MASK",i);
				}
			}
		}
	}

	public void GetBbsBingoNo(
		string pSiteCd,
		string pUserSeq,
		string pObjSeq,
		int pCheckOnly,
		out int pCompleteFlag,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_BBS_BINGO_NO");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.NUMBER,pObjSeq);
			db.ProcedureInParm("pCHECK_ONLY",DbSession.DbType.NUMBER,pCheckOnly);
			db.ProcedureOutParm("pCOMPLETE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pCompleteFlag = db.GetIntValue("pCOMPLETE_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void GetBbsBingoLog(
		string pSiteCd,
		string pUserSeq,
		string pObjSeq,
		out int pBingoNo,
		out int pNewNoFlag,
		out int pCardNoFlag,
		out int pCompleteFlag,
		out int pBingoBallFlag,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_BBS_BINGO_LOG");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.NUMBER,pObjSeq);
			db.ProcedureOutParm("pBINGO_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNEW_NO_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCARD_NO_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCOMPLETE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pBINGO_BALL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pBingoNo = db.GetIntValue("pBINGO_NO");
			pNewNoFlag = db.GetIntValue("pNEW_NO_FLAG");
			pCardNoFlag = db.GetIntValue("pCARD_NO_FLAG");
			pCompleteFlag = db.GetIntValue("pCOMPLETE_FLAG");
			pBingoBallFlag = db.GetIntValue("pBINGO_BALL_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public DataSet GetBbsBingoLogData(
		string pSiteCd,
		string pUserSeq,
		string pObjSeq
	) {
		int iBingoNo;
		int iNewNoFlag;
		int iCardNoFlag;
		int iCompleteFlag;
		int iBingoBallFlag;
		string sResult = string.Empty;

		GetBbsBingoLog(
			pSiteCd,
			pUserSeq,
			pObjSeq,
			out iBingoNo,
			out iNewNoFlag,
			out iCardNoFlag,
			out iCompleteFlag,
			out iBingoBallFlag,
			out sResult
		);

		DataSet oDataSet = new DataSet();

		DataTable dt = new DataTable();
		dt.Columns.Add("BINGO_NO",Type.GetType("System.String"));
		dt.Columns.Add("NEW_NO_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("CARD_NO_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("COMPLETE_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("BINGO_BALL_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("RESULT",Type.GetType("System.String"));
		oDataSet.Tables.Add(dt);

		DataRow dr = oDataSet.Tables[0].Rows.Add();
		dr["BINGO_NO"] = iBingoNo.ToString();
		dr["NEW_NO_FLAG"] = iNewNoFlag.ToString();
		dr["CARD_NO_FLAG"] = iCardNoFlag.ToString();
		dr["COMPLETE_FLAG"] = iCompleteFlag.ToString();
		dr["BINGO_BALL_FLAG"] = iBingoBallFlag.ToString();
		dr["RESULT"] = sResult;

		return oDataSet;
	}

	public void GetBbsBingoPrize(
		string pSiteCd,
		string pUserSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_BBS_BINGO_PRIZE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
