﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class PossessionGameItemSeekCondition : SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex"];
		}
		set {
			this.Query["sex"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public string GameItemCategoryType {
		get {
			return this.Query["item_category"];
		}
		set {
			this.Query["item_category"] = value;
		}
	}
	
	public string PresentFlag {
		get {
			return this.Query["present_flag"];
		}
		set {
			this.Query["present_flag"] = value;
		}
	}

	public string ItemSeq {
		get {
			return this.Query["item_seq"];
		}
		set {
			this.Query["item_seq"] = value;
		}
	}

	public string TrapFlg {
		get {
			return this.Query["trap_flg"];
		}
		set {
			this.Query["trap_flg"] = value;
		}
	}

	public PossessionGameItemSeekCondition()
		: this(new NameValueCollection()) {
	}

	public PossessionGameItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["sex"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex"]));
		this.Query["item_category"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_category"]));
		this.Query["present_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["present_flag"]));
		this.Query["item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_seq"]));
	}
}
#endregion ============================================================================================================

public class PossessionGameItem:DbSession {
	public PossessionGameItem(){}

	public DataSet GetPossessionItem(PossessionGameItemSeekCondition pCondition){
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT,			");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE		");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_GAME_ITEM01");
		
		// where
		string sWhereClause = String.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oWhereParams);

		if(ds.Tables[0].Rows.Count == 0){
			DataRow dr;
			dr = ds.Tables[0].NewRow();
			dr["POSSESSION_COUNT"] = "0";
			ds.Tables[0].Rows.Add(dr);
		}

		return ds;
	}

	public DataSet GetOneByCategoryType(
		string pSiteCd,
		string pSexCd,
		string pUserSeq,
		string pUserCharNo,
		string pGameItemCategoryType
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_GAME_ITEM01							");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND									");
		oSqlBuilder.AppendLine("	SEX_CD = :SEX_CD AND									");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND								");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND						");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE = :GAME_ITEM_CATEGORY_TYPE AND	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG = :PUBLISH_FLAG							");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE",pGameItemCategoryType));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPossessionItemData(PossessionGameItemSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ,				");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM				");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_GAME_ITEM01");

		// where		
		string sWhereClause = String.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oWhereParams);

		return oDataSet;
	}
	
	public int GetPageCount(PossessionGameItemSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_GAME_ITEM02 P	");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		
		OracleParameter[] oWhereParams = oParamList.ToArray();
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(PossessionGameItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	public DataSet GetPageCollectionBase(PossessionGameItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ					,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM					,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER					,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER					,	");
		oSqlBuilder.AppendLine("	PRESENT_FLAG					,	");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT				,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_CD				,	");
		oSqlBuilder.AppendLine("	PRICE							,	");
		oSqlBuilder.AppendLine("	PRICE AS FIXED_PRICE			,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_GROUP_TYPE	,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE			,	");
		oSqlBuilder.AppendLine("	DESCRIPTION						,	");
		oSqlBuilder.AppendLine("	0 AS SALE_FLAG						");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_GAME_ITEM02		");

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		string sSortExpression = string.Empty;
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		GameItemSeekCondition subCondition = new GameItemSeekCondition();
		subCondition.SiteCd = pCondition.SiteCd;
		subCondition.SexCd = pCondition.SexCd;
		
		using(GameItem oGameItem = new GameItem()) {
			oGameItem.AppendAttrGameItemSale(oDataSet,subCondition);
		}
		
		return oDataSet;
	}
	
	private OracleParameter[] CreateWhere(PossessionGameItemSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (pCondition.SeekMode == PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_PARTNER_LIST) {
			if (!String.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
				SysPrograms.SqlAppendWhere(" USER_SEQ = :PARTNER_USER_SEQ",ref pWhereClause);
				oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
			}

			if (!String.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
				SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhereClause);
				oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
			}
		} else {
			if (!String.IsNullOrEmpty(pCondition.UserSeq)) {
				SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref pWhereClause);
				oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
			}

			if (!String.IsNullOrEmpty(pCondition.UserCharNo)) {
				SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
				oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
			}
		}

		if (String.IsNullOrEmpty(pCondition.TrapFlg)) {
			pCondition.TrapFlg = ViCommConst.FLAG_OFF_STR;
		}
		
		if (!String.IsNullOrEmpty(pCondition.GameItemCategoryType)) {
			if (
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) ||
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) ||
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE) ||
				pCondition.TrapFlg.Equals(ViCommConst.FLAG_ON_STR)
			) {
				SysPrograms.SqlAppendWhere(" GAME_ITEM_CATEGORY_TYPE		= :GAME_ITEM_CATEGORY_TYPE",ref pWhereClause);
				oParamList.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE",pCondition.GameItemCategoryType));
			} else {
				SysPrograms.SqlAppendWhere(" GAME_ITEM_CATEGORY_TYPE IN (:RESTORE,:TRAP,:TOOL,:TICKET,:DOUBLE_TRAP,:ROSE)",ref pWhereClause);
				oParamList.Add(new OracleParameter(":RESTORE",PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE));
				oParamList.Add(new OracleParameter(":TRAP",PwViCommConst.GameItemCategory.ITEM_CATEGORY_TRAP));
				oParamList.Add(new OracleParameter(":TOOL",PwViCommConst.GameItemCategory.ITEM_CATEGOAY_TOOL));
				oParamList.Add(new OracleParameter(":TICKET",PwViCommConst.GameItemCategory.ITEM_CATEGORY_TICKET));
				oParamList.Add(new OracleParameter(":DOUBLE_TRAP",PwViCommConst.GameItemCategory.ITEM_CATEGORY_DOUBLE_TRAP));
				oParamList.Add(new OracleParameter(":ROSE",PwViCommConst.GameItemCategory.ITEM_CATEGORY_ROSE));
			}
		}
		
		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}
		
		if (!String.IsNullOrEmpty(pCondition.PresentFlag)) {
			SysPrograms.SqlAppendWhere(" PRESENT_FLAG = :PRESENT_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRESENT_FLAG",pCondition.PresentFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.ItemSeq)) {
			SysPrograms.SqlAppendWhere(" GAME_ITEM_SEQ	= :GAME_ITEM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pCondition.ItemSeq));
		}
		
		SysPrograms.SqlAppendWhere(" PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(PossessionGameItemSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_ATTACK): {
					sSortExpression = " ORDER BY ATTACK_POWER DESC, GAME_ITEM_SEQ ASC";
					break;
				}
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_DEFENCE): {
					sSortExpression = " ORDER BY DEFENCE_POWER DESC, GAME_ITEM_SEQ ASC";
					break;
				}
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_NEW): {
					sSortExpression = " ORDER BY PUBLISH_DATE DESC, GAME_ITEM_SEQ ASC";
					break;
				}
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH): {
					sSortExpression = " ORDER BY PRICE DESC , GAME_ITEM_SEQ ASC";
					break;
				}
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_LOW): {
					sSortExpression = " ORDER BY PRICE ASC , GAME_ITEM_SEQ ASC";
					break;
				}	
				
			default:
				sSortExpression = " ORDER BY GAME_ITEM_SEQ DESC ";
				break;
		}
		return sSortExpression;
	}

	public DataSet GetPossessionEquipItems(string sSiteCd,string sUserSeq,string sUserCharNo,string sPresentFlag,string sSort) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ					,	");
		oSqlBuilder.AppendLine("	SEX_CD							,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM					,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER					,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER					,	");
		oSqlBuilder.AppendLine("	ENDURANCE						,	");
		oSqlBuilder.AppendLine("	ENDURANCE_NM					,	");
		oSqlBuilder.AppendLine("	DESCRIPTION						,	");
		oSqlBuilder.AppendLine("	REMARKS							,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_CD				,	");
		oSqlBuilder.AppendLine("	PRICE							,	");
		oSqlBuilder.AppendLine("	PRESENT_FLAG					,	");
		oSqlBuilder.AppendLine("	PRESENT_GAME_ITEM_SEQ			,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE			,	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG					,	");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT					");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_GAME_ITEM02   		");

		SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		SysPrograms.SqlAppendWhere(" USER_SEQ	= :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		SysPrograms.SqlAppendWhere(" USER_CHAR_NO	= :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		SysPrograms.SqlAppendWhere(" PRESENT_FLAG	= :PRESENT_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":PRESENT_FLAG",sPresentFlag));
		SysPrograms.SqlAppendWhere(" GAME_ITEM_CATEGORY_GROUP_TYPE	= :GAME_ITEM_CATEGORY_GROUP_TYPE",ref sWhereClause);
		oParamList.Add(new OracleParameter(":GAME_ITEM_CATEGORY_GROUP_TYPE",PwViCommConst.GameItemCatregoryGroup.EQUIP));
		SysPrograms.SqlAppendWhere(" PUBLISH_FLAG	= :PUBLISH_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		string sSortExpression = string.Empty;

		if (sSort.Equals(PwViCommConst.GameItemSort.GAME_ITEM_SORT_ATTACK)) {
			sSortExpression = " ORDER BY ATTACK_POWER DESC";
		} else {
			sSortExpression = " ORDER BY DEFENCE_POWER DESC";
		}

		oSqlBuilder.AppendLine(sSortExpression);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	public string GetGameItemPossessionCount(PossessionGameItemSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT						");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_POSSESSION_GAME_ITEM					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	= :GAME_ITEM_SEQ		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pCondition.ItemSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			return oDataSet.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString();
		} else {
			return "0";
		}
	}
	
	public bool CheckExistPossessionItem(string sSiteCd,string sUserSeq,string sUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	1										");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_POSSESSION_GAME_ITEM					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			");

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
