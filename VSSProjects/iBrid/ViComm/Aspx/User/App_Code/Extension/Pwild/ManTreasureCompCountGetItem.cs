﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 月別男性用お宝ｺﾝﾌﾟｱｲﾃﾑ
--	Progaram ID		: ManTreasureCompCountGetItem
--
--  Creation Date	: 2011.08.22
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;

[Serializable]
public class ManTreasureCompCountGetItemSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public string DisplayMonth {
		get {
			return this.Query["display_month"];
		}
		set {
			this.Query["display_month"] = value;
		}
	}

	public ManTreasureCompCountGetItemSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTreasureCompCountGetItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["display_month"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["display_month"]));
	}
}

[System.Serializable]
public class ManTreasureCompCountGetItem:DbSession {
	public ManTreasureCompCountGetItem() {
	}

	public DataSet GetPageCollection(ManTreasureCompCountGetItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(ManTreasureCompCountGetItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondition);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ					,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM					,	");
		oSqlBuilder.AppendLine("	COMP_COUNT_GET_ITEM_SEQ			,	");
		oSqlBuilder.AppendLine("	COMPLETE_COUNT						");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	VW_PW_M_COMP_COUNT_GET_ITEM01		");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		AppendItemGetFlag(oDataSet,pCondition);
		AppendSelfCompleteCount(oDataSet,pCondition);

		return oDataSet;
	}

	private void AppendItemGetFlag(DataSet oDataSet,ManTreasureCompCountGetItemSeekCondition pCondition) {
		if(oDataSet.Tables[0].Rows.Count < 1) {
			return;
		}

		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		if (!string.IsNullOrEmpty(pCondition.DisplayMonth)) {
			SysPrograms.SqlAppendWhere("DISPLAY_MONTH = :DISPLAY_MONTH",ref sWhereClause);
			oParamList.Add(new OracleParameter(":DISPLAY_MONTH",pCondition.DisplayMonth));
		}

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	COMP_COUNT_GET_ITEM_SEQ			");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_MAN_COMP_COUNT_GET_ITEM_LOG	");
		oSqlBuilder.AppendLine(sWhereClause);
		
		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		DataColumn col = new DataColumn("ITEM_GET_FLAG",System.Type.GetType("System.String"));
		oDataSet.Tables[0].Columns.Add(col);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			dr["ITEM_GET_FLAG"] = "0";
		}

		if (subDS.Tables[0].Rows.Count > 0) {
			oDataSet.Tables[0].PrimaryKey = new DataColumn[] { oDataSet.Tables[0].Columns["COMP_COUNT_GET_ITEM_SEQ"] };

			foreach (DataRow subDR in subDS.Tables[0].Rows) {
				DataRow dr = oDataSet.Tables[0].Rows.Find(subDR["COMP_COUNT_GET_ITEM_SEQ"]);
				dr["ITEM_GET_FLAG"] = "1";
			}
		}
	}

	private void AppendSelfCompleteCount(DataSet oDataSet,ManTreasureCompCountGetItemSeekCondition pCondition) {
		if (oDataSet.Tables[0].Rows.Count < 1) {
			return;
		}

		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		if (!string.IsNullOrEmpty(pCondition.DisplayMonth) && !string.IsNullOrEmpty(pCondition.SiteCd)) {
			DateTime dtGameStartDate;

			using (SocialGame oSocialGame = new SocialGame()) {
				dtGameStartDate = oSocialGame.GetGameStartDate(pCondition.SiteCd);
			}

			int iYear = int.Parse(pCondition.DisplayMonth.Substring(0,4));
			int iMonth = int.Parse(pCondition.DisplayMonth.Substring(5,2));
			string sStartDisplayDay = string.Format("{0}/01",pCondition.DisplayMonth);
			string sEndDisplayDay = string.Format("{0}/{1:d2}",pCondition.DisplayMonth,DateTime.DaysInMonth(iYear,iMonth).ToString());

			if (DateTime.Parse(sStartDisplayDay) < dtGameStartDate) {
				sStartDisplayDay = dtGameStartDate.ToString("yyyy/MM/dd");
			}

			SysPrograms.SqlAppendWhere("DISPLAY_DAY >= :START_DISPLAY_DAY",ref sWhereClause);
			oParamList.Add(new OracleParameter(":START_DISPLAY_DAY",sStartDisplayDay));

			SysPrograms.SqlAppendWhere("DISPLAY_DAY <= :END_DISPLAY_DAY",ref sWhereClause);
			oParamList.Add(new OracleParameter(":END_DISPLAY_DAY",sEndDisplayDay));
		}

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_COMPLETE_LOG	");
		oSqlBuilder.AppendLine(sWhereClause);

		int iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		DataColumn col = new DataColumn("SELF_COMPLETE_COUNT",System.Type.GetType("System.String"));
		oDataSet.Tables[0].Columns.Add(col);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			dr["SELF_COMPLETE_COUNT"] = iRecCount.ToString();
		}
	}

	private OracleParameter[] CreateWhere(ManTreasureCompCountGetItemSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.DisplayMonth)) {
			SysPrograms.SqlAppendWhere("DISPLAY_MONTH = :DISPLAY_MONTH",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DISPLAY_MONTH",pCondition.DisplayMonth));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(ManTreasureCompCountGetItemSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = " ORDER BY COMPLETE_COUNT ASC ";
		return sSortExpression;
	}
}
