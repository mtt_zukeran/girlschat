﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 会員つぶやき
--	Progaram ID		: ManTweetEx
--  Creation Date	: 2015.01.22
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class ManTweetExSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["usercharno"];
		}
		set {
			this.Query["usercharno"] = value;
		}
	}

	public string ManTweetSeq {
		get {
			return this.Query["tweetseq"];
		}
		set {
			this.Query["tweetseq"] = value;
		}
	}

	public bool AppendLikeCount = false;
	public bool AppendCommentCount = false;

	public string ReadFlag {
		get {
			return this.Query["readflag"];
		}
		set {
			this.Query["readflag"] = value;
		}
	}

	public string CastUserSeq;
	public string CastCharNo;
	public bool AppendCastLikedFlag = false;
	public bool AppendCastReadFlag = false;

	public ManTweetExSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTweetExSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["usercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["usercharno"]));
		this.Query["tweetseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tweetseq"]));
		this.Query["readflag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["readflag"]));
	}
}

[Serializable]
public class ManTweetEx:DbSession {
	public ManTweetEx() {
	}

	public int GetPageCount(ManTweetExSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.SITE_CD			= T_USER_MAN_CHARACTER.SITE_CD		AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ		AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_CHAR_NO	= T_USER_MAN_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManTweetExSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.SITE_CD,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.USER_SEQ,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.MAN_TWEET_SEQ,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.TWEET_TEXT,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.TWEET_DATE,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.PIC_SEQ AS MAN_TWEET_PIC_SEQ,");
		oSqlBuilder.AppendLine("	GET_MAN_PHOTO_IMG_PATH(T_MAN_TWEET.SITE_CD,T_USER.LOGIN_ID,T_MAN_TWEET.PIC_SEQ,0) AS MAN_TWEET_IMG_PATH,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T_MAN_TWEET.SITE_CD,T_USER.LOGIN_ID,T_MAN_TWEET.PIC_SEQ,0) AS MAN_TWEET_SMALL_IMG_PATH,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.UNAUTH_PIC_FLAG AS MAN_TWEET_UNAUTH_PIC_FLAG,");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.HANDLE_NM,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.AGE,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.RICHINO_RANK,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T_MAN_TWEET.SITE_CD,T_USER.LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	CASE WHEN (T_USER_MAN_CHARACTER.REGIST_DATE >= TO_CHAR(SYSDATE - T_SITE_MANAGEMENT.MAN_NEW_FACE_DAYS,'YYYY/MM/DD')) THEN 1 ELSE 0 END AS NEW_MAN_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.SITE_CD			= T_USER_MAN_CHARACTER.SITE_CD		AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ		AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_CHAR_NO	= T_USER_MAN_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.USER_SEQ = T_USER.USER_SEQ)");
		oSqlBuilder.AppendLine("	INNER JOIN T_SITE_MANAGEMENT");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.SITE_CD = T_SITE_MANAGEMENT.SITE_CD)");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY T_MAN_TWEET.TWEET_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		if (pCondtion.AppendLikeCount) {
			AppendLikeCount(oDataSet);
		}

		if (pCondtion.AppendCommentCount) {
			AppendCommentCount(oDataSet);
		}

		if (pCondtion.AppendCastLikedFlag) {
			AppendCastLikedFlag(oDataSet,pCondtion);
		}

		if (pCondtion.AppendCastReadFlag) {
			AppendCastReadFlag(oDataSet,pCondtion);
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManTweetExSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("T_MAN_TWEET.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("T_MAN_TWEET.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("T_MAN_TWEET.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ManTweetSeq)) {
			SysPrograms.SqlAppendWhere("T_MAN_TWEET.MAN_TWEET_SEQ = :MAN_TWEET_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAN_TWEET_SEQ",pCondition.ManTweetSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq) && !string.IsNullOrEmpty(pCondition.CastCharNo)) {
			if (pCondition.ReadFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("EXISTS(SELECT 1 FROM T_MAN_TWEET_READ WHERE SITE_CD = T_MAN_TWEET.SITE_CD AND MAN_USER_SEQ = T_MAN_TWEET.USER_SEQ AND CAST_USER_SEQ = :READ_CAST_USER_SEQ AND CAST_CHAR_NO = :READ_CAST_CHAR_NO)",ref pWhereClause);
				oParamList.Add(new OracleParameter(":READ_CAST_USER_SEQ",pCondition.CastUserSeq));
				oParamList.Add(new OracleParameter(":READ_CAST_CHAR_NO",pCondition.CastCharNo));
			}

			SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = T_MAN_TWEET.SITE_CD AND USER_SEQ = T_MAN_TWEET.USER_SEQ AND USER_CHAR_NO = T_MAN_TWEET.USER_CHAR_NO AND PARTNER_USER_SEQ = :REFUSE_CAST_USER_SEQ AND PARTNER_USER_CHAR_NO = :REFUSE_CAST_CHAR_NO)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REFUSE_CAST_USER_SEQ",pCondition.CastUserSeq));
			oParamList.Add(new OracleParameter(":REFUSE_CAST_CHAR_NO",pCondition.CastCharNo));
		}

		SysPrograms.SqlAppendWhere("T_MAN_TWEET.ADMIN_DEL_FLAG = 0",ref pWhereClause);
		SysPrograms.SqlAppendWhere("T_MAN_TWEET.DEL_FLAG = 0",ref pWhereClause);

		SysPrograms.SqlAppendWhere("T_USER_MAN_CHARACTER.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	private void AppendLikeCount(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("LIKE_COUNT",Type.GetType("System.String"));

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			int iLikeCount = 0;
			iLikeCount = GetLikeCount(iBridUtil.GetStringValue(dr["SITE_CD"]),iBridUtil.GetStringValue(dr["MAN_TWEET_SEQ"]));
			dr["LIKE_COUNT"] = iLikeCount.ToString();
		}
	}

	private void AppendCommentCount(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("COMMENT_COUNT",Type.GetType("System.String"));

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			int iCommentCount = 0;
			iCommentCount = GetCommentCount(iBridUtil.GetStringValue(dr["SITE_CD"]),iBridUtil.GetStringValue(dr["MAN_TWEET_SEQ"]));
			dr["COMMENT_COUNT"] = iCommentCount.ToString();
		}
	}

	private void AppendCastLikedFlag(DataSet pDS,ManTweetExSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		if (string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		pDS.Tables[0].Columns.Add("SELF_MAN_TWEET_LIKED_FLAG",Type.GetType("System.String"));

		string sSiteCd = string.Empty;
		List<string> sManTweetSeqList = new List<string>();

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["SELF_MAN_TWEET_LIKED_FLAG"] = ViCommConst.FLAG_OFF_STR;
			sSiteCd = iBridUtil.GetStringValue(dr["SITE_CD"]);
			sManTweetSeqList.Add(iBridUtil.GetStringValue(dr["MAN_TWEET_SEQ"]));
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAN_TWEET_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_LIKE");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	USER_SEQ		= :CAST_USER_SEQ	AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :CAST_CHAR_NO		AND");
		oSqlBuilder.AppendFormat("	MAN_TWEET_SEQ IN ({0})",string.Join(",",sManTweetSeqList.ToArray())).AppendLine();

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["MAN_TWEET_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow dr = pDS.Tables[0].Rows.Find(subDR["MAN_TWEET_SEQ"]);
			dr["SELF_MAN_TWEET_LIKED_FLAG"] = ViCommConst.FLAG_ON_STR;
		}
	}

	private void AppendCastReadFlag(DataSet pDS,ManTweetExSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		if (string.IsNullOrEmpty(pCondition.CastUserSeq) || string.IsNullOrEmpty(pCondition.CastCharNo)) {
			return;
		}

		pDS.Tables[0].Columns.Add("READ_FLAG",Type.GetType("System.String"));

		if (pCondition.ReadFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			foreach (DataRow dr in pDS.Tables[0].Rows) {
				dr["READ_FLAG"] = ViCommConst.FLAG_ON_STR;
			}

			return;
		}

		string sSiteCd = string.Empty;
		List<string> sTweetManUserSeqList = new List<string>();

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["READ_FLAG"] = ViCommConst.FLAG_OFF_STR;
			sSiteCd = iBridUtil.GetStringValue(dr["SITE_CD"]);

			if (!sTweetManUserSeqList.Contains(iBridUtil.GetStringValue(dr["USER_SEQ"]))) {
				sTweetManUserSeqList.Add(iBridUtil.GetStringValue(dr["USER_SEQ"]));
			}
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_READ");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ	= :CAST_USER_SEQ	AND");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO	= :CAST_CHAR_NO		AND");
		oSqlBuilder.AppendFormat("	MAN_USER_SEQ IN ({0})",string.Join(",",sTweetManUserSeqList.ToArray())).AppendLine();

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		List<string> sReadManUserSeqList = new List<string>();

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			sReadManUserSeqList.Add(iBridUtil.GetStringValue(subDR["MAN_USER_SEQ"]));
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			if (sReadManUserSeqList.Contains(iBridUtil.GetStringValue(dr["USER_SEQ"]))) {
				dr["READ_FLAG"] = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	private int GetLikeCount(string pSiteCd,string pManTweetSeq) {
		int iRecCount = 0;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_LIKE");
		oSqlBuilder.AppendLine("	INNER JOIN T_CAST_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET_LIKE.SITE_CD = T_CAST_CHARACTER.SITE_CD	AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET_LIKE.USER_SEQ = T_CAST_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET_LIKE.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_LIKE.SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_LIKE.MAN_TWEET_SEQ = :MAN_TWEET_SEQ AND");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.NA_FLAG = 0");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_TWEET_SEQ",pManTweetSeq));

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iRecCount;
	}

	private int GetCommentCount(string pSiteCd,string pManTweetSeq) {
		int iRecCount = 0;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT");
		oSqlBuilder.AppendLine("	INNER JOIN T_CAST_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET_COMMENT.SITE_CD = T_CAST_CHARACTER.SITE_CD	AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET_COMMENT.USER_SEQ = T_CAST_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET_COMMENT.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT.SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT.MAN_TWEET_SEQ = :MAN_TWEET_SEQ AND");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT.ADMIN_DEL_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT.DEL_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.NA_FLAG = 0");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_TWEET_SEQ",pManTweetSeq));

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iRecCount;
	}
}
