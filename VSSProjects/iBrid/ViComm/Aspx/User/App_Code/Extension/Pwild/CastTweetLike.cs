﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: アイドルテレビ・アイドルつぶやきイイネ
--	Progaram ID		: ManTweetLike
--
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class CastTweetLikeSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string CastTweetUserSeq;
	public string CastTweetUserCharNo;

	public string CastTweetSeq {
		get {
			return this.Query["tweetseq"];
		}
		set {
			this.Query["tweetseq"] = value;
		}
	}

	public CastTweetLikeSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastTweetLikeSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["tweetseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tweetseq"]));
	}
}

#endregion ============================================================================================================

public class CastTweetLike:ManBase {
	public CastTweetLike()
		: base() {
	}
	public CastTweetLike(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastTweetLikeSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_CAST_TWEET_LIKE00 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastTweetLikeSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "						,	");
		oSqlBuilder.AppendLine("	P.RICHINO_RANK									");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_CAST_TWEET_LIKE00 P							");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.CAST_TWEET_LIKE_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastTweetLikeSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastTweetSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_TWEET_SEQ		= :CAST_TWEET_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_TWEET_SEQ",pCondition.CastTweetSeq));
		}
		
		if (!string.IsNullOrEmpty(pCondition.CastTweetUserSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_TWEET_USER_SEQ		= :CAST_TWEET_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_TWEET_USER_SEQ",pCondition.CastTweetUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastTweetUserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.CAST_TWEET_USER_CHAR_NO		= :CAST_TWEET_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_TWEET_USER_CHAR_NO",pCondition.CastTweetUserCharNo));
		}
		
		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}
}