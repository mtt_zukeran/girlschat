﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 会員つぶやきコメントランキング
--	Progaram ID		: ManTweetCommentCnt
--
--  Creation Date	: 2013.03.27
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManTweetCommentCntSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string TermSeq {
		get {
			return this.Query["termseq"];
		}
		set {
			this.Query["termseq"] = value;
		}
	}

	public ManTweetCommentCntSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTweetCommentCntSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["termseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["termseq"]));
	}
}

public class ManTweetCommentCnt:DbSession {
	private const int DISP_RANK_TYPE_MANAGE = 0; // ランキング表示順位(管理画面)
	private const int DISP_RANK_TYPE_NOW = 1; // ランキング表示順位(現在ランキング)
	private const int DISP_RANK_TYPE_PAST = 2; // ランキング表示順位(過去ランキング)

	public ManTweetCommentCnt() {
	}

	public DataSet GetListParts(ManTweetCommentCntSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SUB.*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		DENSE_RANK() OVER(");
		oSqlBuilder.AppendLine("			PARTITION BY SITE_CD,TERM_SEQ");
		oSqlBuilder.AppendLine("			ORDER BY COMMENT_COUNT DESC,LAST_COMMENT_DATE ASC");
		oSqlBuilder.AppendLine("		) AS RANK_NO");
		oSqlBuilder.AppendLine("		,COMMENT_COUNT");
		oSqlBuilder.AppendLine("		,TERM_START_DATE");
		oSqlBuilder.AppendLine("		,TERM_END_DATE");
		oSqlBuilder.AppendLine("		,DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		VW_MAN_TWEET_COMMENT_CNT01");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) SUB");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RANK_NO IN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			DISP_RANK_NO");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_TWEET_COMMENT_DISP_RANK");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			DISP_RANK_SEQ=SUB.DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("			AND DISP_RANK_TYPE = :DISP_RANK_TYPE");
		oSqlBuilder.AppendLine("			AND SEX_CD=:DISP_SEX_CD");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	RANK_NO ASC");
		oParamList.Add(new OracleParameter(":DISP_RANK_TYPE",DISP_RANK_TYPE_NOW));
		oParamList.Add(new OracleParameter(":DISP_SEX_CD",ViCommConst.OPERATOR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetListReward(ManTweetCommentCntSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SUB.*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		DENSE_RANK() OVER(");
		oSqlBuilder.AppendLine("			PARTITION BY SITE_CD,TERM_SEQ");
		oSqlBuilder.AppendLine("			ORDER BY COMMENT_COUNT DESC,LAST_COMMENT_DATE ASC");
		oSqlBuilder.AppendLine("		) AS RANK_NO");
		oSqlBuilder.AppendLine("		,COMMENT_COUNT");
		oSqlBuilder.AppendLine("		,TERM_START_DATE");
		oSqlBuilder.AppendLine("		,TERM_END_DATE");
		oSqlBuilder.AppendLine("		,DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		VW_MAN_TWEET_COMMENT_CNT01");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) SUB");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RANK_NO IN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			DISP_RANK_NO");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_TWEET_COMMENT_DISP_RANK");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			DISP_RANK_SEQ=SUB.DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("			AND DISP_RANK_TYPE = :DISP_RANK_TYPE");
		oSqlBuilder.AppendLine("			AND SEX_CD=:DISP_SEX_CD");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	RANK_NO ASC");
		oParamList.Add(new OracleParameter(":DISP_RANK_TYPE",DISP_RANK_TYPE_PAST));
		oParamList.Add(new OracleParameter(":DISP_SEX_CD",ViCommConst.OPERATOR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManTweetCommentCntSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.TermSeq)) {
			SysPrograms.SqlAppendWhere("TERM_SEQ = :TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TERM_SEQ",pCondition.TermSeq));
		}

		return oParamList.ToArray();
	}
}
