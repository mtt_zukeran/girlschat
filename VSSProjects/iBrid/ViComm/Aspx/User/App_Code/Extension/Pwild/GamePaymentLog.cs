﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ ゲーム内獲得報酬

--	Progaram ID		: GamePaymentLog
--
--  Creation Date	: 2012.10.09
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class GamePaymentLogSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string SelfUserSeq {
		get {
			return this.Query["self_user_seq"];
		}
		set {
			this.Query["self_user_seq"] = value;
		}
	}

	public string SelfUserCharNo {
		get {
			return this.Query["self_user_char_no"];
		}
		set {
			this.Query["self_user_char_no"] = value;
		}
	}
	
	public string GamePaymentType {
		get {
			return this.Query["game_payment_type"];
		}
		set {
			this.Query["game_payment_type"] = value;
		}
	}

	public GamePaymentLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GamePaymentLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["self_user_seq_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_seq_seq"]));
		this.Query["self_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_char_no"]));
		this.Query["game_payment_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_payment_type"]));
	}
}

#endregion ============================================================================================================

public class GamePaymentLog:ManBase {
	public GamePaymentLog() {
	}

	public GamePaymentLog(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(GamePaymentLogSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine(" SELECT														");
		oSqlBuilder.AppendLine("	COUNT(*)												");
		oSqlBuilder.AppendLine(" FROM														");
		oSqlBuilder.AppendLine("	VW_PW_GAME_PAYMENT_LOG01 GPL						,	");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P							");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	GPL.SITE_CD					= P.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	GPL.PARTNER_USER_SEQ		= P.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	GPL.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	GPL.SITE_CD					= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	GPL.USER_SEQ				= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	GPL.USER_CHAR_NO			= :SELF_USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG				= :STAFF_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG					IN(:NA_FLAG,:NA_FLAG2)	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_REFUSE															");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																			");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		if (!string.IsNullOrEmpty(pCondtion.GamePaymentType)) {
			oSqlBuilder.AppendLine("	AND GPL.GAME_PAYMENT_TYPE = :GAME_PAYMENT_TYPE");
			oParamList.Add(new OracleParameter(":GAME_PAYMENT_TYPE",pCondtion.GamePaymentType));
		}

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GamePaymentLogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "				,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	GPL.GAME_PAYMENT_TYPE				,	");
		oSqlBuilder.AppendLine("	GPL.MOVIE_SEQ						,	");
		oSqlBuilder.AppendLine("	GPL.MOVIE_TITLE						,	");
		oSqlBuilder.AppendLine("	GPL.MOVIE_DOC						,	");
		oSqlBuilder.AppendLine("	GPL.CAST_GAME_PIC_SEQ				,	");
		oSqlBuilder.AppendLine("	GPL.PIC_TITLE						,	");
		oSqlBuilder.AppendLine("	GPL.PIC_DOC							,	");
		oSqlBuilder.AppendLine("	GPL.CREATE_DATE						,	");
		oSqlBuilder.AppendLine("	FP.FRIENDLY_RANK						");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_PW_GAME_PAYMENT_LOG01 GPL		,	");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P		,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			USER_SEQ										,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("			RANK AS FRIENDLY_RANK							,	");
		oSqlBuilder.AppendLine("			FRIENDLY_POINT										");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00									");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	) FP														");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	GPL.SITE_CD					= P.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	GPL.PARTNER_USER_SEQ		= P.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	GPL.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD					= FP.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ					= FP.USER_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO				= FP.USER_CHAR_NO		(+)	AND	");
		oSqlBuilder.AppendLine("	GPL.SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	GPL.USER_SEQ				= :SELF_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	GPL.USER_CHAR_NO			= :SELF_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG				= :STAFF_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG					IN(:NA_FLAG,:NA_FLAG2)		AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_REFUSE															");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		if (!string.IsNullOrEmpty(pCondtion.GamePaymentType)) {
			oSqlBuilder.AppendLine("	AND GPL.GAME_PAYMENT_TYPE = :GAME_PAYMENT_TYPE");
			oParamList.Add(new OracleParameter(":GAME_PAYMENT_TYPE",pCondtion.GamePaymentType));
		}

		string sSortExpression = " ORDER BY GPL.CREATE_DATE DESC, P.USER_SEQ ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}
}
