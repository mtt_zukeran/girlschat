﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class GameItemSaleSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SexCd;
	public string UserSeq;
	public string UserCharNo;
	public string StageLevel;
	public string NoticeDispFlag;

	public string GameItemCategoryType {
		get {
			return this.Query["item_category"];
		}
		set {
			this.Query["item_category"] = value;
		}
	}

	public string GameItemGetCd {
		get {
			return this.Query["get"];
		}
		set {
			this.Query["get"] = value;
		}
	}

	public string PresentFlag {
		get {
			return this.Query["present_flag"];
		}
		set {
			this.Query["present_flag"] = value;
		}
	}

	public string SaleStatus {
		get {
			return this.Query["sale_status"];
		}
		set {
			this.Query["sale_status"] = value;
		}
	}

	public string PublishFlag;
	public string DateNowFlag;

	public GameItemSaleSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameItemSaleSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["item_category"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_category"]));
		this.Query["get"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["get"]));
		this.Query["present_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["present_flag"]));
		this.Query["sale_status"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sale_status"]));
	}
}

#endregion ============================================================================================================

public class GameItemSale:DbSession {
	public GameItemSale() {
	}

	public int GetPageCount(GameItemSaleSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM_SALE01	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameItemSaleSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(GameItemSaleSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ														,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM														,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER														,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER														,	");
		oSqlBuilder.AppendLine("	ENDURANCE_NM														,	");
		oSqlBuilder.AppendLine("	DESCRIPTION															,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_CD													,	");
		oSqlBuilder.AppendLine("	FIXED_PRICE															,	");
		oSqlBuilder.AppendLine("	PRICE																,	");
		oSqlBuilder.AppendLine("	PRESENT_FLAG														,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE												,	");
		oSqlBuilder.AppendLine("	PUBLISH_DATE														,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION_COUNT,0) AS POSSESSION_COUNT							,	");
		oSqlBuilder.AppendLine("	0 AS REC_NO_PER_PAGE													");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			ITEM.SITE_CD												,	");
		oSqlBuilder.AppendLine("			ITEM.GAME_ITEM_SEQ											,	");
		oSqlBuilder.AppendLine("			ITEM.SEX_CD													,	");
		oSqlBuilder.AppendLine("			ITEM.GAME_ITEM_NM											,	");
		oSqlBuilder.AppendLine("			ITEM.ATTACK_POWER											,	");
		oSqlBuilder.AppendLine("			ITEM.DEFENCE_POWER											,	");
		oSqlBuilder.AppendLine("			ITEM.ENDURANCE_NM											,	");
		oSqlBuilder.AppendLine("			ITEM.STAGE_LEVEL											,	");
		oSqlBuilder.AppendLine("			ITEM.DESCRIPTION											,	");
		oSqlBuilder.AppendLine("			ITEM.GAME_ITEM_GET_CD										,	");
		oSqlBuilder.AppendLine("			ITEM.PRICE								AS FIXED_PRICE		,	");
		oSqlBuilder.AppendLine("			ITEM.DISCOUNTED_PRICE					AS PRICE			,	");
		oSqlBuilder.AppendLine("			ITEM.PRESENT_FLAG											,	");
		oSqlBuilder.AppendLine("			ITEM.PUBLISH_FLAG											,	");
		oSqlBuilder.AppendLine("			ITEM.GAME_ITEM_CATEGORY_TYPE								,	");
		oSqlBuilder.AppendLine("			ITEM.SALE_START_DATE										,	");
		oSqlBuilder.AppendLine("			ITEM.SALE_END_DATE											,	");
		oSqlBuilder.AppendLine("			ITEM.PUBLISH_DATE											,	");
		oSqlBuilder.AppendLine("			POSSESSION.POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			VW_PW_GAME_ITEM_SALE01 ITEM									,	");
		oSqlBuilder.AppendLine("			(																");
		oSqlBuilder.AppendLine("				SELECT														");
		oSqlBuilder.AppendLine("					SITE_CD												,	");
		oSqlBuilder.AppendLine("					GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("					POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("				FROM														");
		oSqlBuilder.AppendLine("					T_POSSESSION_GAME_ITEM									");
		oSqlBuilder.AppendLine("				WHERE														");
		oSqlBuilder.AppendLine("					SITE_CD				= :IN_SITE_CD					AND	");
		oSqlBuilder.AppendLine("					USER_SEQ			= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO		= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("			) POSSESSION													");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			ITEM.SITE_CD			= POSSESSION.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("			ITEM.GAME_ITEM_SEQ		= POSSESSION.GAME_ITEM_SEQ		(+)		");
		oSqlBuilder.AppendLine("	)																		");
		
		oParamList.Add(new OracleParameter(":IN_SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		// where		
		string sWhereClause = String.Empty;
		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		string sSortExpression = string.Empty;
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		int iRecNoPerPage = 0;
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow oDR in oDataSet.Tables[0].Rows) {
				iRecNoPerPage = (int.Parse(oDR["RNUM"].ToString()) - iStartIndex);
				oDR["REC_NO_PER_PAGE"] = iRecNoPerPage.ToString();
			}
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(GameItemSaleSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
			
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if(!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD	= :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!String.IsNullOrEmpty(pCondition.PresentFlag)) {
			SysPrograms.SqlAppendWhere(" PRESENT_FLAG	= :PRESENT_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRESENT_FLAG",pCondition.PresentFlag));
		}

		if (!String.IsNullOrEmpty(pCondition.GameItemGetCd)) {
			SysPrograms.SqlAppendWhere(" GAME_ITEM_GET_CD = :GAME_ITEM_GET_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_ITEM_GET_CD",pCondition.GameItemGetCd));
		}

		if (!String.IsNullOrEmpty(pCondition.StageLevel)) {
			SysPrograms.SqlAppendWhere(" STAGE_LEVEL	<= :STAGE_LEVEL",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_LEVEL",pCondition.StageLevel));
		}

		if (!String.IsNullOrEmpty(pCondition.GameItemCategoryType)) {
			if (
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) ||
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) ||
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
			) {
				SysPrograms.SqlAppendWhere(" GAME_ITEM_CATEGORY_TYPE		= :GAME_ITEM_CATEGORY_TYPE",ref pWhereClause);
				oParamList.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE",pCondition.GameItemCategoryType));
			} else {
				SysPrograms.SqlAppendWhere(" GAME_ITEM_CATEGORY_TYPE IN (:RESTORE,:TRAP,:DOUBLE_TRAP)",ref pWhereClause);
				oParamList.Add(new OracleParameter(":RESTORE",PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE));
				oParamList.Add(new OracleParameter(":TRAP",PwViCommConst.GameItemCategory.ITEM_CATEGORY_TRAP));
				oParamList.Add(new OracleParameter(":DOUBLE_TRAP",PwViCommConst.GameItemCategory.ITEM_CATEGORY_DOUBLE_TRAP));
			}
		}

		if (!String.IsNullOrEmpty(pCondition.DateNowFlag)) {
			SysPrograms.SqlAppendWhere(" :NOW_DATE	BETWEEN SALE_START_DATE AND SALE_END_DATE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NOW_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input));
		}

		if (!String.IsNullOrEmpty(pCondition.PublishFlag)) {
			SysPrograms.SqlAppendWhere(" PUBLISH_FLAG	= :PUBLISH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PUBLISH_FLAG",pCondition.PublishFlag));
		}

		if (!String.IsNullOrEmpty(pCondition.NoticeDispFlag)) {
			SysPrograms.SqlAppendWhere(" NOTICE_DISP_FLAG	= :NOTICE_DISP_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NOTICE_DISP_FLAG",pCondition.PublishFlag));
		}

		return oParamList.ToArray();
	}

	public int GetPageCountNotice(GameItemSaleSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {

		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	COUNT(*)															");
		oSqlBuilder.AppendLine("FROM(																	");
		oSqlBuilder.AppendLine(this.GetNoticeSelectField());
		oSqlBuilder.AppendLine(")																		");

		oParamList.Add(new OracleParameter(":IN_SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		// where
		string sWhereClause = string.Empty;
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		OracleParameter[] oWhereParams = oParamList.ToArray();

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollectionNotice(GameItemSaleSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	SITE_CD															,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ													,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM													,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER													,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER													,	");
		oSqlBuilder.AppendLine("	ENDURANCE_NM													,	");
		oSqlBuilder.AppendLine("	DESCRIPTION														,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_CD												,	");
		oSqlBuilder.AppendLine("	PRICE								AS FIXED_PRICE				,	");
		oSqlBuilder.AppendLine("	DISCOUNTED_PRICE					AS PRICE					,	");
		oSqlBuilder.AppendLine("	DISCOUNT_RATE													,	");
		oSqlBuilder.AppendLine("	PRESENT_FLAG													,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE											,	");
		oSqlBuilder.AppendLine("	NOTICE_DISP_FLAG												,	");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT													");
		oSqlBuilder.AppendLine("FROM(																	");
		oSqlBuilder.AppendLine(this.GetNoticeSelectField());
		oSqlBuilder.AppendLine(")																		");

		oParamList.Add(new OracleParameter(":IN_SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		string sSortExpression = string.Empty;
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
	
	private string GetNoticeSelectField() {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("	SELECT																");
		oSqlBuilder.AppendLine("		ITEM.SITE_CD												,	");
		oSqlBuilder.AppendLine("		ITEM.GAME_ITEM_SEQ											,	");
		oSqlBuilder.AppendLine("		ITEM.SEX_CD													,	");
		oSqlBuilder.AppendLine("		ITEM.GAME_ITEM_NM											,	");
		oSqlBuilder.AppendLine("		ITEM.ATTACK_POWER											,	");
		oSqlBuilder.AppendLine("		ITEM.DEFENCE_POWER											,	");
		oSqlBuilder.AppendLine("		ITEM.ENDURANCE_NM											,	");
		oSqlBuilder.AppendLine("		ITEM.DESCRIPTION											,	");
		oSqlBuilder.AppendLine("		ITEM.GAME_ITEM_GET_CD										,	");
		oSqlBuilder.AppendLine("		ITEM.PRICE													,	");
		oSqlBuilder.AppendLine("		ITEM.DISCOUNTED_PRICE										,	");
		oSqlBuilder.AppendLine("		ITEM.DISCOUNT_RATE											,	");
		oSqlBuilder.AppendLine("		ITEM.PRESENT_FLAG											,	");
		oSqlBuilder.AppendLine("		ITEM.GAME_ITEM_CATEGORY_TYPE								,	");
		oSqlBuilder.AppendLine("		ITEM.STAGE_LEVEL											,	");
		oSqlBuilder.AppendLine("		ITEM.PUBLISH_FLAG											,	");
		oSqlBuilder.AppendLine("		ITEM.NOTICE_DISP_FLAG										,	");
		oSqlBuilder.AppendLine("		ITEM.PUBLISH_DATE											,	");
		oSqlBuilder.AppendLine("		NVL(POSSESSION.POSSESSION_COUNT,0) AS POSSESSION_COUNT			");
		oSqlBuilder.AppendLine("	FROM																");
		oSqlBuilder.AppendLine("		VW_PW_GAME_ITEM_SALE01 ITEM									,	");
		oSqlBuilder.AppendLine("		(																");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				SITE_CD												,	");
		oSqlBuilder.AppendLine("				SEX_CD												,	");
		oSqlBuilder.AppendLine("				SALE_SCHEDULE_SEQ										");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_GAME_ITEM_SALE_SCHEDULE								");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD				= :IN_SITE_CD					AND	");
		oSqlBuilder.AppendLine("				SEX_CD				= :SEX_CD						AND	");
		oSqlBuilder.AppendLine("				SALE_START_DATE		> SYSDATE						AND	");
		oSqlBuilder.AppendLine("				ROWNUM				= 1									");
		oSqlBuilder.AppendLine("		) NOTICE													,	");
		oSqlBuilder.AppendLine("		(																");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				SITE_CD												,	");
		oSqlBuilder.AppendLine("				GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("				POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_POSSESSION_GAME_ITEM									");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD				= :IN_SITE_CD					AND	");
		oSqlBuilder.AppendLine("				USER_SEQ			= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("				USER_CHAR_NO		= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("		) POSSESSION													");
		oSqlBuilder.AppendLine("	WHERE																");
		oSqlBuilder.AppendLine("		ITEM.SITE_CD				= NOTICE.SITE_CD				AND	");
		oSqlBuilder.AppendLine("		ITEM.SALE_SCHEDULE_SEQ		= NOTICE.SALE_SCHEDULE_SEQ		AND	");
		oSqlBuilder.AppendLine("		ITEM.SITE_CD				= POSSESSION.SITE_CD		(+)	AND	");
		oSqlBuilder.Append("		ITEM.GAME_ITEM_SEQ			= POSSESSION.GAME_ITEM_SEQ	(+)		");
		
		return oSqlBuilder.ToString();
	}

	protected string CreateOrderExpresion(GameItemSaleSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.GameItemSort.GAME_ITEM_SORT_NEW:
				sSortExpression = " ORDER BY PUBLISH_DATE DESC";
				break;
			case PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH:
				sSortExpression = " ORDER BY PRICE DESC , GAME_ITEM_SEQ ASC";
				break;
			case PwViCommConst.GameItemSort.GAME_ITEM_SORT_LOW:
				sSortExpression = " ORDER BY PRICE ASC , GAME_ITEM_SEQ ASC";
				break;
			default:
				sSortExpression = " ORDER BY GAME_ITEM_SEQ DESC ";
				break;
		}

		return sSortExpression;
	}
	
	public DataSet GetSaleInfo(GameItemSaleSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SALE_COMMENT					,	");
		oSqlBuilder.AppendLine("	SALE_NOTICE_COMMENT				,	");
		oSqlBuilder.AppendLine("	SALE_START_DATE					,	");
		oSqlBuilder.AppendLine("	SALE_END_DATE						");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_SALE_SCHEDULE			");

		//Where
		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		
		if(pCondition.SaleStatus.Equals(PwViCommConst.GameSaleState.SOON)) {
			SysPrograms.SqlAppendWhere("SYSDATE < SALE_START_DATE",ref sWhereClause);
			SysPrograms.SqlAppendWhere("ROWNUM = 1",ref sWhereClause);
		}
		else {
			SysPrograms.SqlAppendWhere("SYSDATE BETWEEN SALE_START_DATE AND SALE_END_DATE",ref sWhereClause);
		}
		
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY SALE_START_DATE ASC");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public DataSet GetCurrentSaleStatus(string sSiteCd,string sSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CASE");
		oSqlBuilder.AppendLine("		WHEN SYSDATE BETWEEN SALE_START_DATE AND SALE_END_DATE THEN 1");
		oSqlBuilder.AppendLine("		ELSE 2");
		oSqlBuilder.AppendLine("	END GAME_ITEM_SALE_STATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_SALE_SCHEDULE");

		List<OracleParameter> oParamList = new List<OracleParameter>();

		string sWhereClause = string.Empty;
		if (!string.IsNullOrEmpty(sSiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		}

		if (!string.IsNullOrEmpty(sSexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",sSexCd));
		}

		SysPrograms.SqlAppendWhere(" SALE_END_DATE >= SYSDATE",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" ROWNUM = 1",ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);
		
		oSqlBuilder.AppendLine("ORDER BY SALE_END_DATE ASC");

		string sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count <= 0) {
			DataRow dr = oDataSet.Tables[0].NewRow();
			dr["GAME_ITEM_SALE_STATE"] = PwViCommConst.GameSaleState.CLOSE;
			oDataSet.Tables[0].Rows.Add(dr);
		}

		return oDataSet;
	}
}
