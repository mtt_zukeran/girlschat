﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性ｹﾞｰﾑｷｬﾗｸﾀｰ仲間
--	Progaram ID		: ManGameCharacterFellow
--
--  Creation Date	: 2011.09.28
--  Creater			: PW A.Taba
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

[Serializable]
public class ManGameCharacterFellowSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string FriendApplicationStatus;
	public string PartyMemberFlag;

	public ManGameCharacterFellowSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManGameCharacterFellowSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_seq"]));
		this.Query["game_item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_item_seq"]));
	}
}

[System.Serializable]
public class ManGameCharacterFellow:ManBase {
	public ManGameCharacterFellow()
		: base() {
	}

	public ManGameCharacterFellow(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ManGameCharacterFellowSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHAR_FELLOW00 P	");

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManGameCharacterFellowSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause,ref sWhereClauseFriend));
		
		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	P.*												,	");
		oSqlBuilder.AppendLine("	AF.MAX_FORCE_COUNT AS MAX_ATTACK_FORCE_COUNT	,	");
		oSqlBuilder.AppendLine(" 	FP.RANK	AS FRIENDLY_RANK						,	");
		oSqlBuilder.AppendLine(" 	FP.FRIENDLY_POINT									");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	(SELECT								");
		oSqlBuilder.AppendLine("		" + ManBasicField() + "		,	");
		oSqlBuilder.AppendLine("		PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("		GAME_HANDLE_NM				,	");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_TYPE			,	");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_LEVEL		,	");
		oSqlBuilder.AppendLine("		LAST_HUG_DAY				,	");
		oSqlBuilder.AppendLine("		APPLICATION_DATE			,	");
		oSqlBuilder.AppendLine("		APPLICATION_PERMIT_DATE		,	");
		oSqlBuilder.AppendLine("		SITE_USE_STATUS					");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_MAN_GAME_CHAR_FELLOW00 P	");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" 	) P,								");
		oSqlBuilder.AppendLine("	T_ATTACK_FORCE AF				,	");
		oSqlBuilder.AppendLine(" 	(SELECT								");
		oSqlBuilder.AppendLine(" 		SITE_CD						,	");
		oSqlBuilder.AppendLine(" 		USER_SEQ					,	");
		oSqlBuilder.AppendLine(" 		USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine(" 		RANK						,	");
		oSqlBuilder.AppendLine(" 		FRIENDLY_POINT					");
		oSqlBuilder.AppendLine(" 	FROM								");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00	FP			");
		oSqlBuilder.AppendLine(sWhereClauseFriend);
		oSqlBuilder.AppendLine(" 	) FP								");
		oSqlBuilder.AppendLine(" WHERE									");
		oSqlBuilder.AppendLine("	P.SITE_CD				= AF.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	P.PARTNER_USER_SEQ		= AF.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	P.PARTNER_USER_CHAR_NO	= AF.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine(" 	P.SITE_CD				= FP.SITE_CD		(+) AND	");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_SEQ		= FP.USER_SEQ		(+) AND ");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_CHAR_NO	= FP.USER_CHAR_NO	(+)		");

		string sSortExpression = this.CreateOrderExpresion(pCondtion);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	public DataSet GetOneFellowApplication(ManGameCharacterFellowSeekCondition pCondtion) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();


		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.*									");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	(SELECT								");
		oSqlBuilder.AppendLine("		PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("		GAME_HANDLE_NM				,	");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_TYPE			,	");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_LEVEL		,	");
		oSqlBuilder.AppendLine("		LAST_HUG_DAY				,	");
		oSqlBuilder.AppendLine("		APPLICATION_DATE			,	");
		oSqlBuilder.AppendLine("		APPLICATION_PERMIT_DATE		,	");
		oSqlBuilder.AppendLine("		SITE_USE_STATUS					");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_MAN_GAME_CHAR_FELLOW00 P	");
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine(" 	) P									");
		oSqlBuilder.AppendLine("	WHERE ROWNUM <= 1	  				");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManGameCharacterFellowSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.FriendApplicationStatus)) {
			SysPrograms.SqlAppendWhere("FELLOW_APPLICATION_STATUS = :FELLOW_APPLICATION_STATUS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pCondition.FriendApplicationStatus));
		}

		if (!string.IsNullOrEmpty(pCondition.PartyMemberFlag)) {
			SysPrograms.SqlAppendWhere("PARTY_MEMBER_FLAG = :PARTY_MEMBER_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTY_MEMBER_FLAG",pCondition.PartyMemberFlag));
		}

		SysPrograms.SqlAppendWhere("NA_FLAG IN (:NA_FLAG1,:NA_FLAG2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG1",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhere(ManGameCharacterFellowSeekCondition pCondition,ref string pWhereClause,ref string pWhereClauseFriend) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref pWhereClause));
		
		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :USER_SEQ",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClauseFriend);
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(ManGameCharacterFellowSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.ManGameCharacterFellowSort.APPLICATION_DATE_DESC:
				sSortExpression = " ORDER BY P.APPLICATION_DATE DESC";
				break;
			case PwViCommConst.ManGameCharacterFellowSort.APPLICATION_PERMIT_DATE_DESC:
				sSortExpression = " ORDER BY P.APPLICATION_PERMIT_DATE DESC";
				break;
			case PwViCommConst.ManGameCharacterFellowSort.FRIENDLY_RANK_ASC:
				sSortExpression = " ORDER BY FRIENDLY_RANK, FP.FRIENDLY_POINT DESC";
				break;
			default:
				sSortExpression = " ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}

	public DataSet GetPartyMember(ManGameCharacterFellowSeekCondition pCondition) {
		DataSet oTempDataSet = this.GetPageCollection(pCondition,1,10);
		DataSet oDataSet = oTempDataSet.Clone();
		DataTable oNewTable = oDataSet.Tables[0];

		for (int iType = 1; iType <= 3; iType++) {
			DataRow[] copyRows = oTempDataSet.Tables[0].Select("GAME_CHARACTER_TYPE = " + iType.ToString());

			if (copyRows.Length > 0) {
				oNewTable.ImportRow(copyRows[0]);
			} else {
				DataRow oNewRow = oNewTable.NewRow();
				oNewRow["GAME_CHARACTER_TYPE"] = iType.ToString();
				oNewTable.Rows.Add(oNewRow);
			}
		}

		return oDataSet;
	}
}

