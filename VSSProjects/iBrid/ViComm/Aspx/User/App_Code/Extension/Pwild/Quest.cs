﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエストエントリー
--	Progaram ID		: QuestEmtryLog
--
--  Creation Date	: 2012.07.16
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class QuestSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string QuestSeq {
		get {
			return this.Query["quest_seq"];
		}
		set {
			this.Query["quest_seq"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public QuestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public QuestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_seq"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
	}
}

#endregion ============================================================================================================

public class Quest:DbSession {
	public Quest() {
	}

	public void QuestEntryExe(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		string pQuestSeq,
		string pQuestType,
		string pLevelQuestSeq,
		string pLittleQuestSeq,
		string pOtherQuestSeq,
		string pRetireFlag,
		ref string pEntryLogSeq,
		out string pEntryClearFlag,
		out string pLevelQuestClearFlag,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("QUEST_ENTRY");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,pQuestSeq);
			db.ProcedureInParm("pQUEST_TYPE",DbSession.DbType.VARCHAR2,pQuestType);
			db.ProcedureInParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2,pLevelQuestSeq);
			db.ProcedureInParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2,pLittleQuestSeq);
			db.ProcedureInParm("pOTHER_QUEST_SEQ",DbSession.DbType.VARCHAR2,pOtherQuestSeq);
			db.ProcedureInParm("pRETIRE_FLAG",DbSession.DbType.VARCHAR2,pRetireFlag);
			db.ProcedureBothParm("pENTRY_LOG_SEQ",DbSession.DbType.VARCHAR2,pEntryLogSeq);
			db.ProcedureOutParm("pENTRY_CLEAR_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLEVEL_QUEST_CLEAR_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pEntryLogSeq = db.GetStringValue("pENTRY_LOG_SEQ");
			pEntryClearFlag = db.GetStringValue("pENTRY_CLEAR_FLAG");
			pLevelQuestClearFlag = db.GetStringValue("pLEVEL_QUEST_CLEAR_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}		
	}
	
	public void CheckQuestClear(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pTrialCategoryNo,
		string pTrialCategoryDetailNo,
		out string pQuestClearFlag,
		out string pResult,
		string pSexCd
	) {
		pQuestClearFlag = ViCommConst.FLAG_OFF_STR;
		pResult = string.Empty;
/*
		string pQuestSeq = string.Empty;
		string pLevelQuestSeq = string.Empty;
		string pEntryLogSeq = string.Empty;
		string pEXQuestEntryClearFlag;
		string pLevelQuestClearFlag;
		pEXQuestEntryClearFlag = ViCommConst.FLAG_OFF_STR;
		
		using(DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_QUEST_CLEAR");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pTRIAL_CATEGORY_NO",DbSession.DbType.VARCHAR2,pTrialCategoryNo);
			db.ProcedureInParm("pTRIAL_CATEGORY_DETAIL_NO",DbSession.DbType.VARCHAR2,pTrialCategoryDetailNo);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF_STR);
			db.ProcedureOutParm("pQUEST_CLEAR_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLEVEL_QUEST_CLEAR_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			pQuestClearFlag = db.GetStringValue("pQUEST_CLEAR_FLAG");
			pLevelQuestClearFlag = db.GetStringValue("pLEVEL_QUEST_CLEAR_FLAG");
			pQuestSeq = db.GetStringValue("pQUEST_SEQ");
			pLevelQuestSeq = db.GetStringValue("pLEVEL_QUEST_SEQ");
			pResult = db.GetStringValue("pRESULT");
		}

		if (pResult.Equals(PwViCommConst.GameQuestCheckClearResult.RESULT_OK) &&
			pLevelQuestClearFlag.Equals(ViCommConst.FLAG_ON_STR)
		) {
			this.QuestEntryExe(
				pSiteCd,
				pUserSeq,
				pUserCharNo,
				pSexCd,
				pQuestSeq,
				PwViCommConst.GameQuestType.EX_QUEST,
				pLevelQuestSeq,
				string.Empty,
				string.Empty,
				ViCommConst.FLAG_OFF_STR,
				ref pEntryLogSeq,
				out pEXQuestEntryClearFlag,
				out pLevelQuestClearFlag,
				out pResult
			);
		}
*/
	}

	public DataSet GetPageCollection(QuestSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	P.QUEST_SEQ		");
		oSqlBuilder.AppendLine("FROM				");
		oSqlBuilder.AppendLine("	T_QUEST P		");

		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine(sWhereClause);
		
		string sSortExpression = " ORDER BY P.QUEST_SEQ DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		return oDataSet;
	}

	protected OracleParameter[] CreateWhere(QuestSeekCondition pCondition,ref string pWhereClause) {
		ArrayList list = new ArrayList();
		pWhereClause = pWhereClause ?? string.Empty;

		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
			list.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}
		
		if (!string.IsNullOrEmpty(pCondition.QuestSeq)) {
			SysPrograms.SqlAppendWhere(" P.QUEST_SEQ	= :QUEST_SEQ",ref pWhereClause);
			list.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD	= :SEX_CD",ref pWhereClause);
			list.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(" P.PUBLISH_FLAG	= :PUBLISH_FLAG",ref pWhereClause);
		list.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		SysPrograms.SqlAppendWhere(" SYSDATE	BETWEEN P.PUBLISH_START_DATE AND P.GET_REWARD_END_DATE",ref pWhereClause);

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
