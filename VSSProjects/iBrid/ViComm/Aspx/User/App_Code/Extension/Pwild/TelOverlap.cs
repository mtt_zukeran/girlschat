﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 電話番号重複
--	Progaram ID		: TelOverlap
--  Creation Date	: 2014.09.16
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class TelOverlap:DbSession {
	public TelOverlap() {
	}

	public bool IsExist(string pUserSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_TEL_OVERLAP");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ");

		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
