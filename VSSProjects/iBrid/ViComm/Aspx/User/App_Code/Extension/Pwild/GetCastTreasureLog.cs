﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・お宝獲得ログ
--	Progaram ID		: GetCastTreasureLog
--
--  Creation Date	: 2011.11.14
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class GetCastTreasureLog:DbSession {
	public GetCastTreasureLog() {
	}

	public DataSet GetOne(string pSiteCd,string pGetTreasureLogSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	SITE_CD					,							");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE		,							");
		oSqlBuilder.AppendLine("	GET_TREASURE_LOG_SEQ	,							");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ		,							");
		oSqlBuilder.AppendLine("	CAST_TREASURE_NM									");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	VW_PW_GET_CAST_TREASURE_LOG01						");
		oSqlBuilder.AppendLine(" WHERE													");
		oSqlBuilder.AppendLine("	SITE_CD				 = :SITE_CD AND					");
		oSqlBuilder.AppendLine("	GET_TREASURE_LOG_SEQ = :GET_TREASURE_LOG_SEQ		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":GET_TREASURE_LOG_SEQ",pGetTreasureLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	public void GetCastTreasureLogCreate(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pStageGroupType,
		string pCastTreasureSeq,
		out string pGetTreasureLogSeq,
		out string pStatus
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_CAST_TREASURE_LOG_CREATE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSTAGE_GROUP_TYPE",DbSession.DbType.VARCHAR2,pStageGroupType);
			db.ProcedureInParm("pCAST_TREASURE_SEQ",DbSession.DbType.VARCHAR2,pCastTreasureSeq);
			db.ProcedureOutParm("pGET_TREASURE_LOG_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pGetTreasureLogSeq = db.GetStringValue("pGET_TREASURE_LOG_SEQ");
			pStatus = db.GetStringValue("pSTATUS");
		}
	}
}
