﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者新着いいね通知
--	Progaram ID		: CastLikeNoticve
--
--  Creation Date	: 2016.10.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

[System.Serializable]
public class CastLikeNotice:DbSession {
	public CastLikeNotice() {
	}

	/// <summary>
	/// 出演者新着いいね通知 登録・解除
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pCastSeq"></param>
	/// <param name="pCastCharNo"></param>
	/// <param name="pContentsType"></param>
	/// <param name="pObjSeq"></param>
	/// <param name="pCastDiaryReportDay"></param>
	/// <param name="pCastDiarySubSeq"></param>
	/// <param name="pManSeq"></param>
	/// <param name="pRemoveFlag"></param>
	/// <param name="pLikeDate"></param>
	/// <returns></returns>
	public void CastLikeNoticeMainte(
		string pSiteCd,
		string pCastSeq,
		string pCastCharNo,
		string pContentsType,
		string pObjSeq,
		string pCastDiaryReportDay,
		string pCastDiarySubSeq,
		string pManSeq
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_LIKE_NOTICE_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_SEQ",DbSession.DbType.VARCHAR2,pCastSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pCONTENTS_TYPE",DbSession.DbType.VARCHAR2,pContentsType);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,pObjSeq);
			db.ProcedureInParm("pCAST_DIARY_REPORT_DAY",DbSession.DbType.VARCHAR2,pCastDiaryReportDay);
			db.ProcedureInParm("pCAST_DIARY_SUB_SEQ",DbSession.DbType.VARCHAR2,pCastDiarySubSeq);
			db.ProcedureInParm("pMAN_SEQ",DbSession.DbType.VARCHAR2,pManSeq);
			db.ProcedureInParm("pREMOVE_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pLIKE_DATE",DbSession.DbType.DATE,null);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
	
	/// <summary>
	/// 出演者新着いいね通知 確認
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pCastSeq"></param>
	/// <param name="pCastCharNo"></param>
	/// <param name="pContentsType"></param>
	/// <param name="pObjSeq"></param>
	/// <param name="pCastDiaryReportDay"></param>
	/// <param name="pCastDiarySubSeq"></param>
	/// <returns></returns>
	public void CastLikeNoticeConfirm(
		string pSiteCd,
		string pCastSeq,
		string pCastCharNo,
		string pContentsType,
		string pObjSeq,
		string pCastDiaryReportDay,
		string pCastDiarySubSeq
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_LIKE_NOTICE_CONFIRM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_SEQ",DbSession.DbType.VARCHAR2,pCastSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pCONTENTS_TYPE",DbSession.DbType.VARCHAR2,pContentsType);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,pObjSeq);
			db.ProcedureInParm("pCAST_DIARY_REPORT_DAY",DbSession.DbType.VARCHAR2,pCastDiaryReportDay);
			db.ProcedureInParm("pCAST_DIARY_SUB_SEQ",DbSession.DbType.VARCHAR2,pCastDiarySubSeq);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public int GetCastLikeNoticeUnconfirmedCount(string pSiteCd,string pCastSeq,string pCastCharNO) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	COUNT(*)																	");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	T_CAST_LIKE_NOTICE P														");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	P.SITE_CD			= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("	P.CAST_SEQ			= :CAST_SEQ											AND	");
		oSqlBuilder.AppendLine("	P.CAST_CHAR_NO		= :CAST_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("	P.CONFIRMED_FLAG	= :CONFIRMED_FLAG									AND	");
		oSqlBuilder.AppendLine("	P.NOT_DISPLAY_FLAG	= :NOT_DISPLAY_FLAG									AND	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		(																		");
		oSqlBuilder.AppendLine("			CONTENTS_TYPE	= :CONTENTS_TYPE_PIC							AND	");
		oSqlBuilder.AppendLine("			EXISTS																");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_CAST_PIC													");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= P.CAST_SEQ					AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= P.CAST_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("					PIC_SEQ					= P.OBJ_SEQ						AND	");
		oSqlBuilder.AppendLine("					PIC_TYPE				= :PIC_TYPE						AND	");
		oSqlBuilder.AppendLine("					OBJ_NA_FLAG				= :OBJ_NA_FLAG_PIC				AND	");
		oSqlBuilder.AppendLine("					OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPLOVE_FLAG_PIC		AND	");
		oSqlBuilder.AppendLine("					OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG_PIC			");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("		)																	OR	");
		oSqlBuilder.AppendLine("		(																		");
		oSqlBuilder.AppendLine("			CONTENTS_TYPE	= :CONTENTS_TYPE_MOVIE							AND	");
		oSqlBuilder.AppendLine("			EXISTS																");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_CAST_MOVIE												");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= P.CAST_SEQ					AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= P.CAST_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("					MOVIE_SEQ				= P.OBJ_SEQ						AND	");
		oSqlBuilder.AppendLine("					MOVIE_TYPE				= :MOVIE_TYPE					AND	");
		oSqlBuilder.AppendLine("					OBJ_NA_FLAG				= :OBJ_NA_FLAG_MOVIE			AND	");
		oSqlBuilder.AppendLine("					OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPLOVE_FLAG_MOVIE	AND	");
		oSqlBuilder.AppendLine("					OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG_MOVIE		");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("		)																		");
		oSqlBuilder.AppendLine("	)																			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_SEQ",pCastSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCastCharNO));
		oParamList.Add(new OracleParameter(":CONFIRMED_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":NOT_DISPLAY_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":CONTENTS_TYPE_PIC",PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_PIC));
		oParamList.Add(new OracleParameter(":PIC_TYPE",ViCommConst.ATTACHED_PROFILE));
		oParamList.Add(new OracleParameter(":OBJ_NA_FLAG_PIC",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPLOVE_FLAG_PIC",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG_PIC",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":CONTENTS_TYPE_MOVIE",PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_MOVIE));
		oParamList.Add(new OracleParameter(":MOVIE_TYPE",ViCommConst.ATTACHED_PROFILE));
		oParamList.Add(new OracleParameter(":OBJ_NA_FLAG_MOVIE",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPLOVE_FLAG_MOVIE",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG_MOVIE",ViCommConst.FLAG_OFF));

		int iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		
		return iRecCount;
	}
}