﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員
--	Progaram ID		: ManNeighborUser
--
--  Creation Date	: 2015.02.05
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class ManNeighborUserSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string PrefectureCd;
	public string LastLoginDate;

	public ManNeighborUserSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManNeighborUserSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

#endregion ============================================================================================================

[System.Serializable]
public class ManNeighborUser:DbSession {

	public decimal recCount;

	public ManNeighborUser()
		: base() {
	}

	public int GetPageCount(ManNeighborUserSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P		,	");
		if (!string.IsNullOrEmpty(pCondition.PrefectureCd)) {
			oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT		,	");
		}
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT SM			,	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT3	,	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT5	,	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT13		");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManNeighborUserSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,out sWhereClause));

		oSqlBuilder.AppendLine(" SELECT																");
		oSqlBuilder.AppendLine("	P.SITE_CD													,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ													,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO												,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID													,	");
		oSqlBuilder.AppendLine("	P.CHARACTER_ONLINE_STATUS									,	");
		oSqlBuilder.AppendLine("	P.HANDLE_NM													,	");
		oSqlBuilder.AppendLine("	P.AGE														,	");
		oSqlBuilder.AppendLine("	P.PIC_SEQ													,	");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,P.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH	,	");
		oSqlBuilder.AppendLine("	CASE															");
		oSqlBuilder.AppendLine("		WHEN P.CHARACTER_ONLINE_STATUS = 0 THEN						");
		oSqlBuilder.AppendLine("			SM.OFFLINE_GUIDANCE										");
		oSqlBuilder.AppendLine("		WHEN P.CHARACTER_ONLINE_STATUS = 1 THEN						");
		oSqlBuilder.AppendLine("			SM.LOGINED_GUIDANCE										");
		oSqlBuilder.AppendLine("		WHEN P.CHARACTER_ONLINE_STATUS = 2 THEN						");
		oSqlBuilder.AppendLine("			SM.WAITING_GUIDANCE										");
		oSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 3) THEN					");
		oSqlBuilder.AppendLine("			SM.TALKING_WSHOT_GUIDANCE								");
		oSqlBuilder.AppendLine("	END AS MOBILE_ONLINE_STATUS_NM								,	");
		oSqlBuilder.AppendLine("	CASE															");
		oSqlBuilder.AppendLine("		WHEN (P.REGIST_DATE >= TO_CHAR(SYSDATE - SM.MAN_NEW_FACE_DAYS,'YYYY/MM/DD')) THEN");
		oSqlBuilder.AppendLine("			1														");
		oSqlBuilder.AppendLine("		ELSE														");
		oSqlBuilder.AppendLine("			0														");
		oSqlBuilder.AppendLine("	END AS NEW_MAN_FLAG											,	");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			NVL(COUNT(USER_SEQ),0) AS CNT							");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_FAVORIT												");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_CHAR_NO					");
		oSqlBuilder.AppendLine("	) AS LIKE_ME_FLAG											,	");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			NVL(COUNT(USER_SEQ),0) AS CNT							");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_FAVORIT												");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :SELF_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :SELF_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= P.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO				");
		oSqlBuilder.AppendLine("	) AS FAVORIT_FLAG											,	");
		oSqlBuilder.AppendLine("	AT3.DISPLAY_VALUE AS MAN_ATTR_VALUE03						,	");
		oSqlBuilder.AppendLine("	AT5.DISPLAY_VALUE AS MAN_ATTR_VALUE05						,	");
		oSqlBuilder.AppendLine("	AT13.DISPLAY_VALUE AS MAN_ATTR_VALUE13							");
		oSqlBuilder.AppendLine(" FROM																");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P									,	");
		if (!string.IsNullOrEmpty(pCondition.PrefectureCd)) {
			oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT									,	");
		}
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT SM										,	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT3								,	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT5								,	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_ATTR_VALUE01 AT13									");

		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY P.SITE_CD,P.CHARACTER_ONLINE_STATUS DESC,P.LAST_ACTION_DATE DESC,P.USER_SEQ";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManNeighborUserSeekCondition pCondition,out string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		pWhereClause = string.Empty;

		if (!string.IsNullOrEmpty(pCondition.PrefectureCd)) {
			SysPrograms.SqlAppendWhere(" AT.SITE_CD			= P.SITE_CD",ref pWhereClause);
			SysPrograms.SqlAppendWhere(" AT.USER_SEQ		= P.USER_SEQ",ref pWhereClause);
			SysPrograms.SqlAppendWhere(" AT.USER_CHAR_NO	= P.USER_CHAR_NO",ref pWhereClause);
		}
		SysPrograms.SqlAppendWhere(" P.SITE_CD			= SM.SITE_CD",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" P.SITE_CD			= AT3.SITE_CD		(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ			= AT3.USER_SEQ		(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= AT3.USER_CHAR_NO	(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" AT3.ITEM_NO		= :ITEM_NO03			",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.SITE_CD			= AT5.SITE_CD		(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ			= AT5.USER_SEQ		(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= AT5.USER_CHAR_NO	(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" AT5.ITEM_NO		= :ITEM_NO05			",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.SITE_CD			= AT13.SITE_CD		(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ			= AT13.USER_SEQ		(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= AT13.USER_CHAR_NO	(+)	",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" AT13.ITEM_NO		= :ITEM_NO13			",ref pWhereClause);
		oParamList.Add(new OracleParameter(":ITEM_NO03","03"));
		oParamList.Add(new OracleParameter(":ITEM_NO05","05"));
		oParamList.Add(new OracleParameter(":ITEM_NO13","13"));

		SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		if (!string.IsNullOrEmpty(pCondition.PrefectureCd)) {
			SysPrograms.SqlAppendWhere(" AT.ITEM_NO	= :ITEM_NO01",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ITEM_NO01","01"));

			SysPrograms.SqlAppendWhere(" AT.ITEM_CD	= :ITEM_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ITEM_CD",pCondition.PrefectureCd));
		}

		SysPrograms.SqlAppendWhere(" P.LAST_LOGIN_DATE	>= :LAST_LOGIN_DATE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Parse(pCondition.LastLoginDate),ParameterDirection.Input));

		SysPrograms.SqlAppendWhere(" P.NA_FLAG	= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE P.SITE_CD = SITE_CD AND P.USER_SEQ = USER_SEQ AND P.USER_CHAR_NO = USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_CHAR_NO)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":SELF_CHAR_NO",pCondition.UserCharNo));

		return oParamList.ToArray();
	}
}
