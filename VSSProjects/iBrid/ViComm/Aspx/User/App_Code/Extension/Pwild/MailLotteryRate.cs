﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メールdeガチャ確率設定
--	Progaram ID		: MailLotteryRate
--
--  Creation Date	: 2015.01.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class MailLotteryRate:DbSession {
	public MailLotteryRate() {
	}

	public DataSet GetOne(string pSiteCd,string pMailLotteryRateSeq,string pSecondLotteryFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	MAIL_LOTTERY_RATE_SEQ									,	");
		oSqlBuilder.AppendLine("	MAIL_LOTTERY_RATE_NM									,	");
		oSqlBuilder.AppendLine("	REWARD_POINT												");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_MAIL_LOTTERY_RATE											");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	MAIL_LOTTERY_RATE_SEQ	= :MAIL_LOTTERY_RATE_SEQ		AND	");
		oSqlBuilder.AppendLine("	SECOND_LOTTERY_FLAG		= :SECOND_LOTTERY_FLAG				");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAIL_LOTTERY_RATE_SEQ",pMailLotteryRateSeq));
		oParamList.Add(new OracleParameter(":SECOND_LOTTERY_FLAG",pSecondLotteryFlag));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
