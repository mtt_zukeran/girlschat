﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 出演者待機スケジュール
--	Progaram ID		: WaitSchedule
--
--  Creation Date	: 2014.11.17
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class WaitScheduleSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["usercharno"];
		}
		set {
			this.Query["usercharno"] = value;
		}
	}

	public string WaitScheduleSeq {
		get {
			return this.Query["waitscheduleseq"];
		}
		set {
			this.Query["waitscheduleseq"] = value;
		}
	}
	
	public string WaitStartTime {
		get {
			return this.Query["waitstarttime"];
		}
		set {
			this.Query["waitstarttime"] = value;
		}
	}

	public string ExWaitingFlag {
		get {
			return this.Query["exwaitflag"];
		}
		set {
			this.Query["exwaitflag"] = value;
		}
	}
	
	public string MinWaitStartTime;
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public WaitScheduleSeekCondition()
		: this(new NameValueCollection()) {
	}

	public WaitScheduleSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["usercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["usercharno"]));
		this.Query["waitscheduleseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["waitscheduleseq"]));
		this.Query["waitstarttime"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["waitstarttime"]));
		this.Query["exwaitflag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["exwaitflag"]));
	}
}

#endregion ============================================================================================================

public class WaitSchedule:CastBase {
	public WaitSchedule()
		: base() {
	}
	public WaitSchedule(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(WaitScheduleSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_WAIT_SCHEDULE00 P		");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(WaitScheduleSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "					,	");
		oSqlBuilder.AppendLine("	P.WAIT_SCHEDULE_SEQ							,	");
		oSqlBuilder.AppendLine("	P.WAIT_START_TIME								");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_WAIT_SCHEDULE00 P							");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.WAIT_START_TIME ASC,P.USER_SEQ ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		this.AppendCastAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(WaitScheduleSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.WaitScheduleSeq)) {
			SysPrograms.SqlAppendWhere(" P.WAIT_SCHEDULE_SEQ		= :WAIT_SCHEDULE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":WAIT_SCHEDULE_SEQ",pCondition.WaitScheduleSeq));
		}
		
		if (!string.IsNullOrEmpty(pCondition.WaitStartTime)) {
			SysPrograms.SqlAppendWhere(" P.WAIT_START_TIME		>= :WAIT_START_TIME",ref pWhereClause);
			oParamList.Add(new OracleParameter(":WAIT_START_TIME",DateTime.Parse(pCondition.WaitStartTime)));
		}

		if (!string.IsNullOrEmpty(pCondition.MinWaitStartTime)) {
			SysPrograms.SqlAppendWhere(" P.WAIT_START_TIME		>= :MIN_WAIT_START_TIME",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MIN_WAIT_START_TIME",DateTime.Parse(pCondition.MinWaitStartTime)));
		}
		
		if (iBridUtil.GetStringValue(pCondition.ExWaitingFlag).Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" P.CHARACTER_ONLINE_STATUS IN(:CHARACTER_ONLINE_FLAG1,:CHARACTER_ONLINE_FLAG2)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CHARACTER_ONLINE_FLAG1",ViCommConst.USER_OFFLINE));
			oParamList.Add(new OracleParameter(":CHARACTER_ONLINE_FLAG2",ViCommConst.USER_LOGINED));
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND WAITING_VIEW_STATUS != 0)",ref pWhereClause);
		
		return oParamList.ToArray();
	}

	public void WaitScheduleMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pWaitScheduleSeq,DateTime pWaitStartTime,int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WAIT_SCHEDULE_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pWAIT_SCHEDULE_SEQ",DbSession.DbType.VARCHAR2,pWaitScheduleSeq);
			db.ProcedureInParm("pWAIT_START_TIME",DbSession.DbType.DATE,pWaitStartTime);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,pDelFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void DeleteOverWaitSchedule(string pSiteCd,string pUserSeq,string pUserCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_OVER_WAIT_SCHEDULE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}