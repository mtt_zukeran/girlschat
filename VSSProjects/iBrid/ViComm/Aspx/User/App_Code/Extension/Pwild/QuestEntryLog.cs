﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエストエントリー履歴
--	Progaram ID		: QuestEntryLog
--
--  Creation Date	: 2012.07.17
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class QuestEntryLogSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string QuestSeq {
		get {
			return this.Query["quest_seq"];
		}
		set {
			this.Query["quest_seq"] = value;
		}
	}

	public string QuestType {
		get {
			return this.Query["quest_type"];
		}
		set {
			this.Query["quest_type"] = value;
		}
	}
	
	public string GetRewardFlag {
		get {
			return this.Query["get_reward_flag"];
		}
		set {
			this.Query["get_reward_flag"] = value;
		}
	}
	
	public string QuestStatus {
		get {
			return this.Query["quest_status"];
		}
		set {
			this.Query["quest_status"] = value;
		}
	}
	
	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public QuestEntryLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public QuestEntryLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_seq"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["quest_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_type"]));
		this.Query["get_reward_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["get_reward_flag"]));
		this.Query["quest_status"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_status"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
	}
}

#endregion ============================================================================================================

public class QuestEntryLog:DbSession {
	public QuestEntryLog() {
	}

	public DataSet GetPageCollection(QuestEntryLogSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	QUEST_SEQ								,	");
		oSqlBuilder.AppendLine("	QUEST_TYPE								,	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ							,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ						,	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_SEQ							,	");
		oSqlBuilder.AppendLine("	QUEST_LEVEL								,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_NM							,	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_NM							,	");
		oSqlBuilder.AppendLine("	ENTRY_LOG_SEQ								");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	VW_PW_QUEST_REWARD_GET01					");

		SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD		",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" USER_SEQ		= :USER_SEQ		",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" USER_CHAR_NO	= :USER_CHAR_NO	",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" SEX_CD			= :SEX_CD		",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" SYSDATE		BETWEEN PUBLISH_START_DATE AND GET_REWARD_END_DATE",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" PUBLISH_FLAG	= :PUBLISH_FLAG",ref sWhereClause);

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = "ORDER BY QUEST_TYPE ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(QuestEntryLogSeekCondition pCondition,ref string pWhereClause) {
		ArrayList list = new ArrayList();
		pWhereClause = pWhereClause ?? string.Empty;

		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		if (!String.IsNullOrEmpty(pCondition.QuestSeq)) {
			SysPrograms.SqlAppendWhere(" QUEST_SEQ = :QUEST_SEQ",ref pWhereClause);
			list.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		}

		if (!String.IsNullOrEmpty(pCondition.GetRewardFlag)) {
			SysPrograms.SqlAppendWhere(" GET_REWARD_FLAG = :GET_REWARD_FLAG",ref pWhereClause);
			list.Add(new OracleParameter(":GET_REWARD_FLAG",pCondition.GetRewardFlag));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetPageCollectionEntryQuest(QuestEntryLogSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	QUEST_SEQ								,	");
		oSqlBuilder.AppendLine("	QUEST_TYPE								,	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ							,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ						,	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_SEQ							,	");
		oSqlBuilder.AppendLine("	QUEST_LEVEL								,	");
		oSqlBuilder.AppendLine("	QUEST_EX_REMARKS						,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_NM							,	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_NM							,	");
		oSqlBuilder.AppendLine("	ENTRY_LOG_SEQ							,	");
		oSqlBuilder.AppendLine("	CASE										");
		oSqlBuilder.AppendLine("		WHEN									");
		oSqlBuilder.AppendLine("			END_DATE <= PUBLISH_END_DATE		");
		oSqlBuilder.AppendLine("		THEN									");
		oSqlBuilder.AppendLine("			END_DATE							");
		oSqlBuilder.AppendLine("		ELSE									");
		oSqlBuilder.AppendLine("			PUBLISH_END_DATE					");
		oSqlBuilder.AppendLine("	END AS END_DATE								");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	VW_PW_QUEST_ENTRY_LOG01						");

		SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD		",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" USER_SEQ		= :USER_SEQ		",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" USER_CHAR_NO	= :USER_CHAR_NO	",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" SEX_CD			= :SEX_CD		",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" SYSDATE		BETWEEN PUBLISH_START_DATE AND PUBLISH_END_DATE",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" SYSDATE		<= END_DATE",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" PUBLISH_FLAG	= :PUBLISH_FLAG",ref sWhereClause);

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		oParamList.AddRange(this.CreateWhereForEntryQuest(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = "ORDER BY UPDATE_DATE ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}



	private OracleParameter[] CreateWhereForEntryQuest(QuestEntryLogSeekCondition pCondition,ref string pWhereClause) {
		ArrayList list = new ArrayList();
		pWhereClause = pWhereClause ?? string.Empty;

		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		if (!String.IsNullOrEmpty(pCondition.QuestSeq)) {
			SysPrograms.SqlAppendWhere(" QUEST_SEQ = :QUEST_SEQ",ref pWhereClause);
			list.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		}

		if (!String.IsNullOrEmpty(pCondition.QuestStatus)) {
			SysPrograms.SqlAppendWhere(" QUEST_STATUS = :QUEST_STATUS",ref pWhereClause);
			list.Add(new OracleParameter(":QUEST_STATUS",pCondition.QuestStatus));
		}

		if (!String.IsNullOrEmpty(pCondition.QuestType)) {
			SysPrograms.SqlAppendWhere(" QUEST_TYPE = :QUEST_TYPE",ref pWhereClause);
			list.Add(new OracleParameter(":QUEST_TYPE",pCondition.QuestType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
