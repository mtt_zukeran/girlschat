﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せ･参加者(男性)


--	Progaram ID		: InvestigateEntrant
--
--  Creation Date	: 2015.02.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class InvestigateEntrantSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	public string ExecutionDay {
		get {
			return this.Query["execution_day"];
		}
		set {
			this.Query["execution_day"] = value;
		}
	}
	public string CastUserSeq {
		get {
			return this.Query["cast_user_seq"];
		}
		set {
			this.Query["cast_user_seq"] = value;
		}
	}
	public string CastCharNo {
		get {
			return this.Query["cast_char_no"];
		}
		set {
			this.Query["cast_char_no"] = value;
		}
	}

	public InvestigateEntrantSeekCondition()
		: this(new NameValueCollection()) {
	}
	public InvestigateEntrantSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["execution_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["execution_day"]));
		this.Query["cast_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_user_seq"]));
		this.Query["cast_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_user_char_no"]));
	}
}
[System.Serializable]
public class InvestigateEntrant:DbSession {
	public InvestigateEntrant() {
	}

	public void InvestigateEntrantEnter(InvestigateEntrantSeekCondition pCondition,out string pErrorMask,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("INVESTIGATE_ENTRANT_ENTER");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pCondition.SiteCd);
			db.ProcedureInParm("pEXECUTION_DAY",DbSession.DbType.VARCHAR2,pCondition.ExecutionDay);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pCondition.UserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pCondition.UserCharNo);
			db.ProcedureOutParm("pERROR_MASK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pErrorMask = db.GetStringValue("pERROR_MASK");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void InvestigateEntrantCaught(InvestigateEntrantSeekCondition pCondition,out string pCompleteFlag,out string pErrorMask,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("INVESTIGATE_ENTRANT_CAUGHT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pCondition.SiteCd);
			db.ProcedureInParm("pEXECUTION_DAY",DbSession.DbType.VARCHAR2,pCondition.ExecutionDay);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pCondition.UserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pCondition.UserCharNo);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCondition.CastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCondition.CastCharNo);
			db.ProcedureOutParm("pCOMPLETE_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pERROR_MASK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pCompleteFlag = db.GetStringValue("pCOMPLETE_FLAG");
			pErrorMask = db.GetStringValue("pERROR_MASK");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void InvestigateEntrantAcquiredPoint(InvestigateEntrantSeekCondition pCondition,out string pErrorMask,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("INVESTIGATE_POINT_ACQUIRED");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pCondition.SiteCd);
			db.ProcedureInParm("pEXECUTION_DAY",DbSession.DbType.VARCHAR2,pCondition.ExecutionDay);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pCondition.UserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pCondition.UserCharNo);
			db.ProcedureOutParm("pERROR_MASK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pErrorMask = db.GetStringValue("pERROR_MASK");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public DataSet GetOne(InvestigateEntrantSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	CAUGHT_FLAG								,	");
		oSqlBuilder.AppendLine("	CAUGHT_TIME								,	");
		oSqlBuilder.AppendLine("	POINT_ACQUIRED_FLAG						,	");
		oSqlBuilder.AppendLine("	POINT_ACQUIRED_TIME							");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_ENTRANT						");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND ");
		oSqlBuilder.AppendLine("	EXECUTION_DAY	= :EXECUTION_DAY		AND ");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ				AND ");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO				");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":EXECUTION_DAY",pCondition.ExecutionDay));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public string GetGotAcquiredPoint(InvestigateEntrantSeekCondition pCondition) {
		string sValue = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	IE.ENTRY_LEVEL * IVT.POINT_PER_ENTRANT AS GOT_POINT			");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_ENTRANT IE								,	");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_SCHEDULE IVT									");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	IE.SITE_CD			= IVT.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	IE.EXECUTION_DAY	= IVT.EXECUTION_DAY					AND	");
		oSqlBuilder.AppendLine("	IE.SITE_CD			= :SITE_CD							AND ");
		oSqlBuilder.AppendLine("	IE.EXECUTION_DAY	= :EXECUTION_DAY					AND ");
		oSqlBuilder.AppendLine("	IE.USER_SEQ			= :USER_SEQ							AND ");
		oSqlBuilder.AppendLine("	IE.USER_CHAR_NO		= :USER_CHAR_NO							");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":EXECUTION_DAY",pCondition.ExecutionDay));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["GOT_POINT"]);
		}

		return sValue;
	}
}
