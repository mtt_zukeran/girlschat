﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 応援依頼
--	Progaram ID		: SupportRequest
--
--  Creation Date	: 2011.09.05
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class SupportRequestSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string BattleLogSeq {
		get {
			return this.Query["battle_log_seq"];
		}
		set {
			this.Query["battle_log_seq"] = value;
		}
	}

	public string FriendApplicationStatus {
		get {
			return this.Query["friend_application_status"];
		}
		set {
			this.Query["friend_application_status"] = value;
		}
	}

	public SupportRequestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public SupportRequestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["battle_log_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["battle_log_seq"]));
		this.Query["friend_application_status"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["friend_application_status"]));
	}
}

#endregion ============================================================================================================

public class SupportRequest:DbSession {
	public SupportRequest() {
	}

	public DataSet GetPageCollectionForMan(SupportRequestSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	SP.SITE_CD												,	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_SEQ										,	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("	SP.GAME_HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	SP.GAME_CHARACTER_LEVEL									,	");
		oSqlBuilder.AppendLine("	SP.GAME_CHARACTER_TYPE									,	");
		oSqlBuilder.AppendLine("	SP.HANDLE_NM											,	");
		oSqlBuilder.AppendLine("	SP.AGE													,	");
		oSqlBuilder.AppendLine("	SP.SMALL_PHOTO_IMG_PATH									,	");
		oSqlBuilder.AppendLine("	SP.MOBILE_ONLINE_STATUS_NM								,	");
		oSqlBuilder.AppendLine("	SP.ACT_CATEGORY_IDX										,	");
		oSqlBuilder.AppendLine("	SP.LOGIN_ID												,	");
		oSqlBuilder.AppendLine("	SP.CRYPT_VALUE											,	");
		oSqlBuilder.AppendLine("	FP.RANK								AS FRIENDLY_RANK		");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			TSR.SITE_CD										,	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ									,	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine("			FELLOW.PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("			FELLOW.PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_CHARACTER_LEVEL						,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("			FELLOW.HANDLE_NM								,	");
		oSqlBuilder.AppendLine("			FELLOW.AGE										,	");
		oSqlBuilder.AppendLine("			FELLOW.LAST_LOGIN_DATE							,	");
		oSqlBuilder.AppendLine("			FELLOW.SMALL_PHOTO_IMG_PATH						,	");
		oSqlBuilder.AppendLine("			CASE												");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 0			");
		oSqlBuilder.AppendLine("				THEN TSM.OFFLINE_GUIDANCE						");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 1			");
		oSqlBuilder.AppendLine("				THEN TSM.LOGINED_GUIDANCE						");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 2			");
		oSqlBuilder.AppendLine("				THEN TSM.WAITING_GUIDANCE						");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 3			");
		oSqlBuilder.AppendLine("				THEN TSM.TALKING_WSHOT_GUIDANCE					");
		oSqlBuilder.AppendLine("			END AS MOBILE_ONLINE_STATUS_NM					,	");
		oSqlBuilder.AppendLine("			FELLOW.ACT_CATEGORY_IDX							,	");
		oSqlBuilder.AppendLine("			FELLOW.LOGIN_ID									,	");
		oSqlBuilder.AppendLine("			FELLOW.CRYPT_VALUE									");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_SUPPORT_REQUEST TSR							,	");
		oSqlBuilder.AppendLine("			VW_PW_CAST_GAME_CHR_FLW_MBR02 FELLOW			,	");
		oSqlBuilder.AppendLine("			T_SITE_MANAGEMENT TSM								");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ							= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO						= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			TSR.BATTLE_LOG_SEQ						= :BATTLE_LOG_SEQ					AND	");
		oSqlBuilder.AppendLine("			FELLOW.FELLOW_APPLICATION_STATUS		= :FELLOW_APPLICATION_STATUS		AND	");
		oSqlBuilder.AppendLine("		   (FELLOW.NA_FLAG							IN (:NA_FLAG,:NA_FLAG_2))			AND	");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= FELLOW.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ							= FELLOW.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO						= FELLOW.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_SEQ					= FELLOW.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_CHAR_NO				= FELLOW.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			FELLOW.SITE_CD							= TSM.SITE_CD							");
		oSqlBuilder.AppendLine("	) SP																,	");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00 FP													");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	SP.SITE_CD						= FP.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	SP.USER_SEQ						= FP.USER_SEQ					(+)	AND	");
		oSqlBuilder.AppendLine("	SP.USER_CHAR_NO					= FP.USER_CHAR_NO				(+)	AND	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_SEQ				= FP.PARTNER_USER_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_CHAR_NO			= FP.PARTNER_USER_CHAR_NO		(+)		");

		oParamList.AddRange(this.CreateWhere(pCondition));

		sSortExpression = "ORDER BY SP.LAST_LOGIN_DATE DESC";		

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionForCast(SupportRequestSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	SP.SITE_CD												,	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_SEQ										,	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("	SP.GAME_HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	SP.GAME_CHARACTER_LEVEL									,	");
		oSqlBuilder.AppendLine("	SP.GAME_CHARACTER_TYPE									,	");
		oSqlBuilder.AppendLine("	SP.MOBILE_ONLINE_STATUS_NM								,	");
		oSqlBuilder.AppendLine("	SP.LOGIN_ID												,	");
		oSqlBuilder.AppendLine("	SP.SITE_USE_STATUS										,	");
		oSqlBuilder.AppendLine("	SP.SMALL_PHOTO_IMG_PATH									,	");
		oSqlBuilder.AppendLine("	SP.AGE													,	");
		oSqlBuilder.AppendLine("	FP.RANK								AS FRIENDLY_RANK		");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			TSR.SITE_CD										,	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ									,	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine("			FELLOW.PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("			FELLOW.PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_CHARACTER_LEVEL						,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("			FELLOW.LAST_LOGIN_DATE							,	");
		oSqlBuilder.AppendLine("			CASE												");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 0			");
		oSqlBuilder.AppendLine("				THEN TSM.OFFLINE_GUIDANCE						");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 1			");
		oSqlBuilder.AppendLine("				THEN TSM.LOGINED_GUIDANCE						");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 2			");
		oSqlBuilder.AppendLine("				THEN TSM.WAITING_GUIDANCE						");
		oSqlBuilder.AppendLine("				WHEN FELLOW.CHARACTER_ONLINE_STATUS = 3			");
		oSqlBuilder.AppendLine("				THEN TSM.TALKING_WSHOT_GUIDANCE					");
		oSqlBuilder.AppendLine("			END AS MOBILE_ONLINE_STATUS_NM					,	");
		oSqlBuilder.AppendLine("			FELLOW.LOGIN_ID									,	");
		oSqlBuilder.AppendLine("			FELLOW.SITE_USE_STATUS							,	");
		oSqlBuilder.AppendLine("			FELLOW.SMALL_PHOTO_IMG_PATH						,	");
		oSqlBuilder.AppendLine("			FELLOW.AGE											");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_SUPPORT_REQUEST TSR							,	");
		oSqlBuilder.AppendLine("			VW_PW_GAME_FELLOW01 FELLOW						,	");
		oSqlBuilder.AppendLine("			T_SITE_MANAGEMENT TSM								");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ							= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO						= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			TSR.BATTLE_LOG_SEQ						= :BATTLE_LOG_SEQ					AND	");
		oSqlBuilder.AppendLine("			FELLOW.FELLOW_APPLICATION_STATUS		= :FELLOW_APPLICATION_STATUS		AND	");
		oSqlBuilder.AppendLine("		   (FELLOW.NA_FLAG							IN (:NA_FLAG,:NA_FLAG_2))			AND	");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= FELLOW.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ							= FELLOW.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO						= FELLOW.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_SEQ					= FELLOW.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_CHAR_NO				= FELLOW.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			FELLOW.SITE_CD							= TSM.SITE_CD							");
		oSqlBuilder.AppendLine("	) SP															,	");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00 FP												");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	SP.SITE_CD					= FP.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_SEQ			= FP.USER_SEQ					(+)	AND	");
		oSqlBuilder.AppendLine("	SP.PARTNER_USER_CHAR_NO		= FP.USER_CHAR_NO				(+)	AND	");
		oSqlBuilder.AppendLine("	SP.USER_SEQ					= FP.PARTNER_USER_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	SP.USER_CHAR_NO				= FP.PARTNER_USER_CHAR_NO		(+)		");

		oParamList.AddRange(this.CreateWhere(pCondition));

		sSortExpression = "ORDER BY SP.LAST_LOGIN_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SupportRequestSeekCondition pCondition) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pCondition.FriendApplicationStatus));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		
		return oParamList.ToArray();
	}

	public void ExecuteSupportRequest(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,string pBattleLogSeq,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SUPPORT_REQUEST");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pBATTLE_LOG_SEQ",DbSession.DbType.VARCHAR2,pBattleLogSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public int GetRecCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iRecCount = 0;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	COUNT(*)													");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01 P									");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD						= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	SUPPORT_USER_SEQ			= :SUPPORT_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	SUPPORT_USER_CHAR_NO		= :SUPPORT_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	SUPPORT_END_FLAG			= 0							AND	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST_END_FLAG	= 0							AND	");
		oSqlBuilder.AppendLine("	CHECK_DATE					IS NULL						AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS													");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			*													");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_REFUSE											");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD				AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SUPPORT_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SUPPORT_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	)															");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SUPPORT_USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":SUPPORT_USER_CHAR_NO",pUserCharNo));

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iRecCount;
	}
	
	public void UpdateSupportRequestCheck(string pSiteCd,string pUserSeq,string pUserCharNo,string pBattleLogSeq,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_SUPPORT_REQUEST_CHECK");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pBATTLE_LOG_SEQ",DbSession.DbType.VARCHAR2,pBattleLogSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
}
