﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画閲覧履歴
--	Progaram ID		: CastMovieView
--  Creation Date	: 2014.01.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class CastMovieView:DbSession {
	public CastMovieView() {
	}

	public void RegistCastMovieView(
		string pSiteCd,
		string pManUserSeq,
		string pMovieSeq
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_CAST_MOVIE_VIEW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.NUMBER,pMovieSeq);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}
}
