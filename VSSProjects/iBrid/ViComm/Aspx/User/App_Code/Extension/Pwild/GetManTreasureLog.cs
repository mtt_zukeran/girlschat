﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・お宝獲得ログ
--	Progaram ID		: GetManTreasureLog
--
--  Creation Date	: 2011.09.08
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class GetManTreasureLog:DbSession {
	public GetManTreasureLog() {
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pGetTreasureLogSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ AS USER_SEQ,					");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO AS USER_CHAR_NO,				");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ,							");
		oSqlBuilder.AppendLine("	DISPLAY_DAY,								");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_NM,						");
		oSqlBuilder.AppendLine("	OBJ_PHOTO_IMG_PATH,							");
		oSqlBuilder.AppendLine("	CAST_HANDLE_NM,								");
		oSqlBuilder.AppendLine("	AGE,										");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH,								");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH						");
		oSqlBuilder.AppendLine(" FROM											");
		oSqlBuilder.AppendLine("	VW_PW_GET_MAN_TREASURE_LOG01				");
		oSqlBuilder.AppendLine(" WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND						");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND					");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND			");
		oSqlBuilder.AppendLine("	GET_TREASURE_LOG_SEQ = :GET_TREASURE_LOG_SEQ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":GET_TREASURE_LOG_SEQ",pGetTreasureLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public bool CheckRecent(string pSiteCd,string pUserSeq,string pUserCharNo,string pGetTreasureLogSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	GET_TREASURE_LOG_SEQ							");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_GET_MAN_TREASURE_LOG							");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND							");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND						");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND				");
		oSqlBuilder.AppendLine("	GET_TREASURE_LOG_SEQ = :GET_TREASURE_LOG_SEQ	");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":GET_TREASURE_LOG_SEQ",pGetTreasureLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		}

		return false;
	}

	public void GetManTreasureLogCreate(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pCastUserSeq,
		string pCastCharNo,
		string pCastGamePicSeq,
		out string pGetTreasureLogSeq,
		out string pStatus
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_MAN_TREASURE_LOG_CREATE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pCAST_GAME_PIC_SEQ",DbSession.DbType.VARCHAR2,pCastGamePicSeq);
			db.ProcedureOutParm("pGET_TREASURE_LOG_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		
			pGetTreasureLogSeq = db.GetStringValue("pGET_TREASURE_LOG_SEQ");
			pStatus = db.GetStringValue("pSTATUS");
		}
	}
	
	public DataSet GetOneForTutorial(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	*															");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			INNER.*											,	");
		oSqlBuilder.AppendLine("			ROWNUM AS RNUM										");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			(													");
		oSqlBuilder.AppendLine("				SELECT											");
		oSqlBuilder.AppendLine("					CAST_USER_SEQ AS USER_SEQ,					");
		oSqlBuilder.AppendLine("					CAST_CHAR_NO AS USER_CHAR_NO,				");
		oSqlBuilder.AppendLine("					CAST_GAME_PIC_SEQ,							");
		oSqlBuilder.AppendLine("					DISPLAY_DAY,								");
		oSqlBuilder.AppendLine("					CAST_GAME_PIC_ATTR_NM,						");
		oSqlBuilder.AppendLine("					OBJ_PHOTO_IMG_PATH,							");
		oSqlBuilder.AppendLine("					GET_TREASURE_LOG_SEQ						");
		oSqlBuilder.AppendLine("				FROM											");
		oSqlBuilder.AppendLine("					VW_PW_GET_MAN_TREASURE_LOG01				");
		oSqlBuilder.AppendLine("				WHERE											");
		oSqlBuilder.AppendLine("					SITE_CD = :SITE_CD AND						");
		oSqlBuilder.AppendLine("					USER_SEQ = :USER_SEQ AND					");
		oSqlBuilder.AppendLine("					USER_CHAR_NO = :USER_CHAR_NO				");
		oSqlBuilder.AppendLine("				ORDER BY UPDATE_DATE DESC						");
		oSqlBuilder.AppendLine("			) INNER												");
		oSqlBuilder.AppendLine("	)															");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	RNUM = 1													");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
