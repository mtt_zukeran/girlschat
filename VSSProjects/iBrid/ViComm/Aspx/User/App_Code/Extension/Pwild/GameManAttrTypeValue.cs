﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・男性会員属性区分
--	Progaram ID		: 
--
--  Creation Date	: 2012.12.13
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[System.Serializable]
public class GameManAttrTypeValue:DbSession {
	public GameManAttrTypeValue() {
	}
	
	public string GetMaxManAttrSeqByManAttrTypeSeq(string pSiteCd,string pManAttrTypeSeq) {
		string sValue = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	MAX(MAN_ATTR_SEQ) AS MAX_MAN_ATTR_SEQ			");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_MAN_ATTR_TYPE_VALUE							");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_ATTR_TYPE_SEQ",pManAttrTypeSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["MAX_MAN_ATTR_SEQ"].ToString();
		}
		
		return sValue;
	}
}
