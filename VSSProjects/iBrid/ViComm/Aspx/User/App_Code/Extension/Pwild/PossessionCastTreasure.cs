﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ 所持お宝(女性用)

--	Progaram ID		: PossessionCastTreasure
--
--  Creation Date	: 2011.09.22
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using Oracle.DataAccess.Client;
using ViComm.Extension.Pwild;

[Serializable]
public class PossessionCastTreasureSeekCondition:SeekConditionBase {
	public string SiteCd;
	
	public string UserSeq;
	public string UserCharNo;
	
	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public string CastTreasureSeq {
		get {
			return this.Query["cast_treasure_seq"];
		}
		set {
			this.Query["cast_treasure_seq"] = value;
		}
	}
	
	public string StageGroupType {
		get {
			return this.Query["stage_group_type"];
		}
		set {
			this.Query["stage_group_type"] = value;
		}
	}

	public string CanRobFlag;

	public PossessionCastTreasureSeekCondition()
		: this(new NameValueCollection()) {
	}

	public PossessionCastTreasureSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["cast_treasure_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_treasure_seq"]));
		this.Query["stage_group_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_group_type"]));
	}
}

[System.Serializable]
public class PossessionCastTreasure:DbSession {
	public PossessionCastTreasure() {
	}

	public DataSet GetPageCollection(PossessionCastTreasureSeekCondition pCondition) {
		return GetPageCollectionBase(pCondition);
	}

	public DataSet GetPageCollectionBase(PossessionCastTreasureSeekCondition pCondition) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_SEQ																	,	");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_NM																		,	");
		oSqlBuilder.AppendLine("	CASE																						");
		oSqlBuilder.AppendLine("		WHEN PCT.POSSESSION_COUNT > 1 THEN 1													");
		oSqlBuilder.AppendLine("		WHEN PCT.POSSESSION_COUNT = 1 AND NVL(CTCL.TREASURE_COMPLETE_FLAG,0) = 0 THEN 1			");
		oSqlBuilder.AppendLine("		ELSE 0																					");
		oSqlBuilder.AppendLine("	END AS CAN_ROB_FLAG																		,	");
		oSqlBuilder.AppendLine("	NVL(PCT.POSSESSION_FLAG,0) AS POSSESSION_FLAG												");
		oSqlBuilder.AppendLine("FROM																							");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE CT																		,	");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			SITE_CD																			,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ																,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT																,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE																,	");
		oSqlBuilder.AppendLine("			1 AS POSSESSION_FLAG																");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE															");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD													AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :PARTNER_USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :PARTNER_USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE												");
		oSqlBuilder.AppendLine("	) PCT																					,	");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			SITE_CD																			,	");
		oSqlBuilder.AppendLine("			1 AS TREASURE_COMPLETE_FLAG														,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE																	");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE_COMPLETE_LOG														");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD													AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :PARTNER_USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :PARTNER_USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE												");
		oSqlBuilder.AppendLine("	) CTCL																						");
		oSqlBuilder.AppendLine("WHERE																							");
		oSqlBuilder.AppendLine("	CT.SITE_CD				= PCT.SITE_CD												(+)	AND	");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_SEQ	= PCT.CAST_TREASURE_SEQ										(+)	AND	");
		oSqlBuilder.AppendLine("	CT.STAGE_GROUP_TYPE		= PCT.STAGE_GROUP_TYPE										(+)	AND	");
		oSqlBuilder.AppendLine("	CT.SITE_CD				= CTCL.SITE_CD												(+)	AND	");
		oSqlBuilder.AppendLine("	CT.STAGE_GROUP_TYPE		= CTCL.STAGE_GROUP_TYPE										(+)	AND	");
		oSqlBuilder.AppendLine("	CT.SITE_CD				= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("	CT.STAGE_GROUP_TYPE		= :STAGE_GROUP_TYPE													");
		oSqlBuilder.AppendLine("ORDER BY CT.CAST_TREASURE_SEQ ASC																");
		// where		

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":STAGE_GROUP_TYPE",pCondition.StageGroupType));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(PossessionCastTreasureSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ		= :PARTNER_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO		= :PARTNER_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}

		if (!String.IsNullOrEmpty(pCondition.CanRobFlag)) {
			SysPrograms.SqlAppendWhere(" CAN_ROB_COUNT		> 0",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.CastTreasureSeq)) {
			SysPrograms.SqlAppendWhere(" CAST_GAME_PIC_SEQ		= :CAST_GAME_PIC_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCondition.CastTreasureSeq));
		}

		return oParamList.ToArray();
	}

	public DataSet GetOne(PossessionCastTreasureSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ							");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	(											");
		oSqlBuilder.AppendLine("		SELECT									");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ					");
		oSqlBuilder.AppendLine("		FROM									");
		oSqlBuilder.AppendLine("			VW_PW_POSSESSION_CAST_TRSR01		");
		// where		
		SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		
		SysPrograms.SqlAppendWhere(" USER_SEQ		= :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));

		SysPrograms.SqlAppendWhere(" USER_CHAR_NO		= :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		
		SysPrograms.SqlAppendWhere(" TO_NUMBER(STAGE_GROUP_TYPE)	<= :STAGE_GROUP_TYPE",ref sWhereClause);
		oParamList.Add(new OracleParameter(":STAGE_GROUP_TYPE",pCondition.StageGroupType));

		SysPrograms.SqlAppendWhere(" CAN_ROB_COUNT		> 0",ref sWhereClause);

		if(!string.IsNullOrEmpty(pCondition.CastTreasureSeq)) {
			SysPrograms.SqlAppendWhere(" CAST_TREASURE_SEQ		= :CAST_TREASURE_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":CAST_TREASURE_SEQ",pCondition.CastTreasureSeq));
		}
		
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	)											");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	ROWNUM = 1									");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	protected string CreateOrderExpresion(PossessionCastTreasureSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = " ORDER BY UPDATE_DATE DESC";
		return sSortExpression;
	}

	private void AppendAttr(DataSet pDS,PossessionCastTreasureSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["CAST_TREASURE_SEQ"] };

		DataColumn col;
		col = new DataColumn(string.Format("POSSESSION_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder;
		string sExecSql;
		string sWhereClause;
		List<OracleParameter> oParamList;
		
		List<string> oTreasureSeqList = new List<string>();

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			oTreasureSeqList.Add(pDR["CAST_TREASURE_SEQ"].ToString());
			pDR["POSSESSION_COUNT"] = "0";
		}
		
		string sTreasureSeqCsv = string.Join(",",oTreasureSeqList.ToArray());

		oSqlBuilder = new StringBuilder();
		sWhereClause = string.Empty;
		oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ									,	");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_POSSESSION_CAST_TREASURE								");

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD	= :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ	= :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO	= :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		SysPrograms.SqlAppendWhere(string.Format("CAST_TREASURE_SEQ		IN ({0})",sTreasureSeqCsv),ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);
		
		sExecSql = oSqlBuilder.ToString();
		DataSet subDS = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow targetDR = pDS.Tables[0].Rows.Find(subDR["CAST_TREASURE_SEQ"]);
			targetDR["POSSESSION_COUNT"] = subDR["POSSESSION_COUNT"];
		}
	}
	
	public DataSet GetCastTreasureCompStatus(PossessionCastTreasureSeekCondition pCondition) {
		
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_SEQ								,	");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_NM									,	");
		oSqlBuilder.AppendLine("	CASE													");
		oSqlBuilder.AppendLine("		WHEN NVL(PCT.POSSESSION_COUNT,0) > 0 THEN 1			");
		oSqlBuilder.AppendLine("		ELSE 0												");
		oSqlBuilder.AppendLine("	END AS POSSESSION_FLAG								,	");
		oSqlBuilder.AppendLine("	RANK() OVER(ORDER BY CT.CAST_TREASURE_SEQ ASC) AS CAST_TREASURE_TYPE	");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE CT									,	");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			SITE_CD										,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ							,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT								");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE						");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	) PCT													");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	CT.SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	CT.STAGE_GROUP_TYPE		= :STAGE_GROUP_TYPE			AND	");
		oSqlBuilder.AppendLine("	CT.SITE_CD				= PCT.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_SEQ	= PCT.CAST_TREASURE_SEQ	(+)		");
		oSqlBuilder.AppendLine("ORDER BY CT.CAST_TREASURE_SEQ								");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":STAGE_GROUP_TYPE",pCondition.StageGroupType));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}
