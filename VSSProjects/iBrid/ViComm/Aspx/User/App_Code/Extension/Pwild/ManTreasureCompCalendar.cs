﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｺﾝﾌﾟｶﾚﾝﾀﾞｰ
--	Progaram ID		: ManTreasureCompCalendar
--
--  Creation Date	: 2011.08.22
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;

[Serializable]
public class ManTreasureCompCalendarSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public string StartDisplayDay {
		get {
			return this.Query["start_display_day"];
		}
		set {
			this.Query["start_display_day"] = value;
		}
	}

	public string EndDisplayDay {
		get {
			return this.Query["end_display_day"];
		}
		set {
			this.Query["end_display_day"] = value;
		}
	}

	public string RestAttrCount {
		get {
			return this.Query["rest_attr_count"];
		}
		set {
			this.Query["rest_attr_count"] = value;
		}
	}
	
	public string TreasureCompleteFlag;

	public ManTreasureCompCalendarSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTreasureCompCalendarSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["start_display_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["start_display_day"]));
		this.Query["end_display_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["end_display_day"]));
		this.Query["rest_attr_count"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["rest_attr_count"]));
	}
}

[System.Serializable]
public class ManTreasureCompCalendar:DbSession {
	public ManTreasureCompCalendar() {
	}

	public DataSet GetPageCollectionAppendEmpty(ManTreasureCompCalendarSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		DataSet oTmpDataSet = this.GetPageCollection(pCondition,1,31);
		return this.AppendEmptyDays(oTmpDataSet,pCondition,pPageNo,pRecPerPage);		
	}

	public DataSet GetPageCollection(ManTreasureCompCalendarSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereTreasure = string.Empty;
		string sWhereComplete = string.Empty;
		string sWhereAll = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhereTreasure(pCondition,ref sWhereTreasure));
		oParamList.AddRange(this.CreateWhereComplete(pCondition,ref sWhereComplete));
		oParamList.AddRange(this.CreateWhereAll(pCondition,ref sWhereAll));
		string sSortExpression = this.CreateOrderExpresion(pCondition);

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	REST_ATTR_COUNT_BY_DAY									,	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY												,	");
		oSqlBuilder.AppendLine("	TREASURE_COMPLETE_FLAG										");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("(																");
		oSqlBuilder.AppendLine("	SELECT 														");
		oSqlBuilder.AppendLine("		T2.REST_ATTR_COUNT_BY_DAY							,	");
		oSqlBuilder.AppendLine("		T2.DISPLAY_DAY										,	");
		oSqlBuilder.AppendLine("		CASE													");
		oSqlBuilder.AppendLine("			WHEN COMP.TREASURE_COMPLETE_FLAG IS NULL THEN 0		");
		oSqlBuilder.AppendLine("			ELSE 1												");
		oSqlBuilder.AppendLine("		END AS TREASURE_COMPLETE_FLAG							");
		oSqlBuilder.AppendLine("	FROM														");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendFormat("			{0} - COUNT(T1.CAST_GAME_PIC_ATTR_SEQ) AS REST_ATTR_COUNT_BY_DAY,",PwViCommConst.MAN_TREASURE_ATTR_NUM).AppendLine();
		oSqlBuilder.AppendLine("			DISPLAY_DAY											");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("		(														");
		oSqlBuilder.AppendLine("			SELECT												");
		oSqlBuilder.AppendLine("				DISTINCT CAST_GAME_PIC_ATTR_SEQ				,	");
		oSqlBuilder.AppendLine("				DISPLAY_DAY										");
		oSqlBuilder.AppendLine("			FROM												");
		oSqlBuilder.AppendLine("				VW_PW_MAN_TREASURE02							");
		oSqlBuilder.AppendLine(sWhereTreasure);
		oSqlBuilder.AppendLine("		)T1														");
		oSqlBuilder.AppendLine("		GROUP BY DISPLAY_DAY									");
		oSqlBuilder.AppendLine("	)T2														,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			1 AS TREASURE_COMPLETE_FLAG						,	");
		oSqlBuilder.AppendLine("			DISPLAY_DAY											");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_MAN_TREASURE_COMPLETE_LOG							");
		oSqlBuilder.AppendLine(sWhereComplete);
		oSqlBuilder.AppendLine("	) COMP														");
		oSqlBuilder.AppendLine("	WHERE														");
		oSqlBuilder.AppendLine("		T2.DISPLAY_DAY	= COMP.DISPLAY_DAY	(+)					");
		oSqlBuilder.AppendLine(")																");
		oSqlBuilder.AppendLine(sWhereAll);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private DataSet AppendEmptyDays(DataSet pTmpDataSet,ManTreasureCompCalendarSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		DataSet oDataSet = pTmpDataSet.Clone();
		DateTime dtStartDay = DateTime.Parse(pCondition.StartDisplayDay).AddDays((double)iStartIndex);
		DateTime dtEndDay = dtStartDay.AddDays((double)(pRecPerPage-1));

		if (dtEndDay > DateTime.Parse(pCondition.EndDisplayDay)) {
			dtEndDay = DateTime.Parse(pCondition.EndDisplayDay);
		}

		pTmpDataSet.Tables[0].PrimaryKey = new DataColumn[] { pTmpDataSet.Tables[0].Columns["DISPLAY_DAY"] };

		while (DateTime.Compare(dtStartDay,dtEndDay) <= 0) {
			DataRow dr = oDataSet.Tables[0].Rows.Add();
			dr["DISPLAY_DAY"] = dtStartDay.ToString("yyyy/MM/dd");

			if (pTmpDataSet.Tables[0].Rows.Find(dr["DISPLAY_DAY"]) != null) {
				DataRow drTmp = pTmpDataSet.Tables[0].Rows.Find(dr["DISPLAY_DAY"]);
				dr["REST_ATTR_COUNT_BY_DAY"] = drTmp["REST_ATTR_COUNT_BY_DAY"].ToString();
				dr["TREASURE_COMPLETE_FLAG"] = drTmp["TREASURE_COMPLETE_FLAG"].ToString();
			} else {
				dr["REST_ATTR_COUNT_BY_DAY"] = PwViCommConst.MAN_TREASURE_ATTR_NUM;
				dr["TREASURE_COMPLETE_FLAG"] = "0";
			}
			dtStartDay = dtStartDay.AddDays(1);
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhereTreasure(ManTreasureCompCalendarSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}
		
		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.StartDisplayDay)) {
			SysPrograms.SqlAppendWhere("DISPLAY_DAY >= :START_DISPLAY_DAY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":START_DISPLAY_DAY",pCondition.StartDisplayDay));
		}

		if (!string.IsNullOrEmpty(pCondition.EndDisplayDay)) {
			SysPrograms.SqlAppendWhere("DISPLAY_DAY <= :END_DISPLAY_DAY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":END_DISPLAY_DAY",pCondition.EndDisplayDay));
		}

		SysPrograms.SqlAppendWhere("PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereComplete(ManTreasureCompCalendarSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.StartDisplayDay)) {
			SysPrograms.SqlAppendWhere("DISPLAY_DAY >= :START_DISPLAY_DAY",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.EndDisplayDay)) {
			SysPrograms.SqlAppendWhere("DISPLAY_DAY <= :END_DISPLAY_DAY",ref pWhereClause);
		}

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereAll(ManTreasureCompCalendarSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.RestAttrCount)) {
			SysPrograms.SqlAppendWhere("REST_ATTR_COUNT_BY_DAY > '0'",ref pWhereClause);
			SysPrograms.SqlAppendWhere("REST_ATTR_COUNT_BY_DAY <= :LIMIT_REST_COUNT",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LIMIT_REST_COUNT",pCondition.RestAttrCount));
		}
		
		if(!string.IsNullOrEmpty(pCondition.TreasureCompleteFlag)) {
			SysPrograms.SqlAppendWhere("TREASURE_COMPLETE_FLAG	= :TREASURE_COMPLETE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TREASURE_COMPLETE_FLAG",pCondition.TreasureCompleteFlag));
		}

		return oParamList.ToArray();
	}
	
	private string CreateOrderExpresion(ManTreasureCompCalendarSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.ManTreasureCompCalendarSort.DISPLAY_DAY_ASC:
				sSortExpression = "ORDER BY DISPLAY_DAY ASC";
				break;
			case PwViCommConst.ManTreasureCompCalendarSort.REST_ATTR_COUNT_ASC:
				sSortExpression = "ORDER BY REST_ATTR_COUNT_BY_DAY ASC, DISPLAY_DAY DESC";
				break;
			default:
				sSortExpression = "ORDER BY DISPLAY_DAY ASC";
				break;
		}

		return sSortExpression;
	}
}
