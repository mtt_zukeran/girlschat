﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class GameItemSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	public string SexCd {
		get {
			return this.Query["sex"];
		}
		set {
			this.Query["sex"] = value;
		}
	}
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	public string StageLevel {
		get {
			return this.Query["stage_level"];
		}
		set {
			this.Query["stage_level"] = value;
		}
	}
	public string GameItemCategoryType {
		get {
			return this.Query["item_category"];
		}
		set {
			this.Query["item_category"] = value;
		}
	}
	public string GameItemCategoryGroupType {
		get {
			return this.Query["item_category_group"];
		}
		set {
			this.Query["item_category_group"] = value;
		}
	}
	public string GameItemGetCd {
		get {
			return this.Query["get"];
		}
		set {
			this.Query["get"] = value;
		}
	}
	public string PresentFlag {
		get {
			return this.Query["present_flag"];
		}
		set {
			this.Query["present_flag"] = value;
		}
	}
	public string ItemSeq {
		get {
			return this.Query["item_seq"];
		}
		set {
			this.Query["item_seq"] = value;
		}
	}

	public GameItemSeekCondition()
		: this(new NameValueCollection()) {
	}
	public GameItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["sex"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex"]));
		this.Query["stage_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_level"]));
		this.Query["item_category"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_category"]));
		this.Query["present_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["present_flag"]));
		this.Query["get"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["get"]));
		this.Query["item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_seq"]));
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
	}
}

#endregion ============================================================================================================

public class GameItem:DbSession {
	public GameItem() {
	}

	public int GetPageCount(GameItemSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM04 P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameItemSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	public DataSet GetPageCollectionBase(GameItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	P.SITE_CD													,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ												,	");
		oSqlBuilder.AppendLine("	P.SEX_CD													,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_NM												,	");
		oSqlBuilder.AppendLine("	P.ATTACK_POWER												,	");
		oSqlBuilder.AppendLine("	P.DEFENCE_POWER												,	");
		oSqlBuilder.AppendLine("	P.ENDURANCE													,	");
		oSqlBuilder.AppendLine("	P.ENDURANCE_NM												,	");
		oSqlBuilder.AppendLine("	P.DESCRIPTION												,	");
		oSqlBuilder.AppendLine("	P.REMARKS													,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_GET_CD											,	");
		oSqlBuilder.AppendLine("	P.PRICE														,	");
		oSqlBuilder.AppendLine("	P.PRICE						AS FIXED_PRICE					,	");
		oSqlBuilder.AppendLine("	P.PRESENT_FLAG												,	");
		oSqlBuilder.AppendLine("	P.PRESENT_GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("	P.STAGE_LEVEL												,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_CATEGORY_TYPE									,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_CATEGORY_GROUP_TYPE								,	");
		oSqlBuilder.AppendLine("	P.PUBLISH_FLAG												,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.POSSESSION_COUNT,0) AS POSSESSION_COUNT		,	");
		oSqlBuilder.AppendLine("	0							AS SALE_FLAG					,	");
		oSqlBuilder.AppendLine("	0							AS REC_NO_PER_PAGE					");
		oSqlBuilder.AppendLine(" FROM																");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM04 P			 								,	");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_POSSESSION_GAME_ITEM									");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD			= :P_SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("	) POSSESSION													");

		// where		
		string sWhereClause = String.Empty;

		SysPrograms.SqlAppendWhere("P.SITE_CD	= POSSESSION.SITE_CD	(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere("P.GAME_ITEM_SEQ	= POSSESSION.GAME_ITEM_SEQ	(+)",ref sWhereClause);
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":P_SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		string sSortExpression = string.Empty;
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		AppendAttrGameItemSale(oDataSet,pCondition);

		int iRecNoPerPage = 0;
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow oDR in oDataSet.Tables[0].Rows) {
				iRecNoPerPage = (int.Parse(oDR["RNUM"].ToString()) - iStartIndex);
				oDR["REC_NO_PER_PAGE"] = iRecNoPerPage.ToString();
			}
		}

		return oDataSet;
	}

	public void AppendAttrGameItemSale(DataSet pDS,GameItemSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		List<string> itemSeqList = new List<string>();

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			itemSeqList.Add(pDR["GAME_ITEM_SEQ"].ToString());
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ						,	");
		oSqlBuilder.AppendLine("	P.DISCOUNTED_PRICE						");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM_SALE02	P				");

		// where		
		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD	= :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(" :NOW_DATE	BETWEEN P.SALE_START_DATE AND P.SALE_END_DATE",ref sWhereClause);
		oParamList.Add(new OracleParameter(":NOW_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input));

		SysPrograms.SqlAppendWhere(string.Format(" P.GAME_ITEM_SEQ IN ({0})",string.Join(",",itemSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["GAME_ITEM_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["GAME_ITEM_SEQ"]);
			pDR["PRICE"] = subDR["DISCOUNTED_PRICE"];
			pDR["SALE_FLAG"] = 1;
		}
	}

	protected OracleParameter[] CreateWhere(GameItemSeekCondition pCondition,ref string pWhereClause) {
		ArrayList list = new ArrayList();
		pWhereClause = pWhereClause ?? string.Empty;

		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		if (!string.IsNullOrEmpty(pCondition.ItemSeq)) {
			SysPrograms.SqlAppendWhere(" P.GAME_ITEM_SEQ = :GAME_ITEM_SEQ",ref pWhereClause);
			list.Add(new OracleParameter(":GAME_ITEM_SEQ",pCondition.ItemSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
			list.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD	= :SEX_CD",ref pWhereClause);
			list.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!String.IsNullOrEmpty(pCondition.PresentFlag)) {
			SysPrograms.SqlAppendWhere(" P.PRESENT_FLAG	= :PRESENT_FLAG",ref pWhereClause);
			list.Add(new OracleParameter(":PRESENT_FLAG",pCondition.PresentFlag));
		}

		if (!String.IsNullOrEmpty(pCondition.GameItemGetCd)) {
			SysPrograms.SqlAppendWhere(" P.GAME_ITEM_GET_CD = :GAME_ITEM_GET_CD",ref pWhereClause);
			list.Add(new OracleParameter(":GAME_ITEM_GET_CD",pCondition.GameItemGetCd));
		}

		if (!String.IsNullOrEmpty(pCondition.StageLevel)) {
			SysPrograms.SqlAppendWhere(" P.STAGE_LEVEL	<= :STAGE_LEVEL",ref pWhereClause);
			list.Add(new OracleParameter(":STAGE_LEVEL",pCondition.StageLevel));
		}

		SysPrograms.SqlAppendWhere(" P.PUBLISH_FLAG	= :PUBLISH_FLAG",ref pWhereClause);
		list.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		if (!String.IsNullOrEmpty(pCondition.GameItemCategoryType)) {
			if (
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) ||
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) ||
				pCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
			) {
				SysPrograms.SqlAppendWhere(" P.GAME_ITEM_CATEGORY_TYPE		= :GAME_ITEM_CATEGORY_TYPE",ref pWhereClause);
				list.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE",pCondition.GameItemCategoryType));
			} else {
				SysPrograms.SqlAppendWhere(" P.GAME_ITEM_CATEGORY_TYPE IN (:RESTORE,:TRAP,:DOUBLE_TRAP)",ref pWhereClause);
				list.Add(new OracleParameter(":RESTORE",PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE));
				list.Add(new OracleParameter(":TRAP",PwViCommConst.GameItemCategory.ITEM_CATEGORY_TRAP));
				list.Add(new OracleParameter(":DOUBLE_TRAP",PwViCommConst.GameItemCategory.ITEM_CATEGORY_DOUBLE_TRAP));
			}
		}

		if (!String.IsNullOrEmpty(pCondition.GameItemCategoryGroupType)) {
			SysPrograms.SqlAppendWhere(" P.GAME_ITEM_CATEGORY_GROUP_TYPE	= :GAME_ITEM_CATEGORY_GROUP_TYPE",ref pWhereClause);
			list.Add(new OracleParameter(":GAME_ITEM_CATEGORY_GROUP_TYPE",pCondition.GameItemCategoryGroupType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	protected string CreateOrderExpresion(GameItemSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_NEW): {
					sSortExpression = " ORDER BY P.PUBLISH_DATE DESC";
					break;
				}
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH): {
					sSortExpression = " ORDER BY P.PRICE DESC , P.GAME_ITEM_SEQ ASC";
					break;
				}
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_LOW): {
					sSortExpression = " ORDER BY P.PRICE ASC , P.GAME_ITEM_SEQ ASC";
					break;
				}
			case (PwViCommConst.GameItemSort.GAME_ITEM_SORT_TYPE): {
					sSortExpression = " ORDER BY P.GAME_ITEM_CATEGORY_TYPE ASC";
					break;
				}
			default:
				sSortExpression = " ORDER BY P.GAME_ITEM_SEQ DESC ";
				break;
		}
		return sSortExpression;
	}

	public void BuyGameItem(string pSiteCd,string pUserSeq,string pUserCharNo,string pItemSeq,string pBuyNum,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BUY_GAME_ITEM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,pItemSeq);
			db.ProcedureInParm("pBUY_QUANTITY",DbSession.DbType.NUMBER,int.Parse(pBuyNum));
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void UseTrap(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,string pTreasureSeq,int pUsedTrapCount,int pDoubleTrapFlag,out string pResult) {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("USE_TRAP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pTREASURE_SEQ",DbSession.DbType.VARCHAR2,pTreasureSeq);
			db.ProcedureInParm("pUSED_TRAP_COUNT",DbSession.DbType.NUMBER,pUsedTrapCount);
			db.ProcedureInParm("pDOUBLE_TRAP_FLAG",DbSession.DbType.NUMBER,pDoubleTrapFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void UseMosaicErase(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastUserSeq,string pCastUserCharNo,string pCastGamePicSeq,int pGameItemGetChargeFlg,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USE_MOSAIC_ERASE");

			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastUserCharNo);
			db.ProcedureInParm("pCAST_GAME_PIC_SEQ",DbSession.DbType.VARCHAR2,pCastGamePicSeq);
			db.ProcedureInParm("pGAME_ITEM_GET_CHARGE_FLG",DbSession.DbType.NUMBER,pGameItemGetChargeFlg);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void UseMovieTicket(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastUserSeq,string pCastUserCharNo,string pCastMovieSeq,int pGameItemGetChargeFlg,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USE_MOVIE_TICKET");

			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastUserCharNo);
			db.ProcedureInParm("pCAST_MOVIE_SEQ",DbSession.DbType.VARCHAR2,pCastMovieSeq);
			db.ProcedureInParm("pGAME_ITEM_GET_CHARGE_FLG",DbSession.DbType.NUMBER,pGameItemGetChargeFlg);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public string PresentGameItem(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pSexCd,string pGameItemSeq) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PRESENT_GAME_ITEM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,pGameItemSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string GetGameItemNm(string pSiteCd,string pSexCd,string pGameItemSeq) {
		string sGameItemNm = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT			");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	T_GAME_ITEM	");
		oSqlBuilder.AppendLine(" WHERE			");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND				");
		oSqlBuilder.AppendLine("	SEX_CD = :SEX_CD AND				");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ = :GAME_ITEM_SEQ AND	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG = :PUBLISH_FLAG		");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add("SITE_CD",pSiteCd);
				this.cmd.Parameters.Add("SEX_CD",pSexCd);
				this.cmd.Parameters.Add("GAME_ITEM_SEQ",pGameItemSeq);
				this.cmd.Parameters.Add("PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sGameItemNm = iBridUtil.GetStringValue(oDataReader["GAME_ITEM_NM"]);
				}
			}
		} finally {
			this.conn.Close();
		}

		return sGameItemNm;
	}

	public DataSet GetGameItemDataList(GameItemSeekCondition pCondition,string[] itemSeqArr) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_NM										,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_GET_CD									,	");
		oSqlBuilder.AppendLine("	ITEM.PRICE												,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.POSSESSION_COUNT,0) POSSESSION_COUNT		,	");
		oSqlBuilder.AppendLine("	0							AS SALE_FLAG					");
		oSqlBuilder.AppendLine(" FROM															");
		oSqlBuilder.AppendLine("	T_GAME_ITEM ITEM										,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			GAME_ITEM_SEQ									,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT									");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_POSSESSION_GAME_ITEM								");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	) POSSESSION												");

		SysPrograms.SqlAppendWhere(" ITEM.SITE_CD = POSSESSION.SITE_CD	(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" ITEM.GAME_ITEM_SEQ = POSSESSION.GAME_ITEM_SEQ	(+)",ref sWhereClause);
		
		SysPrograms.SqlAppendWhere(" ITEM.SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		SysPrograms.SqlAppendWhere(string.Format(" ITEM.GAME_ITEM_SEQ IN ({0})",string.Join(",",itemSeqArr)),ref sWhereClause);

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" ITEM.SEX_CD = :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.PresentFlag)) {
			SysPrograms.SqlAppendWhere(" ITEM.PRESENT_FLAG = :PRESENT_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":PRESENT_FLAG",pCondition.PresentFlag));
		}

		SysPrograms.SqlAppendWhere(" ITEM.PUBLISH_FLAG = :PUBLISH_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oTempDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendAttrGameItemSale(oTempDataSet,pCondition);
		
		return oTempDataSet;
	}

	public string GetGameItem(string pSiteCd,string pSexCd,string pGameItemCategoryType,string pItemDistinction) {

		string sValue = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	,										");
		oSqlBuilder.AppendLine("	PRICE													");
		oSqlBuilder.AppendLine(" FROM														");
		oSqlBuilder.AppendLine("	T_GAME_ITEM												");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	SEX_CD  = :SEX_CD									AND	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE = :GAME_ITEM_CATEGORY_TYPE	AND	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG = :PUBLISH_FLAG						AND	");
		oSqlBuilder.AppendLine("	PRESENT_FLAG = :PRESENT_FLAG							");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add("SITE_CD",pSiteCd);
				this.cmd.Parameters.Add("SEX_CD",pSexCd);
				this.cmd.Parameters.Add("GAME_ITEM_CATEGORY_TYPE",pGameItemCategoryType);
				this.cmd.Parameters.Add("PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);
				this.cmd.Parameters.Add("PRESENT_FLAG",ViCommConst.FLAG_OFF_STR);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					if (pItemDistinction == "GAME_ITEM_SEQ") {
						sValue = iBridUtil.GetStringValue(oDataReader["GAME_ITEM_SEQ"].ToString());
					} else if (pItemDistinction == "PRICE") {
						sValue = iBridUtil.GetStringValue(oDataReader["PRICE"].ToString());
					}
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public void GamePossessionItemMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pItemSeq) {
		
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GAME_POSSESSION_ITEM_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,pItemSeq);
			db.ProcedureInParm("pADD_COUNT", DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("pWITHOUT_COMMIT", DbSession.DbType.NUMBER, 1);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public DataSet GetClearTownSeqData(String pSiteCd,string pGameItemSeq,string pStageSeq) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	STAGE_SEQ					,	");
		oSqlBuilder.AppendLine("	TOWN_SEQ						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	T_CLEAR_TOWN_GET_ITEM			");
		
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pGameItemSeq)) {
			SysPrograms.SqlAppendWhere(" GAME_ITEM_SEQ = :GAME_ITEM_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pGameItemSeq));
		}

		if (!string.IsNullOrEmpty(pStageSeq)) {
			SysPrograms.SqlAppendWhere(" STAGE_SEQ <= :STAGE_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_SEQ",pStageSeq));
		}

		SysPrograms.SqlAppendWhere(" ROWNUM <= 1",ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" ORDER BY STAGE_SEQ	DESC			");


		DataSet oTempDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oTempDataSet;
	}
	
	public DataSet GetOnePresentGameItemByPresentGameItemSeq(string pSiteCd,string pSexCd,string pPresentGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ											");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_GAME_ITEM												");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	SEX_CD					= :SEX_CD					AND	");
		oSqlBuilder.AppendLine("	PRESENT_FLAG			= :PRESENT_FLAG				AND	");
		oSqlBuilder.AppendLine("	PRESENT_GAME_ITEM_SEQ	= :PRESENT_GAME_ITEM_SEQ		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));
		oParamList.Add(new OracleParameter(":PRESENT_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":PRESENT_GAME_ITEM_SEQ",pPresentGameItemSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public void PresentGameItemStaffExt(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPresentId,
		string[] pGameItemSeq,
		string[] pAddCount,
		string pDirectFlag,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PRESENT_GAME_ITEM_STAFF_EXT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPRESENT_ID",DbSession.DbType.VARCHAR2,pPresentId);
			db.ProcedureInArrayParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,pGameItemSeq);
			db.ProcedureInArrayParm("pADD_COUNT",DbSession.DbType.VARCHAR2,pAddCount);
			db.ProcedureInParm("pDIRECT_FLAG",DbSession.DbType.VARCHAR2,pDirectFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public string CheckPresentGameItemStaffExt(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPresentId,
		string[] pGameItemSeq,
		string[] pAddCount,
		string pDirectFlag,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PRESENT_GAME_ITEM_STAFF_EXT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPRESENT_ID",DbSession.DbType.VARCHAR2,pPresentId);
			db.ProcedureInArrayParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,pGameItemSeq);
			db.ProcedureInArrayParm("pADD_COUNT",DbSession.DbType.VARCHAR2,pAddCount);
			db.ProcedureInParm("pDIRECT_FLAG",DbSession.DbType.VARCHAR2,pDirectFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pCHECK_ONLY",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON_STR);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
			
			return pResult;
		}
	}

	public DataSet GetGameItemByCategoryType(GameItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ												,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_CATEGORY_TYPE									,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.POSSESSION_COUNT,0) AS POSSESSION_COUNT		,	");
		oSqlBuilder.AppendLine("	P.PRICE														,	");
		oSqlBuilder.AppendLine("	0 AS SALE_FLAG													");
		oSqlBuilder.AppendLine(" FROM																");
		oSqlBuilder.AppendLine("	T_GAME_ITEM P				 								,	");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_POSSESSION_GAME_ITEM									");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("	) POSSESSION													");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	P.SITE_CD					= POSSESSION.SITE_CD		(+)	AND	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ				= POSSESSION.GAME_ITEM_SEQ	(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_CATEGORY_TYPE	= :GAME_ITEM_CATEGORY_TYPE		AND	");
		oSqlBuilder.AppendLine("	P.SEX_CD					= :SEX_CD						AND	");
		oSqlBuilder.AppendLine("	P.PRESENT_FLAG				= :PRESENT_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.PUBLISH_FLAG				= :PUBLISH_FLAG						");

		// where

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE",pCondition.GameItemCategoryType));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PRESENT_FLAG",pCondition.PresentFlag));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		// order by
		string sSortExpression = string.Empty;
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		AppendAttrGameItemSale(oDataSet,pCondition);

		return oDataSet;
	}
}
