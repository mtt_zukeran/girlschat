﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 出演者ニュース
--	Progaram ID		: CastNews
--
--  Creation Date	: 2013.02.02
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class CastNewsSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["usercharno"];
		}
		set {
			this.Query["usercharno"] = value;
		}
	}

	public string NewsType;
	public List<string> NewsTypeList = new List<string>();

	public string OnlyFanClubFlag {
		get {
			return this.Query["onlyfanclub"];
		}
		set {
			this.Query["onlyfanclub"] = value;
		}
	}

	public string OnlyFavoritFlag = ViCommConst.FLAG_OFF_STR;

	public CastNewsSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastNewsSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["usercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["usercharno"]));
		this.Query["onlyfanclub"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["onlyfanclub"]));
	}
}

#endregion ============================================================================================================

public class CastNews:CastBase {
	public CastNews()
		: base() {
	}
	public CastNews(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastNewsSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_CAST_NEWS00 P			");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastNewsSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + CastInfoFields() + "					,	");
		oSqlBuilder.AppendLine("	P.NEWS_TYPE									,	");
		oSqlBuilder.AppendLine("	P.NEWS_DATE										");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_PW_CAST_NEWS00 P								");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.NEWS_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private string CastInfoFields() {
		string sField = CastBasicField();

		sField = sField + ",P.BLOG_SEQ ";

		return sField;
	}

	private OracleParameter[] CreateWhere(CastNewsSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.NewsType)) {
			SysPrograms.SqlAppendWhere(" P.NEWS_TYPE		= :NEWS_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NEWS_TYPE",pCondition.NewsType));
		}

		if (pCondition.NewsTypeList.Count > 0) {
			SysPrograms.SqlAppendWhere(string.Format("P.NEWS_TYPE IN({0})",string.Join(",",pCondition.NewsTypeList.ToArray())),ref pWhereClause);
		}

		if (pCondition.OnlyFanClubFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			string sSelfUserSeq = string.Empty;
			if(this.sessionObj != null) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				sSelfUserSeq = oSessionMan.userMan.userSeq;
			}
			
			if (!string.IsNullOrEmpty(sSelfUserSeq)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" EXISTS (													");
				oSubQueryBuilder.AppendLine("	SELECT													");
				oSubQueryBuilder.AppendLine("		1													");
				oSubQueryBuilder.AppendLine("	FROM													");
				oSubQueryBuilder.AppendLine("		T_FANCLUB_STATUS									");
				oSubQueryBuilder.AppendLine("	WHERE													");
				oSubQueryBuilder.AppendLine("		SITE_CD					= P.SITE_CD				AND	");
				oSubQueryBuilder.AppendLine("		MAN_USER_SEQ			= :SELF_USER_SEQ		AND	");
				oSubQueryBuilder.AppendLine("		CAST_USER_SEQ			= P.USER_SEQ			AND	");
				oSubQueryBuilder.AppendLine("		CAST_CHAR_NO			= P.USER_CHAR_NO		AND	");
				oSubQueryBuilder.AppendLine("		RESIGN_FLAG				= :RESIGN_FLAG			AND	");
				oSubQueryBuilder.AppendLine("		EXPIRE_DATE				> SYSDATE					");
				oSubQueryBuilder.AppendLine(" )															");
				SysPrograms.SqlAppendWhere(oSubQueryBuilder.ToString(),ref pWhereClause);
				oParamList.Add(new OracleParameter(":SELF_USER_SEQ",sSelfUserSeq));
				oParamList.Add(new OracleParameter(":RESIGN_FLAG",ViCommConst.FLAG_OFF_STR));
			}
		}

		if (pCondition.OnlyFavoritFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			string sSelfUserSeq = string.Empty;
			string sSelfUserCharNo = string.Empty;

			if (this.sessionObj != null) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				sSelfUserSeq = oSessionMan.userMan.userSeq;
				sSelfUserCharNo = oSessionMan.userMan.userCharNo;
			}

			if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine("EXISTS (");
				oSubQueryBuilder.AppendLine("	SELECT");
				oSubQueryBuilder.AppendLine("		1");
				oSubQueryBuilder.AppendLine("	FROM");
				oSubQueryBuilder.AppendLine("		T_FAVORIT");
				oSubQueryBuilder.AppendLine("	WHERE");
				oSubQueryBuilder.AppendLine("		SITE_CD = P.SITE_CD AND");
				oSubQueryBuilder.AppendLine("		USER_SEQ = :FAVORIT_USER_SEQ AND");
				oSubQueryBuilder.AppendLine("		USER_CHAR_NO = :FAVORIT_USER_CHAR_NO AND");
				oSubQueryBuilder.AppendLine("		PARTNER_USER_SEQ = P.USER_SEQ AND");
				oSubQueryBuilder.AppendLine("		PARTNER_USER_CHAR_NO = P.USER_CHAR_NO");
				oSubQueryBuilder.AppendLine(")");

				SysPrograms.SqlAppendWhere(oSubQueryBuilder.ToString(),ref pWhereClause);

				oParamList.Add(new OracleParameter(":FAVORIT_USER_SEQ",sSelfUserSeq));
				oParamList.Add(new OracleParameter(":FAVORIT_USER_CHAR_NO",sSelfUserCharNo));
			}
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	public DataSet RemoveNonPublishData(DataSet pDataSet,int pNeedCount) {
		pDataSet.Tables[0].Columns.Add(new DataColumn("RANK_NO",Type.GetType("System.String")));
		DataSet ds = pDataSet.Clone();
		int iRankNo = 1;

		foreach (DataRow dr in pDataSet.Tables[0].Rows) {
			bool bImportFlag = false;
			string sNewsType = iBridUtil.GetStringValue(dr["NEWS_TYPE"]);
			string sLoginViewStatus = iBridUtil.GetStringValue(dr["LOGIN_VIEW_STATUS"]);
			string sWaitingViewStatus = iBridUtil.GetStringValue(dr["WAITING_VIEW_STATUS"]);
			string sBbsNonPublishFlag = iBridUtil.GetStringValue(dr["BBS_NON_PUBLISH_FLAG"]);
			string sBlogNonPublishFlag = iBridUtil.GetStringValue(dr["BLOG_NON_PUBLISH_FLAG"]);
			string sDiaryNonPublishFlag = iBridUtil.GetStringValue(dr["DIARY_NON_PUBLISH_FLAG"]);
			string sProfilePicNonPublishFlag = iBridUtil.GetStringValue(dr["PROFILE_PIC_NON_PUBLISH_FLAG"]);

			switch (sNewsType) {
				case PwViCommConst.CastNewsType.NEWS_TYPE_LOGIN:
					if (sLoginViewStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_STANDBY:
				case PwViCommConst.CastNewsType.NEWS_TYPE_TALK:
					if (sWaitingViewStatus.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_BBS_PIC:
				case PwViCommConst.CastNewsType.NEWS_TYPE_BBS_MOV:
				case PwViCommConst.CastNewsType.NEWS_TYPE_BBS:
					if (sBbsNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_BLOG:
					if (sBlogNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_FREE_PIC:
					if (sProfilePicNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				case PwViCommConst.CastNewsType.NEWS_TYPE_DIARY:
					if (sDiaryNonPublishFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						bImportFlag = true;
					}
					break;
				default:
					bImportFlag = true;
					break;
			}

			if (bImportFlag) {
				dr["RANK_NO"] = iRankNo.ToString();
				iRankNo++;
				ds.Tables[0].ImportRow(dr);
			}

			if (ds.Tables[0].Rows.Count >= pNeedCount) {
				break;
			}
		}

		return ds;
	}

	public void LogCastNews(string pSiteCd,string pUserSeq,string pUserCharNo,string pNewsType) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_CAST_NEWS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pNEWS_TYPE",DbSession.DbType.VARCHAR2,pNewsType);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}