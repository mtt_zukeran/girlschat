﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・欲しい物
--	Progaram ID		: WantedItem
--
--  Creation Date	: 2011.08.15
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class WantedItemSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}
	
	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public string PartnerFlg {
		get {
			return this.Query["partner_flg"];
		}
		set {
			this.Query["partner_flg"] = value;
		}
	}

	public WantedItemSeekCondition()
		: this(new NameValueCollection()) {
	}

	public WantedItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
	}
}

[System.Serializable]
public class WantedItem:DbSession {
	public WantedItem() {
	}

	public DataSet GetListWantedItem(WantedItemSeekCondition pCondtion) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseItem = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	WANTED.WANTED_ITEM_SEQ									,	");
		oSqlBuilder.AppendLine("	WANTED.GAME_ITEM_SEQ									,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_NM										,	");
		oSqlBuilder.AppendLine("	ITEM.ATTACK_POWER										,	");
		oSqlBuilder.AppendLine("	ITEM.DEFENCE_POWER										,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_GET_CD									,	");
		oSqlBuilder.AppendLine("	ITEM.PRICE												,	");
		oSqlBuilder.AppendLine("	ITEM.PRICE AS FIXED_PRICE								,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.POSSESSION_COUNT,0) AS POSSESSION_COUNT	,	");
		oSqlBuilder.AppendLine("	0 AS SALE_FLAG												");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	(						");
		oSqlBuilder.AppendLine("	SELECT					");
		oSqlBuilder.AppendLine("		WANTED_ITEM_SEQ	,	");
		oSqlBuilder.AppendLine("		SITE_CD			,	");
		oSqlBuilder.AppendLine("		USER_SEQ		,	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ		");
		oSqlBuilder.AppendLine("	FROM					");
		oSqlBuilder.AppendLine("		T_WANTED_ITEM		");
		
		// where
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) WANTED,				");
		oSqlBuilder.AppendLine("	(						");
		oSqlBuilder.AppendLine("	SELECT					");
		oSqlBuilder.AppendLine("		SITE_CD			,	");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ	,	");
		oSqlBuilder.AppendLine("		GAME_ITEM_NM	,	");
		oSqlBuilder.AppendLine("		ATTACK_POWER	,	");
		oSqlBuilder.AppendLine("		DEFENCE_POWER	,	");
		oSqlBuilder.AppendLine("		GAME_ITEM_GET_CD,	");
		oSqlBuilder.AppendLine("		PRICE				");
		oSqlBuilder.AppendLine("	FROM					");
		oSqlBuilder.AppendLine("		T_GAME_ITEM			");
		
		// where
		oParamList.AddRange(this.CreateWhereItem(pCondtion,ref sWhereClauseItem));
		oSqlBuilder.AppendLine(sWhereClauseItem);
		
		oSqlBuilder.AppendLine("	) ITEM,								");
		oSqlBuilder.AppendLine("	T_POSSESSION_GAME_ITEM POSSESSION	");
		oSqlBuilder.AppendLine(" WHERE									");
		oSqlBuilder.AppendLine("	WANTED.SITE_CD = ITEM.SITE_CD					(+) AND	");
		oSqlBuilder.AppendLine(" 	WANTED.GAME_ITEM_SEQ = ITEM.GAME_ITEM_SEQ		(+) AND	");
		oSqlBuilder.AppendLine(" 	WANTED.SITE_CD = POSSESSION.SITE_CD				(+) AND	");
		oSqlBuilder.AppendLine(" 	WANTED.USER_SEQ = POSSESSION.USER_SEQ			(+) AND	");
		oSqlBuilder.AppendLine(" 	WANTED.USER_CHAR_NO = POSSESSION.USER_CHAR_NO	(+) AND	");
		oSqlBuilder.AppendLine(" 	WANTED.GAME_ITEM_SEQ = POSSESSION.GAME_ITEM_SEQ	(+) 	");
		
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		
		DataSet oTmpDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendSale(oTmpDataSet,pCondtion);
		
		DataSet oDataSet = new DataSet();
		DataTable oDataTable = oDataSet.Tables.Add();
		oDataTable.Columns.Add("WANTED_ITEM_SEQ",Type.GetType("System.String"));
		oDataTable.Columns.Add("GAME_ITEM_SEQ",Type.GetType("System.String"));
		oDataTable.Columns.Add("GAME_ITEM_NM",Type.GetType("System.String"));
		oDataTable.Columns.Add("ATTACK_POWER",Type.GetType("System.String"));
		oDataTable.Columns.Add("DEFENCE_POWER",Type.GetType("System.String"));
		oDataTable.Columns.Add("GAME_ITEM_GET_CD",Type.GetType("System.String"));
		oDataTable.Columns.Add("PRICE",Type.GetType("System.String"));
		oDataTable.Columns.Add("FIXED_PRICE",Type.GetType("System.String"));
		oDataTable.Columns.Add("POSSESSION_COUNT",Type.GetType("System.String"));
		oDataTable.Columns.Add("SALE_FLAG",Type.GetType("System.String"));
		
		for (int i = 0;i < 3;i++) {
			if (oTmpDataSet.Tables[0].Rows.Count >= (i+1)) {
				DataRow oDataRow = oDataTable.Rows.Add();
				oDataRow["WANTED_ITEM_SEQ"] = oTmpDataSet.Tables[0].Rows[i]["WANTED_ITEM_SEQ"];
				oDataRow["GAME_ITEM_SEQ"] = oTmpDataSet.Tables[0].Rows[i]["GAME_ITEM_SEQ"];
				oDataRow["GAME_ITEM_NM"] = oTmpDataSet.Tables[0].Rows[i]["GAME_ITEM_NM"];
				oDataRow["ATTACK_POWER"] = oTmpDataSet.Tables[0].Rows[i]["ATTACK_POWER"];
				oDataRow["DEFENCE_POWER"] = oTmpDataSet.Tables[0].Rows[i]["DEFENCE_POWER"];
				oDataRow["GAME_ITEM_GET_CD"] = oTmpDataSet.Tables[0].Rows[i]["GAME_ITEM_GET_CD"];
				oDataRow["PRICE"] = oTmpDataSet.Tables[0].Rows[i]["PRICE"];
				oDataRow["FIXED_PRICE"] = oTmpDataSet.Tables[0].Rows[i]["FIXED_PRICE"];
				oDataRow["POSSESSION_COUNT"] = oTmpDataSet.Tables[0].Rows[i]["POSSESSION_COUNT"];
				oDataRow["SALE_FLAG"] = oTmpDataSet.Tables[0].Rows[i]["SALE_FLAG"];
			} else {
				DataRow oDataRow = oDataTable.Rows.Add();
				oDataRow["WANTED_ITEM_SEQ"] = string.Empty;
				oDataRow["GAME_ITEM_SEQ"] = string.Empty;
				oDataRow["GAME_ITEM_NM"] = string.Empty;
				oDataRow["ATTACK_POWER"] = string.Empty;
				oDataRow["DEFENCE_POWER"] = string.Empty;
				oDataRow["GAME_ITEM_GET_CD"] = string.Empty;
				oDataRow["PRICE"] = string.Empty;
				oDataRow["FIXED_PRICE"] = string.Empty;
				oDataRow["POSSESSION_COUNT"] = string.Empty;
				oDataRow["SALE_FLAG"] = ViCommConst.FLAG_OFF_STR;
			}
		}
		
		return oDataSet;
	}

	public DataSet GetListWantedItemPartner(WantedItemSeekCondition pCondtion) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseItem = string.Empty;
		string sWhereClauseItemPossession = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	WANTED.WANTED_ITEM_SEQ									,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_NM										,	");
		oSqlBuilder.AppendLine("	NVL(POSSESSION.POSSESSION_COUNT,0) AS POSSESSION_COUNT		");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	(						");
		oSqlBuilder.AppendLine("	SELECT					");
		oSqlBuilder.AppendLine("		WANTED_ITEM_SEQ	,	");
		oSqlBuilder.AppendLine("		SITE_CD			,	");
		oSqlBuilder.AppendLine("		USER_SEQ		,	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ		");
		oSqlBuilder.AppendLine("	FROM					");
		oSqlBuilder.AppendLine("		T_WANTED_ITEM		");

		// where
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) WANTED,						");
		oSqlBuilder.AppendLine("	(								");
		oSqlBuilder.AppendLine("	SELECT							");
		oSqlBuilder.AppendLine("		SITE_CD					,	");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ			,	");
		oSqlBuilder.AppendLine("		PRESENT_GAME_ITEM_SEQ	,	");
		oSqlBuilder.AppendLine("		GAME_ITEM_NM				");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		T_GAME_ITEM					");

		// where
		oParamList.AddRange(this.CreateWhereItem(pCondtion,ref sWhereClauseItem));
		oSqlBuilder.AppendLine(sWhereClauseItem);

		oSqlBuilder.AppendLine("	) ITEM,														");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			GAME_ITEM_SEQ									,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT									");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_POSSESSION_GAME_ITEM								");

		// where
		oParamList.AddRange(this.CreateWhereItemPossession(pCondtion,ref sWhereClauseItemPossession));
		oSqlBuilder.AppendLine(sWhereClauseItemPossession);
		
		oSqlBuilder.AppendLine("	) POSSESSION												");
		oSqlBuilder.AppendLine(" WHERE															");
		oSqlBuilder.AppendLine("	WANTED.SITE_CD = ITEM.SITE_CD						(+) AND	");
		oSqlBuilder.AppendLine(" 	WANTED.GAME_ITEM_SEQ = ITEM.PRESENT_GAME_ITEM_SEQ	(+) AND	");
		oSqlBuilder.AppendLine(" 	ITEM.SITE_CD = POSSESSION.SITE_CD					(+) AND	");
		oSqlBuilder.AppendLine(" 	ITEM.GAME_ITEM_SEQ = POSSESSION.GAME_ITEM_SEQ		(+) 	");
		
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	private void AppendSale(DataSet pDS,WantedItemSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		string sWhereClause = String.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> itemSeqList = new List<string>();
		
		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			itemSeqList.Add(pDR["GAME_ITEM_SEQ"].ToString());
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ		,	");
		oSqlBuilder.AppendLine("	DISCOUNTED_PRICE		");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM_SALE02	");

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}
		
		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(string.Format(" GAME_ITEM_SEQ IN ({0})",string.Join(",",itemSeqList.ToArray())),ref sWhereClause);
		SysPrograms.SqlAppendWhere(" SYSDATE BETWEEN SALE_START_DATE AND SALE_END_DATE",ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);
		
		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["GAME_ITEM_SEQ"] };
		
		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["GAME_ITEM_SEQ"]);
			pDR["PRICE"] = subDR["DISCOUNTED_PRICE"];
			pDR["SALE_FLAG"] = ViCommConst.FLAG_ON_STR;
		}
	}

	private OracleParameter[] CreateWhere(WantedItemSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}
		
		if (string.IsNullOrEmpty(pCondition.PartnerFlg)) {
		
			if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
				SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref pWhereClause);
				oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
			}

			if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
				SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
				oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
			}

		}
		
		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :PARTNER_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}

		
		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereItem(WantedItemSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}
		
		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereItemPossession(WantedItemSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(WantedItemSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		
		sSortExpression = "ORDER BY WANTED_ITEM_SEQ ASC";
		
		return sSortExpression;
	}

	public string WantedItemMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,string pWantedItemSeq,string pGameItemSeq,string pDelFlag) {
		string sResult = string.Empty;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WANTED_ITEM_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureBothParm("pWANTED_ITEM_SEQ",DbSession.DbType.VARCHAR2,pWantedItemSeq);
			db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,pGameItemSeq);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}
		return sResult;
	}

	public string GetGameItemSeq(string pSiteCd,string pUserSeq,string pUserCharNo,string pWantedItemSeq) {
		string sGameItemSeq = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_WANTED_ITEM	");
		oSqlBuilder.AppendLine(" WHERE				");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND				");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND			");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND	");
		oSqlBuilder.AppendLine("	WANTED_ITEM_SEQ = :WANTED_ITEM_SEQ	");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add("SITE_CD",pSiteCd);
				this.cmd.Parameters.Add("USER_SEQ",pUserSeq);
				this.cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				this.cmd.Parameters.Add("WANTED_ITEM_SEQ",pWantedItemSeq);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sGameItemSeq = iBridUtil.GetStringValue(oDataReader["GAME_ITEM_SEQ"]);
				}
			}
		} finally {
			this.conn.Close();
		}

		return sGameItemSeq;
	}
	
	public DataSet GetOneByGameItemSeq(string pSiteCd,string pUserSeq,string pUserCharNo,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	WANTED_ITEM_SEQ								");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_WANTED_ITEM								");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ		= :GAME_ITEM_SEQ		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pGameItemSeq));

		DataSet oDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDS;
	}
}
