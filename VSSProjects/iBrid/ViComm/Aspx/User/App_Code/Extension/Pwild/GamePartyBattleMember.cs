﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class GamePartyBattleMemberSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public GamePartyBattleMemberSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GamePartyBattleMemberSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
	}
}

#endregion ============================================================================================================

public class GamePartyBattleMember:DbSession {
	public GamePartyBattleMember() {
	}

	public string GetPartyMemberMask(GamePartyBattleMemberSeekCondition pCondition) {
		int iMemberMsk = 0;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE								");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER02							");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	PARTY_MEMBER_FLAG	= :PARTY_MEMBER_FLAG	AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":PARTY_MEMBER_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if(oDataSet.Tables[0].Rows.Count > 0) {
			foreach(DataRow dr in oDataSet.Tables[0].Rows) {
				if(dr["GAME_CHARACTER_TYPE"].ToString().Equals(PwViCommConst.GameCharacterType.TYPE01)) {
					iMemberMsk = iMemberMsk + 1;
				} else if(dr["GAME_CHARACTER_TYPE"].ToString().Equals(PwViCommConst.GameCharacterType.TYPE02)) {
					iMemberMsk = iMemberMsk + 2;
				} else if(dr["GAME_CHARACTER_TYPE"].ToString().Equals(PwViCommConst.GameCharacterType.TYPE03)) {
					iMemberMsk = iMemberMsk + 4;
				}
			}
		}
		
		return iMemberMsk.ToString();
	}
}