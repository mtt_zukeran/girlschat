﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ 日別ゲーム内獲得報酬

--	Progaram ID		: DailyGamePaymentLog
--
--  Creation Date	: 2012.10.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class DailyGamePaymentLogSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string StartDate {
		get {
			return this.Query["start_date"];
		}
		set {
			this.Query["start_date"] = value;
		}
	}
	
	public string EndDate {
		get {
			return this.Query["end_date"];
		}
		set {
			this.Query["end_date"] = value;
		}
	}

	public DailyGamePaymentLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public DailyGamePaymentLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["user_seq_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["start_date"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["start_date"]));
		this.Query["end_date"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["end_date"]));
	}
}

#endregion ============================================================================================================

public class DailyGamePaymentLog:DbSession {
	public DailyGamePaymentLog() {
	}

	public DataSet GetOne(DailyGamePaymentLogSeekCondition pCondition) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																					");
		oSqlBuilder.AppendLine("	NVL(SUM(USE_MOSAIC_ERASE_POINT),0) AS USE_MOSAIC_ERASE_POINT					,	");
		oSqlBuilder.AppendLine("	NVL(SUM(USE_MOVIE_TICKET_POINT),0) AS USE_MOVIE_TICKET_POINT					,	");
		oSqlBuilder.AppendLine("	NVL(SUM(GET_DATE_POINT),0) AS GET_DATE_POINT									,	");
		oSqlBuilder.AppendLine("	NVL(SUM(GET_PRESENT_GAME_ITEM_POINT),0) AS GET_PRESENT_GAME_ITEM_POINT			,	");
		oSqlBuilder.AppendLine("	NVL(SUM(GET_COMPLETE_BONUS_POINT),0) AS GET_COMPLETE_BONUS_POINT				,	");
		oSqlBuilder.AppendLine("	NVL(SUM(USE_MOSAIC_ERASE_POINT),0) + NVL(SUM(USE_MOVIE_TICKET_POINT),0) + NVL(SUM(GET_DATE_POINT),0) + NVL(SUM(GET_PRESENT_GAME_ITEM_POINT),0) + NVL(SUM(GET_COMPLETE_BONUS_POINT),0) AS GAME_TOTAL_GET_POINT	");
		oSqlBuilder.AppendLine("FROM																					");
		oSqlBuilder.AppendLine("	T_DAILY_GAME_PAYMENT_LOG															");
		oSqlBuilder.AppendLine("WHERE																					");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ														AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO													AND	");
		oSqlBuilder.AppendLine("	REPORT_DAY		>= :START_DATE													AND	");
		oSqlBuilder.AppendLine("	REPORT_DAY		<= :END_DATE														");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":START_DATE",pCondition.StartDate));
		oParamList.Add(new OracleParameter(":END_DATE",pCondition.EndDate));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
