﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 美女コレお知らせ
--	Progaram ID		: GameInformation
--
--  Creation Date	: 2012.11.20
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

[Serializable]
public class GameInformationSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string EndFlag {
		get {
			return this.Query["end_flag"];
		}
		set {
			this.Query["end_flag"] = value;
		}
	}

	public GameInformationSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameInformationSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["end_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["end_flag"]));
	}
}

[System.Serializable]
public class GameInformation:DbSession {
	public GameInformation()
		: base() {
	}

	public DataSet GetPageCollection(GameInformationSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																					");
		oSqlBuilder.AppendLine("	HANDLE_NM																		,	");
		oSqlBuilder.AppendLine("	ACTION_TYPE																			");
		oSqlBuilder.AppendLine("FROM																					");
		oSqlBuilder.AppendLine("	VW_PW_GAME_INFORMATION01 P															");
		oSqlBuilder.AppendLine("WHERE																					");
		oSqlBuilder.AppendLine("	P.SITE_CD							= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= :USER_SEQ									AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= :USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("	P.END_FLAG							= :END_FLAG									AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG						= :STAFF_FLAG								AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG							IN(:NA_FLAG,:NA_FLAG2)						AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG						AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																			");
		oSqlBuilder.AppendLine("	(																					");
		oSqlBuilder.AppendLine("		SELECT																			");
		oSqlBuilder.AppendLine("			*																			");
		oSqlBuilder.AppendLine("		FROM																			");
		oSqlBuilder.AppendLine("			T_REFUSE																	");
		oSqlBuilder.AppendLine("		WHERE																			");
		oSqlBuilder.AppendLine("			SITE_CD						= P.SITE_CD									AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= P.PARTNER_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= P.PARTNER_USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= P.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= P.USER_CHAR_NO								");
		oSqlBuilder.AppendLine("	)																					");

		string sSortExpression = " ORDER BY P.CREATE_DATE DESC";
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		oParamList.Add(new OracleParameter(":END_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionForCast(GameInformationSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																					");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM																	,	");
		oSqlBuilder.AppendLine("	ACTION_TYPE																			");
		oSqlBuilder.AppendLine("FROM																					");
		oSqlBuilder.AppendLine("	VW_PW_GAME_INFORMATION02 P															");
		oSqlBuilder.AppendLine("WHERE																					");
		oSqlBuilder.AppendLine("	P.SITE_CD							= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= :USER_SEQ									AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= :USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("	P.END_FLAG							= :END_FLAG									AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG						= :STAFF_FLAG								AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG							IN(:NA_FLAG,:NA_FLAG2)						AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG						AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																			");
		oSqlBuilder.AppendLine("	(																					");
		oSqlBuilder.AppendLine("		SELECT																			");
		oSqlBuilder.AppendLine("			*																			");
		oSqlBuilder.AppendLine("		FROM																			");
		oSqlBuilder.AppendLine("			T_REFUSE																	");
		oSqlBuilder.AppendLine("		WHERE																			");
		oSqlBuilder.AppendLine("			SITE_CD						= P.SITE_CD									AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= P.PARTNER_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= P.PARTNER_USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= P.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= P.USER_CHAR_NO								");
		oSqlBuilder.AppendLine("	)																					");

		string sSortExpression = " ORDER BY P.CREATE_DATE DESC";

		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		oParamList.Add(new OracleParameter(":END_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
	
	public int GetInformationCountForMan(string pSiteCd,string pUserSeq,string pUserCharNo,string pLastHugLogCheckDate) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		int iRecCount = 0;
		
		oSqlBuilder.AppendLine("SELECT																							");
		oSqlBuilder.AppendLine("	COUNT(*)																					");
		oSqlBuilder.AppendLine("FROM																							");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			1 AS REC																			");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER																	");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD												AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO											AND	");
		oSqlBuilder.AppendLine("			UNASSIGNED_FORCE_COUNT	> 0															");
		oSqlBuilder.AppendLine("		UNION ALL																				");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																		");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_GAME_FELLOW																		");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= :USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			FELLOW_APPLICATION_STATUS	= :FELLOW_APPLICATION_STATUS							");
		oSqlBuilder.AppendLine("		UNION ALL																				");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																		");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION CM															,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER GC																	");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			CM.SITE_CD					= GC.SITE_CD										AND	");
		oSqlBuilder.AppendLine("			CM.PARTNER_USER_SEQ			= GC.USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			CM.PARTNER_USER_CHAR_NO		= GC.USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			CM.SITE_CD					= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("			CM.USER_SEQ					= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			CM.USER_CHAR_NO				= :USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			CM.PARTNER_HUG_COUNT		> 0													AND	");
		if (!string.IsNullOrEmpty(pLastHugLogCheckDate)) {
			oSqlBuilder.AppendLine("			TO_CHAR(CM.LAST_PARTNER_HUG_DATE,'YYYY/MM/DD HH24:MI:SS') > :LAST_HUG_LOG_CHECK_DATE	AND	");
		}
		oSqlBuilder.AppendLine("			GC.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG					AND	");
		oSqlBuilder.AppendLine("			GC.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("			GC.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG						AND	");
		oSqlBuilder.AppendLine("			GC.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG				");
		oSqlBuilder.AppendLine("		UNION ALL																				");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																		");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			(																					");
		oSqlBuilder.AppendLine("				SELECT																			");
		oSqlBuilder.AppendLine("					GET_FLAG																	");
		oSqlBuilder.AppendLine("				FROM																			");
		oSqlBuilder.AppendLine("					(																			");
		oSqlBuilder.AppendLine("						SELECT																	");
		oSqlBuilder.AppendLine("							GET_FLAG															");
		oSqlBuilder.AppendLine("						FROM																	");
		oSqlBuilder.AppendLine("							VW_PW_CAST_GAME_PRE_HISTORY01 P										");
		oSqlBuilder.AppendLine("						WHERE																	");
		oSqlBuilder.AppendLine("							SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("							PARTNER_USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("							PARTNER_USER_CHAR_NO	= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("							NA_FLAG					IN(:NA_FLAG,:NA_FLAG2)					AND	");
		oSqlBuilder.AppendLine("							NOT EXISTS															");
		oSqlBuilder.AppendLine("							(																	");
		oSqlBuilder.AppendLine("								SELECT															");
		oSqlBuilder.AppendLine("									*															");
		oSqlBuilder.AppendLine("								FROM															");
		oSqlBuilder.AppendLine("									T_REFUSE													");
		oSqlBuilder.AppendLine("								WHERE															");
		oSqlBuilder.AppendLine("									SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("									USER_SEQ				= P.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("									USER_CHAR_NO			= P.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("									PARTNER_USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("									PARTNER_USER_CHAR_NO	= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("							)																	");
		oSqlBuilder.AppendLine("						ORDER BY CREATE_DATE DESC												");
		oSqlBuilder.AppendLine("					)																			");
		oSqlBuilder.AppendLine("				WHERE																			");
		oSqlBuilder.AppendLine("					ROWNUM <= 20																");
		oSqlBuilder.AppendLine("			)																					");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			GET_FLAG = :GET_FLAG																");
		oSqlBuilder.AppendLine("		UNION ALL																				");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																		");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_GAME_INTRO_LOG																	");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ														AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO													AND	");
		oSqlBuilder.AppendLine("			GET_FLAG		= :GET_FLAG															");
		oSqlBuilder.AppendLine("		UNION ALL																				");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																		");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01 P															");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ			= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO		= :USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			SUPPORT_END_FLAG			= :SUPPORT_END_FLAG									AND	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_END_FLAG	= :SUPPORT_REQUEST_END_FLAG							AND	");
		oSqlBuilder.AppendLine("			CHECK_DATE					IS NULL												AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS																			");
		oSqlBuilder.AppendLine("			(																					");
		oSqlBuilder.AppendLine("				SELECT																			");
		oSqlBuilder.AppendLine("					*																			");
		oSqlBuilder.AppendLine("				FROM																			");
		oSqlBuilder.AppendLine("					T_REFUSE																	");
		oSqlBuilder.AppendLine("				WHERE																			");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD										AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= P.USER_SEQ									AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= P.USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ		= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO	= :USER_CHAR_NO										");
		oSqlBuilder.AppendLine("			)																					");
		oSqlBuilder.AppendLine("		UNION ALL																				");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			CASE																				");
		oSqlBuilder.AppendLine("				WHEN																			");
		oSqlBuilder.AppendLine("					NOT EXISTS																	");
		oSqlBuilder.AppendLine("					(																			");
		oSqlBuilder.AppendLine("						SELECT																	");
		oSqlBuilder.AppendLine("							*																	");
		oSqlBuilder.AppendLine("						FROM																	");
		oSqlBuilder.AppendLine("							T_GAME_LOGIN_BONUS_DAY_LOG											");
		oSqlBuilder.AppendLine("						WHERE																	");
		oSqlBuilder.AppendLine("							SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("							USER_SEQ		= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("							USER_CHAR_NO	= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("							REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')						");
		oSqlBuilder.AppendLine("					)																			");
		oSqlBuilder.AppendLine("				THEN																			");
		oSqlBuilder.AppendLine("					1																			");
		oSqlBuilder.AppendLine("				ELSE																			");
		oSqlBuilder.AppendLine("					0																			");
		oSqlBuilder.AppendLine("			END REC																				");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			DUAL																				");
		oSqlBuilder.AppendLine("	)																							");
		oSqlBuilder.AppendLine("WHERE																							");
		oSqlBuilder.AppendLine("	REC > 0																						");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION));
		if (!string.IsNullOrEmpty(pLastHugLogCheckDate)) {
			oParamList.Add(new OracleParameter(":LAST_HUG_LOG_CHECK_DATE",pLastHugLogCheckDate));
		}
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":GET_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":SUPPORT_END_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":SUPPORT_REQUEST_END_FLAG",ViCommConst.FLAG_OFF_STR));

		OracleParameter[] oWhereParams = oParamList.ToArray();

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		
		return iRecCount;
	}

	public int GetInformationCountForCast(string pSiteCd,string pUserSeq,string pUserCharNo,string pLastHugLogCheckDate) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		int iRecCount = 0;
		
		oSqlBuilder.AppendLine("SELECT																						");
		oSqlBuilder.AppendLine("	COUNT(*)																				");
		oSqlBuilder.AppendLine("FROM																						");
		oSqlBuilder.AppendLine("	(																						");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			1 AS REC																		");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER																");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			UNASSIGNED_FORCE_COUNT		> 0													");
		oSqlBuilder.AppendLine("		UNION ALL																			");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																	");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			T_GAME_FELLOW																	");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			FELLOW_APPLICATION_STATUS	= :FELLOW_APPLICATION_STATUS						");
		oSqlBuilder.AppendLine("		UNION ALL																			");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																	");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION CM														,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER GC																");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			CM.SITE_CD					= GC.SITE_CD									AND	");
		oSqlBuilder.AppendLine("			CM.PARTNER_USER_SEQ			= GC.USER_SEQ									AND	");
		oSqlBuilder.AppendLine("			CM.PARTNER_USER_CHAR_NO		= GC.USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("			CM.SITE_CD					= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			CM.USER_SEQ					= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			CM.USER_CHAR_NO				= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			CM.PARTNER_HUG_COUNT		> 0												AND	");
		if (!string.IsNullOrEmpty(pLastHugLogCheckDate)) {
			oSqlBuilder.AppendLine("			TO_CHAR(CM.LAST_PARTNER_HUG_DATE,'YYYY/MM/DD HH24:MI:SS') > :LAST_HUG_LOG_CHECK_DATE	AND	");
		}
		oSqlBuilder.AppendLine("			GC.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG				AND	");
		oSqlBuilder.AppendLine("			GC.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("			GC.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG					AND	");
		oSqlBuilder.AppendLine("			GC.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG			");
		oSqlBuilder.AppendLine("		UNION ALL																			");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																	");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			(																				");
		oSqlBuilder.AppendLine("				SELECT																		");
		oSqlBuilder.AppendLine("					GET_FLAG																");
		oSqlBuilder.AppendLine("				FROM																		");
		oSqlBuilder.AppendLine("					(																		");
		oSqlBuilder.AppendLine("						SELECT																");
		oSqlBuilder.AppendLine("							GET_FLAG														");
		oSqlBuilder.AppendLine("						FROM																");
		oSqlBuilder.AppendLine("							VW_PW_MAN_GAME_PRE_HISTORY01 P									");
		oSqlBuilder.AppendLine("						WHERE																");
		oSqlBuilder.AppendLine("							SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("							PARTNER_USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("							PARTNER_USER_CHAR_NO	= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("							NA_FLAG					IN(:NA_FLAG,:NA_FLAG2)				AND	");
		oSqlBuilder.AppendLine("							NOT EXISTS														");
		oSqlBuilder.AppendLine("							(																");
		oSqlBuilder.AppendLine("								SELECT														");
		oSqlBuilder.AppendLine("									*														");
		oSqlBuilder.AppendLine("								FROM														");
		oSqlBuilder.AppendLine("									T_REFUSE												");
		oSqlBuilder.AppendLine("								WHERE														");
		oSqlBuilder.AppendLine("									SITE_CD					= P.SITE_CD					AND	");
		oSqlBuilder.AppendLine("									USER_SEQ				= P.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("									USER_CHAR_NO			= P.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("									PARTNER_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("									PARTNER_USER_CHAR_NO	= :USER_CHAR_NO					");
		oSqlBuilder.AppendLine("							)																");
		oSqlBuilder.AppendLine("					ORDER BY CREATE_DATE DESC												");
		oSqlBuilder.AppendLine("					)																		");
		oSqlBuilder.AppendLine("				WHERE																		");
		oSqlBuilder.AppendLine("					ROWNUM <= 20															");
		oSqlBuilder.AppendLine("			)																				");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			GET_FLAG = :GET_FLAG															");
		oSqlBuilder.AppendLine("		UNION ALL																			");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																	");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			T_GAME_INTRO_LOG																");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD												AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO											AND	");
		oSqlBuilder.AppendLine("			GET_FLAG			= :GET_FLAG													");
		oSqlBuilder.AppendLine("		UNION ALL																			");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			COUNT(*) AS REC																	");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01 P														");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_SEQ			= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			SUPPORT_USER_CHAR_NO		= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			SUPPORT_END_FLAG			= :SUPPORT_END_FLAG								AND	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_END_FLAG	= :SUPPORT_REQUEST_END_FLAG						AND	");
		oSqlBuilder.AppendLine("			CHECK_DATE IS NULL															AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS																		");
		oSqlBuilder.AppendLine("			(																				");
		oSqlBuilder.AppendLine("				SELECT																		");
		oSqlBuilder.AppendLine("					*																		");
		oSqlBuilder.AppendLine("				FROM																		");
		oSqlBuilder.AppendLine("					T_REFUSE																");
		oSqlBuilder.AppendLine("				WHERE																		");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD									AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= P.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= P.USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ		= :USER_SEQ									AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO	= :USER_CHAR_NO									");
		oSqlBuilder.AppendLine("			)																				");
		oSqlBuilder.AppendLine("		UNION ALL																			");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			1 AS REC																		");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			T_SOCIAL_GAME SG															,	");
		oSqlBuilder.AppendLine("			(																				");
		oSqlBuilder.AppendLine("				SELECT																		");
		oSqlBuilder.AppendLine("					SITE_CD																,	");
		oSqlBuilder.AppendLine("					PRESENT_COUNT															");
		oSqlBuilder.AppendLine("				FROM																		");
		oSqlBuilder.AppendLine("					T_ONCE_DAY_PRESENT_LOG													");
		oSqlBuilder.AppendLine("				WHERE																		");
		oSqlBuilder.AppendLine("					SITE_CD			= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("					USER_SEQ		= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO	= :USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("					REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')							");
		oSqlBuilder.AppendLine("			) PL																			");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			SG.SITE_CD						= PL.SITE_CD (+)							AND	");
		oSqlBuilder.AppendLine("			SG.FREE_PRESENT_LIMIT_COUNT		> NVL(PL.PRESENT_COUNT,0)						");
		oSqlBuilder.AppendLine("		UNION ALL																			");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			CASE																			");
		oSqlBuilder.AppendLine("				WHEN																		");
		oSqlBuilder.AppendLine("					NOT EXISTS																");
		oSqlBuilder.AppendLine("					(																		");
		oSqlBuilder.AppendLine("						SELECT																");
		oSqlBuilder.AppendLine("							*																");
		oSqlBuilder.AppendLine("						FROM																");
		oSqlBuilder.AppendLine("							T_GAME_LOGIN_BONUS_DAY_LOG										");
		oSqlBuilder.AppendLine("						WHERE																");
		oSqlBuilder.AppendLine("							SITE_CD			= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("							USER_SEQ		= :USER_SEQ									AND	");
		oSqlBuilder.AppendLine("							USER_CHAR_NO	= :USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("							REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')					");
		oSqlBuilder.AppendLine("					)																		");
		oSqlBuilder.AppendLine("				THEN																		");
		oSqlBuilder.AppendLine("					1																		");
		oSqlBuilder.AppendLine("				ELSE																		");
		oSqlBuilder.AppendLine("					0																		");
		oSqlBuilder.AppendLine("			END REC																			");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			DUAL																			");
		oSqlBuilder.AppendLine("	)																						");
		oSqlBuilder.AppendLine("WHERE																						");
		oSqlBuilder.AppendLine("	REC > 0																					");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION));
		if (!string.IsNullOrEmpty(pLastHugLogCheckDate)) {
			oParamList.Add(new OracleParameter(":LAST_HUG_LOG_CHECK_DATE",pLastHugLogCheckDate));
		}
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":GET_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":SUPPORT_END_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":SUPPORT_REQUEST_END_FLAG",ViCommConst.FLAG_OFF_STR));

		OracleParameter[] oWhereParams = oParamList.ToArray();

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		
		return iRecCount;
	}
}

