﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ｺﾐｭﾆｹｰｼｮﾝ
--	Progaram ID		: GameCommunication
--
--  Creation Date	: 2011.08.09
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class GameCommunicationSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}
	
	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public GameCommunicationSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameCommunicationSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
	}
}

[System.Serializable]
public class GameCommunication:DbSession {
	public GameCommunication() {
	}

	public DataSet GetOne(GameCommunicationSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	COMM.PARTNER_USER_SEQ										,	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("	COMM.SUPPORT_COUNT											,	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_SUPPORT_COUNT									,	");
		oSqlBuilder.AppendLine("	COMM.HUG_COUNT												,	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_HUG_COUNT										,	");
		oSqlBuilder.AppendLine("	COMM.PRESENT_COUNT											,	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_PRESENT_COUNT									,	");
		oSqlBuilder.AppendLine("	COMM.LAST_HUG_DATE											,	");
		oSqlBuilder.AppendLine("	CHR.GAME_HANDLE_NM											,	");
		oSqlBuilder.AppendLine("	CHR.GAME_CHARACTER_TYPE											");
		if (!string.IsNullOrEmpty(pCondition.SexCd) && pCondition.SexCd.Equals(ViCommConst.MAN)) {
			oSqlBuilder.AppendLine("	,	CC.HANDLE_NM											");
		}
		oSqlBuilder.AppendLine(" FROM																");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ									,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine("			SUPPORT_COUNT										,	");
		oSqlBuilder.AppendLine("			PARTNER_SUPPORT_COUNT								,	");
		oSqlBuilder.AppendLine("			HUG_COUNT											,	");
		oSqlBuilder.AppendLine("			PARTNER_HUG_COUNT									,	");
		oSqlBuilder.AppendLine("			PRESENT_COUNT										,	");
		oSqlBuilder.AppendLine("			PARTNER_PRESENT_COUNT								,	");
		oSqlBuilder.AppendLine("			LAST_HUG_DATE											");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION									");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");
		oSqlBuilder.AppendLine("	) COMM														,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER CHR											");
		if(!string.IsNullOrEmpty(pCondition.SexCd) && pCondition.SexCd.Equals(ViCommConst.MAN)) {
			oSqlBuilder.AppendLine("	,	T_CAST_CHARACTER CC");
		}
		oSqlBuilder.AppendLine(" WHERE																");
		oSqlBuilder.AppendLine("	COMM.SITE_CD				= CHR.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_USER_SEQ		= CHR.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_USER_CHAR_NO	= CHR.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	CHR.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	CHR.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	CHR.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	CHR.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");

		if (!string.IsNullOrEmpty(pCondition.SexCd) && pCondition.SexCd.Equals(ViCommConst.MAN)) {
			oSqlBuilder.AppendLine(" AND COMM.SITE_CD			= CC.SITE_CD			AND	");
			oSqlBuilder.AppendLine(" COMM.PARTNER_USER_SEQ		= CC.USER_SEQ			AND	");
			oSqlBuilder.AppendLine(" COMM.PARTNER_USER_CHAR_NO	= CC.USER_CHAR_NO			");
		}

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	public int GetPartnerHugUncheckedCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pLastHugLogCheckDate) {
		int iCount = 0;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION COMM		,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER GC					");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	COMM.SITE_CD				= GC.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_USER_SEQ		= GC.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_USER_CHAR_NO	= GC.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	COMM.SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	COMM.USER_SEQ				= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	COMM.USER_CHAR_NO			= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	COMM.PARTNER_HUG_COUNT		> 0						AND	");	
		oSqlBuilder.AppendLine("	GC.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		
		if (!string.IsNullOrEmpty(pLastHugLogCheckDate)) {
			oSqlBuilder.AppendLine("	AND TO_CHAR(LAST_PARTNER_HUG_DATE,'YYYY/MM/DD HH24:MI:SS') > :LAST_HUG_LOG_CHECK_DATE ");
			oParamList.Add(new OracleParameter(":LAST_HUG_LOG_CHECK_DATE",pLastHugLogCheckDate));
		}
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		iCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iCount;
	}

	public string HugGameCharacter(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("HUG_GAME_CHARACTER");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureOutParm("pHUG_BONUS_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}
}
