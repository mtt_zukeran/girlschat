﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性お宝
--	Progaram ID		: ManTreasure
--
--  Creation Date	: 2011.08.25
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;


[Serializable]
public class ManTreasureSeekCondition : SeekConditionBase {

	public string SiteCd {
		get { return this.Query["site"]; }
		set { this.Query["site"] = value; }
	}
	public string UserSeq {
		get { return this.Query["user_seq"]; }
		set { this.Query["user_seq"] = value; }
	}
	public string UserCharNo {
		get { return this.Query["user_char_no"]; }
		set { this.Query["user_char_no"] = value; }
	}
	public string DisplayDay {
		get { return this.Query["display_day"]; }
		set { this.Query["display_day"] = value; }
	}
	public string CastGamePicAttrSeq {
		get { return this.Query["cast_game_pic_attr_seq"]; }
		set { this.Query["cast_game_pic_attr_seq"] = value; }
	}
	public string CastGamePicSeq {
		get { return this.Query["cast_game_pic_seq"]; }
		set { this.Query["cast_game_pic_seq"] = value; }
	}
	public string PartnerUserSeq {
		get { return this.Query["partner_user_seq"]; }
		set { this.Query["partner_user_seq"] = value; }
	}
	public string PartnerUserCharNo {
		get { return this.Query["partner_user_char_no"]; }
		set { this.Query["partner_user_char_no"] = value; }
	}
	public string SortOrder {
		get { return this.Query["sort_order"]; }
		set { this.Query["sort_order"] = value; }
	}
	
	public string PossessionFlag {
		get { return this.Query["possession_flag"];}
		set { this.Query["possession_flag"] = value;}
	}
	public string ExceptTutorialNotPossessionFlag {
		get { return this.Query["except_tutorial_not_possession_flag"];}
		set { this.Query["except_tutorial_not_possession_flag"] = value;}
	}

	public ManTreasureSeekCondition() : this(new NameValueCollection()) { }
	public ManTreasureSeekCondition(NameValueCollection pQuery) : base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["display_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["display_day"]));
		this.Query["cast_game_pic_attr_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_game_pic_attr_seq"]));
		this.Query["cast_game_pic_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_game_pic_seq"]));
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["sort_order"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sort_order"]));
		this.Query["possession_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["possession_flag"]));
		this.Query["except_tutorial_not_possession_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["except_tutorial_not_possession_flag"]));
	}
}

[System.Serializable]
public class ManTreasure:CastBase
{
	public ManTreasure() {
	}

	public ManTreasure(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ManTreasureSeekCondition pCondition, int pRecPerPage, out decimal pRecCount) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_MAN_TREASURE01 P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder, oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManTreasureSeekCondition pCondition, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition, pPageNo, pRecPerPage);
	}

	private DataSet GetPageCollectionBase(ManTreasureSeekCondition pCondition,int pPageNo,int pRecPerPage)
	{
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	P.SITE_CD							,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_SEQ					,	");
		oSqlBuilder.AppendLine("	P.PIC_TITLE							,	");
		oSqlBuilder.AppendLine("	P.PIC_DOC							,	");
		oSqlBuilder.AppendLine("	P.HANDLE_NM							,	");
		oSqlBuilder.AppendLine("	P.AGE								,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	P.DISPLAY_DAY						,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_ATTR_NM				,	");
		oSqlBuilder.AppendLine("	P.OBJ_PHOTO_IMG_PATH				,	");
		oSqlBuilder.AppendLine("	P.OBJ_SMALL_PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("	P.OBJ_BLUR_PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("	P.OBJ_SMALL_BLUR_PHOTO_IMG_PATH		, 	");
		oSqlBuilder.AppendLine("	P.ACT_CATEGORY_IDX					,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID							,	");
		oSqlBuilder.AppendLine("	P.CRYPT_VALUE						,	");
		oSqlBuilder.AppendLine("	P.TREASURE_TUTORIAL_MISSION_FLAG	,	");
		oSqlBuilder.AppendLine("	P.TREASURE_TUTORIAL_BATTLE_FLAG		,	");
		oSqlBuilder.AppendLine("	P.PRIORITY							,	");
		oSqlBuilder.AppendLine("	CASE															");
		oSqlBuilder.AppendLine("		WHEN														");
		oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			OR	");
		oSqlBuilder.AppendLine("			P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	OR	");
		oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				OR	");
		oSqlBuilder.AppendLine("			P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		oSqlBuilder.AppendLine("		THEN														");
		oSqlBuilder.AppendLine("			1														");
		oSqlBuilder.AppendLine("		ELSE														");
		oSqlBuilder.AppendLine("			0														");
		oSqlBuilder.AppendLine("	END AS TUTORIAL_FLAG											");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	VW_PW_MAN_TREASURE01 P					");
		
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_ON_STR));

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition, ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		string sWrappedSql = string.Empty;
		oParamList.AddRange(this.CreatePossManTreasureSql(oSqlBuilder.ToString(),pCondition, out sWrappedSql));
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(sWrappedSql, sSortExpression, iStartIndex, pRecPerPage, out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql, oParamList.ToArray());
		return oDataSet;
	}

	public OracleParameter[] CreateWhere(ManTreasureSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD", ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.UserCharNo));

		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ	= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}
		
		if (!string.IsNullOrEmpty(pCondition.DisplayDay)) {
			SysPrograms.SqlAppendWhere(" P.DISPLAY_DAY	= :DISPLAY_DAY", ref pWhereClause);
			oParamList.Add(new OracleParameter(":DISPLAY_DAY", pCondition.DisplayDay));
		}

		if (!string.IsNullOrEmpty(pCondition.CastGamePicAttrSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_GAME_PIC_ATTR_SEQ	= :CAST_GAME_PIC_ATTR_SEQ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_GAME_PIC_ATTR_SEQ", pCondition.CastGamePicAttrSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastGamePicSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_GAME_PIC_SEQ	= :CAST_GAME_PIC_SEQ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ", pCondition.CastGamePicSeq));
		}
		
		if(!string.IsNullOrEmpty(pCondition.PossessionFlag)) {
			if(pCondition.PossessionFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere(" EXISTS (SELECT * FROM T_POSSESSION_MAN_TREASURE WHERE SITE_CD = :SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND CAST_GAME_PIC_SEQ = P.CAST_GAME_PIC_SEQ)",ref pWhereClause);
			} else {
				SysPrograms.SqlAppendWhere(" NOT EXISTS (SELECT * FROM T_POSSESSION_MAN_TREASURE WHERE SITE_CD = :SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND CAST_GAME_PIC_SEQ = P.CAST_GAME_PIC_SEQ)",ref pWhereClause);
			}
		}
		
		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND GAME_PIC_NON_PUBLISH_DATE < P.UPLOAD_DATE)",ref pWhereClause);
		
		if (pCondition.ExceptTutorialNotPossessionFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" ((TREASURE_TUTORIAL_MISSION_FLAG = :TREASURE_TUTORIAL_MISSION_FLAG AND TREASURE_TUTORIAL_BATTLE_FLAG = :TREASURE_TUTORIAL_BATTLE_FLAG) OR EXISTS(SELECT * FROM T_POSSESSION_MAN_TREASURE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND CAST_GAME_PIC_SEQ = P.CAST_GAME_PIC_SEQ))",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TREASURE_TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
			oParamList.Add(new OracleParameter(":TREASURE_TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		}

		SysPrograms.SqlAppendWhere(" P.PUBLISH_FLAG	= :PUBLISH_FLAG", ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG", ViCommConst.FLAG_ON_STR));

		SysPrograms.SqlAppendWhere(
			"(EXISTS(SELECT * FROM T_POSSESSION_MAN_TREASURE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND CAST_GAME_PIC_SEQ = P.CAST_GAME_PIC_SEQ) OR " +
			"(NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO) AND " +
			"NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND PARTNER_USER_SEQ = P.USER_SEQ AND PARTNER_USER_CHAR_NO = P.USER_CHAR_NO)))",
			ref pWhereClause
		);

		return oParamList.ToArray();
	}

	public string CreateOrderExpresion(ManTreasureSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		if (pCondition.SeekMode.Equals(PwViCommConst.INQUIRY_GAME_TREASURE_PARTNER)) {
			if (!string.IsNullOrEmpty(pCondition.SortOrder)) {
				sSortExpression = string.Format("ORDER BY P.DISPLAY_DAY {0}", pCondition.SortOrder);
			}else {
				sSortExpression = string.Format("ORDER BY P.DISPLAY_DAY DESC");
			}
		} else {
			sSortExpression = " ORDER BY PMT.POSSESSION_DATE DESC NULLS LAST";
		}

		return sSortExpression;
	}

	private OracleParameter[] CreatePossManTreasureSql(string pSql, ManTreasureSeekCondition pCondition, out string pWrappedSql) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																							");
		oSqlBuilder.AppendLine("	P.*																						,	");
		oSqlBuilder.AppendLine("	NVL(PMT.POSSESSION_COUNT	,'0')								AS	POSSESSION_COUNT	,	");
		oSqlBuilder.AppendLine("	NVL(PMT.MOSAIC_ERASE_FLAG	,'0')								AS	MOSAIC_ERASE_FLAG	,	");
		oSqlBuilder.AppendLine("	NVL(PMT.USED_TRAP_COUNT + PMT.USED_DOUBLE_TRAP_COUNT ,'0')		AS	USED_TRAP_COUNT			");
		oSqlBuilder.AppendLine("FROM																							");
		oSqlBuilder.AppendFormat("	( {0} )	P																				,	", pSql);
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("	SELECT																						");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.SITE_CD													,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.CAST_USER_SEQ												,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.CAST_CHAR_NO												,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.CAST_GAME_PIC_SEQ											,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.POSSESSION_COUNT											,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.MOSAIC_ERASE_FLAG											,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.USED_TRAP_COUNT											,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.USED_DOUBLE_TRAP_COUNT									,	");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.UPDATE_DATE					AS	POSSESSION_DATE				");
		oSqlBuilder.AppendLine("	FROM																						");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE																");
		oSqlBuilder.AppendLine("	WHERE																						");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.SITE_CD		= :PMT_SITE_CD							AND		");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.USER_SEQ		= :PMT_USER_SEQ							AND		");
		oSqlBuilder.AppendLine("		T_POSSESSION_MAN_TREASURE.USER_CHAR_NO	= :PMT_USER_CHAR_NO								");
		oSqlBuilder.AppendLine("	)	PMT																						");
		oSqlBuilder.AppendLine("WHERE																							");
		oSqlBuilder.AppendLine("	P.SITE_CD					= PMT.SITE_CD										(+) AND		");
		oSqlBuilder.AppendLine("	P.USER_SEQ					= PMT.CAST_USER_SEQ									(+) AND		");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO				= PMT.CAST_CHAR_NO									(+) AND		");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_SEQ			= PMT.CAST_GAME_PIC_SEQ								(+)			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":PMT_SITE_CD", pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":PMT_USER_SEQ", pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":PMT_USER_CHAR_NO", pCondition.UserCharNo));


		pWrappedSql = oSqlBuilder.ToString();
		return oParamList.ToArray();

	}

	public String GetTotalPossessionManTreasureCount(ManTreasureSeekCondition pCondition) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	SUM(POSSESSION_COUNT)	POSSESSION_COUNT				");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_POSSESSION_MAN_TREASURE								");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO					");
		oSqlBuilder.AppendLine(" GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO						");
		
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["POSSESSION_COUNT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public String GetManTreasureDisplayDay(string sCastGamePicSeq) {
		string sValue = null;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	DISPLAY_DAY										");
		oSqlBuilder.AppendLine(" FROM							   					");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE									");
		oSqlBuilder.AppendLine(" WHERE												");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ		= :CAST_GAME_PIC_SEQ	");
		
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":CAST_GAME_PIC_SEQ",sCastGamePicSeq);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["DISPLAY_DAY"].ToString());
				} 
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public void CheckManTreasureComplete(string pSiteCd,string pUserSeq,string pUserCharNo,string pDisplayDay) {
		
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_MAN_TREASURE_COMPLETE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pDISPLAY_DAY",DbSession.DbType.VARCHAR2,pDisplayDay);
			db.ProcedureOutParm("pNEW_COMPLETE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	
	}

	public DataSet GetCastSeqData(string pSiteCd,string pCastGamePicSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	USER_SEQ						,				");
		oSqlBuilder.AppendLine("	USER_CHAR_NO									");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE									");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ	= :CAST_GAME_PIC_SEQ	AND	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG		= :PUBLISH_FLAG				");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCastGamePicSeq));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetManTreasureSeqData(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	USER_SEQ						,						");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					,						");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ										");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE											");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG	AND	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY				= :DISPLAY_DAY					");
		oSqlBuilder.AppendLine(" ORDER BY DBMS_RANDOM.RANDOM								");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",DateTime.Now.ToString("yyyy/MM/dd")));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;

	}
	
	public DataSet GetManTreasureCompleteLogByDisplayDay(string pSiteCd,string pUserSeq,string pUserCharNo,string pDisplayDay) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	DISPLAY_DAY												");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_COMPLETE_LOG								");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY		= :DISPLAY_DAY					");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",pDisplayDay));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
	
	public void GetAimDisplayDay(string pSiteCd,string pUserSeq,string pUserCharNo,out string pDisplayDay,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_AIM_DISPLAY_DAY");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pDISPLAY_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			pDisplayDay = db.GetStringValue("pDISPLAY_DAY");
			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public DataSet GetOneGameTreasureData(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	MT.MAN_TREASURE_COUNT													,	");
		oSqlBuilder.AppendLine("	GCC.USE_MOSAIC_ERASE_PERSON_COUNT										,	");
		oSqlBuilder.AppendLine("	PMT.POSSESSION_PIC_PERSON_COUNT											,	");
		oSqlBuilder.AppendLine("	PMT2.POSSESSION_PIC_COUNT												,	");
		oSqlBuilder.AppendLine("	CM.MOVIE_COUNT															,	");
		oSqlBuilder.AppendLine("	GCC2.USE_MOVIE_TICKET_PERSON_COUNT										,	");
		oSqlBuilder.AppendLine("	GCC3.USE_MOVIE_TICKET_COUNT													");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			COUNT(*) AS MAN_TREASURE_COUNT										");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_MAN_TREASURE														");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			PUBLISH_FLAG	= :PUBLISH_FLAG										");
		oSqlBuilder.AppendLine("	) MT																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			COUNT(*) AS USE_MOSAIC_ERASE_PERSON_COUNT							");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION												");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			USE_MOSAIC_ERASE_COUNT	> 0											");
		oSqlBuilder.AppendLine("	) GCC																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			COUNT(*) AS POSSESSION_PIC_PERSON_COUNT								");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					1															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_POSSESSION_MAN_TREASURE PMT								");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD		= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("					EXISTS														");
		oSqlBuilder.AppendLine("					(															");
		oSqlBuilder.AppendLine("						SELECT													");
		oSqlBuilder.AppendLine("							*													");
		oSqlBuilder.AppendLine("						FROM													");
		oSqlBuilder.AppendLine("							T_MAN_TREASURE										");
		oSqlBuilder.AppendLine("						WHERE													");
		oSqlBuilder.AppendLine("							SITE_CD				= PMT.SITE_CD				AND	");
		oSqlBuilder.AppendLine("							CAST_GAME_PIC_SEQ	= PMT.CAST_GAME_PIC_SEQ		AND	");
		oSqlBuilder.AppendLine("							USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("							USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("							PUBLISH_FLAG		= :PUBLISH_FLAG					");
		oSqlBuilder.AppendLine("					)															");
		oSqlBuilder.AppendLine("				GROUP BY USER_SEQ,USER_CHAR_NO									");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("	) PMT																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			NVL(SUM(POSSESSION_COUNT),0) AS POSSESSION_PIC_COUNT				");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_POSSESSION_MAN_TREASURE PMT										");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			EXISTS																");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_MAN_TREASURE												");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD				= PMT.SITE_CD						AND	");
		oSqlBuilder.AppendLine("					CAST_GAME_PIC_SEQ	= PMT.CAST_GAME_PIC_SEQ				AND	");
		oSqlBuilder.AppendLine("					USER_SEQ			= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO		= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("					PUBLISH_FLAG		= :PUBLISH_FLAG							");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("	) PMT2																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			COUNT(*) AS MOVIE_COUNT												");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_CAST_MOVIE														");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			MOVIE_TYPE				= :MOVIE_TYPE							AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPROVE_FLAG					AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG						");
		oSqlBuilder.AppendLine("	) CM																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			COUNT(*) AS USE_MOVIE_TICKET_PERSON_COUNT							");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION												");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT	> 0											");
		oSqlBuilder.AppendLine("	) GCC2																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			NVL(SUM(USE_MOVIE_TICKET_COUNT),0) AS USE_MOVIE_TICKET_COUNT		");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION												");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT	> 0											");
		oSqlBuilder.AppendLine("	) GCC3																		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public string ModifyManTreasure(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastGamePicSeq,string pPicDoc) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_MAN_TREASURE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_GAME_PIC_SEQ",DbSession.DbType.VARCHAR2,pCastGamePicSeq);
			db.ProcedureInParm("pPIC_DOC",DbSession.DbType.VARCHAR2,pPicDoc);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public DataSet GetOneTutorialBattle(string pSiteCd,string pDisplayDay) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_SEQ										");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE												");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD						= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY					= :DISPLAY_DAY				AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG		= :TUTORIAL_BATTLE_FLAG			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",pDisplayDay));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_ON_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
