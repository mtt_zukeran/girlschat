﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: お友達紹介確認データクラス
--	Progaram ID		: IntroCheck
--
--  Creation Date	: 2015.12.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class IntroCheck:DbSession {
	public IntroCheck() {
	}
	
	public string GetUserIntroCountCheckDate(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sValue = string.Empty;
		DataSet oDataSet;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParmList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	USER_INTRO_COUNT_CHECK_DATE			");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_INTRO_CHECK						");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		");
		
		oParmList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParmList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParmList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		
		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParmList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["USER_INTRO_COUNT_CHECK_DATE"].ToString();
		}
		
		return sValue;
	}

	public void UpdateUserIntroCountCheck(string pSiteCd,string pUserSeq,string pUserCharNo) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_USER_INTRO_COUNT_CHECK");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}