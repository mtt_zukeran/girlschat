﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 応援依頼
--	Progaram ID		: SupportRequest
--
--  Creation Date	: 2011.09.05
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class CastSupportRequestSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string BattleLogSeq {
		get {
			return this.Query["battle_log_seq"];
		}
		set {
			this.Query["battle_log_seq"] = value;
		}
	}

	public string FriendApplicationStatus {
		get {
			return this.Query["friend_application_status"];
		}
		set {
			this.Query["friend_application_status"] = value;
		}
	}

	public CastSupportRequestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastSupportRequestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["battle_log_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["battle_log_seq"]));
		this.Query["friend_application_status"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["friend_application_status"]));
	}
}

#endregion ============================================================================================================

public class CastSupportRequest:CastBase {
	public CastSupportRequest() {
	}

	public CastSupportRequest(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public DataSet GetPageCollection(CastSupportRequestSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "								,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL									,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE									,	");
		oSqlBuilder.AppendLine("	FP.RANK								AS FRIENDLY_RANK		");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ AS SELF_USER_SEQ					,	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO AS SELF_USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("			" + CastBasicField() + "						,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_CHARACTER_LEVEL						,	");
		oSqlBuilder.AppendLine("			FELLOW.GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("			CASE												");
		oSqlBuilder.AppendLine("				WHEN											");
		oSqlBuilder.AppendLine("					F.OFFLINE_LAST_UPDATE_DATE IS NOT NULL	AND	");
		oSqlBuilder.AppendLine("					F.OFFLINE_LAST_UPDATE_DATE < SYSDATE		");
		oSqlBuilder.AppendLine("				THEN											");
		oSqlBuilder.AppendLine("					F.OFFLINE_LAST_UPDATE_DATE					");
		oSqlBuilder.AppendLine("				ELSE											");
		oSqlBuilder.AppendLine("					P.LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("			END VIEW_LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_SUPPORT_REQUEST TSR							,	");
		oSqlBuilder.AppendLine("			VW_PW_CAST_GAME_CHR_FLW_MBR02 FELLOW			,	");
		oSqlBuilder.AppendLine("			VW_PW_CAST_GAME_CHARACTER00 P					,	");
		oSqlBuilder.AppendLine("			T_SITE_MANAGEMENT TSM							,	");
		oSqlBuilder.AppendLine("		(																");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				SITE_CD												,	");
		oSqlBuilder.AppendLine("				USER_SEQ											,	");
		oSqlBuilder.AppendLine("				USER_CHAR_NO										,	");
		oSqlBuilder.AppendLine("				OFFLINE_LAST_UPDATE_DATE								");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_FAVORIT												");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("				WAITING_VIEW_STATUS		= :WAITING_VIEW_STATUS		AND	");
		oSqlBuilder.AppendLine("				LOGIN_VIEW_STATUS		= :LOGIN_VIEW_STATUS			");
		oSqlBuilder.AppendLine("		) F																");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ							= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO						= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			TSR.BATTLE_LOG_SEQ						= :BATTLE_LOG_SEQ					AND	");
		oSqlBuilder.AppendLine("			FELLOW.FELLOW_APPLICATION_STATUS		= :FELLOW_APPLICATION_STATUS		AND	");
		oSqlBuilder.AppendLine("		   (FELLOW.NA_FLAG							IN (:NA_FLAG,:NA_FLAG_2))			AND	");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= FELLOW.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			TSR.USER_SEQ							= FELLOW.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			TSR.USER_CHAR_NO						= FELLOW.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_SEQ					= FELLOW.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_CHAR_NO				= FELLOW.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= P.SITE_CD							AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_SEQ					= P.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_CHAR_NO				= P.USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			TSR.SITE_CD								= F.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_SEQ					= F.USER_SEQ					(+)	AND	");
		oSqlBuilder.AppendLine("			TSR.SUPPORT_USER_CHAR_NO				= F.USER_CHAR_NO				(+)	AND	");
		oSqlBuilder.AppendLine("			FELLOW.SITE_CD							= TSM.SITE_CD							");
		oSqlBuilder.AppendLine("	) P																,	");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00 FP													");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	P.SITE_CD						= FP.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	P.SELF_USER_SEQ					= FP.USER_SEQ					(+)	AND	");
		oSqlBuilder.AppendLine("	P.SELF_USER_CHAR_NO				= FP.USER_CHAR_NO				(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ						= FP.PARTNER_USER_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO					= FP.PARTNER_USER_CHAR_NO		(+)		");

		oParamList.AddRange(this.CreateWhere(pCondition));

		sSortExpression = "ORDER BY P.VIEW_LAST_LOGIN_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastSupportRequestSeekCondition pCondition) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pCondition.FriendApplicationStatus));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":WAITING_VIEW_STATUS","1"));
		oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS","1"));

		return oParamList.ToArray();
	}
}
