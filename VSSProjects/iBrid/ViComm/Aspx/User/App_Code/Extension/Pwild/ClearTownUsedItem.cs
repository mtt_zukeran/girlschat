﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾅﾝﾊﾟ時利用ｱｲﾃﾑ
--	Progaram ID		: ClearTownUsedItem
--
--  Creation Date	: 2011.09.14
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ClearTownUsedItemSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string SexCd;
	public string GameItemGetCd;
	public string TotalFlag;

	public string StageSeq {
		get {
			return this.Query["stage_seq"];
		}
		set {
			this.Query["stage_seq"] = value;
		}
	}

	public string TownSeq {
		get {
			return this.Query["town_seq"];
		}
		set {
			this.Query["town_seq"] = value;
		}
	}

	public ClearTownUsedItemSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ClearTownUsedItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["stage_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_seq"]));
		this.Query["town_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["town_seq"]));
	}
}

public class ClearTownUsedItem:DbSession {
	public ClearTownUsedItem() {
	}

	public DataSet GetList(
		string sSiteCd,
		string sSexCd,
		string sUserSeq,
		string sUserCharNo,
		string sStageSeq,
		string sTownSeq
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	U.GAME_ITEM_SEQ,								");
		oSqlBuilder.AppendLine("	I.GAME_ITEM_NM,									");
		oSqlBuilder.AppendLine("	I.PRICE,										");
		oSqlBuilder.AppendLine("	U.ITEM_COUNT AS USED_COUNT,						");
		oSqlBuilder.AppendLine("	NVL(P.POSSESSION_COUNT,0) AS POSSESSION_COUNT	");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		SITE_CD,									");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ,								");
		oSqlBuilder.AppendLine("		ITEM_COUNT									");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		T_CLEAR_TOWN_USED_ITEM						");
		oSqlBuilder.AppendLine("	WHERE											");
		oSqlBuilder.AppendLine("		SITE_CD		= :SITE_CD AND					");
		oSqlBuilder.AppendLine("		STAGE_SEQ	= :STAGE_SEQ AND				");
		oSqlBuilder.AppendLine("		TOWN_SEQ	= :TOWN_SEQ						");
		oSqlBuilder.AppendLine("	) U,											");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		SITE_CD,									");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ,								");
		oSqlBuilder.AppendLine("		GAME_ITEM_NM,								");
		oSqlBuilder.AppendLine("		PRICE										");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		T_GAME_ITEM									");
		oSqlBuilder.AppendLine("	WHERE											");
		oSqlBuilder.AppendLine("		SITE_CD			= :SITE_CD AND				");
		oSqlBuilder.AppendLine("		SEX_CD			= :SEX_CD AND				");
		oSqlBuilder.AppendLine("		PUBLISH_FLAG	= :PUBLISH_FLAG				");
		oSqlBuilder.AppendLine("	) I,											");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		SITE_CD,									");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ,								");
		oSqlBuilder.AppendLine("		POSSESSION_COUNT							");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		T_POSSESSION_GAME_ITEM						");
		oSqlBuilder.AppendLine("	WHERE											");
		oSqlBuilder.AppendLine("		SITE_CD			= :SITE_CD AND				");
		oSqlBuilder.AppendLine("		USER_SEQ		= :USER_SEQ AND				");
		oSqlBuilder.AppendLine("		USER_CHAR_NO	= :USER_CHAR_NO				");
		oSqlBuilder.AppendLine("	) P												");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	U.SITE_CD		= I.SITE_CD AND					");
		oSqlBuilder.AppendLine("	U.GAME_ITEM_SEQ	= I.GAME_ITEM_SEQ AND			");
		oSqlBuilder.AppendLine("	U.SITE_CD		= P.SITE_CD (+) AND				");
		oSqlBuilder.AppendLine("	U.GAME_ITEM_SEQ = P.GAME_ITEM_SEQ (+)			");
		oSqlBuilder.AppendLine("ORDER BY											");
		oSqlBuilder.AppendLine("  GAME_ITEM_SEQ										");

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",sSexCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":STAGE_SEQ",sStageSeq));
		oParamList.Add(new OracleParameter(":TOWN_SEQ",sTownSeq));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionUsedItemShortage(ClearTownUsedItemSeekCondition pCondition) {
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																				");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_SEQ															,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_NM															,	");
		oSqlBuilder.AppendLine("	ITEM.PRICE																	,	");
		oSqlBuilder.AppendLine("	ITEM.PRICE AS FIXED_PRICE													,	");
		oSqlBuilder.AppendLine("	0 AS SALE_FLAG																,	");
		oSqlBuilder.AppendLine("	0 AS USED_ITEM_TOTAL_PRICE													,	");
		oSqlBuilder.AppendLine("	ITEM.ITEM_COUNT																,	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_GET_CD														,	");
		oSqlBuilder.AppendLine("	ITEM.ITEM_COUNT - NVL(POSSESSION.POSSESSION_COUNT,0) AS USED_ITEM_SHORTAGE	,	");
		oSqlBuilder.AppendLine("	0 AS CLEAR_TOWN_GET_ITEM_NO														");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	VW_PW_CLEAR_TOWN_USED_ITEM01 ITEM											,	");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			SITE_CD																,	");
		oSqlBuilder.AppendLine("			GAME_ITEM_SEQ														,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT														");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_POSSESSION_GAME_ITEM POSSESSION										");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO											");
		oSqlBuilder.AppendLine("	) POSSESSION																	");
		oSqlBuilder.AppendLine("WHERE																				");
		oSqlBuilder.AppendLine("	ITEM.SITE_CD		= POSSESSION.SITE_CD								(+)	AND	");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_SEQ	= POSSESSION.GAME_ITEM_SEQ							(+)	AND	");
		oSqlBuilder.AppendLine("	ITEM.SITE_CD		= :SITE_CD												AND	");
		oSqlBuilder.AppendLine("	ITEM.STAGE_SEQ		= :STAGE_SEQ											AND	");
		oSqlBuilder.AppendLine("	ITEM.TOWN_SEQ		= :TOWN_SEQ												AND	");
		oSqlBuilder.AppendLine("	ITEM.SEX_CD			= :SEX_CD												AND	");
		oSqlBuilder.AppendLine("	ITEM.PUBLISH_FLAG	= :PUBLISH_FLAG											AND	");
		oSqlBuilder.AppendLine("	ITEM.ITEM_COUNT		> NVL(POSSESSION.POSSESSION_COUNT,0)						");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":STAGE_SEQ",pCondition.StageSeq));
		oParamList.Add(new OracleParameter(":TOWN_SEQ",pCondition.TownSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		
		if(!string.IsNullOrEmpty(pCondition.GameItemGetCd)) {
			oSqlBuilder.AppendLine("	AND	ITEM.GAME_ITEM_GET_CD = :GAME_ITEM_GET_CD");
			oParamList.Add(new OracleParameter(":GAME_ITEM_GET_CD",pCondition.GameItemGetCd));
		}
		
		oSqlBuilder.AppendLine("ORDER BY ITEM.GAME_ITEM_GET_CD NULLS LAST,GAME_ITEM_SEQ");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		GameItemSeekCondition pGameItemCondition = new GameItemSeekCondition();
		pGameItemCondition.SiteCd = pCondition.SiteCd;
		pGameItemCondition.SexCd = pCondition.SexCd;
		using (GameItem oGameItem = new GameItem()) {
			oGameItem.AppendAttrGameItemSale(oDataSet,pGameItemCondition);
		}
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			int iPrice;
			int iUsedItemShortage;
			int iClearTownGetItemNo = 1;
			foreach(DataRow oDR in oDataSet.Tables[0].Rows) {
				int.TryParse(oDR["PRICE"].ToString(),out iPrice);
				int.TryParse(oDR["USED_ITEM_SHORTAGE"].ToString(),out iUsedItemShortage);
				oDR["USED_ITEM_TOTAL_PRICE"] = iBridUtil.GetStringValue(iPrice * iUsedItemShortage);

				if (string.IsNullOrEmpty(oDR["GAME_ITEM_GET_CD"].ToString())) {
					oDR["CLEAR_TOWN_GET_ITEM_NO"] = iClearTownGetItemNo;
					iClearTownGetItemNo++;
				}
			}
		}
		
		if(!string.IsNullOrEmpty(pCondition.TotalFlag) && pCondition.TotalFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			if(oDataSet.Tables[0].Rows.Count > 1) {
				int iTotalPrice = 0;
				DataSet oTotalDS = new DataSet();
				DataTable oTotalDT = new DataTable();
				DataRow oTotalDR;
				oTotalDS.Tables.Add(oTotalDT);

				oTotalDT.Columns.Add("USED_ITEM_TOTAL_PRICE",Type.GetType("System.String"));
				
				foreach(DataRow oDR in oDataSet.Tables[0].Rows) {
					iTotalPrice += int.Parse(oDR["USED_ITEM_TOTAL_PRICE"].ToString());
				}

				oTotalDR = oTotalDT.NewRow();
				oTotalDR["USED_ITEM_TOTAL_PRICE"] = iTotalPrice.ToString();
				oTotalDT.Rows.Add(oTotalDR);
				
				return oTotalDS;
			} else {
				return null;
			}
		} else {
			return oDataSet;
		}
	}
}
