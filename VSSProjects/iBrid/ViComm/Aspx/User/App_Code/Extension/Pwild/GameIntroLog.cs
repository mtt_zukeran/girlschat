﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・友達紹介記録
--	Progaram ID		: GameIntroLog
--
--  Creation Date	: 2012.02.07
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class GameIntroLogSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public string GetFlag {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public GameIntroLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameIntroLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["get_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["get_flag"]));
	}
}

public class GameIntroLog:DbSession {
	public GameIntroLog() {
	}

	public int GetRecCount(GameIntroLogSeekCondition pCondtion) {
		int iRecCount = 0;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine("FROM				");
		oSqlBuilder.AppendLine("	T_GAME_INTRO_LOG");

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return iRecCount;
	}
	
	public int GetPageCount(GameIntroLogSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		pRecCount = this.GetRecCount(pCondtion);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameIntroLogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(GameIntroLogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	SITE_CD				,	");
		oSqlBuilder.AppendLine("	GAME_INTRO_LOG_SEQ	,	");
		oSqlBuilder.AppendLine("	CREATE_DATE			,	");
		oSqlBuilder.AppendLine("	GAME_INTRO_SEQ		,	");
		oSqlBuilder.AppendLine("	INTRO_COMMENT		,	");
		oSqlBuilder.AppendLine("	0 AS REC_NO_PER_PAGE	");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_GAME_INTRO_LOG01	");

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		int iRecNoPerPage = 0;
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow oDR in oDataSet.Tables[0].Rows) {
				iRecNoPerPage = (int.Parse(oDR["RNUM"].ToString()) - iStartIndex);
				oDR["REC_NO_PER_PAGE"] = iRecNoPerPage.ToString();
			}
		}

		return oDataSet;
	}

	public DataSet GetOneIntroLog(GameIntroLogSeekCondition pCondtion) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.*									");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	(SELECT								");
		oSqlBuilder.AppendLine("		NULL AS GAME_HANDLE_NM		,	");
		oSqlBuilder.AppendLine("		NULL AS HANDLE_NM			,	");
		oSqlBuilder.AppendLine("		CREATE_DATE						");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_GAME_INTRO_LOG01 P		");
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine(" 	) P									");
		oSqlBuilder.AppendLine("	WHERE ROWNUM <= 1	  				");
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	private OracleParameter[] CreateWhere(GameIntroLogSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}
		
		if (!string.IsNullOrEmpty(pCondition.GetFlag)) {
			SysPrograms.SqlAppendWhere("GET_FLAG = :GET_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GET_FLAG",pCondition.GetFlag));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(GameIntroLogSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY CREATE_DATE DESC";

		return sSortExpression;
	}

	public string LogGameIntro(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd) {
		string sResult;
		
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_GAME_INTRO");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}
		
		return sResult;
	}
}
