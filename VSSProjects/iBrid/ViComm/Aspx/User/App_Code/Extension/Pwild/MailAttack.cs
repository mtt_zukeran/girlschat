﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: アタックリスト
--	Progaram ID		: MailAttack
--
--  Creation Date	: 2013.04.05
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class MailAttackSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string LastLoginDateFrom;
	public string LastLoginDateTo;
	public string NonExistMailAddrFlag;
	public string CastMailRxType;
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public string NoRxMail24h {
		get {
			return this.Query["norxmail24h"];
		}
		set {
			this.Query["norxmail24h"] = value;
		}
	}

	public MailAttackSeekCondition()
		: this(new NameValueCollection()) {
	}

	public MailAttackSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["norxmail24h"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["norxmail24h"]));
	}
}

public class MailAttack:DbSession {
	public MailAttack() {
	}

	public int GetPageCount(MailAttackSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClauseUserManCharacter = string.Empty;
		string sWhereClauseUser = string.Empty;
		string sWhereClauseCommunication = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.SelfUserSeq) || string.IsNullOrEmpty(pCondition.SelfUserCharNo)) {
			pRecCount = 0;
			return 0;
		}
		
		oParamList.AddRange(this.CreateWhereUserManCharacter(pCondition,ref sWhereClauseUserManCharacter));
		oParamList.AddRange(this.CreateWhereUser(pCondition,ref sWhereClauseUser));
		oParamList.AddRange(this.CreateWhereCommunication(pCondition,ref sWhereClauseCommunication));

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	COUNT(*)																");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			P.SITE_CD													,	");
		oSqlBuilder.AppendLine("			P.USER_SEQ													,	");
		oSqlBuilder.AppendLine("			P.USER_CHAR_NO													");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER P											");
		oSqlBuilder.AppendLine(sWhereClauseUserManCharacter);
		oSqlBuilder.AppendLine("	) P																	,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			U.USER_SEQ														");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER U														");
		oSqlBuilder.AppendLine(sWhereClauseUser);
		oSqlBuilder.AppendLine("	) U																	,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			C.SITE_CD													,	");
		oSqlBuilder.AppendLine("			C.USER_SEQ													,	");
		oSqlBuilder.AppendLine("			C.USER_CHAR_NO													");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_COMMUNICATION C												");
		oSqlBuilder.AppendLine(sWhereClauseCommunication);
		oSqlBuilder.AppendLine("	) C																		");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	P.USER_SEQ				= U.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD				= C.SITE_CD									AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ				= C.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO			= C.USER_CHAR_NO								");

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(MailAttackSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		DataSet oDataSet = null;
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClauseUserManCharacter = string.Empty;
		string sWhereClauseUser = string.Empty;
		string sWhereClauseCommunication = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		if (string.IsNullOrEmpty(pCondition.SiteCd) || string.IsNullOrEmpty(pCondition.SelfUserSeq) || string.IsNullOrEmpty(pCondition.SelfUserCharNo)) {
			return oDataSet;
		}

		oParamList.AddRange(this.CreateWhereUserManCharacter(pCondition,ref sWhereClauseUserManCharacter));
		oParamList.AddRange(this.CreateWhereUser(pCondition,ref sWhereClauseUser));
		oParamList.AddRange(this.CreateWhereCommunication(pCondition,ref sWhereClauseCommunication));

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	P.SITE_CD															,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ															,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO														,	");
		oSqlBuilder.AppendLine("	P.HANDLE_NM															,	");
		oSqlBuilder.AppendLine("	P.AGE																,	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID															,	");
		oSqlBuilder.AppendLine("	P.CAST_MAIL_RX_TYPE													,	");
		oSqlBuilder.AppendLine("	P.NA_FLAG															,	");
		oSqlBuilder.AppendLine("	C.TOTAL_TALK_COUNT													,	");
		oSqlBuilder.AppendLine("	C.TOTAL_TX_MAIL_COUNT												,	");
		oSqlBuilder.AppendLine("	C.LAST_TALK_DATE													,	");
		oSqlBuilder.AppendLine("	C.LAST_TX_MAIL_DATE													,	");
		oSqlBuilder.AppendLine("	C.REGIST_FAVORITE_DATE_BY_MAN										,	");
		oSqlBuilder.AppendLine("	C.LAST_MARKING_DATE_BY_MAN											,	");
		oSqlBuilder.AppendLine("	C.LAST_MAIL_READ_DATE_BY_MAN										,	");
		oSqlBuilder.AppendLine("	C.PARTNER_LAST_RX_MAIL_DATE											,	");
		oSqlBuilder.AppendLine("	C.PARTNER_TOTAL_RX_MAIL_COUNT										,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			NVL(COUNT(USER_SEQ),0) AS CNT									");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_FAVORIT														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD							AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	) AS LIKE_ME_FLAG													,	");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(P.SITE_CD,U.LOGIN_ID,P.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			P.SITE_CD													,	");
		oSqlBuilder.AppendLine("			P.USER_SEQ													,	");
		oSqlBuilder.AppendLine("			P.USER_CHAR_NO												,	");
		oSqlBuilder.AppendLine("			P.HANDLE_NM													,	");
		oSqlBuilder.AppendLine("			P.AGE														,	");
		oSqlBuilder.AppendLine("			P.CAST_MAIL_RX_TYPE											,	");
		oSqlBuilder.AppendLine("			P.NA_FLAG													,	");
		oSqlBuilder.AppendLine("			P.PIC_SEQ														");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER P											");
		oSqlBuilder.AppendLine(sWhereClauseUserManCharacter);
		oSqlBuilder.AppendLine("	) P																	,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			U.USER_SEQ													,	");
		oSqlBuilder.AppendLine("			U.LOGIN_ID													,	");
		oSqlBuilder.AppendLine("			U.LAST_LOGIN_DATE											,	");
		oSqlBuilder.AppendLine("			U.NON_EXIST_MAIL_ADDR_FLAG										");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER U														");
		oSqlBuilder.AppendLine(sWhereClauseUser);
		oSqlBuilder.AppendLine("	) U																	,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			C.SITE_CD													,	");
		oSqlBuilder.AppendLine("			C.USER_SEQ													,	");
		oSqlBuilder.AppendLine("			C.USER_CHAR_NO												,	");
		oSqlBuilder.AppendLine("			NVL(C.TOTAL_TALK_COUNT,0) AS TOTAL_TALK_COUNT				,	");
		oSqlBuilder.AppendLine("			NVL(C.TOTAL_TX_MAIL_COUNT,0) AS TOTAL_TX_MAIL_COUNT			,	");
		oSqlBuilder.AppendLine("			C.LAST_TALK_DATE											,	");
		oSqlBuilder.AppendLine("			C.LAST_TX_MAIL_DATE											,	");
		oSqlBuilder.AppendLine("			C.REGIST_FAVORITE_DATE_BY_MAN								,	");
		oSqlBuilder.AppendLine("			C.LAST_MARKING_DATE_BY_MAN									,	");
		oSqlBuilder.AppendLine("			C.LAST_MAIL_READ_DATE_BY_MAN								,	");
		oSqlBuilder.AppendLine("			C.LAST_RX_MAIL_DATE AS PARTNER_LAST_RX_MAIL_DATE			,	");
		oSqlBuilder.AppendLine("			NVL(C.TOTAL_RX_MAIL_COUNT,0) AS PARTNER_TOTAL_RX_MAIL_COUNT		");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_COMMUNICATION C												");
		oSqlBuilder.AppendLine(sWhereClauseCommunication);
		oSqlBuilder.AppendLine("	) C																		");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	P.USER_SEQ				= U.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD				= C.SITE_CD									AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ				= C.USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO			= C.USER_CHAR_NO								");

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		sSortExpression = CreateOrderExpression(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhereUserManCharacter(MailAttackSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		if (!string.IsNullOrEmpty(pCondition.SelfUserSeq) && !string.IsNullOrEmpty(pCondition.SelfUserCharNo)) {
			StringBuilder oSqlBuilder = new StringBuilder();

			oSqlBuilder.Append("NOT EXISTS (");
			oSqlBuilder.Append("SELECT 1 FROM T_REFUSE");
			oSqlBuilder.Append(" WHERE");
			oSqlBuilder.Append(" T_REFUSE.SITE_CD = P.SITE_CD");
			oSqlBuilder.Append(" AND T_REFUSE.USER_SEQ = P.USER_SEQ");
			oSqlBuilder.Append(" AND T_REFUSE.USER_CHAR_NO = P.USER_CHAR_NO");
			oSqlBuilder.Append(" AND T_REFUSE.PARTNER_USER_SEQ = :REFUSED_USER_SEQ");
			oSqlBuilder.Append(" AND T_REFUSE.PARTNER_USER_CHAR_NO = :REFUSED_USER_CHAR_NO");
			oSqlBuilder.Append(")");
			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhereClause);

			oParamList.Add(new OracleParameter(":REFUSED_USER_SEQ",pCondition.SelfUserSeq));
			oParamList.Add(new OracleParameter(":REFUSED_USER_CHAR_NO",pCondition.SelfUserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastMailRxType)) {
			SysPrograms.SqlAppendWhere("P.CAST_MAIL_RX_TYPE = :CAST_MAIL_RX_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_MAIL_RX_TYPE",pCondition.CastMailRxType));
		}

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereUser(MailAttackSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.LastLoginDateFrom)) {
			DateTime dtLastLoginDateFrom;
			if (DateTime.TryParse(pCondition.LastLoginDateFrom,out dtLastLoginDateFrom)) {
				SysPrograms.SqlAppendWhere("U.LAST_LOGIN_DATE >= :LAST_LOGIN_DATE_FROM",ref pWhereClause);
				oParamList.Add(new OracleParameter(":LAST_LOGIN_DATE_FROM",OracleDbType.Date,dtLastLoginDateFrom,ParameterDirection.Input));
			}
		}

		if (!string.IsNullOrEmpty(pCondition.LastLoginDateTo)) {
			DateTime dtLastLoginDateTo;
			if (DateTime.TryParse(pCondition.LastLoginDateTo,out dtLastLoginDateTo)) {
				SysPrograms.SqlAppendWhere("U.LAST_LOGIN_DATE <= :LAST_LOGIN_DATE_TO",ref pWhereClause);
				oParamList.Add(new OracleParameter(":LAST_LOGIN_DATE_TO",OracleDbType.Date,DateTime.Parse(pCondition.LastLoginDateTo),ParameterDirection.Input));
			}
		}

		if (!string.IsNullOrEmpty(pCondition.NonExistMailAddrFlag)) {
			SysPrograms.SqlAppendWhere("U.NON_EXIST_MAIL_ADDR_FLAG = :NON_EXIST_MAIL_ADDR_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NON_EXIST_MAIL_ADDR_FLAG",pCondition.NonExistMailAddrFlag));
		}

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereCommunication(MailAttackSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("C.SITE_CD = :COMM_SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":COMM_SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SelfUserSeq) && !string.IsNullOrEmpty(pCondition.SelfUserCharNo)) {
			SysPrograms.SqlAppendWhere("C.PARTNER_USER_SEQ = :COMM_USER_SEQ",ref pWhereClause);
			SysPrograms.SqlAppendWhere("C.PARTNER_USER_CHAR_NO = :COMM_USER_CHAR_NO",ref pWhereClause);
			
			StringBuilder oSqlBuilder = new StringBuilder();

			oSqlBuilder.Append("(C.TOTAL_TALK_COUNT > 0");
			oSqlBuilder.Append(" OR C.TOTAL_TX_MAIL_COUNT > 0");
			oSqlBuilder.Append(" OR C.LAST_MARKING_DATE_BY_MAN IS NOT NULL");
			oSqlBuilder.Append(" OR C.LAST_MAIL_READ_DATE_BY_MAN IS NOT NULL");
			oSqlBuilder.Append(" OR EXISTS(SELECT 1 FROM T_FAVORIT WHERE SITE_CD = C.SITE_CD AND USER_SEQ = C.USER_SEQ AND USER_CHAR_NO = C.USER_CHAR_NO AND PARTNER_USER_SEQ = :COMM_USER_SEQ AND PARTNER_USER_CHAR_NO = :COMM_USER_CHAR_NO)");
			oSqlBuilder.Append(")");
			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhereClause);
			
			oParamList.Add(new OracleParameter(":COMM_USER_SEQ",pCondition.SelfUserSeq));
			oParamList.Add(new OracleParameter(":COMM_USER_CHAR_NO",pCondition.SelfUserCharNo));
		}

		if (pCondition.NoRxMail24h.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("(C.LAST_RX_MAIL_DATE <= :LAST_RX_MAIL_DATE OR C.LAST_RX_MAIL_DATE IS NULL)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LAST_RX_MAIL_DATE",OracleDbType.Date,DateTime.Now.AddDays(-1),ParameterDirection.Input));
		}

		return oParamList.ToArray();
	}
	
	private string CreateOrderExpression(MailAttackSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case (PwViCommConst.MailAttackSort.LAST_LOGIN_DATE): {
				sSortExpression = " ORDER BY U.LAST_LOGIN_DATE DESC,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.LAST_TALK_DATE): {
				sSortExpression = " ORDER BY C.LAST_TALK_DATE DESC NULLS LAST,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.LAST_TX_MAIL_DATE): {
				sSortExpression = " ORDER BY C.LAST_TX_MAIL_DATE DESC NULLS LAST,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.REGIST_FAVORITE_DATE_BY_MAN): {
					sSortExpression = " ORDER BY C.REGIST_FAVORITE_DATE_BY_MAN DESC NULLS LAST,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.LAST_MARKING_DATE_BY_MAN) : {
					sSortExpression = " ORDER BY C.LAST_MARKING_DATE_BY_MAN DESC NULLS LAST,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.LAST_MAIL_READ_DATE_BY_MAN): {
					sSortExpression = " ORDER BY C.LAST_MAIL_READ_DATE_BY_MAN DESC NULLS LAST,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.TOTAL_RX_MAIL_COUNT): {
				sSortExpression = " ORDER BY C.PARTNER_TOTAL_RX_MAIL_COUNT DESC,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.LAST_RX_MAIL_DATE): {
				sSortExpression = " ORDER BY C.PARTNER_LAST_RX_MAIL_DATE DESC NULLS LAST,P.USER_SEQ";
				break;
			}
			case (PwViCommConst.MailAttackSort.LAST_RX_MAIL_DATE_ASC): {
				sSortExpression = " ORDER BY C.PARTNER_LAST_RX_MAIL_DATE ASC NULLS FIRST,P.USER_SEQ";
				break;
			}
			default:
				sSortExpression = " ORDER BY U.LAST_LOGIN_DATE DESC,P.USER_SEQ";
				break;
		}
		return sSortExpression;
	}
}
