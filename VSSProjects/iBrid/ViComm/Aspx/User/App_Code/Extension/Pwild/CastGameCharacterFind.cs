﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ検索
--	Progaram ID		: CastGameCharacterFind
--
--  Creation Date	: 2011.08.10
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterFindSeekCondition:SeekConditionBase {

	public List<string> attrTypeSeq;
	public List<string> attrValue;
	public List<string> groupingFlag;
	public List<string> likeSearchFlag;
	public List<string> notEqualFlag;

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string ExtTreasurePic {
		get {
			return this.Query["ext_treasure_pic"];
		}
		set {
			this.Query["ext_treasure_pic"] = value;
		}
	}

	public string TreasurePic {
		get {
			return this.Query["treasure_pic"];
		}
		set {
			this.Query["treasure_pic"] = value;
		}
	}

	public string ExtTreasureMovie {
		get {
			return this.Query["ext_treasure_movie"];
		}
		set {
			this.Query["ext_treasure_movie"] = value;
		}
	}

	public string TreasureMovie {
		get {
			return this.Query["treasure_movie"];
		}
		set {
			this.Query["treasure_movie"] = value;
		}
	}

	public string ExtRelation {
		get {
			return this.Query["ext_relation"];
		}
		set {
			this.Query["ext_relation"] = value;
		}
	}

	public string Relation {
		get {
			return this.Query["relation"];
		}
		set {
			this.Query["relation"] = value;
		}
	}

	public string ExtStartLevel {
		get {
			return this.Query["ext_start_level"];
		}
		set {
			this.Query["ext_start_level"] = value;
		}
	}

	public string StartLevel {
		get {
			return this.Query["start_level"];
		}
		set {
			this.Query["start_level"] = value;
		}
	}

	public string ExtEndLevel {
		get {
			return this.Query["ext_end_level"];
		}
		set {
			this.Query["ext_end_level"] = value;
		}
	}

	public string EndLevel {
		get {
			return this.Query["end_level"];
		}
		set {
			this.Query["end_level"] = value;
		}
	}

	public string Online {
		get {
			return this.Query["online"];
		}
		set {
			this.Query["online"] = value;
		}
	}

	public string NewCast {
		get {
			return this.Query["newcast"];
		}
		set {
			this.Query["newcast"] = value;
		}
	}

	public string NewCastDay {
		get {
			return this.Query["newcastday"];
		}
		set {
			this.Query["newcastday"] = value;
		}
	}
	
	public string Handle {
		get {
			return this.Query["handle"];
		}
		set {
			this.Query["handle"] = value;
		}
	}

	public string Agefrom {
		get {
			return this.Query["agefrom"];
		}
		set {
			this.Query["agefrom"] = value;
		}
	}

	public string Ageto {
		get {
			return this.Query["ageto"];
		}
		set {
			this.Query["ageto"] = value;
		}
	}

	public string Comment {
		get {
			return this.Query["comment"];
		}
		set {
			this.Query["comment"] = value;
		}
	}

	public string GameType {
		get {
			return this.Query["game_type"];
		}
		set {
			this.Query["game_type"] = value;
		}
	}
	
	public string CastTodayFlag {
		get {
			return this.Query["cast_today_flag"];
		}
		set {
			this.Query["cast_today_flag"] = value;
		}
	}

	public CastGameCharacterFindSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterFindSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {

		attrTypeSeq = new List<string>();
		attrValue = new List<string>();
		groupingFlag = new List<string>();
		likeSearchFlag = new List<string>();
		notEqualFlag = new List<string>();

		this.Query["ext_treasure_pic"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_treasure_pic"]));
		this.Query["treasure_pic"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["treasure_pic"]));
		this.Query["ext_treasure_movie"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_treasure_movie"]));
		this.Query["treasure_movie"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["treasure_movie"]));
		this.Query["ext_relation"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_relation"]));
		this.Query["relation"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["relation"]));
		this.Query["ext_start_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_start_level"]));
		this.Query["start_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["start_level"]));
		this.Query["ext_end_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_end_level"]));
		this.Query["end_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["end_level"]));
		this.Query["online"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["online"]));
		this.Query["newcast"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["newcast"]));
		this.Query["handle"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["handle"]));
		this.Query["agefrom"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["agefrom"]));
		this.Query["ageto"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ageto"]));
		this.Query["comment"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["comment"]));
		this.Query["game_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_type"]));
		this.Query["cast_today_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_today_flag"]));

		for (int i = 0;i < ViCommConst.MAX_ATTR_COUNT;i++) {
			this.attrTypeSeq.Add(iBridUtil.GetStringValue(pQuery[string.Format("typeseq{0:D2}",i + 1)]));
			this.attrValue.Add(iBridUtil.GetStringValue(pQuery[string.Format("item{0:D2}",i + 1)]));
			this.groupingFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("group{0:D2}",i + 1)]));
			this.likeSearchFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("like{0:D2}",i + 1)]));
			this.notEqualFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("noteq{0:D2}",i + 1)]));

			this.Query[string.Format("typeseq{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("typeseq{0:D2}",i + 1)]));
			this.Query[string.Format("item{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("item{0:D2}",i + 1)]));
			this.Query[string.Format("group{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("group{0:D2}",i + 1)]));
			this.Query[string.Format("like{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("like{0:D2}",i + 1)]));
			this.Query[string.Format("noteq{0:D2}",i + 1)] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery[string.Format("noteq{0:D2}",i + 1)]));
		}
	}
}

[System.Serializable]
public class CastGameCharacterFind:CastBase {
	public CastGameCharacterFind()
		: base() {
	}
	
	public CastGameCharacterFind(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}
	
	public int GetPageCount(CastGameCharacterFindSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {

		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine(" 	COUNT(*)							");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	(SELECT								");
		oSqlBuilder.AppendLine("		P.SITE_CD					,	");
		oSqlBuilder.AppendLine("		P.USER_SEQ					,	");
		oSqlBuilder.AppendLine("		P.USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHARACTER00 P	");
		
		for (int i = 0;i < pCondtion.attrValue.Count;i++) {
			if ((!pCondtion.attrValue[i].Equals("")) && (!pCondtion.attrValue[i].Equals("*"))) {
				oSqlBuilder.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
			}
		}

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine(" 	) P,								");
		oSqlBuilder.AppendLine(" 	(SELECT								");
		oSqlBuilder.AppendLine(" 		SITE_CD						,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_CHAR_NO			");
		oSqlBuilder.AppendLine(" 	FROM								");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00				");

		oParamList.AddRange(this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend));
		oSqlBuilder.AppendLine(sWhereClauseFriend);

		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT					");
		oSqlBuilder.AppendLine(" WHERE									");
		oSqlBuilder.AppendLine("	" + CreateJoinField(pCondtion) + "	");

		OracleParameter[] oWhereParams = oParamList.ToArray();

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterFindSeekCondition pCondtion,int pPageNo,int pRecPerPage) {

		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	P.*										,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.RANK	AS FRIENDLY_RANK	,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.FRIENDLY_POINT			,	");
		oSqlBuilder.AppendLine("	CASE										");
		oSqlBuilder.AppendLine("		WHEN									");
		oSqlBuilder.AppendLine("			EXISTS								");
		oSqlBuilder.AppendLine("			(									");
		oSqlBuilder.AppendLine("				SELECT							");
		oSqlBuilder.AppendLine("					*							");
		oSqlBuilder.AppendLine("				FROM							");
		oSqlBuilder.AppendLine("					T_MOST_SWEET_HEART			");
		oSqlBuilder.AppendLine("				WHERE							");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= FRIENDLY_POINT.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= FRIENDLY_POINT.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ		= P.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO					");
		oSqlBuilder.AppendLine("			)									");
		oSqlBuilder.AppendLine("		THEN 1									");
		oSqlBuilder.AppendLine("		ELSE 0									");
		oSqlBuilder.AppendLine("	END AS MOST_SWEET_HEART_FLAG				");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	(SELECT										");
		oSqlBuilder.AppendLine("		" + CastBasicField() + "			,	");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE				,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL				,	");
		oSqlBuilder.AppendLine("		P.EXP								,	");
		oSqlBuilder.AppendLine("		P.SITE_USE_STATUS					,	");
		oSqlBuilder.AppendLine("		CASE												");
		oSqlBuilder.AppendLine("			WHEN											");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE IS NOT NULL AND	");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE < SYSDATE		");
		oSqlBuilder.AppendLine("			THEN											");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE					");
		oSqlBuilder.AppendLine("			ELSE											");
		oSqlBuilder.AppendLine("				P.LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("		END VIEW_LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("	FROM													");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHARACTER00 P					,	");
		oSqlBuilder.AppendLine("		(																");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				SITE_CD												,	");
		oSqlBuilder.AppendLine("				USER_SEQ											,	");
		oSqlBuilder.AppendLine("				USER_CHAR_NO										,	");
		oSqlBuilder.AppendLine("				OFFLINE_LAST_UPDATE_DATE								");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_FAVORIT												");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("				WAITING_VIEW_STATUS		= :WAITING_VIEW_STATUS		AND	");
		oSqlBuilder.AppendLine("				LOGIN_VIEW_STATUS		= :LOGIN_VIEW_STATUS			");
		oSqlBuilder.AppendLine("		) F																");

		for (int i = 0;i < pCondtion.attrValue.Count;i++) {
			if ((!pCondtion.attrValue[i].Equals("")) && (!pCondtion.attrValue[i].Equals("*"))) {
				oSqlBuilder.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
			}
		}
		SysPrograms.SqlAppendWhere(" P.SITE_CD					= F.SITE_CD			(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ					= F.USER_SEQ		(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO				= F.USER_CHAR_NO	(+)",ref sWhereClause);
		oParamList.Add(new OracleParameter(":WAITING_VIEW_STATUS","1"));
		oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS","1"));

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine(" 	) P,										");
		oSqlBuilder.AppendLine(" 	(SELECT										");
		oSqlBuilder.AppendLine(" 		SITE_CD								,	");
		oSqlBuilder.AppendLine("		USER_SEQ							,	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_SEQ					,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine(" 		RANK								,	");
		oSqlBuilder.AppendLine(" 		FRIENDLY_POINT							");
		oSqlBuilder.AppendLine(" 	FROM										");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00						");

		oParamList.AddRange(this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend));
		oSqlBuilder.AppendLine(sWhereClauseFriend);

		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT							");
		oSqlBuilder.AppendLine(" WHERE											");
		oSqlBuilder.AppendLine("	" + CreateJoinField(pCondtion) + "			");

		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		AppendCastAttr(oDataSet);
		AppendManTreasure(oDataSet,pCondtion);
		AppendCastMovie(oDataSet,pCondtion);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastGameCharacterFindSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
		
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.ExtStartLevel) || !string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
			string sStartLevel = null;
			string sEndLevel = null;

			if (!string.IsNullOrEmpty(pCondition.ExtStartLevel) && !string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
				sStartLevel = pCondition.ExtStartLevel;
				sEndLevel = pCondition.ExtEndLevel;
			} else if (!string.IsNullOrEmpty(pCondition.ExtStartLevel) && string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
				sStartLevel = pCondition.ExtStartLevel;
				sEndLevel = "9999";
			} else if (string.IsNullOrEmpty(pCondition.ExtStartLevel) && !string.IsNullOrEmpty(pCondition.ExtEndLevel)) {
				sStartLevel = "1";
				sEndLevel = pCondition.ExtEndLevel;
			}

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		>= :GAME_CHARACTER_LEVEL_START",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_START",sStartLevel));

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		<= :GAME_CHARACTER_LEVEL_END",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_END",sEndLevel));

		} else if (!string.IsNullOrEmpty(pCondition.StartLevel) || !string.IsNullOrEmpty(pCondition.EndLevel)) {
			string sStartLevel = null;
			string sEndLevel = null;

			if (!string.IsNullOrEmpty(pCondition.StartLevel) && !string.IsNullOrEmpty(pCondition.EndLevel)) {
				sStartLevel = pCondition.StartLevel;
				sEndLevel = pCondition.EndLevel;
			} else if (!string.IsNullOrEmpty(pCondition.StartLevel) && string.IsNullOrEmpty(pCondition.EndLevel)) {
				sStartLevel = pCondition.StartLevel;
				sEndLevel = "9999";
			} else if (string.IsNullOrEmpty(pCondition.StartLevel) && !string.IsNullOrEmpty(pCondition.EndLevel)) {
				sStartLevel = "1";
				sEndLevel = pCondition.EndLevel;
			}

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		>= :GAME_CHARACTER_LEVEL_START",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_START",sStartLevel));

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		<= :GAME_CHARACTER_LEVEL_END",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_END",sEndLevel));
		}

		if (!string.IsNullOrEmpty(pCondition.Online) && pCondition.Online != ViCommConst.FLAG_OFF_STR) {
			
			switch (int.Parse(pCondition.Online)) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) ",ref pWhereClause);
					oParamList.Add(new OracleParameter(":ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					oParamList.Add(new OracleParameter(":ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :USER_SEQ AND PARTNER_USER_CHAR_NO = :USER_CHAR_NO AND WAITING_VIEW_STATUS != :WAITING_VIEW_STATUS)",ref pWhereClause);
					oParamList.Add(new OracleParameter(":WAITING_VIEW_STATUS","0"));
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhereClause);
					oParamList.Add(new OracleParameter(":ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
					oParamList.Add(new OracleParameter(":ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					oParamList.Add(new OracleParameter(":ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));

					SysPrograms.SqlAppendWhere("P.LAST_ACTION_DATE >= :LAST_ACTION_DATE ",ref pWhereClause);
					oParamList.Add(new OracleParameter(":LAST_ACTION_DATE",OracleDbType.Date,DateTime.Now.AddDays(-1),ParameterDirection.Input));

					SysPrograms.SqlAppendWhere("P.PRIORITY > :PRIORITY ",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PRIORITY",ViCommConst.FLAG_OFF));

					SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :USER_SEQ AND PARTNER_USER_CHAR_NO = :USER_CHAR_NO AND LOGIN_VIEW_STATUS = :LOGIN_VIEW_STATUS AND OFFLINE_LAST_UPDATE_DATE < SYSDATE)",ref pWhereClause);
					oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS","1"));
					break;
			}			
		}

		if (!string.IsNullOrEmpty(pCondition.NewCastDay)) {
			SysPrograms.SqlAppendWhere("P.START_PERFORM_DAY  >= :NEW_DAY ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NEW_DAY",DateTime.Now.AddDays(-1 * int.Parse(pCondition.NewCastDay)).ToString("yyyy/MM/dd")));
		}
		
		if (!string.IsNullOrEmpty(pCondition.Handle)) {
			SysPrograms.SqlAppendWhere(" P.HANDLE_NM LIKE '%' || :HANDLE_NM || '%' ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":HANDLE_NM",pCondition.Handle));
		}

		if (!string.IsNullOrEmpty(pCondition.Agefrom)) {
			SysPrograms.SqlAppendWhere(" P.AGE >= :AGE_FROM",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AGE_FROM",pCondition.Agefrom));
		}

		if (!string.IsNullOrEmpty(pCondition.Ageto)) {
			SysPrograms.SqlAppendWhere(" P.AGE <= :AGE_TO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AGE_TO",pCondition.Ageto));

		}

		if (!string.IsNullOrEmpty(pCondition.ExtTreasurePic) || !string.IsNullOrEmpty(pCondition.TreasurePic)) {
			SysPrograms.SqlAppendWhere(" EXISTS (SELECT * FROM T_MAN_TREASURE MT WHERE SITE_CD = P.SITE_CD AND " +
			 " USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PUBLISH_FLAG = :PUBLISH_FLAG	AND	" +
			 "NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = MT.SITE_CD AND USER_SEQ = MT.USER_SEQ AND USER_CHAR_NO = MT.USER_CHAR_NO AND PARTNER_USER_SEQ = :USER_SEQ AND PARTNER_USER_CHAR_NO = :USER_CHAR_NO AND GAME_PIC_NON_PUBLISH_DATE < MT.UPLOAD_DATE))",ref pWhereClause);
			 oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		}

		if (!string.IsNullOrEmpty(pCondition.ExtTreasureMovie) || !string.IsNullOrEmpty(pCondition.TreasureMovie)) {
			SysPrograms.SqlAppendWhere(" EXISTS (SELECT * FROM T_CAST_MOVIE WHERE SITE_CD = P.SITE_CD AND " +
			 " USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND MOVIE_TYPE = :GAME_MOVIE_TYPE )   ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));

			SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :USER_SEQ AND PARTNER_USER_CHAR_NO = :USER_CHAR_NO AND BBS_NON_PUBLISH_FLAG = :BBS_NON_PUBLISH_FLAG)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		}

		if (pCondition.CastTodayFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			DateTime dPreviousDay = DateTime.Now;
			string sPreviousDay = DateTime.Now.ToString("yyyy/MM/dd");

			SysPrograms.SqlAppendWhere(" EXISTS (SELECT * FROM T_MAN_TREASURE MT WHERE SITE_CD = P.SITE_CD AND " +
			 " USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PUBLISH_FLAG = :PUBLISH_FLAG AND DISPLAY_DAY = :DISPLAY_DAY AND NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = MT.SITE_CD AND USER_SEQ = MT.USER_SEQ AND USER_CHAR_NO = MT.USER_CHAR_NO AND PARTNER_USER_SEQ = :USER_SEQ AND PARTNER_USER_CHAR_NO = :USER_CHAR_NO AND GAME_PIC_NON_PUBLISH_DATE < MT.UPLOAD_DATE))   ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON));
			oParamList.Add(new OracleParameter(":DISPLAY_DAY",sPreviousDay));
		}

		if (!string.IsNullOrEmpty(pCondition.Comment)) {
			string[] sComment = pCondition.Comment.Trim().Split(' ');
			StringBuilder sWhere = new StringBuilder();
			sWhere.Append("(");
			for (int i = 0;i < sComment.Length;i++) {
				if (sComment[i] != "") {
					if (i != 0) {
						sWhere.Append(" OR ");
					}
					sWhere.Append(string.Format(" (P.COMMENT_LIST LIKE '%' || :COMMENT_LIST{0} || '%' OR P.COMMENT_DETAIL LIKE '%' || :COMMENT_DETAIL{0} || '%' OR P.COMMENT_ADMIN LIKE '%' || :COMMENT_ADMIN{0} || '%') ",i.ToString()));
					oParamList.Add(new OracleParameter(string.Format(":COMMENT_LIST{0}",i.ToString()),sComment[i]));
					oParamList.Add(new OracleParameter(string.Format(":COMMENT_DETAIL{0}",i.ToString()),sComment[i]));
					oParamList.Add(new OracleParameter(string.Format(":COMMENT_ADMIN{0}",i.ToString()),sComment[i]));
				}
			}
			sWhere.Append(")");
			SysPrograms.SqlAppendWhere(sWhere.ToString(),ref pWhereClause);

		}

		if (!string.IsNullOrEmpty(pCondition.GameType)) {
			SysPrograms.SqlAppendWhere("P.GAME_CHARACTER_TYPE = :GAME_CHARACTER_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_TYPE",pCondition.GameType));
		}

		SysPrograms.SqlAppendWhere(" P.STAFF_FLAG = :STAFF_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		SysPrograms.SqlAppendWhere("SITE_USE_STATUS = :SITE_USE_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_USE_STATUS",ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME));

		SysPrograms.SqlAppendWhere(string.Format(" P.NA_FLAG IN({0},{1})",ViCommConst.NaFlag.OK,ViCommConst.NaFlag.NO_AUTH),ref pWhereClause);

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :USER_SEQ AND PARTNER_USER_CHAR_NO = :USER_CHAR_NO)",ref pWhereClause);

		oParamList.AddRange(CreateAttrQuery(pCondition,ref pWhereClause));

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereFriend(CastGameCharacterFindSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
		
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ExtRelation) || !string.IsNullOrEmpty(pCondition.Relation)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.RANK		<= :RANK",ref pWhereClause);
			if (!string.IsNullOrEmpty(pCondition.ExtRelation)) {
				oParamList.Add(new OracleParameter(":RANK",pCondition.ExtRelation));
			} else if (!string.IsNullOrEmpty(pCondition.Relation)) {
				oParamList.Add(new OracleParameter(":RANK",pCondition.Relation));
			}
		}

		return oParamList.ToArray();
	}

	protected string CreateJoinField(CastGameCharacterFindSeekCondition pCondition) {
		StringBuilder sSql = new StringBuilder();

		if (!string.IsNullOrEmpty(pCondition.ExtRelation) || !string.IsNullOrEmpty(pCondition.Relation)) {
			sSql.Append(" 	P.SITE_CD				= FRIENDLY_POINT.SITE_CD				AND	").AppendLine();
			sSql.Append(" 	P.USER_SEQ				= FRIENDLY_POINT.PARTNER_USER_SEQ		AND ").AppendLine();
			sSql.Append(" 	P.USER_CHAR_NO			= FRIENDLY_POINT.PARTNER_USER_CHAR_NO		").AppendLine();
		} else {
			sSql.Append(" 	P.SITE_CD				= FRIENDLY_POINT.SITE_CD				(+) AND	").AppendLine();
			sSql.Append(" 	P.USER_SEQ				= FRIENDLY_POINT.PARTNER_USER_SEQ		(+) AND ").AppendLine();
			sSql.Append(" 	P.USER_CHAR_NO			= FRIENDLY_POINT.PARTNER_USER_CHAR_NO	(+)		").AppendLine();
		}

		return sSql.ToString();
	}

	private string CreateOrderExpresion(CastGameCharacterFindSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.CastGameCharacterFindSort.LAST_LOGIN_DATE_DESC:
				sSortExpression = "ORDER BY VIEW_LAST_LOGIN_DATE DESC NULLS LAST";
				break;
			case PwViCommConst.CastGameCharacterFindSort.GAME_CHARACTER_LEVEL_DESC:
				sSortExpression = "ORDER BY GAME_CHARACTER_LEVEL DESC, EXP DESC";
				break;
			case PwViCommConst.CastGameCharacterFindSort.FRIENDLY_RANK_ASC:
				sSortExpression = "ORDER BY MOST_SWEET_HEART_FLAG DESC,FRIENDLY_RANK, FRIENDLY_POINT DESC,P.USER_SEQ";
				break;
			case PwViCommConst.CastGameCharacterFindSort.RANDOM:
				sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";
				break;
			case PwViCommConst.CastGameCharacterFindSort.MOST_SWEET_HEART_NEXT_RANDOM:
				sSortExpression = "ORDER BY MOST_SWEET_HEART_FLAG DESC,DBMS_RANDOM.RANDOM";
				break;
			default:
				sSortExpression = "ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}

	private void AppendManTreasure(DataSet pDS,CastGameCharacterFindSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("GAME_TREASURE_PIC_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											");
			oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_PIC_COUNT			");
			oSqlBuilder.AppendLine(" FROM							   				");
			oSqlBuilder.AppendLine("	T_MAN_TREASURE P							");
			oSqlBuilder.AppendLine(" WHERE											");
			oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND			");
			oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND			");
			oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO	AND			");
			oSqlBuilder.AppendLine("	PUBLISH_FLAG	= :PUBLISH_FLAG	AND			");
			oSqlBuilder.AppendLine("	NOT EXISTS														");
			oSqlBuilder.AppendLine("	(																");
			oSqlBuilder.AppendLine("		SELECT														");
			oSqlBuilder.AppendLine("			*														");
			oSqlBuilder.AppendLine("		FROM														");
			oSqlBuilder.AppendLine("			T_FAVORIT												");
			oSqlBuilder.AppendLine("		WHERE														");
			oSqlBuilder.AppendLine("			SITE_CD						= P.SITE_CD				AND	");
			oSqlBuilder.AppendLine("			USER_SEQ					= P.USER_SEQ			AND	");
			oSqlBuilder.AppendLine("			USER_CHAR_NO				= P.USER_CHAR_NO		AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= :SELF_USER_SEQ		AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= :SELF_USER_CHAR_NO	AND	");
			oSqlBuilder.AppendLine("			GAME_PIC_NON_PUBLISH_DATE	< P.UPLOAD_DATE				");
			oSqlBuilder.AppendLine("	)																");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;
					cmd.Parameters.Add(":SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					cmd.Parameters.Add(":SELF_USER_SEQ",pCondition.UserSeq);
					cmd.Parameters.Add(":SELF_USER_CHAR_NO",pCondition.UserCharNo);
					cmd.Parameters.Add(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);


					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["GAME_TREASURE_PIC_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_TREASURE_PIC_COUNT"];
				}
			}
		}
	}

	private void AppendCastMovie(DataSet pDS,CastGameCharacterFindSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("GAME_TREASURE_MOVIE_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT													");
			oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_MOVIE_COUNT				");
			oSqlBuilder.AppendLine(" FROM													");
			oSqlBuilder.AppendLine("	T_CAST_MOVIE P										");
			oSqlBuilder.AppendLine(" WHERE													");
			oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND	");
			oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND	");
			oSqlBuilder.AppendLine("	MOVIE_TYPE				= :MOVIE_TYPE			AND	");
			oSqlBuilder.AppendLine("	OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPROVE_FLAG	AND	");
			oSqlBuilder.AppendLine("	OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG	AND	");
			oSqlBuilder.AppendLine("	NOT EXISTS														");
			oSqlBuilder.AppendLine("	(																");
			oSqlBuilder.AppendLine("		SELECT														");
			oSqlBuilder.AppendLine("			*														");
			oSqlBuilder.AppendLine("		FROM														");
			oSqlBuilder.AppendLine("			T_FAVORIT												");
			oSqlBuilder.AppendLine("		WHERE														");
			oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD					AND	");
			oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ				AND	");
			oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO			AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ			AND	");
			oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		AND	");
			oSqlBuilder.AppendLine("			BBS_NON_PUBLISH_FLAG	= :BBS_NON_PUBLISH_FLAG			");
			oSqlBuilder.AppendLine("	)																");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;
					cmd.Parameters.Add(":SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					cmd.Parameters.Add(":SELF_USER_SEQ",pCondition.UserSeq);
					cmd.Parameters.Add(":SELF_USER_CHAR_NO",pCondition.UserCharNo);
					cmd.Parameters.Add(":MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME);
					cmd.Parameters.Add(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR);
					cmd.Parameters.Add(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR);
					cmd.Parameters.Add(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["GAME_TREASURE_MOVIE_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_TREASURE_MOVIE_COUNT"];
				}
			}
		}
	}

	private OracleParameter[] CreateAttrQuery(CastGameCharacterFindSeekCondition pCondition,ref string pWhere) {
		string sQuery = string.Empty;
		ArrayList pList;
		pList = new ArrayList();

		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.SITE_CD			= P.SITE_CD					",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_SEQ			= P.USER_SEQ				",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO			",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0}	",i + 1),ref pWhere);
				pList.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));

				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						sQuery = " ( ";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									sQuery += " OR ";
								}
								sQuery += string.Format(" (AT{0}.CAST_ATTR_INPUT_VALUE	LIKE '%' || :CAST_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						sQuery += ")";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');

					if (sValues.Length > 1) {
						sQuery = string.Format(" AT{0}.CAST_ATTR_SEQ IN ( ",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0) {
								sQuery += " , ";
							}
							sQuery += string.Format(" :CAST_ATTR_SEQ{0}_{1}	",i + 1,j);
							pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						sQuery += " ) ";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					} else {
						if (pCondition.notEqualFlag.Count > i && pCondition.notEqualFlag[i].Equals("1")) {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ <> :CAST_ATTR_SEQ{0} ",i + 1),ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ = :CAST_ATTR_SEQ{0} ",i + 1),ref pWhere);
						}
						pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					SysPrograms.SqlAppendWhere(string.Format(" AT{0}.GROUPING_CD	= :GROUPING_CD{0}	",i + 1),ref pWhere);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}

		return (OracleParameter[])pList.ToArray(typeof(OracleParameter));
	}

	public int GetPageCountExistMovie(CastGameCharacterFindSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {

		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	COUNT(*)																	");
		oSqlBuilder.AppendLine(" FROM																			");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHARACTER00 P											,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			USER_SEQ														,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO													,	");
		oSqlBuilder.AppendLine("			COUNT(*)	AS GAME_TREASURE_MOVIE_COUNT							");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_CAST_MOVIE														");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			MOVIE_TYPE				= :GAME_MOVIE_TYPE						AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_APPROVE_FLAG	= 0										AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_PUBLISH_FLAG	= 0											");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO									");
		oSqlBuilder.AppendLine("	) M																		,	");
		oSqlBuilder.AppendLine(" 	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.SITE_CD										,	");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00.USER_SEQ									,	");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00.USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.RANK										,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.FRIENDLY_POINT									");
		oSqlBuilder.AppendLine(" 		FROM																	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00													");

		// where		
		oParamList.AddRange(this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend));
		oSqlBuilder.AppendLine(sWhereClauseFriend);

		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT															");
		oSqlBuilder.AppendLine(" WHERE																			");
		oSqlBuilder.AppendLine("	P.SITE_CD				= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG				IN(:NA_FLAG1,:NA_FLAG2)							AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG			= 0												AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG	= 0												AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD				= M.SITE_CD										AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ				= M.USER_SEQ									AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO			= M.USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("	" + CreateJoinField(pCondtion) + "	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_FAVORIT															");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			BBS_NON_PUBLISH_FLAG	= :BBS_NON_PUBLISH_FLAG						");
		oSqlBuilder.AppendLine("	)																		AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_REFUSE															");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																			");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":NA_FLAG1",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":GAME_MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.UserCharNo));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollectionExistMovie(CastGameCharacterFindSeekCondition pCondtion,int pPageNo,int pRecPerPage) {

		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "												,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM														,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE													,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL													,	");
		oSqlBuilder.AppendLine("	M.GAME_TREASURE_MOVIE_COUNT												,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.RANK	AS FRIENDLY_RANK									,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.FRIENDLY_POINT											,	");
		oSqlBuilder.AppendLine("	CASE																		");
		oSqlBuilder.AppendLine("		WHEN																	");
		oSqlBuilder.AppendLine("			EXISTS																");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_MOST_SWEET_HEART											");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= FRIENDLY_POINT.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= FRIENDLY_POINT.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ		= P.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO					");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("		THEN 1																	");
		oSqlBuilder.AppendLine("		ELSE 0																	");
		oSqlBuilder.AppendLine("	END AS MOST_SWEET_HEART_FLAG												");
		oSqlBuilder.AppendLine(" FROM																			");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHARACTER00 P											,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			USER_SEQ														,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO													,	");
		oSqlBuilder.AppendLine("			COUNT(*)	AS GAME_TREASURE_MOVIE_COUNT							");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_CAST_MOVIE														");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			MOVIE_TYPE				= :GAME_MOVIE_TYPE						AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_APPROVE_FLAG	= 0										AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_PUBLISH_FLAG	= 0											");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO									");
		oSqlBuilder.AppendLine("	) M																		,	");
		oSqlBuilder.AppendLine(" 	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.SITE_CD										,	");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00.USER_SEQ									,	");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00.USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.RANK										,	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00.FRIENDLY_POINT									");
		oSqlBuilder.AppendLine(" 		FROM																	");
		oSqlBuilder.AppendLine(" 			MV_FRIENDLY_POINT00													");

		// where		
		oParamList.AddRange(this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend));
		oSqlBuilder.AppendLine(sWhereClauseFriend);

		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT															");
		oSqlBuilder.AppendLine(" WHERE																			");
		oSqlBuilder.AppendLine("	P.SITE_CD				= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG				IN(:NA_FLAG1,:NA_FLAG2)							AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG			= 0												AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG	= 0												AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD				= M.SITE_CD										AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ				= M.USER_SEQ									AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO			= M.USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("	" + CreateJoinField(pCondtion) + "	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_FAVORIT															");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			BBS_NON_PUBLISH_FLAG	= :BBS_NON_PUBLISH_FLAG						");
		oSqlBuilder.AppendLine("	)																		AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_REFUSE															");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																			");
		
		oSqlBuilder.AppendLine();
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":NA_FLAG1",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":GAME_MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.UserCharNo));

		// order by
		sSortExpression = this.CreateOrderExpresionExistMovie(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private string CreateOrderExpresionExistMovie(CastGameCharacterFindSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.CastGameCharacterFindSort.GAME_CHARACTER_LEVEL_DESC:
				sSortExpression = "ORDER BY GAME_CHARACTER_LEVEL DESC, EXP DESC";
				break;
			case PwViCommConst.CastGameCharacterFindSort.FRIENDLY_RANK_ASC:
				sSortExpression = "ORDER BY MOST_SWEET_HEART_FLAG DESC,FRIENDLY_RANK, FRIENDLY_POINT DESC";
				break;
			case PwViCommConst.CastGameCharacterFindSort.RANDOM:
				sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";
				break;
			case PwViCommConst.CastGameCharacterFindSort.MOST_SWEET_HEART_NEXT_RANDOM:
				sSortExpression = "ORDER BY MOST_SWEET_HEART_FLAG DESC,DBMS_RANDOM.RANDOM";
				break;
			default:
				sSortExpression = "ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}
}