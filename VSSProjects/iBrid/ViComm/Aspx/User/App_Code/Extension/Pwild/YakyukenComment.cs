﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 野球拳コメント
--	Progaram ID		: YakyukenComment
--
--  Creation Date	: 2013.04.29
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class YakyukenCommentSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string CastUserSeq;
	public string CastCharNo;
	public string YakyukenCommentSeq;
	public string RandomFlag = ViCommConst.FLAG_OFF_STR;

	public YakyukenCommentSeekCondition()
		: this(new NameValueCollection()) {
	}

	public YakyukenCommentSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class YakyukenComment:DbSession {
	public YakyukenComment() {
	}

	public int GetPageCount(YakyukenCommentSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_YAKYUKEN_COMMENT");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(YakyukenCommentSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	YAKYUKEN_COMMENT_SEQ,");
		oSqlBuilder.AppendLine("	COMMENT_TEXT,");
		oSqlBuilder.AppendLine("	CREATE_DATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_YAKYUKEN_COMMENT");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = CreateOrderExpresion(pCondtion);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(YakyukenCommentSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenCommentSeq)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_COMMENT_SEQ = :YAKYUKEN_COMMENT_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_COMMENT_SEQ",pCondition.YakyukenCommentSeq));
		}

		SysPrograms.SqlAppendWhere("DEL_FLAG = :DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(YakyukenCommentSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		if (pCondition.RandomFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";
		} else {
			sSortExpression = "ORDER BY SITE_CD,CAST_USER_SEQ,CAST_CHAR_NO,CREATE_DATE DESC";
		}

		return sSortExpression;
	}

	public void RegistYakyukenComment(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		string pCommentText,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_YAKYUKEN_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void ModifyYakyukenComment(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		string pYakyukenCommentSeq,
		string pCommentText,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_YAKYUKEN_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pYAKYUKEN_COMMENT_SEQ",DbSession.DbType.VARCHAR2,pYakyukenCommentSeq);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
}
