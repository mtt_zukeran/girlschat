﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ファンクラブ状態
--	Progaram ID		: FanClubStatus
--
--  Creation Date	: 2013.01.31
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class FanClubStatusSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string CastUserSeq {
		get {
			return this.Query["castuserseq"];
		}
		set {
			this.Query["castuserseq"] = value;
		}
	}

	public string CastCharNo {
		get {
			return this.Query["castcharno"];
		}
		set {
			this.Query["castcharno"] = value;
		}
	}

	public string ManUserSeq {
		get {
			return this.Query["manuserseq"];
		}
		set {
			this.Query["manuserseq"] = value;
		}
	}

	public string EnableFlag {
		get {
			return this.Query["enable"];
		}
		set {
			this.Query["enable"] = value;
		}
	}

	public string MemberRank {
		get {
			return this.Query["memberrank"];
		}
		set {
			this.Query["memberrank"] = value;
		}
	}

	public List<string> attrTypeSeq;
	public List<string> attrValue;
	public List<string> groupingFlag;
	public List<string> likeSearchFlag;
	public List<string> notEqualFlag;
	public List<string> rangeFlag;

	public FanClubStatusSeekCondition()
		: this(new NameValueCollection()) {
	}

	public FanClubStatusSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["castuserseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castuserseq"]));
		this.Query["castcharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castcharno"]));
		this.Query["manuserseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["manuserseq"]));
		this.Query["enable"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["enable"]));
		this.Query["memberrank"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["memberrank"]));

		attrTypeSeq = new List<string>();
		attrValue = new List<string>();
		groupingFlag = new List<string>();
		likeSearchFlag = new List<string>();
		notEqualFlag = new List<string>();
		rangeFlag = new List<string>();

		for (int i = 0;i < ViCommConst.MAX_ATTR_COUNT;i++) {
			attrValue.Add(iBridUtil.GetStringValue(pQuery[string.Format("item{0:D2}",i + 1)]));
			attrTypeSeq.Add(iBridUtil.GetStringValue(pQuery[string.Format("typeseq{0:D2}",i + 1)]));
			groupingFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("group{0:D2}",i + 1)]));
			likeSearchFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("like{0:D2}",i + 1)]));
			notEqualFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("noteq{0:D2}",i + 1)]));
			rangeFlag.Add(iBridUtil.GetStringValue(pQuery[string.Format("range{0:D2}",i + 1)]));
		}
	}
}

public class FanClubStatus:DbSession {
	public FanClubStatus() {
	}

	public string GetMemberRank(string pSiteCd,string pManUserSeq,string pCastUserSeq,string pCastCharNo) {
		string sMemberRank = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	NVL(MEMBER_RANK,0) AS MEMBER_RANK");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FANCLUB_STATUS");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ = :MAN_USER_SEQ AND");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ = :CAST_USER_SEQ AND");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO = :CAST_CHAR_NO AND");
		oSqlBuilder.AppendLine("	RESIGN_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	EXPIRE_DATE > SYSDATE");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pManUserSeq));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCastCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sMemberRank = oDataSet.Tables[0].Rows[0]["MEMBER_RANK"].ToString();
		}

		return sMemberRank;
	}

	public string GetBestMemberRank(string pSiteCd,string pManUserSeq) {
		string sBestMemberRank = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	NVL(MAX(MEMBER_RANK),0) AS BEST_MEMBER_RANK");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FANCLUB_STATUS");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ = :MAN_USER_SEQ AND");
		oSqlBuilder.AppendLine("	RESIGN_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	EXPIRE_DATE > SYSDATE");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pManUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sBestMemberRank = oDataSet.Tables[0].Rows[0]["BEST_MEMBER_RANK"].ToString();
		}

		return sBestMemberRank;
	}
	
	public string GetMyFanClubMemberRank(string pSiteCd,string pManUserSeq,string pCastUserSeq,string pCastCharNo) {
		string sMyFanClubMemberRank = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	CASE													");
		oSqlBuilder.AppendLine("		WHEN												");
		oSqlBuilder.AppendLine("			EXISTS											");
		oSqlBuilder.AppendLine("			(												");
		oSqlBuilder.AppendLine("				SELECT										");
		oSqlBuilder.AppendLine("					1										");
		oSqlBuilder.AppendLine("				FROM										");
		oSqlBuilder.AppendLine("					T_USER_MAN_CHARACTER_EX					");
		oSqlBuilder.AppendLine("				WHERE										");
		oSqlBuilder.AppendLine("					SITE_CD				= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("					USER_SEQ			= :USER_SEQ		AND	");
		oSqlBuilder.AppendLine("					SP_PREMIUM_END_DATE	> SYSDATE			");
		oSqlBuilder.AppendLine("			)												");
		oSqlBuilder.AppendLine("		THEN												");
		oSqlBuilder.AppendLine("			5												");
		oSqlBuilder.AppendLine("		ELSE												");
		oSqlBuilder.AppendLine("			NVL((											");
		oSqlBuilder.AppendLine("				SELECT										");
		oSqlBuilder.AppendLine("					MEMBER_RANK								");
		oSqlBuilder.AppendLine("				FROM										");
		oSqlBuilder.AppendLine("					T_FANCLUB_STATUS						");
		oSqlBuilder.AppendLine("				WHERE										");
		oSqlBuilder.AppendLine("					SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("					MAN_USER_SEQ	= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("					CAST_USER_SEQ	= :CAST_USER_SEQ	AND	");
		oSqlBuilder.AppendLine("					CAST_CHAR_NO	= :CAST_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("					RESIGN_FLAG		= 0					AND	");
		oSqlBuilder.AppendLine("					EXPIRE_DATE		> SYSDATE				");
		oSqlBuilder.AppendLine("			),0)											");
		oSqlBuilder.AppendLine("	END AS MEMBER_RANK										");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	DUAL													");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pManUserSeq));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCastCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sMyFanClubMemberRank = oDataSet.Tables[0].Rows[0]["MEMBER_RANK"].ToString();
		}

		return sMyFanClubMemberRank;
		
	}

	public string GetFanClubAdmissionEnable(string pSiteCd,string pManUserSeq,string pCastUserSeq,string pCastCharNo) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_FANCLUB_ADMISSION_ENABLE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public void FanClubAdmission(string pSiteCd,string pCastUserSeq,string pCastUserCharNo,string pManUserSeq,string pForceNonQuick,out string sResult,out string pSid) {
		sResult = string.Empty;
		pSid = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("FANCLUB_ADMISSION");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastUserCharNo);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pFORCE_NON_QUICK",DbSession.DbType.VARCHAR2,pForceNonQuick);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
			pSid = db.GetStringValue("pSID");
		}
	}

	public void FanClubAdmissionPt(string pSiteCd,string pCastUserSeq,string pCastUserCharNo,string pManUserSeq,out string sResult) {
		sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("FANCLUB_ADMISSION_PT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastUserCharNo);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}
	}

	public void FanClubExtensionPt(string pSiteCd,string pCastUserSeq,string pCastUserCharNo,string pManUserSeq,out string sResult) {
		sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("FANCLUB_EXTENSION_PT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastUserCharNo);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}
	}

	public string FanClubResign(string pSiteCd,string pCastUserSeq,string pCastUserCharNo,string pManUserSeq,string pRemarks) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("FANCLUB_RESIGN");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastUserCharNo);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,pRemarks);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}
}

public class ManFanClubStatus:ManBase {
	public ManFanClubStatus()
		: base() {
	}
	public ManFanClubStatus(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(FanClubStatusSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_FANCLUB_STATUS00 P");

		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				oSqlBuilder.AppendLine(string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1));
			}
		}

		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(FanClubStatusSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	" + ManBasicField() + ",");
		oSqlBuilder.AppendLine("	P.CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	P.CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	P.MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("	P.ADMISSION_DATE,");
		oSqlBuilder.AppendLine("	P.EXPIRE_DATE,");
		oSqlBuilder.AppendLine("	P.RESIGN_FLAG,");
		oSqlBuilder.AppendLine("	P.COIN_COUNT,");
		oSqlBuilder.AppendLine("	P.MEMBER_RANK,");
		oSqlBuilder.AppendLine("	P.ENABLE_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_FANCLUB_STATUS00 P");

		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				oSqlBuilder.AppendLine(string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1));
			}
		}

		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = this.CreateOrderExpresion(pCondition);
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		AppendManAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(FanClubStatusSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("P.CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("P.CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ManUserSeq)) {
			SysPrograms.SqlAppendWhere("P.MAN_USER_SEQ = :MAN_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.EnableFlag)) {
			SysPrograms.SqlAppendWhere("P.ENABLE_FLAG = :ENABLE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ENABLE_FLAG",pCondition.EnableFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.MemberRank)) {
			SysPrograms.SqlAppendWhere("P.MEMBER_RANK = :MEMBER_RANK",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MEMBER_RANK",pCondition.MemberRank));
		}

		if (pCondition.attrValue.Count > 0) {
			SeekCondition oSeekCondition = new SeekCondition();
			oSeekCondition.attrTypeSeq = pCondition.attrTypeSeq;
			oSeekCondition.attrValue = pCondition.attrValue;
			oSeekCondition.groupingFlag = pCondition.groupingFlag;
			oSeekCondition.likeSearchFlag = pCondition.likeSearchFlag;
			oSeekCondition.notEqualFlag = pCondition.notEqualFlag;
			oSeekCondition.rangeFlag = pCondition.rangeFlag;

			ArrayList list = new ArrayList();
			CreateAttrQuery(oSeekCondition,ref pWhereClause,ref list);
			oParamList.AddRange((OracleParameter[])list.ToArray(typeof(OracleParameter)));
		}

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(FanClubStatusSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY ADMISSION_DATE ASC";

		return sSortExpression;
	}
}

public class CastFanClubStatus:CastBase {
	public CastFanClubStatus()
		: base() {
	}
	public CastFanClubStatus(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(FanClubStatusSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_FANCLUB_STATUS00 P");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(FanClubStatusSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	" + CastBasicField() + ",");
		oSqlBuilder.AppendLine("	P.CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	P.CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	P.MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("	P.ADMISSION_DATE,");
		oSqlBuilder.AppendLine("	P.NEXT_SETTLE_DATE,");
		oSqlBuilder.AppendLine("	P.EXPIRE_DATE,");
		oSqlBuilder.AppendLine("	P.RESIGN_FLAG,");
		oSqlBuilder.AppendLine("	P.COIN_COUNT,");
		oSqlBuilder.AppendLine("	P.MEMBER_RANK,");
		oSqlBuilder.AppendLine("	P.MEMBER_RANK_EXPIRE_DATE,");
		oSqlBuilder.AppendLine("	P.SETTLE_NG_FLAG,");
		oSqlBuilder.AppendLine("	P.ENABLE_FLAG,");
		oSqlBuilder.AppendLine("	P.PAYMENT_TYPE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_FANCLUB_STATUS00 P");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = this.CreateOrderExpresion(pCondition);
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(FanClubStatusSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("P.CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("P.CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ManUserSeq)) {
			SysPrograms.SqlAppendWhere("P.MAN_USER_SEQ = :MAN_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.EnableFlag)) {
			SysPrograms.SqlAppendWhere("P.ENABLE_FLAG = :ENABLE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ENABLE_FLAG",pCondition.EnableFlag));
		}

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(FanClubStatusSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY ADMISSION_DATE ASC";

		return sSortExpression;
	}
}