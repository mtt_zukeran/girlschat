﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 投票順位
--	Progaram ID		: ElectionEntry
--  Creation Date	: 2013.10.18
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ElectionEntrySeekCondition:SeekConditionBase {
	public string SiteCd;
	public string ElectionPeriodSeq {
		get {
			return this.Query["electionperiodseq"];
		}
		set {
			this.Query["electionperiodseq"] = value;
		}
	}

	public string FixRankFlag;

	public string CastUserSeq {
		get {
			return this.Query["castuserseq"];
		}
		set {
			this.Query["castuserseq"] = value;
		}
	}

	public string CastCharNo {
		get {
			return this.Query["castcharno"];
		}
		set {
			this.Query["castcharno"] = value;
		}
	}

	public string ElectionPeriodStatus {
		get {
			return this.Query["electionperiodstatus"];
		}
		set {
			this.Query["electionperiodstatus"] = value;
		}
	}
	
	public string SecondPeriodFlag;

	public ElectionEntrySeekCondition()
		: this(new NameValueCollection()) {
	}

	public ElectionEntrySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["castuserseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castuserseq"]));
		this.Query["castcharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castcharno"]));
		this.Query["electionperiodseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["electionperiodseq"]));
		this.Query["electionperiodstatus"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["electionperiodstatus"]));
	}
}

public class ElectionEntry:CastBase {
	public ElectionEntry() {
	}
	public ElectionEntry(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ElectionEntrySeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sViewNm = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		if (pCondition.FixRankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sViewNm = "VW_ELECTION_RANK_FIX00";
		} else {
			sViewNm = "VW_ELECTION_ENTRY00";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendFormat("	{0} P",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ElectionEntrySeekCondition pCondition,int pPageNo,int pRecPerPage) {
		string sViewNm = string.Empty;
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		if (pCondition.FixRankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sViewNm = "VW_ELECTION_RANK_FIX00";
		} else {
			sViewNm = "VW_ELECTION_ENTRY00";
		}

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "		,	");
		if (pCondition.ElectionPeriodStatus.Equals(PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD)) {
			oSqlBuilder.AppendLine("	FIRST_VOTE_COUNT AS VOTE_COUNT	,	");
			oSqlBuilder.AppendLine("	FIRST_VOTE_RANK AS VOTE_RANK,	");
			oSqlBuilder.AppendLine("	FIRST_LAST_VOTE_DATE AS LAST_VOTE_DATE	,	");
		} else if (pCondition.ElectionPeriodStatus.Equals(PwViCommConst.ElectionPeriodStatus.SECOND_PERIOD)) {
			oSqlBuilder.AppendLine("	SECOND_VOTE_COUNT AS VOTE_COUNT	,	");
			oSqlBuilder.AppendLine("	SECOND_VOTE_RANK AS VOTE_RANK	,	");
			oSqlBuilder.AppendLine("	SECOND_LAST_VOTE_DATE AS LAST_VOTE_DATE	,	");
		} else {
			oSqlBuilder.AppendLine("	VOTE_COUNT								,	");
			oSqlBuilder.AppendLine("	VOTE_RANK	,	");
			oSqlBuilder.AppendLine("	SECOND_LAST_VOTE_DATE AS LAST_VOTE_DATE	,	");
		}
		oSqlBuilder.AppendLine("	ELECTION_PERIOD_SEQ				,	");
		oSqlBuilder.AppendLine("	ELECTION_PERIOD_NM				,	");
		oSqlBuilder.AppendLine("	FIRST_PERIOD_START_DATE			,	");
		oSqlBuilder.AppendLine("	FIRST_PERIOD_END_DATE			,	");
		oSqlBuilder.AppendLine("	SECOND_PERIOD_START_DATE		,	");
		oSqlBuilder.AppendLine("	SECOND_PERIOD_END_DATE			,	");
		oSqlBuilder.AppendLine("	SECOND_PERIOD_FLAG				,	");
		oSqlBuilder.AppendLine("	TICKET_POINT					,	");
		oSqlBuilder.AppendLine("	FIX_RANK_FLAG					,	");
		oSqlBuilder.AppendLine("	FIX_RANK_DATE						");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendFormat("	{0} P",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = CreateOrderExpresion(pCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		AppendCastAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ElectionEntrySeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.ElectionPeriodSeq)) {
			SysPrograms.SqlAppendWhere("ELECTION_PERIOD_SEQ = :ELECTION_PERIOD_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ELECTION_PERIOD_SEQ",pCondition.ElectionPeriodSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.SecondPeriodFlag)) {
			SysPrograms.SqlAppendWhere("SECOND_PERIOD_FLAG = :SECOND_PERIOD_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SECOND_PERIOD_FLAG",pCondition.SecondPeriodFlag));
		}

		SysPrograms.SqlAppendWhere("RETIRE_FLAG = :RETIRE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":RETIRE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("REFUSE_FLAG = :REFUSE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":REFUSE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(ElectionEntrySeekCondition pCondition) {
		string sSortExpression = string.Empty;
		switch (pCondition.ElectionPeriodStatus) {
			default:
				sSortExpression = "ORDER BY SITE_CD,ELECTION_PERIOD_SEQ,VOTE_RANK ASC";
				break;
		}
		return sSortExpression;
	}

	public void RegistElectionVote(
		string pSiteCd,
		string pManUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		int pVoteCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_ELECTION_VOTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.NUMBER,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pVOTE_COUNT",DbSession.DbType.NUMBER,pVoteCount);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void RegistElectionEntry(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_ELECTION_ENTRY");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.NUMBER,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
}
