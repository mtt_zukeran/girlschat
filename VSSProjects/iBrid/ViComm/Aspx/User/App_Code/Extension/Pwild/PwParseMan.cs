﻿using System;
using System.IO;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Collections.Specialized;
using ViComm;
using ViComm.Extension.Pwild;

public partial class PwParseMan:PwParseViComm {
	private ParseMan parseMan;
	public PwParseMan(ParseMan pParseMan)
		: base(pParseMan) {
		this.parseMan = pParseMan;
	}

	protected override GameCharacter getGameCharacter() {
		return this.parseMan.sessionMan.userMan.gameCharacter;
	}

	internal override string Parse(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;

		switch (pTag) {
			case "$SELF_USER_SEQ":
				sValue = this.parseMan.sessionMan.userMan.userSeq;
				break;
			case "$SELF_USER_CHAR_NO":
				sValue = this.parseMan.sessionMan.userMan.userCharNo;
				break;
			case "$SETUP_GAME_TUTORIAL_STATUS":
				SetupGameTutorialStatus(pTag,pArgument);
				break;
			case "$GET_GAME_HANDLE_NM":
				sValue = this.getGameHandleName(pTag,pArgument);
				sValue = sValue + "ｻﾝ";
				break;
			case "$GET_CAST_GAME_CHARACTER_DATA":
				sValue = this.getCastGameCharacterData(pTag,pArgument);
				break;
			case "$GAME_SALE_STATUS":
				sValue = this.GetGameItemSaleStatus(pTag,pArgument);
				break;
			case "$GET_GAME_ITEM_NM":
				sValue = this.GetGameItemNm(pTag,pArgument);
				break;
			case "$SELF_GAME_TODAY_DATE_COUNT":
				sValue = this.GetTodayDateCount(pTag,pArgument);
				break;
			case "$GAME_DATE_LIMIT_COUNT":
				sValue = this.GetDateLimitCount(pTag,pArgument);
				break;
			case "$GAME_FRIENDLY_POINT":
				sValue = this.GetFriendlyPoint(pTag,pArgument);
				break;
			case "$GAME_TOTAL_FRIENDLY_POINT":
				sValue = this.GetTotalFriendlyPoint(pTag,pArgument);
				break;
			case "$GAME_FORCE_RECOVERY_COOP_POINT":
				sValue = this.GetForceRecoveryCoopPoint(pTag,pArgument);
				break;
			case "$SELF_GAME_HAS_APPLICATION_COUNT":
				sValue = this.GetSelfHasApplicationCount(pTag,pArgument);
				break;
			case "$SELF_GAME_PARTNER_HUG_UNCHECKED_COUNT":
				sValue = this.GetSelfPartnerHugUncheckedCount(pTag,pArgument);
				break;
			case "$SELF_GAME_PARTNER_PRESENT_UNCHECKED_COUNT":
				sValue = this.GetSelfPartnerPresentUncheckedCount(pTag,pArgument);
				break;
			case "$SELF_GAME_ITEM_POSSESSION_COUNT_BY_CATEGORY":
				sValue = this.GetSelfGameItemPossessionCountByCategory(pTag,pArgument);
				break;
			case "$GAME_SWEET_HEART_COUNT":
				sValue = this.GetSweetHeartCount(pTag,pArgument);
				break;
			case "$GET_TOTAL_POSSESSION_MAN_TREASURE_COUNT":
				sValue = this.GetTotalPossessionManTreasureCount(pTag,pArgument);
				break;
			case "$DIV_IS_ATTACK_COUNT_OVER": {
					this.parseMan.SetNoneDisplay(!this.CheckAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$DIV_IS_NOT_ATTACK_COUNT_OVER": {
					this.parseMan.SetNoneDisplay(this.CheckAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$DIV_IS_PARTY_ATTACK_COUNT_OVER": {
					this.parseMan.SetNoneDisplay(!this.CheckPartyAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$DIV_IS_NOT_PARTY_ATTACK_COUNT_OVER": {
					this.parseMan.SetNoneDisplay(this.CheckPartyAttackCountOver(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR));
					break;
				}
			case "$SELF_GAME_STAGE_HONOR_BEFORE":

				sValue = GetHonor();
				break;
			case "$GET_GAME_STAGE_CLEAR_FLAG":
				sValue = GetStageClearFlag();
				break;
			case "$SELF_GAME_NOW_HUG_COUNT":
				sValue = GetNowHugCount();
				break;
			case "$GET_MAN_GAME_SERVICE_POINT":
				sValue = GetManGameServicePoint();
				break;
			case "$GET_GAME_ITEM_PRICE":
				sValue = GetGameItemPrice(pTag,pArgument,"PRICE");
				break;
			case "$GET_GAME_NAFLAG":
				sValue = GetNaFlag(pTag,pArgument);
				break;
			case "$GET_SELF_GAME_NAFLAG":
				sValue = GetSelfNaFlag();
				break;
			/*case "$GAME_LOTTERY_FREE_FLAG":
				sValue = GetGameLotteryFreeFlag(pTag,pArgument);
				break;*/
			case "$GAME_LOTTERY_NOT_COMP_FREE_FLAG":
				sValue = GetGameLotteryNotCompFreeFlag(pTag,pArgument);
				break;
			case "$SELF_GAME_INTRO_LOG_COUNT":
				sValue = this.GetSelfGameIntroLogCount(pTag,pArgument);
				break;
			case "$SELF_SITE_USE_STATUS":
				sValue = this.parseMan.sessionMan.userMan.characterEx.siteUseStatus;
				break;
			case "$DIV_IS_DATE_PARTNER_EXIST":
				this.parseMan.SetNoneDisplay(!this.CheckExistGameDatePartner(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_DATE_PARTNER_EXIST":
				this.parseMan.SetNoneDisplay(this.CheckExistGameDatePartner(pTag,pArgument));
				break;
			case "$SELF_GAME_FELLOW_COUNT_ALL_STATUS":
				sValue = this.GetGameFellowCountAllStatus(pTag,pArgument);
				break;
			case "$SELF_GAME_FAVORITE_COUNT":
				sValue = this.GetSelfFavoriteCount(pTag,pArgument);
				break;
			case "$GET_GAME_CHARACTER_TYPE":
				sValue = this.getGameCharacterType(pTag,pArgument);
				break;
			case "$DIV_IS_SELF_POSSESSION_GAME_ITEM_EXIST":
				this.parseMan.SetNoneDisplay(!this.CheckExistPossessionGameItem(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_SELF_POSSESSION_GAME_ITEM_EXIST":
				this.parseMan.SetNoneDisplay(this.CheckExistPossessionGameItem(pTag,pArgument));
				break;
			case "$DIV_IS_SELF_GAME_FAVORITE_EXIST":
				this.parseMan.SetNoneDisplay(!this.CheckExistGameFavorite(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_SELF_GAME_FAVORITE_EXIST":
				this.parseMan.SetNoneDisplay(this.CheckExistGameFavorite(pTag,pArgument));
				break;
			case "$GAME_TUTORIAL_TOWN_SEQ":
				sValue = this.GetGameTutorialTownSeq(pTag,pArgument);
				break;
			case "$SELF_GAME_SUPPORTED_REQUEST_COUNT":
				sValue = this.GetSelfRequestedSupportCount(pTag,pArgument);
				break;
			case "$SELF_GAME_APPLICATION_COUNT":
				sValue = this.GetSelfApplicationCount(pTag,pArgument);
				break;
			case "$SELF_PROFILE_UPDATE_DATE":
				sValue = this.parseMan.sessionMan.userMan.profileUpdateDate;
				break;
			case "$PAY_GAME_FLAG":
				sValue = this.parseMan.sessionMan.payGameFlag;
				break;
			case "$GET_CAST_GAME_PIC_ATTR_NM":
				sValue = this.GetCastGamePicAttrNm(pTag,pArgument);
				break;
			case "$GET_ROSE_EXPIRATION_DATE":
				sValue = this.GetRoseExpirationDate(pTag,pArgument);
				break;
			case "$GET_GAME_FIND_CAST_COUNT":
				sValue = GetCastFindCount(pTag,pArgument);
				break;
			case "$DIV_IS_GAME_CAST_EXIST":{
				string sCount = GetCastFindCount(pTag,pArgument);
				int iCount;
				int.TryParse(sCount,out iCount);
				this.parseMan.SetNoneDisplay(!(iCount > 0));
				break;
			}
			case "$DIV_IS_NOT_GAME_CAST_EXIST": {
				string sCount = GetCastFindCount(pTag,pArgument);
				int iCount;
				int.TryParse(sCount,out iCount);
				this.parseMan.SetNoneDisplay((iCount > 0));
				break;
			}
			case "$GAME_NOW_PUBLISH_QUEST_SEQ":
				sValue = this.GetNowPublishQuestSeq(pTag,pArgument);
				break;
			case "$OBJ_USED_HISTORY_FLG": {
					this.parseMan.SetFieldValue(pTag,"OBJ_USED_HISTORY_FLG",out sValue);
				break;
			}
			case "$GET_MAN_TREASURE_SCRIPT":
				sValue = this.GetManTreasureScript(pTag,pArgument);
				break;
			case "$GET_MAN_TREASURE_FLASH_VARS":
				sValue = this.GetManTreasureFlashVars(pTag,pArgument);
				break;
			case "$GET_GAME_ITEM_SEQ":
				sValue = this.GetGameItemSeq(pTag,pArgument);
				break;
			case "$GET_CAST_HANDLE_NM":
				sValue = this.GetCastHandleNm(pTag,pArgument);
				break;
			case "$DIV_IS_NOW_PUBLISHED_QUEST":
				this.parseMan.SetNoneDisplay(!this.CheckQuestPublish(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_NOW_PUBLISHED_QUEST":
				this.parseMan.SetNoneDisplay(this.CheckQuestPublish(pTag,pArgument));
				break;
			case "$GET_GAME_INFORMATION_COUNT":
				sValue = this.GetGameInformationCount(pTag,pArgument);
				break;
			case "$SELF_GAME_LOGIN_BONUS_TODAY_GET_FLAG":
				sValue = this.GetGameLoginBonusGetFlag(pTag,pArgument);
				break;
			case "$GET_GAME_TUTORIAL_BATTLE_TREASURE_ATTR_SEQ":
				sValue = this.GetGameTutorialBattleTreasureAttrSeq(pTag,pArgument);
				break;
			case "$GAME_TUTORIAL_STAGE_SEQ":
				sValue = this.GetGameTutorialStageSeq(pTag,pArgument);
				break;
			case "$SELF_CAST_PIC_GACHA_WIN_COUNT":
				sValue = this.GetSelfCastPicGachaWinCount(pTag,pArgument);
				break;
			case "$GET_PHOTO_FLASH_VARS":
				sValue = this.GetPhotoFlashVars(pTag,pArgument);
				break;
			case "$GET_PHOTO_SCRIPT":
				sValue = this.GetPhotoScript(pTag,pArgument);
				break;
			case "$GET_PHOTO_SCRIPT_SP":
				sValue = this.GetPhotoScriptSp(pTag,pArgument);
				break;
			case "$SELF_ORIGINAL_WOMAN_USER_SEQ":
				sValue = this.parseMan.sessionMan.originalWomanObj.userWoman.userSeq;
				break;
			case "$SELF_ORIGINAL_WOMAN_USER_CHAR_NO":
				sValue = this.parseMan.sessionMan.originalWomanObj.userWoman.curCharNo;
				break;
			case "$BLADE_SCRIPT_SP":
				sValue = this.BladeScriptSp(pTag,pArgument);
				break;
			case "$SELF_NG_COUNT":
				sValue = this.parseMan.sessionMan.userMan.ngCount.ToString();
				break;
			case "$SELF_NG_SKULL_COUNT":
				sValue = this.parseMan.sessionMan.userMan.ngSkullCount.ToString();
				break;
			case "$SELF_DAYS_FROM_REGIST":
				sValue = this.GetDaysFromRegist(pTag,pArgument);
				break;
			case "$SELF_FIRST_SETTLE_ENABLE_FLAG":
				sValue = this.GetFirstSettleEnableFlag(pTag,pArgument);
				break;
			case "$SELF_DAYS_FIRST_SETTLE_ENBALE":
				sValue = this.GetDaysFirstSettleEnable(pTag,pArgument);
				break;
			case "$SELF_LIKE_ME_COUNT":
				sValue = this.GetLikeMeCastCharacterCount(pTag,pArgument);
				break;
			case "$SELF_FAVORIT_COUNT":
				sValue = this.GetSelfFavoritCastCharacterCount(pTag,pArgument);
				break;
			case "$QUERY_AD_FOR_PHP":
				sValue = this.GetQueryAdForPhp(pTag,pArgument);
				break;
			//お気に入りグループ
			case "$SELF_FAVORIT_GROUP_COUNT":
				sValue = this.GetSelfFavoritGroupCount();
				break;
			case "$GET_FAVORIT_GROUP_NM":
				sValue = this.GetFavoritGroupNm(pTag,pArgument);
				break;
			//ファンクラブ
			case "$SELF_SP_PREMIUM_END_DATE":
				sValue = parseMan.sessionMan.userMan.characterEx.spPremiumEndDate;
				break;
			case "$SELF_BEST_FANCLUB_MEMBER_RANK":
				sValue = this.GetSelfBestFanClubMemberRank();
				break;
			case "$FANCLUB_FEE_POINT":
				sValue = this.GetFanclubFeePoint();
				break;
			//ピックアップ
			case "$GET_PICKUP_OBJS_COMMENT_COUNT":
				sValue = this.GetPickupObjsCommentCount(pTag,pArgument);
				break;
			//この娘を探せ
			case "$WANTED_MAIL_RX_TIME_SHORT_FLAG":
				sValue = this.CheckWantedMailRxTimeShort(pTag,pArgument);
				break;
			case "$WANTED_NOT_ACQUIRED_POINT_FLAG":
				sValue = this.CheckNotAcquiredPointExists(pTag,pArgument);
				break;
			case "$WANTED_CANNOT_ENTRY_FLAG":
				if (!this.parseMan.sessionMan.userMan.castMailRxType.Equals(ViCommConst.MAIL_RX_BOTH)) {
					sValue = ViCommConst.FLAG_ON_STR;
				} else if (!this.parseMan.sessionMan.userMan.InfoMailRxType.Equals(ViCommConst.MAIL_RX_BOTH)) {
					sValue = ViCommConst.FLAG_ON_STR;
				} else if (this.CheckWantedMailRxTimeShort(pTag,pArgument).Equals(ViCommConst.FLAG_ON_STR)) {
					sValue = ViCommConst.FLAG_ON_STR;
				} else if (!this.parseMan.sessionMan.userMan.nonExistEmailAddrFlag.Equals(0)) {
					sValue = ViCommConst.FLAG_ON_STR;
				} else {
					sValue = ViCommConst.FLAG_OFF_STR;
				}
				break;
			case "$GET_CAST_BBS_PIC_COUNT":
				sValue = this.GetCastBbsPicCount(pTag,pArgument).ToString();
				break;
			case "$GET_CAST_BBS_MOVIE_COUNT":
				sValue = this.GetCastBbsMovieCount(pTag,pArgument).ToString();
				break;
			case "$GET_CAST_BBS_PIC_MOVIE_COUNT": {
				int iValue = 0;
				int iPicCount = this.GetCastBbsPicCount(pTag,pArgument);
				int iMovCount = this.GetCastBbsMovieCount(pTag,pArgument);
				iValue = iPicCount + iMovCount;
				sValue = iValue.ToString();
				break;
			}
			case "$SELF_VOTE_TICKET_COUNT":
				sValue = this.GetSelfVoteTicketCount(pTag,pArgument).ToString();
				break;
			case "$SELF_ELECTION_VOTE_TICKET_COUNT":
				sValue = this.GetSelfElectionVoteTicketCount(pTag,pArgument).ToString();
				break;

			// お宝deビンゴ
			case "$DIV_IS_GET_BBS_BINGO_NO_ENABLE":
				this.parseMan.SetNoneDisplay(!GetBbsBingoNoEnable(pTag,pArgument));
				break;
			// 出演者動画コメント
			case "$DIV_IS_CAST_MOVIE_COMMENT_ENABLE":
				this.parseMan.SetNoneDisplay(!GetCastMovieCommentEnable(pTag,pArgument));
				break;
			// 出演者画像コメント
			case "$DIV_IS_CAST_PIC_COMMENT_ENABLE":
				this.parseMan.SetNoneDisplay(!GetCastPicCommentEnable(pTag,pArgument));
				break;
			case "$SELF_DEVICE_TOKEN":
				sValue = this.parseMan.sessionMan.deviceToken;
				break;
			case "$SELF_DEVICE_UUID":
				sValue = this.parseMan.sessionMan.deviceUuid;
				break;
			case "$SELF_DEVICE_ID":
				sValue = this.parseMan.sessionMan.deviceId;
				break;
			case "$SELF_ANDROID_ID":
				sValue = this.parseMan.sessionMan.androidId;
				break;
			case "$SELF_DEVICE_TEL":
				sValue = this.parseMan.sessionMan.deviceTel;
				break;
			case "$SELF_DEVICE_SERIAL_NO":
				sValue = this.parseMan.sessionMan.deviceSerialNo;
				break;
			case "$SELF_MAC_ADDRESS":
				sValue = this.parseMan.sessionMan.macAddress;
				break;
			case "$SELF_GAME_IMG_PATH":
				sValue = this.GetSelfGameImgPath(pTag,pArgument);
				break;
			case "$DIV_IS_SELF_TEL_OVERLAP":
				this.parseMan.SetNoneDisplay(!GetSelfTelOverlap(pTag,pArgument));
				break;
			//プレゼントメール
			case "$DIV_IS_PRESENT_MAIL_TERM":
				this.parseMan.SetNoneDisplay(!CheckCurrentPresentMail(pTag,pArgument));
				break;
			//メールおねだり
			case "$DIV_CAN_REGIST_MAIL_REQUEST":
				this.parseMan.SetNoneDisplay(!CheckCanRegistMailRequestLog(pTag,pArgument));
				break;
			case "$DIV_CAN_NOT_REGIST_MAIL_REQUEST":
				this.parseMan.SetNoneDisplay(CheckCanRegistMailRequestLog(pTag,pArgument));
				break;
			case "$SELF_REST_MAIL_REQUEST_COUNT":
				sValue = this.GetRestMailRequestCount(pTag,pArgument);
				break;
			//メールdeガチャ
			case "$DIV_IS_MAIL_LOTTERY_OPEN":
				this.parseMan.SetNoneDisplay(!this.GetMailLotteryOpening(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_MAIL_LOTTERY_OPEN":
				this.parseMan.SetNoneDisplay(this.GetMailLotteryOpening(pTag,pArgument));
				break;
			case "$DIV_IS_CAN_GET_MAIL_LOTTERY":
				this.parseMan.SetNoneDisplay(!this.CheckCanGetMailLottery(pTag,pArgument));
				break;

			case "$GCAPP_AUTH_LINK":
				sValue = GetGcappAuthLink(pTag,pArgument);
				break;
			case "$SELF_GCAPP_VERSION":
				sValue = GetSelfGcappVersion(pTag,pArgument);
				break;
			case "$DIV_IS_SELF_GCAPP_NEED_UPDATE":
				this.parseMan.SetNoneDisplay(!this.IsSelfGcappNeedUpdate(pTag,pArgument));
				break;
			case "$LINK_LIST_CAST_DIARY":
				sValue = "ListCastDiary.aspx";
				break;
			case "$SELF_LAST_SETTLE_TYPE":
				sValue = GetSelfLastSettleType(pTag,pArgument);
				break;
			//プロフ登録でPtゲット
			case "$DIV_IS_PROFILE_COMPLETE":
				this.parseMan.SetNoneDisplay(!this.CheckProfileComplete(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_PROFILE_COMPLETE":
				this.parseMan.SetNoneDisplay(this.CheckProfileComplete(pTag,pArgument));
				break;
			case "$SELF_PROFILE_SECRET_COUNT":
				sValue = GetProfileSecretCount(pTag,pArgument);
				break;
			case "$SELF_PROFILE_TEXT_LENGTH":
				sValue = GetProfileCommentLength(pTag,pArgument);
				break;
			case "$DIV_IS_ADDABLE_PROFILE_POINT":
				this.parseMan.SetNoneDisplay(!this.CheckAddableProfilePoint(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_ADDABLE_PROFILE_POINT":
				this.parseMan.SetNoneDisplay(this.CheckAddableProfilePoint(pTag,pArgument));
				break;
			//返信率100%の女の子
			case "$DIV_IS_QUICK_RESPONSE_ENTRY":
				this.parseMan.SetNoneDisplay(!this.IsQuickResponseEntry(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_QUICK_RESPONSE_ENTRY":
				this.parseMan.SetNoneDisplay(this.IsQuickResponseEntry(pTag,pArgument));
				break;
			//クレジット定額購入
			case "$SELF_CREDIT_AUTO_SETTLE_EXIST_FLAG":
				sValue = this.GetCreditAutoSettleExistFlag(pTag,pArgument) == true ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;

			#region ---------- セッション開始後利用可能・システム関連 ----------
			case "$ADD_PRESENT_GAME_ITEM_STAFF_EXT":
				this.ExecutePresentGameItemStaffExt(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.userCharNo,pArgument);
				break;
			case "$DIV_IS_ADDABLE_PRESENT_GAME_ITEM_STAFF_EXT":
				this.parseMan.SetNoneDisplay(!this.CheckPresentGameItemStaffExt(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.userCharNo,pArgument).Equals(PwViCommConst.PresentGameItemStaffExt.RESULT_OK));
				break;
			case "$DIV_IS_NOT_ADDABLE_PRESENT_GAME_ITEM_STAFF_EXT":
				this.parseMan.SetNoneDisplay(this.CheckPresentGameItemStaffExt(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.userCharNo,pArgument).Equals(PwViCommConst.PresentGameItemStaffExt.RESULT_OK));
				break;
			case "$SELF_GAME_FELLOW_COUNT_EX":
				sValue = GetSelfGameFellowCountEx(pTag,pArgument);
				break;
			#endregion
			#region □■□■□ PARTS定義 □■□■□
			case "$POSSESSION_GAME_ITEM_PARTS":
				sValue = this.GetListPossessionGameItem(pTag,pArgument);
				break;
			case "$CAST_GAME_CHARACTER_PARTS":
				sValue = this.GetListGameCharacter(pTag,pArgument);
				break;
			case "$CAST_GAME_CHARACTER_OUTER_PARTS":
				sValue = this.GetCastGameCharacterOuter(pTag,pArgument);
				break;
			case "$CAST_GAME_PIC_DATA_PARTS":
				sValue = this.GetListManTreasure(pTag,pArgument);
				break;
			case "$CAST_GAME_CHARACTER_FELLOW_NO_HUG_PARTS":
				sValue = this.GetListGameCharacterFellowNoHug(pTag,pArgument);
				break;
			case "$PARTY_BATTLE_MEMBER_PARTS":
				sValue = this.GetListPartyBattleMember(pTag,pArgument);
				break;
			case "$GAME_ITEM_PARTS":
				sValue = this.GetGameItemDataList(pTag,pArgument);
				break;
			case "$USE_RECOVERY_ITEM_LINK_PARTS":
				sValue = this.GetRecoveryItemData(pTag,pArgument);
				break;
			case "$GET_MAN_TREASURE_LOG_PARTS":
				sValue = this.GetGetManTreasureLog(pTag,pArgument);
				break;
			case "$GAME_FELLOW_LOG_PARTS":
				sValue = this.GetFellowLog(pTag,pArgument);
				break;
			case "$GAME_FAVORITE_LOG_PARTS":
				sValue = this.GetFavoriteLog(pTag,pArgument);
				break;
			case "$GAME_PARTNER_HUG_PARTS":
				sValue = this.GetGamePartnerHug(pTag,pArgument);
				break;
			case "$GAME_MOSAIC_RANK_PARTS":
				sValue = this.GetMosaicRankData(pTag,pArgument);
				break;
			case "$GAME_CAST_TODAY_PARTS":
				sValue = this.GetCastToday(pTag,pArgument);
				break;
			case "$COMP_ALL_CALENDAR_PARTS":
				sValue = this.GetListAllCompCalendar(pTag,pArgument);
				break;
			case "$GAME_BATTLE_LOG_PARTS":
				sValue = this.GetBattleLog(pTag,pArgument);
				break;
			case "$NEXT_STAGE_PARTS":
				sValue = this.GetNextStage(pTag,pArgument);
				break;
			case "$GET_NEXT_STAGE_PARTS":
				sValue = this.GetNextStage(pTag,pArgument);
				break;
			case "$GET_ONE_BOSS_DATA_PARTS":
				sValue = this.GetOneBossData(pTag,pArgument);
				break;
			case "$CAST_TREASURE_PARTS":
				sValue = this.GetCastTreasureData(pTag,pArgument);
				break;
			case "$STAGE_CLEAR_POINT_PARTS":
				sValue = this.GetStageClearPoint(pTag,pArgument);
				break;
			case "$GET_CLEAR_TOWN_SEQ_DATA_PARTS":
				sValue = this.GetClearTownSeqData(pTag,pArgument);
				break;
			case "$CAST_MOVIE_PARTS":
				sValue = this.GetListCastMovie(pTag,pArgument);
				break;
			case "$CAST_PIC_PARTS":
				sValue = this.GetListCastPic(pTag,pArgument);
				break;
			case "$CAST_BBS_OBJ_PARTS":
				sValue = this.GetListCastBbsObj(pTag,pArgument);
				break;
			case "$OBJ_REVIEW_TOTAL_PARTS":
				sValue = this.GetObjReviewTotal(pTag,pArgument);
				break;
			case "$GAME_ITEM_BY_CATEGORY_PARTS":
				sValue = this.GetItemDataByCategory(pTag,pArgument);
				break;
			case "$CAST_GAME_CHARACTER_OUTER_TUTORIAL_GET_TREASURE_PARTS":
				sValue = this.GetCastGameCharacterOuterTutorialGetTreasure(pTag,pArgument);
				break;
			case "$GET_MAN_TREASURE_LOG_TUTORIAL_PARTS":
				sValue = this.GetGetManTreasureLogTutorial(pTag,pArgument);
				break;
			case "$GAME_TREASURE_COMP_ITEM_PARTS":
				sValue = this.GetGameTreasureCompItem(pTag,pArgument);
				break;
			case "$GAME_INTRO_ITEM_LIST_PARTS":
				sValue = this.GetGameIntroItemByIntroCount(pTag,pArgument);
				break;
			case "$GAME_NEW_HUG_PARTS":
				sValue = this.GetOneNewHug(pTag,pArgument);
				break;
			case "$GAME_PARTNER_PRESENT_GET_ONE_UNCHECKED_PARTS":
				sValue = this.GetOneSelfPartnerPresentUnchecked(pTag,pArgument);
				break;
			case "$GAME_GET_ONE_FELLOW_APPLICATION_PARTS":
				sValue = this.GetOneFellowApplication(pTag,pArgument);
				break;
			case "$GAME_GET_ONE_SUPPORT_REQUEST_PARTS":
				sValue = this.GetOneSupportRequest(pTag,pArgument);
				break;
			case "$REQUEST_PARTS":
				sValue = this.GetListRequest(pTag,pArgument);
				break;
			case "$GAME_INFORMATION_PARTS":
				sValue = this.GetInformation(pTag,pArgument);
				break;
			case "$GAME_QUEST_REWARD_LIST_PARTS":
				sValue = this.GetQuestRewardList(pTag,pArgument);
				break;
			case "$GAME_QUEST_INFORMATION_PARTS":
				sValue = this.GetQuestInformation(pTag,pArgument);
				break;
			case "$GAME_QUEST_CLEAR_INFO_PARTS":
				sValue = this.GetQuestClearInfo(pTag,pArgument);
				break;
			case "$GAME_QUEST_ENTRY_QUEST_INFO":
				sValue = this.GetEntryQuestInfo(pTag,pArgument);
				break;
			case "$SELF_MAN_TREASURE_ATTR_PARTS":
				sValue = this.GetManTreasureCompStatus(pTag,pArgument);
				break;
			case "$GAME_TOWN_PARTS":
				sValue = this.GetListTown(pTag,pArgument);
				break;
			case "$GAME_SWEET_HEART_PARTS":
				sValue = this.GetSweetHeartData(pTag,pArgument);
				break;
			case "$GAME_HER_SWEET_HEART_PARTS":
				sValue = this.GetHerSweetHeartData(pTag,pArgument);
				break;
			case "$INFO_MAIL_PARTS":
				sValue = this.GetInfoMail(pTag,pArgument);
				break;
			case "$CAST_TWEET_PARTS":
				sValue = this.GetCastTweet(pTag,pArgument);
				break;
			case "$MAN_TWEET_PARTS":
				sValue = this.GetManTweet(pTag,pArgument);
				break;
			case "$CAST_DATA_BY_USER_SEQ_PARTS":
				sValue = this.GetCastDataByUserSeq(pTag,pArgument);
				break;
			case "$FANCLUB_STATUS_PARTS":
				sValue = this.GetFanClubStatus(pTag,pArgument);
				break;
			case "$CAST_NEWS_PARTS":
				sValue = this.GetCastNews(pTag,pArgument);
				break;
			case "$CAST_FAVORIT_NEWS_PARTS":
				sValue = this.GetCastFavoritNews(pTag,pArgument);
				break;
			case "$CAST_NEW_FACE_PARTS":
				sValue = this.GetCastNewFace(pTag,pArgument);
				break;
			case "$USER_MAIL_PARTS":
				sValue = this.GetUserMail(pTag,pArgument);
				break;
			case "$GET_TOTAL_TX_RX_MAIL_COUNT":
				sValue = this.GetTotalTxRxMailCount(pTag,pArgument);
				break;
			case "$SELF_LIKE_ME_PARTS":
				sValue = this.GetLikeMeCastCharacter(pTag,pArgument);
				break;
			case "$SELF_FAVORIT_PARTS":
				sValue = this.GetSelfFavoritCastCharacter(pTag,pArgument);
				break;
			//ピックアップ
			case "$PICKUP_OBJS_VOTE_COUNT_PARTS":
				sValue = this.GetPickupObjsVoteCountData(pTag,pArgument);
				break;
			//この娘を探せ
			case "$WANTED_COMPLETE_SHEET_INDEX_PARTS":
				sValue = this.MakeWantedCompleteSheetIndex(pTag,pArgument);
				break;
			case "$WANTED_MONTHLY_BOUNTY_DATA_PARTS":
				sValue = this.GetWantedMonthlyPointData(pTag,pArgument);
				break;
			//お宝deビンゴ
			case "$BBS_BINGO_LOG_PARTS":
				sValue = this.GetBbsBingoLog(pTag,pArgument);
				break;
			case "$BBS_BINGO_ENTRY_PRIZE_PARTS":
				sValue = this.GetBbsBingoEntryPrize(pTag,pArgument);
				break;
			case "$BBS_BINGO_CURRENT_TERM_PARTS":
				sValue = this.GetBbsBingoCurrentTerm(pTag,pArgument);
				break;
			case "$BBS_BINGO_LATEST_TERM_PARTS":
				sValue = this.GetBbsBingoLatestTerm(pTag,pArgument);
				break;
			//オーディションネット投票
			case "$ELECTION_RANK_PARTS":
				sValue = this.GetOneRankElection(pTag,pArgument);
				break;
			case "$ONLINE_CAST_PARTS":
				sValue = this.GetOnlineCast(pTag,pArgument);
				break;
			case "$SELF_MARKING_PARTS":
				sValue = this.GetSelfMarking(pTag,pArgument);
				break;
			case "$CAST_WAIT_SCHEDULE_PARTS":
				sValue = this.GetCastWaitSchedule(pTag,pArgument);
				break;
			case "$CAST_LOGIN_PLAN_PARTS":
				sValue = this.GetCastLoginPlan(pTag,pArgument);
				break;
			case "$MAIL_REQUEST_CONFIRM_PARTS":
				sValue = this.MailRequestConfirm(pTag,pArgument);
				break;
			case "$MAIL_LOTTERY_RATE_PARTS":
				sValue = this.GetMailLotteryRate(pTag,pArgument);
				break;
			case "$QUICK_RESPONSE_PARTS":
				sValue = this.GetCastQuickResponse(pTag,pArgument);
				break;
			//リッチーノ
			case "$SELF_RICHINO_RANK_KEEP_PARTS":
				sValue = GetSelfRichinoRankKeepAmt(pTag,pArgument);
				break;
			#endregion

			default:
				switch (this.parseMan.currentTableIdx) {
					case PwViCommConst.DATASET_GAME_ITEM:
						sValue = this.ParseGameItem(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_PARTNER_GAME_CHARACTER:
						sValue = this.ParseGameCharacter(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_TREASURE:
						sValue = this.ParseManTreasure(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_DATE_PLAN:
						sValue = this.ParseDatePlan(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_STAGE:
						sValue = this.ParseStage(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_TOWN:
						sValue = this.ParseTown(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_INTRO_LOG:
						sValue = this.ParseGameIntroLog(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_OBJ_REVIEW:
						sValue = this.ParseObjReview(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_REQUEST:
						sValue = this.ParseRequest(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_REQUEST_VALUE:
						sValue = this.ParseRequestValue(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_QUEST:
						sValue = this.ParseQuest(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_FAVORIT_GROUP:
						sValue = this.ParseFavoritGroup(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_LOTTERY:
						sValue = this.ParseLottery(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_GAME_QUEST_ENTRY_LOG:
						sValue = this.ParseQuestEntryLog(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_BLOG_COMMENT:
						sValue = this.ParseBlogComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAN_TWEET:
						sValue = this.ParseManTweet(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAN_TWEET_COMMENT:
						sValue = this.ParseManTweetComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CAST_TWEET:
						sValue = this.ParseCastTweet(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CAST_TWEET_COMMENT:
						sValue = this.ParseCastTweetComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAN_TWEET_LIKE:
						sValue = this.ParseManTweetLike(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_YAKYUKEN_GAME:
						sValue = this.ParseYakyukenGame(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_YAKYUKEN_PIC:
						sValue = this.ParseYakyukenPic(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_YAKYUKEN_JYANKEN_PIC:
						sValue = this.ParseYakyukenJyankenPic(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_PICKUP_OBJS_COMMENT:
						sValue = this.ParsePickupObjsComment(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_WANTED_BOUNTY:
						sValue = this.ParseWantedBounty(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_EVENT:
						sValue = this.ParseEvent(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_BBS_BINGO:
						sValue = this.ParseBbsBingo(pTag,pArgument,out pParsed);
						break;
					case ViCommConst.DATASET_MAN:
						sValue = this.ParseMan(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_ELECTION_ENTRY:
						sValue = this.ParseElectionEntry(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_WAIT_SCHEDULE:
						sValue = this.ParseWaitSchedule(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_PRESENT_MAIL_ITEM:
						sValue = this.ParsePresentMailItem(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_LOGIN_PLAN:
						sValue = this.ParseLoginPlan(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_MAIL_LOTTERY:
						sValue = this.ParseMailLottery(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_DEFECT_REPORT:
						sValue = this.ParseDefectReport(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_RICHINO_RANK:
						sValue = this.ParseRichinoRank(pTag,pArgument,out pParsed);
						break;
					case PwViCommConst.DATASET_CREDIT_AUTO_SETTLE:
						sValue = this.ParseCreditAutoSettle(pTag,pArgument,out pParsed);
						break;
					default:
						pParsed = false;
						break;
				}
				break;
		}

		if (!pParsed) {
			sValue = base.Parse(pTag,pArgument,out pParsed);
		}
		return sValue;
	}


	private void SetupGameTutorialStatus(string pTag,string pArgument) {
		SetupGameTutorialStatus(pTag,this.parseMan.sessionMan.site.siteCd,this.parseMan.sessionMan.userMan.userSeq,this.parseMan.sessionMan.userMan.userCharNo,pArgument);
	}

	public string getGameHandleName(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string user_seq;
		string user_char_no;

		if (arrText.Length > 1) {
			user_seq = arrText[0];
			user_char_no = arrText[1];

			GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
			oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
			oCondition.PartnerUserSeq = user_seq;
			oCondition.PartnerUserCharNo = user_char_no;
			oCondition.SeekMode = ulong.Parse(this.parseMan.sessionObjs.seekMode);

			using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
				DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);

				DataRow dr = ds.Tables[0].Rows[0];
				sValue = dr["GAME_HANDLE_NM"].ToString();
			}
		}

		return sValue;
	}

	private string GetGameItemSaleStatus(string pTag,string pArgument) {
		string sValue = string.Empty;

		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sSexCd = this.parseMan.sessionMan.sexCd;

		DataSet ds = null;
		GameItemSale oGameItemSale = new GameItemSale();
		ds = oGameItemSale.GetCurrentSaleStatus(sSiteCd,sSexCd);

		DataRow dr = ds.Tables[0].Rows[0];

		sValue = dr["GAME_ITEM_SALE_STATE"].ToString();

		return sValue;
	}

	private string GetGameItemNm(string pTag,string pArgument) {
		string sGameItemNm = string.Empty;
		if (!pArgument.Equals(string.Empty)) {
			sGameItemNm = GetGameItemNm(pTag,this.parseMan.sessionMan.site.siteCd,this.parseMan.sessionMan.sexCd,pArgument);
		}
		return sGameItemNm;
	}

	private string GetTodayDateCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		using (DatePlan oDatePlan = new DatePlan()) {
			sValue = oDatePlan.GetTodayDateCount(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				this.parseMan.sessionMan.userMan.userCharNo
			);
		}
		return sValue;
	}

	private string GetDateLimitCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["DATE_LIMIT_COUNT"].ToString();
			}
		}
		return sValue;
	}

	private string GetForceRecoveryCoopPoint(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["FORCE_RECOVERY_COOP_POINT"].ToString();
			}
		}
		return sValue;
	}

	private string GetSelfHasApplicationCount(string pTag,string pArgument) {
		string sValue = GetSelfHasApplicationCount(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo
		);
		return sValue;
	}

	private string GetSelfPartnerHugUncheckedCount(string pTag,string pArgument) {
		string sValue = GetSelfPartnerHugUncheckedCount(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo,
			this.getGameCharacter().lastHugLogCheckDate
		);
		return sValue;
	}

	private string GetSelfPartnerPresentUncheckedCount(string pTag,string pArgument) {
		string sValue = GetSelfPartnerPresentUncheckedCount(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo,
			PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY
		);
		return sValue;
	}

	private string GetSelfGameItemPossessionCountByCategory(string pTag,string pArgument) {
		string sValue = GetSelfGameItemPossessionCountByCategory(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.sexCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo,
			pArgument
		);
		return sValue;
	}

	private string CheckAttackCountOver(string pTag,string pArgument) {
		string sAttackCountOverFlag = string.Empty;
		if (string.IsNullOrEmpty(pArgument)) {
			return sAttackCountOverFlag;
		}

		string[] textArr = pArgument.Split(',');
		if (textArr.Length < 2) {
			return sAttackCountOverFlag;
		}

		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = parseMan.sessionMan.userMan.userCharNo;
		string sPartnerUserSeq = textArr[0];
		string sPartnerUserCharNo = textArr[1];

		using (DailyBattleLog oDailyBattleLog = new DailyBattleLog()) {
			sAttackCountOverFlag = oDailyBattleLog.CheckAttackCountOver(sSiteCd,sUserSeq,sUserCharNo,sPartnerUserSeq,sPartnerUserCharNo);
		}

		return sAttackCountOverFlag;
	}

	private string CheckPartyAttackCountOver(string pTag,string pArgument) {
		string sPartyAttackCountOverFlag = string.Empty;
		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = parseMan.sessionMan.userMan.userCharNo;

		using (DailyPartyBattleLog oDailyPartyBattleLog = new DailyPartyBattleLog()) {
			sPartyAttackCountOverFlag = oDailyPartyBattleLog.CheckPartyAttackCountOver(sSiteCd,sUserSeq,sUserCharNo);
		}

		return sPartyAttackCountOverFlag;
	}

	private string GetHonor() {

		string sValue;
		int iStageLevel = 0;
		iStageLevel = int.Parse(this.getGameCharacter().stageLevel.ToString()) - 1;

		using (Stage oStage = new Stage()) {
			sValue = oStage.GetHonor(parseMan.sessionMan.site.siteCd,iStageLevel,this.parseMan.sessionMan.sexCd);
		}

		return sValue;

	}

	private string GetStageClearFlag() {

		string sValue;
		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = parseMan.sessionMan.userMan.userCharNo;

		using (Stage oStage = new Stage()) {
			sValue = oStage.GetStageClearFlag(sSiteCd,sUserSeq,sUserCharNo,this.getGameCharacter().stageSeq.ToString());
		}

		return sValue;

	}

	private string GetNowHugCount() {
		string sValue;
		using (GameFellow oGameFellow = new GameFellow()) {
			sValue = oGameFellow.GetNowHugCount(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.userCharNo);
		}
		return sValue;
	}

	private string GetManGameServicePoint() {
		string sValue = string.Empty;
		int iValue = 0;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				iValue = int.Parse(ds.Tables[0].Rows[0]["MAN_GAME_SERVICE_POINT"].ToString());
				sValue = String.Format("{0:#,0}",iValue);
			}
		}
		return sValue;
	}

	private string GetGameItemPrice(string pTag,string pArgument,string pItemDistinction) {
		string sValue = string.Empty;

		using (GameItem oGameItem = new GameItem()) {
			sValue = oGameItem.GetGameItem(this.parseMan.sessionMan.userMan.siteCd,this.parseMan.sessionMan.sexCd,pArgument,pItemDistinction);
		}

		return sValue;
	}

	public string GetNaFlag(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string pPartnerUserSeq;
		string pPartnerUserCharNo;

		if (arrText.Length > 1) {
			pPartnerUserSeq = arrText[0];
			pPartnerUserCharNo = arrText[1];

			DataSet oDataSet;
			using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
				oDataSet = oCastGameCharacter.GetOne(this.parseMan.sessionMan.userMan.siteCd,pPartnerUserSeq,pPartnerUserCharNo);
			}

			if (oDataSet.Tables[0].Rows.Count > 0) {
				sValue = oDataSet.Tables[0].Rows[0]["NA_FLAG"].ToString();
			}
			return sValue;
		}

		return sValue;
	}

	public string GetSelfNaFlag() {
		string sValue = "";

		DataSet oDataSet;
		using (ManCharacter oManCharacter = new ManCharacter()) {
			oDataSet = oManCharacter.GetOne(this.parseMan.sessionMan.userMan.siteCd,parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.userCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["NA_FLAG"].ToString();
		}
		return sValue;
	}

	private string GetSelfGameIntroLogCount(string pTag,string pArgument) {
		string sValue = GetSelfGameIntroLogCount(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo,
			pArgument
		);
		return sValue;
	}

	private string GetListCastMovie(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);
		string sLoginid = iBridUtil.GetStringValue(oPartsArgs.Query["loginid"]);
		string sCharNo = ViCommConst.MAIN_CHAR_NO;
		string sMovieType = iBridUtil.GetStringValue(oPartsArgs.Query["movietype"]);
		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		bool bOnlyUsed = iBridUtil.GetStringValue(oPartsArgs.Query["used"]).Equals(ViCommConst.FLAG_ON_STR);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();

		oCondition.pickupId = iBridUtil.GetStringValue(oPartsArgs.Query["pickupid"]);
		oCondition.sortType = iBridUtil.GetStringValue(oPartsArgs.Query["sorttype"]);

		using (CastMovie oCastMovie = new CastMovie(parseMan.sessionObjs)) {
			if (!sObjSeq.Equals(string.Empty)) {
				oTmpDataSet = oCastMovie.GetPageCollectionBySeq(sObjSeq,true);
			} else {
				oTmpDataSet = oCastMovie.GetPageCollection(
					this.parseMan.sessionMan.site.siteCd,
					sLoginid,
					sCharNo,
					string.Empty,
					false,
					sMovieType,
					string.Empty,
					sAttrTypeSeq,
					sAttrSeq,
					bOnlyUsed,
					oCondition,
					ViCommConst.SORT_NO,
					1,
					oPartsArgs.NeedCount,
					true
				);
			}
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_PARTNER_MOVIE);

		return sValue;
	}

	private string GetListCastPic(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);
		string sLoginid = iBridUtil.GetStringValue(oPartsArgs.Query["loginid"]);
		string sCharNo = ViCommConst.MAIN_CHAR_NO;
		string sPicType = iBridUtil.GetStringValue(oPartsArgs.Query["pictype"]);
		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		bool bOnlyUsed = iBridUtil.GetStringValue(oPartsArgs.Query["used"]).Equals(ViCommConst.FLAG_ON_STR);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();

		oCondition.pickupId = iBridUtil.GetStringValue(oPartsArgs.Query["pickupid"]);
		oCondition.sortType = iBridUtil.GetStringValue(oPartsArgs.Query["sorttype"]);

		using (CastPic oCastPic = new CastPic(parseMan.sessionObjs)) {
			if (!sObjSeq.Equals(string.Empty)) {
				oTmpDataSet = oCastPic.GetPageCollectionBySeq(sObjSeq,sPicType);
			} else {
				oTmpDataSet = oCastPic.GetPageCollection(
					this.parseMan.sessionMan.site.siteCd,
					sLoginid,
					sCharNo,
					false,
					sPicType,
					string.Empty,
					sAttrTypeSeq,
					sAttrSeq,
					bOnlyUsed,
					oCondition,
					1,
					oPartsArgs.NeedCount
				);
			}
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_PARTNER_PIC);

		return sValue;
	}

	private string GetListCastBbsObj(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);
		string sLoginid = iBridUtil.GetStringValue(oPartsArgs.Query["loginid"]);
		string sCharNo = ViCommConst.MAIN_CHAR_NO;
		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		bool bAllBbs = iBridUtil.GetStringValue(oPartsArgs.Query["allbbs"]).Equals("1");
		bool bOnlyUsed = iBridUtil.GetStringValue(oPartsArgs.Query["used"]).Equals(ViCommConst.FLAG_ON_STR);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.unusedFlag = iBridUtil.GetStringValue(oPartsArgs.Query["unused"]);
		oCondition.rankType = iBridUtil.GetStringValue(oPartsArgs.Query["ranktype"]);
		oCondition.bookmarkFlag = iBridUtil.GetStringValue(oPartsArgs.Query["bkm"]);

		using (CastBbsObj oCastBbsObj = new CastBbsObj(parseMan.sessionObjs)) {
			if (!sObjSeq.Equals(string.Empty)) {
				oTmpDataSet = oCastBbsObj.GetPageCollectionBySeq(sObjSeq,bAllBbs,true);
			} else {
				oTmpDataSet = oCastBbsObj.GetPageCollection(
					this.parseMan.sessionMan.site.siteCd,
					sLoginid,
					sCharNo,
					ViCommConst.FLAG_OFF_STR,
					false,
					"",
					sAttrTypeSeq,
					sAttrSeq,
					bAllBbs,
					bOnlyUsed,
					oCondition,
					1,
					oPartsArgs.NeedCount,
					true
				);
			}
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_PARTNER_BBS_OBJ);

		return sValue;
	}

	private string GetSelfFavoriteCount(string pTag,string pArgument) {
		return GetSelfFavoriteCount(
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo
		);
	}

	public string getGameCharacterType(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string user_seq;
		string user_char_no;

		if (arrText.Length > 1) {
			user_seq = arrText[0];
			user_char_no = arrText[1];

			GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
			oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
			oCondition.PartnerUserSeq = user_seq;
			oCondition.PartnerUserCharNo = user_char_no;
			oCondition.SeekMode = ulong.Parse(this.parseMan.sessionObjs.seekMode);

			using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
				DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);

				if (ds.Tables[0].Rows.Count > 0) {
					sValue = ds.Tables[0].Rows[0]["GAME_CHARACTER_TYPE"].ToString();
				}
			}
		}

		return sValue;
	}

	private string GetObjReviewTotal(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oTmpDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);

		if (string.IsNullOrEmpty(sObjSeq)) {
			return string.Empty;
		}

		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			oTmpDataSet = oObjReviewHistory.GetObjReviewTotalForParts(
				this.parseMan.sessionMan.site.siteCd,
				sObjSeq
			);
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oTmpDataSet,PwViCommConst.DATASET_OBJ_REVIEW);

		return sValue;
	}

	private string GetSelfRequestedSupportCount(string pTag,string pArgument) {
		string sValue = GetSelfRequestedSupportCount(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo
		);
		return sValue;
	}

	private string GetSelfApplicationCount(string pTag,string pArgument) {
		string sValue = GetSelfApplicationCount(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo
		);
		return sValue;
	}

	private string GetListRequest(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		PwRequestSeekCondition oCondition = new PwRequestSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.SexCd = ViCommConst.MAN;
		oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.PublicStatus = PwViCommConst.RequestPublicStatus.PUBLIC;
		DataSet oDataSet;
		using (PwRequest oPwRequest = new PwRequest()) {
			oDataSet = oPwRequest.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}
		if (oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_REQUEST);
	}

	protected string GetSelfGameFellowCountEx(string pTag,string pArgument) {
		string sValue = string.Empty;
		sValue = GetSelfGameFellowCountEx(
			pTag,
			this.parseMan.sessionMan.site.siteCd,
			this.parseMan.sessionMan.userMan.userSeq,
			this.parseMan.sessionMan.userMan.userCharNo,
			pArgument
		);
		return sValue;
	}

	private string GetSelfFavoritGroupCount() {
		decimal dRecCount = 0;

		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.UserSeq = this.parseMan.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oFavoritGroup.GetPageCount(oCondition,1,out dRecCount);
		}

		return dRecCount.ToString();
	}

	private string GetFavoritGroupNm(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		string[] oArgArr = pArgument.Split(',');

		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseMan,oArgArr,0);
		string sPartnerUserSeq = iBridUtil.GetStringValue(oQuery["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(oQuery["partner_user_char_no"]);
		string sFavoritGroupSeq = iBridUtil.GetStringValue(oQuery["grpseq"]);

		if (!string.IsNullOrEmpty(sPartnerUserSeq) && !string.IsNullOrEmpty(sPartnerUserCharNo)) {
			using (Favorit oFavorit = new Favorit()) {
				oDataSet = oFavorit.GetOne(
					this.parseMan.sessionMan.site.siteCd,
					this.parseMan.sessionMan.userMan.userSeq,
					this.parseMan.sessionMan.userMan.userCharNo,
					sPartnerUserSeq,
					sPartnerUserCharNo
				);
			}

			if (oDataSet.Tables[0].Rows.Count != 0) {
				sValue = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["FAVORIT_GROUP_NM"]);

				if (string.IsNullOrEmpty(sValue)) {
					sValue = "ｸﾞﾙｰﾌﾟ未設定";
				}
			}
		} else if (!string.IsNullOrEmpty(sFavoritGroupSeq)) {
			FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
			oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
			oCondition.FavoritGroupSeq = sFavoritGroupSeq;

			using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
				oDataSet = oFavoritGroup.GetPageCollection(oCondition,1,1);

				if (oDataSet.Tables[0].Rows.Count != 0) {
					sValue = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["FAVORIT_GROUP_NM"]);
				}
			}
		}

		return sValue;
	}

	public string getCastGameCharacterData(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string sUserSeq;
		string sUserCharNo;
		string sColumn;

		if (arrText.Length > 2) {
			sUserSeq = arrText[0];
			sUserCharNo = arrText[1];
			sColumn = arrText[2];

			CastGameCharacterSeekCondition oCondition = new CastGameCharacterSeekCondition();
			oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
			oCondition.UserSeq = sUserSeq;
			oCondition.UserCharNo = sUserCharNo;

			using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
				DataSet ds = oCastGameCharacter.GetOneByUserSeq(oCondition);

				if(ds.Tables[0].Rows.Count > 0) {
					DataRow dr = ds.Tables[0].Rows[0];
					sValue = dr[sColumn].ToString();
				}
			}
		}

		return sValue;
	}

	private string GetManTreasureScript(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseMan,oArgArr,0);
		string sGetTreasureLogSeq = iBridUtil.GetStringValue(oQuery["get_treasure_log_seq"]);

		if (string.IsNullOrEmpty(sGetTreasureLogSeq)) {
			return sValue;
		}

		string sWebPhisicalDir = parseMan.sessionMan.site.webPhisicalDir;
		string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.GAME_FLASH_DIR);
		string sScriptPath = Path.Combine(sFlashDir,"get_man_treasure_ip.js");
		string sPhotoImgPath = string.Empty;
		string sObjPhotoImgPath = string.Empty;

		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			DataSet oDataSet = oGetManTreasureLog.GetOne(
				parseMan.sessionMan.site.siteCd,
				parseMan.sessionMan.userMan.userSeq,
				parseMan.sessionMan.userMan.userCharNo,
				sGetTreasureLogSeq
			);

			if (oDataSet.Tables[0].Rows.Count != 0) {
				sPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"]));
				sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]));
			}
		}

		if (string.IsNullOrEmpty(sPhotoImgPath) || string.IsNullOrEmpty(sObjPhotoImgPath)) {
			return sValue;
		}

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			using (StreamReader srJs = new StreamReader(sScriptPath)) {
				sValue = srJs.ReadToEnd();
			}

			//プロフ画像読み込み
			byte[] bufPhotoImg = FileHelper.GetFileBuffer(sPhotoImgPath);
			string sPhotoImgBase64 = Convert.ToBase64String(bufPhotoImg);

			//お宝画像読み込み
			byte[] bufObjPhotoImg = FileHelper.GetFileBuffer(sObjPhotoImgPath);
			string sObjPhotoImgBase64 = Convert.ToBase64String(bufObjPhotoImg);

			sValue = sValue.Replace("%IMAGE1%",sPhotoImgBase64);
			sValue = sValue.Replace("%IMAGE2%",sObjPhotoImgBase64);
		}

		return sValue;
	}

	private string GetManTreasureFlashVars(string pTag,string pArgument) {
		string sValue = string.Empty;
		NameValueCollection oQuery = new NameValueCollection(parseMan.sessionMan.requestQuery.QueryString);
		string sGetTreasureLogSeq = iBridUtil.GetStringValue(oQuery["get_treasure_log_seq"]);
		string sTreasureCompleteFlag = iBridUtil.GetStringValue(oQuery["treasure_complete_flag"]);
		string sLevelUpFlag = iBridUtil.GetStringValue(oQuery["level_up_flag"]);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		string sImgUrl1 = string.Empty;
		string sImgUrl2 = string.Empty;
		string sNextUrl = string.Empty;

		if (!string.IsNullOrEmpty(sGetTreasureLogSeq)) {
			string sPhotoImgPath = string.Empty;
			string sObjPhotoImgPath = string.Empty;

			using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
				DataSet oDataSet = oGetManTreasureLog.GetOne(
					parseMan.sessionMan.site.siteCd,
					parseMan.sessionMan.userMan.userSeq,
					parseMan.sessionMan.userMan.userCharNo,
					sGetTreasureLogSeq
				);

				if (oDataSet.Tables[0].Rows.Count != 0) {
					sImgUrl1 = System.Web.HttpUtility.UrlEncode(Path.Combine("../",iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"])),enc);
					sImgUrl2 = System.Web.HttpUtility.UrlEncode(Path.Combine("../",iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"])),enc);
				}
			}
		}

		if (sTreasureCompleteFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			UrlBuilder oUrlBuilder = new UrlBuilder("GameDisplayDoc.aspx");
			oUrlBuilder.AddParameter("doc","8053");
			foreach (string sKey in oQuery.AllKeys) {
				oUrlBuilder.AddParameter(sKey,iBridUtil.GetStringValue(oQuery[sKey]));
			}
			sNextUrl = System.Web.HttpUtility.UrlEncode(oUrlBuilder.ToString(),enc);
		} else {
			if (sLevelUpFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				UrlBuilder oUrlBuilder = new UrlBuilder("GameDisplayDoc.aspx");
				oUrlBuilder.AddParameter("doc","8054");
				foreach (string sKey in oQuery.AllKeys) {
					oUrlBuilder.AddParameter(sKey,iBridUtil.GetStringValue(oQuery[sKey]));
				}
				sNextUrl = System.Web.HttpUtility.UrlEncode(oUrlBuilder.ToString(),enc);
			} else {
				UrlBuilder oUrlBuilder = new UrlBuilder("ViewGameTownClearMissionOK.aspx");
				foreach (string sKey in oQuery.AllKeys) {
					oUrlBuilder.AddParameter(sKey,iBridUtil.GetStringValue(oQuery[sKey]));
				}
				sNextUrl = System.Web.HttpUtility.UrlEncode(oUrlBuilder.ToString(),enc);
			}
		}

		sValue = string.Format("imgurl1={0}&imgurl2={1}&nextUrl={2}",sImgUrl1,sImgUrl2,sNextUrl);

		return sValue;
	}
	
	private string GetGameInformationCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		string sLastHugLogCheckDate = this.parseMan.sessionMan.userMan.gameCharacter.lastHugLogCheckDate;
		
		using (GameInformation oGameInformation = new GameInformation()) {
			int iValue = oGameInformation.GetInformationCountForMan(sSiteCd,sUserSeq,sUserCharNo,sLastHugLogCheckDate);
			sValue = iValue.ToString();
		}
		
		return sValue;
	}
	
	private string GetGameLoginBonusGetFlag(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		
		using (GameLoginBonusDaily oGameLoginBonusDaily = new GameLoginBonusDaily()) {
			sValue = oGameLoginBonusDaily.GetGameLoginBonusGetFlag(sSiteCd,sUserSeq,sUserCharNo);
		}
		
		return sValue;
	}
	
	private string GetSelfCastPicGachaWinCount(string pTag,string pArgument) {
		string sValue = "0";

		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;

		using (CastPicGacha oCastPicGacha = new CastPicGacha()) {
			sValue = oCastPicGacha.GetCastPicGachaWinCount(sSiteCd,sUserSeq,sUserCharNo);
		}
		
		return sValue;
	}

	private string GetInfoMail(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sMailSearchKey = iBridUtil.GetStringValue(oPartsArgs.Query["search_key"]);
		string sMailReadFlag = iBridUtil.GetStringValue(oPartsArgs.Query["read_flag"]);
		int? iMailReadFlag = null;
		if (!sMailReadFlag.Equals("")) {
			iMailReadFlag = int.Parse(sMailReadFlag);
		}
		using (MailBox oMailBox = new MailBox()) {
			oDataSet = oMailBox.GetPageCollection(
				parseMan.sessionMan.site.siteCd,
				parseMan.sessionMan.userMan.userSeq,
				parseMan.sessionMan.userMan.userCharNo,
				"",
				"",
				parseMan.sessionMan.sexCd,
				ViCommConst.MAIL_BOX_INFO,
				ViCommConst.RX,
				true,
				sMailSearchKey,
				iMailReadFlag,
				null,
				false,
				null,
				null,
				null,
				1,
				oPartsArgs.NeedCount
			);
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
	}

	private string GetPhotoFlashVars(string pTag,string pArgument) {
		string sValue = string.Empty;
		NameValueCollection oQuery = new NameValueCollection(parseMan.sessionMan.requestQuery.QueryString);
		string sPicSeq = iBridUtil.GetStringValue(oQuery["pic_seq"]);
		string sNextUrl = iBridUtil.GetStringValue(oQuery["next_url"]);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		string sImgUrl = sImgUrl = System.Web.HttpUtility.UrlEncode(Path.Combine("../",PwViCommConst.NoPicJpgPath),enc);
		string sComment = string.Empty;

		if (!string.IsNullOrEmpty(sPicSeq)) {
			string sObjPhotoImgPath = string.Empty;

			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				if (oObjUsedHistory.GetOne(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,ViCommConst.ATTACH_PIC_INDEX.ToString(),sPicSeq)) {
					using (CastPic oCastPic = new CastPic(parseMan.sessionObjs)) {
						DataSet oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);

						if (oDataSet.Tables[0].Rows.Count > 0) {
							sImgUrl = System.Web.HttpUtility.UrlEncode(Path.Combine("../",iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"])),enc);
							sComment = System.Web.HttpUtility.UrlEncode(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]));
						}
					}
				}
			}
		}

		sNextUrl = System.Web.HttpUtility.UrlEncode(sNextUrl,enc);

		sValue = string.Format("IMGURL={0}&COMMENT={1}&nextURL={2}",sImgUrl,sComment,sNextUrl);

		return sValue;
	}

	private string GetPhotoScript(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseMan,oArgArr,0);
		string sPicSeq = iBridUtil.GetStringValue(oQuery["pic_seq"]);

		if (string.IsNullOrEmpty(sPicSeq)) {
			return sValue;
		}

		string sWebPhisicalDir = parseMan.sessionMan.site.webPhisicalDir;
		string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.FLASH_DIR);
		string sScriptPath = Path.Combine(sFlashDir,"get_photo_ip.js");
		string sObjPhotoImgPath = string.Empty;
		string sObjPicDoc = string.Empty;

		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			if (!oObjUsedHistory.GetOne(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,ViCommConst.ATTACH_PIC_INDEX.ToString(),sPicSeq)) {
				return sValue;
			}
		}

		using (CastPic oCastPic = new CastPic(parseMan.sessionObjs)) {
			DataSet oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);

			if (oDataSet.Tables[0].Rows.Count > 0) {
				sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]));
				sObjPicDoc = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]);
			}
		}

		if (string.IsNullOrEmpty(sObjPhotoImgPath)) {
			return sValue;
		}

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			using (StreamReader srJs = new StreamReader(sScriptPath)) {
				sValue = srJs.ReadToEnd();
			}

			//お宝画像読み込み
			byte[] bufObjPhotoImg = FileHelper.GetFileBuffer(sObjPhotoImgPath);
			string sObjPhotoImgBase64 = Convert.ToBase64String(bufObjPhotoImg);

			sValue = sValue.Replace("%IMAGE%",sObjPhotoImgBase64);
			sValue = sValue.Replace("%COMMENT%",sObjPicDoc);
		}

		return sValue;
	}
	
	private string GetCastDataByUserSeq(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sUserSeq = iBridUtil.GetStringValue(oPartsArgs.Query["userseq"]);
		string sUserCharNo = iBridUtil.GetStringValue(oPartsArgs.Query["usercharno"]);
		if (string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sUserCharNo)) {
			return string.Empty;
		}
		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			oDataSet = oCast.GetOne(parseMan.sessionMan.site.siteCd,sUserSeq,sUserCharNo,1);
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetFanClubStatus(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		FanClubStatusSeekCondition oCondition = new FanClubStatusSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
		oCondition.ManUserSeq = parseMan.sessionMan.userMan.userSeq;

		using (CastFanClubStatus oCastFanClubStatus = new CastFanClubStatus(parseMan.sessionObjs)) {
			oDataSet = oCastFanClubStatus.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}
	
	private string GetCastNews(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		CastNewsSeekCondition oCondition = new CastNewsSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		using (CastNews oCastNews = new CastNews(parseMan.sessionObjs)) {
			oDataSet = oCastNews.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetCastFavoritNews(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		int iRecPerPage = oPartsArgs.NeedCount + 20;
		CastFavoritNewsSeekCondition oCondition = new CastFavoritNewsSeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.SelfUserSeq = this.parseMan.sessionMan.userMan.userSeq;

		using (CastFavoritNews oCastFavoritNews = new CastFavoritNews()) {
			DataSet dsTemp = oCastFavoritNews.GetPageCollection(oCondition,1,iRecPerPage);
			oDataSet = oCastFavoritNews.RemoveNonPublishData(dsTemp,oPartsArgs.NeedCount);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetSelfBestFanClubMemberRank() {
		string sValue = "0";

		if (DateTime.Parse(parseMan.sessionMan.userMan.characterEx.spPremiumEndDate) > DateTime.Now) {
			return "5";
		} else {
			using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
				sValue = oFanClubStatus.GetBestMemberRank(
					parseMan.sessionMan.site.siteCd,
					parseMan.sessionMan.userMan.userSeq
				);
			}
		}
		return sValue;
	}

	private string GetCastNewFace(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.conditionFlag = ViCommConst.INQUIRY_CAST_NEW_FACE;
		oCondition.siteCd = parseMan.sessionMan.site.siteCd;
		oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.newCastDay = parseMan.sessionMan.site.newFaceDays;
		oCondition.orderAsc = iBridUtil.GetStringValue(oPartsArgs.Query["orderasc"]);
		oCondition.orderDesc = iBridUtil.GetStringValue(oPartsArgs.Query["orderdesc"]);

		if (iBridUtil.GetStringValue(oPartsArgs.Query["random"]).Equals(ViCommConst.FLAG_ON_STR)) {
			oCondition.directRandom = ViCommConst.FLAG_ON;
		}

		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			oDataSet = oCast.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetPhotoScriptSp(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sPicSeq = iBridUtil.GetStringValue(parseMan.sessionMan.requestQuery.QueryString["pic_seq"]);

		if (string.IsNullOrEmpty(sPicSeq)) {
			return sValue;
		}

		string sWebPhisicalDir = parseMan.sessionMan.site.webPhisicalDir;
		string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.FLASH_DIR);
		string sScriptPath = Path.Combine(sFlashDir,pArgument);
		string sObjPhotoImgPath = string.Empty;
		string sObjPicDoc = string.Empty;

		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			if (!oObjUsedHistory.GetOne(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,ViCommConst.ATTACH_PIC_INDEX.ToString(),sPicSeq)) {
				return sValue;
			}
		}

		using (CastPic oCastPic = new CastPic(parseMan.sessionObjs)) {
			DataSet oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);

			if (oDataSet.Tables[0].Rows.Count > 0) {
				sObjPhotoImgPath = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]);
				sObjPicDoc = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]);
			}
		}

		if (string.IsNullOrEmpty(sObjPhotoImgPath)) {
			return sValue;
		}

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			using (StreamReader srJs = new StreamReader(sScriptPath)) {
				sValue = srJs.ReadToEnd();
			}

			sValue = sValue.Replace("%IMAGE%",sObjPhotoImgPath);
			sValue = sValue.Replace("%COMMENT%",sObjPicDoc);
			sValue = sValue.Replace("%NEXT_URL%",iBridUtil.GetStringValue(parseMan.sessionMan.requestQuery.QueryString["next_url"]));
		}

		return sValue;
	}

	private string GetPickupObjsVoteCountData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		string sPickupId = iBridUtil.GetStringValue(oPartsArgs.Query["pickupid"]);
		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);
		if (string.IsNullOrEmpty(sPickupId) || string.IsNullOrEmpty(sObjSeq)) {
			return string.Empty;
		}
		using (PickupObjsVoteCount oPickupObjsVoteCount = new PickupObjsVoteCount()) {
			oDataSet = oPickupObjsVoteCount.GetOne(sSiteCd,sUserSeq,sUserCharNo,sPickupId,sObjSeq);
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_PARTNER_PIC);
	}

	private string GetPickupObjsCommentCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		decimal dCommentCount;
		
		string[] sArr = pArgument.Split(',');
		
		if (sArr.Length < 2) {
			return string.Empty;
		}
		
		PickupObjsCommentSeekCondition oCondition = new PickupObjsCommentSeekCondition();
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.PickupId = sArr[0];
		oCondition.ObjSeq = sArr[1];

		if (this.parseMan.sessionMan.sexCd.Equals(ViCommConst.MAN) && this.parseMan.sessionMan.IsImpersonated == true) {
			oCondition.SelfUserSeq = this.parseMan.sessionMan.originalWomanObj.userWoman.userSeq;
			oCondition.SelfUserCharNo = this.parseMan.sessionMan.originalWomanObj.userWoman.curCharNo;
		}

		using (PickupObjsComment oPickupObjsComment = new PickupObjsComment()) {
			oPickupObjsComment.GetPageCount(oCondition,1,out dCommentCount);
		}
		
		return dCommentCount.ToString();
	}

	private string MakeWantedCompleteSheetIndex(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet ds = new DataSet();
		ds.Tables.Add();
		DataColumn col;
		col = new DataColumn("STAMP_SHEET_NO",System.Type.GetType("System.String"));
		ds.Tables[0].Columns.Add(col);

		col = new DataColumn("STAMP_SHEET_AVAILABLE_FLAG",System.Type.GetType("System.String"));
		ds.Tables[0].Columns.Add(col);
		
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		string sReportMonth = iBridUtil.GetStringValue(oPartsArgs.Query["reportmonth"]);

		using (WantedCompSheetCnt oWantedCompSheetCnt = new WantedCompSheetCnt()) {
			int iCompleteCount;
			iCompleteCount = oWantedCompSheetCnt.GetCompleteSheetCount(sSiteCd,sUserSeq,sUserCharNo,sReportMonth);
			
			int iAvailableSheetNo = 0;
			int iSheetNo = 0;
			if (iCompleteCount == 5) {
				iAvailableSheetNo = iCompleteCount;
			} else {
				iAvailableSheetNo = iCompleteCount + 1;
			}
			
			for (int i = 0;i < 5;i++) {
				iSheetNo = i + 1;
				ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
				ds.Tables[0].Rows[i]["STAMP_SHEET_NO"] = iSheetNo.ToString();

				ds.Tables[0].Rows[i]["STAMP_SHEET_AVAILABLE_FLAG"] = ViCommConst.FLAG_OFF_STR;
				
				if (i + 1 <= iAvailableSheetNo) {
					ds.Tables[0].Rows[i]["STAMP_SHEET_AVAILABLE_FLAG"] = ViCommConst.FLAG_ON_STR;
				}
			}
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,ds,ViCommConst.DATASET_CAST);
	}

	private string BladeScriptSp(string pTag,string pArgument) {
		string sValue = string.Empty;

		string[] sArr = pArgument.Split(',');
		
		string sWebPhisicalDir = parseMan.sessionMan.site.webPhisicalDir;
		string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.FLASH_DIR);
		string sScriptPath = Path.Combine(sFlashDir,sArr[0]);

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			using (StreamReader srJs = new StreamReader(sScriptPath)) {
				sValue = srJs.ReadToEnd();
			}
			
			if (sArr.Length > 1) {
				sValue = sValue.Replace("%NEXT_URL%",sArr[1]);
			}
		}

		return sValue;
	}
	
	private string GetWantedMonthlyPointData(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sUserSeq = this.parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = this.parseMan.sessionMan.userMan.userCharNo;
		string sReportMonth = DateTime.Now.AddMonths(-1).ToString("yyyyMM");

		using (WantedCompSheetCnt oWantedCompSheetCnt = new WantedCompSheetCnt()) {
			oDataSet = oWantedCompSheetCnt.GetWantedMonthlyPoint(sSiteCd,sUserSeq,sUserCharNo,sReportMonth);
		}
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_WANTED_BOUNTY);
	}
	
	private string CheckWantedMailRxTimeShort(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = parseMan.sessionMan.userMan.userCharNo;
		using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
			oDataSet = oUserManCharacter.GetOneByUserSeq(sSiteCd,sUserSeq,sUserCharNo);
			
			int iTotalTime = 0;
			int iStartTime = 0;
			int iEndTime = 0;
			string[] sStartTimeArr;
			
			string[] sEndTimeArr;

			string sUseFlag = oDataSet.Tables[0].Rows[0]["MOBILE_MAIL_RX_TIME_USE_FLAG"].ToString();
			string sStartTime = oDataSet.Tables[0].Rows[0]["MOBILE_MAIL_RX_START_TIME"].ToString();
			string sEndTime = oDataSet.Tables[0].Rows[0]["MOBILE_MAIL_RX_END_TIME"].ToString();
			string sUseFlag2 = oDataSet.Tables[0].Rows[0]["MOBILE_MAIL_RX_TIME_USE_FLAG2"].ToString();
			string sStartTime2 = oDataSet.Tables[0].Rows[0]["MOBILE_MAIL_RX_START_TIME2"].ToString();
			string sEndTime2 = oDataSet.Tables[0].Rows[0]["MOBILE_MAIL_RX_END_TIME2"].ToString();
			
			if (sUseFlag.Equals(ViCommConst.FLAG_OFF_STR) && sUseFlag2.Equals(ViCommConst.FLAG_OFF_STR)) {
				return ViCommConst.FLAG_OFF_STR;
			}
			
			if (sUseFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sStartTimeArr = sStartTime.Split(':');
				iStartTime = int.Parse(sStartTimeArr[0]) * 60;
				iStartTime = iStartTime + (int.Parse(sStartTimeArr[1]));

				sEndTimeArr = sEndTime.Split(':');
				iEndTime = int.Parse(sEndTimeArr[0]) * 60;
				iEndTime = iEndTime + (int.Parse(sEndTimeArr[1]));
				
				if (iStartTime >= iEndTime) {
					iEndTime = iEndTime + 1440;
				}
				
				iTotalTime = iEndTime - iStartTime;
			}

			if (sUseFlag2.Equals(ViCommConst.FLAG_ON_STR)) {
				sStartTimeArr = sStartTime2.Split(':');
				iStartTime = int.Parse(sStartTimeArr[0]) * 60;
				iStartTime = iStartTime + (int.Parse(sStartTimeArr[1]));

				sEndTimeArr = sEndTime2.Split(':');
				iEndTime = int.Parse(sEndTimeArr[0]) * 60;
				iEndTime = iEndTime + (int.Parse(sEndTimeArr[1]));

				if (iStartTime >= iEndTime) {
					iEndTime = iEndTime + 1440;
				}

				iTotalTime = iTotalTime + iEndTime - iStartTime;
			}
			
			if (iTotalTime >= 180) {
				sValue = ViCommConst.FLAG_OFF_STR;
			} else {
				sValue = ViCommConst.FLAG_ON_STR;
			}
		}
		
		return sValue;
	}
	
	private string CheckNotAcquiredPointExists(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = parseMan.sessionMan.userMan.userCharNo;
		
		using (WantedCompSheetCnt oWantedCompSheetCnt = new WantedCompSheetCnt()) {
			sValue = oWantedCompSheetCnt.CheckNotAcquiredPointExists(sSiteCd,sUserSeq,sUserCharNo);
		}
		
		return sValue;
	}

	private string GetFanclubFeePoint() {
		string sFanclubFeePoint = string.Empty;

		using (Site oSite = new Site()) {
			oSite.GetValue(this.parseMan.sessionMan.site.siteCd,"FANCLUB_FEE_POINT",ref sFanclubFeePoint);
		}

		return sFanclubFeePoint;
	}

	private int GetCastBbsPicCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseMan,oArgArr,0);

		SeekCondition oCondition = new SeekCondition();
		oCondition.unusedFlag = iBridUtil.GetStringValue(oQuery["unused"]);

		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sLoginId = iBridUtil.GetStringValue(oQuery["loginid"]);
		string sUserCharNo = ViCommConst.MAIN_CHAR_NO;

		using (CastPic oCastPic = new CastPic(parseMan.sessionObjs)) {
			oCastPic.GetPageCount(
				sSiteCd,
				sLoginId,
				sUserCharNo,
				false,
				ViCommConst.ATTACHED_BBS.ToString(),
				"",
				string.Empty,
				string.Empty,
				false,
				oCondition,
				1,
				out dRecCount
			);
		}

		return (int)dRecCount;
	}

	private int GetCastBbsMovieCount(string pTag,string pArgument) {
		decimal dRecCount = 0;
		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseMan,oArgArr,0);

		SeekCondition oCondition = new SeekCondition();
		oCondition.unusedFlag = iBridUtil.GetStringValue(oQuery["unused"]);

		string sSiteCd = this.parseMan.sessionMan.site.siteCd;
		string sLoginId = iBridUtil.GetStringValue(oQuery["loginid"]);
		string sUserCharNo = ViCommConst.MAIN_CHAR_NO;

		using (CastMovie oCastMovie = new CastMovie(parseMan.sessionObjs)) {
			oCastMovie.GetPageCount(
				sSiteCd,
				sLoginId,
				sUserCharNo,
				"",
				false,
				ViCommConst.ATTACHED_BBS.ToString(),
				"",
				string.Empty,
				string.Empty,
				false,
				oCondition,
				1,
				true,
				out dRecCount
			);
		}

		return (int)dRecCount;
	}

	private int GetSelfVoteTicketCount(string pTag,string pArgument) {
		int iTicketCount = 0;

		using (VoteTerm oVoteTerm = new VoteTerm()) {
			oVoteTerm.GetVoteTicketCount(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				out iTicketCount
			);
		}

		return iTicketCount;
	}

	private bool GetBbsBingoNoEnable(string pTag,string pArgument) {
		string sObjSeq = this.parseMan.parseContainer.Parse(pArgument);

		if (string.IsNullOrEmpty(sObjSeq)) {
			return false;
		}

		int iCompleteFlag;
		string sResult = string.Empty;

		using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
			oBbsBingoTerm.GetBbsBingoNo(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				sObjSeq,
				ViCommConst.FLAG_ON,
				out iCompleteFlag,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_OK)) {
			return true;
		} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_NOT_VIEW)) {
			return true;
		} else {
			return false;
		}
	}

	private string GetBbsBingoLog(string pTag,string pArgument) {
		string sValue = string.Empty;

		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sObjSeq = iBridUtil.GetStringValue(oPartsArgs.Query["objseq"]);

		if (string.IsNullOrEmpty(sObjSeq)) {
			return sValue;
		}

		DataSet oDataSet = null;

		using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
			oDataSet = oBbsBingoTerm.GetBbsBingoLogData(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				sObjSeq
			);
		}

		sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_BBS_BINGO);

		return sValue;
	}

	private string GetBbsBingoEntryPrize(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet = null;

		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		BbsBingoEntrySeekCondition oCondition = new BbsBingoEntrySeekCondition(oPartsArgs.Query);
		oCondition.SiteCd = this.parseMan.sessionMan.site.siteCd;
		oCondition.PrizeFlag = ViCommConst.FLAG_ON_STR;

		if (string.IsNullOrEmpty(oCondition.TermSeq)) {
			using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
				oCondition.TermSeq = oBbsBingoTerm.GetLatestTermSeq(this.parseMan.sessionMan.site.siteCd);
			}
		}

		using (BbsBingoEntry oBbsBingoEntry = new BbsBingoEntry()) {
			oDataSet = oBbsBingoEntry.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAN);
		}

		return sValue;
	}

	private string GetBbsBingoCurrentTerm(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
			oDataSet = oBbsBingoTerm.GetCurrent(this.parseMan.sessionMan.site.siteCd);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_BBS_BINGO);
		}

		return sValue;
	}

	private string GetBbsBingoLatestTerm(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
			oDataSet = oBbsBingoTerm.GetLatest(this.parseMan.sessionMan.site.siteCd);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_BBS_BINGO);
		}

		return sValue;
	}

	private int GetSelfElectionVoteTicketCount(string pTag,string pArgument) {
		int iTicketCount = 0;

		using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
			oElectionPeriod.GetElectionVoteTicketCount(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				out iTicketCount
			);
		}

		return iTicketCount;
	}

	private string GetOneRankElection(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		DataSet dsElectionPeriod;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
			dsElectionPeriod = oElectionPeriod.GetCurrent(this.parseMan.sessionMan.site.siteCd);
		}

		if (dsElectionPeriod.Tables[0].Rows.Count > 0) {
			ElectionEntrySeekCondition oCondition = new ElectionEntrySeekCondition(oPartsArgs.Query);
			using (ElectionEntry oElectionEntry = new ElectionEntry(parseMan.sessionObjs)) {
				oCondition.SiteCd = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["SITE_CD"]);
				oCondition.ElectionPeriodSeq = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["ELECTION_PERIOD_SEQ"]);
				oCondition.FixRankFlag = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["FIX_RANK_FLAG"]);
				oCondition.ElectionPeriodStatus = string.Empty;

				DateTime oFirstPeriodEndDate = DateTime.Parse(dsElectionPeriod.Tables[0].Rows[0]["FIRST_PERIOD_END_DATE"].ToString());
				if (oFirstPeriodEndDate >= DateTime.Now) {
					oCondition.ElectionPeriodStatus = PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD;
				}

				oDataSet = oElectionEntry.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
			}
		} else {
			oDataSet = new DataSet();
			DataTable dt = new DataTable();
			oDataSet.Tables.Add(dt);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_ELECTION_ENTRY);
		}

		return sValue;
	}

	private bool GetCastMovieCommentEnable(string pTag,string pArgument) {
		if (parseMan.sessionMan.logined && !parseMan.sessionMan.IsImpersonated) {
			bool bCommented = false;
			string sMovieSeq = this.parseMan.parseContainer.Parse(pArgument);

			if (!string.IsNullOrEmpty(sMovieSeq)) {
				using (CastMovieComment oCastMovieComment = new CastMovieComment()) {
					bCommented = oCastMovieComment.IsExist(
						parseMan.sessionMan.site.siteCd,
						sMovieSeq,
						parseMan.sessionMan.userMan.userSeq
					);
				}
			}

			if (bCommented) {
				return false;
			}
		} else if (parseMan.sessionMan.IsImpersonated) {
			return false;
		}

		return true;
	}

	private bool GetCastPicCommentEnable(string pTag,string pArgument) {
		if (parseMan.sessionMan.logined && !parseMan.sessionMan.IsImpersonated) {
			bool bCommented = false;
			string sPicSeq = this.parseMan.parseContainer.Parse(pArgument);

			if (!string.IsNullOrEmpty(sPicSeq)) {
				using (CastPicComment oCastPicComment = new CastPicComment()) {
					bCommented = oCastPicComment.IsExist(
						parseMan.sessionMan.site.siteCd,
						sPicSeq,
						parseMan.sessionMan.userMan.userSeq
					);
				}
			}

			if (bCommented) {
				return false;
			}
		} else if (parseMan.sessionMan.IsImpersonated) {
			return false;
		}

		return true;
	}

	private string GetUserMail(string pTag,string pArgument) {
		DataSet oTempDataSet = null;
		DataSet oDataSet = new DataSet();
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sPartnerLoginId = iBridUtil.GetStringValue(oPartsArgs.Query["loginid"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(oPartsArgs.Query["partner_user_char_no"]);
		string sTxRxType = iBridUtil.GetStringValue(oPartsArgs.Query["tx_rx_type"]);
		string sMailSeq = iBridUtil.GetStringValue(oPartsArgs.Query["mail_seq"]);
		string sMailSearchKey = iBridUtil.GetStringValue(oPartsArgs.Query["search_key"]);
		string sSortReverseFlag = iBridUtil.GetStringValue(oPartsArgs.Query["sort_reverse"]);
		string sUpdateReadFlag = iBridUtil.GetStringValue(oPartsArgs.Query["update_read"]);
		
		using (MailBox oMailBox = new MailBox()) {
			oTempDataSet = oMailBox.GetPageCollectionBase(
				parseMan.sessionMan.site.siteCd,
				parseMan.sessionMan.userMan.userSeq,
				parseMan.sessionMan.userMan.userCharNo,
				sPartnerLoginId,
				sPartnerUserCharNo,
				parseMan.sessionMan.sexCd,
				ViCommConst.MAIL_BOX_USER,
				sTxRxType,
				true,
				sMailSeq,
				sMailSearchKey,
				null,
				null,
				false,
				string.Empty,
				string.Empty,
				string.Empty,
				1,
				oPartsArgs.NeedCount
			);
		}
		
		if (!parseMan.sessionMan.adminFlg) {
			if (sUpdateReadFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				List<string> oMailSeqList = new List<string>();

				foreach (DataRow dr in oTempDataSet.Tables[0].Rows) {
					if (iBridUtil.GetStringValue(dr["TXRX_TYPE"]).Equals(ViCommConst.RX) && iBridUtil.GetStringValue(dr["READ_FLAG"]).Equals(ViCommConst.FLAG_OFF_STR)) {
						oMailSeqList.Add(iBridUtil.GetStringValue(dr["MAIL_SEQ"]));
					}
				}

				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailReadFlagBatch(oMailSeqList.ToArray());
				}
			}
		}
		
		if (sSortReverseFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			List<DataRow> oDataRowList = new List<DataRow>();
			foreach (DataRow oDR in oTempDataSet.Tables[0].Rows) {
				oDataRowList.Add(oDR);
			}
			DataRow[] oDataRowArray = oDataRowList.ToArray();
			
			Array.Reverse(oDataRowArray);
			DataTable oDataTable = oTempDataSet.Tables[0].Clone();
			DataRow oDataRowCopy;
			
			foreach (DataRow oDR in oDataRowArray) {
				oDataRowCopy = oDataTable.NewRow();
				oDataRowCopy.ItemArray = oDR.ItemArray;
				oDataTable.Rows.Add(oDataRowCopy);
			}
			
			oDataSet.Tables.Add(oDataTable);
		} else {
			oDataSet = oTempDataSet;
		}
		
		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_MAIL);
	}

	private string GetTotalTxRxMailCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iTotalTxMailCount = 0;
		int iTotalRxMailCount = 0;
		DataSet oDataSet = null;

		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseMan,oArgArr,0);

		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sUserCharNo = parseMan.sessionMan.userMan.userCharNo;
		string sPartnerUserSeq = iBridUtil.GetStringValue(oQuery["partner_user_seq"]);
		string sPartnerCharNo = iBridUtil.GetStringValue(oQuery["partner_char_no"]);

		if (string.IsNullOrEmpty(sPartnerUserSeq) || string.IsNullOrEmpty(sPartnerCharNo)) {
			return sValue;
		}
		
		using (Communication oCommunication = new Communication()) {
			oDataSet = oCommunication.GetOneByUserSeq(sSiteCd,sUserSeq,sUserCharNo,sPartnerUserSeq,sPartnerCharNo);
		}
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			int.TryParse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_TX_MAIL_COUNT"]),out iTotalTxMailCount);
			int.TryParse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_RX_MAIL_COUNT"]),out iTotalRxMailCount);
		}
		
		int iTotalTxRxMailCount = iTotalTxMailCount + iTotalRxMailCount;
		
		sValue = iBridUtil.GetStringValue(iTotalTxRxMailCount);
		
		return sValue;
	}

	private string GetOnlineCast(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.siteCd = parseMan.sessionMan.site.siteCd;
		oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.conditionFlag = ViCommConst.INQUIRY_ONLINE;
		oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;

		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			oDataSet = oCast.GetPageCollection(oCondition,1,oPartsArgs.NeedCount,false);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetSelfMarking(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.siteCd = parseMan.sessionMan.site.siteCd;
		oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.conditionFlag = PwViCommConst.INQUIRY_SELF_MARKING;
		oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;

		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			oDataSet = oCast.GetPageCollection(oCondition,1,oPartsArgs.NeedCount,false);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetSelfGameImgPath(string pTag,string pArgument) {
		string sValue = string.Empty;

		if (!parseMan.sessionMan.userMan.smallPhotoImgPath.Equals("image/A001/photo_men1.gif")) {
			sValue = "../" + parseMan.sessionMan.userMan.smallPhotoImgPath;
		} else {
			sValue = string.Format("../Image/{0}/game/man_type_a{1}.gif",parseMan.sessionMan.site.siteCd,getGameCharacter().gameCharacterType);
		}

		return sValue;
	}

	private bool GetSelfTelOverlap(string pTag,string pArgument) {
		bool bExist = false;

		using (TelOverlap oTelOverlap = new TelOverlap()) {
			bExist = oTelOverlap.IsExist(parseMan.sessionMan.userMan.userSeq);
		}

		return bExist;
	}

	private string GetCastWaitSchedule(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		WaitScheduleSeekCondition oCondition = new WaitScheduleSeekCondition();
		oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
		oCondition.SelfUserSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.SelfUserCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.UserSeq = iBridUtil.GetStringValue(oPartsArgs.Query["userseq"]);
		oCondition.UserCharNo = iBridUtil.GetStringValue(oPartsArgs.Query["usercharno"]);
		oCondition.ExWaitingFlag = iBridUtil.GetStringValue(oPartsArgs.Query["exwaitflag"]);

		string sMin;
		if (DateTime.Now.Minute < 30) {
			sMin = "00";
		} else {
			sMin = "30";
		}
		oCondition.MinWaitStartTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),sMin);

		using (WaitSchedule oWaitSchedule = new WaitSchedule(parseMan.sessionObjs)) {
			oDataSet = oWaitSchedule.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_WAIT_SCHEDULE);
	}
	
	private bool CheckCurrentPresentMail(string pTag,string pArgument) {
		bool bExists = false;
		using (PresentMailTerm oPresentMailTerm = new PresentMailTerm()) {
			bExists = oPresentMailTerm.CheckCurrentPresentMailTerm(parseMan.sessionMan.site.siteCd);
		}
		
		return bExists;
	}
	
	private bool CheckCanRegistMailRequestLog(string pTag,string pArgument) {
		string sCastUserSeq = string.Empty;
		string sCastCharNo = string.Empty;
		
		string[] sArr = pArgument.Split(',');
		
		if (sArr.Length < 2) {
			return false;
		}
		
		sCastUserSeq = sArr[0];
		sCastCharNo = sArr[1];
		
		int iRequestLimitCount;
		string sResult = string.Empty;
		using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
			oMailRequestLog.RegistMailRequestLog(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,sCastUserSeq,sCastCharNo,ViCommConst.FLAG_ON,out iRequestLimitCount,out sResult);
		}
		
		if (sResult.Equals(PwViCommConst.RegistMailRequestLogResult.RESULT_OK)) {
			return true;
		} else {
			return false;
		}
	}

	private string GetCastLoginPlan(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		LoginPlanSeekCondition oCondition = new LoginPlanSeekCondition();
		oCondition.SiteCd = parseMan.sessionMan.site.siteCd;
		oCondition.SelfUserSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.SelfUserCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.UserSeq = iBridUtil.GetStringValue(oPartsArgs.Query["userseq"]);
		oCondition.UserCharNo = iBridUtil.GetStringValue(oPartsArgs.Query["usercharno"]);

		string sMin;
		if (DateTime.Now.Minute < 30) {
			sMin = "00";
		} else {
			sMin = "30";
		}
		oCondition.MinLoginStartTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),sMin);

		using (LoginPlan oLoginPlan = new LoginPlan(parseMan.sessionObjs)) {
			oDataSet = oLoginPlan.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_LOGIN_PLAN);
	}
	
	private string GetRestMailRequestCount(string pTag,string pArgument) {
		string sValue = string.Empty;

		using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
			sValue = oMailRequestLog.GetRestMailRequestCount(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.userCharNo);
		}
		
		return sValue;
	}
	
	private string MailRequestConfirm(string pTag,string pArgument) {
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		
		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sCastUserSeq = iBridUtil.GetStringValue(oPartsArgs.Query["castuserseq"]);
		string sCastCharNo = iBridUtil.GetStringValue(oPartsArgs.Query["castcharno"]);
		string sCastLoginId = iBridUtil.GetStringValue(oPartsArgs.Query["castloginid"]);
		string sRestMailRequestCount = string.Empty;
		string sMailRequestLimitCount = string.Empty;
		string sResult = string.Empty;
		using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
			oMailRequestLog.MailRequestConfirm(sSiteCd,sUserSeq,sCastUserSeq,sCastCharNo,out sRestMailRequestCount,out sMailRequestLimitCount,out sResult);
		}

		DataSet oDataSet = new DataSet();
		DataTable oDataTable = new DataTable();
		oDataSet.Tables.Add(oDataTable);
		oDataTable.Columns.Add("LOGIN_ID",Type.GetType("System.String"));
		oDataTable.Columns.Add("USER_CHAR_NO",Type.GetType("System.String"));
		oDataTable.Columns.Add("REST_MAIL_REQUEST_COUNT",Type.GetType("System.String"));
		oDataTable.Columns.Add("MAIL_REQUEST_LIMIT_COUNT",Type.GetType("System.String"));
		oDataTable.Columns.Add("MAIL_REQUEST_CONFIRM_STATUS",Type.GetType("System.String"));
		DataRow oDataRow = oDataTable.NewRow();
		oDataRow["LOGIN_ID"] = sCastLoginId;
		oDataRow["USER_CHAR_NO"] = sCastCharNo;
		oDataRow["REST_MAIL_REQUEST_COUNT"] = sRestMailRequestCount;
		oDataRow["MAIL_REQUEST_LIMIT_COUNT"] = sMailRequestLimitCount;
		oDataRow["MAIL_REQUEST_CONFIRM_STATUS"] = sResult;
		oDataTable.Rows.Add(oDataRow);

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private bool GetMailLotteryOpening(string pTag,string pArgument) {
		bool bOK = false;

		using (MailLottery oMailLottery = new MailLottery()) {
			bOK = oMailLottery.GetMailLotteryOpening(this.parseMan.sessionMan.site.siteCd,this.parseMan.sessionMan.sexCd);
		}

		return bOK;
	}

	private string GetMailLotteryRate(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		string sMailLotteryRateSeq = iBridUtil.GetStringValue(oPartsArgs.Query["lottery"]);
		string sSecondLotteryFlag = iBridUtil.GetStringValue(oPartsArgs.Query["second"]);

		if (string.IsNullOrEmpty(sSecondLotteryFlag)) {
			sSecondLotteryFlag = ViCommConst.FLAG_OFF_STR;
		}

		using (MailLotteryRate oMailLotteryRate = new MailLotteryRate()) {
			oDataSet = oMailLotteryRate.GetOne(this.parseMan.sessionMan.site.siteCd,sMailLotteryRateSeq,sSecondLotteryFlag);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_MAIL_LOTTERY);
	}

	private bool CheckCanGetMailLottery(string pTag,string pArgument) {
		string sMailLotteryRateSeq;
		string sLoseFlag;
		string sResult;
		
		if (string.IsNullOrEmpty(pArgument)) {
			return false;
		}

		using (MailLottery oMailLottery = new MailLottery()) {
			oMailLottery.GetMailLottery(
				this.parseMan.sessionMan.site.siteCd,
				this.parseMan.sessionMan.userMan.userSeq,
				this.parseMan.sessionMan.userMan.userCharNo,
				this.parseMan.sessionMan.sexCd,
				pArgument,
				string.Empty,
				ViCommConst.FLAG_ON,
				out sMailLotteryRateSeq,
				out sLoseFlag,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			return true;
		} else {
			return false;
		}
	}

	private string GetGcappAuthLink(string pTag,string pArgument) {
		string sLink = string.Empty;

		if (parseMan.sessionMan.logined) {
			string sData = string.Format("uid={0}&pw={1}&sex={2}",parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.loginPassword,ViCommConst.MAN);
			sData = AEScryptHelper.Encrypt(sData);
			sLink = string.Format("girlschatapp://browser/?data={0}",sData);
		}

		return sLink;
	}

	private string GetSelfGcappVersion(string pTag,string pArgument) {
		string sResult = string.Empty;

		if (parseMan.sessionMan.logined) {
			using (User oUser = new User()) {
				if (oUser.GetOne(parseMan.sessionMan.userMan.userSeq,ViCommConst.MAN)) {
					sResult = oUser.gcappLastLoginVersion;
				}
			}
		}

		return sResult;
	}

	private bool IsSelfGcappNeedUpdate(string pTag,string pArgument) {
		if (parseMan.sessionMan.logined) {
			string sSelfGcappVersion = string.Empty;
			string sLatestGcappVersion = string.Empty;

			using (User oUser = new User()) {
				if (oUser.GetOne(parseMan.sessionMan.userMan.userSeq,ViCommConst.MAN)) {
					sSelfGcappVersion = oUser.gcappLastLoginVersion;
				}
			}

			if (!string.IsNullOrEmpty(sSelfGcappVersion)) {
				using (SiteManagementEx oSiteManagementEx = new SiteManagementEx()) {
					if (oSiteManagementEx.GetOne(parseMan.sessionMan.site.siteCd)) {
						if (parseMan.sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
							sLatestGcappVersion = oSiteManagementEx.latestGcappVeriOS;
						} else if (parseMan.sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
							sLatestGcappVersion = oSiteManagementEx.latestGcappVerAndroid;
						}
					}
				}

				if (!string.IsNullOrEmpty(sLatestGcappVersion) && !sSelfGcappVersion.Equals(sLatestGcappVersion)) {
					return true;
				}
			}
		}

		return false;
	}
	
	private string GetDaysFromRegist(string pTag,string pArgument) {
		DateTime dtRegistDate = DateTime.Parse(parseMan.sessionMan.userMan.registDay).Date;
		DateTime dtToday = DateTime.Today;
		TimeSpan tsDaysFromRegist = dtToday - dtRegistDate;
		
		return tsDaysFromRegist.Days.ToString();
	}
	
	private string GetFirstSettleEnableFlag(string pTag,string pArgument) {
		string sValue;
		
		int iFirstSettleEnableDays = 0;
		DateTime dtRegistDate = DateTime.Parse(parseMan.sessionMan.userMan.registDay).Date;
		DateTime dtToday = DateTime.Today;
		TimeSpan tsDaysFromRegist = dtToday - dtRegistDate;
		bool bFirstSettleEnable;

		using (Site oSite = new Site()) {
			string sFirstSettleEnableDays = string.Empty;
			oSite.GetValue(parseMan.sessionMan.site.siteCd,"FIRST_SETTLE_ENABLE_DAYS",ref sFirstSettleEnableDays);

			if (!int.TryParse(sFirstSettleEnableDays,out iFirstSettleEnableDays)) {
				iFirstSettleEnableDays = 0;
			}
		}

		if (iFirstSettleEnableDays == 0) {
			bFirstSettleEnable = true;
		} else if (iFirstSettleEnableDays >= tsDaysFromRegist.Days) {
			bFirstSettleEnable = true;
		} else {
			bFirstSettleEnable = false;
		}

		if (parseMan.sessionMan.userMan.totalReceiptAmt > 0 || !bFirstSettleEnable) {
			sValue = ViCommConst.FLAG_OFF_STR;
		} else {
			sValue = ViCommConst.FLAG_ON_STR;
		}
		
		return sValue;
	}

	private string GetDaysFirstSettleEnable(string pTag,string pArgument) {
		string sValue = string.Empty;

		int iFirstSettleEnableDays = 0;
		DateTime dtRegistDate = DateTime.Parse(parseMan.sessionMan.userMan.registDay).Date;
		DateTime dtToday = DateTime.Today;
		TimeSpan tsDaysFromRegist = dtToday - dtRegistDate;

		using (Site oSite = new Site()) {
			string sFirstSettleEnableDays = string.Empty;
			oSite.GetValue(parseMan.sessionMan.site.siteCd,"FIRST_SETTLE_ENABLE_DAYS",ref sFirstSettleEnableDays);

			if (!int.TryParse(sFirstSettleEnableDays,out iFirstSettleEnableDays)) {
				iFirstSettleEnableDays = 0;
			}
		}
		
		int iDaysFirstSettleEnable = 0;

		if (iFirstSettleEnableDays >= tsDaysFromRegist.Days && iFirstSettleEnableDays > 0 && parseMan.sessionMan.userMan.totalReceiptAmt == 0) {
			iDaysFirstSettleEnable = iFirstSettleEnableDays - tsDaysFromRegist.Days;
			sValue = iDaysFirstSettleEnable.ToString();
		}

		return sValue;
	}

	private string GetSelfLastSettleType(string pTag,string pArgument) {
		string sSettleType = string.Empty;

		using (Receipt oReceipt = new Receipt()) {
			sSettleType = oReceipt.GetLastSettleType(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq);
		}

		return sSettleType;
	}
	
	private bool CheckProfileComplete(string pTag,string pArgument) {
		bool bOk = true;

		int i = 0;

		Regex regex = new Regex("内緒",RegexOptions.Compiled);

		for (i = 0;i < parseMan.sessionMan.userMan.attrList.Count;i++) {
			if (parseMan.sessionMan.userMan.attrList[i].inputType != ViCommConst.INPUT_TYPE_TEXT) {
				if (parseMan.sessionMan.userMan.attrList[i].profileReqItemFlag == false) {
					if (string.IsNullOrEmpty(parseMan.sessionMan.userMan.attrList[i].attrSeq)) {
						bOk = false;
						break;
					}
				}
			}
		}
		
		return bOk;
	}
	
	private string GetProfileSecretCount(string pTag,string pArgument) {
		int iProfileSecretCount = 0;

		int i = 0;

		Regex regex = new Regex("内緒",RegexOptions.Compiled);

		for (i = 0;i < parseMan.sessionMan.userMan.attrList.Count;i++) {
			if (parseMan.sessionMan.userMan.attrList[i].profileReqItemFlag == false) {
				if (regex.Match(parseMan.sessionMan.userMan.attrList[i].attrNm).Success) {
					iProfileSecretCount = iProfileSecretCount + 1;
				}
			}
		}
		
		return iProfileSecretCount.ToString();
	}

	private string GetProfileCommentLength(string pTag,string pArgument) {
		int iLength = 0;

		int i = 0;

		for (i = 0;i < parseMan.sessionMan.userMan.attrList.Count;i++) {
			if (parseMan.sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
				iLength = parseMan.sessionMan.userMan.attrList[i].attrImputValue.Length;
				break;
			}
		}

		return iBridUtil.GetStringValue(iLength);
	}

	private bool CheckAddableProfilePoint(string pTag,string pArgument) {
		bool bOk = true;

		int iProfileSecretCount = 0;

		int i = 0;

		Regex regex = new Regex("内緒",RegexOptions.Compiled);

		for (i = 0;i < parseMan.sessionMan.userMan.attrList.Count;i++) {
			if (parseMan.sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
				if (string.IsNullOrEmpty(parseMan.sessionMan.userMan.attrList[i].attrImputValue)) {
					bOk = false;
					break;
				} else if (parseMan.sessionMan.userMan.attrList[i].attrImputValue.Length < 30) {
					bOk = false;
					break;
				}
			} else {
				if (parseMan.sessionMan.userMan.attrList[i].profileReqItemFlag == false) {
					if (string.IsNullOrEmpty(parseMan.sessionMan.userMan.attrList[i].attrSeq)) {
						bOk = false;
						break;
					} else if (regex.Match(parseMan.sessionMan.userMan.attrList[i].attrNm).Success) {
						iProfileSecretCount = iProfileSecretCount + 1;
					}
				}
			}
		}

		if (iProfileSecretCount > 3) {
			bOk = false;
		}

		return bOk;
	}

	private string GetLikeMeCastCharacter(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		SeekCondition oCondition = new SeekCondition();
		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			int iRandomFlag;
			int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["random"]),out iRandomFlag);
			
			oCondition.siteCd = parseMan.sessionMan.site.siteCd;
			oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
			oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
			oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
			oCondition.conditionFlag = ViCommConst.INQUIRY_LIKE_ME;
			oCondition.directRandom = iRandomFlag;

			oDataSet = oCast.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
		}

		return sValue;
	}

	private string GetLikeMeCastCharacterCount(string pTag,string pArgument) {
		decimal dTotalRecCount = 0;

		SeekCondition oCondition = new SeekCondition();
		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			oCondition.siteCd = parseMan.sessionMan.site.siteCd;
			oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
			oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
			oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
			oCondition.conditionFlag = ViCommConst.INQUIRY_LIKE_ME;

			oCast.GetPageCount(oCondition,1,out dTotalRecCount);
		}

		return dTotalRecCount.ToString();
	}

	private string GetSelfFavoritCastCharacter(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		SeekCondition oCondition = new SeekCondition();
		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			int iRandomFlag;
			int.TryParse(iBridUtil.GetStringValue(oPartsArgs.Query["random"]),out iRandomFlag);

			oCondition.siteCd = parseMan.sessionMan.site.siteCd;
			oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
			oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
			oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
			oCondition.conditionFlag = ViCommConst.INQUIRY_FAVORIT;
			oCondition.directRandom = iRandomFlag;

			oDataSet = oCast.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
		}

		return sValue;
	}

	private string GetSelfFavoritCastCharacterCount(string pTag,string pArgument) {
		decimal dTotalRecCount = 0;

		SeekCondition oCondition = new SeekCondition();
		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			oCondition.siteCd = parseMan.sessionMan.site.siteCd;
			oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
			oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
			oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
			oCondition.conditionFlag = ViCommConst.INQUIRY_FAVORIT;

			oCast.GetPageCount(oCondition,1,out dTotalRecCount);
		}

		return dTotalRecCount.ToString();
	}
	
	private string GetQueryAdForPhp(string pTag,string pArgument) {
		string sValue = string.Empty;
		List<string> oQueryList = new List<string>();
		
		if (!string.IsNullOrEmpty(this.parseMan.sessionMan.adCd)) {
			oQueryList.Add(string.Format("ad={0}",this.parseMan.sessionMan.adCd));
		}
		
		if (!string.IsNullOrEmpty(this.parseMan.sessionMan.affiliateCompany)) {
			oQueryList.Add(string.Format("cp={0}",this.parseMan.sessionMan.affiliateCompany));
		}
		
		if (!string.IsNullOrEmpty(this.parseMan.sessionMan.affiliaterCd)) {
			oQueryList.Add(string.Format("aff={0}",this.parseMan.sessionMan.affiliaterCd));
		}
		
		if (this.parseMan.sessionMan.adGroup != null && !string.IsNullOrEmpty(this.parseMan.sessionMan.adGroup.adGroupCd)) {
			oQueryList.Add(string.Format("adg={0}",this.parseMan.sessionMan.adGroup.adGroupCd));
		}
		
		if (!string.IsNullOrEmpty(this.parseMan.sessionMan.introducerFriendCd)) {
			oQueryList.Add(string.Format("intro={0}",this.parseMan.sessionMan.introducerFriendCd));
		}

		if (oQueryList.Count > 0) {
			sValue = string.Join("&",oQueryList.ToArray());
		}
		
		return sValue;
	}

	private string GetCastQuickResponse(string pTag,string pArgument) {
		DataSet oDataSet = null;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);
		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.conditionFlag = ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE;
		oCondition.siteCd = parseMan.sessionMan.site.siteCd;
		oCondition.userSeq = parseMan.sessionMan.userMan.userSeq;
		oCondition.userCharNo = parseMan.sessionMan.userMan.userCharNo;
		oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;

		using (Cast oCast = new Cast(parseMan.sessionObjs)) {
			oDataSet = oCast.GetPageCollection(oCondition,1,oPartsArgs.NeedCount);
		}

		return this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,ViCommConst.DATASET_CAST);
	}

	private bool IsQuickResponseEntry(string pTag,string pArgument) {
		bool bOK = false;
		
		string[] sValues = pArgument.Split(',');
		
		if (sValues.Length != 2) {
			return false;
		}
		
		string sSiteCd = parseMan.sessionMan.site.siteCd;
		string sSelfUserSeq = parseMan.sessionMan.userMan.userSeq;
		string sSelfCharNo = parseMan.sessionMan.userMan.userCharNo;
		string sCastUserSeq = sValues[0];
		string sCastCharNo = sValues[1];
		
		using (Cast oCast = new Cast()) {
			bOK = oCast.IsQuickResponseEntry(sSiteCd,sSelfUserSeq,sSelfCharNo,sCastUserSeq,sCastCharNo);
		}
		
		return bOK;
	}

	private string GetSelfRichinoRankKeepAmt(string pTag,string pArgument) {
		string sValue = string.Empty;
		DataSet oDataSet;
		ParseViComm.DesignPartsArgument oPartsArgs = ParseViComm.DesignPartsArgument.Parse(this.parseMan,pTag,pArgument);

		using (RichinoRank oRichinoRank = new RichinoRank()) {
			oDataSet = oRichinoRank.GetSelfRichinoRankKeepAmt(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq,parseMan.sessionMan.userMan.userCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = this.parseMan.parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,PwViCommConst.DATASET_RICHINO_RANK);
		}

		return sValue;
	}
	
	private bool GetCreditAutoSettleExistFlag(string pTag,string pArgument) {
		bool bExist = false;

		using (CreditAutoSettleStatus oCreditAutoSettleStatus = new CreditAutoSettleStatus()) {
			bExist = oCreditAutoSettleStatus.IsExistAvailable(parseMan.sessionMan.site.siteCd,parseMan.sessionMan.userMan.userSeq);
		}
		
		return bExist;
	}
}