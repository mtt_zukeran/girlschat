﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class RecoveryForce:DbSession {
	public RecoveryForce() {
	}

	public void RecoveryForceFull(string pSiteCd,string pUserSeq,string pUserCharNo,int pUseCoopPointFlag,int? pGameItemSeq,int pMskForce,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECOVERY_FORCE_FULL");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pUSE_COOP_POINT_FLAG",DbSession.DbType.NUMBER,pUseCoopPointFlag);
			db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.NUMBER,pGameItemSeq);
			db.ProcedureInParm("pMSK_FORCE",DbSession.DbType.NUMBER,pMskForce);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
}
