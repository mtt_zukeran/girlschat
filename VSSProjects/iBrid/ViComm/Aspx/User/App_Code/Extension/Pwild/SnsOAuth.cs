﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: SNS OAuth認証
--	Progaram ID		: SnsOAuth
--
--  Creation Date	: 2014.01.16
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class SnsOAuth:DbSession {
	public SnsOAuth() {
	}

	public DataSet GetOneBySnsType(string pSiteCd,string pSnsType) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SNS_NAME					,	");
		oSqlBuilder.AppendLine("	CONSUMER_KEY				,	");
		oSqlBuilder.AppendLine("	CONSUMER_SECRET				,	");
		oSqlBuilder.AppendLine("	REQUEST_TOKEN_URL			,	");
		oSqlBuilder.AppendLine("	AUTHORIZE_URL				,	");
		oSqlBuilder.AppendLine("	ACCESS_TOKEN_URL			,	");
		oSqlBuilder.AppendLine("	GRAPH_URL					,	");
		oSqlBuilder.AppendLine("	SCOPE_URL1					,	");
		oSqlBuilder.AppendLine("	SCOPE_URL2						");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_SNS_OAUTH						");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	SNS_TYPE	= :SNS_TYPE			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SNS_TYPE",pSnsType));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
