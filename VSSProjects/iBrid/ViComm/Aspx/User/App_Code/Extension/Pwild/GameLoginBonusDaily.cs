﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class GameLoginBonusDailySeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SexCd;
	public string DateNowFlag;
	public string PublishFlag;

	public GameLoginBonusDailySeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameLoginBonusDailySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

#endregion ============================================================================================================

public class GameLoginBonusDaily:DbSession {
	public GameLoginBonusDaily() {
	}

	public DataSet GetDailyLoginBonusData(GameLoginBonusDailySeekCondition pCondition) {

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	P.SITE_CD							,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ						,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_NM						,	");
		oSqlBuilder.AppendLine("	P.ITEM_COUNT	AS	GAME_ITEM_COUNT	,	");
		oSqlBuilder.AppendLine("	P.GAME_POINT							");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM03	P					");

		// where
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		string sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(GameLoginBonusDailySeekCondition pCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (pCondition == null)
			throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		if (!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD	= :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(" ( P.GAME_LOGIN_BONUS_START_DAY <= CURRENT_DATE AND P.GAME_LOGIN_BONUS_END_DAY >= CURRENT_DATE ) ",ref pWhereClause);

		if (!String.IsNullOrEmpty(pCondition.PublishFlag)) {
			SysPrograms.SqlAppendWhere(" (P.GAME_ITEM_SEQ	IS NOT NULL	AND P.PUBLISH_FLAG = :PUBLISH_FLAG)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PUBLISH_FLAG",pCondition.PublishFlag));
		}

		return oParamList.ToArray();
	}

	public string GetLoginBonusDayGamePoint(string pSiteCd,string pSexCd) {

		string sValue;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	SUM(GAME_POINT)	AS	GAME_POINT_TOTAL			");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	T_GAME_LOGIN_BONUS_DAY							");
		oSqlBuilder.AppendLine(" WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD						= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	SEX_CD						= :SEX_CD		AND	");
		oSqlBuilder.AppendLine("	GAME_LOGIN_BONUS_START_DATE	<= CURRENT_DATE	AND	");
		oSqlBuilder.AppendLine("	GAME_LOGIN_BONUS_END_DATE	>= CURRENT_DATE		");
		
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":SEX_CD",pSexCd);
				
				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["GAME_POINT_TOTAL"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}
	
	public string GetGameLoginBonusGetFlag(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sValue = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	COUNT(*) AS REC											");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_GAME_LOGIN_BONUS_DAY_LOG								");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("	REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());		

		if (oDataSet.Tables[0].Rows[0]["REC"].ToString().Equals(ViCommConst.FLAG_OFF_STR)) {
			sValue = ViCommConst.FLAG_OFF_STR;
		} else {
			sValue = ViCommConst.FLAG_ON_STR;
		}

		return sValue;
	}
}
