﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メール通知アプリ Android端末UUID
--	Progaram ID		: DeviceUuid
--
--  Creation Date	: 2015.01.15
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class DeviceUuid:DbSession {
	public DeviceUuid() {
	}

	public void DeviceUuidMainte(string pSiteCd,string pUserSeq,string pDeviceId,string pAndroidId,string pMacAddress,string pDeviceSerialNo,ref string pDeviceUuid) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DEVICE_UUID_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pDEVICE_ID",DbSession.DbType.VARCHAR2,pDeviceId);
			db.ProcedureInParm("pANDROID_ID",DbSession.DbType.VARCHAR2,pAndroidId);
			db.ProcedureInParm("pMAC_ADDRESS",DbSession.DbType.VARCHAR2,pMacAddress);
			db.ProcedureInParm("pDEVICE_SERIAL_NO",DbSession.DbType.VARCHAR2,pDeviceSerialNo);
			db.ProcedureBothParm("pDEVICE_UUID",DbSession.DbType.VARCHAR2,pDeviceUuid);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pDeviceUuid = db.GetStringValue("pDEVICE_UUID");
		}
	}
}
