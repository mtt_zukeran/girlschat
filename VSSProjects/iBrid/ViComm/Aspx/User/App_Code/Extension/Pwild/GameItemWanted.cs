﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ｱｲﾃﾑ(欲しい物登録)
--	Progaram ID		: GameItemWanted
--
--  Creation Date	: 2011.08.16
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class GameItemWantedSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string GameItemCategoryType {
		get {
			return this.Query["item_category"];
		}
		set {
			this.Query["item_category"] = value;
		}
	}

	public string StageLevel {
		get {
			return this.Query["stage_level"];
		}
		set {
			this.Query["stage_level"] = value;
		}
	}

	public GameItemWantedSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameItemWantedSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["item_category"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_category"]));
	}
}

[System.Serializable]
public class GameItemWanted:DbSession {
	public GameItemWanted() {
	}

	public int GetPageCount(GameItemWantedSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		string sWhereClauseWanted = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	(								");
		oSqlBuilder.AppendLine("	SELECT							");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ				");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		VW_PW_GAME_ITEM_PRESENT01	");

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) ITEM						");
		oSqlBuilder.AppendLine("	LEFT OUTER JOIN				");
		oSqlBuilder.AppendLine("	(							");
		oSqlBuilder.AppendLine("	SELECT						");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ			");
		oSqlBuilder.AppendLine("	FROM						");
		oSqlBuilder.AppendLine("		T_WANTED_ITEM			");

		oParamList.AddRange(this.CreateWhereWanted(pCondtion,ref sWhereClauseWanted));
		oSqlBuilder.AppendLine(sWhereClauseWanted);

		oSqlBuilder.AppendLine("	) WANTED									");
		oSqlBuilder.AppendLine("	ON ITEM.GAME_ITEM_SEQ = WANTED.GAME_ITEM_SEQ");
		oSqlBuilder.AppendLine(" WHERE											");
		oSqlBuilder.AppendLine("	WANTED.GAME_ITEM_SEQ IS NULL				");

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameItemWantedSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(GameItemWantedSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseWanted = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_SEQ,				");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_NM,				");
		oSqlBuilder.AppendLine("	ITEM.ATTACK_POWER,				");
		oSqlBuilder.AppendLine("	ITEM.DEFENCE_POWER,				");
		oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_GET_CD,			");
		oSqlBuilder.AppendLine("	ITEM.PRICE,						");
		oSqlBuilder.AppendLine("	ITEM.PRICE AS FIXED_PRICE,		");
		oSqlBuilder.AppendLine("	PUBLISH_DATE,					");
		oSqlBuilder.AppendLine("	0 AS POSSESSION_COUNT,			");
		oSqlBuilder.AppendLine("	0 AS SALE_FLAG					");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	(								");
		oSqlBuilder.AppendLine("	SELECT							");
		oSqlBuilder.AppendLine("		SITE_CD,					");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ,				");
		oSqlBuilder.AppendLine("		GAME_ITEM_NM,				");
		oSqlBuilder.AppendLine("		ATTACK_POWER,				");
		oSqlBuilder.AppendLine("		DEFENCE_POWER,				");
		oSqlBuilder.AppendLine("		GAME_ITEM_GET_CD,			");
		oSqlBuilder.AppendLine("		PRICE,						");
		oSqlBuilder.AppendLine("		PUBLISH_DATE				");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		VW_PW_GAME_ITEM_PRESENT01	");

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) ITEM				");
		oSqlBuilder.AppendLine("	LEFT OUTER JOIN		");
		oSqlBuilder.AppendLine("	(					");
		oSqlBuilder.AppendLine("	SELECT				");
		oSqlBuilder.AppendLine("		SITE_CD,		");
		oSqlBuilder.AppendLine("		GAME_ITEM_SEQ	");
		oSqlBuilder.AppendLine("	FROM				");
		oSqlBuilder.AppendLine("		T_WANTED_ITEM	");

		oParamList.AddRange(this.CreateWhereWanted(pCondtion,ref sWhereClauseWanted));
		oSqlBuilder.AppendLine(sWhereClauseWanted);

		oSqlBuilder.AppendLine("	) WANTED								");
		oSqlBuilder.AppendLine("	ON	ITEM.SITE_CD = WANTED.SITE_CD AND	");
		oSqlBuilder.AppendLine("		ITEM.GAME_ITEM_SEQ = WANTED.GAME_ITEM_SEQ	");
		oSqlBuilder.AppendLine(" WHERE										");
		oSqlBuilder.AppendLine("	WANTED.GAME_ITEM_SEQ IS NULL			");

		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		AppendPosession(oDataSet,pCondtion);
		AppendSale(oDataSet,pCondtion);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(GameItemWantedSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
		
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));

			if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
				SysPrograms.SqlAppendWhere("OPPOSITE_SEX_CD = :OPPOSITE_SEX_CD",ref pWhereClause);
				oParamList.Add(new OracleParameter(":OPPOSITE_SEX_CD",ViCommConst.OPERATOR));
			} else {
				SysPrograms.SqlAppendWhere("OPPOSITE_SEX_CD = :OPPOSITE_SEX_CD",ref pWhereClause);
				oParamList.Add(new OracleParameter(":OPPOSITE_SEX_CD",ViCommConst.MAN));
			}
		}

		if (!string.IsNullOrEmpty(pCondition.GameItemCategoryType)) {
			SysPrograms.SqlAppendWhere("GAME_ITEM_CATEGORY_TYPE = :GAME_ITEM_CATEGORY_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE",pCondition.GameItemCategoryType));
		}

		if (!String.IsNullOrEmpty(pCondition.StageLevel)) {
			SysPrograms.SqlAppendWhere("STAGE_LEVEL <= :STAGE_LEVEL",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_LEVEL",pCondition.StageLevel));
		}
		
		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhereWanted(GameItemWantedSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(GameItemWantedSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.GameItemSort.GAME_ITEM_SORT_NEW:
				sSortExpression = "ORDER BY PUBLISH_DATE DESC";
				break;
			case PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH:
				sSortExpression = "ORDER BY PRICE DESC, GAME_ITEM_SEQ ASC";
				break;
			case PwViCommConst.GameItemSort.GAME_ITEM_SORT_LOW:
				sSortExpression = "ORDER BY PRICE ASC, GAME_ITEM_SEQ ASC";
				break;
			case PwViCommConst.GameItemSort.GAME_ITEM_SORT_TYPE:
				sSortExpression = "ORDER BY GAME_ITEM_CATEGORY_TYPE ASC";
				break;
			default:
				sSortExpression = "ORDER BY GAME_ITEM_SEQ DESC ";
				break;
		}

		return sSortExpression;
	}

	private void AppendPosession(DataSet pDS,GameItemWantedSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		string sWhereClause = String.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> itemSeqList = new List<string>();

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			itemSeqList.Add(pDR["GAME_ITEM_SEQ"].ToString());
		}

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ,			");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT		");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	T_POSSESSION_GAME_ITEM	");
		
		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		SysPrograms.SqlAppendWhere(string.Format(" GAME_ITEM_SEQ IN ({0})",string.Join(",",itemSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["GAME_ITEM_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["GAME_ITEM_SEQ"]);
			pDR["POSSESSION_COUNT"] = subDR["POSSESSION_COUNT"];
		}
	}

	private void AppendSale(DataSet pDS,GameItemWantedSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		string sWhereClause = String.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> itemSeqList = new List<string>();

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			itemSeqList.Add(pDR["GAME_ITEM_SEQ"].ToString());
		}

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ		,	");
		oSqlBuilder.AppendLine("	DISCOUNTED_PRICE		");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM_SALE02	");

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(string.Format(" GAME_ITEM_SEQ IN ({0})",string.Join(",",itemSeqList.ToArray())),ref sWhereClause);
		SysPrograms.SqlAppendWhere(" SYSDATE BETWEEN SALE_START_DATE AND SALE_END_DATE",ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["GAME_ITEM_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["GAME_ITEM_SEQ"]);
			pDR["PRICE"] = subDR["DISCOUNTED_PRICE"];
			pDR["SALE_FLAG"] = ViCommConst.FLAG_ON_STR;
		}
	}
}
