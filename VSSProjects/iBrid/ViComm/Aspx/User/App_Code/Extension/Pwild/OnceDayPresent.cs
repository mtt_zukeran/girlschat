﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・無料ﾌﾟﾚｾﾞﾝﾄ
--	Progaram ID		: OnceDayPresent
--
--  Creation Date	: 2012.03.30
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class OnceDayPresent:GameItem {
	public OnceDayPresent() {
	}

	public new int GetPageCount(GameItemSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = string.Empty;

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	VW_ONCE_DAY_PRESENT01 P	");

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public new DataSet GetPageCollection(GameItemSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	public new DataSet GetPageCollectionBase(GameItemSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sWhereClause = String.Empty;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	P.SITE_CD					,	");
		oSqlBuilder.AppendLine("	P.SEX_CD					,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ				,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_NM				,	");
		oSqlBuilder.AppendLine("	P.DESCRIPTION				,	");
		oSqlBuilder.AppendLine("	P.PRICE						,	");
		oSqlBuilder.AppendLine("	P.PRESENT_FLAG				,	");
		oSqlBuilder.AppendLine("	P.PUBLISH_FLAG				,	");
		oSqlBuilder.AppendLine("	P.PUBLISH_DATE				,	");
		oSqlBuilder.AppendLine("	P.CREATE_DATE				,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_CATEGORY_TYPE	,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_CATEGORY_NM		,	");
		oSqlBuilder.AppendLine("	P.UPDATE_DATE				,	");
		oSqlBuilder.AppendLine("	ITEM.ATTACK_POWER			,	");
		oSqlBuilder.AppendLine("	ITEM.DEFENCE_POWER				");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_ONCE_DAY_PRESENT01 P		,	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM	ITEM				");
		
		SysPrograms.SqlAppendWhere(" P.SITE_CD = ITEM.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.PRESENT_GAME_ITEM_SEQ = ITEM.GAME_ITEM_SEQ",ref sWhereClause);

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondtion);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public string GetOnceDayPresentDoneFlag(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sFlag = ViCommConst.FLAG_OFF_STR;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	1													");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	T_ONCE_DAY_PRESENT_LOG								");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sFlag = ViCommConst.FLAG_ON_STR;
		}

		return sFlag;
	}

	public string ExecOnceDayPresent(string pSiteCd,string pCastUserSeq,string pCastUserCharNo,string pManUserSeq,string pManUserCharNo,string pGameItemSeq) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ONCE_DAY_PRESENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastUserCharNo);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pMAN_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pManUserCharNo);
			db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,pGameItemSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public int GetRestFreePresentDoneCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																				");
		oSqlBuilder.AppendLine("	SG.FREE_PRESENT_LIMIT_COUNT - NVL(PL.PRESENT_COUNT,0) AS REST_PRESENT_COUNT		");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	T_SOCIAL_GAME SG															,	");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			SITE_CD																,	");
		oSqlBuilder.AppendLine("			PRESENT_COUNT															");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			T_ONCE_DAY_PRESENT_LOG PL												");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')							");
		oSqlBuilder.AppendLine("	) PL																			");
		oSqlBuilder.AppendLine("WHERE																				");
		oSqlBuilder.AppendLine("	SG.SITE_CD	= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD	= PL.SITE_CD												(+)		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			iCount = int.Parse(oDataSet.Tables[0].Rows[0]["REST_PRESENT_COUNT"].ToString());
		}

		return iCount;
	}
}
