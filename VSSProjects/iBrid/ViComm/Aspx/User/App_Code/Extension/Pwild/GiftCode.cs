﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ギフト券管理
--	Progaram ID		: GiftCode
--
--  Creation Date	: 2017.04.11
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class GiftCode:DbSession {
	/// <summary>未割当</summary>
	public const string STATUS_UNALLOCATED = "0";
	/// <summary>仮割当</summary>
	public const string STATUS_TEMP_ALLOCATED = "1";
	/// <summary>割当済み</summary>
	public const string STATUS_ALLOCATED = "2";

	/// <summary>
	/// 検索条件
	/// </summary>
	public class SearchCondition {
		public string siteCd;
		public string userSeq;
		public string giftCodeType;
		public string amount;
		public string status;
		public string paymentYear;
		public string giftCdRowId;

		public SearchCondition() {
			this.siteCd = string.Empty;
			this.userSeq = string.Empty;
			this.giftCodeType = string.Empty;
			this.amount = string.Empty;
			this.status = string.Empty;
			this.paymentYear = string.Empty;
			this.giftCdRowId = string.Empty;
		}
	}

	public GiftCode() {
	}

	/// <summary>
	/// 検索条件を生成
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhereClause"></param>
	/// <returns></returns>
	private OracleParameter[] CreateWhereType(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 公開中のギフト券（コード）のみ
		SysPrograms.SqlAppendWhere("PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		// ギフト券種類
		if (!string.IsNullOrEmpty(pSearchCondition.giftCodeType)) {
			SysPrograms.SqlAppendWhere("GIFT_CODE_TYPE = :GIFT_CODE_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GIFT_CODE_TYPE",pSearchCondition.giftCodeType));
		}
		// 金額
		if (!string.IsNullOrEmpty(pSearchCondition.amount)) {
			SysPrograms.SqlAppendWhere("AMOUNT = :AMOUNT",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AMOUNT",pSearchCondition.amount));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// ギフト券の種類一覧取得
	/// </summary>
	/// <returns></returns>
	public DataSet GetGiftCodeTypeCollection() {
		SearchCondition pSearchCondition = new SearchCondition();
		return this.GetGiftCodeType(pSearchCondition);
	}

	/// <summary>
	/// ギフト券の種類データ取得
	/// </summary>
	/// <returns></returns>
	public DataSet GetGiftCodeType(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	GIFT_CODE_TYPE");
		oSqlBuilder.AppendLine("	,GIFT_CODE_TYPE_NM");
		oSqlBuilder.AppendLine("	,AMOUNT");
		oSqlBuilder.AppendLine("	,IMAGE_DIR_PATH");
		oSqlBuilder.AppendLine("	,PAYMENT_FEE");
		oSqlBuilder.AppendLine("	,PAYMENT_DELAY_DAYS");
		oSqlBuilder.AppendLine("	,(");
		oSqlBuilder.AppendLine("		SELECT COUNT(*)");
		oSqlBuilder.AppendLine("		FROM T_GIFT_CODE_MANAGE");
		oSqlBuilder.AppendLine("		WHERE GIFT_CODE_TYPE=GCT.GIFT_CODE_TYPE");
		oSqlBuilder.AppendLine("			AND AMOUNT=GCT.AMOUNT");
		oSqlBuilder.AppendLine("			AND EXPIRATION_DATE > SYSDATE");
		oSqlBuilder.AppendLine("			AND PAYMENT_STATUS=:PAYMENT_STATUS");
		oSqlBuilder.AppendLine("	) AS GIFT_REMAINING");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_GIFT_CODE_TYPE_MANAGE GCT");
		oParamList.Add(new OracleParameter(":PAYMENT_STATUS",STATUS_UNALLOCATED));

		oParamList.AddRange(this.CreateWhereType(pSearchCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	GIFT_CODE_TYPE DESC");

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// ギフトコード割当 (＆判定)
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pLoginId"></param>
	/// <param name="pGiftCodeType"></param>
	/// <param name="pAmount"></param>
	/// <param name="pPaymentNum"></param>
	/// <param name="pCheckOnlyFlag"></param>
	/// <param name="pResult"></param>
	public void PaymentGiftCode(
		string pSiteCd
		,string pUserSeq
		,string pLoginId
		,string pGiftCodeType
		,string pAmount
		,string pPaymentNum
		,string pCheckOnlyFlag
		,out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PAYMENT_GIFT_CODE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pGIFT_CODE_TYPE",DbSession.DbType.VARCHAR2,pGiftCodeType);
			db.ProcedureInParm("pAMOUNT",DbSession.DbType.VARCHAR2,pAmount);
			db.ProcedureInParm("pPAYMENT_NUM",DbSession.DbType.VARCHAR2,pPaymentNum);
			db.ProcedureInParm("pCHECK_ONLY_FLAG",DbSession.DbType.VARCHAR2,pCheckOnlyFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	/// <summary>
	/// 交換済みのギフトコード一覧取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <returns></returns>
	public DataSet GetPaymentHistoryGiftCdCollection(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	GCT.GIFT_CODE_TYPE");
		oSqlBuilder.AppendLine("	,GCT.GIFT_CODE_TYPE_NM");
		oSqlBuilder.AppendLine("	,GCT.AMOUNT");
		oSqlBuilder.AppendLine("	,GCT.IMAGE_DIR_PATH");
		oSqlBuilder.AppendLine("	,TO_CHAR(GC.PAYMENT_DATE,'MM/DD') AS PAYMENT_DATE_SHORT");
		oSqlBuilder.AppendLine("	,TO_CHAR(GC.EXPIRATION_DATE,'YYYY/MM/DD') AS EXPIRATION_DATE");
		oSqlBuilder.AppendLine("	,GC.GIFT_CODE");
		oSqlBuilder.AppendLine("	,GC.ROWID AS GIFT_ROWID");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_PAYMENT_HISTORY PH");
		oSqlBuilder.AppendLine("	INNER JOIN T_GIFT_CODE_MANAGE GC");
		oSqlBuilder.AppendLine("		ON PH.USER_SEQ=GC.USER_SEQ");
		oSqlBuilder.AppendLine("		AND PH.PAYMENT_DATE=GC.PAYMENT_DATE");
		oSqlBuilder.AppendLine("	INNER JOIN T_GIFT_CODE_TYPE_MANAGE GCT");
		oSqlBuilder.AppendLine("		ON GCT.GIFT_CODE_TYPE=GC.GIFT_CODE_TYPE");
		oSqlBuilder.AppendLine("		AND GCT.AMOUNT=GC.AMOUNT");

		oParamList.AddRange(this.CreateWherePaymentHistory(pSearchCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	GC.PAYMENT_DATE DESC");

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// 検索条件を生成
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhereClause"></param>
	/// <returns></returns>
	private OracleParameter[] CreateWherePaymentHistory(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 公開中のギフト券（コード）のみ
		SysPrograms.SqlAppendWhere("GCT.PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		// 割当済みのギフトコードのみ
		SysPrograms.SqlAppendWhere("GC.PAYMENT_STATUS = :PAYMENT_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PAYMENT_STATUS",STATUS_ALLOCATED));

		// サイトCD
		if (!string.IsNullOrEmpty(pSearchCondition.siteCd)) {
			SysPrograms.SqlAppendWhere("GC.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.siteCd));
		}
		// ユーザSEQ
		if (!string.IsNullOrEmpty(pSearchCondition.userSeq)) {
			SysPrograms.SqlAppendWhere("GC.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pSearchCondition.userSeq));
		}
		// ギフト券種類
		if (!string.IsNullOrEmpty(pSearchCondition.giftCodeType)) {
			SysPrograms.SqlAppendWhere("GC.GIFT_CODE_TYPE = :GIFT_CODE_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GIFT_CODE_TYPE",pSearchCondition.giftCodeType));
		}
		// 金額
		if (!string.IsNullOrEmpty(pSearchCondition.amount)) {
			SysPrograms.SqlAppendWhere("GC.AMOUNT = :AMOUNT",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AMOUNT",pSearchCondition.amount));
		}
		// ギフトコードレコードID
		if (!string.IsNullOrEmpty(pSearchCondition.giftCdRowId)) {
			SysPrograms.SqlAppendWhere("GC.ROWID = :GIFT_CD_ROWID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GIFT_CD_ROWID",pSearchCondition.giftCdRowId));
		}
		// 交換日時(年)
		if (!string.IsNullOrEmpty(pSearchCondition.paymentYear)) {
			int iYear = 0;
			if (int.TryParse(pSearchCondition.paymentYear,out iYear)) {
				SysPrograms.SqlAppendWhere("GC.PAYMENT_DATE >= TO_DATE(:PAYMENT_DATE_FROM,'YYYY-MM')",ref pWhereClause);
				oParamList.Add(new OracleParameter(":PAYMENT_DATE_FROM",string.Format("{0}-01",iYear.ToString())));
				iYear += 1;
				SysPrograms.SqlAppendWhere("GC.PAYMENT_DATE < TO_DATE(:PAYMENT_DATE_TO,'YYYY-MM')",ref pWhereClause);
				oParamList.Add(new OracleParameter(":PAYMENT_DATE_TO",string.Format("{0}-01",iYear.ToString())));
			}
		}

		return oParamList.ToArray();
	}
}
