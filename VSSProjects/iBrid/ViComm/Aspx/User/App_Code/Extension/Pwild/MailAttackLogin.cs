﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メールアタックイベント(一定期間ログインしていない男性)期間設定
--	Progaram ID		: MailAttackLogin
--
--  Creation Date	: 2016.05.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class MailAttackLogin:DbSession {
	public MailAttackLogin() {
	}

	public DataSet GetOneCurrent(string pSiteCd) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	MAIL_ATTACK_LOGIN_SEQ					,	");
		oSqlBuilder.AppendLine("	NOT_LOGIN_MAX_DAYS						,	");
		oSqlBuilder.AppendLine("	NOT_LOGIN_MIN_DAYS							");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_MAIL_ATTACK_LOGIN							");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	START_DATE		<= SYSDATE				AND	");
		oSqlBuilder.AppendLine("	END_DATE		>= SYSDATE					");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public string RegistMailAttackLoginTx(string pSiteCd,string pTxUserSeq,string pTxCharNo,string pRxUserSeq,string pRxCharNo,string pTxMailSeq,int pCheckOnlyFlag) {
		string sResult;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_MAIL_ATTACK_LOGIN_TX");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pTX_USER_SEQ",DbSession.DbType.VARCHAR2,pTxUserSeq);
			db.ProcedureInParm("pTX_CHAR_NO",DbSession.DbType.VARCHAR2,pTxCharNo);
			db.ProcedureInParm("pRX_USER_SEQ",DbSession.DbType.VARCHAR2,pRxUserSeq);
			db.ProcedureInParm("pRX_CHAR_NO",DbSession.DbType.VARCHAR2,pRxCharNo);
			db.ProcedureInParm("pTX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pTxMailSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pCHECK_ONLY_FLAG",DbSession.DbType.NUMBER,pCheckOnlyFlag);
			db.ExecuteProcedure();
			
			sResult = db.GetStringValue("pRESULT");
		}
		
		return sResult;
	}

	public void AddMailAttackLoginBonus(string pSiteCd,string pManUserSeq,string pManCharNo,string pCastUserSeq,string pCastCharNo,string pAttachedObjType) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_MAIL_ATTACK_LOGIN_BONUS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pMAN_CHAR_NO",DbSession.DbType.VARCHAR2,pManCharNo);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pATTACHED_OBJ_TYPE",DbSession.DbType.VARCHAR2,pAttachedObjType);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}