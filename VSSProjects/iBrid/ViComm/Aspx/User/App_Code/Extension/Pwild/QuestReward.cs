﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエスト報酬
--	Progaram ID		: QuestReward
--
--  Creation Date	: 2012.07.09
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class QuestRewardSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string QuestSeq {
		get {
			return this.Query["quest_seq"];
		}
		set {
			this.Query["quest_seq"] = value;
		}
	}
	
	public string LevelQuestSeq {
		get {
			return this.Query["level_quest_seq"];
		}
		set {
			this.Query["level_quest_seq"] = value;
		}
	}
	
	public string LittleQuestSeq {
		get {
			return this.Query["little_quest_seq"];
		}
		set {
			this.Query["little_quest_seq"] = value;
		}
	}
	
	public string OtherQuestSeq {
		get {
			return this.Query["other_quest_seq"];
		}
		set {
			this.Query["other_quest_seq"] = value;
		}
	}
	
	public string QuestType {
		get {
			return this.Query["quest_type"];
		}
		set {
			this.Query["quest_type"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public QuestRewardSeekCondition()
		: this(new NameValueCollection()) {
	}

	public QuestRewardSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_seq"]));
		this.Query["level_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["level_quest_seq"]));
		this.Query["little_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["little_quest_seq"]));
		this.Query["other_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["other_quest_seq"]));
		this.Query["quest_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_type"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
	}
}

#endregion ============================================================================================================

public class QuestReward:DbSession {
	public QuestReward() {
	}

	public DataSet GetPageCollection(QuestRewardSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string pWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	QUEST_REWARD_SEQ					,	");
		oSqlBuilder.AppendLine("	GAME_POINT							,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ						,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_COUNT						,	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ							,	");
		oSqlBuilder.AppendLine("	TICKET_COUNT						,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM						,	");
		oSqlBuilder.AppendLine("	LOTTERY_NM							,	");
		oSqlBuilder.AppendLine("	TICKET_NM								");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_PW_QUEST_REWARD01					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ		= :QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("	SEX_CD			= :SEX_CD			AND	");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		
		if(pCondition.QuestType.Equals(PwViCommConst.GameQuestType.LEVEL_QUEST)) {
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ			= :LEVEL_QUEST_SEQ			AND	");
			oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
		} else if(pCondition.QuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ			= :LEVEL_QUEST_SEQ			AND	");
			oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
			oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ		= :LITTLE_QUEST_SEQ			AND");
			oParamList.Add(new OracleParameter(":LITTLE_QUEST_SEQ",pCondition.LittleQuestSeq));
		} else if(pCondition.QuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ			= :LEVEL_QUEST_SEQ			AND	");
			oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
		} else if(pCondition.QuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
			oSqlBuilder.AppendLine("	OTHER_QUEST_SEQ			= :OTHER_QUEST_SEQ			AND	");
			oParamList.Add(new OracleParameter(":OTHER_QUEST_SEQ",pCondition.OtherQuestSeq));
		}

		oSqlBuilder.AppendLine("	QUEST_TYPE	= :QUEST_TYPE	");
		oParamList.Add(new OracleParameter(":QUEST_TYPE",pCondition.QuestType));

		sSortExpression = "ORDER BY QUEST_REWARD_SEQ ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public void QuestRewardGetExe(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pQuestSeq,
		string pQuestType,
		string pEntryLogSeq,
		ref string pLevelQuestSeq,
		out string pLittleQuestSeq,
		out string pOtherQuestSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_QUEST_REWARD");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,pQuestSeq);
			db.ProcedureInParm("pQUEST_TYPE",DbSession.DbType.VARCHAR2,pQuestType);
			db.ProcedureInParm("pENTRY_LOG_SEQ",DbSession.DbType.VARCHAR2,pEntryLogSeq);
			db.ProcedureBothParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2,pLevelQuestSeq);
			db.ProcedureOutParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pOTHER_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			pLevelQuestSeq = db.GetStringValue("pLEVEL_QUEST_SEQ");
			pLittleQuestSeq = db.GetStringValue("pLITTLE_QUEST_SEQ");
			pOtherQuestSeq = db.GetStringValue("pOTHER_QUEST_SEQ");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
