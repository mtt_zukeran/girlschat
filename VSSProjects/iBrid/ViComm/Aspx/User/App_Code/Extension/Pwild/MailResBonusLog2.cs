﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メール返信ボーナス履歴2
--	Progaram ID		: MailResBonusLog2
--
--  Creation Date	: 2016.09.05
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class MailResBonusLog2:DbSession {
	public MailResBonusLog2() {
	}

	/// <summary>
	/// メール返信ボーナス履歴登録・更新
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pManUserSeq"></param>
	/// <param name="pManMailSeq"></param>
	/// <param name="pCastUserSeq"></param>
	/// <param name="pCastMailSeq"></param>
	/// <param name="pCastWaitTxFlag"></param>
	/// <param name="pResult"></param>
	/// <param name="pStatus"></param>
	public void MailResBonusLogMainte(
		string pSiteCd,
		string pManUserSeq,
		string pManMailSeq,
		string pCastUserSeq,
		string pCastMailSeq,
		out string pResult,
		out string pStatus
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAIL_RES_BONUS_LOG2_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pMAN_MAIL_SEQ",DbSession.DbType.VARCHAR2,pManMailSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_MAIL_SEQ",DbSession.DbType.VARCHAR2,pCastMailSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
			pStatus = db.GetStringValue("pSTATUS");
		}
	}

	/// <summary>
	/// ポイント付与
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pCastSeq"></param>
	/// <param name="pCastMailSeq"></param>
	/// <param name="pAddPoint"></param>
	/// <param name="pResult"></param>
	public void AddMailResBonusPoint(
		string pSiteCd,
		string pCastSeq,
		string pCastMailSeq,
		out string pAddPoint,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_MAIL_RES_BONUS_POINT2");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_SEQ",DbSession.DbType.VARCHAR2,pCastSeq);
			db.ProcedureInParm("pCAST_MAIL_SEQ",DbSession.DbType.VARCHAR2,pCastMailSeq);
			db.ProcedureOutParm("pADD_POINT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pAddPoint = db.GetStringValue("pADD_POINT");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	/// <summary>
	/// ポイント付与対象の会員かどうかをチェックする
	/// 　(メール返信前)
	/// </summary>
	/// <param name="pMailResBonusSeq"></param>
	/// <param name="pMailResBonusLimitSec1"></param>
	/// <param name="pMailResBonusLimitSec2"></param>
	/// <param name="pMailResBonusMaxNum"></param>
	/// <param name="pMailResBonusUnreceivedDays"></param>
	/// <param name="pSiteCd"></param>
	/// <param name="pManSeq"></param>
	/// <param name="pCastSeq"></param>
	/// <param name="pMainFlag"></param>
	/// <param name="pSubFlag"></param>
	public void CheckUser(
		string pMailResBonusSeq,
		string pMailResBonusLimitSec1,
		string pMailResBonusLimitSec2,
		string pMailResBonusMaxNum,
		string pMailResBonusUnreceivedDays,
		string pSiteCd,
		string pManSeq,
		string pCastSeq,
		out string pMainFlag,
		out string pSubFlag
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CASE WHEN");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		T1.PARTNER_USER_SEQ IS NULL");
		oSqlBuilder.AppendLine("		OR (");
		oSqlBuilder.AppendLine("			T1.PARTNER_USER_SEQ = :CAST_USER_SEQ");
		oSqlBuilder.AppendLine("			AND T1.LAST_TX_MAIL_DATE <= (SYSDATE - NUMTODSINTERVAL(:UNRECEIVED_DAYS, 'DAY'))");
		oSqlBuilder.AppendLine("		)");
		oSqlBuilder.AppendLine("		OR (");
		oSqlBuilder.AppendLine("			MRBL.MAIL_RES_BONUS_SEQ = :MAIL_RES_BONUS_SEQ");
		oSqlBuilder.AppendLine("			AND 1 = MRBL.MAIL_RES_NUM");
		oSqlBuilder.AppendLine("			AND MRBL.CAST_MAIL_SEND_DATE IS NULL");
		oSqlBuilder.AppendLine("			AND MRBL.CAST_WAIT_TX_FLAG = 0");
		oSqlBuilder.AppendLine("			AND (SYSDATE - NUMTODSINTERVAL(:RES_LIMIT_SEC1, 'SECOND')) <= MRBL.MAN_MAIL_SEND_DATE");
		oSqlBuilder.AppendLine("		)");
		oSqlBuilder.AppendLine("	) THEN 1 ELSE 0 END AS IS_MAIL_RES_BONUS_1,");
		oSqlBuilder.AppendLine("	CASE WHEN");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		T1.PARTNER_USER_SEQ = :CAST_USER_SEQ");
		oSqlBuilder.AppendLine("		AND MRBL.MAIL_RES_BONUS_SEQ = :MAIL_RES_BONUS_SEQ");
		oSqlBuilder.AppendLine("		AND ((");
		oSqlBuilder.AppendLine("				:RES_BONUS_MAX_NUM > MRBL.MAIL_RES_NUM");
		oSqlBuilder.AppendLine("				AND 1 < MRBL.MAIL_RES_NUM");
		oSqlBuilder.AppendLine("			) OR (");
		oSqlBuilder.AppendLine("				1 = MRBL.MAIL_RES_NUM");
		oSqlBuilder.AppendLine("				AND (SYSDATE - NUMTODSINTERVAL(:RES_LIMIT_SEC1, 'SECOND')) > MRBL.MAN_MAIL_SEND_DATE");
		oSqlBuilder.AppendLine("			) OR (");
		oSqlBuilder.AppendLine("				:RES_BONUS_MAX_NUM = MRBL.MAIL_RES_NUM");
		oSqlBuilder.AppendLine("				AND MRBL.CAST_MAIL_SEND_DATE IS NULL");
		oSqlBuilder.AppendLine("				AND MRBL.CAST_WAIT_TX_FLAG = 0");
		oSqlBuilder.AppendLine("				AND (SYSDATE - NUMTODSINTERVAL(:RES_LIMIT_SEC2, 'SECOND')) <= MRBL.MAN_MAIL_SEND_DATE");
		oSqlBuilder.AppendLine("			)");
		oSqlBuilder.AppendLine("		)");
		oSqlBuilder.AppendLine("	) THEN 1 ELSE 0 END AS IS_MAIL_RES_BONUS_2");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			P.SITE_CD,");
		oSqlBuilder.AppendLine("			P.USER_SEQ,");
		oSqlBuilder.AppendLine("			C.PARTNER_USER_SEQ,");
		oSqlBuilder.AppendLine("			C.LAST_TX_MAIL_DATE");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			(");
		oSqlBuilder.AppendLine("				SELECT");
		oSqlBuilder.AppendLine("					UMC.SITE_CD,");
		oSqlBuilder.AppendLine("					U.USER_SEQ,");
		oSqlBuilder.AppendLine("					U.ADMIN_FLAG,");
		oSqlBuilder.AppendLine("					UMC.NA_FLAG");
		oSqlBuilder.AppendLine("				FROM");
		oSqlBuilder.AppendLine("					T_USER U,");
		oSqlBuilder.AppendLine("					T_USER_MAN_CHARACTER UMC");
		oSqlBuilder.AppendLine("				WHERE");
		oSqlBuilder.AppendLine("					U.USER_SEQ = UMC.USER_SEQ");
		oSqlBuilder.AppendLine("					AND UMC.SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("					AND U.USER_SEQ = :MAN_USER_SEQ");
		oSqlBuilder.AppendLine("			) P");
		oSqlBuilder.AppendLine("			LEFT JOIN T_COMMUNICATION C");
		oSqlBuilder.AppendLine("				ON P.SITE_CD = C.SITE_CD");
		oSqlBuilder.AppendLine("				AND P.USER_SEQ = C.USER_SEQ");
		oSqlBuilder.AppendLine("				AND C.PARTNER_USER_SEQ = :CAST_USER_SEQ");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			P.ADMIN_FLAG = :USER_ADMIN_FLAG");
		oSqlBuilder.AppendLine("			AND P.NA_FLAG = :USER_NA_FLAG");
		oSqlBuilder.AppendLine("			AND NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND PARTNER_USER_SEQ = :REFUSE_CAST_USER_SEQ)");
		oSqlBuilder.AppendLine("	) T1");
		oSqlBuilder.AppendLine("	LEFT JOIN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			IMRBL2.*");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			(");
		oSqlBuilder.AppendLine("				SELECT");
		oSqlBuilder.AppendLine("					SITE_CD,");
		oSqlBuilder.AppendLine("					MAIL_RES_BONUS_SEQ,");
		oSqlBuilder.AppendLine("					MAX(MAIL_RES_NUM) AS MAIL_RES_NUM,");
		oSqlBuilder.AppendLine("					MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("					CAST_USER_SEQ");
		oSqlBuilder.AppendLine("				FROM");
		oSqlBuilder.AppendLine("					T_MAIL_RES_BONUS_LOG2");
		oSqlBuilder.AppendLine("				WHERE");
		oSqlBuilder.AppendLine("					MAIL_RES_BONUS_SEQ = :MAIL_RES_BONUS_SEQ");
		oSqlBuilder.AppendLine("					AND SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("					AND MAN_USER_SEQ = :MAN_USER_SEQ");
		oSqlBuilder.AppendLine("					AND CAST_USER_SEQ = :CAST_USER_SEQ");
		oSqlBuilder.AppendLine("				GROUP BY");
		oSqlBuilder.AppendLine("					SITE_CD, MAIL_RES_BONUS_SEQ, MAN_USER_SEQ, CAST_USER_SEQ");
		oSqlBuilder.AppendLine("			) IMRBL1,");
		oSqlBuilder.AppendLine("			T_MAIL_RES_BONUS_LOG2 IMRBL2");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			IMRBL1.SITE_CD = IMRBL2.SITE_CD");
		oSqlBuilder.AppendLine("			AND IMRBL1.MAIL_RES_BONUS_SEQ = IMRBL2.MAIL_RES_BONUS_SEQ");
		oSqlBuilder.AppendLine("			AND IMRBL1.MAIL_RES_NUM = IMRBL2.MAIL_RES_NUM");
		oSqlBuilder.AppendLine("			AND IMRBL1.MAN_USER_SEQ = IMRBL2.MAN_USER_SEQ");
		oSqlBuilder.AppendLine("			AND IMRBL1.CAST_USER_SEQ = IMRBL2.CAST_USER_SEQ");
		oSqlBuilder.AppendLine("	) MRBL");
		oSqlBuilder.AppendLine("		ON T1.SITE_CD = MRBL.SITE_CD");
		oSqlBuilder.AppendLine("		AND T1.USER_SEQ = MRBL.MAN_USER_SEQ");
		oSqlBuilder.AppendLine("		AND T1.PARTNER_USER_SEQ = MRBL.CAST_USER_SEQ");
		oParamList.Add(new OracleParameter(":MAIL_RES_BONUS_SEQ",pMailResBonusSeq));
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pManSeq));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCastSeq));
		oParamList.Add(new OracleParameter(":REFUSE_CAST_USER_SEQ",pCastSeq));
		oParamList.Add(new OracleParameter(":USER_ADMIN_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":USER_NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":RES_LIMIT_SEC1",pMailResBonusLimitSec1));
		oParamList.Add(new OracleParameter(":RES_LIMIT_SEC2",pMailResBonusLimitSec2));
		oParamList.Add(new OracleParameter(":RES_BONUS_MAX_NUM",pMailResBonusMaxNum));
		oParamList.Add(new OracleParameter(":UNRECEIVED_DAYS",pMailResBonusUnreceivedDays));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pMainFlag = ViCommConst.FLAG_OFF_STR;
		pSubFlag = ViCommConst.FLAG_OFF_STR;

		if (oDataSet.Tables[0].Rows.Count > 0) {
			DataRow dr = oDataSet.Tables[0].Rows[0];
			if (dr["IS_MAIL_RES_BONUS_1"].ToString().Equals("1")) {
				pMainFlag = ViCommConst.FLAG_ON_STR;
			}
			if (dr["IS_MAIL_RES_BONUS_2"].ToString().Equals("1")) {
				pSubFlag = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	/// <summary>
	/// ポイント付与対象の会員への送信かどうかをチェックする
	/// 　(メール返信後、ポイント付与前)
	/// </summary>
	/// <param name="pMailResBonusSeq"></param>
	/// <param name="pMailResBonusLimitSec1"></param>
	/// <param name="pMailResBonusLimitSec2"></param>
	/// <param name="pMailResBonusMaxNum"></param>
	/// <param name="pSiteCd"></param>
	/// <param name="pManSeq"></param>
	/// <param name="pCastSeq"></param>
	/// <param name="pCastMailSeq"></param>
	/// <param name="pMainFlag"></param>
	/// <param name="pSubFlag"></param>
	public void CheckSendMail(
		string pMailResBonusSeq,
		string pMailResBonusLimitSec1,
		string pMailResBonusLimitSec2,
		string pMailResBonusMaxNum,
		string pSiteCd,
		string pManSeq,
		string pCastSeq,
		string pCastMailSeq,
		out string pMainFlag,
		out string pSubFlag
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CASE WHEN MAIL_RES_NUM = 1");
		oSqlBuilder.AppendLine("		AND MAN_MAIL_SEND_DATE >= (NVL(CAST_MAIL_SEND_DATE,SYSDATE) - NUMTODSINTERVAL(:RES_LIMIT_SEC1, 'SECOND'))");
		oSqlBuilder.AppendLine("		THEN 1 ELSE 0 END AS IS_MAIL_RES_BONUS_1,");
		oSqlBuilder.AppendLine("	CASE WHEN MAIL_RES_NUM BETWEEN 2 AND :RES_BONUS_MAX_NUM");
		oSqlBuilder.AppendLine("		AND MAN_MAIL_SEND_DATE >= (NVL(CAST_MAIL_SEND_DATE,SYSDATE) - NUMTODSINTERVAL(:RES_LIMIT_SEC2, 'SECOND'))");
		oSqlBuilder.AppendLine("		THEN 1 ELSE 0 END AS IS_MAIL_RES_BONUS_2");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAIL_RES_BONUS_LOG2");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	MAIL_RES_BONUS_SEQ = :MAIL_RES_BONUS_SEQ");
		oSqlBuilder.AppendLine("	AND SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("	AND MAN_USER_SEQ = :MAN_USER_SEQ");
		oSqlBuilder.AppendLine("	AND CAST_USER_SEQ = :CAST_USER_SEQ");
		oSqlBuilder.AppendLine("	AND CAST_MAIL_SEQ = :CAST_MAIL_SEQ");
		oParamList.Add(new OracleParameter(":MAIL_RES_BONUS_SEQ",pMailResBonusSeq));
		oParamList.Add(new OracleParameter(":RES_LIMIT_SEC1",pMailResBonusLimitSec1));
		oParamList.Add(new OracleParameter(":RES_LIMIT_SEC2",pMailResBonusLimitSec2));
		oParamList.Add(new OracleParameter(":RES_BONUS_MAX_NUM",pMailResBonusMaxNum));
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pManSeq));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCastSeq));
		oParamList.Add(new OracleParameter(":CAST_MAIL_SEQ",pCastMailSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pMainFlag = ViCommConst.FLAG_OFF_STR;
		pSubFlag = ViCommConst.FLAG_OFF_STR;

		if (oDataSet.Tables[0].Rows.Count > 0) {
			DataRow dr = oDataSet.Tables[0].Rows[0];
			if (dr["IS_MAIL_RES_BONUS_1"].ToString().Equals("1")) {
				pMainFlag = ViCommConst.FLAG_ON_STR;
			}
			if (dr["IS_MAIL_RES_BONUS_2"].ToString().Equals("1")) {
				pSubFlag = ViCommConst.FLAG_ON_STR;
			}
		}
	}
}
