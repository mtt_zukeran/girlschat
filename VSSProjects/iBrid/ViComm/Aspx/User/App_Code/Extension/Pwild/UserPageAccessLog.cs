﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 会員ページアクセスログ
--	Progaram ID		: UserPageAccessLog
--  Creation Date	: 2014.02.11
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class UserPageAccessLog:DbSession {
	public UserPageAccessLog() {
	}

	public void RegistUserPageAccessLog(
		string pSiteCd,
		string pUserSeq,
		string pProgramRoot,
		string pProgramId,
		string pHtmlDocType,
		string pRequestQuery,
		int pPriorityAccessCount,
		string pPageUserAgentType,
		string pCarrierType,
		int pLoginFlag,
		string pUserRank,
		int pNewUserFlag
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_USER_PAGE_ACCESS_LOG");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pPROGRAM_ROOT",DbSession.DbType.VARCHAR2,pProgramRoot);
			db.ProcedureInParm("pPROGRAM_ID",DbSession.DbType.VARCHAR2,pProgramId);
			db.ProcedureInParm("pHTML_DOC_TYPE",DbSession.DbType.VARCHAR2,pHtmlDocType);
			db.ProcedureInParm("pREQUEST_QUERY",DbSession.DbType.VARCHAR2,pRequestQuery);
			db.ProcedureInParm("pPRIORITY_ACCESS_COUNT",DbSession.DbType.NUMBER,pPriorityAccessCount);
			db.ProcedureInParm("pPAGE_USER_AGENT_TYPE",DbSession.DbType.VARCHAR2,pPageUserAgentType);
			db.ProcedureInParm("pCARRIER_TYPE",DbSession.DbType.VARCHAR2,pCarrierType);
			db.ProcedureInParm("pLOGIN_FLAG",DbSession.DbType.NUMBER,pLoginFlag);
			db.ProcedureInParm("pUSER_RANK",DbSession.DbType.VARCHAR2,pUserRank);
			db.ProcedureInParm("pNEW_USER_FLAG",DbSession.DbType.NUMBER,pNewUserFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}
}
