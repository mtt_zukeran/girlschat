﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ動画
--	Progaram ID		: CastGameCharacterMovie
--
--  Creation Date	: 2011.08.26
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterMovieSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public string CastUserMovieSeq {
		get {
			return this.Query["cast_user_movie_seq"];
		}
		set {
			this.Query["cast_user_movie_seq"] = value;
		}
	}

	public CastGameCharacterMovieSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterMovieSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["cast_user_movie_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_user_movie_seq"]));
	}
}

[System.Serializable]
public class CastGameCharacterMovie:CastBase {
	public CastGameCharacterMovie() {
	}

	public CastGameCharacterMovie(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastGameCharacterMovieSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_MOVIE01 P	");

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterMovieSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause,ref sWhereClauseFriend));

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	P.*							,	");
		oSqlBuilder.AppendLine("	FP.RANK AS FRIENDLY_RANK		");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	(SELECT							");
		oSqlBuilder.AppendLine("		SITE_CD					,	");
		oSqlBuilder.AppendLine("		USER_SEQ				,	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("		LOGIN_ID				,	");
		oSqlBuilder.AppendLine("		MOVIE_SEQ				,	");
		oSqlBuilder.AppendLine("		GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("		MOVIE_TITLE				,	");
		oSqlBuilder.AppendLine("		MOVIE_DOC				,	");
		oSqlBuilder.AppendLine("		UPLOAD_DATE				,	");
		oSqlBuilder.AppendLine("		CRYPT_VALUE				,	");
		oSqlBuilder.AppendLine("		CASE											");
		oSqlBuilder.AppendLine("			WHEN										");
		oSqlBuilder.AppendLine("				EXISTS									");
		oSqlBuilder.AppendLine("				(										");
		oSqlBuilder.AppendLine("					SELECT								");
		oSqlBuilder.AppendLine("						*								");
		oSqlBuilder.AppendLine("					FROM								");
		oSqlBuilder.AppendLine("						T_OBJ_USED_HISTORY				");
		oSqlBuilder.AppendLine("					WHERE								");
		oSqlBuilder.AppendLine("						SITE_CD		= P.SITE_CD		AND	");
		oSqlBuilder.AppendLine("						USER_SEQ	= :USER_SEQ		AND	");
		oSqlBuilder.AppendLine("						OBJ_TYPE	= :OBJ_TYPE		AND	");
		oSqlBuilder.AppendLine("						OBJ_SEQ		= P.MOVIE_SEQ		");
		oSqlBuilder.AppendLine("				)										");
		oSqlBuilder.AppendLine("			THEN 1										");
		oSqlBuilder.AppendLine("			ELSE 0										");
		oSqlBuilder.AppendLine("		END AS OBJ_USED_HISTORY_FLG						");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_MOVIE01 P	");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" 	) P,							");
		oSqlBuilder.AppendLine(" 	(SELECT							");
		oSqlBuilder.AppendLine(" 		SITE_CD					,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine(" 		RANK						");
		oSqlBuilder.AppendLine(" 	FROM							");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00			");
		oSqlBuilder.AppendLine(sWhereClauseFriend);
		oSqlBuilder.AppendLine(" 	) FP							");
		oSqlBuilder.AppendLine(" WHERE								");
		oSqlBuilder.AppendLine(" 	P.SITE_CD		= FP.SITE_CD				(+) AND	");
		oSqlBuilder.AppendLine(" 	P.USER_SEQ		= FP.PARTNER_USER_SEQ		(+) AND	");
		oSqlBuilder.AppendLine(" 	P.USER_CHAR_NO	= FP.PARTNER_USER_CHAR_NO	(+)		");

		string sSortExpression = this.CreateOrderExpresion(pCondtion);

		oParamList.Add(new OracleParameter(":OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX.ToString()));
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		if (pCondtion.SeekMode == PwViCommConst.INQUIRY_GAME_CAST_MOVIE_VIEW) {
			AppendTicketPossession(oDataSet,pCondtion);
		}
		
		return oDataSet;
	}

	private void AppendObjUsedHistory(DataSet pDS,CastGameCharacterMovieSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("OBJ_USED_HISTORY_FLG"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		DataRow dr = pDS.Tables[0].Rows[0];
		
		bool bDupUsed;
		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			bDupUsed = oObjUsedHistory.GetOne(
								pCondition.SiteCd,
								pCondition.UserSeq,
								ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
								pCondition.CastUserMovieSeq);
		}
		
		if (bDupUsed) {
			dr["OBJ_USED_HISTORY_FLG"] =  "1"; 
		}else {
			dr["OBJ_USED_HISTORY_FLG"] = "0"; 
		}
	}

	private void AppendTicketPossession(DataSet pDS,CastGameCharacterMovieSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col = new DataColumn("GAME_ITEM_POSSESSION_COUNT",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		DataRow dr = pDS.Tables[0].Rows[0];
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT										");
		oSqlBuilder.AppendLine(" FROM														");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_GAME_ITEM01							");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE	= :GAME_ITEM_CATEGORY_TYPE		");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				this.cmd.Parameters.Add(":GAME_ITEM_CATEGORY_TYPE",PwViCommConst.GameItemCategory.ITEM_CATEGORY_TICKET);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					dr["GAME_ITEM_POSSESSION_COUNT"] = iBridUtil.GetStringValue(oDataReader["POSSESSION_COUNT"].ToString());
				} else {
					dr["GAME_ITEM_POSSESSION_COUNT"] = "0";
				}
			}
		} finally {
			this.conn.Close();
		}
	}
	
	private OracleParameter[] CreateWhere(CastGameCharacterMovieSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.UserCharNo));

		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :PARTNER_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserMovieSeq)) {
			SysPrograms.SqlAppendWhere("MOVIE_SEQ = :MOVIE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MOVIE_SEQ",pCondition.CastUserMovieSeq));
		}

		SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("MOVIE_TYPE = :MOVIE_TYPE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));
		
		SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND BBS_NON_PUBLISH_FLAG = :BBS_NON_PUBLISH_FLAG)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhere(CastGameCharacterMovieSeekCondition pCondition,ref string pWhereClause,ref string pWhereClauseFriend) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref pWhereClause));

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClauseFriend);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClauseFriend);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(CastGameCharacterMovieSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.CastGameCharacterMovieSort.UPLOAD_DATE_DESC:
				sSortExpression = "ORDER BY UPLOAD_DATE DESC";
				break;
			case PwViCommConst.CastGameCharacterMovieSort.UPLOAD_DATE_ASC:
				sSortExpression = "ORDER BY UPLOAD_DATE ASC";
				break;
			default:
				sSortExpression = "ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}
	
	public string GetGameCastMovieCount(string sSiteCd,string sUserSeq,string sUserCharNo) {
		string sValue = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		DataSet oDataSet = new DataSet();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	COUNT(*) AS MOVIE_COUNT					");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_MOVIE01					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["MOVIE_COUNT"].ToString();
		}
		
		return sValue;
	}
}

