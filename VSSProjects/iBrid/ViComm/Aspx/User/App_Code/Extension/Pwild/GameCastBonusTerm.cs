﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者 限定コンプ報酬付与期間
--	Progaram ID		: GameCastBonusTerm
--
--  Creation Date	: 2012.09.17
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;

[Serializable]
public class GameCastBonusTermSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public GameCastBonusTermSeekCondition()
		: this(new NameValueCollection()) {
	}
	public GameCastBonusTermSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
	}
}

[System.Serializable]
public class GameCastBonusTerm:DbSession {
	public GameCastBonusTerm() {
	}

	public DataSet CheckGameCastBonusTerm(GameCastBonusTermSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	CASE													");
		oSqlBuilder.AppendLine("		WHEN SYSDATE BETWEEN START_DATE AND END_DATE THEN 1	");
		oSqlBuilder.AppendLine("		ELSE 0												");
		oSqlBuilder.AppendLine("	END AS BONUS_GET_TERM_FLAG								");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_GAME_CAST_BONUS_TERM									");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD									");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}
