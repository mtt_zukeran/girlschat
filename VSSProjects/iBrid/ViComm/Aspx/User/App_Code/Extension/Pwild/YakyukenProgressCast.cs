﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 野球拳キャスト進捗
--	Progaram ID		: YakyukenProgressCast
--
--  Creation Date	: 2013.05.07
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class YakyukenProgressCast:DbSession {
	public YakyukenProgressCast() {
	}

	public DataSet GetOne(string pSiteCd,string pYakyukenGameSeq) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("YAKYUKEN_GAME_SEQ = :YAKYUKEN_GAME_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":YAKYUKEN_GAME_SEQ",pYakyukenGameSeq));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	LAST_WIN_YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	CLEAR_YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	CLEAR_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_YAKYUKEN_PROGRESS_CAST");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public void RegistYakyukenProgressCast(
		string pSiteCd,
		string pYakyukenGameSeq,
		string pManJyankenType,
		out string pCastJyankenType,
		out string pWinStatus,
		out string pYakyukenType,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_YAKYUKEN_PROGRESS_CAST");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pYAKYUKEN_GAME_SEQ",DbSession.DbType.VARCHAR2,pYakyukenGameSeq);
			db.ProcedureInParm("pMAN_JYANKEN_TYPE",DbSession.DbType.NUMBER,pManJyankenType);
			db.ProcedureOutParm("pCAST_JYANKEN_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pWIN_STATUS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pYAKYUKEN_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pCastJyankenType = db.GetStringValue("pCAST_JYANKEN_TYPE");
			pWinStatus = db.GetStringValue("pWIN_STATUS");
			pYakyukenType = db.GetStringValue("pYAKYUKEN_TYPE");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
