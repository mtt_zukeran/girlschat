﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝写真
--	Progaram ID		: ManTreasureAttr
--
--  Creation Date	: 2011.08.23
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using Oracle.DataAccess.Client;

[Serializable]
public class ManTreasureAttrSeekCondition : SeekConditionBase {

	public string SiteCd {
		get { return this.Query["site"]; }
		set { this.Query["site"] = value; }
	}
	public string UserSeq {
		get { return this.Query["user_seq"]; }
		set { this.Query["user_seq"] = value; }
	}
	public string UserCharNo {
		get { return this.Query["user_char_no"]; }
		set { this.Query["user_char_no"] = value; }
	}
	public string DisplayDay {
		get { return this.Query["display_day"]; }
		set { this.Query["display_day"] = value; }
	}

	public ManTreasureAttrSeekCondition() : this(new NameValueCollection()) { }
	public ManTreasureAttrSeekCondition(NameValueCollection pQuery) : base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["display_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["display_day"]));
	}

}
[System.Serializable]
public class ManTreasureAttr:DbSession
{
	public ManTreasureAttr() {
	}

	public int GetPageCount(ManTreasureAttrSeekCondition pCondition, int pRecPerPage, out decimal pRecCount) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_ATTR P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder, oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}
	public DataSet GetPageCollection(ManTreasureAttrSeekCondition pCondition, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition, pPageNo, pRecPerPage);
	}
	public DataSet GetPageCollectionBase(ManTreasureAttrSeekCondition pCondition, int pPageNo, int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_ATTR_SEQ			,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_ATTR_NM				,	");
		oSqlBuilder.AppendLine("	P.PRIORITY							,	");
		oSqlBuilder.AppendLine("	CASE									");
		oSqlBuilder.AppendLine("		WHEN EXISTS							");
		oSqlBuilder.AppendLine("			(								");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_MAN_TREASURE												");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("					CAST_GAME_PIC_ATTR_SEQ	= P.CAST_GAME_PIC_ATTR_SEQ		AND	");
		oSqlBuilder.AppendLine("					TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG			AND	");
		oSqlBuilder.AppendLine("					DISPLAY_DAY				= :DISPLAY_DAY						");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("		THEN 1								");
		oSqlBuilder.AppendLine("		ELSE 0								");
		oSqlBuilder.AppendLine("	END AS TUTORIAL_BATTLE_FLAG				");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_ATTR P					");
		

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition, ref sWhereClause));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",pCondition.DisplayDay));
		
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, iStartIndex, pRecPerPage, out sExecSql));


		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql, oParamList.ToArray());
		AppendPossession(oDataSet,pCondition);
		return oDataSet;
	
	}

	public OracleParameter[] CreateWhere(ManTreasureAttrSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null)
			throw new ArgumentNullException();

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		return oParamList.ToArray();
	}

	public string CreateOrderExpresion(ManTreasureAttrSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.SeekMode) {
			default:
				sSortExpression = " ORDER BY P.PRIORITY ASC ";
				break;
		}
		return sSortExpression;
	}

	public void AppendPossession(DataSet pDataSet, ManTreasureAttrSeekCondition pCondition) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT																 ").AppendLine();
		oSqlBuilder.Append("	PMT.POSSESSION_COUNT											,").AppendLine();
		oSqlBuilder.Append("	PMT.MOSAIC_ERASE_FLAG											,").AppendLine();
		oSqlBuilder.Append("	PMT.OBJ_SMALL_PHOTO_IMG_PATH									,").AppendLine();
		oSqlBuilder.Append("	PMT.OBJ_SMALL_BLUR_PHOTO_IMG_PATH								 ").AppendLine();
		oSqlBuilder.Append("FROM																 ").AppendLine();
		oSqlBuilder.Append("	VW_PW_POSSESSION_MAN_TRSR01		PMT								 ").AppendLine();
		oSqlBuilder.Append("WHERE																 ").AppendLine();
		oSqlBuilder.Append("	PMT.SITE_CD						= :SITE_CD					AND  ").AppendLine();
		oSqlBuilder.Append("	PMT.USER_SEQ					= :USER_SEQ					AND  ").AppendLine();
		oSqlBuilder.Append("	PMT.USER_CHAR_NO				= :USER_CHAR_NO				AND  ").AppendLine();
		oSqlBuilder.Append("	PMT.DISPLAY_DAY					= :DISPLAY_DAY				AND	 ").AppendLine();
		oSqlBuilder.Append("	PMT.CAST_GAME_PIC_ATTR_SEQ		= :CAST_GAME_PIC_ATTR_SEQ	AND	 ").AppendLine();
		oSqlBuilder.Append("	PMT.PUBLISH_FLAG				= :PUBLISH_FLAG					 ").AppendLine();
		oSqlBuilder.Append("ORDER BY															 ").AppendLine();
		oSqlBuilder.Append("	PMT.MOSAIC_ERASE_FLAG DESC,PMT.UPDATE_DATE DESC					 ").AppendLine();

		pDataSet.Tables[0].Columns.Add(new DataColumn("POSSESSION_COUNT_SUMMARY"		, Type.GetType("System.String")));
		pDataSet.Tables[0].Columns.Add(new DataColumn("MOSAIC_ERASE_FLAG"				, Type.GetType("System.String")));
		pDataSet.Tables[0].Columns.Add(new DataColumn("OBJ_SMALL_PHOTO_IMG_PATH"		, Type.GetType("System.String")));
		pDataSet.Tables[0].Columns.Add(new DataColumn("OBJ_SMALL_BLUR_PHOTO_IMG_PATH"	, Type.GetType("System.String")));

		foreach (DataRow dr in pDataSet.Tables[0].Rows) {
			dr["POSSESSION_COUNT_SUMMARY"] = "0";
			dr["MOSAIC_ERASE_FLAG"] = "0";
			dr["OBJ_SMALL_PHOTO_IMG_PATH"]		= string.Empty;
			dr["OBJ_SMALL_BLUR_PHOTO_IMG_PATH"] = string.Empty;
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
					cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
					cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
					cmd.Parameters.Add(":CAST_GAME_PIC_ATTR_SEQ", dr["CAST_GAME_PIC_ATTR_SEQ"].ToString());
					cmd.Parameters.Add(":DISPLAY_DAY", pCondition.DisplayDay);
					cmd.Parameters.Add(":PUBLISH_FLAG", ViCommConst.FLAG_ON_STR);

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					if(dsSub.Tables[0].Rows.Count > 0){
						int iPossessionCountSummary = 0;
						foreach (DataRow drSub in dsSub.Tables[0].Rows) {
							iPossessionCountSummary += int.Parse(drSub["POSSESSION_COUNT"].ToString());
						}
						dr["POSSESSION_COUNT_SUMMARY"]		= iPossessionCountSummary.ToString();
						dr["MOSAIC_ERASE_FLAG"]				= dsSub.Tables[0].Rows[0]["MOSAIC_ERASE_FLAG"].ToString();
						dr["OBJ_SMALL_PHOTO_IMG_PATH"]		= dsSub.Tables[0].Rows[0]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
						dr["OBJ_SMALL_BLUR_PHOTO_IMG_PATH"] = dsSub.Tables[0].Rows[0]["OBJ_SMALL_BLUR_PHOTO_IMG_PATH"].ToString();
					}
				}
			}
		}
	}

	public DataSet GetManTreasureAttr() {

		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_SEQ				,	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_NM					");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_ATTR 					");
		oSqlBuilder.AppendLine(" ORDER BY PRIORITY ASC						");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return oDataSet;

	}
	
	public string GetCastGamePicAttrNmBySeq(string sSiteCd,string sCastGamePicAttrSeq) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_NM										");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_ATTR 										");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_SEQ	= :CAST_GAME_PIC_ATTR_SEQ			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":CAST_GAME_PIC_ATTR_SEQ",sCastGamePicAttrSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		} else {
			return oDataSet.Tables[0].Rows[0]["CAST_GAME_PIC_ATTR_NM"].ToString();
		}
	}
}
