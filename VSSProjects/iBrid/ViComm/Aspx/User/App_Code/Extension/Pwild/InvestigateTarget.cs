﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せ･ターゲット


--	Progaram ID		: InvestigateTarget
--
--  Creation Date	: 2015.02.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class InvestigateTargetSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string InvestigateTargetSeq {
		get {
			return this.Query["targetseq"];
		}
		set {
			this.Query["targetseq"] = value;
		}
	}
	
	public string ExecutionDay {
		get {
			return this.Query["execution_day"];
		}
		set {
			this.Query["execution_day"] = value;
		}
	}
	
	public string SelfUserSeq;
	public string SelfUserCharNo;
	public string IncludingExecutionDayOrLaterFlag;

	public InvestigateTargetSeekCondition()
		: this(new NameValueCollection()) {
	}
	public InvestigateTargetSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["targetseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["targetseq"]));
		this.Query["execution_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["execution_day"]));
	}
}
[System.Serializable]
public class InvestigateTarget:DbSession {
	public InvestigateTarget()
		: base() {
	}

	public int GetPageCount(InvestigateTargetSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	COUNT(P.USER_SEQ)				");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_INVESTIGATE_TARGET00	P		");

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(InvestigateTargetSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;
		string sSortExpression = "ORDER BY P.EXECUTION_DAY, P.SITE_CD, P.USER_SEQ, P.USER_CHAR_NO, P.REGIST_DATE";
		string sExecSql;
		
		oSqlBuilder.AppendLine("SELECT																				");
		oSqlBuilder.AppendLine("	P.SITE_CD																	,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ																	,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO																,	");
		oSqlBuilder.AppendLine("	P.INVESTIGATE_TARGET_SEQ													,	");
		oSqlBuilder.AppendLine("	P.HANDLE_NM																	,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID																	,	");
		oSqlBuilder.AppendLine("	CASE																			");
		oSqlBuilder.AppendLine("		WHEN (F.PROFILE_PIC_NON_PUBLISH_FLAG = 2) THEN								");
		oSqlBuilder.AppendLine("			GET_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,NULL)							");
		oSqlBuilder.AppendLine("		ELSE																		");
		oSqlBuilder.AppendLine("			GET_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,P.PROFILE_PIC_SEQ)				");
		oSqlBuilder.AppendLine("	END AS PHOTO_IMG_PATH														,	");
		oSqlBuilder.AppendLine("	CASE																			");
		oSqlBuilder.AppendLine("		WHEN (F.PROFILE_PIC_NON_PUBLISH_FLAG = 2) THEN								");
		oSqlBuilder.AppendLine("			GET_SMALL_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,NULL)						");
		oSqlBuilder.AppendLine("		ELSE																		");
		oSqlBuilder.AppendLine("			GET_SMALL_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,P.PROFILE_PIC_SEQ)		");
		oSqlBuilder.AppendLine("	END AS SMALL_PHOTO_IMG_PATH													,	");
		oSqlBuilder.AppendLine("	P.EXECUTION_DAY																,	");
		oSqlBuilder.AppendLine("	P.REGIST_DATE																,	");
		oSqlBuilder.AppendLine("	P.FIRST_HINT_ANNOUNCE_TIME													,	");
		oSqlBuilder.AppendLine("	P.SECOND_HINT_ANNOUNCE_TIME													,	");
		oSqlBuilder.AppendLine("	P.THIRD_HINT_ANNOUNCE_TIME														");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	VW_INVESTIGATE_TARGET00	P													,	");
		oSqlBuilder.AppendLine("	T_FAVORIT F																		");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE															");
		oWhereBuilder.AppendLine("	P.SITE_CD				= F.SITE_CD					(+)	AND	");
		oWhereBuilder.AppendLine("	P.USER_SEQ				= F.USER_SEQ				(+)	AND	");
		oWhereBuilder.AppendLine("	P.USER_CHAR_NO			= F.USER_CHAR_NO			(+)	AND	");
		oWhereBuilder.AppendLine("	:SELF_USER_SEQ			= F.PARTNER_USER_SEQ		(+)	AND	");
		oWhereBuilder.AppendLine("	:SELF_USER_CHAR_NO		= F.PARTNER_USER_CHAR_NO	(+)		");
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.SelfUserCharNo));
		sWhereClause = oWhereBuilder.ToString();
		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.Append(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		AppendWantedHint(oDataSet);
		return oDataSet;
	}

	public DataSet GetOne(InvestigateTargetSeekCondition pCondtion) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	P.INVESTIGATE_REGIST_DATE		 ");
		oSqlBuilder.AppendLine("FROM								 ");
		oSqlBuilder.AppendLine("	VW_INVESTIGATE_TARGET00	P		 ");

		string sWhereClause = string.Empty;

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.Append(sWhereClause);

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(InvestigateTargetSeekCondition pCondtion,ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondtion.SiteCd)) {
			SysPrograms.SqlAppendWhere(":SITE_CD = P.SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondtion.UserSeq)) {
			SysPrograms.SqlAppendWhere(":USER_SEQ = P.USER_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));
		}
		
		if (!string.IsNullOrEmpty(pCondtion.UserCharNo)) {
			SysPrograms.SqlAppendWhere(":USER_CHAR_NO = P.USER_CHAR_NO",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		}
		
		if (!string.IsNullOrEmpty(pCondtion.ExecutionDay)) {
			if (ViCommConst.FLAG_ON_STR.Equals(pCondtion.IncludingExecutionDayOrLaterFlag)) {
				SysPrograms.SqlAppendWhere(":EXECUTION_DAY <= P.EXECUTION_DAY",ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere(":EXECUTION_DAY = P.EXECUTION_DAY",ref pWhere);
			}
			oOracleParameterList.Add(new OracleParameter(":EXECUTION_DAY",pCondtion.ExecutionDay));
		}

		if (!string.IsNullOrEmpty(pCondtion.InvestigateTargetSeq)) {
			SysPrograms.SqlAppendWhere(":INVESTIGATE_TARGET_SEQ = P.INVESTIGATE_TARGET_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":INVESTIGATE_TARGET_SEQ",pCondtion.InvestigateTargetSeq));
		}

		return oOracleParameterList.ToArray();
	}

	public bool IsExistInvestigateTarget(InvestigateTargetSeekCondition pCondition,string pEntrantUserSeq,string pEntrantUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter>oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	COUNT(IVT.USER_SEQ)	AS ROW_COUNT				");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_TARGET IVT					,	");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_ENTRANT IVE						");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	IVT.SITE_CD			= IVE.SITE_CD			AND ");
		oSqlBuilder.AppendLine("	IVT.EXECUTION_DAY	= IVE.EXECUTION_DAY		AND ");

		oSqlBuilder.AppendLine("	IVT.SITE_CD			= :SITE_CD				AND ");
		oSqlBuilder.AppendLine("	IVT.EXECUTION_DAY	= :EXECUTION_DAY		AND ");
		oSqlBuilder.AppendLine("	IVT.USER_SEQ		= :USER_SEQ				AND ");
		oSqlBuilder.AppendLine("	IVT.USER_CHAR_NO	= :USER_CHAR_NO			AND ");

		oSqlBuilder.AppendLine("	IVE.USER_SEQ		= :ENTRANT_USER_SEQ		AND ");
		oSqlBuilder.AppendLine("	IVE.USER_CHAR_NO	= :ENTRANT_USER_CHAR_NO		");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":EXECUTION_DAY",pCondition.ExecutionDay));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":ENTRANT_USER_SEQ",pEntrantUserSeq));
		oParamList.Add(new OracleParameter(":ENTRANT_USER_CHAR_NO",pEntrantUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return (int.Parse(oDataSet.Tables[0].Rows[0]["ROW_COUNT"].ToString()) > 0);

	}

	protected void AppendWantedHint(DataSet pDS) {

		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	CAT.CAST_ATTR_TYPE_NM									,	");
		oSqlBuilder.AppendLine("	CAT.ITEM_NO												,	");
		oSqlBuilder.AppendLine("	CASE														");
		oSqlBuilder.AppendLine("		WHEN CAT.INPUT_TYPE = '1'								");
		oSqlBuilder.AppendLine("		THEN CAV.CAST_ATTR_INPUT_VALUE							");
		oSqlBuilder.AppendLine("		ELSE CAV.CAST_ATTR_NM									");
		oSqlBuilder.AppendLine("	END AS DISPLAY_VALUE										");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_WANTED_HINT WH										,	");
		oSqlBuilder.AppendLine("	T_CAST_ATTR_TYPE CAT									,	");
		oSqlBuilder.AppendLine("	VW_CAST_ATTR_VALUE02 CAV									");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	WH.SITE_CD				= :SITE_CD						AND ");
		oSqlBuilder.AppendLine("	WH.DAY_CD				= :DAY_CD						AND ");
		oSqlBuilder.AppendLine("	CAV.SITE_CD				= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	CAV.USER_SEQ			= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	CAV.USER_CHAR_NO		= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	WH.SITE_CD				= CAT.SITE_CD					AND ");
		oSqlBuilder.AppendLine("	WH.CAST_ATTR_TYPE_SEQ	= CAT.CAST_ATTR_TYPE_SEQ		AND	");
		oSqlBuilder.AppendLine("	WH.SITE_CD				= CAV.SITE_CD				(+)	AND	");
		oSqlBuilder.AppendLine("	WH.CAST_ATTR_TYPE_SEQ	= CAV.CAST_ATTR_TYPE_SEQ	(+)		");
		oSqlBuilder.AppendLine("ORDER BY WH.PRIORITY											");

		string sExecSql = oSqlBuilder.ToString();

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_WANTED_HINT;i++) {
			col = new DataColumn(string.Format("INVESTIGATE_HINT_TYPE{0}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
			col = new DataColumn(string.Format("INVESTIGATE_HINT_DISPLAY_VALUE{0}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			oParamList = new List<OracleParameter>();
			
			oParamList.Add(new OracleParameter(":SITE_CD",dr["SITE_CD"].ToString()));
			oParamList.Add(new OracleParameter(":DAY_CD",int.Parse(DateTime.Parse(dr["EXECUTION_DAY"].ToString()).DayOfWeek.ToString("d")) + 1));
			oParamList.Add(new OracleParameter(":USER_SEQ",dr["USER_SEQ"].ToString()));
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",dr["USER_CHAR_NO"].ToString()));
			DataSet dsSub = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
			
			int iHintCnt = 1;
			foreach (DataRow drSub in dsSub.Tables[0].Rows) {
				dr[string.Format("INVESTIGATE_HINT_TYPE{0}",iHintCnt)] = drSub["CAST_ATTR_TYPE_NM"].ToString();
				dr[string.Format("INVESTIGATE_HINT_DISPLAY_VALUE{0}",iHintCnt)] = drSub["DISPLAY_VALUE"].ToString();
				iHintCnt += 1;
			}
		}
	}
}