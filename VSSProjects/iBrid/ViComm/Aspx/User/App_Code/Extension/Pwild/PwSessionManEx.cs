﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ViComm.Extension.Pwild;
using ViComm;
using MobileLib;
using iBridCommLib;

/// <summary>
/// PwSessionMan の概要の説明です

/// </summary>
partial class SessionMan {

	protected DataSet CreateListDataSetEx(HttpRequest pRequest,ulong pMode,out int pCurrentPage,out int pRowCountOfPage,out int pMaxPage,out string pLoginId) {

		DataSet ds = null;

        pLoginId = iBridUtil.GetStringValue(pRequest.QueryString["loginid"]);

		if (int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["pageno"]), out pCurrentPage) == false) {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]), out pCurrentPage);

			if (pCurrentPage == 0) {
				pCurrentPage = 1;
			}
		}

		if (iBridUtil.GetStringValue(pRequest.QueryString["list"]).Equals("1")) {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["lrows"]), out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileManListCount;
			}
		} else {
			int.TryParse(iBridUtil.GetStringValue(pRequest.QueryString["drows"]), out pRowCountOfPage);
			if (pRowCountOfPage == 0) {
				pRowCountOfPage = site.mobileManDetailListCount;
			}
		}

		pMaxPage = 1;

		switch (pMode) {
			case PwViCommConst.INQUIRY_GAME_ITEM_SHOP_LIST: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.StageLevel = userMan.gameCharacter.stageLevel.ToString();
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				if (
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
				) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (oCondition.PresentFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemGetCd)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_VIEW: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SHOP_VIEW: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SHOP_PRESENT: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition(pRequest.QueryString);
				pRowCountOfPage = 5;
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.StageLevel = userMan.gameCharacter.stageLevel.ToString();
				oCondition.PresentFlag = ViCommConst.FLAG_ON_STR;
				oCondition.GameItemCategoryGroupType = PwViCommConst.GameItemCatregoryGroup.EQUIP;
				if (String.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItem oGameItem = new GameItem()) {
					pMaxPage = oGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_LIST: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.Level = this.userMan.gameCharacter.gameCharacterLevel.ToString();
				oCondition.SexCd = this.sexCd;
				oCondition.ExceptSelf = ViCommConst.FLAG_ON_STR;
				oCondition.ExceptBattled = ViCommConst.FLAG_ON_STR;
				oCondition.Random = ViCommConst.FLAG_ON_STR;
				oCondition.NotTutorialFlg = ViCommConst.FLAG_ON_STR;
				if (oCondition.LevelRange.Equals("")) {
					oCondition.LevelRange = PwViCommConst.GameCharSeek.LEVEL_RANGE;
				}
				using (GameCharacterBattle oGameCharacter = new GameCharacterBattle()) {
					pMaxPage = oGameCharacter.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacter.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_VIEW: {
				pRowCountOfPage = 1;
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				if(string.IsNullOrEmpty(userMan.gameCharacter.tutorialStatus)) {
					oCondition.NotTutorialFlg = ViCommConst.FLAG_ON_STR;
				}
				
				using (GameCharacterBattle oGameCharacter = new GameCharacterBattle()) {
					pMaxPage = oGameCharacter.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacter.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}

			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_COMMUNICATION_VIEW: {
				pRowCountOfPage = 1;
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				using (GameCharacterBattle oGameCharacter = new GameCharacterBattle()) {
					pMaxPage = oGameCharacter.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacter.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			
			case PwViCommConst.INQUIRY_GAME_TREASURE_ATTR: {
				ManTreasureAttrSeekCondition oCondition = new ManTreasureAttrSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;

				if (oCondition.DisplayDay.Equals(string.Empty)) {
					oCondition.DisplayDay = DateTime.Now.ToString("yyyy/MM/dd");
				}

				using (ManTreasureAttr oManTreasureAttr = new ManTreasureAttr()) {
					pMaxPage = oManTreasureAttr.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTreasureAttr.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}

				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_DATE_PLAN: {
				DatePlanSeekCondition oCondition = new DatePlanSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.StageLevel = this.userMan.gameCharacter.stageLevel.ToString();
				oCondition.ChargeFlag = ViCommConst.FLAG_OFF_STR;
				using (DatePlan oDatePlan = new DatePlan()) {
					pMaxPage = oDatePlan.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oDatePlan.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_DATE_PLAN_CHARGE: {
				DatePlanSeekCondition oCondition = new DatePlanSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.StageLevel = this.userMan.gameCharacter.stageLevel.ToString();
				oCondition.ChargeFlag = ViCommConst.FLAG_ON_STR;
				using (DatePlan oDatePlan = new DatePlan()) {
					pMaxPage = oDatePlan.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oDatePlan.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE: {
				ManTreasureSeekCondition oCondition = new ManTreasureSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				if (oCondition.DisplayDay.Equals(string.Empty)) {
					oCondition.DisplayDay = DateTime.Now.ToString("yyyy/MM/dd");
				}
				oCondition.ExceptTutorialNotPossessionFlag = ViCommConst.FLAG_ON_STR;
				
				using (ManTreasure oManTreasure = new ManTreasure()) {
					pMaxPage = oManTreasure.GetPageCount(oCondition, pRowCountOfPage, out totalRowCount);
					ds = oManTreasure.GetPageCollection(oCondition, pCurrentPage, pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE_VIEW: {
				pRowCountOfPage = 1;
				ManTreasureSeekCondition oCondition = new ManTreasureSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				using (ManTreasure oManTreasure = new ManTreasure()) {
					pMaxPage = 1;
					ds = oManTreasure.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_STAGE: {
				pRowCountOfPage = 1;
				StageSeekCondition oCondition = new StageSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SexCd = this.sexCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.StageLevel = this.userMan.gameCharacter.stageLevel.ToString();
				if (!string.IsNullOrEmpty(pRequest.QueryString["stage_group_type"])) {
					using (Stage oStage = new Stage()) {
						pMaxPage = 1;
						ds = oStage.GetOneByStageGroupType(oCondition);
					}
				} else if (!string.IsNullOrEmpty(pRequest.QueryString["stage_seq"])) {
					using (Stage oStage = new Stage()) {
						pMaxPage = 1;
						ds = oStage.GetOneByStageSeq(oCondition);
					}
				} else {
					oCondition.StageSeq = this.userMan.gameCharacter.stageSeq;
					using (Stage oStage = new Stage()) {
						pMaxPage = 1;
						ds = oStage.GetOneByStageSeq(oCondition);
					}
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FAVORITE_LIST: {
				CastGameCharacterFavoriteSeekCondition oCondition = new CastGameCharacterFavoriteSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFavoriteSort.GAME_FAVORITE_REGIST_DATE_DESC;
				}
				using (CastGameCharacterFavorite oCastGameCharacterFavorite = new CastGameCharacterFavorite()) {
					pMaxPage = oCastGameCharacterFavorite.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFavorite.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FAVORITE_PRESENT: {
				CastGameCharacterFavoriteSeekCondition oCondition = new CastGameCharacterFavoriteSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFavoriteSort.FRIENDLY_RANK_ASC;
				}
				using (CastGameCharacterFavorite oCastGameCharacterFavorite = new CastGameCharacterFavorite()) {
					pMaxPage = oCastGameCharacterFavorite.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFavorite.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FAVORITE_DATE_LIST: {
				CastGameCharacterFavoriteSeekCondition oCondition = new CastGameCharacterFavoriteSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SiteUseStatus = ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFavoriteSort.LAST_LOGIN_DATE_DESC;
				}
				using (CastGameCharacterFavorite oCastGameCharacterFavorite = new CastGameCharacterFavorite()) {
					pMaxPage = oCastGameCharacterFavorite.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFavorite.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_MEMBER_LIST: {
				CastGameCharacterFellowMemberSeekCondition oCondition = new CastGameCharacterFellowMemberSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.FellowApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFellowMemberSort.FRIENDLY_RANK_ASC;
				}
				using (CastGameCharacterFellowMember oCastGameCharacterFellowMember = new CastGameCharacterFellowMember()) {
					pMaxPage = oCastGameCharacterFellowMember.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFellowMember.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_TREASURE_LIST: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.Random = ViCommConst.FLAG_ON_STR;
				using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
					ds = oGameCharacterBattle.GetPageCollectionTreasure(oCondition,pCurrentPage,pRowCountOfPage);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_LOSE_LIST: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				if(String.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameCharacterBattleLoseSort.LAST_LOSE_DATE;
				}
				using(GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
					pMaxPage = oGameCharacterBattle.GetPageCountLose(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameCharacterBattle.GetPageCollectionLose(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_LIST: {
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_ATTACK;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					pMaxPage = oPossessionGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_PARTNER_LIST: {
				pRowCountOfPage = PwViCommConst.PARTNER_POSSESSION_ITEM_PER_PAGE;
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_ATTACK;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					pMaxPage = oPossessionGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_LIST: {
				CastGameCharacterFellowSeekCondition oCondition = new CastGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFellowSort.APPLICATION_PERMIT_DATE_DESC;
				}
				using (CastGameCharacterFellow oCastGameCharacterFellow = new CastGameCharacterFellow()) {
					pMaxPage = oCastGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_DATE_FELLOW_LIST: {
				CastGameCharacterFellowSeekCondition oCondition = new CastGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFellowSort.LAST_LOGIN_DATE_DESC;
				}
				using (CastGameCharacterFellow oCastGameCharacterFellow = new CastGameCharacterFellow()) {
					pMaxPage = oCastGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_APPLICATION_LIST: {
				CastGameCharacterFellowSeekCondition oCondition = new CastGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.APPLICATION;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFellowSort.APPLICATION_DATE_DESC;
				}
				using (CastGameCharacterFellow oCastGameCharacterFellow = new CastGameCharacterFellow()) {
					pMaxPage = oCastGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_HAS_APPLICATION_LIST: {
				CastGameCharacterFellowSeekCondition oCondition = new CastGameCharacterFellowSeekCondition(pRequest.Params);
				oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_APPLICATION_LIST;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFellowSort.APPLICATION_DATE_DESC;
				}
				using (CastGameCharacterFellow oCastGameCharacterFellow = new CastGameCharacterFellow()) {
					pMaxPage = oCastGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE_BATTLE_LIST: {
				PossessionManTreasureSeekCondition oCondition = new PossessionManTreasureSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = this.userMan.userSeq;
				oCondition.SelfUserCharNo = this.userMan.userCharNo;
				oCondition.ExceptSelfFlag = ViCommConst.FLAG_ON_STR;
				oCondition.CanRobFlag = ViCommConst.FLAG_ON_STR;
				oCondition.TutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.TreasureTutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.TresaureTutorialMissionFlag = ViCommConst.FLAG_OFF_STR;
				
				using (PossessionManTreasure oPossessionManTreasure = new PossessionManTreasure()) {
					pMaxPage = oPossessionManTreasure.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionManTreasure.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW: {
				pRowCountOfPage = 1;
				CastGameCharacterViewSeekCondition oCondition = new CastGameCharacterViewSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				using (CastGameCharacterView oCastGameCharacterView = new CastGameCharacterView()) {
					pMaxPage = oCastGameCharacterView.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterView.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TREASURE_PARTNER: {
				ManTreasureSeekCondition oCondition = new ManTreasureSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;

				using (ManTreasure oManTreasure = new ManTreasure()) {
					pMaxPage = oManTreasure.GetPageCount(oCondition, pRowCountOfPage, out totalRowCount);
					ds = oManTreasure.GetPageCollection(oCondition, pCurrentPage, pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_COMP_CALENDAR: {
				pRowCountOfPage = PwViCommConst.GameCompCalendarRowCountOfPage;
				bool bCurrentPageMax = false;
				DateTime dtGameStartDate;
				using (SocialGame oSocialGame = new SocialGame()) {
					dtGameStartDate = oSocialGame.GetGameStartDate(this.userMan.siteCd);
				}
				ManTreasureCompCalendarSeekCondition oCondition = new ManTreasureCompCalendarSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManTreasureCompCalendarSort.DISPLAY_DAY_ASC;
				}
				if (string.IsNullOrEmpty(oCondition.StartDisplayDay) || string.IsNullOrEmpty(oCondition.EndDisplayDay)) {
					oCondition.StartDisplayDay = DateTime.Now.ToString("yyyy/MM/01");
					oCondition.EndDisplayDay = string.Format("{0:d4}/{1:d2}/{2:d2}",DateTime.Now.Year,DateTime.Now.Month,DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month));
					bCurrentPageMax = true;
				}
				if (DateTime.Parse(oCondition.StartDisplayDay) < dtGameStartDate) {
					oCondition.StartDisplayDay = dtGameStartDate.ToString("yyyy/MM/dd");
				}
				if (DateTime.Parse(oCondition.EndDisplayDay) > DateTime.Now) {
					oCondition.EndDisplayDay = DateTime.Now.ToString("yyyy/MM/dd");
				}
				TimeSpan tsDays = DateTime.Parse(oCondition.EndDisplayDay) - DateTime.Parse(oCondition.StartDisplayDay);
				decimal dTotalRowCount = (tsDays.Days > 0) ? tsDays.Days+1 : 1;
				pMaxPage = (int)Math.Ceiling(dTotalRowCount / pRowCountOfPage);
				if (bCurrentPageMax == true) {
					pCurrentPage = pMaxPage;
				}
				using (ManTreasureCompCalendar oManTreasureCompCalendar = new ManTreasureCompCalendar()) {
					ds = oManTreasureCompCalendar.GetPageCollectionAppendEmpty(oCondition,pCurrentPage,pRowCountOfPage);
				}
				System.Text.Encoding enc = System.Text.Encoding.GetEncoding("Shift_JIS");
				oCondition.StartDisplayDay = HttpUtility.UrlEncode(oCondition.StartDisplayDay,enc);
				oCondition.EndDisplayDay = HttpUtility.UrlEncode(oCondition.EndDisplayDay,enc);
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOGIN_BONUS_MONTHLY_LIST: {
				GameLoginBonusMonthlySeekCondition oCondition = new GameLoginBonusMonthlySeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				DateTime dt = DateTime.Now;
				oCondition.Month = string.Format("{0}/{1}",dt.ToString("yyyy"),dt.ToString("MM"));
				oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;

				using (GameLoginBonusMonthly oGameLoginBonusMonthly = new GameLoginBonusMonthly()) {
					pMaxPage = oGameLoginBonusMonthly.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameLoginBonusMonthly.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SALE_LIST: {
				GameItemSaleSeekCondition oCondition = new GameItemSaleSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.StageLevel = userMan.gameCharacter.stageLevel.ToString();
				oCondition.DateNowFlag = ViCommConst.FLAG_ON_STR;
				oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				if (
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
				) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (oCondition.PresentFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemGetCd)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItemSale oGameItemSale = new GameItemSale()) {
					pMaxPage = oGameItemSale.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItemSale.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_SALE_NOTICE_LIST: {
				GameItemSaleSeekCondition oCondition = new GameItemSaleSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.StageLevel = userMan.gameCharacter.stageLevel.ToString();
				oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.NoticeDispFlag = ViCommConst.FLAG_ON_STR;
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.PresentFlag)) {
					oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
				}
				if (
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) &&
					!oCondition.GameItemCategoryType.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)
				) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (oCondition.PresentFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.GameItemGetCd)) {
					oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItemSale oGameItemSale = new GameItemSale()) {
					pMaxPage = oGameItemSale.GetPageCountNotice(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItemSale.GetPageCollectionNotice(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FIND_LIST: {
				CastGameCharacterFindSeekCondition oCondition = new CastGameCharacterFindSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				if (oCondition.NewCast.Equals(ViCommConst.FLAG_ON_STR)) {
					oCondition.NewCastDay = site.newFaceDays.ToString();
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFindSort.LAST_LOGIN_DATE_DESC;
				}
				using (CastGameCharacterFind oCastGameCharacterFind = new CastGameCharacterFind()) {
					pMaxPage = oCastGameCharacterFind.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFind.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_MOVIE_LIST: {
				CastGameCharacterFindSeekCondition oCondition = new CastGameCharacterFindSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				if (oCondition.NewCast.Equals(ViCommConst.FLAG_ON_STR)) {
					oCondition.NewCastDay = site.newFaceDays.ToString();
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFindSort.FRIENDLY_RANK_ASC;
				}
				using (CastGameCharacterFind oCastGameCharacterFind = new CastGameCharacterFind()) {
					pMaxPage = oCastGameCharacterFind.GetPageCountExistMovie(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFind.GetPageCollectionExistMovie(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_COMMUNICATION_PARTNER_HUG: {
				CastGameCommunicationPartnerHugSeekCondition oCondition = new CastGameCommunicationPartnerHugSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				using (CastGameCommunicationPartnerHug oCastGameCommunicationPartnerHug = new CastGameCommunicationPartnerHug()) {
					pMaxPage = oCastGameCommunicationPartnerHug.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCommunicationPartnerHug.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_WANTED_ITEM: {
				WantedItemSeekCondition oCondition = new WantedItemSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.SexCd = this.sexCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				using (WantedItem oWantedItem = new WantedItem()) {
					pMaxPage = 1;
					ds = oWantedItem.GetListWantedItem(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_ITEM_WANTED: {
				GameItemWantedSeekCondition oCondition = new GameItemWantedSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.SexCd = this.sexCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.StageLevel = this.userMan.gameCharacter.stageLevel.ToString();
				if (string.IsNullOrEmpty(oCondition.GameItemCategoryType)) {
					oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY;
				}
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (GameItemWanted oGameItemWanted = new GameItemWanted()) {
					pMaxPage = oGameItemWanted.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameItemWanted.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_WANTED_ITEM_PARTNER: {
				WantedItemSeekCondition oCondition = new WantedItemSeekCondition(pRequest.Params);
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.PartnerFlg = ViCommConst.FLAG_ON_STR;
				using (WantedItem oWantedItem = new WantedItem()) {
					pMaxPage = 1;
					ds = oWantedItem.GetListWantedItemPartner(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_RECOVERY_FORCE_FULL: {
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE;
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					ds = oPossessionGameItem.GetPageCollection(oCondition,1,1);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_STAGE_BOSS_BATTLE: {
				StageSeekCondition oCondition = new StageSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.StageClearFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.NextStageMovedFlag = ViCommConst.FLAG_OFF_STR;
				using (Stage oGameStage = new Stage()) {
					ds = oGameStage.GetOneBossData(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_STAGE_BOSS_BATTLE_RESULT: {
				StageSeekCondition oCondition = new StageSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = this.sexCd;
				using (Stage oStage = new Stage()) {
					ds = oStage.GetOneStageClearData(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_MOVIE_LIST: {
				CastGameCharacterMovieSeekCondition oCondition = new CastGameCharacterMovieSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterMovieSort.UPLOAD_DATE_DESC;
				}
				using (CastGameCharacterMovie oCastGameCharacterMovie = new CastGameCharacterMovie()) {
					pMaxPage = oCastGameCharacterMovie.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterMovie.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_MOVIE_VIEW: {
				pRowCountOfPage = 1;
				pCurrentPage = 1;
				CastGameCharacterMovieSeekCondition oCondition = new CastGameCharacterMovieSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				using (CastGameCharacterMovie oCastGameCharacterMovie = new CastGameCharacterMovie()) {
					pMaxPage = oCastGameCharacterMovie.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterMovie.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				pLoginId = string.Empty;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_MOVIE_PLAY: {
				pRowCountOfPage = 1;
				using (CastMovie oCastMovie = new CastMovie()) {
					ds = oCastMovie.GetPageCollectionBySeq(iBridUtil.GetStringValue(pRequest.QueryString["objseq"]),true);
				}
				pLoginId = string.Empty;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_TOWN: {
				TownSeekCondition oCondition = new TownSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.SexCd = this.sexCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				using (Town oTown = new Town()) {
					pMaxPage = oTown.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oTown.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_START:
			case PwViCommConst.INQUIRY_GAME_BATTLE_START_PARTY: {
				GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				
				if(!string.IsNullOrEmpty(this.userMan.gameCharacter.tutorialStatus)) {
					oCondition.TutorialFlag = ViCommConst.FLAG_ON_STR;
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_ON_STR;
				} else {
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
				}

				using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
					ds = oGameCharacterBattle.GetOneBattleStart(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_START_SUPPORT: {
				GameCharacterBattleSupportSeekCondition oCondition = new GameCharacterBattleSupportSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SupportRequestFlag = ViCommConst.FLAG_ON_STR;
				oCondition.SexCd = ViCommConst.OPERATOR;

				using (GameCharacterBattleSupport oGameCharacterBattleSupport = new GameCharacterBattleSupport()) {
					ds = oGameCharacterBattleSupport.GetOne(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_WIN: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SINGLE;
				oCondition.AttackWinFlag = ViCommConst.FLAG_ON_STR;
				
				using(BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOne(oCondition);
				}
				
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_WIN_PARTY: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.PARTY;
				oCondition.AttackWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOne(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_WIN_SUPPORT: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SUPPORT;
				oCondition.AttackWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneSupport(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SINGLE;
				oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOne(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE_PARTY: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.PARTY;
				oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOne(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE_SUPPORT: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.BattleType = PwViCommConst.GameBattleType.SUPPORT;
				oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

				using (BattleLog oBattleLog = new BattleLog()) {
					ds = oBattleLog.GetOneSupport(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_SUPPORT_REQUEST: {
				CastSupportRequestSeekCondition oCondition = new CastSupportRequestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;

				using (CastSupportRequest oCastSupportRequest = new CastSupportRequest()) {
					ds = oCastSupportRequest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_PRESENT_LIST: {
				CastGameCharacterFellowSeekCondition oCondition = new CastGameCharacterFellowSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFellowSort.FRIENDLY_RANK_ASC;
				}
				using (CastGameCharacterFellow oCastGameCharacterFellow = new CastGameCharacterFellow()) {
					pMaxPage = oCastGameCharacterFellow.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFellow.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY: {
				LotterySeekCondition oCondition = new LotterySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.LotteryPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.RecentLotteryFlag = ViCommConst.FLAG_ON_STR;
				
				using(Lottery oLottery = new Lottery()) {
					ds = oLottery.GetOne(oCondition);
				}
				
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_NOT_COMP: {
				LotterySeekCondition oCondition = new LotterySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				using (Lottery oLottery = new Lottery()) {
					ds = oLottery.GetOneNotComp(oCondition);
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT: {
				LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				using(LotteryGetItem oLotteryGameItem = new LotteryGetItem()) {
					ds = oLotteryGameItem.GetOneLotteryGetItem(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT_NOT_COMP: {
				LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				using(LotteryGetItem oLotteryGameItem = new LotteryGetItem()) {
					ds = oLotteryGameItem.GetOneLotteryGetItemNotComp(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_LIST: {
				LotterySeekCondition oCondition = new LotterySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.LotteryPublishFlag = ViCommConst.FLAG_ON_STR;
				oCondition.RecentLotteryFlag = ViCommConst.FLAG_OFF_STR;
				using(Lottery oLottery = new Lottery()) {
					ds = oLottery.GetOne(oCondition);
				}
				break;
			}
			
			case PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_PRESENT: {
				PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition(pRequest.QueryString);
				pRowCountOfPage = 5;
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.PresentFlag = ViCommConst.FLAG_ON_STR;
				if (String.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_HIGH;
				}
				using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
					pMaxPage = oPossessionGameItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionGameItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_TODAY: {
				CastGameCharacterFindSeekCondition oCondition = new CastGameCharacterFindSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.userMan.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.CastTodayFlag = ViCommConst.FLAG_ON_STR;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastGameCharacterFindSort.RANDOM;
				}
				using (CastGameCharacterFind oCastGameCharacterFind = new CastGameCharacterFind()) {
					pMaxPage = oCastGameCharacterFind.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterFind.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_VIEW: {
				LotteryGetItemSeekCondition oCondition = new LotteryGetItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.GameItemPublishFlag = ViCommConst.FLAG_ON_STR;

				using (LotteryGetItem oLotteryGameItem = new LotteryGetItem()) {
					if (oCondition.CompItemFlg == ViCommConst.FLAG_ON_STR) {
						ds = oLotteryGameItem.GetOneLotteryCompGetItem(oCondition);
					} else {
						ds = oLotteryGameItem.GetOneLotteryGetItem(oCondition);
					}
				}
					
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY: {
					CastGameCharacterPresentHistorySeekCondition oCondition = new CastGameCharacterPresentHistorySeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = this.userMan.userSeq;
				oCondition.SelfUserCharNo = this.userMan.userCharNo;
				using (CastGameCharacterPresentHistory oCastGameCharacterPresentHistory = new CastGameCharacterPresentHistory()) {
					pMaxPage = oCastGameCharacterPresentHistory.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastGameCharacterPresentHistory.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_FELLOW_LOG: {
					NewsSeekCondition oCondition = new NewsSeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;

				using (News oNews = new News()) {
					pMaxPage = oNews.GetPageCountFellowLog(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oNews.GetPageCollectionFellowLog(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_FAVORITE_LOG: {
				NewsSeekCondition oCondition = new NewsSeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;

				using (News oNews = new News()) {
					pMaxPage = oNews.GetPageCountFavoriteLog(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oNews.GetPageCollectionFavoriteLog(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_BATTLE: {
				GameCharacterPerformanceSeekCondition oCondition = new GameCharacterPerformanceSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = this.sexCd;
				oCondition.RankType = PwViCommConst.RankType.BATTLE_WIN;
				using (GameCharacterPerformance oGameCharacterPerformance = new GameCharacterPerformance()) {
					pMaxPage = oGameCharacterPerformance.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					if (oCondition.SelfRank == ViCommConst.FLAG_ON_STR) {
						oCondition.UserSeq = this.userMan.userSeq;
						oCondition.UserCharNo = this.userMan.userCharNo;
						int iMyPage = oGameCharacterPerformance.GetMyPageCount(oCondition,pRowCountOfPage);

						if (iMyPage > 0) {
							pCurrentPage = iMyPage;
						} else {
							pCurrentPage = pMaxPage;
						}
					}
					ds = oGameCharacterPerformance.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					oCondition.SelfRank = null;
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CHARACTER_RANK_COMP: {
				GameManTreasureCompleteLogSeekCondition oCondition = new GameManTreasureCompleteLogSeekCondition(pRequest.Params);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = this.site.siteCd;
				using (GameManTreasureCompleteLog oGameManTreasureCompleteLog = new GameManTreasureCompleteLog()) {
					pMaxPage = oGameManTreasureCompleteLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					if (oCondition.SelfRank == ViCommConst.FLAG_ON_STR) {
						oCondition.UserSeq = this.userMan.userSeq;
						oCondition.UserCharNo = this.userMan.userCharNo;
						int iMyPage = oGameManTreasureCompleteLog.GetMyPageCount(oCondition,pRowCountOfPage);

						if (iMyPage > 0) {
							pCurrentPage = iMyPage;
						} else {
							pCurrentPage = pMaxPage;
						}
					}
					ds = oGameManTreasureCompleteLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					oCondition.SelfRank = null;
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CLEAR_MISSION_RECOVERY_FORCE: {
				GameItemSeekCondition oCondition = new GameItemSeekCondition();
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				oCondition.GameItemCategoryGroupType = PwViCommConst.GameItemCatregoryGroup.RESTORE;
				oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
			
				using (GameItem oGameItem = new GameItem()) {
					ds = oGameItem.GetPageCollection(oCondition,1,1);
				}
				
				break;
			}
			case PwViCommConst.INQUIRY_GAME_BATTLE_LOG: {
				BattleLogSeekCondition oCondition = new BattleLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				
				if(string.IsNullOrEmpty(oCondition.BattleType)) {
					oCondition.BattleType = PwViCommConst.GameBattleType.SINGLE;
				}

				using (BattleLog oBattleLog = new BattleLog()) {
					pMaxPage = oBattleLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBattleLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_COMMUNICATION: {
				GameCommunicationSeekCondition oCondition = new GameCommunicationSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;
				
				using (GameCommunication oGameCommunication = new GameCommunication()) {
					ds = oGameCommunication.GetOne(oCondition);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_GAME_INTRO_LOG: {
				GameIntroLogSeekCondition oCondition = new GameIntroLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.GetFlag = ViCommConst.FLAG_OFF_STR;
				using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
					pMaxPage = oGameIntroLog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oGameIntroLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_CLEAR_TOWN_USED_ITEM_SHORTAGE_LIST: {
				ClearTownUsedItemSeekCondition oCondition = new ClearTownUsedItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = this.sexCd;

				using (ClearTownUsedItem oClearTownUsedItem = new ClearTownUsedItem()) {
					ds = oClearTownUsedItem.GetPageCollectionUsedItemShortage(oCondition);
				}
				this.seekConditionEx = oCondition;

				break;
			}
			case PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY: {
				ObjReviewHistorySeekCondition oCondition = new ObjReviewHistorySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CommentFlag = ViCommConst.FLAG_ON_STR;
				using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
					pMaxPage = oObjReviewHistory.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oObjReviewHistory.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_REQUEST: {
				PwRequestSeekCondition oCondition = new PwRequestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.MAN;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.PublicStatus = PwViCommConst.RequestPublicStatus.PUBLIC;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.RequestSort.GOOD_COUNT_DESC;
				}
				using (PwRequest oPwRequest = new PwRequest()) {
					pMaxPage = oPwRequest.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPwRequest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_REQUEST_VALUE: {
				RequestValueSeekCondition oCondition = new RequestValueSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SexCd = ViCommConst.MAN;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				using (RequestValue oRequestValue = new RequestValue()) {
					pMaxPage = oRequestValue.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oRequestValue.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_POSSESSION_MAN_TREASURE: {
				PossessionManTreasureSeekCondition oCondition = new PossessionManTreasureSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.ContainTutorialTreasureFlag = ViCommConst.FLAG_ON_STR;
				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.ManTreasureGetSort.UPDATE_DATE_DESC;
				}
				using (PossessionManTreasure oPossessionManTreasure = new PossessionManTreasure()) {
					pMaxPage = oPossessionManTreasure.GetPageCount01(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPossessionManTreasure.GetPageCollection01(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_LEVEL_QUEST: {
				LevelQuestSeekCondition oCondition = new LevelQuestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = ViCommConst.MAN;

				using (LevelQuest oLevelQuest = new LevelQuest()) {
					ds = oLevelQuest.GetOne(oCondition);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_OTHER_QUEST: {
				OtherQuestSeekCondition oCondition = new OtherQuestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = ViCommConst.MAN;

				using (OtherQuest oOtherQuest = new OtherQuest()) {
					pMaxPage = oOtherQuest.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oOtherQuest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_REGIST_QUEST_ENTRY: {
				string sQuestType = iBridUtil.GetStringValue(pRequest.QueryString["quest_type"]);

				if (sQuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
					LittleQuestSeekCondition oCondition = new LittleQuestSeekCondition(pRequest.QueryString);
					oCondition.SiteCd = this.site.siteCd;
					oCondition.UserSeq = this.userMan.userSeq;
					oCondition.UserCharNo = this.userMan.userCharNo;
					oCondition.SexCd = ViCommConst.MAN;

					using (LittleQuest oLittleQuest = new LittleQuest()) {
						ds = oLittleQuest.GetPageCollection(oCondition,1,1);
					}
					this.seekConditionEx = oCondition;
				} else if (sQuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
					LevelQuestSeekCondition oCondition = new LevelQuestSeekCondition(pRequest.QueryString);
					oCondition.SiteCd = this.site.siteCd;
					oCondition.UserSeq = this.userMan.userSeq;
					oCondition.UserCharNo = this.userMan.userCharNo;
					oCondition.SexCd = ViCommConst.MAN;

					using (LevelQuest oLevelQuest = new LevelQuest()) {
						ds = oLevelQuest.GetOne(oCondition);
					}
					this.seekConditionEx = oCondition;
				} else if (sQuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
					OtherQuestSeekCondition oCondition = new OtherQuestSeekCondition(pRequest.QueryString);
					oCondition.SiteCd = this.site.siteCd;
					oCondition.UserSeq = this.userMan.userSeq;
					oCondition.UserCharNo = this.userMan.userCharNo;
					oCondition.SexCd = ViCommConst.MAN;

					using (OtherQuest oOtherQuest = new OtherQuest()) {
						pMaxPage = oOtherQuest.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
						ds = oOtherQuest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					}
					this.seekConditionEx = oCondition;
				}

				break;
			}
			case PwViCommConst.INQUIRY_GAME_QUEST_REWARD_GET: {
				QuestEntryLogSeekCondition oCondition = new QuestEntryLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = ViCommConst.MAN;
				oCondition.GetRewardFlag = ViCommConst.FLAG_OFF_STR;

				using (QuestEntryLog oQuestEntryLog = new QuestEntryLog()) {
					ds = oQuestEntryLog.GetPageCollection(oCondition,1,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_GAME_POSSESSION_MAN_TREASURE_BATTLE: {
				PossessionManTreasureSeekCondition oCondition = new PossessionManTreasureSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.SelfUserSeq = this.userMan.userSeq;
				oCondition.SelfUserCharNo = this.userMan.userCharNo;
				oCondition.ExceptSelfFlag = ViCommConst.FLAG_ON_STR;
				oCondition.ExceptBattledFlag = ViCommConst.FLAG_ON_STR;
				oCondition.CanRobFlag = ViCommConst.FLAG_ON_STR;
				oCondition.Sort = PwViCommConst.ManTreasureGetSort.UPDATE_DATE_DESC;
				
				if(!string.IsNullOrEmpty(userMan.gameCharacter.tutorialStatus)) {
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_ON_STR;
					oCondition.TreasureTutorialBattleFlag = ViCommConst.FLAG_ON_STR;
				} else {
					oCondition.TutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
					oCondition.TreasureTutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
				}
				oCondition.TresaureTutorialMissionFlag = ViCommConst.FLAG_OFF_STR;

				if((this.userMan.gameCharacter.maxAttackPower + this.userMan.gameCharacter.maxDefencePower) > 0) {
					int iBattlePowerPercentFrom = 0;
					int iBattlePowerPercentTo = 0;

					using (SocialGame oSocialGame = new SocialGame()) {
						oSocialGame.GetBattlePowerPercent(this.site.siteCd,out iBattlePowerPercentFrom,out iBattlePowerPercentTo);
					}

					oCondition.MaxPowerFrom = ((this.userMan.gameCharacter.maxAttackPower + this.userMan.gameCharacter.maxDefencePower) * iBattlePowerPercentFrom / 100).ToString();
					oCondition.MaxPowerTo = ((this.userMan.gameCharacter.maxAttackPower + this.userMan.gameCharacter.maxDefencePower) * iBattlePowerPercentTo / 100).ToString();
				}

				using (PossessionManTreasure oPossessionManTreasure = new PossessionManTreasure()) {
					pMaxPage = oPossessionManTreasure.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);

					if (totalRowCount > 0) {
						ds = oPossessionManTreasure.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					} else {
						oCondition.MaxPowerFrom = null;
						oCondition.MaxPowerTo = null;

						pMaxPage = oPossessionManTreasure.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
						ds = oPossessionManTreasure.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					}
				}
				this.seekConditionEx = oCondition;
				
				break;
			}
			case PwViCommConst.INQUIRY_FAVORIT_GROUP: {
				FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.UserSeq = this.userMan.userSeq;
				oCondition.UserCharNo = this.userMan.userCharNo;
				oCondition.SexCd = ViCommConst.MAN;
				using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
					pMaxPage = oFavoritGroup.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
		case PwViCommConst.INQUIRY_BLOG_COMMENT: {
				BlogCommentSeekCondition oCondition = new BlogCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;

				if (string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["sort"]))) {
					oCondition.Sort = PwViCommConst.BlogCommentSort.CREATE_DATE_DESC;
				}
				
				using (BlogComment oBlogComment = new BlogComment()) {
					pMaxPage = oBlogComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBlogComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_SELF_MAN_TWEET: {
				ManTweetExSeekCondition oCondition = new ManTweetExSeekCondition();
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				if (currentAspx.Equals("ListSelfManTweet.aspx")) {
					oCondition.AppendLikeCount = true;
					oCondition.AppendCommentCount = true;
				} else if (currentAspx.Equals("DeleteManTweet.aspx")) {
					oCondition.ManTweetSeq = iBridUtil.GetStringValue(pRequest.QueryString["tweetseq"]);
				}
				using (ManTweetEx oManTweetEx = new ManTweetEx()) {
					pMaxPage = oManTweetEx.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTweetEx.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAN_TWEET_COMMENT: {
				ManTweetCommentSeekCondition oCondition = new ManTweetCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				using (ManTweetComment oManTweetComment = new ManTweetComment()) {
					pMaxPage = oManTweetComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTweetComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_TWEET: {
				CastTweetSeekCondition oCondition = new CastTweetSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;
				oCondition.SelfUserCharNo = userMan.userCharNo;
				oCondition.OnlyFanClubFlag = ViCommConst.FLAG_OFF_STR;
				using (CastTweet oCastTweet = new CastTweet()) {
					pMaxPage = oCastTweet.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastTweet.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_TWEET_COMMENT: {
				CastTweetCommentSeekCondition oCondition = new CastTweetCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				using (CastTweetComment oCastTweetComment = new CastTweetComment()) {
					pMaxPage = oCastTweetComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastTweetComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_FANCLUB_STATUS: {
				FanClubStatusSeekCondition oCondition = new FanClubStatusSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.ManUserSeq = userMan.userSeq;
				using (CastFanClubStatus oCastFanClubStatus = new CastFanClubStatus()) {
					pMaxPage = oCastFanClubStatus.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastFanClubStatus.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_NEWS: {
				CastNewsSeekCondition oCondition = new CastNewsSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.OnlyFanClubFlag = ViCommConst.FLAG_ON_STR;
				using (CastNews oCastNews = new CastNews()) {
					pMaxPage = oCastNews.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastNews.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_MAN_TWEET_LIKE: {
				ManTweetLikeSeekCondition oCondition = new ManTweetLikeSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				using (ManTweetLike oManTweetLike = new ManTweetLike()) {
					pMaxPage = oManTweetLike.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oManTweetLike.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YAKYUKEN_GAME: {
				YakyukenGameCastSeekCondition oCondition = new YakyukenGameCastSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				using (YakyukenGameCast oYakyukenGameCast = new YakyukenGameCast()) {
					pMaxPage = oYakyukenGameCast.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oYakyukenGameCast.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_YAKYUKEN_PIC_VIEW: {
				YakyukenPicCastSeekCondition oCondition = new YakyukenPicCastSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;

				using (YakyukenProgress oYakyukenProgress = new YakyukenProgress()) {
					DataSet oDataSet = oYakyukenProgress.GetOne(site.siteCd,userMan.userSeq,oCondition.YakyukenGameSeq);

					if (oDataSet.Tables[0].Rows.Count > 0) {
						string sClearYakyukenType = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CLEAR_YAKYUKEN_TYPE"]);
						string sLastWinYakyukenType = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["LAST_WIN_YAKYUKEN_TYPE"]);
						oCondition.MaxYakyukenType = sClearYakyukenType;

						if (string.IsNullOrEmpty(oCondition.YakyukenType)) {
							if (sLastWinYakyukenType.Equals("6")) {
								oCondition.YakyukenType = "1";
							} else {
								oCondition.YakyukenType = sLastWinYakyukenType;
							}
						}
					} else {
						oCondition.MaxYakyukenType = "1";
					}
				}
				using (YakyukenPicCast oYakyukenPicCast = new YakyukenPicCast()) {
					pMaxPage = 1;
					ds = oYakyukenPicCast.GetPageCollection(oCondition,1,1);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_PICKUP_OBJS_COMMENT: {
				PickupObjsCommentSeekCondition oCondition = new PickupObjsCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				using (PickupObjsComment oPickupObjsComment = new PickupObjsComment()) {
					pMaxPage = oPickupObjsComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPickupObjsComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_WANTED_CAUGHT_LOG: {
				WantedCaughtLogSeekCondition oCondition = new WantedCaughtLogSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;
				oCondition.SelfUserCharNo = userMan.userCharNo;
				
				if (string.IsNullOrEmpty(oCondition.ReportMonth)) {
					oCondition.ReportMonth = DateTime.Now.ToString("yyyyMM");
				}
				
				if (string.IsNullOrEmpty(oCondition.SheetNo)) {
					using (WantedCompSheetCnt oWantedCompSheetCnt = new WantedCompSheetCnt()) {
						int iSheetNo;
						int iCompleteCount = oWantedCompSheetCnt.GetCompleteSheetCount(site.siteCd,userMan.userSeq,userMan.userCharNo,oCondition.ReportMonth);
						
						if (iCompleteCount == 5) {
							iSheetNo = iCompleteCount;
						} else {
							iSheetNo = iCompleteCount + 1;
						}
						
						oCondition.SheetNo = iSheetNo.ToString();
					}
				}

				using (WantedCaughtLog oWantedCaughtLog = new WantedCaughtLog()) {
					ds = oWantedCaughtLog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					
					int iShortageRow = 6 - ds.Tables[0].Rows.Count;
					
					if (iShortageRow > 0) {
						for (int i = 0;i < iShortageRow;i++) {
							ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
						}
					}
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_EVENT: {
				EventSeekCondition oCondition = new EventSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				using (Event oEvent = new Event()) {
					pMaxPage = oEvent.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oEvent.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
		case PwViCommConst.INQUIRY_DRAFT_MAIL: {
				CastDraftMailSeekCondition oCondition = new CastDraftMailSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.TxUserSeq = userMan.userSeq;
				oCondition.TxUserCharNo = userMan.userCharNo;
				using (CastDraftMail oCastDraftMail = new CastDraftMail()) {
					ds = oCastDraftMail.GetPageCollection(oCondition,1,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY_PIC: {
				ObjReviewHistoryPicSeekCondition oCondition = new ObjReviewHistoryPicSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CommentFlag = ViCommConst.FLAG_ON_STR;
				using (ObjReviewHistoryPic oObjReviewHistoryPic = new ObjReviewHistoryPic()) {
					pMaxPage = oObjReviewHistoryPic.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oObjReviewHistoryPic.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY_MOVIE: {
				ObjReviewHistoryMovieSeekCondition oCondition = new ObjReviewHistoryMovieSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = this.site.siteCd;
				oCondition.CommentFlag = ViCommConst.FLAG_ON_STR;
				using (ObjReviewHistoryMovie oObjReviewHistoryMovie = new ObjReviewHistoryMovie()) {
					pMaxPage = oObjReviewHistoryMovie.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oObjReviewHistoryMovie.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_VOTE_RANK_CURRENT: {
				DataSet dsVoteTerm;


				VoteRankSeekCondition oCondition = new VoteRankSeekCondition(pRequest.QueryString);

				if (!string.IsNullOrEmpty(oCondition.VoteTermSeq)) {
					using (VoteTerm oVoteTerm = new VoteTerm()) {
						dsVoteTerm = oVoteTerm.GetOneBySeq(site.siteCd,oCondition.VoteTermSeq);
					}
				} else {
					using (VoteTerm oVoteTerm = new VoteTerm()) {
						dsVoteTerm = oVoteTerm.GetCurrent(site.siteCd);
					}
				}

				if (dsVoteTerm.Tables[0].Rows.Count >= 1) {
					oCondition.SiteCd = iBridUtil.GetStringValue(dsVoteTerm.Tables[0].Rows[0]["SITE_CD"]);
					oCondition.VoteTermSeq = iBridUtil.GetStringValue(dsVoteTerm.Tables[0].Rows[0]["VOTE_TERM_SEQ"]);
					oCondition.FixRankFlag = iBridUtil.GetStringValue(dsVoteTerm.Tables[0].Rows[0]["FIX_RANK_FLAG"]);

					using (VoteRank oVoteRank = new VoteRank()) {
						pMaxPage = oVoteRank.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
						ds = oVoteRank.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					}
					this.seekConditionEx = oCondition;
				}

				break;
			}
			case PwViCommConst.INQUIRY_VOTE_TERM_VOTABLE: {
				using (VoteTerm oVoteTerm = new VoteTerm()) {
					ds = oVoteTerm.GetVotable(site.siteCd);
				}
				break;
			}
			case PwViCommConst.INQUIRY_BBS_BINGO_TERM_LATEST: {
				using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
					ds = oBbsBingoTerm.GetLatest(site.siteCd);
				}
				break;
			}
			case PwViCommConst.INQUIRY_BBS_BINGO_LOG: {
				string sObjSeq = iBridUtil.GetStringValue(pRequest.QueryString["objseq"]);
				using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
					ds = oBbsBingoTerm.GetBbsBingoLogData(site.siteCd,userMan.userSeq,sObjSeq);
				}
				break;
			}
			case PwViCommConst.INQUIRY_BBS_BINGO_ENTRY_PRIZE: {
				BbsBingoEntrySeekCondition oCondition = new BbsBingoEntrySeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.PrizeFlag = ViCommConst.FLAG_ON_STR;

				if (string.IsNullOrEmpty(oCondition.TermSeq)) {
					using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
						oCondition.TermSeq = oBbsBingoTerm.GetLatestTermSeq(site.siteCd);
					}
				}

				using (BbsBingoEntry oBbsBingoEntry = new BbsBingoEntry()) {
					pMaxPage = oBbsBingoEntry.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBbsBingoEntry.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_ELECTION_ENTRY_CURRENT: {
				DataSet dsElectionPeriod;

				ElectionEntrySeekCondition oCondition = new ElectionEntrySeekCondition(pRequest.QueryString);

				using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
					dsElectionPeriod = oElectionPeriod.GetCurrent(site.siteCd);
				}

				if (dsElectionPeriod.Tables[0].Rows.Count >= 1) {
					oCondition.SiteCd = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["SITE_CD"]);
					oCondition.ElectionPeriodSeq = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["ELECTION_PERIOD_SEQ"]);
					oCondition.FixRankFlag = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["FIX_RANK_FLAG"]);
					
					if (string.IsNullOrEmpty(oCondition.ElectionPeriodStatus)) {
						DateTime oFirstPeriodEndDate = DateTime.Parse(dsElectionPeriod.Tables[0].Rows[0]["FIRST_PERIOD_END_DATE"].ToString());
						if (oFirstPeriodEndDate >= DateTime.Now) {
							oCondition.ElectionPeriodStatus = PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD;
						} else {
							oCondition.SecondPeriodFlag = ViCommConst.FLAG_ON_STR;
						}
					} else if (!oCondition.ElectionPeriodStatus.Equals(PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD)) {
						oCondition.SecondPeriodFlag = ViCommConst.FLAG_ON_STR;
					}

					using (ElectionEntry oElectionEntry = new ElectionEntry()) {
						pMaxPage = oElectionEntry.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
						ds = oElectionEntry.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					}
					this.seekConditionEx = oCondition;
				} else {
					ds = new DataSet();
					DataTable dt = new DataTable();
					ds.Tables.Add(dt);
				}

				break;
			}
			case PwViCommConst.INQUIRY_ELECTION_ENTRY_PAST: {
				DataSet dsElectionPeriod;

				ElectionEntrySeekCondition oCondition = new ElectionEntrySeekCondition(pRequest.QueryString);

				using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
					if (!string.IsNullOrEmpty(oCondition.ElectionPeriodSeq)) {
						dsElectionPeriod = oElectionPeriod.GetOneBySeq(site.siteCd,oCondition.ElectionPeriodSeq);
					} else {
						dsElectionPeriod = oElectionPeriod.GetRecentPast(site.siteCd);
					}
				}

				if (dsElectionPeriod.Tables[0].Rows.Count >= 1) {
					oCondition.SiteCd = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["SITE_CD"]);
					oCondition.ElectionPeriodSeq = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["ELECTION_PERIOD_SEQ"]);
					oCondition.FixRankFlag = iBridUtil.GetStringValue(dsElectionPeriod.Tables[0].Rows[0]["FIX_RANK_FLAG"]);
					
					if (!oCondition.ElectionPeriodStatus.Equals(PwViCommConst.ElectionPeriodStatus.FRIST_PERIOD)) {
						oCondition.SecondPeriodFlag = ViCommConst.FLAG_ON_STR;
					}

					using (ElectionEntry oElectionEntry = new ElectionEntry()) {
						pMaxPage = oElectionEntry.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
						ds = oElectionEntry.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
					}
					this.seekConditionEx = oCondition;
				} else {
					ds = new DataSet();
					DataTable dt = new DataTable();
					ds.Tables.Add(dt);
				}

				break;
			}
			case PwViCommConst.INQUIRY_ELECTOIN_PERIOD_VOTABLE: {
					using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
						ds = oElectionPeriod.GetVotable(site.siteCd);
				}
				break;
			}
			case PwViCommConst.INQUIRY_CAST_PROFILE_MOVIE_LIKE_WEEKLY: {
				CastMovieSeekCondition oCondition = new CastMovieSeekCondition();
				using (CastMovie oCastMovie = new CastMovie()) {
					seekCondition.InitCondtition();
					seekCondition.rankType = PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY;
					pMaxPage = oCastMovie.GetPageCount(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",string.Empty,string.Empty,seekCondition,pRowCountOfPage,true,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",string.Empty,string.Empty,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_PROFILE_MOVIE_LIKE_MONTHLY: {
				CastMovieSeekCondition oCondition = new CastMovieSeekCondition();
				using (CastMovie oCastMovie = new CastMovie()) {
					seekCondition.InitCondtition();
					seekCondition.rankType = PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY;
					pMaxPage = oCastMovie.GetPageCount(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",string.Empty,string.Empty,seekCondition,pRowCountOfPage,true,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",string.Empty,string.Empty,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_PROFILE_PIC_LIKE_WEEKLY: {
				CastPicSeekCondition oCondition = new CastPicSeekCondition();
				using (CastPic oCastPic = new CastPic()) {
					seekCondition.InitCondtition();
					seekCondition.rankType = PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY;
					pMaxPage = oCastPic.GetPageCount(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,string.Empty,string.Empty,seekCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,string.Empty,string.Empty,seekCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_PROFILE_PIC_LIKE_MONTHLY: {
				CastPicSeekCondition oCondition = new CastPicSeekCondition();
				using (CastPic oCastPic = new CastPic()) {
					seekCondition.InitCondtition();
					seekCondition.rankType = PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY;
					pMaxPage = oCastPic.GetPageCount(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,string.Empty,string.Empty,seekCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,string.Empty,string.Empty,seekCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_MOVIE_VIEW: {
				string sMovieSeq = iBridUtil.GetStringValue(pRequest.QueryString["movieseq"]);
				ds = new DataSet();

				if (!string.IsNullOrEmpty(sMovieSeq)) {
					using (CastMovie oCastMovie = new CastMovie()) {
						ds = oCastMovie.GetPageCollectionBySeq(sMovieSeq,false);
					}
				}

				if (ds.Tables[0].Rows.Count > 0) {
					totalRowCount = 1;
				}
				break;
			}
			case PwViCommConst.INQUIRY_CAST_MOVIE_COMMENT: {
				CastMovieCommentSeekCondition oCondition = new CastMovieCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;

				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastMovieCommentSort.COMMENT_DATE_NEW;
				}

				using (CastMovieComment oCastMovieComment = new CastMovieComment()) {
					pMaxPage = oCastMovieComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastMovieComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}

				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_PIC_VIEW: {
				string sPicSeq = iBridUtil.GetStringValue(pRequest.QueryString["picseq"]);
				ds = new DataSet();

				if (!string.IsNullOrEmpty(sPicSeq)) {
					using (CastPic oCastPic = new CastPic()) {
						ds = oCastPic.GetPageCollectionBySeq(sPicSeq);
					}
				}

				if (ds.Tables[0].Rows.Count > 0) {
					totalRowCount = 1;
				}
				break;
			}
			case PwViCommConst.INQUIRY_CAST_PIC_COMMENT: {
				CastPicCommentSeekCondition oCondition = new CastPicCommentSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;

				if (string.IsNullOrEmpty(oCondition.Sort)) {
					oCondition.Sort = PwViCommConst.CastPicCommentSort.COMMENT_DATE_NEW;
				}

				using (CastPicComment oCastPicComment = new CastPicComment()) {
					pMaxPage = oCastPicComment.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastPicComment.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}

				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_BLOG_RANKING: {
				BlogSeekCondition oCondition = new BlogSeekCondition(pRequest.QueryString);
				oCondition.SeekMode = pMode;
				oCondition.SiteCd = site.siteCd;
				
				if (string.IsNullOrEmpty(oCondition.RankType)) {
					oCondition.RankType = PwViCommConst.BlogRankType.WEEKLY;
				}
				
				using (Blog oBlog = new Blog()) {
					pMaxPage = oBlog.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oBlog.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_FAVORIT_NEWS: {
				int iNeedCount = 20;
				int iRecPerPage = iNeedCount + 20;
				CastFavoritNewsSeekCondition oCondition = new CastFavoritNewsSeekCondition();
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;

				using (CastFavoritNews oCastFavoritNews = new CastFavoritNews()) {
					DataSet dsTemp = oCastFavoritNews.GetPageCollection(oCondition,1,iRecPerPage);
					ds = oCastFavoritNews.RemoveNonPublishData(dsTemp,iNeedCount);
				}

				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_WAIT_SCHEDULE: {
				WaitScheduleSeekCondition oCondition = new WaitScheduleSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;
				oCondition.SelfUserCharNo = userMan.userCharNo;
				
				string sMin;
				if (DateTime.Now.Minute < 30) {
					sMin = "00";
				} else {
					sMin = "30";
				}
				oCondition.MinWaitStartTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),sMin);
				
				using (WaitSchedule oWaitSchedule = new WaitSchedule()) {
					pMaxPage = oWaitSchedule.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oWaitSchedule.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_PRESENT_MAIL_ITEM: {
					PresentMailItemSeekCondition oCondition = new PresentMailItemSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.PublishFlag = ViCommConst.FLAG_ON_STR;

				using (PresentMailItem oPresentMailItem = new PresentMailItem()) {
					pMaxPage = oPresentMailItem.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oPresentMailItem.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_LOGIN_PLAN: {
					LoginPlanSeekCondition oCondition = new LoginPlanSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;
				oCondition.SelfUserCharNo = userMan.userCharNo;

				string sMin;
				if (DateTime.Now.Minute < 30) {
					sMin = "00";
				} else {
					sMin = "30";
				}
				oCondition.MinLoginStartTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),sMin);

				using (LoginPlan oLoginPlan = new LoginPlan()) {
					pMaxPage = oLoginPlan.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oLoginPlan.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_INVESTIGATE_SCHEDULE: {
				InvestigateScheduleSeekCondition oCondition = new InvestigateScheduleSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
				using (InvestigateSchedule oInvestigateSchedule = new InvestigateSchedule()) {
					ds = oInvestigateSchedule.GetOne(oCondition);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_INVESTIGATE_TARGET: {
				InvestigateTargetSeekCondition oCondition = new InvestigateTargetSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				if (string.IsNullOrEmpty(oCondition.ExecutionDay)) {
					oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
				}
				using (InvestigateTarget oInvestigateTarget = new InvestigateTarget()) {
					ds = oInvestigateTarget.GetPageCollection(oCondition,1,1);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CAST_CAN_MAIL_REQUEST: {
				CastCanMailRequestSeekCondition oCondition = new CastCanMailRequestSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;
				oCondition.SelfUserCharNo = userMan.userCharNo;
				oCondition.NewCastDay = site.newFaceDays;

				using (CastCanMailRequest oCastCanMailRequest = new CastCanMailRequest()) {
					pMaxPage = oCastCanMailRequest.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastCanMailRequest.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				break;
			}
			case PwViCommConst.INQUIRY_MAIL_LOTTERY_TICKET_COUNT: {
				using (MailLotteryTicketCount oMailLotteryTicketCount = new MailLotteryTicketCount()) {
					ds = oMailLotteryTicketCount.GetOne(site.siteCd,userMan.userSeq,userMan.userCharNo,sexCd);
				}
				break;
			}
			case PwViCommConst.INQUIRY_CAST_DIARY_EX: {
				CastDiaryExSeekCondition oCondition = new CastDiaryExSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;

				if (!string.IsNullOrEmpty(oCondition.UserSeq)) {
					oCondition.UserCharNo = ViCommConst.MAIN_CHAR_NO;
				}

				oCondition.ManUserSeq = userMan.userSeq;
				oCondition.ManUserCharNo = userMan.userCharNo;

				using (CastDiaryEx oCastDiaryEx = new CastDiaryEx()) {
					pMaxPage = oCastDiaryEx.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastDiaryEx.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_DEFECT_REPORT: {
				DefectReportSeekCondition oCondition = new DefectReportSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SexCd = sexCd;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.DisplayType = PwViCommConst.DefectReportDislayType.PUBLIC;

				using (DefectReport oDefectReport = new DefectReport()) {
					pMaxPage = oDefectReport.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oDefectReport.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			
			case PwViCommConst.INQUIRY_DEFECT_REPORT_PERSONAL: {
				DefectReportSeekCondition oCondition = new DefectReportSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.UserSeq = userMan.userSeq;
				oCondition.UserCharNo = userMan.userCharNo;
				oCondition.SexCd = sexCd;
				oCondition.DeleteFlag = ViCommConst.FLAG_OFF_STR;
				oCondition.DisplayType = PwViCommConst.DefectReportDislayType.PERSONAL;

				using (DefectReport oDefectReport = new DefectReport()) {
					pMaxPage = oDefectReport.GetPageCount(oCondition,pRowCountOfPage,out totalRowCount);
					ds = oDefectReport.GetPageCollection(oCondition,pCurrentPage,pRowCountOfPage);
				}
				seekConditionEx = oCondition;
				break;
			}
			case PwViCommConst.INQUIRY_CREDIT_AUTO_SETTLE: {
				CreditAutoSettleSeekCondition oCondition = new CreditAutoSettleSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;
				
				using (CreditAutoSettle oCreditAutoSettle = new CreditAutoSettle()) {
					ds = oCreditAutoSettle.GetPageCollection(oCondition);
				}
				break;
			}
			case PwViCommConst.INQUIRY_CREDIT_AUTO_SETTLE_STATUS: {
				CreditAutoSettleStatusSeekCondition oCondition = new CreditAutoSettleStatusSeekCondition(pRequest.QueryString);
				oCondition.SiteCd = site.siteCd;
				oCondition.SelfUserSeq = userMan.userSeq;

				using (CreditAutoSettleStatus oCreditAutoSettleStatus = new CreditAutoSettleStatus()) {
					ds = oCreditAutoSettleStatus.GetPageCollection(oCondition);
				}
				break;
			}
			// いいねした画像一覧
			case PwViCommConst.INQUIRY_CAST_PIC_MY_LIKE: {
				string sAttrTypeSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]);
				string sAttrSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]);

				CastPicSeekCondition oCondition = new CastPicSeekCondition();
				using (CastPic oCastPic = new CastPic()) {
					seekCondition.InitCondtition();
					// 検索条件を取得＆設定
					seekCondition = ExtendSeekCondition(seekCondition,pRequest);
					// いいねした画像のみ検索
					seekCondition.isLikedOnlyFlag = ViCommConst.FLAG_ON_STR;

					pMaxPage = oCastPic.GetPageCount(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,out totalRowCount);
					ds = oCastPic.GetPageCollection(site.siteCd,"","",false,ViCommConst.ATTACHED_PROFILE.ToString(),actCategorySeq,sAttrTypeSeq,sAttrSeq,seekCondition,pCurrentPage,pRowCountOfPage);
				}
				this.seekConditionEx = oCondition;
				break;
			}
			// いいねした動画一覧
			case PwViCommConst.INQUIRY_CAST_MOVIE_MY_LIKE: {
				string sAttrTypeSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]);
				string sAttrSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]);

				CastMovieSeekCondition oCondition = new CastMovieSeekCondition();
				using (CastMovie oCastMovie = new CastMovie()) {
					seekCondition.InitCondtition();
					// 検索条件を取得＆設定
					seekCondition = ExtendSeekCondition(seekCondition,pRequest);
					// いいねした画像のみ検索
					seekCondition.isLikedOnlyFlag = ViCommConst.FLAG_ON_STR;

					pMaxPage = oCastMovie.GetPageCount(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",sAttrTypeSeq,sAttrSeq,seekCondition,pRowCountOfPage,true,out totalRowCount);
					ds = oCastMovie.GetPageCollection(site.siteCd,string.Empty,string.Empty,"",false,ViCommConst.ATTACHED_PROFILE.ToString(),"",sAttrTypeSeq,sAttrSeq,seekCondition,ViCommConst.SORT_NO,pCurrentPage,pRowCountOfPage,true);
				}
				this.seekConditionEx = oCondition;
				break;
			}
		}

		return ds;
	}
}