﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・男性お宝規定回数ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑ取得ﾛｸﾞ
--	Progaram ID		: ManCompCountGetItemLog
--
--  Creation Date	: 2011.12.19
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[System.Serializable]
public class ManCompCountGetItemLog:DbSession {
	public ManCompCountGetItemLog() {
	}

	public DataSet GetOne(string pDisplayMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	COMP_COUNT_GET_ITEM_SEQ						");
		oSqlBuilder.AppendLine(" FROM											");
		oSqlBuilder.AppendLine("	T_MAN_COMP_COUNT_GET_ITEM					");
		oSqlBuilder.AppendLine(" WHERE											");
		oSqlBuilder.AppendLine("	DISPLAY_MONTH = :DISPLAY_MONTH 				");

		oParamList.Add(new OracleParameter(":DISPLAY_MONTH",pDisplayMonth));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
