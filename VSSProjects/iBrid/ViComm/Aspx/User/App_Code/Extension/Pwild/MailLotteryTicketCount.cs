﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メールdeガチャ確率設定
--	Progaram ID		: MailLotteryTicketCount
--
--  Creation Date	: 2015.01.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class MailLotteryTicketCount:DbSession {
	public MailLotteryTicketCount() {
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																										");
		oSqlBuilder.AppendLine("	ML.NEED_SECOND_LOTTERY_COUNT																		,	");
		oSqlBuilder.AppendLine("	NVL(TC.POSSESSION_COUNT,0) AS POSSESSION_COUNT														,	");
		oSqlBuilder.AppendLine("	TRUNC(NVL(TC.POSSESSION_COUNT,0) / ML.NEED_SECOND_LOTTERY_COUNT) AS CAN_GET_SECOND_LOTTERY_COUNT		");
		oSqlBuilder.AppendLine("FROM																										");
		oSqlBuilder.AppendLine("	T_MAIL_LOTTERY ML																					,	");
		oSqlBuilder.AppendLine("	(																										");
		oSqlBuilder.AppendLine("		SELECT																								");
		oSqlBuilder.AppendLine("			SITE_CD																						,	");
		oSqlBuilder.AppendLine("			MAIL_LOTTERY_SEQ																			,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT																				");
		oSqlBuilder.AppendLine("		FROM																								");
		oSqlBuilder.AppendLine("			T_MAIL_LOTTERY_TICKET_COUNT																		");
		oSqlBuilder.AppendLine("		WHERE																								");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD																	AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ																	AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO																	");
		oSqlBuilder.AppendLine("	) TC																									");
		oSqlBuilder.AppendLine("WHERE																										");
		oSqlBuilder.AppendLine("	ML.SITE_CD				= :SITE_CD																	AND	");
		oSqlBuilder.AppendLine("	ML.SEX_CD				= :SEX_CD																	AND	");
		oSqlBuilder.AppendLine("	SYSDATE BETWEEN ML.START_DATE AND END_DATE															AND	");
		oSqlBuilder.AppendLine("	ML.SITE_CD				= TC.SITE_CD															(+)	AND	");
		oSqlBuilder.AppendLine("	ML.MAIL_LOTTERY_SEQ		= TC.MAIL_LOTTERY_SEQ													(+)		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
