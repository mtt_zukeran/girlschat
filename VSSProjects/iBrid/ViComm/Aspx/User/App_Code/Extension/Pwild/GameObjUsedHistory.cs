﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｹﾞｰﾑ用ｵﾌﾞｼﾞｪｸﾄ利用履歴

--	Progaram ID		: GameObjUsedHistory
--
--  Creation Date	: 2011.10.25
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;


[Serializable]
public class GameObjUsedHistorySeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string ObjCategory {
		get {
			return this.Query["obj_category"];
		}
		set {
			this.Query["obj_category"] = value;
		}
	}
	
	public string ObjSeq {
		get {
			return this.Query["obj_seq"];
		}
		set {
			this.Query["obj_seq"] = value;
		}
	}

	public GameObjUsedHistorySeekCondition()
		: this(new NameValueCollection()) {
	}
	
	public GameObjUsedHistorySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["obj_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["obj_seq"]));
	}

}
[System.Serializable]
public class GameObjUsedHistory:DbSession {
	public GameObjUsedHistory() {
	}

	public int GetPageCount(GameObjUsedHistorySeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_GAME_OBJ_USED_HISTORY01 	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}
	
	public DataSet GetPageCollection(GameObjUsedHistorySeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	SITE_CD								,	");
		oSqlBuilder.AppendLine("	USER_SEQ							,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM						,	");
		oSqlBuilder.AppendLine("	AGE									,	");
		oSqlBuilder.AppendLine("	USED_DATE								");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	VW_PW_GAME_OBJ_USED_HISTORY01 			");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		return oDataSet;

	}

	public OracleParameter[] CreateWhere(GameObjUsedHistorySeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ObjCategory)) {
			SysPrograms.SqlAppendWhere(" OBJ_CATEGORY	= :OBJ_CATEGORY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_CATEGORY",pCondition.ObjCategory));
		}

		
		if (!string.IsNullOrEmpty(pCondition.ObjSeq)) {
			SysPrograms.SqlAppendWhere(" OBJ_SEQ	= :OBJ_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_SEQ",pCondition.ObjSeq));
		}

		return oParamList.ToArray();
	}

	public DataSet GetObjUsedHistoryPageCollection(GameObjUsedHistorySeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	SITE_CD								,	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ					,	");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM						,	");
		oSqlBuilder.AppendLine("	OBJ_SEQ								,	");
		oSqlBuilder.AppendLine("	OBJ_TYPE							,	");
		oSqlBuilder.AppendLine("	OBJ_TITLE							,	");
		oSqlBuilder.AppendLine("	USED_DATE								");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	VW_PW_GAME_OBJ_USED_HISTORY02 			");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		return oDataSet;

	}
	
	public string CreateOrderExpresion(GameObjUsedHistorySeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY USED_DATE DESC NULLS LAST";

		return sSortExpression;
	}


}
