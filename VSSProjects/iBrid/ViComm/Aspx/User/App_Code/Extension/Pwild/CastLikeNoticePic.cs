﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者画像新着いいね通知
--	Progaram ID		: CastLikeNoticvePic
--
--  Creation Date	: 2016.10.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

[Serializable]
public class CastLikeNoticePicSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;
	public string SelfCharNo;

	public CastLikeNoticePicSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastLikeNoticePicSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[System.Serializable]
public class CastLikeNoticePic:DbSession {
	public CastLikeNoticePic() {
	}

	/// <summary>
	/// 検索結果の総件数を取得
	/// </summary>
	/// <param name="pCondition"></param>
	/// <param name="pRecPerPage"></param>
	/// <param name="pRecCount"></param>
	/// <returns></returns>
	public int GetPageCount(CastLikeNoticePicSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	COUNT(*)											");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	T_CAST_LIKE_NOTICE P								");
		oSqlBuilder.AppendLine("	INNER JOIN T_CAST_PIC PIC							");
		oSqlBuilder.AppendLine("		ON P.SITE_CD				= PIC.SITE_CD		");
		oSqlBuilder.AppendLine("		AND P.CAST_SEQ				= PIC.USER_SEQ		");
		oSqlBuilder.AppendLine("		AND P.CAST_CHAR_NO			= PIC.USER_CHAR_NO	");
		oSqlBuilder.AppendLine("		AND P.OBJ_SEQ				= PIC.PIC_SEQ		");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	/// <summary>
	/// 検索結果のデータ取得
	/// </summary>
	/// <param name="pCondition"></param>
	/// <param name="pPageNo"></param>
	/// <param name="pRecPerPage"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(CastLikeNoticePicSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		string sSortExpression = " ORDER BY P.SITE_CD,P.LAST_LIKE_DATE DESC,P.CAST_LIKE_NOTICE_SEQ DESC";
		string sWhereClause = string.Empty;
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT																							");
		oSqlBuilder.AppendLine("	T1.*																					,	");
		oSqlBuilder.AppendLine("	GET_PHOTO_IMG_PATH(T1.SITE_CD,U.LOGIN_ID,T1.OBJ_SEQ) AS OBJ_PHOTO_IMG_PATH				,	");
		oSqlBuilder.AppendLine("	GET_SMALL_PHOTO_IMG_PATH(T1.SITE_CD,U.LOGIN_ID,T1.OBJ_SEQ) AS OBJ_SMALL_PHOTO_IMG_PATH	,	");
		oSqlBuilder.AppendLine("	T1.UNCONFIRMED_COUNT - 1 AS UNCONFIRMED_COUNT_OTHER										,	");
		oSqlBuilder.AppendLine("	T1.LIKE_COUNT - 1 AS LIKE_COUNT_OTHER													,	");
		oSqlBuilder.AppendLine("	M.HANDLE_NM AS MAN_HANDLE_NM																");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			*											");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			(											");
		oSqlBuilder.AppendLine("				SELECT									");
		oSqlBuilder.AppendLine("					INNER.*							,	");
		oSqlBuilder.AppendLine("					ROWNUM AS RNUM					,	");
		oSqlBuilder.AppendLine("					ROWNUM AS RANK						");
		oSqlBuilder.AppendLine("				FROM									");
		oSqlBuilder.AppendLine("					(									");
		oSqlBuilder.AppendLine("						SELECT													");
		oSqlBuilder.AppendLine("							P.SITE_CD										,	");
		oSqlBuilder.AppendLine("							P.CAST_LIKE_NOTICE_SEQ							,	");
		oSqlBuilder.AppendLine("							P.CAST_SEQ										,	");
		oSqlBuilder.AppendLine("							P.CAST_CHAR_NO									,	");
		oSqlBuilder.AppendLine("							P.CONTENTS_TYPE									,	");
		oSqlBuilder.AppendLine("							P.OBJ_SEQ										,	");
		oSqlBuilder.AppendLine("							P.UNCONFIRMED_COUNT								,	");
		oSqlBuilder.AppendLine("							P.CONFIRMED_FLAG								,	");
		oSqlBuilder.AppendLine("							P.LAST_LIKE_DATE								,	");
		oSqlBuilder.AppendLine("							P.LAST_LIKE_MAN_SEQ								,	");
		oSqlBuilder.AppendLine("							PIC.PIC_TITLE									,	");
		oSqlBuilder.AppendLine("							PIC.LIKE_COUNT										");
		oSqlBuilder.AppendLine("						FROM													");
		oSqlBuilder.AppendLine("							T_CAST_LIKE_NOTICE P								");
		oSqlBuilder.AppendLine("							INNER JOIN T_CAST_PIC PIC							");
		oSqlBuilder.AppendLine("								ON P.SITE_CD			= PIC.SITE_CD			");
		oSqlBuilder.AppendLine("								AND P.CAST_SEQ			= PIC.USER_SEQ			");
		oSqlBuilder.AppendLine("								AND P.CAST_CHAR_NO		= PIC.USER_CHAR_NO		");
		oSqlBuilder.AppendLine("								AND P.OBJ_SEQ			= PIC.PIC_SEQ			");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("					) INNER								");
		oSqlBuilder.AppendLine("				WHERE									");
		oSqlBuilder.AppendLine("					ROWNUM <= :LAST_ROW					");
		oSqlBuilder.AppendLine("			)											");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			RANK > :FIRST_ROW							");
		oSqlBuilder.AppendLine("	) T1												");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER U									");
		oSqlBuilder.AppendLine("		ON T1.CAST_SEQ				= U.USER_SEQ		");
		oSqlBuilder.AppendLine("	INNER JOIN T_SITE_MANAGEMENT S						");
		oSqlBuilder.AppendLine("		ON T1.SITE_CD				= S.SITE_CD			");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER_MAN_CHARACTER M					");
		oSqlBuilder.AppendLine("		ON T1.SITE_CD				= M.SITE_CD			");
		oSqlBuilder.AppendLine("		AND T1.LAST_LIKE_MAN_SEQ	= M.USER_SEQ		");
		oSqlBuilder.AppendLine("ORDER BY T1.RNUM										");

		oParamList.Add(new OracleParameter(":LAST_ROW",iStartIndex + pRecPerPage));
		oParamList.Add(new OracleParameter(":FIRST_ROW",iStartIndex));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	/// <summary>
	/// 検索条件を生成
	/// </summary>
	/// <param name="pCondition"></param>
	/// <param name="pWhereClause"></param>
	/// <returns></returns>
	private OracleParameter[] CreateWhere(CastLikeNoticePicSeekCondition pCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere("P.CAST_SEQ = :CAST_SEQ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":CAST_SEQ",pCondition.SelfUserSeq));

		SysPrograms.SqlAppendWhere("P.CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.SelfCharNo));


		SysPrograms.SqlAppendWhere("P.CONTENTS_TYPE = :CONTENTS_TYPE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":CONTENTS_TYPE",PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_PIC));

		SysPrograms.SqlAppendWhere("P.NOT_DISPLAY_FLAG = :NOT_DISPLAY_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NOT_DISPLAY_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere("PIC.PIC_TYPE = :PIC_TYPE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PIC_TYPE",ViCommConst.ATTACHED_PROFILE));

		SysPrograms.SqlAppendWhere("PIC.OBJ_NA_FLAG = :OBJ_NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere("PIC.OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere("PIC.OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));

		return oParamList.ToArray();
	}

	public int GetCastLikeNoticeUnconfirmedCount(string pSiteCd,string pCastSeq,string pCastCharNO) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	COUNT(*)														");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	T_CAST_LIKE_NOTICE P											");
		oSqlBuilder.AppendLine("WHERE																");
		oSqlBuilder.AppendLine("	P.SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	P.CAST_SEQ			= :CAST_SEQ								AND	");
		oSqlBuilder.AppendLine("	P.CAST_CHAR_NO		= :CAST_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("	P.CONFIRMED_FLAG	= :CONFIRMED_FLAG						AND	");
		oSqlBuilder.AppendLine("	P.NOT_DISPLAY_FLAG	= :NOT_DISPLAY_FLAG						AND	");
		oSqlBuilder.AppendLine("	CONTENTS_TYPE	= :CONTENTS_TYPE							AND	");
		oSqlBuilder.AppendLine("	EXISTS															");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			*														");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_CAST_PIC												");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.CAST_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.CAST_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PIC_SEQ					= P.OBJ_SEQ					AND	");
		oSqlBuilder.AppendLine("			PIC_TYPE				= :PIC_TYPE					AND	");
		oSqlBuilder.AppendLine("			OBJ_NA_FLAG				= :OBJ_NA_FLAG				AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPLOVE_FLAG		AND	");
		oSqlBuilder.AppendLine("			OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG			");
		oSqlBuilder.AppendLine("	)																");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_SEQ",pCastSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCastCharNO));
		oParamList.Add(new OracleParameter(":CONFIRMED_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":NOT_DISPLAY_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":CONTENTS_TYPE",PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_PIC));
		oParamList.Add(new OracleParameter(":PIC_TYPE",ViCommConst.ATTACHED_PROFILE));
		oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPLOVE_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));

		int iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iRecCount;
	}
}