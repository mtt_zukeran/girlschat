﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｹﾞｰﾑｷｬﾗｸﾀｰﾊﾞﾄﾙ
--	Progaram ID		: GameCharacterBattle
--
--  Creation Date	: 2011.07.30
--  Creater			: PW A.Taba
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

using System.Collections.Specialized;
using System.Collections.Generic;

[Serializable]
public class GameCharacterBattleSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public string Level {
		get {
			return this.Query["level"];
		}
		set {
			this.Query["level"] = value;
		}
	}

	public string LevelRange {
		get {
			return this.Query["level_range"];
		}
		set {
			this.Query["level_range"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public string CastGamePicSeq {
		get {
			return this.Query["cast_game_pic_seq"];
		}
		set {
			this.Query["cast_game_pic_seq"] = value;
		}
	}

	public string CastTreasureSeq {
		get {
			return this.Query["cast_treasure_seq"];
		}
		set {
			this.Query["cast_treasure_seq"] = value;
		}
	}

	public string Random {
		get {
			return this.Query["random"];
		}
		set {
			this.Query["random"] = value;
		}
	}

	public string NotTutorialFlg {
		get {
			return this.Query["not_tutorial_flg"];
		}
		set {
			this.Query["not_tutorial_flg"] = value;
		}
	}
	
	public string TutorialBattleFlag {
		get {
			return this.Query["tutorial_battle_flag"];
		}
		set {
			this.Query["tutorial_battle_flag"] = value;
		}
	}
	
	public string ExceptSelf;
	public string ExceptBattled;
	public string TutorialFlag;
	public string SelfStageGroupType;
	public string MaxPowerFrom;
	public string MaxPowerTo;

	public GameCharacterBattleSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameCharacterBattleSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["level_range"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["level_range"]));
		this.Query["cast_game_pic_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_game_pic_seq"]));
		this.Query["cast_treasure_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_treasure_seq"]));
		this.Query["random"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["random"]));
		this.Query["tutorial_battle_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tutorial_battle_flag"]));
		this.ExceptSelf = ViCommConst.FLAG_OFF_STR;
		this.ExceptBattled = ViCommConst.FLAG_OFF_STR;
	}
}

[System.Serializable]
public class GameCharacterBattle:DbSession {

	public int GetPageCount(GameCharacterBattleSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER04 P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameCharacterBattleSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(GameCharacterBattleSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	" + CreateField(pCondtion) + "					,	");
		oSqlBuilder.AppendLine("	P.MAX_ATTACK_FORCE_COUNT			");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER04 P							");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		if (pCondtion.SeekMode == PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_VIEW) {
			AppendCommunicationTotal(oDataSet,pCondtion);
		} else if (pCondtion.SeekMode == PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_COMMUNICATION_VIEW) {
			AppendCommunication(oDataSet,pCondtion);
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(GameCharacterBattleSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (pCondition.ExceptSelf.Equals(ViCommConst.FLAG_ON_STR)) {
			if (!string.IsNullOrEmpty(pCondition.UserSeq) && !string.IsNullOrEmpty(pCondition.UserCharNo)) {
				SysPrograms.SqlAppendWhere(" NOT( P.USER_SEQ	= :USER_SEQ AND P.USER_CHAR_NO = :USER_CHAR_NO ) ",ref pWhereClause);
				oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
				oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
			}
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" P.SEX_CD		= :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.LevelRange) && !string.IsNullOrEmpty(pCondition.Level)) {
			int startLevel;
			int endLevel;

			startLevel = int.Parse(pCondition.Level) - int.Parse(pCondition.LevelRange);
			endLevel = int.Parse(pCondition.Level) + int.Parse(pCondition.LevelRange);

			if (startLevel <= 0) {
				startLevel = 1;
			}

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		>= :GAME_CHARACTER_LEVEL_START",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_START",startLevel));

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_LEVEL		<= :GAME_CHARACTER_LEVEL_END",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_LEVEL_END",endLevel));

			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_TYPE		IS NOT NULL",ref pWhereClause);
		}

		if (pCondition.ExceptBattled.Equals(ViCommConst.FLAG_ON_STR) &&
			!string.IsNullOrEmpty(pCondition.UserSeq) &&
			!string.IsNullOrEmpty(pCondition.UserCharNo)
			) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("NOT EXISTS (															");
			oSqlBuilder.AppendLine("	SELECT																");
			oSqlBuilder.AppendLine("		*																");
			oSqlBuilder.AppendLine("	FROM																");
			oSqlBuilder.AppendLine("		VW_PW_DAILY_BATTLE_LOG01										");
			oSqlBuilder.AppendLine("	WHERE																");
			oSqlBuilder.AppendLine("		SITE_CD = P.SITE_CD AND					");
			oSqlBuilder.AppendLine("		USER_SEQ = :USER_SEQ AND										");
			oSqlBuilder.AppendLine("		USER_CHAR_NO = :USER_CHAR_NO AND								");
			oSqlBuilder.AppendLine("		PARTNER_USER_SEQ = P.USER_SEQ AND			");
			oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO = P.USER_CHAR_NO AND	");
			oSqlBuilder.AppendLine("		REPORT_DAY = :REPORT_DAY										");
			oSqlBuilder.AppendLine(")																		");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
			string sDate = DateTime.Now.ToString("yyyy/MM/dd");
			oParamList.Add(new OracleParameter(":REPORT_DAY",sDate));
		}

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		if(pCondition.SeekMode == PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_LIST) {
			SysPrograms.SqlAppendWhere(" P.TUTORIAL_STATUS IS NULL",ref pWhereClause);
		}

		SysPrograms.SqlAppendWhere(" P.STAFF_FLAG = :STAFF_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere("(P.NA_FLAG IN (:NA_FLAG,:NA_FLAG_2))",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(GameCharacterBattleSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		if (pCondition.Random.Equals(ViCommConst.FLAG_ON_STR)) {
			sSortExpression = " ORDER BY DBMS_RANDOM.RANDOM ";
		} else {
			sSortExpression = " ORDER BY P.SITE_CD ";
		}

		return sSortExpression;
	}

	protected string CreateField(GameCharacterBattleSeekCondition pCondition) {
		StringBuilder sSql = new StringBuilder();

		if (pCondition.SeekMode == PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_VIEW) {
			sSql.Append("P.SITE_CD					,	").AppendLine();
			sSql.Append("P.USER_SEQ					,	").AppendLine();
			sSql.Append("P.USER_CHAR_NO				,	").AppendLine();
			sSql.Append("P.GAME_HANDLE_NM			,	").AppendLine();
			sSql.Append("P.GAME_CHARACTER_TYPE		,	").AppendLine();
			sSql.Append("P.GAME_CHARACTER_LEVEL		,	").AppendLine();
			sSql.Append("P.FELLOW_COUNT				,	").AppendLine();
			sSql.Append("P.TUTORIAL_STATUS			,	").AppendLine();
			sSql.Append("P.TOTAL_WIN_COUNT			,	").AppendLine();
			sSql.Append("P.TOTAL_LOSS_COUNT			,	").AppendLine();
			sSql.Append("P.TOTAL_PARTY_WIN_COUNT	,	").AppendLine();
			sSql.Append("P.TOTAL_PARTY_LOSS_COUNT	,	").AppendLine();
			sSql.Append("P.AGE						").AppendLine();
		} else {
			sSql.Append("P.SITE_CD					,	").AppendLine();
			sSql.Append("P.USER_SEQ					,	").AppendLine();
			sSql.Append("P.USER_CHAR_NO				,	").AppendLine();
			sSql.Append("P.GAME_HANDLE_NM			,	").AppendLine();
			sSql.Append("P.GAME_CHARACTER_TYPE		,	").AppendLine();
			sSql.Append("P.GAME_CHARACTER_LEVEL		,	").AppendLine();
			sSql.Append("P.FELLOW_COUNT				,	").AppendLine();
			sSql.Append("P.TUTORIAL_STATUS			,	").AppendLine();
			sSql.Append("P.AGE							").AppendLine();
		}

		return sSql.ToString();
	}

	private void AppendCommunication(DataSet pDS,GameCharacterBattleSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("ATTACK_WIN_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("ATTACK_LOSS_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("PARTY_ATTACK_WIN_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("PARTY_ATTACK_LOSS_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("DEFENCE_WIN_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("DEFENCE_LOSS_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("PARTY_DEFENCE_WIN_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("PARTY_DEFENCE_LOSS_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("GET_POINT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("LOSS_POINT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("TREASURE_GET_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("TREASURE_LOSS_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	ATTACK_WIN_COUNT			,							");
		oSqlBuilder.AppendLine("	ATTACK_LOSS_COUNT			,							");
		oSqlBuilder.AppendLine("	PARTY_ATTACK_WIN_COUNT		,							");
		oSqlBuilder.AppendLine("	PARTY_ATTACK_LOSS_COUNT		,							");
		oSqlBuilder.AppendLine("	DEFENCE_WIN_COUNT			,							");
		oSqlBuilder.AppendLine("	DEFENCE_LOSS_COUNT			,							");
		oSqlBuilder.AppendLine("	PARTY_DEFENCE_WIN_COUNT		,							");
		oSqlBuilder.AppendLine("	PARTY_DEFENCE_LOSS_COUNT	,							");
		oSqlBuilder.AppendLine("	GET_POINT					,							");
		oSqlBuilder.AppendLine("	LOSS_POINT					,							");
		oSqlBuilder.AppendLine("	TREASURE_GET_COUNT			,							");
		oSqlBuilder.AppendLine("	TREASURE_LOSS_COUNT										");
		oSqlBuilder.AppendLine(" FROM														");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION									");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND ");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;
					cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
					cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
					cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
					cmd.Parameters.Add(":PARTNER_USER_SEQ",pDS.Tables[0].Rows[0]["USER_SEQ"]);
					cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",pDS.Tables[0].Rows[0]["USER_CHAR_NO"]);

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					if (dsSub.Tables[0].Rows.Count > 0) {
						dr["ATTACK_WIN_COUNT"] = dsSub.Tables[0].Rows[0]["ATTACK_WIN_COUNT"];
						dr["ATTACK_LOSS_COUNT"] = dsSub.Tables[0].Rows[0]["ATTACK_LOSS_COUNT"];
						dr["PARTY_ATTACK_WIN_COUNT"] = dsSub.Tables[0].Rows[0]["PARTY_ATTACK_WIN_COUNT"];
						dr["PARTY_ATTACK_LOSS_COUNT"] = dsSub.Tables[0].Rows[0]["PARTY_ATTACK_LOSS_COUNT"];
						dr["DEFENCE_WIN_COUNT"] = dsSub.Tables[0].Rows[0]["DEFENCE_WIN_COUNT"];
						dr["DEFENCE_LOSS_COUNT"] = dsSub.Tables[0].Rows[0]["DEFENCE_LOSS_COUNT"];
						dr["PARTY_DEFENCE_WIN_COUNT"] = dsSub.Tables[0].Rows[0]["PARTY_DEFENCE_WIN_COUNT"];
						dr["PARTY_DEFENCE_LOSS_COUNT"] = dsSub.Tables[0].Rows[0]["PARTY_DEFENCE_LOSS_COUNT"];
						dr["GET_POINT"] = dsSub.Tables[0].Rows[0]["GET_POINT"];
						dr["LOSS_POINT"] = dsSub.Tables[0].Rows[0]["LOSS_POINT"];
						dr["TREASURE_GET_COUNT"] = dsSub.Tables[0].Rows[0]["TREASURE_GET_COUNT"];
						dr["TREASURE_LOSS_COUNT"] = dsSub.Tables[0].Rows[0]["TREASURE_LOSS_COUNT"];
					} else {
						dr["ATTACK_WIN_COUNT"] = 0;
						dr["ATTACK_LOSS_COUNT"] = 0;
						dr["PARTY_ATTACK_WIN_COUNT"] = 0;
						dr["PARTY_ATTACK_LOSS_COUNT"] = 0;
						dr["DEFENCE_WIN_COUNT"] = 0;
						dr["DEFENCE_LOSS_COUNT"] = 0;
						dr["PARTY_DEFENCE_WIN_COUNT"] = 0;
						dr["PARTY_DEFENCE_LOSS_COUNT"] = 0;
						dr["GET_POINT"] = 0;
						dr["LOSS_POINT"] = 0;
						dr["TREASURE_GET_COUNT"] = 0;
						dr["TREASURE_LOSS_COUNT"] = 0;
					}
				}
			}
		}
	}

	private void AppendCommunicationTotal(DataSet pDS,GameCharacterBattleSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("WIN_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("LOSS_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("PARTY_WIN_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn(string.Format("PARTY_LOSS_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																																	");
		oSqlBuilder.AppendLine("	NVL(SUM(T_GAME_COMMUNICATION.ATTACK_WIN_COUNT + T_GAME_COMMUNICATION.DEFENCE_WIN_COUNT),0)					AS WIN_COUNT		,	");
		oSqlBuilder.AppendLine("	NVL(SUM(T_GAME_COMMUNICATION.ATTACK_LOSS_COUNT + T_GAME_COMMUNICATION.DEFENCE_LOSS_COUNT),0)				AS LOSS_COUNT		,	");
		oSqlBuilder.AppendLine("	NVL(SUM(T_GAME_COMMUNICATION.PARTY_ATTACK_WIN_COUNT + T_GAME_COMMUNICATION.PARTY_DEFENCE_WIN_COUNT),0)		AS PARTY_WIN_COUNT	,	");
		oSqlBuilder.AppendLine("	NVL(SUM(T_GAME_COMMUNICATION.PARTY_ATTACK_LOSS_COUNT + T_GAME_COMMUNICATION.PARTY_DEFENCE_LOSS_COUNT),0)	AS PARTY_LOSS_COUNT		");
		oSqlBuilder.AppendLine(" FROM																																	");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION																												");
		oSqlBuilder.AppendLine(" WHERE																																	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND																									");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND																									");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO																										");

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pDS.Tables[0].Rows[0]["SITE_CD"]);
					cmd.Parameters.Add(":USER_SEQ",pDS.Tables[0].Rows[0]["USER_SEQ"]);
					cmd.Parameters.Add(":USER_CHAR_NO",pDS.Tables[0].Rows[0]["USER_CHAR_NO"]);

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					if (dsSub.Tables[0].Rows.Count > 0) {
						dr["WIN_COUNT"] = dsSub.Tables[0].Rows[0]["WIN_COUNT"];
						dr["LOSS_COUNT"] = dsSub.Tables[0].Rows[0]["LOSS_COUNT"];
						dr["PARTY_WIN_COUNT"] = dsSub.Tables[0].Rows[0]["PARTY_WIN_COUNT"];
						dr["PARTY_LOSS_COUNT"] = dsSub.Tables[0].Rows[0]["PARTY_LOSS_COUNT"];
					} else {
						dr["WIN_COUNT"] = 0;
						dr["LOSS_COUNT"] = 0;
						dr["PARTY_WIN_COUNT"] = 0;
						dr["PARTY_LOSS_COUNT"] = 0;
					}
				}
			}
		}
	}

	public DataSet GetOneByUserSeq(GameCharacterBattleSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM	,								");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE	,							");
		oSqlBuilder.AppendLine("	TUTORIAL_STATUS	,								");
		oSqlBuilder.AppendLine("	FELLOW_COUNT									");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER								");
		oSqlBuilder.AppendLine(" WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO					");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.PartnerUserCharNo));
		
		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	public int GetPageCountLose(GameCharacterBattleSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	(SELECT							");
		oSqlBuilder.AppendLine("		P.SITE_CD				,	");
		oSqlBuilder.AppendLine("		P.USER_SEQ				,	");
		oSqlBuilder.AppendLine("		P.USER_CHAR_NO				");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		(SELECT												");
		oSqlBuilder.AppendLine("			SITE_CD										,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO							");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			VW_PW_GAME_CHARACTER_LOSE01	PARTNER				");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			NOT														");
		oSqlBuilder.AppendLine("			(														");
		oSqlBuilder.AppendLine("				PARTNER_USER_SEQ		= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_CHAR_NO	= :USER_CHAR_NO				");
		oSqlBuilder.AppendLine("			)													AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS																");
		oSqlBuilder.AppendLine("			(																		");
		oSqlBuilder.AppendLine("				SELECT																");
		oSqlBuilder.AppendLine("					*																");
		oSqlBuilder.AppendLine("				FROM																");
		oSqlBuilder.AppendLine("					VW_PW_DAILY_BATTLE_LOG01 DAILY_LOG								");
		oSqlBuilder.AppendLine("				WHERE																");
		oSqlBuilder.AppendLine("					DAILY_LOG.SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.USER_SEQ					= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.USER_CHAR_NO				= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.REPORT_DAY				= :REPORT_DAY					AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.SITE_CD					= PARTNER.SITE_CD				AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.PARTNER_USER_SEQ			= PARTNER.PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.PARTNER_USER_CHAR_NO		= PARTNER.PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("			)																				");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));

		oSqlBuilder.AppendLine("		GROUP BY							");
		oSqlBuilder.AppendLine("			SITE_CD					,		");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,		");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO			");
		oSqlBuilder.AppendLine("		) LOG							,	");
		oSqlBuilder.AppendLine("		T_GAME_CHARACTER P				,	");
		if(pCondition.SexCd.Equals(ViCommConst.MAN)) {
			oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER CHARACTER		");
		} else {
			oSqlBuilder.AppendLine("		T_CAST_CHARACTER CHARACTER			");
		}
		oSqlBuilder.AppendLine("	WHERE																");
		oSqlBuilder.AppendLine("		LOG.SITE_CD							= P.SITE_CD								AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_SEQ				= P.USER_SEQ							AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_CHAR_NO			= P.USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("		LOG.SITE_CD							= CHARACTER.SITE_CD						AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_SEQ				= CHARACTER.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_CHAR_NO			= CHARACTER.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_STATUS					IS NULL									AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG				AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG					AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("		CHARACTER.NA_FLAG			IN (:NA_FLAG,:NA_FLAG_2)							");

		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		oSqlBuilder.AppendLine(")");

		OracleParameter[] oWhereParams = oParamList.ToArray();

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		if (pRecCount > int.Parse(PwViCommConst.BATTLE_LOSE_LIMIT_ROW)) {
			pRecCount = int.Parse(PwViCommConst.BATTLE_LOSE_LIMIT_ROW);
		}

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollectionLose(GameCharacterBattleSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		string sSortExpression = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine(" *															");
		oSqlBuilder.AppendLine(" FROM														");
		oSqlBuilder.AppendLine("	(SELECT													");
		oSqlBuilder.AppendLine("		P.SITE_CD										,	");
		oSqlBuilder.AppendLine("		P.USER_SEQ										,	");
		oSqlBuilder.AppendLine("		P.USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM								,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE							,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL							,	");
		oSqlBuilder.AppendLine("		P.FELLOW_COUNT									,	");
		oSqlBuilder.AppendLine("		LOG.LOSS_COUNT									,	");
		oSqlBuilder.AppendLine("		LOG.LAST_LOSE_DATE								,	");
		oSqlBuilder.AppendLine("		CHARACTER.AGE									,	");
		oSqlBuilder.AppendLine("		AF.MAX_FORCE_COUNT AS MAX_ATTACK_FORCE_COUNT		");
		oSqlBuilder.AppendLine("	FROM							");
		oSqlBuilder.AppendLine("		(SELECT												");
		oSqlBuilder.AppendLine("			SITE_CD										,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			COUNT(*) AS LOSS_COUNT						,	");
		oSqlBuilder.AppendLine("			MAX(CREATE_DATE) AS LAST_LOSE_DATE				");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			VW_PW_GAME_CHARACTER_LOSE01 PARTNER				");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			NOT														");
		oSqlBuilder.AppendLine("			(														");
		oSqlBuilder.AppendLine("				PARTNER_USER_SEQ		= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_CHAR_NO	= :USER_CHAR_NO				");
		oSqlBuilder.AppendLine("			)													AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS														");
		oSqlBuilder.AppendLine("			(																");
		oSqlBuilder.AppendLine("				SELECT														");
		oSqlBuilder.AppendLine("					*														");
		oSqlBuilder.AppendLine("				FROM														");
		oSqlBuilder.AppendLine("					VW_PW_DAILY_BATTLE_LOG01 DAILY_LOG										");
		oSqlBuilder.AppendLine("				WHERE																		");
		oSqlBuilder.AppendLine("					DAILY_LOG.SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.USER_SEQ					= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.USER_CHAR_NO				= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.REPORT_DAY				= :REPORT_DAY					AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.SITE_CD					= PARTNER.SITE_CD				AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.PARTNER_USER_SEQ			= PARTNER.PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("					DAILY_LOG.PARTNER_USER_CHAR_NO		= PARTNER.PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("			)																				");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));

		oSqlBuilder.AppendLine("		GROUP BY							");
		oSqlBuilder.AppendLine("			SITE_CD					,		");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,		");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO			");
		oSqlBuilder.AppendLine("		) LOG						,		");
		oSqlBuilder.AppendLine("		T_GAME_CHARACTER P			,		");
		oSqlBuilder.AppendLine("		T_ATTACK_FORCE AF			,		");
		if (pCondition.SexCd.Equals(ViCommConst.MAN)) {
			oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER CHARACTER		");
		} else {
			oSqlBuilder.AppendLine("		T_CAST_CHARACTER CHARACTER			");
		}
		oSqlBuilder.AppendLine("	WHERE																");
		oSqlBuilder.AppendLine("		LOG.SITE_CD					= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_SEQ		= P.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("		LOG.SITE_CD					= CHARACTER.SITE_CD				AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_SEQ		= CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_CHAR_NO	= CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("		LOG.SITE_CD					= AF.SITE_CD					AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_SEQ		= AF.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("		LOG.PARTNER_USER_CHAR_NO	= AF.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_STATUS					IS NULL									AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG				AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG					AND	");
		oSqlBuilder.AppendLine("		P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("		CHARACTER.NA_FLAG			IN (:NA_FLAG,:NA_FLAG_2)			");

		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		oSqlBuilder.AppendLine("	)");

		if (pCondition.Sort.Equals(PwViCommConst.GameCharacterBattleLoseSort.LAST_LOSE_DATE)) {
			sSortExpression = " ORDER BY LAST_LOSE_DATE DESC,USER_SEQ";
		} else if (pCondition.Sort.Equals(PwViCommConst.GameCharacterBattleLoseSort.LOSE_COUNT)) {
			sSortExpression = " ORDER BY LOSS_COUNT DESC,USER_SEQ";
		}

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionTreasure(GameCharacterBattleSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	PARTNER.SITE_CD									,	");
		oSqlBuilder.AppendLine("	PARTNER.USER_SEQ								,	");
		oSqlBuilder.AppendLine("	PARTNER.USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine("	PARTNER.GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("	PARTNER.GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("	PARTNER.GAME_CHARACTER_LEVEL					,	");
		oSqlBuilder.AppendLine("	PARTNER.FELLOW_COUNT							,	");
		oSqlBuilder.AppendLine("	PARTNER.AGE										,	");
		oSqlBuilder.AppendLine("	AF.MAX_FORCE_COUNT AS MAX_ATTACK_FORCE_COUNT		");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER01	PARTNER					,	");
		oSqlBuilder.AppendLine("	T_ATTACK_FORCE AF									");
		oSqlBuilder.AppendLine("WHERE																");
		oSqlBuilder.AppendLine("	PARTNER.SITE_CD			= AF.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	PARTNER.USER_SEQ		= AF.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	PARTNER.USER_CHAR_NO	= AF.USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	PARTNER.SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	EXISTS															");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			*														");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_MAN_TREASURE											");
		oSqlBuilder.AppendLine("		WHERE 														");
		oSqlBuilder.AppendLine("			USER_SEQ		=	:PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	=	:PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			CAST_GAME_PIC_SEQ	=	:CAST_GAME_PIC_SEQ			AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_MISSION_FLAG	=	:TUTORIAL_MISSION_FLAG	AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_BATTLE_FLAG	=	:TUTORIAL_BATTLE_FLAG		");
		oSqlBuilder.AppendLine("	)															AND	");
		oSqlBuilder.AppendLine("	NOT																");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		PARTNER.USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("		PARTNER.USER_CHAR_NO	= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)															AND	");
		oSqlBuilder.AppendLine("	PARTNER.SEX_CD				= :SEX_CD					AND	");
		oSqlBuilder.AppendLine("	PARTNER.TUTORIAL_STATUS		IS NULL						AND	");
		oSqlBuilder.AppendLine("	PARTNER.CAST_GAME_PIC_SEQ	= :CAST_GAME_PIC_SEQ		AND	");
		oSqlBuilder.AppendLine("	PARTNER.CAN_ROB_COUNT		> 0							AND	");
		oSqlBuilder.AppendLine("	PARTNER.NA_FLAG				IN (:NA_FLAG,:NA_FLAG_2)	AND	");
		oSqlBuilder.AppendLine("	PARTNER.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	PARTNER.TUTORIAL_MISSION_TREASURE_FLAG		= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	PARTNER.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	PARTNER.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS											");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			VW_PW_DAILY_BATTLE_LOG01	DAILY_LOG								");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			DAILY_LOG.SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.USER_SEQ					= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.USER_CHAR_NO				= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.REPORT_DAY				= :REPORT_DAY						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.SITE_CD					= PARTNER.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.PARTNER_USER_SEQ			= PARTNER.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.PARTNER_USER_CHAR_NO		= PARTNER.USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	)																					");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondtion.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondtion.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondtion.SexCd));
		oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCondtion.CastGamePicSeq));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		// order by
		sSortExpression = " ORDER BY DBMS_RANDOM.RANDOM ";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public int GetPageCountTreasureWoman(GameCharacterBattleSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM											");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_CAST_TRSR01		PCT	,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER					GC	,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER					CC	,	");
		oSqlBuilder.AppendLine("	T_ATTACK_FORCE						AF		");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= GC.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_SEQ			= GC.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_CHAR_NO		= GC.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= CC.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_SEQ			= CC.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_CHAR_NO		= CC.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= AF.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_SEQ			= AF.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_CHAR_NO		= AF.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	NOT															");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		PCT.USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("		PCT.USER_CHAR_NO	= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)														AND	");
		oSqlBuilder.AppendLine("	GC.SEX_CD							= :SEX_CD							AND	");
		oSqlBuilder.AppendLine("	PCT.CAST_TREASURE_SEQ				= :CAST_TREASURE_SEQ				AND	");
		oSqlBuilder.AppendLine("	PCT.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	PCT.CAN_ROB_COUNT					> 0									AND	");
		oSqlBuilder.AppendLine("	CC.NA_FLAG							IN (:NA_FLAG,:NA_FLAG_2)			AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			VW_PW_DAILY_BATTLE_LOG01	DAILY_LOG								");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			DAILY_LOG.SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.USER_SEQ					= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.USER_CHAR_NO				= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.REPORT_DAY				= :REPORT_DAY						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.SITE_CD					= PCT.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.PARTNER_USER_SEQ			= PCT.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.PARTNER_USER_CHAR_NO		= PCT.USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																					");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondtion.SexCd));
		oParamList.Add(new OracleParameter(":CAST_TREASURE_SEQ",pCondtion.CastTreasureSeq));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",pCondtion.TutorialBattleFlag));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollectionTreasureWoman(GameCharacterBattleSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	PCT.SITE_CD												,	");
		oSqlBuilder.AppendLine("	PCT.USER_SEQ											,	");
		oSqlBuilder.AppendLine("	PCT.USER_CHAR_NO										,	");
		oSqlBuilder.AppendLine("	GC.GAME_HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	GC.GAME_CHARACTER_TYPE									,	");
		oSqlBuilder.AppendLine("	GC.GAME_CHARACTER_LEVEL									,	");
		oSqlBuilder.AppendLine("	GC.FELLOW_COUNT											,	");
		oSqlBuilder.AppendLine("	CC.AGE													,	");
		oSqlBuilder.AppendLine("	AF.MAX_FORCE_COUNT AS MAX_ATTACK_FORCE_COUNT				");
		oSqlBuilder.AppendLine(" FROM											");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_CAST_TRSR01		PCT	,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER					GC	,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER					CC	,	");
		oSqlBuilder.AppendLine("	T_ATTACK_FORCE						AF		");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= GC.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_SEQ			= GC.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_CHAR_NO		= GC.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= CC.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_SEQ			= CC.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_CHAR_NO		= CC.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= AF.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_SEQ			= AF.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	PCT.USER_CHAR_NO		= AF.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PCT.SITE_CD				= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	NOT															");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		PCT.USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("		PCT.USER_CHAR_NO	= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)														AND	");
		oSqlBuilder.AppendLine("	GC.SEX_CD				= :SEX_CD					AND	");
		oSqlBuilder.AppendLine("	PCT.CAST_TREASURE_SEQ	= :CAST_TREASURE_SEQ		AND	");
		oSqlBuilder.AppendLine("	PCT.CAN_ROB_COUNT		> 0							AND	");
		oSqlBuilder.AppendLine("	CC.NA_FLAG				IN (:NA_FLAG,:NA_FLAG_2)	AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");

		if (!string.IsNullOrEmpty(pCondtion.MaxPowerFrom)) {
			oSqlBuilder.AppendLine("	(GC.MAX_ATTACK_POWER + GC.MAX_DEFENCE_POWER) >= :MAX_POWER_FROM AND ");
		}

		if (!string.IsNullOrEmpty(pCondtion.MaxPowerTo)) {
			oSqlBuilder.AppendLine("	(GC.MAX_ATTACK_POWER + GC.MAX_DEFENCE_POWER) <= :MAX_POWER_TO AND ");
		}

		oSqlBuilder.AppendLine("	NOT EXISTS																	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			*																	");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			VW_PW_DAILY_BATTLE_LOG01	DAILY_LOG								");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			DAILY_LOG.SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.USER_SEQ					= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.USER_CHAR_NO				= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.REPORT_DAY				= :REPORT_DAY						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.SITE_CD					= PCT.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.PARTNER_USER_SEQ			= PCT.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			DAILY_LOG.PARTNER_USER_CHAR_NO		= PCT.USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																					");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondtion.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondtion.UserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondtion.SexCd));
		oParamList.Add(new OracleParameter(":CAST_TREASURE_SEQ",pCondtion.CastTreasureSeq));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",pCondtion.TutorialBattleFlag));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		if (!string.IsNullOrEmpty(pCondtion.MaxPowerFrom)) {
			oParamList.Add(new OracleParameter(":MAX_POWER_FROM",pCondtion.MaxPowerFrom));
		}

		if (!string.IsNullOrEmpty(pCondtion.MaxPowerTo)) {
			oParamList.Add(new OracleParameter(":MAX_POWER_TO",pCondtion.MaxPowerTo));
		}

		// order by
		sSortExpression = sSortExpression = " ORDER BY DBMS_RANDOM.RANDOM ";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneBattleStart(GameCharacterBattleSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	PARTNER.SITE_CD																,	");
		oSqlBuilder.AppendLine("	PARTNER.USER_SEQ				AS PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("	PARTNER.USER_CHAR_NO			AS PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	PARTNER.GAME_HANDLE_NM														,	");
		oSqlBuilder.AppendLine("	PARTNER.GAME_CHARACTER_TYPE													,	");
		oSqlBuilder.AppendLine("	PARTNER.ATTACK_MAX_FORCE_COUNT	AS MAX_ATTACK_FORCE_COUNT					,	");
		oSqlBuilder.AppendLine("	PARTNER.DEFENCE_FORCE_COUNT													,	");
		oSqlBuilder.AppendLine("	PARTNER.NA_FLAG					AS PARTNER_NA_FLAG							,	");
		oSqlBuilder.AppendLine("	NULL							AS CAST_GAME_PIC_SEQ							");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER03 PARTNER													");
		oSqlBuilder.AppendLine("WHERE																				");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :PARTNER_USER_SEQ									AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :PARTNER_USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG					AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG						AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	NOT																				");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		USER_SEQ			= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO		= :USER_CHAR_NO											");
		oSqlBuilder.AppendLine("	)																			AND	");
		oSqlBuilder.AppendLine("	SEX_CD					= :SEX_CD											AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																		");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			*																		");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			VW_PW_DAILY_BATTLE_LOG01 LOG											");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			LOG.SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			LOG.USER_SEQ				= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			LOG.USER_CHAR_NO			= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			LOG.REPORT_DAY				= :REPORT_DAY							AND	");
		oSqlBuilder.AppendLine("			LOG.SITE_CD					= PARTNER.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			LOG.PARTNER_USER_SEQ		= PARTNER.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			LOG.PARTNER_USER_CHAR_NO	= PARTNER.USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																				");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",pCondition.TutorialBattleFlag));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendManTreasureData(oDataSet,pCondition);

		return oDataSet;
	}

	private void AppendManTreasureData(DataSet pDS,GameCharacterBattleSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count < 1) {
			return;
		}

		PossessionManTreasureSeekCondition subCondition = new PossessionManTreasureSeekCondition();
		subCondition.SiteCd = pCondition.SiteCd;
		subCondition.UserSeq = pCondition.PartnerUserSeq;
		subCondition.UserCharNo = pCondition.PartnerUserCharNo;
		subCondition.SelfUserSeq = pCondition.UserSeq;
		subCondition.SelfUserCharNo = pCondition.UserCharNo;
		subCondition.CanRobFlag = ViCommConst.FLAG_ON_STR;
		subCondition.CastGamePicSeq = pCondition.CastGamePicSeq;
		
		if(!string.IsNullOrEmpty(pCondition.TutorialFlag) && pCondition.TutorialFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			subCondition.TutorialBattleFlag = ViCommConst.FLAG_ON_STR;
			subCondition.TreasureTutorialBattleFlag = ViCommConst.FLAG_ON_STR;
		} else {
			subCondition.TutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
			subCondition.TreasureTutorialBattleFlag = ViCommConst.FLAG_OFF_STR;
		}

		DataSet subDs;
		using (PossessionManTreasure oPossessionManTreasure = new PossessionManTreasure()) {
			subDs = oPossessionManTreasure.GetOne(subCondition);
		}

		if (subDs.Tables[0].Rows.Count >= 1) {
			pDS.Tables[0].Rows[0]["CAST_GAME_PIC_SEQ"] = subDs.Tables[0].Rows[0]["CAST_GAME_PIC_SEQ"];
		}
	}

	public DataSet GetOneBattleStartWoman(GameCharacterBattleSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	PARTNER.SITE_CD																,	");
		oSqlBuilder.AppendLine("	PARTNER.USER_SEQ				AS PARTNER_USER_SEQ							,	");
		oSqlBuilder.AppendLine("	PARTNER.USER_CHAR_NO			AS PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	PARTNER.GAME_HANDLE_NM														,	");
		oSqlBuilder.AppendLine("	PARTNER.GAME_CHARACTER_TYPE													,	");
		oSqlBuilder.AppendLine("	PARTNER.ATTACK_MAX_FORCE_COUNT	AS MAX_ATTACK_FORCE_COUNT					,	");
		oSqlBuilder.AppendLine("	PARTNER.DEFENCE_FORCE_COUNT													,	");
		oSqlBuilder.AppendLine("	PARTNER.NA_FLAG					AS PARTNER_NA_FLAG							,	");
		oSqlBuilder.AppendLine("	NULL							AS CAST_TREASURE_SEQ							");
		oSqlBuilder.AppendLine("FROM																				");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER03 PARTNER													");
		oSqlBuilder.AppendLine("WHERE																				");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :PARTNER_USER_SEQ									AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :PARTNER_USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG					AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG						AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	NOT																				");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		USER_SEQ			= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO		= :USER_CHAR_NO											");
		oSqlBuilder.AppendLine("	)																			AND	");
		oSqlBuilder.AppendLine("	SEX_CD					= :SEX_CD											AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																		");
		oSqlBuilder.AppendLine("	(																				");
		oSqlBuilder.AppendLine("		SELECT																		");
		oSqlBuilder.AppendLine("			*																		");
		oSqlBuilder.AppendLine("		FROM																		");
		oSqlBuilder.AppendLine("			VW_PW_DAILY_BATTLE_LOG01 LOG											");
		oSqlBuilder.AppendLine("		WHERE																		");
		oSqlBuilder.AppendLine("			LOG.SITE_CD					= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			LOG.USER_SEQ				= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			LOG.USER_CHAR_NO			= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			LOG.REPORT_DAY				= :REPORT_DAY							AND	");
		oSqlBuilder.AppendLine("			LOG.SITE_CD					= PARTNER.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			LOG.PARTNER_USER_SEQ		= PARTNER.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			LOG.PARTNER_USER_CHAR_NO	= PARTNER.USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)																				");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",pCondition.TutorialBattleFlag));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendCastTreasureData(oDataSet,pCondition);

		return oDataSet;
	}

	private void AppendCastTreasureData(DataSet pDS,GameCharacterBattleSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count < 1) {
			return;
		}

		PossessionCastTreasureSeekCondition subCondition = new PossessionCastTreasureSeekCondition();
		subCondition.SiteCd = pCondition.SiteCd;
		subCondition.UserSeq = pCondition.PartnerUserSeq;
		subCondition.UserCharNo = pCondition.PartnerUserCharNo;
		subCondition.CastTreasureSeq = pCondition.CastTreasureSeq;
		subCondition.StageGroupType = pCondition.SelfStageGroupType;

		DataSet subDs;
		using (PossessionCastTreasure oPossessionCastTreasure = new PossessionCastTreasure()) {
			subDs = oPossessionCastTreasure.GetOne(subCondition);
		}

		if (subDs.Tables[0].Rows.Count >= 1) {
			pDS.Tables[0].Rows[0]["CAST_TREASURE_SEQ"] = subDs.Tables[0].Rows[0]["CAST_TREASURE_SEQ"];
		}
	}
}
