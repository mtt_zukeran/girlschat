﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・裏クエスト
--	Progaram ID		: OtherQuest
--
--  Creation Date	: 2012.07.09
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class OtherQuestSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string QuestSeq {
		get {
			return this.Query["quest_seq"];
		}
		set {
			this.Query["quest_seq"] = value;
		}
	}

	public string OtherQuestSeq {
		get {
			return this.Query["other_quest_seq"];
		}
		set {
			this.Query["other_quest_seq"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public OtherQuestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public OtherQuestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_Cd"]));
		this.Query["quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_seq"]));
		this.Query["other_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["other_quest_seq"]));
	}
}

#endregion ============================================================================================================

public class OtherQuest:DbSession {
	public OtherQuest() {
	}

	public int GetPageCount(OtherQuestSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string pWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	COUNT(*)																	");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ														,	");
		oSqlBuilder.AppendLine("			OTHER_QUEST_SEQ													,	");
		oSqlBuilder.AppendLine("			OTHER_QUEST_NM													,	");
		oSqlBuilder.AppendLine("			REMARKS															,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_D													,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_H													,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_M														");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			VW_PW_OTHER_QUEST01	OQ												");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ		= :QUEST_SEQ									AND	");
		if (!string.IsNullOrEmpty(pCondition.OtherQuestSeq)) {
			oSqlBuilder.AppendLine("			OTHER_QUEST_SEQ	= :OTHER_QUEST_SEQ							AND	");
		}
		oSqlBuilder.AppendLine("			SEX_CD			= :SEX_CD										AND	");
		oSqlBuilder.AppendLine("			PUBLISH_FLAG	= :PUBLISH_FLAG									AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS															");
		oSqlBuilder.AppendLine("				(																");
		oSqlBuilder.AppendLine("					SELECT														");
		oSqlBuilder.AppendLine("						*														");
		oSqlBuilder.AppendLine("					FROM														");
		oSqlBuilder.AppendLine("						T_OTHER_QUEST_CLR_CNT CC								");
		oSqlBuilder.AppendLine("					WHERE														");
		oSqlBuilder.AppendLine("						CC.SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("						CC.USER_SEQ			= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("						CC.USER_CHAR_NO		= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("						CC.QUEST_SEQ		= :QUEST_SEQ					AND	");
		oSqlBuilder.AppendLine("						CC.SITE_CD			= OQ.SITE_CD					AND	");
		oSqlBuilder.AppendLine("						CC.QUEST_SEQ		= OQ.QUEST_SEQ					AND	");
		oSqlBuilder.AppendLine("						CC.OTHER_QUEST_SEQ	= OQ.OTHER_QUEST_SEQ			AND	");
		oSqlBuilder.AppendLine("						CC.CLEAR_COUNT		>= OQ.CLEAR_LIMIT					");
		oSqlBuilder.AppendLine("				)															AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS															");
		oSqlBuilder.AppendLine("				(																");
		oSqlBuilder.AppendLine("					SELECT														");
		oSqlBuilder.AppendLine("						*														");
		oSqlBuilder.AppendLine("					FROM														");
		oSqlBuilder.AppendLine("						T_DAILY_OTHER_QUEST_CLR_CNT	DCC							");
		oSqlBuilder.AppendLine("					WHERE														");
		oSqlBuilder.AppendLine("						DCC.SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("						DCC.USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("						DCC.USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("						DCC.QUEST_SEQ			= :QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("						DCC.REPORT_DAY			= :REPORT_DAY				AND	");
		oSqlBuilder.AppendLine("						DCC.SITE_CD				= OQ.SITE_CD				AND	");
		oSqlBuilder.AppendLine("						DCC.QUEST_SEQ			= OQ.QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("						DCC.OTHER_QUEST_SEQ		= OQ.OTHER_QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("						DCC.CLEAR_COUNT			>= OQ.DAILY_CLEAR_LIMIT			");
		oSqlBuilder.AppendLine("				)																");
		oSqlBuilder.AppendLine("	) LQ																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ														,	");
		oSqlBuilder.AppendLine("			OTHER_QUEST_SEQ													,	");
		oSqlBuilder.AppendLine("			ENTRY_LOG_SEQ													,	");
		oSqlBuilder.AppendLine("			QUEST_STATUS														");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_QUEST_ENTRY_LOG													");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ		= :QUEST_SEQ									AND	");
		oSqlBuilder.AppendLine("			QUEST_TYPE		= :QUEST_TYPE									AND	");
		oSqlBuilder.AppendLine("			QUEST_STATUS	= :QUEST_STATUS										");
		oSqlBuilder.AppendLine("	) QEL																		");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	LQ.SITE_CD				= QEL.SITE_CD								(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ			= QEL.QUEST_SEQ								(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.OTHER_QUEST_SEQ		= QEL.OTHER_QUEST_SEQ						(+)		");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		if (!string.IsNullOrEmpty(pCondition.OtherQuestSeq)) {
			oParamList.Add(new OracleParameter(":OTHER_QUEST_SEQ",pCondition.OtherQuestSeq));
		}
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":QUEST_TYPE",PwViCommConst.GameQuestType.OTHER_QUEST));
		oParamList.Add(new OracleParameter(":QUEST_STATUS",PwViCommConst.GameQuestEntryStatus.ENTRY));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));

		OracleParameter[] oWhereParams = oParamList.ToArray();

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(OtherQuestSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string pWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ															,	");
		oSqlBuilder.AppendLine("	LQ.OTHER_QUEST_SEQ														,	");
		oSqlBuilder.AppendLine("	LQ.OTHER_QUEST_NM														,	");
		oSqlBuilder.AppendLine("	LQ.REMARKS																,	");
		oSqlBuilder.AppendLine("	NVL(LQ.TIME_LIMIT_D,0) AS TIME_LIMIT_D									,	");
		oSqlBuilder.AppendLine("	NVL(LQ.TIME_LIMIT_H,0) AS TIME_LIMIT_H									,	");
		oSqlBuilder.AppendLine("	NVL(LQ.TIME_LIMIT_M,0) AS TIME_LIMIT_M									,	");
		oSqlBuilder.AppendLine("	QEL.ENTRY_LOG_SEQ														,	");
		oSqlBuilder.AppendLine("	QEL.QUEST_STATUS														,	");
		oSqlBuilder.AppendLine("	QUEST_TYPE																,	");
		oSqlBuilder.AppendLine("	CASE																		");
		oSqlBuilder.AppendLine("		WHEN																	");
		oSqlBuilder.AppendLine("			EXISTS																");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					1															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_QUEST_ENTRY_LOG											");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD			= LQ.SITE_CD							AND	");
		oSqlBuilder.AppendLine("					QUEST_SEQ		= LQ.QUEST_SEQ							AND	");
		oSqlBuilder.AppendLine("					QUEST_TYPE		= LQ.QUEST_TYPE							AND	");
		oSqlBuilder.AppendLine("					USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO	= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("					QUEST_STATUS	= :QUEST_STATUS								");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("		THEN 1																	");
		oSqlBuilder.AppendLine("		ELSE 0																	");
		oSqlBuilder.AppendLine("	END AS NOW_ENTRY_FLAG													,	");
		oSqlBuilder.AppendLine("	NULL AS LEVEL_QUEST_SEQ													,	");
		oSqlBuilder.AppendLine("	NULL AS LITTLE_QUEST_SEQ												,	");
		oSqlBuilder.AppendLine("	CASE																		");
		oSqlBuilder.AppendLine("		WHEN																	");
		oSqlBuilder.AppendLine("			QEL.END_DATE <= LQ.PUBLISH_END_DATE									");
		oSqlBuilder.AppendLine("		THEN																	");
		oSqlBuilder.AppendLine("			QEL.END_DATE														");
		oSqlBuilder.AppendLine("		ELSE																	");
		oSqlBuilder.AppendLine("			LQ.PUBLISH_END_DATE													");
		oSqlBuilder.AppendLine("	END AS END_DATE																");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ														,	");
		oSqlBuilder.AppendLine("			OTHER_QUEST_SEQ													,	");
		oSqlBuilder.AppendLine("			OTHER_QUEST_NM													,	");
		oSqlBuilder.AppendLine("			REMARKS															,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_D													,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_H													,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_M													,	");
		oSqlBuilder.AppendLine("			3 AS QUEST_TYPE													,	");
		oSqlBuilder.AppendLine("			PUBLISH_END_DATE													");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			VW_PW_OTHER_QUEST01	OQ												");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ		= :QUEST_SEQ									AND	");
		if (!string.IsNullOrEmpty(pCondition.OtherQuestSeq)) {
			oSqlBuilder.AppendLine("			OTHER_QUEST_SEQ	= :OTHER_QUEST_SEQ							AND	");
		}
		oSqlBuilder.AppendLine("			SEX_CD			= :SEX_CD										AND	");
		oSqlBuilder.AppendLine("			SYSDATE BETWEEN PUBLISH_START_DATE AND PUBLISH_END_DATE			AND	");
		oSqlBuilder.AppendLine("			PUBLISH_FLAG	= :PUBLISH_FLAG									AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS															");
		oSqlBuilder.AppendLine("				(																");
		oSqlBuilder.AppendLine("					SELECT														");
		oSqlBuilder.AppendLine("						*														");
		oSqlBuilder.AppendLine("					FROM														");
		oSqlBuilder.AppendLine("						T_OTHER_QUEST_CLR_CNT CC								");
		oSqlBuilder.AppendLine("					WHERE														");
		oSqlBuilder.AppendLine("						CC.SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("						CC.USER_SEQ			= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("						CC.USER_CHAR_NO		= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("						CC.QUEST_SEQ		= :QUEST_SEQ					AND	");
		oSqlBuilder.AppendLine("						CC.SITE_CD			= OQ.SITE_CD					AND	");
		oSqlBuilder.AppendLine("						CC.QUEST_SEQ		= OQ.QUEST_SEQ					AND	");
		oSqlBuilder.AppendLine("						CC.OTHER_QUEST_SEQ	= OQ.OTHER_QUEST_SEQ			AND	");
		oSqlBuilder.AppendLine("						CC.CLEAR_COUNT		>= OQ.CLEAR_LIMIT					");
		oSqlBuilder.AppendLine("				)															AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS															");
		oSqlBuilder.AppendLine("				(																");
		oSqlBuilder.AppendLine("					SELECT														");
		oSqlBuilder.AppendLine("						*														");
		oSqlBuilder.AppendLine("					FROM														");
		oSqlBuilder.AppendLine("						T_DAILY_OTHER_QUEST_CLR_CNT	DCC							");
		oSqlBuilder.AppendLine("					WHERE														");
		oSqlBuilder.AppendLine("						DCC.SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("						DCC.USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("						DCC.USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("						DCC.QUEST_SEQ			= :QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("						DCC.REPORT_DAY			= :REPORT_DAY				AND	");
		oSqlBuilder.AppendLine("						DCC.SITE_CD				= OQ.SITE_CD				AND	");
		oSqlBuilder.AppendLine("						DCC.QUEST_SEQ			= OQ.QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("						DCC.OTHER_QUEST_SEQ		= OQ.OTHER_QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("						DCC.CLEAR_COUNT			>= OQ.DAILY_CLEAR_LIMIT			");
		oSqlBuilder.AppendLine("				)																");
		oSqlBuilder.AppendLine("	) LQ																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ														,	");
		oSqlBuilder.AppendLine("			OTHER_QUEST_SEQ													,	");
		oSqlBuilder.AppendLine("			ENTRY_LOG_SEQ													,	");
		oSqlBuilder.AppendLine("			QUEST_STATUS													,	");
		oSqlBuilder.AppendLine("			END_DATE															");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_QUEST_ENTRY_LOG													");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ		= :QUEST_SEQ									AND	");
		oSqlBuilder.AppendLine("			QUEST_TYPE		= :QUEST_TYPE									AND	");
		oSqlBuilder.AppendLine("			QUEST_STATUS	= :QUEST_STATUS										");
		oSqlBuilder.AppendLine("	) QEL																		");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	LQ.SITE_CD				= QEL.SITE_CD								(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ			= QEL.QUEST_SEQ								(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.OTHER_QUEST_SEQ		= QEL.OTHER_QUEST_SEQ						(+)		");

		sSortExpression = "ORDER BY OTHER_QUEST_SEQ ASC";

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		if (!string.IsNullOrEmpty(pCondition.OtherQuestSeq)) {
			oParamList.Add(new OracleParameter(":OTHER_QUEST_SEQ",pCondition.OtherQuestSeq));
		}
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":QUEST_TYPE",PwViCommConst.GameQuestType.OTHER_QUEST));
		oParamList.Add(new OracleParameter(":QUEST_STATUS",PwViCommConst.GameQuestEntryStatus.ENTRY));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneForInformation(OtherQuestSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	OTHER_QUEST_NM									");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	VW_PW_OTHER_QUEST01								");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ			= :QUEST_SEQ			AND	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_SEQ		= :OTHER_QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("	SEX_CD				= :SEX_CD					");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		oParamList.Add(new OracleParameter(":OTHER_QUEST_SEQ",pCondition.OtherQuestSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return oDataSet;
	}
}
