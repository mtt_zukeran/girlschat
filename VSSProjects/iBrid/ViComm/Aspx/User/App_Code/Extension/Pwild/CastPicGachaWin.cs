﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: お宝ガチャ当たり
--	Progaram ID		: CastPicGachaWin
--
--  Creation Date	: 2013.02.07
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class CastPicGachaWin:DbSession {
	public CastPicGachaWin()
		: base() {
	}

	public void UseCastPicGachaWin(string pSiteCd,string pUserSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USE_CAST_PIC_GACHA_WIN");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}