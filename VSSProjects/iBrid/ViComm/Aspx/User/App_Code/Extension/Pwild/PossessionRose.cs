﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class PossessionRoseSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public PossessionRoseSeekCondition()
		: this(new NameValueCollection()) {
	}

	public PossessionRoseSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
	}
}
#endregion ============================================================================================================

public class PossessionRose:DbSession {
	public PossessionRose() {
	}

	public string GetRoseExpirationDate(PossessionRoseSeekCondition pCondition) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	PR.MIN_GET_DATE + SG.ROSE_EXPIRATION_HOURS / 24 AS ROSE_EXPIRATION_DATE		");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			MIN(GET_DATE) AS MIN_GET_DATE										");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_POSSESSION_ROSE													");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO										");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD														");
		oSqlBuilder.AppendLine("	) PR																	,	");
		oSqlBuilder.AppendLine("	T_SOCIAL_GAME SG															");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	PR.SITE_CD		= SG.SITE_CD												");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			return oDataSet.Tables[0].Rows[0]["ROSE_EXPIRATION_DATE"].ToString();
		} else {
			return string.Empty;
		}
	}
}
