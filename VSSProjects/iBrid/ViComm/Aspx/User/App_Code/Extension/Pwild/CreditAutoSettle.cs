﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: クレジット自動決済設定データクラス
--	Progaram ID		: CreditAutoSettle
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class CreditAutoSettleSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;

	public CreditAutoSettleSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CreditAutoSettleSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class CreditAutoSettle:DbSession {
	public CreditAutoSettle() {
	}

	public DataSet GetPageCollection(CreditAutoSettleSeekCondition pCondtion) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	P.SITE_CD											,	");
		oSqlBuilder.AppendLine("	P.CREDIT_AUTO_SETTLE_SEQ							,	");
		oSqlBuilder.AppendLine("	P.SETTLE_POINT										,	");
		oSqlBuilder.AppendLine("	P.SETTLE_AMT										,	");
		oSqlBuilder.AppendLine("	CASE													");
		oSqlBuilder.AppendLine("		WHEN SS.USER_SEQ IS NOT NULL						");
		oSqlBuilder.AppendLine("		THEN 1												");
		oSqlBuilder.AppendLine("		ELSE 0												");
		oSqlBuilder.AppendLine("	END AS EXIST_FLAG									,	");
		oSqlBuilder.AppendLine("	SS.NEXT_SETTLE_DATE										");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_CREDIT_AUTO_SETTLE P								,	");
		oSqlBuilder.AppendLine("	T_CREDIT_AUTO_SETTLE_STATUS SS							");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY P.SITE_CD,P.SETTLE_AMT,P.CREDIT_AUTO_SETTLE_SEQ	");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CreditAutoSettleSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.SITE_CD = SS.SITE_CD (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.CREDIT_AUTO_SETTLE_SEQ = SS.CREDIT_AUTO_SETTLE_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(":SELF_USER_SEQ = SS.USER_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(":UNAVAILABLE_FLAG = SS.UNAVAILABLE_FLAG (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(":RESIGN_FLAG = SS.RESIGN_FLAG (+)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":UNAVAILABLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":RESIGN_FLAG",ViCommConst.FLAG_OFF_STR));

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		return oParamList.ToArray();
	}
	
	public DataSet GetOne(string pSiteCd,string pCreditAutoSettleSeq) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	SETTLE_AMT											,	");
		oSqlBuilder.AppendLine("	SETTLE_POINT											");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_CREDIT_AUTO_SETTLE									");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	CREDIT_AUTO_SETTLE_SEQ	= :CREDIT_AUTO_SETTLE_SEQ		");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CREDIT_AUTO_SETTLE_SEQ",pCreditAutoSettleSeq));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}
