﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 会員登録拒否メールアドレス
--	Progaram ID		: ManRejectEmailAddr
--  Creation Date	: 2015.05.20
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class ManRejectEmailAddr:DbSession {
	public ManRejectEmailAddr() {
	}

	public bool IsExist(string pTempRegistId) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_REJECT_EMAIL_ADDR");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	LAST_TEMP_REGIST_ID = :LAST_TEMP_REGIST_ID");

		oParamList.Add(new OracleParameter(":LAST_TEMP_REGIST_ID",pTempRegistId));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
