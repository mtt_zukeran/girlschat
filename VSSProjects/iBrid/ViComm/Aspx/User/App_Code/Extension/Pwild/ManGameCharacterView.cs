﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性ｹﾞｰﾑｷｬﾗｸﾀｰ詳細
--	Progaram ID		: ManGameCharacterView
--
--  Creation Date	: 2011.09.27
--  Creater			: PW A.Taba
--
**************************************************************************/

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;

using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

[Serializable]
public class ManGameCharacterViewSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public ManGameCharacterViewSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManGameCharacterViewSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
	}
}

[System.Serializable]
public class ManGameCharacterView:ManBase {
	public ManGameCharacterView()
		: base() {
	}
	public ManGameCharacterView(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ManGameCharacterViewSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P	");
		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManGameCharacterViewSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "				,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE				,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL				,	");
		oSqlBuilder.AppendLine("	P.FELLOW_COUNT						,	");
		oSqlBuilder.AppendLine("	P.TOTAL_WIN_COUNT					,	");
		oSqlBuilder.AppendLine("	P.TOTAL_LOSS_COUNT					,	");
		oSqlBuilder.AppendLine("	P.TOTAL_PARTY_WIN_COUNT				,	");
		oSqlBuilder.AppendLine("	P.TOTAL_PARTY_LOSS_COUNT			,	");
		oSqlBuilder.AppendLine("	P.SITE_USE_STATUS						");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P			");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		AppendFellow(oDataSet,pCondtion);
		AppendGameCommunication(oDataSet,pCondtion);
		AppendFriendlyPoint(oDataSet,pCondtion);
		AppendFavorite(oDataSet,pCondtion);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManGameCharacterViewSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	private void AppendFellow(DataSet pDS,ManGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("FELLOW_APPLICATION_STATUS"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	FELLOW_APPLICATION_STATUS								");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_GAME_FELLOW											");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND ");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",dr["USER_CHAR_NO"]);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["FELLOW_APPLICATION_STATUS"] = dsSub.Tables[0].Rows[0]["FELLOW_APPLICATION_STATUS"];
				}
			}
		}
	}

	private void AppendGameCommunication(DataSet pDS,ManGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("LAST_HUG_DATE"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	LAST_HUG_DATE											");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION									");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND ");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",dr["USER_CHAR_NO"]);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["LAST_HUG_DATE"] = dsSub.Tables[0].Rows[0]["LAST_HUG_DATE"];
				}
			}
		}
	}

	private void AppendFriendlyPoint(DataSet pDS,ManGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("FRIENDLY_RANK"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	RANK													");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"]);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",pCondition.UserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["FRIENDLY_RANK"] = dsSub.Tables[0].Rows[0]["RANK"];
				} else {
					dr["FRIENDLY_RANK"] = null;
				}
			}
		}
	}

	private void AppendFavorite(DataSet pDS,ManGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("GAME_FAVORITE"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	SITE_CD													");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_GAME_FAVORIT											");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",dr["USER_CHAR_NO"]);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["GAME_FAVORITE"] = ViCommConst.FLAG_ON_STR;
				} else {
					dr["GAME_FAVORITE"] = ViCommConst.FLAG_OFF_STR;
				}
			}
		}
	}
}

