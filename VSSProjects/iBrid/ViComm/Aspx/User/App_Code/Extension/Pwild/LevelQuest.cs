﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・レベルクエスト
--	Progaram ID		: LevelQuest
--
--  Creation Date	: 2012.07.09
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class LevelQuestSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string QuestSeq {
		get {
			return this.Query["quest_seq"];
		}
		set {
			this.Query["quest_seq"] = value;
		}
	}
	
	public string LevelQuestSeq {
		get {
			return this.Query["level_quest_seq"];
		}
		set {
			this.Query["level_quest_seq"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public LevelQuestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public LevelQuestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_seq"]));
		this.Query["level_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["level_quest_seq"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
	}
}

#endregion ============================================================================================================

public class LevelQuest:DbSession {
	public LevelQuest() {
	}

	public DataSet GetOne(LevelQuestSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ											,	");
		oSqlBuilder.AppendLine("	LQ.LEVEL_QUEST_SEQ										,	");
		oSqlBuilder.AppendLine("	LQ.QUEST_LEVEL											,	");
		oSqlBuilder.AppendLine("	LQ.QUEST_EX_REMARKS AS REMARKS							,	");
		oSqlBuilder.AppendLine("	QEL.ENTRY_LOG_SEQ										,	");
		oSqlBuilder.AppendLine("	NVL(QEL.QUEST_STATUS,0) AS QUEST_STATUS					,	");
		oSqlBuilder.AppendLine("	2 AS QUEST_TYPE											,	");
		oSqlBuilder.AppendLine("	NULL AS LITTLE_QUEST_SEQ								,	");
		oSqlBuilder.AppendLine("	NULL AS OTHER_QUEST_SEQ									,	");
		oSqlBuilder.AppendLine("	CASE														");
		oSqlBuilder.AppendLine("		WHEN													");
		oSqlBuilder.AppendLine("			QEL.END_DATE <= LQ.PUBLISH_END_DATE					");
		oSqlBuilder.AppendLine("		THEN													");
		oSqlBuilder.AppendLine("			QEL.END_DATE										");
		oSqlBuilder.AppendLine("		ELSE													");
		oSqlBuilder.AppendLine("			LQ.PUBLISH_END_DATE									");
		oSqlBuilder.AppendLine("	END AS END_DATE												");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	VW_PW_LEVEL_QUEST01 LQ									,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ										,	");
		oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ									,	");
		oSqlBuilder.AppendLine("			ENTRY_LOG_SEQ									,	");
		oSqlBuilder.AppendLine("			QUEST_STATUS									,	");
		oSqlBuilder.AppendLine("			END_DATE											");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_QUEST_ENTRY_LOG									");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ			= :QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("			QUEST_TYPE			= :QUEST_TYPE					");
		oSqlBuilder.AppendLine("	) QEL														");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	LQ.SITE_CD			= QEL.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ		= QEL.QUEST_SEQ					(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.LEVEL_QUEST_SEQ	= QEL.LEVEL_QUEST_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ		= :QUEST_SEQ						AND	");
		oSqlBuilder.AppendLine("	LQ.SEX_CD			= :SEX_CD							AND	");
		oSqlBuilder.AppendLine("	LQ.PUBLISH_FLAG		= :PUBLISH_FLAG						AND	");
		oSqlBuilder.AppendLine("	SYSDATE BETWEEN LQ.PUBLISH_START_DATE AND LQ.PUBLISH_END_DATE	AND");
		oSqlBuilder.AppendLine("	LQ.QUEST_LEVEL		=										");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			NVL(MAX(QUEST_LEVEL),0) + 1							");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			VW_PW_LEVEL_QUEST_CLR_HST01							");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ			= :QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("			QUEST_EX_END_FLAG	= :QUEST_EX_END_FLAG			");
		oSqlBuilder.AppendLine("	)															");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":QUEST_TYPE",PwViCommConst.GameQuestType.EX_QUEST));
		oParamList.Add(new OracleParameter(":QUEST_EX_END_FLAG",ViCommConst.FLAG_ON_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	public DataSet GetOneForInformation(LevelQuestSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	QUEST_SEQ												,	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ											,	");
		oSqlBuilder.AppendLine("	QUEST_LEVEL												,	");
		oSqlBuilder.AppendLine("	QUEST_EX_REMARKS AS REMARKS									");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	VW_PW_LEVEL_QUEST01											");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ			= :QUEST_SEQ						AND	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ		= :LEVEL_QUEST_SEQ					AND	");
		oSqlBuilder.AppendLine("	SEX_CD				= :SEX_CD							AND	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG		= :PUBLISH_FLAG							");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
