﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエストクリア条件
--	Progaram ID		: QuestTrial
--
--  Creation Date	: 2012.07.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class QuestTrialSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string QuestSeq {
		get {
			return this.Query["quest_seq"];
		}
		set {
			this.Query["quest_seq"] = value;
		}
	}
	
	public string QuestType {
		get {
			return this.Query["quest_type"];
		}
		set {
			this.Query["quest_type"] = value;
		}
	}

	public string LevelQuestSeq {
		get {
			return this.Query["level_quest_seq"];
		}
		set {
			this.Query["level_quest_seq"] = value;
		}
	}

	public string LittleQuestSeq {
		get {
			return this.Query["little_quest_seq"];
		}
		set {
			this.Query["little_quest_seq"] = value;
		}
	}
	
	public string OtherQuestSeq {
		get {
			return this.Query["other_quest_seq"];
		}
		set {
			this.Query["other_quest_seq"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public QuestTrialSeekCondition()
		: this(new NameValueCollection()) {
	}

	public QuestTrialSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_Cd"]));
		this.Query["quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_seq"]));
		this.Query["quest_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_type"]));
		this.Query["level_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["level_quest_seq"]));
		this.Query["little_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["little_quest_seq"]));
		this.Query["other_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["other_quest_seq"]));
	}
}

#endregion ============================================================================================================

public class QuestTrial:DbSession {
	public QuestTrial() {
	}

	public DataSet GetPageCollectionQuestTrialStatus(QuestTrialSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string pWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	NVL(QT.QUEST_TRIAL_COUNT,0) - NVL(QTS.EXEC_COUNT,0) AS REST_COUNT		,	");
		oSqlBuilder.AppendLine("	QT.COUNT_UNIT															,	");
		oSqlBuilder.AppendLine("	QTS.CLEAR_FLAG																");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ														,	");
		oSqlBuilder.AppendLine("			QUEST_TRIAL_SEQ													,	");
		oSqlBuilder.AppendLine("			QUEST_TRIAL_COUNT												,	");
		oSqlBuilder.AppendLine("			COUNT_UNIT															");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_QUEST_TRIAL														");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ		= :QUEST_SEQ									AND	");

		if (pCondition.QuestType.Equals(PwViCommConst.GameQuestType.LEVEL_QUEST)) {
			oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ			= :LEVEL_QUEST_SEQ					AND	");
			oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
		} else if (pCondition.QuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
			oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ			= :LEVEL_QUEST_SEQ					AND	");
			oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
			oSqlBuilder.AppendLine("			LITTLE_QUEST_SEQ		= :LITTLE_QUEST_SEQ					AND");
			oParamList.Add(new OracleParameter(":LITTLE_QUEST_SEQ",pCondition.LittleQuestSeq));
		} else if (pCondition.QuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
			oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ			= :LEVEL_QUEST_SEQ					AND	");
			oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
		} else if (pCondition.QuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
			oSqlBuilder.AppendLine("			OTHER_QUEST_SEQ			= :OTHER_QUEST_SEQ					AND	");
			oParamList.Add(new OracleParameter(":OTHER_QUEST_SEQ",pCondition.OtherQuestSeq));
		}
		oSqlBuilder.AppendLine("		QUEST_TYPE			= :QUEST_TYPE										");
		oSqlBuilder.AppendLine("	) QT																	,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			QUEST_TRIAL_SEQ													,	");
		oSqlBuilder.AppendLine("			EXEC_COUNT														,	");
		oSqlBuilder.AppendLine("			CLEAR_FLAG															");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_QUEST_TRIAL_STATUS QTS											");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ										AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO									AND	");
		oSqlBuilder.AppendLine("			EXISTS																");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_QUEST_ENTRY_LOG QEL										");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD				= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("					USER_SEQ			= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO		= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("					QUEST_SEQ			= :QUEST_SEQ						AND	");
		oSqlBuilder.AppendLine("					QUEST_STATUS		= :QUEST_STATUS						AND	");
		oSqlBuilder.AppendLine("					QTS.SITE_CD			= QEL.SITE_CD						AND	");
		oSqlBuilder.AppendLine("					QTS.USER_SEQ		= QEL.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("					QTS.USER_CHAR_NO	= QEL.USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("					QTS.ENTRY_LOG_SEQ	= QEL.ENTRY_LOG_SEQ						");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("	) QTS																		");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	QT.SITE_CD			= QTS.SITE_CD										AND	");
		oSqlBuilder.AppendLine("	QT.QUEST_TRIAL_SEQ	= QTS.QUEST_TRIAL_SEQ									");

		sSortExpression = "ORDER BY QT.QUEST_TRIAL_SEQ ASC";

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		oParamList.Add(new OracleParameter(":QUEST_TYPE",pCondition.QuestType));
		oParamList.Add(new OracleParameter(":QUEST_STATUS",PwViCommConst.GameQuestEntryStatus.ENTRY));

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
}
