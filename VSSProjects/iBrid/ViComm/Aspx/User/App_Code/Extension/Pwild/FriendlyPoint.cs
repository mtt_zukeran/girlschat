﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾚﾝﾄﾞﾘｰﾎﾟｲﾝﾄ
--	Progaram ID		: FriendlyPoint
--
--  Creation Date	: 2011.09.05
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class FriendlyPointSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}


	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public string Rank {
		get {
			return this.Query["rank"];
		}
		set {
			this.Query["rank"] = value;
		}
	}

	public FriendlyPointSeekCondition()
		: this(new NameValueCollection()) {
	}

	public FriendlyPointSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
	}
}

[System.Serializable]
public class FriendlyPoint:DbSession {

	public int GetFriendlyRank(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	RANK													");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				this.cmd.Parameters.Add(":PARTNER_USER_SEQ",pPartnerUserSeq);
				this.cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["RANK"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return int.Parse(sValue);
	}

	public string GetFriendlyPoint(FriendlyPointSeekCondition pCondition) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	FRIENDLY_POINT											");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_FRIENDLY_POINT										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");
		oSqlBuilder.AppendLine(" ORDER BY FRIENDLY_POINT_SEQ DESC				 			");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				this.cmd.Parameters.Add(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq);
				this.cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["FRIENDLY_POINT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public string GetTotalFriendlyPoint(FriendlyPointSeekCondition pCondition) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	SUM(FRIENDLY_POINT)		AS TOTAL_FRIENDLY_POINT			");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_FRIENDLY_POINT										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");
		oSqlBuilder.AppendLine(" ORDER BY FRIENDLY_POINT_SEQ DESC				 			");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				this.cmd.Parameters.Add(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq);
				this.cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["TOTAL_FRIENDLY_POINT"].ToString());

					if (string.IsNullOrEmpty(sValue)) {
						sValue = "0";
					}
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public String GetSweetHeartCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = string.Empty;

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	COUNT(*) AS FRIENDLY_COUNT								");
		oSqlBuilder.AppendLine("FROM 														");
		oSqlBuilder.AppendLine("( SELECT 													");
		oSqlBuilder.AppendLine("	* 														");
		oSqlBuilder.AppendLine("	FROM VW_PW_CAST_GAME_CHARACTER00 P 						");
		this.CreateWhere(ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(")P,		  													");
		oSqlBuilder.AppendLine("( SELECT 													");
		oSqlBuilder.AppendLine("   * 														");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	RANK					= :RANK							");
		oSqlBuilder.AppendLine(" ) FRIENDLY_POINT											");
		oSqlBuilder.AppendLine("WHERE			 											");
		oSqlBuilder.AppendLine("	P.SITE_CD		= FRIENDLY_POINT.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ		= FRIENDLY_POINT.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO	= FRIENDLY_POINT.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS														");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			*														");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_REFUSE												");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	)																");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				this.cmd.Parameters.Add(":RANK",1);
				this.cmd.Parameters.Add(":STAFF_FLAG",ViCommConst.FLAG_OFF);
				this.cmd.Parameters.Add(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":SITE_USE_STATUS",ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["FRIENDLY_COUNT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public String GetWomanSweetHeartCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = string.Empty;

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	COUNT(*) AS FRIENDLY_COUNT								");
		oSqlBuilder.AppendLine("FROM 														");
		oSqlBuilder.AppendLine("( SELECT 													");
		oSqlBuilder.AppendLine("	* 														");
		oSqlBuilder.AppendLine("	FROM VW_PW_MAN_GAME_CHARACTER00 P 						");
		this.CreateWhere(ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(")P,		  													");
		oSqlBuilder.AppendLine("( SELECT 													");
		oSqlBuilder.AppendLine("   * 														");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	RANK					= :RANK							");
		oSqlBuilder.AppendLine(" ) FRIENDLY_POINT											");
		oSqlBuilder.AppendLine("WHERE			 											");
		oSqlBuilder.AppendLine(" P.SITE_CD		= FRIENDLY_POINT.SITE_CD		    AND     ");
		oSqlBuilder.AppendLine(" P.USER_SEQ		= FRIENDLY_POINT.USER_SEQ			AND     ");
		oSqlBuilder.AppendLine(" P.USER_CHAR_NO	= FRIENDLY_POINT.USER_CHAR_NO			    ");


		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				this.cmd.Parameters.Add(":RANK",1);
				this.cmd.Parameters.Add(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				this.cmd.Parameters.Add(":SITE_USE_STATUS",ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["FRIENDLY_COUNT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	private void CreateWhere(ref string pWhereClause) {

		pWhereClause = pWhereClause ?? string.Empty;

		SysPrograms.SqlAppendWhere(" P.STAFF_FLAG = :STAFF_FLAG",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);

		SysPrograms.SqlAppendWhere("SITE_USE_STATUS = :SITE_USE_STATUS",ref pWhereClause);

		SysPrograms.SqlAppendWhere(string.Format(" P.NA_FLAG IN({0},{1})",ViCommConst.NaFlag.OK,ViCommConst.NaFlag.NO_AUTH),ref pWhereClause);
	}

	public DataSet GetHerSweetHeartData(FriendlyPointSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	GC.USER_SEQ															,	");
		oSqlBuilder.AppendLine("	GC.USER_CHAR_NO														,	");
		oSqlBuilder.AppendLine("	GC.GAME_HANDLE_NM														");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			SITE_CD														,	");
		oSqlBuilder.AppendLine("			USER_SEQ													,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO													");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00												");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			RANK					= :RANK								AND	");
		oSqlBuilder.AppendLine("			NOT(															");
		oSqlBuilder.AppendLine("				USER_SEQ			= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("				USER_CHAR_NO		= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("			)																");
		oSqlBuilder.AppendLine("	) FP																,	");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 GC											");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	FP.SITE_CD							= GC.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	FP.USER_SEQ							= GC.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	FP.USER_CHAR_NO						= GC.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG					AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG						AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG			AND	");
		oSqlBuilder.AppendLine("	GC.NA_FLAG							IN(:NA_FLAG,:NA_FLAG2)						AND	");
		oSqlBuilder.AppendLine("	GC.STAFF_FLAG						= :STAFF_FLAG									");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":RANK",pCondition.Rank));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF));

		// order by
		string sSortExpression = string.Empty;
		sSortExpression = " ORDER BY DBMS_RANDOM.RANDOM";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
}