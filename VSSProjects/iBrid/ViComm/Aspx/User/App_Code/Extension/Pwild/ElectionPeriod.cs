﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 投票設定
--	Progaram ID		: ElectionPeriod
--  Creation Date	: 2013.10.18
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class ElectionPeriod:DbSession {
	public ElectionPeriod() {
	}

	public DataSet GetCurrent(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	*												");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		SITE_CD									,	");
		oSqlBuilder.AppendLine("		ELECTION_PERIOD_SEQ						,	");
		oSqlBuilder.AppendLine("		FIX_RANK_FLAG							,	");
		oSqlBuilder.AppendLine("		FIRST_PERIOD_END_DATE						");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		T_ELECTION_PERIOD							");
		oSqlBuilder.AppendLine("	WHERE											");
		oSqlBuilder.AppendLine("		SITE_CD					= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("		FIRST_PERIOD_START_DATE	<= SYSDATE			");
		oSqlBuilder.AppendLine("	ORDER BY										");
		oSqlBuilder.AppendLine("		SITE_CD,FIRST_PERIOD_START_DATE DESC		");
		oSqlBuilder.AppendLine("	)												");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	ROWNUM = 1										");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneBySeq(string pSiteCd,string pElectionPeriodSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	SITE_CD											,	");
		oSqlBuilder.AppendLine("	ELECTION_PERIOD_SEQ								,	");
		oSqlBuilder.AppendLine("	FIX_RANK_FLAG										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	T_ELECTION_PERIOD									");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	ELECTION_PERIOD_SEQ		= :ELECTION_PERIOD_SEQ		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":ELECTION_PERIOD_SEQ",pElectionPeriodSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetRecentPast(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	*												");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		SITE_CD									,	");
		oSqlBuilder.AppendLine("		ELECTION_PERIOD_SEQ						,	");
		oSqlBuilder.AppendLine("		FIX_RANK_FLAG								");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		T_ELECTION_PERIOD							");
		oSqlBuilder.AppendLine("	WHERE											");
		oSqlBuilder.AppendLine("		SITE_CD					= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("		SECOND_PERIOD_END_DATE	<= SYSDATE			");
		oSqlBuilder.AppendLine("	ORDER BY										");
		oSqlBuilder.AppendLine("		SITE_CD,FIRST_PERIOD_START_DATE DESC		");
		oSqlBuilder.AppendLine("	)												");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	ROWNUM = 1										");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetVotable(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	*												");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	(SELECT											");
		oSqlBuilder.AppendLine("		SITE_CD									,	");
		oSqlBuilder.AppendLine("		ELECTION_PERIOD_SEQ						,	");
		oSqlBuilder.AppendLine("		FIRST_PERIOD_START_DATE					,	");
		oSqlBuilder.AppendLine("		FIRST_PERIOD_END_DATE					,	");
		oSqlBuilder.AppendLine("		SECOND_PERIOD_START_DATE				,	");
		oSqlBuilder.AppendLine("		SECOND_PERIOD_END_DATE					,	");
		oSqlBuilder.AppendLine("		TICKET_POINT							,	");
		oSqlBuilder.AppendLine("		FIX_RANK_FLAG							,	");
		oSqlBuilder.AppendLine("		FIX_RANK_DATE								");
		oSqlBuilder.AppendLine("	FROM											");
		oSqlBuilder.AppendLine("		T_ELECTION_PERIOD							");
		oSqlBuilder.AppendLine("	WHERE											");
		oSqlBuilder.AppendLine("		SITE_CD					= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("		FIRST_PERIOD_START_DATE	<= SYSDATE		AND	");
		oSqlBuilder.AppendLine("		SECOND_PERIOD_END_DATE	>= SYSDATE			");
		oSqlBuilder.AppendLine("	ORDER BY										");
		oSqlBuilder.AppendLine("		SITE_CD,FIRST_PERIOD_START_DATE DESC		");
		oSqlBuilder.AppendLine("	)												");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	ROWNUM = 1										");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetListPast(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	ELECTION_PERIOD_NM						,	");
		oSqlBuilder.AppendLine("	ELECTION_PERIOD_SEQ							");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_ELECTION_PERIOD							");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	SECOND_PERIOD_END_DATE	< SYSDATE			");
		oSqlBuilder.AppendLine("ORDER BY										");
		oSqlBuilder.AppendLine("	SITE_CD,FIRST_PERIOD_START_DATE DESC		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetCurrentEntry(string pSiteCd,string pCastUserSeq,string pCastCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	*																	");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	(SELECT																");
		oSqlBuilder.AppendLine("		P.SITE_CD													,	");
		oSqlBuilder.AppendLine("		P.ELECTION_PERIOD_SEQ										,	");
		oSqlBuilder.AppendLine("		P.FIX_RANK_FLAG												,	");
		oSqlBuilder.AppendLine("		P.ENTRY_START_DATE											,	");
		oSqlBuilder.AppendLine("		P.ENTRY_END_DATE											,	");
		oSqlBuilder.AppendLine("		P.FIRST_PERIOD_START_DATE									,	");
		oSqlBuilder.AppendLine("		P.FIRST_PERIOD_END_DATE										,	");
		oSqlBuilder.AppendLine("		P.SECOND_PERIOD_START_DATE									,	");
		oSqlBuilder.AppendLine("		P.SECOND_PERIOD_END_DATE									,	");
		oSqlBuilder.AppendLine("		CASE															");
		oSqlBuilder.AppendLine("			WHEN EE.ELECTION_ENTRY_SEQ IS NOT NULL AND EE.RETIRE_FLAG = 0");
		oSqlBuilder.AppendLine("			THEN 1														");
		oSqlBuilder.AppendLine("			ELSE 0														");
		oSqlBuilder.AppendLine("		END AS ELECTION_ENTRY_FLAG									,	");
		oSqlBuilder.AppendLine("		NVL(EE.RETIRE_FLAG,0) AS RETIRE_FLAG						,	");
		oSqlBuilder.AppendLine("		NVL(EE.REFUSE_FLAG,0) AS REFUSE_FLAG							");
		oSqlBuilder.AppendLine("	FROM																");
		oSqlBuilder.AppendLine("		T_ELECTION_PERIOD P											,	");
		oSqlBuilder.AppendLine("		(																");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				SITE_CD												,	");
		oSqlBuilder.AppendLine("				ELECTION_PERIOD_SEQ									,	");
		oSqlBuilder.AppendLine("				ELECTION_ENTRY_SEQ									,	");
		oSqlBuilder.AppendLine("				RETIRE_FLAG											,	");
		oSqlBuilder.AppendLine("				REFUSE_FLAG												");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_ELECTION_ENTRY										");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("				CAST_USER_SEQ	= :CAST_USER_SEQ					AND	");
		oSqlBuilder.AppendLine("				CAST_CHAR_NO	= :CAST_CHAR_NO							");
		oSqlBuilder.AppendLine("		) EE															");
		oSqlBuilder.AppendLine("	WHERE																");
		oSqlBuilder.AppendLine("		P.SITE_CD				= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("		P.ENTRY_START_DATE		<= SYSDATE							AND	");
		oSqlBuilder.AppendLine("		P.SITE_CD				= EE.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("		P.ELECTION_PERIOD_SEQ	= EE.ELECTION_PERIOD_SEQ		(+)		");
		oSqlBuilder.AppendLine("	ORDER BY															");
		oSqlBuilder.AppendLine("		P.SITE_CD,P.FIRST_PERIOD_START_DATE DESC						");
		oSqlBuilder.AppendLine("	)																	");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	ROWNUM = 1															");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCastCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public void GetElectionVoteTicketCount(
		string pSiteCd,
		string pManUserSeq,
		out int pTicketCount
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_ELECTION_VOTE_TICKET_COUNT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureOutParm("pTICKET_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pTicketCount = db.GetIntValue("pTICKET_COUNT");
		}
	}

	public void AddElectionVoteTicketByPt(
		string pSiteCd,
		string pManUserSeq,
		int pAddCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_ELECTION_VOTE_TICKET_BY_PT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pADD_COUNT",DbSession.DbType.NUMBER,pAddCount);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void AddElectionVoteTicketByCd(
		string pSiteCd,
		string pManUserSeq,
		string pCodeStr,
		out int pAddCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_ELECTION_VOTE_TICKET_BY_CD");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pCODE_STR",DbSession.DbType.VARCHAR2,pCodeStr);
			db.ProcedureOutParm("pADD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pAddCount = db.GetIntValue("pADD_COUNT");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
