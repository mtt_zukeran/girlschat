﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 会員つぶやき
--	Progaram ID		: ManTweet
--
--  Creation Date	: 2013.01.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class ManTweet:DbSession {
	public ManTweet() {
	}

	public void WriteManTweet(string pSiteCd,string pUserSeq,string pUserCharNo,string pTweetText,out string pManTweetSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_MAN_TWEET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pTWEET_TEXT",DbSession.DbType.VARCHAR2,pTweetText);
			db.ProcedureOutParm("pMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pManTweetSeq = db.GetStringValue("pMAN_TWEET_SEQ");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void RegistManTweetLike(string pSiteCd,string pUserSeq,string pUserCharNo,string pManTweetSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_MAN_TWEET_LIKE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,pManTweetSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void DeleteManTweet(string pSiteCd,string pUserSeq,string pUserCharNo,string pManTweetSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_MAN_TWEET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,pManTweetSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public DateTime? GetLastTweetDate(string pSiteCd,string pUserSeq) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	MAX(TWEET_DATE) AS LAST_TWEET_DATE		");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_MAN_TWEET								");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ	= :USER_SEQ					");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (!string.IsNullOrEmpty(oDataSet.Tables[0].Rows[0]["LAST_TWEET_DATE"].ToString())) {
			return (DateTime)oDataSet.Tables[0].Rows[0]["LAST_TWEET_DATE"];
		} else {
			return null;
		}
	}
	
	public int GetTweetCntEachDay(string pSiteCd,string pUserSeq,DateTime pTargetDate) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	COUNT(*) AS TODAY_TWEET_CNT				");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_MAN_TWEET								");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ	= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine(" 	TWEET_DATE 	>= TRUNC(:TARGET_DATE)	AND ");
		oSqlBuilder.AppendLine(" 	TWEET_DATE 	< TRUNC(:TARGET_DATE) + 1	");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":TARGET_DATE",pTargetDate));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return int.Parse(oDataSet.Tables[0].Rows[0]["TODAY_TWEET_CNT"].ToString());
	}
	
	public DataSet GetOne(string pSiteCd,string pManTweetSeq) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	USER_SEQ								,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO								");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_MAN_TWEET									");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	MAN_TWEET_SEQ	= :MAN_TWEET_SEQ			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAN_TWEET_SEQ",pManTweetSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}