﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: メールアタックリスト (3分以内メール返信ボーナス)
--	Progaram ID		: ManMailAttackLimit2
--  Creation Date	: 2016.09.05
--  Creater			: M&TT Zukeran
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManMailAttackLimit2SeekCondition:SeekConditionBase {
	public string SiteCd;
	public string MailResBonusSeq;
	public string ManUserSeq;
	public string ManMailSeq;
	public string CastUserSeq;
	public string CastUserCharNo;
	public string CastMailSeq;
	public string UnreceivedDays;
	public string ResBonusMaxNum;
	public string ResLimitSec1;
	public string ResLimitSec2;
	public string MainFlag;  // 3分以内メール返信ボーナス対象（1:3分以内、0:10分以内）

	public ManMailAttackLimit2SeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManMailAttackLimit2SeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[Serializable]
public class ManMailAttackLimit2:DbSession {
	public ManMailAttackLimit2() {
	}

	/// <summary>
	/// 検索結果の総件数を取得
	/// </summary>
	/// <param name="pCondition"></param>
	/// <param name="pRecPerPage"></param>
	/// <param name="pRecCount"></param>
	/// <returns></returns>
	public int GetPageCount(ManMailAttackLimit2SeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sTbl = GetTable(pCondition,ref oParamList,ref sWhereClause);
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine(sTbl);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	/// <summary>
	/// 検索結果のデータ取得
	/// </summary>
	/// <param name="pCondition"></param>
	/// <param name="pPageNo"></param>
	/// <param name="pRecPerPage"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(ManMailAttackLimit2SeekCondition pCondition,int pPageNo,int pRecPerPage) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sInnerTbl = GetTable(pCondition,ref oParamList,ref sWhereClause);
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		sSortExpression = this.CreateOrderExpression(pCondition);
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T1.*,");
		oSqlBuilder.AppendLine("	AT1.DISPLAY_VALUE AS MAN_ATTR_VALUE01,");
		oSqlBuilder.AppendLine("	AT8.DISPLAY_VALUE AS MAN_ATTR_VALUE08,");
		oSqlBuilder.AppendLine("	AT11.DISPLAY_VALUE AS MAN_ATTR_VALUE11,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	CASE");
		oSqlBuilder.AppendLine("		WHEN (T1.REGIST_DATE >= TO_CHAR(SYSDATE - SM.MAN_NEW_FACE_DAYS,'YYYY/MM/DD')) THEN");
		oSqlBuilder.AppendLine("			1");
		oSqlBuilder.AppendLine("		ELSE");
		oSqlBuilder.AppendLine("			0");
		oSqlBuilder.AppendLine("	END AS NEW_MAN_FLAG,");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			NVL(COUNT(USER_SEQ),0) AS CNT");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_FAVORIT");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			SITE_CD						= T1.SITE_CD");
		oSqlBuilder.AppendLine("			AND USER_SEQ				= T1.USER_SEQ");
		oSqlBuilder.AppendLine("			AND USER_CHAR_NO			= T1.USER_CHAR_NO");
		oSqlBuilder.AppendLine("			AND PARTNER_USER_SEQ		= :SELF_USER_SEQ");
		oSqlBuilder.AppendLine("			AND PARTNER_USER_CHAR_NO	= :SELF_CHAR_NO");
		oSqlBuilder.AppendLine("	) AS LIKE_ME_FLAG,");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			NVL(COUNT(USER_SEQ),0) AS CNT");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_FAVORIT");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			SITE_CD						= T1.SITE_CD");
		oSqlBuilder.AppendLine("			AND	USER_SEQ				= :SELF_USER_SEQ");
		oSqlBuilder.AppendLine("			AND	USER_CHAR_NO			= :SELF_CHAR_NO");
		oSqlBuilder.AppendLine("			AND	PARTNER_USER_SEQ		= T1.USER_SEQ");
		oSqlBuilder.AppendLine("			AND	PARTNER_USER_CHAR_NO	= T1.USER_CHAR_NO");
		oSqlBuilder.AppendLine("	) AS FAVORIT_FLAG,");
		oSqlBuilder.AppendLine("	CASE");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 0 THEN");
		oSqlBuilder.AppendLine("			SM.OFFLINE_GUIDANCE");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 1 THEN");
		oSqlBuilder.AppendLine("			SM.LOGINED_GUIDANCE");
		oSqlBuilder.AppendLine("		WHEN T1.CHARACTER_ONLINE_STATUS = 2 THEN");
		oSqlBuilder.AppendLine("			SM.WAITING_GUIDANCE");
		oSqlBuilder.AppendLine("		WHEN (T1.CHARACTER_ONLINE_STATUS = 3) THEN");
		oSqlBuilder.AppendLine("			SM.TALKING_WSHOT_GUIDANCE");
		oSqlBuilder.AppendLine("	END AS MOBILE_ONLINE_STATUS_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			I1.*,");
		oSqlBuilder.AppendLine("			ROWNUM AS RNUM");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			(");
		oSqlBuilder.AppendLine("				SELECT");
		oSqlBuilder.AppendLine("					P.*,");
		oSqlBuilder.AppendLine("					NVL(C.TOTAL_RX_MAIL_COUNT,0) AS PARTNER_TOTAL_RX_MAIL_COUNT,");
		oSqlBuilder.AppendLine("					C.LAST_RX_MAIL_DATE AS PARTNER_LAST_RX_MAIL_DATE");
		oSqlBuilder.AppendLine("				FROM");
		oSqlBuilder.AppendLine(sInnerTbl);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("			) I1");
		oSqlBuilder.AppendLine("	) T1");
		oSqlBuilder.AppendLine("	INNER JOIN T_SITE_MANAGEMENT SM");
		oSqlBuilder.AppendLine("		ON T1.SITE_CD = SM.SITE_CD");
		// プロフィール項目 (地域)
		oSqlBuilder.AppendLine("	LEFT JOIN VW_USER_MAN_ATTR_VALUE01 AT1");
		oSqlBuilder.AppendLine("		ON T1.SITE_CD		= AT1.SITE_CD");
		oSqlBuilder.AppendLine("		AND T1.USER_SEQ		= AT1.USER_SEQ");
		oSqlBuilder.AppendLine("		AND T1.USER_CHAR_NO	= AT1.USER_CHAR_NO");
		oSqlBuilder.AppendLine("		AND AT1.ITEM_NO		= :ITEM_NO01");
		// プロフィール項目 (好きなタイプ)
		oSqlBuilder.AppendLine("	LEFT JOIN VW_USER_MAN_ATTR_VALUE01 AT8");
		oSqlBuilder.AppendLine("		ON T1.SITE_CD		= AT8.SITE_CD");
		oSqlBuilder.AppendLine("		AND T1.USER_SEQ		= AT8.USER_SEQ");
		oSqlBuilder.AppendLine("		AND T1.USER_CHAR_NO	= AT8.USER_CHAR_NO");
		oSqlBuilder.AppendLine("		AND AT8.ITEM_NO		= :ITEM_NO08");
		// プロフィール項目 (sex経験)
		oSqlBuilder.AppendLine("	LEFT JOIN VW_USER_MAN_ATTR_VALUE01 AT11");
		oSqlBuilder.AppendLine("		ON T1.SITE_CD		= AT11.SITE_CD");
		oSqlBuilder.AppendLine("		AND T1.USER_SEQ		= AT11.USER_SEQ");
		oSqlBuilder.AppendLine("		AND T1.USER_CHAR_NO	= AT11.USER_CHAR_NO");
		oSqlBuilder.AppendLine("		AND AT11.ITEM_NO	= :ITEM_NO11");
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":SELF_CHAR_NO",pCondition.CastUserCharNo));
		oParamList.Add(new OracleParameter(":ITEM_NO01","01"));
		oParamList.Add(new OracleParameter(":ITEM_NO08","08"));
		oParamList.Add(new OracleParameter(":ITEM_NO11","11"));

		// offset, limit
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	T1.RNUM > :FIRST_ROW");
		oSqlBuilder.AppendLine("	AND T1.RNUM <= :LAST_ROW");
		oParamList.Add(new OracleParameter(":LAST_ROW",iStartIndex + pRecPerPage));
		oParamList.Add(new OracleParameter(":FIRST_ROW",iStartIndex));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	/// <summary>
	/// 検索条件を生成
	/// </summary>
	/// <param name="pCondition"></param>
	/// <param name="pWhereClause"></param>
	/// <returns></returns>
	private OracleParameter[] CreateWhere(ManMailAttackLimit2SeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 閲覧しているサイト
		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		// テストユーザ以外
		SysPrograms.SqlAppendWhere("P.ADMIN_FLAG = :ADMIN_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":ADMIN_FLAG",ViCommConst.FLAG_OFF));

		// 拒否されていない男性会員
		SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND PARTNER_USER_SEQ = :REFUSE_CAST_USER_SEQ)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":REFUSE_CAST_USER_SEQ",pCondition.CastUserSeq));

		// ｷｬﾗｸﾀｰ状態 OK
		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		StringBuilder oSqlBuilder2 = new StringBuilder();
		if (ViCommConst.FLAG_ON_STR.Equals(pCondition.MainFlag)) {
			
			oSqlBuilder2.AppendLine("P.USER_RANK = :USER_RANK");
			oSqlBuilder2.AppendLine("AND (");
			oSqlBuilder2.AppendLine("	P.NON_EXIST_MAIL_ADDR_FLAG = :NON_EXIST_MAIL_ADDR_FLAG");
			oSqlBuilder2.AppendLine("	OR P.CHARACTER_ONLINE_STATUS != :CHARACTER_ONLINE_STATUS");
			oSqlBuilder2.AppendLine(")");
			oSqlBuilder2.AppendLine("AND (");
			oSqlBuilder2.AppendLine("	C.PARTNER_USER_SEQ IS NULL");
			oSqlBuilder2.AppendLine("	OR (");
			oSqlBuilder2.AppendLine("		C.PARTNER_USER_SEQ = :PARTNER_USER_SEQ");
			oSqlBuilder2.AppendLine("		AND (");
			oSqlBuilder2.AppendLine("			C.LAST_TX_MAIL_DATE <= (SYSDATE - NUMTODSINTERVAL(:UNRECEIVED_DAYS, 'DAY'))");
			oSqlBuilder2.AppendLine("			OR C.LAST_TX_MAIL_DATE IS NULL");
			oSqlBuilder2.AppendLine("		)");
			oSqlBuilder2.AppendLine("	)");
			oSqlBuilder2.AppendLine("	OR (");
			oSqlBuilder2.AppendLine("		MRBL.MAIL_RES_NUM = 1");
			oSqlBuilder2.AppendLine("		AND MRBL.CAST_MAIL_SEND_DATE IS NULL");
			oSqlBuilder2.AppendLine("		AND MRBL.CAST_WAIT_TX_FLAG = 0");
			oSqlBuilder2.AppendLine("		AND (SYSDATE - NUMTODSINTERVAL(:RES_LIMIT_SEC1, 'SECOND')) <= MRBL.MAN_MAIL_SEND_DATE");
			oSqlBuilder2.AppendLine("	)");
			oSqlBuilder2.AppendLine(")");

			// 1通もメールを送信していない男性会員
			// or 1週間以上、自分に対してメールを送信していない男性会員
			// or 1通目のメール受信後、3分以内で未返信
			SysPrograms.SqlAppendWhere(oSqlBuilder2.ToString(),ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_RANK",ViCommConst.USER_RANK_B));
			oParamList.Add(new OracleParameter(":NON_EXIST_MAIL_ADDR_FLAG",ViCommConst.MAIL_ADDR_OK));
			oParamList.Add(new OracleParameter(":CHARACTER_ONLINE_STATUS",ViCommConst.USER_OFFLINE));
			oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.CastUserSeq));
			oParamList.Add(new OracleParameter(":UNRECEIVED_DAYS",pCondition.UnreceivedDays));
			oParamList.Add(new OracleParameter(":RES_LIMIT_SEC1",pCondition.ResLimitSec1));
		} else if (ViCommConst.FLAG_OFF_STR.Equals(pCondition.MainFlag)) {
			SysPrograms.SqlAppendWhere("MRBL.MAIL_RES_BONUS_SEQ = :MAIL_RES_BONUS_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAIL_RES_BONUS_SEQ",pCondition.MailResBonusSeq));

			oSqlBuilder2.AppendLine("(");
			oSqlBuilder2.AppendLine("	(");
			oSqlBuilder2.AppendLine("		MRBL.MAIL_RES_NUM BETWEEN 2 AND :RES_BONUS_MAX_NUM - 1");
			oSqlBuilder2.AppendLine("	) OR (");
			oSqlBuilder2.AppendLine("		MRBL.MAIL_RES_NUM = 1");
			oSqlBuilder2.AppendLine("		AND MRBL.MAN_MAIL_SEND_DATE < (SYSDATE - NUMTODSINTERVAL(:RES_LIMIT_SEC1, 'SECOND'))");
			oSqlBuilder2.AppendLine("	) OR (");
			oSqlBuilder2.AppendLine("		MRBL.MAIL_RES_NUM = :RES_BONUS_MAX_NUM");
			oSqlBuilder2.AppendLine("		AND MRBL.CAST_MAIL_SEND_DATE IS NULL");
			oSqlBuilder2.AppendLine("		AND MRBL.CAST_WAIT_TX_FLAG = 0");
			oSqlBuilder2.AppendLine("		AND (SYSDATE - NUMTODSINTERVAL(:RES_LIMIT_SEC2, 'SECOND')) <= MRBL.MAN_MAIL_SEND_DATE");
			oSqlBuilder2.AppendLine("	)");
			oSqlBuilder2.AppendLine(")");

			// 返信数がn通未満 or n通目のメール受信後、10分以内で未返信
			SysPrograms.SqlAppendWhere(oSqlBuilder2.ToString(),ref pWhereClause);
			oParamList.Add(new OracleParameter(":RES_BONUS_MAX_NUM",pCondition.ResBonusMaxNum));
			oParamList.Add(new OracleParameter(":RES_LIMIT_SEC1",pCondition.ResLimitSec1));
			oParamList.Add(new OracleParameter(":RES_LIMIT_SEC2",pCondition.ResLimitSec2));

			// 閲覧している出演者
			SysPrograms.SqlAppendWhere("C.PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.CastUserSeq));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// ソート条件を生成
	/// </summary>
	/// <param name="pCondition"></param>
	/// <returns></returns>
	private string CreateOrderExpression(ManMailAttackLimit2SeekCondition pCondition) {
		string sOrder = string.Empty;
		switch (pCondition.Sort) {
			// 最終ログイン日時降順
			case PwViCommConst.MailResBonusAttackSort.LAST_LOGIN_DATE_DESC:
				sOrder = "ORDER BY P.LAST_LOGIN_DATE DESC NULLS LAST";
				break;
			// 最終メール受信日時降順、最終ログイン日時降順
			case PwViCommConst.MailResBonusAttackSort.LAST_RX_MAIL_DATE_DESC:
			default:
				sOrder = "ORDER BY C.LAST_TX_MAIL_DATE DESC NULLS LAST, P.LAST_LOGIN_DATE DESC NULLS LAST";
				break;
		}
		return sOrder;
	}

	/// <summary>
	/// 参照用テーブルを取得
	/// </summary>
	/// <returns></returns>
	private string GetTable(ManMailAttackLimit2SeekCondition pCondition,ref List<OracleParameter> oParamList,ref string pWhereClause) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("VW_USER_MAN_CHARACTER00 P");
		if (ViCommConst.FLAG_ON_STR.Equals(pCondition.MainFlag)) {
			// アタックリスト（3分以内メール返信）用
			// (受信履歴が存在していなくてもよい)
			oSqlBuilder.AppendLine("LEFT JOIN ");
		} else if (ViCommConst.FLAG_OFF_STR.Equals(pCondition.MainFlag)) {
			// アタックリスト（10分以内メール返信）用
			// (1通以上、n通未満の返信履歴が必要)
			oSqlBuilder.AppendLine("INNER JOIN ");
		}
		oSqlBuilder.AppendLine("T_COMMUNICATION C");
		oSqlBuilder.AppendLine("	ON P.SITE_CD = C.SITE_CD");
		oSqlBuilder.AppendLine("		AND P.USER_SEQ = C.USER_SEQ");
		oSqlBuilder.AppendLine("		AND C.PARTNER_USER_SEQ = :CAST_USER_SEQ");
		if (ViCommConst.FLAG_ON_STR.Equals(pCondition.MainFlag)) {
			// アタックリスト（3分以内メール返信）用
			// (1通のみ受信は履歴が存在していても問題ない)
			oSqlBuilder.AppendLine("LEFT JOIN ");
		} else if (ViCommConst.FLAG_OFF_STR.Equals(pCondition.MainFlag)) {
			// アタックリスト（10分以内メール返信）用
			// (1通以上、n通未満の返信履歴が必要)
			oSqlBuilder.AppendLine("INNER JOIN ");
		}
		oSqlBuilder.AppendLine("(");
		oSqlBuilder.AppendLine("	SELECT");
		oSqlBuilder.AppendLine("		IMRBL2.*");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		(");
		oSqlBuilder.AppendLine("			SELECT");
		oSqlBuilder.AppendLine("				SITE_CD,");
		oSqlBuilder.AppendLine("				MAIL_RES_BONUS_SEQ,");
		oSqlBuilder.AppendLine("				MAX(MAIL_RES_NUM) AS MAIL_RES_NUM,");
		oSqlBuilder.AppendLine("				MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("				CAST_USER_SEQ");
		oSqlBuilder.AppendLine("			FROM");
		oSqlBuilder.AppendLine("				T_MAIL_RES_BONUS_LOG2");
		oSqlBuilder.AppendLine("			WHERE");
		oSqlBuilder.AppendLine("				MAIL_RES_BONUS_SEQ = :MAIL_RES_BONUS_SEQ");
		oSqlBuilder.AppendLine("				AND CAST_USER_SEQ = :CAST_USER_SEQ");
		oSqlBuilder.AppendLine("			GROUP BY");
		oSqlBuilder.AppendLine("				SITE_CD, MAIL_RES_BONUS_SEQ, MAN_USER_SEQ, CAST_USER_SEQ");
		oSqlBuilder.AppendLine("		) IMRBL1,");
		oSqlBuilder.AppendLine("		T_MAIL_RES_BONUS_LOG2 IMRBL2");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		IMRBL1.SITE_CD = IMRBL2.SITE_CD");
		oSqlBuilder.AppendLine("		AND IMRBL1.MAIL_RES_BONUS_SEQ = IMRBL2.MAIL_RES_BONUS_SEQ");
		oSqlBuilder.AppendLine("		AND IMRBL1.MAIL_RES_NUM = IMRBL2.MAIL_RES_NUM");
		oSqlBuilder.AppendLine("		AND IMRBL1.MAN_USER_SEQ = IMRBL2.MAN_USER_SEQ");
		oSqlBuilder.AppendLine("		AND IMRBL1.CAST_USER_SEQ = IMRBL2.CAST_USER_SEQ");
		oSqlBuilder.AppendLine(") MRBL");
		oSqlBuilder.AppendLine("	ON C.SITE_CD = MRBL.SITE_CD");
		oSqlBuilder.AppendLine("		AND C.USER_SEQ = MRBL.MAN_USER_SEQ");
		oSqlBuilder.AppendLine("		AND C.PARTNER_USER_SEQ = MRBL.CAST_USER_SEQ");
		oParamList.Add(new OracleParameter(":MAIL_RES_BONUS_SEQ",pCondition.MailResBonusSeq));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		return oSqlBuilder.ToString();
	}
}
