﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 未送信メール
--	Progaram ID		: DraftMail
--
--  Creation Date	: 2013.09.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class DraftMail:DbSession {
	public DraftMail(){
	}

	public void MainteDraftMail(
		string pSiteCd,
		string pTxUserSeq,
		string pTxUserCharNo,
		string pTxSexCd,
		string pRxUserSeq,
		string pRxUserCharNo,
		string pDraftMailSeq,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		string pMailTemplateNo,
		int pReturnMailFlag,
		string pReturnOrgMailSeq
	) {
		string sMailSeq = string.Empty;
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(pMailDoc,ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAINTE_DRAFT_MAIL");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pTX_USER_SEQ",DbSession.DbType.VARCHAR2,pTxUserSeq);
			db.ProcedureInParm("pTX_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pTxUserCharNo);
			db.ProcedureInParm("pTX_SEX_CD",DbSession.DbType.VARCHAR2,pTxSexCd);
			db.ProcedureInParm("pRX_USER_SEQ",DbSession.DbType.VARCHAR2,pRxUserSeq);
			db.ProcedureInParm("pRX_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pRxUserCharNo);
			db.ProcedureInParm("pDRAFT_MAIL_SEQ",DbSession.DbType.VARCHAR2,pDraftMailSeq);
			db.ProcedureInParm("pMAIL_TITLE",DbSession.DbType.VARCHAR2,pMailTitle);
			db.ProcedureInArrayParm("pMAIL_DOC",DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("pMAIL_DOC_COUNT",DbType.NUMBER,iDocCount);
			db.ProcedureInParm("pATTACHED_OBJ_TYPE",DbType.VARCHAR2,pAttachedObjType);
			db.ProcedureInParm("pMAIL_TEMPLATE_NO",DbType.VARCHAR2,pMailTemplateNo);
			db.ProcedureInParm("pRETURN_MAIL_FLAG",DbType.NUMBER,pReturnMailFlag);
			db.ProcedureInParm("pRETURN_ORG_MAIL_SEQ",DbType.VARCHAR2,pReturnOrgMailSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			string sResult = db.GetStringValue("pRESULT");
		}
	}

	public void DeleteDraftMail(string pSiteCd,string pTxUserSeq,string pTxUserCharNo,string[] pDraftMailSeqList) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_DRAFT_MAIL");
			db.ProcedureInParm("pSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pTX_USER_SEQ",DbType.VARCHAR2,pTxUserSeq);
			db.ProcedureInParm("pTX_USER_CHAR_NO",DbType.VARCHAR2,pTxUserCharNo);
			db.ProcedureInArrayParm("pDRAFT_MAIL_SEQ",DbType.VARCHAR2,pDraftMailSeqList.Length,pDraftMailSeqList);
			db.ProcedureOutParm("pRESULT",DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}