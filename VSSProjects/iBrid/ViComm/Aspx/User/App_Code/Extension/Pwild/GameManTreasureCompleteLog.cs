﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ男性お宝ｺﾝﾌﾟﾛｸﾞ
--	Progaram ID		: GameManTreasureCompleteLog
--
--  Creation Date	: 2011.09.22
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class GameManTreasureCompleteLogSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string SelfRank {
		get {
			return this.Query["self_rank"];
		}
		set {
			this.Query["self_rank"] = value;
		}
	}

	public GameManTreasureCompleteLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameManTreasureCompleteLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["position"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["position"]));
		this.Query["self_rank"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_rank"]));
	}
}

[System.Serializable]
public class GameManTreasureCompleteLog:DbSession {

	public int GetPageCount(GameManTreasureCompleteLogSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	VW_PW_GAME_MAN_TREA_COMP_LOG01	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameManTreasureCompleteLogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	USER_SEQ				,		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			,		");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM			,		");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE		,		");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_LEVEL	,		");
		oSqlBuilder.AppendLine("	COMPLETE_DATE					");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_GAME_MAN_TREA_COMP_LOG01 	");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		AppendRank(oDataSet,iStartIndex);
		return oDataSet;
	}

	private void AppendRank(DataSet pDS,int pStartIndex) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		int iCount = pStartIndex;
		col = new DataColumn(string.Format("GAME_RANK"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			iCount++;
			dr["GAME_RANK"] = iCount;
		}
	}

	private OracleParameter[] CreateWhere(GameManTreasureCompleteLogSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		string sToday = DateTime.Now.ToString("yyyy/MM/dd");

		SysPrograms.SqlAppendWhere(" DISPLAY_DAY = :DISPLAY_DAY",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",sToday));
		
		SysPrograms.SqlAppendWhere(" STAFF_FLAG = :STAFF_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		
		SysPrograms.SqlAppendWhere(" NA_FLAG IN(:NA_FLAG,:NA_FLAG2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_FLAG = :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_TREASURE_FLAG = :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_FLAG = :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_TREASURE_FLAG = :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(GameManTreasureCompleteLogSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = string.Format("ORDER BY COMPLETE_DATE ");
		return sSortExpression;
	}

	public int GetMyPageCount(GameManTreasureCompleteLogSeekCondition pCondition,int pRecPerPage) {

		decimal iRecCount = 0;
		int iCount = 0;
		string sSortExpression = string.Empty;

		string sToday = DateTime.Now.ToString("yyyy/MM/dd");
		
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD			,				");
		oSqlBuilder.AppendLine("	USER_SEQ		,				");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_GAME_MAN_TREA_COMP_LOG01	");
		oSqlBuilder.AppendLine(" WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY	= :DISPLAY_DAY	AND	");
		oSqlBuilder.AppendLine("	STAFF_FLAG	= :STAFF_FLAG	AND	");
		oSqlBuilder.AppendLine("	NA_FLAG		IN(:NA_FLAG,:NA_FLAG2)	AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);
		oSqlBuilder.AppendLine(sSortExpression);
		
		using (DataSet ds = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":DISPLAY_DAY",sToday);
				cmd.Parameters.Add(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":NA_FLAG",ViCommConst.NaFlag.OK);
				cmd.Parameters.Add(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH);
				cmd.Parameters.Add(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR);
				
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}

			foreach (DataRow dr in ds.Tables[0].Rows) {
				iCount++;
				if (dr["SITE_CD"].ToString() == pCondition.SiteCd &&
				    dr["USER_SEQ"].ToString() == pCondition.UserSeq &&
				    dr["USER_CHAR_NO"].ToString() == pCondition.UserCharNo) {
					iRecCount = iCount;
					break;
				}
			}
		}

		return (int)Math.Ceiling(iRecCount / pRecPerPage);
	}
}

