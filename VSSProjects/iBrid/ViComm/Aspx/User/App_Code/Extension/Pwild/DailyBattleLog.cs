﻿using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class DailyBattleLog:DbSession {
	public DailyBattleLog() {
	}

	public string CheckAttackCountOver(string sSiteCd,string sUserSeq,string sUserCharNo,string sPartnerUserSeq,string sPartnerUserCharNo) {
		string sAttackCountOverFlag = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT																						");
		oSqlBuilder.AppendLine("	CASE																					");
		oSqlBuilder.AppendLine("		WHEN LOG.ATTACK_COUNT >= T_SOCIAL_GAME.SAME_USER_ATTACK_LIMIT_COUNT	");
		oSqlBuilder.AppendLine("		THEN 1																				");
		oSqlBuilder.AppendLine("		ELSE 0																				");
		oSqlBuilder.AppendLine("	END AS ATTACK_COUNT_OVER_FLAG															");
		oSqlBuilder.AppendLine("FROM																						");
		oSqlBuilder.AppendLine("	(																						");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			SITE_CD																		,	");
		oSqlBuilder.AppendLine("			ATTACK_COUNT																	");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			T_DAILY_BATTLE_LOG																");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD											AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :PARTNER_USER_SEQ									AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY				= :REPORT_DAY											");
		oSqlBuilder.AppendLine("	) LOG																				,	");
		oSqlBuilder.AppendLine("	T_SOCIAL_GAME																			");
		oSqlBuilder.AppendLine("WHERE																						");
		oSqlBuilder.AppendLine("	LOG.SITE_CD	= T_SOCIAL_GAME.SITE_CD														");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",sPartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",sPartnerUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			sAttackCountOverFlag = oDataSet.Tables[0].Rows[0]["ATTACK_COUNT_OVER_FLAG"].ToString();
		}
		
		return sAttackCountOverFlag;
	}
}