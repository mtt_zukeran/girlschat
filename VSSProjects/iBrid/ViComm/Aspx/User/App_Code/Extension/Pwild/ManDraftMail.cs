﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 未送信メール（宛先が会員）
--	Progaram ID		: ManDraftMail
--
--  Creation Date	: 2013.09.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class ManDraftMailSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string TxUserSeq;
	public string TxUserCharNo;

	public ManDraftMailSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManDraftMailSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

#endregion ============================================================================================================

public class ManDraftMail:ManBase {
	public ManDraftMail()
		: base() {
	}
	public ManDraftMail(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public DataSet GetPageCollection(ManDraftMailSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "						,	");
		oSqlBuilder.AppendLine("	NG_SKULL_COUNT								,	");
		oSqlBuilder.AppendLine("	P.DRAFT_MAIL_SEQ							,	");
		oSqlBuilder.AppendLine("	P.MAIL_TITLE								,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC1									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC2									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC3									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC4									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC5									,	");
		oSqlBuilder.AppendLine("	P.ATTACHED_OBJ_TYPE							,	");
		oSqlBuilder.AppendLine("	P.MAIL_TEMPLATE_NO							,	");
		oSqlBuilder.AppendLine("	P.RETURN_MAIL_FLAG							,	");
		oSqlBuilder.AppendLine("	P.RETURN_ORG_MAIL_SEQ						,	");
		oSqlBuilder.AppendLine("	P.LAST_UPDATE_DATE								");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_MAN_DRAFT_MAIL00 P							");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.LAST_UPDATE_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManDraftMailSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" P.TX_USER_SEQ		= :TX_USER_SEQ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TX_USER_SEQ",pCondition.TxUserSeq));

		SysPrograms.SqlAppendWhere(" P.TX_USER_CHAR_NO		= :TX_USER_CHAR_NO",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TX_USER_CHAR_NO",pCondition.TxUserCharNo));

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :TX_USER_SEQ AND PARTNER_USER_CHAR_NO = :TX_USER_CHAR_NO)",ref pWhereClause);

		return oParamList.ToArray();
	}

	public DataSet GetOne(string pSiteCd,string pTxUserSeq,string pTxUserCharNo,string pDraftMailSeq) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	USER_SEQ									,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine("	HANDLE_NM									,	");
		oSqlBuilder.AppendLine("	P.DRAFT_MAIL_SEQ							,	");
		oSqlBuilder.AppendLine("	P.MAIL_TITLE								,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC1									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC2									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC3									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC4									,	");
		oSqlBuilder.AppendLine("	P.MAIL_DOC5									,	");
		oSqlBuilder.AppendLine("	P.MAIL_TEMPLATE_NO							,	");
		oSqlBuilder.AppendLine("	P.RETURN_MAIL_FLAG							,	");
		oSqlBuilder.AppendLine("	P.RETURN_ORG_MAIL_SEQ						,	");
		oSqlBuilder.AppendLine("	P.LAST_UPDATE_DATE							,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID										");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	VW_MAN_DRAFT_MAIL00 P							");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	TX_USER_SEQ			= :TX_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	TX_USER_CHAR_NO		= :TX_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	DRAFT_MAIL_SEQ		= :DRAFT_MAIL_SEQ			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":TX_USER_SEQ",pTxUserSeq));
		oParamList.Add(new OracleParameter(":TX_USER_CHAR_NO",pTxUserCharNo));
		oParamList.Add(new OracleParameter(":DRAFT_MAIL_SEQ",pDraftMailSeq));

		DataSet oDataSeq = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSeq;
	}
}