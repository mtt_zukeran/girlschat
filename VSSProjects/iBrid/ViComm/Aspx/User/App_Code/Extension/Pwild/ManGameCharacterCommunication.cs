﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 恋キャバ パトロン一覧
--	Progaram ID		: ManGameCharacterCommunication
--
--  Creation Date	: 2012.10.09
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

[Serializable]
public class ManGameCharacterCommunicationSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string SelfUserSeq {
		get {
			return this.Query["self_user_seq"];
		}
		set {
			this.Query["self_user_seq"] = value;
		}
	}

	public string SelfUserCharNo {
		get {
			return this.Query["self_user_char_no"];
		}
		set {
			this.Query["self_user_char_no"] = value;
		}
	}

	public ManGameCharacterCommunicationSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManGameCharacterCommunicationSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["self_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_seq"]));
		this.Query["self_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_char_no"]));
	}
}

[System.Serializable]
public class ManGameCharacterCommunication:ManBase {
	public ManGameCharacterCommunication()
		: base() {
	}

	public ManGameCharacterCommunication(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ManGameCharacterCommunicationSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	COUNT(*)																							");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P																	,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			SITE_CD																					,	");
		oSqlBuilder.AppendLine("			USER_SEQ																				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO																			,	");
		oSqlBuilder.AppendLine("			USE_MOSAIC_ERASE_COUNT																	,	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT																	,	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT_CHARGE															,	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT - USE_MOVIE_TICKET_COUNT_CHARGE AS USE_MOVIE_TICKET_COUNT_FREE	,	");
		oSqlBuilder.AppendLine("			PRESENT_COUNT																			,	");
		oSqlBuilder.AppendLine("			DATE_COUNT																				,	");
		oSqlBuilder.AppendLine("			CHARGE_DATE_COUNT																		,	");
		oSqlBuilder.AppendLine("			DATE_COUNT - CHARGE_DATE_COUNT AS FREE_DATE_COUNT											");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION																		");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO												");
		oSqlBuilder.AppendLine("	) GCO																							,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			SITE_CD																					,	");
		oSqlBuilder.AppendLine("			USER_SEQ																				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO																			,	");
		oSqlBuilder.AppendLine("			SUM(POSSESSION_COUNT) AS TOTAL_PIC_POSSESSION_COUNT											");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_POSSESSION_MAN_TREASURE PMT																");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD																	AND	");
		oSqlBuilder.AppendLine("			EXISTS																						");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					*																					");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					T_MAN_TREASURE																		");
		oSqlBuilder.AppendLine("				WHERE																					");
		oSqlBuilder.AppendLine("					SITE_CD				= PMT.SITE_CD												AND	");
		oSqlBuilder.AppendLine("					CAST_GAME_PIC_SEQ	= PMT.CAST_GAME_PIC_SEQ										AND	");
		oSqlBuilder.AppendLine("					USER_SEQ			= :SELF_USER_SEQ											AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO		= :SELF_USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("					PUBLISH_FLAG		= :PUBLISH_FLAG													");
		oSqlBuilder.AppendLine("			)																							");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO															");
		oSqlBuilder.AppendLine("	) PMT																							,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			SITE_CD																					,	");
		oSqlBuilder.AppendLine("			USER_SEQ																				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO																			,	");
		oSqlBuilder.AppendLine("			RANK AS FRIENDLY_RANK																	,	");
		oSqlBuilder.AppendLine("			FRIENDLY_POINT																				");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00																			");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO												");
		oSqlBuilder.AppendLine("	) FP																								");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	P.SITE_CD							= :SITE_CD													AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG							IN(:NA_FLAG,:NA_FLAG2)										AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG						= :STAFF_FLAG												AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG									AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG							AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG										AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG							AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD							= GCO.SITE_CD											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= GCO.USER_SEQ											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= GCO.USER_CHAR_NO										(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD							= PMT.SITE_CD											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= PMT.USER_SEQ											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= PMT.USER_CHAR_NO										(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD							= FP.SITE_CD											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= FP.USER_SEQ											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= FP.USER_CHAR_NO										(+)	AND	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		NVL(GCO.USE_MOSAIC_ERASE_COUNT,0)		> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(GCO.USE_MOVIE_TICKET_COUNT,0)		> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(GCO.PRESENT_COUNT,0)				> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(GCO.DATE_COUNT,0)					> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(PMT.TOTAL_PIC_POSSESSION_COUNT,0)	> 0														");
		oSqlBuilder.AppendLine("	)																								AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																							");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			*																							");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_REFUSE																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD														AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ													AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO												");
		oSqlBuilder.AppendLine("	)																									");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManGameCharacterCommunicationSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("		" + ManBasicField() + "																		,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM																				,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE																			,	");
		oSqlBuilder.AppendLine("	FP.FRIENDLY_RANK																				,	");
		oSqlBuilder.AppendLine("	NVL(GCO.USE_MOSAIC_ERASE_COUNT,0) AS USE_MOSAIC_ERASE_COUNT										,	");
		oSqlBuilder.AppendLine("	NVL(GCO.USE_MOVIE_TICKET_COUNT,0) AS USE_MOVIE_TICKET_COUNT										,	");
		oSqlBuilder.AppendLine("	NVL(GCO.USE_MOVIE_TICKET_COUNT_CHARGE,0) AS USE_MOVIE_TICKET_COUNT_CHARGE						,	");
		oSqlBuilder.AppendLine("	NVL(GCO.USE_MOVIE_TICKET_COUNT_FREE,0) AS USE_MOVIE_TICKET_COUNT_FREE							,	");
		oSqlBuilder.AppendLine("	NVL(GCO.PRESENT_COUNT,0) AS PRESENT_COUNT														,	");
		oSqlBuilder.AppendLine("	NVL(GCO.CHARGE_DATE_COUNT,0) AS CHARGE_DATE_COUNT												,	");
		oSqlBuilder.AppendLine("	NVL(GCO.FREE_DATE_COUNT,0) AS FREE_DATE_COUNT													,	");
		oSqlBuilder.AppendLine("	NVL(PMT.TOTAL_PIC_POSSESSION_COUNT,0) AS TOTAL_PIC_POSSESSION_COUNT									");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	VW_PW_MAN_GAME_CHARACTER00 P																	,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			SITE_CD																					,	");
		oSqlBuilder.AppendLine("			USER_SEQ																				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO																			,	");
		oSqlBuilder.AppendLine("			USE_MOSAIC_ERASE_COUNT																	,	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT																	,	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT_CHARGE															,	");
		oSqlBuilder.AppendLine("			USE_MOVIE_TICKET_COUNT - USE_MOVIE_TICKET_COUNT_CHARGE AS USE_MOVIE_TICKET_COUNT_FREE	,	");
		oSqlBuilder.AppendLine("			PRESENT_COUNT																			,	");
		oSqlBuilder.AppendLine("			DATE_COUNT																				,	");
		oSqlBuilder.AppendLine("			CHARGE_DATE_COUNT																		,	");
		oSqlBuilder.AppendLine("			DATE_COUNT - CHARGE_DATE_COUNT AS FREE_DATE_COUNT											");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_GAME_COMMUNICATION																		");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO												");
		oSqlBuilder.AppendLine("	) GCO																							,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			SITE_CD																					,	");
		oSqlBuilder.AppendLine("			USER_SEQ																				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO																			,	");
		oSqlBuilder.AppendLine("			SUM(POSSESSION_COUNT) AS TOTAL_PIC_POSSESSION_COUNT											");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_POSSESSION_MAN_TREASURE PMT																");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD																	AND	");
		oSqlBuilder.AppendLine("			EXISTS																						");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					*																					");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					T_MAN_TREASURE																		");
		oSqlBuilder.AppendLine("				WHERE																					");
		oSqlBuilder.AppendLine("					SITE_CD				= PMT.SITE_CD												AND	");
		oSqlBuilder.AppendLine("					CAST_GAME_PIC_SEQ	= PMT.CAST_GAME_PIC_SEQ										AND	");
		oSqlBuilder.AppendLine("					USER_SEQ			= :SELF_USER_SEQ											AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO		= :SELF_USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("					PUBLISH_FLAG		= :PUBLISH_FLAG													");
		oSqlBuilder.AppendLine("			)																							");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO															");
		oSqlBuilder.AppendLine("	) PMT																							,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			SITE_CD																					,	");
		oSqlBuilder.AppendLine("			USER_SEQ																				,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO																			,	");
		oSqlBuilder.AppendLine("			RANK AS FRIENDLY_RANK																	,	");
		oSqlBuilder.AppendLine("			FRIENDLY_POINT																				");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00																			");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO												");
		oSqlBuilder.AppendLine("	) FP																								");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	P.SITE_CD							= :SITE_CD													AND	");
		oSqlBuilder.AppendLine("	P.NA_FLAG							IN(:NA_FLAG,:NA_FLAG2)										AND	");
		oSqlBuilder.AppendLine("	P.STAFF_FLAG						= :STAFF_FLAG												AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG									AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG							AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG										AND	");
		oSqlBuilder.AppendLine("	P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG							AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD							= GCO.SITE_CD											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= GCO.USER_SEQ											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= GCO.USER_CHAR_NO										(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD							= PMT.SITE_CD											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= PMT.USER_SEQ											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= PMT.USER_CHAR_NO										(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD							= FP.SITE_CD											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							= FP.USER_SEQ											(+)	AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						= FP.USER_CHAR_NO										(+)	AND	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		NVL(GCO.USE_MOSAIC_ERASE_COUNT,0)		> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(GCO.USE_MOVIE_TICKET_COUNT,0)		> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(GCO.PRESENT_COUNT,0)				> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(GCO.DATE_COUNT,0)					> 0													OR	");
		oSqlBuilder.AppendLine("		NVL(PMT.TOTAL_PIC_POSSESSION_COUNT,0)	> 0														");
		oSqlBuilder.AppendLine("	)																								AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																							");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			*																							");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_REFUSE																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD														AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ													AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ												AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO												");
		oSqlBuilder.AppendLine("	)																									");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondtion.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondtion.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondtion.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		string sSortExpression = this.CreateOrderExpresion(pCondtion);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private string CreateOrderExpresion(ManGameCharacterCommunicationSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.GameManCommunicationSort.SORT_FRIENDLY_RANK:
				sSortExpression = " ORDER BY FP.FRIENDLY_RANK ASC NULLS LAST,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
				break;
			case PwViCommConst.GameManCommunicationSort.SORT_PRESENT_COUNT:
				sSortExpression = " ORDER BY GCO.PRESENT_COUNT DESC NULLS LAST,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
				break;
			case PwViCommConst.GameManCommunicationSort.SORT_USE_MOSAIC_ERASE:
				sSortExpression = " ORDER BY GCO.USE_MOSAIC_ERASE_COUNT DESC NULLS LAST,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
				break;
			case PwViCommConst.GameManCommunicationSort.SORT_TOTAL_PIC_POSSESSION_COUNT:
				sSortExpression = " ORDER BY PMT.TOTAL_PIC_POSSESSION_COUNT DESC NULLS LAST,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
				break;
			case PwViCommConst.GameManCommunicationSort.SORT_USE_MOVIE_TICKET:
				sSortExpression = " ORDER BY GCO.USE_MOVIE_TICKET_COUNT DESC NULLS LAST,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
				break;
			case PwViCommConst.GameManCommunicationSort.SORT_CHARGE_DATE_COUNT:
				sSortExpression = " ORDER BY GCO.CHARGE_DATE_COUNT DESC NULLS LAST,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
				break;
			default:
				sSortExpression = " ORDER BY FP.FRIENDLY_RANK ASC NULLS LAST,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
				break;
		}

		return sSortExpression;
	}
}