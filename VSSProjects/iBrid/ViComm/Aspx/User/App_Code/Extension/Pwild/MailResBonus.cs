﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メール返信ボーナス
--	Progaram ID		: MailResBonus
--
--  Creation Date	: 2016.07.28
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;


public class MailResBonus:DbSession {
	public MailResBonus() {
	}

	public void AddMailResBonusPoint(
		string pSiteCd,
		string pCastSeq,
		string pCastMailSeq,
		int pCheckOnlyFlag,
		out string pAddPoint,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_MAIL_RES_BONUS_POINT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_SEQ",DbSession.DbType.VARCHAR2,pCastSeq);
			db.ProcedureInParm("pCAST_MAIL_SEQ",DbSession.DbType.VARCHAR2,pCastMailSeq);
			db.ProcedureInParm("pCHECK_ONLY_FLAG",DbSession.DbType.NUMBER,pCheckOnlyFlag);
			db.ProcedureOutParm("pADD_POINT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pAddPoint = db.GetStringValue("pADD_POINT");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
