﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ ｶﾞﾁｬｱｲﾃﾑ

--	Progaram ID		: LotteryGetItem
--
--  Creation Date	: 2011.09.07
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class LotteryGetItemSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string SexCd;
	public string GameItemPublishFlag;
	public string PickupFlag;
	
	public string LotterySeq {
		get {
			return this.Query["lottery_seq"];
		}
		set {
			this.Query["lottery_seq"] = value;
		}
	}
	
	public string GameItemSeq {
		get {
			return this.Query["game_item_seq"];
		}
		set {
			this.Query["game_item_seq"] = value;
		}
	}

	public string RareItemFlag {
		get {
			return this.Query["rare_item_flag"];
		}
		set {
			this.Query["rare_item_flag"] = value;
		}
	}

	public string CompItemFlg {
		get {
			return this.Query["comp_item_flg"];
		}
		set {
			this.Query["comp_item_flg"] = value;
		}
	}

	public string SetLotteryItemGetLogSeq {
		get {
			return this.Query["set_lottery_item_get_log_seq"];
		}
		set {
			this.Query["set_lottery_item_get_log_seq"] = value;
		}
	}

	public LotteryGetItemSeekCondition()
		: this(new NameValueCollection()) {
	}

	public LotteryGetItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["lottery_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["lottery_seq"]));
		this.Query["game_item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_item_seq"]));
		this.Query["rare_item_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["rare_item_flag"]));
		this.Query["comp_item_flg"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["comp_item_flg"]));
		this.Query["set_lottery_item_get_log_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["set_lottery_item_get_log_seq"]));
		
	}
}

#endregion ============================================================================================================

public class LotteryGetItem:DbSession {
	public LotteryGetItem() {
	}

	public DataSet GetList(LotteryGetItemSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ				,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM				,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER				,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER				,	");
		oSqlBuilder.AppendLine("	ENDURANCE_NM				,	");
		oSqlBuilder.AppendLine("	RARE_ITEM_FLAG					");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_LOTTERY_GET_ITEM01		");

		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" LOTTERY_SEQ = :LOTTERY_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetListAddGetFlag(LotteryGetItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ					,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ				,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM				,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER				,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER				,	");
		oSqlBuilder.AppendLine("	ENDURANCE_NM				,	");
		oSqlBuilder.AppendLine("	RARE_ITEM_FLAG				,	");
		oSqlBuilder.AppendLine("	CASE							");
		oSqlBuilder.AppendLine("		WHEN						");
		oSqlBuilder.AppendLine("			EXISTS					");
		oSqlBuilder.AppendLine("			(						");
		oSqlBuilder.AppendLine("				SELECT							");
		oSqlBuilder.AppendLine("					*							");
		oSqlBuilder.AppendLine("				FROM							");
		oSqlBuilder.AppendLine("					T_LOTTERY_ITEM_GET_LOG		");
		oSqlBuilder.AppendLine("				WHERE							");
		oSqlBuilder.AppendFormat("					SITE_CD									= '{0}'										AND	",pCondition.SiteCd).AppendLine();
		oSqlBuilder.AppendFormat("					USER_SEQ								= {0}										AND	",pCondition.UserSeq).AppendLine();
		oSqlBuilder.AppendFormat("					USER_CHAR_NO							= '{0}'										AND	",pCondition.UserCharNo).AppendLine();
		oSqlBuilder.AppendFormat("					LOTTERY_SEQ								= {0}										AND	",pCondition.LotterySeq).AppendLine();
		oSqlBuilder.AppendLine("					VW_PW_LOTTERY_GET_ITEM01.GAME_ITEM_SEQ	= T_LOTTERY_ITEM_GET_LOG.GAME_ITEM_SEQ			");
		oSqlBuilder.AppendLine("			)						");
		oSqlBuilder.AppendLine("		THEN 1						");
		oSqlBuilder.AppendLine("		ELSE 0						");
		oSqlBuilder.AppendLine("	END AS LOTTERY_ITEM_GET_FLAG	");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_LOTTERY_GET_ITEM01		");

		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" LOTTERY_SEQ = :LOTTERY_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		string sSortExpression = " ORDER BY LOTTERY_ITEM_GET_FLAG DESC,GAME_ITEM_SEQ ";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}
	
	public DataSet GetOneLotteryGetItem(LotteryGetItemSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT		");
		oSqlBuilder.AppendLine("	*		");
		oSqlBuilder.AppendLine("FROM		");
		oSqlBuilder.AppendLine("	(		");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			GET_LOG.LOTTERY_SEQ												,	");
		oSqlBuilder.AppendLine("			GET_LOG.GAME_ITEM_SEQ											,	");
		oSqlBuilder.AppendLine("			GET_LOG.GAME_ITEM_NM											,	");
		oSqlBuilder.AppendLine("			GET_LOG.ATTACK_POWER											,	");
		oSqlBuilder.AppendLine("			GET_LOG.DEFENCE_POWER											,	");
		oSqlBuilder.AppendLine("			GET_LOG.ENDURANCE_NM											,	");
		oSqlBuilder.AppendLine("			GET_LOG.POSSESSION_COUNT										,	");
		oSqlBuilder.AppendLine("			LOTTERY.PUBLISH_START_DATE										,	");
		oSqlBuilder.AppendLine("			LOTTERY.PUBLISH_END_DATE										,	");
		oSqlBuilder.AppendLine("			LOTTERY.LOTTERY_NM												,	");
		oSqlBuilder.AppendLine("			LOTTERY.COMPLETE_GET_ITEM_SEQ	AS COMP_ITEM_SEQ				,	");
		oSqlBuilder.AppendLine("			LOTTERY.GAME_ITEM_NM			AS COMP_ITEM_NM					,	");
		oSqlBuilder.AppendLine("			LOTTERY.ATTACK_POWER			AS COMP_ITEM_ATTACK_POWER		,	");
		oSqlBuilder.AppendLine("			LOTTERY.DEFENCE_POWER			AS COMP_ITEM_DEFENCE_POWER		,	");
		oSqlBuilder.AppendLine("			LOTTERY.ENDURANCE				AS COMP_ITEM_ENDURANCE			,	");
		oSqlBuilder.AppendLine("			LOTTERY.ENDURANCE_NM			AS COMP_ITEM_ENDURANCE_NM		,	");
		oSqlBuilder.AppendLine("			LOTTERY.TICKET_NM												,	");
		oSqlBuilder.AppendLine("			CASE																");
		oSqlBuilder.AppendLine("				WHEN SYSDATE BETWEEN LOTTERY.PUBLISH_START_DATE AND LOTTERY.PUBLISH_END_DATE		");
		oSqlBuilder.AppendLine("				THEN 1														");
		oSqlBuilder.AppendLine("				ELSE 0														");
		oSqlBuilder.AppendLine("			END AS NOW_LOTTERY_FLAG										,	");
		oSqlBuilder.AppendLine("			NVL(PLT.POSSESSION_COUNT,0) AS POSSESSION_TICKET_COUNT		,	");
		oSqlBuilder.AppendLine("			RARE_ITEM_FLAG													");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			VW_PW_LOTTERY_ITEM_GET_LOG01 GET_LOG						,	");
		oSqlBuilder.AppendLine("			VW_PW_LOTTERY01	LOTTERY										,	");
		oSqlBuilder.AppendLine("			(																");
		oSqlBuilder.AppendLine("				SELECT														");
		oSqlBuilder.AppendLine("					SITE_CD												,	");
		oSqlBuilder.AppendLine("					LOTTERY_SEQ											,	");
		oSqlBuilder.AppendLine("					POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("				FROM														");
		oSqlBuilder.AppendLine("					T_POSSESSION_LOTTERY_TICKET								");
		oSqlBuilder.AppendLine("				WHERE														");
		oSqlBuilder.AppendLine("					SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("					USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO	= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("			) PLT															");
		
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" GET_LOG.SITE_CD	= LOTTERY.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" GET_LOG.LOTTERY_SEQ	= LOTTERY.LOTTERY_SEQ",ref sWhereClause);

		SysPrograms.SqlAppendWhere(" GET_LOG.SITE_CD	= PLT.SITE_CD	(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" GET_LOG.LOTTERY_SEQ	= PLT.LOTTERY_SEQ	(+)",ref sWhereClause);

		if(!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.SITE_CD	= :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}
		
		if(!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.USER_SEQ	= :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}
		
		if(!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.USER_CHAR_NO	= :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if(!string.IsNullOrEmpty(pCondition.LotterySeq)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.LOTTERY_SEQ = :LOTTERY_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));
		}
		
		if(!string.IsNullOrEmpty(pCondition.GameItemSeq)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.GAME_ITEM_SEQ	= :GAME_ITEM_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pCondition.GameItemSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.SEX_CD	= :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(" GET_LOG.GAME_ITEM_PUBLISH_FLAG = :GAME_ITEM_PUBLISH_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":GAME_ITEM_PUBLISH_FLAG",pCondition.GameItemPublishFlag));
		
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("		ORDER BY GET_LOG.GAME_ITEM_GET_DATE DESC");
		oSqlBuilder.AppendLine("	)											");
		oSqlBuilder.AppendLine("WHERE ROWNUM = 1								");
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		LotterySeekCondition subCondition = new LotterySeekCondition();
		subCondition.SiteCd = pCondition.SiteCd;
		subCondition.UserSeq = pCondition.UserSeq;
		subCondition.UserCharNo = pCondition.UserCharNo;
		subCondition.SexCd = pCondition.SexCd;
		
		using(Lottery oLottery = new Lottery()) {
			oLottery.AppendItemCount(oDataSet,subCondition);
		}

		return oDataSet;
	}

	public DataSet GetOneLotteryGetItemNotComp(LotteryGetItemSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT											");
		oSqlBuilder.AppendLine("	GET_LOG.LOTTERY_SEQ						,	");
		oSqlBuilder.AppendLine("	GET_LOG.GAME_ITEM_SEQ					,	");
		oSqlBuilder.AppendLine("	GET_LOG.GAME_ITEM_COUNT					,	");
		oSqlBuilder.AppendLine("	GET_LOG.GAME_ITEM_NM					,	");
		oSqlBuilder.AppendLine("	GET_LOG.GAME_ITEM_CATEGORY_TYPE			,	");
		oSqlBuilder.AppendLine("	GET_LOG.SET_LOTTERY_ITEM_GET_LOG_SEQ	,	");
		oSqlBuilder.AppendLine("	GET_LOG.LOTTERY_SET_FLAG				,	");
		oSqlBuilder.AppendLine("	GET_LOG.ATTACK_POWER					,	");
		oSqlBuilder.AppendLine("	GET_LOG.DEFENCE_POWER					,	");
		oSqlBuilder.AppendLine("	GET_LOG.ENDURANCE						,	");
		oSqlBuilder.AppendLine("	GET_LOG.ENDURANCE_NM					,	");
		oSqlBuilder.AppendLine("	GET_LOG.POSSESSION_COUNT				,	");
		oSqlBuilder.AppendLine("	GET_LOG.PM_ITEM_FLAG					,	");
		oSqlBuilder.AppendLine("	LOTTERY.PUBLISH_START_DATE				,	");
		oSqlBuilder.AppendLine("	LOTTERY.PUBLISH_END_DATE				,	");
		oSqlBuilder.AppendLine("	LOTTERY.PUBLISH_END_DATE				,	");
		oSqlBuilder.AppendLine("	LOTTERY.PRICE	AS LOTTERY_PRICE		,	");
		oSqlBuilder.AppendLine("	LOTTERY.PRICE_1	AS LOTTERY_PRICE_1		,	");
		oSqlBuilder.AppendLine("	LOTTERY.PRICE_2	AS LOTTERY_PRICE_2		,	");
		oSqlBuilder.AppendLine("	LOTTERY.PRICE_3	AS LOTTERY_PRICE_3		,	");
		oSqlBuilder.AppendLine("	LOTTERY.LOTTERY_NM						,	");
		oSqlBuilder.AppendLine("	LOTTERY.LOTTERY_COUNT_1					,	");
		oSqlBuilder.AppendLine("	LOTTERY.LOTTERY_COUNT_2					,	");
		oSqlBuilder.AppendLine("	LOTTERY.LOTTERY_COUNT_3					,	");
		oSqlBuilder.AppendLine("	LOTTERY.LOTTERY_SET_FLG_1				,	");
		oSqlBuilder.AppendLine("	LOTTERY.LOTTERY_SET_FLG_2				,	");
		oSqlBuilder.AppendLine("	LOTTERY.LOTTERY_SET_FLG_3				,	");
		oSqlBuilder.AppendLine("	LOTTERY.PREMIUM_COUNT_1   				,	");
		oSqlBuilder.AppendLine("	LOTTERY.PREMIUM_COUNT_2   				,	");
		oSqlBuilder.AppendLine("	LOTTERY.PREMIUM_COUNT_3   					");
		oSqlBuilder.AppendLine(" FROM											");
		oSqlBuilder.AppendLine("	VW_PW_SET_LOTTERY_NOCOMP_LOG01	GET_LOG	,	");
		oSqlBuilder.AppendLine("	T_LOTTERY_NOT_COMP				LOTTERY		");

		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" GET_LOG.SITE_CD	= LOTTERY.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" GET_LOG.LOTTERY_SEQ	= LOTTERY.LOTTERY_SEQ",ref sWhereClause);

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.SITE_CD	= :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.USER_SEQ	= :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.USER_CHAR_NO	= :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.LotterySeq)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.LOTTERY_SEQ = :LOTTERY_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.SetLotteryItemGetLogSeq)) {
			SysPrograms.SqlAppendWhere(" GET_LOG.SET_LOTTERY_ITEM_GET_LOG_SEQ	= :SET_LOTTERY_ITEM_GET_LOG_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SET_LOTTERY_ITEM_GET_LOG_SEQ",pCondition.SetLotteryItemGetLogSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" LOTTERY.SEX_CD	= :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(" GET_LOG.GAME_ITEM_PUBLISH_FLAG = :GAME_ITEM_PUBLISH_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":GAME_ITEM_PUBLISH_FLAG",pCondition.GameItemPublishFlag));

		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine(" ORDER BY GET_LOG.PM_ITEM_FLAG DESC,GET_LOG.CREATE_DATE ASC ");
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	public DataSet GetOneLotteryCompGetItem(LotteryGetItemSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		
		oSqlBuilder.AppendLine("	SELECT																");
		oSqlBuilder.AppendLine("		LOTTERY.LOTTERY_SEQ											,	");
		oSqlBuilder.AppendLine("		LOTTERY.COMPLETE_GET_ITEM_SEQ	AS GAME_ITEM_SEQ			,	");
		oSqlBuilder.AppendLine("		LOTTERY.GAME_ITEM_NM										,	");
		oSqlBuilder.AppendLine("		LOTTERY.ATTACK_POWER										,	");
		oSqlBuilder.AppendLine("		LOTTERY.DEFENCE_POWER										,	");
		oSqlBuilder.AppendLine("		LOTTERY.ENDURANCE_NM										,	");
		oSqlBuilder.AppendLine("		NVL(POSSESSION.POSSESSION_COUNT,0) AS POSSESSION_COUNT		,	");
		oSqlBuilder.AppendLine("		LOTTERY.PUBLISH_START_DATE									,	");
		oSqlBuilder.AppendLine("		LOTTERY.PUBLISH_END_DATE									,	");
		oSqlBuilder.AppendLine("		LOTTERY.LOTTERY_NM											,	");
		oSqlBuilder.AppendLine("		LOTTERY.GAME_ITEM_NM		AS COMP_ITEM_NM					,	");
		oSqlBuilder.AppendLine("		LOTTERY.ATTACK_POWER		AS COMP_ITEM_ATTACK_POWER		,	");
		oSqlBuilder.AppendLine("		LOTTERY.DEFENCE_POWER		AS COMP_ITEM_DEFENCE_POWER		,	");
		oSqlBuilder.AppendLine("		LOTTERY.ENDURANCE			AS COMP_ITEM_ENDURANCE			,	");
		oSqlBuilder.AppendLine("		LOTTERY.ENDURANCE_NM		AS COMP_ITEM_ENDURANCE_NM		,	");
		oSqlBuilder.AppendLine("		LOTTERY.TICKET_NM											,	");
		oSqlBuilder.AppendLine("		CASE															");
		oSqlBuilder.AppendLine("			WHEN SYSDATE BETWEEN LOTTERY.PUBLISH_START_DATE AND LOTTERY.PUBLISH_END_DATE		");
		oSqlBuilder.AppendLine("				THEN 1														");
		oSqlBuilder.AppendLine("				ELSE 0														");
		oSqlBuilder.AppendLine("			END AS NOW_LOTTERY_FLAG											");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			VW_PW_LOTTERY01	LOTTERY	,										");
		oSqlBuilder.AppendLine("			(																");
		oSqlBuilder.AppendLine("				SELECT														");
		oSqlBuilder.AppendLine("					SITE_CD												,	");
		oSqlBuilder.AppendLine("					GAME_ITEM_SEQ										,	");
		oSqlBuilder.AppendLine("					POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("				FROM														");
		oSqlBuilder.AppendLine("					T_POSSESSION_GAME_ITEM									");
		oSqlBuilder.AppendLine("				WHERE														");
		oSqlBuilder.AppendLine("					SITE_CD			= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("					USER_SEQ		= :USER_SEQ							AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO	= :USER_CHAR_NO							");
		oSqlBuilder.AppendLine("			) POSSESSION													");

		string sWhereClause = string.Empty;

		SysPrograms.SqlAppendWhere("LOTTERY.SITE_CD	= POSSESSION.SITE_CD	(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere("LOTTERY.COMPLETE_GET_ITEM_SEQ	= POSSESSION.GAME_ITEM_SEQ	(+)",ref sWhereClause);
		
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" LOTTERY.SITE_CD	= :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.LotterySeq)) {
			SysPrograms.SqlAppendWhere(" LOTTERY.LOTTERY_SEQ = :LOTTERY_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.SetLotteryItemGetLogSeq)) {
			SysPrograms.SqlAppendWhere(" LOTTERY.COMPLETE_GET_ITEM_SEQ	= :COMPLETE_GET_ITEM_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":COMPLETE_GET_ITEM_SEQ",pCondition.GameItemSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" LOTTERY.SEX_CD	= :SEX_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		SysPrograms.SqlAppendWhere(" LOTTERY.GAME_ITEM_PUBLISH_FLAG = :GAME_ITEM_PUBLISH_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":GAME_ITEM_PUBLISH_FLAG",pCondition.GameItemPublishFlag));

		SysPrograms.SqlAppendWhere(" ROWNUM = 1",ref sWhereClause);
		
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	private OracleParameter[] CreateWhere(LotteryGetItemSeekCondition pCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (pCondition == null)
			throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		if (!String.IsNullOrEmpty(pCondition.GameItemSeq)) {
			SysPrograms.SqlAppendWhere(" GAME_ITEM_SEQ = :GAME_ITEM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pCondition.GameItemSeq));
		}

		if (!String.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD	= :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!String.IsNullOrEmpty(pCondition.RareItemFlag)) {
			SysPrograms.SqlAppendWhere(" RARE_ITEM_FLAG = :RARE_ITEM_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":RARE_ITEM_FLAG",pCondition.RareItemFlag));
		}

		if (!String.IsNullOrEmpty(pCondition.GameItemPublishFlag)) {
			SysPrograms.SqlAppendWhere(" GAME_ITEM_PUBLISH_FLAG = :GAME_ITEM_PUBLISH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_ITEM_PUBLISH_FLAG",pCondition.GameItemPublishFlag));
		}
		
		if (!String.IsNullOrEmpty(pCondition.PickupFlag)) {
			SysPrograms.SqlAppendWhere(" PICKUP_FLAG = :PICKUP_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PICKUP_FLAG",pCondition.PickupFlag));
		}

		return oParamList.ToArray();
	}

	public String GetLotteryItemGetLog(string pSiteCd,string pUserSeq,string pUserCharNo,int pLotterySeq) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	COUNT(*)	COUNT						");
		oSqlBuilder.AppendLine(" FROM							   			");
		oSqlBuilder.AppendLine("	T_LOTTERY_ITEM_GET_LOG					");
		oSqlBuilder.AppendLine(" WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ			= :LOTTERY_SEQ		");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				this.cmd.Parameters.Add(":LOTTERY_SEQ",pLotterySeq);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["COUNT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public DataSet GetListNotComp(LotteryGetItemSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ											,	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_NM											,	");
		oSqlBuilder.AppendLine("	P.ATTACK_POWER											,	");
		oSqlBuilder.AppendLine("	P.DEFENCE_POWER											,	");
		oSqlBuilder.AppendLine("	P.ENDURANCE_NM											,	");
		oSqlBuilder.AppendLine("	P.PM_ITEM_FLAG											,	");
		oSqlBuilder.AppendLine("	NVL(PGI.POSSESSION_COUNT,0) AS POSSESSION_COUNT				");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	VW_PW_LOTTERY_GET_ITEM_NC01	P							,	");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			SITE_CD											,	");
		oSqlBuilder.AppendLine("			GAME_ITEM_SEQ									,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT									");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_POSSESSION_GAME_ITEM								");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	)PGI														");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	P.SITE_CD					= PGI.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_SEQ				= PGI.GAME_ITEM_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	P.LOTTERY_SEQ				= :LOTTERY_SEQ				AND	");
		oSqlBuilder.AppendLine("	P.GAME_ITEM_PUBLISH_FLAG	= :GAME_ITEM_PUBLISH_FLAG		");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":LOTTERY_SEQ",pCondition.LotterySeq));
		oParamList.Add(new OracleParameter(":GAME_ITEM_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		
		if (!string.IsNullOrEmpty(pCondition.PickupFlag)) {
			oSqlBuilder.AppendLine(" AND P.PICKUP_FLAG = :PICKUP_FLAG");
			oParamList.Add(new OracleParameter(":PICKUP_FLAG",pCondition.PickupFlag));
		}

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
