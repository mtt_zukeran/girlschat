﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 男性ｹﾞｰﾑｷｬﾗｸﾀｰ
--	Progaram ID		: ManCharacter
--
--  Creation Date	: 2011.09.28
--  Creater			: PW A.Taba
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;

using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

public class ManCharacter:DbSession {
	public ManCharacter() {
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	NA_FLAG						");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER		");
		oSqlBuilder.AppendLine(" WHERE							");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND		");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
