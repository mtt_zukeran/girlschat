﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾊﾞｲﾄﾌﾟﾗﾝ
--	Progaram ID		: PartTimeJobPlan
--
--  Creation Date	: 2011.07.27
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class PartTimeJobPlanSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string StageLevel {
		get {
			return this.Query["stage_level"];
		}
		set {
			this.Query["stage_level"] = value;
		}
	}

	public string PartTimeJobSeq {
		get {
			return this.Query["part_time_job_seq"];
		}
		set {
			this.Query["part_time_job_seq"] = value;
		}
	}

	public PartTimeJobPlanSeekCondition()
		: this(new NameValueCollection()) {
	}

	public PartTimeJobPlanSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["stage_level"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_level"]));
		this.Query["part_time_job_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["part_time_job_seq"]));
	}
}

[System.Serializable]
public class PartTimeJobPlan:DbSession {
	public PartTimeJobPlan() {
	}

	public int GetPageCount(PartTimeJobPlanSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_PART_TIME_JOB_PLAN01	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(PartTimeJobPlanSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(PartTimeJobPlanSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	SITE_CD					,	");
		oSqlBuilder.AppendLine("	PART_TIME_JOB_SEQ		,	");
		oSqlBuilder.AppendLine("	PART_TIME_JOB_NM		,	");
		oSqlBuilder.AppendLine("	INCOME					,	");
		oSqlBuilder.AppendLine("	WAITING_MIN				,	");
		oSqlBuilder.AppendLine("	0 AS REC_NO_PER_PAGE		");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_PART_TIME_JOB_PLAN01	");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		int iRecNoPerPage = 0;
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow oDR in oDataSet.Tables[0].Rows) {
				iRecNoPerPage = (int.Parse(oDR["RNUM"].ToString()) - iStartIndex);
				oDR["REC_NO_PER_PAGE"] = iRecNoPerPage.ToString();
			}
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(PartTimeJobPlanSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.StageLevel)) {
			SysPrograms.SqlAppendWhere(" STAGE_LEVEL <= :STAGE_LEVEL",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_LEVEL",pCondition.StageLevel));
		}

		if (!string.IsNullOrEmpty(pCondition.PartTimeJobSeq)) {
			SysPrograms.SqlAppendWhere(" PART_TIME_JOB_SEQ = :PART_TIME_JOB_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PART_TIME_JOB_SEQ",pCondition.PartTimeJobSeq));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(PartTimeJobPlanSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY INCOME ASC";

		return sSortExpression;
	}

	public string GetPartTimeJob(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartTimeJobSeq) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_PART_TIME_JOB");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPART_TIME_JOB_SEQ",DbSession.DbType.VARCHAR2,pPartTimeJobSeq);
			db.ProcedureOutParm("pLEFT_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}
		
		return sResult;
	}

	public string GetTodayPartTimeJobCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sPartTimeJobCount = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	PART_TIME_JOB_COUNT							");
		oSqlBuilder.AppendLine(" FROM											");
		oSqlBuilder.AppendLine("	T_DAILY_PART_TIME_JOB_LOG					");
		oSqlBuilder.AppendLine(" WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD						 AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ						 AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO					 AND	");
		oSqlBuilder.AppendLine("	REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sPartTimeJobCount = oDataSet.Tables[0].Rows[0]["PART_TIME_JOB_COUNT"].ToString();
		}

		return sPartTimeJobCount;
	}
}
