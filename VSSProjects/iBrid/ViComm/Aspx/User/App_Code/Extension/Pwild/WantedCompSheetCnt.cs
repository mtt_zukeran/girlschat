﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: この娘を探せ 完成スタンプシート数
--	Progaram ID		: WantedCompSheetCnt
--
--  Creation Date	: 2013.06.07
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class WantedCompSheetCnt:DbSession {
	public WantedCompSheetCnt() {
	}
	
	public int GetCompleteSheetCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	COMPLETE_COUNT							");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_WANTED_COMP_SHEET_CNT					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	REPORT_MONTH	= :REPORT_MONTH			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",pReportMonth));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return int.Parse(oDataSet.Tables[0].Rows[0]["COMPLETE_COUNT"].ToString());
		} else {
			return 0;
		}
	}
	
	public void WantedMonthlyPointAcquired(string pSiteCd,string pUserSeq,string pUserCharNo,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WANTED_MONTHLY_POINT_ACQUIRED");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public DataSet GetWantedMonthlyPoint(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportMonth) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT																							");
		oSqlBuilder.AppendLine("	WCSC.SITE_CD																			,	");
		oSqlBuilder.AppendLine("	WCSC.COMPLETE_COUNT																		,	");
		oSqlBuilder.AppendLine("	TOTAL_COMP.TOTAL_COMP_CNT																,	");
		oSqlBuilder.AppendLine("	SME.WANTED_MONTHLY_BOUNTY_POINT															,	");
		oSqlBuilder.AppendLine("	TRUNC(SME.WANTED_MONTHLY_BOUNTY_POINT / TOTAL_COMP.TOTAL_COMP_CNT) AS BOUNTY_POINT_PER_SHEET					,	");
		oSqlBuilder.AppendLine("	TRUNC(SME.WANTED_MONTHLY_BOUNTY_POINT / TOTAL_COMP.TOTAL_COMP_CNT) * WCSC.COMPLETE_COUNT AS SELF_BOUNTY_POINT		");
		oSqlBuilder.AppendLine("FROM																							");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			SITE_CD																			,	");
		oSqlBuilder.AppendLine("			COMPLETE_COUNT																		");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_WANTED_COMP_SHEET_CNT																");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ														AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO													AND	");
		oSqlBuilder.AppendLine("			REPORT_MONTH	= :REPORT_MONTH														");
		oSqlBuilder.AppendLine("	) WCSC																					,	");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			SUM(COMPLETE_COUNT) AS TOTAL_COMP_CNT												");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_WANTED_COMP_SHEET_CNT																");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			REPORT_MONTH	= :REPORT_MONTH														");
		oSqlBuilder.AppendLine("	) TOTAL_COMP																			,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX  SME																	");
		oSqlBuilder.AppendLine("WHERE																							");
		oSqlBuilder.AppendLine("	WCSC.SITE_CD			= SME.SITE_CD														");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",pReportMonth));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
	
	public string CheckNotAcquiredPointExists(string pSiteCd,string pUserSeq,string pUserCharNo) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	1															");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_WANTED_COMP_SHEET_CNT WCSC							,	");
		oSqlBuilder.AppendLine("	T_REQ_TX_MAIL_DTL RTMD										");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	WCSC.SITE_CD				= RTMD.RX_SITE_CD			AND	");
		oSqlBuilder.AppendLine("	WCSC.USER_SEQ				= RTMD.RX_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	WCSC.USER_CHAR_NO			= RTMD.RX_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	WCSC.SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	WCSC.USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	WCSC.USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	WCSC.REPORT_MONTH			= :REPORT_MONTH				AND	");
		oSqlBuilder.AppendLine("	WCSC.POINT_ACQUIRED_FLAG	= :POINT_ACQUIRED_FLAG		AND	");
		oSqlBuilder.AppendLine("	RTMD.MAIL_TYPE				= :MAIL_TYPE				AND	");
		oSqlBuilder.AppendLine("	RTMD.REQUEST_TX_DATE		>= SYSDATE - 3					");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",DateTime.Now.AddMonths(-1).ToString("yyyyMM")));
		oParamList.Add(new OracleParameter(":POINT_ACQUIRED_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":MAIL_TYPE",ViCommConst.MAIL_TP_WANTED_BOUNTY_ACQUIRED));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}
}