﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 投票設定
--	Progaram ID		: VoteTerm
--  Creation Date	: 2013.10.18
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class VoteTerm:DbSession {
	public VoteTerm() {
	}

	public DataSet GetCurrent(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		SITE_CD,");
		oSqlBuilder.AppendLine("		VOTE_TERM_SEQ,");
		oSqlBuilder.AppendLine("		FIX_RANK_FLAG");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_VOTE_TERM");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("		VOTE_START_DATE <= SYSDATE");
		oSqlBuilder.AppendLine("	ORDER BY");
		oSqlBuilder.AppendLine("		SITE_CD,VOTE_START_DATE DESC");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM = 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneBySeq(string pSiteCd,string pVoteTermSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	SITE_CD								,	");
		oSqlBuilder.AppendLine("	VOTE_TERM_SEQ						,	");
		oSqlBuilder.AppendLine("	FIX_RANK_FLAG							");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_VOTE_TERM								");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	VOTE_TERM_SEQ	= :VOTE_TERM_SEQ		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":VOTE_TERM_SEQ",pVoteTermSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetVotable(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		SITE_CD,");
		oSqlBuilder.AppendLine("		VOTE_TERM_SEQ,");
		oSqlBuilder.AppendLine("		VOTE_START_DATE,");
		oSqlBuilder.AppendLine("		VOTE_END_DATE,");
		oSqlBuilder.AppendLine("		TICKET_POINT,");
		oSqlBuilder.AppendLine("		FIX_RANK_FLAG,");
		oSqlBuilder.AppendLine("		FIX_RANK_DATE");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_VOTE_TERM");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("		VOTE_START_DATE <= SYSDATE AND");
		oSqlBuilder.AppendLine("		VOTE_END_DATE >= SYSDATE");
		oSqlBuilder.AppendLine("	ORDER BY");
		oSqlBuilder.AppendLine("		SITE_CD,VOTE_START_DATE DESC");
		oSqlBuilder.AppendLine("	)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM = 1");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public void GetVoteTicketCount(
		string pSiteCd,
		string pManUserSeq,
		out int pTicketCount
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_VOTE_TICKET_COUNT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureOutParm("pTICKET_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pTicketCount = db.GetIntValue("pTICKET_COUNT");
		}
	}

	public void AddVoteTicketByPt(
		string pSiteCd,
		string pManUserSeq,
		int pAddCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_VOTE_TICKET_BY_PT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pADD_COUNT",DbSession.DbType.NUMBER,pAddCount);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void AddVoteTicketByCode(
		string pSiteCd,
		string pManUserSeq,
		string pCodeStr,
		out int pAddCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_VOTE_TICKET_BY_CODE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pCODE_STR",DbSession.DbType.VARCHAR2,pCodeStr);
			db.ProcedureOutParm("pADD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pAddCount = db.GetIntValue("pADD_COUNT");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
