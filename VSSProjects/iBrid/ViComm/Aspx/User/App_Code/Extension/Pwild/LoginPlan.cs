﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 出演者ログインスケジュール
--	Progaram ID		: LoginPlan
--
--  Creation Date	: 2014.12.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class LoginPlanSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["usercharno"];
		}
		set {
			this.Query["usercharno"] = value;
		}
	}

	public string LoginPlanSeq {
		get {
			return this.Query["loginplanseq"];
		}
		set {
			this.Query["loginplanseq"] = value;
		}
	}

	public string LoginStartTime {
		get {
			return this.Query["loginstarttime"];
		}
		set {
			this.Query["loginstarttime"] = value;
		}
	}

	public string MinLoginStartTime;
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public LoginPlanSeekCondition()
		: this(new NameValueCollection()) {
	}

	public LoginPlanSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["usercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["usercharno"]));
		this.Query["loginplanseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["loginplanseq"]));
		this.Query["loginstarttime"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["loginstarttime"]));
	}
}

#endregion ============================================================================================================

public class LoginPlan:CastBase {
	public LoginPlan()
		: base() {
	}
	public LoginPlan(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(LoginPlanSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_LOGIN_PLAN00 P		");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(LoginPlanSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "					,	");
		oSqlBuilder.AppendLine("	P.LOGIN_PLAN_SEQ							,	");
		oSqlBuilder.AppendLine("	P.LOGIN_START_TIME								");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_LOGIN_PLAN00 P								");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.LOGIN_START_TIME ASC,P.USER_SEQ ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		this.AppendCastAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(LoginPlanSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.LoginPlanSeq)) {
			SysPrograms.SqlAppendWhere(" P.LOGIN_PLAN_SEQ		= :LOGIN_PLAN_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_PLAN_SEQ",pCondition.LoginPlanSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.LoginStartTime)) {
			SysPrograms.SqlAppendWhere(" P.LOGIN_START_TIME		>= :LOGIN_START_TIME",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_START_TIME",DateTime.Parse(pCondition.LoginStartTime)));
		}

		if (!string.IsNullOrEmpty(pCondition.MinLoginStartTime)) {
			SysPrograms.SqlAppendWhere(" P.LOGIN_START_TIME		>= :MIN_LOGIN_START_TIME",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MIN_LOGIN_START_TIME",DateTime.Parse(pCondition.MinLoginStartTime)));
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND LOGIN_VIEW_STATUS != 0)",ref pWhereClause);

		return oParamList.ToArray();
	}

	public void LoginPlanMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pLoginPlanSeq,DateTime pLoginStartTime,int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOGIN_PLAN_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pLOGIN_PLAN_SEQ",DbSession.DbType.VARCHAR2,pLoginPlanSeq);
			db.ProcedureInParm("pLOGIN_START_TIME",DbSession.DbType.DATE,pLoginStartTime);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,pDelFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void DeleteOverLoginPlan(string pSiteCd,string pUserSeq,string pUserCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_OVER_LOGIN_PLAN");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}