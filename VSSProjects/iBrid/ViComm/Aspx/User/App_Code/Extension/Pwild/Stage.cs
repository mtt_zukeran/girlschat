﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ｽﾃｰｼﾞ
--	Progaram ID		: Stage
--
--  Creation Date	: 2011.07.25
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class StageSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;	
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public string StageSeq {
		get {
			return this.Query["stage_seq"];
		}
		set {
			this.Query["stage_seq"] = value;
		}
	}

	public string StageLevel {
		get {
			return this.Query["stage_level"];
		}
		set {
			this.Query["stage_level"] = value;
		}
	}

	public string StageGroupType {
		get {
			return this.Query["stage_group_type"];
		}
		set {
			this.Query["stage_group_type"] = value;
		}
	}

	public string StageClearFlag {
		get {
			return this.Query["stage_clear_flag"];
		}
		set {
			this.Query["stage_clear_flag"] = value;
		}
	}
	
	public string NextStageMovedFlag {
		get {
			return this.Query["next_stage_moved_flag"];
		}
		set {
			this.Query["next_stage_moved_flag"] = value;
		}
	}

	public StageSeekCondition()
		: this(new NameValueCollection()) {
	}

	public StageSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["stage_group_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_group_type"]));
		this.Query["stage_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["stage_seq"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
	}
}

[System.Serializable]
public class Stage:DbSession {
	public Stage() {
	}

	public DataSet GetOneByStageSeq(StageSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	STAGE.STAGE_SEQ,				");
		oSqlBuilder.AppendLine("	STAGE.STAGE_NM,					");
		oSqlBuilder.AppendLine("	STAGE.HONOR,					");
		oSqlBuilder.AppendLine("	STAGE.STAGE_GROUP_TYPE,			");
		oSqlBuilder.AppendLine("	STAGE.TOWN_COUNT,				");
		oSqlBuilder.AppendLine("	LIMIT.MAN_LIMIT_STAGE_SEQ,		");
		oSqlBuilder.AppendLine("	LIMIT.CAST_LIMIT_STAGE_SEQ,		");
		oSqlBuilder.AppendLine("	0 AS STAGE_CLEAR_FLAG,			");
		oSqlBuilder.AppendLine("	0 AS TOWN_CLEAR_COUNT,			");
		oSqlBuilder.AppendLine("	STAGE.STAGE_LEVEL,				");
		oSqlBuilder.AppendLine("	STAGE.STAGE_GROUP_NM,			");
		oSqlBuilder.AppendLine("	STAGE.BOSS_NM					");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	(SELECT							");
		oSqlBuilder.AppendLine("		SITE_CD,					");
		oSqlBuilder.AppendLine("		STAGE_SEQ,					");
		oSqlBuilder.AppendLine("		STAGE_NM,					");
		oSqlBuilder.AppendLine("		HONOR,						");
		oSqlBuilder.AppendLine("		STAGE_GROUP_TYPE,			");
		oSqlBuilder.AppendLine("		TOWN_COUNT,					");
		oSqlBuilder.AppendLine("		STAGE_LEVEL,				");
		oSqlBuilder.AppendLine("		STAGE_GROUP_NM,				");
		oSqlBuilder.AppendLine("		BOSS_NM						");
		oSqlBuilder.AppendLine("	 FROM							");
		oSqlBuilder.AppendLine("		VW_PW_GAME_STAGE02			");

		// where
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) STAGE,						");
		oSqlBuilder.AppendLine("	T_SOCIAL_GAME_LIMIT LIMIT		");
		oSqlBuilder.AppendLine(" WHERE								");
		oSqlBuilder.AppendLine(" 	STAGE.SITE_CD = LIMIT.SITE_CD (+)");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		AppendStageClearFlag(oDataSet,pCondition);
		AppendTownClearCount(oDataSet,pCondition);

		return oDataSet;
	}

	public DataSet GetOneByStageGroupType(StageSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	STAGE.STAGE_SEQ,				");
		oSqlBuilder.AppendLine("	STAGE.STAGE_NM,					");
		oSqlBuilder.AppendLine("	STAGE.HONOR,					");
		oSqlBuilder.AppendLine("	STAGE.STAGE_GROUP_TYPE,			");
		oSqlBuilder.AppendLine("	STAGE.TOWN_COUNT,				");
		oSqlBuilder.AppendLine("	LIMIT.MAN_LIMIT_STAGE_SEQ,		");
		oSqlBuilder.AppendLine("	LIMIT.CAST_LIMIT_STAGE_SEQ,		");
		oSqlBuilder.AppendLine("	0 AS STAGE_CLEAR_FLAG,			");
		oSqlBuilder.AppendLine("	0 AS TOWN_CLEAR_COUNT,			");
		oSqlBuilder.AppendLine("	STAGE.STAGE_LEVEL,				");
		oSqlBuilder.AppendLine("	STAGE.STAGE_GROUP_NM			");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	(SELECT							");
		oSqlBuilder.AppendLine("		INNER.*						");
		oSqlBuilder.AppendLine("	 FROM							");
		oSqlBuilder.AppendLine("		(SELECT						");
		oSqlBuilder.AppendLine("			SITE_CD,				");
		oSqlBuilder.AppendLine("			STAGE_SEQ,				");
		oSqlBuilder.AppendLine("			STAGE_NM,				");
		oSqlBuilder.AppendLine("			HONOR,					");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE,		");
		oSqlBuilder.AppendLine("			TOWN_COUNT,				");
		oSqlBuilder.AppendLine("			STAGE_LEVEL,			");
		oSqlBuilder.AppendLine("			STAGE_GROUP_NM			");
		oSqlBuilder.AppendLine("		 FROM						");
		oSqlBuilder.AppendLine("			VW_PW_GAME_STAGE02		");

		// where
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("		 ORDER BY					");
		oSqlBuilder.AppendLine("			STAGE_LEVEL DESC		");
		oSqlBuilder.AppendLine("		) INNER						");
		oSqlBuilder.AppendLine("		WHERE						");
		oSqlBuilder.AppendLine("			ROWNUM <= 1				");
		oSqlBuilder.AppendLine("	) STAGE,						");
		oSqlBuilder.AppendLine("	T_SOCIAL_GAME_LIMIT LIMIT		");
		oSqlBuilder.AppendLine(" WHERE								");
		oSqlBuilder.AppendLine(" 	STAGE.SITE_CD = LIMIT.SITE_CD (+)");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		AppendStageClearFlag(oDataSet,pCondition);
		AppendTownClearCount(oDataSet,pCondition);

		return oDataSet;
	}

	private void AppendStageClearFlag(DataSet pDS,StageSeekCondition pCondition) {
		string sStageSeq = string.Empty;

		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		} else {
			sStageSeq = pDS.Tables[0].Rows[0]["STAGE_SEQ"].ToString();
		}

		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	STAGE_CLEAR_FLAG	");
		oSqlBuilder.AppendLine(" FROM					");
		oSqlBuilder.AppendLine("	T_STAGE_PROGRESS	");

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":STAGE_SEQ",sStageSeq));

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		oSqlBuilder.AppendLine(sWhereClause);
		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (subDS.Tables[0].Rows.Count > 0) {
			pDS.Tables[0].Rows[0]["STAGE_CLEAR_FLAG"] = subDS.Tables[0].Rows[0]["STAGE_CLEAR_FLAG"];
		} else {
			pDS.Tables[0].Rows[0]["STAGE_CLEAR_FLAG"] = ViCommConst.FLAG_OFF_STR;
		}
	}

	private void AppendTownClearCount(DataSet pDS,StageSeekCondition pCondition) {
		string sStageSeq = string.Empty;

		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		} else {
			sStageSeq = pDS.Tables[0].Rows[0]["STAGE_SEQ"].ToString();
		}

		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*) AS TOWN_CLEAR_COUNT");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	T_TOWN_PROGRESS				");

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":STAGE_SEQ",sStageSeq));

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		SysPrograms.SqlAppendWhere(" TOWN_CLEAR_FLAG = :TOWN_CLEAR_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TOWN_CLEAR_FLAG",ViCommConst.FLAG_ON_STR));

		oSqlBuilder.AppendLine(sWhereClause);
		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (subDS.Tables[0].Rows.Count > 0) {
			pDS.Tables[0].Rows[0]["TOWN_CLEAR_COUNT"] = subDS.Tables[0].Rows[0]["TOWN_CLEAR_COUNT"];
		}
	}

	private OracleParameter[] CreateWhere(StageSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.StageSeq)) {
			SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_SEQ",pCondition.StageSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.StageGroupType)) {
			SysPrograms.SqlAppendWhere(" STAGE_GROUP_TYPE = :STAGE_GROUP_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_GROUP_TYPE",pCondition.StageGroupType));
		}

		if (!string.IsNullOrEmpty(pCondition.StageLevel)) {
			SysPrograms.SqlAppendWhere(" STAGE_LEVEL <= :STAGE_LEVEL",ref pWhereClause);
			oParamList.Add(new OracleParameter(":STAGE_LEVEL",pCondition.StageLevel));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(StageSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY STAGE_LEVEL ASC";

		return sSortExpression;
	}

	public DataSet GetOneBossData(StageSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	T_STAGE.STAGE_SEQ					,	");
		oSqlBuilder.AppendLine("	T_STAGE.STAGE_NM					,	");
		oSqlBuilder.AppendLine("	T_STAGE.BOSS_NM						,	");
		oSqlBuilder.AppendLine("	T_STAGE.BOSS_DESCRIPTION				");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	T_STAGE								,	");
		oSqlBuilder.AppendLine("	(										");
		oSqlBuilder.AppendLine("		SELECT								");
		oSqlBuilder.AppendLine("			SITE_CD						,	");
		oSqlBuilder.AppendLine("			STAGE_SEQ					,	");
		oSqlBuilder.AppendLine("			STAGE_CLEAR_FLAG			,	");
		oSqlBuilder.AppendLine("			NEXT_STAGE_MOVED_FLAG			");
		oSqlBuilder.AppendLine("		FROM								");
		oSqlBuilder.AppendLine("			T_STAGE_PROGRESS				");
		oSqlBuilder.AppendLine("		WHERE								");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			STAGE_SEQ			= :STAGE_SEQ			");
		oSqlBuilder.AppendLine("	) PROGRESS								");
		oSqlBuilder.AppendLine(" WHERE								");
		oSqlBuilder.AppendLine("	T_STAGE.SITE_CD					= PROGRESS.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	T_STAGE.STAGE_SEQ				= PROGRESS.STAGE_SEQ		(+)	AND	");
		oSqlBuilder.AppendLine("	T_STAGE.SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	T_STAGE.STAGE_SEQ				= :STAGE_SEQ					AND	");
		oSqlBuilder.AppendLine("	T_STAGE.SEX_CD					= :SEX_CD						AND	");
		oSqlBuilder.AppendLine("	(PROGRESS.STAGE_CLEAR_FLAG		= :STAGE_CLEAR_FLAG			OR PROGRESS.STAGE_CLEAR_FLAG IS NULL)		AND	");
		oSqlBuilder.AppendLine("	(PROGRESS.NEXT_STAGE_MOVED_FLAG	= :NEXT_STAGE_MOVED_FLAG	OR PROGRESS.NEXT_STAGE_MOVED_FLAG IS NULL)		");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":STAGE_SEQ",pCondition.StageSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":STAGE_CLEAR_FLAG",pCondition.StageClearFlag));
		oParamList.Add(new OracleParameter(":NEXT_STAGE_MOVED_FLAG",pCondition.NextStageMovedFlag));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetOneStageClearData(StageSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	EXP							,	");
		oSqlBuilder.AppendLine("	HONOR						,	");
		oSqlBuilder.AppendLine("	STAGE_NM					,	");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE			,	");
		oSqlBuilder.AppendLine("	STAGE_GROUP_NM				,	");
		oSqlBuilder.AppendLine("	NEXT_STAGE_NM				,	");
		oSqlBuilder.AppendLine("	NEXT_STAGE_GROUP_TYPE		,	");
		oSqlBuilder.AppendLine("	NEXT_STAGE_GROUP_NM			,	");
		oSqlBuilder.AppendLine("	BOSS_NM							");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_GAME_STAGE03				");

		// where
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		
		return oDataSet;
	}

	public DataSet GetStageClearItem(StageSeekCondition pCondition) {
		
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ							,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM							,	");
		oSqlBuilder.AppendLine("	ITEM_COUNT AS GAME_ITEM_COUNT			,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_GROUP_TYPE				");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	VW_PW_GAME_ITEM_STAGE_CLEAR01				");

		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" STAGE_SEQ = :STAGE_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":STAGE_SEQ",pCondition.StageSeq));

		SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));

		oSqlBuilder.AppendLine(sWhereClause);
		
		oSqlBuilder.AppendLine(" ORDER BY GAME_ITEM_SEQ ASC");

		DataSet pDS = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return pDS;
	}

	public void ClearStage(string pSiteCd,string pUserSeq,string pUserCharNo,string pStageSeq,out int pLevelUpFlag,out int pAddForceCount,out int pStageClearPointFlag,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CLEAR_STAGE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.VARCHAR2,pStageSeq);
			db.ProcedureOutParm("pLEVEL_UP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pADD_FORCE_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("pGET_GAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pGET_GAME_ITEM_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pGET_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTAGE_CLEAR_POINT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pLevelUpFlag = db.GetIntValue("pLEVEL_UP_FLAG");
			pAddForceCount = db.GetIntValue("pADD_FORCE_COUNT");
			pStageClearPointFlag = db.GetIntValue("pSTAGE_CLEAR_POINT_FLAG");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public string GetHonor(string pSiteCd,int pStageLevel,string pSexCd) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	HONOR							");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	T_STAGE							");
		oSqlBuilder.AppendLine(" WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD		 = :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	STAGE_LEVEL	 = :STAGE_LEVEL	AND	");
		oSqlBuilder.AppendLine("	SEX_CD		 = :SEX_CD			");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":STAGE_LEVEL",pStageLevel);
				this.cmd.Parameters.Add(":SEX_CD",pSexCd);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["HONOR"].ToString());
				} else {
					sValue = null;
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public string GetStageClearFlag(string pSiteCd,string pUserSeq,string pUserCharNo,string pStageSeq) {
		string sValue;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	STAGE_CLEAR_FLAG					");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	T_STAGE_PROGRESS					");
		oSqlBuilder.AppendLine(" WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD		 = :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ	 = :USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("	STAGE_SEQ    = :STAGE_SEQ			");


		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				this.cmd.Parameters.Add(":STAGE_SEQ",pStageSeq);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["STAGE_CLEAR_FLAG"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}
	
	public string GetStageLevelInStageTypeByStageSeq(string pSiteCd,string pStageSeq,string pSexCd) {
		string sValue = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	GROUP_LEVEL															");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("(																		");
		oSqlBuilder.AppendLine("	SELECT																");
		oSqlBuilder.AppendLine("		STAGE_SEQ													,	");
		oSqlBuilder.AppendLine("		ROW_NUMBER() OVER (ORDER BY STAGE_LEVEL ASC) GROUP_LEVEL		");
		oSqlBuilder.AppendLine("	FROM																");
		oSqlBuilder.AppendLine("		T_STAGE															");
		oSqlBuilder.AppendLine("	WHERE																");
		oSqlBuilder.AppendLine("		SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("		STAGE_GROUP_TYPE		= (										");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				STAGE_GROUP_TYPE										");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_STAGE													");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD		= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("				STAGE_SEQ	= :STAGE_SEQ								");
		oSqlBuilder.AppendLine("		)															AND	");
		oSqlBuilder.AppendLine("		SEX_CD				 = :SEX_CD									");
		oSqlBuilder.AppendLine(")																		");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	STAGE_SEQ = :STAGE_SEQ												");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":STAGE_SEQ",pStageSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["GROUP_LEVEL"].ToString();
		} else {
			sValue = "1";
		}
		
		return sValue;
	}
	
	public string GetPreGroupStageSeq(string pSiteCd,string pSexCd,string pStageSeq) {
		string sValue = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	PRE.STAGE_SEQ AS PRE_GROUP_STAGE_SEQ					");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_STAGE PRE											,	");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			SITE_CD										,	");
		oSqlBuilder.AppendLine("			STAGE_SEQ									,	");
		oSqlBuilder.AppendLine("			STAGE_LEVEL									,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE							,	");
		oSqlBuilder.AppendLine("			SEX_CD											");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			T_STAGE											");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			STAGE_SEQ	= :STAGE_SEQ					AND	");
		oSqlBuilder.AppendLine("			SEX_CD		= :SEX_CD							");
		oSqlBuilder.AppendLine("	) SELF													");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	PRE.SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PRE.SEX_CD				= :SEX_CD					AND	");
		oSqlBuilder.AppendLine("	PRE.SITE_CD				= SELF.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	PRE.SEX_CD				= SELF.SEX_CD				AND	");
		oSqlBuilder.AppendLine("	PRE.STAGE_LEVEL			< SELF.STAGE_LEVEL			AND	");
		oSqlBuilder.AppendLine("	PRE.STAGE_GROUP_TYPE	!= SELF.STAGE_GROUP_TYPE	AND ");
		oSqlBuilder.AppendLine("	ROWNUM					= 1								");
		oSqlBuilder.AppendLine("ORDER BY PRE.STAGE_LEVEL DESC								");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":STAGE_SEQ",pStageSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["PRE_GROUP_STAGE_SEQ"].ToString();
		}

		return sValue;
	}

	public string GetStageGroupNo(string pSiteCd,string pSexCd,string pStageSeq) {
		string sValue = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	SGNO.STAGE_GROUP_NO														");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	T_STAGE ST															,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			SITE_CD														,	");
		oSqlBuilder.AppendLine("			SEX_CD														,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											,	");
		oSqlBuilder.AppendLine("			RANK() OVER(ORDER BY MIN(STAGE_LEVEL)) AS STAGE_GROUP_NO		");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_STAGE															");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD=:SITE_CD										 AND	");
		oSqlBuilder.AppendLine("			SEX_CD=:SEX_CD													");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,SEX_CD,STAGE_GROUP_TYPE							");
		oSqlBuilder.AppendLine("	) SGNO																	");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	ST.SITE_CD=:SITE_CD												 AND	");
		oSqlBuilder.AppendLine("	ST.STAGE_SEQ=:STAGE_SEQ											 AND	");
		oSqlBuilder.AppendLine("	ST.SEX_CD=:SEX_CD												 AND	");
		oSqlBuilder.AppendLine("	ST.SITE_CD=SGNO.SITE_CD											 AND	");
		oSqlBuilder.AppendLine("	ST.SEX_CD=SGNO.SEX_CD											 AND	");
		oSqlBuilder.AppendLine("	ST.STAGE_GROUP_TYPE=SGNO.STAGE_GROUP_TYPE								");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":STAGE_SEQ",pStageSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["STAGE_GROUP_NO"].ToString();
		}

		return sValue;
	}
	
	public DataSet GetGameTutorialStageSeq(StageSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	STAGE_SEQ							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_STAGE								");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	SEX_CD			= :SEX_CD		AND	");
		oSqlBuilder.AppendLine("	STAGE_LEVEL		= (SELECT MIN(STAGE_LEVEL) FROM T_STAGE WHERE SITE_CD = :SITE_CD AND SEX_CD = :SEX_CD)");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}
}
