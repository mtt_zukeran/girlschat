﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: おねだりできる出演者
--	Progaram ID		: CastCanMailRequest
--
--  Creation Date	: 2015.03.13
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class CastCanMailRequestSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public string HandleNm {
		get {
			return this.Query["ext_handle"];
		}
		set {
			this.Query["ext_handle"] = value;
		}
	}

	public string AgeRange {
		get {
			return this.Query["ext_agerange"];
		}
		set {
			this.Query["ext_agerange"] = value;
		}
	}

	public string UserStatus {
		get {
			return this.Query["ext_userstatus"];
		}
		set {
			this.Query["ext_userstatus"] = value;
		}
	}

	public string CastItem01 {
		get {
			return this.Query["ext_item01"];
		}
		set {
			this.Query["ext_item01"] = value;
		}
	}

	public string CastItem09 {
		get {
			return this.Query["ext_item09"];
		}
		set {
			this.Query["ext_item09"] = value;
		}
	}

	public string CastItem11 {
		get {
			return this.Query["ext_item11"];
		}
		set {
			this.Query["ext_item11"] = value;
		}
	}

	public string CastItem12 {
		get {
			return this.Query["ext_item12"];
		}
		set {
			this.Query["ext_item12"] = value;
		}
	}
	
	public int NewCastDay;
	
	public CastCanMailRequestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastCanMailRequestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["ext_handle"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_handle"]));
		this.Query["ext_agerange"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_agerange"]));
		this.Query["ext_userstatus"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_userstatus"]));
		this.Query["ext_waittype"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_waittype"]));
		this.Query["ext_item01"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_item01"]));
		this.Query["ext_item09"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_item09"]));
		this.Query["ext_item11"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_item11"]));
		this.Query["ext_item12"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ext_item12"]));
	}
}

#endregion ============================================================================================================

public class CastCanMailRequest:DbSession {
	public CastCanMailRequest() {
	}

	public int GetPageCount(CastCanMailRequestSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string pWhereClause = string.Empty;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	VW_CAST_CHARACTER00 P			,	");
		oSqlBuilder.AppendLine("	T_FAVORIT F						,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX SME			");

		SysPrograms.SqlAppendWhere(" P.SITE_CD				= SME.SITE_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.SITE_CD				= F.SITE_CD (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ				= F.USER_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO			= F.USER_CHAR_NO (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" :SELF_USER_SEQ			= F.PARTNER_USER_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" :SELF_USER_CHAR_NO		= F.PARTNER_USER_CHAR_NO (+)",ref pWhereClause);

		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref pWhereClause);
		oSqlBuilder.AppendLine(pWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastCanMailRequestSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string pWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sPagingSql = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		StringBuilder oOuterSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																					");
		oSqlBuilder.AppendLine("	P.SITE_CD																		,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ																		,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO																	,	");
		oSqlBuilder.AppendLine("	P.AGE																			,	");
		oSqlBuilder.AppendLine("	P.HANDLE_NM																		,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID																		,	");
		oSqlBuilder.AppendLine("	CASE																				");
		oSqlBuilder.AppendLine("		WHEN (F.PROFILE_PIC_NON_PUBLISH_FLAG = 2) THEN									");
		oSqlBuilder.AppendLine("			GET_SMALL_PHOTO_IMG_PATH (P.SITE_CD,P.LOGIN_ID,NULL)						");
		oSqlBuilder.AppendLine("		ELSE																			");
		oSqlBuilder.AppendLine("			GET_SMALL_PHOTO_IMG_PATH (P.SITE_CD,P.LOGIN_ID,P.PROFILE_PIC_SEQ)			");
		oSqlBuilder.AppendLine("	END AS SMALL_PHOTO_IMG_PATH														,	");
		oSqlBuilder.AppendLine("	CASE																				");
		oSqlBuilder.AppendLine("		WHEN (F.PROFILE_PIC_NON_PUBLISH_FLAG = 2) THEN									");
		oSqlBuilder.AppendLine("			GET_PHOTO_IMG_PATH (P.SITE_CD,P.LOGIN_ID,NULL)								");
		oSqlBuilder.AppendLine("		ELSE																			");
		oSqlBuilder.AppendLine("			GET_PHOTO_IMG_PATH (P.SITE_CD,P.LOGIN_ID,P.PROFILE_PIC_SEQ)					");
		oSqlBuilder.AppendLine("	END AS PHOTO_IMG_PATH															,	");
		oSqlBuilder.AppendLine("	P.COMMENT_LIST																	,	");
		oSqlBuilder.AppendLine("	P.COMMENT_DETAIL																,	");
		oSqlBuilder.AppendLine("	P.MONITOR_TALK_TYPE																,	");
		oSqlBuilder.AppendLine("	P.MONITOR_ENABLE_FLAG															,	");
		oSqlBuilder.AppendLine("	P.WSHOT_NA_FLAG																	,	");
		oSqlBuilder.AppendLine("	P.CONNECT_TYPE																	,	");
		oSqlBuilder.AppendLine("	P.CHARACTER_ONLINE_STATUS														,	");
		oSqlBuilder.AppendLine("	P.IVP_REQUEST_SEQ																,	");
		oSqlBuilder.AppendLine("	P.ACT_CATEGORY_SEQ																,	");
		oSqlBuilder.AppendLine("	P.START_PERFORM_DAY																,	");
		oSqlBuilder.AppendLine("	P.TALK_LOCK_FLAG																,	");
		oSqlBuilder.AppendLine("	P.TALK_LOCK_DATE																,	");
		oSqlBuilder.AppendLine("	F.OFFLINE_LAST_UPDATE_DATE														,	");
		oSqlBuilder.AppendLine("	NVL(F.WAITING_VIEW_STATUS,0) AS WAITING_VIEW_STATUS								,	");
		oSqlBuilder.AppendLine("	NVL(F.LOGIN_VIEW_STATUS,0) AS LOGIN_VIEW_STATUS									,	");
		oSqlBuilder.AppendLine("	NVL(F.USE_COMMENT_DETAIL_FLAG,0) AS USE_COMMENT_DETAIL_FLAG							");
		oSqlBuilder.AppendLine("FROM																					");
		oSqlBuilder.AppendLine("	VW_CAST_CHARACTER00 P															,	");
		oSqlBuilder.AppendLine("	T_FAVORIT F																		,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX SME															");

		SysPrograms.SqlAppendWhere(" P.SITE_CD				= SME.SITE_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.SITE_CD				= F.SITE_CD (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ				= F.USER_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO			= F.USER_CHAR_NO (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" :SELF_USER_SEQ			= F.PARTNER_USER_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" :SELF_USER_CHAR_NO		= F.PARTNER_USER_CHAR_NO (+)",ref pWhereClause);

		oParamList.AddRange(this.CreateWhere(pCondition,ref pWhereClause));
		oSqlBuilder.AppendLine(pWhereClause);
		
		sSortExpression = "ORDER BY P.SITE_CD,P.LAST_LOGIN_DATE DESC,P.NA_FLAG,P.CHARACTER_ONLINE_STATUS,P.USER_SEQ,P.USER_CHAR_NO";
		
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		oOuterSqlBuilder.AppendLine("SELECT																						");
		oOuterSqlBuilder.AppendLine("	P.*																					,	");
		oOuterSqlBuilder.AppendLine("	NVL(ECN.CRYPT_VALUE,P.USER_CHAR_NO) AS CRYPT_VALUE									,	");
		oOuterSqlBuilder.AppendLine("	CASE																					");
		oOuterSqlBuilder.AppendLine("		WHEN (P.OFFLINE_LAST_UPDATE_DATE > SYSDATE) THEN									");
		oOuterSqlBuilder.AppendLine("			SM.LOGINED_GUIDANCE																");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 3) THEN											");
		oOuterSqlBuilder.AppendLine("			CASE																			");
		oOuterSqlBuilder.AppendLine("				WHEN (P.WAITING_VIEW_STATUS = 1) THEN										");
		oOuterSqlBuilder.AppendLine("					SM.OFFLINE_GUIDANCE														");
		oOuterSqlBuilder.AppendLine("				WHEN (P.WAITING_VIEW_STATUS = 2) THEN										");
		oOuterSqlBuilder.AppendLine("					SM.LOGINED_GUIDANCE														");
		oOuterSqlBuilder.AppendLine("				WHEN (P.MONITOR_TALK_TYPE != 0) AND (IVP.REQUEST_ID IN ('01','07','11','17')) AND (IVP.CONNECT_TYPE = '1') THEN");
		oOuterSqlBuilder.AppendLine("					SM.TALKING_PARTY_GUIDANCE												");
		oOuterSqlBuilder.AppendLine("				WHEN (P.MONITOR_TALK_TYPE != 0) AND (IVP.REQUEST_ID IN ('01','07','11','17','06')) AND (IVP.CONNECT_TYPE = '2') AND (SM.SUPPORT_VOICE_MONITOR_FLAG != 0) THEN");
		oOuterSqlBuilder.AppendLine("					SM.TALKING_PARTY_GUIDANCE												");
		oOuterSqlBuilder.AppendLine("				ELSE																		");
		oOuterSqlBuilder.AppendLine("					SM.TALKING_WSHOT_GUIDANCE												");
		oOuterSqlBuilder.AppendLine("			END																				");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 0) THEN											");
		oOuterSqlBuilder.AppendLine("			SM.OFFLINE_GUIDANCE																");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 1) THEN											");
		oOuterSqlBuilder.AppendLine("			CASE																			");
		oOuterSqlBuilder.AppendLine("				WHEN (P.LOGIN_VIEW_STATUS = 1) THEN											");
		oOuterSqlBuilder.AppendLine("					SM.OFFLINE_GUIDANCE														");
		oOuterSqlBuilder.AppendLine("				ELSE																		");
		oOuterSqlBuilder.AppendLine("					SM.LOGINED_GUIDANCE														");
		oOuterSqlBuilder.AppendLine("			END																				");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 2) THEN											");
		oOuterSqlBuilder.AppendLine("			CASE																			");
		oOuterSqlBuilder.AppendLine("				WHEN (P.WAITING_VIEW_STATUS = 1) THEN										");
		oOuterSqlBuilder.AppendLine("					SM.OFFLINE_GUIDANCE														");
		oOuterSqlBuilder.AppendLine("				WHEN (P.WAITING_VIEW_STATUS = 2) THEN										");
		oOuterSqlBuilder.AppendLine("					SM.LOGINED_GUIDANCE														");
		oOuterSqlBuilder.AppendLine("				ELSE																		");
		oOuterSqlBuilder.AppendLine("					SM.WAITING_GUIDANCE														");
		oOuterSqlBuilder.AppendLine("		END																					");
		oOuterSqlBuilder.AppendLine("	END AS MOBILE_ONLINE_STATUS_NM														,	");
		oOuterSqlBuilder.AppendLine("	CASE																					");
		oOuterSqlBuilder.AppendLine("		WHEN (P.OFFLINE_LAST_UPDATE_DATE > SYSDATE) THEN									");
		oOuterSqlBuilder.AppendLine("			1																				");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 3) THEN											");
		oOuterSqlBuilder.AppendLine("			CASE																			");
		oOuterSqlBuilder.AppendLine("				WHEN P.WAITING_VIEW_STATUS = 1 THEN											");
		oOuterSqlBuilder.AppendLine("					0																		");
		oOuterSqlBuilder.AppendLine("				WHEN P.WAITING_VIEW_STATUS = 2 THEN											");
		oOuterSqlBuilder.AppendLine("					1																		");
		oOuterSqlBuilder.AppendLine("				ELSE																		");
		oOuterSqlBuilder.AppendLine("					P.CHARACTER_ONLINE_STATUS												");
		oOuterSqlBuilder.AppendLine("			END																				");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 1) THEN											");
		oOuterSqlBuilder.AppendLine("			CASE																			");
		oOuterSqlBuilder.AppendLine("				WHEN P.LOGIN_VIEW_STATUS = 1 THEN											");
		oOuterSqlBuilder.AppendLine("					0																		");
		oOuterSqlBuilder.AppendLine("				ELSE																		");
		oOuterSqlBuilder.AppendLine("					P.CHARACTER_ONLINE_STATUS												");
		oOuterSqlBuilder.AppendLine("			END																				");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CHARACTER_ONLINE_STATUS = 2) THEN											");
		oOuterSqlBuilder.AppendLine("			CASE																			");
		oOuterSqlBuilder.AppendLine("				WHEN P.WAITING_VIEW_STATUS = 1 THEN											");
		oOuterSqlBuilder.AppendLine("					0																		");
		oOuterSqlBuilder.AppendLine("				WHEN P.WAITING_VIEW_STATUS = 2 THEN											");
		oOuterSqlBuilder.AppendLine("					1																		");
		oOuterSqlBuilder.AppendLine("				ELSE																		");
		oOuterSqlBuilder.AppendLine("					P.CHARACTER_ONLINE_STATUS												");
		oOuterSqlBuilder.AppendLine("			END																				");
		oOuterSqlBuilder.AppendLine("		ELSE																				");
		oOuterSqlBuilder.AppendLine("			P.CHARACTER_ONLINE_STATUS														");
		oOuterSqlBuilder.AppendLine("	END AS FAKE_ONLINE_STATUS															,	");
		oOuterSqlBuilder.AppendLine("	CASE																					");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CONNECT_TYPE = '1') THEN													");
		oOuterSqlBuilder.AppendLine("			SM.CONNECT_TYPE_VIDEO_GUIDANCE													");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CONNECT_TYPE = '2') THEN													");
		oOuterSqlBuilder.AppendLine("			SM.CONNECT_TYPE_VOICE_GUIDANCE													");
		oOuterSqlBuilder.AppendLine("		WHEN (P.CONNECT_TYPE = '3') THEN													");
		oOuterSqlBuilder.AppendLine("			SM.CONNECT_TYPE_BOTH_GUIDANCE													");
		oOuterSqlBuilder.AppendLine("	END AS MOBILE_CONNECT_TYPE_NM														,	");
		oOuterSqlBuilder.AppendLine("	CASE																					");
		oOuterSqlBuilder.AppendLine("		WHEN (P.START_PERFORM_DAY >= TO_CHAR(SYSDATE - SM.NEW_FACE_DAYS,'YYYY/MM/DD')) THEN	");
		oOuterSqlBuilder.AppendLine("			SM.NEW_FACE_MARK																");
		oOuterSqlBuilder.AppendLine("		ELSE																				");
		oOuterSqlBuilder.AppendLine("			NULL																			");
		oOuterSqlBuilder.AppendLine("	END AS NEW_CAST_SIGN																,	");
		oOuterSqlBuilder.AppendLine("	(SELECT NVL(COUNT(USER_SEQ),0) AS CNT FROM T_FAVORIT									");
		oOuterSqlBuilder.AppendLine("		WHERE																				");
		oOuterSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD											AND	");
		oOuterSqlBuilder.AppendLine("			USER_SEQ				= :SELF_USER_SEQ									AND	");
		oOuterSqlBuilder.AppendLine("			USER_CHAR_NO			= :SELF_USER_CHAR_NO								AND	");
		oOuterSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= P.USER_SEQ										AND	");
		oOuterSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO ) AS FAVORIT_FLAG					,	");
		oOuterSqlBuilder.AppendLine("	CASE																					");
		oOuterSqlBuilder.AppendLine("		WHEN P.LOGIN_VIEW_STATUS IS NOT NULL THEN											");
		oOuterSqlBuilder.AppendLine("			1																				");
		oOuterSqlBuilder.AppendLine("		ELSE																				");
		oOuterSqlBuilder.AppendLine("			0																				");
		oOuterSqlBuilder.AppendLine("	END AS LIKE_ME_FLAG																	,	");
		oOuterSqlBuilder.AppendLine("	NVL(P.USE_COMMENT_DETAIL_FLAG,0) AS USE_COMMENT_DETAIL_FLAG							,	");
		oOuterSqlBuilder.AppendLine("	AC.PRIORITY AS ACT_CATEGORY_IDX														,	");
		oOuterSqlBuilder.AppendLine("	IVP.MAN_USER_SITE_CD																,	");
		oOuterSqlBuilder.AppendLine("	AT02.DISPLAY_VALUE AS CAST_ATTR_VALUE02												,	");
		oOuterSqlBuilder.AppendLine("	AT09.DISPLAY_VALUE AS CAST_ATTR_VALUE09													");
		oOuterSqlBuilder.AppendLine("FROM																						");
		oOuterSqlBuilder.AppendLine("	({0}) P																				,	");
		oOuterSqlBuilder.AppendLine("	T_ACT_CATEGORY AC																	,	");
		oOuterSqlBuilder.AppendLine("	T_EX_CHAR_NO ECN																	,	");
		oOuterSqlBuilder.AppendLine("	T_IVP_REQUEST IVP																	,	");
		oOuterSqlBuilder.AppendLine("	T_SITE_MANAGEMENT SM																,	");
		oOuterSqlBuilder.AppendLine("	VW_CAST_ATTR_VALUE01 AT02															,	");
		oOuterSqlBuilder.AppendLine("	VW_CAST_ATTR_VALUE01 AT09																");
		oOuterSqlBuilder.AppendLine("WHERE																						");
		oOuterSqlBuilder.AppendLine("	P.SITE_CD			= AC.SITE_CD													AND	");
		oOuterSqlBuilder.AppendLine("	P.ACT_CATEGORY_SEQ	= AC.ACT_CATEGORY_SEQ											AND	");
		oOuterSqlBuilder.AppendLine("	P.SITE_CD			= ECN.SITE_CD													AND	");
		oOuterSqlBuilder.AppendLine("	P.USER_CHAR_NO		= ECN.USER_CHAR_NO												AND	");
		oOuterSqlBuilder.AppendLine("	:LATEST_FLAG		= :LATEST_FLAG													AND	");
		oOuterSqlBuilder.AppendLine("	P.SITE_CD			= SM.SITE_CD													AND	");
		oOuterSqlBuilder.AppendLine("	P.IVP_REQUEST_SEQ	= IVP.IVP_REQUEST_SEQ										(+)	AND	");
		oOuterSqlBuilder.AppendLine("	P.SITE_CD			= AT02.SITE_CD												(+)	AND	");
		oOuterSqlBuilder.AppendLine("	P.USER_SEQ			= AT02.USER_SEQ												(+)	AND	");
		oOuterSqlBuilder.AppendLine("	P.USER_CHAR_NO		= AT02.USER_CHAR_NO											(+)	AND	");
		oOuterSqlBuilder.AppendLine("	AT02.ITEM_NO		= :ITEM_NO02													AND	");
		oOuterSqlBuilder.AppendLine("	P.SITE_CD			= AT09.SITE_CD												(+)	AND	");
		oOuterSqlBuilder.AppendLine("	P.USER_SEQ			= AT09.USER_SEQ												(+)	AND	");
		oOuterSqlBuilder.AppendLine("	P.USER_CHAR_NO		= AT09.USER_CHAR_NO											(+)	AND	");
		oOuterSqlBuilder.AppendLine("	AT09.ITEM_NO		= :ITEM_NO09														");

		oParamList.Add(new OracleParameter(":LATEST_FLAG",ViCommConst.FLAG_ON));
		oParamList.Add(new OracleParameter(":ITEM_NO02","02"));
		oParamList.Add(new OracleParameter(":ITEM_NO09","09"));
		
		DataSet oDataSet = ExecuteSelectQueryBase(string.Format(oOuterSqlBuilder.ToString(),sExecSql),oParamList.ToArray());

		FakeOnlineStatus(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastCanMailRequestSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		SysPrograms.SqlAppendWhere("(SYSDATE BETWEEN SME.MAIL_REQUEST_OPEN_START_DATE AND SME.MAIL_REQUEST_OPEN_END_DATE OR NOT EXISTS(SELECT 1 FROM T_COMMUNICATION WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND PARTNER_USER_SEQ = P.USER_SEQ AND PARTNER_USER_CHAR_NO = P.USER_CHAR_NO AND TOTAL_RX_MAIL_COUNT > 0))",ref pWhereClause);

		if (!string.IsNullOrEmpty(pCondition.HandleNm)) {
			SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE '%' || :EXT_HANDLE_NM || '%' ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":EXT_HANDLE_NM",pCondition.HandleNm));
		}

		if (!string.IsNullOrEmpty(pCondition.AgeRange)) {
			string[] sAgeRange = HttpUtility.UrlDecode(pCondition.AgeRange).Split(':');
			string sAgeFrom = sAgeRange[0];

			SysPrograms.SqlAppendWhere("P.AGE >= :EXT_AGE_FROM ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":EXT_AGE_FROM",sAgeFrom));
			
			if (sAgeRange.Length > 1) {
				string sAgeTo = sAgeRange[1];
				SysPrograms.SqlAppendWhere("P.AGE <=  :EXT_AGE_TO ",ref pWhereClause);
				oParamList.Add(new OracleParameter(":EXT_AGE_TO",sAgeTo));
			}
		}
		
		if (!string.IsNullOrEmpty(pCondition.CastItem01)) {
			string[] sItemCdList01;
			sItemCdList01 = HttpUtility.UrlDecode(pCondition.CastItem01).Split(':');
			SysPrograms.SqlAppendWhere(string.Format(" EXISTS(SELECT 1 FROM VW_CAST_ATTR_VALUE01 WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND ITEM_NO = '01' AND ITEM_CD IN ('{0}'))",string.Join("','",sItemCdList01)),ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.CastItem09)) {
			string[] sItemCdList09;
			sItemCdList09 = HttpUtility.UrlDecode(pCondition.CastItem09).Split(':');
			SysPrograms.SqlAppendWhere(string.Format(" EXISTS(SELECT 1 FROM VW_CAST_ATTR_VALUE01 WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND ITEM_NO = '09' AND ITEM_CD IN ('{0}'))",string.Join("','",sItemCdList09)),ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.CastItem11)) {
			SysPrograms.SqlAppendWhere(" EXISTS(SELECT 1 FROM VW_CAST_ATTR_VALUE01 WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND ITEM_NO = '11' AND ITEM_CD = :ITEM_CD11)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ITEM_CD11",pCondition.CastItem11));
		}

		if (!string.IsNullOrEmpty(pCondition.CastItem12)) {
			SysPrograms.SqlAppendWhere(" EXISTS(SELECT 1 FROM VW_CAST_ATTR_VALUE01 WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND ITEM_NO = '12' AND ITEM_CD = :ITEM_CD12)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ITEM_CD12",pCondition.CastItem12));
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" EXISTS(SELECT 1 FROM T_CAST_CHARACTER_EX WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND MAIL_REQUEST_REFUSE_FLAG = :MAIL_REQUEST_REFUSE_FLAG_OFF)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":MAIL_REQUEST_REFUSE_FLAG_OFF",ViCommConst.FLAG_OFF));

		switch (pCondition.UserStatus) {
			case "1":
				SysPrograms.SqlAppendWhere(" (F.WAITING_VIEW_STATUS IS NULL OR F.WAITING_VIEW_STATUS = 0 OR F.OFFLINE_LAST_UPDATE_DATE > SYSDATE)",ref pWhereClause);
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhereClause);
				oParamList.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				oParamList.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;
			case "2":
				SysPrograms.SqlAppendWhere("P.START_PERFORM_DAY  >= :NEW_DAY ",ref pWhereClause);
				oParamList.Add(new OracleParameter("NEW_DAY",DateTime.Now.AddDays(-1 * pCondition.NewCastDay).ToString("yyyy/MM/dd")));
				SysPrograms.SqlAppendWhere(" (F.LOGIN_VIEW_STATUS IS NULL OR F.LOGIN_VIEW_STATUS = 0 OR F.OFFLINE_LAST_UPDATE_DATE > SYSDATE)",ref pWhereClause);
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhereClause);
				oParamList.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				oParamList.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				oParamList.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;
			default:
				SysPrograms.SqlAppendWhere(" (F.LOGIN_VIEW_STATUS IS NULL OR F.LOGIN_VIEW_STATUS = 0 OR F.OFFLINE_LAST_UPDATE_DATE > SYSDATE)",ref pWhereClause);
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhereClause);
				oParamList.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				oParamList.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				oParamList.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;
		}

		return oParamList.ToArray();
	}

	private void FakeOnlineStatus(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
		}
	}
}
