﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい
--	Progaram ID		: PwRequest
--  Creation Date	: 2012.06.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class PwRequestSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SexCd;
	public string DeleteFlag;
	public string BlogFlag;
	public string RichFlag;
	public string PublicStatus;

	public string RequestSeq {
		get {
			return this.Query["req_seq"];
		}
		set {
			this.Query["req_seq"] = value;
		}
	}

	public string RequestCategorySeq {
		get {
			return this.Query["ctg_seq"];
		}
		set {
			this.Query["ctg_seq"] = value;
		}
	}

	public string RequestProgressSeq {
		get {
			return this.Query["prg_seq"];
		}
		set {
			this.Query["prg_seq"] = value;
		}
	}

	public string Keyword {
		get {
			return this.Query["kwd"];
		}
		set {
			this.Query["kwd"] = value;
		}
	}

	public PwRequestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public PwRequestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["req_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["req_seq"]));
		this.Query["ctg_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ctg_seq"]));
		this.Query["prg_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["prg_seq"]));
		this.Query["kwd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["kwd"]));
	}
}

public class PwRequest:DbSession {
	public PwRequest() {
	}

	public int GetPageCount(PwRequestSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sViewNm = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		if (pCondtion.SexCd.Equals(ViCommConst.MAN)) {
			sViewNm = "VW_PW_MAN_REQUEST01";
		} else {
			sViewNm = "VW_PW_CAST_REQUEST01";
		}

		oSqlBuilder.AppendLine("SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM			");
		oSqlBuilder.AppendFormat("	{0}	",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(PwRequestSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(PwRequestSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sViewNm = string.Empty;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		if (pCondtion.SexCd.Equals(ViCommConst.MAN)) {
			sViewNm = "VW_PW_MAN_REQUEST01";
		} else {
			sViewNm = "VW_PW_CAST_REQUEST01";
		}

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	*	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendFormat("	{0}	",sViewNm);
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(PwRequestSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.DeleteFlag)) {
			SysPrograms.SqlAppendWhere("DELETE_FLAG = :DELETE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DELETE_FLAG",pCondition.DeleteFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.RequestSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_SEQ = :REQUEST_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_SEQ",pCondition.RequestSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.RequestCategorySeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_CATEGORY_SEQ = :REQUEST_CATEGORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_CATEGORY_SEQ",pCondition.RequestCategorySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.RequestProgressSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_PROGRESS_SEQ = :REQUEST_PROGRESS_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_PROGRESS_SEQ",pCondition.RequestProgressSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.Keyword)) {
			SysPrograms.SqlAppendWhere("(TITLE LIKE '%' || :KEYWORD01 || '%' OR TEXT LIKE '%' || :KEYWORD02 || '%' OR ADMIN_COMMENT LIKE '%' || :KEYWORD03 || '%')",ref pWhereClause);
			oParamList.Add(new OracleParameter(":KEYWORD01",pCondition.Keyword));
			oParamList.Add(new OracleParameter(":KEYWORD02",pCondition.Keyword));
			oParamList.Add(new OracleParameter(":KEYWORD03",pCondition.Keyword));
		}

		if (!string.IsNullOrEmpty(pCondition.BlogFlag)) {
			SysPrograms.SqlAppendWhere("BLOG_FLAG = :BLOG_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_FLAG",pCondition.BlogFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.RichFlag)) {
			SysPrograms.SqlAppendWhere("RICH_FLAG = :RICH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":RICH_FLAG",pCondition.RichFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.PublicStatus)) {
			SysPrograms.SqlAppendWhere("PUBLIC_STATUS = :PUBLIC_STATUS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PUBLIC_STATUS",pCondition.PublicStatus));
		}

		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(PwRequestSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.RequestSort.GOOD_COUNT_DESC:
				sSortExpression = " ORDER BY GOOD_COUNT DESC, CREATE_DATE DESC";
				break;
			case PwViCommConst.RequestSort.BAD_COUNT_DESC:
				sSortExpression = " ORDER BY BAD_COUNT DESC, CREATE_DATE DESC";
				break;
			case PwViCommConst.RequestSort.PUBLIC_DATE_DESC:
				sSortExpression = " ORDER BY PUBLIC_DATE DESC";
				break;
			case PwViCommConst.RequestSort.PUBLIC_DATE_ASC:
				sSortExpression = " ORDER BY PUBLIC_DATE ASC";
				break;
			case PwViCommConst.RequestSort.VALUE_COUNT_DESC:
				sSortExpression = " ORDER BY VALUE_COUNT DESC, CREATE_DATE DESC";
				break;
			case PwViCommConst.RequestSort.LAST_VALUE_DATE_DESC:
				sSortExpression = " ORDER BY LAST_VALUE_DATE DESC NULLS LAST, CREATE_DATE DESC";
				break;
			case PwViCommConst.RequestSort.LAST_VALUE_DATE_ASC:
				sSortExpression = " ORDER BY LAST_VALUE_DATE ASC NULLS LAST, CREATE_DATE DESC";
				break;
			default:
				sSortExpression = " ORDER BY GOOD_COUNT DESC, CREATE_DATE DESC";
				break;
		}

		return sSortExpression;
	}

	public DataSet GetRequestProgress(string pSiteCd) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_SEQ,	");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_NM		");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_REQUEST_PROGRESS		");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetRequestCategory(string pSiteCd,string pSexCd,string pBlogFlag,string pRichFlag) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		if (!string.IsNullOrEmpty(pBlogFlag)) {
			SysPrograms.SqlAppendWhere("BLOG_FLAG = :BLOG_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_FLAG",pBlogFlag));
		}

		if (!string.IsNullOrEmpty(pRichFlag)) {
			SysPrograms.SqlAppendWhere("RICH_FLAG = :RICH_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":RICH_FLAG",pRichFlag));
		}

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	REQUEST_CATEGORY_SEQ,	");
		oSqlBuilder.AppendLine("	REQUEST_CATEGORY_NM		");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_REQUEST_CATEGORY		");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public string WriteRequest(
		string pSiteCd,
		string pSexCd,
		string pUserSeq,
		string pUserCharNo,
		string pTitle,
		string pText,
		string pRequestCategorySeq,
		out string pRequestSeq
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_REQUEST");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pTITLE",DbSession.DbType.VARCHAR2,pTitle);
			db.ProcedureInParm("pTEXT",DbSession.DbType.VARCHAR2,pText);
			db.ProcedureInParm("pREQUEST_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,pRequestCategorySeq);
			db.ProcedureOutParm("pREQUEST_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pRequestSeq = db.GetStringValue("pREQUEST_SEQ");
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string ModifyRequest(
		string pSiteCd,
		string pRequestSeq,
		string pRequestCategorySeq,
		string pAdminComment,
		string pRequestProgressSeq
	) {
		string sResult = string.Empty;

		return sResult;
	}

	public string DeleteRequest(
		string pSiteCd,
		string pRequestSeq,
		string pDeleteFlag
	) {
		string sResult = string.Empty;

		return sResult;
	}
}
