﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メールおねだり履歴
--	Progaram ID		: AccessMailLog
--
--  Creation Date	: 2014.12.17
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class AccessMailLog:DbSession {
	public AccessMailLog()
		: base() {
	}

	public void RegistAccessMailLog(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailTemplateNo,string pSexCd,string pTxMailDay) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_ACCESS_MAIL_LOG");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,pMailTemplateNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pTX_MAIL_DAY",DbSession.DbType.VARCHAR2,pTxMailDay);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}