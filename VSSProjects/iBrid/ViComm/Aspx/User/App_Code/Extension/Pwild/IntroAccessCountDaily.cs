﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 出演者異性紹介アクセス数データクラス
--	Progaram ID		: IntroAccessCountDaily
--
--  Creation Date	: 2015.12.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class IntroAccessCountDaily:DbSession {
	public IntroAccessCountDaily() {
	}

	public void RegistIntroAccessCount(string pIntroducerFriendCd) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_INTRO_ACCESS_COUNT");
			db.ProcedureInParm("pINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,pIntroducerFriendCd);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}