﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ仲間(ﾒﾝﾊﾞｰ選択)
--	Progaram ID		: CastGameCharacterFellowMember
--
--  Creation Date	: 2011.08.06
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterFellowMemberSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string FellowApplicationStatus {
		get {
			return this.Query["friend_application_status"];
		}
		set {
			this.Query["friend_application_status"] = value;
		}
	}

	public string Type {
		get {
			return this.Query["type"];
		}
		set {
			this.Query["type"] = value;
		}
	}

	public string LastHugDay {
		get {
			return this.Query["last_hug_day"];
		}
		set {
			this.Query["last_hug_day"] = value;
		}
	}
	
	public string PartyMemberFlag {
		get {
			return this.Query["party_member_flag"];
		}
		set {
			this.Query["party_member_flag"] = value;
		}
	}

	public CastGameCharacterFellowMemberSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterFellowMemberSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["type"]));
	}
}

[System.Serializable]
public class CastGameCharacterFellowMember:CastBase {
	public CastGameCharacterFellowMember()
		: base() {
	}
	public CastGameCharacterFellowMember(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastGameCharacterFellowMemberSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	COUNT(*)								");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHARACTER00 P		,	");
		oSqlBuilder.AppendLine("	T_GAME_FELLOW MBR						");
		// where
		string sWhereClause = string.Empty;
		SysPrograms.SqlAppendWhere(" P.SITE_CD = MBR.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ = MBR.PARTNER_USER_SEQ",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO = MBR.PARTNER_USER_CHAR_NO",ref sWhereClause);
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterFellowMemberSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT																			");
		oSqlBuilder.AppendLine("		" + CastBasicField() + "											,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM														,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE													,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL													,	");
		oSqlBuilder.AppendLine("	P.EXP																	,	");
		oSqlBuilder.AppendLine("	P.PARTNER_USER_SEQ														,	");
		oSqlBuilder.AppendLine("	P.PARTNER_USER_CHAR_NO													,	");
		oSqlBuilder.AppendLine("	P.PARTY_MEMBER_FLAG														,	");
		oSqlBuilder.AppendLine("	P.MAX_ATTACK_FORCE_COUNT												,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.RANK	AS FRIENDLY_RANK									,	");
		oSqlBuilder.AppendLine(" 	FRIENDLY_POINT.FRIENDLY_POINT												");
		oSqlBuilder.AppendLine(" FROM																			");
		oSqlBuilder.AppendLine("	(SELECT																		");
		oSqlBuilder.AppendLine("		" + CastBasicField() + "											,	");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM													,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE												,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL												,	");
		oSqlBuilder.AppendLine("		P.EXP																,	");
		oSqlBuilder.AppendLine("		MBR.PARTNER_USER_SEQ												,	");
		oSqlBuilder.AppendLine("		MBR.PARTNER_USER_CHAR_NO											,	");
		oSqlBuilder.AppendLine("		MBR.PARTY_MEMBER_FLAG												,	");
		oSqlBuilder.AppendLine("		AF.MAX_FORCE_COUNT AS MAX_ATTACK_FORCE_COUNT							");
		oSqlBuilder.AppendLine("	FROM																		");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHARACTER00 P										,	");
		oSqlBuilder.AppendLine("		T_GAME_FELLOW MBR													,	");
		oSqlBuilder.AppendLine("		T_ATTACK_FORCE AF														");
		// where
		SysPrograms.SqlAppendWhere(" P.SITE_CD = MBR.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ = MBR.PARTNER_USER_SEQ",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO = MBR.PARTNER_USER_CHAR_NO",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.SITE_CD = AF.SITE_CD",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_SEQ = AF.USER_SEQ",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO = AF.USER_CHAR_NO",ref sWhereClause);
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" 	) P																		,	");
		oSqlBuilder.AppendLine(" 	(SELECT																		");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.SITE_CD											,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.PARTNER_USER_SEQ								,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.PARTNER_USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.RANK											,	");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00.FRIENDLY_POINT										");
		oSqlBuilder.AppendLine(" 	FROM																		");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00														");
		// where		
		this.CreateWhereFriend(pCondtion,ref sWhereClauseFriend);
		oSqlBuilder.AppendLine(sWhereClauseFriend);
		oSqlBuilder.AppendLine(" 	) FRIENDLY_POINT															");
		oSqlBuilder.AppendLine(" WHERE																			");
		oSqlBuilder.AppendLine(" 	P.SITE_CD				= FRIENDLY_POINT.SITE_CD					(+) AND	");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_SEQ		= FRIENDLY_POINT.PARTNER_USER_SEQ			(+) AND	");
		oSqlBuilder.AppendLine(" 	P.PARTNER_USER_CHAR_NO	= FRIENDLY_POINT.PARTNER_USER_CHAR_NO		(+)		");

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastGameCharacterFellowMemberSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
		
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" MBR.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" MBR.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.FellowApplicationStatus)) {
			SysPrograms.SqlAppendWhere(" MBR.FELLOW_APPLICATION_STATUS = :FELLOW_APPLICATION_STATUS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pCondition.FellowApplicationStatus));
		}

		if (!string.IsNullOrEmpty(pCondition.Type)) {
			SysPrograms.SqlAppendWhere(" P.GAME_CHARACTER_TYPE = :GAME_CHARACTER_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GAME_CHARACTER_TYPE",pCondition.Type));
		}

		if (!string.IsNullOrEmpty(pCondition.LastHugDay)) {
			SysPrograms.SqlAppendWhere(" (MBR.LAST_HUG_DAY <> :LAST_HUG_DAY OR LAST_HUG_DAY IS NULL)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LAST_HUG_DAY",pCondition.LastHugDay));
		}
		
		if(!string.IsNullOrEmpty(pCondition.PartyMemberFlag)) {
			SysPrograms.SqlAppendWhere(" MBR.PARTY_MEMBER_FLAG = :PARTY_MEMBER_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTY_MEMBER_FLAG",pCondition.PartyMemberFlag));
		}

		SysPrograms.SqlAppendWhere("(P.NA_FLAG IN (:NA_FLAG,:NA_FLAG_2))",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	private void CreateWhereFriend(CastGameCharacterFellowMemberSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.SITE_CD = :SITE_CD",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.USER_SEQ = :USER_SEQ",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" MV_FRIENDLY_POINT00.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
		}
	}

	protected string CreateOrderExpresion(CastGameCharacterFellowMemberSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.CastGameCharacterFellowMemberSort.FRIENDLY_RANK_ASC:
				sSortExpression = "ORDER BY FRIENDLY_RANK, FRIENDLY_POINT DESC";
				break;
			case PwViCommConst.CastGameCharacterFellowMemberSort.GAME_CHARACTER_LEVEL_DESC:
				sSortExpression = "ORDER BY GAME_CHARACTER_LEVEL DESC, EXP DESC";
				break;
			default:
				sSortExpression = "ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}

	public DataSet GetTeamMemberSeqData(string pSiteCd,string pUserSeq,string pUserCharNo,string pFriendApplicationStatus,
										 string pType,string pPartyMemberFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ				,											");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO														");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHR_FLW_MBR01												");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	SITE_CD							= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	USER_SEQ						= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("	FRIEND_APPLICATION_STATUS		= :FRIEND_APPLICATION_STATUS			AND	");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE				= :GAME_CHARACTER_TYPE					AND	");
		oSqlBuilder.AppendLine("	PARTY_MEMBER_FLAG				= :PARTY_MEMBER_FLAG					AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG				AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG		AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG					AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":FRIEND_APPLICATION_STATUS",pFriendApplicationStatus));
		oParamList.Add(new OracleParameter(":GAME_CHARACTER_TYPE",pType));
		oParamList.Add(new OracleParameter(":PARTY_MEMBER_FLAG",pPartyMemberFlag));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;

	}

	public DataSet GetListPartyBattleMember(CastGameCharacterFellowMemberSeekCondition pCondition) {
		DataSet rsrcDS = GetPageCollection(pCondition,1,10);

		DataSet ds = MakePartyList(rsrcDS);

		return ds;
	}
	
	private DataSet MakePartyList(DataSet rsrcDS) {
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();
		DataRow dr;
		ds.Tables.Add(dt);

		dt.Columns.Add("USER_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("USER_CHAR_NO",Type.GetType("System.String"));
		dt.Columns.Add("GAME_HANDLE_NM",Type.GetType("System.String"));
		dt.Columns.Add("HANDLE_NM",Type.GetType("System.String"));
		dt.Columns.Add("AGE",Type.GetType("System.String"));
		dt.Columns.Add("GAME_CHARACTER_TYPE",Type.GetType("System.String"));
		dt.Columns.Add("GAME_CHARACTER_LEVEL",Type.GetType("System.String"));
		dt.Columns.Add("SMALL_PHOTO_IMG_PATH",Type.GetType("System.String"));
		dt.Columns.Add("FRIENDLY_RANK",Type.GetType("System.String"));
		dt.Columns.Add("ACT_CATEGORY_IDX",Type.GetType("System.String"));
		dt.Columns.Add("LOGIN_ID",Type.GetType("System.String"));
		dt.Columns.Add("RNUM",Type.GetType("System.String"));
		dt.Columns.Add("CRYPT_VALUE",Type.GetType("System.String"));
		dt.Columns.Add("MAX_ATTACK_FORCE_COUNT",Type.GetType("System.String"));

		dt.PrimaryKey = new DataColumn[] { dt.Columns["GAME_CHARACTER_TYPE"] };

		dr = dt.NewRow();
		dr = CreateBlankRow(dr,PwViCommConst.WomanGameCharacterType.TYPE01);
		dt.Rows.Add(dr);

		dr = dt.NewRow();
		dr = CreateBlankRow(dr,PwViCommConst.WomanGameCharacterType.TYPE02);
		dt.Rows.Add(dr);

		dr = dt.NewRow();
		dr = CreateBlankRow(dr,PwViCommConst.WomanGameCharacterType.TYPE03);
		dt.Rows.Add(dr);

		foreach (DataRow rsrcDR in rsrcDS.Tables[0].Rows) {
			DataRow targetDR = dt.Rows.Find(rsrcDR["GAME_CHARACTER_TYPE"]);
			targetDR["USER_SEQ"] = rsrcDR["USER_SEQ"];
			targetDR["USER_CHAR_NO"] = rsrcDR["USER_CHAR_NO"];
			targetDR["GAME_HANDLE_NM"] = rsrcDR["GAME_HANDLE_NM"];
			targetDR["HANDLE_NM"] = rsrcDR["HANDLE_NM"];
			targetDR["AGE"] = rsrcDR["AGE"];
			targetDR["GAME_CHARACTER_LEVEL"] = rsrcDR["GAME_CHARACTER_LEVEL"];
			targetDR["SMALL_PHOTO_IMG_PATH"] = rsrcDR["SMALL_PHOTO_IMG_PATH"];
			targetDR["FRIENDLY_RANK"] = rsrcDR["FRIENDLY_RANK"];
			targetDR["ACT_CATEGORY_IDX"] = rsrcDR["ACT_CATEGORY_IDX"];
			targetDR["LOGIN_ID"] = rsrcDR["LOGIN_ID"];
			targetDR["RNUM"] = rsrcDR["RNUM"];
			targetDR["CRYPT_VALUE"] = rsrcDR["CRYPT_VALUE"];
			targetDR["MAX_ATTACK_FORCE_COUNT"] = rsrcDR["MAX_ATTACK_FORCE_COUNT"];
		}

		return ds;
	}

	private DataRow CreateBlankRow(DataRow dr,string sCharType) {
		dr["USER_SEQ"] = "";
		dr["USER_CHAR_NO"] = "";
		dr["GAME_HANDLE_NM"] = "";
		dr["GAME_CHARACTER_TYPE"] = sCharType;
		dr["GAME_CHARACTER_LEVEL"] = "";
		dr["SMALL_PHOTO_IMG_PATH"] = "";
		dr["FRIENDLY_RANK"] = "";
		dr["ACT_CATEGORY_IDX"] = "";
		dr["LOGIN_ID"] = "";
		dr["RNUM"] = "";
		dr["CRYPT_VALUE"] = "";
		dr["MAX_ATTACK_FORCE_COUNT"] = "";

		return dr;
	}
}

