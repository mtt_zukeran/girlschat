﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者画像コメント
--	Progaram ID		: CastPicComment
--  Creation Date	: 2013.12.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class CastPicCommentSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string CastPicCommentSeq {
		get {
			return this.Query["commentseq"];
		}
		set {
			this.Query["commentseq"] = value;
		}
	}
	public string PicSeq {
		get {
			return this.Query["picseq"];
		}
		set {
			this.Query["picseq"] = value;
		}
	}
	public string CastPicUserSeq;
	public string CastPicUserCharNo;

	public CastPicCommentSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastPicCommentSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["commentseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["commentseq"]));
		this.Query["picseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["picseq"]));
	}
}

public class CastPicComment:DbSession {
	public CastPicComment() {
	}

	public int GetPageCount(CastPicCommentSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_PIC_COMMENT01");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastPicCommentSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_PIC_COMMENT_SEQ,");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	COMMENT_DOC,");
		oSqlBuilder.AppendLine("	COMMENT_DATE,");
		oSqlBuilder.AppendLine("	CAST_PIC_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_PIC_USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	CAST_PIC_UPLOAD_DATE,");
		oSqlBuilder.AppendLine("	CAST_PIC_TYPE,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	AGE,");
		oSqlBuilder.AppendLine("	LOGIN_ID");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_PIC_COMMENT01");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = this.CreateOrderExpresion(pCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastPicCommentSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastPicCommentSeq)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_COMMENT_SEQ = :CAST_PIC_COMMENT_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_PIC_COMMENT_SEQ",pCondition.CastPicCommentSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PicSeq)) {
			SysPrograms.SqlAppendWhere("PIC_SEQ = :PIC_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PIC_SEQ",pCondition.PicSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastPicUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_USER_SEQ = :CAST_PIC_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_PIC_USER_SEQ",pCondition.CastPicUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastPicUserCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_USER_CHAR_NO = :CAST_PIC_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_PIC_USER_CHAR_NO",pCondition.CastPicUserCharNo));
		}

		SysPrograms.SqlAppendWhere("DELETE_FLAG = :DELETE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DELETE_FLAG",ViCommConst.FLAG_OFF));

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(CastPicCommentSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.CastPicCommentSort.COMMENT_DATE_NEW:
				sSortExpression = "ORDER BY SITE_CD,PIC_SEQ,COMMENT_DATE DESC";
				break;
			case PwViCommConst.CastPicCommentSort.COMMENT_DATE_OLD:
				sSortExpression = "ORDER BY SITE_CD,PIC_SEQ,COMMENT_DATE ASC";
				break;
			default:
				sSortExpression = "ORDER BY SITE_CD,PIC_SEQ,COMMENT_DATE DESC";
				break;
		}

		return sSortExpression;
	}

	public bool IsExist(string pSiteCd,string pPicSeq,string pManUserSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_PIC_COMMENT");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	PIC_SEQ = :PIC_SEQ AND");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ = :MAN_USER_SEQ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":PIC_SEQ",pPicSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pManUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void WriteCastPicComment(
		string pSiteCd,
		string pPicSeq,
		string pManUserSeq,
		string pCommentDoc,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_CAST_PIC_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pCOMMENT_DOC",DbSession.DbType.VARCHAR2,pCommentDoc);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void DeleteCastPicComment(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		string pCastPicCommentSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_CAST_PIC_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.NUMBER,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pCAST_PIC_COMMENT_SEQ",DbSession.DbType.NUMBER,pCastPicCommentSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
