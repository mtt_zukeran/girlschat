﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 女性ｽﾃｰｼﾞｸﾞﾙｰﾌﾟ
--	Progaram ID		: StageGroup
--
--  Creation Date	: 2011.10.15
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;


[Serializable]
public class StageGroupSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}
	
	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}
	
	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}
	
	public string StageSeq {
		get {
			return this.Query["stage_seq"];
		}
		set {
			this.Query["stage_seq"] = value;
		}
	}
	
	public string RestTreasureCount {
		get {
			return this.Query["rest_treasure_count"];
		}
		set {
			this.Query["rest_treasure_count"] = value;
		}
	}
	
	public StageGroupSeekCondition()
		: this(new NameValueCollection()) {
	}
	public StageGroupSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["rest_treasure_count"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["rest_treasure_count"]));
	}
}

[System.Serializable]
public class StageGroup:DbSession {
	public StageGroup() {
	}

	public int GetPageCount(StageGroupSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP	");

		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(StageGroupSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE												,	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_NM												,	");
		oSqlBuilder.AppendLine("	NVL(PCT1.POSSESSION_COUNT,0) AS POSSESSION_COUNT				,	");
		oSqlBuilder.AppendLine("	PCT2.CAST_TREASURE_SEQ											,	");
		oSqlBuilder.AppendLine("	NVL(CT.TREASURE_COUNT,0) AS TREASURE_COUNT						,	");
		oSqlBuilder.AppendLine("	CTCL.TREASURE_COMPLETE_FLAG										,	");
		oSqlBuilder.AppendLine("	CTCL.COMPLETE_DATE													");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP SG												,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			COUNT(1) AS POSSESSION_COUNT							,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE									");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO								");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("	) PCT1															,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE										,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE T1								");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			UPDATE_DATE =												");
		oSqlBuilder.AppendLine("			(															");
		oSqlBuilder.AppendLine("				SELECT													");
		oSqlBuilder.AppendLine("					MAX(UPDATE_DATE)									");
		oSqlBuilder.AppendLine("				FROM													");
		oSqlBuilder.AppendLine("					T_POSSESSION_CAST_TREASURE T2						");
		oSqlBuilder.AppendLine("				WHERE													");
		oSqlBuilder.AppendLine("					T1.SITE_CD		= T2.SITE_CD					AND	");
		oSqlBuilder.AppendLine("					T1.USER_SEQ		= T2.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("					T1.USER_CHAR_NO	= T2.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("					T1.STAGE_GROUP_TYPE = T2.STAGE_GROUP_TYPE			");
		oSqlBuilder.AppendLine("			)															");
		oSqlBuilder.AppendLine("	) PCT2															,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE										,	");
		oSqlBuilder.AppendLine("			COUNT(1) AS TREASURE_COUNT									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE												");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD	= :SITE_CD											");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("	) CT															,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE										,	");
		oSqlBuilder.AppendLine("			1 AS TREASURE_COMPLETE_FLAG								,	");
		oSqlBuilder.AppendLine("			COMPLETE_DATE												");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE_COMPLETE_LOG								");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO								");
		oSqlBuilder.AppendLine("	) CTCL																");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= PCT1.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= PCT1.STAGE_GROUP_TYPE				(+)	AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= PCT2.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= PCT2.STAGE_GROUP_TYPE				(+)	AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= CT.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= CT.STAGE_GROUP_TYPE				(+)	AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= CTCL.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= CTCL.STAGE_GROUP_TYPE				(+)	AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	SG.SEX_CD				= :SEX_CD								AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE <=												");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			TO_NUMBER(STAGE_GROUP_TYPE)									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_STAGE														");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("			STAGE_SEQ	= :SELF_STAGE_SEQ								");
		oSqlBuilder.AppendLine("	)																	");

		sSortExpression = "ORDER BY TO_NUMBER(SG.STAGE_GROUP_TYPE) ASC";

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":SELF_STAGE_SEQ",pCondition.StageSeq));

		
		sSortExpression = this.CreateOrderExpresion();

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(StageGroupSeekCondition pCondition,ref string pWhereClause) {

		StringBuilder oSqlBuilder = new StringBuilder();
		
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere(" SEX_CD		= :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}
		
		if (!string.IsNullOrEmpty(pCondition.StageSeq)) {
			oSqlBuilder.AppendLine(" STAGE_GROUP_TYPE <=							");
			oSqlBuilder.AppendLine("	 (											");
			oSqlBuilder.AppendLine("		SELECT									");
			oSqlBuilder.AppendLine("			TO_NUMBER(STAGE_GROUP_TYPE)			");
			oSqlBuilder.AppendLine("		FROM									");
			oSqlBuilder.AppendLine("			T_STAGE								");
			oSqlBuilder.AppendLine("		WHERE									");
			oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD			AND	");
			oSqlBuilder.AppendLine("			STAGE_SEQ	= :SELF_STAGE_SEQ		");
			oSqlBuilder.AppendLine("	 )											");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_STAGE_SEQ",pCondition.StageSeq));
		}
		
		return oParamList.ToArray();
	}
	
	protected string CreateOrderExpresion() {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY TO_NUMBER(STAGE_GROUP_TYPE) ASC";
	
		return sSortExpression;
	}

	public string GetStageGroupNm(string pSiteCd,string pStageGroupType,string SexCd) {
		string sValue = null;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	STAGE_GROUP_NM											");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP											");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE		= :STAGE_GROUP_TYPE		AND		");
		oSqlBuilder.AppendLine("	SEX_CD					= :SEX_CD						");
		
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":STAGE_GROUP_TYPE",pStageGroupType);
				this.cmd.Parameters.Add(":SEX_CD",SexCd);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["STAGE_GROUP_NM"].ToString());
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public DataSet GetSoonCompCastTreasure(StageGroupSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE													,	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_NM													,	");
		oSqlBuilder.AppendLine("	CT.TREASURE_COUNT													,	");
		oSqlBuilder.AppendLine("	PCT.POSSESSION_COUNT													");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP SG													,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			SITE_CD														,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											,	");
		oSqlBuilder.AppendLine("			COUNT(*) AS TREASURE_COUNT										");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE													");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD										");
		oSqlBuilder.AppendLine("			GROUP BY SITE_CD,STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("	)CT																	,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			SITE_CD														,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											,	");
		oSqlBuilder.AppendLine("			COUNT(*) AS POSSESSION_COUNT									");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE PCT									");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ									AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("			NOT EXISTS														");
		oSqlBuilder.AppendLine("			(																");
		oSqlBuilder.AppendLine("				SELECT														");
		oSqlBuilder.AppendLine("					*														");
		oSqlBuilder.AppendLine("				FROM														");
		oSqlBuilder.AppendLine("					T_CAST_TREASURE_COMPLETE_LOG CTCL						");
		oSqlBuilder.AppendLine("				WHERE														");
		oSqlBuilder.AppendLine("					CTCL.SITE_CD			= PCT.SITE_CD				AND	");
		oSqlBuilder.AppendLine("					CTCL.USER_SEQ			= PCT.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("					CTCL.USER_CHAR_NO		= PCT.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("					CTCL.STAGE_GROUP_TYPE	= PCT.STAGE_GROUP_TYPE			");
		oSqlBuilder.AppendLine("			)																");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,STAGE_GROUP_TYPE									");
		oSqlBuilder.AppendLine("	)PCT																	");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= CT.SITE_CD								AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= CT.STAGE_GROUP_TYPE						AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= PCT.SITE_CD								AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= PCT.STAGE_GROUP_TYPE						AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	SG.SEX_CD				= :SEX_CD										");
		
		if(!string.IsNullOrEmpty(pCondition.RestTreasureCount)) {
			oSqlBuilder.AppendLine(" AND (CT.TREASURE_COUNT - PCT.POSSESSION_COUNT) <= :REST_TREASURE_COUNT");
			oParamList.Add(new OracleParameter(":REST_TREASURE_COUNT",pCondition.RestTreasureCount));
		}
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));

		string sSortExpression = "ORDER BY PCT.POSSESSION_COUNT DESC,TO_NUMBER(SG.STAGE_GROUP_TYPE) ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		return oDataSet;	
	}

	private void AppendSoonCastTreasurePossessionCount(DataSet pDS,DataSet pTmpDataSet,StageGroupSeekCondition pCondtion) {
		if (pTmpDataSet.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		int iSoonWomanTreasureAttrNum = PwViCommConst.WOMAN_TREASURE_ATTR_NUM;
		iSoonWomanTreasureAttrNum = iSoonWomanTreasureAttrNum - 1;

		col = new DataColumn(string.Format("POSSESSION_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow drTmp in pTmpDataSet.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											");
			oSqlBuilder.AppendLine("	COUNT(*) AS POSSESSION_COUNT				");
			oSqlBuilder.AppendLine(" FROM							   				");
			oSqlBuilder.AppendLine("	T_POSSESSION_CAST_TREASURE					");
			oSqlBuilder.AppendLine(" WHERE											");
			oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
			oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ			AND	");
			oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO		AND	");
			oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE		");

			using (DataSet dsp = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pCondtion.SiteCd);
					cmd.Parameters.Add(":USER_SEQ",pCondtion.UserSeq);
					cmd.Parameters.Add(":USER_CHAR_NO",pCondtion.UserCharNo);
					cmd.Parameters.Add(":STAGE_GROUP_TYPE",drTmp["STAGE_GROUP_TYPE"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsp);
					}

					if (int.Parse(dsp.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString()) <= iSoonWomanTreasureAttrNum && int.Parse(dsp.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString()) != 0 )
					{
						DataRow dr = pDS.Tables[0].Rows.Add();
						dr["SITE_CD"] = drTmp["SITE_CD"].ToString();
						dr["STAGE_GROUP_TYPE"] = drTmp["STAGE_GROUP_TYPE"].ToString();
						dr["STAGE_GROUP_NM"] = drTmp["STAGE_GROUP_NM"].ToString();
						dr["POSSESSION_COUNT"] = dsp.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString();
					
					} 
				}
			}
		}
	}

	public int GetPageCountCastTreasureStageGroupPartner(StageGroupSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	COUNT(*)													");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP SG											");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SG.SITE_CD = :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	SG.SEX_CD = :SEX_CD										AND	");
		oSqlBuilder.AppendLine("	EXISTS														");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			*													");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE PTN						");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			PTN.SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("			PTN.USER_SEQ			= :PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			PTN.USER_CHAR_NO		= :PARTNER_USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			PTN.STAGE_GROUP_TYPE	= SG.STAGE_GROUP_TYPE		");
		oSqlBuilder.AppendLine("	)														AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE <=										");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			TO_NUMBER(STAGE_GROUP_TYPE)							");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_STAGE												");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("			STAGE_SEQ	= :SELF_STAGE_SEQ						");
		oSqlBuilder.AppendLine("	)															");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":SELF_STAGE_SEQ",pCondition.StageSeq));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}
	
	public DataSet GetPageCollectionCastTreasureStageGroupPartner(StageGroupSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE												,	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_NM												,	");
		oSqlBuilder.AppendLine("	PCT1.POSSESSION_COUNT											,	");
		oSqlBuilder.AppendLine("	PCT2.CAST_TREASURE_SEQ											,	");
		oSqlBuilder.AppendLine("	CT.TREASURE_COUNT												,	");
		oSqlBuilder.AppendLine("	NVL(CTCL.TREASURE_COMPLETE_FLAG,0) AS TREASURE_COMPLETE_FLAG		");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP SG												,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			COUNT(1) AS POSSESSION_COUNT							,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE									");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :PARTNER_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :PARTNER_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("	) PCT1															,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE										,	");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ											");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE T1								");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :PARTNER_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :PARTNER_USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			UPDATE_DATE =												");
		oSqlBuilder.AppendLine("			(															");
		oSqlBuilder.AppendLine("				SELECT													");
		oSqlBuilder.AppendLine("					MAX(UPDATE_DATE)									");
		oSqlBuilder.AppendLine("				FROM													");
		oSqlBuilder.AppendLine("					T_POSSESSION_CAST_TREASURE T2						");
		oSqlBuilder.AppendLine("				WHERE													");
		oSqlBuilder.AppendLine("					T1.SITE_CD		= T2.SITE_CD					AND	");
		oSqlBuilder.AppendLine("					T1.USER_SEQ		= T2.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("					T1.USER_CHAR_NO	= T2.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("					T1.STAGE_GROUP_TYPE = T2.STAGE_GROUP_TYPE			");
		oSqlBuilder.AppendLine("			)															");
		oSqlBuilder.AppendLine("	) PCT2															,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE										,	");
		oSqlBuilder.AppendLine("			COUNT(1) AS TREASURE_COUNT									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE												");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD	= :SITE_CD											");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,STAGE_GROUP_TYPE								");
		oSqlBuilder.AppendLine("	) CT															,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			SITE_CD													,	");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE										,	");
		oSqlBuilder.AppendLine("			1 AS TREASURE_COMPLETE_FLAG									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE_COMPLETE_LOG								");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :PARTNER_USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :PARTNER_USER_CHAR_NO						");
		oSqlBuilder.AppendLine("	) CTCL																");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= PCT1.SITE_CD							AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= PCT1.STAGE_GROUP_TYPE					AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= PCT2.SITE_CD							AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= PCT2.STAGE_GROUP_TYPE					AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= CT.SITE_CD							AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= CT.STAGE_GROUP_TYPE					AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= CTCL.SITE_CD						(+)	AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE		= CTCL.STAGE_GROUP_TYPE				(+)	AND	");
		oSqlBuilder.AppendLine("	SG.SITE_CD				= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	SG.SEX_CD				= :SEX_CD								AND	");
		oSqlBuilder.AppendLine("	EXISTS																");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			*															");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_POSSESSION_CAST_TREASURE PTN								");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			PTN.SITE_CD				= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			PTN.USER_SEQ			= :PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			PTN.USER_CHAR_NO		= :PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PTN.STAGE_GROUP_TYPE	= SG.STAGE_GROUP_TYPE				");
		oSqlBuilder.AppendLine("	)																AND	");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_TYPE <=												");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			TO_NUMBER(STAGE_GROUP_TYPE)									");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			T_STAGE														");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("			STAGE_SEQ	= :SELF_STAGE_SEQ								");
		oSqlBuilder.AppendLine("	)																	");

		sSortExpression = "ORDER BY TO_NUMBER(SG.STAGE_GROUP_TYPE) ASC";

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":SELF_STAGE_SEQ",pCondition.StageSeq));

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public void GetAimStageGroupType(string pSiteCd,string pUserSeq,string pUserCharNo,out string pStageGroupType,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_AIM_STAGE_GROUP_TYPE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pSTAGE_GROUP_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pStageGroupType = db.GetStringValue("pSTAGE_GROUP_TYPE");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
