﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ
--	Progaram ID		: CastGameCharacter
--
--  Creation Date	: 2011.07.26
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using MobileLib;
using ViComm;

[Serializable]
public class CastGameCharacterSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string ManUserSeq {
		get {
			return this.Query["man_user_seq"];
		}
		set {
			this.Query["man_user_seq"] = value;
		}
	}
	
	public string ManUserCharNo {
		get {
			return this.Query["man_user_char_no"];
		}
		set {
			this.Query["man_user_char_no"] = value;
		}
	}

	public CastGameCharacterSeekCondition() : this(new NameValueCollection()) {
	}

	public CastGameCharacterSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
	}
}

[System.Serializable]
public class CastGameCharacter:CastBase {

	public CastGameCharacter()
		: base() {
	}

	public CastGameCharacter(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public DataSet GetOneByUserSeq(CastGameCharacterSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.Append("SELECT																").AppendLine();
		oSqlBuilder.AppendFormat("{0}														,	", this.CastBasicField()).AppendLine();
		oSqlBuilder.Append("	P.GAME_HANDLE_NM											,	").AppendLine();
		oSqlBuilder.Append("	P.GAME_CHARACTER_TYPE										,	").AppendLine();
		oSqlBuilder.Append("	P.GAME_CHARACTER_LEVEL										,	").AppendLine();
		oSqlBuilder.Append("	P.USER_SEQ						AS PARTNER_USER_SEQ			,	").AppendLine();
		oSqlBuilder.Append("	P.USER_CHAR_NO					AS PARTNER_USER_CHAR_NO		,	").AppendLine();
		oSqlBuilder.Append("	CASE															").AppendLine();
		oSqlBuilder.Append("		WHEN														").AppendLine();
		oSqlBuilder.Append("			P.TUTORIAL_MISSION_FLAG				= :TUTORIAL_MISSION_FLAG			OR	").AppendLine();
		oSqlBuilder.Append("			P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	OR	").AppendLine();
		oSqlBuilder.Append("			P.TUTORIAL_BATTLE_FLAG				= :TUTORIAL_BATTLE_FLAG				OR	").AppendLine();
		oSqlBuilder.Append("			P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG		").AppendLine();
		oSqlBuilder.Append("		THEN														").AppendLine();
		oSqlBuilder.Append("			1														").AppendLine();
		oSqlBuilder.Append("		ELSE														").AppendLine();
		oSqlBuilder.Append("			0														").AppendLine();
		oSqlBuilder.Append("	END AS TUTORIAL_FLAG											").AppendLine();
		oSqlBuilder.Append("FROM																").AppendLine();
		oSqlBuilder.Append("	VW_PW_CAST_GAME_CHARACTER00		P								").AppendLine();
		oSqlBuilder.Append("WHERE																").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD		= :SITE_CD		AND								").AppendLine();
		oSqlBuilder.Append("	P.USER_SEQ		= :USER_SEQ		AND								").AppendLine();
		oSqlBuilder.Append("	P.USER_CHAR_NO	= :USER_CHAR_NO	AND								").AppendLine();
		oSqlBuilder.Append("	NOT EXISTS														").AppendLine();
		oSqlBuilder.Append("	(																").AppendLine();
		oSqlBuilder.Append("		SELECT														").AppendLine();
		oSqlBuilder.Append("			*														").AppendLine();
		oSqlBuilder.Append("		FROM														").AppendLine();
		oSqlBuilder.Append("			T_REFUSE												").AppendLine();
		oSqlBuilder.Append("		WHERE														").AppendLine();
		oSqlBuilder.Append("			SITE_CD					= P.SITE_CD					AND	").AppendLine();
		oSqlBuilder.Append("			USER_SEQ				= P.USER_SEQ				AND	").AppendLine();
		oSqlBuilder.Append("			USER_CHAR_NO			= P.USER_CHAR_NO			AND	").AppendLine();
		oSqlBuilder.Append("			PARTNER_USER_SEQ		= :SELF_USER_SEQ			AND	").AppendLine();
		oSqlBuilder.Append("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO			").AppendLine();
		oSqlBuilder.Append("	)																").AppendLine();

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ", pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO", pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.ManUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.ManUserCharNo));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_ON_STR));

		string sSortExpression = " ORDER BY P.SITE_CD ";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, 0, 1, out sExecSql));

		string sWrappedSql = string.Empty;
		oParamList.AddRange(CreateOtherInfoSql(sExecSql,pCondition,out sWrappedSql));

		DataSet oDataSet = ExecuteTemplateQuery(sWrappedSql, oParamList, string.Empty, string.Empty, string.Empty);

		FakeOnlineStatus(oDataSet);
		AppendCastAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateOtherInfoSql(string pSql,CastGameCharacterSeekCondition pCondition, out string pWrappedSql) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	P.*																		,	");
		oSqlBuilder.AppendLine("	NVL(FRIENDLY_POINT.FRIENDLY_POINT,0) AS		FRIENDLY_POINT				,	");
		oSqlBuilder.AppendLine("	FRIENDLY_POINT.RANK					 AS		FRIENDLY_RANK				,	");
		oSqlBuilder.AppendLine("	PIC_COUNT.PIC_UPLOAD_COUNT			 AS		GAME_TREASURE_PIC_COUNT		,	");
		oSqlBuilder.AppendLine("	MOVIE_COUNT.MOVIE_UPLOAD_COUNT		 AS		GAME_TREASURE_MOVIE_COUNT		");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendFormat("{0}																			", pSql).AppendLine();
		oSqlBuilder.AppendLine("	)P																		,	");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			SITE_CD															,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ												,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO											,	");
		oSqlBuilder.AppendLine("			FRIENDLY_POINT													,	");
		oSqlBuilder.AppendLine("			RANK																");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00													");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00.SITE_CD			= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00.USER_SEQ		= :MAN_USER_SEQ			AND		");
		oSqlBuilder.AppendLine("			MV_FRIENDLY_POINT00.USER_CHAR_NO	= :MAN_USER_CHAR_NO				");
		oSqlBuilder.AppendLine("	) FRIENDLY_POINT													,		");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			COUNT(*) AS PIC_UPLOAD_COUNT										");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_MAN_TREASURE MT													");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD									AND		");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ									AND		");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO								AND		");
		oSqlBuilder.AppendLine("			PUBLISH_FLAG	= :PUBLISH_FLAG								AND		");
		oSqlBuilder.AppendLine("			NOT EXISTS															");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_FAVORIT													");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD						= MT.SITE_CD				AND	");
		oSqlBuilder.AppendLine("					USER_SEQ					= MT.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO				= MT.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ			= :MAN_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO		= :MAN_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("					GAME_PIC_NON_PUBLISH_DATE	< MT.UPLOAD_DATE				");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("	)PIC_COUNT															,		");
		oSqlBuilder.AppendLine("	(																			");
		oSqlBuilder.AppendLine("		SELECT																	");
		oSqlBuilder.AppendLine("			COUNT(*) AS MOVIE_UPLOAD_COUNT										");
		oSqlBuilder.AppendLine("		FROM																	");
		oSqlBuilder.AppendLine("			T_CAST_MOVIE CM														");
		oSqlBuilder.AppendLine("		WHERE																	");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD							AND		");
		oSqlBuilder.AppendLine("			USER_SEQ				= :USER_SEQ							AND		");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= :USER_CHAR_NO						AND		");
		oSqlBuilder.AppendLine("			MOVIE_TYPE				= :GAME_MOVIE_TYPE					AND		");
		oSqlBuilder.AppendLine("			OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPROVE_FLAG				AND		");
		oSqlBuilder.AppendLine("			OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG				AND		");
		oSqlBuilder.AppendLine("			NOT EXISTS															");
		oSqlBuilder.AppendLine("			(																	");
		oSqlBuilder.AppendLine("				SELECT															");
		oSqlBuilder.AppendLine("					*															");
		oSqlBuilder.AppendLine("				FROM															");
		oSqlBuilder.AppendLine("					T_FAVORIT													");
		oSqlBuilder.AppendLine("				WHERE															");
		oSqlBuilder.AppendLine("					SITE_CD					= CM.SITE_CD					AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= CM.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= CM.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ		= :MAN_USER_SEQ					AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO	= :MAN_USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("					BBS_NON_PUBLISH_FLAG	= :BBS_NON_PUBLISH_FLAG				");
		oSqlBuilder.AppendLine("			)																	");
		oSqlBuilder.AppendLine("	)MOVIE_COUNT																");
		oSqlBuilder.AppendLine("WHERE																			");
		oSqlBuilder.AppendLine("	P.SITE_CD		= FRIENDLY_POINT.SITE_CD				(+)			AND		");
		oSqlBuilder.AppendLine("	P.USER_SEQ		= FRIENDLY_POINT.PARTNER_USER_SEQ		(+)			AND		");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO	= FRIENDLY_POINT.PARTNER_USER_CHAR_NO	(+)					");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ", pCondition.ManUserSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_CHAR_NO", pCondition.ManUserCharNo));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":GAME_MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		
		pWrappedSql = oSqlBuilder.ToString();

		return oParamList.ToArray();
	}

	public DataSet GetOneOuter(CastGameCharacterSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	CC.*,										");
		oSqlBuilder.AppendLine("	EX.SITE_USE_STATUS,							");
		oSqlBuilder.AppendLine("	GC.GAME_HANDLE_NM,							");
		oSqlBuilder.AppendLine("	GC.GAME_CHARACTER_TYPE,						");
		oSqlBuilder.AppendLine("	GC.GAME_CHARACTER_LEVEL						");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	(SELECT										");
		oSqlBuilder.AppendLine("		" + CastBasicField() + "				");
		oSqlBuilder.AppendLine("	FROM										");
		oSqlBuilder.AppendLine("		VW_CAST_CHARACTER00 P					");
		oSqlBuilder.AppendLine("	WHERE										");
		oSqlBuilder.AppendLine("		P.SITE_CD		= :SITE_CD		AND		");
		oSqlBuilder.AppendLine("		P.USER_SEQ		= :USER_SEQ		AND		");
		oSqlBuilder.AppendLine("		P.USER_CHAR_NO	= :USER_CHAR_NO			");
		oSqlBuilder.AppendLine("	) CC,										");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER_EX EX,						");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER GC							");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	CC.SITE_CD		= EX.SITE_CD		AND		");
		oSqlBuilder.AppendLine("	CC.USER_SEQ		= EX.USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	CC.USER_CHAR_NO = EX.USER_CHAR_NO	AND		");
		oSqlBuilder.AppendLine("	CC.SITE_CD		= GC.SITE_CD		(+) AND	");
		oSqlBuilder.AppendLine("	CC.USER_SEQ		= GC.USER_SEQ		(+) AND	");
		oSqlBuilder.AppendLine("	CC.USER_CHAR_NO = GC.USER_CHAR_NO	(+)	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS													");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			*													");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_REFUSE											");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD					= CC.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= CC.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= CC.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :MAN_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :MAN_USER_CHAR_NO			");
		oSqlBuilder.AppendLine("	)															");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_CHAR_NO",pCondition.ManUserCharNo));

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),0,1,out sExecSql));

		string sWrappedSql = string.Empty;
		oParamList.AddRange(CreateOtherInfoSql(sExecSql,pCondition,out sWrappedSql));

		DataSet oDataSet = ExecuteTemplateQuery(sWrappedSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		AppendCastAttr(oDataSet);

		return oDataSet;
	}

	public string GetManTreasurePicCount(CastGameCharacterSeekCondition pCondtion) {
		string sValue;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_PIC_COUNT		");
		oSqlBuilder.AppendLine(" FROM							   			");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE							");
		oSqlBuilder.AppendLine(" WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND		");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pCondtion.SiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pCondtion.UserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pCondtion.UserCharNo);
				
				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["GAME_TREASURE_PIC_COUNT"].ToString());
				} else {
					sValue = "0";
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public DataSet GetMosaicRank(string pSiteCd,string pSelfUserSeq,string pSelfUserCharNo,int pRecCount) {
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "	,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM				,	");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE			,	");
		oSqlBuilder.AppendLine("	TOTAL_MOSAIC_ERASE_COUNT	,	");
		oSqlBuilder.AppendLine("	MOSAIC_RANK						");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_CAST_MOSAIC_RANK00 P		");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			*																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_FAVORIT														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			SITE_CD						= P.SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= P.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= P.USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= :SELF_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= :SELF_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			GAME_PIC_NON_PUBLISH_DATE	IS NOT NULL							");
		oSqlBuilder.AppendLine("	)														AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS													");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			*													");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_REFUSE											");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD					= P.SITE_CD				AND	");
		oSqlBuilder.AppendLine("			USER_SEQ				= P.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO			= P.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	)															");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pSelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pSelfUserCharNo));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		string sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,0,pRecCount,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);
		
		return oDataSet;
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	NA_FLAG						");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER			");
		oSqlBuilder.AppendLine(" WHERE							");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND		");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public void GetSeqForMission(string pSiteCd,string pUserSeq,string pUserCharNo,string pAllFlag,out string pPartnerUserSeq,out string pPartnerUserCharNo) {
		pPartnerUserSeq = string.Empty;
		pPartnerUserCharNo = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		SysPrograms.SqlAppendWhere("TUTORIAL_MISSION_FLAG = :TUTORIAL_MISSION_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere("TUTORIAL_MISSION_TREASURE_FLAG = :TUTORIAL_MISSION_TREASURE_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere("TUTORIAL_BATTLE_FLAG = :TUTORIAL_BATTLE_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere("TUTORIAL_BATTLE_TREASURE_FLAG = :TUTORIAL_BATTLE_TREASURE_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND PARTNER_USER_SEQ = P.USER_SEQ AND PARTNER_USER_CHAR_NO = P.USER_CHAR_NO)",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pUserCharNo));

		if (!pAllFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("LAST_LOGIN_DATE > SYSDATE-1",ref sWhereClause);
		}

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	*									");
		oSqlBuilder.AppendLine("FROM (									");
		oSqlBuilder.AppendLine("	SELECT								");
		oSqlBuilder.AppendLine("		USER_SEQ					,	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHARACTER02 P	");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	ORDER BY DBMS_RANDOM.RANDOM			");
		oSqlBuilder.AppendLine(") INNER									");
		oSqlBuilder.AppendLine("WHERE ROWNUM <= 1						");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			pPartnerUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
			pPartnerUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		}
	}

	public bool IsTelDupli(string pUserSeq,string pTel) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,USER_STATUS FROM T_USER WHERE TEL = :TEL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":TEL",pTel);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return bExist;
	}

	public void ModifyGameUserWoman(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pCastNm,
		string pTel,
		string pBirthday,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_GAME_USER_WOMAN");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_NM",DbSession.DbType.VARCHAR2,pCastNm);
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("pBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public void ModifyGameUserWomanProf(
		string pUserSeq,
		string pUserCharNo,
		string pHandleNm,
		string pCommentList,
		string pCommentDetail,
		Int64 pOkPlayList,
		string pBirthday,
		string[] pAttrTypeSeq,
		string[] pAttrSeq,
		string[] pAttrInputValue,
		int pMaxAttrCount,
		out string pResult
	) {
		SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];
		string sCommentList = "";
		string sCommentDetail = "";
		
		using (DbSession db = new DbSession()) {
			if (pCommentList != null) {
				sCommentList = Mobile.EmojiToCommTag(userObjs.carrier,pCommentList);
			}
			if (pCommentDetail != null) {
				sCommentDetail = Mobile.EmojiToCommTag(userObjs.carrier,pCommentDetail);
			}
			string[] sAttrInputValue = new string[pMaxAttrCount];

			for (int i = 0;i < pMaxAttrCount;i++) {
				if (pAttrInputValue[i] != null) {
					sAttrInputValue[i] = Mobile.EmojiToCommTag(userObjs.carrier,pAttrInputValue[i]);
				}
			}
			
			db.PrepareProcedure("MODIFY_GAME_USER_WOMAN_PROF");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2,sCommentList);
			db.ProcedureInParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2,sCommentDetail);
			db.ProcedureInParm("POK_PLAY_LIST",DbSession.DbType.NUMBER,pOkPlayList);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInArrayParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrTypeSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,pMaxAttrCount,sAttrInputValue);
			db.ProcedureInParm("PMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,pMaxAttrCount);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("PRESULT");
		}
	}
	
	public DataSet GetOneForTutorialMission(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	USER_SEQ															,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO															");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER														");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	SITE_CD							= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	SEX_CD							= :SEX_CD							AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		
		oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter("SEX_CD",ViCommConst.OPERATOR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
	
	public void ModifyGameHandleNm(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pGameHandleNm,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_GAME_HANDLE_NM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pGAME_HANDLE_NM",DbSession.DbType.VARCHAR2,pGameHandleNm);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public int GetOnlineStatusGame(string pUserSeq,string pUserCharNo) {
		DataSet ds;

		if (string.IsNullOrEmpty(pUserCharNo)) {
			pUserCharNo = ViCommConst.MAIN_CHAR_NO;
		}

		int iCharacterOnlineStatus = ViCommConst.USER_OFFLINE;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"SITE_CD,CHARACTER_ONLINE_STATUS " +
							"FROM " +
								"T_CAST_CHARACTER " +
							"WHERE " +
								"USER_SEQ =:USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_CHARACTER");
					if (ds.Tables["T_CAST_CHARACTER"].Rows.Count != 0) {
						foreach (DataRow dr in ds.Tables[0].Rows) {
							if (iCharacterOnlineStatus < int.Parse(dr["CHARACTER_ONLINE_STATUS"].ToString())) {
								iCharacterOnlineStatus = int.Parse(dr["CHARACTER_ONLINE_STATUS"].ToString());
							}
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iCharacterOnlineStatus;
	}

	public DataSet GetCastGameCharacterData(string sSiteCd,string sUserSeq,string sUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	HANDLE_NM								");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER						");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		
		return oDataSet;
	}
}

