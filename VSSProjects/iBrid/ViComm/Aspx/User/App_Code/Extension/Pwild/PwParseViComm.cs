﻿using System;
using System.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using iBridCommLib;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Collections.Specialized;
using ViComm;
using ViComm.Extension.Pwild;
using System.Configuration;
using MobileLib;

public partial class PwParseViComm:ParseViComm {
	private ParseViComm parseViComm;

	public PwParseViComm(ParseViComm pParseViComm) {
		this.parseViComm = pParseViComm;
	}

	protected virtual GameCharacter getGameCharacter() {
		return null;
	}
	
	internal virtual string Parse(string pTag,string pArgument,out bool pParsed) {
		pParsed = true;
		string sValue = string.Empty;
		Encoding encSjis = Encoding.GetEncoding(932); // shift-jis 

		switch (pTag) {
			#region ---------- ｹﾞｰﾑｷｬﾗｸﾀｰ ----------
			case "$SELF_GAME_HANDLE_NM":
				sValue = this.getGameCharacter().gameHandleNm;
				sValue = sValue + "ｻﾝ";
				break;
			case "$SELF_GAME_CHARACTER_TYPE":
				sValue = this.getGameCharacter().gameCharacterType;
				break;
			case "$SELF_GAME_POINT":
				sValue = this.getGameCharacter().gamePoint.ToString();
				break;
			case "$SELF_GAME_EXP":
				sValue = this.getGameCharacter().exp.ToString();
				break;
			case "$SELF_GAME_REST_EXP":
				sValue = this.getGameCharacter().restExp.ToString();
				break;
			case "$SELF_GAME_CHARACTER_LEVEL":
				sValue = this.getGameCharacter().gameCharacterLevel.ToString();
				break;
			case "$SELF_GAME_UNASSIGNED_FORCE_COUNT":
				sValue = this.getGameCharacter().unassignedForceCount.ToString();
				break;
			case "$SELF_GAME_TUTORIAL_STATUS":
				if (this.getGameCharacter() != null) {
					sValue = this.getGameCharacter().tutorialStatus;
				}
				break;
			case "$SELF_GAME_COOPERATION_POINT":
				sValue = this.getGameCharacter().cooperationPoint.ToString();
				break;
			case "$SELF_GAME_TOTAL_ATTACK_WIN_COUNT":
				sValue = this.getGameCharacter().totalAttackWinCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_ATTACK_LOSS_COUNT":
				sValue = this.getGameCharacter().totalAttackLossCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_DEFENCE_WIN_COUNT":
				sValue = this.getGameCharacter().totalDefenceWinCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_DEFENCE_LOSS_COUNT":
				sValue = this.getGameCharacter().totalDefenceLossCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_HELP_SUCCESS_COUNT":
				sValue = this.getGameCharacter().totalHelpSuccessCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_HELP_FAIL_COUNT":
				sValue = this.getGameCharacter().totalHelpFailCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_PARTY_ATTACK_WIN_COUNT":
				sValue = this.getGameCharacter().totalPartyAttackWinCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_PARTY_ATTACK_LOSS_COUNT":
				sValue = this.getGameCharacter().totalPartyAttackLossCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_PARTY_DEFENCE_WIN_COUNT":
				sValue = this.getGameCharacter().totalPartyDefenceWinCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_PARTY_DEFENCE_LOSS_COUNT":
				sValue = this.getGameCharacter().totalPartyDefenceLossCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_TREASURE_GET_COUNT":
				sValue = this.getGameCharacter().totalTreasureGetCount.ToString();
				break;
			case "$SELF_GAME_TOTAL_TREASURE_LOSS_COUNT":
				sValue = this.getGameCharacter().totalTreasureLossCount.ToString();
				break;
			case "$SELF_GAME_DATE_EXEC_DATE":
				sValue = this.getGameCharacter().dateExecDate;
				break;
			case "$SELF_GAME_PART_TIME_JOB_EXEC_DATE":
				sValue = this.getGameCharacter().partTimeJobExecDate;
				break;
			case "$SELF_GAME_MISSION_FORCE_COUNT":
				sValue = this.getGameCharacter().missionForceCount.ToString();
				break;
			case "$SELF_GAME_MISSION_MAX_FORCE_COUNT":
				sValue = this.getGameCharacter().missionMaxForceCount.ToString();
				break;
			case "$SELF_GAME_ATTACK_FORCE_COUNT":
				sValue = this.getGameCharacter().attackForceCount.ToString();
				break;
			case "$SELF_GAME_ATTACK_MAX_FORCE_COUNT":
				sValue = this.getGameCharacter().attackMaxForceCount.ToString();
				break;
			case "$SELF_GAME_DEFENCE_FORCE_COUNT":
				sValue = this.getGameCharacter().defenceForceCount.ToString();
				break;
			case "$SELF_GAME_DEFENCE_MAX_FORCE_COUNT":
				sValue = this.getGameCharacter().defenceMaxForceCount.ToString();
				break;
			case "$SELF_GAME_STAGE_HONOR":
				sValue = this.getGameCharacter().stageHonor.ToString();
				break;
			case "$SELF_GAME_FELLOW_COUNT_LIMIT":
				sValue = this.getGameCharacter().fellowCountLimit.ToString();
				break;
			case "$SELF_GAME_FELLOW_COUNT":
				sValue = this.getGameCharacter().fellowCount.ToString();
				break;
			case "$SELF_GAME_MISSION_NON_COMP_RECOVERY_FLAG":
				sValue = this.getGameCharacter().missionNonCompRecoveryFlag.ToString();
				break;
			case "$SELF_GAME_MISSION_FORCE_RECOVERY_DATE":
				sValue = this.GetGameMissionForceRecoveryDate(pTag,pArgument);
				break;
			case "$SELF_GAME_STAGE_SEQ":
				sValue = this.getGameCharacter().stageSeq;
				break;
			case "$SELF_GAME_STAGE_GROUP_TYPE":
				sValue = this.getGameCharacter().stageGroupType;
				break;
			#endregion

			case "$MAN_GAME_CHARCTER_TYPE_NM":
				sValue = GetManGameCharacterTypeNm(pTag, pArgument);
				break;
			case "$CAST_GAME_CHARCTER_TYPE_NM":
				sValue = GetCastGameCharacterTypeNm(pTag, pArgument);
				break;
			case "$GAME_ATTACK_POWER_EXPRESSION":
				sValue = GetAttackPower(pTag,pArgument);
				break;
			case "$GAME_ATTACK_POWER_SHOP_EXPRESSION":
				sValue = GetAttackPowerShop(pTag,pArgument);
				break;
			case "$GAME_DEFENCE_POWER_EXPRESSION":
				sValue = this.GetDefencePower(pTag,pArgument);
				break;
			case "$GAME_DEFENCE_POWER_SHOP_EXPRESSION":
				sValue = this.GetDefencePowerShop(pTag,pArgument);
				break;
			case "$CONVERT_MIN_TO_TIME":
				sValue = this.GetConvertMinToTime(pTag,pArgument);
				break;
			case "$PARM_SEEK_CONDITION":
				sValue = this.parseViComm.sessionObjs.seekConditionEx.GetQuery()[pArgument];
				break;
			case "$PARM_SEEK_CONDITION_ENCODE":
				sValue = HttpUtility.UrlEncode(iBridUtil.GetStringValue(this.parseViComm.sessionObjs.seekConditionEx.GetQuery()[pArgument]),encSjis);
				break;
			case "$PARM_SEEK_CONDITION_DECODE":
				sValue = HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.parseViComm.sessionObjs.seekConditionEx.GetQuery()[pArgument]),encSjis);
				break;
			case "$GAME_REMOVE_FELLOW_FORCE":
				sValue = this.GetRemoveFellowForce(pTag,pArgument);
				break;
			case "$GAME_REMOVE_FELLOW_PARTNER_FORCE":
				sValue = this.GetRemoveFellowPartnerForce(pTag,pArgument);
				break;
			case "$GAME_LIST_COUNT_UP":
				sValue = this.CreateListBoxCountUp(pTag,pArgument);
				break;
			case "$GAME_MISSION_FORCE_RECOVERY_MINUTE":
				sValue = PwViCommConst.GAME_MISSION_FORCE_RECOVERY_MINUTE.ToString();
				break;
			case "$GAME_HUG_BONUS_END_DATE":
				sValue = this.GetHugBonusEndDate(pTag,pArgument);
				break;
			case "$GET_GAME_TUTORIAL_STATUS":
				sValue = this.GetPartnerTutorialStatus(pTag,pArgument);
				break;
			case "$GET_GAME_ITEM_IMG":
				sValue = this.GetGameSeqImg(pTag,pArgument);
				break;
			case "$GET_GAME_ITEM_VIEW_IMG":
				sValue = this.GetGameSeqViewImg(pTag,pArgument);
				break;
			case "$GET_GAME_BOSS_IMG":
				sValue = this.GetGameSeqImg(pTag,pArgument);
				break;
			case "$GET_GAME_CAST_TREASURE_IMG":
				sValue = this.GetGameSeqImg(pTag,pArgument);
				break;
			case "$GET_GAME_CAST_TREASURE_VIEW_IMG":
				sValue = this.GetGameSeqViewImg(pTag,pArgument);
				break;
			case "$RANGE":
				sValue = "～";
				break;
			case "$GAME_BUTTON_LINK":
				sValue = GetNextButtonTag(pArgument);
				break;
			case "$GET_GAME_TOWN_INCOME_HUG_BONUS":
				sValue = GetGameTownIncomeHugBonus(pArgument);
				break;
			case "$GAME_BUTTON":
				sValue = GetButtonTag(pArgument);
				break;
			case "$GAME_PARTY_MSK":
				sValue = GetGamePartyMskStr(pTag,pArgument);
				break;
			case "$SELF_GAME_LAST_PARTY_ATTACK_DATE":
				sValue = this.getGameCharacter().lastPartyAttackDate.ToString();
				break;
			case "$SELF_GAME_NEXT_PARTY_ATTACK_DATE":
				if (this.getGameCharacter() != null) {
					sValue = this.GetNextPartyAttackDate(this.getGameCharacter().lastPartyAttackDate.ToString());
				}
				break;
			case "$GAME_AGE_CHECK_CHECKBUTTON":
				sValue = this.GetAgeCheckCheckButton(pTag,pArgument);
				break;
			case "$GAME_RE_QUERY_FOR_ANIMATION":
				sValue = this.GetGameReQueryForAnimation(pTag,pArgument);
				break;
			case "$GET_SPLIT_PIECE":
				sValue = this.GetSplitPiece(pTag,pArgument);
				break;
			case "$GAME_ADD_FELLOW_FORCE":
				sValue = this.GetAddFellowForce(pTag,pArgument);
				break;
			case "$GAME_PT_BATTLE_ATTACK_LIMIT_COUNT":
				sValue = this.GetPtBattleAttackLimitCount(pTag,pArgument);
				break;
			case "$MAN_GAME_CHARACTER_COUNT":
				sValue = this.GetManGameCharacterCount(pTag,pArgument);
				break;
			case "$CAST_GAME_CHARACTER_COUNT":
				sValue = this.GetCastGameCharacterCount(pTag,pArgument);
				break;
			case "$GET_PAGENO_OF_RNUM":
				sValue = this.GetPageNoOfRnum(pTag,pArgument);
				break;
			case "$GET_LASTDAY_IN_MONTH":
				sValue = this.GetLastdayInMonth(pTag,pArgument);
				break;
			case "$GET_NAVIGATE_ABSOLUTE_URL_ENCODE":
				sValue = this.GetNavigateAbsoluteUrlEncode(pTag,pArgument);
				break;
			case "$URL_HOST":
				sValue = HttpContext.Current.Request.Url.Host;
				break;
			case "$SITE_HOST_NM":
				sValue = parseViComm.sessionObjs.site.hostNm;
				break;
			case "$JOB_OFFER_SITE_HOST_NM":
				sValue = parseViComm.sessionObjs.site.jobOfferSiteHostNm;
				break;
			case "$GET_CODE_NM":
				sValue = this.GetCodeNm(pTag,pArgument);
				break;
			case "$EMOJI_TO_COMM_TAG":
				sValue = Mobile.EmojiToCommTag(parseViComm.sessionObjs.carrier,pArgument);
				break;
			case "$GAME_COL1":
				sValue = "#000000";
				break;
			case "$GAME_COL2":
				sValue = "#BC0D3B";
				break;
			case "$GAME_COL2_WO":
				sValue = "#3C3E7F";
				break;
			case "$GAME_COL3":
				sValue = "#948a54";
				break;
			case "$GAME_COL11":
				sValue = "#492208";
				break;
			case "$GAME_CAST_TYPE_COL_SELF":
				sValue = this.GetGameCastTypeColSelf();
				break;
			case "$GAME_CAST_TYPE_COL_SELF2":
				sValue = this.GetGameCastTypeColSelf2();
				break;
			case "$GAME_MAN_TYPE_COL_SELF":
				sValue = this.GetGameManTypeColSelf();
				break;
			case "$GAME_MAN_TYPE_COL_SELF2":
				sValue = this.GetGameManTypeColSelf2();
				break;
			case "$SELF_GAME_NM_COL":
				sValue = this.GetSelfGameNmCol();
				break;
			case "$GAME_MAN_TITLE":
				sValue = "大争奪!!Legend of Night";
				break;
			case "$GAME_WO_TITLE":
				sValue = "恋する?!ｷｬﾊﾞ嬢";
				break;
			case "$TODAY_DISPLAY_DAY":
				sValue = DateTime.Now.ToString("yyyy/MM/dd");
				break;
			case "$BROWSER_ID":
				sValue = HttpContext.Current.Request.Browser.Id;
				break;
			case "$CONVERT_DAY_OF_WEEK":
				sValue = this.ConvertDayOfWeek(pTag,pArgument);
				break;
			case "$GET_ADD_MONTH":
				sValue = this.GetAddMonth(pTag,pArgument);
				break;
			case "$CONVERT_DAY_OF_WEEK_STR":
				sValue = this.ConvertDayOfWeekStr(pTag,pArgument);
				break;
			case "$RANDOM_STR":
				sValue = GetRandomStr(pTag,pArgument);
				break;
			case "$SWITCH_STRING_BY_INDEX":
				sValue = SwitchStringByIndex(pTag,pArgument);
				break;
			default:
				pParsed = false;
				break;
		}
		return sValue;
	}

	protected void SetupGameTutorialStatus(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo,string pTutorialStatus) {
		using (GameCharacter oGameCharacter = new GameCharacter()) {
			oGameCharacter.SetupTutorialStatus(pSiteCd, pUserSeq, pUserCharNo, pTutorialStatus);
		}
	}

	private string GetManGameCharacterTypeNm(string pTag, string pArgument) {
		string sGameCharacterType = this.parseViComm.parseContainer.Parse(pArgument);
		string sResult = string.Empty;
		if (sGameCharacterType.Equals(PwViCommConst.GameCharacterType.TYPE01)) {
			sResult = "武闘";
		}
		else if (sGameCharacterType.Equals(PwViCommConst.GameCharacterType.TYPE02)) {
			sResult = "色男";
		}
		else if (sGameCharacterType.Equals(PwViCommConst.GameCharacterType.TYPE03)) {
			sResult = "知能";
		}
		return sResult;
	}

	private string GetCastGameCharacterTypeNm(string pTag, string pArgument)
	{
		string sGameCharacterType = this.parseViComm.parseContainer.Parse(pArgument);
		string sResult = string.Empty;
		if (sGameCharacterType.Equals(PwViCommConst.WomanGameCharacterType.TYPE01)) {
			sResult = "小悪魔";
		}
		else if (sGameCharacterType.Equals(PwViCommConst.WomanGameCharacterType.TYPE02)) {
			sResult = "癒し系";
		}
		else if (sGameCharacterType.Equals(PwViCommConst.WomanGameCharacterType.TYPE03)) {
			sResult = "お姉系";
		}
		return sResult;
	}
	
	private string GetAttackPower(string pTag, string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');
		
		string sSiteCd = this.parseViComm.sessionObjs.site.siteCd;
		string sUserSeq;
		string sUserCharNo;
		string sForceCount;
		
		if(arrText.Length > 2) {
			sUserSeq = arrText[0];
			sUserCharNo = arrText[1];
			sForceCount = arrText[2];
			
			sValue = GamePowerExpressionHelper.GameAttackPowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);
		}
		
		return sValue;
	}

	private string GetAttackPowerShop(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string sSiteCd = this.parseViComm.sessionObjs.site.siteCd;
		string sUserSeq;
		string sUserCharNo;
		string sForceCount;
		string sBuyItemPower;
		string sBuyQuantity;
		string sBuyItemCategory;

		if (arrText.Length > 5) {
			sUserSeq = arrText[0];
			sUserCharNo = arrText[1];
			sForceCount = arrText[2];
			sBuyItemPower = arrText[3];
			sBuyQuantity = arrText[4];
			sBuyItemCategory = arrText[5];

			sValue = GamePowerExpressionHelper.GameAttackPowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount,sBuyItemPower,sBuyQuantity,sBuyItemCategory);
		}

		return sValue;
	}

	private string GetDefencePower(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string sSiteCd = this.parseViComm.sessionObjs.site.siteCd;
		string sUserSeq;
		string sUserCharNo;
		string sForceCount;

		if (arrText.Length > 2) {
			sUserSeq = arrText[0];
			sUserCharNo = arrText[1];
			sForceCount = arrText[2];

			sValue = GamePowerExpressionHelper.GameDefencePowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);
		}

		return sValue;
	}

	private string GetDefencePowerShop(string pTag,string pArgument) {
		string sValue = "";

		string[] arrText = pArgument.Split(',');

		string sSiteCd = this.parseViComm.sessionObjs.site.siteCd;
		string sUserSeq;
		string sUserCharNo;
		string sForceCount;
		string sBuyItemPower;
		string sBuyQuantity;
		string sBuyItemCategory;

		if (arrText.Length > 5) {
			sUserSeq = arrText[0];
			sUserCharNo = arrText[1];
			sForceCount = arrText[2];
			sBuyItemPower = arrText[3];
			sBuyQuantity = arrText[4];
			sBuyItemCategory = arrText[5];

			sValue = GamePowerExpressionHelper.GameDefencePowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount,sBuyItemPower,sBuyQuantity,sBuyItemCategory);
		}

		return sValue;
	}

	private string GetConvertMinToTime(string pTag,string pArgument) {
		string[] sValues = pArgument.Split(',');
		string sFormat = sValues[1];
		int iArg = 0;
		int iHour = 0;
		int iMin = 0;
		string sMin = this.parseViComm.parseContainer.Parse(sValues[0]);

		if (int.TryParse(sMin,out iArg)) {
			iHour += (int)Math.Floor((decimal)(iArg / 60));
			iMin += iArg % 60;
			sFormat = sFormat.Replace("HH","{0:d2}");
			sFormat = sFormat.Replace("H","{0}");
			sFormat = sFormat.Replace("mm","{1:d2}");
			sFormat = sFormat.Replace("m","{1}");
			return String.Format(sFormat,iHour,iMin);
		} else {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}
	}

	protected string GetGameItemNm(string pTag,string pSiteCd,string pSexCd,string pGameItemSeq) {
		string sGameItemNm = string.Empty;
		using (GameItem oGameItem = new GameItem()) {
			sGameItemNm = oGameItem.GetGameItemNm(pSiteCd,pSexCd,pGameItemSeq);
		}
		return sGameItemNm;
	}
	
	private string GetRemoveFellowForce(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["REMOVE_FELLOW_FORCE"].ToString();
			}
		}
		return sValue;
	}

	private string GetRemoveFellowPartnerForce(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["REMOVE_FELLOW_PARTNER_FORCE"].ToString();
			}
		}
		return sValue;
	}

	protected string GetSelfHasApplicationCount(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		using (GameFellow oGameFellow = new GameFellow()) {
			iCount = oGameFellow.GetApplicationCount(pSiteCd,pUserSeq,pUserCharNo,PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION);
		}
		return iCount.ToString();
	}

	protected string GetSelfPartnerHugUncheckedCount(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo,string pLastHugLogCheckDate) {
		int iCount = 0;

		if (string.IsNullOrEmpty(pLastHugLogCheckDate)) {
			using (GameCharacter oGameCharacter = new GameCharacter()) {
				pLastHugLogCheckDate = oGameCharacter.GetLastHugLogCheckDate(pSiteCd,pUserSeq,pUserCharNo);
			}
		}
		
		using (GameCommunication oGameCommunication = new GameCommunication()) {
			iCount = oGameCommunication.GetPartnerHugUncheckedCount(pSiteCd,pUserSeq,pUserCharNo,pLastHugLogCheckDate);
		}
		return iCount.ToString();
	}
	
	protected string GetSelfPartnerPresentUncheckedCount(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo,ulong uSeekMode) {
		int iCount = 0;
		GameCharacterPresentHistorySeekCondition oCondition = new GameCharacterPresentHistorySeekCondition();
		oCondition.SiteCd = pSiteCd;
		oCondition.PartnerUserSeq = pUserSeq;
		oCondition.PartnerUserCharNo = pUserCharNo;
		oCondition.SeekMode = uSeekMode;
		using (GameCharacterPresentHistory oGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
			iCount = oGameCharacterPresentHistory.GetPartnerPresentUncheckedCount(oCondition);
		}
		return iCount.ToString();
	}

	private string CreateListBoxCountUp(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iMaxCount;
		string sPostName = string.Empty;
		string sUnit = string.Empty;
		string[] textArr = pArgument.Split(',');

		if (textArr.Length < 2) {
			return string.Empty;
		}

		iMaxCount = int.Parse(textArr[0]);
		sPostName = textArr[1];

		if (textArr.Length > 2) {
			sUnit = textArr[2];
		}

		StringBuilder oHtmlBuilder = new StringBuilder();
		oHtmlBuilder.AppendFormat("<select name=\"{0}\">",sPostName).AppendLine();

		for (int i = 1;i <= iMaxCount;i++) {
			oHtmlBuilder.AppendFormat("	<option value=\"{0}\">{0}{1}</option>",i.ToString(),sUnit).AppendLine();
		}

		oHtmlBuilder.AppendLine("</select>");

		sValue = oHtmlBuilder.ToString();

		return sValue;
	}

	private string GetGameMissionForceRecoveryDate(string pTag,string pArgument) {
		string sDate = string.Empty;

		if (this.getGameCharacter().missionNonCompRecoveryFlag.Equals(ViCommConst.FLAG_ON)) {
			int iMinute = (this.getGameCharacter().missionMaxForceCount - this.getGameCharacter().missionForceCount) * PwViCommConst.GAME_MISSION_FORCE_RECOVERY_MINUTE;
			sDate = DateTime.Now.AddMinutes(iMinute).ToString();
		}

		return sDate;
	}

	protected string GetSelfGameItemPossessionCountByCategory(string pTag,string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,string pGameItemCategoryType) {
		if (string.IsNullOrEmpty(pGameItemCategoryType)) {
			return string.Empty;
		}

		DataSet oDataSet;
		string sPossessionCount = "0";

		using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			oDataSet = oPossessionGameItem.GetOneByCategoryType(
				pSiteCd,
				pSexCd,
				pUserSeq,
				pUserCharNo,
				pGameItemCategoryType
			);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sPossessionCount = oDataSet.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString();
		}

		return sPossessionCount;
	}

	private string GetHugBonusEndDate(string pTag,string pArgument) {
		string sValue = string.Empty;
		
		if(string.IsNullOrEmpty(this.getGameCharacter().lastHugBonusGetDate)) {
			return sValue;
		}
		
		try {
			DateTime dt = DateTime.Parse(this.getGameCharacter().lastHugBonusGetDate);
		
			sValue = dt.AddMinutes(PwViCommConst.GAME_HUG_BONUS_MINUTE).ToString();
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}
		
		return sValue;
	}
	
	private string GetNextPartyAttackDate(string sLastPartyAttackDate) {
		if(string.IsNullOrEmpty(sLastPartyAttackDate)) {
			return "";
		}
		
		int iPtBattleAttackIntervalMin = 0;
		SocialGameSeekCondition pCondition = new SocialGameSeekCondition();
		pCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		using(SocialGame oSocialGame = new SocialGame()) {
			DataSet oDataSet = oSocialGame.GetOne(pCondition);
			if (oDataSet.Tables[0].Rows.Count > 0) {
				int.TryParse(oDataSet.Tables[0].Rows[0]["PT_BATTLE_ATTACK_INTERVAL_MIN"].ToString(),out iPtBattleAttackIntervalMin);
			}
		}
		DateTime oLastPartyAttackDate = DateTime.Parse(sLastPartyAttackDate);
		return oLastPartyAttackDate.AddMinutes(iPtBattleAttackIntervalMin).ToString();
	}
	
	private string GetPartnerTutorialStatus(string pTag,string pArgument) {
		if(string.IsNullOrEmpty(pArgument)) {
			return string.Empty;
		}
		
		string[] textArr = pArgument.Split(',');
		
		if(textArr.Length < 2) {
			return string.Empty;
		}
		
		GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		oCondition.PartnerUserSeq = textArr[0];
		oCondition.PartnerUserCharNo = textArr[1];
		
		DataSet oDataSet = null;
		using(GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
			oDataSet = oGameCharacterBattle.GetOneByUserSeq(oCondition);
		}
		
		if(oDataSet.Tables[0].Rows.Count == 0) {
			return string.Empty;
		}
		
		return oDataSet.Tables[0].Rows[0]["TUTORIAL_STATUS"].ToString();
	}

	private string GetGameSeqImg(string pTag,string pArgument) {
		
		string sValue;
		
		string sSeq = this.parseViComm.parseContainer.Parse(pArgument);

		sValue = iBridUtil.GetStringValue(sSeq).PadLeft(15,'0') + ".gif";

		return sValue;
	}

	private string GetGameSeqViewImg(string pTag,string pArgument) {

		string sValue;

		string sSeq = this.parseViComm.parseContainer.Parse(pArgument);

		sValue = iBridUtil.GetStringValue(sSeq).PadLeft(15,'0') + "_v.gif";

		return sValue;
	}

	private string GetNextButtonTag(string pArgument) {
		string sText = string.Empty;
		string sNextUrl = "";
		string[] sValues = pArgument.Split(',');
		sText = sValues[0];
		if (sValues.Length > 1) {
			sNextUrl = sValues[1];
		}
		return string.Format("<input value=\"{0}\" type=\"hidden\" name=\"NEXTLINK\" />",sNextUrl)
		+ string.Format("<input value=\"{0}\" type=\"submit\" name=\"{1}\" accesskey=\"5\" />",this.parseViComm.parseContainer.Parse(sText),ViCommConst.BUTTON_NEXT_LINK);
	}

	private string GetGameTownIncomeHugBonus(string pArgument) {

		string sValue;
				
		using (Town oTown = new Town()) {
			sValue = oTown.GetGameTownIncomeHugBonus(this.parseViComm.sessionObjs.site.siteCd,pArgument);
		}
		
		return sValue;
	}

	private string GetButtonTag(string pArgument) {
		string sText = string.Empty;
		string sName = ViCommConst.BUTTON_SUBMIT;
		string[] sValues = pArgument.Split(',');
		sText = sValues[0];
		if (sValues.Length > 1) {
			sName = sValues[1];
		}
		return string.Format("<input value=\"{0}\" type=\"submit\" name=\"{1}\" accesskey=\"5\" />",this.parseViComm.parseContainer.Parse(sText),sName);
	}
	
	private string GetGamePartyMskStr(string pTag,string pArgument) {
		string sValue = "0";
		
		if (string.IsNullOrEmpty(pArgument)) {
			return string.Empty;
		}

		string[] textArr = pArgument.Split(',');

		if (textArr.Length < 2) {
			return string.Empty;
		}

		GamePartyBattleMemberSeekCondition oCondition = new GamePartyBattleMemberSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		oCondition.UserSeq = textArr[0];
		oCondition.UserCharNo = textArr[1];

		using (GamePartyBattleMember oGamePartyBattleMember = new GamePartyBattleMember()) {
			sValue = oGamePartyBattleMember.GetPartyMemberMask(oCondition);
		}
		
		if(sValue.Equals("0")) {
			sValue = "8";
		}

		return sValue;
	}
	
	protected string GetSelfGameIntroLogCount(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo,string pGetFlag) {
		int iCount = 0;
		GameIntroLogSeekCondition oCondition = new GameIntroLogSeekCondition();
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.GetFlag = pGetFlag;
		using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
			iCount = oGameIntroLog.GetRecCount(oCondition);
		}
		return iCount.ToString();
	}
	
	protected string GetAgeCheckCheckButton(string pTag,string pArgument) {
		string sValue = string.Empty;

		StringBuilder oHtmlBuilder = new StringBuilder();

		oHtmlBuilder.AppendLine("<input type=\"checkbox\" name=\"checkAge\" value=\"0\" checked />はい、18歳以上です<br>");

		sValue = oHtmlBuilder.ToString();

		return sValue;
	}

	protected string GetGameReQueryForAnimation(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');
		if (oArgs.Length == 0) {
			return this.parseViComm.sessionObjs.requestQuery.Url.Query;
		}

		NameValueCollection oQeury = new NameValueCollection(this.parseViComm.sessionObjs.requestQuery.QueryString);
		
		oQeury.Remove("doc");

		List<string> items = new List<String>();
		foreach (string name in oQeury)
			items.Add(string.Concat(name,"=",oQeury[name]));
		
		return System.Web.HttpUtility.UrlEncode(string.Join("&",items.ToArray()));
	}

	protected void ExecutePresentGameItemStaffExt(string sSiteCd,string sUserSeq,string sUserCharNo,string pArgument) {
		string[] arrText = pArgument.Split(',');
		if (arrText.Length < 3) {
			return;
		}

		string sPresentId = string.Empty;
		List<string> oGameItemSeqList = new List<string>();
		List<string> oAddCountList = new List<string>();

		for (int i = 0;i < arrText.Length;i++) {
			if (i == 0) {
				sPresentId = arrText[i];
			} else if ((i % 2) == 1) {
				oGameItemSeqList.Add(arrText[i]);
			} else {
				oAddCountList.Add(arrText[i]);
			}
		}

		string[] oGameItemSeq = oGameItemSeqList.ToArray();
		string[] oAddCount = oAddCountList.ToArray();
		string sDirectFlag = ViCommConst.FLAG_ON_STR;
		string sResult;

		using (GameItem oGameItem = new GameItem()) {
			oGameItem.PresentGameItemStaffExt(sSiteCd,sUserSeq,sUserCharNo,sPresentId,oGameItemSeq,oAddCount,sDirectFlag,out sResult);
		}
	}

	protected string CheckPresentGameItemStaffExt(string sSiteCd,string sUserSeq,string sUserCharNo,string pArgument) {
		string[] arrText = pArgument.Split(',');
		if (arrText.Length < 3) {
			return PwViCommConst.PresentGameItemStaffExt.RESULT_NG;
		}

		string sPresentId = string.Empty;
		List<string> oGameItemSeqList = new List<string>();
		List<string> oAddCountList = new List<string>();

		for (int i = 0;i < arrText.Length;i++) {
			if (i == 0) {
				sPresentId = arrText[i];
			} else if ((i % 2) == 1) {
				oGameItemSeqList.Add(arrText[i]);
			} else {
				oAddCountList.Add(arrText[i]);
			}
		}

		string[] oGameItemSeq = oGameItemSeqList.ToArray();
		string[] oAddCount = oAddCountList.ToArray();
		string sDirectFlag = ViCommConst.FLAG_ON_STR;
		string sResult;

		using (GameItem oGameItem = new GameItem()) {
			oGameItem.CheckPresentGameItemStaffExt(sSiteCd,sUserSeq,sUserCharNo,sPresentId,oGameItemSeq,oAddCount,sDirectFlag,out sResult);
		}
		
		return sResult;
	}

	protected string GetSplitPiece(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');

		if (oArgs.Length < 3) {
			return string.Empty;
		}

		string sArg0 = parseViComm.ParseArg(oArgs,0,string.Empty);
		string sArg1 = parseViComm.ParseArg(oArgs,1,string.Empty);
		int iArg2;

		if (!int.TryParse(parseViComm.ParseArg(oArgs,2,string.Empty),out iArg2)) {
			return string.Empty;
		}

		string[] oPieces = sArg0.Split(new string[] { sArg1 },StringSplitOptions.None);

		if (oPieces.Length < iArg2) {
			return string.Empty;
		}

		return oPieces[(iArg2 - 1)];
	}

	protected string GetSelfFavoriteCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iRecCount = 0;

		GameFavoriteSeekCondition oCondition = new GameFavoriteSeekCondition();
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;

		using (GameFavorite oGameFavorite = new GameFavorite()) {
			iRecCount = oGameFavorite.GetRecCount(oCondition);
		}

		return iRecCount.ToString();
	}

	private string GetAddFellowForce(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["ADD_FELLOW_FORCE"].ToString();
			}
		}
		return sValue;
	}

	private string GetPtBattleAttackLimitCount(string pTag,string pArgument) {
		string sValue = string.Empty;
		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		using (SocialGame oSocialGame = new SocialGame()) {
			DataSet ds = oSocialGame.GetOne(oCondition);
			if (ds.Tables[0].Rows.Count > 0) {
				sValue = ds.Tables[0].Rows[0]["PT_BATTLE_ATTACK_LIMIT_COUNT"].ToString();
			}
		}
		return sValue;
	}

	protected string GetSelfRequestedSupportCount(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		using (SupportRequest oSupportRequest = new SupportRequest()) {
			iCount = oSupportRequest.GetRecCount(pSiteCd,pUserSeq,pUserCharNo);
		}
		return iCount.ToString();
	}

	protected string GetSelfApplicationCount(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		using (GameFellow oGameFellow = new GameFellow()) {
			iCount = oGameFellow.GetApplicationCount(pSiteCd,pUserSeq,pUserCharNo,PwViCommConst.GameCharFellowApplicationCd.APPLICATION);
		}
		return iCount.ToString();
	}

	protected string GetRequestValueNm(string pValueNo) {
		string sValueNm = string.Empty;

		switch (pValueNo) {
			case "1":
				sValueNm = "良い";
				break;
			case "2":
				sValueNm = "悪い";
				break;
			case "3":
				sValueNm = "重複";
				break;
		}

		return sValueNm;
	}

	private string GetManGameCharacterCount(string pTag,string pArgument) {
		int iCount;
		GameCharacterSeekCondition oCondition = new GameCharacterSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		oCondition.SexCd = ViCommConst.MAN;

		using (GameCharacter oGameCharacter = new GameCharacter()) {
			iCount = oGameCharacter.GetRecCount(oCondition);
		}

		return iCount.ToString();
	}

	private string GetCastGameCharacterCount(string pTag,string pArgument) {
		int iCount;
		GameCharacterSeekCondition oCondition = new GameCharacterSeekCondition();
		oCondition.SiteCd = this.parseViComm.sessionObjs.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;

		using (GameCharacter oGameCharacter = new GameCharacter()) {
			iCount = oGameCharacter.GetRecCount(oCondition);
		}

		return iCount.ToString();
	}

	/// <summary>
	/// *件目が表示されるページ番号を返す
	/// $GET_PAGENO_OF_RNUM(*件目,1ページの表示件数);
	/// </summary>
	private string GetPageNoOfRnum(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');

		if (oArgs.Length < 2) {
			return string.Empty;
		}

		double dRnum = double.Parse(parseViComm.ParseArg(oArgs,0,"0"));
		double dRowCountOfPage = double.Parse(parseViComm.ParseArg(oArgs,1,"0"));

		if (dRnum == 0 || dRowCountOfPage == 0) {
			return string.Empty;
		}

		int iPageNo = (int)Math.Ceiling(dRnum / dRowCountOfPage);

		return iPageNo.ToString();
	}

	protected string GetSelfGameFellowCountEx(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo,string pArgument) {
		
		int iValue;
		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseViComm,oArgArr,0);

		GameFellowSeekCondition oCondition = new GameFellowSeekCondition(oQuery);
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.FellowApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.FELLOW;

		using (GameFellow oGameFellow = new GameFellow()) {
			iValue = oGameFellow.GetSelfGameFellowCountEx(oCondition);
		}

		return iValue.ToString();
	}

	private string GetLastdayInMonth(string pTag,string pArgument) {
		int iYear = DateTime.Now.Year;
		int iMonth = DateTime.Now.Month;
		DateTime dt = new DateTime(iYear,iMonth,DateTime.DaysInMonth(iYear,iMonth));
		return dt.ToString();
	}
	
	private string GetNavigateAbsoluteUrlEncode(string pTag,string pArgument) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sValue = this.parseViComm.sessionObjs.GetNavigateAbsoluteUrl(pArgument);
		sValue = HttpUtility.UrlEncode(sValue,enc);
		return sValue;
	}
	
	private string GetCodeNm(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] oArgArr = pArgument.Split(',');
		NameValueCollection oQuery = ParseViComm.DesignPartsArgument.Convert2Query(this.parseViComm,oArgArr,0);
		
		string sCodeType = oQuery["codetype"].ToString();
		string sCode = oQuery["code"].ToString();
		
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			if (oCodeDtl.GetOne(sCodeType,sCode)) {
				sValue = oCodeDtl.codeNm;
			}
		}
		
		return sValue;
	}

	private string GetGameCastTypeColSelf() {
		string sValue = string.Empty;

		if (this.getGameCharacter() != null) {
			switch (this.getGameCharacter().gameCharacterType) {
				case PwViCommConst.WomanGameCharacterType.TYPE01:
					sValue = "#f7bdca";
					break;
				case PwViCommConst.WomanGameCharacterType.TYPE02:
					sValue = "#d6e697";
					break;
				case PwViCommConst.WomanGameCharacterType.TYPE03:
					sValue = "#e7bcf6";
					break;
			}
		}

		return sValue;
	}

	private string GetGameCastTypeColSelf2() {
		string sValue = string.Empty;

		if (this.getGameCharacter() != null) {
			switch (this.getGameCharacter().gameCharacterType) {
				case PwViCommConst.WomanGameCharacterType.TYPE01:
					sValue = "#fad7df";
					break;
				case PwViCommConst.WomanGameCharacterType.TYPE02:
					sValue = "#e6f0c1";
					break;
				case PwViCommConst.WomanGameCharacterType.TYPE03:
					sValue = "#f1d7fa";
					break;
			}
		}

		return sValue;
	}

	private string GetGameManTypeColSelf() {
		string sValue = string.Empty;

		if (this.getGameCharacter() != null) {
			switch (this.getGameCharacter().gameCharacterType) {
				case PwViCommConst.GameCharacterType.TYPE01:
					sValue = "#2e5f00";
					break;
				case PwViCommConst.GameCharacterType.TYPE02:
					sValue = "#00457b";
					break;
				case PwViCommConst.GameCharacterType.TYPE03:
					sValue = "#7b000c";
					break;
			}
		}

		return sValue;
	}

	private string GetGameManTypeColSelf2() {
		string sValue = string.Empty;

		if (this.getGameCharacter() != null) {
			switch (this.getGameCharacter().gameCharacterType) {
				case PwViCommConst.GameCharacterType.TYPE01:
					sValue = "#1f4000";
					break;
				case PwViCommConst.GameCharacterType.TYPE02:
					sValue = "#003056";
					break;
				case PwViCommConst.GameCharacterType.TYPE03:
					sValue = "#560008";
					break;
			}
		}

		return sValue;
	}

	private string GetSelfGameNmCol() {
		string sValue = string.Empty;

		if (this.getGameCharacter() != null) {
			switch (this.getGameCharacter().gameCharacterType) {
				case PwViCommConst.WomanGameCharacterType.TYPE01:
					sValue = "#ff009c";
					break;
				case PwViCommConst.WomanGameCharacterType.TYPE02:
					sValue = "#00cc00";
					break;
				case PwViCommConst.WomanGameCharacterType.TYPE03:
					sValue = "#7800ff";
					break;	
			}
		}

		return sValue;
	}

	private string ConvertDayOfWeek(string pTag,string pArgument) {
		DateTime dt;
		if (!DateTime.TryParse(pArgument,out dt)) {
			return string.Empty;
		}
		//月1 火2 水3 木4 金5 土6 日7という仕様 
		int iDay = int.Parse(dt.DayOfWeek.ToString("d"));
		if (iDay == 0) {
			iDay = 7;
		}
		return iDay.ToString();
	}

	private string GetAddMonth(string pTag,string pArgument) {
		string sValue = string.Empty;
		string[] sValues = pArgument.Split(',');
		
		try {
			DateTime dt;
			int iAddMonth;
			
			if (sValues.Length < 2) {
				return string.Empty;
			}
			
			if (!int.TryParse(sValues[1],out iAddMonth)) {
				return string.Empty;
			}

			string sTempConvert = this.parseViComm.parseContainer.Parse(sValues[0]);

			if (!DateTime.TryParse(sTempConvert,out dt)) {
				return string.Empty;
			}
			
			if (sValues.Length >= 3) {
				sValue = dt.AddMonths(iAddMonth).ToString(sValues[2]);
			} else {
				sValue = dt.AddMonths(iAddMonth).ToString();
			}
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}
		
		return sValue;
	}

	private string ConvertDayOfWeekStr(string pTag,string pArgument) {
		DateTime dt;
		if (!DateTime.TryParse(pArgument,out dt)) {
			return string.Empty;
		}
		
		string[] sWeek = {"日","月","火","水","木","金","土"};
		
		int iDay = int.Parse(dt.DayOfWeek.ToString("d"));
		
		return sWeek[iDay];
	}

	private string GetRandomStr(string pTag,string pArgument) {
		string sValue = string.Empty;
		int iLength = 8;

		if (!string.IsNullOrEmpty(pArgument)) {
			int.TryParse(pArgument,out iLength);
		}

		sValue = RandomHelper.GeneratePassword(iLength);

		return sValue;
	}

	private string SwitchStringByIndex(string pTag,string pArgument) {
		string sValue = string.Empty;

		if (string.IsNullOrEmpty(pArgument)) {
			return string.Empty;
		}
		
		string sArgument = this.parseViComm.parseContainer.Parse(pArgument);

		string[] sArr = sArgument.Split(',');
		int iIdx;
		
		if (sArr.Length < 3) {
			return string.Empty;
		}
		
		if (!int.TryParse(sArr[0],out iIdx)) {
			return string.Empty;
		}
		
		if (sArr.Length < iIdx + 2) {
			return string.Empty;
		}
		
		sValue = sArr[iIdx + 1];

		return sValue;
	}
}
