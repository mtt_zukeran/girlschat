﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 出演者メール添付動画ストックデータクラス
--	Progaram ID		: CastMailMovieStock
--  Creation Date	: 2015.10.14
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastMailMovieStockSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string MovieSeq {
		get {
			return this.Query["movieseq"];
		}
		set {
			this.Query["movieseq"] = value;
		}
	}

	public CastMailMovieStockSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastMailMovieStockSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["movieseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["movieseq"]));
	}
}

[Serializable]
public class CastMailMovieStock:DbSession {
	public CastMailMovieStock() {
	}

	public int GetPageCount(CastMailMovieStockSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE P	,	");
		oSqlBuilder.AppendLine("	T_USER U			");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastMailMovieStockSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	P.MOVIE_SEQ				,	");
		oSqlBuilder.AppendLine("	P.MOVIE_TITLE			,	");
		oSqlBuilder.AppendLine("	P.UPLOAD_DATE				");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE P			,	");
		oSqlBuilder.AppendLine("	T_USER U					");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY P.UPLOAD_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastMailMovieStockSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.USER_SEQ = U.USER_SEQ",ref pWhereClause);

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere("P.USER_SEQ = :USER_SEQ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));

		SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));

		if (!string.IsNullOrEmpty(pCondition.MovieSeq)) {
			SysPrograms.SqlAppendWhere("P.MOVIE_SEQ = :MOVIE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MOVIE_SEQ",pCondition.MovieSeq));
		}

		SysPrograms.SqlAppendWhere("P.MOVIE_TYPE = :MOVIE_TYPE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":MOVIE_TYPE",ViCommConst.ATTACHED_MAIL));

		SysPrograms.SqlAppendWhere("P.OBJ_NA_FLAG = :OBJ_NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere("P.OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere("P.MAIL_STOCK_FLAG = :MAIL_STOCK_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":MAIL_STOCK_FLAG",ViCommConst.FLAG_ON_STR));

		return oParamList.ToArray();
	}

	public void UpdateMailStockPic(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieSeq,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_STOCK_MOVIE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.VARCHAR2,pMovieSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
}
