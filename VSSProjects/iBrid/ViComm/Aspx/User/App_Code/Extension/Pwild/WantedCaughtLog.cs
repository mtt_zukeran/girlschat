﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: この娘を探せ 出演者発見ログ
--	Progaram ID		: WantedCaughtLog
--
--  Creation Date	: 2013.06.07
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class WantedCaughtLogSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["usercharno"];
		}
		set {
			this.Query["usercharno"] = value;
		}
	}
	
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public string ReportMonth {
		get {
			return this.Query["reportmonth"];
		}
		set {
			this.Query["reportmonth"] = value;
		}
	}

	public string SheetNo {
		get {
			return this.Query["sheetno"];
		}
		set {
			this.Query["sheetno"] = value;
		}
	}

	public WantedCaughtLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public WantedCaughtLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["usercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["usercharno"]));
		this.Query["reportmonth"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["reportmonth"]));
		this.Query["sheetno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sheetno"]));
	}
}

#endregion ============================================================================================================

public class WantedCaughtLog:CastBase {
	public WantedCaughtLog()
		: base() {
	}
	public WantedCaughtLog(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(WantedCaughtLogSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_WANTED_CAUGHT_LOG00 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(WantedCaughtLogSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "										,	");
		oSqlBuilder.AppendLine("	CASE																");
		oSqlBuilder.AppendLine("		WHEN															");
		oSqlBuilder.AppendLine("			EXISTS(														");
		oSqlBuilder.AppendLine("				SELECT													");
		oSqlBuilder.AppendLine("					1													");
		oSqlBuilder.AppendLine("				FROM													");
		oSqlBuilder.AppendLine("					T_REFUSE											");
		oSqlBuilder.AppendLine("				WHERE													");
		oSqlBuilder.AppendLine("					SITE_CD					= P.SITE_CD				AND	");
		oSqlBuilder.AppendLine("					USER_SEQ				= P.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO			= P.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ		= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("			)															");
		oSqlBuilder.AppendLine("		THEN															");
		oSqlBuilder.AppendLine("			1															");
		oSqlBuilder.AppendLine("		WHEN															");
		oSqlBuilder.AppendLine("			P.NA_FLAG != :NA_FLAG										");
		oSqlBuilder.AppendLine("		THEN															");
		oSqlBuilder.AppendLine("			1															");
		oSqlBuilder.AppendLine("		ELSE															");
		oSqlBuilder.AppendLine("			0															");
		oSqlBuilder.AppendLine("		END AS NOT_DISPLAY_FLAG											");
		oSqlBuilder.AppendLine(" FROM																	");
		oSqlBuilder.AppendLine("	VW_WANTED_CAUGHT_LOG00 P											");

		// where

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.CREATE_DATE ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(WantedCaughtLogSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.MAN_USER_SEQ		= :SELF_USER_SEQ",ref pWhereClause);
		SysPrograms.SqlAppendWhere(" P.MAN_USER_CHAR_NO	= :SELF_USER_CHAR_NO",ref pWhereClause);

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ReportMonth)) {
			SysPrograms.SqlAppendWhere(" P.REPORT_MONTH		= :REPORT_MONTH",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_MONTH",pCondition.ReportMonth));
		}

		if (!string.IsNullOrEmpty(pCondition.SheetNo)) {
			SysPrograms.SqlAppendWhere(" P.SHEET_NO		= :SHEET_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SHEET_NO",pCondition.SheetNo));
		}

		return oParamList.ToArray();
	}
}