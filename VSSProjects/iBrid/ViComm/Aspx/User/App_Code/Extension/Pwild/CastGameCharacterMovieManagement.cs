﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ動画管理
--	Progaram ID		: CastGameCharacterMovieManagement
--
--  Creation Date	: 2011.10.10
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterMovieManagementSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string CastUserMovieSeq {
		get {
			return this.Query["cast_user_movie_seq"];
		}
		set {
			this.Query["cast_user_movie_seq"] = value;
		}
	}

	public string PublishFlag {
		get {
			return this.Query["publish_flag"];
		}
		set {
			this.Query["publish_flag"] = value;
		}
	}

	public CastGameCharacterMovieManagementSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterMovieManagementSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["cast_user_movie_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_user_movie_seq"]));
		this.Query["publish_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["publish_flag"]));
	}
}

[System.Serializable]
public class CastGameCharacterMovieManagement:DbSession {

	public int GetPageCount(CastGameCharacterMovieManagementSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_MOVIE01 ");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterMovieManagementSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD						,	");
		oSqlBuilder.AppendLine("	USER_SEQ					,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine("	LOGIN_ID					,	");
		oSqlBuilder.AppendLine("	MOVIE_SEQ					,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM				,	");
		oSqlBuilder.AppendLine("	MOVIE_TITLE					,	");
		oSqlBuilder.AppendLine("	MOVIE_DOC					,	");
		oSqlBuilder.AppendLine("	UPLOAD_DATE					,	");
		oSqlBuilder.AppendLine("	READING_COUNT				,	");
		oSqlBuilder.AppendLine("	UNAUTH_MARK					,	");
		oSqlBuilder.AppendLine("	CRYPT_VALUE						");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_MOVIE01 		");
		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastGameCharacterMovieManagementSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserMovieSeq)) {
			SysPrograms.SqlAppendWhere(" MOVIE_SEQ = :MOVIE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MOVIE_SEQ",pCondition.CastUserMovieSeq));
		}

		if (pCondition.PublishFlag == ViCommConst.FLAG_ON_STR) {
			SysPrograms.SqlAppendWhere(" OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR));

			SysPrograms.SqlAppendWhere(" OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
		}else {
			SysPrograms.SqlAppendWhere(" OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_ON_STR));

			SysPrograms.SqlAppendWhere(" OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
		}
		
		SysPrograms.SqlAppendWhere(" MOVIE_TYPE = :MOVIE_TYPE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(CastGameCharacterMovieManagementSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_NEW) {
			sSortExpression = " ORDER BY UPLOAD_DATE DESC NULLS LAST";
		} else if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_READING_COUNT) {
			sSortExpression = " ORDER BY READING_COUNT DESC , USER_SEQ";
		} else {
			sSortExpression = " ORDER BY USER_SEQ";
		}
		
		return sSortExpression;
	}

	public DataSet GetCastMovie(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieSeq) {

		DataSet ds = null;
		
	    StringBuilder oSqlBuilder = new StringBuilder();
	    oSqlBuilder.AppendLine("SELECT											");
	    oSqlBuilder.AppendLine("	MOVIE_TITLE					,				");
	    oSqlBuilder.AppendLine("	MOVIE_TYPE					,				");
	    oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_SEQ	,				");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_SEQ							");
	    oSqlBuilder.AppendLine(" FROM							   				");
	    oSqlBuilder.AppendLine("	T_CAST_MOVIE								");
	    oSqlBuilder.AppendLine(" WHERE											");
	    oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND		");
	    oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND		");
	    oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND		");
	    oSqlBuilder.AppendLine("	MOVIE_SEQ		= :MOVIE_SEQ				");

	    try {
	        this.conn = this.DbConnect();

	        using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
	            this.cmd.BindByName = true;
	            this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
	            this.cmd.Parameters.Add(":USER_SEQ",pUserSeq);
	            this.cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
	            this.cmd.Parameters.Add(":MOVIE_SEQ",pMovieSeq);

				using (ds = new DataSet()) {

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(ds,"T_CAST_MOVIE");
					}
				}
	        }
	    } finally {
	        this.conn.Close();
		}

		return ds;
			
	}
}

