﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 新着女の子情報
--	Progaram ID		: CastGameCharacterToday
--
--  Creation Date	: 2011.10.31
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;

[Serializable]
public class CastGameCharacterTodaySeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	
	public string SelfUserSeq {
		get {
			return this.Query["self_user_seq"];
		}
		set {
			this.Query["self_user_seq"] = value;
		}
	}
	
	public string SelfUserCharNo {
		get {
			return this.Query["self_user_char_no"];
		}
		set {
			this.Query["self_user_char_no"] = value;
		}
	}

	public CastGameCharacterTodaySeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterTodaySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["self_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_seq"]));
		this.Query["self_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_char_no"]));
	}
}

[System.Serializable]
public class CastGameCharacterToday:DbSession {
	public CastGameCharacterToday() {
	}

	public int GetPageCount(CastGameCharacterTodaySeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHR_TODAY01	P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterTodaySeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD						,	");
		oSqlBuilder.AppendLine("	USER_SEQ					,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM				,	");
		oSqlBuilder.AppendLine("	HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	AGE							,	");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE				");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHR_TODAY01 P	");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		AppendManTreasure(oDataSet);
		
		return oDataSet;
	}

	private void AppendManTreasure(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("GAME_TREASURE_PIC_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											");
			oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_PIC_COUNT			");
			oSqlBuilder.AppendLine(" FROM							   				");
			oSqlBuilder.AppendLine("	T_MAN_TREASURE								");
			oSqlBuilder.AppendLine(" WHERE											");
			oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND			");
			oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND			");
			oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO	AND			");
			oSqlBuilder.AppendLine("	PUBLISH_FLAG	= :PUBLISH_FLAG				");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;
					cmd.Parameters.Add(":SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
					cmd.Parameters.Add(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);


					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["GAME_TREASURE_PIC_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_TREASURE_PIC_COUNT"];
				}
			}
		}
	}

	public OracleParameter[] CreateWhere(CastGameCharacterTodaySeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		DateTime dPreviousDay = DateTime.Now;
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		string sPreviousDay = DateTime.Now.ToString("yyyy/MM/dd");

		SysPrograms.SqlAppendWhere(" EXISTS (SELECT * FROM T_MAN_TREASURE MT WHERE SITE_CD = P.SITE_CD AND " +
		" USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PUBLISH_FLAG = :PUBLISH_FLAG AND DISPLAY_DAY = :DISPLAY_DAY  AND NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = MT.SITE_CD AND USER_SEQ = MT.USER_SEQ AND USER_CHAR_NO = MT.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND GAME_PIC_NON_PUBLISH_DATE < MT.UPLOAD_DATE))   ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON));
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",sPreviousDay));

		SysPrograms.SqlAppendWhere("(NA_FLAG IN (:NA_FLAG,:NA_FLAG_2))",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		return oParamList.ToArray();
	}

	public string CreateOrderExpresion(CastGameCharacterTodaySeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY DBMS_RANDOM.RANDOM";

		return sSortExpression;
	}
}
