﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: メールおねだり履歴
--	Progaram ID		: MailRequestLog
--
--  Creation Date	: 2014.12.17
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class MailRequestLogSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string CastUserSeq;
	public string CastCharNo;

	public MailRequestLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public MailRequestLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

#endregion ============================================================================================================

public class MailRequestLog:ManBase {
	public MailRequestLog()
		: base() {
	}
	public MailRequestLog(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(MailRequestLogSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_MAIL_REQUEST_LOG00 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(MailRequestLogSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "						,	");
		oSqlBuilder.AppendLine("	P.CREATE_DATE AS MAIL_REQUEST_DATE			,	");
		oSqlBuilder.AppendLine("	P.RICHINO_RANK									");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_MAIL_REQUEST_LOG00 P							");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.CREATE_DATE DESC,P.USER_SEQ ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(MailRequestLogSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_USER_SEQ		= :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere(" P.CAST_CHAR_NO		= :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND PARTNER_USER_SEQ = P.CAST_USER_SEQ AND PARTNER_USER_CHAR_NO = P.CAST_CHAR_NO)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.CAST_USER_SEQ AND USER_CHAR_NO = P.CAST_CHAR_NO AND PARTNER_USER_SEQ = P.USER_SEQ)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" P.NOT_DISPLAY_FLAG		= :NOT_DISPLAY_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NOT_DISPLAY_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	public void RegistMailRequestLog(string pSiteCd,string pUserSeq,string pCastUserSeq,string pCastCharNo,int pCheckOnlyFlag,out int pRequestLimitCount,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_MAIL_REQUEST_LOG");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pCHECK_ONLY_FLAG",DbSession.DbType.NUMBER,pCheckOnlyFlag);
			db.ProcedureOutParm("pREQUEST_LIMIT_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			pRequestLimitCount = db.GetIntValue("pREQUEST_LIMIT_COUNT");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void UpdateMailRequestLog(string pSiteCd,string pUserSeq,string pCastUserSeq,string pCastCharNo,string pCastTxMailSeq) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_REQUEST_LOG");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pCAST_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pCastTxMailSeq);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateMailRequestLogMan(string pSiteCd,string pUserSeq,string pCastUserSeq,string pCastCharNo,string pManTxMailSeq) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_REQUEST_LOG_MAN");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pMAN_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pManTxMailSeq);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public DataSet GetReservedMailRequestCount(MailRequestLogSeekCondition pCondtion) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT											");
		oSqlBuilder.AppendLine("	COUNT(*) AS RESERVED_MAIL_REQUEST_COUNT		");
		oSqlBuilder.AppendLine(" FROM											");
		oSqlBuilder.AppendLine("	VW_MAIL_REQUEST_LOG00 P						");
		// where
		string sWhereClause = string.Empty;

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public string GetRestMailRequestCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT																							");
		oSqlBuilder.AppendLine("	NVL(RR.MAIL_REQUEST_LIMIT_COUNT,1) - NVL(TC.TODAY_COUNT,0) AS REST_MAIL_REQUEST_COUNT		");
		oSqlBuilder.AppendLine(" FROM																							");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			COUNT(*) AS TODAY_COUNT																");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_MAIL_REQUEST_LOG																	");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ														AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY		= TO_CHAR(SYSDATE,'YYYY/MM/DD')										");
		oSqlBuilder.AppendLine("	) TC																					,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER_EX U																,	");
		oSqlBuilder.AppendLine("	T_RICHINO_RANK RR																			");
		oSqlBuilder.AppendLine(" WHERE																							");
		oSqlBuilder.AppendLine("	U.SITE_CD		= RR.SITE_CD														(+)	AND	");
		oSqlBuilder.AppendLine("	U.RICHINO_RANK	= RR.RICHINO_RANK													(+)	AND	");
		oSqlBuilder.AppendLine("	U.SITE_CD		= :SITE_CD																AND	");
		oSqlBuilder.AppendLine("	U.USER_SEQ		= :USER_SEQ																AND	");
		oSqlBuilder.AppendLine("	U.USER_CHAR_NO	= :USER_CHAR_NO																");
		// where
		string sWhereClause = string.Empty;
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REST_MAIL_REQUEST_COUNT"]);
	}

	public void MailRequestConfirm(
		string pSiteCd,
		string pUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		out string pRestMailRequestCount,
		out string pMailRequestLimitCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAIL_REQUEST_CONFIRM");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("pREST_MAIL_REQUEST_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREQUEST_LIMIT_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pRestMailRequestCount = db.GetStringValue("pREST_MAIL_REQUEST_COUNT");
			pMailRequestLimitCount = db.GetStringValue("pREQUEST_LIMIT_COUNT");
			pResult = db.GetStringValue("pRESULT");
		}
	}
}