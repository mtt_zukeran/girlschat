﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者画像イイネ
--	Progaram ID		: CastPicLike
--  Creation Date	: 2013.12.23
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class CastPicLikeSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string PicSeq {
		get {
			return this.Query["picseq"];
		}
		set {
			this.Query["picseq"] = value;
		}
	}
	public string CastPicUserSeq;
	public string CastPicUserCharNo;

	public CastPicLikeSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastPicLikeSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["picseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["picseq"]));
	}
}

public class CastPicLike:DbSession {
	public CastPicLike() {
	}

	public int GetPageCount(CastPicLikeSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_PIC_LIKE01");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastPicLikeSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	LIKE_DATE,");
		oSqlBuilder.AppendLine("	CAST_PIC_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_PIC_USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	AGE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_PIC_LIKE01");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY SITE_CD,PIC_SEQ,LIKE_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public string IsExist(string pSiteCd,string pPicSeq,string pManUserSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_PIC_LIKE");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	PIC_SEQ = :PIC_SEQ AND");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ = :MAN_USER_SEQ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":PIC_SEQ",pPicSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pManUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}

	private OracleParameter[] CreateWhere(CastPicLikeSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.PicSeq)) {
			SysPrograms.SqlAppendWhere("PIC_SEQ = :PIC_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PIC_SEQ",pCondition.PicSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastPicUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_USER_SEQ = :CAST_PIC_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_PIC_USER_SEQ",pCondition.CastPicUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastPicUserCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_USER_CHAR_NO = :CAST_PIC_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_PIC_USER_CHAR_NO",pCondition.CastPicUserCharNo));
		}

		return oParamList.ToArray();
	}

	public void RegistCastPicLike(
		string pSiteCd,
		string pPicSeq,
		string pManUserSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_CAST_PIC_LIKE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	/// <summary>
	/// いいね解除
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pPicSeq"></param>
	/// <param name="pManUserSeq"></param>
	/// <param name="pResult"></param>
	public void DeregistCastPicLike(
		string pSiteCd,
		string pPicSeq,
		string pManUserSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DEREGIST_CAST_PIC_LIKE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
