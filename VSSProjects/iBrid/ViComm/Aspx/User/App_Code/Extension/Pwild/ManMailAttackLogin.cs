﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: メールアタックイベント(一定期間ログインしていない男性)
--	Progaram ID		: ManMailAttackLogin
--  Creation Date	: 2016.05.03
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManMailAttackLoginSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SelfUserSeq;
	public string SelfCharNo;
	public string MailAttackLoginSeq;
	public string NotLoginMaxDays;
	public string NotLoginMinDays;

	public ManMailAttackLoginSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManMailAttackLoginSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[Serializable]
public class ManMailAttackLogin:ManBase {
	public ManMailAttackLogin() {
	}

	public int GetPageCount(ManMailAttackLoginSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P		,	");
		oSqlBuilder.AppendLine("	T_MAIL_ATTACK_LOGIN_RX_CNT R		");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManMailAttackLoginSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		sSortExpression = this.CreateOrderExpression(pCondition);

		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	T1.*																							,	");
		oSqlBuilder.AppendLine("	GET_MAN_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0) AS PHOTO_IMG_PATH					,	");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("	NVL(C.TX_MAIL_COUNT,0) AS TX_MAIL_COUNT																");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			*																							");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					INNER.*																			,	");
		oSqlBuilder.AppendLine("					ROWNUM AS RNUM																	,	");
		oSqlBuilder.AppendLine("					ROWNUM AS RANK																		");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					(																					");
		oSqlBuilder.AppendLine("						SELECT																			");
		oSqlBuilder.AppendLine("							P.*																			");
		oSqlBuilder.AppendLine("						FROM																			");
		oSqlBuilder.AppendLine("							(																			");
		oSqlBuilder.AppendLine("								SELECT																	");
		oSqlBuilder.AppendLine("									P.SITE_CD														,	");
		oSqlBuilder.AppendLine("									P.USER_SEQ														,	");
		oSqlBuilder.AppendLine("									P.USER_CHAR_NO													,	");
		oSqlBuilder.AppendLine("									P.HANDLE_NM														,	");
		oSqlBuilder.AppendLine("									P.AGE															,	");
		oSqlBuilder.AppendLine("									P.LOGIN_ID														,	");
		oSqlBuilder.AppendLine("									P.CHARACTER_ONLINE_STATUS										,	");
		oSqlBuilder.AppendLine("									P.USER_STATUS													,	");
		oSqlBuilder.AppendLine("									P.PIC_SEQ														,	");
		oSqlBuilder.AppendLine("									P.LAST_LOGIN_DATE												,	");
		oSqlBuilder.AppendLine("									P.TOTAL_RECEIPT_COUNT											,	");
		oSqlBuilder.AppendLine("									P.REC_SEQ														,	");
		oSqlBuilder.AppendLine("									NVL(R.RX_MAIL_COUNT,0) AS RX_MAIL_COUNT								");
		oSqlBuilder.AppendLine("								FROM																	");
		oSqlBuilder.AppendLine("									VW_USER_MAN_CHARACTER00 P										,	");
		oSqlBuilder.AppendLine("									T_MAIL_ATTACK_LOGIN_RX_CNT R										");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("							) P																			");
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("					) INNER																				");
		oSqlBuilder.AppendLine("				WHERE																					");
		oSqlBuilder.AppendLine("					ROWNUM <= :LAST_ROW																	");
		oSqlBuilder.AppendLine("			)																							");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			RANK > :FIRST_ROW																			");
		oSqlBuilder.AppendLine("	) T1																							,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			SITE_CD																					,	");
		oSqlBuilder.AppendLine("			RX_USER_SEQ																				,	");
		oSqlBuilder.AppendLine("			RX_CHAR_NO																				,	");
		oSqlBuilder.AppendLine("			COUNT(*) AS TX_MAIL_COUNT																	");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_MAIL_ATTACK_LOGIN_TX_LOG																	");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= :LOG_SITE_CD													AND	");
		oSqlBuilder.AppendLine("			MAIL_ATTACK_LOGIN_SEQ	= :LOG_MAIL_ATTACK_LOGIN_SEQ									AND	");
		oSqlBuilder.AppendLine("			TX_USER_SEQ				= :LOG_SELF_USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			TX_CHAR_NO				= :LOG_SELF_CHAR_NO												AND	");
		oSqlBuilder.AppendLine("			CREATE_DATE				>= SYSDATE - 1														");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,RX_USER_SEQ,RX_CHAR_NO															");
		oSqlBuilder.AppendLine("	) C																									");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("T1.SITE_CD				= C.SITE_CD																(+)	AND	");
		oSqlBuilder.AppendLine("	T1.USER_SEQ			= C.RX_USER_SEQ															(+)	AND	");
		oSqlBuilder.AppendLine("	T1.USER_CHAR_NO		= C.RX_CHAR_NO															(+)		");
		oSqlBuilder.AppendLine("ORDER BY T1.RNUM																						");

		oParamList.Add(new OracleParameter(":LOG_SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":LOG_MAIL_ATTACK_LOGIN_SEQ",pCondition.MailAttackLoginSeq));
		oParamList.Add(new OracleParameter(":LOG_SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":LOG_SELF_CHAR_NO",pCondition.SelfCharNo));
		oParamList.Add(new OracleParameter(":LAST_ROW",iStartIndex + pRecPerPage));
		oParamList.Add(new OracleParameter(":FIRST_ROW",iStartIndex));
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		AppendManAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManMailAttackLoginSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.SITE_CD = R.SITE_CD (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere(":MAIL_ATTACK_LOGIN_SEQ = R.MAIL_ATTACK_LOGIN_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_SEQ = R.USER_SEQ (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = R.USER_CHAR_NO (+)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":MAIL_ATTACK_LOGIN_SEQ",pCondition.MailAttackLoginSeq));
		
		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere("P.LAST_LOGIN_DATE >= SYSDATE - :NOT_LOGIN_MAX_DAYS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NOT_LOGIN_MAX_DAYS",pCondition.NotLoginMaxDays));
		SysPrograms.SqlAppendWhere("P.LAST_LOGIN_DATE <= SYSDATE - :NOT_LOGIN_MIN_DAYS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NOT_LOGIN_MIN_DAYS",pCondition.NotLoginMinDays));

		SysPrograms.SqlAppendWhere("P.USER_RANK = :USER_RANK",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_RANK",ViCommConst.USER_RANK_B));
		SysPrograms.SqlAppendWhere("P.BAL_POINT <= 7",ref pWhereClause);

		SysPrograms.SqlAppendWhere("P.CAST_MAIL_RX_TYPE = :CAST_MAIL_RX_TYPE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":CAST_MAIL_RX_TYPE",ViCommConst.MAIL_RX_BOTH));

		SysPrograms.SqlAppendWhere("P.NON_EXIST_MAIL_ADDR_FLAG = :NON_EXIST_MAIL_ADDR_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NON_EXIST_MAIL_ADDR_FLAG",ViCommConst.FLAG_OFF));

		SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :REFUSE_CAST_USER_SEQ AND PARTNER_USER_CHAR_NO = :REFUSE_CAST_CHAR_NO)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":REFUSE_CAST_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":REFUSE_CAST_CHAR_NO",pCondition.SelfCharNo));

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	private string CreateOrderExpression(ManMailAttackLoginSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case (PwViCommConst.MailAttackLoginSort.RX_MAIL_COUNT_ASC): {
					sSortExpression = "ORDER BY P.RX_MAIL_COUNT ASC,P.USER_SEQ ASC";
					break;
				}
			case (PwViCommConst.MailAttackLoginSort.TOTAL_RECEIPT_COUNT_DESC): {
					sSortExpression = "ORDER BY P.TOTAL_RECEIPT_COUNT DESC,P.USER_SEQ ASC";
					break;
				}
			case (PwViCommConst.MailAttackLoginSort.LAST_LOGIN_DATE_DESC): {
					sSortExpression = "ORDER BY P.LAST_LOGIN_DATE DESC,P.USER_SEQ ASC";
					break;
				}
			case (PwViCommConst.MailAttackLoginSort.RANDOM): {
					sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";
					break;
				}
			default:
				sSortExpression = "ORDER BY P.RX_MAIL_COUNT ASC,P.USER_SEQ ASC";
				break;
		}
		return sSortExpression;
	}
}
