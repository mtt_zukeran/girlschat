﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ仲間

--	Progaram ID		: CastGameCharacterFellow
--
--  Creation Date	: 2011.08.08
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterFellowSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string FriendApplicationStatus {
		get {
			return this.Query["fellow_application_status"];
		}
		set {
			this.Query["fellow_application_status"] = value;
		}
	}

	public string SiteUseStatus;

	public CastGameCharacterFellowSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterFellowSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["item_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["item_seq"]));
	}
}

[System.Serializable]
public class CastGameCharacterFellow:CastBase {
	public CastGameCharacterFellow()
		: base() {
	}

	public CastGameCharacterFellow(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastGameCharacterFellowSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHAR_FELLOW00 P	");

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterFellowSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sWhereClauseFriend = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause,ref sWhereClauseFriend));

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.*,								");
		oSqlBuilder.AppendLine("	FP.RANK AS FRIENDLY_RANK,			");
		oSqlBuilder.AppendLine(" 	FP.FRIENDLY_POINT					");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	(SELECT								");
		oSqlBuilder.AppendLine("		" + CastBasicField() + "	,   ");
		oSqlBuilder.AppendLine("		P.GAME_HANDLE_NM				,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_TYPE			,	");
		oSqlBuilder.AppendLine("		P.GAME_CHARACTER_LEVEL			,	");
		oSqlBuilder.AppendLine("		P.LAST_HUG_DAY					,	");
		oSqlBuilder.AppendLine("		P.APPLICATION_DATE				,	");
		oSqlBuilder.AppendLine("		P.APPLICATION_PERMIT_DATE		,	");
		oSqlBuilder.AppendLine("		P.SELF_USER_SEQ					,	");
		oSqlBuilder.AppendLine("		P.SELF_USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine("		CASE												");
		oSqlBuilder.AppendLine("			WHEN											");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE IS NOT NULL AND	");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE < SYSDATE		");
		oSqlBuilder.AppendLine("			THEN											");
		oSqlBuilder.AppendLine("				F.OFFLINE_LAST_UPDATE_DATE					");
		oSqlBuilder.AppendLine("			ELSE											");
		oSqlBuilder.AppendLine("				P.LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("		END VIEW_LAST_LOGIN_DATE							");
		oSqlBuilder.AppendLine("	FROM													");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHAR_FELLOW00 P					,	");
		oSqlBuilder.AppendLine("		(																");
		oSqlBuilder.AppendLine("			SELECT														");
		oSqlBuilder.AppendLine("				SITE_CD												,	");
		oSqlBuilder.AppendLine("				USER_SEQ											,	");
		oSqlBuilder.AppendLine("				USER_CHAR_NO										,	");
		oSqlBuilder.AppendLine("				OFFLINE_LAST_UPDATE_DATE								");
		oSqlBuilder.AppendLine("			FROM														");
		oSqlBuilder.AppendLine("				T_FAVORIT												");
		oSqlBuilder.AppendLine("			WHERE														");
		oSqlBuilder.AppendLine("				SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_SEQ		= :SELF_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("				PARTNER_USER_CHAR_NO	= :SELF_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("				WAITING_VIEW_STATUS		= :WAITING_VIEW_STATUS		AND	");
		oSqlBuilder.AppendLine("				LOGIN_VIEW_STATUS		= :LOGIN_VIEW_STATUS			");
		oSqlBuilder.AppendLine("		) F																");
		SysPrograms.SqlAppendWhere("P.SITE_CD		= F.SITE_CD			(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_SEQ		= F.USER_SEQ		(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= F.USER_CHAR_NO	(+)",ref sWhereClause);
		oParamList.Add(new OracleParameter(":WAITING_VIEW_STATUS","1"));
		oParamList.Add(new OracleParameter(":LOGIN_VIEW_STATUS","1"));
		
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" 	) P,								");
		oSqlBuilder.AppendLine(" 	(SELECT								");
		oSqlBuilder.AppendLine(" 		SITE_CD						,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine(" 		PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine(" 		RANK						,	");
		oSqlBuilder.AppendLine(" 		FRIENDLY_POINT					");
		oSqlBuilder.AppendLine(" 	FROM								");
		oSqlBuilder.AppendLine(" 		MV_FRIENDLY_POINT00				");
		oSqlBuilder.AppendLine(sWhereClauseFriend);
		oSqlBuilder.AppendLine(" 	) FP								");
		oSqlBuilder.AppendLine(" WHERE									");
		oSqlBuilder.AppendLine(" 	P.SITE_CD		= FP.SITE_CD				(+) AND	");
		oSqlBuilder.AppendLine(" 	P.USER_SEQ		= FP.PARTNER_USER_SEQ		(+) AND	");
		oSqlBuilder.AppendLine(" 	P.USER_CHAR_NO	= FP.PARTNER_USER_CHAR_NO	(+)		");

		string sSortExpression = this.CreateOrderExpresion(pCondtion);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		this.AppendCastAttr(oDataSet);

		return oDataSet;
	}

	public DataSet GetOneFellowApplication(CastGameCharacterFellowSeekCondition pCondtion) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.*									");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	(SELECT								");
		oSqlBuilder.AppendLine("		GAME_HANDLE_NM				,	");
		oSqlBuilder.AppendLine("		HANDLE_NM					,	");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_TYPE			,	");
		oSqlBuilder.AppendLine("		GAME_CHARACTER_LEVEL		,	");
		oSqlBuilder.AppendLine("		APPLICATION_DATE			,	");
		oSqlBuilder.AppendLine("		APPLICATION_PERMIT_DATE		,	");
		oSqlBuilder.AppendLine("		USER_SEQ					,	");
		oSqlBuilder.AppendLine("		USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	FROM								");
		oSqlBuilder.AppendLine("		VW_PW_CAST_GAME_CHAR_FELLOW00 P	");
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" 	) P									");
		oSqlBuilder.AppendLine("	WHERE ROWNUM <= 1	  				");


		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}


	private OracleParameter[] CreateWhere(CastGameCharacterFellowSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("P.SELF_USER_SEQ = :SELF_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("P.SELF_USER_CHAR_NO = :SELF_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.FriendApplicationStatus)) {
			SysPrograms.SqlAppendWhere("P.FELLOW_APPLICATION_STATUS = :FELLOW_APPLICATION_STATUS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pCondition.FriendApplicationStatus));
		}
		
		SysPrograms.SqlAppendWhere("P.SITE_USE_STATUS = :SITE_USE_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_USE_STATUS",ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME));

		SysPrograms.SqlAppendWhere("P.NA_FLAG IN (:NA_FLAG1,:NA_FLAG2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG1",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		
		return oParamList.ToArray();
	}

	private OracleParameter[] CreateWhere(CastGameCharacterFellowSeekCondition pCondition,ref string pWhereClause,ref string pWhereClauseFriend) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref pWhereClause));

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :SELF_USER_SEQ",ref pWhereClauseFriend);
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :SELF_USER_CHAR_NO",ref pWhereClauseFriend);
		}
		
		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(CastGameCharacterFellowSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.CastGameCharacterFellowSort.APPLICATION_DATE_DESC:
				sSortExpression = "ORDER BY APPLICATION_DATE DESC";
				break;
			case PwViCommConst.CastGameCharacterFellowSort.APPLICATION_PERMIT_DATE_DESC:
				sSortExpression = "ORDER BY APPLICATION_PERMIT_DATE DESC";
				break;
			case PwViCommConst.CastGameCharacterFellowSort.FRIENDLY_RANK_ASC:
				sSortExpression = "ORDER BY FRIENDLY_RANK, FRIENDLY_POINT DESC";
				break;
			case PwViCommConst.CastGameCharacterFellowSort.LAST_LOGIN_DATE_DESC:
				sSortExpression = "ORDER BY VIEW_LAST_LOGIN_DATE DESC NULLS LAST";
				break;
			default:
				sSortExpression = "ORDER BY P.SITE_CD";
				break;
		}

		return sSortExpression;
	}
}