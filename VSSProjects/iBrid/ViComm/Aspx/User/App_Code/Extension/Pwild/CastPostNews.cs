﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 投稿ニュース
--	Progaram ID		: CastPostNews
--
--  Creation Date	: 2014.12.24
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class CastPostNewsSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public CastPostNewsSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastPostNewsSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class CastPostNews:DbSession {
	public CastPostNews() {
	}

	public DataSet GetPageCollection(CastPostNewsSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	NEWS_TYPE,");
		oSqlBuilder.AppendLine("	NEWS_DATE,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	BLOG_SEQ,");
		oSqlBuilder.AppendLine("	NA_FLAG,");
		oSqlBuilder.AppendLine("	ACT_CATEGORY_IDX,");
		oSqlBuilder.AppendLine("	CRYPT_VALUE");
		oSqlBuilder.AppendLine("FROM (");
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.SITE_CD,");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.USER_SEQ,");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.NEWS_TYPE,");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.NEWS_DATE,");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.HANDLE_NM,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.BLOG_SEQ,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.NA_FLAG,");
		oSqlBuilder.AppendLine("	T_ACT_CATEGORY.PRIORITY AS ACT_CATEGORY_IDX,");
		oSqlBuilder.AppendLine("	NVL(T_EX_CHAR_NO.CRYPT_VALUE,T_CAST_NEWS.USER_CHAR_NO) AS CRYPT_VALUE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_NEWS,");
		oSqlBuilder.AppendLine("	T_USER,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER,");
		oSqlBuilder.AppendLine("	T_ACT_CATEGORY,");
		oSqlBuilder.AppendLine("	T_EX_CHAR_NO");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.USER_SEQ = T_USER.USER_SEQ AND");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.SITE_CD = T_CAST_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.USER_SEQ = T_CAST_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.SITE_CD = T_ACT_CATEGORY.SITE_CD AND");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.ACT_CATEGORY_SEQ = T_ACT_CATEGORY.ACT_CATEGORY_SEQ AND");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.SITE_CD = T_EX_CHAR_NO.SITE_CD AND");
		oSqlBuilder.AppendLine("	T_CAST_NEWS.USER_CHAR_NO = T_EX_CHAR_NO.USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	1 = T_EX_CHAR_NO.LATEST_FLAG");
		oSqlBuilder.AppendLine(") P");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY P.NEWS_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastPostNewsSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		List<string> NewsTypeList = new List<string>();
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_BBS_PIC);
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_BBS_MOV);
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_FREE_PIC);
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_FREE_MOV);
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_BBS);
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_BLOG);
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_YAKYUKEN);
		NewsTypeList.Add(PwViCommConst.CastNewsType.NEWS_TYPE_DIARY);
		SysPrograms.SqlAppendWhere(string.Format("P.NEWS_TYPE IN({0})",string.Join(",",NewsTypeList.ToArray())),ref pWhereClause);

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}
}
