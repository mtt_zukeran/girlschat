﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: プレゼントメールアイテム
--	Progaram ID		: PresentMailItem
--
--  Creation Date	: 2014.12.02
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class PresentMailItemSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string PublishFlag;

	public PresentMailItemSeekCondition()
		: this(new NameValueCollection()) {
	}

	public PresentMailItemSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class PresentMailItem:DbSession {
	public PresentMailItem() {
	}

	public int GetPageCount(PresentMailItemSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_PRESENT_MAIL_ITEM01 P	");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(PresentMailItemSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		string sExecSql = string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	P.PRESENT_MAIL_TERM_SEQ		,	");
		oSqlBuilder.AppendLine("	P.PRESENT_MAIL_ITEM_SEQ		,	");
		oSqlBuilder.AppendLine("	P.PRESENT_MAIL_ITEM_NM		,	");
		oSqlBuilder.AppendLine("	P.CHARGE_POINT				,	");
		oSqlBuilder.AppendLine("	P.REWARD_POINT					");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PRESENT_MAIL_ITEM01 P		");
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpression(pCondition);
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(PresentMailItemSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}
		
		if (iBridUtil.GetStringValue(pCondition.PublishFlag).Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("SYSDATE BETWEEN P.TERM_START_DATE AND P.TERM_END_DATE",ref pWhereClause);
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpression(PresentMailItemSeekCondition pCondition) {
		string sSortExpression;
		
		switch (pCondition.Sort) {
			case PwViCommConst.PresentMailItemSort.CHARGE_POINT_DESC:
				sSortExpression = " ORDER BY P.CHARGE_POINT DESC,P.PRESENT_MAIL_ITEM_SEQ ASC";
				break;
			case PwViCommConst.PresentMailItemSort.CHARGE_POINT_ASC:
				sSortExpression = " ORDER BY P.CHARGE_POINT ASC,P.PRESENT_MAIL_ITEM_SEQ ASC";
				break;
			default:
				sSortExpression = " ORDER BY P.CHARGE_POINT DESC,P.PRESENT_MAIL_ITEM_SEQ ASC";
				break;
		}

		return sSortExpression;
	}
	
	public int GetChargePoint(string pSiteCd,string pPresentMailItemSeq) {
		int iValue = 0;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	CHARGE_POINT												");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_PRESENT_MAIL_ITEM											");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	PRESENT_MAIL_ITEM_SEQ	= :PRESENT_MAIL_ITEM_SEQ			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":PRESENT_MAIL_ITEM_SEQ",pPresentMailItemSeq));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			if (!int.TryParse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CHARGE_POINT"]),out iValue)) {
				iValue = 0;
			}
		}
		
		return iValue;
	}
	
	public DataSet GetCurrnetPresentMailItemList (string pSiteCd,string pSort) {
		DataSet oDataSet = null;
		PresentMailItemSeekCondition oPresentMailItemSeekCondition = new PresentMailItemSeekCondition();
		oPresentMailItemSeekCondition.Sort = pSort;
		string sSortExpression = CreateOrderExpression(oPresentMailItemSeekCondition);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	P.PRESENT_MAIL_ITEM_SEQ									,	");
		oSqlBuilder.AppendLine("	P.PRESENT_MAIL_ITEM_NM									,	");
		oSqlBuilder.AppendLine("	P.CHARGE_POINT												");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	VW_PRESENT_MAIL_ITEM01 P									");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	P.SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	SYSDATE BETWEEN P.TERM_START_DATE AND P.TERM_END_DATE		");
		oSqlBuilder.AppendLine(sSortExpression);

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}
