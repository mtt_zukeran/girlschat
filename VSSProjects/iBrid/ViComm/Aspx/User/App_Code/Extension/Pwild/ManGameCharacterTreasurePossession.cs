﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝を所持している男性
--	Progaram ID		: ManGameCharacterTreasurePossession
--
--  Creation Date	: 2011.10.10
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;

[Serializable]
public class ManGameCharacterTreasurePossessionSeekCondition:SeekConditionBase {

	public string SiteCd;

	public string CastUserSeq {
		get {
			return this.Query["cast_user_seq"];
		}
		set {
			this.Query["cast_user_seq"] = value;
		}
	}

	public string CastCharNo {
		get {
			return this.Query["cast_char_no"];
		}
		set {
			this.Query["cast_char_no"] = value;
		}
	}

	public string CastGamePicSeq {
		get {
			return this.Query["cast_game_pic_seq"];
		}
		set {
			this.Query["cast_game_pic_seq"] = value;
		}
	}

	public string MosaicEraseFlag {
		get {
			return this.Query["mosaic_erase_flag"];
		}
		set {
			this.Query["mosaic_erase_flag"] = value;
		}
	}

	public ManGameCharacterTreasurePossessionSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManGameCharacterTreasurePossessionSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["cast_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_user_seq"]));
		this.Query["cast_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_char_no"]));
		this.Query["cast_game_pic_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_game_pic_seq"]));
		this.Query["mosaic_erase_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["mosaic_erase_flag"]));
	}
}

[System.Serializable]
public class ManGameCharacterTreasurePossession:DbSession {
	public ManGameCharacterTreasurePossession() {
	}

	public int GetPageCount(ManGameCharacterTreasurePossessionSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER01 	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManGameCharacterTreasurePossessionSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(ManGameCharacterTreasurePossessionSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	SITE_CD					,	");
		oSqlBuilder.AppendLine("	USER_SEQ				,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("	AGE						,	");
		oSqlBuilder.AppendLine("	UPDATE_DATE					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER01		");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		return oDataSet;
	}

	public OracleParameter[] CreateWhere(ManGameCharacterTreasurePossessionSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastGamePicSeq)) {
			SysPrograms.SqlAppendWhere("CAST_GAME_PIC_SEQ = :CAST_GAME_PIC_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCondition.CastGamePicSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.MosaicEraseFlag)) {
			SysPrograms.SqlAppendWhere("MOSAIC_ERASE_FLAG = :MOSAIC_ERASE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MOSAIC_ERASE_FLAG",pCondition.MosaicEraseFlag));
		}

		return oParamList.ToArray();
	}

	public string CreateOrderExpresion(ManGameCharacterTreasurePossessionSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY UPDATE_DATE DESC NULLS LAST";

		return sSortExpression;
	}
}
