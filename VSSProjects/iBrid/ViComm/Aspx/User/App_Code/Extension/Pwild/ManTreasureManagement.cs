﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性お宝管理

--	Progaram ID		: ManTreasureManagement
--
--  Creation Date	: 2011.10.10
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;

[Serializable]
public class ManTreasureManagementSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string CastGamePicSeq {
		get {
			return this.Query["cast_game_pic_seq"];
		}
		set {
			this.Query["cast_game_pic_seq"] = value;
		}
	}
	
	public string PublishFlag {
		get {
			return this.Query["publish_flag"];
		}
		set {
			this.Query["publish_flag"] = value;
		}
	}

	public ManTreasureManagementSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTreasureManagementSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["cast_game_pic_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_game_pic_seq"]));
		this.Query["publish_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["publish_flag"]));
	}
}

[System.Serializable]
public class ManTreasureManagement:DbSession {
	public ManTreasureManagement() {
	}

	public int GetPageCount(ManTreasureManagementSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PW_MAN_TREASURE01 P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManTreasureManagementSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(ManTreasureManagementSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine("	P.SITE_CD							,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ							,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_SEQ					,	");
		oSqlBuilder.AppendLine("	P.PIC_TITLE							,	");
		oSqlBuilder.AppendLine("	P.PIC_DOC							,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_ATTR_NM				,	");
		oSqlBuilder.AppendLine("	P.OBJ_SMALL_PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("	P.OBJ_PHOTO_IMG_PATH				,	");
		oSqlBuilder.AppendLine("	P.TOTAL_MOSAIC_ERASE_COUNT			,	");
		oSqlBuilder.AppendLine("	P.TOTAL_MAN_POSSESSION_COUNT		,	");
		oSqlBuilder.AppendLine("	P.PUBLISH_FLAG						,	");
		oSqlBuilder.AppendLine("	P.UNAUTH_MARK						,	");
		oSqlBuilder.AppendLine("	P.UPLOAD_DATE							");
		oSqlBuilder.AppendLine(" FROM										");
		oSqlBuilder.AppendLine("	VW_PW_MAN_TREASURE01 P					");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		return oDataSet;
	}

	public OracleParameter[] CreateWhere(ManTreasureManagementSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ	= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CastGamePicSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_GAME_PIC_SEQ	= :CAST_GAME_PIC_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCondition.CastGamePicSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PublishFlag)) {
			SysPrograms.SqlAppendWhere(" P.PUBLISH_FLAG	= :PUBLISH_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PUBLISH_FLAG",pCondition.PublishFlag));
		}
		
		return oParamList.ToArray();
	}

	public string CreateOrderExpresion(ManTreasureManagementSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_NEW) {
			sSortExpression = " ORDER BY P.UPLOAD_DATE DESC NULLS LAST";
		} else if (pCondition.Sort == PwViCommConst.GameCharacterSort.GAME_CHARACTER_SORT_PROTECT_COUNT) {
			sSortExpression = string.Format("ORDER BY P.TOTAL_MOSAIC_ERASE_COUNT DESC , P.USER_SEQ ");
		} else {
			sSortExpression = string.Format("ORDER BY P.USER_SEQ ");
		}

		return sSortExpression;
	}
}
