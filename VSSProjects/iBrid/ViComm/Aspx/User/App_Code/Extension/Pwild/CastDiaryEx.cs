﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者つぶやき
--	Progaram ID		: CastDiaryEx
--  Creation Date	: 2015.05.01
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastDiaryExSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}

	public string UserCharNo;

	public string ReportDay {
		get {
			return this.Query["reportday"];
		}
		set {
			this.Query["reportday"] = value;
		}
	}

	public string CastDiarySubSeq {
		get {
			return this.Query["subseq"];
		}
		set {
			this.Query["subseq"] = value;
		}
	}

	public string ManUserSeq;
	public string ManUserCharNo;

	public CastDiaryExSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastDiaryExSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["reportday"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["reportday"]));
		this.Query["subseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["subseq"]));
	}
}

public class CastDiaryEx:DbSession {
	public CastDiaryEx() {
	}

	public int GetPageCount(CastDiaryExSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_DIARY");
		oSqlBuilder.AppendLine("	INNER JOIN T_CAST_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_CAST_DIARY.SITE_CD = T_CAST_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("			T_CAST_DIARY.USER_SEQ = T_CAST_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("			T_CAST_DIARY.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastDiaryExSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.SITE_CD,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.USER_SEQ,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.REPORT_DAY,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.CAST_DIARY_SUB_SEQ,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.HTML_DOC1,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.HTML_DOC2,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.HTML_DOC3,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.HTML_DOC4,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.ATTACHED_OBJ_TYPE,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.PIC_SEQ,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.CREATE_DATE AS DIARY_CREATE_DATE,");
		oSqlBuilder.AppendLine("	T_CAST_DIARY.LIKE_COUNT,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.HANDLE_NM,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.AGE,");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID,");
		oSqlBuilder.AppendLine("	GET_PHOTO_IMG_PATH(T_CAST_DIARY.SITE_CD,T_USER.LOGIN_ID,T_CAST_DIARY.PIC_SEQ,T_CAST_CHARACTER.PIC_SEQ) AS OBJ_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	GET_SMALL_PHOTO_IMG_PATH(T_CAST_DIARY.SITE_CD,T_USER.LOGIN_ID,T_CAST_DIARY.PIC_SEQ,T_CAST_CHARACTER.PIC_SEQ) AS OBJ_SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	CASE WHEN (T_CAST_CHARACTER.START_PERFORM_DAY >= TO_CHAR(SYSDATE - T_SITE_MANAGEMENT.NEW_FACE_DAYS,'YYYY/MM/DD')) THEN T_SITE_MANAGEMENT.NEW_FACE_MARK ELSE NULL END AS NEW_CAST_SIGN,");
		oSqlBuilder.AppendLine("	GET_SMALL_PHOTO_IMG_PATH(T_CAST_CHARACTER.SITE_CD,T_USER.LOGIN_ID,T_CAST_CHARACTER.PIC_SEQ) AS SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_DIARY");
		oSqlBuilder.AppendLine("	INNER JOIN T_CAST_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_CAST_DIARY.SITE_CD = T_CAST_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("			T_CAST_DIARY.USER_SEQ = T_CAST_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("			T_CAST_DIARY.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER");
		oSqlBuilder.AppendLine("		ON (T_CAST_DIARY.USER_SEQ = T_USER.USER_SEQ)");
		oSqlBuilder.AppendLine("	INNER JOIN T_SITE_MANAGEMENT");
		oSqlBuilder.AppendLine("		ON (T_CAST_DIARY.SITE_CD = T_SITE_MANAGEMENT.SITE_CD)");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY T_CAST_DIARY.CREATE_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		AppendLikeMe(oDataSet,pCondtion);
		AppendFavorit(oDataSet,pCondtion);

		return oDataSet;
	}

	private void AppendLikeMe(DataSet pDS,CastDiaryExSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		if (string.IsNullOrEmpty(pCondition.ManUserSeq) || string.IsNullOrEmpty(pCondition.ManUserCharNo)) {
			return;
		}

		string sSiteCd = string.Empty;
		List<string> sCastUserSeqList = new List<string>();

		pDS.Tables[0].Columns.Add("LIKE_ME_FLAG",Type.GetType("System.String"));

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["LIKE_ME_FLAG"] = ViCommConst.FLAG_OFF_STR;
			sSiteCd = iBridUtil.GetStringValue(dr["SITE_CD"]);

			if (!sCastUserSeqList.Contains(iBridUtil.GetStringValue(dr["USER_SEQ"]))) {
				sCastUserSeqList.Add(iBridUtil.GetStringValue(dr["USER_SEQ"]));
			}
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	FAVORIT.SITE_CD,");
		oSqlBuilder.AppendLine("	FAVORIT.USER_SEQ,");
		oSqlBuilder.AppendLine("	FAVORIT.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	PROFILE_PIC_NON_PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("	GET_SMALL_PHOTO_IMG_PATH(FAVORIT.SITE_CD,T_USER.LOGIN_ID,NULL) AS SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		SITE_CD,");
		oSqlBuilder.AppendLine("		USER_SEQ,");
		oSqlBuilder.AppendLine("		USER_CHAR_NO,");
		oSqlBuilder.AppendLine("		PROFILE_PIC_NON_PUBLISH_FLAG");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_FAVORIT");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendFormat("		USER_SEQ IN ({0}) AND",string.Join(",",sCastUserSeqList.ToArray())).AppendLine();
		oSqlBuilder.AppendLine("		USER_CHAR_NO = :CAST_CHAR_NO AND");
		oSqlBuilder.AppendLine("		PARTNER_USER_SEQ = :MAN_USER_SEQ AND");
		oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO = :MAN_USER_CHAR_NO");
		oSqlBuilder.AppendLine("	) FAVORIT");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER");
		oSqlBuilder.AppendLine("		ON (FAVORIT.USER_SEQ = T_USER.USER_SEQ)");

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_CHAR_NO",pCondition.ManUserCharNo));

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		subDS.Tables[0].PrimaryKey = new DataColumn[] { subDS.Tables[0].Columns["SITE_CD"],subDS.Tables[0].Columns["USER_SEQ"],subDS.Tables[0].Columns["USER_CHAR_NO"] };

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			object[] findTheseVals = new object[] { dr["SITE_CD"],dr["USER_SEQ"],dr["USER_CHAR_NO"] };
			DataRow subDR = subDS.Tables[0].Rows.Find(findTheseVals);

			if (subDR != null) {
				dr["LIKE_ME_FLAG"] = ViCommConst.FLAG_ON_STR;

				if (iBridUtil.GetStringValue(subDR["PROFILE_PIC_NON_PUBLISH_FLAG"]).Equals("2")) {
					dr["SMALL_PHOTO_IMG_PATH"] = iBridUtil.GetStringValue(subDR["SMALL_PHOTO_IMG_PATH"]);
				}
			}
		}
	}

	private void AppendFavorit(DataSet pDS,CastDiaryExSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		if (string.IsNullOrEmpty(pCondition.ManUserSeq) || string.IsNullOrEmpty(pCondition.ManUserCharNo)) {
			return;
		}

		string sSiteCd = string.Empty;
		List<string> sCastUserSeqList = new List<string>();

		pDS.Tables[0].Columns.Add("FAVORIT_FLAG",Type.GetType("System.String"));

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["FAVORIT_FLAG"] = ViCommConst.FLAG_OFF_STR;
			sSiteCd = iBridUtil.GetStringValue(dr["SITE_CD"]);

			if (!sCastUserSeqList.Contains(iBridUtil.GetStringValue(dr["USER_SEQ"]))) {
				sCastUserSeqList.Add(iBridUtil.GetStringValue(dr["USER_SEQ"]));
			}
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ,");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FAVORIT");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :MAN_USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :MAN_USER_CHAR_NO AND");
		oSqlBuilder.AppendFormat("	PARTNER_USER_SEQ IN ({0}) AND",string.Join(",",sCastUserSeqList.ToArray())).AppendLine();
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO = :CAST_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_CHAR_NO",pCondition.ManUserCharNo));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",ViCommConst.MAIN_CHAR_NO));

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		subDS.Tables[0].PrimaryKey = new DataColumn[] { subDS.Tables[0].Columns["SITE_CD"],subDS.Tables[0].Columns["PARTNER_USER_SEQ"],subDS.Tables[0].Columns["PARTNER_USER_CHAR_NO"] };

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			object[] findTheseVals = new object[] { dr["SITE_CD"],dr["USER_SEQ"],dr["USER_CHAR_NO"] };
			DataRow subDR = subDS.Tables[0].Rows.Find(findTheseVals);

			if (subDR != null) {
				dr["FAVORIT_FLAG"] = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	private OracleParameter[] CreateWhere(CastDiaryExSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("T_CAST_DIARY.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("T_CAST_DIARY.USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("T_CAST_DIARY.USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ReportDay)) {
			SysPrograms.SqlAppendWhere("T_CAST_DIARY.REPORT_DAY = :REPORT_DAY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_DAY",pCondition.ReportDay));
		}

		if (!string.IsNullOrEmpty(pCondition.CastDiarySubSeq)) {
			SysPrograms.SqlAppendWhere("T_CAST_DIARY.CAST_DIARY_SUB_SEQ = :CAST_DIARY_SUB_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_DIARY_SUB_SEQ",pCondition.CastDiarySubSeq));
		}

		SysPrograms.SqlAppendWhere("T_CAST_DIARY.WAIT_TX_FLAG = 0",ref pWhereClause);
		SysPrograms.SqlAppendWhere("T_CAST_DIARY.DEL_FLAG = 0",ref pWhereClause);
		SysPrograms.SqlAppendWhere("T_CAST_DIARY.ADMIN_DEL_FLAG = 0",ref pWhereClause);

		SysPrograms.SqlAppendWhere("T_CAST_CHARACTER.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		if (!string.IsNullOrEmpty(pCondition.ManUserSeq) && !string.IsNullOrEmpty(pCondition.ManUserCharNo)) {
			SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_FAVORIT WHERE T_CAST_DIARY.SITE_CD = T_FAVORIT.SITE_CD AND T_CAST_DIARY.USER_SEQ = T_FAVORIT.USER_SEQ AND T_CAST_DIARY.USER_CHAR_NO = T_FAVORIT.USER_CHAR_NO AND :FAVORIT_PARTNER_USER_SEQ = T_FAVORIT.PARTNER_USER_SEQ AND	:FAVORIT_PARTNER_USER_CHAR_NO = T_FAVORIT.PARTNER_USER_CHAR_NO AND 1 = T_FAVORIT.DIARY_NON_PUBLISH_FLAG)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FAVORIT_PARTNER_USER_SEQ",pCondition.ManUserSeq));
			oParamList.Add(new OracleParameter(":FAVORIT_PARTNER_USER_CHAR_NO",pCondition.ManUserCharNo));

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS,2)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE T_REFUSE.SITE_CD = T_CAST_DIARY.SITE_CD AND T_REFUSE.USER_SEQ = T_CAST_DIARY.USER_SEQ AND T_REFUSE.USER_CHAR_NO = T_CAST_DIARY.USER_CHAR_NO AND T_REFUSE.PARTNER_USER_SEQ = :REFUSE_PARTNER_USER_SEQ AND T_REFUSE.PARTNER_USER_CHAR_NO = :REFUSE_PARTNER_USER_CHAR_NO)",ref pWhereClause);
					oParamList.Add(new OracleParameter(":REFUSE_PARTNER_USER_SEQ",pCondition.ManUserSeq));
					oParamList.Add(new OracleParameter(":REFUSE_PARTNER_USER_CHAR_NO",pCondition.ManUserCharNo));
				}
			}
		}

		return oParamList.ToArray();
	}
}
