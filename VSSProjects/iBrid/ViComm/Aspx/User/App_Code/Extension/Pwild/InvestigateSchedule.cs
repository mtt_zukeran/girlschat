﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せ･スケジュール


--	Progaram ID		: InvestigateSchedule
--
--  Creation Date	: 2015.02.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class InvestigateScheduleSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}
	public string ExecutionDay {
		get {
			return this.Query["execution_day"];
		}
		set {
			if (string.IsNullOrEmpty(value)) {
				this.Query["execution_day"] = DateTime.Now.ToString("yyyy/MM/dd");
			} else {
				this.Query["execution_day"] = value;
			}
		}
	}

	public InvestigateScheduleSeekCondition()
		: this(new NameValueCollection()) {
	}
	public InvestigateScheduleSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["execution_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["execution_day"]));
	}
}

[System.Serializable]
public class InvestigateSchedule:DbSession {
	public InvestigateSchedule() {

	}

	public DataSet GetOne(InvestigateScheduleSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	SITE_CD								,	");
		oSqlBuilder.AppendLine("	EXECUTION_DAY						,	");
		oSqlBuilder.AppendLine("	FIRST_HINT_ANNOUNCE_TIME			,	");
		oSqlBuilder.AppendLine("	SECOND_HINT_ANNOUNCE_TIME			,	");
		oSqlBuilder.AppendLine("	THIRD_HINT_ANNOUNCE_TIME			,	");
		oSqlBuilder.AppendLine("	BOUNTY_POINT						,	");
		oSqlBuilder.AppendLine("	POINT_PER_ENTRANT					,	");
		oSqlBuilder.AppendLine("	REVISION_NO								");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_SCHEDULE					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	EXECUTION_DAY	= :EXECUTION_DAY		");
		oSqlBuilder.AppendLine("ORDER BY EXECUTION_DAY						");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":EXECUTION_DAY",pCondition.ExecutionDay));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
}
