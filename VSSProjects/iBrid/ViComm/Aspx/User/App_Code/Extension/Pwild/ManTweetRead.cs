﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 会員つぶやき購読
--	Progaram ID		: ManTweetRead
--
--  Creation Date	: 2013.03.20
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class ManTweetReadSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string ManUserSeq;
	public List<string> ManUserSeqList = new List<string>();
	public string CastUserSeq;
	public string CastCharNo;

	public ManTweetReadSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTweetReadSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

public class ManTweetRead:DbSession {
	public ManTweetRead() {
	}

	public int GetPageCount(ManTweetReadSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_READ");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManTweetReadSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_READ");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManTweetReadSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.ManUserSeq)) {
			SysPrograms.SqlAppendWhere("MAN_USER_SEQ = :MAN_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		}

		if (pCondition.ManUserSeqList.Count > 0) {
			SysPrograms.SqlAppendWhere(string.Format("MAN_USER_SEQ IN ({0})",string.Join(",",pCondition.ManUserSeqList.ToArray())),ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(ManTweetReadSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = "ORDER BY CREATE_DATE ASC";

		return sSortExpression;
	}

	public void RegistManTweetRead(string pSiteCd,string pManUserSeq,string pCastUserSeq,string pCastCharNo,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_MAN_TWEET_READ");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void DeleteManTweetRead(string pSiteCd,string pManUserSeq,string pCastUserSeq,string pCastCharNo,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_MAN_TWEET_READ");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
