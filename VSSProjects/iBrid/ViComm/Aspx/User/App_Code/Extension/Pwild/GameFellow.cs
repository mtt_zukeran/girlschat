﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・仲間

--	Progaram ID		: GameFellow
--
--  Creation Date	: 2011.08.16
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[Serializable]
public class GameFellowSeekCondition:SeekConditionBase {

	public string SiteCd;

	public string UserSeq;

	public string UserCharNo;

	public string FellowApplicationStatus;

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public string GameType {
		get {
			return this.Query["game_type"];
		}
		set {
			this.Query["game_type"] = value;
		}
	}
		
	public GameFellowSeekCondition()
		: this(new NameValueCollection()) {
	}
	public GameFellowSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
		this.Query["game_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_type"]));
	}
}

#endregion ============================================================================================================

[System.Serializable]
public class GameFellow:DbSession {
	public GameFellow() {
	}

	public DataSet GetOneByUserSeqForCast(GameFellowSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	FELLOW.PARTNER_USER_SEQ AS	USER_SEQ						,	");
		oSqlBuilder.AppendLine("	FELLOW.PARTNER_USER_CHAR_NO	AS USER_CHAR_NO					,	");
		oSqlBuilder.AppendLine("	FELLOW.PARTNER_USER_SEQ										,	");
		oSqlBuilder.AppendLine("	FELLOW.PARTNER_USER_CHAR_NO									,	");
		oSqlBuilder.AppendLine("	FELLOW.GAME_HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	FELLOW.GAME_CHARACTER_LEVEL									,	");
		oSqlBuilder.AppendLine("	FELLOW.GAME_CHARACTER_TYPE									,	");
		oSqlBuilder.AppendLine("	FELLOW.LOGIN_ID												,	");
		oSqlBuilder.AppendLine("	FELLOW.SITE_USE_STATUS										,	");
		oSqlBuilder.AppendLine("	FELLOW.SMALL_PHOTO_IMG_PATH									,	");
		oSqlBuilder.AppendLine("	FELLOW.AGE													,	");
		oSqlBuilder.AppendLine("	FELLOW.CHARACTER_ONLINE_STATUS								,	");
		oSqlBuilder.AppendLine("	FELLOW.REC_SEQ												,	");
		oSqlBuilder.AppendLine("	FELLOW.NA_FLAG												,	");
		oSqlBuilder.AppendLine("	FELLOW.TOTAL_TALK_COUNT										,	");
		oSqlBuilder.AppendLine("	FELLOW.TOTAL_RX_MAIL_COUNT									,	");
		oSqlBuilder.AppendLine("	FELLOW.TOTAL_TX_MAIL_COUNT									,	");
		oSqlBuilder.AppendLine("	FP.RANK	AS FRIENDLY_RANK									,	");
		oSqlBuilder.AppendLine("	CASE															");
		oSqlBuilder.AppendLine("		WHEN FELLOW.CHARACTER_ONLINE_STATUS = 0						");
		oSqlBuilder.AppendLine("		THEN TSM.OFFLINE_GUIDANCE									");
		oSqlBuilder.AppendLine("		WHEN FELLOW.CHARACTER_ONLINE_STATUS = 1						");
		oSqlBuilder.AppendLine("		THEN TSM.LOGINED_GUIDANCE									");
		oSqlBuilder.AppendLine("		WHEN FELLOW.CHARACTER_ONLINE_STATUS = 2						");
		oSqlBuilder.AppendLine("		THEN TSM.WAITING_GUIDANCE									");
		oSqlBuilder.AppendLine("		WHEN FELLOW.CHARACTER_ONLINE_STATUS = 3						");
		oSqlBuilder.AppendLine("		THEN TSM.TALKING_WSHOT_GUIDANCE								");
		oSqlBuilder.AppendLine("	END AS MOBILE_ONLINE_STATUS_NM								,	");
		oSqlBuilder.AppendLine("	REC.RECORDING_DATE AS MAN_PF_RECORDING_DATE					,	");
		oSqlBuilder.AppendLine("	REC.FOWARD_TERM_SCH_DATE AS MAN_PF_FOWARD_TERM_SCH_DATE		,	");
		oSqlBuilder.AppendLine("	REC.WAITING_TYPE AS MAN_PF_WAITING_TYPE							");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			SITE_CD									,	");
		oSqlBuilder.AppendLine("			USER_SEQ								,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO							,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ						,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO					,	");
		oSqlBuilder.AppendLine("			GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_LEVEL					,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("			CHARACTER_ONLINE_STATUS					,	");
		oSqlBuilder.AppendLine("			LOGIN_ID								,	");
		oSqlBuilder.AppendLine("			SITE_USE_STATUS							,	");
		oSqlBuilder.AppendLine("			SMALL_PHOTO_IMG_PATH					,	");
		oSqlBuilder.AppendLine("			AGE										,	");
		oSqlBuilder.AppendLine("			REC_SEQ									,	");
		oSqlBuilder.AppendLine("			NA_FLAG									,	");
		oSqlBuilder.AppendLine("			TOTAL_TALK_COUNT						,	");
		oSqlBuilder.AppendLine("			TOTAL_RX_MAIL_COUNT						,	");
		oSqlBuilder.AppendLine("			TOTAL_TX_MAIL_COUNT							");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			VW_PW_GAME_FELLOW01							");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			SITE_CD						= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= :USER_SEQ						AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= :PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= :PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			FELLOW_APPLICATION_STATUS	= :FELLOW_APPLICATION_STATUS	AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("			TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		oSqlBuilder.AppendLine("	) FELLOW															,	");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00	FP												,	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT TSM												,	");
		oSqlBuilder.AppendLine("	T_RECORDING REC															");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	FELLOW.SITE_CD					= FP.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	FELLOW.PARTNER_USER_SEQ			= FP.USER_SEQ					(+)	AND	");
		oSqlBuilder.AppendLine("	FELLOW.PARTNER_USER_CHAR_NO		= FP.USER_CHAR_NO				(+)	AND	");
		oSqlBuilder.AppendLine("	FELLOW.USER_SEQ					= FP.PARTNER_USER_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	FELLOW.USER_CHAR_NO				= FP.PARTNER_USER_CHAR_NO		(+)	AND	");
		oSqlBuilder.AppendLine("	FELLOW.SITE_CD					= TSM.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	FELLOW.REC_SEQ					= REC.REC_SEQ					(+)		");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pCondition.FellowApplicationStatus));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		string sSortExpression = "ORDER BY PARTNER_USER_SEQ ASC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,0,1,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public int GetApplicationCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pFellowApplicationStatus) {
		int iCount = 0;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	COUNT(*)												");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_GAME_FELLOW											");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND									");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND								");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND						");
		oSqlBuilder.AppendLine("	FELLOW_APPLICATION_STATUS = :FELLOW_APPLICATION_STATUS	");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pFellowApplicationStatus));

		iCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iCount;
	}

	public string ApplyGameFellow(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("APPLY_GAME_FELLOW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string CancelApplicationFellow(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CANCEL_APPLICATION_FELLOW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string RemoveGameFellow(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REMOVE_GAME_FELLOW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string AcceptApplicationFellow(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,int pRefuseFlag) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCEPT_APPLICATION_FELLOW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pREFUSE_FLAG",DbSession.DbType.NUMBER,pRefuseFlag);
			db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string SetPartyMember(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,int pAddFlag) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_PARTY_MEMBER");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("pADD_FLAG",DbSession.DbType.NUMBER,pAddFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string GetNowHugCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		string sToday = DateTime.Now.ToString("yyyy/MM/dd");

		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	COUNT(*)													");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_GAME_FELLOW												");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND										");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND									");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND							");
		oSqlBuilder.AppendLine("	FELLOW_APPLICATION_STATUS = :FELLOW_APPLICATION_STATUS	AND	");
		oSqlBuilder.AppendLine("	LAST_HUG_DAY = :LAST_HUG_DAY	");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",PwViCommConst.GameCharFellowApplicationCd.FELLOW));
		oParamList.Add(new OracleParameter(":LAST_HUG_DAY",sToday));

		iCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iCount.ToString();
	}

	public string GetFellowCount(GameFellowSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	COUNT(*) AS COUNT									");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	T_GAME_FELLOW										");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	SITE_CD						= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ					= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO				= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	FELLOW_APPLICATION_STATUS	!= :FELLOW_APPLICATION_STATUS");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",PwViCommConst.GameCharFellowApplicationCd.REFUSED));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return oDataSet.Tables[0].Rows[0]["COUNT"].ToString();
		} else {
			return "0";
		}
	}

	public int GetSelfGameFellowCountEx(GameFellowSeekCondition pCondition) {

		int iCount = 0;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	COUNT(*) AS COUNT									");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	VW_GAME_FELLOW01									");

		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		iCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);

		return iCount;		
	}

	private OracleParameter[] CreateWhere(GameFellowSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.FellowApplicationStatus)) {
			SysPrograms.SqlAppendWhere("FELLOW_APPLICATION_STATUS = :FELLOW_APPLICATION_STATUS",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FELLOW_APPLICATION_STATUS",pCondition.FellowApplicationStatus));
		}

		if (!string.IsNullOrEmpty(pCondition.GameType)) {
			SysPrograms.SqlAppendWhere(" PARTNER_GAME_CHARACTER_TYPE = :PARTNER_GAME_CHARACTER_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PARTNER_GAME_CHARACTER_TYPE",pCondition.GameType));
		}

		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_FLAG = :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_MISSION_TREASURE_FLAG = :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_FLAG = :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" TUTORIAL_BATTLE_TREASURE_FLAG = :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}
	
}
