﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰ詳細
--	Progaram ID		: CastGameCharacterView
--
--  Creation Date	: 2011.08.09
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterViewSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string PartnerUserSeq {
		get {
			return this.Query["partner_user_seq"];
		}
		set {
			this.Query["partner_user_seq"] = value;
		}
	}

	public string PartnerUserCharNo {
		get {
			return this.Query["partner_user_char_no"];
		}
		set {
			this.Query["partner_user_char_no"] = value;
		}
	}

	public CastGameCharacterViewSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterViewSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["partner_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_seq"]));
		this.Query["partner_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["partner_user_char_no"]));
	}
}

[System.Serializable]
public class CastGameCharacterView:CastBase {
	public int GetPageCount(CastGameCharacterViewSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine(" FROM			");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHARACTER00 P	");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterViewSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "	,   ");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE		,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL		,	");
		oSqlBuilder.AppendLine("	P.FELLOW_COUNT				,	");
		oSqlBuilder.AppendLine("	P.TOTAL_WIN_COUNT			,	");
		oSqlBuilder.AppendLine("	P.TOTAL_LOSS_COUNT			,	");
		oSqlBuilder.AppendLine("	P.TOTAL_PARTY_WIN_COUNT		,	");
		oSqlBuilder.AppendLine("	P.TOTAL_PARTY_LOSS_COUNT	,	");
		oSqlBuilder.AppendLine("	P.SITE_USE_STATUS				");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_CHARACTER00 P	");

		// where
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		FakeOnlineStatus(oDataSet);
		AppendCastAttr(oDataSet);
		AppendManTreasure(oDataSet,pCondtion);
		AppendCastMovie(oDataSet,pCondtion);
		AppendFellow(oDataSet,pCondtion);
		AppendGameCommunication(oDataSet,pCondtion);
		AppendFriendlyPoint(oDataSet,pCondtion);
		AppendFavorite(oDataSet,pCondtion);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastGameCharacterViewSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.PartnerUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.PartnerUserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.PartnerUserCharNo));
		}

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG	= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(CastGameCharacterViewSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = " ORDER BY SITE_CD ";
		return sSortExpression;
	}

	private void AppendManTreasure(DataSet pDS,CastGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("GAME_TREASURE_PIC_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_PIC_COUNT		");
		oSqlBuilder.AppendLine(" FROM							   			");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE P						");
		oSqlBuilder.AppendLine(" WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND		");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO	AND		");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG	= :PUBLISH_FLAG	AND		");
		oSqlBuilder.AppendLine("	NOT EXISTS								");
		oSqlBuilder.AppendLine("	(										");
		oSqlBuilder.AppendLine("		SELECT								");
		oSqlBuilder.AppendLine("			*								");
		oSqlBuilder.AppendLine("		FROM								");
		oSqlBuilder.AppendLine("			T_FAVORIT						");
		oSqlBuilder.AppendLine("		WHERE								");
		oSqlBuilder.AppendLine("			SITE_CD						= P.SITE_CD				AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= P.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= P.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= :SELF_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= :SELF_USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			GAME_PIC_NON_PUBLISH_DATE	< P.UPLOAD_DATE				");
		oSqlBuilder.AppendLine("	)																");
		
		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",dr["SITE_CD"]);
				cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"]);
				cmd.Parameters.Add(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);
				cmd.Parameters.Add(":SELF_USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":SELF_USER_CHAR_NO",pCondition.UserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}
				
				dr["GAME_TREASURE_PIC_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_TREASURE_PIC_COUNT"];
			}
		}
	}

	private void AppendCastMovie(DataSet pDS,CastGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("GAME_TREASURE_MOVIE_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	COUNT(*) AS GAME_TREASURE_MOVIE_COUNT				");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE P										");
		oSqlBuilder.AppendLine(" WHERE													");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	MOVIE_TYPE				= :MOVIE_TYPE			AND	");
		oSqlBuilder.AppendLine("	OBJ_NOT_APPROVE_FLAG	= :OBJ_NOT_APPROVE_FLAG	AND	");
		oSqlBuilder.AppendLine("	OBJ_NOT_PUBLISH_FLAG	= :OBJ_NOT_PUBLISH_FLAG	AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS											");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			*											");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_FAVORIT									");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			SITE_CD						= P.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ					= P.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				= P.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			= :SELF_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		= :SELF_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			BBS_NON_PUBLISH_FLAG		= :BBS_NON_PUBLISH_FLAG			");
		oSqlBuilder.AppendLine("	)																	");
		

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",dr["SITE_CD"]);
				cmd.Parameters.Add(":USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":USER_CHAR_NO",dr["USER_CHAR_NO"]);
				cmd.Parameters.Add(":MOVIE_TYPE",ViCommConst.ATTACHED_SOCIAL_GAME);
				cmd.Parameters.Add(":OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":SELF_USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":SELF_USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_ON_STR);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				dr["GAME_TREASURE_MOVIE_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_TREASURE_MOVIE_COUNT"];
			}
		}
	}
	
	private void AppendFellow(DataSet pDS,CastGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("FELLOW_APPLICATION_STATUS"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	FELLOW_APPLICATION_STATUS								");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_GAME_FELLOW											");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND ");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",dr["USER_CHAR_NO"]);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["FELLOW_APPLICATION_STATUS"] = dsSub.Tables[0].Rows[0]["FELLOW_APPLICATION_STATUS"];
				}
			}
		}
	}

	private void AppendGameCommunication(DataSet pDS,CastGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("LAST_HUG_DATE"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	LAST_HUG_DATE											");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_GAME_COMMUNICATION									");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND ");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",dr["USER_CHAR_NO"]);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["LAST_HUG_DATE"] = dsSub.Tables[0].Rows[0]["LAST_HUG_DATE"];
				}
			}
		}
	}

	private void AppendFriendlyPoint(DataSet pDS,CastGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("FRIENDLY_RANK"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	RANK													");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	MV_FRIENDLY_POINT00										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",dr["USER_CHAR_NO"]);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}
				
				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["FRIENDLY_RANK"] = dsSub.Tables[0].Rows[0]["RANK"];
				} else {
					dr["FRIENDLY_RANK"] = null;					
				}
			}
		}
	}

	private void AppendFavorite(DataSet pDS,CastGameCharacterViewSeekCondition pCondition) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;
		col = new DataColumn(string.Format("GAME_FAVORITE"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	SITE_CD													");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_GAME_FAVORIT											");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND		");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO			");

		DataRow dr = pDS.Tables[0].Rows[0];
		using (DataSet dsSub = new DataSet()) {
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pCondition.SiteCd);
				cmd.Parameters.Add(":USER_SEQ",pCondition.UserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pCondition.UserCharNo);
				cmd.Parameters.Add(":PARTNER_USER_SEQ",dr["USER_SEQ"]);
				cmd.Parameters.Add(":PARTNER_USER_CHAR_NO",dr["USER_CHAR_NO"]);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub);
				}

				if (dsSub.Tables[0].Rows.Count > 0) {
					dr["GAME_FAVORITE"] = ViCommConst.FLAG_ON_STR;
				} else {
					dr["GAME_FAVORITE"] = ViCommConst.FLAG_OFF_STR;
				}
			}
		}
	}
}