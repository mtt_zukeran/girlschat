﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 女性検索履歴
--	Progaram ID		: FindCastHistory
--
--  Creation Date	: 2016.10.14
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[System.Serializable]
public class FindCastCondition {
	public string onlineStatus;
	public string ageFrom;
	public string ageTo;
	public string area;
	public string areaDefault;
	public string hDegree;
	public string smDegree;
	public string style;
	public string likeType;
	public string job;
	public string loginTimeZone;
	public string waitTel;
	public string faceDisplay;
	public string handleNm;
	public string withoutNonAdult;
	public string height;
	public string bustFrom;
	public string bustTo;
	public string bloodType;
	public string freeword;

	public FindCastCondition() {
		onlineStatus = string.Empty;
		ageFrom = string.Empty;
		ageTo = string.Empty;
		area = string.Empty;
		areaDefault = string.Empty;
		hDegree = string.Empty;
		smDegree = string.Empty;
		style = string.Empty;
		likeType = string.Empty;
		job = string.Empty;
		loginTimeZone = string.Empty;
		waitTel = string.Empty;
		faceDisplay = string.Empty;
		handleNm = string.Empty;
		withoutNonAdult = string.Empty;
		height = string.Empty;
		bustFrom = string.Empty;
		bustTo = string.Empty;
		bloodType = string.Empty;
		freeword = string.Empty;
	}
}

public class FindCastHistory:DbSession {
	public FindCastHistory()
		: base() {
	}

	/// <summary>
	/// 検索した各項目の値を登録
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pCondition"></param>
	public void RegistFindCastHistory(string pSiteCd,string pUserSeq, FindCastCondition pCondition) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_FIND_CAST_HISTORY");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pONLINE_STATUS",DbSession.DbType.VARCHAR2,pCondition.onlineStatus);
			db.ProcedureInParm("pAGE_FROM",DbSession.DbType.VARCHAR2,pCondition.ageFrom);
			db.ProcedureInParm("pAGE_TO",DbSession.DbType.VARCHAR2,pCondition.ageTo);
			db.ProcedureInParm("pAREA",DbSession.DbType.VARCHAR2,pCondition.area);
			db.ProcedureInParm("pAREA_DEFAULT",DbSession.DbType.VARCHAR2,pCondition.areaDefault);
			db.ProcedureInParm("pH_DEGREE",DbSession.DbType.VARCHAR2,pCondition.hDegree);
			db.ProcedureInParm("pSM_DEGREE",DbSession.DbType.VARCHAR2,pCondition.smDegree);
			db.ProcedureInParm("pSTYLE",DbSession.DbType.VARCHAR2,pCondition.style);
			db.ProcedureInParm("pLIKE_TYPE",DbSession.DbType.VARCHAR2,pCondition.likeType);
			db.ProcedureInParm("pJOB",DbSession.DbType.VARCHAR2,pCondition.job);
			db.ProcedureInParm("pLOGIN_TIME_ZONE",DbSession.DbType.VARCHAR2,pCondition.loginTimeZone);
			db.ProcedureInParm("pWAIT_TEL",DbSession.DbType.VARCHAR2,pCondition.waitTel);
			db.ProcedureInParm("pFACE_DISPLAY",DbSession.DbType.VARCHAR2,pCondition.faceDisplay);
			db.ProcedureInParm("pHANDLE_NM",DbSession.DbType.VARCHAR2,pCondition.handleNm);
			db.ProcedureInParm("pWITHOUT_NON_ADULT",DbSession.DbType.VARCHAR2,pCondition.withoutNonAdult);
			db.ProcedureInParm("pHEIGHT",DbSession.DbType.VARCHAR2,pCondition.height);
			db.ProcedureInParm("pBUST_FROM",DbSession.DbType.VARCHAR2,pCondition.bustFrom);
			db.ProcedureInParm("pBUST_TO",DbSession.DbType.VARCHAR2,pCondition.bustTo);
			db.ProcedureInParm("pBLOOD_TYPE",DbSession.DbType.VARCHAR2,pCondition.bloodType);
			db.ProcedureInParm("pFREE_WORD",DbSession.DbType.VARCHAR2,pCondition.freeword);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}