﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: 投票順位
--	Progaram ID		: VoteRank
--  Creation Date	: 2013.10.18
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class VoteRankSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string VoteTermSeq {
		get {
			return this.Query["votetermseq"];
		}
		set {
			this.Query["votetermseq"] = value;
		}
	}
	
	public string FixRankFlag;

	public string CastUserSeq {
		get {
			return this.Query["castuserseq"];
		}
		set {
			this.Query["castuserseq"] = value;
		}
	}

	public string CastCharNo {
		get {
			return this.Query["castcharno"];
		}
		set {
			this.Query["castcharno"] = value;
		}
	}

	public VoteRankSeekCondition()
		: this(new NameValueCollection()) {
	}

	public VoteRankSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["castuserseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castuserseq"]));
		this.Query["castcharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castcharno"]));
		this.Query["votetermseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["votetermseq"]));
	}
}

public class VoteRank:CastBase {
	public VoteRank() {
	}

	public int GetPageCount(VoteRankSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sViewNm = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		if (pCondition.FixRankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sViewNm = "VW_VOTE_FIX_RANK00";
		} else {
			sViewNm = "VW_VOTE_RANK00";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendFormat("	{0} P",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(VoteRankSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		string sViewNm = string.Empty;
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		if (pCondition.FixRankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sViewNm = "VW_VOTE_FIX_RANK00";
		} else {
			sViewNm = "VW_VOTE_RANK00";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	" + CastBasicField() + ",");
		oSqlBuilder.AppendLine("	VOTE_COUNT,");
		oSqlBuilder.AppendLine("	VOTE_RANK,");
		oSqlBuilder.AppendLine("	VOTE_END_DATE,");
		oSqlBuilder.AppendLine("	TICKET_POINT,");
		oSqlBuilder.AppendLine("	FIX_RANK_FLAG,");
		oSqlBuilder.AppendLine("	FIX_RANK_DATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendFormat("	{0} P",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = CreateOrderExpresion(pCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		AppendCastAttr(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(VoteRankSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.VoteTermSeq)) {
			SysPrograms.SqlAppendWhere("VOTE_TERM_SEQ = :VOTE_TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":VOTE_TERM_SEQ",pCondition.VoteTermSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.CastCharNo));
		}

		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(VoteRankSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		sSortExpression = "ORDER BY SITE_CD,VOTE_TERM_SEQ,VOTE_COUNT DESC,LAST_VOTE_DATE ASC";
		return sSortExpression;
	}

	public void RegistVote(
		string pSiteCd,
		string pManUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		int pVoteCount,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_VOTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pMAN_USER_SEQ",DbSession.DbType.NUMBER,pManUserSeq);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.NUMBER,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pVOTE_COUNT",DbSession.DbType.NUMBER,pVoteCount);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
}
