﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ ﾆｭｰｽ

--	Progaram ID		: News
--
--  Creation Date	: 2011.09.20
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class NewsSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public NewsSeekCondition()
		: this(new NameValueCollection()) {
	}

	public NewsSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}
#endregion ============================================================================================================

public class News:DbSession {
	public News() {
	}

	public int GetPageCountFellowLog(NewsSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_NEWS			,	");
		oSqlBuilder.AppendLine("	(					");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			T_GAME_FELLOW				");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		
		oSqlBuilder.AppendLine("	) FELLOW						,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER				,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER					");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= FELLOW.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= FELLOW.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= FELLOW.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.SITE_CD		= T_CAST_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_SEQ		= T_CAST_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_CHAR_NO	= T_CAST_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_NEWS.NEWS_TYPE				!= :NEWS_TYPE						AND	");
		oSqlBuilder.AppendLine("   (T_CAST_CHARACTER.NA_FLAG		IN (:NA_FLAG,:NA_FLAG_2))			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG		");
		
		oParamList.Add(new OracleParameter(":NEWS_TYPE",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		if (pRecCount > 20) {
			pRecCount = 20;
		}

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public int GetPageCountManFellowLog(NewsSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_NEWS			,	");
		oSqlBuilder.AppendLine("	(					");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			T_GAME_FELLOW				");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) FELLOW						,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER				,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER				");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= FELLOW.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= FELLOW.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= FELLOW.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.SITE_CD		= T_USER_MAN_CHARACTER.SITE_CD		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_CHAR_NO	= T_USER_MAN_CHARACTER.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_NEWS.NEWS_TYPE				NOT IN (:NEWS_TYPE01,:NEWS_TYPE02)					AND	");
		oSqlBuilder.AppendLine("   (T_USER_MAN_CHARACTER.NA_FLAG	IN (:NA_FLAG,:NA_FLAG_2))				");

		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NEWS_TYPE01",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NEWS_TYPE02",PwViCommConst.GameNewsType.NEWS_GET_HONOR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		if (pRecCount > 20) {
			pRecCount = 20;
		}

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollectionFellowLog(NewsSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	USER_SEQ			AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("	NEWS_TYPE										,	");
		oSqlBuilder.AppendLine("	NEWS_ITEM_NM									,	");
		oSqlBuilder.AppendLine("	CREATE_DATE										,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM									,	");
		oSqlBuilder.AppendLine("	HANDLE_NM											");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ							,	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_ITEM_NM						,	");
		oSqlBuilder.AppendLine("			T_NEWS.CREATE_DATE						,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("			T_CAST_CHARACTER.HANDLE_NM					");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_NEWS									,	");
		oSqlBuilder.AppendLine("			(											");
		oSqlBuilder.AppendLine("				SELECT									");
		oSqlBuilder.AppendLine("					SITE_CD							,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ				,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO				");
		oSqlBuilder.AppendLine("				FROM									");
		oSqlBuilder.AppendLine("					T_GAME_FELLOW						");
		
		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		
		oSqlBuilder.AppendLine("			) FELLOW								,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER						,	");
		oSqlBuilder.AppendLine("			T_CAST_CHARACTER							");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= FELLOW.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= FELLOW.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= FELLOW.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.SITE_CD		= T_CAST_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_SEQ		= T_CAST_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_CHAR_NO	= T_CAST_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG = :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE				!= :NEWS_TYPE						AND	");
		oSqlBuilder.AppendLine("		   (T_CAST_CHARACTER.NA_FLAG		IN (:NA_FLAG,:NA_FLAG_2))				");
		oSqlBuilder.AppendLine("	)																				");
		
		// sort
		string sSortExpression = "ORDER BY CREATE_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NEWS_TYPE",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionManFellowLog(NewsSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	USER_SEQ			AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("	NEWS_TYPE										,	");
		oSqlBuilder.AppendLine("	NEWS_ITEM_NM									,	");
		oSqlBuilder.AppendLine("	CREATE_DATE										,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ							,	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_ITEM_NM						,	");
		oSqlBuilder.AppendLine("			T_NEWS.CREATE_DATE						,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.GAME_HANDLE_NM				");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_NEWS									,	");
		oSqlBuilder.AppendLine("			(											");
		oSqlBuilder.AppendLine("				SELECT									");
		oSqlBuilder.AppendLine("					SITE_CD							,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ				,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO				");
		oSqlBuilder.AppendLine("				FROM									");
		oSqlBuilder.AppendLine("					T_GAME_FELLOW						");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("			) FELLOW								,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER						,	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER						");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= FELLOW.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= FELLOW.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= FELLOW.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.SITE_CD		= T_USER_MAN_CHARACTER.SITE_CD		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_CHAR_NO	= T_USER_MAN_CHARACTER.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE				NOT IN (:NEWS_TYPE01,:NEWS_TYPE02)					AND	");
		oSqlBuilder.AppendLine("		   (T_USER_MAN_CHARACTER.NA_FLAG	IN (:NA_FLAG,:NA_FLAG_2))				");
		oSqlBuilder.AppendLine("	)																				");
				
		// sort
		string sSortExpression = "ORDER BY CREATE_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NEWS_TYPE01",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NEWS_TYPE02",PwViCommConst.GameNewsType.NEWS_GET_HONOR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public int GetPageCountFavoriteLog(NewsSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_NEWS			,	");
		oSqlBuilder.AppendLine("	(					");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			T_GAME_FAVORIT				");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) FAVORITE						,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER				,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER					");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= FAVORITE.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= FAVORITE.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= FAVORITE.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.SITE_CD		= T_CAST_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_SEQ		= T_CAST_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_CHAR_NO	= T_CAST_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_NEWS.NEWS_TYPE				!= :NEWS_TYPE						AND	");
		oSqlBuilder.AppendLine("   (T_CAST_CHARACTER.NA_FLAG		IN (:NA_FLAG,:NA_FLAG_2))				");

		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NEWS_TYPE",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		if (pRecCount > 20) {
			pRecCount = 20;
		}

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public int GetPageCountManFavoriteLog(NewsSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_NEWS			,	");
		oSqlBuilder.AppendLine("	(					");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			SITE_CD					,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			T_GAME_FAVORIT				");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("	) FAVORITE						,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER				,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER				");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= FAVORITE.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= FAVORITE.PARTNER_USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= FAVORITE.PARTNER_USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.SITE_CD		= T_USER_MAN_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.USER_CHAR_NO	= T_USER_MAN_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("	T_NEWS.NEWS_TYPE				NOT IN (:NEWS_TYPE01,:NEWS_TYPE02)					AND	");
		oSqlBuilder.AppendLine("   (T_USER_MAN_CHARACTER.NA_FLAG	IN (:NA_FLAG,:NA_FLAG_2))					");

		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NEWS_TYPE01",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NEWS_TYPE02",PwViCommConst.GameNewsType.NEWS_GET_HONOR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		if (pRecCount > 20) {
			pRecCount = 20;
		}

		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}


	public DataSet GetPageCollectionFavoriteLog(NewsSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	USER_SEQ			AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("	NEWS_TYPE										,	");
		oSqlBuilder.AppendLine("	NEWS_ITEM_NM									,	");
		oSqlBuilder.AppendLine("	CREATE_DATE										,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM									,	");
		oSqlBuilder.AppendLine("	HANDLE_NM											");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ							,	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_ITEM_NM						,	");
		oSqlBuilder.AppendLine("			T_NEWS.CREATE_DATE						,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.GAME_HANDLE_NM			,	");
		oSqlBuilder.AppendLine("			T_CAST_CHARACTER.HANDLE_NM					");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_NEWS									,	");
		oSqlBuilder.AppendLine("			(											");
		oSqlBuilder.AppendLine("				SELECT									");
		oSqlBuilder.AppendLine("					SITE_CD							,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ				,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO				");
		oSqlBuilder.AppendLine("				FROM									");
		oSqlBuilder.AppendLine("					T_GAME_FAVORIT						");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("			) FAVORITE								,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER						,	");
		oSqlBuilder.AppendLine("			T_CAST_CHARACTER							");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= FAVORITE.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= FAVORITE.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= FAVORITE.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.SITE_CD		= T_CAST_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_SEQ		= T_CAST_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_CHAR_NO	= T_CAST_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE				!= :NEWS_TYPE						AND	");
		oSqlBuilder.AppendLine("		   (T_CAST_CHARACTER.NA_FLAG		IN (:NA_FLAG,:NA_FLAG_2))				");
		oSqlBuilder.AppendLine("	)																				");

		// sort
		string sSortExpression = "ORDER BY CREATE_DATE DESC NULLS LAST";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NEWS_TYPE",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetPageCollectionManFavoriteLog(NewsSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	USER_SEQ			AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("	NEWS_TYPE										,	");
		oSqlBuilder.AppendLine("	NEWS_ITEM_NM									,	");
		oSqlBuilder.AppendLine("	CREATE_DATE										,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ							,	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE						,	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_ITEM_NM						,	");
		oSqlBuilder.AppendLine("			T_NEWS.CREATE_DATE						,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.GAME_HANDLE_NM				");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_NEWS									,	");
		oSqlBuilder.AppendLine("			(											");
		oSqlBuilder.AppendLine("				SELECT									");
		oSqlBuilder.AppendLine("					SITE_CD							,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_SEQ				,	");
		oSqlBuilder.AppendLine("					PARTNER_USER_CHAR_NO				");
		oSqlBuilder.AppendLine("				FROM									");
		oSqlBuilder.AppendLine("					T_GAME_FAVORIT						");

		// where
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		oSqlBuilder.AppendLine("			) FAVORITE								,	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER						,	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER						");
		oSqlBuilder.AppendLine("		WHERE											");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= FAVORITE.SITE_CD					AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= FAVORITE.PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= FAVORITE.PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_NEWS.SITE_CD					= T_GAME_CHARACTER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_SEQ					= T_GAME_CHARACTER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			T_NEWS.USER_CHAR_NO				= T_GAME_CHARACTER.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.SITE_CD		= T_USER_MAN_CHARACTER.SITE_CD		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.USER_CHAR_NO	= T_USER_MAN_CHARACTER.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_FLAG			= :TUTORIAL_MISSION_FLAG			AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_MISSION_TREASURE_FLAG	= :TUTORIAL_MISSION_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_FLAG			= :TUTORIAL_BATTLE_FLAG				AND	");
		oSqlBuilder.AppendLine("			T_GAME_CHARACTER.TUTORIAL_BATTLE_TREASURE_FLAG	= :TUTORIAL_BATTLE_TREASURE_FLAG	AND	");
		oSqlBuilder.AppendLine("			T_NEWS.NEWS_TYPE				NOT IN (:NEWS_TYPE01,:NEWS_TYPE02)					AND	");
		oSqlBuilder.AppendLine("		   (T_USER_MAN_CHARACTER.NA_FLAG	IN (:NA_FLAG,:NA_FLAG_2))				");
		oSqlBuilder.AppendLine("	)																				");

		// sort
		string sSortExpression = "ORDER BY CREATE_DATE DESC NULLS LAST";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":NEWS_TYPE01",PwViCommConst.GameNewsType.NEWS_TREASURE_LOSS));
		oParamList.Add(new OracleParameter(":NEWS_TYPE02",PwViCommConst.GameNewsType.NEWS_GET_HONOR));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(NewsSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!String.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!String.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!String.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		return oParamList.ToArray();
	}
}