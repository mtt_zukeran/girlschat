﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: アイドルテレビ・会員つぶやきイイネ
--	Progaram ID		: ManTweetLike
--
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class ManTweetLikeSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public string ManTweetSeq {
		get {
			return this.Query["tweetseq"];
		}
		set {
			this.Query["tweetseq"] = value;
		}
	}

	public ManTweetLikeSeekCondition()
		: this(new NameValueCollection()) {
	}

	public ManTweetLikeSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["tweetseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tweetseq"]));
	}
}

#endregion ============================================================================================================

public class ManTweetLike:CastBase {
	public ManTweetLike()
		: base() {
	}
	public ManTweetLike(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(ManTweetLikeSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_MAN_TWEET_LIKE00 P		");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(ManTweetLikeSeekCondition pCondtion,int pPageNo,int pRecPerPage) {

		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "					,	");
		oSqlBuilder.AppendLine("	P.MAN_TWEET_USER_SEQ						,	");
		oSqlBuilder.AppendLine("	P.MAN_TWEET_USER_CHAR_NO						");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_MAN_TWEET_LIKE00 P							");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.MAN_TWEET_LIKE_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(ManTweetLikeSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ManTweetSeq)) {
			SysPrograms.SqlAppendWhere(" P.MAN_TWEET_SEQ		= :MAN_TWEET_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAN_TWEET_SEQ",pCondition.ManTweetSeq));
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope);
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			ArrayList oTmpList = new ArrayList();
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			pWhereClause += SetFlexibleQuery(oFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref oTmpList);
			foreach (OracleParameter oParam in oTmpList) {
				oParamList.Add(oParam);
			}
		}

		return oParamList.ToArray();
	}

	private FlexibleQueryTargetInfo CreateFlexibleQueryTarget(FlexibleQueryType pFlexibleQueryType) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(pFlexibleQueryType);
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			oFlexibleQueryTarget.JealousyDiary = true;
		}
		return oFlexibleQueryTarget;
	}
}