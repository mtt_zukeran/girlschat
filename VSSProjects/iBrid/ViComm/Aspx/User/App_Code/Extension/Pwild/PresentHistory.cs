﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ プレゼント履歴

--	Progaram ID		: PresentHistory
--
--  Creation Date	: 2014.10.09
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class PresentHistory:DbSession {
	public PresentHistory() {
	}

	public int GetTotalPriceDaily(string pSiteCd,string pUserSeq,string pUserCharNo,DateTime pFrom,DateTime pTo) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		int iValue = 0;

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	NVL(SUM(GI.PRICE),0)  AS TOTAL_PRICE			");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_PRESENT_HISTORY PH						,	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM GI									");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	PH.SITE_CD			= GI.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	PH.GAME_ITEM_SEQ	= GI.GAME_ITEM_SEQ		AND	");
		oSqlBuilder.AppendLine("	PH.SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	PH.USER_SEQ			= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	PH.USER_CHAR_NO		= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	PH.CREATE_DATE		>= :CREATE_DATE_FROM	AND	");
		oSqlBuilder.AppendLine("	PH.CREATE_DATE		<= :CREATE_DATE_TO		AND	");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_GET_CD	= :GAME_ITEM_GET_CD			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":CREATE_DATE_FROM",pFrom));
		oParamList.Add(new OracleParameter(":CREATE_DATE_TO",pTo));
		oParamList.Add(new OracleParameter(":GAME_ITEM_GET_CD",PwViCommConst.GameItemGetCd.CHARGE));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			int.TryParse(oDataSet.Tables[0].Rows[0]["TOTAL_PRICE"].ToString(),out iValue);
		}

		return iValue;
	}
}
