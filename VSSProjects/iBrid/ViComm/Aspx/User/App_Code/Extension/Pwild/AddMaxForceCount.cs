﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ 部隊配置
--	Progaram ID		: AddMaxForceCount
--
--  Creation Date	: 2011.09.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class AddMaxForceCount:DbSession {
	public AddMaxForceCount() {
	}

	public void ExecuteAddMaxForceCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		int pAddCount,
		int pMskForce,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADD_MAX_FORCE_COUNT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pADD_COUNT",DbSession.DbType.VARCHAR2,pAddCount);
			db.ProcedureInParm("pMSK_FORCE",DbSession.DbType.VARCHAR2,pMskForce);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}
}

