﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: アイドルテレビ・アイドルつぶやき
--	Progaram ID		: CastTweet
--
--  Creation Date	: 2013.01.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class CastTweetSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string UserSeq {
		get {
			return this.Query["userseq"];
		}
		set {
			this.Query["userseq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["usercharno"];
		}
		set {
			this.Query["usercharno"] = value;
		}
	}
	
	public string CastTweetSeq {
		get {
			return this.Query["tweetseq"];
		}
		set {
			this.Query["tweetseq"] = value;
		}
	}
	
	public string OnlyFanClubFlag;
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public CastTweetSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastTweetSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["userseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["userseq"]));
		this.Query["usercharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["usercharno"]));
		this.Query["tweetseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tweetseq"]));
	}
}

#endregion ============================================================================================================

public class CastTweet:CastBase {
	public CastTweet()
		: base() {
	}
	public CastTweet(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastTweetSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_CAST_TWEET00 P			");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastTweetSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "					,	");
		oSqlBuilder.AppendLine("	P.CAST_TWEET_SEQ							,	");
		oSqlBuilder.AppendLine("	P.TWEET_TEXT								,	");
		oSqlBuilder.AppendLine("	P.TWEET_DATE								,	");
		oSqlBuilder.AppendLine("	0 AS LIKE_COUNT								,	");
		oSqlBuilder.AppendLine("	0 AS COMMENT_COUNT							,	");
		oSqlBuilder.AppendLine("	P.CAST_TWEET_PIC_SEQ						,	");
		oSqlBuilder.AppendLine("	P.CAST_TWEET_IMG_PATH						,	");
		oSqlBuilder.AppendLine("	P.CAST_TWEET_SMALL_IMG_PATH						");
		oSqlBuilder.AppendLine(" FROM												");
		oSqlBuilder.AppendLine("	VW_CAST_TWEET00 P								");

		// where

		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = " ORDER BY P.TWEET_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);
		
		AppendCommentCount(oDataSet);
		AppendLikeCount(oDataSet);

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastTweetSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}
		
		if (!string.IsNullOrEmpty(pCondition.CastTweetSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_TWEET_SEQ		= :CAST_TWEET_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_TWEET_SEQ",pCondition.CastTweetSeq));
		}
		
		if (pCondition.OnlyFanClubFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			string sSelfUserSeq = string.Empty;
			string sSelfUserCharNo = string.Empty;

			if (this.sessionObj != null) {
				if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
					SessionMan oSessionMan = (SessionMan)this.sessionObj;
					sSelfUserSeq = oSessionMan.userMan.userSeq;
					sSelfUserCharNo = oSessionMan.userMan.userCharNo;
				}
			}

			if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" (																");
				oSubQueryBuilder.AppendLine("	EXISTS (													");
				oSubQueryBuilder.AppendLine("		SELECT													");
				oSubQueryBuilder.AppendLine("			1													");
				oSubQueryBuilder.AppendLine("		FROM													");
				oSubQueryBuilder.AppendLine("			T_USER_MAN_CHARACTER_EX								");
				oSubQueryBuilder.AppendLine("		WHERE													");
				oSubQueryBuilder.AppendLine("			SITE_CD					= P.SITE_CD				AND	");
				oSubQueryBuilder.AppendLine("			USER_SEQ				= :SELF_USER_SEQ		AND	");
				oSubQueryBuilder.AppendLine("			USER_CHAR_NO			= :SELF_USER_CHAR_NO	AND	");
				oSubQueryBuilder.AppendLine("			SP_PREMIUM_END_DATE		> SYSDATE					");
				oSubQueryBuilder.AppendLine("	)														OR	");
				oSubQueryBuilder.AppendLine("	EXISTS (													");
				oSubQueryBuilder.AppendLine("		SELECT													");
				oSubQueryBuilder.AppendLine("			1													");
				oSubQueryBuilder.AppendLine("		FROM													");
				oSubQueryBuilder.AppendLine("			T_FANCLUB_STATUS									");
				oSubQueryBuilder.AppendLine("		WHERE													");
				oSubQueryBuilder.AppendLine("			SITE_CD					= P.SITE_CD				AND	");
				oSubQueryBuilder.AppendLine("			MAN_USER_SEQ			= :SELF_USER_SEQ		AND	");
				oSubQueryBuilder.AppendLine("			CAST_USER_SEQ			= P.USER_SEQ			AND	");
				oSubQueryBuilder.AppendLine("			CAST_CHAR_NO			= P.USER_CHAR_NO		AND	");
				oSubQueryBuilder.AppendLine("			RESIGN_FLAG				= :RESIGN_FLAG			AND	");
				oSubQueryBuilder.AppendLine("			EXPIRE_DATE				> SYSDATE				AND	");
				oSubQueryBuilder.AppendLine("			MEMBER_RANK				>= 3						");
				oSubQueryBuilder.AppendLine("	 )															");
				oSubQueryBuilder.AppendLine(" )																");
				SysPrograms.SqlAppendWhere(oSubQueryBuilder.ToString(),ref pWhereClause);
				oParamList.Add(new OracleParameter(":RESIGN_FLAG",ViCommConst.FLAG_OFF_STR));
			}
		}

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(" P.ADMIN_DEL_FLAG		= :ADMIN_DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":ADMIN_DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}
	
	private void AppendCommentCount(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}
		
		foreach(DataRow dr in pDS.Tables[0].Rows) {
			CastTweetCommentSeekCondition oCondition = new CastTweetCommentSeekCondition();
			oCondition.SiteCd = dr["SITE_CD"].ToString();
			oCondition.CastTweetSeq = dr["CAST_TWEET_SEQ"].ToString();
			decimal dRecCount = 0;
			
			using (CastTweetComment oCastTweetComment = new CastTweetComment(sessionObj)) {
				oCastTweetComment.GetPageCount(oCondition,1,out dRecCount);
			}

			dr["COMMENT_COUNT"] = dRecCount.ToString();
		}
	}

	private void AppendLikeCount(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			CastTweetLikeSeekCondition oCondition = new CastTweetLikeSeekCondition();
			oCondition.SiteCd = dr["SITE_CD"].ToString();
			oCondition.CastTweetSeq = dr["CAST_TWEET_SEQ"].ToString();
			decimal dRecCount = 0;

			using (CastTweetLike oCastTweetLike = new CastTweetLike(sessionObj)) {
				oCastTweetLike.GetPageCount(oCondition,1,out dRecCount);
			}

			dr["LIKE_COUNT"] = dRecCount.ToString();
		}
	}

	public void WriteCastTweet(string pSiteCd,string pUserSeq,string pUserCharNo,string pTweetText,out string pCastTweetSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_CAST_TWEET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pTWEET_TEXT",DbSession.DbType.VARCHAR2,pTweetText);
			db.ProcedureOutParm("pCAST_TWEET_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pCastTweetSeq = db.GetStringValue("pCAST_TWEET_SEQ");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void RegistCastTweetLike(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastTweetSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_CAST_TWEET_LIKE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_TWEET_SEQ",DbSession.DbType.VARCHAR2,pCastTweetSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void DeleteCastTweet(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastTweetSeq,out string pResult) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_CAST_TWEET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_TWEET_SEQ",DbSession.DbType.VARCHAR2,pCastTweetSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}