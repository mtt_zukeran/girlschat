﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 野球拳画像
--	Progaram ID		: YakyukenPic
--
--  Creation Date	: 2013.04.30
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class YakyukenPicSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string CastUserSeq;
	public string CastCharNo;
	public string MaxYakyukenType;

	public string YakyukenPicSeq {
		get {
			return this.Query["ypicseq"];
		}
		set {
			this.Query["ypicseq"] = value;
		}
	}

	public string YakyukenGameSeq {
		get {
			return this.Query["ygameseq"];
		}
		set {
			this.Query["ygameseq"] = value;
		}
	}

	public string YakyukenType {
		get {
			return this.Query["ytype"];
		}
		set {
			this.Query["ytype"] = value;
		}
	}

	public YakyukenPicSeekCondition()
		: this(new NameValueCollection()) {
	}

	public YakyukenPicSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["ypicseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ypicseq"]));
		this.Query["ygameseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ygameseq"]));
		this.Query["ytype"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ytype"]));
	}
}

public class YakyukenPic:DbSession {
	public YakyukenPic() {
	}

	public DataSet GetPageCollection(YakyukenPicSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	YAKYUKEN_PIC_SEQ,");
		oSqlBuilder.AppendLine("	YAKYUKEN_GAME_SEQ,");
		oSqlBuilder.AppendLine("	YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	COMMENT_TEXT,");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	AUTH_FLAG,");
		oSqlBuilder.AppendLine("	SHOW_FACE_FLAG,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	AUTH_DATE,");
		oSqlBuilder.AppendLine("	VIEW_COUNT,");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	LAST_WIN_YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	CLEAR_YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	CLEAR_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC01");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY SITE_CD,CAST_USER_SEQ,CAST_CHAR_NO,YAKYUKEN_GAME_SEQ,YAKYUKEN_TYPE ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetList(YakyukenPicSeekCondition pCondtion) {
		DataSet ds = CreateInitData(pCondtion);
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	YAKYUKEN_PIC_SEQ,");
		oSqlBuilder.AppendLine("	YAKYUKEN_GAME_SEQ,");
		oSqlBuilder.AppendLine("	YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	COMMENT_TEXT,");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	AUTH_FLAG,");
		oSqlBuilder.AppendLine("	SHOW_FACE_FLAG,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	AUTH_DATE,");
		oSqlBuilder.AppendLine("	VIEW_COUNT,");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC01");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet dsTmp = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		foreach (DataRow drTmp in dsTmp.Tables[0].Rows) {
			DataRow dr = ds.Tables[0].Rows.Find(drTmp["YAKYUKEN_TYPE"]);
			dr["SITE_CD"] = drTmp["SITE_CD"];
			dr["CAST_USER_SEQ"] = drTmp["CAST_USER_SEQ"];
			dr["CAST_CHAR_NO"] = drTmp["CAST_CHAR_NO"];
			dr["YAKYUKEN_PIC_SEQ"] = drTmp["YAKYUKEN_PIC_SEQ"];
			dr["YAKYUKEN_GAME_SEQ"] = drTmp["YAKYUKEN_GAME_SEQ"];
			dr["COMMENT_TEXT"] = drTmp["COMMENT_TEXT"];
			dr["PIC_SEQ"] = drTmp["PIC_SEQ"];
			dr["AUTH_FLAG"] = drTmp["AUTH_FLAG"];
			dr["SHOW_FACE_FLAG"] = drTmp["SHOW_FACE_FLAG"];
			dr["CREATE_DATE"] = drTmp["CREATE_DATE"];
			dr["AUTH_DATE"] = drTmp["AUTH_DATE"];
			dr["VIEW_COUNT"] = drTmp["VIEW_COUNT"];
			dr["PHOTO_IMG_PATH"] = drTmp["PHOTO_IMG_PATH"];
			dr["SMALL_PHOTO_IMG_PATH"] = drTmp["SMALL_PHOTO_IMG_PATH"];
		}

		return ds;
	}

	private DataSet CreateInitData(YakyukenPicSeekCondition pCondtion) {
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();

		dt.Columns.Add("SITE_CD",Type.GetType("System.String"));
		dt.Columns.Add("CAST_USER_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("CAST_CHAR_NO",Type.GetType("System.String"));
		dt.Columns.Add("YAKYUKEN_PIC_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("YAKYUKEN_GAME_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("YAKYUKEN_TYPE",Type.GetType("System.String"));
		dt.Columns.Add("COMMENT_TEXT",Type.GetType("System.String"));
		dt.Columns.Add("PIC_SEQ",Type.GetType("System.String"));
		dt.Columns.Add("AUTH_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("SHOW_FACE_FLAG",Type.GetType("System.String"));
		dt.Columns.Add("CREATE_DATE",Type.GetType("System.String"));
		dt.Columns.Add("AUTH_DATE",Type.GetType("System.String"));
		dt.Columns.Add("VIEW_COUNT",Type.GetType("System.String"));
		dt.Columns.Add("PHOTO_IMG_PATH",Type.GetType("System.String"));
		dt.Columns.Add("SMALL_PHOTO_IMG_PATH",Type.GetType("System.String"));
		ds.Tables.Add(dt);

		for (int i = 1;i <= 6;i++) {
			DataRow dr = ds.Tables[0].Rows.Add();
			dr["YAKYUKEN_TYPE"] = i.ToString();
			dr["SITE_CD"] = pCondtion.SiteCd;
			dr["CAST_USER_SEQ"] = pCondtion.CastUserSeq;
			dr["CAST_CHAR_NO"] = pCondtion.CastCharNo;
			dr["YAKYUKEN_GAME_SEQ"] = pCondtion.YakyukenGameSeq;
		}

		ds.Tables[0].PrimaryKey = new DataColumn[] { dt.Columns["YAKYUKEN_TYPE"] };

		return ds;
	}

	private OracleParameter[] CreateWhere(YakyukenPicSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenPicSeq)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_PIC_SEQ = :YAKYUKEN_PIC_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_PIC_SEQ",pCondition.YakyukenPicSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenGameSeq)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_GAME_SEQ = :YAKYUKEN_GAME_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_GAME_SEQ",pCondition.YakyukenGameSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenType)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_TYPE = :YAKYUKEN_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_TYPE",pCondition.YakyukenType));
		}

		if (!string.IsNullOrEmpty(pCondition.MaxYakyukenType)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_TYPE <= :MAX_YAKYUKEN_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAX_YAKYUKEN_TYPE",pCondition.MaxYakyukenType));
		}

		return oParamList.ToArray();
	}

	public void RegistYakyukenPicTmp(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		string pYakyukenGameSeq,
		string pYakyukenType,
		string pCommentText,
		out string pYakyukenPicTmpSeq,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_YAKYUKEN_PIC_TMP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pYAKYUKEN_GAME_SEQ",DbSession.DbType.VARCHAR2,pYakyukenGameSeq);
			db.ProcedureInParm("pYAKYUKEN_TYPE",DbSession.DbType.VARCHAR2,pYakyukenType);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureOutParm("pYAKYUKEN_PIC_TMP_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pYakyukenPicTmpSeq = db.GetStringValue("pYAKYUKEN_PIC_TMP_SEQ");
			pResult = db.GetStringValue("pRESULT");
		}
	}

	public void ModifyYakyukenPic(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		string pYakyukenPicSeq,
		string pCommentText,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_YAKYUKEN_PIC");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("pYAKYUKEN_PIC_SEQ",DbSession.DbType.VARCHAR2,pYakyukenPicSeq);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}		
	}
}
