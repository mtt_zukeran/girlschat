﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using ViComm;
using Oracle.DataAccess.Client;
using ViComm.Extension.Pwild;

[Serializable]
public class PossessionManTreasureSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public string SelfUserSeq {
		get {
			return this.Query["self_user_seq"];
		}
		set {
			this.Query["self_user_seq"] = value;
		}
	}

	public string SelfUserCharNo {
		get {
			return this.Query["self_user_char_no"];
		}
		set {
			this.Query["self_user_char_no"] = value;
		}
	}

	public string CastGamePicSeq {
		get {
			return this.Query["cast_game_pic_seq"];
		}
		set {
			this.Query["cast_game_pic_seq"] = value;
		}
	}

	public string CastGamePicAttrSeq {
		get {
			return this.Query["cast_game_pic_attr_seq"];
		}
		set {
			this.Query["cast_game_pic_attr_seq"] = value;
		}
	}
	
	public string DisplayDay {
		get {
			return this.Query["display_day"];
		}
		set {
			this.Query["display_day"] = value;
		}
	}
	
	public string TutorialBattleFlag {
		get {
			return this.Query["tutorial_battle_flag"];
		}
		set {
			this.Query["tutorial_battle_flag"] = value;
		}
	}
	
	public string TreasureTutorialBattleFlag {
		get {
			return this.Query["treasure_tutorial_battle_flag"];
		}
		set {
			this.Query["treasure_tutorial_battle_flag"] = value;
		}
	}
	
	public string TresaureTutorialMissionFlag {
		get {
			return this.Query["treasure_tutorial_mission_flag"];
		}
		set {
			this.Query["treasure_tutorial_mission_flag"] = value;
		}
	}

	public string ExceptSelfFlag {
		get {
			return this.Query["except_self_flag"];
		}
		set {
			this.Query["except_self_flag"] = value;
		}
	}
	
	public string ContainTutorialTreasureFlag {
		get {
			return this.Query["contain_tutorial_treasure_flag"];
		}
		set {
			this.Query["contain_tutorial_treasure_flag"] = value;
		}
	}
	
	public string ExceptBattledFlag {
		get {
			return this.Query["except_battled_flag"];
		}
		set {
			this.Query["except_battled_flag"] = value;
		}
	}

	public string CanRobFlag;
	public string TutorialFlag;
	public string MaxPowerFrom;
	public string MaxPowerTo;

	public PossessionManTreasureSeekCondition() : this(new NameValueCollection()) {
	}

	public PossessionManTreasureSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["self_user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_seq"]));
		this.Query["self_user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["self_user_char_no"]));
		this.Query["cast_game_pic_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_game_pic_seq"]));
		this.Query["cast_game_pic_attr_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_game_pic_attr_seq"]));
		this.Query["display_day"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["display_day"]));
		this.Query["tutorial_battle_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["tutorial_battle_flag"]));
		this.Query["treasure_tutorial_battle_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["treasure_tutorial_battle_flag"]));
		this.Query["treasure_tutorial_mission_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["treasure_tutorial_mission_flag"]));
		this.Query["except_self_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["except_self_flag"]));
		this.Query["contain_tutorial_treasure_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["contain_tutorial_treasure_flag"]));
		this.Query["except_battled_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["except_battled_flag"]));
	}
}

[System.Serializable]
public class PossessionManTreasure:DbSession {
	public PossessionManTreasure() {
	}

	public int GetPageCount(PossessionManTreasureSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine(" FROM					");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_MAN_TRSR01 P ");

		// where
		string sWhereClause = string.Empty;

		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(PossessionManTreasureSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase(pCondition,pPageNo,pRecPerPage);
	}

	public DataSet GetPageCollectionBase(PossessionManTreasureSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();		
		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	P.PIC_TITLE													,	");
		oSqlBuilder.AppendLine("	P.PIC_DOC													,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_SEQ											,	");
		oSqlBuilder.AppendLine("	P.OBJ_SMALL_PHOTO_IMG_PATH									,	");
		oSqlBuilder.AppendLine("	P.OBJ_SMALL_BLUR_PHOTO_IMG_PATH								,	");
		oSqlBuilder.AppendLine("	P.CAST_USER_SEQ AS USER_SEQ									,	");
		oSqlBuilder.AppendLine("	P.CAST_CHAR_NO AS USER_CHAR_NO								,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	P.HANDLE_NM													,	");
		oSqlBuilder.AppendLine("	P.AGE														,	");
		oSqlBuilder.AppendLine("	P.SMALL_PHOTO_IMG_PATH										,	");
		oSqlBuilder.AppendLine("	P.CAST_GAME_PIC_ATTR_NM										,	");
		oSqlBuilder.AppendLine("	P.CAST_NA_FLAG												,	");
		oSqlBuilder.AppendLine("	P.DISPLAY_DAY												,	");
		oSqlBuilder.AppendLine("	P.ACT_CATEGORY_IDX											,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID													,	");
		oSqlBuilder.AppendLine("	P.CRYPT_VALUE												,	");
		oSqlBuilder.AppendLine("	P.USER_SEQ AS PARTNER_USER_SEQ								,	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO AS PARTNER_USER_CHAR_NO						,	");
		oSqlBuilder.AppendLine("	P.GAME_HANDLE_NM AS PARTNER_GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_LEVEL										,	");
		oSqlBuilder.AppendLine("	P.GAME_CHARACTER_TYPE										,	");
		oSqlBuilder.AppendLine("	P.FELLOW_COUNT												,	");
		oSqlBuilder.AppendLine("	P.MAX_ATTACK_FORCE_COUNT									,	");
		oSqlBuilder.AppendLine("	P.PRIORITY													,	");
		oSqlBuilder.AppendLine("	NVL(PMT.MOSAIC_ERASE_FLAG,0) AS MOSAIC_ERASE_FLAG			,	");
		oSqlBuilder.AppendLine("	NVL(PMT.POSSESSION_COUNT,0) AS POSSESSION_COUNT					");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_MAN_TRSR01	P								,	");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			CAST_GAME_PIC_SEQ									,	");
		oSqlBuilder.AppendLine("			MOSAIC_ERASE_FLAG									,	");
		oSqlBuilder.AppendLine("			POSSESSION_COUNT										");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_POSSESSION_MAN_TREASURE								");
		oSqlBuilder.AppendLine("		WHERE														");
		oSqlBuilder.AppendLine("			USER_SEQ		= :SELF_USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :SELF_USER_CHAR_NO					");
		oSqlBuilder.AppendLine("	) PMT															");
		
		// where
		SysPrograms.SqlAppendWhere(" P.SITE_CD				= PMT.SITE_CD				(+)",ref sWhereClause);
		SysPrograms.SqlAppendWhere(" P.CAST_GAME_PIC_SEQ	= PMT.CAST_GAME_PIC_SEQ		(+)",ref sWhereClause);

		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		return oDataSet;
	}

	public DataSet GetOne(PossessionManTreasureSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ							");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	(											");
		oSqlBuilder.AppendLine("		SELECT									");
		oSqlBuilder.AppendLine("			CAST_GAME_PIC_SEQ					");
		oSqlBuilder.AppendLine("		FROM									");
		oSqlBuilder.AppendLine("			VW_PW_POSSESSION_MAN_TRSR01	P		");
		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	)											");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	ROWNUM = 1									");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(PossessionManTreasureSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}
		
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));


		if (pCondition.ExceptSelfFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" NOT(P.USER_SEQ = :SELF_USER_SEQ AND P.USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO		= :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!String.IsNullOrEmpty(pCondition.CanRobFlag)) {
			SysPrograms.SqlAppendWhere(" P.CAN_ROB_COUNT		> 0",ref pWhereClause);
		}
		
		if(!string.IsNullOrEmpty(pCondition.CastGamePicSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_GAME_PIC_SEQ		= :CAST_GAME_PIC_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCondition.CastGamePicSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastGamePicAttrSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_GAME_PIC_ATTR_SEQ		= :CAST_GAME_PIC_ATTR_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_GAME_PIC_ATTR_SEQ",pCondition.CastGamePicAttrSeq));
		}
		
		if(!string.IsNullOrEmpty(pCondition.DisplayDay)) {
			SysPrograms.SqlAppendWhere(" P.DISPLAY_DAY		= :DISPLAY_DAY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DISPLAY_DAY",pCondition.DisplayDay));
		}

		SysPrograms.SqlAppendWhere(" P.STAFF_FLAG		= :STAFF_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":STAFF_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.PARTNER_NA_FLAG		IN(:NA_FLAG,:NA_FLAG2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG2",ViCommConst.NaFlag.NO_AUTH));
		
		if(!string.IsNullOrEmpty(pCondition.TutorialBattleFlag)) {
			SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG		= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",pCondition.TutorialBattleFlag));
		} else {
			SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_FLAG		= :TUTORIAL_BATTLE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
		}

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_FLAG		= :TUTORIAL_MISSION_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_MISSION_TREASURE_FLAG		= :TUTORIAL_MISSION_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_MISSION_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.TUTORIAL_BATTLE_TREASURE_FLAG		= :TUTORIAL_BATTLE_TREASURE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_TREASURE_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.PUBLISH_FLAG		= :PUBLISH_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));

		if (!string.IsNullOrEmpty(pCondition.TreasureTutorialBattleFlag)) {
			SysPrograms.SqlAppendWhere(" P.TREASURE_TUTORIAL_BATTLE_FLAG		= :TREASURE_TUTORIAL_BATTLE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TREASURE_TUTORIAL_BATTLE_FLAG",pCondition.TreasureTutorialBattleFlag));
		} else {
			if(string.IsNullOrEmpty(pCondition.ContainTutorialTreasureFlag) || !pCondition.ContainTutorialTreasureFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere(" P.TREASURE_TUTORIAL_BATTLE_FLAG		= :TREASURE_TUTORIAL_BATTLE_FLAG",ref pWhereClause);
				oParamList.Add(new OracleParameter(":TREASURE_TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_OFF_STR));
			}
		}
		
		if (!string.IsNullOrEmpty(pCondition.TresaureTutorialMissionFlag)) {
			SysPrograms.SqlAppendWhere(" P.TREASURE_TUTORIAL_MISSION_FLAG		= :TREASURE_TUTORIAL_MISSION_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TREASURE_TUTORIAL_MISSION_FLAG",pCondition.TresaureTutorialMissionFlag));
		}

		if (pCondition.ExceptBattledFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("NOT EXISTS (															");
			oSqlBuilder.AppendLine("	SELECT																");
			oSqlBuilder.AppendLine("		*																");
			oSqlBuilder.AppendLine("	FROM																");
			oSqlBuilder.AppendLine("		VW_PW_DAILY_BATTLE_LOG01										");
			oSqlBuilder.AppendLine("	WHERE																");
			oSqlBuilder.AppendLine("		SITE_CD					= P.SITE_CD							AND	");
			oSqlBuilder.AppendLine("		USER_SEQ				= :SELF_USER_SEQ					AND	");
			oSqlBuilder.AppendLine("		USER_CHAR_NO			= :SELF_USER_CHAR_NO				AND	");
			oSqlBuilder.AppendLine("		PARTNER_USER_SEQ		= P.USER_SEQ						AND	");
			oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO					AND	");
			oSqlBuilder.AppendLine("		REPORT_DAY				= :REPORT_DAY							");
			oSqlBuilder.AppendLine(")																		");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));
			oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));
			string sDate = DateTime.Now.ToString("yyyy/MM/dd");
			oParamList.Add(new OracleParameter(":REPORT_DAY",sDate));
		}

		if (!string.IsNullOrEmpty(pCondition.MaxPowerFrom)) {
			SysPrograms.SqlAppendWhere("(P.MAX_ATTACK_POWER + P.MAX_DEFENCE_POWER) >= :MAX_POWER_FROM",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAX_POWER_FROM",pCondition.MaxPowerFrom));
		}

		if (!string.IsNullOrEmpty(pCondition.MaxPowerTo)) {
			SysPrograms.SqlAppendWhere("(P.MAX_ATTACK_POWER + P.MAX_DEFENCE_POWER) <= :MAX_POWER_TO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAX_POWER_TO",pCondition.MaxPowerTo));
		}

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_FAVORIT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.CAST_USER_SEQ AND USER_CHAR_NO = P.CAST_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO AND GAME_PIC_NON_PUBLISH_DATE < P.UPLOAD_DATE)",ref pWhereClause);

		SysPrograms.SqlAppendWhere(
			"(EXISTS(SELECT * FROM T_POSSESSION_MAN_TREASURE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND CAST_GAME_PIC_SEQ = P.CAST_GAME_PIC_SEQ) OR " +
			"(NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.CAST_USER_SEQ AND USER_CHAR_NO = P.CAST_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO) AND " +
			"NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = :SELF_USER_SEQ AND USER_CHAR_NO = :SELF_USER_CHAR_NO AND PARTNER_USER_SEQ = P.CAST_USER_SEQ AND PARTNER_USER_CHAR_NO = P.CAST_CHAR_NO)))",
			ref pWhereClause
		);

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(PossessionManTreasureSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case PwViCommConst.ManTreasureGetSort.UPDATE_DATE_ASC:
				sSortExpression = "ORDER BY UPDATE_DATE ASC";
				break;
			case PwViCommConst.ManTreasureGetSort.UPDATE_DATE_DESC:
				sSortExpression = "ORDER BY UPDATE_DATE DESC";
				break;
			default:
				sSortExpression = "ORDER BY UPDATE_DATE DESC";
				break;
		}
				
		return sSortExpression;
	}

	public int GetPageCount01(PossessionManTreasureSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine(" FROM					");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_MAN_TRSR01 P ");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondition,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection01(PossessionManTreasureSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	PIC_TITLE										,	");
		oSqlBuilder.AppendLine("	PIC_DOC											,	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ								,	");
		oSqlBuilder.AppendLine("	OBJ_SMALL_PHOTO_IMG_PATH						,	");
		oSqlBuilder.AppendLine("	OBJ_SMALL_BLUR_PHOTO_IMG_PATH					,	");
		oSqlBuilder.AppendLine("	HANDLE_NM										,	");
		oSqlBuilder.AppendLine("	AGE												,	");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ				  AS USER_SEQ		,	");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO				  AS USER_CHAR_NO	,	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_NM							,	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY										,	");
		oSqlBuilder.AppendLine("	MOSAIC_ERASE_FLAG								,	");
		oSqlBuilder.AppendLine("	POSSESSION_COUNT								,	");
		oSqlBuilder.AppendLine("	CASE												");
		oSqlBuilder.AppendLine("		WHEN											");
		oSqlBuilder.AppendLine("			TREASURE_TUTORIAL_MISSION_FLAG	= 1		OR	");
		oSqlBuilder.AppendLine("			TREASURE_TUTORIAL_BATTLE_FLAG	= 1			");
		oSqlBuilder.AppendLine("		THEN											");
		oSqlBuilder.AppendLine("			1											");
		oSqlBuilder.AppendLine("		ELSE											");
		oSqlBuilder.AppendLine("			0											");
		oSqlBuilder.AppendLine("	END AS TUTORIAL_FLAG								");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	VW_PW_POSSESSION_MAN_TRSR01	P						");
		// where		
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondition);

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetManTreasureCompStatus(PossessionManTreasureSeekCondition pCondition) {
		
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	MTA.CAST_GAME_PIC_ATTR_SEQ										,	");
		oSqlBuilder.AppendLine("	MTA.CAST_GAME_PIC_ATTR_NM										,	");
		oSqlBuilder.AppendLine("	MTA.PRIORITY													,	");
		oSqlBuilder.AppendLine("	CASE																");
		oSqlBuilder.AppendLine("		WHEN PMT.CAST_GAME_PIC_ATTR_SEQ IS NOT NULL THEN 1				");
		oSqlBuilder.AppendLine("		ELSE 0															");
		oSqlBuilder.AppendLine("	END AS POSSESSION_FLAG											,	");
		oSqlBuilder.AppendLine("	'' AS DISPLAY_DAY													");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_ATTR MTA											,	");
		oSqlBuilder.AppendLine("	(																	");
		oSqlBuilder.AppendLine("		SELECT															");
		oSqlBuilder.AppendLine("			DISTINCT CAST_GAME_PIC_ATTR_SEQ								");
		oSqlBuilder.AppendLine("		FROM															");
		oSqlBuilder.AppendLine("			VW_PW_MAN_TREASURE02										");
		oSqlBuilder.AppendLine("		WHERE															");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("			DISPLAY_DAY		= :DISPLAY_DAY								");
		oSqlBuilder.AppendLine("	) PMT																");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	MTA.SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	MTA.CAST_GAME_PIC_ATTR_SEQ	= PMT.CAST_GAME_PIC_ATTR_SEQ	(+)		");
		oSqlBuilder.AppendLine("ORDER BY PRIORITY														");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",pCondition.DisplayDay));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count > 0) {
			foreach(DataRow oDR in oDataSet.Tables[0].Rows) {
				oDR["DISPLAY_DAY"] = pCondition.DisplayDay;
			}
		}
		
		return oDataSet;
	}
	
	public DataSet GetTutorialBattleTreasureData(PossessionManTreasureSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																	");
		oSqlBuilder.AppendLine("	PMT.USER_SEQ													,	");
		oSqlBuilder.AppendLine("	PMT.USER_CHAR_NO												,	");
		oSqlBuilder.AppendLine("	PMT.CAST_GAME_PIC_SEQ												");
		oSqlBuilder.AppendLine("FROM																	");
		oSqlBuilder.AppendLine("	T_POSSESSION_MAN_TREASURE PMT									,	");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE MT												,	");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER GC													");
		oSqlBuilder.AppendLine("WHERE																	");
		oSqlBuilder.AppendLine("	PMT.SITE_CD					= MT.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	PMT.CAST_GAME_PIC_SEQ		= MT.CAST_GAME_PIC_SEQ				AND	");
		oSqlBuilder.AppendLine("	PMT.SITE_CD					= GC.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	PMT.USER_SEQ				= GC.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	PMT.USER_CHAR_NO			= GC.USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	PMT.SITE_CD					= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	MT.DISPLAY_DAY				= :DISPLAY_DAY						AND	");
		oSqlBuilder.AppendLine("	MT.TUTORIAL_BATTLE_FLAG		= :TREASURE_TUTORIAL_BATTLE_FLAG	AND	");
		oSqlBuilder.AppendLine("	GC.TUTORIAL_BATTLE_FLAG		= :TUTORIAL_BATTLE_FLAG					");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":DISPLAY_DAY",pCondition.DisplayDay));
		oParamList.Add(new OracleParameter(":TREASURE_TUTORIAL_BATTLE_FLAG",pCondition.TreasureTutorialBattleFlag));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",pCondition.TutorialBattleFlag));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
	
	public bool CheckCanUseMosaicErase(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastGamePicSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	1												");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_POSSESSION_MAN_TREASURE						");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ	= :CAST_GAME_PIC_SEQ	AND	");
		oSqlBuilder.AppendLine("	MOSAIC_ERASE_FLAG	= :MOSAIC_ERASE_FLAG		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCastGamePicSeq));
		oParamList.Add(new OracleParameter(":MOSAIC_ERASE_FLAG",ViCommConst.FLAG_OFF_STR));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
