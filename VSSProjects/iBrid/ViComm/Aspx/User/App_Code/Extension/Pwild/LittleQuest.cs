﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・小クエスト
--	Progaram ID		: LittleQuest
--
--  Creation Date	: 2012.07.09
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[System.Serializable]
public class LittleQuestSeekCondition:SeekConditionBase {
	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}

	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}
	
	public string QuestSeq {
		get {
			return this.Query["quest_seq"];
		}
		set {
			this.Query["quest_seq"] = value;
		}
	}

	public string LevelQuestSeq {
		get {
			return this.Query["level_quest_seq"];
		}
		set {
			this.Query["level_quest_seq"] = value;
		}
	}

	public string LittleQuestSeq {
		get {
			return this.Query["little_quest_seq"];
		}
		set {
			this.Query["little_quest_seq"] = value;
		}
	}
	
	public string SexCd {
		get {
			return this.Query["sex_cd"];
		}
		set {
			this.Query["sex_cd"] = value;
		}
	}

	public LittleQuestSeekCondition()
		: this(new NameValueCollection()) {
	}

	public LittleQuestSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_Cd"]));
		this.Query["quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["quest_seq"]));
		this.Query["level_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["level_quest_seq"]));
		this.Query["little_quest_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["little_quest_seq"]));
	}
}

#endregion ============================================================================================================

public class LittleQuest:DbSession {
	public LittleQuest() {
	}

	public DataSet GetPageCollection(LittleQuestSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string pWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ										,	");
		oSqlBuilder.AppendLine("	LQ.LEVEL_QUEST_SEQ									,	");
		oSqlBuilder.AppendLine("	LQ.QUEST_LEVEL										,	");
		oSqlBuilder.AppendLine("	LQ.LEVEL_QUEST_SUB_SEQ								,	");
		oSqlBuilder.AppendLine("	LQ.LITTLE_QUEST_SEQ									,	");
		oSqlBuilder.AppendLine("	LQ.LITTLE_QUEST_NM									,	");
		oSqlBuilder.AppendLine("	LQ.REMARKS											,	");
		oSqlBuilder.AppendLine("	NVL(LQ.TIME_LIMIT_D,0) AS TIME_LIMIT_D				,	");
		oSqlBuilder.AppendLine("	NVL(LQ.TIME_LIMIT_H,0) AS TIME_LIMIT_H				,	");
		oSqlBuilder.AppendLine("	NVL(LQ.TIME_LIMIT_M,0) AS TIME_LIMIT_M				,	");
		oSqlBuilder.AppendLine("	QEL.ENTRY_LOG_SEQ									,	");
		oSqlBuilder.AppendLine("	NVL(QEL.QUEST_STATUS,0) AS QUEST_STATUS				,	");
		oSqlBuilder.AppendLine("	0 AS LAST_ENTRY_OK_FLAG								,	");
		oSqlBuilder.AppendLine("	LQ.QUEST_TYPE										,	");
		oSqlBuilder.AppendLine("	CASE													");
		oSqlBuilder.AppendLine("		WHEN												");
		oSqlBuilder.AppendLine("			EXISTS											");
		oSqlBuilder.AppendLine("			(												");
		oSqlBuilder.AppendLine("				SELECT										");
		oSqlBuilder.AppendLine("					1										");
		oSqlBuilder.AppendLine("				FROM										");
		oSqlBuilder.AppendLine("					T_QUEST_ENTRY_LOG						");
		oSqlBuilder.AppendLine("				WHERE										");
		oSqlBuilder.AppendLine("					SITE_CD			= LQ.SITE_CD		AND	");
		oSqlBuilder.AppendLine("					QUEST_SEQ		= LQ.QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("					LEVEL_QUEST_SEQ	= LQ.LEVEL_QUEST_SEQ	AND	");
		oSqlBuilder.AppendLine("					QUEST_TYPE		= LQ.QUEST_TYPE		AND	");
		oSqlBuilder.AppendLine("					USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("					USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("					QUEST_STATUS	= :QUEST_STATUS_ENTRY	");
		oSqlBuilder.AppendLine("			)												");
		oSqlBuilder.AppendLine("		THEN 1												");
		oSqlBuilder.AppendLine("		ELSE 0												");
		oSqlBuilder.AppendLine("	END AS NOW_ENTRY_FLAG								,	");
		oSqlBuilder.AppendLine("	NULL AS	OTHER_QUEST_SEQ								,	");
		oSqlBuilder.AppendLine("	CASE													");
		oSqlBuilder.AppendLine("		WHEN												");
		oSqlBuilder.AppendLine("			QEL.END_DATE <= LQ.PUBLISH_END_DATE				");
		oSqlBuilder.AppendLine("		THEN												");
		oSqlBuilder.AppendLine("			QEL.END_DATE									");
		oSqlBuilder.AppendLine("		ELSE												");
		oSqlBuilder.AppendLine("			LQ.PUBLISH_END_DATE								");
		oSqlBuilder.AppendLine("	END AS END_DATE											");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			SITE_CD										,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ									,	");
		oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ								,	");
		oSqlBuilder.AppendLine("			QUEST_LEVEL									,	");
		oSqlBuilder.AppendLine("			LEVEL_QUEST_SUB_SEQ							,	");
		oSqlBuilder.AppendLine("			LITTLE_QUEST_SEQ							,	");
		oSqlBuilder.AppendLine("			LITTLE_QUEST_NM								,	");
		oSqlBuilder.AppendLine("			REMARKS										,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_D								,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_H								,	");
		oSqlBuilder.AppendLine("			TIME_LIMIT_M								,	");
		oSqlBuilder.AppendLine("			1 AS QUEST_TYPE								,	");
		oSqlBuilder.AppendLine("			PUBLISH_END_DATE								");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			VW_PW_LITTLE_QUEST01							");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ		= :QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ	= :LEVEL_QUEST_SEQ			AND	");
		if(!string.IsNullOrEmpty(pCondition.LittleQuestSeq)) {
			oSqlBuilder.AppendLine("			LITTLE_QUEST_SEQ	= :LITTLE_QUEST_SEQ		AND	");
		}
		oSqlBuilder.AppendLine("			SEX_CD			= :SEX_CD					AND	");
		oSqlBuilder.AppendLine("			PUBLISH_FLAG	= :PUBLISH_FLAG					");
		oSqlBuilder.AppendLine("	) LQ												,	");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			SITE_CD										,	");
		oSqlBuilder.AppendLine("			QUEST_SEQ									,	");
		oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ								,	");
		oSqlBuilder.AppendLine("			LITTLE_QUEST_SEQ							,	");
		oSqlBuilder.AppendLine("			ENTRY_LOG_SEQ								,	");
		oSqlBuilder.AppendLine("			QUEST_STATUS								,	");
		oSqlBuilder.AppendLine("			END_DATE										");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			T_QUEST_ENTRY_LOG								");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("			USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("			QUEST_SEQ		= :QUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("			LEVEL_QUEST_SEQ	= :LEVEL_QUEST_SEQ			AND	");
		oSqlBuilder.AppendLine("			QUEST_TYPE		= :QUEST_TYPE				AND	");
		oSqlBuilder.AppendLine("			QUEST_STATUS	IN(:QUEST_STATUS_ENTRY,:QUEST_STATUS_CLEAR)");
		oSqlBuilder.AppendLine("	) QEL													");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	LQ.SITE_CD				= QEL.SITE_CD			(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.QUEST_SEQ			= QEL.QUEST_SEQ			(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.LEVEL_QUEST_SEQ		= QEL.LEVEL_QUEST_SEQ	(+)	AND	");
		oSqlBuilder.AppendLine("	LQ.LITTLE_QUEST_SEQ		= QEL.LITTLE_QUEST_SEQ	(+)		");

		sSortExpression = "ORDER BY LEVEL_QUEST_SUB_SEQ ASC";

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
		if(!string.IsNullOrEmpty(pCondition.LittleQuestSeq)) {
			oParamList.Add(new OracleParameter(":LITTLE_QUEST_SEQ",pCondition.LittleQuestSeq));
		}
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG",ViCommConst.FLAG_ON_STR));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":QUEST_TYPE",PwViCommConst.GameQuestType.LITTLE_QUEST));
		oParamList.Add(new OracleParameter(":QUEST_STATUS_ENTRY",PwViCommConst.GameQuestEntryStatus.ENTRY));
		oParamList.Add(new OracleParameter(":QUEST_STATUS_CLEAR",PwViCommConst.GameQuestEntryStatus.CLEAR));

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		AppendLastEntryOK(oDataSet);

		return oDataSet;
	}
	
	private void AppendLastEntryOK(DataSet pDS) {
		if(pDS.Tables[0].Rows.Count == 0) {
			return;
		}
		
		string sLastEntryOkFlag = ViCommConst.FLAG_ON_STR;
		
		foreach(DataRow checkDR in pDS.Tables[0].Rows) {
			if (int.Parse(checkDR["LEVEL_QUEST_SUB_SEQ"].ToString()) < pDS.Tables[0].Rows.Count) {
				if (!checkDR["QUEST_STATUS"].ToString().Equals(PwViCommConst.GameQuestEntryStatus.CLEAR)) {
					sLastEntryOkFlag = ViCommConst.FLAG_OFF_STR;
				}
			}
		}
		
		foreach(DataRow updateDR in pDS.Tables[0].Rows) {
			updateDR["LAST_ENTRY_OK_FLAG"] = sLastEntryOkFlag;
		}
	}
	
	public DataSet GetOneForInformation(LittleQuestSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	QUEST_LEVEL									,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_NM								,	");
		oSqlBuilder.AppendLine("	1 AS QUEST_TYPE									");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	VW_PW_LITTLE_QUEST01							");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ			= :QUEST_SEQ			AND	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ		= :LEVEL_QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ	= :LITTLE_QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("	SEX_CD				= :SEX_CD					");

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":QUEST_SEQ",pCondition.QuestSeq));
		oParamList.Add(new OracleParameter(":LEVEL_QUEST_SEQ",pCondition.LevelQuestSeq));
		oParamList.Add(new OracleParameter(":LITTLE_QUEST_SEQ",pCondition.LittleQuestSeq));
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return oDataSet;
	}
}
