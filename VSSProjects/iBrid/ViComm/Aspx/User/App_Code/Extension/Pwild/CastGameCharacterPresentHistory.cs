﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｷｬｽﾄｹﾞｰﾑｷｬﾗｸﾀｰﾌﾟﾚｾﾞﾝﾄ履歴

--	Progaram ID		: CastGameCharacterPresentHistory
--
--  Creation Date	: 2012.12.07
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using ViComm.Extension.Pwild;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastGameCharacterPresentHistorySeekCondition:SeekConditionBase {

	public string SiteCd;
	public string SelfUserSeq;
	public string SelfUserCharNo;

	public string PresentHistorySeq {
		get {
			return this.Query["present_history_seq"];
		}
		set {
			this.Query["present_history_seq"] = value;
		}
	}

	public string GetFlag {
		get {
			return this.Query["get_flag"];
		}
		set {
			this.Query["get_flag"] = value;
		}
	}

	public CastGameCharacterPresentHistorySeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastGameCharacterPresentHistorySeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["present_history_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["present_history_seq"]));
		this.Query["get_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["get_flag"]));
	}
}

[System.Serializable]
public class CastGameCharacterPresentHistory:CastBase {

	public CastGameCharacterPresentHistory()
		: base() {
	}

	public CastGameCharacterPresentHistory(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(CastGameCharacterPresentHistorySeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine(" FROM									");
		oSqlBuilder.AppendLine("	VW_PW_CAST_GAME_PRE_HISTORY01 P		");
		// where
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" AND ROWNUM <= 20					");

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastGameCharacterPresentHistorySeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "				,	");
		oSqlBuilder.AppendLine("	GAME_HANDLE_NM							,	");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE						,	");
		oSqlBuilder.AppendLine("	CREATE_DATE								,	");
		oSqlBuilder.AppendLine("	PRESENT_HISTORY_SEQ						,	");
		oSqlBuilder.AppendLine("	GET_FLAG								,	");
		oSqlBuilder.AppendLine("	ITEM_COUNT								,	");
		oSqlBuilder.AppendLine("	PRESENT_GAME_ITEM_SEQ					,	");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM							,	");
		oSqlBuilder.AppendLine("	ATTACK_POWER							,	");
		oSqlBuilder.AppendLine("	DEFENCE_POWER							,	");
		oSqlBuilder.AppendLine("	ENDURANCE_NM							,	");
		oSqlBuilder.AppendLine("	0 AS REC_NO_PER_PAGE						");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	(											");
		oSqlBuilder.AppendLine("		SELECT									");
		oSqlBuilder.AppendLine("			" + CastBasicField() + "		,	");
		oSqlBuilder.AppendLine("			GAME_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("			GAME_CHARACTER_TYPE				,	");
		oSqlBuilder.AppendLine("			CREATE_DATE						,	");
		oSqlBuilder.AppendLine("			PRESENT_HISTORY_SEQ				,	");
		oSqlBuilder.AppendLine("			GET_FLAG						,	");
		oSqlBuilder.AppendLine("			ITEM_COUNT						,	");
		oSqlBuilder.AppendLine("			PRESENT_GAME_ITEM_SEQ			,	");
		oSqlBuilder.AppendLine("			GAME_ITEM_NM					,	");
		oSqlBuilder.AppendLine("			ATTACK_POWER					,	");
		oSqlBuilder.AppendLine("			DEFENCE_POWER					,	");
		oSqlBuilder.AppendLine("			ENDURANCE_NM						");
		oSqlBuilder.AppendLine("		FROM									");
		oSqlBuilder.AppendLine("			VW_PW_CAST_GAME_PRE_HISTORY00 P		");

		// where
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresion(pCondtion);
		oSqlBuilder.AppendLine(sSortExpression);
		oSqlBuilder.AppendLine("	) P											");
		oSqlBuilder.AppendLine("WHERE	ROWNUM <= 20			 				");

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		int iRecNoPerPage = 0;
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow oDR in oDataSet.Tables[0].Rows) {
				iRecNoPerPage = (int.Parse(oDR["RNUM"].ToString()) - iStartIndex);
				oDR["REC_NO_PER_PAGE"] = iRecNoPerPage.ToString();
			}
		}

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastGameCharacterPresentHistorySeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(" P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		SysPrograms.SqlAppendWhere(" P.PARTNER_USER_SEQ = :SELF_USER_SEQ",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_SEQ",pCondition.SelfUserSeq));

		SysPrograms.SqlAppendWhere(" P.PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",pCondition.SelfUserCharNo));

		if (!string.IsNullOrEmpty(pCondition.PresentHistorySeq)) {
			SysPrograms.SqlAppendWhere(" P.PRESENT_HISTORY_SEQ = :PRESENT_HISTORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRESENT_HISTORY_SEQ",pCondition.PresentHistorySeq));
		}

		if (!string.IsNullOrEmpty(pCondition.GetFlag)) {
			SysPrograms.SqlAppendWhere(" P.GET_FLAG = :GET_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":GET_FLAG",pCondition.GetFlag));
		}

		SysPrograms.SqlAppendWhere("P.NA_FLAG IN (:NA_FLAG,:NA_FLAG_2)",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		oParamList.Add(new OracleParameter(":NA_FLAG_2",ViCommConst.NaFlag.NO_AUTH));

		SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT * FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(CastGameCharacterPresentHistorySeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY CREATE_DATE DESC ";

		return sSortExpression;
	}
}

