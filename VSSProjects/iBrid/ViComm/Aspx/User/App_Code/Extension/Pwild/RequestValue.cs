﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい評価
--	Progaram ID		: Request
--  Creation Date	: 2012.06.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class RequestValueSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SexCd;
	public string UserSeq;
	public string UserCharNo;
	public string DeleteFlag;

	public string RequestSeq {
		get {
			return this.Query["req_seq"];
		}
		set {
			this.Query["req_seq"] = value;
		}
	}

	public string RequestValueSeq {
		get {
			return this.Query["val_seq"];
		}
		set {
			this.Query["val_seq"] = value;
		}
	}

	public string ValueNo {
		get {
			return this.Query["val_no"];
		}
		set {
			this.Query["val_no"] = value;
		}
	}

	public string CommentFlag {
		get {
			return this.Query["cmt_flg"];
		}
		set {
			this.Query["cmt_flg"] = value;
		}
	}

	public RequestValueSeekCondition()
		: this(new NameValueCollection()) {
	}

	public RequestValueSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["req_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["req_seq"]));
		this.Query["val_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["val_seq"]));
		this.Query["val_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["val_no"]));
		this.Query["cmt_flg"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cmt_flg"]));
	}
}

public class RequestValue:DbSession {
	public RequestValue() {
	}

	public int GetPageCount(RequestValueSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sViewNm = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		if (pCondtion.SexCd.Equals(ViCommConst.MAN)) {
			sViewNm = "VW_PW_MAN_REQUEST_VALUE01";
		} else {
			sViewNm = "VW_PW_CAST_REQUEST_VALUE01";
		}

		oSqlBuilder.AppendLine("SELECT			");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM			");
		oSqlBuilder.AppendFormat("	{0}	",sViewNm).AppendLine();
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(RequestValueSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion,pPageNo,pRecPerPage);
	}

	private DataSet GetPageCollectionBase(RequestValueSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sViewNm = string.Empty;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		if (pCondtion.SexCd.Equals(ViCommConst.MAN)) {
			sViewNm = "VW_PW_MAN_REQUEST_VALUE01";
		} else {
			sViewNm = "VW_PW_CAST_REQUEST_VALUE01";
		}

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	*	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendFormat("	{0}	",sViewNm);
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(RequestValueSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.DeleteFlag)) {
			SysPrograms.SqlAppendWhere("DELETE_FLAG = :DELETE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DELETE_FLAG",pCondition.DeleteFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.RequestSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_SEQ = :REQUEST_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_SEQ",pCondition.RequestSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.RequestValueSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_VALUE_SEQ = :REQUEST_VALUE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_VALUE_SEQ",pCondition.RequestValueSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.ValueNo)) {
			SysPrograms.SqlAppendWhere("VALUE_NO = :VALUE_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":VALUE_NO",pCondition.ValueNo));
		}

		if (!string.IsNullOrEmpty(pCondition.CommentFlag)) {
			if (pCondition.CommentFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("COMMENT_TEXT IS NOT NULL",ref pWhereClause);
			} else if (pCondition.CommentFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				SysPrograms.SqlAppendWhere("COMMENT_TEXT IS NULL",ref pWhereClause);
			}
		}

		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	private string CreateOrderExpresion(RequestValueSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY UPDATE_DATE DESC NULLS LAST,CREATE_DATE DESC";

		return sSortExpression;
	}

	public string WriteRequestValue(
		string pSiteCd,
		string pSexCd,
		string pRequestSeq,
		string pUserSeq,
		string pUserCharNo,
		string pValueNo,
		string pCommentText
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_REQUEST_VALUE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pREQUEST_SEQ",DbSession.DbType.VARCHAR2,pRequestSeq);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pVALUE_NO",DbSession.DbType.VARCHAR2,pValueNo);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string ModifyRequestValue(
		string pSiteCd,
		string pRequestValueSeq,
		string pValueNo,
		string pCommentText,
		out string pCommentPublicStatus
	) {
		string sResult;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_REQUEST_VALUE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pREQUEST_VALUE_SEQ",DbSession.DbType.VARCHAR2,pRequestValueSeq);
			db.ProcedureInParm("pVALUE_NO",DbSession.DbType.VARCHAR2,pValueNo);
			db.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,pCommentText);
			db.ProcedureOutParm("pCOMMENT_PUBLIC_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pCommentPublicStatus = db.GetStringValue("pCOMMENT_PUBLIC_STATUS");
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public string DeleteRequestValue(
		string pSiteCd,
		string pRequestValueSeq,
		string pDeleteFlag
	) {
		string sResult = string.Empty;

		return sResult;
	}
}
