﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ｹﾞｰﾑｷｬﾗｸﾀｰﾊﾞﾄﾙ(応援ﾊﾞﾄﾙ)
--	Progaram ID		: GameCharacterBattleSupport
--
--  Creation Date	: 2011.08.26
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

using System.Collections.Specialized;
using System.Collections.Generic;

[Serializable]
public class GameCharacterBattleSupportSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;
	public string SexCd;
	
	public string BattleLogSeq {
		get {
			return this.Query["battle_log_seq"];
		}
		set {
			this.Query["battle_log_seq"] = value;
		}
	}

	public string SupportRequestFlag;

	public GameCharacterBattleSupportSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameCharacterBattleSupportSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["battle_log_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["battle_log_seq"]));
	}
}

[System.Serializable]
public class GameCharacterBattleSupport:DbSession {
	public DataSet GetOne(GameCharacterBattleSupportSeekCondition pCondition) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT																			");
		oSqlBuilder.AppendLine("	CHARACTER.USER_SEQ							AS PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("	CHARACTER.USER_CHAR_NO						AS PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("	CHARACTER.GAME_CHARACTER_TYPE											,	");
		oSqlBuilder.AppendLine("	CHARACTER.GAME_CHARACTER_LEVEL											,	");
		oSqlBuilder.AppendLine("	CHARACTER.ATTACK_MAX_FORCE_COUNT			AS MAX_ATTACK_FORCE_COUNT	,	");
		oSqlBuilder.AppendLine("	CHARACTER.DEFENCE_FORCE_COUNT											,	");
		oSqlBuilder.AppendLine("	(CHARACTER.TOTAL_ATTACK_WIN_COUNT + CHARACTER.TOTAL_DEFENCE_WIN_COUNT)		AS TOTAL_WIN_COUNT	,	");
		oSqlBuilder.AppendLine("	(CHARACTER.TOTAL_ATTACK_LOSS_COUNT + CHARACTER.TOTAL_DEFENCE_LOSS_COUNT)	AS TOTAL_LOSS_COUNT	,	");
		oSqlBuilder.AppendLine("	CHARACTER.FELLOW_COUNT													,	");
		oSqlBuilder.AppendLine("	(CHARACTER.TOTAL_PARTY_ATTACK_WIN_COUNT + CHARACTER.TOTAL_PARTY_DEFENCE_WIN_COUNT)		AS TOTAL_PARTY_WIN_COUNT	,	");
		oSqlBuilder.AppendLine("	(CHARACTER.TOTAL_PARTY_ATTACK_LOSS_COUNT + CHARACTER.TOTAL_PARTY_DEFENCE_LOSS_COUNT)	AS TOTAL_PARTY_LOSS_COUNT	,	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST.USER_SEQ					AS SUPPORT_USER_SEQ			,	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST.USER_CHAR_NO				AS SUPPORT_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST.BATTLE_LOG_SEQ											,	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST_SUBSEQ													,	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST.SUPPORT_END_FLAG										,	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST.AIM_TREASURE_SEQ										,	");
		oSqlBuilder.AppendLine("	SUPPORT_REQUEST.SUPPORT_REQUEST_END_FLAG									");
		oSqlBuilder.AppendLine("FROM																			");
		oSqlBuilder.AppendLine("	VW_PW_GAME_CHARACTER03	CHARACTER,		");
		oSqlBuilder.AppendLine("	(										");
		oSqlBuilder.AppendLine("		SELECT								");
		oSqlBuilder.AppendLine("			SITE_CD						,	");
		oSqlBuilder.AppendLine("			USER_SEQ					,	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine("			BATTLE_LOG_SEQ				,	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_SUBSEQ		,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_SEQ			,	");
		oSqlBuilder.AppendLine("			PARTNER_USER_CHAR_NO		,	");
		oSqlBuilder.AppendLine("			SUPPORT_END_FLAG			,	");
		oSqlBuilder.AppendLine("			AIM_TREASURE_SEQ			,	");
		oSqlBuilder.AppendLine("			SUPPORT_REQUEST_END_FLAG		");
		oSqlBuilder.AppendLine("		FROM								");
		oSqlBuilder.AppendLine("			VW_PW_SUPPORT_REQUEST01			");

		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" BATTLE_LOG_SEQ = :BATTLE_LOG_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",pCondition.BattleLogSeq));

		SysPrograms.SqlAppendWhere(" SUPPORT_USER_SEQ = :SUPPORT_USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SUPPORT_USER_SEQ",pCondition.UserSeq));

		SysPrograms.SqlAppendWhere(" SUPPORT_USER_CHAR_NO = :SUPPORT_USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SUPPORT_USER_CHAR_NO",pCondition.UserCharNo));

		SysPrograms.SqlAppendWhere(" SUPPORT_REQUEST_FLAG = :SUPPORT_REQUEST_FLAG",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SUPPORT_REQUEST_FLAG",pCondition.SupportRequestFlag));
		
		oSqlBuilder.AppendLine(sWhereClause);
		sWhereClause = string.Empty;
		
		oSqlBuilder.AppendLine("	) SUPPORT_REQUEST	");
		oSqlBuilder.AppendLine("WHERE					");
		oSqlBuilder.AppendLine("	CHARACTER.SITE_CD		= SUPPORT_REQUEST.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	CHARACTER.USER_SEQ		= SUPPORT_REQUEST.PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	CHARACTER.USER_CHAR_NO	= SUPPORT_REQUEST.PARTNER_USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("	CHARACTER.SEX_CD		= :SEX_CD									");
		oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
	
	public bool CheckExistsPartner(string sSiteCd,string sUserSeq,string sUserCharNo,string sBattleLogSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	PARTNER.NA_FLAG										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01							,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER PARTNER							");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.BATTLE_LOG_SEQ			= :BATTLE_LOG_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= PARTNER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.PARTNER_USER_SEQ		= PARTNER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.PARTNER_USER_CHAR_NO	= PARTNER.USER_CHAR_NO			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",sBattleLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if(oDataSet.Tables[0].Rows.Count < 1) {
			return false;
		}
		
		DataRow oDataRow = oDataSet.Tables[0].Rows[0];
		
		if(oDataRow["NA_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return false;
		} else {
			return true;
		}
	}

	public bool CheckExistsSupport(string sSiteCd,string sUserSeq,string sUserCharNo,string sBattleLogSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	SUPPORT.NA_FLAG										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01							,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER SUPPORT							");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.BATTLE_LOG_SEQ			= :BATTLE_LOG_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= SUPPORT.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.USER_SEQ				= SUPPORT.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.USER_CHAR_NO			= SUPPORT.USER_CHAR_NO			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",sBattleLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count < 1) {
			return false;
		}

		DataRow oDataRow = oDataSet.Tables[0].Rows[0];

		if (oDataRow["NA_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return false;
		} else {
			return true;
		}
	}

	public bool CheckExistsPartnerWoman(string sSiteCd,string sUserSeq,string sUserCharNo,string sBattleLogSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	PARTNER.NA_FLAG										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01							,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER PARTNER						");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.BATTLE_LOG_SEQ			= :BATTLE_LOG_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= PARTNER.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.PARTNER_USER_SEQ		= PARTNER.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.PARTNER_USER_CHAR_NO	= PARTNER.USER_CHAR_NO			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",sBattleLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count < 1) {
			return false;
		}

		DataRow oDataRow = oDataSet.Tables[0].Rows[0];

		if (oDataRow["NA_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return false;
		} else {
			return true;
		}
	}

	public bool CheckExistsSupportWoman(string sSiteCd,string sUserSeq,string sUserCharNo,string sBattleLogSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	SUPPORT.NA_FLAG										");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01							,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER SUPPORT						");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_SEQ		= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SUPPORT_USER_CHAR_NO	= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.BATTLE_LOG_SEQ			= :BATTLE_LOG_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.SITE_CD					= SUPPORT.SITE_CD			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.USER_SEQ				= SUPPORT.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	VW_PW_SUPPORT_REQUEST01.USER_CHAR_NO			= SUPPORT.USER_CHAR_NO			");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));
		oParamList.Add(new OracleParameter(":BATTLE_LOG_SEQ",sBattleLogSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count < 1) {
			return false;
		}

		DataRow oDataRow = oDataSet.Tables[0].Rows[0];

		if (oDataRow["NA_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return false;
		} else {
			return true;
		}
	}
}
