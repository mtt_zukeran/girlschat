﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: 野球拳ゲーム（CastBase）
--	Progaram ID		: YakyukenGameCast
--
--  Creation Date	: 2013.05.02
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

[Serializable]
public class YakyukenGameCastSeekCondition:SeekConditionBase {
	public string SiteCd;

	public string CastUserSeq {
		get {
			return this.Query["castuserseq"];
		}
		set {
			this.Query["castuserseq"] = value;
		}
	}

	public string CastCharNo {
		get {
			return this.Query["castcharno"];
		}
		set {
			this.Query["castcharno"] = value;
		}
	}

	public string YakyukenGameSeq {
		get {
			return this.Query["ygameseq"];
		}
		set {
			this.Query["ygameseq"] = value;
		}
	}

	public string ShowFaceFlag {
		get {
			return this.Query["showface"];
		}
		set {
			this.Query["showface"] = value;
		}
	}

	public string ClearFlag {
		get {
			return this.Query["clear"];
		}
		set {
			this.Query["clear"] = value;
		}
	}

	public string ProgressFlag {
		get {
			return this.Query["progress"];
		}
		set {
			this.Query["progress"] = value;
		}
	}

	public YakyukenGameCastSeekCondition()
		: this(new NameValueCollection()) {
	}

	public YakyukenGameCastSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["castuserseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castuserseq"]));
		this.Query["castcharno"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["castcharno"]));
		this.Query["ygameseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["ygameseq"]));
		this.Query["showface"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["showface"]));
		this.Query["clear"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["clear"]));
		this.Query["progress"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["progress"]));
	}
}

public class YakyukenGameCast:CastBase {
	public YakyukenGameCast() {
	}

	public int GetPageCount(YakyukenGameCastSeekCondition pCondition,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_GAME00 P");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(YakyukenGameCastSeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	" + CastBasicField() + ",");
		oSqlBuilder.AppendLine("	YAKYUKEN_GAME_SEQ,");
		oSqlBuilder.AppendLine("	SHOW_FACE_FLAG,");
		oSqlBuilder.AppendLine("	COMPLETE_DATE,");
		oSqlBuilder.AppendLine("	YAKYUKEN_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	YAKYUKEN_SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_GAME00 P");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY COMPLETE_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		AppendYakyukenProgress(oDataSet);

		return oDataSet;
	}

	private void AppendYakyukenProgress(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("LAST_WIN_YAKYUKEN_TYPE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("CLEAR_FLAG",Type.GetType("System.String"));

		string sSiteCd = string.Empty;
		string sManUserSeq = string.Empty;

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			sSiteCd = oSessionMan.site.siteCd;
			sManUserSeq = oSessionMan.userMan.userSeq;
		}

		if (string.IsNullOrEmpty(sSiteCd) || string.IsNullOrEmpty(sManUserSeq)) {
			return;
		}

		string sWhereClause = String.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> sYakyukenGameSeqList = new List<string>();

		foreach(DataRow dr in pDS.Tables[0].Rows) {
			sYakyukenGameSeqList.Add(iBridUtil.GetStringValue(dr["YAKYUKEN_GAME_SEQ"]));
		}

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere("MAN_USER_SEQ = :MAN_USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",sManUserSeq));

		SysPrograms.SqlAppendWhere(string.Format("YAKYUKEN_GAME_SEQ IN ({0})",string.Join(",",sYakyukenGameSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	YAKYUKEN_GAME_SEQ,");
		oSqlBuilder.AppendLine("	LAST_WIN_YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	CLEAR_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_YAKYUKEN_PROGRESS");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["YAKYUKEN_GAME_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["YAKYUKEN_GAME_SEQ"]);
			pDR["LAST_WIN_YAKYUKEN_TYPE"] = subDR["LAST_WIN_YAKYUKEN_TYPE"];
			pDR["CLEAR_FLAG"] = subDR["CLEAR_FLAG"];
		}
	}

	private OracleParameter[] CreateWhere(YakyukenGameCastSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.YakyukenGameSeq)) {
			SysPrograms.SqlAppendWhere("YAKYUKEN_GAME_SEQ = :YAKYUKEN_GAME_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":YAKYUKEN_GAME_SEQ",pCondition.YakyukenGameSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.ShowFaceFlag)) {
			SysPrograms.SqlAppendWhere("SHOW_FACE_FLAG = :SHOW_FACE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SHOW_FACE_FLAG",pCondition.ShowFaceFlag));
		}

		if (!string.IsNullOrEmpty(pCondition.ClearFlag)) {
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				string sManUserSeq = oSessionMan.userMan.userSeq;

				if (!string.IsNullOrEmpty(sManUserSeq)) {
					if (pCondition.ClearFlag.Equals(ViCommConst.FLAG_ON_STR)) {
						SysPrograms.SqlAppendWhere("EXISTS(SELECT 1 FROM T_YAKYUKEN_PROGRESS WHERE SITE_CD = P.SITE_CD AND YAKYUKEN_GAME_SEQ = P.YAKYUKEN_GAME_SEQ AND MAN_USER_SEQ = :CLEAR_MAN_USER_SEQ AND CLEAR_FLAG = 1)",ref pWhereClause);
						oParamList.Add(new OracleParameter(":CLEAR_MAN_USER_SEQ",sManUserSeq));
					} else if (pCondition.ClearFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_YAKYUKEN_PROGRESS WHERE SITE_CD = P.SITE_CD AND YAKYUKEN_GAME_SEQ = P.YAKYUKEN_GAME_SEQ AND MAN_USER_SEQ = :CLEAR_MAN_USER_SEQ AND CLEAR_FLAG = 1)",ref pWhereClause);
						oParamList.Add(new OracleParameter(":CLEAR_MAN_USER_SEQ",sManUserSeq));
					}
				}
			}
		}

		if (!string.IsNullOrEmpty(pCondition.ProgressFlag)) {
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				string sManUserSeq = oSessionMan.userMan.userSeq;

				if (!string.IsNullOrEmpty(sManUserSeq)) {
					if (pCondition.ProgressFlag.Equals(ViCommConst.FLAG_ON_STR)) {
						SysPrograms.SqlAppendWhere("EXISTS(SELECT 1 FROM T_YAKYUKEN_PROGRESS WHERE SITE_CD = P.SITE_CD AND YAKYUKEN_GAME_SEQ = P.YAKYUKEN_GAME_SEQ AND MAN_USER_SEQ = :PROGRESS_MAN_USER_SEQ)",ref pWhereClause);
						oParamList.Add(new OracleParameter(":PROGRESS_MAN_USER_SEQ",sManUserSeq));
					} else if (pCondition.ProgressFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_YAKYUKEN_PROGRESS WHERE SITE_CD = P.SITE_CD AND YAKYUKEN_GAME_SEQ = P.YAKYUKEN_GAME_SEQ AND MAN_USER_SEQ = :PROGRESS_MAN_USER_SEQ)",ref pWhereClause);
						oParamList.Add(new OracleParameter(":PROGRESS_MAN_USER_SEQ",sManUserSeq));
					}
				}
			}
		}

		SysPrograms.SqlAppendWhere("COMPLETE_FLAG = 1",ref pWhereClause);
		SysPrograms.SqlAppendWhere("COMMENT_COUNT > 0",ref pWhereClause);

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}
}
