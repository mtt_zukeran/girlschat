﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 管理者連絡
--	Progaram ID		: AdminReport
--
--  Creation Date	: 2009.07.23
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System.Collections;
using Oracle.DataAccess.Client;
using System.Data;
using System.Text;
using System;
using iBridCommLib;

[System.Serializable]
public class AdminReport:DbSession {
	public decimal recCount;
	public void GetPrevNextDocSeq(
		string pBbsSeq,
		string pSiteCd,
		string pSexCd,
		out string pPrevDocSeq,
		out string pNextDocSeq) {

		pPrevDocSeq = "";
		pNextDocSeq = "";

		try {
			conn = DbConnect("AdminReport.GetPrevNextDocSeq");
			DataSet ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM(");
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		DOC_SEQ,").AppendLine();
			sSql.Append("		LAG(DOC_SEQ)	OVER (ORDER BY DOC_SEQ DESC) AS PREV_DOC_SEQ,").AppendLine();
			sSql.Append("		LEAD(DOC_SEQ)   OVER (ORDER BY DOC_SEQ DESC) AS NEXT_DOC_SEQ");
			sSql.Append("	FROM VW_ADMIN_REPORT01 P ").AppendLine();
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere("",pSiteCd,pSexCd,ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(") WHERE DOC_SEQ =:DOC_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("DOC_SEQ",pBbsSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_ADMIN_REPORT01");
				}
			}
			if (ds.Tables[0].Rows.Count != 0) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (dr["DOC_SEQ"].ToString().Equals(pBbsSeq)) {
						pPrevDocSeq = dr["PREV_DOC_SEQ"].ToString();
						pNextDocSeq = dr["NEXT_DOC_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
	}
	
	public int GetPageCount(string pSiteCd,string pSexCd,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("AdminReport.GetPageCount");
			ds = new DataSet();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_ADMIN_REPORT ";
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere("",pSiteCd,pSexCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {

				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				da.Fill(ds,"T_ADMIN_REPORT");

				if (ds.Tables["T_ADMIN_REPORT"].Rows.Count != 0) {
					dr = ds.Tables["T_ADMIN_REPORT"].Rows[0];
					recCount = Decimal.Parse(dr["ROW_COUNT"].ToString());
					pRecCount = recCount;
					iPages = (int)Math.Ceiling(recCount / pRecPerPage);
				}
			}
			conn.Close();
		} catch {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollectionBySeq(string pDocSeq) {
		return GetPageCollectionBase(pDocSeq,"","",0,0);
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pSexCd,pPageNo,pRecPerPage);
	}

	public DataSet GetPageCollectionBase(string pDocSeq,string pSiteCd,string pSexCd,int pPageNo,int pRecPerPage) {
		DataSet ds;

		conn = DbConnect("AdminReport.GetPageCollection");
		ds = new DataSet();

		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT * FROM( ").AppendLine();
		sSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (").AppendLine();
		sSql.Append("		SELECT ").AppendLine();
		sSql.Append("			DOC_SEQ			,").AppendLine();
		sSql.Append("			ADMIN_REPORT_DATE,").AppendLine();
		sSql.Append("			START_PUB_DAY	,").AppendLine();
		sSql.Append("			END_PUB_DAY		,").AppendLine();
		sSql.Append("			HTML_DOC1		,").AppendLine();
		sSql.Append("			HTML_DOC2		,").AppendLine();
		sSql.Append("			HTML_DOC3		,").AppendLine();
		sSql.Append("			HTML_DOC4		,").AppendLine();
		sSql.Append("			HTML_DOC5		,").AppendLine();
		sSql.Append("			DOC_TITLE		,").AppendLine();
		sSql.Append("			REPORT_HOUR_MIN		").AppendLine();
		sSql.Append("		FROM VW_ADMIN_REPORT01").AppendLine();

		string sWhere = "";

		OracleParameter[] objParms = CreateWhere(pDocSeq,pSiteCd,pSexCd,ref sWhere);
		sSql.Append(sWhere).AppendLine();
		sSql.Append("ORDER BY START_PUB_DAY DESC, REPORT_HOUR_MIN DESC").AppendLine();
		if (pDocSeq.Equals(string.Empty)) {
			sSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
		} else {
			sSql.Append(")INNER ) ");
		}

		using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
			for (int i = 0;i < objParms.Length;i++) {
				cmd.Parameters.Add(objParms[i]);
			}
			if (pDocSeq.Equals(string.Empty)) {
				cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
			}
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"VW_ADMIN_REPORT01");
			}
		}
		conn.Close();
		return ds;
	}

	private OracleParameter[] CreateWhere(string pDocSeq,string pSiteCd,string pSexCd,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pDocSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("DOC_SEQ = :DOC_SEQ",ref pWhere);
			list.Add(new OracleParameter("DOC_SEQ",pDocSeq));
		} else {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));

			if (!pSexCd.Equals("")) {
				SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
				list.Add(new OracleParameter("SEX_CD",pSexCd));
			}

			SysPrograms.SqlAppendWhere("TO_DATE(START_PUB_DAY||' '||REPORT_HOUR_MIN,'YYYY/MM/DD HH24:MI') < SYSDATE",ref pWhere);
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}