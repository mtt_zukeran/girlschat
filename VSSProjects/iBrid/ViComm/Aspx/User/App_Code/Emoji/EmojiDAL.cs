﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using MobileLib;
using MobileLib.Pictograph;

/// <summary>
/// EmojiDAL
/// </summary>
public class EmojiDAL
{
	/// <summary>
	/// 絵文字のカウント
	/// </summary>
	/// <param name="emojiType"></param>
	/// <returns></returns>
	public static int GetCount(string emojiType)
	{
		return GetData(emojiType).Count;
	}

	/// <summary>
	/// 絵文字データの取得
	/// </summary>
	/// <param name="emojiType"></param>
	/// <returns></returns>
	public static PictographDataSet.PictographDataTable GetData(string emojiType)
	{
		switch (emojiType)
		{
			case Mobile.DOCOMO:
				return PictographDefHolder.PictographDoCoMo.Pictograph;
			default:
				return PictographDefHolder.PictographDef.Pictograph;
		}
	}

	/// <summary>
	/// 絵文字データの取得
	/// </summary>
	/// <param name="emojiType"></param>
	/// <param name="limit">件数</param>
	/// <param name="offset">何ページ目か？</param>
	/// <returns></returns>
	public static PictographDataSet.PictographDataTable GetData(string emojiType, int limit, int offset)
	{
		PictographDataSet.PictographDataTable dt = GetData(emojiType);

		int count = dt.Count;
		int endIdx = limit * offset - 1;
		endIdx = endIdx > count - 1 ? count - 1 : endIdx;

		int startIdx = limit * (offset - 1);
		startIdx = endIdx > startIdx ? startIdx : endIdx - limit + 1;

		PictographDataSet.PictographDataTable result = new PictographDataSet.PictographDataTable();
		for (int i = startIdx, ri = 0; i < endIdx + 1; i++, ri++)
		{
			result.ImportRow(dt[i]);
		}
		return result;
	}
}
