﻿using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;

//using MobileLib;
using ViComm;

/// <summary>
/// EmojiHelper の概要の説明です
/// </summary>
public class EmojiBehavior
{
	#region Constant 
	/// <summary>
	/// 絵文字のCSSクラス名
	/// </summary>
	public const string EMOJI_ITEM_CLASS = "emoji_item";

	/// <summary>
	/// 絵文字の内側要素用のCSSクラス名
	/// </summary>
	public const string EMOJI_ITEM_INNER_CLASS = "emoji_item_inner";

	/// <summary>
	/// 絵文字の格納領域ID
	/// </summary>
	public const string EMOJI_CONTAINER_ID = "emoji_container";

	/// <summary>
	/// LIMITのGETキー
	/// </summary>
	public const string LIMIT = "lm";

	/// <summary>
	/// OFFSETのGETキー
	/// </summary>
	public const string OFFSET = "of";

	/// <summary>
	/// 絵文字の種類指定
	/// </summary>
	public const string EMOJI_TYPE = "type";
	#endregion

	/// <summary>
	/// ツールが有効か？
	/// </summary>
	private static bool _isToolEnabled(string carrier)
	{
		bool result = false;

		if (_isPC(carrier))
		{
			result = EmojiConfigHelper.PCEnabled;
		}
		if (_isSmartPhone(carrier))
		{
			result = EmojiConfigHelper.SmartPhoneEnabled;
		}
		return result;
	}

	/// <summary>
	/// PCか？
	/// </summary>
	/// <param name="carrier"></param>
	/// <returns></returns>
	private static bool _isPC(string carrier)
	{
		return (carrier == ViCommConst.CARRIER_OTHERS);
	}

	/// <summary>
	/// SmartPhoneか？
	/// </summary>
	/// <param name="carrier"></param>
	/// <returns></returns>
	private static bool _isSmartPhone(string carrier)
	{
		return (carrier == ViCommConst.ANDROID || carrier == ViCommConst.IPHONE);
	}

	/// <summary>
	/// ページリストの作成
	/// </summary>
	/// <param name="emojiUrl">絵文字ツールのUrl（ QueryParameterを足すだけの処理 ）</param>
	/// <returns></returns>
	private static string _getPageListScript(string emojiUrl, string carrier)
	{
		int limit = _isSmartPhone(carrier) ? 
					EmojiConfigHelper.SmartPhonePalettePerPage : EmojiConfigHelper.PCPalettePerPage;

		string emojiType = EmojiConfigHelper.Type;
		int pageCount = (int)Math.Ceiling( (double)EmojiDAL.GetCount( emojiType ) / (double)limit );

		StringBuilder buf = new StringBuilder();
		buf.Append("<script type='text/javascript'>");
		buf.Append("var EmojiPageList = {");
		for (int i = 1; i < pageCount + 1; i++)
		{
			buf.AppendFormat("'{0}':'{1}',", i, _getEmojiPageUrl(emojiUrl, emojiType, limit, i));
		}
		buf.Remove(buf.Length - 1, 1);
		buf.Append("};");
		buf.Append("</script>");

		return buf.ToString();
	}

	/// <summary>
	/// AnchorのIDを作成
	/// </summary>
	/// <param name="id"></param>
	/// <returns></returns>
	private static string _getAnchorId(string id)
	{
		return "btn_" + id;
	}

	/// <summary>
	/// 絵文字パレットにアクセスするためのURI作成
	/// </summary>
	/// <param name="emojiUrl">絵文字ツールのUrl（ QueryParameterを足すだけの処理 ）</param>
	/// <param name="emojiType">絵文字タイプ</param>
	/// <param name="limit">ページあたりの絵文字数</param>
	/// <param name="offset">表示対象のページ</param>
	/// <returns></returns>
	private static string _getEmojiPageUrl(string emojiUrl, string emojiType, int limit, int offset)
	{
		return string.Format(
			"{0}?{1}={2}&{3}={4}&{5}={6}"
			, emojiUrl
			, EMOJI_TYPE
			, emojiType
			, LIMIT
			, limit
			, OFFSET
			, offset
		);
	}

	/// <summary>
	/// スクリプトのヘッダーライブラリタグ
	/// </summary>
	/// <param name="serverRoot"></param>
	/// <param name="carrier"></param>
	/// <returns></returns>
	public static string GetScript(string serverRoot, string emojiUrl, string carrier)
	{
		StringBuilder buf = new StringBuilder();
		if ( _isToolEnabled(carrier) )
		{
			buf.AppendFormat(
				"\r\n<script src='{0}/scripts/emojitool.js' type='text/javascript'></script>\r\n"
				, serverRoot
			);
			if ( _isSmartPhone(carrier) )
			{
				buf.AppendFormat(
					"<script src='{0}/scripts/emojitoolsp.js' type='text/javascript'></script>\r\n"
					, serverRoot
					);
			}

			buf.Append(_getPageListScript(emojiUrl, carrier));
			buf.Append("\r\n");
		}
		return buf.ToString();
	}

	/// <summary>
	/// CSSのヘッダータグ
	/// </summary>
	/// <param name="serverRoot">サーバーのルート</param>
	/// <param name="carrier">キャリア</param>
	/// <returns></returns>
	public static string GetCssHeaderTag(string serverRoot, string carrier)
	{
		string result = "";
		if (_isToolEnabled(carrier))
		{
			result = string.Format(
						"<link rel='stylesheet' media='all' type='text/css' href='{0}/css/emojitool.css' />\r\n"
						, serverRoot
					);

			if (_isSmartPhone(carrier))
			{
				result += string.Format(
							"<link rel='stylesheet' media='all' type='text/css' href='{0}/css/emojitoolsp.css' />\r\n"
							, serverRoot
						);
			}
		}
		return result;
	}
}

/// <summary>
/// JQuery用のヘルパ（とりあえずinternalで）
/// </summary>
internal static class JQueryHelper
{
	/// <summary>
	/// ID Selectorにして返す
	/// </summary>
	/// <param name="id"></param>
	/// <returns></returns>
	public static string GetSelecterId(string id)
	{
		return "#" + id;
	}

	/// <summary>
	/// Class Selectorにして返す
	/// </summary>
	/// <param name="className"></param>
	/// <returns></returns>
	public static string GetSelecterClass(string className)
	{
		return "." + className;
	}
}


internal static class EmojiConfigHelper
{
	/// <summary>
	/// どの絵文字か？
	/// </summary>
	public static string Type
	{
		get
		{
			string type = NameValueCollectionUtil.GetValue(ConfigurationManager.AppSettings, "EmojiType", "docomo");
			if (type.ToLower() == "docomo")
			{
				return ViCommConst.DOCOMO;
			}
			else
			{
				return "";
			}
		}
	}

	/// <summary>
	/// スマートフォンで有効か？
	/// </summary>
	public static bool SmartPhoneEnabled
	{
		get
		{
			return NameValueCollectionUtil.GetValue(ConfigurationManager.AppSettings, "EmojiSmartPhoneEnabled", false);
		}
	}

	/// <summary>
	/// PCで有効か？
	/// </summary>
	public static bool PCEnabled
	{
		get
		{
			return NameValueCollectionUtil.GetValue(ConfigurationManager.AppSettings, "EmojiPCEnabled", false);
		}
	}

	/// <summary>
	/// ページあたりの絵文字数（PC）
	/// </summary>
	public static int PCPalettePerPage
	{
		get
		{
			return NameValueCollectionUtil.GetValue(ConfigurationManager.AppSettings, "EmojiPCPalettePerPage", 60);
		}
	}

	/// <summary>
	/// ページあたりの絵文字数（スマートフォン）
	/// </summary>
	public static int SmartPhonePalettePerPage
	{
		get
		{
			return NameValueCollectionUtil.GetValue(ConfigurationManager.AppSettings, "EmojiSmartPhonePalettePerPage", 40);
		}
	}
}