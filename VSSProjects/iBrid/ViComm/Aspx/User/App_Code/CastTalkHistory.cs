﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会話履歴(相手は女性)
--	Progaram ID		: CastTalkHistory
--
--  Creation Date	: 2010.11.24
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using System.Configuration;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class CastTalkHistory:CastBase {

	public CastTalkHistory() {
	}

	public int GetPageCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		bool pGroupingFlag,
		int pRecPerPage,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT									").AppendLine();
		oSqlBuilder.Append("	COUNT(TALK_SEQ) AS ROW_COUNT		").AppendLine();
		oSqlBuilder.Append(" FROM									").AppendLine();
		if (pGroupingFlag) {
			oSqlBuilder.Append(" VW_TALK_HISTORY01 T				").AppendLine();
		} else {
			oSqlBuilder.Append(" T_TALK_HISTORY T					").AppendLine();
		}
		oSqlBuilder.Append("	,VW_CAST_CHARACTER00 P				").AppendLine();

		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,false,ref sWhere);

		oSqlBuilder.Append(sWhere);

		try {
			conn = DbConnect("CastTalkHistory.GetPageCount");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,bool bAllInfo,ref string pWhere) {
		ArrayList list = new ArrayList();
		StringBuilder sWhere = new StringBuilder();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}
		sWhere.AppendLine();
		sWhere.Append("	WHERE ").AppendLine();
		sWhere.Append("		T.SITE_CD		= :SITE_CD			").AppendLine();
		sWhere.Append(" AND	T.USER_SEQ		= :USER_SEQ			").AppendLine();
		sWhere.Append("	AND	T.USER_CHAR_NO	= :USER_CHAR_NO		").AppendLine();
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));

		if (!pPartnerLoginId.Equals(string.Empty) && !pPartnerUserCharNo.Equals(string.Empty)) {
			sWhere.Append("	AND	P.LOGIN_ID				= :LOGIN_ID				").AppendLine();
			sWhere.Append("	AND	T.PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	").AppendLine();
			list.Add(new OracleParameter("LOGIN_ID",pPartnerLoginId));
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pPartnerUserCharNo));
		}
		
		sWhere.Append("	AND	P.NA_FLAG		=: NA_FLAG		").AppendLine();
		list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));

		sWhere.Append("	AND	T.DEL_FLAG = :DEL_FLAG	").AppendLine();
		list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));

		sWhere.Append("	AND	T.SITE_CD				= P.SITE_CD			").AppendLine();
		sWhere.Append("	AND	T.PARTNER_USER_SEQ		= P.USER_SEQ		").AppendLine();
		sWhere.Append("	AND	T.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO	").AppendLine();

		pWhere = sWhere.ToString();

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		bool pGroupingFlag,
		int pPageNo,
		int pRecPerPage
	) {
		DataSet ds;
		try {
			conn = DbConnect("TalkHistory.GetPageCollection");
			ds = new DataSet();

			string sOrder = "ORDER BY T.TALK_START_DATE DESC,T.USER_SEQ,T.USER_CHAR_NO ";

			StringBuilder sInnerSql = new StringBuilder();

			sInnerSql.Append("SELECT * FROM ");
			sInnerSql.Append("(SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("	SELECT " + CastBasicField() + ",");
			sInnerSql.Append("	T.TALK_START_DATE			").AppendLine();
			sInnerSql.Append(" ,T.PARTNER_USER_SEQ			").AppendLine();
			sInnerSql.Append(" ,T.PARTNER_USER_CHAR_NO		").AppendLine();
			sInnerSql.Append(" ,T.TALK_SEQ					").AppendLine();
			if (pGroupingFlag) {
				sInnerSql.Append(" ,T.UNIQUE_VALUE				").AppendLine();
			} else {
				sInnerSql.Append("	,T.MEMO						").AppendLine();
				sInnerSql.Append("	,T.TALK_END_DATE			").AppendLine();
				sInnerSql.Append("	,T.TALK_SEQ AS UNIQUE_VALUE	").AppendLine();
				sInnerSql.Append("	,T.CALL_RESULT				").AppendLine();
			}
			sInnerSql.Append("FROM VW_CAST_CHARACTER00 P	 ").AppendLine();
			if (pGroupingFlag) {
				sInnerSql.Append("	,VW_TALK_HISTORY01 T	 ").AppendLine();
			} else {
				sInnerSql.Append("	,T_TALK_HISTORY T		 ").AppendLine();
			}

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,true,ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			//sTables
			StringBuilder sTables = new StringBuilder();
			sTables.AppendLine("T_IVP_REQUEST TALK_HIS_IVP_REQUEST	,");
			//JoinTable
			StringBuilder sJoinTable = new StringBuilder();
			sJoinTable.AppendLine("T1.TALK_SEQ	= TALK_HIS_IVP_REQUEST.TALK_SEQ	(+) AND");
			//JoinField
			StringBuilder sJoinField = new StringBuilder();
			sJoinField.AppendLine("T_IVP_REQUEST.CHARGE_TYPE	,");
			if (pGroupingFlag) {
				sJoinField.AppendLine("(SELECT CALL_RESULT FROM T_TALK_HISTORY WHERE TALK_SEQ = T1.TALK_SEQ) AS CALL_RESULT,");
			}

			sSql = string.Format(sSql,sInnerSql.ToString(),sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);

				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TALK_HISTORY01");
				}
			}
		} finally {
			conn.Close();
		}

		AppendAttr(ds);
		return ds;
	}

	private void AppendAttr(DataSet pDS) {
		string sSql;
		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
						"WHERE " +
							"SITE_CD		= :SITE_CD				AND " +
							"USER_SEQ		= :PARTNER_USER_SEQ		AND " +
							"USER_CHAR_NO	= :PARTNER_USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["PARTNER_USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["PARTNER_USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}
				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}
}

