/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: サイト別広告グループ割当

--	Progaram ID		: SiteAdGroup
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

[System.Serializable]
public class SiteAdGroup:DbSession {

	public SiteAdGroup() {
	}

	public bool GetAdGroupCd(string pSiteCd,string pAdCd,out string pAdGroupCd,out int pGroupType) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pAdGroupCd = "";
		pGroupType = 0;
		try{
			conn = DbConnect("SiteAdGroup.GetAdGroupCd");

			using (cmd = CreateSelectCommand("SELECT AD_GROUP_CD,PREPAID_GROUP_FLAG FROM VW_SITE_AD_GROUP01 WHERE SITE_CD =:SITE_CD AND AD_CD =:AD_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("AD_CD",pAdCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE_AD_GROUP");
					if (ds.Tables["T_SITE_AD_GROUP"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						pAdGroupCd = dr["AD_GROUP_CD"].ToString();
						pGroupType = int.Parse(dr["PREPAID_GROUP_FLAG"].ToString());
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
