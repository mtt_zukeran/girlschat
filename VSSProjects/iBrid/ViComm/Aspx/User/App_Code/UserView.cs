﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーVIEW
--	Progaram ID		: UserView
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Text;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class UserView:DbSession {

	public UserView() {
	}


	public void GetView(
		string pSiteCd,
		string pProgramRoot,
		string pProgramId,
		string pCarrierCd,
		string pAdGroupCd,
		string pUserRank,
		string pActCategorySeq,
		string pDocNo,
		bool pDesignDebug,
		out string pTitle,
		out string pKeyword,
		out string pDescription,
		out int pTableIdx,
		ref string[] pHtml,
		out int pViewMask,
		out string pUserAgentType,
		out string[] pCssFileNmArray,
		out string[] pJavascriptFileNmArray,
		out string pCanonical,
		out int pProtectImageFlag,
		out int pEmojiToolFlag,
		out int pNoIndexFlag,
		out int pNoFollowFlag,
		out int pNoArchiveFlag,
		ref WebFace pHtmlDocWebFace
) {

		pTitle = string.Empty;
		pKeyword = string.Empty;
		pDescription = string.Empty;
		pUserAgentType = string.Empty;
		pCssFileNmArray = new string[] { };
		pJavascriptFileNmArray = new string[] { };
		pCanonical = string.Empty;
		pProtectImageFlag = ViCommConst.FLAG_ON;
		pEmojiToolFlag = ViCommConst.FLAG_ON;
		pNoIndexFlag = ViCommConst.FLAG_OFF;
		pNoFollowFlag = ViCommConst.FLAG_OFF;
		pNoArchiveFlag = ViCommConst.FLAG_OFF;
		pViewMask = 0;

		pTableIdx = SysConst.NOTHING;

		string[] sUserAgentType = new string[32];
		string[] sAdGroupCd = new string[32];
		string[] sUserRank = new string[32];
		int[] iActCategoryCd = new int[32];
		int iMask = 0;

		DataSet ds;
		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT VIEW_KEY_MASK,USER_AGENT_TYPE FROM T_VIEW_KEY").AppendLine();
		sSql.Append("WHERE").AppendLine();
		sSql.Append("SITE_CD		= :SITE_CD		AND ").AppendLine();
		sSql.Append("PROGRAM_ROOT	= :PROGRAM_ROOT	AND	").AppendLine();
		sSql.Append("PROGRAM_ID		= :PROGRAM_ID	AND	").AppendLine();

		bool bEnableSmartPhoneDesign = pDesignDebug;
		if (!bEnableSmartPhoneDesign) {
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesign"]).Equals("1")) {
				if (!iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesignSiteCd"]).Equals(string.Empty)) {
					string[] oEnableSmartPhoneDesignSiteCd = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableSmartPhoneDesignSiteCd"]).Split(',');
					foreach (string sTempSiteCd in oEnableSmartPhoneDesignSiteCd) {
						if (pSiteCd.Equals(sTempSiteCd)) {
							bEnableSmartPhoneDesign = true;
							break;
						}
					}
				} else {
					bEnableSmartPhoneDesign = true;
				}
			}
		}

		if (bEnableSmartPhoneDesign) {
			if (pCarrierCd.Equals(ViCommConst.ANDROID) || pCarrierCd.Equals(ViCommConst.IPHONE) || (pCarrierCd.Equals(ViCommConst.CARRIER_OTHERS) && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableDisplaySPDesignToPCUser"]).Equals(ViCommConst.FLAG_ON_STR))) {
				sSql.Append("USER_AGENT_TYPE IN (:USER_AGENT_TYPE1,:USER_AGENT_TYPE2,:USER_AGENT_TYPE3)").AppendLine();
			} else {
				sSql.Append("USER_AGENT_TYPE = :USER_AGENT_TYPE1").AppendLine();
			}
		} else {
			sSql.Append("USER_AGENT_TYPE = :USER_AGENT_TYPE1").AppendLine();
		}
		sSql.Append("ORDER BY SITE_CD,PROGRAM_ROOT,PROGRAM_ID,USER_AGENT_TYPE DESC,VIEW_KEY_MASK DESC").AppendLine();

		int i = 0;
		try {
			conn = DbConnect("UserView.GetView");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet())
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PROGRAM_ROOT",pProgramRoot);
				cmd.Parameters.Add("PROGRAM_ID",pProgramId);
				if (bEnableSmartPhoneDesign) {
					if (pCarrierCd.Equals(ViCommConst.ANDROID) || (pCarrierCd.Equals(ViCommConst.CARRIER_OTHERS) && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableDisplaySPDesignToPCUser"]).Equals(ViCommConst.FLAG_ON_STR))) {
						cmd.Parameters.Add("USER_AGENT_TYPE1",ViCommConst.DEVICE_ANDROID);
						cmd.Parameters.Add("USER_AGENT_TYPE2",ViCommConst.DEVICE_SMART_PHONE);
						cmd.Parameters.Add("USER_AGENT_TYPE3",ViCommConst.DEVICE_3G);
					} else if (pCarrierCd.Equals(ViCommConst.IPHONE)) {
						cmd.Parameters.Add("USER_AGENT_TYPE1",ViCommConst.DEVICE_IPHONE);
						cmd.Parameters.Add("USER_AGENT_TYPE2",ViCommConst.DEVICE_SMART_PHONE);
						cmd.Parameters.Add("USER_AGENT_TYPE3",ViCommConst.DEVICE_3G);
					} else {
						cmd.Parameters.Add("USER_AGENT_TYPE1",ViCommConst.DEVICE_3G);
					}
				} else {
					cmd.Parameters.Add("USER_AGENT_TYPE1",ViCommConst.DEVICE_3G);
				}

				da.Fill(ds,"T_VIEW_KEY");

				foreach (DataRow dr in ds.Tables[0].Rows) {
					sAdGroupCd[i] = ViCommConst.DEFAUL_AD_GROUP_CD;
					sUserRank[i] = ViCommConst.DEFAUL_USER_RANK;
					iActCategoryCd[i] = ViCommConst.DEFAULT_ACT_CATEGORY_SEQ;
					iMask = int.Parse(dr["VIEW_KEY_MASK"].ToString());

					if ((iMask & ViCommConst.VIEW_MASK_ACT_CATEGORY) != 0) {
						int.TryParse(pActCategorySeq,out iActCategoryCd[i]);
					}
					if ((iMask & ViCommConst.VIEW_MASK_USER_RANK) != 0) {
						sUserRank[i] = pUserRank;
					}
					if ((iMask & ViCommConst.VIEW_MASK_AD_GROUP) != 0) {
						sAdGroupCd[i] = pAdGroupCd;
					}
					sUserAgentType[i] = dr["USER_AGENT_TYPE"].ToString();
					i++;
				}
			}
		} finally {
			conn.Close();
		}

		if (i > 0) {
			for (int j = 0;j < i;j++) {
				if (GetOne(pSiteCd,pProgramRoot,pProgramId,sUserAgentType[j],sAdGroupCd[j],sUserRank[j],iActCategoryCd[j],
						out pTitle,out pKeyword,out pDescription,out pTableIdx,ref pHtml,out pViewMask,out pCssFileNmArray,out pJavascriptFileNmArray,out pProtectImageFlag,out pEmojiToolFlag,out pNoIndexFlag,out pNoFollowFlag,out pNoArchiveFlag)) {
					pUserAgentType = sUserAgentType[j];
					break;
				}
			}
		} else {
			if ((i == 0) && (pProgramId.ToLower().Equals("displaydoc.aspx") || pProgramId.ToLower().Equals("gamedisplaydoc.aspx"))) {
				using (SiteHtmlDoc oDoc = new SiteHtmlDoc()) {
					bool bExistWebFace;
					oDoc.GetDocHtmlInfo(pSiteCd,pDocNo,pCarrierCd,pDesignDebug,out pTitle,out pKeyword,out pDescription,out pTableIdx,ref pHtmlDocWebFace,out bExistWebFace,out pUserAgentType,out pCssFileNmArray,out pJavascriptFileNmArray,out pCanonical,out pProtectImageFlag,out pEmojiToolFlag);
					if (bExistWebFace) {
						pViewMask = ViCommConst.VIEW_MASK_HTML_DOC_COLOR;
					}
				}
			}
			for (int k = 0;k < 10;k++) {
				pHtml[k] = "";
			}
		}
	}

	public bool GetOne(
		string pSiteCd,
		string pProgramRoot,
		string pProgramId,
		string pUserAgentType,
		string pAdGroupCd,
		string pUserRank,
		int pActCategorySeq,
		out string pTitle,
		out string pKeyword,
		out string pDescription,
		out int pTableIdx,
		ref string[] pHtml,
		out int pViewMask,
		out string[] pCssFileNmArray,
		out string[] pJavascriptFileNmArray,
		out int pProtectImageFlag,
		out int pEmojiToolFlag,
		out int pNoIndexFlag,
		out int pNoFollowFlag,
		out int pNoArchiveFlag
	) {
		DataSet ds;
		try {
			conn = DbConnect("UserView.GetOne");
			ds = new DataSet();

			pTitle = string.Empty;
			pKeyword = string.Empty;
			pDescription = string.Empty;
			pCssFileNmArray = new string[] { };
			pJavascriptFileNmArray = new string[] { };
			pViewMask = 0;
			pTableIdx = SysConst.NOTHING;
			pProtectImageFlag = ViCommConst.FLAG_ON;
			pEmojiToolFlag = ViCommConst.FLAG_ON;
			pNoIndexFlag = ViCommConst.FLAG_OFF;
			pNoFollowFlag = ViCommConst.FLAG_OFF;
			pNoArchiveFlag = ViCommConst.FLAG_OFF;

			string sSql = "SELECT " +
								"VIEW_KEY_MASK		," +
								"PAGE_TITLE			," +
								"PAGE_KEYWORD		," +
								"PAGE_DESCRIPTION	," +
								"TABLE_INDEX		," +
								"USER_AGENT_TYPE	," +
								"CSS_FILE_NM1		," +
								"CSS_FILE_NM2		," +
								"CSS_FILE_NM3		," +
								"CSS_FILE_NM4		," +
								"CSS_FILE_NM5		," +
								"JAVASCRIPT_FILE_NM1," +
								"JAVASCRIPT_FILE_NM2," +
								"JAVASCRIPT_FILE_NM3," +
								"JAVASCRIPT_FILE_NM4," +
								"JAVASCRIPT_FILE_NM5," +
								"PROTECT_IMAGE_FLAG," +
								"EMOJI_TOOL_FLAG," +
								"NOINDEX_FLAG," +
								"NOFOLLOW_FLAG," +
								"NOARCHIVE_FLAG," +
								"HTML_DOC_SUB_SEQ	," +
								"HTML_DOC1," +
								"HTML_DOC2," +
								"HTML_DOC3," +
								"HTML_DOC4," +
								"HTML_DOC5," +
								"HTML_DOC6," +
								"HTML_DOC7," +
								"HTML_DOC8," +
								"HTML_DOC9," +
								"HTML_DOC10 " +
							"FROM " +
							" VW_USER_VIEW02 " +
							"WHERE " +
							" SITE_CD			= :SITE_CD			AND " +
							" PROGRAM_ROOT		= :PROGRAM_ROOT		AND " +
							" PROGRAM_ID		= :PROGRAM_ID		AND " +
							" USER_AGENT_TYPE	= :USER_AGENT_TYPE	AND " +
							" AD_GROUP_CD		= :AD_GROUP_CD		AND " +
							" USER_RANK			= :USER_RANK		AND " +
							" ACT_CATEGORY_SEQ	= :ACT_CATEGORY_SEQ	";


			sSql = sSql + " ORDER BY HTML_DOC_SUB_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PROGRAM_ROOT",pProgramRoot);
				cmd.Parameters.Add("PROGRAM_ID",pProgramId);
				cmd.Parameters.Add("USER_AGENT_TYPE",pUserAgentType);
				cmd.Parameters.Add("AD_GROUP_CD",pAdGroupCd);
				cmd.Parameters.Add("USER_RANK",pUserRank);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		for (int i = 0;i < 10;i++) {
			pHtml[i] = "";
		}

		foreach (DataRow dr in ds.Tables[0].Rows) {
			pViewMask = int.Parse(dr["VIEW_KEY_MASK"].ToString());
			pTitle = dr["PAGE_TITLE"].ToString();
			pKeyword = dr["PAGE_KEYWORD"].ToString();
			pDescription = dr["PAGE_DESCRIPTION"].ToString();
			pTableIdx = int.Parse(dr["TABLE_INDEX"].ToString());
			pProtectImageFlag = int.Parse(dr["PROTECT_IMAGE_FLAG"].ToString());
			pEmojiToolFlag = int.Parse(dr["EMOJI_TOOL_FLAG"].ToString());
			pNoIndexFlag = int.Parse(dr["NOINDEX_FLAG"].ToString());
			pNoFollowFlag = int.Parse(dr["NOFOLLOW_FLAG"].ToString());
			pNoArchiveFlag = int.Parse(dr["NOARCHIVE_FLAG"].ToString());

			List<string> oCssFileNmList = new List<string>();
			List<string> oJavascriptFileNmList = new List<string>();
			for (int i = 0; i < 5; i++) {
				string sCssFileNm = iBridUtil.GetStringValue(dr[string.Format("CSS_FILE_NM{0}",i + 1)]);
				if (!string.IsNullOrEmpty(sCssFileNm)) {
					oCssFileNmList.Add(sCssFileNm);
				}

				string sJavascriptFileNm = iBridUtil.GetStringValue(dr[string.Format("JAVASCRIPT_FILE_NM{0}",i + 1)]);
				if (!string.IsNullOrEmpty(sJavascriptFileNm)) {
					oJavascriptFileNmList.Add(sJavascriptFileNm);
				}
			}
			pCssFileNmArray = oCssFileNmList.ToArray();
			pJavascriptFileNmArray = oJavascriptFileNmList.ToArray();
			
			pHtml[int.Parse(dr["HTML_DOC_SUB_SEQ"].ToString())] = dr["HTML_DOC1"].ToString() +
							dr["HTML_DOC2"].ToString() +
							dr["HTML_DOC3"].ToString() +
							dr["HTML_DOC4"].ToString() +
							dr["HTML_DOC5"].ToString() +
							dr["HTML_DOC6"].ToString() +
							dr["HTML_DOC7"].ToString() +
							dr["HTML_DOC8"].ToString() +
							dr["HTML_DOC9"].ToString() +
							dr["HTML_DOC10"].ToString();
		}

		return (ds.Tables[0].Rows.Count > 0);
	}

}
