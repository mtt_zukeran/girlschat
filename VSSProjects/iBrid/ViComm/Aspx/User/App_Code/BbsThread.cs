﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド
--	Progaram ID		: BbsThread
--  Creation Date	: 2011.04.08
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class BbsThread:CastBase {
	public BbsThread()
		: base() {
	}
	public BbsThread(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(string pSiteCd,string pBbsThreadSeq,string pBbsAdminThread,string pSearchKey,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("BbsThread.GetPageCount");
			string sSql = "SELECT COUNT(BBS_THREAD_SEQ) AS ROW_COUNT FROM VW_BBS_THREAD00 P ";

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pBbsThreadSeq,pBbsAdminThread,pSearchKey,false,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	private void GetOrder(string pOrderType,out string pOrder) {
		if (pOrderType.Equals(ViCommConst.BbsThreadSortType.RES_COUNT)) {
			pOrder = " ORDER BY SITE_CD,LAST_THREAD_SUB_SEQ DESC,BBS_THREAD_SEQ DESC ";
		} else if (pOrderType.Equals(ViCommConst.BbsThreadSortType.THREAD_ACCESS_COUNT)) {
			pOrder = " ORDER BY SITE_CD,TODAY_ACCESS_COUNT DESC,BBS_THREAD_SEQ DESC ";
		} else {
			pOrder = " ORDER BY SITE_CD,LAST_RES_WRITE_DATE DESC,BBS_THREAD_SEQ DESC ";
		}
	}

	public DataSet GetPageCollection(string pSiteCd,string pBbsThreadSeq,string pBbsAdminThread,string pOrderType,string pSearchKey,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try {
			conn = DbConnect("BbsThread.GetPageCollectionBase");
			ds = new DataSet();

			string sOrder;
			GetOrder(pOrderType,out sOrder);

			StringBuilder sInnerSql = new StringBuilder();
			sInnerSql.Append("SELECT  * FROM( ");
			sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT " + CastBasicField() + ",").AppendLine();
			sInnerSql.Append("			BBS_THREAD_SEQ						,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_HANDLE_NM				,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_TITLE					,").AppendLine();
			sInnerSql.Append("			ADMIN_THREAD_FLAG					,").AppendLine();
			sInnerSql.Append("			LAST_RES_WRITE_DATE					,").AppendLine();
			sInnerSql.Append("			LAST_THREAD_SUB_SEQ					,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC1						,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC2						,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC3						,").AppendLine();
			sInnerSql.Append("			BBS_THREAD_DOC4						,").AppendLine();
			sInnerSql.Append("			DEL_FLAG							,").AppendLine();
			sInnerSql.Append("			CREATE_DATE							,").AppendLine();
			sInnerSql.Append("			UPDATE_DATE							,").AppendLine();
			sInnerSql.Append("			TODAY_ACCESS_COUNT					").AppendLine();
			sInnerSql.Append("		FROM VW_BBS_THREAD00 P ");

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pBbsThreadSeq,pBbsAdminThread,pSearchKey,true,ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = sessionObj.sqlSyntax[sTemplate].ToString();
			sSql = string.Format(sSql,sInnerSql,"","","");

			string sHint = string.Empty;
			if (sessionObj.sqlHint.ContainsKey("CAST")) {
				sHint = sessionObj.sqlHint["CAST"].ToString();
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);

				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_BBS_THREAD00");

				}
			}
		} finally {
			conn.Close();
		}
		AppendAttr(ds);
		return ds;
	}

	private void AppendAttr(DataSet pDS) {
		string sNewDays = sessionObj.site.newFaceDays.ToString();
		string sNewMark = sessionObj.site.newFaceMark;
		string sSql;
		if (sNewDays.Equals("")) {
			sNewDays = "0";
		}
		string sNewDay = DateTime.Now.AddDays(-1 * int.Parse(sNewDays)).ToString("yyyy/MM/dd");

		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD		AND " +
						"USER_SEQ		= :USER_SEQ		AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();

			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pBbsThreadSeq,string pBbsAdminThread,string pSearchKey,bool bAllInfo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pBbsThreadSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.BBS_THREAD_SEQ = :BBS_THREAD_SEQ",ref pWhere);
			list.Add(new OracleParameter("BBS_THREAD_SEQ",pBbsThreadSeq));
		}

		if (pBbsAdminThread.Equals(ViCommConst.BbsThreadSearchAdminType.ADMIN_FLAG_IS_TRUE)) {
			SysPrograms.SqlAppendWhere("P.ADMIN_THREAD_FLAG = :ADMIN_THREAD_FLAG",ref pWhere);
			list.Add(new OracleParameter("ADMIN_THREAD_FLAG",ViCommConst.FLAG_ON));
		} else if (pBbsAdminThread.Equals(ViCommConst.BbsThreadSearchAdminType.ADMIN_FLAG_IS_FALSE)) {
			SysPrograms.SqlAppendWhere("P.ADMIN_THREAD_FLAG = :ADMIN_THREAD_FLAG",ref pWhere);
			list.Add(new OracleParameter("ADMIN_THREAD_FLAG",ViCommConst.FLAG_OFF));
		}

		if (sessionObj.sexCd.Equals(ViCommConst.WOMAN)) {
			SessionWoman sessionWoman = (SessionWoman)sessionObj;
			string sThreadTypeSql = " P.THREAD_TYPE IN (:THREAD_TYPE1 ";
			list.Add(new OracleParameter("THREAD_TYPE1",ViCommConst.BbsThreadType.NORMAL));
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.enabledRichinoFlag == ViCommConst.FLAG_ON) {
				sThreadTypeSql += ",:THREAD_TYPE2 ";
				list.Add(new OracleParameter("THREAD_TYPE2",ViCommConst.BbsThreadType.RICHINO));
			}
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.enabledBlogFlag == ViCommConst.FLAG_ON) {
				sThreadTypeSql += ",:THREAD_TYPE3 ";
				list.Add(new OracleParameter("THREAD_TYPE3",ViCommConst.BbsThreadType.BLOG));
			}
			sThreadTypeSql += " ) ";
			SysPrograms.SqlAppendWhere(sThreadTypeSql,ref pWhere);
		}

		SysPrograms.SqlAppendWhere("P.DEL_FLAG = :DEL_FLAG",ref pWhere);
		list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));

		if (!pSearchKey.Equals(string.Empty)) {
			StringBuilder oSql = new StringBuilder();

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_BBS_SEARCH_MULTIPLE_ENTRIES,2)) {
					pSearchKey = pSearchKey.Replace("　"," ");
					string[] sArray = pSearchKey.Split(' ');
					string sInnerWhere;
					List<string> oInnerList = new List<string>();

					sInnerWhere = string.Empty;
					for (int h = 1;h <= sArray.Length;h++) {
						SysPrograms.SqlAppendWhere(string.Format("P.BBS_THREAD_TITLE LIKE '%' || :BBS_THREAD_TITLE_{0} || '%'",h.ToString()),ref sInnerWhere);
						list.Add(new OracleParameter(string.Format("BBS_THREAD_TITLE_{0}",h.ToString()),sArray[h - 1]));
					}
					sInnerWhere = sInnerWhere.Replace("WHERE","");
					oInnerList.Add(string.Format("({0})",sInnerWhere));

					for (int i = 1;i <= 4;i++) {
						sInnerWhere = string.Empty;
						for (int j = 1;j <= sArray.Length;j++) {
							SysPrograms.SqlAppendWhere(string.Format("P.BBS_THREAD_DOC{0} LIKE '%' || :BBS_THREAD_DOC{0}_{1} || '%'",i.ToString(),j.ToString()),ref sInnerWhere);
							list.Add(new OracleParameter(string.Format("BBS_THREAD_DOC{0}_{1}",i.ToString(),j.ToString()),sArray[j - 1]));
						}
						sInnerWhere = sInnerWhere.Replace("WHERE","");
						oInnerList.Add(string.Format("({0})",sInnerWhere));
					}
					string[] oSqlArray = oInnerList.ToArray();
					SysPrograms.SqlAppendWhere(string.Format(" ( {0} ) ",string.Join(" OR ",oSqlArray)),ref pWhere);
				} else {
					oSql.AppendLine(" (	P.BBS_THREAD_TITLE	 LIKE '%' || :BBS_THREAD_TITLE	|| '%'	OR	");
					oSql.AppendLine("	P.BBS_THREAD_DOC1	 LIKE '%' || :BBS_THREAD_DOC1	|| '%'	OR	");
					oSql.AppendLine("	P.BBS_THREAD_DOC2	 LIKE '%' || :BBS_THREAD_DOC2	|| '%'	OR	");
					oSql.AppendLine("	P.BBS_THREAD_DOC3	 LIKE '%' || :BBS_THREAD_DOC3	|| '%'	OR	");
					oSql.AppendLine("	P.BBS_THREAD_DOC4	 LIKE '%' || :BBS_THREAD_DOC4	|| '%'	)");
					SysPrograms.SqlAppendWhere(oSql.ToString(),ref pWhere);
					list.Add(new OracleParameter("BBS_THREAD_TITLE",pSearchKey));
					list.Add(new OracleParameter("BBS_THREAD_DOC1",pSearchKey));
					list.Add(new OracleParameter("BBS_THREAD_DOC2",pSearchKey));
					list.Add(new OracleParameter("BBS_THREAD_DOC3",pSearchKey));
					list.Add(new OracleParameter("BBS_THREAD_DOC4",pSearchKey));
				}
			}
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool GetValue(string pSiteCd,string pBbsThreadSeq,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"* " +
							"FROM " +
								"T_BBS_THREAD " +
							"WHERE " +
								"SITE_CD		= :SITE_CD			AND	" +
								"BBS_THREAD_SEQ	= :BBS_THREAD_SEQ	";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("BBS_THREAD_SEQ",pBbsThreadSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BBS_THREAD");
					if (ds.Tables["T_BBS_THREAD"].Rows.Count != 0) {
						dr = ds.Tables["T_BBS_THREAD"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void WriteBbsThread(string pSiteCd,string pUserSeq,string pUserCharNo,string pBbsThreadSeq,string pBbsThreadHandleNm,string pBbsThreadTitle,string pBbsThreadDoc,out string pNewBbsThreadSeq) {
		WriteBbsThreadBase(pSiteCd,pUserSeq,pUserCharNo,pBbsThreadSeq,pBbsThreadHandleNm,pBbsThreadTitle,pBbsThreadDoc,ViCommConst.FLAG_OFF,out pNewBbsThreadSeq);
	}

	public void DeleteBbsThread(string pSiteCd,string pUserSeq,string pUserCharNo,string pBbsThreadSeq) {
		string sNewBbsThreadSeq = string.Empty;
		WriteBbsThreadBase(pSiteCd,pUserSeq,pUserCharNo,pBbsThreadSeq,string.Empty,string.Empty,string.Empty,ViCommConst.FLAG_ON,out sNewBbsThreadSeq);
	}

	private void WriteBbsThreadBase(string pSiteCd,string pUserSeq,string pUserCharNo,string pBbsThreadSeq,string pBbsThreadHandleNm,string pBbsThreadTitle,string pBbsThreadDoc,int pDelFlag,out string pNewBbsThreadSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_BBS_THREAD");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pBBS_THREAD_SEQ",DbSession.DbType.VARCHAR2,pBbsThreadSeq);
			db.ProcedureInParm("pBBS_THREAD_HANDLE_NM",DbSession.DbType.VARCHAR2,pBbsThreadHandleNm);
			db.ProcedureInParm("pBBS_THREAD_TITLE",DbSession.DbType.VARCHAR2,pBbsThreadTitle);
			db.ProcedureInParm("pBBS_THREAD_DOC",DbSession.DbType.VARCHAR2,pBbsThreadDoc);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pNEW_BBS_THREAD_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pNewBbsThreadSeq = db.GetStringValue("pNEW_BBS_THREAD_SEQ");
		}
	}

	public void AccessBbsThread(string pBbsThreadSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_BBS_THREAD");
			db.ProcedureInParm("pBBS_THREAD_SEQ",DbSession.DbType.VARCHAR2,pBbsThreadSeq);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public bool IsExist(string pSiteCd,string pBbsThreadSeq) {
		//スレッドが会員から見た時に存在しているかチェック
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect("BbsThread.IsExist");

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine(" SELECT												");
			oSqlBuilder.AppendLine(" 	BBS_THREAD_SEQ									");
			oSqlBuilder.AppendLine(" FROM												");
			oSqlBuilder.AppendLine(" 	T_BBS_THREAD									");
			oSqlBuilder.AppendLine(" WHERE												");
			oSqlBuilder.AppendLine(" 	SITE_CD				= :SITE_CD				AND	");
			oSqlBuilder.AppendLine("	BBS_THREAD_SEQ		= :BBS_THREAD_SEQ		AND	");
			oSqlBuilder.AppendLine("	DEL_FLAG			= :DEL_FLAG					");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("BBS_THREAD_SEQ",pBbsThreadSeq);
				cmd.Parameters.Add("DEL_FLAG",ViCommConst.FLAG_OFF);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BBS_THREAD");
					if (ds.Tables["T_BBS_THREAD"].Rows.Count != 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
