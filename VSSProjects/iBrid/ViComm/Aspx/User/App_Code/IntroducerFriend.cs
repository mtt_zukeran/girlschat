﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 友達紹介


--	Progaram ID		: IntroducerFriend
--
--  Creation Date	: 2012.02.20
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class IntroducerFriendSeekCondition : SeekConditionBase {
	private string sSiteCd;
	private string sUserSeq;
	private string sUserCharNo;
	private string sIntroducerFriendCd;


	public string SiteCd {
		get {
			return this.sSiteCd;
		}
		set {
			this.sSiteCd = value;
		}
	}
	public string UserSeq {
		get {
			return this.sUserSeq;
		}
		set {
			this.sUserSeq = value;
		}
	}
	public string UserCharNo {
		get {
			return this.sUserCharNo;
		}
		set {
			this.sUserCharNo = value;
		}
	}
	public string IntroducerFriendCd {
		get {
			return this.sIntroducerFriendCd;
		}
		set {
			this.sIntroducerFriendCd = value;
		}
	}

	public IntroducerFriendSeekCondition()
		: this(new NameValueCollection()) { }
	public IntroducerFriendSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.sSiteCd = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.sUserSeq = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.sUserCharNo = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["charno"]));
		this.sIntroducerFriendCd = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["intro_friend_cd"]));
	}
}

/// <summary>
/// CastIntroducerFriend の概要の説明です
/// </summary>
public class CastIntroducerFriend:DbSession {
	public CastIntroducerFriend() { }


	public int GetPageCount(IntroducerFriendSeekCondition pCondition, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("	SELECT																		");
		oSqlBuilder.AppendLine("		COUNT(*)	AS CNT														");
		oSqlBuilder.AppendLine("	FROM																		");
		oSqlBuilder.AppendLine("		(																		");
		oSqlBuilder.AppendLine("			SELECT																");
		oSqlBuilder.AppendLine("				T_USER.USER_SEQ													");
		oSqlBuilder.AppendLine("			FROM																");
		oSqlBuilder.AppendLine("				T_USER															");
		oSqlBuilder.AppendLine("			WHERE																");
		oSqlBuilder.AppendLine("				T_USER.INTRODUCER_FRIEND_CD	= :INTRODUCER_FRIEND_CD			AND	");
		oSqlBuilder.AppendLine("				T_USER.SEX_CD				= :SEX_CAST							");
		oSqlBuilder.AppendLine("		)T1_CAST															,	");
		oSqlBuilder.AppendLine("		T_CAST																,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER														");
		oSqlBuilder.AppendLine("	WHERE																		");
		oSqlBuilder.AppendLine("		T1_CAST.USER_SEQ				= T_CAST.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.SITE_CD		= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.USER_SEQ		= T1_CAST.USER_SEQ					AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.USER_CHAR_NO	= :USER_CHAR_NO						AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.NA_FLAG		= :NA_FLAG								");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter(":INTRODUCER_FRIEND_CD", pCondition.IntroducerFriendCd));
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
				cmd.Parameters.Add(new OracleParameter(":USER_CHAR_NO", ViCommConst.MAIN_CHAR_NO));
				cmd.Parameters.Add(new OracleParameter(":SEX_CAST", ViCommConst.OPERATOR));
				cmd.Parameters.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(IntroducerFriendSeekCondition pCondition, int pPageNo, int pRecPerPage) {
		int iStartIndex = (pPageNo  - 1) * pRecPerPage;
		
		DataSet oDataSet = new DataSet();
		StringBuilder oSqlBuilder = new StringBuilder();


		oSqlBuilder.AppendLine("	SELECT																																				");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.SITE_CD																													,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.USER_SEQ																													,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.USER_CHAR_NO																												,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.HANDLE_NM																													,	");
		oSqlBuilder.AppendLine("		GET_PHOTO_IMG_PATH			(T_CAST_CHARACTER.SITE_CD,T1_CAST.LOGIN_ID,T_CAST_CHARACTER.PIC_SEQ)				AS PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("		GET_SMALL_PHOTO_IMG_PATH	(T_CAST_CHARACTER.SITE_CD,T1_CAST.LOGIN_ID,T_CAST_CHARACTER.PIC_SEQ)				AS SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.PIC_SEQ																													,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.AGE																														,	");
		oSqlBuilder.AppendLine("		T1_CAST.LOGIN_ID																															,	");
		oSqlBuilder.AppendLine("		T1_CAST.SEX_CD																																,	");
		oSqlBuilder.AppendLine("		T1_CAST.REGIST_DATE																																");
		oSqlBuilder.AppendLine("	FROM																																				");
		oSqlBuilder.AppendLine("		(																																				");
		oSqlBuilder.AppendLine("			SELECT 																																		");
		oSqlBuilder.AppendLine("				T_USER.USER_SEQ																														,	");
		oSqlBuilder.AppendLine("				T_USER.LOGIN_ID																														,	");
		oSqlBuilder.AppendLine("				T_USER.SEX_CD																														,	");
		oSqlBuilder.AppendLine("				T_USER.REGIST_DATE																														");
		oSqlBuilder.AppendLine("			FROM																																		");
		oSqlBuilder.AppendLine("				T_USER																																	");
		oSqlBuilder.AppendLine("			WHERE																																		");
		oSqlBuilder.AppendLine("				T_USER.INTRODUCER_FRIEND_CD		= :INTRODUCER_FRIEND_CD																				AND	");
		oSqlBuilder.AppendLine("				T_USER.SEX_CD					= :SEX_CAST																								");
		oSqlBuilder.AppendLine("		)T1_CAST																																	,	");
		oSqlBuilder.AppendLine("		T_CAST																																		,	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER																																");
		oSqlBuilder.AppendLine("	WHERE																																				");
		oSqlBuilder.AppendLine("		T1_CAST.USER_SEQ				= T_CAST.USER_SEQ																							AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.SITE_CD		= :SITE_CD																									AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.USER_SEQ		= T1_CAST.USER_SEQ																							AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.USER_CHAR_NO	= :USER_CHAR_NO																								AND	");
		oSqlBuilder.AppendLine("		T_CAST_CHARACTER.NA_FLAG		= :NA_FLAG																										");

		string sSortExpression = "ORDER BY  REGIST_DATE DESC,SITE_CD, USER_SEQ, USER_CHAR_NO";
		string sPagingSql = string.Empty;
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, iStartIndex, pRecPerPage, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter(":INTRODUCER_FRIEND_CD", pCondition.IntroducerFriendCd));
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
				cmd.Parameters.Add(new OracleParameter(":USER_CHAR_NO", ViCommConst.MAIN_CHAR_NO));
				cmd.Parameters.Add(new OracleParameter(":SEX_CAST", ViCommConst.OPERATOR));
				cmd.Parameters.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}

		return oDataSet;
	}
}


/// <summary>
/// ManIntroducerFriend の概要の説明です
/// </summary>
public class ManIntroducerFriend : DbSession {
	public ManIntroducerFriend() { }

	public int GetPageCount(IntroducerFriendSeekCondition pCondition, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	COUNT(*)	AS CNT														");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			T_USER.USER_SEQ													");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER															");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			T_USER.INTRODUCER_FRIEND_CD	= :INTRODUCER_FRIEND_CD			AND	");
		oSqlBuilder.AppendLine("			T_USER.SEX_CD				= :SEX_MAN							");
		oSqlBuilder.AppendLine("	)T1_MAN																,	");
		oSqlBuilder.AppendLine("	T_USER_MAN															,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER													");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	T1_MAN.USER_SEQ						= T_USER_MAN.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.SITE_CD		= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_SEQ		= T1_MAN.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.NA_FLAG		= :NA_FLAG						AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			1																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_REFUSE														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.SITE_CD		= T_REFUSE.SITE_CD		AND	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.USER_SEQ		= T_REFUSE.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.USER_CHAR_NO	= T_REFUSE.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			T_REFUSE.PARTNER_USER_SEQ			= :PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_REFUSE.PARTNER_USER_CHAR_NO		= :PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	)																		");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter(":INTRODUCER_FRIEND_CD", pCondition.IntroducerFriendCd));
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
				cmd.Parameters.Add(new OracleParameter(":USER_CHAR_NO", ViCommConst.MAIN_CHAR_NO));
				cmd.Parameters.Add(new OracleParameter(":SEX_MAN", ViCommConst.MAN));
				cmd.Parameters.Add(new OracleParameter(":PARTNER_USER_SEQ", pCondition.UserSeq));
				cmd.Parameters.Add(new OracleParameter(":PARTNER_USER_CHAR_NO", pCondition.UserCharNo));
				cmd.Parameters.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(IntroducerFriendSeekCondition pCondition, int pPageNo, int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		DataSet oDataSet = new DataSet();
		StringBuilder oSqlBuilder = new StringBuilder();


		oSqlBuilder.AppendLine("	SELECT																																				");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.SITE_CD																												,	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.USER_SEQ																												,	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.USER_CHAR_NO																											,	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.HANDLE_NM																												,	");
		oSqlBuilder.AppendLine("		GET_MAN_PHOTO_IMG_PATH		(T_USER_MAN_CHARACTER.SITE_CD,T1_MAN.LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0)		AS PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("		GET_MAN_SMALL_PHOTO_IMG_PATH(T_USER_MAN_CHARACTER.SITE_CD,T1_MAN.LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0)		AS SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.PIC_SEQ																												,	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.AGE																													,	");
		oSqlBuilder.AppendLine("		T_USER_MAN.TOTAL_RECEIPT_AMT																												,	");
		oSqlBuilder.AppendLine("		T_USER_MAN.REGIST_SERVICE_POINT_FLAG																										,	");
		oSqlBuilder.AppendLine("		T1_MAN.LOGIN_ID																																,	");
		oSqlBuilder.AppendLine("		T1_MAN.SEX_CD																																,	");
		oSqlBuilder.AppendLine("		T1_MAN.REGIST_DATE																																");
		oSqlBuilder.AppendLine("	FROM																																				");
		oSqlBuilder.AppendLine("		(																																				");
		oSqlBuilder.AppendLine("			SELECT																																		");
		oSqlBuilder.AppendLine("				T_USER.USER_SEQ																														,	");
		oSqlBuilder.AppendLine("				T_USER.LOGIN_ID																														,	");
		oSqlBuilder.AppendLine("				T_USER.SEX_CD																														,	");
		oSqlBuilder.AppendLine("				T_USER.REGIST_DATE																														");
		oSqlBuilder.AppendLine("			FROM																																		");
		oSqlBuilder.AppendLine("				T_USER																																	");
		oSqlBuilder.AppendLine("			WHERE																																		");
		oSqlBuilder.AppendLine("				T_USER.INTRODUCER_FRIEND_CD		= :INTRODUCER_FRIEND_CD																				AND	");
		oSqlBuilder.AppendLine("				T_USER.SEX_CD					= :SEX_MAN																								");
		oSqlBuilder.AppendLine("		)T1_MAN																																		,	");
		oSqlBuilder.AppendLine("		T_USER_MAN																																	,	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER																															");
		oSqlBuilder.AppendLine("	WHERE																																				");
		oSqlBuilder.AppendLine("		T1_MAN.USER_SEQ						= T_USER_MAN.USER_SEQ																					AND	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.SITE_CD		= :SITE_CD																								AND	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.USER_SEQ		= T1_MAN.USER_SEQ																						AND	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.USER_CHAR_NO	= :USER_CHAR_NO																							AND	");
		oSqlBuilder.AppendLine("		T_USER_MAN_CHARACTER.NA_FLAG		= :NA_FLAG																								AND	");
		oSqlBuilder.AppendLine("		NOT EXISTS																																		");
		oSqlBuilder.AppendLine("		(																																				");
		oSqlBuilder.AppendLine("			SELECT																																		");
		oSqlBuilder.AppendLine("				1																																		");
		oSqlBuilder.AppendLine("			FROM																																		");
		oSqlBuilder.AppendLine("				T_REFUSE																																");
		oSqlBuilder.AppendLine("			WHERE																																		");
		oSqlBuilder.AppendLine("				T_USER_MAN_CHARACTER.SITE_CD		= T_REFUSE.SITE_CD																				AND	");
		oSqlBuilder.AppendLine("				T_USER_MAN_CHARACTER.USER_SEQ		= T_REFUSE.USER_SEQ																				AND	");
		oSqlBuilder.AppendLine("				T_USER_MAN_CHARACTER.USER_CHAR_NO	= T_REFUSE.USER_CHAR_NO																			AND	");
		oSqlBuilder.AppendLine("				T_REFUSE.PARTNER_USER_SEQ			= :PARTNER_USER_SEQ																				AND	");
		oSqlBuilder.AppendLine("				T_REFUSE.PARTNER_USER_CHAR_NO		= :PARTNER_USER_CHAR_NO																				");
		oSqlBuilder.AppendLine("		)																																				");

		string sSortExpression = "ORDER BY  REGIST_DATE DESC,SITE_CD, USER_SEQ, USER_CHAR_NO";
		string sPagingSql = string.Empty;
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, iStartIndex, pRecPerPage, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter(":INTRODUCER_FRIEND_CD", pCondition.IntroducerFriendCd));
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
				cmd.Parameters.Add(new OracleParameter(":USER_CHAR_NO", ViCommConst.MAIN_CHAR_NO));
				cmd.Parameters.Add(new OracleParameter(":SEX_MAN", ViCommConst.MAN));
				cmd.Parameters.Add(new OracleParameter(":PARTNER_USER_SEQ", pCondition.UserSeq));
				cmd.Parameters.Add(new OracleParameter(":PARTNER_USER_CHAR_NO", pCondition.UserCharNo));
				cmd.Parameters.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}

		return oDataSet;
	}

	public int GetReceiptUserCount(IntroducerFriendSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	COUNT(*)	AS CNT														");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			T_USER.USER_SEQ													");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER															");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			T_USER.INTRODUCER_FRIEND_CD	= :INTRODUCER_FRIEND_CD			AND	");
		oSqlBuilder.AppendLine("			T_USER.SEX_CD				= :SEX_MAN							");
		oSqlBuilder.AppendLine("	)T1_MAN																,	");
		oSqlBuilder.AppendLine("	T_USER_MAN															,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER													");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	T1_MAN.USER_SEQ						= T_USER_MAN.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.SITE_CD		= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_SEQ		= T1_MAN.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.NA_FLAG		= :NA_FLAG						AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN.TOTAL_RECEIPT_COUNT		> 0								AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			1																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_REFUSE														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.SITE_CD		= T_REFUSE.SITE_CD		AND	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.USER_SEQ		= T_REFUSE.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.USER_CHAR_NO	= T_REFUSE.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			T_REFUSE.PARTNER_USER_SEQ			= :PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_REFUSE.PARTNER_USER_CHAR_NO		= :PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	)																		");
		
		oParamList.Add(new OracleParameter(":INTRODUCER_FRIEND_CD",pCondition.IntroducerFriendCd));
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		oParamList.Add(new OracleParameter(":SEX_MAN",ViCommConst.MAN));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));
		
		return ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	public DataSet GetNotCheckIntroCount(IntroducerFriendSeekCondition pCondition,DateTime pUserIntroCountCheckDate) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	COUNT(*)	AS NOT_CHECK_MAN_INTRO_COUNT								");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			T_USER.USER_SEQ													");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER															");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			T_USER.INTRODUCER_FRIEND_CD	= :INTRODUCER_FRIEND_CD			AND	");
		oSqlBuilder.AppendLine("			T_USER.SEX_CD				= :SEX_MAN						AND	");
		oSqlBuilder.AppendLine("			T_USER.REGIST_DATE			> :USER_INTRO_COUNT_CHECK_DATE		");
		oSqlBuilder.AppendLine("	)T1_MAN																,	");
		oSqlBuilder.AppendLine("	T_USER_MAN															,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER													");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	T1_MAN.USER_SEQ						= T_USER_MAN.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.SITE_CD		= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_SEQ		= T1_MAN.USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_CHAR_NO	= :USER_CHAR_NO					AND	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.NA_FLAG		= :NA_FLAG						AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			1																");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_REFUSE														");
		oSqlBuilder.AppendLine("		WHERE																");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.SITE_CD		= T_REFUSE.SITE_CD		AND	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.USER_SEQ		= T_REFUSE.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_USER_MAN_CHARACTER.USER_CHAR_NO	= T_REFUSE.USER_CHAR_NO	AND	");
		oSqlBuilder.AppendLine("			T_REFUSE.PARTNER_USER_SEQ			= :PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("			T_REFUSE.PARTNER_USER_CHAR_NO		= :PARTNER_USER_CHAR_NO		");
		oSqlBuilder.AppendLine("	)																		");

		oParamList.Add(new OracleParameter(":INTRODUCER_FRIEND_CD",pCondition.IntroducerFriendCd));
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		oParamList.Add(new OracleParameter(":SEX_MAN",ViCommConst.MAN));
		oParamList.Add(new OracleParameter(":USER_INTRO_COUNT_CHECK_DATE",pUserIntroCountCheckDate));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.UserCharNo));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

}
