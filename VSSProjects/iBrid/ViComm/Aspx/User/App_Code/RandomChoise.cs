﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: RandomChoise
--	Progaram ID		: RandomChoise
--
--  Creation Date	: 2011.06.03
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class RandomChoise : DbSession{
	public RandomChoise() {}
	
	public string siteCd  = null;
	public string userSeq = null;
	public string userCharNo = null;
	
	public IList<RandomChoise> GetRandomChoise(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,string pRandomChoiseType,int pNeedCount) {
		
		IList<RandomChoise> oRandomChoiseList = new List<RandomChoise>();
		
		using(DbSession oDbSession = new DbSession()){
			oDbSession.PrepareProcedure("GET_RANDOM_CHOICE");
			oDbSession.ProcedureInParm("pSITE_CD", DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbType.VARCHAR2, pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbType.VARCHAR2, pUserCharNo);
			oDbSession.ProcedureInParm("pSEX_CD", DbType.VARCHAR2, pSexCd);
			oDbSession.ProcedureInParm("pRANDOM_COHICE_TYPE", DbType.VARCHAR2, pRandomChoiseType);
			oDbSession.ProcedureInParm("pNEED_COUNT", DbType.NUMBER, pNeedCount);
			oDbSession.ProcedureOutArrayParm("pPARTNER_USER_SEQ", DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pPARTNER_USER_CHAR_NO", DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbType.VARCHAR2);
			
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			
			int iRecordCount = oDbSession.GetIntValue("pRECORD_COUNT");
			for(int i=0;i<iRecordCount;i++){
				string sUserSeq = oDbSession.GetArryStringValue("pPARTNER_USER_SEQ", i);
				string sUserCharNo = oDbSession.GetArryStringValue("pPARTNER_USER_CHAR_NO", i);
				
				RandomChoise oEntity = new RandomChoise();
				oEntity.siteCd = pSiteCd;
				oEntity.userSeq = sUserSeq;
				oEntity.userCharNo = sUserCharNo;
				
				oRandomChoiseList.Add(oEntity);
			}
		}
		
		return oRandomChoiseList;
	}
}
