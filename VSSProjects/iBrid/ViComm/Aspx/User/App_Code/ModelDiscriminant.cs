﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 携帯機種判別定義
--	Progaram ID		: ModelDiscriminant
--  Creation Date	: 2010.06.18
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using System;
using iBridCommLib;
using ViComm;


/// <summary>
/// T_MODEL_DISCRIMINANTへのデータアクセスを提供するクラス。

/// </summary>
[System.Serializable]
public class ModelDiscriminant:DbSession {

	public ModelDiscriminant(){}

	/// <summary>
	/// HTTP-USER-AGENTから機種名を抽出する。

	/// </summary>
	/// <remarks>
	/// ただし、auはデバイスID
	/// </remarks>
	/// <param name="pMobileCarrierCd">携帯ｷｬﾘｱ種別</param>
	/// <param name="pUserAgent">HTTP-USER-AGENT</param>
	/// <returns>機種名</returns>
	public string GetModelName(string pMobileCarrierCd, string pUserAgent) {
		
		string modelName = null;
		
		using(DataTable dt = new DataTable()) {
		
			// C#コード自動作成
			StringBuilder objSqlContent = new StringBuilder();
			objSqlContent.Append(" SELECT                        ");
			objSqlContent.Append("     PATTERN                   ");
			objSqlContent.Append(" FROM                          ");
			objSqlContent.Append("     T_MODEL_DISCRIMINANT      ");
			objSqlContent.Append(" WHERE                         ");
			objSqlContent.Append("     MOBILE_CARRIER_CD = :MOBILE_CARRIER_CD ");
			objSqlContent.Append(" ORDER BY                      ");
			objSqlContent.Append("     EVALUATION_ORDER          ");
			
			try {
				conn = DbConnect();

				using (cmd = CreateSelectCommand(objSqlContent.ToString(),conn)) {
					cmd.Parameters.Add("MOBILE_CARRIER_CD", pMobileCarrierCd);
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dt);
					}
				}
				
			}finally {
				conn.Close();
			}
			
			foreach(DataRow dr in dt.Rows) {
				Match match = Regex.Match(pUserAgent,(string)dr["PATTERN"]);
		
				if(match.Groups["model"].Success) {
					modelName = match.Groups["model"].Value;
					break;
				}
			}
		}

		return modelName;
	}
}
