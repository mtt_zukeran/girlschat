﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 料金パック
--	Progaram ID		: Pack
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class Pack:DbSession {


	public string siteCd;
	public string settleType;
	public int salesAmt;
	public int exPoint;
	public int firstSettleServicePoint;
	public string commoditiesCd;
	public string remarks;

	public Pack() {
		settleType = "";
		salesAmt = 0;
		exPoint = 0;
		firstSettleServicePoint = 0;
		commoditiesCd = "";
	}

	public bool GetOne(string pSiteCd,string pSettleType,string pSalesAmt,string pUserSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("Pack.GetOne");

			using (cmd = CreateSelectCommand("SELECT * FROM T_PACK WHERE SITE_CD = :SITE_CD AND SETTLE_TYPE =:SETTLE_TYPE AND SALES_AMT =:SALES_AMT ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_TYPE",pSettleType);
				cmd.Parameters.Add("SALES_AMT",pSalesAmt);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PACK");
					if (ds.Tables["T_PACK"].Rows.Count != 0) {
						dr = ds.Tables["T_PACK"].Rows[0];
						siteCd = dr["SITE_CD"].ToString();
						settleType = dr["SETTLE_TYPE"].ToString();
						salesAmt = int.Parse(dr["SALES_AMT"].ToString());
						commoditiesCd = iBridUtil.GetStringValue(dr["COMMODITIES_CD"]);
						remarks = iBridUtil.GetStringValue(dr["REMARKS"]);
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		if (bExist) {
			exPoint = (int)CalcServicePointImpl(siteCd,pUserSeq,settleType,salesAmt,out firstSettleServicePoint);
		}

		return bExist;
	}



	public DataSet GetList(string pSiteCd,string pSettleType,int pMinSalesAmt,string pUserSeq) {
		DataSet ds;
		try {
			conn = DbConnect("Pack.GetList");
			ds = new DataSet();

			string sSql = "SELECT * FROM T_PACK WHERE SITE_CD = :SITE_CD AND SETTLE_TYPE =:SETTLE_TYPE AND SALES_AMT > :SALES_AMT ";
			sSql = sSql + " ORDER BY SITE_CD,SETTLE_TYPE,SALES_AMT";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_TYPE",pSettleType);
				cmd.Parameters.Add("SALES_AMT",pMinSalesAmt);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
			CalcServicePoint(pUserSeq,ds);
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetNonFirstList(string pSiteCd,string pSettleType,int pMinSalesAmt,string pUserSeq) {
		DataSet ds;
		try {
			conn = DbConnect("Pack.GetList");
			ds = new DataSet();

			string sSql = "SELECT * FROM T_PACK WHERE SITE_CD = :SITE_CD AND SETTLE_TYPE =:SETTLE_TYPE AND SALES_AMT > :SALES_AMT AND FIRST_TIME_FLAG = 0";
			sSql = sSql + " ORDER BY SITE_CD,SETTLE_TYPE,SALES_AMT";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_TYPE",pSettleType);
				cmd.Parameters.Add("SALES_AMT",pMinSalesAmt);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
			CalcServicePoint(pUserSeq,ds);
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetAfterPackList(string pSiteCd,int pCreditBalance,string pUserSeq) {
		DataSet ds;
		try {
			conn = DbConnect("Pack.GetList");
			ds = new DataSet();

			string sSql = "SELECT * FROM T_PACK WHERE SITE_CD = :SITE_CD AND SETTLE_TYPE =:SETTLE_TYPE AND SALES_AMT <= :SALES_AMT ";
			sSql = sSql + " ORDER BY SITE_CD,SETTLE_TYPE,SALES_AMT";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_TYPE",ViCommConst.SETTLE_PAYMENT_AFTER_PACK);
				cmd.Parameters.Add("SALES_AMT",pCreditBalance);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
			CalcServicePoint(pUserSeq,ds);
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void CalcServicePoint(string pUserSeq,DataSet pDataSet) {
		if (pDataSet.Tables.Count > 0) {
			this.CalcServicePoint(pUserSeq,pDataSet.Tables[0]);
		}
	}

	public void CalcServicePoint(string pUserSeq,DataTable pDataTable) {
		if (pDataTable.Rows.Count <= 0) {
			return;
		}
		if (!pDataTable.Columns.Contains("EX_POINT")) {
			DataColumn col = new DataColumn("EX_POINT",System.Type.GetType("System.String"));
			pDataTable.Columns.Add(col);
		}
		
		if (!pDataTable.Columns.Contains("SERVICE_POINT")) {
			DataColumn col = new DataColumn("SERVICE_POINT",System.Type.GetType("System.String"));
			pDataTable.Columns.Add(col);
		}
		
		foreach (DataRow oDataRow in pDataTable.Rows) {
			string sSettleType = iBridUtil.GetStringValue(oDataRow["SETTLE_TYPE"]);
			string sSiteCd = iBridUtil.GetStringValue(oDataRow["SITE_CD"]);
			decimal dSalesAmt = decimal.Parse(iBridUtil.GetStringValue(oDataRow["SALES_AMT"]));
			string sRemarks = iBridUtil.GetStringValue(oDataRow["REMARKS"]);
			int iRegistServicePoint = 0;

			decimal dServicePoint = this.CalcServicePointImpl(sSiteCd,pUserSeq,sSettleType,dSalesAmt,out iRegistServicePoint);
			oDataRow["EX_POINT"] = dServicePoint.ToString();
			oDataRow["REMARKS"] = string.Format(sRemarks,dServicePoint);
			oDataRow["SERVICE_POINT"] = iRegistServicePoint.ToString();
		}
	}
	
	public int CalcServicePointCreditAutoSettle(string pSiteCd,string pUserSeq,decimal pReceiptAmt) {
		int iServicePoint = 0;
		CalcServicePointImpl(pSiteCd,pUserSeq,ViCommConst.SETTLE_CREDIT_PACK,pReceiptAmt,out iServicePoint);
		return iServicePoint;
	}

	private decimal CalcServicePointImpl(string pSiteCd,string pUserSeq,string pSettleType,decimal pReceiptAmt,out int pServicePoint) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CALC_SERVICE_POINT");
			oDbSession.ProcedureInParm("pSITE_CD",DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pSETTLE_TYPE",DbType.VARCHAR2,pSettleType);
			oDbSession.ProcedureInParm("pRECEIPT_AMT",DbType.NUMBER,pReceiptAmt);
			oDbSession.ProcedureOutParm("pTOTAL_EX_POINT",DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSERVICE_POINT",DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			pServicePoint = oDbSession.GetIntValue("pSERVICE_POINT");

			return oDbSession.GetDecimalValue("pTOTAL_EX_POINT");
		}
	}

	public DataSet GetAfterNonFirstPackList(string pSiteCd,int pCreditBalance,string pUserSeq) {
		DataSet ds;
		try {
			conn = DbConnect("Pack.GetList");
			ds = new DataSet();

			string sSql = "SELECT * FROM T_PACK WHERE SITE_CD = :SITE_CD AND SETTLE_TYPE =:SETTLE_TYPE AND SALES_AMT <= :SALES_AMT AND FIRST_TIME_FLAG = 0";
			sSql = sSql + " ORDER BY SITE_CD,SETTLE_TYPE,SALES_AMT";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_TYPE",ViCommConst.SETTLE_PAYMENT_AFTER_PACK);
				cmd.Parameters.Add("SALES_AMT",pCreditBalance);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
			CalcServicePoint(pUserSeq,ds);
		} finally {
			conn.Close();
		}
		return ds;
	}

}
