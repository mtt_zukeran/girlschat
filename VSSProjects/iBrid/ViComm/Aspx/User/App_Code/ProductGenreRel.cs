﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品ジャンル関連

--	Progaram ID		: ProductGenreRel
--
--  Creation Date	: 2010.12.20
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductGenreRel:DbSession {
	public ProductGenreRel() {
	}

	public DataSet GetList(string pSiteCd,string pProductAgentCd,string pProductSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT ");
		oSqlBuilder.AppendLine("	SITE_CD						,	");
		oSqlBuilder.AppendLine("	PRODUCT_AGENT_CD            ,	");
		oSqlBuilder.AppendLine("	PRODUCT_SEQ                 ,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_CD            ,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_NM            ,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_CATEGORY_CD   ,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_CATEGORY_NM   	");
		oSqlBuilder.AppendLine(" FROM ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01 ");
		oSqlBuilder.AppendLine(" WHERE ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.SITE_CD			= :SITE_CD			AND ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.PRODUCT_AGENT_CD = :PRODUCT_AGENT_CD	AND ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.PRODUCT_SEQ		= :PRODUCT_SEQ		AND	");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.PUBLISH_FLAG		= :PUBLISH_FLAG			");
		
		oSqlBuilder.AppendLine(" ORDER BY ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.SITE_CD						, ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.PRODUCT_AGENT_CD				, ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.PRODUCT_SEQ					, ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_GENRE_REL01.PRODUCT_GENRE_CATEGORY_CD	  ");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD",pProductAgentCd);
				cmd.Parameters.Add(":PRODUCT_SEQ",pProductSeq);
				cmd.Parameters.Add(":PUBLISH_FLAG",ViCommConst.FLAG_ON);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}
