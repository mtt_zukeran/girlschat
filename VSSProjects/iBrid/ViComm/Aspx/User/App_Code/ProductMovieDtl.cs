﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductMovieDtl : DbSession {
	public ProductMovieDtl() {}
	
	public DataSet GetList(string pObjSeq, string pCarrierCd){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT												");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.SITE_CD              ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.PRODUCT_AGENT_CD     ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.PRODUCT_SEQ          ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.OBJ_SEQ              ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.CP_SIZE_TYPE         ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.CP_FILE_FORMAT       ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.CP_TARGET_CARRIER    ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.PRODUCT_MOVIE_SUBSEQ ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.FILE_NM              ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.FILE_SIZE            ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.DOWNLOAD_URL             ");
		oSqlBuilder.AppendLine(" FROM                                               ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02                          ");
		oSqlBuilder.AppendLine(" WHERE                                              ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.OBJ_SEQ				= :OBJ_SEQ			AND	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.CP_TARGET_CARRIER	= :CP_TARGET_CARRIER       ");
		oSqlBuilder.AppendLine(" ORDER BY                                       ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.SITE_CD				,");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.PRODUCT_AGENT_CD		,");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.PRODUCT_SEQ			,");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.OBJ_SEQ				,");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.CP_SIZE_TYPE			,");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.CP_FILE_FORMAT		,");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.CP_TARGET_CARRIER	,");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL02.PRODUCT_MOVIE_SUBSEQ	");
		
		DataSet oDataSet = new DataSet();
		try{
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":OBJ_SEQ", pObjSeq);
				this.cmd.Parameters.Add(":CP_TARGET_CARRIER", ConvertCarrierCd2TargetCarrier(pCarrierCd));
				using(da = new OracleDataAdapter(this.cmd)){
					da.Fill(oDataSet);
				}
			}
		}finally{
			this.conn.Close();
		}
		return oDataSet;
	}
	
	private string ConvertCarrierCd2TargetCarrier(string pCarrierCd){
		switch(pCarrierCd){
			case ViCommConst.KDDI:
				return ViCommConst.FILE_SUFFIX_AU;
			case ViCommConst.SOFTBANK:
				return ViCommConst.FILE_SUFFIX_SOFTBANK;
			default:
				return ViCommConst.FILE_SUFFIX_DOCOMO;
		}
	}
}
