﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ゲームキャラクター
--	Progaram ID		: GameCharacter
--
--  Creation Date	: 2011.07.20
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date		Updater			Update Explain
  12/02/17	PW K.Miyazato	UpdateLastHugLogCheck()を追加
  12/02/17	PW K.Miyazato	UpdateLastPresentLogCheck()を追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

using System.Collections.Specialized;
using System.Collections.Generic;

[Serializable]
public class GameCharacterSeekCondition:SeekConditionBase {
	public string SiteCd;
	public string SexCd;

	public GameCharacterSeekCondition()
		: this(new NameValueCollection()) {
	}

	public GameCharacterSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[System.Serializable]
public class GameCharacter:DbSession {
	public bool gameLogined;
	public bool gameFirstLogined;//ﾚﾎﾟｰﾄ用ﾌﾗｸﾞ 
	public string gameHandleNm;
	public string gameCharacterType;
	public int gamePoint;
	public int exp;
	public int gameCharacterLevel;
	public int unassignedForceCount;
	public string tutorialStatus;
	public string stageSeq;
	public int cooperationPoint;
	public int totalAttackWinCount;
	public int totalAttackLossCount;
	public int totalDefenceWinCount;
	public int totalDefenceLossCount;
	public int totalHelpSuccessCount;
	public int totalHelpFailCount;
	public int totalPartyAttackWinCount;
	public int totalPartyAttackLossCount;
	public int totalPartyDefenceWinCount;
	public int totalPartyDefenceLossCount;
	public int totalTreasureGetCount;
	public int totalTreasureLossCount;
	public string lastPartyAttackDate;
	public string dateExecDate;
	public string partTimeJobExecDate;
	public string lastHugLogCheckDate;
	public string lastPresentLogCheckDate;
	public int missionForceCount;
	public int missionMaxForceCount;
	public int missionNonCompRecoveryFlag;
	public int attackForceCount;
	public int attackMaxForceCount;
	public int defenceForceCount;
	public int defenceMaxForceCount;
	public int castGamePoint;
	public int stageLevel;
	public string stageHonor;
	public string stageGroupType;
	public int restExp;
	public int fellowCountLimit;
	public int fellowCount;
	public string lastHugBonusGetDate;
	public int maxAttackPower;
	public int maxDefencePower;

	public GameCharacter() {
		gameLogined = false;
		gameFirstLogined = false;
		gameHandleNm = string.Empty;
		gameCharacterType = string.Empty;
		gamePoint = 0;
		exp = 0;
		gameCharacterLevel = 0;
		unassignedForceCount = 0;
		tutorialStatus = string.Empty;
		stageSeq = string.Empty;
		cooperationPoint = 0;
		totalAttackWinCount = 0;
		totalAttackLossCount = 0;
		totalDefenceWinCount = 0;
		totalDefenceLossCount = 0;
		totalHelpSuccessCount = 0;
		totalHelpFailCount = 0;
		totalPartyAttackWinCount = 0;
		totalPartyAttackLossCount = 0;
		totalPartyDefenceWinCount = 0;
		totalPartyDefenceLossCount = 0;
		totalTreasureGetCount = 0;
		totalTreasureLossCount = 0;
		lastPartyAttackDate = string.Empty;
		dateExecDate = string.Empty;
		partTimeJobExecDate = string.Empty;
		lastHugLogCheckDate = string.Empty;
		lastPresentLogCheckDate = string.Empty;
		missionForceCount = 0;
		missionMaxForceCount = 0;
		missionNonCompRecoveryFlag = 0;
		attackForceCount = 0;
		attackMaxForceCount = 0;
		defenceForceCount = 0;
		defenceMaxForceCount = 0;
		castGamePoint = 0;
		stageLevel = 0;
		stageHonor = string.Empty;
		stageGroupType = string.Empty;
		
		restExp = 0;
		fellowCountLimit = 0;
		fellowCount = 0;
		lastHugBonusGetDate = string.Empty;
		maxAttackPower = 0;
		maxDefencePower = 0;
	}

	public bool GetCurrentInfo(string pSiteCd,string pUserSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("GameCharacter.GetCurrentInfo");

			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT	");
			oSql.AppendLine("	*	");
			oSql.AppendLine("FROM	");
			oSql.AppendLine("	VW_GAME_CHARACTER01	");
			oSql.AppendLine("WHERE	");
			oSql.AppendLine("	SITE_CD			= :SITE_CD		AND	");
			oSql.AppendLine("	USER_SEQ		= :USER_SEQ		AND	");
			oSql.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		");

			using (cmd = CreateSelectCommand(oSql.ToString(),conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_GAME_CHARACTER01");

					if (ds.Tables["VW_GAME_CHARACTER01"].Rows.Count != 0) {
						dr = ds.Tables["VW_GAME_CHARACTER01"].Rows[0];
						gameLogined = true;
						gameHandleNm = dr["GAME_HANDLE_NM"].ToString();
						gameCharacterType = dr["GAME_CHARACTER_TYPE"].ToString();
						gamePoint = int.Parse(dr["GAME_POINT"].ToString());
						exp = int.Parse(dr["TOTAL_EXP"].ToString());
						gameCharacterLevel = int.Parse(dr["GAME_CHARACTER_LEVEL"].ToString());
						unassignedForceCount = int.Parse(dr["UNASSIGNED_FORCE_COUNT"].ToString());
						tutorialStatus = dr["TUTORIAL_STATUS"].ToString();
						stageSeq = dr["STAGE_SEQ"].ToString();
						cooperationPoint = int.Parse(dr["COOPERATION_POINT"].ToString());
						totalAttackWinCount = int.Parse(dr["TOTAL_ATTACK_WIN_COUNT"].ToString());
						totalAttackLossCount = int.Parse(dr["TOTAL_ATTACK_LOSS_COUNT"].ToString());
						totalDefenceWinCount = int.Parse(dr["TOTAL_DEFENCE_WIN_COUNT"].ToString());
						totalDefenceLossCount = int.Parse(dr["TOTAL_DEFENCE_LOSS_COUNT"].ToString());
						totalHelpSuccessCount = int.Parse(dr["TOTAL_HELP_SUCCESS_COUNT"].ToString());
						totalHelpFailCount = int.Parse(dr["TOTAL_HELP_FAIL_COUNT"].ToString());
						totalPartyAttackWinCount = int.Parse(dr["TOTAL_PARTY_ATTACK_WIN_COUNT"].ToString());
						totalPartyAttackLossCount = int.Parse(dr["TOTAL_PARTY_ATTACK_LOSS_COUNT"].ToString());
						totalPartyDefenceWinCount = int.Parse(dr["TOTAL_PARTY_DEFENCE_WIN_COUNT"].ToString());
						totalPartyDefenceLossCount = int.Parse(dr["TOTAL_PARTY_DEFENCE_LOSS_COUNT"].ToString());
						totalTreasureGetCount = int.Parse(dr["TOTAL_TREASURE_GET_COUNT"].ToString());
						totalTreasureLossCount = int.Parse(dr["TOTAL_TREASURE_LOSS_COUNT"].ToString());
						lastPartyAttackDate = dr["LAST_PARTY_ATTACK_DATE"].ToString();
						dateExecDate = dr["DATE_EXEC_DATE"].ToString();
						partTimeJobExecDate = dr["PART_TIME_JOB_EXEC_DATE"].ToString();
						lastHugLogCheckDate = dr["LAST_HUG_LOG_CHECK_DATE"].ToString();
						lastPresentLogCheckDate = dr["LAST_PRESENT_LOG_CHECK_DATE"].ToString();
						missionForceCount = int.Parse(dr["MISSION_FORCE_COUNT"].ToString());
						missionMaxForceCount = int.Parse(dr["MISSION_MAX_FORCE_COUNT"].ToString());
						missionNonCompRecoveryFlag = int.Parse(dr["MISSION_NON_COMP_RECOVERY_FLAG"].ToString());
						attackForceCount = int.Parse(dr["ATTACK_FORCE_COUNT"].ToString());
						attackMaxForceCount = int.Parse(dr["ATTACK_MAX_FORCE_COUNT"].ToString());
						defenceForceCount = int.Parse(dr["DEFENCE_FORCE_COUNT"].ToString());
						defenceMaxForceCount = int.Parse(dr["DEFENCE_MAX_FORCE_COUNT"].ToString());
						castGamePoint = int.Parse(dr["CAST_GAME_POINT"].ToString());
						stageLevel = (!dr["STAGE_LEVEL"].ToString().Equals(string.Empty)) ? int.Parse(dr["STAGE_LEVEL"].ToString()) : 0;
						stageHonor = dr["HONOR"].ToString();
						stageGroupType = dr["STAGE_GROUP_TYPE"].ToString();
						restExp = int.Parse(dr["REST_EXP"].ToString());
						fellowCountLimit = int.Parse(dr["FELLOW_COUNT_LIMIT"].ToString());
						fellowCount = int.Parse(dr["FELLOW_COUNT"].ToString());
						lastHugBonusGetDate = dr["LAST_HUG_BONUS_GET_DATE"].ToString();
						maxAttackPower = int.Parse(dr["MAX_ATTACK_POWER"].ToString());
						maxDefencePower = int.Parse(dr["MAX_DEFENCE_POWER"].ToString());
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	public string SetupGameCharacterType(string pSiteCd,string pUserSeq,string pUserCharNo,string pGameCharacterType) {

		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_GAME_CHARACTER_TYPE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pGAME_CHARACTER_TYPE",DbSession.DbType.VARCHAR2,pGameCharacterType);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");

		}

		return sResult;
	}

	public string RegistNm(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,string pName) {

		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GAME_CHARACTER_CREATE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pGAME_HANDLE_NM",DbSession.DbType.VARCHAR2,pName);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}
		return sResult;
	}

	public void SetupTutorialStatus(string pSiteCd,string pUserSeq,string pUserCharNo,string pTutorialStatus) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETUP_TUTORIAL_STATUS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pTUTORIAL_STATUS",DbSession.DbType.VARCHAR2,pTutorialStatus);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void CreateGamePageView(string pSiteCd,string pUserSeq,string pUserCharNo,int pLoginFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CREATE_GAME_PAGE_VIEW");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pLOGIN_FLAG",DbSession.DbType.VARCHAR2,pLoginFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public DataSet GameCharacterTutorialSeqData(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	USER_SEQ						,						");
		oSqlBuilder.AppendLine("	USER_CHAR_NO											");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER										");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	SEX_CD					= :SEX_CD					AND	");
		oSqlBuilder.AppendLine("	TUTORIAL_BATTLE_FLAG	= :TUTORIAL_BATTLE_FLAG			");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));
		oParamList.Add(new OracleParameter(":TUTORIAL_BATTLE_FLAG",ViCommConst.FLAG_ON_STR));

		string sExecSql = string.Empty;
		sExecSql = oSqlBuilder.ToString();

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;

	}

	public String GetLastHugLogCheckDate(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sValue = null;
	
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	LAST_HUG_LOG_CHECK_DATE									");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER										");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND		");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO					");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
		
				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["LAST_HUG_LOG_CHECK_DATE"].ToString());
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}


	public void UpdateLastHugLogCheck(string pSiteCd,string pUserSeq,string pUserCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_LAST_HUG_LOG_CHECK");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateLastPresentLogCheck(string pSiteCd,string pUserSeq,string pUserCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_LAST_PRESENT_LOG_CHECK");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}


	public DataSet GetInformation(string pSiteCd,string pUserSeq,string pUserCharNo,string pLastHugLogCheckDate) {

		string sValue = null;
		string sInformation = null;
		DataSet oDataSet = null;
		DataSet oDataSetNew = null;
		DataRow dr = null;

		Dictionary<string,string> dict = new Dictionary<string,string>();

		//お友達紹介プレゼントがある 
		GameIntroLogSeekCondition oCondition = new GameIntroLogSeekCondition();
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.GetFlag = ViCommConst.FLAG_OFF_STR;
		using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
			oDataSet = oGameIntroLog.GetOneIntroLog(oCondition);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["CREATE_DATE"].ToString();
			sInformation = "1" + dr["GAME_HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			oDataSetNew = oDataSet.Clone();
		}

		//ﾊｸﾞされた
		oDataSet = null;
		dr = null;
		CastGameCommunicationPartnerHugSeekCondition oCondition2 = new CastGameCommunicationPartnerHugSeekCondition();
		oCondition2.SiteCd = pSiteCd;
		oCondition2.UserSeq = pUserSeq;
		oCondition2.UserCharNo = pUserCharNo;
		oCondition2.LastHugLogCheckDate = pLastHugLogCheckDate;

		using (CastGameCommunicationPartnerHug oCastGameCommunicationPartnerHug = new CastGameCommunicationPartnerHug()) {
			oDataSet = oCastGameCommunicationPartnerHug.GetOneNewHug(oCondition2);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = oDataSet.Tables[0].Rows[0];
			sValue = null;
			sInformation = null;

			sValue = dr["LAST_PARTNER_HUG_DATE"].ToString();
			sInformation = "2" + dr["HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}
		
		//ﾌﾟﾚｾﾞﾝﾄされた
		oDataSet = null;
		dr = null;
		GameCharacterPresentHistorySeekCondition oCondition3 = new GameCharacterPresentHistorySeekCondition();
		oCondition3.SeekMode = PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY;
		oCondition3.SiteCd = pSiteCd;
		oCondition3.PartnerUserSeq = pUserSeq;
		oCondition3.PartnerUserCharNo = pUserCharNo;

		using (GameCharacterPresentHistory oGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
			oDataSet = oGameCharacterPresentHistory.GetOnePresentHistory(oCondition3);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = null;
			sValue = null;
			sInformation = null;

			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["CREATE_DATE"].ToString();
			sInformation = "3" + dr["HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}

		//仲間申請された
		oDataSet = null;
		dr = null;
		CastGameCharacterFellowSeekCondition oCondition4 = new CastGameCharacterFellowSeekCondition();
		oCondition4.SiteCd = pSiteCd;
		oCondition4.UserSeq = pUserSeq;
		oCondition4.UserCharNo = pUserCharNo;
		oCondition4.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION;
		oCondition4.Sort = PwViCommConst.CastGameCharacterFellowSort.APPLICATION_DATE_DESC;

		using (CastGameCharacterFellow oCastGameCharacterFellow = new CastGameCharacterFellow()) {
			oDataSet = oCastGameCharacterFellow.GetOneFellowApplication(oCondition4);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = null;
			sValue = null;
			sInformation = null;

			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["APPLICATION_DATE"].ToString();
			sInformation = "4" + dr["HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}
		
		//応援依頼された
		oDataSet = null;
		dr = null;
		BattleLogSeekCondition oCondition5 = new BattleLogSeekCondition();
		oCondition5.SiteCd = pSiteCd;
		oCondition5.UserSeq = pUserSeq;
		oCondition5.UserCharNo = pUserCharNo;

		using (BattleLog oBattleLog = new BattleLog()) {
			oDataSet = oBattleLog.GetOneSupportRequestAddCast(oCondition5);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = null;
			sValue = null;
			sInformation = null;

			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["CREATE_DATE"].ToString();
			sInformation = "5" + dr["HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}
		
		string[] sortedKeys = new string[dict.Count];
		string[] sortedValues = new string[dict.Count];

		
		dict.Keys.CopyTo(sortedKeys,0);
		dict.Values.CopyTo(sortedValues,0);

		Array.Sort(sortedKeys,sortedValues);

		for (int index = 0;index < sortedKeys.Length;index++) {
			
			if (index == (sortedKeys.Length - 1)) {

				DataColumn col;
				col = new DataColumn(string.Format("INFORMATION_TYPE"),System.Type.GetType("System.String"));
				oDataSetNew.Tables[0].Columns.Add(col);
				
				dr = null;
				dr = oDataSetNew.Tables[0].Rows.Add();				

				dr["INFORMATION_TYPE"] = sortedValues[index].Substring(0,1);
				dr["HANDLE_NM"] = sortedValues[index].Substring(1);
			}
		}
		
		return oDataSetNew;
	}

	public DataSet GetInformationWoman(string pSiteCd,string pUserSeq,string pUserCharNo,string pLastHugLogCheckDate) {

		string sValue = null;
		string sInformation = null;
		DataSet oDataSet = null;
		DataSet oDataSetNew = null;
		DataRow dr = null;

		Dictionary<string,string> dict = new Dictionary<string,string>();

		//お友達紹介プレゼントがある 
		GameIntroLogSeekCondition oCondition = new GameIntroLogSeekCondition();
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.GetFlag = ViCommConst.FLAG_OFF_STR;
		using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
			oDataSet = oGameIntroLog.GetOneIntroLog(oCondition);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["CREATE_DATE"].ToString();
			sInformation = "1" + dr["GAME_HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			oDataSetNew = oDataSet.Clone();
		}

		//ﾊｸﾞされた
		oDataSet = null;
		dr = null;
		ManGameCommunicationPartnerHugSeekCondition oCondition2 = new ManGameCommunicationPartnerHugSeekCondition();
		oCondition2.SiteCd = pSiteCd;
		oCondition2.UserSeq = pUserSeq;
		oCondition2.UserCharNo = pUserCharNo;
		oCondition2.LastHugLogCheckDate = pLastHugLogCheckDate;

		using (ManGameCommunicationPartnerHug oManGameCommunicationPartnerHug = new ManGameCommunicationPartnerHug()) {
			oDataSet = oManGameCommunicationPartnerHug.GetOneNewHug(oCondition2);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = oDataSet.Tables[0].Rows[0];
			sValue = null;
			sInformation = null;

			sValue = dr["LAST_PARTNER_HUG_DATE"].ToString();
			sInformation = "2" + dr["GAME_HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}

		//ﾌﾟﾚｾﾞﾝﾄされた
		oDataSet = null;
		dr = null;
		GameCharacterPresentHistorySeekCondition oCondition3 = new GameCharacterPresentHistorySeekCondition();
		oCondition3.SeekMode = 0;
		oCondition3.SiteCd = pSiteCd;
		oCondition3.PartnerUserSeq = pUserSeq;
		oCondition3.PartnerUserCharNo = pUserCharNo;

		using (GameCharacterPresentHistory oGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
			oDataSet = oGameCharacterPresentHistory.GetOnePresentHistory(oCondition3);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = null;
			sValue = null;
			sInformation = null;

			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["CREATE_DATE"].ToString();
			sInformation = "3" + dr["GAME_HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}

		//仲間申請された
		oDataSet = null;
		dr = null;
		ManGameCharacterFellowSeekCondition oCondition4 = new ManGameCharacterFellowSeekCondition();
		oCondition4.SiteCd = pSiteCd;
		oCondition4.UserSeq = pUserSeq;
		oCondition4.UserCharNo = pUserCharNo;
		oCondition4.FriendApplicationStatus = PwViCommConst.GameCharFellowApplicationCd.HAS_APPLICATION;
		oCondition4.Sort = PwViCommConst.CastGameCharacterFellowSort.APPLICATION_DATE_DESC;

		using (ManGameCharacterFellow oManGameCharacterFellow = new ManGameCharacterFellow()) {
			oDataSet = oManGameCharacterFellow.GetOneFellowApplication(oCondition4);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = null;
			sValue = null;
			sInformation = null;

			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["APPLICATION_DATE"].ToString();
			sInformation = "4" + dr["GAME_HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}

		//応援依頼された
		oDataSet = null;
		dr = null;
		BattleLogSeekCondition oCondition5 = new BattleLogSeekCondition();
		oCondition5.SiteCd = pSiteCd;
		oCondition5.UserSeq = pUserSeq;
		oCondition5.UserCharNo = pUserCharNo;

		using (BattleLog oBattleLog = new BattleLog()) {
			oDataSet = oBattleLog.GetOneSupportRequest(oCondition5);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			dr = null;
			sValue = null;
			sInformation = null;

			dr = oDataSet.Tables[0].Rows[0];

			sValue = dr["CREATE_DATE"].ToString();
			sInformation = "5" + dr["GAME_HANDLE_NM"].ToString();

			dict.Add(sValue,sInformation);
			if (oDataSetNew == null) {
				oDataSetNew = oDataSet.Clone();
			}
		}

		string[] sortedKeys = new string[dict.Count];
		string[] sortedValues = new string[dict.Count];


		dict.Keys.CopyTo(sortedKeys,0);
		dict.Values.CopyTo(sortedValues,0);

		Array.Sort(sortedKeys,sortedValues);

		for (int index = 0;index < sortedKeys.Length;index++) {

			if (index == (sortedKeys.Length - 1)) {

				DataColumn col;
				col = new DataColumn(string.Format("INFORMATION_TYPE"),System.Type.GetType("System.String"));
				oDataSetNew.Tables[0].Columns.Add(col);

				dr = null;
				dr = oDataSetNew.Tables[0].Rows.Add();

				dr["INFORMATION_TYPE"] = sortedValues[index].Substring(0,1);
				dr["GAME_HANDLE_NM"] = sortedValues[index].Substring(1);
			}
		}

		return oDataSetNew;
	}

	public int GetRecCount(GameCharacterSeekCondition pCondition) {
		int iRecCount;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER	");
		oSqlBuilder.AppendLine(sWhereClause);

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iRecCount;
	}

	private OracleParameter[] CreateWhere(GameCharacterSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pCondition.SexCd));
		}

		return oParamList.ToArray();
	}
}
