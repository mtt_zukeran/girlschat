﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 一括送信メール検索条件
--	Progaram ID		: BatchMailSearchCnd
--
--  Creation Date	: 2010.05.26
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System.Data;
using Oracle.DataAccess.Client;

/// <summary>
/// 一括送信メール検索条件
/// </summary>
[System.Serializable]
public class BatchMailSearchCnd:DbSession {

	public DataSet GetList(string pRequestTxMailSeq) {

		DataSet ds = new DataSet();
		string sSql;
		try{
			conn = DbConnect("CasrAttrTypeValue.GetList");

			sSql = "SELECT " +
					   "REQUEST_TX_MAIL_SEQ		," +
					   "MAIL_SEARCH_CND_SUBSEQ	," +
					   "CND_NM					," +
					   "CND_VALUE				" +
					"FROM " +
						"T_BATCH_MAIL_SEARCH_CND " +
					"WHERE " +
					"REQUEST_TX_MAIL_SEQ		= :REQUEST_TX_MAIL_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("REQUEST_TX_MAIL_SEQ",pRequestTxMailSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
