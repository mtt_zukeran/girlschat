/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: メール記録
--	Progaram ID		: Ad
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using Oracle.DataAccess.Client;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

[Serializable]
public class MailLogSeekCondition:SeekConditionBase {
	public string ManUserSiteCd;
	public string ManUserSeq;
	public string ManUserCharNo;
	public string CastSiteCd;
	public string CastUserSeq;
	public string CastCharNo;

	public MailLogSeekCondition()
		: this(new NameValueCollection()) {
	}

	public MailLogSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[System.Serializable]
public class MailLog:DbSession {

	public string TxSiteCd;
	public string TxUserSeq;
	public string TxLoginId;
	public string TxPassword;

	public string RxSiteCd;
	public string RxUserSeq;
	public string RxLoginId;
	public string RxPassword;

	public bool IsValidInvaiteMail(string pKey,out string pManUserSeq,out string pCastUserSeq,out string pCastCharNo,out int pServicePoint,out DateTime pEffectTime,out int pTransfered) {
		DataSet ds;
		DataRow dr;
		bool bOk = false;

		pManUserSeq = "";
		pCastUserSeq = "";
		pCastCharNo = "";
		pServicePoint = 0;
		pTransfered = 0;
		pEffectTime = DateTime.Now;
		try {
			conn = DbConnect("MailLog.IsValidInvaiteMail");

			string sSql = "SELECT " +
							"RX_USER_SEQ					," +
							"TX_USER_SEQ					," +
							"TX_USER_CHAR_NO				," +
							"INVITE_TALK_CAST_CHARGE_FLAG	," +
							"SERVICE_POINT					," +
							"SERVICE_POINT_EFFECTIVE_DATE	," +
							"SERVICE_POINT_TRANSFER_FLAG " +
						"FROM " +
							"VW_REQ_TX_MAIL_DTL01 " +
						"WHERE " +
							"GENERAL2 =:GENERAL2 ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("GENERAL2",pKey);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_REQ_TX_MAIL_DTL01");
					if (ds.Tables["VW_REQ_TX_MAIL_DTL01"].Rows.Count != 0) {

						dr = ds.Tables[0].Rows[0];
						pManUserSeq = dr["RX_USER_SEQ"].ToString();
						pCastUserSeq = dr["TX_USER_SEQ"].ToString();
						pCastCharNo = dr["TX_USER_CHAR_NO"].ToString();
						pServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
						pTransfered = int.Parse(dr["SERVICE_POINT_TRANSFER_FLAG"].ToString());
						if (dr["SERVICE_POINT_TRANSFER_FLAG"].ToString().Equals("0")) {
							pEffectTime = DateTime.Parse(dr["SERVICE_POINT_EFFECTIVE_DATE"].ToString());
							if (pEffectTime > DateTime.Now) {
								bOk = true;
							}
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bOk;
	}


	public bool CheckTxMail(string pSiteCd,string pCastUserSeq,string pCastCharNo,string pManUserSeq,string pMailTemplateNo,out string pExisMsg) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_TX_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,pManUserSeq);
			db.ProcedureInParm("pMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,pMailTemplateNo);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEXIST_MSG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pExisMsg = db.GetStringValue("PEXIST_MSG");
			return db.GetStringValue("PRESULT").Equals("0");
		}
	}


	public void TxLoginMail(string pSiteCd,string pCastUserSeq,string pCastCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_LOGIN_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void TxSettleAcceptMail(string pSiteCd,string pUserSeq,string pSettleCompanyCd,string pSettleType,int pSettleAmt,int pSettlePoint,string pSid,string pMailAddOnInfo1,string pMailAddOnInfo2,string pMailAddOnInfo3,string pMailAddOnInfo4) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_SETTLE_ACCEPT_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PSETTLE_COMPANY_CD",DbSession.DbType.VARCHAR2,pSettleCompanyCd);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,pSettleType);
			db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,pSettleAmt);
			db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,pSettlePoint);
			db.ProcedureInParm("PSETTLE_ID",DbSession.DbType.VARCHAR2,pSid);
			db.ProcedureInParm("PMAIL_ADDON_INFO1",DbSession.DbType.VARCHAR2,pMailAddOnInfo1);
			db.ProcedureInParm("PMAIL_ADDON_INFO2",DbSession.DbType.VARCHAR2,pMailAddOnInfo2);
			db.ProcedureInParm("PMAIL_ADDON_INFO3",DbSession.DbType.VARCHAR2,pMailAddOnInfo3);
			db.ProcedureInParm("PMAIL_ADDON_INFO4",DbSession.DbType.VARCHAR2,pMailAddOnInfo4);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}


	public void UpdateMailDelProtectFlag(string pMailSeq,int pDelProtectFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL_PROTECT_FLAG");
			db.ProcedureInParm("PMAIL_SEQ",DbSession.DbType.VARCHAR2,pMailSeq);
			db.ProcedureInParm("PDEL_PROTECT_FLAG",DbSession.DbType.NUMBER,pDelProtectFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
	
	public bool GetRxMailInfo(string pMailSql,out string pSiteCd,out string pSexCd,out string pTxUserSeq,out string pTxUserCharNo) {
		string sTxLoginId;
		return GetRxMailInfo(pMailSql,out pSiteCd,out pSexCd,out pTxUserSeq,out pTxUserCharNo,out sTxLoginId);
	}

	public bool GetRxMailInfo(string pMailSql,out string pSiteCd,out string pSexCd,out string pTxUserSeq,out string pTxUserCharNo,out string pTxLoginId) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		pSexCd = "";
		pSiteCd = "";
		pTxUserSeq = "";
		pTxUserCharNo = "";
		pTxLoginId = "";
		try {
			conn = DbConnect();

			string sSql = "SELECT " +
								"RX_SITE_CD			," +
								"RX_SEX_CD			," +
								"TX_USER_SEQ		," +
								"TX_USER_CHAR_NO	," +
								"TX_LOGIN_ID		" +
							"FROM " +
								"VW_MAIL_BOX05 " +
							"WHERE " +
								"MAIL_SEQ	= :MAIL_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("MAIL_SEQ",pMailSql);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_BOX05");

					if (ds.Tables["VW_MAIL_BOX05"].Rows.Count != 0) {
						dr = ds.Tables["VW_MAIL_BOX05"].Rows[0];
						pSiteCd = dr["RX_SITE_CD"].ToString();
						pSexCd = dr["RX_SEX_CD"].ToString();
						pTxUserSeq = dr["TX_USER_SEQ"].ToString();
						pTxUserCharNo = dr["TX_USER_CHAR_NO"].ToString();
						pTxLoginId = dr["TX_LOGIN_ID"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public string GetOriginalMailDoc(string pRequestTxMailSeq) {
		string sQuery = "SELECT ORIGINAL_DOC1,ORIGINAL_DOC2,ORIGINAL_DOC3,ORIGINAL_DOC4,ORIGINAL_DOC5 FROM T_REQ_TX_MAIL WHERE REQUEST_TX_MAIL_SEQ = :REQUEST_TX_MAIL_SEQ";
		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sQuery,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":REQUEST_TX_MAIL_SEQ",pRequestTxMailSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);

					if (oDs.Tables[0].Rows.Count > 0) {
						DataRow oRow = oDs.Tables[0].Rows[0];
						return string.Concat(oRow["ORIGINAL_DOC1"],oRow["ORIGINAL_DOC2"],oRow["ORIGINAL_DOC3"],oRow["ORIGINAL_DOC4"],oRow["ORIGINAL_DOC5"]);
					}
				}
			}
		} finally {
			conn.Close();
		}

		return string.Empty;
	}

	public string GetObjTempId(string pRequestTxMailSeq) {
		string sQuery = "SELECT OBJ_TEMP_ID FROM T_REQ_TX_MAIL WHERE REQUEST_TX_MAIL_SEQ = :REQUEST_TX_MAIL_SEQ";
		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sQuery,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":REQUEST_TX_MAIL_SEQ",pRequestTxMailSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);

					if (oDs.Tables[0].Rows.Count > 0) {
						DataRow oRow = oDs.Tables[0].Rows[0];
						return oRow["OBJ_TEMP_ID"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}

		return string.Empty;
	}
	
	public string GetYesterdayTxMailCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		string sValue = "0";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	MAIL_COUNT									");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	VW_CAST_YESTERDAY_TX_MAIL01					");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO				");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = oDataSet.Tables[0].Rows[0]["MAIL_COUNT"].ToString();
		}
		
		return sValue;
	}

	public DataSet GetManChatMail(MailLogSeekCondition pCondition,int pStartIndex,int pRecPerPage) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	ROWNUM AS RNUM,");
		oSqlBuilder.AppendLine("	MAIL.*,");
		oSqlBuilder.AppendLine("	T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE,");
		oSqlBuilder.AppendLine("	CASE WHEN (MAIL.TX_SEX_CD = '1') THEN");
		oSqlBuilder.AppendLine("		GET_MAN_PHOTO_IMG_PATH(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ,0)");
		oSqlBuilder.AppendLine("	ELSE");
		oSqlBuilder.AppendLine("		GET_PHOTO_IMG_PATH(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ)");
		oSqlBuilder.AppendLine("	END AS OBJ_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER.AGE,");
		oSqlBuilder.AppendLine("	T_EX_CHAR_NO.CRYPT_VALUE,");
		oSqlBuilder.AppendLine("	CASE WHEN (T_FAVORIT.PROFILE_PIC_NON_PUBLISH_FLAG = 2) THEN");
		oSqlBuilder.AppendLine("		GET_SMALL_PHOTO_IMG_PATH(MAIL.CAST_SITE_CD,MAIL.LOGIN_ID,NULL)");
		oSqlBuilder.AppendLine("	ELSE");
		oSqlBuilder.AppendLine("		GET_SMALL_PHOTO_IMG_PATH(MAIL.CAST_SITE_CD,MAIL.LOGIN_ID,T_CAST_CHARACTER.PIC_SEQ)");
		oSqlBuilder.AppendLine("	END AS SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");

		oSqlBuilder.AppendLine("(SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		
		oSqlBuilder.AppendLine("(SELECT");
		oSqlBuilder.AppendLine("	INNER.*,");
		oSqlBuilder.AppendLine("	ROWNUM AS RANK");
		oSqlBuilder.AppendLine("FROM");

		oSqlBuilder.AppendLine("(SELECT");
		oSqlBuilder.AppendLine("	MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAIL_TYPE,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	MAIL_DOC1,");
		oSqlBuilder.AppendLine("	MAIL_DOC2,");
		oSqlBuilder.AppendLine("	MAIL_DOC3,");
		oSqlBuilder.AppendLine("	MAIL_DOC4,");
		oSqlBuilder.AppendLine("	MAIL_DOC5,");
		oSqlBuilder.AppendLine("	READ_FLAG,");
		oSqlBuilder.AppendLine("	TX_SITE_CD,");
		oSqlBuilder.AppendLine("	TX_LOGIN_ID,");
		oSqlBuilder.AppendLine("	TX_USER_SEQ,");
		oSqlBuilder.AppendLine("	TX_SEX_CD,");
		oSqlBuilder.AppendLine("	ATTACHED_OBJ_OPEN_FLAG,");
		oSqlBuilder.AppendLine("	DEL_PROTECT_FLAG,");
		oSqlBuilder.AppendLine("	REQUEST_TX_MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("	MAN_USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	CAST_SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	CAST_HANDLE_NM AS HANDLE_NM,");
		oSqlBuilder.AppendLine("	CAST_LOGIN_ID AS LOGIN_ID,");
		oSqlBuilder.AppendLine("	TEXT_MAIL_FLAG,");
		oSqlBuilder.AppendLine("	PRESENT_MAIL_ITEM_SEQ,");
		oSqlBuilder.AppendLine("	PRESENT_RETURN_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAIL_LOG");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	MAN_USER_SITE_CD = :MAN_USER_SITE_CD AND");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ = :MAN_USER_SEQ AND");
		oSqlBuilder.AppendLine("	MAN_USER_CHAR_NO = :MAN_USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	MAN_MAIL_BOX_TYPE = :MAN_MAIL_BOX_TYPE AND");
		oSqlBuilder.AppendLine("	CAST_SITE_CD = :CAST_SITE_CD AND");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ = :CAST_USER_SEQ AND");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO = :CAST_CHAR_NO AND");
		oSqlBuilder.AppendLine("	CAST_MAIL_BOX_TYPE = :CAST_MAIL_BOX_TYPE AND");
		oSqlBuilder.AppendLine("	WAIT_TX_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	MAN_MAIL_DEL_FLAG = 0");
		oSqlBuilder.AppendLine("ORDER BY CREATE_DATE DESC");
		oSqlBuilder.AppendLine(") INNER");

		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM <= :LAST_ROW");
		
		oSqlBuilder.AppendLine(")");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RANK > :FIRST_ROW");
		
		oSqlBuilder.AppendLine(") MAIL");
		oSqlBuilder.AppendLine("INNER JOIN T_REQ_TX_MAIL");
		oSqlBuilder.AppendLine("	ON (MAIL.REQUEST_TX_MAIL_SEQ = T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ)");
		oSqlBuilder.AppendLine("LEFT OUTER JOIN T_CAST_CHARACTER");
		oSqlBuilder.AppendLine("	ON (MAIL.CAST_SITE_CD = T_CAST_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("		MAIL.CAST_USER_SEQ = T_CAST_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("		MAIL.CAST_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine("LEFT OUTER JOIN T_EX_CHAR_NO");
		oSqlBuilder.AppendLine("	ON (MAIL.CAST_SITE_CD = T_EX_CHAR_NO.SITE_CD AND");
		oSqlBuilder.AppendLine("		MAIL.CAST_CHAR_NO = T_EX_CHAR_NO.USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("		1 = T_EX_CHAR_NO.LATEST_FLAG)");
		oSqlBuilder.AppendLine("LEFT OUTER JOIN T_FAVORIT");
		oSqlBuilder.AppendLine("	ON (MAIL.CAST_SITE_CD = T_FAVORIT.SITE_CD AND");
		oSqlBuilder.AppendLine("		MAIL.CAST_USER_SEQ = T_FAVORIT.USER_SEQ AND");
		oSqlBuilder.AppendLine("		MAIL.CAST_CHAR_NO = T_FAVORIT.USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("		MAIL.MAN_USER_SEQ = T_FAVORIT.PARTNER_USER_SEQ AND");
		oSqlBuilder.AppendLine("		MAIL.MAN_USER_CHAR_NO = T_FAVORIT.PARTNER_USER_CHAR_NO)");
		oSqlBuilder.AppendLine("ORDER BY MAIL.CREATE_DATE ASC");

		oParamList.Add(new OracleParameter(":MAN_USER_SITE_CD",pCondition.ManUserSiteCd));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_CHAR_NO",pCondition.ManUserCharNo));
		oParamList.Add(new OracleParameter(":MAN_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));
		oParamList.Add(new OracleParameter(":CAST_SITE_CD",pCondition.CastSiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		oParamList.Add(new OracleParameter(":CAST_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));
		oParamList.Add(new OracleParameter(":FIRST_ROW",pStartIndex));
		oParamList.Add(new OracleParameter(":LAST_ROW",pStartIndex + pRecPerPage));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		AppendTxRxType(oDataSet,pCondition.ManUserSeq);
		AppendQuickResMail(oDataSet);

		return oDataSet;
	}

	public DataSet GetCastChatMail(MailLogSeekCondition pCondition,int pStartIndex,int pRecPerPage) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	ROWNUM AS RNUM,");
		oSqlBuilder.AppendLine("	MAIL.*,");
		oSqlBuilder.AppendLine("	T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE,");
		oSqlBuilder.AppendLine("	CASE WHEN (MAIL.TX_SEX_CD = '1') THEN");
		oSqlBuilder.AppendLine("		GET_MAN_PHOTO_IMG_PATH(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ,0)");
		oSqlBuilder.AppendLine("	ELSE");
		oSqlBuilder.AppendLine("		GET_PHOTO_IMG_PATH(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ)");
		oSqlBuilder.AppendLine("	END AS OBJ_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.AGE,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(MAIL.MAN_USER_SITE_CD,MAIL.LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");

		oSqlBuilder.AppendLine("(SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");

		oSqlBuilder.AppendLine("(SELECT");
		oSqlBuilder.AppendLine("	INNER.*,");
		oSqlBuilder.AppendLine("	ROWNUM AS RANK");
		oSqlBuilder.AppendLine("FROM");

		oSqlBuilder.AppendLine("(SELECT");
		oSqlBuilder.AppendLine("	MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAIL_TYPE,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	MAIL_DOC1,");
		oSqlBuilder.AppendLine("	MAIL_DOC2,");
		oSqlBuilder.AppendLine("	MAIL_DOC3,");
		oSqlBuilder.AppendLine("	MAIL_DOC4,");
		oSqlBuilder.AppendLine("	MAIL_DOC5,");
		oSqlBuilder.AppendLine("	READ_FLAG,");
		oSqlBuilder.AppendLine("	TX_SITE_CD,");
		oSqlBuilder.AppendLine("	TX_LOGIN_ID,");
		oSqlBuilder.AppendLine("	TX_USER_SEQ,");
		oSqlBuilder.AppendLine("	TX_SEX_CD,");
		oSqlBuilder.AppendLine("	DEL_PROTECT_FLAG,");
		oSqlBuilder.AppendLine("	REQUEST_TX_MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAN_USER_SITE_CD,");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("	MAN_USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	MAN_HANDLE_NM AS HANDLE_NM,");
		oSqlBuilder.AppendLine("	MAN_LOGIN_ID AS LOGIN_ID,");
		oSqlBuilder.AppendLine("	TEXT_MAIL_FLAG,");
		oSqlBuilder.AppendLine("	PRESENT_MAIL_ITEM_SEQ,");
		oSqlBuilder.AppendLine("	PRESENT_RETURN_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAIL_LOG");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	MAN_USER_SITE_CD = :MAN_USER_SITE_CD AND");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ = :MAN_USER_SEQ AND");
		oSqlBuilder.AppendLine("	MAN_USER_CHAR_NO = :MAN_USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	MAN_MAIL_BOX_TYPE = :MAN_MAIL_BOX_TYPE AND");
		oSqlBuilder.AppendLine("	CAST_SITE_CD = :CAST_SITE_CD AND");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ = :CAST_USER_SEQ AND");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO = :CAST_CHAR_NO AND");
		oSqlBuilder.AppendLine("	CAST_MAIL_BOX_TYPE = :CAST_MAIL_BOX_TYPE AND");
		oSqlBuilder.AppendLine("	WAIT_TX_FLAG = 0 AND");
		oSqlBuilder.AppendLine("	CAST_MAIL_DEL_FLAG = 0");
		oSqlBuilder.AppendLine("ORDER BY CREATE_DATE DESC");
		oSqlBuilder.AppendLine(") INNER");

		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROWNUM <= :LAST_ROW");

		oSqlBuilder.AppendLine(")");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RANK > :FIRST_ROW");
		
		oSqlBuilder.AppendLine(") MAIL");
		oSqlBuilder.AppendLine("INNER JOIN T_REQ_TX_MAIL");
		oSqlBuilder.AppendLine("	ON (MAIL.REQUEST_TX_MAIL_SEQ = T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ)");
		oSqlBuilder.AppendLine("LEFT OUTER JOIN T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("	ON (MAIL.MAN_USER_SITE_CD = T_USER_MAN_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("		MAIL.MAN_USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("		MAIL.MAN_USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO)");
		oSqlBuilder.AppendLine("ORDER BY MAIL.CREATE_DATE ASC");

		oParamList.Add(new OracleParameter(":MAN_USER_SITE_CD",pCondition.ManUserSiteCd));
		oParamList.Add(new OracleParameter(":MAN_USER_SEQ",pCondition.ManUserSeq));
		oParamList.Add(new OracleParameter(":MAN_USER_CHAR_NO",pCondition.ManUserCharNo));
		oParamList.Add(new OracleParameter(":MAN_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));
		oParamList.Add(new OracleParameter(":CAST_SITE_CD",pCondition.CastSiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pCondition.CastUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pCondition.CastCharNo));
		oParamList.Add(new OracleParameter(":CAST_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));
		oParamList.Add(new OracleParameter(":FIRST_ROW",pStartIndex));
		oParamList.Add(new OracleParameter(":LAST_ROW",pStartIndex + pRecPerPage));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		AppendTxRxType(oDataSet,pCondition.CastUserSeq);
		AppendQuickResMail(oDataSet);

		return oDataSet;
	}

	private void AppendTxRxType(DataSet pDS,string pSelfUserSeq) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("TXRX_TYPE",Type.GetType("System.String"));

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			if (iBridUtil.GetStringValue(dr["TX_USER_SEQ"]).Equals(pSelfUserSeq)) {
				dr["TXRX_TYPE"] = ViCommConst.TX;
			} else {
				dr["TXRX_TYPE"] = ViCommConst.RX;
			}
		}
	}

	private void AppendQuickResMail(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("QUICK_RES_MAIL_FLAG",Type.GetType("System.String"));

		List<string> sMailSeqList = new List<string>();

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["QUICK_RES_MAIL_FLAG"] = ViCommConst.FLAG_OFF_STR;
			sMailSeqList.Add(iBridUtil.GetStringValue(dr["MAIL_SEQ"]));
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAIL_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_QUICK_RES_MAIL");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendFormat("	MAIL_SEQ IN ({0})",string.Join(",",sMailSeqList.ToArray())).AppendLine();

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["MAIL_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow dr = pDS.Tables[0].Rows.Find(subDR["MAIL_SEQ"]);

			if (dr != null) {
				dr["QUICK_RES_MAIL_FLAG"] = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	/// <summary>
	/// 一定期間内のメール受信履歴を取得する
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pUserCharNo"></param>
	/// <param name="pWaitTxFlag"></param>
	/// <param name="pFromDate"></param>
	/// <returns></returns>
	public DataSet GetTermRxMail(string pSiteCd,string pUserSeq,string pUserCharNo,string pWaitTxFlag,string pFromDate) {
		return this.GetTermRxMail(pSiteCd,pUserSeq,pUserCharNo,pWaitTxFlag,pFromDate,ViCommConst.MAIL_BOX_USER);
	}
	public DataSet GetTermRxMail(string pSiteCd,string pUserSeq,string pUserCharNo,string pWaitTxFlag,string pFromDate,string pMailBoxType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAIL_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAIL_LOG");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RX_SITE_CD = :RX_SITE_CD");
		oSqlBuilder.AppendLine("	AND RX_USER_SEQ = :RX_USER_SEQ");
		oSqlBuilder.AppendLine("	AND RX_USER_CHAR_NO = :RX_USER_CHAR_NO");
		oSqlBuilder.AppendLine("	AND RX_MAIL_BOX_TYPE = :RX_MAIL_BOX_TYPE");
		oSqlBuilder.AppendLine("	AND WAIT_TX_FLAG = :WAIT_TX_FLAG");
		oSqlBuilder.AppendLine("	AND CREATE_DATE >= TO_DATE(:CREATE_DATE, 'YYYY-MM-DD HH24:MI:SS')");

		oParamList.Add(new OracleParameter(":RX_SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":RX_USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":RX_USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":RX_MAIL_BOX_TYPE",pMailBoxType));
		oParamList.Add(new OracleParameter(":WAIT_TX_FLAG",pWaitTxFlag));
		oParamList.Add(new OracleParameter(":CREATE_DATE",pFromDate));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		return ds;
	}
}

