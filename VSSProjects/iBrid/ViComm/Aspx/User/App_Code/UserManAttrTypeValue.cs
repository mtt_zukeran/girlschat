﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員属性値
--	Progaram ID		: UserManAttrTypeValue
--
--  Creation Date	: 2009.07.16
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class UserManAttrTypeValue:DbSession {

	public UserManAttrTypeValue() {}

	public DataSet GetList(string pSiteCd,string pManAttrTypeSeq) {
		DataSet ds;
		try{
			conn = DbConnect("UserManAttrTypeValue.GetList");

			string sSql = "SELECT " +
								"MAN_ATTR_SEQ			, " +
								"MAN_ATTR_NM			, " +
								"GROUPING_CD			, " +
								"OMIT_SEEK_CONTION_FLAG	  " +
							"FROM " +
								"T_MAN_ATTR_TYPE_VALUE " +
							"WHERE " +
								"SITE_CD			= :SITE_CD				AND " +
								"MAN_ATTR_TYPE_SEQ = :MAN_ATTR_TYPE_SEQ		" +
							"ORDER BY " +
								"PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAN_ATTR_TYPE_SEQ",pManAttrTypeSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_ATTR_TYPE_VALUE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public void AppendAttr2DataSet(DataSet pDS,string pSiteCdColumnName,string pUserSeqColumnName,string pUserCharNoColumnName) {
		this.AppendAttr2DataSet(pDS.Tables[0],pSiteCdColumnName,pUserSeqColumnName,pUserCharNoColumnName);
	}

	public void AppendAttr2DataSet(DataTable pTargetDataTable,string pSiteCdColumnName,string pUserSeqColumnName,string pUserCharNoColumnName) {
	
		string sNewDays = SessionObjs.Current.site.newFaceDays.ToString();
		string sNewMark = SessionObjs.Current.site.newFaceMark;
		
		if (sNewDays.Equals(string.Empty)) {
			sNewDays = "0";
		}
		
		string sNewDay = DateTime.Now.AddDays(-1 * int.Parse(sNewDays)).ToString("yyyy/MM/dd");
	
		// DataTableに列追加
		for (int i = 1; i <= ViCommConst.MAX_ATTR_COUNT; i++) {
			DataColumn oDataColumn = new DataColumn(string.Format("MAN_ATTR_VALUE{0:D2}", i), Type.GetType("System.String"));
			oDataColumn.DefaultValue = string.Empty;
			pTargetDataTable.Columns.Add(oDataColumn);
		}

			
		foreach (DataRow oDataRow in pTargetDataTable.Rows) {
			string sSiteCd = oDataRow[pSiteCdColumnName].ToString();
			string sUserSeq = oDataRow[pUserSeqColumnName].ToString();
			string sUserCharNo = oDataRow[pUserCharNoColumnName].ToString();
		
			using(DataTable oAttrDataTable = this.GetAttrDataTable(sSiteCd,sUserSeq,sUserCharNo)){
				foreach (DataRow oAttrDataRow in oAttrDataTable.Rows) {
					oDataRow[string.Format("MAN_ATTR_VALUE{0}", oAttrDataRow["ITEM_NO"])] = oAttrDataRow["DISPLAY_VALUE"].ToString();
				}		
			}
		}
	}
	
	public DataTable GetAttrDataTable(string pSiteCd,string pUserSeq,string pUserCharNo){

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT					").AppendLine();
		oSqlBuilder.Append("	DISPLAY_VALUE	,	").AppendLine();
		oSqlBuilder.Append("	ITEM_NO				").AppendLine();
		oSqlBuilder.Append(" FROM								").AppendLine();
		oSqlBuilder.Append("	VW_USER_MAN_ATTR_VALUE01		").AppendLine();
		oSqlBuilder.Append(" WHERE								").AppendLine();
		oSqlBuilder.Append("	SITE_CD			= :SITE_CD		AND		").AppendLine();
		oSqlBuilder.Append("	USER_SEQ		= :USER_SEQ		AND		").AppendLine();
		oSqlBuilder.Append("	USER_CHAR_NO	= :USER_CHAR_NO			").AppendLine();
		oSqlBuilder.Append(" ORDER BY				").AppendLine();
		oSqlBuilder.Append("	MAN_ATTR_TYPE_PRIORITY	").AppendLine();
		

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":USER_SEQ", pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO", pUserCharNo);

				DataTable oAttrDataTable = new DataTable();
				
				using (da = new OracleDataAdapter(cmd)){
					da.Fill(oAttrDataTable);
				}
				
				return oAttrDataTable;
			}
		} finally {
			conn.Close();
		}
	}
	public string GetSeqByItemNo(string pSiteCd,string pAttrTypeSeq,string pItemCd) {

		System.Text.StringBuilder objSqlBuilder = new System.Text.StringBuilder();
		objSqlBuilder.Append(" SELECT                                                                          ").AppendLine();
		objSqlBuilder.Append("     MAN_ATTR_SEQ                                                               ").AppendLine();
		objSqlBuilder.Append(" FROM                                                                            ").AppendLine();
		objSqlBuilder.Append("     T_MAN_ATTR_TYPE_VALUE                                                      ").AppendLine();
		objSqlBuilder.Append(" WHERE                                                                           ").AppendLine();
		objSqlBuilder.Append("         SITE_CD			 = :SITE_CD    AND                                    ").AppendLine();
		objSqlBuilder.Append("		   MAN_ATTR_TYPE_SEQ = :MAN_ATTR_TYPE_SEQ    AND                         ").AppendLine();
		objSqlBuilder.Append("         ITEM_CD			 = :ITEM_CD                                           ").AppendLine();

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":MAN_ATTR_TYPE_SEQ",pAttrTypeSeq);
				cmd.Parameters.Add(":ITEM_CD",pItemCd);

				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
	}
}