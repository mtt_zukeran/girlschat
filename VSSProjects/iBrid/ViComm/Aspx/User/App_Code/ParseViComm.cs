﻿using System;
using System.IO;
using System.Web;
using System.Net;
using System.Configuration;
using System.Text;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;

[Serializable]
public class ParseViComm:MobileLib.ParseMobile {

	public SessionObjs sessionObjs;
	public Regex rgxHtmlTag;
	public Regex rgxArgument;

	public int divNestCount;

	// document body attributes
	private bool delayParseStartBody = false;
	private string colorBack = string.Empty;
	private string colorChar = string.Empty;
	private string colorLink = string.Empty;
	private string colorALink = string.Empty;
	private string colorVLink = string.Empty;

	[NonSerialized]
	private ParseCacheHolder _parseCache = null;
	public ParseCacheHolder ParseCache {
		get {
			if (this._parseCache == null) {
				this._parseCache = new ParseCacheHolder();
			}
			return this._parseCache;
		}
	}

	public ParseViComm()
		: base() {
	}

	public ParseViComm(string pPattern,bool pIsCrawler,string pSessionId,string pDomain,string pBasePath,RegexOptions pOption,string pCarrierCd,SessionObjs pSessionObjs)
		: base(pPattern,pIsCrawler,pSessionId,pDomain,pBasePath,pOption,pCarrierCd) {

		/*
		 * ParseHTMLが旧バージョンの状態になってないかのチェック用。動作的な意味はなし 
		 * checkNewVerがないというビルドエラーが出る場合はParseHTMLが旧バージョンの状態になっている。 
		 */
		if (this.checkNewVer) {
		}
		sessionObjs = pSessionObjs;
		divNestCount = 0;

		this.desingDebug = sessionObjs.designDebug;

		rgxHtmlTag = new Regex("<.*?>",RegexOptions.Compiled);
		rgxArgument = new Regex(@"\(([^\)]*)\)");

		datasetDictionary.Add(ViCommConst.DATASET_CAST,"01 キャスト検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_MAN,"02 男性会員検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_ADMIN_REPORT,"03 管理者連絡検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_MAIL,"04 メール検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_MAIL_RESERVED,"05 (未使用)送信メール検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_TX_BATCH_MAIL,"06 一括送信メール検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PARTNER_MOVIE,"07 相手が録画した動画検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PARTNER_PIC,"08 相手が撮影した写真検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PARTNER_DIARY,"09 相手が書いた日記検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PARTNER_BBS_DOC,"10 相手が書いた掲示板文章検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PARTNER_VOICE,"11 相手が録音した着ボイス検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_SELF_MOVIE,"12 自分が録画した動画検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_SELF_PIC,"13 自分が撮影した写真検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_SELF_DIARY,"14 自分が書いた日記検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_SELF_BBS_DOC,"15 自分が書いた掲示板文章検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_RANKING,"16 ランキング検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_TALK_HISTORY,"17 会話履歴検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_CAST_BONUS,"18 女性会員ボーナス検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_CAST_PAY,"19 女性会員報酬検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_CAST_REGISTED_SITE_LIST,"20 女性会員登録サイト一覧検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_REPUTATION,"21 評判メッセージ検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_REPUTATION_RESERVED,"22 (未使用)自分が書いた評判メッセージ検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_ATTR_TYPE,"23 属性値検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_TALK_MOVIE,"24 会話動画検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PARTNER_BBS_OBJ,"25 相手が投稿した掲示板の動画と写真検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_SELF_BBS_OBJ,"26 自分が投稿した掲示板の動画と写真検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_ASP_AD,"27 ASP広告検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PRODUCT_MOVIE,"29 商品動画検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PRODUCT_PIC,"30 商品写真検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_PRODUCT_THEME,"34 商品着せ替えﾂｰﾙ検索結果");

		datasetDictionary.Add(ViCommConst.DATASET_EX_GAME,"71 ｶﾞﾙﾁｬｹﾞｰﾑ検索");
		datasetDictionary.Add(ViCommConst.DATASET_EX_BEAUTY_CLOCK,"73 美人時計検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_EX_BBS_THREAD,"78 スレッド形式BBS 検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_EX_BBS_RES,"79 スレッド形式BBS レス検索結果");
		datasetDictionary.Add(ViCommConst.DATASET_EX_BINGO_ENTRY,"82 ﾒｰﾙdeﾋﾞﾝｺﾞ ｴﾝﾄﾘｰ検索結果");

#if (PREVIOUS)
#endif
	}

	public override byte[] Perser(string pTag,string pArgument) {
		string sValue = "";

		if (pTag.StartsWith("$x")) {
			return ConvertEmoji(pTag);
		};
		Encoding encSjis = Encoding.GetEncoding(932); // shift-jis 

		switch (pTag) {
			#region □ Webデザイン関連 □ -----------------------------------------------------------
			case "$DOCUMENT_BGCOLOR":
				if (!string.IsNullOrEmpty(pArgument)) {
					colorBack = pArgument;
					delayParseStartBody = true;
				}
				break;
			case "$DOCUMENT_TEXT":
				if (!string.IsNullOrEmpty(pArgument)) {
					colorChar = pArgument;
					delayParseStartBody = true;
				}
				break;
			case "$DOCUMENT_LINK":
				if (!string.IsNullOrEmpty(pArgument)) {
					colorLink = pArgument;
					delayParseStartBody = true;
				}
				break;
			case "$DOCUMENT_ALINK":
				if (!string.IsNullOrEmpty(pArgument)) {
					colorALink = pArgument;
					delayParseStartBody = true;
				}
				break;
			case "$DOCUMENT_VLINK":
				if (!string.IsNullOrEmpty(pArgument)) {
					colorVLink = pArgument;
					delayParseStartBody = true;
				}
				break;
			#endregion ------------------------------------------------------------------------------

			//bgcolor="#FFFFFF" text="#000000" link="#0000CC" alink="#FFFFFF" vlink="#9900FF"
			case "$HTML_START_TAG":
				sValue = GetHtmlTag(true,false,pTag,pArgument);
				break;
			case "$HTML_END_TAG":
				sValue = GetHtmlTag(false,false,pTag,pArgument);
				break;
			case "$HTML_TAG":
				sValue = GetHtmlTag(true,true,pTag,pArgument);
				break;
			case "$DATE":
				sValue = GetDate(pTag,pArgument);
				break;
			case "$ASC":
				sValue = ViCommConst.SORT_ASC;
				break;
			case "$SELF_CROSMILE_APP_FINISH":
				sValue = string.Format("{0}/Finish/",
						iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]));
				break;
			case "$SLEEP":
				// デバッグ用途 
				System.Threading.Thread.Sleep(int.Parse(pArgument));
				sValue = string.Empty;
				break;
			case "$BASE_DIR_MAN":
				sValue = sessionObjs.isCrawler ? CreateParentPath(true) + "vicomm/man" : string.Format("../../../vicomm/man/(S({0}))",sessionObjs.sessionId);
				break;
			case "$BASE_DIR_WOMAN":
				sValue = sessionObjs.isCrawler ? CreateParentPath(true) + "vicomm/woman" : string.Format("../../../vicomm/woman/(S({0}))",sessionObjs.sessionId);
				break;
			case "$BASE_DIR":
				sValue = sessionObjs.isCrawler ? CreateParentPath(false) : string.Format("../../../(S({0}))",sessionObjs.sessionId);
				break;
			case "$BUTTON":
				sValue = GetButtonTag(pArgument);
				break;
			case "$BUTTON_LINK":
				sValue = GetNextButtonTag(pArgument);
				break;
			case "$BUTTON_GOTO":
				sValue = GetGotoButtonTag(pArgument);
				break;
			case "$DESC":
				sValue = ViCommConst.SORT_DESC;
				break;
			case "$HR":
				sValue = ParseHR();
				break;
			case "$CHECKED":
				sValue = "checked";
				break;
			case "$ID":
				sValue = "id";
				break;
			case "$WIDTH":
				sValue = "width";
				break;
			case "$NBSP":
				sValue = "&nbsp;";
				break;
			case "$YEN":
				sValue = "&yen;";
				break;
			case "$MIDDOT":
				sValue = "&middot;";
				break;
			case "$FOOTER":
				sValue = this.Parse(sessionObjs.site.footerHtmlDoc);
				break;
			case "$INFO_MAIL":
				sValue = sessionObjs.site.supportEmailAddr;
				break;
			case "$INFO_TEL":
				sValue = sessionObjs.site.supportTel;
				break;
			case "$SITE_CD":
				sValue = sessionObjs.site.siteCd;
				break;
			case "$SITE_NM":
				sValue = sessionObjs.site.siteNm;
				break;
			case "$SUPPORT_MAIL_SEARCH_FLAG":
				sValue = sessionObjs.site.mailSearchEnabled.ToString();
				break;
			case "$SUPPORT_ATTACHED_MAIL_FLAG":
				sValue = sessionObjs.site.useAttachedMailFlag.ToString();
				break;
			case "$SITE_TOP":
				sValue = ParseSiteTop();
				break;
			case "$MAIL_DOMAIN":
				sValue = sessionObjs.site.mailHost;
				break;
			case "$NUM":
				int iValue;
				int.TryParse(pArgument,out iValue);
				sValue = iValue.ToString();
				break;
			case "$CHR":
				sValue = pArgument;
				break;
			case "$OTHER_SYS_INFO":
				sValue = sessionObjs.site.useOtherSysInfoFlag.ToString();
				break;
			case "$ERROR_MESSAGE":
				sValue = this.Parse(sessionObjs.errorMessage);
				break;
			case "$IMG_PATH":
				sValue = "../Image/" + sessionObjs.site.siteCd + "/";
				break;
			case "$CLEAR_LEFT":
				sValue = "<br clear=\"left\" />";
				break;
			case "$REC_NO_FROM":
				sValue = sessionObjs.startRNum.ToString();
				break;
			case "$DAY_OF_WEEK":
				sValue = GetMaqiaDayOfWeek();
				break;
			case "$REC_NO_TO":
				sValue = sessionObjs.endRNum.ToString();
				break;
			case "$REC_NO_START":
				sValue = sessionObjs.startRNum > 0 ?
							sessionObjs.startRNum.ToString() :
							"0";
				break;
			case "$REC_NO_END":
				sValue = sessionObjs.totalRowCount >= sessionObjs.endRNum ?
							sessionObjs.endRNum.ToString() :
							sessionObjs.totalRowCount.ToString();
				break;
			case "$REC_COUNT":
				sValue = sessionObjs.totalRowCount.ToString();
				break;
			case "$RNUM":
				int iNum = sessionObjs.startRNum + parseContainer.currentRecPos[this.currentTableIdx];
				sValue = iNum.ToString();
				break;
			case "$PAGE_COUNT":
				sValue = sessionObjs.maxPage.ToString();
				break;
			case "$PAGE_NO":
				sValue = sessionObjs.currentPage.ToString();
				break;
			case "$AU_BLANK":
				if (sessionObjs.carrier.Equals(ViCommConst.KDDI)) {
					sValue = "　";
				} else {
					sValue = "";
				}
				break;
			case "$SEEK_MODE_NM":
				sValue = ParseSeekMode(ulong.Parse(sessionObjs.seekMode));
				break;

			case "$FORM_START":
				if (pArgument.Equals("")) {
					sValue = string.Format("<FORM action=\"{0}\" method=\"GET\">",sessionObjs.requestQuery.RawUrl);
				} else {
					sValue = string.Format("<FORM action=\"{0}\" method=\"POST\">",pArgument);
				}
				break;

			case "$FORM_END":
				sValue = "</FORM>";
				break;

			case "$GOOGLE_ANALYTICS":
				sValue = GoogleAnalyticsGetImageUrl();
				break;

			case "$OBJECT_START":
				if (!pArgument.Equals("")) {
					sValue = string.Format("<OBJECT {0}>",Parse(pArgument));
				} else {
					sValue = ("<OBJECT>");
				}
				break;

			case "$OBJECT_END":
				sValue = "</OBJECT>";
				break;

			case "$RANDOM_AD":
				sValue = sValue = parseContainer.Parse(GetRandomAd());
				break;

			case "$RE_QUERY":
				sValue = this.GetReQuery(pTag,pArgument);
				break;

			case "$RE_QUERY_LIST":
				NameValueCollection oQeuryList = new NameValueCollection(sessionObjs.requestQuery.QueryString);
				oQeuryList.Remove("list");
				oQeuryList.Remove("pageno");
				sValue = ConstructQueryString(oQeuryList) + "&list=1";
				break;

			case "$RE_QUERY_DETAIL":
				NameValueCollection oQeuryDetail = new NameValueCollection(sessionObjs.requestQuery.QueryString);
				oQeuryDetail.Remove("list");
				oQeuryDetail.Remove("pageno");
				sValue = ConstructQueryString(oQeuryDetail) + "&list=0";
				break;

			case "$REMOVE_HTML":
				sValue = RemoveHtml(pArgument);
				break;
			case "$QUERY_PARM":
				sValue = iBridUtil.GetStringValue(sessionObjs.requestQuery.QueryString[pArgument]);
				break;
			case "$QUERY_PARM_DECODE":
				sValue = HttpUtility.UrlDecode(iBridUtil.GetStringValue(sessionObjs.requestQuery.QueryString[pArgument]));
				break;
			case "$QUERY_PARM_ENCODE":
				sValue = HttpUtility.UrlEncode(iBridUtil.GetStringValue(sessionObjs.requestQuery.QueryString[pArgument]),encSjis);
				break;
			case "$PARM":
				sValue = iBridUtil.GetStringValue(sessionObjs.requestQuery.Params[pArgument]);
				break;
			case "$PARM_DECODE":
				sValue = HttpUtility.UrlDecode(iBridUtil.GetStringValue(sessionObjs.requestQuery.Params[pArgument]));
				break;
			case "$PARM_ENCODE":
				sValue = HttpUtility.UrlEncode(iBridUtil.GetStringValue(sessionObjs.requestQuery.Params[pArgument]),encSjis);
				break;

			case "$CURRENT_ASPX":
				sValue = iBridUtil.GetStringValue(sessionObjs.currentAspx);
				break;
			case "$CURRENT_REQUEST_SCHEME":
				sValue = sessionObjs.requestQuery.Url.Scheme.ToString();
				break;
			case "$NAVIGATE_URL":
				sValue = sessionObjs.GetNavigateUrl(pArgument);
				break;
			case "$PREVIOUS_GUIDANCE":
				sValue = GetPreviousGuidance(pArgument);
				break;

			case "$NEXT_GUIDANCE":
				sValue = GetNextGuidance(pArgument);
				break;

			case "$HAS_PREVIOUS_FLAG":
				sValue = HasPrevious() ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;

			case "$HAS_NEXT_FLAG":
				sValue = HasNext() ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;

			case "$GIVEN_PAGE_GUIDANCE":
				sValue = GetGivenPageGuidance(pArgument);
				break;
			case "$PAGE_NAVIGATION":
				sValue = GetPageNavigation(pTag,pArgument);
				break;

			case "$MAN_INQ_IDPW_MAIL":
				sValue = string.Format("fogm{0}@{1}",sessionObjs.site.siteCd,sessionObjs.site.mailHost);
				break;

			case "$CAST_INQ_IDPW_MAIL":
				sValue = string.Format("catf@{0}",sessionObjs.site.mailHost);
				break;

			case "$GUID":
				sValue = iBridUtil.GetStringValue(sessionObjs.currentIModeId);
				break;

			case "$IMPERSONATED_FLAG":
				// 偽装しているかどうかを示すﾌﾗｸﾞ 
				sValue = sessionObjs.IsImpersonated ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;

			case "$CURRENT_URL_ENCODE":
				sValue = HttpUtility.UrlEncode(string.Format("{0}?{1}",this.sessionObjs.currentAspx,this.sessionObjs.requestQuery.QueryString.ToString()));
				break;

			case "$REDIRECT":
				if (sessionObjs.responseQuery != null) {
					string sUrl = parseContainer.Parse(pArgument);
					if (sUrl.ToLower().StartsWith("http://") || sUrl.ToLower().StartsWith("https://")) {
						sessionObjs.responseQuery.Redirect(sUrl,false);
					} else if (!iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]).Equals(string.Empty) && sUrl.ToLower().StartsWith(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]))) {
						sessionObjs.responseQuery.Redirect(sUrl,false);
					} else {
						sessionObjs.responseQuery.Redirect(sessionObjs.GetNavigateUrl(sUrl),false);
					}
				}
				break;
			case "$BREADCRUMBS_LIST_NAVI":
				sValue = GetBreadcrumbsListNavi(pArgument);
				break;

			#region □ 写真ｻｲｽﾞ種別	□ ----------------------
			case "$PIC_SIZE_ORIGINAL":
				sValue = ((int)ViCommConst.PicSizeType.ORIGINAL).ToString();
				break;
			case "$PIC_SIZE_LARGE":
				sValue = ((int)ViCommConst.PicSizeType.LARGE).ToString();
				break;
			case "$PIC_SIZE_SMALL":
				sValue = ((int)ViCommConst.PicSizeType.SMALL).ToString();
				break;
			case "$PIC_SIZE_LARGE_BLUR":
				sValue = ((int)ViCommConst.PicSizeType.LARGE_BLUR).ToString();
				break;
			case "$PIC_SIZE_SMALL_BLUR":
				sValue = ((int)ViCommConst.PicSizeType.SMALL_BLUR).ToString();
				break;
			#endregion --------------------------------------

			case "$HTTP_GET":
				sValue = ExecuteHttpGet(pTag,pArgument);
				break;

			case "$SPLIT_LINE":
				sValue = GetSplitLineValue(pTag,pArgument);
				break;

			case "$RANDOM":
				sValue = this.GetRandomValue(pArgument);
				break;

			case "$REMOVE_NEWLINE":
				sValue = string.Concat(pArgument.Split(new string[] { "\r\n","\n","\r" },StringSplitOptions.RemoveEmptyEntries));
				break;

			case "$RANK_SIGN":
				sValue = sessionObjs.rankSign;
				break;

			case "$URL_ENCODE": {
					Encoding enc = Encoding.GetEncoding("Shift_JIS");
					sValue = HttpUtility.UrlEncode(parseContainer.Parse(pArgument),enc);
				}
				break;
			case "$URL_DECODE": {
					Encoding enc = Encoding.GetEncoding("Shift_JIS");
					sValue = HttpUtility.UrlDecode(parseContainer.Parse(pArgument),enc);
				}
				break;

			case "$DOCOMO_MONEY_TRANSFER_TEL": {
					using (DocomoTransfer oDocomoTramsfer = new DocomoTransfer()) {
						sValue = oDocomoTramsfer.GetDocomoTransferTel(sessionObjs.site.siteCd);
					}
				}
				break;

			case "$DOCOMO_MONEY_TRANSFER_NM": {
					using (DocomoTransfer oDocomoTramsfer = new DocomoTransfer()) {
						sValue = oDocomoTramsfer.GetDocomoTransferNm(sessionObjs.site.siteCd);
					}
				}
				break;

			case "$HREF_WITH_ADD_BONUS_POINT":
				sValue = GetHrefWithAddBonusPoint(pArgument);
				break;
			case "$SELF_ENABLE_RICHINO_FLAG":
				sValue = this.EnabledRichino() ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;

			case "$NEW_LINE":
				sValue = Environment.NewLine;
				break;

			case "$REPLACE_NEWLINE_TO_BR":
				sValue = ReplaceBr(this.Parse(pArgument));
				break;

			case "$REPLACE_REGEX":
				string[] sValues = pArgument.Split(',');
				if (sValues.Length < 3) {
					sValue = string.Empty;
					break;
				}

				Regex oRegex = new Regex(@sValues[0],RegexOptions.Compiled | RegexOptions.Multiline);
				sValue = oRegex.Replace(this.Parse(sValues[1]),sValues[2]);

				if (sValues.Length > 3 && ViCommConst.FLAG_ON_STR.Equals(sValues[3])) {
					sValue = this.ReplaceBr(sValue);
				}
				break;

			case "$FORMAT_COMMA":
				int iNumberValue;
				if (int.TryParse(this.Parse(pArgument),out iNumberValue)) {
					sValue = iNumberValue.ToString("N0");
				} else {
					sValue = string.Empty;
				}
				break;

			case "$INSTR":
				sValue = GetInstr(pTag,pArgument);
				break;

			case "$MULTIPLY": {
					int iMultiplicand = 0;
					int iMultiplier = 0;
					string[] sArgs = this.Parse(pArgument).Split(new char[] { ',' },StringSplitOptions.RemoveEmptyEntries);
					if (sArgs.Length > 0) {
						int.TryParse(sArgs[0],out iMultiplicand);

						for (int i = 1; i < sArgs.Length; i++) {
							if (int.TryParse(sArgs[i],out iMultiplier)) {
								iMultiplicand = iMultiplicand * iMultiplier;
							}
						}
					}
					sValue = iMultiplicand.ToString();
				}
				break;

			case "$ADMIN_FLAG":
				sValue = sessionObjs.adminFlg ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				break;

			case "$IS_WITHIN_DAYS": {
					string[] sArguments = pArgument.Split(',');
					if (sArguments.Length < 2) {
						sValue = ViCommConst.FLAG_OFF_STR;
					}
					DateTime dt;
					if (!DateTime.TryParse(this.Parse(sArguments[0]),out dt)) {
						sValue = ViCommConst.FLAG_OFF_STR;
					}
					int iAddDays = 0;
					if (!int.TryParse(sArguments[1],out iAddDays)) {
						sValue = ViCommConst.FLAG_OFF_STR;
					}
					sValue = DateTime.Now <= dt.AddDays(iAddDays) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
				}
				break;

			case "$TALK_APP_LAST_USED_VERSION":
				using (User oUser = new User()) {
					sValue = oUser.GetCrosmileLastUsedVersion(sessionObjs.GetUserSeq());
				}
				break;

			case "$TALK_APP_LAST_USED_VERSION_NUM": {
					Regex regExceptNum = new Regex("[^0-9]",RegexOptions.Compiled);
					using (User oUser = new User()) {
						sValue = regExceptNum.Replace(oUser.GetCrosmileLastUsedVersion(sessionObjs.GetUserSeq()),string.Empty);
					}
				}
				break;

			case "$TALK_SMART_DIRECT_NOT_MONITOR_FLAG":
				using (User oUser = new User()) {
					sValue = oUser.GetTalkDirectSmartNotMonitor(sessionObjs.GetUserSeq());
				}
				break;
				
			case "$180":
				sValue = "´";
				break;

			case "$CASE_FEATURE_PHONE": {
					if (string.IsNullOrEmpty(pArgument)) {
						sValue = string.Empty;
					}
					string[] sArgs = pArgument.Split(',');
					string sArg0 = sArgs[0];
					string sArg1 = string.Empty;
					if (sArgs.Length > 1) {
						sArg1 = sArgs[1];
					}
					sValue = this.Parse(string.Format("<div id=\"$DIV_IS_FEATURE_PHONE;\">{0}</div><div id=\"$DIV_IS_NOT_FEATURE_PHONE;\">{1}</div>",sArg0,sArg1));
				}
				break;

			case "$BINGO_OPENING_SOON_FLAG":
				sValue = GetBingoOpeninSoonFlag(pTag,pArgument);
				break;
			case "$BINGO_OPENING_FLAG":
				sValue = GetBingoOpeningFlag(pTag,pArgument);
				break;
			case "$BINGO_CLOSED_FLAG":
				sValue = GetBingoClosedFlag(pTag,pArgument);
				break;
			case "$BINGO_JACKPOT_NOW":
				sValue = GetBingoJackpot(pTag,pArgument);
				break;
			case "$BINGO_FIRST_ENTRY_FLAG":
				sValue = GetBingoFirstEntryFlag(pTag,pArgument);
				break;
			case "$BINGO_CARD":
				sValue = GetBingoCardTag(pTag,pArgument);
				break;
			case "$BINGO_CARD_NO":
				sValue = GetBingoCardNo(pTag,pArgument);
				break;
			case "$BINGO_START_DATE":
				sValue = GetBingoStartDate(pTag,pArgument);
				break;
			case "$BINGO_END_DATE":
				sValue = GetBingoEndDate(pTag,pArgument);
				break;
			case "$BINGO_FLAG":
				sValue = GetBingoFlag(pTag,pArgument);
				break;
			case "$SELF_BINGO_BALL_FLAG":
				sValue = GetSelfBingoBallFlag(pTag,pArgument);
				break;
			case "$GET_RSS_CHANNEL":
				/* 仕様									*
				 * $GET_RSS(RSSのURL:ITEMNM);			*/

				string[] arrArgChannel = pArgument.Split(':');
				sValue = Rss.GetRss(arrArgChannel[0] + ":" + arrArgChannel[1],-1,arrArgChannel[2]);
				break;

			case "$GET_RSS_ITEM":
				/* 仕様									*
				 * $GET_RSS(RSSのURL:ITEMNO:ITEMNM);	*/

				string[] arrArgItem = pArgument.Split(':');
				sValue = Rss.GetRss(arrArgItem[0] + ":" + arrArgItem[1],int.Parse(arrArgItem[2]),arrArgItem[3]);
				break;
			case "$SP_H3":
				sValue = string.Format("<img src=\"../Image/{0}/space.gif\" width=\"1\" height=\"3\" />",sessionObjs.site.siteCd);
				break;
			case "$SP_H5":
				sValue = string.Format("<img src=\"../Image/{0}/space.gif\" width=\"1\" height=\"5\" />",sessionObjs.site.siteCd);
				break;
			case "$SEMICOLON":
				sValue = ";";
				break;
			case "$D_QUOTE":
				sValue = "\"";
				break;
			case "$GT":
				sValue = "&gt;";
				break;
			case "$LT":
				sValue = "&lt;";
				break;
			case "$HR3":
				sValue = "<hr color=\"#ff0099\" size=\"1\" />";
				break;
			case "$HR_1":
				sValue = "<div align=\"center\"><hr color=\"#ff0099\" size=\"1\" /></div>";
				break;
			case "$HR_3":
				sValue = string.Format("<img src=\"../Image/{0}/doted.gif\" />",sessionObjs.site.siteCd);
				break;
			case "$HR_3A":
				sValue = string.Format("<img src=\"../Image/{0}/doted_a.gif\" width=\"240\" height=\"5\" />",sessionObjs.site.siteCd);
				break;
			case "$HR_3B":
				sValue = string.Format("<div align=\"center\"><img src=\"../Image/{0}/only_doted.gif\" /></div>",sessionObjs.site.siteCd);
				break;
			case "$HR_3BLUE":
				sValue = string.Format("<img src=\"../Image/{0}/doted_blue.gif\" width=\"240\" height=\"5\" />",sessionObjs.site.siteCd);
				break;
			case "$HR_3PINK":
				sValue = string.Format("<img src=\"../Image/{0}/doted_pink.gif\" width=\"240\" height=\"5\" />",sessionObjs.site.siteCd);
				break;
			case "$HR_4":
				sValue = "<div align=\"center\"><hr color=\"#dddddd\" size=\"1\" /></div>";
				break;
			case "$HR_6":
				sValue = "<div align=\"center\"><hr color=\"#ffffff\" /></div>";
				break;
			case "$HR_D_GOLD":
				sValue = string.Format("<div align=\"center\"><img src=\"../Image/{0}/game/game_line_gold.gif\" /></div>",sessionObjs.site.siteCd);
				break;
			case "$HR_D_UPINK":
				sValue = string.Format("<div align=\"center\"><img src=\"../Image/{0}/game/tb_pink.gif\" /></div>",sessionObjs.site.siteCd);
				break;
			case "$HR_W_D":
				sValue = string.Format("<div align=\"center\"><img src=\"../Image/{0}/white_dashed_line.gif\" /></div>",sessionObjs.site.siteCd);
				break;
			case "$BT_MYPAGE":
				sValue = string.Format("$xE6EB;<a accesskey=\"0\" href=\"{0}\">ﾏｲﾍﾟｰｼﾞへ戻る</a>",ParseSiteTop());
				break;
			case "$PREVIOUS_LAST_PAGE_NO":
				sValue = (sessionObjs.maxPage - 1).ToString();
				break;
			case "$PREVIOUS_PAGE_NO":
				sValue = (sessionObjs.currentPage - 1).ToString();
				break;
			case "$PREVIOUS2_PAGE_NO":
				sValue = (sessionObjs.currentPage - 2).ToString();
				break;
			case "$PREVIOUS3_PAGE_NO":
				sValue = (sessionObjs.currentPage - 3).ToString();
				break;
			case "$PREVIOUS4_PAGE_NO":
				sValue = (sessionObjs.currentPage - 4).ToString();
				break;
			case "$NEXT_PAGE_NO":
				sValue = (sessionObjs.currentPage + 1).ToString();
				break;
			case "$NEXT2_PAGE_NO":
				sValue = (sessionObjs.currentPage + 2).ToString();
				break;
			case "$NEXT3_PAGE_NO":
				sValue = (sessionObjs.currentPage + 3).ToString();
				break;
			case "$NEXT4_PAGE_NO":
				sValue = (sessionObjs.currentPage + 4).ToString();
				break;
			default:
				string sTag = pTag.ToUpper();

				if (sTag.StartsWith("<HTML>")) {
					if (sessionObjs.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
						sValue = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">";
					} else if (sessionObjs.carrier.Equals(ViCommConst.ANDROID) || sessionObjs.carrier.Equals(ViCommConst.IPHONE)) {
						sValue = "<!DOCTYPE html>\r\n<html>";
					} else {
						sValue = "<html>";
					}
				} else if (sTag.StartsWith("<TITLE>")) {	// AUは.NET側でHeader生成済み
					if (!sessionObjs.site.pageTitle.Equals("")) {
						sValue = string.Format("<title>{0}",this.Parse(sessionObjs.site.pageTitle));
					} else {
						sValue = "<title>";
					}
				} else if (sTag.StartsWith("</HEAD>")) {	// AUは.NET側でHeader生成済み
					sValue = "\r\n" + ParseMetaTag() + ParseScriptTag() + ParseStyleTag() + ParseHeaderTagAppend() + "</head>";
				} else if (sTag.StartsWith("<BODY")) {
					sValue = ParseHeaderTag() + ParseStartBody();
				} else if (sTag.StartsWith("</FORM")) {
					sValue = ParseEndForm();
				} else if (sTag.StartsWith("<FORM")) {
					sValue = ParseStartForm(pTag);
				} else if (sTag.StartsWith("<IMG")) {
					sValue = ParseImg(sTag);
				} else if (sTag.StartsWith("<DIV")) {
					sValue = pTag;
				} else if (sTag.StartsWith("</DIV")) {
					sValue = pTag;
					//} else if (sTag.StartsWith("$SITE_DOC")) {
					//	sValue = ParseSiteDoc(sTag);
				} else if (sTag.StartsWith("$PGM_HTML")) {
					sValue = ParsePgmHtml(sTag);
				} else if (sTag.StartsWith("$DIFF")) {
					sValue = GetDiff(sTag,pArgument);
				} else if (sTag.StartsWith("$SUM")) {
					sValue = GetSum(sTag,pArgument);
				} else if (sTag.StartsWith("$MOD")) {
					sValue = GetMod(sTag,pArgument);
				} else if (sTag.StartsWith("$DIV")) {
					sValue = GetDiv(sTag,pArgument);
				} else if (sTag.StartsWith("$SEPARATE")) {
					sValue = GetSeparate(sTag,pArgument);
				} else if (sTag.StartsWith("$CONVERT_DATE_TIME")) {
					sValue = GetConvertDateTime(sTag,pArgument);
				} else if (sTag.StartsWith("$CONVERT_TIME")) {
					sValue = GetConvertTime(sTag,pArgument);
				} else if (sTag.StartsWith("$SUBTRACT_DATETIME")) {
					sValue = GetSubtractDateTime(sTag,pArgument);
				} else if (sTag.StartsWith("$COMPARE")) {
					sValue = GetCompare(sTag,pArgument);
				} else if (sTag.StartsWith("$DECODE")) {
					sValue = GetDecode(sTag,pArgument);
				} else if (sTag.StartsWith("$GET_DAY_OF_LAST_WEEK")) {
					sValue = GetDayOfLastWeek(sTag,pArgument);
				} else if (sTag.StartsWith("$GET_DAY_OF_LAST_MONTH")) {
					sValue = GetDayOfLastMonth(sTag,pArgument);
				} else {
					using (UserDefineTag oUserTag = new UserDefineTag()) {
						if (oUserTag.GetUserDefineTagInfo(sessionObjs.site.siteCd,sTag,sessionObjs.currentPageAgentType,sessionObjs.designDebug)) {
							sValue = this.Parse(oUserTag.htmlDoc);
						} else {
							parseErrorList.Add(string.Format("NODEFINE TAG:{0}",sTag));
						}
					}
				}
				break;
		}

		byte[] byteArray = encSjis.GetBytes(sValue);
		return byteArray;
	}

	private string ParseHR() {
		int iSize = 0;
		string sColor = "";

		if (sessionObjs.currentViewMask == 0) {
			iSize = sessionObjs.site.sizeLine;
			sColor = sessionObjs.site.colorLine;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_ACT_CATEGORY) != 0) {
			iSize = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).sizeLine;
			sColor = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorLine;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_USER_RANK) != 0) {
			iSize = sessionObjs.userRank.sizeLine;
			sColor = sessionObjs.userRank.colorLink;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_AD_GROUP) != 0) {
			iSize = sessionObjs.adGroup.sizeLine;
			sColor = sessionObjs.adGroup.colorLink;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_HTML_DOC_COLOR) != 0) {
			iSize = sessionObjs.webFace.sizeLine;
			sColor = sessionObjs.webFace.colorLink;
		}

		return Mobile.HrTag(iSize,sColor);
	}

	protected string ParseHeaderTag() {
		if (sessionObjs.carrier.Equals(ViCommConst.KDDI)) {
			return "";
		}
		string sValue = "\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">\r\n";
		string sValue1 = "";

		if (!sessionObjs.site.pageTitle.Equals("")) {
			sValue1 = string.Format("<title>{0}</title>\r\n",this.Parse(sessionObjs.site.pageTitle));
		}

		return sValue += sValue1 + ParseMetaTag() + ParseScriptTag() + ParseStyleTag() + ParseHeaderTagAppend() + "</head>";
	}

	protected string ParseHeaderTagAppend() {
		return string.Format("{0}\r\n",this.Parse(sessionObjs.site.headerHtmlDoc));
	}

	protected string ParseMetaTag() {

		string sValue2 = "",sValue3 = "",sValue4 = "",sValue5 = "",sValue6 = "",sValue7 = string.Empty;
		string sValue8 = string.Empty;
		string sValue9 = string.Empty;
		string sValue10 = string.Empty;
		string sValue11 = string.Empty;
		string sValue12 = string.Empty;

		if (parseContainer.parseUser.domain.StartsWith("sub.")) {
			sValue5 = "<meta name=\"robots\" content=\"noindex\" />\r\n";
		}

		if (!sessionObjs.site.googleVerification.Equals("") && sessionObjs.isCrawler) {
			sValue4 = string.Format("<meta name=\"google-site-verification\" content=\"{0}\" />\r\n",sessionObjs.site.googleVerification);
		}

		if (!sessionObjs.site.pageKeyword.Equals("")) {
			sValue2 = string.Format("<meta name=\"keywords\" content=\"{0}\" />\r\n",this.Parse(sessionObjs.site.pageKeyword));
		}

		if (!sessionObjs.site.pageDescription.Equals("")) {
			sValue3 = string.Format("<meta name=\"description\" content=\"{0}\" />\r\n",this.Parse(sessionObjs.site.pageDescription));
		}

		List<string> sCssList = new List<string>();
		switch (sessionObjs.carrier) {
			case ViCommConst.DOCOMO:
				sCssList.AddRange(sessionObjs.site.cssArrayDocomo);
				break;
			case ViCommConst.SOFTBANK:
				sCssList.AddRange(sessionObjs.site.cssArraySoftbank);
				break;
			case ViCommConst.KDDI:
				sCssList.AddRange(sessionObjs.site.cssArrayAu);
				break;
			case ViCommConst.IPHONE:
				sCssList.AddRange(sessionObjs.site.cssArrayiPhone);
				break;
			case ViCommConst.ANDROID:
				sCssList.AddRange(sessionObjs.site.cssArrayAndroid);
				break;
			case ViCommConst.CARRIER_OTHERS:
				if (!sessionObjs.currentPageAgentType.Equals(ViCommConst.DEVICE_3G) && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableDisplaySPDesignToPCUser"]).Equals(ViCommConst.FLAG_ON_STR)) {
					sCssList.AddRange(sessionObjs.site.cssArrayAndroid);
					sCssList.Add("pc.css");
				} else {
					sCssList.Add("style.css");
				}
				break;
		}
		StringBuilder oBuilder = new StringBuilder();
		foreach (string sCssFileNm in sCssList) {
			if (string.IsNullOrEmpty(sCssFileNm)) {
				continue;
			}
			oBuilder.AppendFormat("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}/css/{1}\" />\r\n",sessionObjs.root,sCssFileNm);
		}
		sValue6 = oBuilder.ToString();

		if (sessionObjs.carrier.Equals(ViCommConst.ANDROID) || sessionObjs.carrier.Equals(ViCommConst.IPHONE)) {
			string sSmartPhoneViewPort = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["viewPoint"]);
			string s3GViewPort = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["3GviewPoint"]);
			string sViewPort = "width=240px, initial-scale=1.29, minimum-scale=1.3, maximum-scale=1.3, user-scalable=yes";
			if (sessionObjs.currentPageAgentType.Equals(ViCommConst.DEVICE_3G)) {
				if (!string.IsNullOrEmpty(s3GViewPort)) {
					sViewPort = s3GViewPort;
				}
			} else {
				if (!string.IsNullOrEmpty(sSmartPhoneViewPort)) {
					sViewPort = sSmartPhoneViewPort;
				}
			}
			if (!string.IsNullOrEmpty(sViewPort)) {
				sValue7 = string.Format("<meta name=\"viewport\" content=\"{0}\" />\r\n",sViewPort);
			}

			sValue10 = "<meta name=\"format-detection\" content=\"telephone=no\">\r\n";
		}

		if (sessionObjs.emojiToolFlag.Equals(ViCommConst.FLAG_ON)) {
			sValue8 = EmojiBehavior.GetCssHeaderTag(sessionObjs.root,sessionObjs.carrier);
		}

		if (!string.IsNullOrEmpty(sessionObjs.canonical)) {
			sValue11 = string.Format("<link rel=\"canonical\" href=\"{0}\" />\r\n",sessionObjs.canonical);
		}

		sValue12 = GetMetaRobotsTag();

		return sValue9 + sValue3 + sValue2 + sValue5 + sValue4 + sValue6 + sValue8 + sValue7 + sValue10 + sValue11 + sValue12;
	}

	private string GetMetaRobotsTag() {
		string sTag = string.Empty;
		List<string> oContent = new List<string>();

		if (sessionObjs.noIndexFlag.Equals(ViCommConst.FLAG_ON) ||
			iBridUtil.GetStringValue(ConfigurationManager.AppSettings["UseForceNoindex"]).Equals(ViCommConst.FLAG_ON_STR)) {
			oContent.Add("noindex");
		}

		if (sessionObjs.noFollowFlag.Equals(ViCommConst.FLAG_ON)) {
			oContent.Add("nofollow");
		}

		if (sessionObjs.noArchiveFlag.Equals(ViCommConst.FLAG_ON)) {
			oContent.Add("noarchive");
		}

		if (oContent.Count > 0) {
			sTag = string.Format("<meta name=\"robots\" content=\"{0}\" />\r\n",string.Join(",",oContent.ToArray()));
		}

		return sTag;
	}

	protected virtual string ParseScriptTag() {
		StringBuilder oScriptTagBuilder = new StringBuilder();

		if (sessionObjs.carrier.Equals(ViCommConst.ANDROID) || sessionObjs.carrier.Equals(ViCommConst.IPHONE) || sessionObjs.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
			oScriptTagBuilder.AppendLine("<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js\"></script>");
			oScriptTagBuilder.AppendFormat("<script type=\"text/javascript\">(window.jQuery || document.write('<script src=\"{0}/scripts/jquery-1.5.2.min.js\"><\\/script>'));</script>",sessionObjs.root).AppendLine();

			if (sessionObjs.protectImageFlag.Equals(ViCommConst.FLAG_ON)) {
				oScriptTagBuilder.AppendLine(
					string.Format("<script src='{0}/scripts/protectImage.js' type='text/javascript'></script>",sessionObjs.root)
				);
			}

			string sOriginalSexCd = string.Empty;
			if (sessionObjs.logined) {
				if (sessionObjs.sexCd.Equals(ViCommConst.MAN)) {
					sOriginalSexCd = ViCommConst.MAN;
				} else {
					sOriginalSexCd = ViCommConst.OPERATOR;
				}
			} else if (sessionObjs.IsImpersonated) {
				if (sessionObjs.sexCd.Equals(ViCommConst.MAN)) {
					sOriginalSexCd = ViCommConst.OPERATOR;
				} else {
					sOriginalSexCd = ViCommConst.MAN;
				}
			} else {
				if (sessionObjs.requestQuery.Url.Host.Equals(sessionObjs.site.jobOfferSiteHostNm)) {
					sOriginalSexCd = ViCommConst.OPERATOR;
				} else {
					sOriginalSexCd = ViCommConst.MAN;
				}
			}

			string sGoogleAnalyticsScript = string.Empty;
			if (sOriginalSexCd.Equals(ViCommConst.MAN)) {
				sGoogleAnalyticsScript = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["ManGoogleAnalyticsScript"]);
			} else {
				sGoogleAnalyticsScript = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["CastGoogleAnalyticsScript"]);
			}

			if (!string.IsNullOrEmpty(sGoogleAnalyticsScript)) {
				oScriptTagBuilder.AppendFormat("<script type='text/javascript'>{0}</script>",sGoogleAnalyticsScript).AppendLine();
			}
		}

		if (sessionObjs.emojiToolFlag.Equals(ViCommConst.FLAG_ON)) {
			oScriptTagBuilder.Append(
				EmojiBehavior.GetScript(
							sessionObjs.root
							,sessionObjs.GetNavigateUrl(sessionObjs.root + "/img",sessionObjs.sessionId,"emoji.aspx")
							,sessionObjs.carrier)
			);
		}

		foreach (string jsFileNm in sessionObjs.javascriptFileNmArray) {
			if (string.IsNullOrEmpty(jsFileNm)) {
				continue;
			}
			oScriptTagBuilder.AppendFormat("<script src='{0}/js/{1}' type='text/javascript'></script>",sessionObjs.root,jsFileNm).AppendLine();
		}
		sessionObjs.javascriptFileNmArray = new string[] { };

		return oScriptTagBuilder.ToString();
	}

	protected virtual string ParseStyleTag() {
		StringBuilder oTagBuilder = new StringBuilder();

		// for Smart phone
		if (sessionObjs.carrier.Equals(ViCommConst.ANDROID) || sessionObjs.carrier.Equals(ViCommConst.IPHONE) || (sessionObjs.carrier.Equals(ViCommConst.CARRIER_OTHERS) && iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableDisplaySPDesignToPCUser"]).Equals(ViCommConst.FLAG_ON_STR))) {
			if (sessionObjs.currentPageAgentType.Equals(ViCommConst.DEVICE_3G)) {
				oTagBuilder.AppendLine("<style type=\"text/css\"><!--							");

				string enableFixedWidth = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableFixedWidth"]);
				if (enableFixedWidth.Equals(string.Empty) || enableFixedWidth.Equals(ViCommConst.FLAG_ON_STR)) {
					oTagBuilder.AppendLine("body {width: 240px; margin:0px; padding:1px}			");
				}
				oTagBuilder.AppendLine("--></style>												");
			}
		}

		return oTagBuilder.ToString();
	}

	private string ParseStartBody() {
		string sColorBack = "",sColorChar = "",sColorLink = "",sColorALink = "",sColorVLink = "";

		if (sessionObjs.currentViewMask == 0) {
			sColorBack = sessionObjs.site.colorBack;
			sColorChar = sessionObjs.site.colorChar;
			sColorLink = sessionObjs.site.colorLink;
			sColorALink = sessionObjs.site.colorALink;
			sColorVLink = sessionObjs.site.colorVLink;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_ACT_CATEGORY) != 0) {
			sColorBack = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorBack;
			sColorChar = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorChar;
			sColorLink = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorLink;
			sColorALink = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorALink;
			sColorVLink = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorVLink;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_USER_RANK) != 0) {
			sColorBack = sessionObjs.userRank.colorBack;
			sColorChar = sessionObjs.userRank.colorChar;
			sColorLink = sessionObjs.userRank.colorLink;
			sColorALink = sessionObjs.userRank.colorALink;
			sColorVLink = sessionObjs.userRank.colorVLink;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_AD_GROUP) != 0) {
			sColorBack = sessionObjs.adGroup.colorBack;
			sColorChar = sessionObjs.adGroup.colorChar;
			sColorLink = sessionObjs.adGroup.colorLink;
			sColorALink = sessionObjs.adGroup.colorALink;
			sColorVLink = sessionObjs.adGroup.colorVLink;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_HTML_DOC_COLOR) != 0) {
			sColorBack = sessionObjs.webFace.colorBack;
			sColorChar = sessionObjs.webFace.colorChar;
			sColorLink = sessionObjs.webFace.colorLink;
			sColorALink = sessionObjs.webFace.colorALink;
			sColorVLink = sessionObjs.webFace.colorVLink;
		}

		string sStartBody = ParseStartBodyImpl(sColorBack,sColorChar,sColorLink,sColorALink,sColorVLink);
		if (!sessionObjs.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
			if (sessionObjs.carrier.Equals(ViCommConst.IPHONE) || sessionObjs.carrier.Equals(ViCommConst.ANDROID)) {
				if (sessionObjs.currentPageAgentType.Equals(ViCommConst.DEVICE_3G)) {
					string enableFixedWidth = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableFixedWidth"]);
					if (enableFixedWidth.Equals(string.Empty) || enableFixedWidth.Equals(ViCommConst.FLAG_ON_STR)) {
						sStartBody = sStartBody + "<div style=\"width:240px; margin: 0px ;padding:0px; border:none\" >";
					}
				}

			}
		}
		return sStartBody;
	}

	private string ParseStartBodyImpl(string pColorBack,string pColorChar,string pColorLink,string pColorALink,string pColorVLink) {
		this.colorBack = pColorBack;
		this.colorChar = pColorChar;
		this.colorLink = pColorLink;
		this.colorALink = pColorALink;
		this.colorVLink = pColorVLink;

		if (sessionObjs.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_BODY_TEXT_VISIBLE)) {
					return string.Format("<div style=\"background-color:{0};\" id=\"pc\" >\r\n<body link=\"{1}\" alink=\"{2}\" vlink=\"{3}\">",pColorBack,pColorLink,pColorALink,pColorVLink);
				} else {
					return string.Format("<div style=\"background-color:{0};\" id=\"pc\" >\r\n<body text=\"{1}\" link=\"{2}\" alink=\"{3}\" vlink=\"{4}\">",pColorBack,pColorChar,pColorLink,pColorALink,pColorVLink);
				}
			}
		} else {
			string sStartBody = string.Empty;
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_BODY_TEXT_VISIBLE)) {
					sStartBody = string.Format("<body bgcolor=\"{0}\" link=\"{1}\" alink=\"{2}\" vlink=\"{3}\">",pColorBack,pColorLink,pColorALink,pColorVLink);
				} else {
					sStartBody = string.Format("<body bgcolor=\"{0}\" text=\"{1}\" link=\"{2}\" alink=\"{3}\" vlink=\"{4}\">",pColorBack,pColorChar,pColorLink,pColorALink,pColorVLink);
				}
			}
			return sStartBody;
		}
	}
	private string ParseEndBody() {
		string sEndBody = "</body>";

		if (sessionObjs.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
			sEndBody = sEndBody + "</div>";
		}
		return sEndBody;
	}

	public string RegexMatchPostGUIDAction(Match match) {
		return match.Value.Substring(0,match.Value.Length - 1) + "&guid=ON\"";
	}

	protected virtual string ParseStartForm(string pTag) {
		string sValue = string.Format("{0}",pTag);
		Regex regex;

		switch (postAction) {
			case ViCommConst.POST_ACT_UTN:
				postAction = 0;
				if (sessionObjs.carrier.Equals(ViCommConst.DOCOMO)) {
					sValue = string.Format("{0}",pTag.Substring(0,pTag.Length - 1) + " utn>");
				}
				break;

			case ViCommConst.POST_ACT_GUID:
				postAction = 0;
				if (sessionObjs.carrier.Equals(ViCommConst.DOCOMO)) {
					regex = new Regex(@"(action="".*?"")",RegexOptions.Compiled);
					sValue = regex.Replace(pTag,new MatchEvaluator(RegexMatchPostGUIDAction));
				}
				break;

			case ViCommConst.POST_ACT_BOTH_ID:
				postAction = 0;
				if (sessionObjs.carrier.Equals(ViCommConst.DOCOMO)) {
					regex = new Regex(@"(action="".*?"")",RegexOptions.Compiled);
					sValue = regex.Replace(pTag,new MatchEvaluator(RegexMatchPostGUIDAction));
					sValue = string.Format("{0}",sValue.Substring(0,sValue.Length - 1) + " utn>");
				}
				break;
		}

		if (sessionObjs.carrier.Equals(ViCommConst.DOCOMO) || sessionObjs.carrier.Equals(ViCommConst.SOFTBANK) || sessionObjs.carrier.Equals(ViCommConst.KDDI)) {
			int iSize = 1;

			if (sessionObjs.currentViewMask == 0) {
				iSize = sessionObjs.site.sizeChar;

			} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_ACT_CATEGORY) != 0) {
				iSize = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).sizeChar;

			} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_USER_RANK) != 0) {
				iSize = sessionObjs.userRank.sizeChar;

			} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_AD_GROUP) != 0) {
				iSize = sessionObjs.adGroup.sizeChar;

			} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_HTML_DOC_COLOR) != 0) {
				iSize = sessionObjs.webFace.sizeChar;
			}

			sValue += string.Format("<font size=\"{0}\">",iSize);
		}

		return sValue;
	}

	private string ParseEndForm() {
		if (sessionObjs.carrier.Equals(ViCommConst.DOCOMO) || sessionObjs.carrier.Equals(ViCommConst.SOFTBANK) || sessionObjs.carrier.Equals(ViCommConst.KDDI)) {
			return "</font></form>";
		} else {
			return "</form>";
		}
	}

	private string ParseImg(string pTag) {
		if (pTag.ToUpper().IndexOf("SRC=\"/ViComm/") >= 0) {
			return pTag.ToUpper().Replace("SRC=\"/ViComm/","SRC=\"../Image/" + sessionObjs.site.siteCd + "/");
		} else {
			return pTag;
		}
	}
	/*
	private string ParseSiteDoc(string pTag) {
		string sValue = "";
		string sCode = pTag.Replace("$SITE_DOC","");
		sCode = sCode.Replace(";","");
		using (SiteHtmlDoc oSiteHtmlDoc = new SiteHtmlDoc()) {
			oSiteHtmlDoc.GetOne(sessionObjs.site.siteCd,sCode,ViCommConst.DEFUALT_PUB_DAY,out sValue);
		}
		if (!sValue.Equals("")) {
			sValue = this.Parse(sValue);
		}
		return sValue;
	}
	 */

	private string ParsePgmHtml(string pTag) {
		string sValue = "";
		string sCode = pTag.Replace("$PGM_HTML","");
		sCode = sCode.Replace(";","");

		sValue = sessionObjs.htmlDoc[int.Parse(sCode)];
		return this.Parse(sValue);
	}
	public virtual string ParseSiteTop() {
		SessionObjs oSessionObjs = sessionObjs;
		if (sessionObjs.IsImpersonated) {
			oSessionObjs = sessionObjs.GetOriginalObj();
		}

		string sBaseUrl = oSessionObjs.root + oSessionObjs.sysType;

		string sAppendParam = string.Empty;
		if (sessionObjs.carrier.Equals(ViCommConst.KDDI)) {
			sAppendParam = "&" + this.GetAuRefresh();
		}

		if (oSessionObjs.logined) {
			if (oSessionObjs.sexCd.Equals(ViCommConst.MAN)) {
				return oSessionObjs.GetNavigateUrl(sBaseUrl,oSessionObjs.sessionId,oSessionObjs.site.userTopId + "?site=" + oSessionObjs.site.siteCd + sAppendParam);


			} else {
				return oSessionObjs.GetNavigateUrl(sBaseUrl,oSessionObjs.sessionId,"UserTop.aspx?site=" + oSessionObjs.site.siteCd);
			}
		} else {
			if (oSessionObjs.sexCd.Equals(ViCommConst.MAN)) {
				return oSessionObjs.GetNavigateUrl(sBaseUrl,oSessionObjs.sessionId,oSessionObjs.site.nonUserTopId + sAppendParam);
			} else {
				return oSessionObjs.GetNavigateUrl(sBaseUrl,oSessionObjs.sessionId,"NonUserTop.aspx");
			}
		}
	}

	protected string ParseDownloadUrl(string pCastRNum,string pUserSeq,string pUserCharNo,string pLoginId,string pMovieSeq,string pTitle,string pCharNoInfo,bool pSampleMovieFlag) {
		return ParseDownloadUrl(pCastRNum,pUserSeq,pUserCharNo,pLoginId,pMovieSeq,pTitle,pCharNoInfo,pSampleMovieFlag,string.Empty);
	}
	protected string ParseDownloadUrl(string pCastRNum,string pUserSeq,string pUserCharNo,string pLoginId,string pMovieSeq,string pTitle,string pCharNoInfo,bool pSampleMovieFlag,string pDownloadPage) {

		string sBaseUrl = sessionObjs.root + sessionObjs.sysType;
		if (string.IsNullOrEmpty(pDownloadPage)) {
			pDownloadPage = sessionObjs.currentAspx;
		}

		string sUrl = sessionObjs.GetNavigateUrl(sBaseUrl,sessionObjs.sessionId,string.Format("{0}?category={1}&castseq={2}&movieseq={3}&userrecno={4}&loginid={5}&seekmode={6}&playmovie=1",
											pDownloadPage,sessionObjs.currentCategoryIndex,pUserSeq,pMovieSeq,pCastRNum,pLoginId,sessionObjs.seekMode));
		if (pCharNoInfo.Equals("") == false) {
			sUrl = sUrl + "&" + pCharNoInfo;
		}
		return GetDownloadTag(ViCommConst.OPERATOR,sUrl,pUserSeq,pUserCharNo,pMovieSeq,pTitle,pSampleMovieFlag);
	}

	protected string ParseManDownloadUrl(string pManRNum,string pUserSeq,string pUserCharNo,string pLoginId,string pMovieSeq,string pTitle) {
		string sBaseUrl = sessionObjs.root + sessionObjs.sysType;
		string sUrl = sessionObjs.GetNavigateUrl(sBaseUrl,sessionObjs.sessionId,string.Format("{0}?category={1}&usermanseq={2}&movieseq={3}&userrecno={4}&loginid={5}&seekmode={6}&playmovie=1",
											sessionObjs.currentAspx,sessionObjs.currentCategoryIndex,pUserSeq,pMovieSeq,pManRNum,pLoginId,sessionObjs.seekMode));

		return GetDownloadTag(ViCommConst.MAN,sUrl,pUserSeq,pUserCharNo,pMovieSeq,pTitle,false);
	}

	private string GetDownloadTag(string pSexCd,string pUrl,string pUserSeq,string pUserCharNo,string pMovieSeq,string pTitle,bool pSampleMovieFlag) {

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = GetMovie(pSexCd,pMovieSeq,pSampleMovieFlag,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {

			string sErrorMessage = null;
			using (ErrorDtl oErrorDtl = new ErrorDtl()) {
				sErrorMessage = oErrorDtl.GetErrorDtl(sessionObjs.site.siteCd,sErrorCode);
			}

			return string.Format("{0}(<font color=\"#ff0000\">{1}</font>)",pTitle,sErrorMessage);
		} else {
			string sObjTag = "";

			if (ViCommConst.DOCOMO.Equals(sessionObjs.carrier)) {
				if (lSize <= 450000) {
					sObjTag = string.Format("<OBJECT declare id=\"obj{0}\" data=\"{1}\" type=\"video/3gpp\">",pMovieSeq,pUrl) +
												"<PARAM name=\"count\" value=\"0\" valuetype=\"data\">" +
									string.Format("</OBJECT><A href=\"#obj{0}\">{1}</A>",pMovieSeq,pTitle);
				} else {
					sObjTag = string.Format("<OBJECT declare id=\"obj{0}\" data=\"{1}\" type=\"video/3gpp\">",pMovieSeq,pUrl) +
												"<PARAM name=\"stream-type\" value=\"10\" valuetype=\"data\">" +
									string.Format("</OBJECT><A href=\"#obj{0}\">{1}</A>",pMovieSeq,pTitle);
				}

			} else if (ViCommConst.SOFTBANK.Equals(sessionObjs.carrier)) {
				sObjTag = string.Format("<A href=\"{0}\">{1}</A>",pUrl,pTitle);
			} else if (ViCommConst.KDDI.Equals(sessionObjs.carrier)) {
				if (lSize <= 99000) {
					sObjTag = string.Format("<object data=\"{0}&audummy=1\" type=\"video/3gpp2\" copyright=\"yes\" standby=\"{1}\" >",pUrl,pTitle) +
											"<param name=\"disposition\" value=\"devmpzz\" valuetype=\"data\"/>" +
											string.Format("<param name=\"size\" value=\"{0}\" valuetype=\"data\"/>",lSize) +
											string.Format("<param name=\"title\" value=\"MOVIE{0}\" valuetype=\"data\"/></object>",pMovieSeq);
				} else {
					sObjTag = string.Format("<object data=\"{0}&audummy=1\" type=\"video/3gpp2\" copyright=\"no\" standby=\"{1}\" >",pUrl,pTitle) +
											"<param name=\"disposition\" value=\"devmpzz\" valuetype=\"data\"/>" +
											string.Format("<param name=\"size\" value=\"{0}\" valuetype=\"data\"/>",lSize) +
											string.Format("<param name=\"title\" value=\"MOVIE{0}\" valuetype=\"data\"/></object>",pMovieSeq);
				}
			} else if (ViCommConst.ANDROID.Equals(sessionObjs.carrier)) {
				sObjTag = string.Format("<A href=\"{0}\">{1}</A>",pUrl,pTitle);
			} else if (ViCommConst.IPHONE.Equals(sessionObjs.carrier)) {
				if(ViCommConst.Browsers.IPHONE_OLD.Equals(sessionObjs.browserId)){
					sObjTag = string.Format("<embed src=\"{0}\" href=\"{0}\" type=\"video/3gpp\" target=\"myself\" autoplay/>",pUrl);
				}else{
					sObjTag = string.Format("<embed src=\"{0}\" href=\"{0}\" type=\"video/mp4\" target=\"myself\" autoplay/>",pUrl);
				}
			}
			return sObjTag;
		}
	}

	public long GetMovie(string pSexCd,string pMovieSeq,bool pSampleMovieFlag,out string pFileNm,out string pFullPath,out string pLoginId,out string pErrorCode) {
		string sUserSeq = string.Empty,sUserCharNo = string.Empty;
		return GetMovie(pSexCd,pMovieSeq,pSampleMovieFlag,out pFileNm,out pFullPath,out pLoginId,out pErrorCode,out sUserSeq,out sUserCharNo);
	}

	public long GetMovie(string pSexCd,string pMovieSeq,bool pSampleMovieFlag,out string pFileNm,out string pFullPath,out string pLoginId,out string pErrorCode,out string pUserSeq,out string pUserCharNo) {
		long lSize = 0;
		string sFullPathSB = string.Empty;
		pLoginId = string.Empty;
		pFullPath = string.Empty;
		pErrorCode = string.Empty;
		pUserSeq = string.Empty;
		pUserCharNo = string.Empty;

		pFileNm = pMovieSeq.PadLeft(15,'0');

		if (pSexCd.Equals(ViCommConst.MAN)) {
			using (UserManMovie oUserManMovie = new UserManMovie()) {
				oUserManMovie.GetUserManMovieInfo(pMovieSeq,out pLoginId);
				pFullPath = sessionObjs.site.webPhisicalDir +
								ViCommConst.MOVIE_DIRECTRY + "\\" + sessionObjs.site.siteCd + "\\man\\" + pMovieSeq.PadLeft(15,'0');
				sFullPathSB = sessionObjs.site.webPhisicalDir +
								ViCommConst.MOVIE_DIRECTRY + "\\" + sessionObjs.site.siteCd + "\\man\\" + pMovieSeq.PadLeft(15,'0') + "s";
			}
		} else {
			using (CastMovie oCastMovie = new CastMovie()) {
				if (pSampleMovieFlag) {
					oCastMovie.GetCastSampleMovieFileNm(pMovieSeq,out pLoginId);
				} else {
					oCastMovie.GetCastMovieInfo(pMovieSeq,out pLoginId,out pUserSeq,out pUserCharNo);
				}
				pFullPath = sessionObjs.site.webPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + sessionObjs.site.siteCd + "\\operator\\" + pLoginId + "\\" + pFileNm;
				sFullPathSB = sessionObjs.site.webPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + sessionObjs.site.siteCd + "\\operator\\" + pLoginId + "\\" + pFileNm + "s";
			}
		}
		if (ViCommConst.KDDI.Equals(sessionObjs.carrier)) {
			pFullPath += ViCommConst.MOVIE_FOODER2;
			sFullPathSB += ViCommConst.MOVIE_FOODER2;
		} else if(ViCommConst.IPHONE.Equals(sessionObjs.carrier) && !ViCommConst.Browsers.IPHONE_OLD.Equals(sessionObjs.browserId)) {
			pFullPath += ViCommConst.MOVIE_FOODER3;
			sFullPathSB += ViCommConst.MOVIE_FOODER3;				
		} else {
			pFullPath += ViCommConst.MOVIE_FOODER;
			sFullPathSB += ViCommConst.MOVIE_FOODER;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(pFullPath)) {
				FileInfo objInfo = new System.IO.FileInfo(pFullPath);
				lSize = objInfo.Length;
			} else {
				pErrorCode = ViCommConst.ERR_NOTHING_MOVIE;
				lSize = ViCommConst.GET_MOVIE_ERROR_VALUE;
			}
			if ((ViCommConst.SOFTBANK.Equals(sessionObjs.carrier)) && (lSize > 300000)) {
				if (System.IO.File.Exists(sFullPathSB)) {
					FileInfo objInfo = new System.IO.FileInfo(sFullPathSB);
					lSize = objInfo.Length;
					pFileNm += "s";
					pFullPath = sFullPathSB;
				} else {
					pErrorCode = ViCommConst.ERR_NOTHING_MOVIE_FOR_SOFTBANK;
					lSize = ViCommConst.GET_MOVIE_ERROR_VALUE;
				}
			}
		}

		return lSize;
	}

	protected void GetLableAndAccessKey(string pText,out string pLabel,out string pAccessKey) {
		pLabel = "";
		pAccessKey = "";
		string[] sValue = pText.Split(',');
		if (sValue.Length >= 1) {
			pLabel = parseContainer.Parse(sValue[0]);
		}
		if (sValue.Length >= 2) {
			pAccessKey = string.Format(" accesskey=\"{0}\" ",sValue[1]);
		}
	}

	protected string ParseSeekMode(ulong pMode) {
		string sValue = "NO DEFINE";

		switch (pMode) {
			case ViCommConst.INQUIRY_ONLINE:
				sValue = "ｵﾝﾗｲﾝ";
				break;
			case ViCommConst.INQUIRY_CAST_MOVIE:
				sValue = "ﾌﾟﾛﾌｨｰﾙ動画";
				break;
			case ViCommConst.INQUIRY_CAST_MONITOR:
				sValue = "会話ﾓﾆﾀｰ";
				break;
			case ViCommConst.INQUIRY_FAVORIT:
				sValue = "お気に入り";
				break;
			case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
				sValue = "ラブリスト";
				break;
			case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
				sValue = "プチラブリスト";
				break;
			case ViCommConst.INQUIRY_EXTENSION_MOTE_CON:
				sValue = "モテコン";
				break;
			case ViCommConst.INQUIRY_EXTENSION_MOTE_KING:
				sValue = "モテキング";
				break;
			case ViCommConst.INQUIRY_CAST_PICKUP:
				sValue = "ﾋﾟｯｸｱｯﾌﾟ";
				break;
			case ViCommConst.INQUIRY_TALK_MOVIE:
				sValue = "会話動画";
				break;
			case ViCommConst.INQUIRY_TALK_MOVIE_PICKUP:
				sValue = "会話動画ﾋﾟｯｸｱｯﾌﾟ";
				break;
			case ViCommConst.INQUIRY_CONDITION:
				sValue = "条件検索";
				break;
			case ViCommConst.INQUIRY_CAST_WAITING:
				sValue = "待機中";
				break;
			case ViCommConst.INQUIRY_ROOM_MONITOR:
				sValue = "部屋ﾓﾆﾀ";
				break;
			case ViCommConst.INQUIRY_CAST_NEW_FACE:
				sValue = "新人（全て）";
				break;
			case ViCommConst.INQUIRY_TALK_MOVIE_PART:
				sValue = "会話動画(ﾊﾟｰﾄ選択)";
				break;
			case ViCommConst.INQUIRY_CAST_DIARY:
				sValue = "日記";
				break;
			case ViCommConst.INQUIRY_TALK_HISTORY:
				sValue = "会話履歴";
				break;
			case ViCommConst.INQUIRY_MARKING:
				sValue = "足あと";
				break;
			case ViCommConst.INQUIRY_CAST_RECOMMEND:
				sValue = "ONLINE + OFFLINE DISPLAY対象者";
				break;
			case ViCommConst.INQUIRY_DIRECT:
				sValue = "ﾀﾞｲﾚｸﾄ";
				break;
			case ViCommConst.INQUIRY_RX_USER_MAIL_BOX:
				sValue = "ﾒｰﾙBOX(ﾕｰｻﾞｰﾒｰﾙ受信)";
				break;
			case ViCommConst.INQUIRY_RX_INFO_MAIL_BOX:
				sValue = "ﾒｰﾙBOX(お知らせﾒｰﾙ受信)";
				break;

			case ViCommConst.INQUIRY_RX_BLOG_MAIL_BOX:
				sValue = "ﾒｰﾙBOX(ブログ通知メール受信)";
				break;
			case ViCommConst.INQUIRY_TX_USER_MAIL_BOX:
				sValue = "ﾒｰﾙBOX(ﾕｰｻﾞｰﾒｰﾙ送信)";
				break;
			case ViCommConst.INQUIRY_ADMIN_REPORT:
				sValue = "管理者連絡";
				break;
			case ViCommConst.INQUIRY_MAN_NEW:
				sValue = "男性新規会員";
				break;
			case ViCommConst.INQUIRY_MAN_LOGINED_NEW:
				sValue = "男性ﾛｸﾞｲﾝ中新規会員";
				break;
			case ViCommConst.INQUIRY_REFUSE:
				sValue = "拒否";
				break;
			case ViCommConst.INQUIRY_REFUSE_ME:
				sValue = "拒否られ";
				break;
			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2:
				sValue = "日記被閲覧ﾗﾝｷﾝｸﾞ(月～日)";
				break;
			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2:
				sValue = "日記被閲覧ﾗﾝｷﾝｸﾞ(先月)";
				break;
			case ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING:
				sValue = "足あとﾗﾝｷﾝｸﾞ";
				break;
			case ViCommConst.INQUIRY_ONLINE_CAST_NEW_FACE:
				sValue = "新人(ｵﾝﾗｲﾝ)";
				break;
			case ViCommConst.INQUIRY_MAN_ADD_POINT:
				sValue = "ﾎﾟｲﾝﾄ購入男性一覧";
				break;
			case ViCommConst.INQUIRY_CAST_TV_PHONE:
				sValue = "TV電話対象者(全て)";
				break;
			case ViCommConst.INQUIRY_LIKE_ME:
				sValue = "お気に入られ";
				break;
			case ViCommConst.INQUIRY_CAST_LOGINED:
				sValue = "ﾛｸﾞｲﾝ済み";
				break;
			case ViCommConst.INQUIRY_ALL_TALK_HISTORY:
				sValue = "全会話履歴";
				break;
			case ViCommConst.INQUIRY_FORWARD:
				sValue = "VCS PF転送";
				break;
			case ViCommConst.INQUIRY_CAST_ONLINE_TV_PHONE:
				sValue = "TV電話対象者(ｵﾝﾗｲﾝ)";
				break;
			case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_ALL:
				sValue = "会話要求履歴一覧(全て)";
				break;
			case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_GROUP_MAN:
				sValue = "会話要求履歴一覧(男性ｸﾞﾙｰﾌﾟ)";
				break;
			case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_SUCCESS:
				sValue = "会話要求履歴一覧(会話成立分)";
				break;
			case ViCommConst.INQUIRY_WAITING_MAN:
				sValue = "待機男性一覧";
				break;
			case ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE:
				sValue = "個人毎販売動画一覧";
				break;
			case ViCommConst.INQUIRY_SALE_MOVIE:
				sValue = "販売動画一覧";
				break;
			case ViCommConst.INQUIRY_PERSONAL_SALE_VOICE:
				sValue = "個人毎販売着ﾎﾞｲｽ一覧";
				break;
			case ViCommConst.INQUIRY_SALE_VOICE:
				sValue = "販売着ﾎﾞｲｽ一覧";
				break;
			case ViCommConst.INQUIRY_BBS_PIC:
				sValue = "掲示板写真";
				break;
			case ViCommConst.INQUIRY_BBS_MOVIE:
				sValue = "掲示板動画";
				break;
			case ViCommConst.INQUIRY_BBS_DOC:
				sValue = "掲示板文章";
				break;
			case ViCommConst.INQUIRY_GALLERY:
				sValue = "ﾌﾟﾛﾌｨｰﾙ写真一覧";
				break;
			case ViCommConst.INQUIRY_RECOMMEND:
				sValue = "ﾋﾟｯｸｱｯﾌﾟ一覧";
				break;
			case ViCommConst.INQUIRY_EXTENSION_RICHINO_USER:
				sValue = "ﾘｯﾁｰﾉﾕｰｻﾞ一覧";
				break;
		}
		return sValue;
	}

	protected bool IsExistField(string pTag,string pFieldNm) {
		if (GetDataRow(currentTableIdx) != null) {
			if (GetDataRow(currentTableIdx).Table.Columns.Contains(pFieldNm)) {
				return true;
			} else {
				return false;
			}
		} else {
			parseErrorList.Add(string.Format("IsExistField DataRow is null Column Tag:{0} Field:{1}",pTag,pFieldNm));
			return false;
		}
	}

	public bool SetFieldValue(string pTag,string pFieldNm,out string pValue) {
		if (GetDataRow(currentTableIdx) != null) {
			if (GetDataRow(currentTableIdx).Table.Columns.Contains(pFieldNm)) {
				pValue = this.Parse(GetDataRow(currentTableIdx)[pFieldNm].ToString());
				return true;
			} else {
				pValue = "";
				parseErrorList.Add(string.Format("SetFieldValue Unknow Column Tag:{0} Field:{1}",pTag,pFieldNm));
				return false;
			}
		} else {
			pValue = "";
			parseErrorList.Add(string.Format("SetFieldValue DataRow is null Column Tag:{0} Field:{1}",pTag,pFieldNm));
			return false;
		}
	}

	protected bool GetValue(string pTag,string pFieldNm,out string pValue) {
		if (GetDataRow(currentTableIdx) != null) {
			if (GetDataRow(currentTableIdx).Table.Columns.Contains(pFieldNm)) {
				pValue = GetDataRow(currentTableIdx)[pFieldNm].ToString();
				return true;
			} else {
				pValue = "";
				parseErrorList.Add(string.Format("Unknow Column Tag:{0} Field:{1}",pTag,pFieldNm));
				return false;
			}
		} else {
			pValue = "";
			parseErrorList.Add(string.Format("DataRow is null Column Tag:{0} Field:{1}",pTag,pFieldNm));
			return false;
		}
	}

	protected bool GetValue(string pTag,int pTableIdx,string pFieldNm,out string pValue) {
		if (GetDataRow(pTableIdx) != null) {
			if (GetDataRow(pTableIdx).Table.Columns.Contains(pFieldNm)) {
				pValue = GetDataRow(pTableIdx)[pFieldNm].ToString();
				return true;
			} else {
				pValue = "";
				parseErrorList.Add(string.Format("Unknow Column Tag:{0} Field:{1}",pTag,pFieldNm));
				return false;
			}
		} else {
			pValue = "";
			parseErrorList.Add(string.Format("DataRow is null Column Tag:{0} Field:{1}",pTag,pFieldNm));
			return false;
		}
	}

	public bool GetValues(string pTag,string pFields,out string[] pValue) {
		string[] sFields = pFields.Split(',');
		pValue = new string[sFields.Length];
		bool bOk = true;
		for (int i = 0;i < sFields.Length;i++) {
			if (GetDataRow(currentTableIdx) != null) {
				if (GetDataRow(currentTableIdx).Table.Columns.Contains(sFields[i])) {
					pValue[i] = GetDataRow(currentTableIdx)[sFields[i]].ToString();
				} else {
					pValue[i] = "";
					parseErrorList.Add(string.Format("Unknow Column Tag:{0} Field:{1}",pTag,sFields[i]));
					bOk = false;
				}
			} else {
				pValue[i] = "";
				parseErrorList.Add(string.Format("DataRow is null Column Tag:{0} Field:{1}",pTag,sFields[i]));
				bOk = false;
			}
		}
		return bOk;
	}

	protected bool GetValue(string pTag,string pFieldNm,out DateTime pValue) {
		if (GetDataRow(currentTableIdx) != null) {
			if (GetDataRow(currentTableIdx).Table.Columns.Contains(pFieldNm)) {
				pValue = (DateTime)GetDataRow(currentTableIdx)[pFieldNm];
				return true;
			} else {
				pValue = DateTime.MinValue;
				parseErrorList.Add(string.Format("Unknow Column Tag:{0} Field:{1}",pTag,pFieldNm));
				return false;
			}
		} else {
			pValue = DateTime.MinValue;
			parseErrorList.Add(string.Format("DataRow is null Column Tag:{0} Field:{1}",pTag,pFieldNm));
			return false;
		}
	}

	public string IsEqual(string pTag,string pFieldNm,int pCompareValue) {
		return IsEqual(pTag,pFieldNm,pCompareValue.ToString());
	}

	public string IsEqual(string pTag,string pFieldNm,string pCompareValue) {
		string sValue;
		if (SetFieldValue(pTag,pFieldNm,out sValue)) {
			if (sValue.Equals(pCompareValue)) {
				sValue = ViCommConst.FLAG_ON.ToString();
			} else {
				sValue = ViCommConst.FLAG_OFF.ToString();
			}
		} else {
			sValue = ViCommConst.FLAG_OFF.ToString();
		}
		return sValue;
	}

	protected string IsNotEqual(string pTag,string pFieldNm,int pCompareValue) {
		return IsNotEqual(pTag,pFieldNm,pCompareValue.ToString());
	}

	protected string IsNotEqual(string pTag,string pFieldNm,string pCompareValue) {
		string sValue;
		if (SetFieldValue(pTag,pFieldNm,out sValue)) {
			if (!sValue.Equals(pCompareValue)) {
				sValue = ViCommConst.FLAG_ON.ToString();
			} else {
				sValue = ViCommConst.FLAG_OFF.ToString();
			}
		} else {
			sValue = ViCommConst.FLAG_OFF.ToString();
		}
		return sValue;
	}

	public string IsEqual(string pTag,bool pTrue) {
		string sValue;
		if (pTrue) {
			sValue = ViCommConst.FLAG_ON.ToString();
		} else {
			sValue = ViCommConst.FLAG_OFF.ToString();
		}
		return sValue;
	}

	protected string GetDiff(string pTag,string pArgument) {
		string sValue = "";
		string[] arrText = pArgument.Split(',');

		int iDiff = 0;
		int iData = 0;
		int i = 0;
		foreach (string sData in arrText) {
			int.TryParse(sData,out iData);
			if (i == 0) {
				iDiff = iData;
			} else {
				iDiff -= iData;
			}
			i++;
		}
		sValue = iDiff.ToString();
		return sValue;
	}

	protected string GetSum(string pTag,string pArgument) {
		string sValue = "";
		string[] arrText = pArgument.Split(',');

		int iSum = 0;
		int iData = 0;
		foreach (string sData in arrText) {
			int.TryParse(sData,out iData);
			iSum += iData;
		}
		sValue = iSum.ToString();
		return sValue;
	}
	protected string GetMod(string pTag,string pArgument) {
		string[] arrText = pArgument.Split(',');

		int iValue1 = ParseArg(arrText,0,0);
		int iValue2 = ParseArg(arrText,1,0);

		return (iValue1 % iValue2).ToString();
	}

	protected string GetDiv(string pTag,string pArgument) {
		string[] arrText = pArgument.Split(',');

		double dValue1 = double.Parse(ParseArg(arrText,0,"0"));
		double dValue2 = double.Parse(ParseArg(arrText,1,"0"));
		if (dValue2 == 0) {
			return string.Empty;
		}
		double dDiv = dValue1 / dValue2;

		if (arrText.Length > 2) {
			if (arrText[2].Equals("f")) {
				dDiv = Math.Floor(dDiv);
			} else if (arrText[2].Equals("c")) {
				dDiv = Math.Ceiling(dDiv);
			}
		}

		return string.Format("{0:0.###}",dDiv);
	}

	private static readonly Regex _regexPict = new Regex(@"(?<pict>\$x.{4};)",RegexOptions.Compiled);

	protected string GetSeparate(string pTag,string pArgument) {
		string sSeparateDoc;
		int iSeparateCnt;

		string[] sValues = pArgument.Split(',');

		try {
			sSeparateDoc = parseContainer.Parse(sValues[0]);
			sSeparateDoc = rgxHtmlTag.Replace(sSeparateDoc,"");
			iSeparateCnt = int.Parse(sValues[1]);
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}

		try {
			if (!sSeparateDoc.Equals("")) {

				foreach (Match m in _regexPict.Matches(sSeparateDoc)) {
					if (m.Index < iSeparateCnt) {
						iSeparateCnt += (m.Length - 1);
					}
				}
				string sNewDoc = SysPrograms.Substring(sSeparateDoc,iSeparateCnt);
				if (sValues.Length == 3) {
					if (sNewDoc.Length == sSeparateDoc.Length) {
						return sNewDoc;
					} else {
						return sNewDoc + sValues[2];
					}
				} else {
					return sNewDoc;
				}
			} else {
				return "";
			}
		} catch (Exception ex) {
			this.parseErrorList.Add(ex.Message);
			return string.Format("Substring Error Tag:{0}",pTag);
		}
	}

	protected string GetSubtractDateTime(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');

		string sValue = string.Empty;
		string sDate1 = ParseArg(oArgs,0,string.Empty);
		string sDate2 = ParseArg(oArgs,1,string.Empty);
		string sFormat = ParseArg(oArgs,2,"{0}.{1:00}:{2:00}:{3:00}");

		try {
			DateTime oDateTime1 = DateTime.Parse(sDate1);
			DateTime oDateTime2 = DateTime.Parse(sDate2);

			TimeSpan oDiffTimeSpan = oDateTime1 - oDateTime2;
			sValue = string.Format(sFormat,oDiffTimeSpan.Days,oDiffTimeSpan.Hours,oDiffTimeSpan.Minutes,oDiffTimeSpan.Seconds);
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}
		return sValue;
	}

	// 第3引数：加算する秒数（マイナス値も可）
	protected string GetConvertDateTime(string pTag,string pArgument) {
		DateTime dtConvert;
		string sConvertFormat;
		string sConvertAfter;

		string[] sValues = pArgument.Split(',');

		try {
			string sTempConvert = parseContainer.Parse(sValues[0]);
			if (string.IsNullOrEmpty(sTempConvert)) {
				return string.Empty;
			}

			if (sTempConvert.Length == 8) {
				sTempConvert = sTempConvert.Substring(0,4) + "/" + sTempConvert.Substring(4,2) + "/" + sTempConvert.Substring(6,2);
			}
			dtConvert = DateTime.Parse(sTempConvert);

			if (sValues.Length == 3) {
				int iSeconds = 0;

				if (int.TryParse(sValues[2],out iSeconds)) {
					dtConvert = dtConvert.AddSeconds(iSeconds);
				}
			}

			sConvertFormat = sValues[1];

			sConvertAfter = dtConvert.ToString(sConvertFormat);
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}

		return sConvertAfter;
	}

	//先週の○○曜日の日時を求める



	protected string GetDayOfLastWeek(string pTag,string pArgument) {
		DateTime dt = DateTime.Now.AddDays((7 + int.Parse(DateTime.Now.DayOfWeek.ToString("d"))) * -1);
		string sDate = string.Empty;

		try {
			for (int i = 0;i < 7;i++) {
				dt = dt.AddDays(1);
				if (dt.DayOfWeek.ToString("d").Equals(pArgument)) {
					sDate = dt.ToString();
				}
			}
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}

		return sDate;
	}
	
	//1か月前の日時を求める
	protected string GetDayOfLastMonth(string pTag,string pArgument) {
		DateTime dt = DateTime.Now.AddMonths(-1);
		string sDate = string.Empty;
		
		try {
			sDate = dt.ToString();
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}
		
		return sDate;
	}

	protected string GetConvertTime(string pTag,string pArgument) {
		TimeSpan oTime;
		string sConvertFormat;

		string[] sValues = pArgument.Split(',');

		try {
			string sTempConvert = parseContainer.Parse(sValues[0]);
			if (string.IsNullOrEmpty(sTempConvert)) {
				return string.Empty;
			}
			oTime = TimeSpan.Parse(sTempConvert);
			sConvertFormat = ParseArg(sValues,1,string.Empty);


			return (new DateTime(oTime.Ticks)).ToString(sConvertFormat);
		} catch {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}
	}

	protected bool ParseDiv(string pTag,string pArgument) {
		bool bNoneDisplay = false;
		string sPageNo;
		int iCnt;
		switch (pTag) {
			case "$DIV_FLAG_ON":
				int.TryParse(pArgument,out iCnt);
				bNoneDisplay = (iCnt == 0);
				break;

			case "$DIV_FLAG_OFF":
				int.TryParse(pArgument,out iCnt);
				bNoneDisplay = (iCnt != 0);
				break;

			case "$DIV_IS_GREATER":
				bNoneDisplay = !(IsGreater(pTag,pArgument));
				break;

			case "$DIV_IS_NOT_GREATER":
				bNoneDisplay = (IsGreater(pTag,pArgument));
				break;

			case "$DIV_IS_LESS":
				bNoneDisplay = !(IsLess(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_LESS":
				bNoneDisplay = (IsLess(pTag,pArgument));
				break;
			case "$DIV_IS_EQUAL":
				bNoneDisplay = !(IsEqual(pTag,pArgument));
				break;

			case "$DIV_IS_NOT_EQUAL":
				bNoneDisplay = (IsEqual(pTag,pArgument));
				break;

			case "$DIV_IS_EQUAL_STRING":
				bNoneDisplay = !(IsEqualString(pTag,pArgument));
				break;

			case "$DIV_IS_NOT_EQUAL_STRING":
				bNoneDisplay = (IsEqualString(pTag,pArgument));
				break;
			case "$DIV_IS_INSCOPE_STRING":
				bNoneDisplay = !(IsInScopeString(pTag,pArgument));
				break;

			case "$DIV_IS_NOT_INSCOPE_STRING":
				bNoneDisplay = (IsInScopeString(pTag,pArgument));
				break;
			case "$DIV_IS_VALID":
				bNoneDisplay = (!sessionObjs.valid);
				break;

			case "$DIV_IS_NOT_VALID":
				bNoneDisplay = (sessionObjs.valid);
				break;

			case "$DIV_IS_NOT_ZERO":
				int.TryParse(pArgument,out iCnt);
				bNoneDisplay = (iCnt == 0);
				break;

			case "$DIV_IS_ZERO":
				int.TryParse(pArgument,out iCnt);
				bNoneDisplay = (iCnt != 0);
				break;

			case "$DIV_IS_NULL":
				bNoneDisplay = (!pArgument.Equals(""));
				break;

			case "$DIV_IS_NOT_NULL":
				bNoneDisplay = (pArgument.Equals(""));
				break;

			case "$DIV_DOCOMO":
			case "$DIV_IS_DOCOMO":
				bNoneDisplay = !this.IsSameCarrier(ViCommConst.DOCOMO);
				break;

			case "$DIV_NOT_DOCOMO":
			case "$DIV_IS_NOT_DOCOMO":
				bNoneDisplay = this.IsSameCarrier(ViCommConst.DOCOMO);
				break;

			case "$DIV_AU":
			case "$DIV_IS_AU":
				bNoneDisplay = !this.IsSameCarrier(ViCommConst.KDDI);
				break;

			case "$DIV_NOT_AU":
			case "$DIV_IS_NOT_AU":
				bNoneDisplay = this.IsSameCarrier(ViCommConst.KDDI);
				break;

			case "$DIV_SOFTBANK":
			case "$DIV_IS_SOFTBANK":
				bNoneDisplay = !this.IsSameCarrier(ViCommConst.SOFTBANK);
				break;

			case "$DIV_NOT_SOFTBANK":
			case "$DIV_IS_NOT_SOFTBANK":
				bNoneDisplay = this.IsSameCarrier(ViCommConst.SOFTBANK);
				break;

			case "$DIV_IS_ANDROID":
				bNoneDisplay = !this.IsSameCarrier(ViCommConst.ANDROID);
				break;

			case "$DIV_IS_NOT_ANDROID":
				bNoneDisplay = this.IsSameCarrier(ViCommConst.ANDROID);
				break;

			case "$DIV_IS_IPHONE":
				bNoneDisplay = !this.IsSameCarrier(ViCommConst.IPHONE);
				break;

			case "$DIV_IS_NOT_IPHONE":
				bNoneDisplay = this.IsSameCarrier(ViCommConst.IPHONE);
				break;

			case "$DIV_IS_MOBILE":
				bNoneDisplay = !this.IsCarrierContainsOfMobile();
				break;

			case "$DIV_IS_NOT_MOBILE":
				bNoneDisplay = this.IsCarrierContainsOfMobile();
				break;

			case "$DIV_IS_FEATURE_PHONE":
				bNoneDisplay = !this.IsCarrierContainsOfFeature();
				break;

			case "$DIV_IS_NOT_FEATURE_PHONE":
				bNoneDisplay = this.IsCarrierContainsOfFeature();
				break;

			case "$DIV_IS_SMART_PHONE":
				bNoneDisplay = !this.IsCarrierContainsOfSmart();
				break;

			case "$DIV_IS_NOT_SMART_PHONE":
				bNoneDisplay = this.IsCarrierContainsOfSmart();
				break;

			case "$DIV_IS_NOPC":
				bNoneDisplay = !sessionObjs.isNoPc;
				break;

			case "$DIV_IS_NOT_NOPC":
				bNoneDisplay = sessionObjs.isNoPc;
				break;

			case "$DIV_LOGIN_USER":
				bNoneDisplay = !(sessionObjs.logined.Equals(true));
				break;

			case "$DIV_NOT_LOGIN_USER":
				bNoneDisplay = (sessionObjs.logined.Equals(true));
				break;


			case "$DIV_FIRST_PAGE":
				sPageNo = iBridUtil.GetStringValue(sessionObjs.requestQuery.QueryString["pageno"]);
				bNoneDisplay = !(sPageNo.Equals("") || sPageNo.Equals("1"));
				break;

			case "$DIV_NOT_FIRST_PAGE":
				sPageNo = iBridUtil.GetStringValue(sessionObjs.requestQuery.QueryString["pageno"]);
				bNoneDisplay = (sPageNo.Equals("") || sPageNo.Equals("1"));
				break;

			case "$DIV_IS_CAST":
				bNoneDisplay = !(sessionObjs.sexCd.Equals(ViCommConst.WOMAN));
				break;

			case "$DIV_IS_MAN":
				bNoneDisplay = !(sessionObjs.sexCd.Equals(ViCommConst.MAN));
				break;
			case "$DIV_IS_EXTEND_SEARCHE": {
					if (sessionObjs.seekCondition != null) {
						bNoneDisplay = !sessionObjs.seekCondition.hasExtend;
					}
				}
				break;
			case "$DIV_IS_NOT_EXTEND_SEARCHE": {
					if (sessionObjs.seekCondition != null) {
						bNoneDisplay = sessionObjs.seekCondition.hasExtend;
					}
				}
				break;

			case "$DIV_END":
				break;

			case "$DIV_IS_IMPERSONATED":
				bNoneDisplay = !sessionObjs.IsImpersonated;
				break;
			case "$DIV_IS_NOT_IMPERSONATED":
				bNoneDisplay = sessionObjs.IsImpersonated;
				break;
			case "$DIV_IS_ENABLED_BLOG":
				bNoneDisplay = !this.EnabledBlog();
				break;
			case "$DIV_IS_NOT_ENABLED_BLOG":
				bNoneDisplay = this.EnabledBlog();
				break;

			case "$DIV_IS_GREATER_DATETIME":
				bNoneDisplay = !(IsGreaterDataTime(pTag,pArgument));
				break;

			case "$DIV_IS_NOT_GREATER_DATETIME":
				bNoneDisplay = (IsGreaterDataTime(pTag,pArgument));
				break;

			case "$DIV_IS_LESS_DATETIME":
				bNoneDisplay = !(IsLessDataTime(pTag,pArgument));
				break;
			case "$DIV_IS_NOT_LESS_DATETIME":
				bNoneDisplay = (IsLessDataTime(pTag,pArgument));
				break;

			case "$DIV_IS_FRAME_VW":
				bNoneDisplay = !sessionObjs.isFrameVw;
				break;

			case "$DIV_IS_NOT_FRAME_VW":
				bNoneDisplay = sessionObjs.isFrameVw;
				break;

			case "$DIV_IS_ENABLED_PICKUP":
				bNoneDisplay = !IsEnabledPickup(pTag,pArgument);
				break;

			case "$DIV_IS_NOT_ENABLED_PICKUP":
				bNoneDisplay = IsEnabledPickup(pTag,pArgument);
				break;

			case "$DIV_IS_BETWEEN_TIME":
				bNoneDisplay = !IsBetweenTime(pTag,pArgument);
				break;

			case "$DIV_IS_NOT_BETWEEN_TIME":
				bNoneDisplay = IsBetweenTime(pTag,pArgument);
				break;

			case "$DIV_IS_BETWEEN_DATETIME":
				bNoneDisplay = !IsBetweenDateTime(pTag,pArgument);
				break;

			case "$DIV_IS_NOT_BETWEEN_DATETIME":
				bNoneDisplay = IsBetweenDateTime(pTag,pArgument);
				break;

			case "$DIV_IS_GCAPP":
				bNoneDisplay = string.IsNullOrEmpty(sessionObjs.gcappVersion);
				break;
			case "$DIV_IS_NOT_GCAPP":
				bNoneDisplay = !string.IsNullOrEmpty(sessionObjs.gcappVersion);
				break;

			default:
				parseContainer.parseUser.parseErrorList.Add(string.Format("NODEFINE TAG:{0}({1}) ",pTag,pArgument));
				break;
		}

		if (pTag.Equals("$DIV_END") == false) {
			SetNoneDisplay(bNoneDisplay);
		}
		return bNoneDisplay;
	}

	public void SetNoneDisplay(bool pNoneDisplayFlag) {
		if (divList.Count > 0) {
			divList[divList.Count - 1] = (pNoneDisplayFlag ? "2" : "1");
		}
	}

	private string Parse(string pHtmlSource) {
		try {
			return parseContainer.Parse(pHtmlSource);
		} catch {
			return "";
		}
	}

	public static string ConstructQueryString(NameValueCollection parameters) {
		List<string> items = new List<String>();
		foreach (string name in parameters)
			items.Add(string.Concat(name,"=",parameters[name]));
		return string.Join("&",items.ToArray());
	}

	private string GetHtmlTag(bool bIsStartTag,bool bIsSingle,string pTag,string pArgument) {
		string[] oArgArr = pArgument.Split(',');
		string sElement = ParseArg(oArgArr,0,string.Empty).ToLower().Trim();
		List<string> oAttrList = new List<string>();

		for (int i = 1;i < oArgArr.Length;i++) {
			oAttrList.Add(ParseArg(oArgArr,i,string.Empty));
		}

		StringBuilder oValueBuilder = new StringBuilder();
		oValueBuilder.Append("<");
		if (!bIsStartTag) {
			oValueBuilder.Append("/");
		}
		oValueBuilder.Append(sElement.Trim());
		foreach (string sAttr in oAttrList) {
			oValueBuilder.AppendFormat(" {0}",sAttr);
		}
		if (bIsSingle) {
			oValueBuilder.Append(" /");
		}
		oValueBuilder.Append(">");
		return oValueBuilder.ToString();
	}

	private string GetDate(string pTag,string pArgument) {
		int iValue = 0;
		if (string.IsNullOrEmpty(pArgument)) {
			pArgument = "0";
		}

		if (!int.TryParse(pArgument,out iValue)) {
			parseErrorList.Add(string.Format("intParse Error Tag:{0} Field:{1}",pTag,pArgument));
			return string.Empty;
		}

		DateTime? oNow = this.ParseCache.Get(pTag,pArgument) as DateTime?;
		if (oNow == null) {
			oNow = DateTime.Now;
			this.ParseCache.Add(pTag,pArgument,oNow);
		}
		return oNow.Value.AddDays(iValue).ToString();
	}

	private string GetButtonTag(string pArgument) {
		string sText = string.Empty;
		string sName = ViCommConst.BUTTON_SUBMIT;
		string[] sValues = pArgument.Split(',');
		sText = sValues[0];
		if (sValues.Length > 1) {
			sName = sValues[1];
		}
		return string.Format("<input value=\"{0}\" type=\"submit\" name=\"{1}\" />",Parse(sText),sName);
	}

	private string GetNextButtonTag(string pArgument) {
		string sText = string.Empty;
		string sNextUrl = "";
		string[] sValues = pArgument.Split(',');
		sText = sValues[0];
		if (sValues.Length > 1) {
			sNextUrl = sValues[1];
		}
		return string.Format("<input value=\"{0}\" type=\"hidden\" name=\"NEXTLINK\" />",sNextUrl)
		+ string.Format("<input value=\"{0}\" type=\"submit\" name=\"{1}\" />",Parse(sText),ViCommConst.BUTTON_NEXT_LINK);
	}

	private string GetGotoButtonTag(string pArgument) {
		string sText = string.Empty;
		string sNextUrl = string.Empty;
		string sButtonNo = string.Empty;
		string[] sValues = pArgument.Split(',');
		sText = sValues[0];
		if (sValues.Length > 1) {
			sNextUrl = ParseArg(sValues,1,string.Empty);
		}
		if (sValues.Length > 2) {
			sButtonNo = sValues[2];
		}
		return string.Format("<input value=\"{0}\" type=\"hidden\" name=\"GOTOLINK{1}\" />",sNextUrl,sButtonNo)
		+ string.Format("<input type=\"submit\" name=\"{0}{1}\" value=\"{2}\" />",ViCommConst.BUTTON_GOTO_LINK,sButtonNo,Parse(sText));
	}

	private string GetTextboxTag(string pArgument) {
		string[] sValues = pArgument.Split(',');
		string sText = sValues[0];
		string sName = sValues[1];
		return string.Format("<input value=\"{0}\" type=\"submit\" name=\"{1}\" />",sText,sName);
	}

	protected string GetPreviousGuidance(string pArgument) {
		string sBaseUrl = sessionObjs.root + sessionObjs.sysType;
		string sColor,sDisableColor;
		string sGuidance;
		string sAccessKey;
		string[] oArgs = pArgument.Split(',');

		if (sessionObjs.sexCd.Equals(ViCommConst.MAN)) {
			sGuidance = sessionObjs.site.profilePreviousGuidance;
		} else {
			sGuidance = sessionObjs.site.profilePreviousGuidance2;
		}

		sGuidance = ParseArg(oArgs,0,sGuidance);
		sAccessKey = ParseArg(oArgs,1,sessionObjs.site.previousAccessKey);


		GetLinkColor(out sColor,out sDisableColor);
		string sValue;
		if (!HasPrevious()) {
			sValue = string.Format("<font color=\"{0}\">{1}</font>",sDisableColor,sGuidance);
		} else {
			string sHref = sessionObjs.GetNavigateUrl(sBaseUrl,sessionId,sessionObjs.GetNextPrevUrl(sessionObjs.currentPage - 1,sessionObjs.requestQuery,this.dataTable[currentTableIdx],true));
			if (sessionObjs.sexCd.Equals(ViCommConst.WOMAN)) {
				sHref = GetHRefHasSiteCharNo(sHref);
			}
			sValue = string.Format("<a href=\"{0}\" accesskey=\"{1}\">{2}</a>",
				sHref,
				sAccessKey,
				sGuidance);
		}
		return sValue;
	}

	protected string GetNextGuidance(string pArgument) {
		string sBaseUrl = sessionObjs.root + sessionObjs.sysType;
		string sColor,sDisableColor;
		string sGuidance;
		string sAccessKey;
		string[] oArgs = pArgument.Split(',');

		if (sessionObjs.sexCd.Equals(ViCommConst.MAN)) {
			sGuidance = sessionObjs.site.profileNextGuidance;
		} else {
			sGuidance = sessionObjs.site.profileNextGuidance2;
		}
		sGuidance = ParseArg(oArgs,0,sGuidance);
		sAccessKey = ParseArg(oArgs,1,sessionObjs.site.nextAccessKey);


		GetLinkColor(out sColor,out sDisableColor);
		string sValue;
		if (!HasNext()) {
			sValue = string.Format("<font color=\"{0}\">{1}</font>",sDisableColor,sGuidance);
		} else {
			string sHref = sessionObjs.GetNavigateUrl(sBaseUrl,sessionId,sessionObjs.GetNextPrevUrl(sessionObjs.currentPage + 1,sessionObjs.requestQuery,this.dataTable[currentTableIdx],false));
			if (sessionObjs.sexCd.Equals(ViCommConst.WOMAN)) {
				sHref = GetHRefHasSiteCharNo(sHref);
			}
			sValue = string.Format("<a href=\"{0}\" accesskey=\"{1}\">{2}</a>",
					sHref,
					sAccessKey,
					sGuidance);
		}
		return sValue;
	}

	private bool HasPrevious() {
		return !(sessionObjs.currentPage <= 1 && this.isExistPrevRec[this.currentTableIdx] == false);
	}
	private bool HasNext() {
		return !(sessionObjs.currentPage >= sessionObjs.maxPage && this.isExistNextRec[this.currentTableIdx] == false);
	}

	protected string GetGivenPageGuidance(int pPageNo,string pDisplayValue) {
		string sBaseUrl = sessionObjs.root + sessionObjs.sysType;
		string sColor,sDisableColor;

		GetLinkColor(out sColor,out sDisableColor);
		string sValue;
		string sHref = sessionObjs.GetNavigateUrl(sBaseUrl,sessionId,sessionObjs.GetNextPrevUrl(pPageNo,sessionObjs.requestQuery,this.dataTable[currentTableIdx],false));
		if (sessionObjs.sexCd.Equals(ViCommConst.WOMAN)) {
			sHref = GetHRefHasSiteCharNo(sHref);
		}
		sValue = string.Format("<a href=\"{0}\" accesskey=\"{1}\">{2}</a>",
				sHref,
				sessionObjs.site.nextAccessKey,
				pDisplayValue);
		return sValue;
	}

	protected string GetGivenPageGuidance(string pArgument) {
		string[] sValues = pArgument.Split(',');

		int iPageNo = ParseArg(sValues,0,1);
		string sGuidance = ParseArg(sValues,(sValues.Length - 1),iPageNo.ToString());

		return GetGivenPageGuidance(iPageNo,sGuidance);

	}

	protected void GetLinkColor(out string pColor,out string pDisableColor) {

		pColor = "";
		pDisableColor = "";
		if (sessionObjs.currentViewMask == 0) {
			pColor = sessionObjs.site.colorLine;
			pDisableColor = sessionObjs.site.colorBack;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_ACT_CATEGORY) != 0) {
			pColor = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorLink;
			pDisableColor = ((ActCategory)sessionObjs.categoryList[sessionObjs.currentCategoryIndex]).colorBack;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_USER_RANK) != 0) {
			pColor = sessionObjs.userRank.colorLink;
			pDisableColor = sessionObjs.userRank.colorBack;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_AD_GROUP) != 0) {
			pColor = sessionObjs.adGroup.colorLink;
			pDisableColor = sessionObjs.adGroup.colorBack;

		} else if ((sessionObjs.currentViewMask & ViCommConst.VIEW_MASK_HTML_DOC_COLOR) != 0) {
			pColor = sessionObjs.webFace.colorLink;
			pDisableColor = sessionObjs.webFace.colorBack;
		}
	}

	private string GetInqIdpwMailAddr() {
		return string.Format("catf@{0}",sessionObjs.site.mailHost);
	}

	private bool IsGreater(string pTag,string pArgument) {
		int iValue1 = 0,iValue2 = 0;

		string[] sValues = pArgument.Split(',');

		if (!int.TryParse(sValues[0],out iValue1)) {
			parseErrorList.Add(string.Format("intParse Error Tag:{0} Field:{1}",pTag,sValues[0]));
			return false;
		}
		if (!int.TryParse(sValues[1],out iValue2)) {
			parseErrorList.Add(string.Format("intParse Error Tag:{0} Field:{1}",pTag,sValues[1]));
			return false;
		}

		return (iValue1 > iValue2);
	}

	private bool IsEqual(string pTag,string pArgument) {
		int iValue1 = 0,iValue2 = 0;

		string[] sValues = pArgument.Split(',');

		if (!int.TryParse(sValues[0],out iValue1)) {
			parseErrorList.Add(string.Format("intParse Error Tag:{0} Field:{1}",pTag,sValues[0]));
			return false;
		}
		if (!int.TryParse(sValues[1],out iValue2)) {
			parseErrorList.Add(string.Format("intParse Error Tag:{0} Field:{1}",pTag,sValues[1]));
			return false;
		}

		return (iValue1 == iValue2);
	}

	private bool IsEqualString(string pTag,string pArgument) {
		string[] sValues = pArgument.Split(',');
		if (sValues.Length != 2) {
			parseErrorList.Add(string.Format("IsEqualString Error Tag:{0} Argument:{1}",pTag,pArgument));
			return false;
		}
		return (sValues[0].Equals(sValues[1]));
	}

	private bool IsInScopeString(string pTag,string pArgument) {
		bool bIsInScope = false;

		string[] sValues = pArgument.Split(',');
		string sTargetValue = ParseArg(sValues,0,string.Empty);

		if (!string.IsNullOrEmpty(sTargetValue)) {
			for (int i = 1;i < sValues.Length;i++) {
				string sExpectedValue = ParseArg(sValues,i,string.Empty);

				if (string.IsNullOrEmpty(sExpectedValue)) {
					continue;
				}

				if (sTargetValue.Equals(sExpectedValue)) {
					bIsInScope = true;
					break;
				}
			}
		}

		return bIsInScope;
	}

	private bool IsLess(string pTag,string pArgument) {
		int iValue1 = 0,iValue2 = 0;

		string[] sValues = pArgument.Split(',');


		if (!int.TryParse(sValues[0],out iValue1)) {
			parseErrorList.Add(string.Format("intParse Error Tag:{0} Field:{1}",pTag,sValues[0]));
			return false;
		}
		if (!int.TryParse(sValues[1],out iValue2)) {
			parseErrorList.Add(string.Format("intParse Error Tag:{0} Field:{1}",pTag,sValues[1]));
			return false;
		}

		return (iValue1 < iValue2);
	}

	private bool IsGreaterDataTime(string pTag,string pArgument) {
		string[] sValues = pArgument.Split(',');

		DateTime oValue1 = DateTime.Parse(ParseArg(sValues,0,"1900/01/01 00:00:00"));
		DateTime oValue2 = DateTime.Parse(ParseArg(sValues,1,"1900/01/01 00:00:00"));
		return (DateTime.Compare(oValue1,oValue2) > 0);

	}

	private bool IsLessDataTime(string pTag,string pArgument) {
		string[] sValues = pArgument.Split(',');

		DateTime oValue1 = DateTime.Parse(ParseArg(sValues,0,"1900/01/01 00:00:00"));
		DateTime oValue2 = DateTime.Parse(ParseArg(sValues,1,"1900/01/01 00:00:00"));
		return (DateTime.Compare(oValue1,oValue2) < 0);

	}

	protected virtual string GetCastCount(string pTag,string pSiteCd,string pUserSeq,string pUserCharNo,string pActCategorySeq,Cast pCast) {
		int iNewCastFlag;
		int iOnlineStatus;
		ulong uConditionFlag;
		string sLastLoginDate = string.Empty;

		switch (pTag) {
			case "$CAST_ONLINE_COUNT":
				iOnlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
				iNewCastFlag = 0;
				uConditionFlag = 0;
				break;
			case "$CAST_LOGINED_COUNT":
				iOnlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
				iNewCastFlag = 0;
				uConditionFlag = 0;
				break;
			case "$CAST_TVTEL_COUNT":
				iOnlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
				iNewCastFlag = 0;
				uConditionFlag = ViCommConst.INQUIRY_CAST_ONLINE_TV_PHONE;
				break;
			case "$CAST_NEW_COUNT":
				iOnlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
				iNewCastFlag = 1;
				uConditionFlag = 0;
				break;
			case "$CAST_WAITING_COUNT":	// ログイン中 + 待機中 
				iOnlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
				iNewCastFlag = 0;
				uConditionFlag = ViCommConst.INQUIRY_CAST_WAITING;
				break;
			case "$CAST_TALKING_COUNT":
				iOnlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
				iNewCastFlag = 0;
				uConditionFlag = ViCommConst.INQUIRY_CAST_MONITOR;
				break;
			case "$CAST_TODAY_LOGINED_COUNT":
				iOnlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
				iNewCastFlag = 0;
				uConditionFlag = 0;
				sLastLoginDate = DateTime.Now.AddDays(-1).ToString();
				break;
			default:
				iOnlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
				iNewCastFlag = 0;
				uConditionFlag = 0;
				break;
		}

		decimal iRecCount;
		using (SeekCondition oSeekCondition = new SeekCondition()) {
			oSeekCondition.siteCd = pSiteCd;
			oSeekCondition.userSeq = pUserSeq;
			oSeekCondition.userCharNo = pUserCharNo;
			oSeekCondition.onlineStatus = iOnlineStatus;
			oSeekCondition.conditionFlag = uConditionFlag;
			oSeekCondition.categorySeq = pActCategorySeq;
			if (iNewCastFlag != 0) {
				oSeekCondition.newCastDay = sessionObjs.site.newFaceDays;
			} else {
				oSeekCondition.newCastDay = 0;
			}
			if (!string.IsNullOrEmpty(sLastLoginDate)) {
				oSeekCondition.lastLoginDate = sLastLoginDate;
			}
			pCast.GetPageCount(oSeekCondition,1,out iRecCount);
		}
		return pCast.recCount.ToString();
	}

	protected string GetActCategorySeq(string pTag,string pArgument) {
		if (pArgument.Equals("")) {
			return ((ActCategory)sessionObjs.categoryList[1]).actCategorySeq;
		} else {
			return ((ActCategory)sessionObjs.categoryList[int.Parse(pArgument)]).actCategorySeq;
		}
	}

	protected string GetNewAdminReport(string pSiteCd,string pSexCd,int pRNum,string pField) {
		string sValue;
		if (pRNum == 0) {
			pRNum = 1;
		}
		using (AdminReport oAdminReport = new AdminReport()) {
			DataSet ds = oAdminReport.GetPageCollection(pSiteCd,pSexCd,pRNum,1);
			if (ds.Tables["VW_ADMIN_REPORT01"].Rows.Count != 0) {
				sValue = ds.Tables["VW_ADMIN_REPORT01"].Rows[0][pField].ToString();
			} else {
				sValue = "";
			}
		}
		return sValue;
	}

	protected string GetCompare(string pTag,string pArgument) {
		string[] sValues = pArgument.Split(new char[] { ',' },2);
		if (sValues.Length != 2)
			return ViCommConst.FLAG_OFF_STR;

		string sValue1 = parseContainer.Parse(sValues[0]);
		string sValue2 = parseContainer.Parse(sValues[1]);

		return sValue1.Equals(sValue2) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}

	protected string GetDecode(string pTag,string pArgument) {
		string[] sValues = pArgument.Split(new char[] { ',' },4);
		if (sValues.Length < 3)
			return string.Empty;

		string sValue1 = parseContainer.Parse(sValues[0]);
		string sValue2 = parseContainer.Parse(sValues[1]);
		string sValue3 = parseContainer.Parse(sValues[2]);
		string sValue4 = (sValues.Length < 4) ? string.Empty : parseContainer.Parse(sValues[3]);

		return (sValue1.Equals(sValue2)) ? sValue3 : sValue4;
	}

	protected int GetMailNewFlag(string pTag) {
		DateTime dtCreate;
		if (GetValue(pTag,"CREATE_DATE",out dtCreate)) {
			if (DateTime.Now.AddHours(sessionObjs.site.RxMailNewHours * -1) <= dtCreate) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

	public string ParseArg(string[] pArgArry,int pIndex,string pDefaultValue) {
		string sValue = pDefaultValue;

		if (pArgArry.Length >= pIndex + 1 && !string.IsNullOrEmpty(pArgArry[pIndex])) {
			sValue = parseContainer.Parse(pArgArry[pIndex]);
		}
		return sValue;
	}
	public int ParseArg(string[] pArgArry,int pIndex,int pDefaultValue) {
		int iValue = pDefaultValue;
		if (pArgArry.Length >= pIndex + 1 && !string.IsNullOrEmpty(pArgArry[pIndex])) {
			iValue = int.Parse(parseContainer.Parse(pArgArry[pIndex]));
		}
		return iValue;
	}

	protected string GetReQuery(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');
		if (oArgs.Length == 0) {
			return sessionObjs.requestQuery.Url.Query;
		}

		NameValueCollection oQeury = new NameValueCollection(sessionObjs.requestQuery.QueryString);
		for (int i = 0;i < oArgs.Length;i++) {
			oQeury.Remove(oArgs[i]);
		}
		return ConstructQueryString(oQeury);
	}

	private string GetHRefHasSiteCharNo(string pValue) {
		if (!pValue.Contains("site=")) {
			string sSiteCd = iBridUtil.GetStringValue(sessionObjs.requestQuery.QueryString["site"]);
			if (!sSiteCd.Equals(string.Empty)) {
				pValue += string.Format("&site={0}",sSiteCd);
			}
		}
		if (!pValue.Contains("charno=")) {
			string sCharNo = iBridUtil.GetStringValue(sessionObjs.requestQuery.QueryString["charno"]);
			if (!sCharNo.Equals(string.Empty)) {
				pValue += string.Format("&charno={0}",sCharNo);
			}
		}
		return pValue;
	}


	private string GetRandomAd() {
		SessionObjs oSessionObjs = sessionObjs;
		if (sessionObjs.IsImpersonated) {
			oSessionObjs = sessionObjs.GetOriginalObj();
		}

		string sSexCd;
		if (oSessionObjs is SessionMan) {
			sSexCd = ViCommConst.MAN;
		} else {
			sSexCd = ViCommConst.OPERATOR;
		}

		string sAd = "";

		DataSet ds;
		using (RandomAdDayOfWeek oRandomAd = new RandomAdDayOfWeek()) {
			ds = oRandomAd.GetOne(oSessionObjs.site.siteCd,sSexCd);
		}

		//レコードが存在しない場合は終了 
		DataTable dt = ds.Tables[0];
		if (dt.Rows.Count == 0) {
			return sAd;
		}

		//設定されている広告の数を取得 
		DataRow dr = dt.Rows[0];
		int iAllAdCnt = 0;
		foreach (DataColumn dc in dt.Columns) {
			if (dc.ColumnName.IndexOf("HTML_AD") >= 0) {
				if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString())) {
					iAllAdCnt++;
				}
			}
		}

		//設定されていない場合は終了 
		if (iAllAdCnt == 0) {
			return sAd;
		}

		//設定されているものを全て表示した場合は終了 
		if (iAllAdCnt <= oSessionObjs.iAdCnt) {
			return sAd;
		}

		//広告をランダムに取得 
		int iRnd = 0;
		bool bGet = false;
		Random rnd = new Random();
		int iLoopCnt = 0;
		iAllAdCnt++;
		while (!bGet && iLoopCnt < iAllAdCnt * 10) {
			bGet = true;
			iRnd = rnd.Next(1,iAllAdCnt);
			int i;
			for (i = 0;i < 10;i++) {
				if (!string.IsNullOrEmpty(oSessionObjs.arAdNo[i])) {
					if (iRnd.ToString().Equals(oSessionObjs.arAdNo[i])) {
						bGet = false;
						break;
					}
				} else {
					break;
				}
			}
			if (bGet) {
				sAd = dr[string.Format("HTML_AD{0}",iRnd.ToString())].ToString();
				if (!string.IsNullOrEmpty(sAd)) {
					oSessionObjs.arAdNo[i] = iRnd.ToString();
					oSessionObjs.iAdCnt++;
					break;
				} else {
					sAd = "";
					bGet = false;
				}
			}
			iLoopCnt++;
		}
		return sAd;
	}

	protected bool IsSameCarrier(string carrier) {
		return carrier.Equals(sessionObjs.carrier);
	}

	protected bool IsCarrierContainsOfMobile() {
		switch (this.sessionObjs.carrier) {
			case ViCommConst.DOCOMO:
			case ViCommConst.KDDI:
			case ViCommConst.SOFTBANK:
			case ViCommConst.ANDROID:
			case ViCommConst.IPHONE:
				return true;
			default:
				return false;
		}
	}

	protected bool IsCarrierContainsOfFeature() {
		switch (this.sessionObjs.carrier) {
			case ViCommConst.DOCOMO:
			case ViCommConst.KDDI:
			case ViCommConst.SOFTBANK:
				return true;
			default:
				return false;
		}
	}

	protected bool IsCarrierContainsOfSmart() {
		switch (this.sessionObjs.carrier) {
			case ViCommConst.ANDROID:
			case ViCommConst.IPHONE:
				return true;
			default:
				return false;
		}
	}

	public string ReplaceBr(string pValue) {
		string sFlag = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["ReplaceBr"]);
		if (sFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			return pValue.Replace("\r\n","<BR />");
		} else {
			return pValue;
		}
	}

	protected string RemoveHtml(string pArgument) {
		string[] oArgs = pArgument.Split(',');
		string sValue = ParseArg(oArgs,0,string.Empty);
		bool bIgnoreBrTag = ParseArg(oArgs,1,ViCommConst.FLAG_ON_STR).Equals(ViCommConst.FLAG_ON_STR);

		sValue = ViCommPrograms.RemoveHtml(sValue,true);
		if (!bIgnoreBrTag) {
			sValue = ViCommPrograms.ReplaceBr(sValue,Environment.NewLine);
		}
		return sValue;
	}

	protected void AddBonusPoint(string pSiteCd,string pUserSeq,string pArgument) {
		if (pArgument.Equals(string.Empty)) {
			return;
		}
		string sResult = string.Empty;
		using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
			oUserDefPoint.AddBonusPoint(pSiteCd,pUserSeq,pArgument,out sResult);
		}
		if (sResult.Equals(ViCommConst.UserDefPointResult.NOT_FOUND)) {
			parseContainer.parseUser.parseErrorList.Add(string.Format("Not Found UserDefPoint.AddPointId:{0}",pArgument));
		}
	}

	protected string CheckBonusPoint(string pSiteCd,string pUserSeq,string pArgument) {
		if (pArgument.Equals(string.Empty)) {
			return string.Empty;
		}
		string sResult = string.Empty;
		using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
			oUserDefPoint.CheckBonusPoint(pSiteCd,pUserSeq,pArgument,out sResult);
		}
		return sResult;
	}
	public class DesignPartsArgument {
		private int needCount = 3;
		private string htmlTemplate = string.Empty;
		private NameValueCollection query;

		public int NeedCount {
			get {
				return this.needCount;
			}
		}
		public string HtmlTemplate {
			get {
				return this.htmlTemplate;
			}
		}
		public NameValueCollection Query {
			get {
				return this.query;
			}
		}

		public static DesignPartsArgument Parse(ParseViComm pParser,string pTag,string pArgument) {
			string[] oArgArr = pArgument.Split(',');
			string sHtmlTemplate = oArgArr[0];	// パースしない 
			int iNeedCount = pParser.ParseArg(oArgArr,1,3);

			DesignPartsArgument oInstance = new DesignPartsArgument();
			oInstance.htmlTemplate = sHtmlTemplate;
			oInstance.needCount = iNeedCount;
			oInstance.query = Convert2Query(pParser,oArgArr,2);
			return oInstance;
		}

		public static NameValueCollection Convert2Query(ParseViComm pParser,string[] pArguments,int pStartIndex) {

			NameValueCollection oQuery = new NameValueCollection();
			for (int i = pStartIndex;i < pArguments.Length;i++) {
				string[] sQueryItem = pParser.ParseArg(new string[] { pArguments[i] },0,string.Empty).Split(new char[] { '=' },2);
				string sKey = sQueryItem[0];
				string sValue = string.Empty;
				if (sQueryItem.Length == 2) {
					sValue = sQueryItem[1];
				}
				oQuery.Add(sKey,sValue);
			}
			return oQuery;
		}

	}

	#region □■□ Blog 関連 □■□ ===================================================================================

	protected string GetHRefBlogMovieDwonload(string pTag,string pArgument,string pSiteCd,string pUserSeq,string pUserCharNo,string pObjSeq) {
		return ParseDownloadUrl(
							"1",
							pSiteCd,
							pUserSeq,
							pUserCharNo,
							pObjSeq,
							pArgument,
							string.Empty,
							false,
							"ViewBlogObjs.aspx");
	}

	#region BlogObjs ------------------------------------------------------------------------------

	protected string GetListBlogPicUnusedConf(string pTag,string pArgument,string pSiteCd,string pUserSeq,string pUserCharNo) {

		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		BlogObjSeekCondition oCondition = new BlogObjSeekCondition(pPartsArgs.Query);
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ;
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.FileType = ViCommConst.BlogFileType.PIC;
		oCondition.UsedFlag = ViCommConst.FLAG_OFF_STR;

		return GetListBlogObjBase(oCondition,pPartsArgs.HtmlTemplate,pPartsArgs.NeedCount);
	}
	protected string GetListBlogMovieUnusedConf(string pTag,string pArgument,string pSiteCd,string pUserSeq,string pUserCharNo) {

		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		BlogObjSeekCondition oCondition = new BlogObjSeekCondition(pPartsArgs.Query);
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ;
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.FileType = ViCommConst.BlogFileType.MOVIE;
		oCondition.UsedFlag = ViCommConst.FLAG_OFF_STR;

		return GetListBlogObjBase(oCondition,pPartsArgs.HtmlTemplate,pPartsArgs.NeedCount);
	}

	private string GetListBlogObjBase(BlogObjSeekCondition pCondition,string pHtmlTemplate,int iNeedCnt) {

		DataSet oTmpDataSet = null;
		using (BlogObj oBlogObj = new BlogObj(sessionObjs)) {
			oTmpDataSet = oBlogObj.GetPageCollection(pCondition,1,iNeedCnt);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		string iPrevSeekMode = sessionObjs.seekMode;
		SeekConditionBase oPrevSeekConditionBase = sessionObjs.seekConditionEx;
		try {
			sessionObjs.seekMode = pCondition.SeekMode.ToString();
			sessionObjs.seekConditionEx = pCondition;
			return parseContainer.ParseDirect(pHtmlTemplate,oTmpDataSet,ViCommConst.DATASET_EX_BLOG_OBJ);
		} finally {
			sessionObjs.seekMode = iPrevSeekMode;
			sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}
	}

	#endregion ------------------------------------------------------------------------------------


	#region BlogArticle ---------------------------------------------------------------------------
	protected string GetListBlogArticle(string pTag,string pArgument,string pSiteCd) {

		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		BlogArticleSeekCondition oCondition = new BlogArticleSeekCondition(pPartsArgs.Query);
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE;
		oCondition.SiteCd = pSiteCd;
		oCondition.BlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;
		oCondition.UseTemplate = true;

		return GetListBlogArticleBase(oCondition,pPartsArgs.HtmlTemplate,pPartsArgs.NeedCount);
	}
	protected string GetListBlogArticlePickup(string pTag,string pArgument,string pSiteCd) {

		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		BlogArticleSeekCondition oCondition = new BlogArticleSeekCondition(pPartsArgs.Query);
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP;
		oCondition.SiteCd = pSiteCd;
		oCondition.BlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;
		oCondition.UseTemplate = true;

		return GetListBlogArticleBase(oCondition,pPartsArgs.HtmlTemplate,pPartsArgs.NeedCount);
	}
	protected string GetListBlogArticleConf(string pTag,string pArgument,string pSiteCd,string pUserSeq,string pUserCharNo) {

		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		BlogArticleSeekCondition oCondition = new BlogArticleSeekCondition(pPartsArgs.Query);
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE;
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.UseTemplate = true;

		return GetListBlogArticleBase(oCondition,pPartsArgs.HtmlTemplate,pPartsArgs.NeedCount);
	}

	protected string GetListBlogArticleBase(BlogArticleSeekCondition pCondition,string pHtmlTemplate,int iNeedCnt) {

		DataSet oTmpDataSet = null;
		using (BlogArticle oBlogArticle = new BlogArticle(sessionObjs)) {
			oTmpDataSet = oBlogArticle.GetPageCollection(pCondition,1,iNeedCnt);
		}

		if (oTmpDataSet == null)
			return string.Empty;


		string iPrevSeekMode = sessionObjs.seekMode;
		SeekConditionBase oPrevSeekConditionBase = sessionObjs.seekConditionEx;
		try {
			sessionObjs.seekMode = pCondition.SeekMode.ToString();
			sessionObjs.seekConditionEx = pCondition;
			return parseContainer.ParseDirect(pHtmlTemplate,oTmpDataSet,ViCommConst.DATASET_EX_BLOG_ARTICLE);
		} finally {
			sessionObjs.seekMode = iPrevSeekMode;
			sessionObjs.seekConditionEx = oPrevSeekConditionBase;
		}
	}
	protected string GetBlogArticleDocLength(string pTag,string pArgument,string pSiteCd,string pUserSeq,string pUserCharNo,string pBlogSeq,string pBlogArticleSeq) {

		string[] oValues;
		if (!GetValues(pTag,"BLOG_ARTICLE_BODY1,BLOG_ARTICLE_BODY2,BLOG_ARTICLE_BODY3,BLOG_ARTICLE_BODY4,BLOG_ARTICLE_BODY5",out oValues)) {
			return string.Empty;
		}

		StringBuilder oDocBuilder = new StringBuilder();
		for (int i = 0;i < 5;i++) {
			oDocBuilder.Append(oValues[i]);
		}

		return oDocBuilder.ToString().Trim().Length.ToString();
	}
	protected string GetBlogArticleDoc(string pTag,string pArgument,string pSiteCd,string pUserSeq,string pUserCharNo,string pBlogSeq,string pBlogArticleSeq) {

		string[] oValues;
		if (!GetValues(pTag,"BLOG_ARTICLE_BODY1,BLOG_ARTICLE_BODY2,BLOG_ARTICLE_BODY3,BLOG_ARTICLE_BODY4,BLOG_ARTICLE_BODY5",out oValues)) {
			return string.Empty;
		}

		StringBuilder oDocBuilder = new StringBuilder();
		string sReplaceDoc;
		for (int i = 0;i < 5;i++) {
			sReplaceDoc = oValues[i].Replace(Environment.NewLine,"<br />");
			sReplaceDoc = sReplaceDoc.Replace("\n","<br />");
			sReplaceDoc = sReplaceDoc.Replace("\r","<br />");
			oDocBuilder.Append(sReplaceDoc);
		}

		BlogObjSeekCondition oCondition = new BlogObjSeekCondition();
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ;
		oCondition.SiteCd = pSiteCd;
		oCondition.UserSeq = pUserSeq;
		oCondition.UserCharNo = pUserCharNo;
		oCondition.BlogSeq = pBlogSeq;
		oCondition.BlogArticleSeq = pBlogArticleSeq;
		oCondition.Sort = "BLOG_ARTICLE_OBJS_REGIST_DATE ASC";

		using (BlogObj oBlogObj = new BlogObj(sessionObjs)) {
			using (DataSet oBlogObjDataSet = oBlogObj.GetPageCollection(oCondition,1,1000)) {
				return BlogHelper.RegexBlogArticleTag.Replace(oDocBuilder.ToString(),delegate(Match oMatch) {
					string sValue = string.Empty;

					int iIndex = int.Parse(oMatch.Groups["index"].Value) - 1;
					if (iIndex >= 0 && oBlogObjDataSet.Tables[0].Rows.Count > iIndex) {
						DataRow oBlogObjRow = oBlogObjDataSet.Tables[0].Rows[iIndex];

						string sObjSeq = iBridUtil.GetStringValue(oBlogObjRow["OBJ_SEQ"]);
						string sBlogFileType = iBridUtil.GetStringValue(oBlogObjRow["BLOG_FILE_TYPE"]);
						switch (sBlogFileType) {
							case ViCommConst.BlogFileType.MOVIE:
								sValue = GetHRefBlogMovieDwonload(pTag,"動画ﾌｧｲﾙ",pSiteCd,pUserSeq,pUserCharNo,sObjSeq);
								break;
							case ViCommConst.BlogFileType.PIC:
								sValue = string.Format("<center><img src=\"../{0}\"/></center>",iBridUtil.GetStringValue(oBlogObjRow["OBJ_PHOTO_IMG_PATH"]));
								break;
						}
					}
					return sValue;
				});
			}
		}

	}
	#endregion ------------------------------------------------------------------------------------

	#region Blog ----------------------------------------------------------------------------------
	protected string GetListBlog(string pTag,string pArgument,string pSiteCd) {

		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		BlogSeekCondition oCondition = new BlogSeekCondition(pPartsArgs.Query);
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG;
		oCondition.SiteCd = pSiteCd;

		DataSet oTmpDataSet = this.ParseCache.Get(pTag,oCondition.ToString()) as DataSet;

		if (oTmpDataSet == null) {
			using (Blog oBlog = new Blog(sessionObjs)) {
				oTmpDataSet = oBlog.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
			}
			this.ParseCache.Add(pTag,oCondition.ToString(),oTmpDataSet);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_EX_BLOG);
	}


	private string GetQueryBlogBase(IDictionary<string,string> pAppendQuerys) {

		if (sessionObjs.seekConditionEx != null) {

			NameValueCollection oQuery = sessionObjs.seekConditionEx.GetQuery();
			foreach (string sKey in oQuery.Keys) {
				if (string.IsNullOrEmpty(sKey))
					continue;

				string sQueryValue = oQuery[sKey].Trim();
				if (string.IsNullOrEmpty(sQueryValue))
					continue;

				if (!pAppendQuerys.ContainsKey(sKey)) {
					pAppendQuerys.Add(sKey,sQueryValue);
				}

			}
		}
		StringBuilder oQueryBuilder = new StringBuilder();
		foreach (string sKey in pAppendQuerys.Keys) {
			if (string.IsNullOrEmpty(sKey))
				continue;

			oQueryBuilder.AppendFormat("&{0}={1}",sKey,System.Web.HttpUtility.UrlEncode(pAppendQuerys[sKey]));
		}



		return oQueryBuilder.ToString();
	}

	protected string GetQueryBlogArticle(string pBlogArticleSeq) {
		IDictionary<string,string> oQuery = new Dictionary<string,string>();
		oQuery.Add("blog_article_seq",pBlogArticleSeq);
		oQuery.Add("seekmode",sessionObjs.seekMode);
		return GetQueryBlogBase(oQuery);
	}
	protected string GetQueryBlogObj(string pBlogObjSeq) {
		IDictionary<string,string> oQuery = new Dictionary<string,string>();
		oQuery.Add("obj_seq",pBlogObjSeq);
		return GetQueryBlogBase(oQuery);
	}

	#endregion ------------------------------------------------------------------------------------
	#endregion ========================================================================================================


	protected string ExecuteHttpGet(string pTag,string pArgument) {
		string sValue = string.Empty;
		Encoding oEncoding = SysConst.EncodingShiftJIS;

		string[] oArgs = pArgument.Split(new char[] { ',' });
		string sUrl = ParseArg(oArgs,0,string.Empty);
		string sEncoding = ParseArg(oArgs,1,string.Empty);

		if (string.IsNullOrEmpty(sUrl)) {
			throw new ApplicationException("Url is Empty.");
		}

		if (!string.IsNullOrEmpty(sEncoding)) {
			oEncoding = Encoding.GetEncoding(sEncoding);
		}


		System.Net.WebRequest oReq = System.Net.WebRequest.Create(sUrl);
		System.Net.WebResponse oRes = oReq.GetResponse();
		using (StreamReader oSr = new StreamReader(oRes.GetResponseStream(),oEncoding)) {
			sValue = oSr.ReadToEnd();
			oSr.Close();
		}
		return sValue;
	}

	protected string GetSplitLineValue(string pTag,string pArgument) {
		string sValue = string.Empty;

		string[] oArgs = pArgument.Split(new char[] { ',' });
		string sHtml = ParseArg(oArgs,0,string.Empty);
		string sTarget = ParseArg(oArgs,1,string.Empty);

		return GetSplitValueBase(pTag,sHtml,sTarget,new string[] { "\r\n","\n" });
	}
	protected string GetSplitValueBase(string pTag,string pHtml,string pTarget,string[] pSeparators) {
		string sValue = string.Empty;
		StringBuilder oValueBuilder = new StringBuilder();
		foreach (string sSplitValue in pTarget.Split(pSeparators,StringSplitOptions.RemoveEmptyEntries)) {
			oValueBuilder.Append(parseContainer.Parse(string.Format(pHtml,sSplitValue)));
		}
		return oValueBuilder.ToString();
	}

	private string GetRandomValue(string pArgument) {
		string[] args = pArgument.Split(',');
		int result = sessionObjs.GetRandomDictonaryValue(
							ParseArg(args,0,string.Empty)
							,ParseArg(args,1,0)
							,ParseArg(args,2,0)
							);

		return result.ToString();
	}

	private static readonly Regex _regexBody
		= new Regex("(?:<div(?:(?:\"[^\"]*\"|'[^']*'|[^'\">])*)(?:id=\"pc\")(?:(?:\"[^\"]*\"|'[^']*'|[^'\">])*)[>]+?.*\n.*)?(?:<body(?:\"[^\"]*\"|'[^']*'|[^'\">])*>){1}"
						,RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

	public override string AfterParse(string pParsedValue) {
		if (delayParseStartBody) {
			pParsedValue = _regexBody.Replace(pParsedValue,delegate(Match pMatch) {
				return ParseStartBodyImpl(colorBack,colorChar,colorLink,colorALink,colorVLink);
			});

		}
		return pParsedValue;

	}

	private string GetPageNavigation(string pTag,string pArgument) {
		// 引数パース
		string[] oArgArray = pArgument.Split(',');
		int iRange = ParseArg(oArgArray,0,3);
		string sCurrPageFontColor = ParseArg(oArgArray,1,"red");
		bool bNeedEdgeNavi = ParseArg(oArgArray,2,1) == ViCommConst.FLAG_ON;

		string pDesignatedSeparator = string.Empty;
		if (oArgArray.Length > 3 && !string.IsNullOrEmpty(oArgArray[3])) {
			pDesignatedSeparator = oArgArray[3];
		}
		return GetPageNavigation(iRange,sCurrPageFontColor,bNeedEdgeNavi,pDesignatedSeparator);
	}

	private string GetPageNavigation(int pRange,string pCurrPageFontColor,bool pNeedEdgeNavi,string pDesignatedSeparator) {
		StringBuilder oPageNaviBuilder = new StringBuilder();

		int iFirstPageNo = 1;
		int iLastPageNo = sessionObjs.maxPage;
		int iCurrPageNo = sessionObjs.currentPage;

		string sSeparator = "&nbsp;";
		int iDiffRange = pRange / 2;
		int iStartPage = 0;
		int iEndPage = 0;

		if (iCurrPageNo < pRange) {
			/*** 先端 ***/
			iStartPage = iFirstPageNo;
			iEndPage = Math.Min(pRange,iLastPageNo);
		} else if (iCurrPageNo > (iLastPageNo - (pRange - 1))) {
			/*** 末端 ***/
			iStartPage = Math.Max(iFirstPageNo,(iLastPageNo - (pRange - 1)));
			iEndPage = iLastPageNo;
		} else {
			/*** 中間 ***/
			iStartPage = Math.Max(iFirstPageNo,(iCurrPageNo - iDiffRange));
			iEndPage = Math.Min(iLastPageNo,(iCurrPageNo + iDiffRange));
		}


		if (iStartPage > iFirstPageNo) {
			if (pNeedEdgeNavi) {
				oPageNaviBuilder.Append(GetGivenPageGuidance(iFirstPageNo,"<<"));
				oPageNaviBuilder.Append(sSeparator);
			}
			oPageNaviBuilder.Append(GetGivenPageGuidance(iStartPage - 1,"…"));
			oPageNaviBuilder.Append(sSeparator);
		}
		for (int i = iStartPage;i < iEndPage + 1;i++) {
			if (i > iStartPage) {
				oPageNaviBuilder.Append(sSeparator);
			}
			string sGuidance = (i == iCurrPageNo) ? string.Format("<font color='{1}'>{0}</font>",i,pCurrPageFontColor) : i.ToString();
			oPageNaviBuilder.Append(GetGivenPageGuidance(i,sGuidance));
			if (i < iEndPage && !string.IsNullOrEmpty(pDesignatedSeparator)) {
				oPageNaviBuilder.Append(sSeparator);
				oPageNaviBuilder.Append(pDesignatedSeparator);
			}
		}
		if (iEndPage < iLastPageNo) {
			oPageNaviBuilder.Append(sSeparator);
			oPageNaviBuilder.Append(GetGivenPageGuidance(iEndPage + 1,"…"));
			oPageNaviBuilder.Append(sSeparator);
			if (pNeedEdgeNavi) {
				oPageNaviBuilder.Append(GetGivenPageGuidance(iLastPageNo,">>"));
			}
		}

		return oPageNaviBuilder.ToString();
	}

	private string GetHrefWithAddBonusPoint(string pArgument) {
		StringBuilder sHrefWithAddBonusPointBiulder = new StringBuilder();



		return sHrefWithAddBonusPointBiulder.ToString();
	}

	protected bool EnabledBlog() {
		bool bEnabledBlog = false;

		if (sessionObjs is SessionWoman) {
			bEnabledBlog = this.EnabledBlog((SessionWoman)sessionObjs);
		} else if (sessionObjs is SessionMan) {
			if (sessionObjs.IsImpersonated) {
				SessionObjs oOriginalObj = sessionObjs.GetOriginalObj();
				if (oOriginalObj is SessionWoman) {
					bEnabledBlog = this.EnabledBlog((SessionWoman)oOriginalObj);
				} else {
					bEnabledBlog = true;
				}
			} else {
				bEnabledBlog = true;
			}
		}
		return bEnabledBlog;
	}
	private bool EnabledBlog(SessionWoman pSessionWoman) {
		bool bEnabledBlog = false;
		if (pSessionWoman.logined && pSessionWoman.userWoman.CurCharacter != null) {
			bEnabledBlog = (pSessionWoman.userWoman.CurCharacter.characterEx.enabledBlogFlag == ViCommConst.FLAG_ON);
		}
		return bEnabledBlog;
	}

	private bool EnabledRichino() {
		bool bEnabledRichino = false;

		if (sessionObjs is SessionWoman) {
			bEnabledRichino = this.EnabledRichino((SessionWoman)sessionObjs);
		} else if (sessionObjs is SessionMan) {
			if (sessionObjs.IsImpersonated) {
				SessionObjs oOriginalObj = sessionObjs.GetOriginalObj();
				if (oOriginalObj is SessionWoman) {
					bEnabledRichino = this.EnabledRichino((SessionWoman)oOriginalObj);
				} else {
					bEnabledRichino = true;
				}
			} else {
				bEnabledRichino = true;
			}
		}
		return bEnabledRichino;
	}
	private bool EnabledRichino(SessionWoman pSessionWoman) {
		bool bEnabledRichino = false;
		if (pSessionWoman.logined && pSessionWoman.userWoman.CurCharacter != null) {
			bEnabledRichino = (pSessionWoman.userWoman.CurCharacter.characterEx.enabledRichinoFlag == ViCommConst.FLAG_ON);
		}
		return bEnabledRichino;
	}

	private bool IsEnabledPickup(string pTag,string pArgument) {

		string sPickupId = pArgument;

		if (string.IsNullOrEmpty(sPickupId)) {
			return false;
		}

		bool bValue = false;
		using (Pickup oPickup = new Pickup()) {
			if (oPickup.GetOne(sessionObjs.site.siteCd,sPickupId)) {
				bValue = (oPickup.pickupFlag == true &&
							DateTime.Compare(oPickup.publishStartDate,DateTime.Now) <= 0 &&
							DateTime.Compare(DateTime.Now,oPickup.publishEndDate) <= 0);
			}
		}

		return bValue;

	}

	private bool IsBetweenTime(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');
		string sDay = DateTime.Now.ToString("yyyy/MM/dd");
		DateTime oDate = DateTime.Parse(ParseArg(oArgs,0,DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
		DateTime oStartDate = DateTime.Parse(string.Format("{0} {1}",sDay,ParseArg(oArgs,1,"00:00:00")));
		DateTime oEndDate = DateTime.Parse(string.Format("{0} {1}",sDay,ParseArg(oArgs,2,"00:00:01")));

		bool bValue = false;

		//Start <= End(22:00:00～23:00:00)の場合は、　
		//StartからEndの間の場合はTrue、　
		//Start >  End(23:00:00～06:00:00)の場合は、　
		//EndからStartの間でない場合はTrueを返す。　
		if (DateTime.Compare(oStartDate,oEndDate) <= 0) {
			bValue = ((oStartDate.CompareTo(oDate) <= 0) && (oDate.CompareTo(oEndDate) < 0)) ? true : false;
		} else {
			bValue = !((oEndDate.CompareTo(oDate) <= 0) && (oDate.CompareTo(oStartDate) < 0)) ? true : false;
		}

		return bValue;
	}
	private bool IsBetweenDateTime(string pTag,string pArgument) {
		string[] oArgs = pArgument.Split(',');
		DateTime oDate = DateTime.Parse(ParseArg(oArgs,0,DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
		DateTime oStartDate = DateTime.Parse(string.Format("{0}",ParseArg(oArgs,1,"yyyy/MM/dd HH:mm:ss")));
		DateTime oEndDate = DateTime.Parse(string.Format("{0}",ParseArg(oArgs,2,"yyyy/MM/dd HH:mm:ss")));

		bool bValue = false;
		bValue = ((oStartDate.CompareTo(oDate) <= 0) && (oDate.CompareTo(oEndDate) < 0)) ? true : false;

		return bValue;
	}

	private string GetBreadcrumbsListNavi(string pArgument) {
		// Parse Args
		string[] oArgs = pArgument.Split(',');
		string sSeparator = ParseArg(oArgs,0,"/");
		string sHistoryDesign = ParseArg(oArgs,1,"<a href='{0}'>{1}</a>");
		string sCurrentDesign = ParseArg(oArgs,2,"<strong>{1}</strong>");

		// Genarate List
		StringBuilder oBreadcrumbsListNaviBuilder = new StringBuilder();
		string sCurrentKey = Breadcrumb.GenearteKey(sessionObjs);

		foreach (Breadcrumb oBreadcrumb in sessionObjs.breadcrumbList) {
			string sTemplate = sHistoryDesign;
			if (oBreadcrumb.Key.Equals(sCurrentKey)) {
				sTemplate = sCurrentDesign;
			}

			if (oBreadcrumbsListNaviBuilder.Length > 0) {
				oBreadcrumbsListNaviBuilder.Append(sSeparator);
			}
			oBreadcrumbsListNaviBuilder.AppendFormat(sTemplate,oBreadcrumb.Url,oBreadcrumb.Title);
		}

		return oBreadcrumbsListNaviBuilder.ToString();
	}


	public class ParseCacheHolder {
		private IDictionary<string,object> _parseCache = new Dictionary<string,object>();
		public void Add(string pKey,string pSubKey,object pValue) {
			this.Add(GenerateParseCacheKey(pKey,pSubKey),pValue);
		}
		public void Add(string pKey,object pValue) {
			_parseCache.Add(pKey,pValue);
		}
		public object Get(string pKey,string pSubKey) {
			return Get(GenerateParseCacheKey(pKey,pSubKey));
		}
		public object Get(string pKey) {
			if (this.Has(pKey)) {
				return this._parseCache[pKey];
			}
			return null;
		}
		public bool Has(string pKey,string pSubKey) {
			return Has(this.GenerateParseCacheKey(pKey,pSubKey));
		}
		public bool Has(string pKey) {
			return this._parseCache.ContainsKey(pKey);
		}

		private string GenerateParseCacheKey(string pKey,string pSubKey) {
			return string.Format("{0}:::{1}",pKey,pSubKey);
		}
	}

	private string CreateParentPath(bool pLastSlash) {
		string sPath = parseContainer.ChangePathInfo(sessionObjs.requestQuery.Url.ToString());
		int iSlashCount = sPath.IndexOf('/');

		string sParrentPath = string.Empty;

		for (int i = 2;i < iSlashCount - 1;i++) {
			sParrentPath += "../";
		}
		if (!pLastSlash) {
			sParrentPath.Remove(sParrentPath.Length - 1);
		}
		return sParrentPath;
	}

	protected string GetAuRefresh() {
		return "_aurandom=" + DateTime.Now.ToString("ssfff");
	}

	private string GetMaqiaDayOfWeek() {
		//月1 火2 水3 木4 金5 土6 日7という仕様 
		int iDay = int.Parse(DateTime.Now.DayOfWeek.ToString("d"));
		if (iDay == 0) {
			iDay = 7;
		}
		return iDay.ToString();
	}

	protected string IsValidMask(int pCurrentMask,string pIndex) {
		int iIdx,iValue;
		int.TryParse(pIndex,out iIdx);
		if (iIdx > 0) {
			iValue = 1 << (iIdx - 1);
			if ((iValue & pCurrentMask) > 0) {
				return "1";
			} else {
				return "0";
			}
		} else {
			return "0";
		}
	}

	protected string GetBingoOpeningFlag(string pTag,string pArgument) {
		string sOpeningFlag = ViCommConst.FLAG_OFF_STR;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}

		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			DataSet ds = oMailDeBingo.GetCurrentOpeningInfo(sessionObjs.site.siteCd,sSexCd,DateTime.Now);
			if (ds.Tables[0].Rows.Count > 0) {
				sOpeningFlag = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["IS_OPENING"]);
			}
		}

		return sOpeningFlag;
	}

	protected string GetBingoOpeninSoonFlag(string pTag,string pArgument) {
		string sOpeningSoonFlag = ViCommConst.FLAG_OFF_STR;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}

		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			DataSet ds = oMailDeBingo.GetCurrentOpeningInfo(sessionObjs.site.siteCd,sSexCd,DateTime.Now);
			if (ds.Tables[0].Rows.Count > 0) {
				if (iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["IS_OPENING"]).Equals(ViCommConst.FLAG_OFF_STR) &&
					int.Parse(iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["BINGO_NEXT_COUNT"])) > 0) {
					sOpeningSoonFlag = ViCommConst.FLAG_ON_STR;
				}
			}
		}

		return sOpeningSoonFlag;
	}

	protected string GetBingoClosedFlag(string pTag,string pArgument) {
		string sOpeningClosedFlag = ViCommConst.FLAG_OFF_STR;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}

		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			DataSet ds = oMailDeBingo.GetCurrentOpeningInfo(sessionObjs.site.siteCd,sSexCd,DateTime.Now);
			if (ds.Tables[0].Rows.Count > 0) {
				if (iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["IS_OPENING"]).Equals(ViCommConst.FLAG_OFF_STR) &&
					int.Parse(iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["BINGO_NEXT_COUNT"])) == 0) {
					sOpeningClosedFlag = ViCommConst.FLAG_ON_STR;
				}
			}
		}

		return sOpeningClosedFlag;
	}

	protected string GetBingoJackpot(string pTag,string pArgument) {
		string sJackpot = string.Empty;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}

		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			sJackpot = oMailDeBingo.GetCurrentJackpot(sessionObjs.site.siteCd,sSexCd,DateTime.Now);
		}
		return sJackpot;
	}

	protected string GetBingoFirstEntryFlag(string pTag,string pArgument) {
		string sEntryFlag = ViCommConst.FLAG_OFF_STR;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}
		string sUserCharNo = string.Empty;
		if (sSexCd == ViCommConst.MAN) {
			sUserCharNo = ((SessionMan)sessionObjs).userMan.userCharNo;
		} else {
			sUserCharNo = ((SessionWoman)sessionObjs).userWoman.curCharNo;
		}

		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			sEntryFlag = oMailDeBingo.GetCurrentEntryCount(sessionObjs.site.siteCd,
															sessionObjs.GetUserSeq(),
															sUserCharNo,
															sSexCd,
															DateTime.Now);
			sEntryFlag = sEntryFlag.Equals(ViCommConst.FLAG_OFF_STR) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
		}

		return sEntryFlag;
	}

	protected string GetBingoCardTag(string pTag,string pArgument) {
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}
		string sUserCharNo = string.Empty;
		if (sSexCd == ViCommConst.MAN) {
			sUserCharNo = ((SessionMan)sessionObjs).userMan.userCharNo;
		} else {
			sUserCharNo = ((SessionWoman)sessionObjs).userWoman.curCharNo;
		}
		StringBuilder oBingoCardTag = new StringBuilder();
		if (this.sessionObjs.bingoCard != null) {
			this.sessionObjs.bingoCard.GetBingoCard(sessionObjs.site.siteCd,sessionObjs.GetUserSeq(),sUserCharNo,sSexCd);
		}
		BingoCard oBingoCard = this.sessionObjs.bingoCard;
		string sBingoCardTitle = parseContainer.Parse(pArgument);

		if (oBingoCard != null && oBingoCard.haveCard) {
			oBingoCardTag.AppendFormat("<table bgcolor=\"{0}\">",oBingoCard.bingoCardColor);
			oBingoCardTag.AppendFormat("<tr><td colspan=\"5\" align=\"center\"><font size=\"3\" color=\"#FFFFFF\">{0}</font></td></tr>",sBingoCardTitle);
			for (int i = 0;i < 5;i++) {
				oBingoCardTag.Append("<tr>");
				for (int j = 0;j < 5;j++) {
					int iIdx = (i * 5) + j;
					string sBallNo = string.Empty;
					if (oBingoCard.bingoCardMask[iIdx] == "0") {
						sBallNo = "b_heart.gif";
					} else {
						if (oBingoCard.bingoHitMask[iIdx].Equals(ViCommConst.FLAG_ON_STR)) {
							sBallNo = string.Format("{0}_{1}.gif",oBingoCard.bingoCardColorNum + 1,oBingoCard.bingoCardMask[iIdx]);
						} else {
							sBallNo = string.Format("{0}.gif",oBingoCard.bingoCardMask[iIdx]);
						}
					}
					oBingoCardTag.AppendFormat("<td><img src=\"../Image/{0}/bingo/{1}\" /></td>",sessionObjs.site.siteCd,sBallNo);
				}
				oBingoCardTag.Append("</tr>");
			}
			oBingoCardTag.Append("</table>");
		}
		return oBingoCardTag.ToString();
	}

	protected string GetBingoCardNo(string pTag,string pArgument) {
		int iBingoCardNo = 0;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}
		string sUserCharNo = string.Empty;
		if (sSexCd == ViCommConst.MAN) {
			sUserCharNo = ((SessionMan)sessionObjs).userMan.userCharNo;
		} else {
			sUserCharNo = ((SessionWoman)sessionObjs).userWoman.curCharNo;
		}
		if (this.sessionObjs.bingoCard != null) {
			this.sessionObjs.bingoCard.GetBingoCard(sessionObjs.site.siteCd,sessionObjs.GetUserSeq(),sUserCharNo,sSexCd);
		}
		BingoCard oBingoCard = this.sessionObjs.bingoCard;
		if (oBingoCard != null && oBingoCard.haveCard) {
			iBingoCardNo = oBingoCard.bingoCardNo;
		}
		return iBingoCardNo.ToString();
	}

	protected string GetBingoStartDate(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}

		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			return oMailDeBingo.GetBingoStartDate(sessionObjs.site.siteCd,sSexCd);
		}
	}

	protected string GetBingoEndDate(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}

		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			return oMailDeBingo.GetBingoEndDate(sessionObjs.site.siteCd,sSexCd);
		}
	}

	protected string GetBingoFlag(string pTag,string pArgument) {
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}
		string sUserCharNo = string.Empty;
		if (sSexCd == ViCommConst.MAN) {
			sUserCharNo = ((SessionMan)sessionObjs).userMan.userCharNo;
		} else {
			sUserCharNo = ((SessionWoman)sessionObjs).userWoman.curCharNo;
		}

		if (ViCommConst.GetBingoCardResult.OK.Equals(this.sessionObjs.bingoCard.GetBingoCard(this.sessionObjs.site.siteCd,this.sessionObjs.GetUserSeq(),sUserCharNo,sSexCd))) {
			switch (this.sessionObjs.bingoCard.bingoStatus) {
				case ViCommConst.BingoStatus.BINGO:
				case ViCommConst.BingoStatus.BINGO_DENIED:
					return ViCommConst.FLAG_ON_STR;
				default:
					return ViCommConst.FLAG_OFF_STR;
			}
		}
		return ViCommConst.FLAG_OFF_STR;
	}

	protected string GetSelfBingoBallFlag(string pTag,string pArgument) {
		string sSexCd = sessionObjs.sexCd;
		if (sSexCd == ViCommConst.WOMAN) {
			sSexCd = ViCommConst.OPERATOR;
		}
		string sUserCharNo = string.Empty;
		if (sSexCd == ViCommConst.MAN) {
			sUserCharNo = ((SessionMan)sessionObjs).userMan.userCharNo;
		} else {
			sUserCharNo = ((SessionWoman)sessionObjs).userWoman.curCharNo;
		}

		if (this.sessionObjs.bingoCard != null) {
			this.sessionObjs.bingoCard.GetBingoCard(sessionObjs.site.siteCd,sessionObjs.GetUserSeq(),sUserCharNo,sSexCd);
		}

		BingoCard oBingoCard = this.sessionObjs.bingoCard;
		return oBingoCard.bingoBallFlag.ToString();
	}

	private string GoogleAnalyticsGetImageUrl() {
		string sGaAccount = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["GaAccount"]);
		if (sGaAccount.Equals(string.Empty)) {
			return string.Empty;
		}

		if (HttpContext.Current == null) {
			return string.Empty;
		}

		System.Text.StringBuilder url = new System.Text.StringBuilder();
		url.Append("/ga.aspx?");
		url.Append("utmac=").Append(sGaAccount);
		Random RandomClass = new Random();
		url.Append("&utmn=").Append(RandomClass.Next(0x7fffffff));
		string referer = "-";

		if (HttpContext.Current.Request.UrlReferrer != null
			&& "" != HttpContext.Current.Request.UrlReferrer.ToString()) {
			referer = HttpContext.Current.Request.UrlReferrer.ToString();
		}
		url.Append("&utmr=").Append(HttpUtility.UrlEncode(referer));
		if (HttpContext.Current.Request.Url != null) {
			url.Append("&utmp=").Append(HttpUtility.UrlEncode(HttpContext.Current.Request.Url.PathAndQuery));
		}
		url.Append("&guid=ON");

		return string.Format("<img src=\"{0}\" />",url.ToString().Replace("&","&amp;"));
	}

	private string GetInstr(string pTag,string pArgument) {
		string[] sArgs = pArgument.Split(',');
		if (sArgs.Length < 2) {
			return "0";
		}

		string sSource = this.Parse(sArgs[0]);
		if (string.IsNullOrEmpty(sSource)) {
			return "0";
		}
		string sSub = this.Parse(sArgs[1]);
		if (string.IsNullOrEmpty(sSub)) {
			return "0";
		}
		return (sSource.IndexOf(sSub) + 1).ToString();
	}
}

