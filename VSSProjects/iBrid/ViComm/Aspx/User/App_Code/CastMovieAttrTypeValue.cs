﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画属性値
--	Progaram ID		: CastMovieAttrTypeValue
--
--  Creation Date	: 2010.05.19
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastMovieAttrTypeValueSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site_cd"];
		}
		set {
			this.Query["site_cd"] = value;
		}
	}

	public string CastMovieAttrTypeSeq {
		get {
			return this.Query["cast_movie_attr_type_seq"];
		}
		set {
			this.Query["cast_movie_attr_type_seq"] = value;
		}
	}

	public CastMovieAttrTypeValueSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastMovieAttrTypeValueSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.Query["cast_movie_attr_type_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast_movie_attr_type_seq"]));
	}
}

[System.Serializable]
public class CastMovieAttrTypeValue:DbSession {

	public string attrTypeNm;
	public string planningType;
	public string castMovieAttrTypeSeq;

	public CastMovieAttrTypeValue() {
	}

	public bool GetOne(string pSiteCd,string pCastMovieAttrSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("CastPicAttrTypeValue.GetOne");

			string sSql = "SELECT " +
								"CAST_MOVIE_ATTR_SEQ		," +
								"CAST_MOVIE_ATTR_NM		," +
								"CAST_MOVIE_ATTR_TYPE_SEQ	," +
								"CAST_MOVIE_ATTR_TYPE_NM	," +
								"PLANNING_TYPE			," +
								"PRIORITY				," +
								"ITEM_NO				" +
							"FROM " +
								"VW_CAST_MOVIE_ATTR_TYPE_VALUE1 " +
							"WHERE " +
								"SITE_CD				= :SITE_CD	AND		" +
								"CAST_MOVIE_ATTR_SEQ	= :CAST_MOVIE_ATTR_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_MOVIE_ATTR_SEQ",pCastMovieAttrSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						planningType = dr["PLANNING_TYPE"].ToString();
						castMovieAttrTypeSeq = dr["CAST_MOVIE_ATTR_TYPE_SEQ"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetListByItemNo(string pSiteCd,string pItemNo) {
		DataSet ds;
		try{
			conn = DbConnect("CastMovieAttrTypeValue.GetList");

			string sSql = "SELECT " +
								"CAST_MOVIE_ATTR_SEQ		," +
								"CAST_MOVIE_ATTR_NM			," +
								"CAST_MOVIE_ATTR_TYPE_SEQ	," +
								"CAST_MOVIE_ATTR_TYPE_NM	," +
								"PRIORITY					," +
								"ITEM_NO					" +
							"FROM " +
								"VW_CAST_MOVIE_ATTR_TYPE_VALUE1 " +
							"WHERE " +
								"SITE_CD	= :SITE_CD	AND " +
								"ITEM_NO	= :ITEM_NO		" +
							"ORDER BY " +
								"PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ITEM_NO",pItemNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE_ATTR_TYPE_VALUE1");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect("CastMovieAttrTypeValue.GetList");

			string sSql = "SELECT " +
								"CAST_MOVIE_ATTR_SEQ		," +
								"CAST_MOVIE_ATTR_NM			," +
								"CAST_MOVIE_ATTR_TYPE_SEQ	," +
								"CAST_MOVIE_ATTR_TYPE_NM	," +
								"PRIORITY					," +
								"ITEM_NO				" +
							"FROM " +
								"VW_CAST_MOVIE_ATTR_TYPE_VALUE1 " +
							"WHERE " +
								"SITE_CD					= :SITE_CD						AND " +
								"CAST_MOVIE_ATTR_TYPE_SEQ	!= :CAST_MOVIE_ATTR_TYPE_SEQ	AND	" +
								"CAST_MOVIE_ATTR_SEQ		!= :CAST_MOVIE_ATTR_SEQ " +
							"ORDER BY " +
								"CAST_MOVIE_ATTR_TYPE_PRIORITY,PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_MOVIE_ATTR_TYPE_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString());
				cmd.Parameters.Add("CAST_MOVIE_ATTR_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString());

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE_ATTR_TYPE_VALUE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	
	public string GetAttrNm(string pSiteCd,string pAttrSeq) {
		DataSet ds;
		DataRow dr;
		string sAttrNm = string.Empty;

		try {
			conn = DbConnect("CastMovieAttrTypeValue.GetAttrNm");

			string sSql = "SELECT " +
								"CAST_MOVIE_ATTR_NM	" +
							"FROM " +
								"T_CAST_MOVIE_ATTR_TYPE_VALUE " +
							"WHERE " +
								"SITE_CD				= :SITE_CD			AND " +
								"CAST_MOVIE_ATTR_SEQ	= :CAST_MOVIE_ATTR_SEQ	";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_MOVIE_ATTR_SEQ",pAttrSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE_ATTR_TYPE_VALUE");
					if (ds.Tables["T_CAST_MOVIE_ATTR_TYPE_VALUE"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_MOVIE_ATTR_TYPE_VALUE"].Rows[0];
						sAttrNm = dr["CAST_MOVIE_ATTR_NM"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sAttrNm;
	}

	public bool IsDeletable(string pSiteCd,string pAttrSeq) {
		bool bResult = true;

		try {
			conn = DbConnect("CastPicAttrTypeValue.IsDeletable");

			string sSql = "SELECT USE_INDIVIDUAL_PAY_FLAG, POSTER_DEL_NA_FLAG FROM T_CAST_MOVIE_ATTR_TYPE_VALUE WHERE SITE_CD = :SITE_CD AND CAST_MOVIE_ATTR_SEQ = :CAST_MOVIE_ATTR_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (DataSet ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_MOVIE_ATTR_SEQ",pAttrSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE_ATTR_TYPE_VALUE");
					if (ds.Tables["T_CAST_MOVIE_ATTR_TYPE_VALUE"].Rows.Count != 0) {
						DataRow dr = ds.Tables["T_CAST_MOVIE_ATTR_TYPE_VALUE"].Rows[0];

						if (ViCommConst.FLAG_ON_STR.Equals(dr["USE_INDIVIDUAL_PAY_FLAG"].ToString()) && ViCommConst.FLAG_ON_STR.Equals(dr["POSTER_DEL_NA_FLAG"].ToString())) {
							bResult = false;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bResult;
	}

	public DataSet GetListByCastMovieAttrTypeValueSeq(CastMovieAttrTypeValueSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.CAST_MOVIE_ATTR_TYPE_SEQ		,	");
		oSqlBuilder.AppendLine("	P.CAST_MOVIE_ATTR_TYPE_NM		,	");
		oSqlBuilder.AppendLine("	P.CAST_MOVIE_ATTR_SEQ			,	");
		oSqlBuilder.AppendLine("	P.CAST_MOVIE_ATTR_NM				");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_CAST_MOVIE_ATTR_TYPE_VALUE1 P	");

		SysPrograms.SqlAppendWhere(" P.SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));

		if (!string.IsNullOrEmpty(pCondition.CastMovieAttrTypeSeq)) {
			SysPrograms.SqlAppendWhere(" P.CAST_MOVIE_ATTR_TYPE_SEQ = :CAST_MOVIE_ATTR_TYPE_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":CAST_MOVIE_ATTR_TYPE_SEQ",pCondition.CastMovieAttrTypeSeq));
		}

		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(" ORDER BY CAST_MOVIE_ATTR_TYPE_PRIORITY ASC,PRIORITY ASC");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public DataSet GetListForComboBox(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_SEQ,");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_SEQ,");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE_ATTR_TYPE_VALUE");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_SEQ != :CAST_MOVIE_ATTR_TYPE_SEQ AND");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_SEQ != :CAST_MOVIE_ATTR_SEQ");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_SEQ ASC,PRIORITY ASC");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_MOVIE_ATTR_TYPE_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ));
		oParamList.Add(new OracleParameter(":CAST_MOVIE_ATTR_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}