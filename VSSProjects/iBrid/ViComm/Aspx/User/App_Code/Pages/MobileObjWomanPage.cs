﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: MobileObjWomanPage
--
--  Creation Date	: 2010.10.10
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

/// <summary>
/// オブジェクト関係の基本機能を提供する基底クラス
/// </summary>
public abstract class MobileObjWomanPage:MobileWomanPageBase {

	private SelectionList lstObjAttrCombo;

	protected virtual SelectionList ObjAttrCombo {
		set {
			lstObjAttrCombo = value;
		}
	}
	protected abstract void SetControl();

	protected void CreateObjAttrComboBox(string pUserSeq,string pUserCharNo,string pActCategorySeq,string pObjType,string pAttachedType,bool bZeroIsVisible) {
		SetControl();
		DataSet dsAttrType;
		DataSet dsAttr;
		NameValueCollection nvcAttrType = new NameValueCollection();
		NameValueCollection nvcAttr = new NameValueCollection();
		int iTotalCnt = 0;
		int iCnt = 0;
		int iDefaultAttrTypeSeq = 0;
		int iDefaultAttrSeq = 0;
		string sUnAuth = iBridUtil.GetStringValue(Request.QueryString["unauth"]);

		if (pObjType.Equals(ViCommConst.BbsObjType.PIC)) {
			iDefaultAttrTypeSeq = ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ;
			iDefaultAttrSeq = ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ;
		} else {
			iDefaultAttrTypeSeq = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ;
			iDefaultAttrSeq = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ;
		}

		string sPlanningType = string.Empty;
		switch (sessionObj.currentAspx) {
			case "SendBbsObj.aspx":
				string queryDispPlanning = iBridUtil.GetStringValue(Request.QueryString["DispPlanning"]);
				if (queryDispPlanning.Equals(string.Empty)) {
					sPlanningType = ViCommConst.FLAG_OFF_STR;
				} else if (queryDispPlanning.Equals(ViCommConst.FLAG_ON_STR)) {
					sPlanningType = string.Empty;
				}
				break;

			case "ListBbsMovieConf.aspx":
			case "ListBbsObjConf.aspx":
			case "ListBbsPicConf.aspx":
				string sScreenId = iBridUtil.GetStringValue(Request.QueryString["scrid"]);
				if (!string.IsNullOrEmpty(sScreenId)) {
					sPlanningType = ViCommConst.FLAG_ON_STR;
					this.lstObjAttrCombo.Visible = false;
				} else {
					sPlanningType = ViCommConst.FLAG_OFF_STR;
				}
				break;

			default:
				sPlanningType = ViCommConst.FLAG_OFF_STR;
				break;
		}

		if (pAttachedType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			using (CastBbsObj oCastBbs = new CastBbsObj()) {
				dsAttrType = oCastBbs.GetObjAttrTypeCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjType,sUnAuth,false,false,false,sPlanningType,new SeekCondition());
				dsAttr = oCastBbs.GetObjAttrCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,pActCategorySeq,pObjType,sUnAuth,false,false,false,new SeekCondition());
			}
		} else {
			using (CastPic oCastPic = new CastPic()) {
				dsAttrType = oCastPic.GetPicAttrTypeCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,pActCategorySeq,pAttachedType,false,false,new SeekCondition());
				dsAttr = oCastPic.GetPicAttrCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,pActCategorySeq,pAttachedType,false,false,new SeekCondition());
			}
		}
		//合計値を算出
		iTotalCnt = Convert.ToInt32(dsAttrType.Tables[0].Compute("Sum(CNT)",""));

		foreach (DataRow dr in dsAttrType.Tables[0].Rows) {
			int.TryParse(dr["CNT"].ToString(),out iCnt);
			if ((!bZeroIsVisible) || (iCnt > 0)) {
				if (!dr["OBJ_ATTR_TYPE_SEQ"].ToString().Equals(iDefaultAttrTypeSeq.ToString())) {
					nvcAttrType.Add(dr["OBJ_ATTR_TYPE_SEQ"].ToString(),dr["CNT"] + "," + dr["OBJ_ATTR_TYPE_NM"]);
				}
			}
		}
		foreach (DataRow dr in dsAttr.Tables[0].Rows) {
			int.TryParse(dr["CNT"].ToString(),out iCnt);
			if ((!bZeroIsVisible) || (iCnt > 0)) {
				if (!dr["OBJ_ATTR_TYPE_SEQ"].ToString().Equals(iDefaultAttrTypeSeq.ToString()) || !dr["OBJ_ATTR_SEQ"].ToString().Equals(iDefaultAttrSeq.ToString())) {
					nvcAttr.Add(dr["OBJ_ATTR_TYPE_SEQ"].ToString(),dr["CNT"] + "," + dr["OBJ_ATTR_SEQ"] + "," + dr["OBJ_ATTR_NM"]);
				}
			}
		}
		if (!IsAvailableService(ViCommConst.RELEASE_OBJ_ATTR_TYPE_HIDE_ALL)) {
			lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("■全て({0})",iTotalCnt),","));
		}
		foreach (string key in nvcAttrType.Keys) {
			string[] sAttrType = nvcAttrType.GetValues(key)[0].Split(',');
			lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("■{0}({1})",sAttrType[1],sAttrType[0]),string.Format("{0},{1}",key,string.Empty)));

			string[] sAttrValue = nvcAttr.GetValues(key) ?? new string[] { };
			foreach (string sValue in sAttrValue) {
				string[] sAttr = sValue.Split(',');
				lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("└{0}({1})",sAttr[2],sAttr[0]),string.Format("{0},{1}",key,sAttr[1])));
			}
		}

		// 選択した値を復元します。 
		string selectedValue = string.Format("{0},{1}",iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(Request.QueryString["attrseq"]));
		foreach (MobileListItem item in lstObjAttrCombo.Items) {
			if (item.Value == selectedValue) {
				lstObjAttrCombo.SelectedIndex = item.Index;
			}
		}
	}
}
