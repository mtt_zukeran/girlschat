﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: MobileWomanPageBase
--
--  Creation Date	: 2010.08.03
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

/// <summary>
/// 女性画面の基本機能を提供する抽象基底クラス
/// </summary>
public abstract class MobileWomanPageBase:MobilePageBase {

	protected virtual bool CheckSiteUseStatus {
		get {
			return true;
		}
	}

	protected virtual SessionWoman sessionWoman {
		get {
			object oObjs = Session["objs"];
			if (oObjs is SessionWoman) {
				return (SessionWoman)oObjs;
			} else {
				string sHost = iBridUtil.GetStringValue(Request.QueryString["host"]);
				if (sHost.Equals(string.Empty)) {
					sHost = Request.Url.Host;
					string sTestEnv = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestEnv"]);
					if (sTestEnv.Equals("1")) {
						sHost = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestLocation"]);
					}
				}
				SessionHelper oHelper = new SessionHelper(ViCommConst.OPERATOR,Mobile.GetCarrier(HttpContext.Current.Request.Browser.Browser),HttpContext.Current.Request.Browser.Id,"","");
				SessionObjs oNewSession;
				oHelper.CreateSessionInfo(sHost,Session.SessionID,false,false,out oNewSession);
				return (SessionWoman)oNewSession;
			}
		}
	}

	public MobileWomanPageBase() {
	}

	protected override void OnLoad(EventArgs e) {
		string sUserSeq = string.Empty;
		string sPassword = string.Empty;
		string sSexCd = ViCommConst.OPERATOR;
		bool bCheck = false;

		if (sessionWoman.logined) {
			sUserSeq = sessionWoman.userWoman.userSeq;
			sPassword = sessionWoman.userWoman.loginPassword;
			sSexCd = ViCommConst.OPERATOR;
			bCheck = true;
		} else if (sessionWoman.IsImpersonated) {
			sUserSeq = sessionWoman.originalManObj.userMan.userSeq;
			sPassword = sessionWoman.originalManObj.userMan.loginPassword;
			sSexCd = ViCommConst.MAN;
			bCheck = true;
		}

		if (bCheck) {
			CheckCookieData(sUserSeq,sPassword,sSexCd);
		}
		
		base.OnLoad(e);
		if (!this.IsPostBack) {
			/* 初期処理 */
			CheckGameCharacterOnly();
			CheckProfileInvalidFlag();
			if (iBridUtil.GetStringValue(Request.QueryString[ViCommConst.QueryParameters.PARAM_RELOAD_SELF]).Equals(ViCommConst.FLAG_ON_STR)) {
				sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
			}
		} else {
			if (this.IsPostAction(ViCommConst.BUTTON_EXTSEARCH)) {
				UrlBuilder oUrlBuilder;
				this.ListSearchAction(false,out oUrlBuilder);
			}
		}
	}
	/// <summary>
	/// 一覧下部の検索ボタン押下時処理 
	/// </summary>
	protected virtual void ListSearchAction(bool bNotRedirect,out UrlBuilder oUrlBuilder) {

		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		foreach (string sKey in this.Request.QueryString) {
			if (string.IsNullOrEmpty(sKey))
				continue;

			if (sKey.StartsWith("ext_"))
				continue;
			if (sKey.Equals("pageno"))
				continue;
			oParameters.Add(sKey,HttpUtility.UrlDecode(this.Request.QueryString[sKey]));
		}


		oUrlBuilder = new UrlBuilder(SessionObjs.Current.GetNavigateUrl(ViCommPrograms.GetCurrentAspx()),oParameters);

		IList<string> oRemoveKeyList = new List<string>();
		oRemoveKeyList.Add("attrtypeseq");
		oRemoveKeyList.Add("attrseq");
		oRemoveKeyList.Add("sorttype");
		foreach (string sRemoveKey in oRemoveKeyList) {
			if (oUrlBuilder.Parameters.ContainsKey(sRemoveKey)) {
				oUrlBuilder.Parameters.Remove(sRemoveKey);
			}
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["attrtypeseq"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("attrtypeseq",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["attrtypeseq"])));
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["attrseq"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("attrseq",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["attrseq"])));
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["sorttype"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("sorttype",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["sorttype"])));
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["ext_handle"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("ext_handle",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_handle"])));
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["ext_agerange"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("ext_agerange",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_agerange"])));
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["ext_userstatus"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("ext_userstatus",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_userstatus"])));
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["ext_communication"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("ext_communication",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_communication"])));
		}
		if (!iBridUtil.GetStringValue(this.Request.Form["ext_without_batchmail"]).Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("ext_without_batchmail",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_without_batchmail"])));
		}
		foreach (string sKey in this.Request.Form.AllKeys) {
			if (!sKey.StartsWith("ext_item"))
				continue;
			oUrlBuilder.Parameters.Add(sKey,HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form[sKey])));
		}

		if (bNotRedirect == false) {
			RedirectToMobilePage(oUrlBuilder.ToString(true));
		}
	}

	protected virtual void CheckRefuse(string pUserSeq,string pUserCharNo) {
		//相手の拒否リストに載っている場合、リダイレクトさせる。


		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(
					sessionWoman.site.siteCd,
					pUserSeq,
					pUserCharNo,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo)) {
				RedirectToDisplayDoc(ViCommConst.ERR_REFUSE_WOMAN);
			}
		}
	}

	private void CheckProfileInvalidFlag() {
		if (sessionWoman.logined) {
			if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
				if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNmResettingFlag == 1) {
					if (!(sessionWoman.currentAspx.Equals("ModifyUserProfile.aspx") || sessionWoman.currentAspx.Equals("ModifyUserHandleNm.aspx"))) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl("ModifyUserHandleNm.aspx"));
					}
				}
			}
		}
	}

	private void CheckGameCharacterOnly() {
		if (CheckSiteUseStatus) {
			if (sessionWoman.logined) {
				if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
					if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY)) {
						if (!sessionWoman.currentProgramId.Equals("MailForm03.aspx")) {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS);
						}
					}
				}
			}
		}
	}
}
