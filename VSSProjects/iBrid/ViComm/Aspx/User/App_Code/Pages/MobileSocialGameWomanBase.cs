﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public abstract class MobileSocialGameWomanBase:MobileWomanPageBase {

	protected override bool CheckSiteUseStatus {
		get {
			return false;
		}
	}

	protected override void OnLoad(EventArgs e) {
		if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
			sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);

			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].gameCharacter.gameLogined) {
				sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].gameCharacter.CreateGamePageView(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].gameCharacter.gameFirstLogined ? ViCommConst.FLAG_OFF : ViCommConst.FLAG_ON);
				sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].gameCharacter.gameFirstLogined = true;
				
				if (!string.IsNullOrEmpty(this.sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus)) {
					this.DivideTutorialPage();
				}
			}
		}
		base.OnLoad(e);
	}

	private void DivideTutorialPage() {
		string sRedirectUrl = string.Empty;
		string[] sSplitUrl = Request.Path.Split('/');
		string sCurrentAspx = sSplitUrl[sSplitUrl.Length - 1];
		string sBattleLogSeq = string.Empty;
		string sPreExpBattle = string.Empty;

		if ((sCurrentAspx.Equals("GameDisplayDoc.aspx") && iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS)) ||
			(sCurrentAspx.Equals("GameDisplayDoc.aspx") && iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_WOMAN_ID_PIC_MAIL)) ||
			(sCurrentAspx.Equals("ModifyGameUser.aspx") && iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("01")) ||
			(sCurrentAspx.Equals("ModifyGameUserProfile.aspx") && iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("01"))) {
			return;
		}

		if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("0")) {
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "GameCharacterTypeRegist.aspx":
				case "RegistAddOnInfoByGame.aspx":
					break;
				default:
					RedirectToGameDisplayDoc(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_SELECT);
					break;
			}

			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_SELECT)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_SELECT);
				}
			}
		} else if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("1")) {
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "ViewGameStage.aspx":
				case "RegistGameClearMission.aspx":
					break;
				default:
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("ViewGameStage.aspx"));
					break;
			}

			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_COMPLETE)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("ViewGameStage.aspx"));
				}
			}
		} else if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("2")) {
			switch (sCurrentAspx) {
				case "ViewGameStage.aspx":
				case "ViewGameTownClearMissionOK.aspx":
				case "RegistGameClearMission.aspx":
					break;
				default:
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("ViewGameStage.aspx"));
					break;
			}
		} else if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("3")) {
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "ViewGameStage.aspx":
				case "ViewGameTownClearMissionOK.aspx":
				case "RegistGameClearMission.aspx":
				case "ViewGameFlashLevelup.aspx":
					break;
				default:
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("ViewGameStage.aspx"));
					break;
			}

			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_GAME_FLASH_LEVELUP_SP) &&
					!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_GAME_WOMAN_TUTORIAL_EXPLAIN_LEVEL_UP)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("ViewGameStage.aspx"));
				}
			}
		} else if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("4")) {
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "ViewGameTownClearMissionOK.aspx":
				case "ListGameTreasure.aspx":
				case "ViewGameFlashGetCastTreasure.aspx":
				case "RegistGameGetTreasure.aspx":
				case "DivideGameBattleTop.aspx":
				case "ListGameTreasure01.aspx":
				case "ListGameCharacterBattleTreasure.aspx":
				case "RegistGameBattleStart.aspx":
				case "ListGameItemShop.aspx":
				case "ViewGameItem.aspx":
				case "ViewGameItemShop.aspx":
					break;
				default:
					RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_WOMAN_TUTORIAL_EXPLAIN_BATTLE);
					break;
			}

			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_GAME_FLASH_GET_MAN_TREASURE_SP) &&
					!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_GAME_WOMAN_TUTORIAL_EXPLAIN_BATTLE) &&
					!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_WOMAN_GAME_ITEM_BUY_COMPLETE)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_WOMAN_TUTORIAL_EXPLAIN_BATTLE);
				}
			}

		} else if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("5")) {
			this.GetGameTutorialBattleLogSeq(out sBattleLogSeq);
			this.GetGameTutorialPreExpBattle(out sPreExpBattle);
			sRedirectUrl = string.Format("ViewGameBattleResultWin.aspx?battle_log_seq={0}&pre_exp={1}",
											sBattleLogSeq,
											sPreExpBattle);
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "ViewGameBattleResultWin.aspx":
				case "ViewGameFlashBattle.aspx":
				case "GameTutorialEnd.aspx":
					break;
				default:
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sRedirectUrl));
					break;
			}

			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_GAME_FLASH_BATTLE_SP)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sRedirectUrl));
				}
			}
		}
	}

	protected bool CheckOnlineStatusGame(string pManUserSeq,string pManUserCharNo) {
		int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);

		// 自分がトーク中の場合、アクションを起こせない
		if (iCharacterOnlineStatus == ViCommConst.USER_TALKING) {
			return false;
		}

		// 自分が相手から見てオフラインだったら、相手に対してのアクションを起こせない
		//自分が普通にオフライン（直ログインのときありえる） 
		if (iCharacterOnlineStatus == ViCommConst.USER_OFFLINE) {
			return false;
		}

		int iIsSeenOnlineStatus2;
		using (Favorit oFavorit = new Favorit()) {
			iIsSeenOnlineStatus2 = oFavorit.GetIsSeenOnlineStatus(
										sessionWoman.site.siteCd,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										pManUserSeq,
										pManUserCharNo,
										iCharacterOnlineStatus);
		}
		if (iIsSeenOnlineStatus2 == ViCommConst.USER_OFFLINE) {
			return false;
		}

		return true;
	}

	protected virtual void CheckGameRefusedByPartner(string pUserSeq,string pUserCharNo) {
		//相手の拒否リストに載っている場合、リダイレクトさせる。

		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(
					sessionWoman.site.siteCd,
					pUserSeq,
					pUserCharNo,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_GAME_WOMAN_REFUSED_BY_PARTNER);
			}
		}
	}

	protected virtual void CheckGameRefusePartner(string pUserSeq,string pUserCharNo) {
		//自分の拒否リストに相手が載っている場合、リダイレクトさせる。

		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					pUserSeq,
					pUserCharNo)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_GAME_WOMAN_REFUSE_PARTNER);
			}
		}
	}

	private void GetGameTutorialBattleLogSeq(out string sBattleLogSeq) {
		DataSet oDataSet;

		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		sBattleLogSeq = string.Empty;

		using (BattleLog oBattleLog = new BattleLog()) {
			oDataSet = oBattleLog.GetOneForTutorial(sSiteCd,sUserSeq,sUserCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sBattleLogSeq = oDataSet.Tables[0].Rows[0]["BATTLE_LOG_SEQ"].ToString();
		}
	}

	private void GetGameTutorialPreExpBattle(out string pPreExpTutorial) {
		DataSet oDataSet;

		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.sessionWoman.site.siteCd;
		string sBattleWinExp = string.Empty;
		int iBattleWinExp = 0;
		pPreExpTutorial = string.Empty;

		using (SocialGame oSocialGame = new SocialGame()) {
			oDataSet = oSocialGame.GetOne(oCondition);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sBattleWinExp = oDataSet.Tables[0].Rows[0]["BATTLE_WIN_EXP"].ToString();
			int.TryParse(sBattleWinExp,out iBattleWinExp);
			int iPreExpTutorial = this.sessionWoman.userWoman.CurCharacter.gameCharacter.exp - iBattleWinExp;
			pPreExpTutorial = iPreExpTutorial.ToString();
		}
	}
}
