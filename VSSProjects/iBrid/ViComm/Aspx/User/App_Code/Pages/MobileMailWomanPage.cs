﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: MobileMailWomanPage
--
--  Creation Date	: 2010.09.08
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

/// <summary>
/// メールの基本機能を提供する基底クラス
/// </summary>
public abstract class MobileMailWomanPage:MobileWomanPageBase {

	private iBMobileTextBox txtTitle;
	private string txtDoc;
	private iBMobileLabel lblErrorMsg;
	private SelectionList lstMailTemplate;

	protected virtual iBMobileTextBox MailTitle {
		set {
			txtTitle = value;
		}
	}
	protected virtual string MailDoc {
		set {

			txtDoc = value;
		}
	}
	protected virtual iBMobileLabel ErrorMsg {
		set {
			lblErrorMsg = value;
		}
	}

	protected virtual SelectionList MailTemplate {
		set {
			lstMailTemplate = value;
		}
	}

	protected abstract void SetControl();

	protected bool Submit(string pMailDataSeq,bool pIsReturnMail,bool pIsBatchMail,string pSuccessPageNo) {
		SetControl();
		if (!CheckOnlineStatus(pMailDataSeq,pIsBatchMail)) {
			return false;
		}

		bool bReleaseNonTitleByUesrMail = false;
		bReleaseNonTitleByUesrMail = IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL, 2);
		if (bReleaseNonTitleByUesrMail) {
			txtTitle.Text = string.Empty;
			txtTitle.Visible = false;
		}

		// GETパラメータでメールテンプレートナンバーが指定されていた場合、強制的に選択状態にする		
		string sMailTemplateNo = iBridUtil.GetStringValue(this.Request.Params["mail_template_no"]);
		if (!string.IsNullOrEmpty(sMailTemplateNo)) {
			foreach (MobileListItem oItem in this.lstMailTemplate.Items) {
				if (object.Equals(oItem.Value,sMailTemplateNo)) {
					lstMailTemplate.SelectedIndex = oItem.Index;
					break;
				}
			}
		}

		bool bOk = true;

		lblErrorMsg.Text = string.Empty;

		string sUserSeq = string.Empty;
		string sHandleNm;
		string sNGWord;
		string sObjTempId = string.Empty;
		
		string sStayFlag = iBridUtil.GetStringValue(Request.QueryString["stay"]);

		if (!pIsBatchMail) {
			sUserSeq = sessionWoman.userWoman.mailData[pMailDataSeq].rxUserSeq;
			sHandleNm = sessionWoman.userWoman.mailData[pMailDataSeq].rxUserNm;
			if (bOk) {
				if (sessionWoman.site.txMailRefMarking == 1) {
					using (Marking oMarking = new Marking()) {
						if (!oMarking.IsMarking(sessionWoman.site.siteCd,sUserSeq,ViCommConst.MAIN_CHAR_NO,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo)) {
							bOk = false;
							lblErrorMsg.Text = GetErrorMessage(ViCommConst.ERR_STATUS_CAST_TO_MAN_MAIL_NOT_MARKING);
						}
					}
				}
			}
		}

		if (bOk) {
			if (txtTitle.Text.Equals(string.Empty) && txtTitle.Visible == true) {
				bOk = false;
				lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
			}
			if (txtDoc.Equals(string.Empty)) {
				bOk = false;
				lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
			}

			int iDocLengthLimit;

			if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["doc_limit_length"]),out iDocLengthLimit)) {
				iDocLengthLimit = 1000;
			}

			if (txtDoc.Length > iDocLengthLimit) {
				bOk = false;
				int iOverLength = txtDoc.Length - iDocLengthLimit;
				lblErrorMsg.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iBridUtil.GetStringValue(iOverLength),iBridUtil.GetStringValue(iDocLengthLimit));
			}
		}

		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (!IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2)) {
				if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc,out sNGWord) == false) {
					bOk = false;
					lblErrorMsg.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}

		if (!pIsBatchMail) {
			if (bOk) {
				string sCheckTxMailErrorMessage = string.Empty;

				using (MailLog oMailLog = new MailLog()) {
					bOk = oMailLog.CheckTxMail(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sUserSeq,
							lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,
							out sCheckTxMailErrorMessage
						);
				}
				if (!sCheckTxMailErrorMessage.Equals(string.Empty)) {
					lblErrorMsg.Text += sCheckTxMailErrorMessage + "<BR>";
				}
			}
		} else {
			if ((sessionWoman.BatchMailUserList.Count + sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].batchMailCount) > sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castToManBatchMailLimit) {
				bOk = false;
				lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_CAST_TO_MAN_MAIL_LIMIT_OVER);
			}
		}

		bOk = bOk & CheckOther();
		string sMailSeq = string.Empty;
		if (bOk == true) {
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(txtTitle.Text) : txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(txtDoc) : txtDoc));

			using (MailBox oMailBox = new MailBox()) {
				if (oMailBox.IsSameMailSent(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,ViCommConst.MAIN_CHAR_NO,sTitle,sDoc)) {
					lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAIL_SAME_SEND);
					return false;
				}
			}

			int iAttachedObjType;
			string sMailAttachedMethod = string.Empty;
			string sValue;
			using (MailTemplate oTemplate = new MailTemplate()) {
				oTemplate.GetValue(sessionWoman.site.siteCd,lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,"MAIL_ATTACHED_OBJ_TYPE",out sValue);
				oTemplate.GetValue(sessionWoman.site.siteCd,lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,"MAIL_ATTACHED_METHOD",out sMailAttachedMethod);
			}
			int.TryParse(sValue,out iAttachedObjType);
			
			string sMailResBonusQuery;

			if (sMailAttachedMethod.Equals(ViCommConst.ATTACHED_MAILER)) {
				//新添付形式(MAILER)
				if (pIsBatchMail) {
					SetMailInfo(pMailDataSeq,pIsReturnMail);
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("TxMailConfirm.aspx?data={0}&batch={1}&mailtype={2}&mailer={3}",pMailDataSeq,ViCommConst.FLAG_ON_STR,iAttachedObjType,ViCommConst.FLAG_ON_STR)));
				} else {
					SetMailInfo(pMailDataSeq,pIsReturnMail);
					using (MailBox oMailBox = new MailBox()) {
						if (pIsReturnMail == false) {
							oMailBox.TxCastToManMail(
								sessionWoman.site.siteCd,
								lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,
								sessionWoman.userWoman.userSeq,
								sessionWoman.userWoman.curCharNo,
								new string[] { sUserSeq },
								0,
								sTitle,
								sDoc,
								iAttachedObjType.ToString(),
								null,
								null,
								ViCommConst.FLAG_OFF,
								null,
								null,
								out sObjTempId
							);
						} else {
							sMailSeq = oMailBox.TxCastToManReturnMail(
								sessionWoman.site.siteCd,
								lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,
								sessionWoman.userWoman.userSeq,
								sessionWoman.userWoman.curCharNo,
								new string[] { sUserSeq },
								0,
								sTitle,
								sDoc,
								iAttachedObjType.ToString(),
								null,
								null,
								ViCommConst.FLAG_OFF,
								null,
								null,
								sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq,
								out sObjTempId
							);
						}
						sessionWoman.userWoman.mailData[pMailDataSeq].objTempId = sObjTempId;
					}
					DeleteDraftMail(pMailDataSeq);

					// 3分以内メール返信ボーナス
					sMailResBonusQuery = this.CreateMailResBonusEnableQuery(sMailSeq,pMailDataSeq,sUserSeq);

					//メールアタック(新人)
					string sMailAttackNewQuery = string.Empty;
					if (sessionWoman.userWoman.mailData[pMailDataSeq].mailAttackNewFlag == ViCommConst.FLAG_ON) {
						string sMailAttackNewTxLogResult;
						using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
							sMailAttackNewTxLogResult = oMailAttackNew.RegistMailAttackNewTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,ViCommConst.MAIN_CHAR_NO,sMailSeq,ViCommConst.FLAG_ON);
						}
						sMailAttackNewQuery = string.Format("&attacknewresult={0}",sMailAttackNewTxLogResult);
					}
					
					//メールアタック(ログイン)
					string sMailAttackLoginQuery = string.Empty;
					if (sessionWoman.userWoman.mailData[pMailDataSeq].mailAttackLoginFlag == ViCommConst.FLAG_ON) {
						string sMailAttackLoginTxLogResult;
						using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
							sMailAttackLoginTxLogResult = oMailAttackLogin.RegistMailAttackLoginTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,ViCommConst.MAIN_CHAR_NO,sMailSeq,ViCommConst.FLAG_ON);
						}
						sMailAttackLoginQuery = string.Format("&attackloginresult={0}",sMailAttackLoginTxLogResult);
					}

					if (iAttachedObjType == ViCommConst.ATTACH_PIC_INDEX) {
						if (pIsReturnMail) {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&partnermailseq={3}{4}{5}{6}",ViCommConst.SCR_WOMAN_PIC_RETURN_MAILER_COMPLITE,pMailDataSeq,sMailSeq,sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq,sMailResBonusQuery,sMailAttackNewQuery,sMailAttackLoginQuery)));
						} else {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}",ViCommConst.SCR_WOMAN_PIC_TX_MAILER_COMPLITE,pMailDataSeq)));
						}
					} else {
						if (pIsReturnMail) {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&partnermailseq={3}{4}{5}{6}",ViCommConst.SCR_WOMAN_MOVIE_RETURN_MAILER_COMPLITE,pMailDataSeq,sMailSeq,sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq,sMailResBonusQuery,sMailAttackNewQuery,sMailAttackLoginQuery)));
						} else {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}",ViCommConst.SCR_WOMAN_MOVIE_TX_MAILER_COMPLITE,pMailDataSeq)));
						}
					}
				}
			} else {
				//今までの添付形式 
				switch (iAttachedObjType) {
					case ViCommConst.ATTACH_PIC_INDEX:
						SetMailInfo(pMailDataSeq,pIsReturnMail);
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ListMailPic.aspx?data={0}&batch={1}",pMailDataSeq,pIsBatchMail ? "1" : "0")));
						break;

					case ViCommConst.ATTACH_MOVIE_INDEX:
						SetMailInfo(pMailDataSeq,pIsReturnMail);
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ListMailMovie.aspx?data={0}&batch={1}&drows=5",pMailDataSeq,pIsBatchMail ? "1" : "0")));
						break;

					default:
						using (MailBox oMailBox = new MailBox()) {
							if (pIsBatchMail) {
								SetMailInfo(pMailDataSeq,pIsReturnMail);
								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("TxMailConfirm.aspx?data={0}&batch={1}&mailtype={2}",pMailDataSeq,"1",ViCommConst.ATTACH_NOTHING_INDEX.ToString())));
							} else {
								SetMailInfo(pMailDataSeq,pIsReturnMail);
								string sBingoQuery = string.Empty;
								int iCompliateFlag = ViCommConst.FLAG_OFF;
								if (pIsReturnMail == false) {
									sMailSeq = oMailBox.TxCastToManMail(
										sessionWoman.site.siteCd,
										lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										new string[] { sUserSeq },
										0,
										sTitle,
										sDoc,
										null,
										null,
										null,
										ViCommConst.FLAG_OFF,
										null,
										null,
										out sObjTempId
									);
								} else {
									sMailSeq = oMailBox.TxCastToManReturnMail(
										sessionWoman.site.siteCd,
										lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										new string[] { sUserSeq },
										0,
										sTitle,
										sDoc,
										null,
										null,
										null,
										ViCommConst.FLAG_OFF,
										null,
										null,
										sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq,
										out sObjTempId
									);

									int iGetNewBallFlag;
									int iGetOldBallFlag;
									int iGetCardBallFlag;
									int iGetBallNo;
									int iBingoBallFlag;
									string sResult = sessionWoman.bingoCard.GetBingoBall(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.OPERATOR,sMailSeq,sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq,out iGetNewBallFlag,out iGetOldBallFlag,out iGetCardBallFlag,out iGetBallNo,out iCompliateFlag,out iBingoBallFlag);
									sBingoQuery = string.Format("&ballnew={0}&ballold={1}&cardball={2}&ballno={3}&comp={4}&result={5}&bingoball={6}",iGetNewBallFlag,iGetOldBallFlag,iGetCardBallFlag,iGetBallNo,iCompliateFlag,sResult,iBingoBallFlag);
								}
								DeleteDraftMail(pMailDataSeq);
								
								using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
									oMailRequestLog.UpdateMailRequestLog(sessionWoman.site.siteCd,sUserSeq,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sMailSeq);
								}

								string sMailAttackLoginQuery = string.Empty;
								if (sessionWoman.userWoman.mailData[pMailDataSeq].mailAttackLoginFlag == ViCommConst.FLAG_ON) {
									string sMailAttackLoginTxLogResult;
									using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
										sMailAttackLoginTxLogResult = oMailAttackLogin.RegistMailAttackLoginTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,ViCommConst.MAIN_CHAR_NO,sMailSeq,ViCommConst.FLAG_OFF);
									}
									sMailAttackLoginQuery = string.Format("&attackloginresult={0}",sMailAttackLoginTxLogResult);
								}

								string sMailAttackNewQuery = string.Empty;
								if (sessionWoman.userWoman.mailData[pMailDataSeq].mailAttackNewFlag == ViCommConst.FLAG_ON) {
									string sMailAttackNewTxLogResult;
									using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
										sMailAttackNewTxLogResult = oMailAttackNew.RegistMailAttackNewTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,ViCommConst.MAIN_CHAR_NO,sMailSeq,ViCommConst.FLAG_OFF);
									}
									sMailAttackNewQuery = string.Format("&attacknewresult={0}",sMailAttackNewTxLogResult);
								}

								// 3分以内メール返信ボーナス
								sMailResBonusQuery = this.CreateMailResBonusEnableQuery(sMailSeq,pMailDataSeq,sUserSeq);

								string sNextUrl = "DisplayDoc.aspx?doc=" + pSuccessPageNo + sBingoQuery + sMailAttackLoginQuery + sMailAttackNewQuery + sMailResBonusQuery + "&" + Request.QueryString + "&mseq=" + sMailSeq + "&rseq=" + sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq;

								if (sStayFlag.Equals(ViCommConst.FLAG_ON_STR)) {
									NameValueCollection oQeuryList = new NameValueCollection(sessionWoman.requestQuery.QueryString);
									oQeuryList.Remove("ballnew");
									oQeuryList.Remove("ballold");
									oQeuryList.Remove("cardball");
									oQeuryList.Remove("ballno");
									oQeuryList.Remove("comp");
									oQeuryList.Remove("result");
									oQeuryList.Remove("bingoball");
									oQeuryList.Remove("data");
									oQeuryList.Remove("mseq");
									oQeuryList.Remove("rseq");
									oQeuryList.Remove("listno");
									oQeuryList.Remove("appbonus");
									oQeuryList.Remove("mailresbonusflag");
									oQeuryList.Remove("attackloginresult");
									oQeuryList.Remove("attacknewresult");

									List<string> items = new List<String>();
									foreach (string name in oQeuryList)
										items.Add(string.Concat(name,"=",oQeuryList[name]));
									string sQuery = string.Join("&",items.ToArray());
									
									if (pIsReturnMail == false) {
										sNextUrl = string.Format("TxMail.aspx?{0}{1}#btm",sQuery,sMailResBonusQuery);
									} else {
										sNextUrl = string.Format("ReturnMail.aspx?{0}{1}&mseq={2}&rseq={3}{4}{5}{6}#btm",sQuery,sBingoQuery,sMailSeq,sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq,sMailResBonusQuery,sMailAttackLoginQuery,sMailAttackNewQuery);
									}
								}

								if (iCompliateFlag.Equals(ViCommConst.FLAG_ON)) {
									sNextUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)));
								}

								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sNextUrl));
							}
						}
						break;
				}
			}
		}
		return bOk;
	}

	private void SetMailInfo(string sMailDataSeq,bool pIsReturnMail) {
		sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle = txtTitle.Text;
		sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc = txtDoc;
		sessionWoman.userWoman.mailData[sMailDataSeq].mailTemplateNo = lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value.Substring(0,4);
		sessionWoman.userWoman.mailData[sMailDataSeq].queryString = Request.QueryString.ToString();
		sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail = pIsReturnMail;
	}

	protected void SetMailTemplate(bool pIsReturnMail,string pTxMailLayOutType) {
		SetControl();
		using (MailTemplate oTemplate = new MailTemplate()) {
			bool bVoiceOnlyFlag = false;
			if (sessionWoman.carrier.Equals(ViCommConst.IPHONE) || sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.KDDI)) {
				bVoiceOnlyFlag = true;
			}
			DataSet ds = oTemplate.GetCastMailList(sessionWoman.site.siteCd,pIsReturnMail,pTxMailLayOutType,bVoiceOnlyFlag);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstMailTemplate.Items.Add(new MobileListItem(dr["TEMPLATE_NM"].ToString(),dr["MAIL_TEMPLATE_NO"].ToString()));
			}
		}
	}

	protected bool CheckOnlineStatus(string pMailDataSeq,bool pIsBatchMail) {
		int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);

		// 自分がトーク中
		if (iCharacterOnlineStatus == ViCommConst.USER_TALKING) {
			//一括送信だったらメール送信できない 
			if (pIsBatchMail) {
				this.lblErrorMsg.Text = base.GetErrorMessage(ViCommConst.ERR_STATUS_DISABLED_TX_MAIL);
				return false;
				//相手から見てログイン中以外だったらメール送信できない 
			} else {
				int iIsSeenOnlineStatus1;
				using (Favorit oFavorit = new Favorit()) {
					iIsSeenOnlineStatus1 = oFavorit.GetIsSeenOnlineStatus(
														sessionWoman.site.siteCd,
														sessionWoman.userWoman.userSeq,
														sessionWoman.userWoman.curCharNo,
														sessionWoman.userWoman.mailData[pMailDataSeq].rxUserSeq,
														ViCommConst.MAIN_CHAR_NO,
														iCharacterOnlineStatus);
				}
				if (iIsSeenOnlineStatus1 != ViCommConst.USER_LOGINED) {
					this.lblErrorMsg.Text = base.GetErrorMessage(ViCommConst.ERR_STATUS_DISABLED_TX_MAIL);
					return false;
				}
			}
		}

		// 自分が相手から見てオフラインだったらメール送信できない 
		if (!pIsBatchMail) {
			//自分が普通にオフライン（直ログインのときありえる） 
			if (iCharacterOnlineStatus == ViCommConst.USER_OFFLINE) {
				this.lblErrorMsg.Text = base.GetErrorMessage(ViCommConst.ERR_STATUS_DISABLED_TX_MAIL_OFFLINE);
				return false;
			}

			int iIsSeenOnlineStatus2;
			using (Favorit oFavorit = new Favorit()) {
				iIsSeenOnlineStatus2 = oFavorit.GetIsSeenOnlineStatus(
											sessionWoman.site.siteCd,
											sessionWoman.userWoman.userSeq,
											sessionWoman.userWoman.curCharNo,
											sessionWoman.userWoman.mailData[pMailDataSeq].rxUserSeq,
											ViCommConst.MAIN_CHAR_NO,
											iCharacterOnlineStatus);
			}
			if (iIsSeenOnlineStatus2 == ViCommConst.USER_OFFLINE) {
				if (IsAvailableService(ViCommConst.RELEASE_TX_JEALOUSY_REDIRECT,2)) {

					IDictionary<string,string> oParam = new Dictionary<string,string>();
					using (User oUser = new User()) {
						oUser.GetOne(sessionWoman.userWoman.mailData[pMailDataSeq].rxUserSeq,ViCommConst.MAN);
						oParam.Add("loginid",oUser.loginId);
					}
					RedirectToDisplayDoc(ViCommConst.SCR_TX_MAIL_JEALOUSY_REDIRECT,oParam);
				} else {
					this.lblErrorMsg.Text = base.GetErrorMessage(ViCommConst.ERR_STATUS_DISABLED_TX_MAIL_JEALOUSY);
					return false;
				}
			}
		}

		return true;
	}

	protected virtual bool CheckOther() {
		return true;
	}
	
	protected bool Confirm(string pMailDataSeq,bool pIsReturnMail) {
		SetControl();
		if (!CheckOnlineStatus(pMailDataSeq,false)) {
			return false;
		}

		bool bReleaseNonTitleByUesrMail = false;
		bReleaseNonTitleByUesrMail = IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2);
		if (bReleaseNonTitleByUesrMail) {
			txtTitle.Text = string.Empty;
			txtTitle.Visible = false;
		}

		// GETパラメータでメールテンプレートナンバーが指定されていた場合、強制的に選択状態にする		
		string sMailTemplateNo = iBridUtil.GetStringValue(this.Request.Params["mail_template_no"]);
		if (!string.IsNullOrEmpty(sMailTemplateNo)) {
			foreach (MobileListItem oItem in this.lstMailTemplate.Items) {
				if (object.Equals(oItem.Value,sMailTemplateNo)) {
					lstMailTemplate.SelectedIndex = oItem.Index;
					break;
				}
			}
		}

		bool bOk = true;

		lblErrorMsg.Text = string.Empty;

		string sUserSeq = string.Empty;
		string sNGWord;

		sUserSeq = sessionWoman.userWoman.mailData[pMailDataSeq].rxUserSeq;

		if (bOk) {
			if (txtTitle.Text.Equals(string.Empty) && txtTitle.Visible == true) {
				bOk = false;
				lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
			}
			if (txtDoc.Equals(string.Empty)) {
				bOk = false;
				lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
			}

			int iDocLengthLimit;

			if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["doc_limit_length"]),out iDocLengthLimit)) {
				iDocLengthLimit = 1000;
			}

			if (txtDoc.Length > iDocLengthLimit) {
				bOk = false;
				int iOverLength = txtDoc.Length - iDocLengthLimit;
				lblErrorMsg.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iBridUtil.GetStringValue(iOverLength),iBridUtil.GetStringValue(iDocLengthLimit));
			}
		}

		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (!IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2)) {
				if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc,out sNGWord) == false) {
					bOk = false;
					lblErrorMsg.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}

		if (bOk) {
			string sCheckTxMailErrorMessage = string.Empty;

			using (MailLog oMailLog = new MailLog()) {
				bOk = oMailLog.CheckTxMail(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						sUserSeq,
						lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,
						out sCheckTxMailErrorMessage
					);
			}
			if (!sCheckTxMailErrorMessage.Equals(string.Empty)) {
				lblErrorMsg.Text += sCheckTxMailErrorMessage + "<BR>";
			}
		}
		
		string sMailSeq = string.Empty;
		if (bOk == true) {
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(txtTitle.Text) : txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(txtDoc) : txtDoc));

			using (MailBox oMailBox = new MailBox()) {
				if (oMailBox.IsSameMailSent(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,ViCommConst.MAIN_CHAR_NO,sTitle,sDoc)) {
					lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAIL_SAME_SEND);
					return false;
				}
			}
			
			SetMailInfo(pMailDataSeq,pIsReturnMail);

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("TxMailPreview.aspx?data={0}",pMailDataSeq)));
		}
		return bOk;
	}



	protected bool MainteDraftMail(string pMailDataSeq,bool pIsReturnMail) {
		SetControl();
		if (!CheckOnlineStatus(pMailDataSeq,false)) {
			return false;
		}

		bool bReleaseNonTitleByUesrMail = false;
		bReleaseNonTitleByUesrMail = IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2);
		if (bReleaseNonTitleByUesrMail) {
			txtTitle.Text = string.Empty;
			txtTitle.Visible = false;
		}

		// GETパラメータでメールテンプレートナンバーが指定されていた場合、強制的に選択状態にする		
		string sMailTemplateNo = iBridUtil.GetStringValue(this.Request.Params["mail_template_no"]);
		if (!string.IsNullOrEmpty(sMailTemplateNo)) {
			foreach (MobileListItem oItem in this.lstMailTemplate.Items) {
				if (object.Equals(oItem.Value,sMailTemplateNo)) {
					lstMailTemplate.SelectedIndex = oItem.Index;
					break;
				}
			}
		}

		bool bOk = true;

		lblErrorMsg.Text = string.Empty;

		string sUserSeq = string.Empty;
		string sNGWord;

		sUserSeq = sessionWoman.userWoman.mailData[pMailDataSeq].rxUserSeq;

		if (bOk) {

			int iDocLengthLimit;

			if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["doc_limit_length"]),out iDocLengthLimit)) {
				iDocLengthLimit = 1000;
			}

			if (txtDoc.Length > iDocLengthLimit) {
				bOk = false;
				int iOverLength = txtDoc.Length - iDocLengthLimit;
				lblErrorMsg.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iBridUtil.GetStringValue(iOverLength),iBridUtil.GetStringValue(iDocLengthLimit));
			}
			
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (!IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2)) {
				if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc,out sNGWord) == false) {
					bOk = false;
					lblErrorMsg.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}

		string sMailSeq = string.Empty;
		if (bOk == true) {
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(txtTitle.Text) : txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(txtDoc) : txtDoc));

			int iAttachedObjType;
			string sValue;
			using (MailTemplate oTemplate = new MailTemplate()) {
				oTemplate.GetValue(sessionWoman.site.siteCd,lstMailTemplate.Items[lstMailTemplate.SelectedIndex].Value,"MAIL_ATTACHED_OBJ_TYPE",out sValue);
			}
			int.TryParse(sValue,out iAttachedObjType);

			using (DraftMail oDraftMail = new DraftMail()) {
				oDraftMail.MainteDraftMail(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sessionWoman.sexCd,
					sUserSeq,
					ViCommConst.MAIN_CHAR_NO,
					iBridUtil.GetStringValue(sessionWoman.userWoman.mailData[pMailDataSeq].draftMailSeq),
					sTitle,
					sDoc,
					iAttachedObjType.ToString(),
					sMailTemplateNo,
					pIsReturnMail == true ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF,
					iBridUtil.GetStringValue(sessionWoman.userWoman.mailData[pMailDataSeq].returnMailSeq)
				);
			}

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListDraftMail.aspx"));
		}
		return bOk;
	}

	private void DeleteDraftMail(string pMailDataSeq) {
		string sDraftMailSeq = iBridUtil.GetStringValue(sessionWoman.userWoman.mailData[pMailDataSeq].draftMailSeq);
		if (!string.IsNullOrEmpty(sDraftMailSeq)) {
			using (DraftMail oDraftMail = new DraftMail()) {
				oDraftMail.DeleteDraftMail(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,new string[] { sDraftMailSeq });
			}
		}
	}

	/// <summary>
	/// メール返信ボーナスポイント付与チェック
	/// </summary>
	/// <param name="pMailSeq"></param>
	/// <param name="pMailDataSeq"></param>
	/// <param name="sUserSeq"></param>
	/// <returns></returns>
	private string CreateMailResBonusEnableQuery(string pMailSeq,string pMailDataSeq,string sUserSeq) {
		string sMailResBonusQuery = string.Empty;
		if (IsAvailableService(ViCommConst.RELEASE_MAIL_RES_BONUS,2)) {
			using (MailResBonus oMailResBonus = new MailResBonus()) {
				string sAddPoint,sResult;
				oMailResBonus.AddMailResBonusPoint(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					pMailSeq,
					ViCommConst.FLAG_ON,
					out sAddPoint,
					out sResult
				);

				if (sResult.Equals(PwViCommConst.AddMailResBonusPoint.RESULT_OK)) {
					sMailResBonusQuery = "&mailresbonusflag=1";
				}
			}
		}

		// 3分/10分以内メール返信ボーナス　ポイント付与対象チェック
		if (string.IsNullOrEmpty(sMailResBonusQuery)
			&& ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.mailData[pMailDataSeq].mailResBonus2Flag)
		) {
			using (MailResBonusLog2 oMailResBonusLog2 = new MailResBonusLog2()) {
				// 3分以内メール返信ボーナス履歴登録
				string sMailResBonusResult = string.Empty;
				string sMailResBonusStatus = string.Empty;
				oMailResBonusLog2.MailResBonusLogMainte(
					sessionWoman.site.siteCd,
					sUserSeq,
					null,
					sessionWoman.userWoman.userSeq,
					pMailSeq,
					out sMailResBonusResult,
					out sMailResBonusStatus
				);

				// 3分/10分以内メール返信ボーナス　ポイント付与対象チェック
				string pMainFlag;
				string pSubFlag;
				oMailResBonusLog2.CheckSendMail(
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusSeq,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec1,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec2,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusMaxNum,
					sessionWoman.site.siteCd,
					sUserSeq,
					sessionWoman.userWoman.userSeq,
					pMailSeq,
					out pMainFlag,
					out pSubFlag
				);
				if (pMainFlag.Equals(ViCommConst.FLAG_ON_STR) || pSubFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sMailResBonusQuery = "&mailresbonusflag=1&v=2";
				}
			}
		}
		
		return sMailResBonusQuery;
	}
}
