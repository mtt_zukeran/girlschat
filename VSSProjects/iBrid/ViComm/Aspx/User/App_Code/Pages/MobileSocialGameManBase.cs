﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;
using System.Collections.Generic;

public abstract class MobileSocialGameManBase:MobileManPageBase {

	protected override bool CheckSiteUseStatus {
		get {
			return false;
		}
	}

	protected override void OnLoad(EventArgs e) {
		sessionMan.userMan.gameCharacter.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
		if (sessionMan.userMan.gameCharacter.gameLogined) {
			sessionMan.userMan.gameCharacter.CreateGamePageView(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sessionMan.userMan.gameCharacter.gameFirstLogined ? ViCommConst.FLAG_OFF : ViCommConst.FLAG_ON);
			sessionMan.userMan.gameCharacter.gameFirstLogined = true;
		}

		if (!string.IsNullOrEmpty(this.sessionMan.userMan.gameCharacter.tutorialStatus)) {
			this.DivideTutorialPage();
		}

		base.OnLoad(e);

		if (!this.IsPostBack) {
			/* 初期処理 */
			
		} else {
			if (this.IsPostAction(PwViCommConst.BUTTON_EXTSEARCH_GAME_CHAR)) {
				this.ListSearchAction();
			}
		}
	}
	
	/// <summary>
	/// 一覧下部の検索ボタン押下時処理 
	/// </summary>
	protected override void ListSearchAction() {
		
		String sExtTreasurePic;
		String sExtTreasureMovie;
		String sExtExtRelation;
		String sExtExtStartLevel;
		String sExtExtEndLevel;

		IDictionary<string,string> oParameters = new Dictionary<string,string>();

		UrlBuilder oUrlBuilder = new UrlBuilder(SessionObjs.Current.GetNavigateUrl(ViCommPrograms.GetCurrentAspx()),oParameters);
		
		sExtTreasurePic = iBridUtil.GetStringValue(this.Request.Form["ext_treasure_pic"]);
		sExtTreasureMovie = iBridUtil.GetStringValue(this.Request.Form["ext_treasure_movie"]);
		sExtExtRelation = iBridUtil.GetStringValue(this.Request.Form["ext_relation"]);
		sExtExtStartLevel = iBridUtil.GetStringValue(this.Request.Form["ext_start_level"]);
		sExtExtEndLevel = iBridUtil.GetStringValue(this.Request.Form["ext_end_level"]);

		if (!sExtExtStartLevel.Equals("")) {
			if (!SysPrograms.Expression(@"^\d+$",sExtExtStartLevel)) {
				sExtExtStartLevel = null;
			}
		}

		if (!sExtExtEndLevel.Equals("")) {
			if (!SysPrograms.Expression(@"^\d+$",sExtExtEndLevel)) {
				sExtExtEndLevel = null;
			}
		}

		oUrlBuilder.Parameters.Add("ext_treasure_pic",HttpUtility.UrlDecode(sExtTreasurePic));
		oUrlBuilder.Parameters.Add("ext_treasure_movie",HttpUtility.UrlDecode(sExtTreasureMovie));
		oUrlBuilder.Parameters.Add("ext_relation",HttpUtility.UrlDecode(sExtExtRelation));
		oUrlBuilder.Parameters.Add("ext_start_level",HttpUtility.UrlDecode(sExtExtStartLevel));
		oUrlBuilder.Parameters.Add("ext_end_level",HttpUtility.UrlDecode(sExtExtEndLevel));

		RedirectToMobilePage(oUrlBuilder.ToString(true));
	}
	
	private void DivideTutorialPage() {
		string sRedirectUrl = string.Empty;
		string sTownSeq = string.Empty;
		string sPartnerUserSeq = string.Empty;
		string sPartnerUserCharNo = string.Empty;
		string sGetTreasureLogSeq = string.Empty;
		string sBattleLogSeq = string.Empty;
		string sPreExpBattle = string.Empty;
		string sCastGamePicSeq = string.Empty;

		GetPartnerBattleTutorial(out sPartnerUserSeq,out sPartnerUserCharNo,out sCastGamePicSeq);

		string[] sSplitUrl = Request.Path.Split('/');
		string sCurrentAspx = sSplitUrl[sSplitUrl.Length - 1];

		if ((sCurrentAspx.Equals("ModifyGameUserProfile.aspx") && iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("01")) ||
			(sCurrentAspx.Equals("GameDisplayDoc.aspx") && iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_MAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS))) {
			return;
		}
		
		if(this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("0")) {
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "RegistAddOnInfoByGame.aspx":
				case "GameCharacterTypeRegist.aspx":
					break;
				default:
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_SELECT);
					break;
			}

			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_SELECT)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_SELECT);
				}
			}
		} else if (this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("1")) {
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "RegistGameClearMission.aspx":
				case "RegistGameTutorialSkip.aspx":
					break;
				default:
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_COMPLETE);
					break;
			}

			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_COMPLETE)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_COMPLETE);
				}
			}
		} else if (this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("2")) {
			switch (sCurrentAspx) {
				case "ViewGameTownClearMissionOK.aspx":
				case "ListGamePossessionManTreasureBattle.aspx":
				case "RegistGameBattleStart.aspx":
				case "RegistGameTutorialSkip.aspx":
					break;
				default:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("RegistGameBattleStart.aspx?partner_user_seq={0}&partner_user_char_no={1}&cast_game_pic_seq={2}",sPartnerUserSeq,sPartnerUserCharNo,sCastGamePicSeq)));
					break;
			}
		} else if (this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("3")) {
			switch (sCurrentAspx) {
				case "RegistGameBattleStart.aspx":
				case "RegistGameTutorialSkip.aspx":
					break;
				default:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("RegistGameBattleStart.aspx?partner_user_seq={0}&partner_user_char_no={1}&cast_game_pic_seq={2}",sPartnerUserSeq,sPartnerUserCharNo,sCastGamePicSeq)));
					break;
			}
		} else if(this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("4")) {
			switch (sCurrentAspx) {
				case "RegistGameBattleStart.aspx":
				case "RegistGameTutorialSkip.aspx":
					break;
				default:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("RegistGameBattleStart.aspx?partner_user_seq={0}&partner_user_char_no={1}&cast_game_pic_seq={2}",sPartnerUserSeq,sPartnerUserCharNo,sCastGamePicSeq)));
					break;
			}
		} else if (this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("5")) {
			switch (sCurrentAspx) {
				case "RegistGameBattleStart.aspx":
				case "RegistGameTutorialSkip.aspx":
					break;
				default:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("RegistGameBattleStart.aspx?partner_user_seq={0}&partner_user_char_no={1}&cast_game_pic_seq={2}",sPartnerUserSeq,sPartnerUserCharNo,sCastGamePicSeq)));
					break;
			}
		} else if (this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("6")) {
			switch (sCurrentAspx) {
				case "RegistGameBattleStart.aspx":
				case "RegistGameTutorialSkip.aspx":
					break;
				default:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("RegistGameBattleStart.aspx?partner_user_seq={0}&partner_user_char_no={1}&cast_game_pic_seq={2}",sPartnerUserSeq,sPartnerUserCharNo,sCastGamePicSeq)));
					break;
			}
		} else if (this.sessionMan.userMan.gameCharacter.tutorialStatus.Equals("7")) {
			this.GetGameTutorialBattleLogSeq(out sBattleLogSeq);
			this.GetGameTutorialPreExpBattle(out sPreExpBattle);
			sRedirectUrl = string.Format("ViewGameBattleResultWin.aspx?battle_log_seq={0}&pre_exp={1}",
											sBattleLogSeq,
											sPreExpBattle);
			switch (sCurrentAspx) {
				case "GameDisplayDoc.aspx":
				case "ViewGameBattleResultWin.aspx":
				case "ViewGameFlashBattle.aspx":
				case "ViewGameFlashLevelup.aspx":
				case "GameTutorialEnd.aspx":
					break;
				default:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sRedirectUrl));
					break;
			}
			
			if (sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
				if (!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_GAME_FLASH_BATTLE_SP) &&
					!iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(PwViCommConst.SCR_GAME_FLASH_LEVELUP_SP)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sRedirectUrl));
				}
			}
		}
	}
	
	private string GetGameTutorialTownSeq(){
		string sValue = string.Empty;
		
		TownSeekCondition oCondition = new TownSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.SexCd = this.sessionMan.sexCd;
		
		using (Town oTown = new Town()) {
			sValue = oTown.GetGameTutorialTownSeq(oCondition);
		}
		
		return sValue;
	}
	
	private void GetGameTutorialTreasureData(out string sPartnerUserSeq,out string sPartnerUserCharNo,out string sGetTreasureLogSeq) {
		DataSet oDataSet;
		
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		sPartnerUserSeq = string.Empty;
		sPartnerUserCharNo = string.Empty;
		sGetTreasureLogSeq = string.Empty;
		
		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oDataSet = oGetManTreasureLog.GetOneForTutorial(sSiteCd,sUserSeq,sUserCharNo);
		}
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sPartnerUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
			sPartnerUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
			sGetTreasureLogSeq = oDataSet.Tables[0].Rows[0]["GET_TREASURE_LOG_SEQ"].ToString();
		}
	}
	
	private void GetGameTutorialBattleLogSeq(out string sBattleLogSeq) {
		DataSet oDataSet;
		
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		sBattleLogSeq = string.Empty;

		using (BattleLog oBattleLog = new BattleLog()) {
			oDataSet = oBattleLog.GetOneForTutorial(sSiteCd,sUserSeq,sUserCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sBattleLogSeq = oDataSet.Tables[0].Rows[0]["BATTLE_LOG_SEQ"].ToString();
		}
	}

	private void GetGameTutorialPreExpBattle(out string pPreExpTutorial) {
		DataSet oDataSet;

		SocialGameSeekCondition oCondition = new SocialGameSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		string sBattleWinExp = string.Empty;
		int iBattleWinExp = 0;
		pPreExpTutorial = string.Empty;

		using (SocialGame oSocialGame = new SocialGame()) {
			oDataSet = oSocialGame.GetOne(oCondition);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sBattleWinExp = oDataSet.Tables[0].Rows[0]["BATTLE_WIN_EXP"].ToString();
			int.TryParse(sBattleWinExp,out iBattleWinExp);
			int iPreExpTutorial = this.sessionMan.userMan.gameCharacter.exp - iBattleWinExp;
			pPreExpTutorial = iPreExpTutorial.ToString();
		}
	}

	protected virtual void CheckGameRefusedByPartner(string pUserSeq,string pUserCharNo) {
		//相手の拒否リストに載っている場合、リダイレクトさせる。 
		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(
					sessionMan.site.siteCd,
					pUserSeq,
					pUserCharNo,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_GAME_MAN_REFUSED_BY_PARTNER);
			}
		}
	}

	protected virtual void CheckGameRefusePartner(string pUserSeq,string pUserCharNo) {
		//自分の拒否リストに相手が載っている場合、リダイレクトさせる。 
		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					pUserSeq,
					pUserCharNo)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_GAME_MAN_REFUSE_PARTNER);
			}
		}
	}

	private void GetPartnerBattleTutorial(out string pPartnerUserSeq,out string pPartnerUserCharNo,out string pCastGamePicSeq) {
		// チュートリアルバトルの相手データと当日のチュートリアル用お宝SEQを取得
		PossessionManTreasureSeekCondition oCondition = new PossessionManTreasureSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.DisplayDay = DateTime.Now.ToString("yyyy/MM/dd");
		oCondition.TutorialBattleFlag = ViCommConst.FLAG_ON_STR;
		oCondition.TreasureTutorialBattleFlag = ViCommConst.FLAG_ON_STR;
		
		DataSet oDataSet = null;
		using (PossessionManTreasure oPossessionManTreasure = new PossessionManTreasure()) {
			oDataSet = oPossessionManTreasure.GetTutorialBattleTreasureData(oCondition);
		}
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			pPartnerUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
			pPartnerUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
			pCastGamePicSeq = oDataSet.Tables[0].Rows[0]["CAST_GAME_PIC_SEQ"].ToString();
		} else {
			pPartnerUserSeq = string.Empty;
			pPartnerUserCharNo = string.Empty;
			pCastGamePicSeq = string.Empty;
		}
	}
}
