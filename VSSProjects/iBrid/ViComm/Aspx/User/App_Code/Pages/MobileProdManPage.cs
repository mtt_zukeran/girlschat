﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ViComm;

public class MobileProdManPage : MobileManPageBase {
	public MobileProdManPage() {}

	protected override void OnLoad(EventArgs e) {
		if(this.IsPostAction(ViCommConst.BUTTON_EXTSEARCH)){
			this.ExtSearch();		
			return;
		}

		base.OnLoad(e);
	}

	private void ExtSearch() {
		NameValueCollection oQueries = new ProductSeekCondition(this.Request.Form).GetQuery();

		UrlBuilder oUrlBuilder = new UrlBuilder(sessionMan.GetNavigateUrl(ViCommPrograms.GetCurrentAspx()));

		foreach (string sKey in this.Request.QueryString.Keys) {
			if(this.Request.Form[sKey]!=null)continue;
			oQueries.Remove(sKey);
			oQueries.Add(sKey, this.Request.QueryString[sKey]);
		}

		foreach (string sKey in oQueries.Keys) {
			oUrlBuilder.AddParameter(sKey, HttpUtility.UrlEncodeUnicode(oQueries[sKey]));
		}

		RedirectToMobilePage(oUrlBuilder.ToString());
	}
}
