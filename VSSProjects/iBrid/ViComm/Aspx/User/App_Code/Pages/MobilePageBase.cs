﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: MobilePageBase
--
--  Creation Date	: 2010.08.03
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;
using MobileLib.Pictograph;
using ViComm.Extension.Pwild;

/// <summary>
/// 画面の基本機能を提供する抽象基底クラス 
/// </summary>
public abstract class MobilePageBase:System.Web.UI.MobileControls.MobilePage,MobileLib.IEarlyParseEmoji {
	private static readonly Regex _regexPict = new Regex(@"(?<pict>\$x.{4};)",RegexOptions.Compiled);
	private volatile static object _syncRoot = new object();


	#region □■□ プロパティ □■□ ==================================================================================
	private bool clearCondition;

	/// <summary>
	/// セッションオブジェクトを取得する。 
	/// </summary>
	protected virtual SessionObjs sessionObj {
		get {
			return SessionObjs.Current;
		}
	}
	protected virtual bool NeedAccessLog {
		get {
			return false;
		}
	}
	protected virtual bool NeedLogin {
		get {
			return false;
		}
	}

	protected virtual bool ClearCondition {
		get {
			return clearCondition;
		}
		set {
			clearCondition = value;
		}
	}

	/// <summary>
	/// 既にｱｸｾｽ済みのﾍﾟｰｼﾞのリストを取得する。 
	/// </summary>
	public IList<string> AlreadyAccessPageList {
		get {
			if (Session["MobilePageBase.IsFirstAccessInSession"] == null) {
				Session["MobilePageBase.IsFirstAccessInSession"] = new List<string>();
			}
			return (IList<string>)Session["MobilePageBase.IsFirstAccessInSession"];
		}
		private set {
			Session["MobilePageBase.IsFirstAccessInSession"] = value;
		}
	}
	/// <summary>
	/// 同一セッション内で初めてのアクセスかどうかを示す値を取得する。 
	/// </summary>
	public bool IsFirstAccessInSession {
		get {
			return !this.AlreadyAccessPageList.Contains(sessionObj.currentAspx);
		}
	}

	#endregion ========================================================================================================

	public void EarlyParseEmoji(ITextControl pControl) {
		// EmojiTag to Text
		ITextControl oTextControl = (ITextControl)pControl;

		if (oTextControl.Text.Trim().Length > 0) {
			PictographConverter oPictConverter = PictographConverter.Create(sessionObj.carrier);
			oPictConverter.NeedConvert2Image = false;
			oTextControl.Text = _regexPict.Replace(oTextControl.Text,new MatchEvaluator(delegate(Match pMatch) {
				return SysConst.EncodingShiftJIS.GetString(oPictConverter.ConvertTag2Pict(pMatch.Value,true));
			}));
		}
	}

	/// <summary>
	/// エラーメッセージを取得する 
	/// </summary>
	/// <param name="pErrorCode">エラーコード</param>
	/// <returns>エラーメッセージ</returns>
	protected virtual string GetErrorMessage(string pErrorCode,bool pAddBr) {
		string sErrorCd = string.Empty;
		if (sessionObj.designDebug) {
			sErrorCd = string.Format("<br />└ｴﾗｰｺｰﾄﾞ:{0}",pErrorCode);
		}
		using (ErrorDtl oErrorDtl = new ErrorDtl()) {
			if (pAddBr) {
				return string.Concat(string.Format("{0}{1}",oErrorDtl.GetErrorDtl(sessionObj.site.siteCd,pErrorCode),sErrorCd),"<br />");
			} else {
				return string.Format("{0}{1}",oErrorDtl.GetErrorDtl(sessionObj.site.siteCd, pErrorCode),sErrorCd);
			}
		}
	}

	protected virtual string GetErrorMessage(string pErrorCode) {
		return GetErrorMessage(pErrorCode,true);
	}
	/// <summary>
	/// アプリケーションルートからのパスを生成する 
	/// </summary>
	/// <param name="pPageAndQuery"></param>
	/// <returns></returns>
	protected virtual string GenerateFullUrl(string pPageAndQuery) {
		return sessionObj.GetNavigateUrl(sessionObj.root + sessionObj.sysType,sessionObj.sessionId,pPageAndQuery);
	}

	/// <summary>
	/// 個別機能設定が利用可能か取得する 
	/// </summary>
	/// <param name="pReleaseCode"></param>
	/// <returns></returns>
	protected virtual bool IsAvailableService(ulong pReleaseCode) {
		using (ManageCompany oManageCompany = new ManageCompany()) {
			return oManageCompany.IsAvailableService(pReleaseCode);
		}
	}
	protected virtual bool IsAvailableService(ulong pReleaseCode,int pNo) {
		using (ManageCompany oManageCompany = new ManageCompany()) {
			return oManageCompany.IsAvailableService(pReleaseCode,pNo);
		}
	}


	#region □■□ ﾍﾟｰｼﾞ遷移関連 □■□ ===============================================================================

	protected virtual void RedirectToDisplayDoc(string pDocId) {
		this.RedirectToDisplayDoc(pDocId,new Dictionary<string,string>());
	}
	protected virtual void RedirectToDisplayDoc(string pDocId,NameValueCollection pParamters) {
		this.RedirectToDisplayDoc(pDocId,ViCommPrograms.ConvertToDictionary(pParamters));
	}
	protected virtual void RedirectToDisplayDoc(string pDocId,IDictionary<string,string> pParamters) {
		string sBaseUrl = GenerateFullUrl("DisplayDoc.aspx");
		UrlBuilder oUrlBuilder = new UrlBuilder(sBaseUrl);
		oUrlBuilder.Parameters.Add("doc",pDocId);

		foreach (KeyValuePair<string,string> oParameter in pParamters) {
			oUrlBuilder.Parameters.Add(oParameter);
		}

		RedirectToMobilePage(oUrlBuilder.ToString(),true);
	}

	protected virtual void RedirectToErrorDisplayDoc(string pDocId,string pErrorCode) {
		sessionObj.errorMessage = GetErrorMessage(pErrorCode);
		this.RedirectToDisplayDoc(pDocId,new Dictionary<string,string>());
	}

	protected virtual void RedirectToGameDisplayDoc(string pDocId) {
		this.RedirectToGameDisplayDoc(pDocId,new Dictionary<string,string>());
	}
	protected virtual void RedirectToGameDisplayDoc(string pDocId,NameValueCollection pParamters) {
		this.RedirectToGameDisplayDoc(pDocId,ViCommPrograms.ConvertToDictionary(pParamters));
	}
	protected virtual void RedirectToGameDisplayDoc(string pDocId,IDictionary<string,string> pParamters) {
		string sBaseUrl = GenerateFullUrl("GameDisplayDoc.aspx");
		UrlBuilder oUrlBuilder = new UrlBuilder(sBaseUrl);
		oUrlBuilder.Parameters.Add("doc",pDocId);

		foreach (KeyValuePair<string,string> oParameter in pParamters) {
			oUrlBuilder.Parameters.Add(oParameter);
		}

		RedirectToMobilePage(oUrlBuilder.ToString(),true);
	}

	#endregion ========================================================================================================

	protected bool IsPostAction(string pActionName) {
		return !string.IsNullOrEmpty(Request.Params[pActionName]);
	}
	protected bool IsGetAction(string pActionName) {
		bool bIsAction = false;

		bIsAction = bIsAction || iBridUtil.GetStringValue(Request.QueryString[ViCommConst.ACTION_DELETE]).Equals(ViCommConst.FLAG_ON_STR);
		bIsAction = bIsAction || (!iBridUtil.GetStringValue(Request.QueryString[ViCommConst.ACTION]).Equals("") && iBridUtil.GetStringValue(Request.QueryString[ViCommConst.ACTION]).Equals(iBridUtil.GetStringValue(Request.QueryString[ViCommConst.ACTION_DELETE])));

		return bIsAction;
	}

	protected override void OnLoad(EventArgs e) {
		this.AppendToLog();
		
		if (sessionObj != null) {
			sessionObj.parseContainer.parseUser.postAction = 0;
			if (HttpContext.Current != null) {
				sessionObj.requestQuery = HttpContext.Current.Request;
			}
		}

		if (this.IsPostBack) {
			if (Request.Params[ViCommConst.BUTTON_GOTO_LINK] != null) {
				GotoPage(string.Empty);
			}
			for (int i = 0;i < 10;i++) {
				if (Request.Params[ViCommConst.BUTTON_GOTO_LINK + i] != null) {
					GotoPage(i.ToString());
				}
			}
		} else {
			if (this.NeedLogin) {
				if (!sessionObj.logined) {
					this.RedirectToMobilePage(sessionObj.GetNavigateUrl("RegistUserRequestByTermId.aspx"));
				}
			}
		}

		base.OnLoad(e);

		if (!this.IsPostBack) {
			this.SetAlreadyAccess();
			// ﾍﾟｰｼﾞのｱｸｾｽﾛｸﾞを書き込む 
			this.AccessPage(ViCommConst.ProgramAction.LOAD);

			if (!(sessionObj != null && sessionObj.logined)) {
				this.AddBreadcrumb();
			}
			
			//this.RegistPageAccessLog();
		}

		if (sessionObj.carrier.Equals(ViCommConst.DOCOMO)) {
			System.Web.UI.MobileControls.Command oCommand = new System.Web.UI.MobileControls.Command();
			if (!this.ActiveForm.Controls.Contains(oCommand)) {
				oCommand.ID = "cmdDummy";
				this.ActiveForm.Controls.Add(oCommand);
			}
		}
	}

	public BreadcrumbManage GetBreadcrumbManage() {
		BreadcrumbManage oBreadcrumbManage = null;
		string sHtmlDocSeq = "-1";
		if (SessionObjs.Current != null && SessionObjs.Current.currentProgramId != null) {
			if (SessionObjs.Current.currentProgramId.Equals("DisplayDoc.aspx")) {
				sHtmlDocSeq = iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["doc"]);
			}

			oBreadcrumbManage = new BreadcrumbManage();
			oBreadcrumbManage.GetOne(SessionObjs.Current.currentProgramRoot,SessionObjs.Current.currentProgramId,sHtmlDocSeq);
		}

		return oBreadcrumbManage;
	}
	private void AddBreadcrumb() {
		if (!this.IsPostBack) {
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableBreadcrumbList"]).Equals(ViCommConst.FLAG_ON_STR)) {
				BreadcrumbManage oBreadcrumbManage = GetBreadcrumbManage();
				if (oBreadcrumbManage != null && oBreadcrumbManage.enableBreadcrumbFlag) {
					if (oBreadcrumbManage.topBreadcrumbFlag) {
						sessionObj.breadcrumbList.Clear();
					}

					Breadcrumb oNewBreadcrumb = new Breadcrumb(Breadcrumb.GenearteKey(sessionObj),GetCurrentUrl(),sessionObj.parseContainer.Parse(oBreadcrumbManage.breadcrumbTitle));

					if (sessionObj.breadcrumbList.Contains(oNewBreadcrumb)) {
						for (int i = (sessionObj.breadcrumbList.Count - 1);i >= 0;i--) {
							Breadcrumb oBreadcrumb = sessionObj.breadcrumbList[i];
							sessionObj.breadcrumbList.RemoveAt(i);
							if (oBreadcrumb.Key.Equals(oNewBreadcrumb.Key))
								break;
						}
					}
					sessionObj.breadcrumbList.Add(oNewBreadcrumb);
				}
			}
		}
	}
	private string GetCurrentUrl() {
		UrlBuilder oUrlBuilder = new UrlBuilder(sessionObj.currentAspx,this.Request.QueryString);
		return sessionObj.GetNavigateUrl(oUrlBuilder.ToString());
	}


	protected virtual void SetAlreadyAccess() {
		if (this.IsFirstAccessInSession)
			this.AlreadyAccessPageList.Add(sessionObj.currentAspx);
	}

	/// <summary>
	/// ﾍﾟｰｼﾞ別ｱｸｾｽのｶｳﾝﾀをｶｳﾝﾄする 
	/// </summary>
	protected void AccessPage(string pAction) {
		if (!this.NeedAccessLog)
			return;

		try {
			string sUserSeq = sessionObj.GetUserSeq();
			if (string.IsNullOrEmpty(sUserSeq))
				return;

			using (Access oAccess = new Access()) {
				oAccess.AccessPage(
					sessionObj.site.siteCd,
					sessionObj.sysType,
					sessionObj.currentAspx,
					pAction,
					sUserSeq);
			}
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"AccessPage","");
			/* memo:
			 * 　ｱｸｾｽﾛｸﾞ集計のエラー程度で他機能に影響を与えるのは適切ではないので、 
			 * 　エラーログ出力にとどめ、再スローは行わない 
			 */
		}
	}

	/// <summary>
	/// ﾍﾟｰｼﾞ別ｱｸｾｽのﾛｸﾞを登録する
	/// </summary>
	private void RegistPageAccessLog() {
		try {
			int iLoginFlag = ViCommConst.FLAG_OFF;
			string sSexCd;
			string sUserSeq = string.Empty;
			string sUserCharNo = string.Empty;
			string sUserRank = ViCommConst.USER_RANK_A;
			int iPaymentFlag = ViCommConst.FLAG_OFF;
			int iNewUserFlag = ViCommConst.FLAG_OFF;
			int iIsImpersonatedFlag = ViCommConst.FLAG_OFF;
			string sProgramRoot = string.Empty;
			string sHtmlDocType = string.Empty;
			string sCarrierType;

			if (sessionObj.carrier.Equals(ViCommConst.ANDROID)) {
				sCarrierType = PwViCommConst.PageAccessCarrierType.ANDROID;
			} else if (sessionObj.carrier.Equals(ViCommConst.IPHONE)) {
				sCarrierType = PwViCommConst.PageAccessCarrierType.IPHONE;
			} else {
				sCarrierType = PwViCommConst.PageAccessCarrierType.FEATURE_PHONE;
			}
			
			SessionMan oSessionMan;
			SessionWoman oSessionWoman;

			if (sessionObj.logined) {
				iLoginFlag = ViCommConst.FLAG_ON;
			} else if (sessionObj.IsImpersonated) {
				iLoginFlag = ViCommConst.FLAG_ON;
				iIsImpersonatedFlag = ViCommConst.FLAG_ON;
			}

			if (iLoginFlag == ViCommConst.FLAG_ON) {
				if (iIsImpersonatedFlag == ViCommConst.FLAG_ON) {
					if (!sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						sSexCd = ViCommConst.MAN;
						oSessionWoman = (SessionWoman)sessionObj;

						sUserSeq = oSessionWoman.originalManObj.userMan.userSeq;
						sUserRank = oSessionWoman.originalManObj.userMan.userRankCd;
						if (DateTime.Parse(oSessionWoman.originalManObj.userMan.registDay) > DateTime.Now.AddDays(-1 * oSessionWoman.originalManObj.site.manNewFaceDays)) {
							iNewUserFlag = ViCommConst.FLAG_ON;
						}
					} else {
						sSexCd = ViCommConst.OPERATOR;
						oSessionMan = (SessionMan)sessionObj;
						sUserSeq = oSessionMan.originalWomanObj.userWoman.userSeq;
						sUserCharNo = oSessionMan.originalWomanObj.userWoman.curCharNo;
						iPaymentFlag = oSessionMan.originalWomanObj.userWoman.paymentFlag;
						if (DateTime.Parse(oSessionMan.originalWomanObj.userWoman.CurCharacter.startPerformDay) > DateTime.Now.AddDays(-1 * oSessionMan.originalWomanObj.site.newFaceDays)) {
							iNewUserFlag = ViCommConst.FLAG_ON;
						}
					}
				} else {
					sSexCd = sessionObj.sexCd;
					if (sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						oSessionMan = (SessionMan)sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserRank = oSessionMan.userMan.userRankCd;
						if (DateTime.Parse(oSessionMan.userMan.registDay) > DateTime.Now.AddDays(-1 * oSessionMan.site.manNewFaceDays)) {
							iNewUserFlag = ViCommConst.FLAG_ON;
						}
					} else {
						oSessionWoman = (SessionWoman)sessionObj;
						sUserSeq = oSessionWoman.userWoman.userSeq;
						sUserCharNo = oSessionWoman.userWoman.curCharNo;
						iPaymentFlag = oSessionWoman.userWoman.paymentFlag;
						if (DateTime.Parse(oSessionWoman.userWoman.CurCharacter.startPerformDay) > DateTime.Now.AddDays(-1 * oSessionWoman.site.newFaceDays)) {
							iNewUserFlag = ViCommConst.FLAG_ON;
						}
					}
				}
				
				if (sessionObj.pageViewCount > 0 || !sessionObj.currentProgramId.Equals("UserTop.aspx")) {
					sessionObj.pageViewCount++;
				}
			} else {
				if (this.Request.Url.Host.Equals(sessionObj.site.jobOfferSiteHostNm)) {
					sSexCd = ViCommConst.OPERATOR;
				} else {
					sSexCd = ViCommConst.MAN;
				}
			}


			if (sessionObj.currentProgramId.Equals("DisplayDoc.aspx") || sessionObj.currentProgramId.Equals("GameDisplayDoc.aspx")) {
				sProgramRoot = "-";
				sHtmlDocType = iBridUtil.GetStringValue(Request.QueryString["doc"]);
			} else {
				sProgramRoot = sessionObj.currentProgramRoot;
				sHtmlDocType = "-";
			}
			
			string sQuery = this.CreatePageAccessQuery();
			
			if (sSexCd.Equals(ViCommConst.MAN)) {
				using (UserPageAccessLog oUserPageAccessLog = new UserPageAccessLog()) {
					oUserPageAccessLog.RegistUserPageAccessLog(
						sessionObj.site.siteCd,
						sUserSeq,
						sProgramRoot,
						sessionObj.currentProgramId,
						sHtmlDocType,
						sQuery,
						sessionObj.pageViewCount < 6 ? sessionObj.pageViewCount : 0,
						sessionObj.currentPageAgentType,
						sCarrierType,
						iLoginFlag,
						sUserRank,
						iNewUserFlag
					);
				}
			} else {
				using (CastPageAccessLog oCastPageAccessLog = new CastPageAccessLog()) {
					oCastPageAccessLog.RegistCastPageAccessLog(
						sessionObj.site.siteCd,
						sUserSeq,
						sUserCharNo,
						sProgramRoot,
						sessionObj.currentProgramId,
						sHtmlDocType,
						sQuery,
						sessionObj.pageViewCount < 6 ? sessionObj.pageViewCount : 0,
						sessionObj.currentPageAgentType,
						sCarrierType,
						iLoginFlag,
						iPaymentFlag,
						iNewUserFlag
					);
				}
			}
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"RegistPageAccessLog","");
			/* memo:
			 * 　ｱｸｾｽﾛｸﾞ集計のエラー程度で他機能に影響を与えるのは適切ではないので、 
			 * 　エラーログ出力にとどめ、再スローは行わない 
			 */
		}
	}
	
	private string CreatePageAccessQuery() {
		string sQuery = string.Empty;

		switch (sessionObj.currentProgramId) {
			case "UserTop.aspx":
				break;
			default:
				SortedList<string,string> oQueryList = new SortedList<string,string>();
				foreach (string sKey in this.Request.QueryString.AllKeys) {
					if (!string.IsNullOrEmpty(sKey)) {
						string sValue = iBridUtil.GetStringValue(this.Request.QueryString[sKey]);
						if (!string.IsNullOrEmpty(sValue)) {
							oQueryList.Add(sKey.ToLower(),sValue);
						}
					}
				}

				oQueryList.Remove("__ufps");
				oQueryList.Remove("drows");
				oQueryList.Remove("lrows");
				oQueryList.Remove("list");
				oQueryList.Remove("site");
				oQueryList.Remove("charno");
				oQueryList.Remove("_aurandom");
				oQueryList.Remove("guid");

				foreach (KeyValuePair<string,string> oKeyValuePair in oQueryList) {
					sQuery += (sQuery.Length > 0 ? "&" : string.Empty) + oKeyValuePair.Key + "=" + oKeyValuePair.Value;
				}
				break;
		}
		
		return sQuery;
	}

	/// <summary>
	/// メール送信 
	/// </summary>
	/// <param name="fromAddress"></param>
	/// <param name="toAddress"></param>
	/// <param name="title"></param>
	/// <param name="contents"></param>
	protected void SendMail(string fromAddress,string toAddress,string title,string contents) {
		SendMail(fromAddress,toAddress,title,contents,false);
	}
	protected void SendMail(string fromAddress,string toAddress,string title,string contents,bool pUtf8Flag) {
		string sTestHost = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SmtpTestHost"]);
		Encoding enc;
		if (pUtf8Flag) {
			enc = Encoding.GetEncoding("utf-8");
		} else {
			string sSendMailEncode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["SendMailEncode"]).Trim();
			if (string.IsNullOrEmpty(sSendMailEncode)) {
				sSendMailEncode = "Shift_JIS";
			}
			enc = Encoding.GetEncoding(sSendMailEncode);
		}

		Agiletech.Net.Mail.MailMessage msg = new Agiletech.Net.Mail.MailMessage(); 

		msg.SubjectEncoding = enc;
		msg.BodyEncoding = enc;
		msg.From = new Agiletech.Net.Mail.MailAddress(fromAddress);
		msg.To.Add(new Agiletech.Net.Mail.MailAddress(toAddress));
		msg.Subject = title;
		msg.Body = contents;
		msg.Headers.HeaderItems.Add("Date",DateTime.Now.ToString("R").Replace("GMT","+0900"));
		
		Agiletech.Net.Mail.SMTPClient sc = new Agiletech.Net.Mail.SMTPClient(string.Empty);
		if (sTestHost.Equals(string.Empty)) {
			sc.Server = "127.0.0.1";
		} else {
			sc.Server = sTestHost;
		}
		sc.SendMail(msg);
	}

	private void GotoPage(string sButtonNo) {
		string sGotoUrl = iBridUtil.GetStringValue(Request.Params["GOTOLINK" + sButtonNo]);
		string sSysType = sessionObj.sysType;

		if (sessionObj.IsImpersonated == true) {
			if (sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSysType = "/ViComm/woman";
			} else {
				sSysType = "/ViComm/man";
			}
		}

		RedirectToMobilePage(sessionObj.GetNavigateUrl(sessionObj.root + sSysType,Session.SessionID,sGotoUrl));
	}

    protected string GetScreenIdRemovedQueryString()
    {
        StringBuilder sbQuery = new StringBuilder();
        foreach (string queryKey in Request.QueryString.Keys)
        {
            if ("scrid".Equals(queryKey))
            {
                continue;
            }
            sbQuery.Append(string.Format("&{0}={1}", queryKey, Request.QueryString[queryKey]));
        }
        return sbQuery.ToString();
    }
    
    
    private void AppendToLog(){
		try{		
			string format = ConfigurationManager.AppSettings["extendLogFormat"];
			if(!string.IsNullOrEmpty(format)){
				String sUserSeq = string.Empty;
				String sSiteCd = string.Empty;
				String sAdCd = string.Empty;
				String sAdGrpCd = string.Empty;
				String sSexCd = string.Empty;
				String sIsLogined = string.Empty;
				String sAdminFlg = string.Empty;
				
				if (sessionObj != null) {
					sUserSeq = sessionObj.GetUserSeq();
					
					if(sessionObj.site != null){
						sSiteCd = sessionObj.site.siteCd;
					}
					
					sAdCd = sessionObj.adCd;
					sAdGrpCd = sessionObj.adGroup != null ? sessionObj.adGroup.adGroupCd : null;
					sSexCd = sessionObj.sexCd;
					sIsLogined = sessionObj.logined.ToString();
					sAdminFlg = sessionObj.adminFlg.ToString();				
				}
				
				
				
				// ex)__sid={0}&__userseq={1}&__site={2}&__adcd={3}&__adgrpcd={4}&__sex={5}&__logined={6}&__adminflg={7}
				//
				// {0}:SessionId
				// {1}:ﾕｰｻﾞｰSEQ
				// {2}:ｻｲﾄｺｰﾄﾞ
				// {3}:広告ｺｰﾄﾞ
				// {4}:広告ｸﾞﾙｰﾌﾟｺｰﾄﾞ
				// {5]:性別ｺｰﾄﾞ
				// {6}:ﾛｸﾞｲﾝﾌﾗｸﾞ
				// {7}:管理者ﾌﾗｸﾞ
				
				this.Response.AppendToLog(String.Format(format
											, sessionObj.sessionId
											, sUserSeq
											, sSiteCd
											, sAdCd
											, sAdGrpCd
											, sSexCd
											, sIsLogined
											, sAdminFlg));
			}
		}catch(Exception){
			/* memo:特に処理しない */
		}
    }
    
    protected void CheckCookieData (string pUserSeq,string pPassword,string pSexCd) {
		if (this.GetStringValue(ConfigurationManager.AppSettings["EnableCookieCheck"]).Equals(ViCommConst.FLAG_ON_STR)) {
			if (sessionObj.carrier.Equals(ViCommConst.ANDROID) || sessionObj.carrier.Equals(ViCommConst.IPHONE) || (sessionObj.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionObj.adminFlg)) {
				if (this.GetStringValue(ConfigurationManager.AppSettings["EnableCookie"]).Equals(ViCommConst.FLAG_ON_STR)) {
					if (Request.Cookies["maqia"] != null) {
						if (
							this.GetStringValue(Request.Cookies["maqia"]["maqiauid"]) != pUserSeq ||
							this.GetStringValue(Request.Cookies["maqia"]["maqiapw"]) != pPassword ||
							this.GetStringValue(Request.Cookies["maqia"]["maqiasex"]) != pSexCd
						) {
							Response.Redirect(string.Format("http://{0}/user/start.aspx",Request.Url.Host));
						}
					}
				}
			}
		}
	}

	//マルチスレッド問題回避
	private string GetStringValue(object value) {
		if (value == null) {
			return "";
		} else {
			return value.ToString();
		}
	}
}
