﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ViComm;

public abstract class MobileBlogWomanBase :MobileWomanPageBase {
	protected virtual bool NeedBlogRegistPermission { get { return false; } }
	protected virtual bool AllowRedirect2ModifyBlog { get { return true; } }

	protected override void OnLoad(EventArgs e) {
		if(NeedBlogRegistPermission){
			if(sessionWoman.userWoman.CurCharacter.characterEx.enabledBlogFlag != ViCommConst.FLAG_ON){
				// ブログ解説許可が無い場合は通常掲示板の書き込みページへ遷移
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("WriteCastBbs.aspx"));
			}else if(string.IsNullOrEmpty(sessionWoman.userWoman.CurCharacter.characterEx.blogSeq)){
				if (AllowRedirect2ModifyBlog) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("ModifyBlog.aspx"));
				}
			}
		}
		base.OnLoad(e);
	}

}
