﻿using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ViComm;

public abstract class MobileBbsExWomanBase :MobileWomanPageBase {

	protected override void OnLoad(EventArgs e) {
		//BLACK_LISTの処理
		using (CastCharacterBlack oCastCharacterBlack = new CastCharacterBlack()) {
			if (oCastCharacterBlack.IsBlack(sessionWoman.site.siteCd, sessionWoman.userWoman.userSeq, sessionWoman.userWoman.curCharNo, ViCommConst.CastCharacterBlackType.TalkBbs)) {
				RedirectToDisplayDoc(ViCommConst.SCR_BBS_EX_BLACK);
			}
		}

		base.OnLoad(e);
	}

}
