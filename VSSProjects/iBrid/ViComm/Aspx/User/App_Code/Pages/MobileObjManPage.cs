﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: MobileObjManPage
--
--  Creation Date	: 2010.10.10
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

/// <summary>
/// オブジェクト関係の基本機能を提供する基底クラス
/// </summary>
public abstract class MobileObjManPage:MobileManPageBase {
	private const string ATTR_TYPE_TABLE_NAME = "AttrType";
	private const string ATTR_TABLE_NAME = "Attr";

	private SelectionList lstObjAttrCombo;

	protected virtual SelectionList ObjAttrCombo {
		set {
			lstObjAttrCombo = value;
		}
	}

	protected abstract void SetControl();

	protected void CreateObjAttrComboBox(string pUserSeq,string pUserCharNo,string sCategorySeq,string pObjType,string pAttachedType,bool pZeroIsVisible,bool pNeedCache) {
		CreateObjAttrComboBoxBase(pUserSeq,pUserCharNo,sCategorySeq,pObjType,pAttachedType,pZeroIsVisible,false,pNeedCache,false);
	}
	protected void CreateObjAttrComboBox(string pUserSeq,string pUserCharNo,string sCategorySeq,string pObjType,string pAttachedType,bool pZeroIsVisible,bool pRecentNotLogin,bool pNeedCache,bool pOnlyUsed) {
		CreateObjAttrComboBoxBase(pUserSeq,pUserCharNo,sCategorySeq,pObjType,pAttachedType,pZeroIsVisible,pRecentNotLogin,pNeedCache,pOnlyUsed);
	}
	private void CreateObjAttrComboBoxBase(string pUserSeq,string pUserCharNo,string sCategorySeq,string pObjType,string pAttachedType,bool pZeroIsVisible,bool pRecentNotLogin,bool pNeedCache,bool pOnlyUsed) {
		this.SetControl();
		NameValueCollection nvcAttrType = new NameValueCollection();
		NameValueCollection nvcAttr = new NameValueCollection();
		int iTotalCnt = 0;
		int iCnt = 0;
		int iDefaultAttrTypeSeq = 0;
		int iDefaultAttrSeq = 0;

		if (pObjType.Equals(ViCommConst.BbsObjType.PIC)) {
			iDefaultAttrTypeSeq = ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ;
			iDefaultAttrSeq = ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ;
		} else {
			iDefaultAttrTypeSeq = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ;
			iDefaultAttrSeq = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ;
		}

		DataSet dsObjAttr = this.GetObjAttrDataSet(pUserSeq,pUserCharNo,sCategorySeq,pObjType,pAttachedType,pRecentNotLogin,pNeedCache,pOnlyUsed);
		DataTable dtAttrType = dsObjAttr.Tables[ATTR_TYPE_TABLE_NAME];
		DataTable dtAttr = dsObjAttr.Tables[ATTR_TABLE_NAME];

		//合計値を算出
		iTotalCnt = Convert.ToInt32(dtAttrType.Compute("Sum(CNT)",""));

		foreach (DataRow dr in dtAttrType.Rows) {
			int.TryParse(dr["CNT"].ToString(),out iCnt);
			if ((!pZeroIsVisible) || (iCnt > 0)) {
				if (!dr["OBJ_ATTR_TYPE_SEQ"].ToString().Equals(iDefaultAttrTypeSeq.ToString())) {
					nvcAttrType.Add(dr["OBJ_ATTR_TYPE_SEQ"].ToString(),dr["CNT"] + "," + dr["OBJ_ATTR_TYPE_NM"]);
				}
			}
		}
		foreach (DataRow dr in dtAttr.Rows) {
			int.TryParse(dr["CNT"].ToString(),out iCnt);
			if ((!pZeroIsVisible) || (iCnt > 0)) {
				if (!dr["OBJ_ATTR_TYPE_SEQ"].ToString().Equals(iDefaultAttrTypeSeq.ToString()) || !dr["OBJ_ATTR_SEQ"].ToString().Equals(iDefaultAttrSeq.ToString())) {
					nvcAttr.Add(dr["OBJ_ATTR_TYPE_SEQ"].ToString(),dr["CNT"] + "," + dr["OBJ_ATTR_SEQ"] + "," + dr["OBJ_ATTR_NM"]);
				}
			}
		}

		if (!IsAvailableService(ViCommConst.RELEASE_OBJ_ATTR_TYPE_HIDE_ALL)) {
			lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("■全て({0})",iTotalCnt),","));
		}
		if (iTotalCnt > 0) {
			foreach (string key in nvcAttrType.Keys) {
				string[] sAttrType = nvcAttrType.GetValues(key)[0].Split(',');
				lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("■{0}({1})",sAttrType[1],sAttrType[0]),string.Format("{0},{1}",key,string.Empty)));

				string[] sAttrValue = nvcAttr.GetValues(key);
				if (sAttrValue != null) {
					foreach (string sValue in sAttrValue) {
						string[] sAttr = sValue.Split(',');
						lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("└{0}({1})",sAttr[2],sAttr[0]),string.Format("{0},{1}",key,sAttr[1])));
					}
				}
			}
		}
		// 選択した値を復元します。 
		string selectedValue = string.Format("{0},{1}",iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(Request.QueryString["attrseq"]));
		foreach (MobileListItem item in lstObjAttrCombo.Items) {
			if (item.Value == selectedValue) {
				lstObjAttrCombo.SelectedIndex = item.Index;
			}
		}
	}

	private DataSet GetObjAttrDataSet(string pUserSeq,string pUserCharNo,string sCategorySeq,string pObjType,string pAttachedType,bool pRecentNotLogin,bool pNeedCache,bool pOnlyUsed) {
		SeekCondition oSeekCondition = new SeekCondition();
		oSeekCondition.InitCondtition();
		oSeekCondition = sessionMan.ExtendSeekCondition(oSeekCondition,Request);
		oSeekCondition.unusedFlag = iBridUtil.GetStringValue(Request.QueryString["unused"]);
		oSeekCondition.rankType = iBridUtil.GetStringValue(Request.QueryString["ranktype"]);
		oSeekCondition.bookmarkFlag = iBridUtil.GetStringValue(Request.QueryString["bkm"]);
		// いいねした画像一覧
		if (sessionObj.currentAspx.Equals("ListCastPicMyLike.aspx")) {
			// いいねした画像のみ検索
			oSeekCondition.isLikedOnlyFlag = ViCommConst.FLAG_ON_STR;
			// やきもち防止設定も条件に含める
			oSeekCondition.containJealousyFlag = ViCommConst.FLAG_ON_STR;
		}

		// キャッシュにあればキャッシュから取得、なければDBアクセス
		string sViewMode = iBridUtil.GetStringValue(Request.QueryString["viewmode"]);
		string sUnAuth = iBridUtil.GetStringValue(Request.QueryString["unauth"]);

		string sKey = sessionObj.currentAspx + sViewMode;
		if (pOnlyUsed) {
			sKey += "_used";
		}

		if (oSeekCondition.hasExtend) {
			sKey += "_extend";
			if (sessionObj.ObjObjAttrCache.ContainsKey(sKey)) {
				sessionObj.ObjObjAttrCache.Remove(sKey);
			}
		}

		DataSet dsObjAttr = null;
		if (sessionObj.ObjObjAttrCache.ContainsKey(sKey)) {
			dsObjAttr = (DataSet)sessionObj.ObjObjAttrCache[sKey];

			string sAttrTypeSeq = iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]);
			string sAttrseq = iBridUtil.GetStringValue(Request.QueryString["attrseq"]);

			int iCacheCnt = 0;

			if (!string.IsNullOrEmpty(sAttrTypeSeq) && string.IsNullOrEmpty(sAttrseq)) {
				string sFillter = string.Format("OBJ_ATTR_TYPE_SEQ={0}",sAttrTypeSeq);
				DataRow[] oDrs = dsObjAttr.Tables[ATTR_TYPE_TABLE_NAME].Select(sFillter);
				if (oDrs.Length > 0) {
					iCacheCnt = Convert.ToInt32(oDrs[0]["CNT"]);
				}
			} else if (!string.IsNullOrEmpty(sAttrTypeSeq) && !string.IsNullOrEmpty(sAttrseq)) {
				string sFillter = string.Format("OBJ_ATTR_TYPE_SEQ={0} AND OBJ_ATTR_SEQ={1}",sAttrTypeSeq,sAttrseq);
				DataRow[] oDrs = dsObjAttr.Tables[ATTR_TABLE_NAME].Select(sFillter);
				if (oDrs.Length > 0) {
					iCacheCnt = Convert.ToInt32(oDrs[0]["CNT"]);
				}
			} else {
				iCacheCnt = Convert.ToInt32(dsObjAttr.Tables[ATTR_TYPE_TABLE_NAME].Compute("Sum(CNT)",""));
			}

			if ((sessionObj.totalRowCount != iCacheCnt)) {
				dsObjAttr = null;
				sessionObj.ObjObjAttrCache.Remove(sKey);
			}

		}

		if (dsObjAttr == null) {
			dsObjAttr = new DataSet();
			DataSet dsAttrType;
			DataSet dsAttr;

			if (pAttachedType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {

				using (CastBbsObj oCastBbs = new CastBbsObj()) {
					dsAttrType = oCastBbs.GetObjAttrTypeCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,sCategorySeq,pObjType,ViCommConst.FLAG_OFF_STR,pRecentNotLogin,true,pOnlyUsed,ViCommConst.FLAG_OFF_STR,oSeekCondition);
					dsAttr = oCastBbs.GetObjAttrCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,sCategorySeq,pObjType,ViCommConst.FLAG_OFF_STR,pRecentNotLogin,true,pOnlyUsed,oSeekCondition);
				}
			} else {
				using (CastPic oCastPic = new CastPic()) {
					dsAttrType = oCastPic.GetPicAttrTypeCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,sCategorySeq,ViCommConst.ATTACH_PIC_INDEX.ToString(),pRecentNotLogin,pOnlyUsed,oSeekCondition);
					dsAttr = oCastPic.GetPicAttrCount(sessionObj.site.siteCd,pUserSeq,pUserCharNo,sCategorySeq,ViCommConst.ATTACH_PIC_INDEX.ToString(),pRecentNotLogin,pOnlyUsed,oSeekCondition);
				}
			}
			dsAttrType.Tables[0].TableName = ATTR_TYPE_TABLE_NAME;
			dsAttr.Tables[0].TableName = ATTR_TABLE_NAME;
			dsObjAttr.Merge(dsAttrType.Tables[0]);
			dsObjAttr.Merge(dsAttr.Tables[0]);

			if (pNeedCache)
				sessionObj.ObjObjAttrCache.Add(sKey,dsObjAttr);
		}
		return dsObjAttr;
	}

	protected void CreateCastPicAttrComboBox() {
		this.SetControl();
		DataSet dsAttrType;
		DataSet dsAttr;

		using (CastPicAttrType oCastPicAttrType = new CastPicAttrType()) {
			dsAttrType = oCastPicAttrType.GetListForComboBox(sessionObj.site.siteCd);
		}

		using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
			dsAttr = oCastPicAttrTypeValue.GetListForComboBox(sessionObj.site.siteCd);
		}

		if (!IsAvailableService(ViCommConst.RELEASE_OBJ_ATTR_TYPE_HIDE_ALL)) {
			lstObjAttrCombo.Items.Add(new MobileListItem("■全て",","));
		}

		foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
			string sAttrTypeSeq = iBridUtil.GetStringValue(drAttrType["CAST_PIC_ATTR_TYPE_SEQ"]);
			string sAttrTypeNm = iBridUtil.GetStringValue(drAttrType["CAST_PIC_ATTR_TYPE_NM"]);
			lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("■{0}",sAttrTypeNm),string.Format("{0},{1}",sAttrTypeSeq,string.Empty)));

			DataRow[] oAttr = dsAttr.Tables[0].Select("CAST_PIC_ATTR_TYPE_SEQ = " + sAttrTypeSeq);

			foreach (DataRow drAttr in oAttr) {
				string sAttrSeq = iBridUtil.GetStringValue(drAttr["CAST_PIC_ATTR_SEQ"]);
				string sAttrNm = iBridUtil.GetStringValue(drAttr["CAST_PIC_ATTR_NM"]);
				lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("└{0}",sAttrNm),string.Format("{0},{1}",sAttrTypeSeq,sAttrSeq)));
			}
		}

		string sSelectedValue = string.Format("{0},{1}",iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(Request.QueryString["attrseq"]));

		foreach (MobileListItem item in lstObjAttrCombo.Items) {
			if (item.Value == sSelectedValue) {
				lstObjAttrCombo.SelectedIndex = item.Index;
			}
		}
	}

	protected void CreateCastMovieAttrComboBox() {
		this.SetControl();
		DataSet dsAttrType;
		DataSet dsAttr;

		using (CastMovieAttrType oCastMovieAttrType = new CastMovieAttrType()) {
			dsAttrType = oCastMovieAttrType.GetListForComboBox(sessionObj.site.siteCd);
		}

		using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
			dsAttr = oCastMovieAttrTypeValue.GetListForComboBox(sessionObj.site.siteCd);
		}

		if (!IsAvailableService(ViCommConst.RELEASE_OBJ_ATTR_TYPE_HIDE_ALL)) {
			lstObjAttrCombo.Items.Add(new MobileListItem("■全て",","));
		}

		foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
			string sAttrTypeSeq = iBridUtil.GetStringValue(drAttrType["CAST_MOVIE_ATTR_TYPE_SEQ"]);
			string sAttrTypeNm = iBridUtil.GetStringValue(drAttrType["CAST_MOVIE_ATTR_TYPE_NM"]);
			lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("■{0}",sAttrTypeNm),string.Format("{0},{1}",sAttrTypeSeq,string.Empty)));

			DataRow[] oAttr = dsAttr.Tables[0].Select("CAST_MOVIE_ATTR_TYPE_SEQ = " + sAttrTypeSeq);

			foreach (DataRow drAttr in oAttr) {
				string sAttrSeq = iBridUtil.GetStringValue(drAttr["CAST_MOVIE_ATTR_SEQ"]);
				string sAttrNm = iBridUtil.GetStringValue(drAttr["CAST_MOVIE_ATTR_NM"]);
				lstObjAttrCombo.Items.Add(new MobileListItem(string.Format("└{0}",sAttrNm),string.Format("{0},{1}",sAttrTypeSeq,sAttrSeq)));
			}
		}

		string sSelectedValue = string.Format("{0},{1}",iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(Request.QueryString["attrseq"]));

		foreach (MobileListItem item in lstObjAttrCombo.Items) {
			if (item.Value == sSelectedValue) {
				lstObjAttrCombo.SelectedIndex = item.Index;
			}
		}
	}
}
