﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: MobileMailManPage
--
--  Creation Date	: 2010.07.29
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


/// <summary>
/// メールの基本機能を提供する基底クラス
/// </summary>
public abstract class MobileMailManPage:MobileManPageBase {

	private iBMobileTextBox txtTitle;
	private string txtDoc;
	private iBMobileLabel lblErrorMsg;

	protected virtual iBMobileTextBox MailTitle {
		set {
			txtTitle = value;
		}
	}
	protected virtual string MailDoc {
		set {
			txtDoc = value;
		}
	}
	protected virtual iBMobileLabel ErrorMsg {
		set {
			lblErrorMsg = value;
		}
	}

	protected abstract void SetControl();

	protected bool Submit(string pMailDataSeq,bool pIsReturnMail,string pSuccessPageNo) {

		SetControl();

		bool bOk = true;
		bool bReleaseManMailUseMailer = false;
		bReleaseManMailUseMailer = IsAvailableService(ViCommConst.RELEASE_MAN_MAIL_USE_MAILER);

		bool bReleaseNonTitleByUesrMail = false;
		bReleaseNonTitleByUesrMail = IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL, 2);
		
		string sStayFlag = iBridUtil.GetStringValue(Request.QueryString["stay"]);
		
		int iPresentChargePoint = 0;
		if (sessionMan.userMan.mailData[pMailDataSeq].presentMailFlag != ViCommConst.FLAG_ON) {
			sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq = string.Empty;
		}
		if (!string.IsNullOrEmpty(sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq)) {
			using (PresentMailItem oPresentMailItem = new PresentMailItem()) {
				iPresentChargePoint = oPresentMailItem.GetChargePoint(sessionMan.site.siteCd,sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq);
			}
		}

		int iIdx;
		int.TryParse(iBridUtil.GetStringValue(Request.Params["rdoAttach"]),out iIdx);
		//両方使う場合ラジオボタンが4つになっているので、つじつまを合わせる
		if (IsAvailableService(ViCommConst.RELEASE_MAN_MAIL_USE_ALL,2)) {
			switch (iIdx) {
				case ViCommConst.ATTACH_PIC_INDEX:
				case ViCommConst.ATTACH_MOVIE_INDEX:
					bReleaseManMailUseMailer = false;
					break;
				case ViCommConst.ATTACH_MAN_USE_ALL_PIC_INDEX:
				case ViCommConst.ATTACH_MAN_USE_ALL_MOVIE_INDEX:
					bReleaseManMailUseMailer = true;
					break;
				default:
					break;
			}
		}
		lblErrorMsg.Text = string.Empty;

		if (bReleaseNonTitleByUesrMail) {
			txtTitle.Text = string.Empty;
		}

		if (txtTitle.Text.Equals(string.Empty) && !bReleaseNonTitleByUesrMail) {
			bOk = false;
			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Equals(string.Empty)) {
			bOk = false;
			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}
		
		int iDocLengthLimit;
		
		if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["doc_limit_length"]),out iDocLengthLimit)) {
			iDocLengthLimit = 1000;
		}

		if (txtDoc.Length > iDocLengthLimit) {
			bOk = false;
			int iOverLength = txtDoc.Length - iDocLengthLimit;
			lblErrorMsg.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iBridUtil.GetStringValue(iOverLength),iBridUtil.GetStringValue(iDocLengthLimit));
		}

		string sCastSeq = sessionMan.userMan.mailData[pMailDataSeq].rxUserSeq;
		string sCastCharNo = sessionMan.userMan.mailData[pMailDataSeq].rxUserCharNo;
		string sHandleNm = sessionMan.userMan.mailData[pMailDataSeq].rxUserNm;
		int iChargePoint;

		if (!CheckMailBalance(sCastSeq,sCastCharNo,out iChargePoint)) {
			//			bOk = false;
			//			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_POINT_LACK_MAIL));
		}

		if (iPresentChargePoint > 0) {
			int iLimitPoint = 0;
			if (sessionMan.site.usedChargeSettleFlag.Equals(ViCommConst.FLAG_ON)) {
				iLimitPoint = sessionMan.userMan.limitPoint;
			}

			if (!(sessionMan.userMan.balPoint + iLimitPoint >= iChargePoint + iPresentChargePoint)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_POINT_LACK_MAIL));
			}
		}

		string sNGWord;
		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			if (!IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2)) {
				if (sessionMan.ngWord.VaidateDoc(txtTitle.Text + txtDoc,out sNGWord) == false) {
					bOk = false;
					lblErrorMsg.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}

		bOk = bOk & CheckOther();

		if (bOk == false) {
			return bOk;
		} else {
			string sTitle = string.Empty;
			string sDoc = string.Empty;

			sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtTitle.Text) : txtTitle.Text));
			sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtDoc) : txtDoc));

			using (MailBox oMailBox = new MailBox()) {
				if (oMailBox.IsSameMailSent(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sCastSeq,sCastCharNo,sTitle,sDoc)) {
					lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAIL_SAME_SEND);
					return false;
				}
			}

			string sMailSeq = string.Empty;
			switch (iIdx) {
				case ViCommConst.ATTACH_PIC_INDEX:
				case ViCommConst.ATTACH_MAN_USE_ALL_PIC_INDEX:
					if (bReleaseManMailUseMailer) {
						using (MailBox oMailBox = new MailBox()) {
							if (pIsReturnMail) {
								sMailSeq = oMailBox.TxManToCastAttachedMailerReturnMail(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sCastSeq,sCastCharNo,sTitle,sDoc,ViCommConst.ATTACH_PIC_INDEX.ToString(),sessionMan.userMan.mailData[pMailDataSeq].returnMailSeq,sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq,out sessionMan.userMan.mailData[pMailDataSeq].objTempId,sessionMan.userMan.mailData[pMailDataSeq].quickResMailFlag);
							} else {
								sMailSeq = oMailBox.TxManToCastAttachedMailer(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sCastSeq,sCastCharNo,sTitle,sDoc,ViCommConst.ATTACH_PIC_INDEX.ToString(),sessionMan.userMan.mailData[pMailDataSeq].quickResMailFlag,sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq,out sessionMan.userMan.mailData[pMailDataSeq].objTempId);
							}
						}
						
						DeleteDraftMail(pMailDataSeq);

						if (pIsReturnMail) {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}",ViCommConst.SCR_MAN_PIC_RETURN_MAILER_COMPLITE,pMailDataSeq,sMailSeq)));
						} else {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}",ViCommConst.SCR_MAN_PIC_TX_MAILER_COMPLITE,pMailDataSeq,sMailSeq)));
						}
					} else {
						SetMailInfo(pMailDataSeq,pIsReturnMail);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListMailPic.aspx?data={0}&mailtype={1}",pMailDataSeq,ViCommConst.ATTACH_PIC_INDEX.ToString())));
					}
					break;

				case ViCommConst.ATTACH_MOVIE_INDEX:
				case ViCommConst.ATTACH_MAN_USE_ALL_MOVIE_INDEX:
					if (bReleaseManMailUseMailer) {
						using (MailBox oMailBox = new MailBox()) {
							if (pIsReturnMail) {
								sMailSeq = oMailBox.TxManToCastAttachedMailerReturnMail(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sCastSeq,sCastCharNo,sTitle,sDoc,ViCommConst.ATTACH_MOVIE_INDEX.ToString(),sessionMan.userMan.mailData[pMailDataSeq].returnMailSeq,sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq,out sessionMan.userMan.mailData[pMailDataSeq].objTempId,sessionMan.userMan.mailData[pMailDataSeq].quickResMailFlag);
							} else {
								sMailSeq = oMailBox.TxManToCastAttachedMailer(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sCastSeq,sCastCharNo,sTitle,sDoc,ViCommConst.ATTACH_MOVIE_INDEX.ToString(),sessionMan.userMan.mailData[pMailDataSeq].quickResMailFlag,sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq,out sessionMan.userMan.mailData[pMailDataSeq].objTempId);
							}
						}

						DeleteDraftMail(pMailDataSeq);

						if (pIsReturnMail) {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}",ViCommConst.SCR_MAN_MOVIE_RETURN_MAILER_COMPLITE,pMailDataSeq,sMailSeq)));
						} else {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}",ViCommConst.SCR_MAN_MOVIE_TX_MAILER_COMPLITE,pMailDataSeq,sMailSeq)));
						}
					} else {
						SetMailInfo(pMailDataSeq,pIsReturnMail);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListMailMovie.aspx?data={0}&mailtype={1}",pMailDataSeq,ViCommConst.ATTACH_MOVIE_INDEX.ToString())));
					}
					break;

				default:
					using (WebUsedLog oLog = new WebUsedLog()) {
						oLog.CreateWebUsedReport(
								sessionMan.userMan.siteCd,
								sessionMan.userMan.userSeq,
								ViCommConst.CHARGE_TX_MAIL,
								iChargePoint,
								sCastSeq,
								sCastCharNo,
								"");
						
						//プレゼント分のポイント消費をメールのポイント消費とは分ける
						if (sessionMan.userMan.mailData[pMailDataSeq].presentMailFlag == ViCommConst.FLAG_ON && !string.IsNullOrEmpty(sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq)) {
							oLog.CreateWebUsedReport(
								sessionMan.userMan.siteCd,
								sessionMan.userMan.userSeq,
								ViCommConst.CHARGE_PRESENT_MAIL,
								iPresentChargePoint,
								sCastSeq,
								sCastCharNo,
								"",
								0
							);
						}
					}
					using (MailBox oMailBox = new MailBox()) {
						if (pIsReturnMail == false) {
							sMailSeq = oMailBox.TxManToCastMail(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sCastSeq,sCastCharNo,sTitle,sDoc,null,null,null,sessionMan.userMan.mailData[pMailDataSeq].quickResMailFlag,sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq);
						} else {
							sMailSeq = oMailBox.TxManToCastReturnMail(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sCastSeq,sCastCharNo,sTitle,sDoc,null,null,null,sessionMan.userMan.mailData[pMailDataSeq].returnMailSeq,sessionMan.userMan.mailData[pMailDataSeq].presentMailItemSeq,sessionMan.userMan.mailData[pMailDataSeq].quickResMailFlag);
						}
					}

					DeleteDraftMail(pMailDataSeq);
					
					string sBingoQuery = string.Empty;
					int iCompliateFlag = ViCommConst.FLAG_OFF;
					int iGetNewBallFlag;
					int iGetOldBallFlag;
					int iGetCardBallFlag;
					int iGetBallNo;
					int iBingoBallFlag;
					string sResult = sessionMan.bingoCard.GetBingoBall(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN,sMailSeq,string.Empty,out iGetNewBallFlag,out iGetOldBallFlag,out iGetCardBallFlag,out iGetBallNo,out iCompliateFlag,out iBingoBallFlag);
					sBingoQuery = string.Format("&ballnew={0}&ballold={1}&cardball={2}&ballno={3}&comp={4}&result={5}&bingoball={6}",iGetNewBallFlag,iGetOldBallFlag,iGetCardBallFlag,iGetBallNo,iCompliateFlag,sResult,iBingoBallFlag);

					string sNextUrl = "DisplayDoc.aspx?doc=" + pSuccessPageNo + sBingoQuery + "&" + Request.QueryString + "&mseq=" + sMailSeq;

					using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
						oMailRequestLog.UpdateMailRequestLogMan(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sCastSeq,sCastCharNo,sMailSeq);
					}

					using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
						oMailAttackLogin.AddMailAttackLoginBonus(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sCastSeq,sCastCharNo,ViCommConst.ATTACH_NOTHING_INDEX.ToString());
					}

					using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
						oMailAttackNew.AddMailAttackNewBonus(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sCastSeq,sCastCharNo,ViCommConst.ATTACH_NOTHING_INDEX.ToString());
					}

					// 3分以内メール返信ボーナス履歴登録
					using (MailResBonusLog2 oMailResBonusLog2 = new MailResBonusLog2()) {
						string sMailResBonusResult = string.Empty;
						string sMailResBonusStatus = string.Empty;
						oMailResBonusLog2.MailResBonusLogMainte(
							sessionMan.site.siteCd,
							sessionMan.userMan.userSeq,
							sMailSeq,
							sCastSeq,
							null,
							out sMailResBonusResult,
							out sMailResBonusStatus
						);
					}

					if (sStayFlag.Equals(ViCommConst.FLAG_ON_STR)) {
						NameValueCollection oQeuryList = new NameValueCollection(sessionMan.requestQuery.QueryString);
						oQeuryList.Remove("ballnew");
						oQeuryList.Remove("ballold");
						oQeuryList.Remove("cardball");
						oQeuryList.Remove("ballno");
						oQeuryList.Remove("comp");
						oQeuryList.Remove("result");
						oQeuryList.Remove("bingoball");
						oQeuryList.Remove("data");
						oQeuryList.Remove("mseq");
						oQeuryList.Remove("listno");
						oQeuryList.Remove("appbonus");

						List<string> items = new List<String>();
						foreach (string name in oQeuryList)
							items.Add(string.Concat(name,"=",oQeuryList[name]));
						string sQuery = string.Join("&",items.ToArray());
						
						if (pIsReturnMail == false) {
							sNextUrl = string.Format("TxMail.aspx?{0}{1}&mseq={2}#btm",sQuery,sBingoQuery,sMailSeq);
						} else {
							sNextUrl = string.Format("ReturnMail.aspx?{0}{1}&mseq={2}#btm",sQuery,sBingoQuery,sMailSeq);
						}
					}

					if (iCompliateFlag.Equals(ViCommConst.FLAG_ON)) {
						sNextUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)));
					}

					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
					break;
			}
		}
		return bOk;
	}

	private void SetMailInfo(string pMailDataSeq,bool pIsReturnMail) {
		sessionMan.userMan.mailData[pMailDataSeq].mailTitle = txtTitle.Text;
		sessionMan.userMan.mailData[pMailDataSeq].mailDoc = txtDoc;
		sessionMan.userMan.mailData[pMailDataSeq].queryString = Request.QueryString.ToString();
		sessionMan.userMan.mailData[pMailDataSeq].isReturnMail = pIsReturnMail;
		sessionMan.userMan.mailData[pMailDataSeq].mailAttachType = iBridUtil.GetStringValue(Request.Params["rdoAttach"]);
	}


	protected bool CheckMailBalance(string pCastSeq,string pCastCharNo,out int pChargePoint) {
		bool bOk = false;
		int iIdx;
		int.TryParse(iBridUtil.GetStringValue(Request.Params["rdoAttach"]),out iIdx);
		switch (iIdx) {
			case ViCommConst.ATTACH_PIC_INDEX:
			case ViCommConst.ATTACH_MAN_USE_ALL_PIC_INDEX:
				bOk = sessionMan.CheckTxMailPicBalance(pCastSeq,pCastCharNo,out pChargePoint);
				break;
			case ViCommConst.ATTACH_MOVIE_INDEX:
			case ViCommConst.ATTACH_MAN_USE_ALL_MOVIE_INDEX:
				bOk = sessionMan.CheckTxMailMovieBalance(pCastSeq,pCastCharNo,out pChargePoint);
				break;
			default:
				bOk = sessionMan.CheckMailBalance(pCastSeq,pCastCharNo,out pChargePoint);
				break;
		}
		return bOk;
	}

	protected virtual bool CheckOther() {
		return true;
	}

	protected bool Confirm(string pMailDataSeq,bool pIsReturnMail) {

		SetControl();

		bool bOk = true;

		bool bReleaseNonTitleByUesrMail = false;
		bReleaseNonTitleByUesrMail = IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2);

		int iIdx;
		int.TryParse(iBridUtil.GetStringValue(Request.Params["rdoAttach"]),out iIdx);
		//両方使う場合ラジオボタンが4つになっているので、つじつまを合わせる

		lblErrorMsg.Text = string.Empty;

		if (bReleaseNonTitleByUesrMail) {
			txtTitle.Text = string.Empty;
		}

		if (txtTitle.Text.Equals(string.Empty) && !bReleaseNonTitleByUesrMail) {
			bOk = false;
			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Equals(string.Empty)) {
			bOk = false;
			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		int iDocLengthLimit;

		if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["doc_limit_length"]),out iDocLengthLimit)) {
			iDocLengthLimit = 1000;
		}

		if (txtDoc.Length > iDocLengthLimit) {
			bOk = false;
			int iOverLength = txtDoc.Length - iDocLengthLimit;
			lblErrorMsg.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iBridUtil.GetStringValue(iOverLength),iBridUtil.GetStringValue(iDocLengthLimit));
		}

		string sCastSeq = sessionMan.userMan.mailData[pMailDataSeq].rxUserSeq;
		string sCastCharNo = sessionMan.userMan.mailData[pMailDataSeq].rxUserCharNo;
		int iChargePoint;

		if (!CheckMailBalance(sCastSeq,sCastCharNo,out iChargePoint)) {
			//			bOk = false;
			//			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_POINT_LACK_MAIL));
		}

		string sNGWord;
		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			if (!IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2)) {
				if (sessionMan.ngWord.VaidateDoc(txtTitle.Text + txtDoc,out sNGWord) == false) {
					bOk = false;
					lblErrorMsg.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}

		if (bOk == false) {
			return bOk;
		} else {
			string sTitle = string.Empty;
			string sDoc = string.Empty;

			sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtTitle.Text) : txtTitle.Text));
			sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtDoc) : txtDoc));

			using (MailBox oMailBox = new MailBox()) {
				if (oMailBox.IsSameMailSent(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sCastSeq,sCastCharNo,sTitle,sDoc)) {
					lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAIL_SAME_SEND);
					return false;
				}
			}

			SetMailInfo(pMailDataSeq,pIsReturnMail);

			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"TxMailPreview.aspx?data=" + pMailDataSeq));
		}
		return bOk;
	}



	protected bool MainteDraftMail(string pMailDataSeq,bool pIsReturnMail) {

		SetControl();

		bool bOk = true;

		bool bReleaseNonTitleByUesrMail = false;
		bReleaseNonTitleByUesrMail = IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2);

		int iIdx;
		int.TryParse(iBridUtil.GetStringValue(Request.Params["rdoAttach"]),out iIdx);
		//両方使う場合ラジオボタンが4つになっているので、つじつまを合わせる

		lblErrorMsg.Text = string.Empty;

		if (bReleaseNonTitleByUesrMail) {
			txtTitle.Text = string.Empty;
		}

		string sCastSeq = sessionMan.userMan.mailData[pMailDataSeq].rxUserSeq;
		string sCastCharNo = sessionMan.userMan.mailData[pMailDataSeq].rxUserCharNo;

		string sNGWord;
		if (bOk) {
			int iDocLengthLimit;

			if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["doc_limit_length"]),out iDocLengthLimit)) {
				iDocLengthLimit = 1000;
			}

			if (txtDoc.Length > iDocLengthLimit) {
				bOk = false;
				int iOverLength = txtDoc.Length - iDocLengthLimit;
				lblErrorMsg.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iBridUtil.GetStringValue(iOverLength),iBridUtil.GetStringValue(iDocLengthLimit));
			}
			
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			if (!IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2)) {
				if (sessionMan.ngWord.VaidateDoc(txtTitle.Text + txtDoc,out sNGWord) == false) {
					bOk = false;
					lblErrorMsg.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}

		if (bOk == false) {
			return bOk;
		} else {
			string sTitle = string.Empty;
			string sDoc = string.Empty;

			sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtTitle.Text) : txtTitle.Text));
			sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtDoc) : txtDoc));

			using (DraftMail oDraftMail = new DraftMail()) {
				oDraftMail.MainteDraftMail(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sessionMan.sexCd,
					sCastSeq,
					sCastCharNo,
					iBridUtil.GetStringValue(sessionMan.userMan.mailData[pMailDataSeq].draftMailSeq),
					sTitle,
					sDoc,
					iIdx.ToString(),
					string.Empty,
					pIsReturnMail == true ? ViCommConst.FLAG_ON :ViCommConst.FLAG_OFF,
					iBridUtil.GetStringValue(sessionMan.userMan.mailData[pMailDataSeq].returnMailSeq)
				);
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListDraftMail.aspx"));
		}
		return bOk;
	}

	private void DeleteDraftMail(string pMailDataSeq) {
		string sDraftMailSeq = iBridUtil.GetStringValue(sessionMan.userMan.mailData[pMailDataSeq].draftMailSeq);
		if (!string.IsNullOrEmpty(sDraftMailSeq)) {
			using (DraftMail oDraftMail = new DraftMail()) {
				oDraftMail.DeleteDraftMail(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,new string[] {sDraftMailSeq});
			}
		}
	}

	protected bool PresentSelect(string pMailDataSeq,bool pIsReturnMail) {

		SetControl();

		bool bOk = true;

		bool bReleaseNonTitleByUesrMail = false;
		bReleaseNonTitleByUesrMail = IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2);

		int iIdx;
		int.TryParse(iBridUtil.GetStringValue(Request.Params["rdoAttach"]),out iIdx);
		//両方使う場合ラジオボタンが4つになっているので、つじつまを合わせる

		lblErrorMsg.Text = string.Empty;

		if (bReleaseNonTitleByUesrMail) {
			txtTitle.Text = string.Empty;
		}

		if (txtTitle.Text.Equals(string.Empty) && !bReleaseNonTitleByUesrMail) {
			bOk = false;
			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Equals(string.Empty)) {
			bOk = false;
			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		int iDocLengthLimit;

		if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["doc_limit_length"]),out iDocLengthLimit)) {
			iDocLengthLimit = 1000;
		}

		if (txtDoc.Length > iDocLengthLimit) {
			bOk = false;
			int iOverLength = txtDoc.Length - iDocLengthLimit;
			lblErrorMsg.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iBridUtil.GetStringValue(iOverLength),iBridUtil.GetStringValue(iDocLengthLimit));
		}

		string sCastSeq = sessionMan.userMan.mailData[pMailDataSeq].rxUserSeq;
		string sCastCharNo = sessionMan.userMan.mailData[pMailDataSeq].rxUserCharNo;
		int iChargePoint;

		if (!CheckMailBalance(sCastSeq,sCastCharNo,out iChargePoint)) {
			//			bOk = false;
			//			lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_POINT_LACK_MAIL));
		}

		string sNGWord;
		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			if (!IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2)) {
				if (sessionMan.ngWord.VaidateDoc(txtTitle.Text + txtDoc,out sNGWord) == false) {
					bOk = false;
					lblErrorMsg.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}

		if (bOk == false) {
			return bOk;
		} else {
			string sTitle = string.Empty;
			string sDoc = string.Empty;

			sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtTitle.Text) : txtTitle.Text));
			sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(txtDoc) : txtDoc));

			using (MailBox oMailBox = new MailBox()) {
				if (oMailBox.IsSameMailSent(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sCastSeq,sCastCharNo,sTitle,sDoc)) {
					lblErrorMsg.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAIL_SAME_SEND);
					return false;
				}
			}

			SetMailInfo(pMailDataSeq,pIsReturnMail);

			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListPresentMailItem.aspx?data=" + pMailDataSeq + "&sort=" + iBridUtil.GetStringValue(Request.Form["sort"])));
		}
		return bOk;
	}
}
