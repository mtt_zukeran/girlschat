﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 
--	Progaram ID		: MobileManPageBase
--
--  Creation Date	: 2010.07.29
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


/// <summary>
/// 男性画面の基本機能を提供する抽象基底クラス 
/// </summary>
public abstract class MobileManPageBase:MobilePageBase {

	protected virtual bool CheckSiteUseStatus {
		get {
			return true;
		}
	}

	protected virtual SessionMan sessionMan {
		get {
			object oObjs = Session["objs"];
			if (oObjs is SessionMan) {
				return (SessionMan)oObjs;
			} else {
				string sHost = iBridUtil.GetStringValue(Request.QueryString["host"]);
				if (sHost.Equals(string.Empty)) {
					sHost = Request.Url.Host;
					string sTestEnv = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestEnv"]);
					if (sTestEnv.Equals("1")) {
						sHost = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TestLocation"]);
					}
				}
				SessionHelper oHelper = new SessionHelper(ViCommConst.MAN,Mobile.GetCarrier(HttpContext.Current.Request.Browser.Browser),HttpContext.Current.Request.Browser.Id,"","");
				SessionObjs oNewSession;
				oHelper.CreateSessionInfo(sHost,Session.SessionID,false,false,out oNewSession);
				return (SessionMan)oNewSession;
			}
		}
	}

	protected override void OnLoad(EventArgs e) {
		string sUserSeq = string.Empty;
		string sPassword = string.Empty;
		string sSexCd = ViCommConst.MAN;
		bool bCheck = false;
		
		if (sessionMan.logined) {
			sUserSeq = sessionMan.userMan.userSeq;
			sPassword = sessionMan.userMan.loginPassword;
			sSexCd = ViCommConst.MAN;
			bCheck = true;
		} else if (sessionMan.IsImpersonated) {
			sUserSeq = sessionMan.originalWomanObj.userWoman.userSeq;
			sPassword = sessionMan.originalWomanObj.userWoman.loginPassword;
			sSexCd = ViCommConst.OPERATOR;
			bCheck = true;
		}
		
		if (bCheck) {
			CheckCookieData(sUserSeq,sPassword,sSexCd);
		}
		
		base.OnLoad(e);
		if (!this.IsPostBack) {
			/* 初期処理 */
			CheckInvalidStatus();
		} else {
			if (this.IsPostAction(ViCommConst.BUTTON_EXTSEARCH)) {
				this.ListSearchAction();
			}
		}
	}

	/// <summary>
	/// 一覧下部の検索ボタン押下時処理 
	/// </summary>
	protected virtual void ListSearchAction() {


		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		foreach (string sKey in this.Request.QueryString) {
			if (string.IsNullOrEmpty(sKey))
				continue;

			if (sKey.StartsWith("ext_"))
				continue;
			if (sKey.Equals("pageno"))
				continue;
			oParameters.Add(sKey,HttpUtility.UrlDecode(this.Request.QueryString[sKey]));
		}


		UrlBuilder oUrlBuilder = new UrlBuilder(SessionObjs.Current.GetNavigateUrl(ViCommPrograms.GetCurrentAspx()),oParameters);

		IList<string> oRemoveKeyList = new List<string>();
		oRemoveKeyList.Add("attrtypeseq");
		oRemoveKeyList.Add("attrseq");
		oRemoveKeyList.Add("sorttype");
		foreach (string sRemoveKey in oRemoveKeyList) {
			if (oUrlBuilder.Parameters.ContainsKey(sRemoveKey)) {
				oUrlBuilder.Parameters.Remove(sRemoveKey);
			}
		}
		oUrlBuilder.Parameters.Add("attrtypeseq",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["attrtypeseq"])));
		oUrlBuilder.Parameters.Add("attrseq",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["attrseq"])));
		oUrlBuilder.Parameters.Add("sorttype",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["sorttype"])));
		oUrlBuilder.Parameters.Add("ext_handle",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_handle"])));
		oUrlBuilder.Parameters.Add("ext_agerange",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_agerange"])));
		oUrlBuilder.Parameters.Add("ext_userstatus",HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form["ext_userstatus"])));
		foreach (string sKey in this.Request.Form.AllKeys) {
			if (!sKey.StartsWith("ext_item"))
				continue;
			oUrlBuilder.Parameters.Add(sKey,HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.Form[sKey])));
		}

		RedirectToMobilePage(oUrlBuilder.ToString(true));
	}

	protected virtual void CheckRefuse(string pUserSeq,string pUserCharNo) {
		//相手の拒否リストに載っている場合、リダイレクトさせる。 
		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(
					sessionMan.site.siteCd,
					pUserSeq,
					pUserCharNo,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo)) {
				RedirectToDisplayDoc(ViCommConst.ERR_REFUSE_MAN);
			}
		}
	}

	private void CheckInvalidStatus() {
		if (sessionMan.logined) {
			// 初回Callにより電話番号重複が発覚した場合ログイン後に禁止状態となる。 
			if (sessionMan.userMan.userStatus.Equals(ViCommConst.USER_MAN_STOP)) {
				if (!Request.Path.Contains("DisplayDoc.aspx") || !iBridUtil.GetStringValue(Request.QueryString["doc"]).Equals(ViCommConst.ERR_FRAME_DUPLICATE_TEL)) {
					RedirectToDisplayDoc(ViCommConst.ERR_FRAME_DUPLICATE_TEL);
				}
			}

			string[] sSplitUrl = Request.Path.Split('/');
			string sCurrentAspx = sSplitUrl[sSplitUrl.Length - 1];

			if (CheckSiteUseStatus) {
				if (sessionMan.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY)) {
					switch (sessionMan.currentProgramId) {
						case "CCheckComplite.aspx":
						case "PaymentCCheck.aspx":
						case "PaymentCVSDirect.aspx":
						case "PaymentCreditQuick.aspx":
						case "PaymentCreditZero.aspx":
						case "PaymentCreditZeroAuthority.aspx":
						case "PaymentCreditZeroQuick.aspx":
						case "PaymentCreditZeroQuickSelect.aspx":
						case "PaymentEdyZero.aspx":
						case "PaymentGMoney.aspx":
						case "PaymentGigaPoint.aspx":
						case "PaymentSecurityMoney.aspx":
						case "PostEdyZero.aspx":
						case "PostGigaPoint.aspx":
						case "MailForm03.aspx":
						case "Profile.aspx":
						case "DisplayDoc.aspx":
						case "ListGetAspAd01.aspx":
						case "ListPickup25.aspx":
						case "ListPickup30.aspx":
						case "ListPickup01.aspx":
						case "ListCreditAutoSettle.aspx":
						case "ListCreditAutoSettle01.aspx":
						case "ListCreditAutoSettleStatus.aspx":
						case "PaymentCreditZeroAuto.aspx":
						case "PaymentCreditZeroVoice.aspx":
						case "PaymentCreditZeroVoiceAuto.aspx":
						case "PaymentCreditZeroVoiceAuth.aspx":
						case "PaymentCreditZeroQuickAuto.aspx":
						case "CancelCreditAutoSettle.aspx":
							break;
						default:
							RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyGameUserProfile.aspx"));
							break;
					}
				}
			}
			if (sessionMan.userMan.handleNmResettingFlag == 1) {
				if (!(sCurrentAspx.Equals("ModifyUserProfile.aspx") || sCurrentAspx.Equals("ModifyUserHandleNm.aspx"))) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserHandleNm.aspx"));
				}
			}
			if (sessionMan.userMan.bulkImpNonUseFlag == 1) {
				if (!(sCurrentAspx.Equals("UserTop.aspx") || sCurrentAspx.Equals("ModifyUserProfile.aspx") || sCurrentAspx.Equals("ModifyUserHandleNm.aspx"))) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("UserTop.aspx"));
				}
			}
			if (!sCurrentAspx.Equals("Profile.aspx")) {
				if (sCurrentAspx.Equals("DisplayDoc.aspx") || sCurrentAspx.Equals("GameDisplayDoc.aspx")) {
					sessionMan.markingReferer = string.Format("{0}?doc={1}",sCurrentAspx,iBridUtil.GetStringValue(Request.QueryString["doc"]));
				} else if (!iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("")) {
					sessionMan.markingReferer = string.Format("{0}?scrid={1}",sCurrentAspx,iBridUtil.GetStringValue(Request.QueryString["scrid"]));
				} else {
					sessionMan.markingReferer = sCurrentAspx;
				}
			}
			if (sCurrentAspx.Equals("DisplayDoc.aspx")) {
				if (iBridUtil.GetStringValue(Request.QueryString["doc"]) == "016") {
					sessionMan.payGameFlag = ViCommConst.FLAG_OFF_STR;
				} else if (iBridUtil.GetStringValue(Request.QueryString["doc"]) == PwViCommConst.SCR_GAME_MAN_ADD_POINT_SELECT) {
					sessionMan.payGameFlag = ViCommConst.FLAG_ON_STR;
				}
			}
			string sPayGameFlag = iBridUtil.GetStringValue(this.Request.QueryString["pay_game_flag"]);
			if (sPayGameFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sessionMan.payGameFlag = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	protected DataSet GetPackDataSet(string pSettleType,int pMinSalesAmt) {
		DataSet ds;
		using (Pack oPack = new Pack()) {
			using (UserMan oUserMan = new UserMan()) {
				oUserMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
				if (oUserMan.totalReceiptAmt == 0) {
					ds = oPack.GetList(sessionMan.site.siteCd,pSettleType,pMinSalesAmt,sessionMan.userMan.userSeq);
				} else {
					ds = oPack.GetNonFirstList(sessionMan.site.siteCd,pSettleType,pMinSalesAmt,sessionMan.userMan.userSeq);
				}

			}
			oPack.CalcServicePoint(sessionMan.userMan.userSeq,ds);
		}

		return ds;
	}

	protected DataSet GetAfterPackDataSet(int pCreditBalance) {
		DataSet ds;
		using (Pack oPack = new Pack()) {
			using (UserMan oUserMan = new UserMan()) {
				oUserMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
				if (oUserMan.totalReceiptAmt == 0) {
					ds = oPack.GetAfterPackList(sessionMan.site.siteCd,pCreditBalance,sessionMan.userMan.userSeq);
				} else {
					ds = oPack.GetAfterNonFirstPackList(sessionMan.site.siteCd,pCreditBalance,sessionMan.userMan.userSeq);
				}

			}
			oPack.CalcServicePoint(sessionMan.userMan.userSeq,ds);
		}
		return ds;
	}
}
