﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会話動画
--	Progaram ID		: TalkMovie
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using ViComm;

[System.Serializable]
public class TalkMovie:DbSession {

	public TalkMovie() {
	}

	public DataSet GetPartList(string pSiteCd,string pMovieSeq) {
		DataSet ds;
		try{
			conn = DbConnect("TalkMovie.GetPartList");
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,MOVIE_SEQ,MOVIE_PART_NO ";
			
			string sSql = "SELECT		" +
								"MOVIE_SEQ			," +
								"MOVIE_TITLE		," +
								"MOVIE_PART_NO		," +
								"HTML_DOC AS MOVIE_DOC," +
								"PART_TITLE			," +
								"START_POS_SEC		," +
								"PLAY_SEC			," +
								"SITE_CD			," +
								"USER_SEQ			," +
								"USER_CHAR_NO		," +
								"CRYPT_VALUE		," +
								"LOGIN_ID			," +
								"HANDLE_NM			," +
								"ACT_CATEGORY_IDX	," +
								"SMALL_PHOTO_IMG_PATH," +
								"PHOTO_IMG_PATH		," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							"FROM " +
								" VW_TALK_PART_MOVIE01 " +
							"WHERE " +
								" SITE_CD	= :SITE_CD	AND " +
								" MOVIE_SEQ	= :MOVIE_SEQ " + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TALK_PART_MOVIE01");
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public DataSet GetCastMovieList(string pSiteCd,string pCastSeq) {
		DataSet ds;
		try{
			conn = DbConnect("TalkMovie.GetCastMovieList");
			ds = new DataSet();

			string sSql = "SELECT " +
								"MOVIE_SEQ	," +
								"MOVIE_TITLE," +
								"UPLOAD_DATE " +
							"FROM " +
								" T_TALK_MOVIE " +
							"WHERE " +
								" SITE_CD		= :SITE_CD	AND " +
								" USER_SEQ		= :USER_SEQ	AND " +
								" OBJ_NOT_APPROVE_FLAG	= 0			AND " +
								" OBJ_NOT_PUBLISH_FLAG	= 0 " +
								"ORDER BY " +
								"SITE_CD,MOVIE_SEQ DESC ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pCastSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TALK_MOVIE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCastMovieListByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,string pPickupFlag) {
		DataSet ds;
		try{
			conn = DbConnect("TalkMovie.GetCastMovieListByLoginId");
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,MOVIE_SEQ DESC ";
			
			string sSql = "SELECT " +
								"MOVIE_SEQ			," +
								"MOVIE_TITLE		," +
								"UPLOAD_DATE		," +
								"HTML_DOC	AS MOVIE_DOC ," +
								"CHARGE_POINT		," +
								"CRYPT_VALUE		," +
								"HANDLE_NM			," +
								"COMMENT_LIST		," +
								"LOGIN_ID			," +
								"ACT_CATEGORY_IDX	," +
								"SMALL_PHOTO_IMG_PATH	," +
								"PHOTO_IMG_PATH		," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							"FROM " +
								" VW_TALK_MOVIE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pLoginId,pUserCharNo,ViCommConst.FLAG_OFF,ViCommConst.FLAG_OFF,pPickupFlag,ref sWhere);

			sSql += sWhere + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TALK_MOVIE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pLoginId,string pUserCharNo,int pNotApproveFlag,int pNotPublishFlag,string pPickupFlag,ref string pWhere) {
		ArrayList list = new ArrayList();

		iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pLoginId.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		if (!pUserCharNo.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		}

		iBridCommLib.SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
		list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));

		iBridCommLib.SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
		list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));

		if (!pPickupFlag.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PICKUP_FLAG = :PICKUP_FLAG",ref pWhere);
			list.Add(new OracleParameter("PICKUP_FLAG",int.Parse(pPickupFlag)));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetMovieCharge(string pSiteCd,string pMovieSeq) {
		DataSet ds;
		DataRow dr;
		int iChargePoint = 0;
		try{
			conn = DbConnect("TalkMovie.GetMovieCharge");
			ds = new DataSet();

			string sSql = "SELECT CHARGE_POINT FROM T_TALK_MOVIE WHERE SITE_CD = :SITE_CD AND MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TALK_MOVIE");
					if (ds.Tables["T_TALK_MOVIE"].Rows.Count != 0) {
						dr = ds.Tables["T_TALK_MOVIE"].Rows[0];
						iChargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iChargePoint;
	}

	public int GetRandomMovie(string pSiteCd,out string pCastSeq,out string pCastCharNo,out string pRnum) {
		int iRecCnt = 0;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_RANDOM_MOVIE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRNUM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			iRecCnt = db.GetIntValue("PRECORD_COUNT");
			pCastSeq = db.GetStringValue("PUSER_SEQ");
			pCastCharNo = db.GetStringValue("PUSER_CHAR_NO");
			pRnum = db.GetStringValue("PRNUM");
		}
		return iRecCnt;
	}
}