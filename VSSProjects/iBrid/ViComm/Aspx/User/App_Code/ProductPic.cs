﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductPic : ProductBase {
	public ProductPic() : base() { }
	public ProductPic(SessionObjs pSessionObj) : base(pSessionObj) { }
	
	public DataSet GetListByType(string pSiteCd,string pProductAgentCd,string pProductSeq,string pProductPicType){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT																						");
		oSqlBuilder.AppendLine(" 	SITE_CD				,                                                                   ");
		oSqlBuilder.AppendLine(" 	PRODUCT_AGENT_CD	,                                                                   ");
		oSqlBuilder.AppendLine(" 	PRODUCT_SEQ			,                                                                   ");
		oSqlBuilder.AppendLine(" 	OBJ_SEQ				,                                                                   ");
		oSqlBuilder.AppendLine(" 	PRODUCT_PIC_TYPE	,                                                                   ");
		oSqlBuilder.AppendLine(" 	DISPLAY_POSITION	,                                                                   ");
		oSqlBuilder.AppendLine(" 	GET_PRODUCT_IMG_PATH			                                                        ");
		oSqlBuilder.AppendLine(" 		(VW_PRODUCT_PIC00.SITE_CD,VW_PRODUCT_PIC00.PRODUCT_ID,VW_PRODUCT_PIC00.OBJ_SEQ)     ");
		oSqlBuilder.AppendLine(" 			AS	PRODUCT_PIC_PATH			,                                               ");
		oSqlBuilder.AppendLine(" 	GET_PRODUCT_IMG_PATH_BLUR		                                                        ");
		oSqlBuilder.AppendLine(" 		(VW_PRODUCT_PIC00.SITE_CD,VW_PRODUCT_PIC00.PRODUCT_ID,VW_PRODUCT_PIC00.OBJ_SEQ)     ");
		oSqlBuilder.AppendLine(" 			AS	PRODUCT_PIC_BLUR_PATH		,                                               ");
		oSqlBuilder.AppendLine(" 	GET_PRODUCT_IMG_PATH_S			                                                        ");
		oSqlBuilder.AppendLine(" 		(VW_PRODUCT_PIC00.SITE_CD,VW_PRODUCT_PIC00.PRODUCT_ID,VW_PRODUCT_PIC00.OBJ_SEQ)     ");
		oSqlBuilder.AppendLine(" 			AS	PRODUCT_PIC_S_PATH			,                                               ");
		oSqlBuilder.AppendLine(" 	GET_PRODUCT_IMG_PATH_S_BLUR		                                                        ");
		oSqlBuilder.AppendLine(" 		(VW_PRODUCT_PIC00.SITE_CD,VW_PRODUCT_PIC00.PRODUCT_ID,VW_PRODUCT_PIC00.OBJ_SEQ)     ");
		oSqlBuilder.AppendLine(" 			AS	PRODUCT_PIC_S_BLUR_PATH		,                                               ");
		oSqlBuilder.AppendLine(" 	GET_PRODUCT_IMG_PATH_ORIGINAL	                                                        ");
		oSqlBuilder.AppendLine(" 		(VW_PRODUCT_PIC00.SITE_CD,VW_PRODUCT_PIC00.PRODUCT_ID,VW_PRODUCT_PIC00.OBJ_SEQ)     ");
		oSqlBuilder.AppendLine(" 			AS	PRODUCT_PIC_ORIGINAL_PATH		                                            ");
		oSqlBuilder.AppendLine(" FROM                                                                                       ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_PIC00		                                                                ");
		oSqlBuilder.AppendLine(" WHERE                                                                                      ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_PIC00.SITE_CD     		= :SITE_CD				AND                      	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_PIC00.PRODUCT_AGENT_CD  	= :PRODUCT_AGENT_CD     AND                         ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_PIC00.PRODUCT_SEQ       	= :PRODUCT_SEQ          AND                       	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_PIC00.PRODUCT_PIC_TYPE  	= :PRODUCT_PIC_TYPE                                 ");
		oSqlBuilder.AppendLine(" ORDER BY                                                                          			");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_PIC00.DISPLAY_POSITION                                                       ");

		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD", pSiteCd);
				this.cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
				this.cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
				this.cmd.Parameters.Add(":PRODUCT_PIC_TYPE", pProductPicType);

				using (this.da = new OracleDataAdapter(this.cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;
	}


	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(ProductSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT	");
		oSqlBuilder.AppendLine(base.CreateCountFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oWhereParams);

				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			this.conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionBySeq(string pProductSeq) {
		ProductSeekCondition oCondtion = new ProductSeekCondition();
		oCondtion.ProductSeq = pProductSeq;
		return this.GetPageCollectionBase(oCondtion, 0, 0);
	}
	public DataSet GetPageCollection(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion, pPageNo, pRecPerPage);
	}
	private DataSet GetPageCollectionBase(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {

		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.Append(base.CreateFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where		
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresionBase(pCondtion);

		return ExecutePageCollection(oSqlBuilder.ToString(), pCondtion, oWhereParams, sSortExpression, iStartIndex, pRecPerPage);
	}
	
	private string CreateFrom(ProductSeekCondition pCondition){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine(base.CreateFromBase(pCondition));
		return oSqlBuilder.ToString();
	}

	private OracleParameter[] CreateWhere(ProductSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 商品共通のWHERE条件を作成
		oParamList.AddRange(base.CreateWhereBase(pCondition, ref pWhereClause));
		
		// 公開条件：コンテンツ写真が登録されていること
		StringBuilder oSubSqlBuilder = new StringBuilder();
		oSubSqlBuilder.AppendLine(" EXISTS(							");
		oSubSqlBuilder.AppendLine("		SELECT 1 FROM T_PRODUCT_PIC	");
		oSubSqlBuilder.AppendLine("		WHERE						");
		oSubSqlBuilder.AppendLine("			T_PRODUCT_PIC.SITE_CD			= VW_PRODUCT00.SITE_CD			AND ");
		oSubSqlBuilder.AppendLine("			T_PRODUCT_PIC.PRODUCT_AGENT_CD	= VW_PRODUCT00.PRODUCT_AGENT_CD	AND ");
		oSubSqlBuilder.AppendLine("			T_PRODUCT_PIC.PRODUCT_SEQ		= VW_PRODUCT00.PRODUCT_SEQ		AND ");
		oSubSqlBuilder.AppendLine("			T_PRODUCT_PIC.PRODUCT_PIC_TYPE	= :PRODUCT_PIC_TYPE					");
		oSubSqlBuilder.AppendLine(" )");
		SysPrograms.SqlAppendWhere(oSubSqlBuilder.ToString(), ref pWhereClause);
		oParamList.Add(new OracleParameter(":PRODUCT_PIC_TYPE", ViCommConst.ProductPicType.CONTENTS));
		
		

		return oParamList.ToArray();
	}
	#endregion ========================================================================================================

	protected override string GetProductType(ProductSeekCondition pCondition) {
		return ProductHelper.GetPicProductType(pCondition.AdultFlag);
	}
}
