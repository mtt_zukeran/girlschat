/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: Session管理

--	Progaram ID		: WebSession
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Collections.Specialized;
using iBridCommLib;

[System.Serializable]
public class WebSession:DbSession {

	public WebSession() {
	}

	public void StartUpSystem(string pMachineNm){
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("STARTUP_SYSTEM");
			db.ProcedureInParm("PSESSION_OWNER_NM",DbSession.DbType.VARCHAR2,pMachineNm);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void StartSession(string pSessionId,string pMachineNm) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("START_SESSION");
			db.ProcedureInParm("PWEB_SESSION_ID",DbSession.DbType.VARCHAR2,pSessionId);
			db.ProcedureInParm("PSESSION_OWNER_NM",DbSession.DbType.VARCHAR2,pMachineNm);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void EndSession(string pSessionId) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("END_SESSION");
			db.ProcedureInParm("PWEB_SESSION_ID",DbSession.DbType.VARCHAR2,pSessionId);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}


	public void VoteSession(string pSessionId) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("VOTE_SESSION");
			db.ProcedureInParm("PWEB_SESSION_ID", DbSession.DbType.VARCHAR2, pSessionId);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
