﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ＷＥＢサービス利用明細
--	Progaram ID		: WebUsedLog
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using MobileLib;
using ViComm;

[System.Serializable]
public class WebUsedLog:DbSession {

	public WebUsedLog() {
	}


	public void CreateWebUsedReport(string pSiteCd,string pUserSeq,string pChargeType,int pChargePoint,string pCastUserSeq,string pCastCharNo,string pChargeSupplement) {
		CreateWebUsedReport(pSiteCd,pUserSeq,pChargeType,pChargePoint,pCastUserSeq,pCastCharNo,pChargeSupplement,-1);
	}

	public void CreateWebUsedReport(string pSiteCd,string pUserSeq,string pChargeType,int pChargePoint,string pCastUserSeq,string pCastCharNo,string pChargeSupplement,int pPaymentAmt) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CREATE_WEB_USED_REPORT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,pChargeType);
			db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,pChargePoint);
			db.ProcedureInParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("PCHARGE_SUPPLEMENT",DbSession.DbType.VARCHAR2,pChargeSupplement);
			db.ProcedureInParm("PWITHOUT_COMMIT",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PPAYMENT_AMT",DbSession.DbType.NUMBER,pPaymentAmt);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
