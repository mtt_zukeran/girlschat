﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サービス利用明細
--	Progaram ID		: UsedLog
--
--  Creation Date	: 2010.12.21
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Text;
using MobileLib;
using ViComm;

[System.Serializable]
public class UsedLog:DbSession {

	public UsedLog() {
	}

	public string GetPaymentPointByTalkSeq(string pTalkSeq) {
		DataSet ds;
		DataRow dr;
		string sPaymentPoint = "0";

		StringBuilder sSql = new StringBuilder();
		sSql.AppendLine("SELECT	");
		sSql.AppendLine("	PAYMENT_POINT	");
		sSql.AppendLine("FROM	");
		sSql.AppendLine("	T_USED_LOG		");
		sSql.AppendLine("WHERE	");
		sSql.AppendLine("	TALK_SEQ	= :TALK_SEQ	");

		try {
			conn = DbConnect("UsedLog.GetPaymentPointByTalkSeq");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("TALK_SEQ",pTalkSeq);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					sPaymentPoint = dr["PAYMENT_POINT"].ToString();
				}
			}
		} finally {
			conn.Close();
		}
		return sPaymentPoint;
	}

	public string GetPaymentAmtByTalkSeq(string pTalkSeq) {
		DataSet ds;
		DataRow dr;
		string sPaymentAmt = "0";

		StringBuilder sSql = new StringBuilder();
		sSql.AppendLine("SELECT	");
		sSql.AppendLine("	PAYMENT_AMT	");
		sSql.AppendLine("FROM	");
		sSql.AppendLine("	T_USED_LOG		");
		sSql.AppendLine("WHERE	");
		sSql.AppendLine("	TALK_SEQ	= :TALK_SEQ	");

		try {
			conn = DbConnect("UsedLog.GetPaymentAmtByTalkSeq");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("TALK_SEQ",pTalkSeq);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					sPaymentAmt = dr["PAYMENT_AMT"].ToString();
				}
			}
		} finally {
			conn.Close();
		}
		return sPaymentAmt;
	}

	/// <summary>
	/// 一定期間内の通話履歴を取得する
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pUserCharNo"></param>
	/// <param name="pFromDate"></param>
	/// <returns></returns>
	public DataSet GetTermTalkLog(string pSiteCd,string pUserSeq,string pUserCharNo,string pFromDate) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	UL.IVP_REQUEST_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_IVP_REQUEST IR");
		oSqlBuilder.AppendLine("	INNER JOIN T_USED_LOG UL ON UL.IVP_REQUEST_SEQ = IR.IVP_REQUEST_SEQ");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	IR.CAST_SITE_CD = :CAST_SITE_CD");
		oSqlBuilder.AppendLine("	AND IR.CAST_USER_SEQ = :CAST_USER_SEQ");
		oSqlBuilder.AppendLine("	AND IR.CAST_CHAR_NO = :CAST_CHAR_NO");
		oSqlBuilder.AppendLine("	AND UL.CHARGE_SEC > 0");
		oSqlBuilder.AppendLine("	AND UL.CHARGE_END_DATE >= TO_DATE(:CHARGE_END_DATE, 'YYYY-MM-DD HH24:MI:SS')");

		oParamList.Add(new OracleParameter(":CAST_SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":CHARGE_END_DATE",pFromDate));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		return ds;
	}
}
