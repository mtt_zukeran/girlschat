﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ピックアップオブジェ 投票数
--	Progaram ID		: PickupObjsVoteCount
--
--  Creation Date	: 2013.05.28
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class PickupObjsVoteCount:DbSession {
	public PickupObjsVoteCount() {
	}

	public DataSet GetOne(string pSiteCd,string pSelfUserSeq,string pSelfUserCharNo,string pPickupId,string pObjSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	PICKUP_ID						,	");
		oSqlBuilder.AppendLine("	OBJ_SEQ AS PIC_SEQ				,	");
		oSqlBuilder.AppendLine("	VOTE_COUNT						,	");
		oSqlBuilder.AppendLine("	PICKUP_OBJS_VOTE_RANK_NO		,	");
		oSqlBuilder.AppendLine("	0 AS TODAY_VOTED_FLAG				");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_PICKUP_OBJS_VOTE_COUNT01			");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	PICKUP_ID	= :PICKUP_ID		AND	");
		oSqlBuilder.AppendLine("	OBJ_SEQ		= :OBJ_SEQ				");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":PICKUP_ID",pPickupId));
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			using (PickupObjsVote oPickupObjsVote = new PickupObjsVote()) {
				oDataSet.Tables[0].Rows[0]["TODAY_VOTED_FLAG"] = oPickupObjsVote.CheckExistVoteByDay(pSiteCd,pSelfUserSeq,pSelfUserCharNo,pPickupId,pObjSeq);
			}
		}

		return oDataSet;
	}
}