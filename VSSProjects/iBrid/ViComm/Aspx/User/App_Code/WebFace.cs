﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: ＷＥＢデザイン設定
--	Progaram ID		: WebFace
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

[System.Serializable]
public class WebFace:DbSession {

	public string colorLine;
	public string colorIndex;
	public string colorChar;
	public string colorBack;
	public string colorLink;
	public string colorALink;
	public string colorVLink;
	public int sizeLine;
	public int sizeChar;
	
	public WebFace() {
	}


	public bool GetOne(string pWebFaceSeq,string pSexCd) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT ").AppendLine();
		sSql.Append("	COLOR_LINE			,").AppendLine();
		sSql.Append("	COLOR_INDEX			,").AppendLine();
		sSql.Append("	COLOR_CHAR			,").AppendLine();
		sSql.Append("	COLOR_BACK			,").AppendLine();
		sSql.Append("	COLOR_LINK			,").AppendLine();
		sSql.Append("	COLOR_ALINK			,").AppendLine();
		sSql.Append("	COLOR_VLINK			,").AppendLine();
		sSql.Append("	SIZE_LINE			,").AppendLine();
		sSql.Append("	SIZE_CHAR			,").AppendLine();
		sSql.Append("	COLOR_LINE_WOMAN  	,").AppendLine();
		sSql.Append("	COLOR_INDEX_WOMAN 	,").AppendLine();
		sSql.Append("	COLOR_CHAR_WOMAN  	,").AppendLine();
		sSql.Append("	COLOR_BACK_WOMAN  	,").AppendLine();
		sSql.Append("	COLOR_LINK_WOMAN  	,").AppendLine();
		sSql.Append("	COLOR_ALINK_WOMAN 	,").AppendLine();
		sSql.Append("	COLOR_VLINK_WOMAN 	,").AppendLine();
		sSql.Append("	SIZE_LINE_WOMAN   	,").AppendLine();
		sSql.Append("	SIZE_CHAR_WOMAN   	").AppendLine();
		sSql.Append("FROM T_WEB_FACE WHERE WEB_FACE_SEQ = :WEB_FACE_SEQ");

		using (cmd = CreateSelectCommand(sSql.ToString(),conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("WEB_FACE_SEQ",pWebFaceSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_WEB_FACE");

				if (ds.Tables["T_WEB_FACE"].Rows.Count != 0) {
					dr = ds.Tables["T_WEB_FACE"].Rows[0];
					bExist = true;
					if (pSexCd.Equals(ViComm.ViCommConst.WOMAN)) {
						colorLine = dr["COLOR_LINE_WOMAN"].ToString();
						colorIndex = dr["COLOR_INDEX_WOMAN"].ToString();
						colorChar = dr["COLOR_CHAR_WOMAN"].ToString();
						colorBack = dr["COLOR_BACK_WOMAN"].ToString();
						colorLink = dr["COLOR_LINK_WOMAN"].ToString();
						colorALink = dr["COLOR_ALINK_WOMAN"].ToString();
						colorVLink = dr["COLOR_VLINK_WOMAN"].ToString();
						sizeLine = int.Parse(dr["SIZE_LINE_WOMAN"].ToString());
						sizeChar = int.Parse(dr["SIZE_CHAR_WOMAN"].ToString());
					} else {
						colorLine = dr["COLOR_LINE"].ToString();
						colorIndex = dr["COLOR_INDEX"].ToString();
						colorChar = dr["COLOR_CHAR"].ToString();
						colorBack = dr["COLOR_BACK"].ToString();
						colorLink = dr["COLOR_LINK"].ToString();
						colorALink = dr["COLOR_ALINK"].ToString();
						colorVLink = dr["COLOR_VLINK"].ToString();
						sizeLine = int.Parse(dr["SIZE_LINE"].ToString());
						sizeChar = int.Parse(dr["SIZE_CHAR"].ToString());
					}
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
