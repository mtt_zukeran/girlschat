﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップキャラクター
--	Progaram ID		: PickupCharacter
--
--  Creation Date	: 2011.06.30
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;

using ViComm;
using iBridCommLib;

[System.Serializable]
public class PickupCharacter:DbSession {
	public string siteCd = string.Empty;
	public string userSeq = string.Empty;
	public string userCharNo = string.Empty;
	public string pickupId = string.Empty;
	public bool pickupFlag = false;
	public DateTime pickupStartPubDay;
	public DateTime pickupEndPubDay;
	
	
	public PickupCharacter() {}
	
	public bool GetOne(string pSiteCd, string pUserSeq, string pUserCharNo, string pPickupId){
		bool bExists =false;
		StringBuilder sSql = new StringBuilder();

		sSql.Append("SELECT											  ").AppendLine();
		sSql.Append("	SITE_CD										, ").AppendLine();
		sSql.Append("	USER_SEQ									, ").AppendLine();
		sSql.Append("	USER_CHAR_NO								, ").AppendLine();
		sSql.Append("	PICKUP_ID									, ").AppendLine();
		sSql.Append("	PICKUP_FLAG									, ").AppendLine();
		sSql.Append("	PICKUP_START_PUB_DAY						, ").AppendLine();
		sSql.Append("	PICKUP_END_PUB_DAY							  ").AppendLine();
		sSql.Append("FROM											  ").AppendLine();
		sSql.Append("	T_PICKUP_CHARACTER							  ").AppendLine();
		sSql.Append("WHERE											  ").AppendLine();
		sSql.Append("	SITE_CD				= :SITE_CD			AND	  ").AppendLine();
		sSql.Append("	USER_SEQ			= :USER_SEQ			AND	  ").AppendLine();
		sSql.Append("	USER_CHAR_NO		= :USER_CHAR_NO		AND	  ").AppendLine();
		sSql.Append("	PICKUP_ID			= :PICKUP_ID			  ").AppendLine();


		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add(":PICKUP_ID",pPickupId);

				using (da = new OracleDataAdapter(cmd))
				using (DataSet ds = new DataSet()) {
					da.Fill(ds);

					if (ds.Tables[0].Rows.Count != 0) {
						bExists = true;
						DataRow dr =ds.Tables[0].Rows[0];

						siteCd = iBridUtil.GetStringValue(dr["SITE_CD"]);
						userSeq = iBridUtil.GetStringValue(dr["USER_SEQ"]);
						userCharNo = iBridUtil.GetStringValue(dr["USER_CHAR_NO"]);
						pickupId = iBridUtil.GetStringValue(dr["PICKUP_ID"]);
						pickupFlag = iBridUtil.GetStringValue(dr["PICKUP_FLAG"]).Equals("1");
						pickupStartPubDay = DateTime.Parse(dr["PICKUP_START_PUB_DAY"].ToString());
						pickupEndPubDay = DateTime.Parse(dr["PICKUP_END_PUB_DAY"].ToString());

					}
				}

			}
		} finally {
			conn.Close();
		}

		return bExists;
		
	}
}
