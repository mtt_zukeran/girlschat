﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public abstract class ProductBase : DbSession {
	protected SessionObjs sessionObj = null;

	public ProductBase() : this(SessionObjs.Current) { }
	public ProductBase(SessionObjs pSessionObj) { 
		this.sessionObj = pSessionObj;
	}
	
	protected abstract string GetProductType(ProductSeekCondition pCondition);

	protected OracleParameter[] CreateWhereBase(ProductSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		DateTime dtNow = DateTime.Now;
		string sProductType = this.GetProductType(pCondition);
		string sRankingType = this.GetRankingType(pCondition);

		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 公開条件
		SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PUBLISH_START_DATE	<=	:PUBLISH_START_DATE		", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PUBLISH_END_DATE		>=	:PUBLISH_END_DATE		", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PUBLISH_FLAG			=	:PUBLISH_FLAG			", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" VW_PRODUCT00.DEL_FLAG				=	:DEL_FLAG				", ref pWhereClause);
		oParamList.Add(new OracleParameter(":PUBLISH_START_DATE", dtNow));
		oParamList.Add(new OracleParameter(":PUBLISH_END_DATE", dtNow));
		oParamList.Add(new OracleParameter(":PUBLISH_FLAG", ViCommConst.FLAG_ON));
		oParamList.Add(new OracleParameter(":DEL_FLAG", ViCommConst.FLAG_OFF));

		
		// サイトコード
		SysPrograms.SqlAppendWhere(" VW_PRODUCT00.SITE_CD = :SITE_CD ", ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));

		// 商品種別
		if (!string.IsNullOrEmpty(sProductType)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_TYPE = :PRODUCT_TYPE ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRODUCT_TYPE", sProductType));
		}
		// 商品SEQ(1件検索)
		if (!string.IsNullOrEmpty(pCondition.ProductSeq)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_SEQ = :PRODUCT_SEQ ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRODUCT_SEQ", pCondition.ProductSeq));
		}
		if (!string.IsNullOrEmpty(pCondition.CastSummary)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.CAST_SUMMARY LIKE '%' || :CAST_SUMMARY || '%' ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_SUMMARY", pCondition.CastSummary));
		}
		if (!string.IsNullOrEmpty(pCondition.Discription)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_DISCRIPTION LIKE '%' || :PRODUCT_DISCRIPTION || '%' ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRODUCT_DISCRIPTION", pCondition.Discription));
		}
		if (!string.IsNullOrEmpty(pCondition.Series)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_SERIES_SUMMARY LIKE '%' || :PRODUCT_SERIES_SUMMARY || '%' ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRODUCT_SERIES_SUMMARY", pCondition.Series));
		}
		if (!string.IsNullOrEmpty(pCondition.Maker)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_MAKER_SUMMARY LIKE '%' || :PRODUCT_MAKER_SUMMARY || '%' ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRODUCT_MAKER_SUMMARY", pCondition.Maker));
		}
		// =====================
		// ピックアップ検索
		// =====================
		if (!string.IsNullOrEmpty(pCondition.PickupId)) {
			// 結合条件
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.SITE_CD			=	T_PRODUCT_PICKUP.SITE_CD			", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_AGENT_CD	=	T_PRODUCT_PICKUP.PRODUCT_AGENT_CD	", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_SEQ		=	T_PRODUCT_PICKUP.PRODUCT_SEQ		", ref pWhereClause);
			// 検索条件
			SysPrograms.SqlAppendWhere(" T_PRODUCT_PICKUP.PICKUP_ID		=	:PICKUP_ID							", ref pWhereClause);
			oParamList.Add(new OracleParameter(":PICKUP_ID", pCondition.PickupId));	
		}
		
		// =====================
		// ジャンル検索
		// =====================
		if (!string.IsNullOrEmpty(pCondition.Genre)) {
			oParamList.AddRange(this.AppendGenreQuery(pCondition.GetGenreConditions(),"1_",ref pWhereClause));
		}
		// =====================
		// ジャンル検索(絞込み)
		// =====================
		if (!string.IsNullOrEmpty(pCondition.NarrowGenre)) {
			oParamList.AddRange(this.AppendGenreQuery(pCondition.GetNarrowGenreConditions(), "2_", ref pWhereClause));
		}
		// =====================
		// キーワード検索
		// =====================
		if(!string.IsNullOrEmpty(pCondition.Keyword)){
			StringBuilder oSubSqlBuilder = new StringBuilder();
			oSubSqlBuilder.AppendLine(" (");

			oSubSqlBuilder.AppendLine(" EXISTS(");
			oSubSqlBuilder.AppendLine(" SELECT 1 FROM T_PRODUCT_KEYWORD");
			oSubSqlBuilder.AppendLine(" WHERE ");
			oSubSqlBuilder.AppendLine("		VW_PRODUCT00.SITE_CD			=	T_PRODUCT_KEYWORD.SITE_CD			AND ");
			oSubSqlBuilder.AppendLine("		VW_PRODUCT00.PRODUCT_AGENT_CD	=	T_PRODUCT_KEYWORD.PRODUCT_AGENT_CD	AND ");
			oSubSqlBuilder.AppendLine("		VW_PRODUCT00.PRODUCT_SEQ		=	T_PRODUCT_KEYWORD.PRODUCT_SEQ		AND ");
			oSubSqlBuilder.AppendLine("		T_PRODUCT_KEYWORD.PRODUCT_KEYWORD_VALUE LIKE '%' || :PRODUCT_KEYWORD_VALUE ||'%'");
			oSubSqlBuilder.AppendLine(" )");
			oSubSqlBuilder.AppendLine(" OR VW_PRODUCT00.PRODUCT_NM				LIKE	'%'	|| :PRODUCT_KEYWORD_VALUE || '%'");
			oSubSqlBuilder.AppendLine(" OR VW_PRODUCT00.CAST_SUMMARY			LIKE	'%'	|| :PRODUCT_KEYWORD_VALUE || '%'");
			oSubSqlBuilder.AppendLine(" OR VW_PRODUCT00.PRODUCT_DISCRIPTION		LIKE	'%'	|| :PRODUCT_KEYWORD_VALUE || '%'");
			oSubSqlBuilder.AppendLine(" OR VW_PRODUCT00.PRODUCT_SERIES_SUMMARY	LIKE	'%'	|| :PRODUCT_KEYWORD_VALUE || '%'");

			oSubSqlBuilder.AppendLine(" )");

			SysPrograms.SqlAppendWhere(oSubSqlBuilder.ToString(), ref pWhereClause);

			oParamList.Add(new OracleParameter(":PRODUCT_KEYWORD_VALUE",pCondition.Keyword));	
		}
		// =====================
		// 購入履歴
		// =====================
		if(IsBuyingHist(pCondition)){
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.SITE_CD			=	T_OBJ_USED_HISTORY.SITE_CD  ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" :BUYING_HIST_OBJ_TYPE			=	T_OBJ_USED_HISTORY.OBJ_TYPE ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_SEQ		=	T_OBJ_USED_HISTORY.OBJ_SEQ  ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" :BUYING_HIST_USER_SEQ			=	T_OBJ_USED_HISTORY.USER_SEQ ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BUYING_HIST_OBJ_TYPE", ViCommConst.ATTACHED_PRODUCT));
			oParamList.Add(new OracleParameter(":BUYING_HIST_USER_SEQ", pCondition.UserSeq));	
		}

		// =====================
		// お気に入り		// =====================
		if (IsFavorit(pCondition)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.SITE_CD			=	T_FAVORITE_PRODUCT.SITE_CD  ",ref pWhereClause);
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_AGENT_CD	=	T_FAVORITE_PRODUCT.PRODUCT_AGENT_CD ",ref pWhereClause);
			SysPrograms.SqlAppendWhere(" :FAVORITE_USER_SEQ				=	T_FAVORITE_PRODUCT.USER_SEQ  ",ref pWhereClause);
			SysPrograms.SqlAppendWhere(" :FAVORITE_USER_CHAR_NO			=	T_FAVORITE_PRODUCT.USER_CHAR_NO ",ref pWhereClause);
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_SEQ		=	T_FAVORITE_PRODUCT.PRODUCT_SEQ ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":FAVORITE_USER_SEQ",pCondition.UserSeq));
			oParamList.Add(new OracleParameter(":FAVORITE_USER_CHAR_NO",pCondition.UserCharNo));
		}
		// =====================
		// ランキング
		// =====================
		if(IsRanking(pCondition)){
			// T_PRODUCT_RANKING×T_PRODUCT_RANKING_DTL
			SysPrograms.SqlAppendWhere(" T_PRODUCT_RANKING.SITE_CD			=	T_PRODUCT_RANKING_DTL.SITE_CD		", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_RANKING.PRODUCT_TYPE		=	T_PRODUCT_RANKING_DTL.PRODUCT_TYPE  ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_RANKING.RANKING_TYPE		=	T_PRODUCT_RANKING_DTL.RANKING_TYPE  ", ref pWhereClause);
			// T_PRODUCT_RANKING_DTL×T_PRODUCT
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.SITE_CD			=	T_PRODUCT_RANKING_DTL.SITE_CD  ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_AGENT_CD	=	T_PRODUCT_RANKING_DTL.PRODUCT_AGENT_CD ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_SEQ		=	T_PRODUCT_RANKING_DTL.PRODUCT_SEQ ", ref pWhereClause);
			// condition
			SysPrograms.SqlAppendWhere(" T_PRODUCT_RANKING_DTL.PRODUCT_TYPE	=	:PRODUCT_TYPE ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" T_PRODUCT_RANKING_DTL.RANKING_TYPE	=	:RANKING_TYPE ", ref pWhereClause);

			oParamList.Add(new OracleParameter(":PRODUCT_TYPE", sProductType));
			oParamList.Add(new OracleParameter(":RANKING_TYPE", sRankingType));
		}

		// =====================
		// 関連アーティスト		// =====================
		if (!string.IsNullOrEmpty(pCondition.RelCastLoginId)
			|| !string.IsNullOrEmpty(pCondition.RelCast)) {
			StringBuilder oSubSqlBuilder = new StringBuilder();
			oSubSqlBuilder.AppendLine("EXISTS(");
			oSubSqlBuilder.AppendLine(" SELECT 																				    ");
			oSubSqlBuilder.AppendLine(" 	1		                                                                            ");
			oSubSqlBuilder.AppendLine(" FROM                                                                                    ");
			oSubSqlBuilder.AppendLine(" 	T_PRODUCT_CAST_CHARACTER_REL,                                                       ");
			oSubSqlBuilder.AppendLine(" 	VW_CAST_CHARACTER00                                                                 ");
			oSubSqlBuilder.AppendLine(" WHERE                                                                                   ");
			oSubSqlBuilder.AppendLine(" 	T_PRODUCT_CAST_CHARACTER_REL.SITE_CD 			= VW_PRODUCT00.SITE_CD 			AND ");
			oSubSqlBuilder.AppendLine(" 	T_PRODUCT_CAST_CHARACTER_REL.PRODUCT_AGENT_CD 	= VW_PRODUCT00.PRODUCT_AGENT_CD AND ");
			oSubSqlBuilder.AppendLine(" 	T_PRODUCT_CAST_CHARACTER_REL.PRODUCT_SEQ 		= VW_PRODUCT00.PRODUCT_SEQ		AND ");
			oSubSqlBuilder.AppendLine(" 	T_PRODUCT_CAST_CHARACTER_REL.SITE_CD 			= VW_CAST_CHARACTER00.SITE_CD 		AND ");
			oSubSqlBuilder.AppendLine(" 	T_PRODUCT_CAST_CHARACTER_REL.USER_SEQ 			= VW_CAST_CHARACTER00.USER_SEQ 		AND ");
			oSubSqlBuilder.AppendLine(" 	T_PRODUCT_CAST_CHARACTER_REL.USER_CHAR_NO 		= VW_CAST_CHARACTER00.USER_CHAR_NO  AND ");
			if (!string.IsNullOrEmpty(pCondition.RelCastLoginId)) {
				oSubSqlBuilder.AppendLine(" 	VW_CAST_CHARACTER00.LOGIN_ID = :REL_CAST_LOGINID			AND		");
				oParamList.Add(new OracleParameter(":REL_CAST_LOGINID", pCondition.RelCastLoginId));
			}
			if (!string.IsNullOrEmpty(pCondition.RelCast)) {
				oSubSqlBuilder.AppendLine(" 	VW_CAST_CHARACTER00.HANDLE_NM LIKE '%' || :REL_CAST || '%'	AND     ");
				oParamList.Add(new OracleParameter(":REL_CAST", pCondition.RelCast));
			}
			oSubSqlBuilder.AppendLine(" 	0=0															                        ");
			oSubSqlBuilder.AppendLine(")");

			SysPrograms.SqlAppendWhere(oSubSqlBuilder.ToString(), ref pWhereClause);
		}
		// =====================
		// 新着のみ検索
		// =====================
		if((!string.IsNullOrEmpty(pCondition.WithoutNotnew)) && pCondition.WithoutNotnew.Equals("1")) {
			SysPrograms.SqlAppendWhere("VW_PRODUCT00.NOT_NEW_FLAG	!=	:NOT_NEW_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NOT_NEW_FLAG",ViCommConst.FLAG_ON));
		}
		
		return oParamList.ToArray();
	}

	private OracleParameter[] AppendGenreQuery(IList<ProductSeekCondition.GenreCondition> pGenreConditionList, string sPrefix, ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		if (pGenreConditionList.Count > 0) {
			StringBuilder oSubSqlBuilder = new StringBuilder();
			oSubSqlBuilder.AppendLine(" EXISTS(");
			oSubSqlBuilder.AppendLine(" SELECT 1 FROM T_PRODUCT_GENRE_REL");
			oSubSqlBuilder.AppendLine(" WHERE ");
			oSubSqlBuilder.AppendLine("		VW_PRODUCT00.SITE_CD			=	T_PRODUCT_GENRE_REL.SITE_CD				AND ");
			oSubSqlBuilder.AppendLine("		VW_PRODUCT00.PRODUCT_AGENT_CD	=	T_PRODUCT_GENRE_REL.PRODUCT_AGENT_CD	AND ");
			oSubSqlBuilder.AppendLine("		VW_PRODUCT00.PRODUCT_SEQ		=	T_PRODUCT_GENRE_REL.PRODUCT_SEQ			AND ");
			oSubSqlBuilder.AppendLine("		(T_PRODUCT_GENRE_REL.PRODUCT_GENRE_CATEGORY_CD,T_PRODUCT_GENRE_REL.PRODUCT_GENRE_CD) IN (");
			for (int i = 0; i < pGenreConditionList.Count; i++) {
				ProductSeekCondition.GenreCondition oGenreCondtion = pGenreConditionList[i];
				if (i != 0) {
					oSubSqlBuilder.Append("			,");
				}
				oSubSqlBuilder.AppendFormat("			(:PRODUCT_GENRE_CATEGORY_CD_{1}{0},:PRODUCT_GENRE_CD_{1}{0})", i, sPrefix);
				oParamList.Add(new OracleParameter(string.Format(":PRODUCT_GENRE_CATEGORY_CD_{1}{0}", i, sPrefix), oGenreCondtion.CategoryCd));
				oParamList.Add(new OracleParameter(string.Format(":PRODUCT_GENRE_CD_{1}{0}", i, sPrefix), oGenreCondtion.GenreCd));
			}
			oSubSqlBuilder.AppendLine("		)");
			oSubSqlBuilder.AppendLine(" )");

			SysPrograms.SqlAppendWhere(oSubSqlBuilder.ToString(), ref pWhereClause);
		}	
		return oParamList.ToArray();
	}
	
	protected virtual string CreateOrderExpresionBase(ProductSeekCondition pCondition){
		string sSortExpression = string.Empty;
		if(string.IsNullOrEmpty(pCondition.Sort)){
			sSortExpression = " ORDER BY VW_PRODUCT00.PUBLISH_START_DATE DESC,VW_PRODUCT00.PRODUCT_SEQ DESC ";

			if (pCondition.RandomFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				// パフォーマンスに問題あり。いずれshuffle方式に変更する
				sSortExpression = " ORDER BY RANDOM_VAL,VW_PRODUCT00.PRODUCT_SEQ DESC ";
			} else if (this.IsBuyingHist(pCondition)) {
				sSortExpression = " ORDER BY BUY_DATE DESC ,VW_PRODUCT00.PRODUCT_SEQ DESC ";
			} else if (this.IsFavorit(pCondition)){
				sSortExpression = " ORDER BY FAVORITE_REGIST_DATE DESC ,VW_PRODUCT00.PRODUCT_SEQ DESC ";
			} else if(this.IsRanking(pCondition)){
				sSortExpression = " ORDER BY RANKING ";
			}
		}else{
			sSortExpression = string.Format("ORDER BY {0} ",pCondition.Sort);
		}
		return sSortExpression;
	}


	protected string CreateFromBase(ProductSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		if (!string.IsNullOrEmpty(pCondition.PickupId)) {
			oSqlBuilder.Append("	T_PRODUCT_PICKUP,	");
		}
		if(this.IsBuyingHist(pCondition)){
			oSqlBuilder.Append("	T_OBJ_USED_HISTORY,	");
		}
		if (this.IsFavorit(pCondition)) {
			oSqlBuilder.Append("	T_FAVORITE_PRODUCT,	");
		}
		if (this.IsRanking(pCondition)) {
			oSqlBuilder.Append("	T_PRODUCT_RANKING		,	");
			oSqlBuilder.Append("	T_PRODUCT_RANKING_DTL	,	");
		}
		oSqlBuilder.Append("	VW_PRODUCT00	");
		return oSqlBuilder.ToString();
	}
	protected string CreateCountFieldBase(ProductSeekCondition pCondition) {
		return " 	COUNT(VW_PRODUCT00.PRODUCT_ID)	";
	}

	protected string CreateFieldBase(ProductSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		if(pCondition.RandomFlag.Equals(ViCommConst.FLAG_ON_STR)){
			// パフォーマンスに問題があるので、いずれshuffle方式に変える			oSqlBuilder.AppendLine(" 	DBMS_RANDOM.STRING('u',20)		AS RANDOM_VAL				, 	");　
		}
		if (this.IsFavorit(pCondition)) {
			oSqlBuilder.AppendLine(" 	T_FAVORITE_PRODUCT.REGIST_DATE  AS FAVORITE_REGIST_DATE  	, 	");
		}
		if (this.IsBuyingHist(pCondition)) {
			oSqlBuilder.AppendLine(" 	T_OBJ_USED_HISTORY.USED_DATE    AS BUY_DATE		  			, 	");
		}
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.SITE_CD					, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_AGENT_CD        	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_SEQ             	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_ID              	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_TYPE            	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_NM              	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_DISCRIPTION     	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PART_NUMBER             	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PUBLISH_START_DATE      	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PUBLISH_END_DATE        	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_SERIES_SUMMARY  	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.CAST_SUMMARY            	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_MAKER_SUMMARY   	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.RELEASE_DATE            	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.GROSS_PRICE             	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.APPLY_CHARGE_POINT         , 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.CHARGE_POINT            	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.SPECIAL_CHARGE_POINT    	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PUBLISH_FLAG            	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.DEL_FLAG                	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.REMARKS                 	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.LINK_ID                 	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.LINK_DATE               	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.UNEDITABLE_FLAG         	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.REVISION_NO             	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.UPDATE_DATE             	, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PICKUP_MASK                , 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PUBLISH_START_DAY          , 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.BUY_COUNT					, 	");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.FAVORIT_ME_COUNT            	");
		


		return oSqlBuilder.ToString();
	}

	protected virtual OracleParameter[] CreateOuterProductSql(ProductSeekCondition pCondition, string pBaseSql, out string pSql) {
		return this.CreateOuterProductSql(pCondition, pBaseSql,out pSql, "--", "--", "--");
	}
	protected OracleParameter[] CreateOuterProductSql(ProductSeekCondition pCondition, string pBaseSql, out string pSql, string pJoinTables, string pJoinConditions, string pOuterField) {

		string sProductSql = sessionObj.sqlSyntax["PRODUCT"].ToString();
		pSql = string.Format(sProductSql, pBaseSql, pJoinTables, pJoinConditions, pOuterField);
		return new OracleParameter[]{};
	}

	protected DataSet ExecutePageCollection(string sSql, ProductSeekCondition pCondition, OracleParameter[] oParams, string sSortExpression, int iStartIndex, int pRecPerPage) {
		// pagingクエリ作成
		string sExecSql = sSql;
		
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(sExecSql, sSortExpression, iStartIndex, pRecPerPage, out sExecSql);

		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();
			OracleParameter[] oTemplateParams = CreateOuterProductSql(pCondition,sExecSql, out sExecSql);
			using (this.cmd = CreateSelectCommand(sExecSql, this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oParams);
				this.cmd.Parameters.AddRange(oPagingParams);
				this.cmd.Parameters.AddRange(oTemplateParams);
				this.cmd.Parameters.Add(":USER_SEQ", pCondition.UserSeq);
				this.cmd.Parameters.Add(":USER_CHAR_NO", pCondition.UserCharNo);
				this.cmd.Parameters.Add(":LATEST_FLAG", ViCommConst.FLAG_ON);
				this.cmd.Parameters.Add(":OBJ_TYPE", ViCommConst.ATTACHED_PRODUCT);

				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.Fill(da,oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;
	}

	
	private bool IsBuyingHist(ProductSeekCondition pCondition) {
		return ((pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_PIC_BUYING_HIST)
			|| (pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_MOVIE_BUYING_HIST)
			|| (pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_THEME_BUYING_HIST));
	}

	private bool IsFavorit(ProductSeekCondition pCondition) {
		return ((pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_PIC_FAVORITE)
			|| (pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_MOVIE_FAVORITE)
			|| (pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_AUCTION_FAVORITE));
	}

	private bool IsRanking(ProductSeekCondition pCondition) {
		return ((pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_MOVIE_WEELKY_RANKING)
			|| (pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_MOVIE_MONTHLY_RANKING)
			|| (pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_PIC_WEELKY_RANKING)
			|| (pCondition.SeekMode == ViCommConst.INQUIRY_PRODUCT_PIC_MONTHLY_RANKING));
	}

	private string GetRankingType(ProductSeekCondition pCondition) {
		switch (pCondition.SeekMode) {
			case ViCommConst.INQUIRY_PRODUCT_MOVIE_WEELKY_RANKING:
			case ViCommConst.INQUIRY_PRODUCT_PIC_WEELKY_RANKING:
				return ViCommConst.ProductRankingType.Weekly;
			case ViCommConst.INQUIRY_PRODUCT_MOVIE_MONTHLY_RANKING:
			case ViCommConst.INQUIRY_PRODUCT_PIC_MONTHLY_RANKING:
				return ViCommConst.ProductRankingType.Monthly;
			default:
				return string.Empty;				
		}
	}
}
