﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者日記イイネ--	Progaram ID		: CastDiaryLike
--  Creation Date	: 2014.07.07
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Configuration;
using MobileLib;
using iBridCommLib;
using ViComm;

[Serializable]
public class CastDiaryLikeSeekCondition : SeekConditionBase{
	public string SiteCd;
	public string UserSeq;
	public string UserCharNo;

	public string ReportDay {
		get {
			return this.Query["reportday"];
		}
		set {
			this.Query["reportday"] = value;
		}
	}

	public string CastDiarySubSeq {
		get {
			return this.Query["subseq"];
		}
		set {
			this.Query["subseq"] = value;
		}
	}

	public CastDiaryLikeSeekCondition() : this(new NameValueCollection()) {
	}

	public CastDiaryLikeSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["reportday"] = iBridUtil.GetStringValue(pQuery["reportday"]);
		this.Query["subseq"] = iBridUtil.GetStringValue(pQuery["subseq"]);
	}
}

public class CastDiaryLike:DbSession {
	public CastDiaryLike() {
	}

	public int GetPageCount(CastDiaryLikeSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_DIARY_LIKE01");
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(CastDiaryLikeSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	LIKE_DATE,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	AGE,");
		oSqlBuilder.AppendLine("	RICHINO_RANK,");
		oSqlBuilder.AppendLine("	NEW_MAN_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_CAST_DIARY_LIKE01");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY LIKE_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(CastDiaryLikeSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null) {
			throw new ArgumentNullException();
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCondition.UserCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.ReportDay)) {
			SysPrograms.SqlAppendWhere("REPORT_DAY = :REPORT_DAY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_DAY",pCondition.ReportDay));
		}

		if (!string.IsNullOrEmpty(pCondition.CastDiarySubSeq)) {
			SysPrograms.SqlAppendWhere("CAST_DIARY_SUB_SEQ = :CAST_DIARY_SUB_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_DIARY_SUB_SEQ",pCondition.CastDiarySubSeq));
		}

		return oParamList.ToArray();
	}
}
