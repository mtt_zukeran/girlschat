﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 決済会社
--	Progaram ID		: SettleCompany
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;

[System.Serializable]
public class SettleCompany:DbSession {


	public string cpIdNo;
	public string cpPassword;
	public string settleUrl;
	public string continueSettleUrl;
	public string backUrl;

	public SettleCompany() {
		cpIdNo = "";
		cpPassword = "";
		settleUrl = "";
		continueSettleUrl = "";
		backUrl = "";
	}

	public bool GetOne(string pSiteCd,string pSettleCompanyCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect("SettleCompany.GetOne");

			using (cmd = CreateSelectCommand("SELECT CP_ID_NO,CP_PASSWORD,SETTLE_URL,CONTINUE_SETTLE_URL,BACK_URL FROM T_SETTLE_COMPANY WHERE SITE_CD = :SITE_CD AND SETTLE_COMPANY_CD =:SETTLE_COMPANY_CD ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_COMPANY_CD",pSettleCompanyCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SETTLE_COMPANY");
					if (ds.Tables["T_SETTLE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_SETTLE_COMPANY"].Rows[0];
						cpIdNo = dr["CP_ID_NO"].ToString();
						cpPassword = dr["CP_PASSWORD"].ToString();
						settleUrl = dr["SETTLE_URL"].ToString();
						continueSettleUrl = dr["CONTINUE_SETTLE_URL"].ToString();
						backUrl = dr["BACK_URL"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
