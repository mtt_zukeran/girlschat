/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: メールテンプレート

--	Progaram ID		: MailTemplate
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class MailTemplate:DbSession {

	public string htmlDoc;
	public string mailTitle;
	public string mailTemplateType;

	public MailTemplate() {
		htmlDoc = "";
		mailTitle = "";
		mailTemplateType = "";
	}

	public DataSet GetCastMailList(string pSiteCd,bool pIsRetrunMail,string pTxMailLayOutType,bool pVoiceOnlyFlag) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	MAIL_TEMPLATE_NO		,").AppendLine();
			sSql.Append("	TEMPLATE_NM				,").AppendLine();
			sSql.Append("	MAIL_ATTACHED_OBJ_TYPE	").AppendLine();
			sSql.Append("FROM						").AppendLine();
			sSql.Append("	T_MAIL_TEMPLATE			").AppendLine();
			sSql.Append("WHERE						").AppendLine();
			if (pTxMailLayOutType.Equals(ViCommConst.TxMailLayOutType.NORMAL_MAIL)) {
				sSql.Append("SITE_CD =:SITE_CD AND MAIL_TEMPLATE_TYPE IN (:TYPE1) ").AppendLine();
			} else if (pTxMailLayOutType.Equals(ViCommConst.TxMailLayOutType.MEETING_MAIL)) {
				if (pVoiceOnlyFlag) {
					sSql.Append("SITE_CD =:SITE_CD AND MAIL_TEMPLATE_TYPE IN (:TYPE3) ").AppendLine();
				} else {
					sSql.Append("SITE_CD =:SITE_CD AND MAIL_TEMPLATE_TYPE IN (:TYPE2,:TYPE3) ").AppendLine();
				}
			} else {
				if (pVoiceOnlyFlag) {
					sSql.Append("SITE_CD =:SITE_CD AND MAIL_TEMPLATE_TYPE IN (:TYPE1,:TYPE3) ").AppendLine();
				} else {
					sSql.Append("SITE_CD =:SITE_CD AND MAIL_TEMPLATE_TYPE IN (:TYPE1,:TYPE2,:TYPE3) ").AppendLine();
				}
			}
			sSql.Append("ORDER BY SITE_CD,MAIL_TEMPLATE_TYPE,MAIL_TEMPLATE_NO");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);

				if (!pTxMailLayOutType.Equals(ViCommConst.TxMailLayOutType.MEETING_MAIL)) {
					if (pIsRetrunMail) {
						cmd.Parameters.Add("TYPE1",ViCommConst.MAIL_TP_RETURN_TO_USER);
					} else {
						cmd.Parameters.Add("TYPE1",ViCommConst.MAIL_TP_APPROACH_TO_USER);
					}
				}

				if (!pTxMailLayOutType.Equals(ViCommConst.TxMailLayOutType.NORMAL_MAIL)) {
					if (!pVoiceOnlyFlag) {
						cmd.Parameters.Add("TYPE2",ViCommConst.MAIL_TP_MEETING_TV);
					}
					cmd.Parameters.Add("TYPE3",ViCommConst.MAIL_TP_MEETING_VOICE);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_TEMPLATE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCastInvaiteMailList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"MAIL_TEMPLATE_NO	," +
								"TEMPLATE_NM		" +
							"FROM " +
								"T_MAIL_TEMPLATE " +
							"WHERE " +
								"SITE_CD =:SITE_CD AND MAIL_TEMPLATE_TYPE IN (:TYPE1) " +
							"ORDER BY SITE_CD,MAIL_TEMPLATE_TYPE,MAIL_TEMPLATE_NO ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("TYPE1",ViCommConst.MAIL_TP_INVITE_TALK);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_TEMPLATE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetValue(string pSiteCd,string pMailTemplateNo,string pItem,out string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"MAIL_ATTACHED_OBJ_TYPE ," +
								"MAIL_ATTACHED_METHOD	," +
								"TEMPLATE_NM			" +
							"FROM " +
								"T_MAIL_TEMPLATE " +
							"WHERE " +
								"SITE_CD =:SITE_CD AND MAIL_TEMPLATE_NO = :MAIL_TEMPLATE_NO ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAIL_TEMPLATE_NO",pMailTemplateNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_TEMPLATE");
					if (ds.Tables["T_MAIL_TEMPLATE"].Rows.Count != 0) {
						dr = ds.Tables["T_MAIL_TEMPLATE"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}

