﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 決済ログ
--	Progaram ID		: SettleLog
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using MobileLib;

[System.Serializable]
public class SettleLog:DbSession {

	public SettleLog() {
	}


	public void LogSettleRequest(string pSiteCd,string pUserSeq,string pSettleCompanyCd,string pSettleType,string pSettleStatus,int pSettleAmt,int pExPoint,string pSettleID,string pAspAdCd,out string pSid) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_SETTLE_REQUEST");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PSETTLE_COMPNAY_CD",DbSession.DbType.VARCHAR2,pSettleCompanyCd);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,pSettleType);
			db.ProcedureInParm("PSETTLE_STATUS",DbSession.DbType.VARCHAR2,pSettleStatus);
			db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,pSettleAmt);
			db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,pExPoint);
			db.ProcedureInParm("PSETTLE_ID",DbSession.DbType.VARCHAR2,pSettleID);
			db.ProcedureInParm("PASP_AD_CD",DbSession.DbType.VARCHAR2,pAspAdCd);
			db.ProcedureOutParm("PSETTLE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pSid = db.GetStringValue("PSID");
		}
	}

	public bool LogSettleResult(string pSid,string pUserSeq,int pSettleAmt,int pSettlePoint,string pResponse) {
		string sRet;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_SETTLE_RESULT");
			db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,pSid);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,pSettleAmt);
			db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,pSettlePoint);
			db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,pResponse);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sRet = db.GetStringValue("PRESULT");
		}
		return sRet.Equals("0");
	}

}
