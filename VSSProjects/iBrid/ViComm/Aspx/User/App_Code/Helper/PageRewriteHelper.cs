﻿using System;
using System.Text;
using System.IO;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Collections.Generic;
using System.Collections.Specialized;

using iBridCommLib;
using ViComm;

/// <summary>
/// PageOverrideHelper の概要の説明です/// </summary>
public class PageRewriteHelper {


	#region □■□ inner class(PageRewriteInfo) □■□ ================================================================
	private class PageRewriteInfo{
		private string programRoot = null;
		private string programId = null;
		
		
		public string ProgramRoot{
			get{ return this.programRoot;}
		}
		
		public string ProgramId{
			get { return this.programId; }
		}

		public PageRewriteInfo(string pProgramRoot,string pProgramId) {
			this.programRoot = pProgramRoot;
			this.programId = pProgramId;
		}
		
		public string GenerateRewriteUrl(string pAppRoot,string pOverrideAlias){
			if(pAppRoot.EndsWith("/")){
				pAppRoot = pAppRoot.Substring(0, pAppRoot.Length - 1);
			}
			return string.Format(string.Format("{0}/{1}/override/{2}/{3}", pAppRoot, this.programRoot, pOverrideAlias, this.programId));
		}	
		
	}
	#endregion ========================================================================================================

	private volatile static object syncRoot = 1;
	private static HybridDictionary enableOverridePageDict = new HybridDictionary();
	private static string overrideAlias = null;
	private static bool isDebugMode = false;
	private static bool initialized = false;

	[ThreadStatic]
	private static string originalRequestPath = string.Empty;
	[ThreadStatic]
	private static bool rewrited = false;
	
	public static void Initialize() {
		if(initialized) return;
		lock(syncRoot){
			if (initialized) return; // ダブルチェックロッキング

			overrideAlias = ConfigurationManager.AppSettings["PageRewriteAlias"];
			isDebugMode = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["PageRewriteDebug"]).Equals(ViComm.ViCommConst.FLAG_ON_STR);

			if (!string.IsNullOrEmpty(overrideAlias)){ 
				using(Program oProgram = new Program()){
					using(DataSet oProgramDataSet = oProgram.GetEnableOverrideList()){
						if (oProgramDataSet.Tables.Count > 0 && oProgramDataSet.Tables[0].Rows.Count > 0) {
							
							// overrideを許可しているページのリストを作成する
							foreach (DataRow oProgramDataRow in oProgramDataSet.Tables[0].Rows) {

								string sProgramId = iBridUtil.GetStringValue(oProgramDataRow["PROGRAM_ID"]);
								string sProgramRoot = iBridUtil.GetStringValue(oProgramDataRow["PROGRAM_ROOT"]);
								
								string sKey = string.Format("{0}/{1}", sProgramRoot.ToLower(), sProgramId.ToLower());

								enableOverridePageDict.Add(sKey,new PageRewriteInfo(sProgramRoot,sProgramId));
									
							}
						}
					}
				}
			}

			
			initialized = true;
		}
	}
	
	public static void Rewrite(HttpContext pHttpContext) {
		if(string.IsNullOrEmpty(overrideAlias)) return ;
	
		string sRequestPath = pHttpContext.Request.Path.ToLower();
		if(!sRequestPath.Contains(".aspx"))return ;

		originalRequestPath = string.Empty;
		rewrited = false;
		
		PageRewriteInfo oPageRewriteInfo = GetPageRewriteInfo(sRequestPath);
		if (oPageRewriteInfo != null) {
			string sAppPath = pHttpContext.Request.ApplicationPath;
			originalRequestPath = pHttpContext.Request.Url.PathAndQuery;
			pHttpContext.RewritePath(oPageRewriteInfo.GenerateRewriteUrl(sAppPath, overrideAlias));
			rewrited = true;
		} else if (isDebugMode) {
			// デバッグモードの場合はT_PROGRAM無視
			StringBuilder oStringBuilder = new StringBuilder();
			
			int iSegmentIndex = 0;
			foreach(string sSegment in pHttpContext.Request.Url.Segments){
				if (iSegmentIndex == pHttpContext.Request.Url.Segments.Length - 1) {
					oStringBuilder.AppendFormat("override/{0}/", overrideAlias);
				}
				oStringBuilder.Append(sSegment);
				iSegmentIndex += 1;
			}
			
			string sRewriteUrl = oStringBuilder.ToString().ToLower();
			if (File.Exists(pHttpContext.Server.MapPath(sRewriteUrl))) {
				originalRequestPath = pHttpContext.Request.Url.PathAndQuery;
				pHttpContext.RewritePath(sRewriteUrl);
				rewrited = true;				
			}			
		}
	}

	public static void Reset(HttpContext pHttpContext) {
		if (string.IsNullOrEmpty(overrideAlias)) return;

		if (rewrited) {
			pHttpContext.RewritePath(originalRequestPath);
		
			originalRequestPath = string.Empty;
			rewrited = false;
		}		
	}
	
	private static PageRewriteInfo GetPageRewriteInfo(string pRequestPath) {
		foreach (string sKey in enableOverridePageDict.Keys) {
			if (pRequestPath.Contains(sKey)) {
				return (PageRewriteInfo)enableOverridePageDict[sKey];
			}
		}
		return null;
	}

}
