﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;
/// <summary>
/// VoiceHelper の概要の説明です
/// </summary>
public class VoiceHelper {
	public VoiceHelper() {}
	
	public void Download(string pSiteCd, string pLoginId,string pVoiceSeq){

		string sVoiceFileNm = this.GenerateVoiceFileNm(pVoiceSeq, SessionObjs.Current.carrier);
		string sVoiceUrl = string.Format("voice/{0}/operator/{1}/{2}", pSiteCd, pLoginId, sVoiceFileNm);
		string sUrl = SessionObjs.Current.GetNavigateUrl(sVoiceUrl);
		string sVoiceDir = ViCommPrograms.GetCastVoiceDir(SessionObjs.Current.site.webPhisicalDir, pSiteCd, pLoginId);
		string sVoiceFilePath = System.IO.Path.Combine(sVoiceDir, sVoiceFileNm);

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			FileInfo oVoiceFileInfo = new FileInfo(sVoiceFilePath);
			
			if (SessionObjs.Current.carrier.Equals(ViCommConst.DOCOMO)) {
				// =======================
				//  for docomo
				// =======================
				HttpContext.Current.Response.Clear();
				HttpContext.Current.Response.ContentType = "video/3gpp";
				HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + sVoiceFileNm + "\"");
				HttpContext.Current.Response.AddHeader("Content-Length", oVoiceFileInfo.Length.ToString());
				HttpContext.Current.Response.WriteFile(oVoiceFileInfo.FullName);
				HttpContext.Current.Response.Flush();
				HttpContext.Current.Response.End();

			}else if (SessionObjs.Current.carrier.Equals(ViCommConst.KDDI)) {
				// =======================
				//  for au
				// =======================
				using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME, ViCommConst.FILE_UPLOAD_PASSWORD)) {
					
					HttpContext.Current.Response.Clear();
					HttpContext.Current.Response.ContentType = "application/x-smaf";
					HttpContext.Current.Response.AppendHeader("Content-Length", oVoiceFileInfo.Length.ToString());
					HttpContext.Current.Response.WriteFile(oVoiceFileInfo.FullName);
					HttpContext.Current.Response.Flush();
					HttpContext.Current.Response.End();
				}

			} else {
				// =======================
				//  for softbank
				// =======================
				HttpContext.Current.Response.Redirect(sUrl, true);
			}
		}
	}

	public string GetDownloadTag(SessionObjs pSessionObj, string pSiteCd, string pLoginId, string pTargetVoiceSeq, string pText) {
		string sDownloadTag = string.Empty;

		string sVoiceFilNm = this.GenerateVoiceFileNm(pTargetVoiceSeq, pSessionObj.carrier);
		string sVoiceDir = ViCommPrograms.GetCastVoiceDir(pSessionObj.site.webPhisicalDir, pSiteCd, pLoginId);
		string sVoiceFilePath = System.IO.Path.Combine(sVoiceDir, sVoiceFilNm);

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, "", ViCommConst.FILE_UPLOAD_PASSWORD)) {
			// ファイルが存在しない場合
			if (!File.Exists(sVoiceFilePath)) {
				string sErrorMessage = null;
				using (ErrorDtl oErrorDtl = new ErrorDtl()) {
					sErrorMessage = oErrorDtl.GetErrorDtl(pSiteCd, ViCommConst.ERR_NOTHING_VOICE);
				}
				return string.Format("{0}(<font color=\"#ff0000\">{1}</font>)", pText, sErrorMessage);
			}
			
			FileInfo oVoiceFileInfo = new FileInfo(sVoiceFilePath);


			UrlBuilder oBaseUrlBuilder = new UrlBuilder("ViewSaleVoice.aspx");
			oBaseUrlBuilder.Parameters.Add("voiceseq", pTargetVoiceSeq);
			oBaseUrlBuilder.Parameters.Add("action", "download");
			string sBaseUrl = oBaseUrlBuilder.ToString();

			if (pSessionObj.carrier.Equals(ViCommConst.DOCOMO)) {
			
				// =======================
				//  for docomo
				// =======================
				if (oVoiceFileInfo.Length <= 450000) {
					sDownloadTag = string.Format("<OBJECT declare id=\"obj{0}\" data=\"{1}\" type=\"video/3gpp\">", pTargetVoiceSeq, sBaseUrl) +
									string.Format("</OBJECT><A href=\"#obj{0}\">{1}</A>", pTargetVoiceSeq, pText);
				} else {
				    sDownloadTag = string.Format("<OBJECT declare id=\"obj{0}\" data=\"{1}\" type=\"video/3gpp\">", pTargetVoiceSeq, sBaseUrl) +
				                                "<PARAM name=\"stream-type\" value=\"10\" valuetype=\"data\">" +
				                    string.Format("</OBJECT><A href=\"#obj{0}\">{1}</A>", pTargetVoiceSeq, pText);
				}
			} else if (pSessionObj.carrier.Equals(ViCommConst.KDDI)) {
				// =======================
				//  for au
				// =======================

				sBaseUrl = pSessionObj.GetNavigateAbsoluteUrl(sBaseUrl);

				StringBuilder oStringBuilder = new StringBuilder();
				oStringBuilder.AppendFormat("<object data=\"{0}\" type=\"application/x-smaf\" copyright=\"no\" standby=\"{1}\">", sBaseUrl, pText);
				oStringBuilder.AppendFormat("<param name=\"title\" value=\"{0}\" valuetype=\"data\" />", pTargetVoiceSeq);
				oStringBuilder.AppendFormat("</object>").AppendLine();
					
				sDownloadTag = oStringBuilder.ToString();
			} else {
				// =======================
				//  for softbank
				// =======================
				
				sDownloadTag = string.Format("<a href=\"{0}\">{1}</a>", sBaseUrl, pText);
			}


			return sDownloadTag;			
		}
	}
	
	/// <summary>
	/// 着ボイスの物理ファイル名を取得する
	/// </summary>
	/// <param name="pSeq"></param>
	/// <returns>着ボイスの物理ファイル名</returns>
	public string GenerateVoiceFileNm(string pSeq,string pTargetCarrier){
		string sFileNm = iBridUtil.addZero(pSeq,ViCommConst.OBJECT_NM_LENGTH);
		string sFileSuffix = string.Empty;
		string sFileExtension = string.Empty;

		switch (pTargetCarrier) {
			case ViCommConst.DOCOMO:
				sFileSuffix = ViCommConst.FILE_SUFFIX_DOCOMO;
				sFileExtension = ViCommConst.FILE_EXTENSION_DOCOMO;
				break;
			case ViCommConst.KDDI:
				sFileSuffix = ViCommConst.FILE_SUFFIX_AU;
				sFileExtension = ViCommConst.FILE_EXTENSION_AU;
				break;
			case ViCommConst.SOFTBANK:
				sFileSuffix = ViCommConst.FILE_SUFFIX_SOFTBANK;
				sFileExtension = ViCommConst.FILE_EXTENSION_SOFTBANK;
				break;
			default:
				return string.Empty;
		}

		return string.Format("{0}_{1}.{2}", sFileNm, sFileSuffix, sFileExtension);
	}

	public bool IsAlreadyUsed(string pSelfSiteCd, string pSelfUserSeq, string pObjType, string pObjSeq) {
		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			return oObjUsedHistory.GetOne(pSelfSiteCd, pSelfUserSeq, pObjType, pObjSeq);
		}
	}
}
