﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

/// <summary>
/// RandomHelper の概要の説明です
/// </summary>
public class RandomHelper {

	private static IList<int> GenerateSeed(int pMaxValue) {
		IList<int> oSeed = new List<int>();
		for (int i = 0; i < pMaxValue; i++) {
			oSeed.Add(i);
		}
		return oSeed;
	}

	public static List<int> GenerateRandom(int pMaxValue, int pNeedCount) {
		List<int> oRandomValueList = new List<int>();
		Random oRandom = new Random(Environment.TickCount);

		IList<int> oSeed = GenerateSeed(pMaxValue);

		for (int i = 0; i < pNeedCount; i++) {
			int iRandomIndex = oRandom.Next(0, oSeed.Count);
			oRandomValueList.Add(oSeed[iRandomIndex]);

			oSeed.RemoveAt(iRandomIndex);
		}

		return oRandomValueList;
	}

	private static readonly string passwordChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	/// <summary>
	/// ランダムな文字列を生成する
	/// </summary>
	/// <param name="length">生成する文字列の長さ</param>
	/// <returns>生成された文字列</returns>
	public static string GeneratePassword(int length) {
		StringBuilder sb = new StringBuilder(length);
		Random r = new Random();

		for (int i = 0;i < length;i++) {
			int pos = r.Next(passwordChars.Length);
			char c = passwordChars[pos];
			sb.Append(c);
		}

		return sb.ToString();
	}
}
