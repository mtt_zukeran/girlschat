﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

/// <summary>
/// MailHelper の概要の説明です

/// </summary>
public class MailHelper {

	public static string ConvertVarForSend(string sValue) {
		sValue = sValue.Replace("##","%%4%%");
		sValue = sValue.Replace("＃＃","%%4%%");
		sValue = sValue.Replace("♯♯","%%4%%");
		return sValue;
	}
	public static string ConvertVarForDisplay(string sValue) {
		sValue = sValue.Replace("%%4%%","##");
		return sValue;
	}
}
