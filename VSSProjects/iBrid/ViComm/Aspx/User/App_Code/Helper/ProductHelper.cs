﻿using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;

public class ProductHelper {

	public static string ConvertProductType2ChargeType(string pProductType) {
		switch (pProductType) {
			case ViCommConst.ProductType.ADULT_MOVIE:
				return ViCommConst.CHARGE_PROD_ADULT_MOV;
			case ViCommConst.ProductType.NORMAL_MOVIE:
				return ViCommConst.CHARGE_PROD_MOV;
			case ViCommConst.ProductType.ADULT_PIC:
				return ViCommConst.CHARGE_PROD_ADULT_PIC;
			case ViCommConst.ProductType.NORMAL_PIC:
				return ViCommConst.CHARGE_PROD_PIC;
			case ViCommConst.ProductType.ADULT_AUCTION:
				return ViCommConst.CHARGE_PROD_ADULT_AUCTION;
			case ViCommConst.ProductType.NORMAL_AUCTION:
				return ViCommConst.CHARGE_PROD_AUCTION;
			case ViCommConst.ProductType.THEME:
				return ViCommConst.CHARGE_PROD_THEME;
			default:
				return string.Empty;
		}
	}
	
	public static string GetAdultFlag(string pProductType){
		switch (pProductType) {
			case ViCommConst.ProductType.ADULT_MOVIE:
			case ViCommConst.ProductType.ADULT_PIC:
				return ViCommConst.FLAG_ON_STR;
			case ViCommConst.ProductType.NORMAL_MOVIE:
			case ViCommConst.ProductType.NORMAL_PIC:
				return ViCommConst.FLAG_OFF_STR;
			default:
				return string.Empty;
		}
	}
	
	public static string GetPicProductType(string sAdultFlag) {
		if (sAdultFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			return ViCommConst.ProductType.ADULT_PIC;
		} else {
			return ViCommConst.ProductType.NORMAL_PIC;
		}
	}
	public static string GetMovieProductType(string sAdultFlag) {
		if (sAdultFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			return ViCommConst.ProductType.ADULT_MOVIE;
		} else {
			return ViCommConst.ProductType.NORMAL_MOVIE;
		}
	}
	public static string GetAuctionProductType(string sAdultFlag) {
		if (sAdultFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			return ViCommConst.ProductType.ADULT_AUCTION;
		} else {
			return ViCommConst.ProductType.NORMAL_AUCTION;
		}
	}	
	
	public static string ConvertPicSizeType2DataColumnNm(ViCommConst.PicSizeType pPicSizeType) {
		switch (pPicSizeType) {
			case ViCommConst.PicSizeType.ORIGINAL:
				return "PRODUCT_PIC_ORIGINAL_PATH";
			case ViCommConst.PicSizeType.LARGE:
				return "PRODUCT_PIC_PATH";
			case ViCommConst.PicSizeType.LARGE_BLUR:
				return "PRODUCT_PIC_BLUR_PATH";
			case ViCommConst.PicSizeType.SMALL:
				return "PRODUCT_PIC_S_PATH";
			case ViCommConst.PicSizeType.SMALL_BLUR:
				return "PRODUCT_PIC_S_BLUR_PATH";
			default:
				return string.Empty;
		}
	}
	public static bool IsAlreadyUsedMovie(string pProductSeq, string pProductType,string pMovieSeq) {
		if(!string.IsNullOrEmpty(pMovieSeq)){
			using(ProductMovie oProductMovie = new ProductMovie()){
				DataSet oDataSet = oProductMovie.GetOne(pMovieSeq);
				if(oDataSet.Tables.Count>0 && oDataSet.Tables[0].Rows.Count > 0){
					string sProductMovieType = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PRODUCT_MOVIE_TYPE"]);
					if (sProductMovieType.Equals(ViCommConst.ProductMovieType.SAMPLE)) {
						return true;
					}
				}
			}
		}
		return IsAlreadyUsed(pProductSeq, pProductType);
	}

	public static bool IsAlreadyUsed(string pProductSeq, string pProductType){
		SessionMan sessionMan = (SessionMan)SessionObjs.Current;
		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			return oObjUsedHistory.GetOne(sessionMan.userMan.siteCd, sessionMan.userMan.userSeq, ViCommConst.ATTACHED_PRODUCT.ToString(), pProductSeq);
		}
	}
	
	public static string GenerateAuthId(string pSiteCd,string pUserSeq,string pProductSeq){
	string sAuthId = string.Format("{0}:{1}:{2}", pSiteCd, pProductSeq, pUserSeq);
		return Convert.ToBase64String(Encoding.UTF8.GetBytes(sAuthId));
	}
	
	public static string GenerateDownloadUrl(string pProductId,string pProductType,string pObjSeq,string pFileNm,string pUseType){
		UrlBuilder oUrlBuilder = new UrlBuilder("DownloadProdMov.aspx");
		oUrlBuilder.Parameters.Add(ViCommConst.ACTION,ViCommConst.BUTTON_ACTION_DOWNLOAD);
		oUrlBuilder.Parameters.Add("product_id",pProductId);
		oUrlBuilder.Parameters.Add("product_type",pProductType);
		oUrlBuilder.Parameters.Add("obj_seq",pObjSeq);
		oUrlBuilder.Parameters.Add("file_nm", pFileNm);
		oUrlBuilder.Parameters.Add("use_type",pUseType);
		return oUrlBuilder.ToString();
	}

	public static string GenerateDownloadUrl4CP(string pBaseUrl, string pSiteCd, string pProductAgentCd, string pProductSeq, string pUserSeq) {
		string sDownloadUrl = string.Empty;
		using (ProductAgent oProductAgent = new ProductAgent()) {
			using (DataSet oDataSet = oProductAgent.GetOne(pProductAgentCd)) {
				DataRow oProductAgentRow = oDataSet.Tables[0].Rows[0];

				string sMid = iBridUtil.GetStringValue(oProductAgentRow["PRODUCT_AGENT_UID"]);
				string sMpw = ViCommPrograms.GetHashMd5(iBridUtil.GetStringValue(oProductAgentRow["PRODUCT_AGENT_PWD"]));
				string sAuthId = GenerateAuthId(pSiteCd, pUserSeq, pProductSeq);

				sDownloadUrl = pBaseUrl;
				sDownloadUrl = sDownloadUrl.Replace("<#MID>", sMid);
				sDownloadUrl = sDownloadUrl.Replace("<#MPW>", sMpw);
				sDownloadUrl = sDownloadUrl.Replace("<#UNIQ>", sAuthId);
			}
		}
		return sDownloadUrl;
	}

	#region □■□ 購入処理 □■□ ================================================================
	public static bool Purchase(string pProductSeq, string pProductType) {
		SessionMan sessionMan = (SessionMan)SessionObjs.Current;
		
		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			// 購入済みかチェック
			if (!IsAlreadyUsed(pProductSeq,pProductType)) {
				// 購入処理
				if (!PurchaseImpl(pProductSeq, pProductType)) {
					return false;
				}
			}
		}
		return true;
	}
	private static bool PurchaseImpl(string pProductSeq, string pProductType) {
		SessionMan sessionMan = (SessionMan)SessionObjs.Current;

		int iChargePoint = 0;
		
		// ポイント残のチェック。残ポイントが必要ポイントを下回る場合は警告画面へ遷移する
		if (!sessionMan.CheckProductBalance(pProductSeq, out iChargePoint)) {
			return false;
		}
		
		string sSiteCd;
		string sProductyAgentCd;
		using(Product oProduct = new Product()){
			DataSet oDataSet = oProduct.GetOneBySeq(pProductSeq);
			sSiteCd = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["SITE_CD"]);
			sProductyAgentCd = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PRODUCT_AGENT_CD"]);
		}
		
		using(ProductBuyHistory oProductBuyHistory = new ProductBuyHistory()){
			oProductBuyHistory.CreateHistory(
				sSiteCd,
				sProductyAgentCd,
				pProductSeq,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				ProductHelper.ConvertProductType2ChargeType(pProductType));
		}


		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd, sessionMan.userMan.userSeq);
		return true;
	}
	#endregion ====================================================================================
	
}
