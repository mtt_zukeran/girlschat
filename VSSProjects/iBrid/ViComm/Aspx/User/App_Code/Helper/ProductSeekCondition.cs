﻿using System;
using System.Web;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using ViComm;

[Serializable]
public class ProductSeekCondition : IDisposable{
	public class GenreCondition {
		private const char GENRE_CATEGORY_SEPARATOR = '-';

		private string genreCd = null;
		private string categoryCd = null;
		public string GenreCd { get { return this.genreCd; } }
		public string CategoryCd { get { return this.categoryCd; } }

		private GenreCondition(string pCategoryCd,string pGenreCd) {
			this.categoryCd = pCategoryCd;
			this.genreCd = pGenreCd;
		}

		public static GenreCondition Parse(string pValue) {
			string[] oValueArr = pValue.Split(GENRE_CATEGORY_SEPARATOR);
			string sCategoryCd = oValueArr[0];
			string sGenreCd = string.Empty;
			if(oValueArr.Length >= 2){
				sGenreCd = oValueArr[1];			
			}
			
			return new GenreCondition(sCategoryCd,sGenreCd);
		}
	}
	
	
	public const char SEPARATOR = ':';
	
	private NameValueCollection query = new NameValueCollection();

	public string SiteCd { 
		get { return this.query["site_cd"]; }
		set { this.query["site_cd"] = value; } 
	}

	public string ProductId {
		get { return this.query["product_id"]; }
		set { this.query["product_id"] = value; }
	}

	public string ProductSeq {
		get { return this.query["product_seq"]; }
		set { this.query["product_seq"] = value; }
	}

	public string AdultFlag {
		get { return this.query["adult"]; }
		set { this.query["adult"] = value; }
	}

	public string UserSeq {
		get { return this.query["user_seq"]; }
		set { this.query["user_seq"] = value; }
	}

	public string UserCharNo {
		get { return this.query["user_char_no"]; }
		set { this.query["user_char_no"] = value; }
	}

	public ulong SeekMode {
		get { return ulong.Parse(this.query["seekmode"] ?? "-1"); }
		set { this.query["seekmode"] = value.ToString(); }
	}
	
	public string PickupId{
		get { return this.query["pickup_id"]; }
		set { this.query["pickup_id"] = value; }
	}

	public string Genre {
		get { return this.query["genre"]; }
		set { this.query["genre"] = value; }
	}

	public string NarrowGenre {
		get { return this.query["narrow_genre"]; }
		set { this.query["narrow_genre"] = value; }
	}
	
	public string Keyword {
		get { return this.query["keyword"]; }
		set { this.query["keyword"] = value; }
	}
	public string ProductNm {
		get { return this.query["product_nm"]; }
		set { this.query["product_nm"] = value; }
	}
	public string RandomFlag{
		get { return this.query["random"]; }
		set { this.query["random"] = value; }
	}
	public string CastSummary {
		get { return this.query["cast"]; }
		set { this.query["cast"] = value; }
	}
	public string Discription {
		get { return this.query["discription"]; }
		set { this.query["discription"] = value; }
	}
	public string Series {
		get { return this.query["series"]; }
		set { this.query["series"] = value; }
	}
	public string Maker {
		get { return this.query["maker"]; }
		set { this.query["maker"] = value; }
	}
	public string RelCast {
		get { return this.query["rel_cast"]; }
		set { this.query["rel_cast"] = value; }
	}
	public string RelCastLoginId {
		get { return this.query["rel_cast_loginid"]; }
		set { this.query["rel_cast_loginid"] = value; }
	}
	public string Sort {
		get { return this.query["sort"]; }
		set { this.query["sort"] = value; }
	}
	public string WithoutNotnew {
		get { return this.query["without_notnew"];}
		set { this.query["without_notnew"] = value; }
	}
	public string AuctionStartGreater {
		get { return this.query["auction_start_greater"]; }
		set { this.query["auction_start_greater"] = value; }
	}
	public string AuctionStartGreaterEq {
		get { return this.query["auction_start_greater_eq"]; }
		set { this.query["auction_start_greater_eq"] = value; }
	}
	public string AuctionStartLess {
		get { return this.query["auction_start_less"]; }
		set { this.query["auction_start_less"] = value; }
	}
	public string AuctionStartLessEq {
		get { return this.query["auction_start_less_eq"]; }
		set { this.query["auction_start_less_eq"] = value; }
	}
	public string AuctionEndGreater {
		get { return this.query["auction_end_greater"]; }
		set { this.query["auction_end_greater"] = value; }
	}
	public string AuctionEndGreaterEq {
		get { return this.query["auction_end_greater_eq"]; }
		set { this.query["auction_end_greater_eq"] = value; }
	}
	public string AuctionEndLess {
		get { return this.query["auction_end_less"]; }
		set { this.query["auction_end_less"] = value; }
	}
	public string AuctionEndLessEq {
		get { return this.query["auction_end_less_eq"]; }
		set { this.query["auction_end_less_eq"] = value; }
	}
	public string BlindEndGreater {
		get { return this.query["blind_end_greater"]; }
		set { this.query["blind_end_greater"] = value; }
	}
	public string BlindEndGreaterEq {
		get { return this.query["blind_end_greater_eq"]; }
		set { this.query["blind_end_greater_eq"] = value; }
	}
	public string BlindEndLess {
		get { return this.query["blind_end_less"]; }
		set { this.query["blind_end_less"] = value; }
	}
	public string BlindEndLessEq {
		get { return this.query["blind_end_less_eq"]; }
		set { this.query["blind_end_less_eq"] = value; }
	}
	public string IsEntered {
		get { return this.query["is_entered"]; }
		set { this.query["is_entered"] = value; }
	}
	public string IsWin {
		get { return this.query["is_win"]; }
		set { this.query["is_win"] = value; }
	}	
	public ProductSeekCondition() : this(new NameValueCollection()) { }
	public ProductSeekCondition(NameValueCollection pQuery) {
		this.query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.query["product_id"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["product_id"]));
		this.query["product_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["product_seq"]));
		this.query["adult"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["adult"]));
		this.query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.query["seekmode"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["seekmode"]));
		this.query["pickup_id"] =HttpUtility.UrlDecode( iBridUtil.GetStringValue(pQuery["pickup_id"]));
		this.query["genre"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["genre"]));
		this.query["narrow_genre"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["narrow_genre"]));
		this.query["keyword"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["keyword"]));
		this.query["product_nm"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["product_nm"]));
		this.query["random"] =HttpUtility.UrlDecode( iBridUtil.GetStringValue(pQuery["random"]));
		this.query["cast"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["cast"]));
		this.query["discription"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["discription"]));
		this.query["series"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["series"]));
		this.query["maker"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["maker"]));
		this.query["rel_cast_loginid"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["rel_cast_loginid"]));
		this.query["rel_cast"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["rel_cast"]));
		this.query["without_notnew"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["without_notnew"]));
		this.query["sort"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sort"]));
		this.query["auction_start_greater"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_start_greater"]));
		this.query["auction_start_greater_eq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_start_greater_eq"]));
		this.query["auction_start_less"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_start_less"]));
		this.query["auction_start_less_eq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_start_less_eq"]));
		this.query["auction_end_greater"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_end_greater"]));
		this.query["auction_end_greater_eq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_end_greater_eq"]));
		this.query["auction_end_less"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_end_less"]));
		this.query["auction_end_less_eq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["auction_end_less_eq"]));
		this.query["blind_end_greater"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blind_end_greater"]));
		this.query["blind_end_greater_eq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blind_end_greater_eq"]));
		this.query["blind_end_less"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blind_end_less"]));
		this.query["blind_end_less_eq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blind_end_less_eq"]));
		this.query["is_entered"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["is_entered"]));
		this.query["is_win"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["is_win"]));
	}

	
	public NameValueCollection GetQuery(){
		return this.query;
	}
	
	public IList<GenreCondition> GetGenreConditions(){
		IList<GenreCondition> oGenreConditions = new List<GenreCondition>();
		foreach(string sValue in this.Genre.Split(SEPARATOR)){
			if(string.IsNullOrEmpty(sValue))continue;
			oGenreConditions.Add(GenreCondition.Parse(sValue));
		}
		return oGenreConditions;
	}
	public IList<GenreCondition> GetNarrowGenreConditions() {
		IList<GenreCondition> oGenreConditions = new List<GenreCondition>();
		foreach (string sValue in this.NarrowGenre.Split(SEPARATOR)) {
			if (string.IsNullOrEmpty(sValue)) continue;
			oGenreConditions.Add(GenreCondition.Parse(sValue));
		}
		return oGenreConditions;
	}

	#region IDisposable メンバ

	public void Dispose() {}

	#endregion
	
}
