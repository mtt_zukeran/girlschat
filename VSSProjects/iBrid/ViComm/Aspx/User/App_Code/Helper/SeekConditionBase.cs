﻿using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;

using iBridCommLib;

public enum SeekType : int {
	Next = -1,
	Current = 0,
	Previous = 1
}

[Serializable]
public abstract class SeekConditionBase {
	private NameValueCollection query = new NameValueCollection();
	protected NameValueCollection Query{get{return this.query;}}
	public NameValueCollection GetQuery() { return this.query; }
	
	private SeekType seekType = SeekType.Current;
	private bool useTemplate = false;
		
	public ulong SeekMode {
		get { return ulong.Parse(this.Query["seekmode"] ?? "-1"); }
		set { this.Query["seekmode"] = value.ToString(); }
	}

	public string Sort {
		get { return this.Query["sort"]; }
		set { this.Query["sort"] = value; }
	}
	public string PrevSeq {
		get { return this.Query["prevseq"]; }
		set { this.Query["prevseq"] = value; }
	}
	public string NextSeq {
		get { return this.Query["nextseq"]; }
		set { this.Query["nextseq"] = value; }
	}

	public bool UseTemplate {
		get { return useTemplate; }
		set { useTemplate = value; }
	}
	public SeekType SeekType {
		get { return seekType; }
		set { seekType = value; }
	}
	
	public SeekConditionBase(NameValueCollection pQuery) {
		this.Query["seekmode"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["seekmode"]));
		this.Query["sort"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sort"]));
		this.Query["prevseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["prevseq"]));
		this.Query["nextseq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["nextseq"]));
	}

	public override string ToString() {
		StringBuilder oStringBuilder = new StringBuilder();
		foreach(string sKey in this.query.Keys){
			if(string.IsNullOrEmpty(sKey)){
				continue;
			}
			
			string sValue = this.query[sKey];
			if(string.IsNullOrEmpty(sValue)){
				continue;
			}
			
			if(oStringBuilder.Length>0){
				oStringBuilder.Append("&");
			}
			oStringBuilder.AppendFormat("{0}={1}",sKey,sValue);
		}
		return oStringBuilder.ToString();
	}
	
	
}
