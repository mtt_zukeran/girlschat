﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 日付操作に関する機能を提供するHelperクラス
--	Progaram ID		: DateTimeHelper
--
--  Creation Date	: 2014.01.15
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public class DateTimeHelper {
	private static DateTime UNIX_EPOCH = new DateTime(1970,1,1,0,0,0,0);
	
	public static long GetUnixTimeStamp(DateTime pTargetTime) {
		pTargetTime = pTargetTime.ToUniversalTime();
		TimeSpan tsElapsedTime = pTargetTime - UNIX_EPOCH;
		return (long)tsElapsedTime.TotalSeconds;
	}
}