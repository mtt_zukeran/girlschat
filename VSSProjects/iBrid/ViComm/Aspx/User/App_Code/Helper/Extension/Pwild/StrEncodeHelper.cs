﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 文字列エンコードに関する機能を提供するHelperクラス
--	Progaram ID		: StrEncodeHelper
--
--  Creation Date	: 2014.01.16
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public class StrEncodeHelper {
	public class RFC3986Uri {
		public static readonly string UnreservedCharacters = "-._~";
		public static readonly string ReservedCharacters = UnreservedCharacters + ":/?#[]@!$&'()*+,;=";

		/// <summary>
		/// RFC3986に基づいてURLエンコードを行います。
		/// </summary>
		public static string EscapeDataString(string sToEscape,Encoding oEscapeEncoding) {
			return PercentEncodeString(sToEscape,UnreservedCharacters,oEscapeEncoding);
		}
		
		public static string EscapeDataString(string sToEscape) {
			return EscapeDataString(sToEscape,Encoding.UTF8);
		}

		/// <summary>
		/// RFC3986に基づいてURI文字列のURLエンコードを行います。
		/// </summary>
		public static string EscapeUriString(string sToEscape,Encoding oEscapeEncoding) {
			return PercentEncodeString(sToEscape,ReservedCharacters,oEscapeEncoding);
		}
		
		public static string EscapeUriString(string sToEscape) {
			return EscapeUriString(sToEscape,Encoding.UTF8);
		}

		private static string PercentEncodeString(string sToEscape,string sDontEscapeCharacters,Encoding oEscapeEncoding) {
			StringBuilder oEncodedString = new StringBuilder();

			foreach (char c in sToEscape) {
				if (('0' <= c && c <= '9') ||
					('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') ||
					(0 <= sDontEscapeCharacters.IndexOf(c))) {
					//エンコードしない文字の場合
					oEncodedString.Append(c);
				} else {
					//エンコードする文字の場合
					oEncodedString.Append(HexEscape(c,oEscapeEncoding));
				}
			}

			return oEncodedString.ToString();
		}

		/// <summary>
		/// 指定した文字のパーセントエンコーディング（百分率符号化）を行います。
		/// </summary>
		private static string HexEscape(char cChar,Encoding oEscapeEncoding) {
			if (255 < (int)cChar) {
				//cCharが255を超えるときはUri.HexEscapeが使えない
				StringBuilder oBuf = new StringBuilder();
				byte[] btCharacterBytes = oEscapeEncoding.GetBytes(cChar.ToString());
				
				foreach (byte b in btCharacterBytes) {
					oBuf.AppendFormat("%{0:X2}",b);
				}

				return oBuf.ToString();
			}

			return Uri.HexEscape(cChar);
		}
	}
}