﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ファイル操作に関する機能を提供するHelperクラス
--	Progaram ID		: FileHelper
--
--  Creation Date	: 2012.10.17
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public class FileHelper {
	public static byte[] GetFileBuffer(string sFileNm) {
		FileInfo oFileInfo = new FileInfo(sFileNm);
		byte[] oBuffer = new byte[oFileInfo.Length];
		using (FileStream oFileStream = oFileInfo.Open(FileMode.Open,FileAccess.Read,FileShare.Read)) {
			oFileStream.Read(oBuffer,0,oBuffer.Length);
			oFileStream.Close();
		}
		return oBuffer;
	}
}
