﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑの攻撃力や防御力に関する機能を提供するHelperクラス
--	Progaram ID		: GamePowerExpressionHelper
--
--  Creation Date	: 2011.07.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Web;

using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

/// <summary>
/// GameItemHelper の概要の説明です
/// </summary>
public class GamePowerExpressionHelper {
	public static string GameAttackPowerExpression(string sSiteCd,string sUserSeq,string sUserCharNo,string sForceCount) {
		return GameAttackPowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount,"","","");
	}
	
	public static string GameAttackPowerExpression(string sSiteCd ,string sUserSeq,string sUserCharNo,string sForceCount,string sBuyItemPower,string sBuyItemQuantity,string sBuyItemCategory) {
		string sAttackPower = "";
		
		int iForceCount;
		int.TryParse(sForceCount,out iForceCount);
		
		
		int iBuyItemPower;
		int iBuyItemQuantity;
		
		if(!String.IsNullOrEmpty(sBuyItemPower)) {
			int.TryParse(sBuyItemPower,out iBuyItemPower);
		}
		else {
			iBuyItemPower = 0;
		}
		
		if(!String.IsNullOrEmpty(sBuyItemQuantity)) {
			int.TryParse(sBuyItemQuantity,out iBuyItemQuantity);
		}
		else {
			iBuyItemQuantity = 0;
		}

		int iAccessoryCount	= iForceCount;
		int iClothesCount = iForceCount;
		int iVehicleCount = iForceCount;
		
		int iAttackPower = 0;
		
		int iBuyItemFlag = 0;
		
		using(PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			string sPresentFlag = ViCommConst.FLAG_OFF_STR;
			string sSort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_ATTACK;
			DataSet ds = oPossessionGameItem.GetPossessionEquipItems(sSiteCd,sUserSeq,sUserCharNo,sPresentFlag,sSort);
			
			int iItemPower;
			int iPossessionCount;
			string sItemCategory;
			
			foreach(DataRow dr in ds.Tables[0].Rows) {
				int.TryParse(iBridUtil.GetStringValue(dr["ATTACK_POWER"]),out iItemPower);
				int.TryParse(iBridUtil.GetStringValue(dr["POSSESSION_COUNT"]),out iPossessionCount);
				sItemCategory = iBridUtil.GetStringValue(dr["GAME_ITEM_CATEGORY_TYPE"]);
				
				if(sItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) && iAccessoryCount > 0) {
					if(iBuyItemPower == 0 || iBuyItemFlag > 0 || !sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY)) {
						iAttackPower += getPowerPossessionItem(iItemPower,iPossessionCount,ref iAccessoryCount);
					}
					else {
						iAttackPower += getPower(iItemPower,iPossessionCount,iBuyItemPower,iBuyItemQuantity,ref iAccessoryCount,ref iBuyItemFlag);
					}
				}
				else if(sItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) && iClothesCount > 0) {
					if(iBuyItemPower == 0 || iBuyItemFlag > 0 || !sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES)) {
						iAttackPower += getPowerPossessionItem(iItemPower,iPossessionCount,ref iClothesCount);
					}
					else {
						iAttackPower += getPower(iItemPower,iPossessionCount,iBuyItemPower,iBuyItemQuantity,ref iClothesCount,ref iBuyItemFlag);
					}
				}
				else if(sItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE) && iVehicleCount > 0) {
					if(iBuyItemPower == 0 || iBuyItemFlag > 0 || !sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)) {
						iAttackPower += getPowerPossessionItem(iItemPower,iPossessionCount,ref iVehicleCount);
					}
					else {
						iAttackPower += getPower(iItemPower,iPossessionCount,iBuyItemPower,iBuyItemQuantity,ref iVehicleCount,ref iBuyItemFlag);
					}
				}
				
				if(iAccessoryCount == 0 && iClothesCount == 0 && iVehicleCount == 0) {
					break;
				}
			}
			
			if(iBuyItemPower > 0 && iBuyItemFlag == 0) {
				if(sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) && iAccessoryCount > 0) {
					iAttackPower += getPowerBuyItem(iBuyItemPower,iBuyItemQuantity,ref iAccessoryCount,out iBuyItemFlag);
				}
				else if(sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) && iClothesCount > 0) {
					iAttackPower += getPowerBuyItem(iBuyItemPower,iBuyItemQuantity,ref iClothesCount,out iBuyItemFlag);
				}
				else if(sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE) && iVehicleCount > 0) {
					iAttackPower += getPowerBuyItem(iBuyItemPower,iBuyItemQuantity,ref iVehicleCount,out iBuyItemFlag);
				}
			}
			
			sAttackPower = iBridUtil.GetStringValue(iAttackPower);
			return sAttackPower;
		}
	}

	public static string GameDefencePowerExpression(string sSiteCd,string sUserSeq,string sUserCharNo,string sForceCount) {
		return GameDefencePowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount,"","","");
	}

	public static string GameDefencePowerExpression(string sSiteCd,string sUserSeq,string sUserCharNo,string sForceCount,string sBuyItemPower,string sBuyItemQuantity,string sBuyItemCategory) {
		string sDefencePower = "";

		int iForceCount;
		int.TryParse(sForceCount,out iForceCount);


		int iBuyItemPower;
		int iBuyItemQuantity;

		if (!String.IsNullOrEmpty(sBuyItemPower)) {
			int.TryParse(sBuyItemPower,out iBuyItemPower);
		} else {
			iBuyItemPower = 0;
		}

		if (!String.IsNullOrEmpty(sBuyItemQuantity)) {
			int.TryParse(sBuyItemQuantity,out iBuyItemQuantity);
		} else {
			iBuyItemQuantity = 0;
		}

		int iAccessoryCount = iForceCount;
		int iClothesCount = iForceCount;
		int iVehicleCount = iForceCount;

		int iDefencePower = 0;

		int iBuyItemFlag = 0;

		using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			string sPresentFlag = ViCommConst.FLAG_OFF_STR;
			string sSort = PwViCommConst.GameItemSort.GAME_ITEM_SORT_DEFENCE;
			DataSet ds = oPossessionGameItem.GetPossessionEquipItems(sSiteCd,sUserSeq,sUserCharNo,sPresentFlag,sSort);

			int iItemPower;
			int iPossessionCount;
			string sItemCategory;

			foreach (DataRow dr in ds.Tables[0].Rows) {
				int.TryParse(iBridUtil.GetStringValue(dr["DEFENCE_POWER"]),out iItemPower);
				int.TryParse(iBridUtil.GetStringValue(dr["POSSESSION_COUNT"]),out iPossessionCount);
				sItemCategory = iBridUtil.GetStringValue(dr["GAME_ITEM_CATEGORY_TYPE"]);

				if (sItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) && iAccessoryCount > 0) {
					if (iBuyItemPower == 0 || iBuyItemFlag > 0 || !sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY)) {
						iDefencePower += getPowerPossessionItem(iItemPower,iPossessionCount,ref iAccessoryCount);
					} else {
						iDefencePower += getPower(iItemPower,iPossessionCount,iBuyItemPower,iBuyItemQuantity,ref iAccessoryCount,ref iBuyItemFlag);
					}
				} else if (sItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) && iClothesCount > 0) {
					if (iBuyItemPower == 0 || iBuyItemFlag > 0 || !sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES)) {
						iDefencePower += getPowerPossessionItem(iItemPower,iPossessionCount,ref iClothesCount);
					} else {
						iDefencePower += getPower(iItemPower,iPossessionCount,iBuyItemPower,iBuyItemQuantity,ref iClothesCount,ref iBuyItemFlag);
					}
				} else if (sItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE) && iVehicleCount > 0) {
					if (iBuyItemPower == 0 || iBuyItemFlag > 0 || !sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE)) {
						iDefencePower += getPowerPossessionItem(iItemPower,iPossessionCount,ref iVehicleCount);
					} else {
						iDefencePower += getPower(iItemPower,iPossessionCount,iBuyItemPower,iBuyItemQuantity,ref iVehicleCount,ref iBuyItemFlag);
					}
				}

				if (iAccessoryCount == 0 && iClothesCount == 0 && iVehicleCount == 0) {
					break;
				}
			}

			if (iBuyItemPower > 0 && iBuyItemFlag == 0) {
				if (sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY) && iAccessoryCount > 0) {
					iDefencePower += getPowerBuyItem(iBuyItemPower,iBuyItemQuantity,ref iAccessoryCount,out iBuyItemFlag);
				}
				else if (sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES) && iClothesCount > 0) {
					iDefencePower += getPowerBuyItem(iBuyItemPower,iBuyItemQuantity,ref iClothesCount,out iBuyItemFlag);
				}
				else if (sBuyItemCategory.Equals(PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE) && iVehicleCount > 0) {
					iDefencePower += getPowerBuyItem(iBuyItemPower,iBuyItemQuantity,ref iVehicleCount,out iBuyItemFlag);
				}
			}

			sDefencePower = iBridUtil.GetStringValue(iDefencePower);
			return sDefencePower;
		}
	}
	
	private static int getPowerPossessionItem(int iItemPower,int iPossessionCount,ref int iForceCount) {
		int iPower = 0;

		int iEquipCount;

		if (iForceCount > iPossessionCount) {
			iEquipCount = iPossessionCount;
			iForceCount -= iPossessionCount;
		} else {
			iEquipCount = iForceCount;
			iForceCount = 0;
		}

		iPower = iItemPower * iEquipCount;
		
		return iPower;
	}
	
	private static int getPowerBuyItem(int iBuyItemPower, int iBuyItemQuantity, ref int iForceCount,out int iBuyFlag) {
		int iPower = 0;
		
		int iBuyItemEquipCount;
		
		if(iForceCount > iBuyItemQuantity) {
			iBuyItemEquipCount = iBuyItemQuantity;
			iForceCount -= iBuyItemQuantity;
		}
		else {
			iBuyItemEquipCount = iForceCount;
			iForceCount = 0;
		}
		
		iBuyFlag = 1;
		
		iPower = iBuyItemPower * iBuyItemEquipCount;
		
		return iPower;
	}
	
	private static int getPower(int iItemPower,int iPossessionCount,int iBuyItemPower,int iBuyItemQuantity,ref int iForceCount,ref int iBuyItemFlag) {
		int iPower = 0;
		
		if(iBuyItemPower > iItemPower) {
			iPower += getPowerBuyItem(iBuyItemPower,iBuyItemQuantity,ref iForceCount,out iBuyItemFlag);
		}
		
		if(iForceCount > 0) {
			iPower += getPowerPossessionItem(iItemPower,iPossessionCount,ref iForceCount);
		}
		
		return iPower;
	}
}
