﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 入力チェックに関する機能を提供するHelperクラス
--	Progaram ID		: PwValidator
--
--  Creation Date	: 2011.10.13
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Web;

using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public class PwValidator {

	/// -----------------------------------------------------------------------------
	/// <summary>
	///     文字列が数値であるかどうかを返します。</summary>
	/// <param name="stTarget">
	///     検査対象となる文字列。<param>
	/// <returns>
	///     指定した文字列が数値であれば true。それ以外は false。</returns>
	/// -----------------------------------------------------------------------------
	public static bool IsNumeric(string stTarget) {
		double dNullable;

		return double.TryParse(
			stTarget,
			System.Globalization.NumberStyles.Any,
			null,
			out dNullable
		);
	}
}