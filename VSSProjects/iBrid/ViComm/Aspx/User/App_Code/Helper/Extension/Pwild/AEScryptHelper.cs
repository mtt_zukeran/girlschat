﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 暗号化に関する機能を提供するHelperクラス
--	Progaram ID		: AEScryptHelper
--
--  Creation Date	: 2014.01.15
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public class AEScryptHelper {
	public static String _key = @"メール通知アプリ";			//秘密鍵
	private static readonly String _iv = @"AESAPPCLIENT_KEY";	//公開鍵

	public AEScryptHelper() {
	}

	private static string Key {
		get {
			String newKey = _key;
			if (!_key.Equals("") && _key.Length != 32) {
				newKey = MD5(_key);
				newKey = SHA256(newKey);
			}
			return newKey;
		}
	}

	private static string IV {
		get {
			return _iv;
		}
	}

	public static string Encrypt(string text) {
		byte[] data = null;
		if (!AEScryptHelper.IsBase64(text)) {
			data = getByteWithString(HttpUtility.UrlDecode(text.Trim()));
		} else {
			return null;
		}

		try {
			RijndaelManaged rjm = initStreamCipher();

			byte[] cipherText = null;
			using (MemoryStream ms = new MemoryStream()) {
				using (CryptoStream cs = new CryptoStream(ms,rjm.CreateEncryptor(),CryptoStreamMode.Write)) {
					cs.Write(data,0,data.Length);
				}
				cipherText = ms.ToArray();
			}

			String hex = "";

			for (int i = 0;i < cipherText.Length;i++) {
				String c = Convert.ToString(cipherText[i],16);
				if (c.Length % 2 == 1) {
					c = "0" + c;
				}
				hex += c;
			}

			return Convert.ToBase64String(cipherText);
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"Encrypt",text);
		}
		return null;
	}

	public static String Decrypt(String str) {
		Byte[] data = null;
		if (IsBase64(str)) {
			data = Convert.FromBase64String(str.Trim());
		} else {
			return null;
		}

		try {
			RijndaelManaged rjm = initStreamCipher();

			byte[] plainText = null;
			using (MemoryStream ms = new MemoryStream()) {
				using (CryptoStream cs = new CryptoStream(ms,rjm.CreateDecryptor(),CryptoStreamMode.Write)) {
					cs.Write(data,0,data.Length);
				}
				plainText = ms.ToArray();
			}

			return getStringWithByte(plainText);
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"Decrypt",str);
		}
		return null;
	}

	private static RijndaelManaged initStreamCipher() {
		byte[] key = getByteWithByteString(Key);
		byte[] iv = getByteWithString(IV);

		RijndaelManaged rjm = null;
		try {
			rjm = new RijndaelManaged();
			rjm.Padding = PaddingMode.Zeros;
			rjm.Mode = CipherMode.CBC;
			rjm.BlockSize = 128;
			rjm.FeedbackSize = 8;
			rjm.KeySize = 128;
			rjm.Key = key;
			rjm.IV = iv;
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"initStreamCipher","");
		}
		return rjm;
	}

	public static bool IsBase64(String base64String) {
		if (base64String.Replace(" ","").Length % 4 != 0)
			return false;
		try {
			Convert.FromBase64String(base64String);
			if (!base64String.ToLower().Equals(base64String)) {
				return true;
			}
		} catch (Exception ex) {
			//
		}
		return false;
	}

	public static Byte[] getByteWithString(String str) {
		Byte[] byteValue = System.Text.UTF8Encoding.UTF8.GetBytes(str);
		return byteValue;
	}

	public static byte[] getByteWithByteString(String str) {
		if (str.Length % 2 != 0)
			return null;
		byte[] buffer = new byte[str.Length / 2];
		for (int i = 0;i < str.Length / 2;i++) {
			buffer[i] = Convert.ToByte(str.Substring(i * 2,2),16);
		}
		return buffer;
	}

	public static String getStringWithByte(Byte[] byteValue) {
		String str = System.Text.UTF8Encoding.UTF8.GetString(byteValue);
		return str;
	}

	public static String getStringWithStringByte(byte[] buffer) {
		String str = "";
		for (int i = 0;i < buffer.Length;i++) {
			str += Convert.ToString(buffer[i],16);
		}
		return str;
	}

	public static String SHA256(String str) {
		byte[] byteValue = Encoding.UTF8.GetBytes(str);

		//ハッシュ値を取得する
		SHA256 crypto = new SHA256Managed();	//SHA256CryptoServiceProvider
		byte[] hashValue = crypto.ComputeHash(byteValue);

		//バイト配列をUTF8エンコードで文字列化
		StringBuilder hashedText = new StringBuilder();
		for (int i = 0;i < hashValue.Length;i++) {
			hashedText.AppendFormat("{0:X2}",hashValue[i]);
		}
		return hashedText.ToString().ToLower();
	}

	public static String MD5(String str) {
		byte[] byteValue = Encoding.UTF8.GetBytes(str);

		//ハッシュ値を取得する
		MD5 crypto = new MD5CryptoServiceProvider();
		byte[] hashValue = crypto.ComputeHash(byteValue);

		//バイト配列をUTF8エンコードで文字列化
		StringBuilder hashedText = new StringBuilder();
		for (int i = 0;i < hashValue.Length;i++) {
			hashedText.AppendFormat("{0:X2}",hashValue[i]);
		}
		return hashedText.ToString().ToLower();
	}

	public static bool IsNotContainUpperChar(string pStr) {
		if (string.IsNullOrEmpty(pStr)) {
			return false;
		}

		foreach (char c in pStr) {
			if (Char.IsUpper(c)) {
				return false;
			}
		}
		
		return true;
	}
}