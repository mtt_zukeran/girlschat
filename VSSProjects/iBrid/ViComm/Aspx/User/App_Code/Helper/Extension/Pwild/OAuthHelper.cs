﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: SNS OAuthに関する機能を提供するHelperクラス
--	Progaram ID		: OAuthHelper
--
--  Creation Date	: 2014.01.17
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Web;
using System.Net;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public class OAuthHelper {
	public class Twitter {
		public static string GenerateSignature(SortedDictionary<string,string> pParamDictionary,string pRequestUrl,string pConsumerSecret,string pOAuthTokenSecret) {
			string sSignature = string.Empty;

			string sBaseStrParams = string.Empty;

			sBaseStrParams = GenerateParameta(pParamDictionary);

			string sBaseStr = "GET&" + StrEncodeHelper.RFC3986Uri.EscapeDataString(pRequestUrl) + "&" + StrEncodeHelper.RFC3986Uri.EscapeDataString(sBaseStrParams);

			string sStringKey = StrEncodeHelper.RFC3986Uri.EscapeDataString(pConsumerSecret) + "&";
			
			if (!string.IsNullOrEmpty(pOAuthTokenSecret)) {
				sStringKey = sStringKey + StrEncodeHelper.RFC3986Uri.EscapeDataString(pOAuthTokenSecret);
			}

			byte[] oDataByteArray = Encoding.UTF8.GetBytes(sBaseStr);
			byte[] oKeyMaterialArray = Encoding.UTF8.GetBytes(sStringKey);

			HMACSHA1 oHashAlgorithm = new HMACSHA1(oKeyMaterialArray);
			byte[] oHashValue = oHashAlgorithm.ComputeHash(oDataByteArray);

			oHashAlgorithm.Clear();
			sSignature = Convert.ToBase64String(oHashValue);

			return sSignature;
		}

		public static string GenerateParameta(SortedDictionary<string,string> pParamDictionary) {
			string sParams = string.Empty;

			foreach (KeyValuePair<string,string> oKeyValuePair in pParamDictionary) {
				sParams += (sParams.Length > 0 ? "&" : string.Empty) + StrEncodeHelper.RFC3986Uri.EscapeDataString(oKeyValuePair.Key) + "=" + StrEncodeHelper.RFC3986Uri.EscapeDataString(oKeyValuePair.Value);
			}

			return sParams;
		}

		public static string PostRequest(string pRequestUrl,string pParams) {
			string sResponse = string.Empty;
			HttpWebRequest oWebRequest = (HttpWebRequest)WebRequest.Create(pRequestUrl + "?" + pParams);
			oWebRequest.Method = "GET";

			HttpWebResponse oWebResponse = oWebRequest.GetResponse() as HttpWebResponse;

			using (Stream responseStream = oWebResponse.GetResponseStream()) {
				using (StreamReader oReader = new StreamReader(oWebResponse.GetResponseStream())) {
					string sFirstLine = oReader.ReadLine();

					sResponse = sFirstLine;
				}
			}

			return sResponse;
		}

		public static Dictionary<string,string> GetToken(string pValue) {
			Dictionary<string,string> oDictionary = new Dictionary<string,string>();

			string[] sPairArr = pValue.Split('&');
			string[] sTempArr = new string[2];

			foreach (string sPair in sPairArr) {
				sTempArr = sPair.Split('=');
				oDictionary.Add(HttpUtility.UrlDecode(sTempArr[0]),HttpUtility.UrlDecode(sTempArr[1]));
			}

			return oDictionary;
		}
	}
}