﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ用のFlashLiteに関する機能を提供するHelperクラス
--	Progaram ID		: GameFlashLiteHelper
--
--  Creation Date	: 2011.11.02
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;

using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public class GameFlashLiteHelper {
	private static readonly Encoding _enc = Encoding.GetEncoding("Shift_JIS");

	public static void ResponseFlashSocialGame(string pFileNm,string pNextPageNm,string pGetParamStr,NameValueCollection pSwfParam) {
		byte[] oBuffer = GetSwfBuffer(pFileNm);
		string sNextUrl = GetNextUrl(SessionObjs.Current,pNextPageNm,pGetParamStr);
		pSwfParam.Add("nextUrl",sNextUrl);
		IDictionary<string,string> oParam = GetParams(pSwfParam);
		byte[] oSwfBuffer = RewriteSwfBuffer(oBuffer,oParam);
		OutputSwf(oSwfBuffer);
	}

	public static void ResponseFlashSocialGame(string pFileNm,IDictionary<string,string> pParam) {
		byte[] oBuffer = GetSwfBuffer(pFileNm);
		byte[] oSwfBuffer = RewriteSwfBuffer(oBuffer,pParam);
		OutputSwf(oSwfBuffer);
	}

	private static byte[] GetSwfBuffer(string pFileNm) {
		string sWebPhisicalDir = SessionObjs.Current.site.webPhisicalDir;
		string sFileDir = Path.Combine(sWebPhisicalDir,PwViCommConst.GAME_FLASH_DIR);
		string sFilePath = Path.Combine(sFileDir,pFileNm);
		byte[] oBuffer;

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			FileInfo oFileInfo = new FileInfo(sFilePath);
			oBuffer = new byte[oFileInfo.Length];
			using (FileStream oFileStream = oFileInfo.Open(FileMode.Open,FileAccess.Read,FileShare.Read)) {
				oFileStream.Read(oBuffer,0,oBuffer.Length);
				oFileStream.Close();
			}
		}

		return oBuffer;
	}

	public static void OutputSwf(byte[] pSwfBuffer) {
		HttpResponse oHttpResponse = HttpContext.Current.Response;
		oHttpResponse.Clear();
		oHttpResponse.AddHeader("Content-Type","application/x-shockwave-flash");
		oHttpResponse.OutputStream.Write(pSwfBuffer,0,pSwfBuffer.Length);
		oHttpResponse.Flush();
		oHttpResponse.End();
	}

	private static string GetNextUrl(SessionObjs pSessionObjs,string pNextPageNm,string pGetParamStr) {
		string sNextPageUrl = pNextPageNm + "?" + pGetParamStr;

		string sNextUrl = pSessionObjs.GetNavigateAbsoluteUrl(sNextPageUrl);
		
		return sNextUrl;
	}

	private static IDictionary<string,string> GetParams(NameValueCollection oGameParam) {
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		
		List<string> oGetParamList = new List<string>();
		string value = string.Empty;
		string[] keys = oGameParam.AllKeys;
		if(keys.Length > 0) {
			foreach(string key in keys) {
				value = oGameParam[key];
				oParam.Add(key,value);
			}
		}

		return oParam;
	}
	
	/////////////////
	/// <summary>
	/// SWFにパラメータを埋め込む
	/// </summary>
	/// <param name="pSrcBuffer">元とのなるSWFバッファ</param>
	/// <param name="oParam">埋め込む対象のパラメータ</param>
	/// <returns>パラメータを埋め込んだSWFバッファ</returns>
	public static byte[] RewriteSwfBuffer(byte[] pSwfBuffer,IDictionary<string,string> oParam) {

		byte[] oTagBuffer = GenerateDoActionTag(oParam);
		int iHeaderLength = GetHeaderLength(pSwfBuffer);
		int iNewSwfBufferSize = pSwfBuffer.Length + oTagBuffer.Length;
		byte[] oLengthBuffer = BitConverter.GetBytes((int)(iNewSwfBufferSize));

		using (MemoryStream oStream = new MemoryStream(iNewSwfBufferSize)) {
			oStream.Write(pSwfBuffer,0,4);
			// ★パラメータ埋め込み後のSWFサイズ
			oStream.Write(oLengthBuffer,0,oLengthBuffer.Length);
			oStream.Write(pSwfBuffer,8,iHeaderLength - 8);
			// ★DoActionタグ
			oStream.Write(oTagBuffer,0,oTagBuffer.Length);
			oStream.Write(pSwfBuffer,iHeaderLength,pSwfBuffer.Length - iHeaderLength);
			return oStream.GetBuffer();
		}
	}

	private static string GetGameFileNm(string pSiteCd,string pGameType,string pSexCd) {
		using (Game oGame = new Game()) {
			using (DataSet oGameDataSet = oGame.GetOne(pSiteCd,pGameType,pSexCd)) {
				if (oGameDataSet.Tables[0].Rows.Count == 0) {
					throw new ApplicationException(string.Format("DATA NOT FOUND.(SITE_CD:{0},GAME_TYPE:{1},SEX_CD:{2}",pSiteCd,pGameType,pSexCd));
				}

				DataRow oGameDataRow = oGameDataSet.Tables[0].Rows[0];
				return iBridUtil.GetStringValue(oGameDataRow["FILE_NM"]);
			}
		}
	}

	/// <summary>
	/// SWFのヘッダ長を取得する

	/// </summary>
	/// <param name="pSwfBuffer">対象となるSWFバッファ</param>
	/// <returns>SWFのヘッダ長</returns>
	private static int GetHeaderLength(byte[] pSwfBuffer) {
		int bRectBit = (pSwfBuffer[8] >> 1) + 5;
		return (int)Math.Ceiling((((8 - (bRectBit & 7)) & 7) + bRectBit) / (decimal)8) + 12;
	}

	/// <summary>
	/// DoAction タグを生成する

	/// </summary>
	/// <param name="pParams">埋め込む変数名、及び変数値のﾘｽﾄ</param>
	/// <returns>DoActionタグ バッファ</returns>
	private static byte[] GenerateDoActionTag(IDictionary<string,string> pParams) {

		List<byte> oTagBodyBuffer = new List<byte>();
		foreach (string sKey in pParams.Keys) {
			string sValue = pParams[sKey];
			// ActionPush 変数名のプッシュ命令
			oTagBodyBuffer.AddRange(GenerateActionPush(sKey));
			// ActionPush 変数値のプッシュ命令
			oTagBodyBuffer.AddRange(GenerateActionPush(sValue));
			// ActionSetVariable 変数の設定命令
			oTagBodyBuffer.Add((byte)0x1d);
		}
		oTagBodyBuffer.Add((byte)0x00);


		List<byte> oTagBuffer = new List<byte>();

		oTagBuffer.Add((byte)0x3f);
		oTagBuffer.Add((byte)0x03);
		// TagBody のサイズ(int=4byte)
		oTagBuffer.AddRange(BitConverter.GetBytes(oTagBodyBuffer.Count));
		// TagBody
		oTagBuffer.AddRange(oTagBodyBuffer);

		return oTagBuffer.ToArray();
	}

	/// <summary>
	/// ActionPush命令を作成する
	/// </summary>
	/// <param name="pValue">ActionPushでスタックにプッシュさせる対象値</param>
	/// <returns>ActionPush命令バッファ</returns>
	private static byte[] GenerateActionPush(string pValue) {

		byte[] oBuffer = _enc.GetBytes(pValue);

		List<byte> oTagBuffer = new List<byte>();

		// ActionPush
		oTagBuffer.Add((byte)0x96);
		// ActionPush命令のサイズ(short=2byte)
		oTagBuffer.AddRange(BitConverter.GetBytes((short)(oBuffer.Length + 2)));
		// 0x00 : string
		oTagBuffer.Add((byte)0x00);
		// value
		oTagBuffer.AddRange(oBuffer);
		// null
		oTagBuffer.Add((byte)0x00);

		return oTagBuffer.ToArray();
	}
}