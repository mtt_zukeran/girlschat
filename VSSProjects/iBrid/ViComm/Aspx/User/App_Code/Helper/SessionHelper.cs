﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

/// <summary>
/// SessionHelper の概要の説明です



/// </summary>
public class SessionHelper {
	public string sexCd;
	private string adCd;
	private string carrierType;
	private string browserId;
	private string introducerFriendCd;

	public SessionHelper(string pSexCd,string pCarrierType,string pBrowser,string pAdCd,string pIntroducerFriendCd) {
		sexCd = pSexCd;
		carrierType = pCarrierType;
		browserId = pBrowser;
		adCd = pAdCd;
		introducerFriendCd = pIntroducerFriendCd;
	}

	public void CreateSessionInfo(string pHostNm,string pSessionId,bool pCrawler,bool pCookieLess,out SessionObjs pNewObj) {
		string sPattern;

		if (sexCd.Equals(ViCommConst.MAN)) {
			if (!carrierType.Equals(ViCommConst.KDDI)) {
				sPattern = @"(<!--[^->]+-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<body.*?>|<form.*?>|<\/form.*?>)";
			} else {
				sPattern = @"(<!--[^->]+-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<\/head.*?>|<title>|<body.*?>|<form.*?>|<\/form.*?>)";
			}
		} else {
			if (!carrierType.Equals(ViCommConst.KDDI)) {
				sPattern = @"(<!--[^->]+-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<body.*?>|<form.*?>|<\/form.*?>|<a +href.*?>)";
			} else {
				sPattern = @"(<!--[^->]+-->|(\$\w{1,})\(?(;?|[^;]*;,[^)]*|[^)""]*)?\)?;|<div\sid=""?([^>]*);""?>|<div\s?.*?>|<\/div.*?>|<html>|<\/head.*?>|<title>|<body.*?>|<form.*?>|<\/form.*?>|<a +href.*?>)";
			}
		}

		bool bDebug = iBridUtil.GetStringValue(HttpContext.Current.Request.QueryString["design"]).Equals("1");

		SessionObjs prevObj = (SessionObjs)HttpContext.Current.Session["objs"];

		if (prevObj is SessionObjs) {
			if (adCd.Equals(string.Empty)) {
				adCd = prevObj.adCd;
			}
			pCrawler = prevObj.isCrawler;
			pCookieLess = prevObj.cookieLess;
		}

		if (sexCd.Equals(ViCommConst.MAN)) {
			SessionMan userObjs;
			if (prevObj is SessionWoman && ((SessionWoman)prevObj).originalManObj is SessionMan && ((SessionWoman)prevObj).originalManObj.logined) {
				userObjs = ((SessionWoman)prevObj).originalManObj;
				HttpContext.Current.Session["objs"] = null;
				HttpContext.Current.Session["objs"] = userObjs;
			} else {
				userObjs = new SessionMan(
											   pHostNm,
											   pSessionId,
											   carrierType,
											   browserId,
											   HttpContext.Current.Request.UserAgent,
											   adCd,
											   HttpContext.Current.Request.QueryString,
											   pCrawler,
											   pCookieLess
										   );

				HttpContext.Current.Session["objs"] = null;
				HttpContext.Current.Session["objs"] = userObjs;
				userObjs.designDebug = bDebug;
				userObjs.rakutenDebug = (HttpContext.Current.Request.QueryString["rakuten_debug"] ?? string.Empty).Equals("1");				
				userObjs.introducerFriendCd = this.introducerFriendCd;

				ParseMan oParseMan = new ParseMan(
						sPattern,
						pCrawler,
						pSessionId,
						HttpContext.Current.Request.Url.Host,			// 開発環境でも実行できるようにするため
						"user/vicomm/man",
						RegexOptions.IgnoreCase,
						userObjs.carrier,
						userObjs);
				ParseHTML oParseHtml = new ParseHTML(oParseMan);
				oParseMan.parseContainer = oParseHtml;
				userObjs.parseContainer = oParseHtml;
				userObjs.sysType = "/ViComm/man";
			}
		} else {
			SessionWoman userObjs;
			if (prevObj is SessionMan && ((SessionMan)prevObj).originalWomanObj is SessionWoman && ((SessionMan)prevObj).originalWomanObj.logined) {
				userObjs = ((SessionMan)prevObj).originalWomanObj;
				HttpContext.Current.Session["objs"] = null;
				HttpContext.Current.Session["objs"] = userObjs;
			} else {
				userObjs = new SessionWoman(
												pHostNm,
												pSessionId,
												carrierType,
												browserId,
												HttpContext.Current.Request.UserAgent,
												adCd,
												HttpContext.Current.Request.QueryString,
												pCrawler,
												pCookieLess
											);

				HttpContext.Current.Session["objs"] = null;
				HttpContext.Current.Session["objs"] = userObjs;
				userObjs.designDebug = bDebug;
				userObjs.introducerFriendCd = this.introducerFriendCd;

				ParseWoman oParseWoman = new ParseWoman(
						sPattern,
						pCrawler,
						pSessionId,
						HttpContext.Current.Request.Url.Host,			// 開発環境でも実行できるようにするため
						"user/vicomm/woman",
						RegexOptions.IgnoreCase,
						userObjs.carrier,
						userObjs);
				ParseHTML oParseHtml = new ParseHTML(oParseWoman);
				oParseWoman.parseContainer = oParseHtml;
				userObjs.parseContainer = oParseHtml;
				userObjs.sysType = "/ViComm/woman";
			}
		}
		pNewObj = (SessionObjs)HttpContext.Current.Session["objs"];

		if (prevObj is SessionObjs) {
			pNewObj.mainMenu = prevObj.mainMenu;
			pNewObj.currentIModeId = prevObj.currentIModeId;
			pNewObj.adminFlg = prevObj.adminFlg;
			pNewObj.designDebug = prevObj.designDebug;
			pNewObj.introducerFriendCd = prevObj.introducerFriendCd;
			pNewObj.affiliaterCd = prevObj.affiliaterCd;
			pNewObj.affiliateCompany = prevObj.affiliateCompany;
			pNewObj.rankSign = prevObj.rankSign;
			pNewObj.isFrameVw = prevObj.isFrameVw;
			pNewObj.breadcrumbList = prevObj.breadcrumbList;
			pNewObj.snsType = prevObj.snsType;
			pNewObj.snsId = prevObj.snsId;
			pNewObj.pageViewCount = prevObj.pageViewCount;

			if (pNewObj is SessionMan) {
				if (prevObj is SessionWoman) {
					((SessionMan)pNewObj).originalWomanObj = (SessionWoman)prevObj;
				} else if (prevObj.IsImpersonated) {
					// 男性→男性の場合、親となる女性のｵﾌﾞｼﾞｪｸﾄを引き継ぐ
					((SessionMan)pNewObj).originalWomanObj = (SessionWoman)prevObj.GetOriginalObj();
				}
			}
			if (pNewObj is SessionWoman) {
				if (prevObj is SessionMan) {
					((SessionWoman)pNewObj).originalManObj = (SessionMan)prevObj;
				} else if (prevObj.IsImpersonated) {
					// 女性→女性の場合、親となる男性のｵﾌﾞｼﾞｪｸﾄを引き継ぐ
					((SessionWoman)pNewObj).originalManObj = (SessionMan)prevObj.GetOriginalObj();
				}
			}
		}
		prevObj = null;
	}
}
