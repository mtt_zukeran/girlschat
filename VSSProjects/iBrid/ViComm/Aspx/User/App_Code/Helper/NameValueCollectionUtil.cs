﻿using System;
using System.Collections.Specialized;


/// <summary>
/// NameValueCollectionUtil の概要の説明です
/// </summary>
public static class NameValueCollectionUtil
{
	//動的キャストができないためGenericsで実装できない…ので、多態性で解決します

	/// <summary>
	/// 値の取得
	/// </summary>
	/// <param name="nvc"></param>
	/// <param name="key">キー</param>
	/// <param name="unMatchValue">キーにヒットしなかった、またはCastできなかった場合の値</param>
	/// <returns></returns>
	public static bool GetValue(NameValueCollection nvc, string key, bool unMatchValue)
	{
		bool result;
		return bool.TryParse(nvc[key], out result) ? result : unMatchValue;
	}

	/// <summary>
	/// 値の取得
	/// </summary>
	/// <param name="nvc"></param>
	/// <param name="key">キー</param>
	/// <param name="unMatchValue">キーにヒットしなかった、またはCastできなかった場合の値</param>
	/// <returns></returns>
	public static int GetValue(NameValueCollection nvc, string key, int unMatchValue)
	{
		int result;
		return int.TryParse(nvc[key], out result) ? result : unMatchValue;
	}

	/// <summary>
	/// 値を取得
	/// </summary>
	/// <param name="nvc"></param>
	/// <param name="key">キー</param>
	/// <param name="unMatchValue">キーにヒットしなかった、またはCastできなかった場合の値</param>
	/// <returns></returns>
	public static string GetValue(NameValueCollection nvc, string key, string unMatchValue)
	{
		return string.IsNullOrEmpty(nvc[key]) ? unMatchValue : nvc[key];
	}

}
