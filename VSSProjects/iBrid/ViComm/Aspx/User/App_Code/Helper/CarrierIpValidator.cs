﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using ViComm;
using iBridCommLib;

public class CarrierIpValidator {
	private static readonly object _syncRoot = 1;
	private static IDictionary<string, IList<string>> _carrierIpRepos = null;
	
	public static void Clear(){
		lock(_syncRoot){
			_carrierIpRepos = null;
		}
	}
	
	public static void Configure(string pDirectory){
		if(_carrierIpRepos == null){
			// ダブルチェックロッキング イディオム
			lock (_syncRoot) {
				if(_carrierIpRepos == null){
					using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, string.Empty, ViCommConst.FILE_UPLOAD_PASSWORD)) {
						if (!Directory.Exists(pDirectory)) return;
						
						_carrierIpRepos = new Dictionary<string, IList<string>>();
						
						foreach (string sFileNm in Directory.GetFiles(pDirectory, "*.ip", SearchOption.TopDirectoryOnly)) {
							IList<string> oCarrierIpList = new List<string>();
							using (StreamReader sr = new StreamReader(sFileNm, SysConst.EncodingShiftJIS)) {
								while (sr.Peek() >= 0) {
									string sLine = sr.ReadLine().Trim();
									if (!sLine.StartsWith("#") && !string.IsNullOrEmpty(sLine)) {
										oCarrierIpList.Add(sLine);
									}
								}
							}
							FileInfo oFileInfo = new FileInfo(sFileNm);
							_carrierIpRepos.Add(oFileInfo.Name.Replace(oFileInfo.Extension, string.Empty).ToLower(), oCarrierIpList);
						}
					}
					
				}
			}
		}
	}
	
	public static bool Validate(string pCarrierCd,string pIpAddress){
		if(IsBypassAddress(pIpAddress)) return true;
		
		bool bValidateCarrierIp = false;

		if (_carrierIpRepos == null || !_carrierIpRepos.ContainsKey(pCarrierCd)) {
			// 設定が存在しない場合はチェックしない(全許容)
			bValidateCarrierIp = true;
		} else {
			foreach (string sIpAddr in _carrierIpRepos[pCarrierCd]) {
				if (SysPrograms.CheckNetMask(pIpAddress, sIpAddr)) {
					bValidateCarrierIp = true;
					break;
				}
			}
		}

		return bValidateCarrierIp;
	}
	
	public static bool IsBypassAddress(string pIpAddress){
		if (_carrierIpRepos != null && _carrierIpRepos.ContainsKey("bypass")) {
			foreach (string sIpAddr in _carrierIpRepos["bypass"]) {
				if (SysPrograms.CheckNetMask(pIpAddress, sIpAddr)) {
					return true;
				}
			}
		}
		return false;
	}
	
}
