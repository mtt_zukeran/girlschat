﻿using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;

/// <summary>
/// MovieHelper の概要の説明です

/// </summary>
public class MovieHelper {
	public MovieHelper() {
	}

	/// <summary>
	/// RangeRequestかどうかを示す値を取得する。


	/// </summary>
	/// <returns>
	/// RangeRequestの２回目以降のリクエストの場合はtrue。それ以外はfalse。


	/// </returns>
	public static bool IsRangeRequest() {
		if (HttpContext.Current == null || HttpContext.Current.Request == null)
			return false;

		string sHttpRange = HttpContext.Current.Request.Headers["Range"] ?? string.Empty;
		bool bIsRangeRequest = !sHttpRange.Equals(string.Empty) && !sHttpRange.ToLower().StartsWith("bytes=0-");

		if (EventLogWriter.EnabledDebug) {
			StringBuilder oHeaderString = new StringBuilder(Environment.NewLine);
			foreach (string sKey in HttpContext.Current.Request.Headers.Keys) {
				oHeaderString.AppendFormat("{0}:{1}",sKey,HttpContext.Current.Request.Headers[sKey]).AppendLine();
			}

			EventLogWriter.Debug(oHeaderString);
		}

		if (HttpContext.Current.Request.HttpMethod.ToLower().Equals("head")) {
			bIsRangeRequest = true;
		}


		return bIsRangeRequest;
	}

	public static bool Download(string pMovieSeq,string pUserSeq,string pUserCharNo) {
		return Download(pMovieSeq,pUserSeq,pUserCharNo,ViCommConst.OPERATOR);
	}
	public static bool Download(string pMovieSeq,string pUserSeq,string pUserCharNo,string pSexCd) {
		ParseHTML oParseHTML = SessionObjs.Current.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,pSexCd);
				return true;
			} else {
				return false;
			}
		}
	}

	public static void ResponseMovie(string pLoginId,string pFileNm,string pFullPath,long pSize,string pSexCd) {
		string sVirtualPath = string.Empty;
		string sVirtualPathSmartphone = string.Empty;
		string sVirtualPathIPhone = string.Empty;
		if (pSexCd.Equals(ViCommConst.MAN)) {
			sVirtualPath = string.Format("movie/{0}/man/{1}{2}",SessionObjs.Current.site.siteCd,pFileNm,ViCommConst.MOVIE_FOODER2);
			sVirtualPathSmartphone = string.Format("movie/{0}/man/{1}{2}",SessionObjs.Current.site.siteCd,pFileNm,ViCommConst.MOVIE_FOODER);
			sVirtualPathIPhone = string.Format("movie/{0}/man/{1}{2}",SessionObjs.Current.site.siteCd,pFileNm,ViCommConst.MOVIE_FOODER3);
		} else {
			sVirtualPath = string.Format("movie/{0}/operator/{1}/{2}{3}",SessionObjs.Current.site.siteCd,pLoginId,pFileNm,ViCommConst.MOVIE_FOODER2);
			sVirtualPathSmartphone = string.Format("movie/{0}/operator/{1}/{2}{3}",SessionObjs.Current.site.siteCd,pLoginId,pFileNm,ViCommConst.MOVIE_FOODER);
			sVirtualPathIPhone = string.Format("movie/{0}/operator/{1}/{2}{3}",SessionObjs.Current.site.siteCd,pLoginId,pFileNm,ViCommConst.MOVIE_FOODER3);
		}

		switch (SessionObjs.Current.carrier) {
			case ViCommConst.IPHONE:
				if (ViCommConst.Browsers.IPHONE_OLD.Equals(SessionObjs.Current.browserId)) {
					HttpContext.Current.Response.Redirect(SessionObjs.Current.GetNavigateUrl(
							SessionObjs.Current.root + SessionObjs.Current.sysType,
							SessionObjs.Current.sessionId,
							sVirtualPathSmartphone));
				}else{
					HttpContext.Current.Response.Redirect(SessionObjs.Current.GetNavigateUrl(
							SessionObjs.Current.root + SessionObjs.Current.sysType,
							SessionObjs.Current.sessionId,
							sVirtualPathIPhone));				
				}
				break;

			case ViCommConst.KDDI:
				HttpContext.Current.Response.ContentType = "video/3gpp2";
				if (pSize <= 99000) {
					HttpContext.Current.Response.AddHeader("Content-Disposition","attachment; filename=\"" + pFileNm + ViCommConst.MOVIE_FOODER2 + "\"");
				} else {
					HttpContext.Current.Response.Redirect(SessionObjs.Current.GetNavigateUrl(
						SessionObjs.Current.root + SessionObjs.Current.sysType,
						SessionObjs.Current.sessionId,
						sVirtualPath));
				}
				break;

			case ViCommConst.SOFTBANK:
				HttpContext.Current.Response.ContentType = "video/3gpp";
				HttpContext.Current.Response.AddHeader("Content-Disposition","attachment; filename=\"" + pFileNm + ViCommConst.MOVIE_FOODER + "\"");
				HttpContext.Current.Response.AddHeader("Cache-Control","no-store");
				break;

			case ViCommConst.ANDROID:
				bool bAndroidMovieNotDownload = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["androidMovieNotDownload"]).Equals("1");
				if (bAndroidMovieNotDownload) {
					//ダウンロードさせずに再生するアプリを選択
					HttpContext.Current.Response.Redirect(SessionObjs.Current.GetNavigateUrl(
							SessionObjs.Current.root + SessionObjs.Current.sysType,
							SessionObjs.Current.sessionId,
							sVirtualPathSmartphone));
				} else {
					//ダウンロードが始まる
					HttpContext.Current.Response.ContentType = "video/3gpp";
					HttpContext.Current.Response.AddHeader("Content-Disposition","attachment; filename=\"" + pFileNm + ViCommConst.MOVIE_FOODER + "\"");
				}
				break;
			default:
				HttpContext.Current.Response.ContentType = "video/3gpp";
				HttpContext.Current.Response.AddHeader("Content-Disposition","attachment; filename=\"" + pFileNm + ViCommConst.MOVIE_FOODER + "\"");
				break;

		}
		HttpContext.Current.Response.AddHeader("Content-Length",pSize.ToString());
		HttpContext.Current.Response.Clear();
		HttpContext.Current.Response.WriteFile(pFullPath);
		HttpContext.Current.Response.Flush();
		HttpContext.Current.Response.End();

	}
}
