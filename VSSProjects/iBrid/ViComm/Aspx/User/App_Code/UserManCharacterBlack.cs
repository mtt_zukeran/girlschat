﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員ブラックリスト
--	Progaram ID		: UserManCharacterBlack
--  Creation Date	: 2011.05.18
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class UserManCharacterBlack:DbSession {
	public UserManCharacterBlack() {
	}

	public bool IsBlack(string pSiteCd,string pUserSeq,string pUserCharNo,string pBlackType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER_BLACK	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO		AND");
		oSqlBuilder.AppendLine("	BLACK_TYPE		= :BLACK_TYPE		AND");
		oSqlBuilder.AppendLine("	DEL_FLAG		= 0");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("USER_SEQ",pUserSeq));
				cmd.Parameters.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
				cmd.Parameters.Add(new OracleParameter("BLACK_TYPE",pBlackType));

				int iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount > 0;
			}
		} finally {
			conn.Close();
		}
	}
}
