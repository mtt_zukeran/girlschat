﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品ジャンル

--	Progaram ID		: ProductGenre
--
--  Creation Date	: 2010.12.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductGenre:DbSession {
	public ProductGenre() {
	}

	public DataSet GetListByProductType(string pSiteCd,string pProductType) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT									");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.SITE_CD						SITE_CD						,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_CD			PRODUCT_GENRE_CD			,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_NM			PRODUCT_GENRE_NM			,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_CATEGORY_CD	PRODUCT_GENRE_CATEGORY_CD   ,  	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_NM	PRODUCT_GENRE_CATEGORY_NM     	");
		
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE,			");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY	");
		oSqlBuilder.AppendLine(" WHERE				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD					= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE				= :PRODUCT_TYPE								AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD					= T_PRODUCT_GENRE.SITE_CD	AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_CD	= T_PRODUCT_GENRE.PRODUCT_GENRE_CATEGORY_CD	AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PUBLISH_FLAG						= :PUBLISH_FLAG									");
		oSqlBuilder.AppendLine(" ORDER BY						");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD					, ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_CD	, ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_CD					  ");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PUBLISH_FLAG",ViCommConst.FLAG_ON);
				cmd.Parameters.Add(":PRODUCT_TYPE",pProductType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}
