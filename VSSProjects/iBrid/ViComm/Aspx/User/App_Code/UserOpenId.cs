﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: UserOpenId
--	Title			: 「ﾕｰｻﾞｰ・ｵｰﾌﾟﾝID」テーブルへのデータアクセスを提供するクラス

--	Progaram ID		: UserOpenId
--
--  Creation Date	: 2010.08.20
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class UserOpenId:DbSession {
	public UserOpenId() {
	}

	/// <summary>
	///	OpenIDを元にﾕｰｻﾞｰSEQを取得する。取得できない場合はnullを返す。

	/// </summary>
	/// <param name="pSiteCd">サイトコード</param>
	/// <param name="pOpenId">OpenID</param>
	/// <returns>ﾕｰｻﾞｰSEQ</returns>
	public string GetUserSeq(string pSiteCd,string pOpenId,string pOpenIdType) {
		string sUserSeq = null;

		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT                                            ").AppendLine();
		oSqlBuilder.Append("     USER_SEQ                                      ").AppendLine();
		oSqlBuilder.Append(" FROM                                              ").AppendLine();
		oSqlBuilder.Append("     VW_USER_OPEN_ID01                             ").AppendLine();
		oSqlBuilder.Append(" WHERE                                             ").AppendLine();
		oSqlBuilder.Append("     SITE_CD		= :SITE_CD       AND           ").AppendLine();
		oSqlBuilder.Append("     OPEN_ID		= :OPEN_ID		 AND           ").AppendLine();
		oSqlBuilder.Append("     OPEN_ID_TYPE	= :OPEN_ID_TYPE  AND           ").AppendLine();
		oSqlBuilder.Append("     ENABLED		= :ENABLED			           ").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = new OracleCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":OPEN_ID",pOpenId);
				cmd.Parameters.Add(":OPEN_ID_TYPE",pOpenIdType);
				cmd.Parameters.Add(":ENABLED",ViCommConst.FLAG_ON);
				sUserSeq = (cmd.ExecuteScalar() ?? string.Empty).ToString();
			}

		} finally {
			conn.Close();
		}
		return sUserSeq;
	}
}
