﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 足あと
--	Progaram ID		: Marking
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Marking:DbSession {

	public void MarkingMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pSexCd,int pDelFlag) {
		this.MarkingMainte(pSiteCd,pUserSeq,pUserCharNo,pPartnerUserSeq,pPartnerUserCharNo,pSexCd,pDelFlag,null);
	}

	public void MarkingMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pSexCd,int pDelFlag,string pRefererAspxNm) {
		if(SessionObjs.Current.adminFlg) return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MARKING_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("PPARTNER_SEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pDO_COMMIT",DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("pMARKING_DATE",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("pREFERER_ASPX_NM",DbSession.DbType.VARCHAR2,pRefererAspxNm);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	public bool IsMarking(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds = new DataSet();
		try{
			conn = DbConnect();
			
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("	COUNT(SITE_CD) AS IS_MARKING ");
			sSql.Append("FROM ");
			sSql.Append("	T_MARKING ");
			sSql.Append("WHERE ");
			sSql.Append("	SITE_CD					= :SITE_CD				AND ");
			sSql.Append("	USER_SEQ				= :USER_SEQ				AND ");
			sSql.Append("	USER_CHAR_NO			= :USER_CHAR_NO			AND ");
			sSql.Append("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND ");
			sSql.Append("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return Convert.ToInt32(ds.Tables[0].Rows[0]["IS_MARKING"]) != 0;
	}

	/// <summary>
	/// 足あとリストの未確認件数を取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pUserCharNo"></param>
	/// <returns></returns>
	public int GetUnconfirmMarkingCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd) {
		DataSet ds;
		DataRow dr;
		int iRecCount = 0;
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("	COUNT(T_MARKING.SITE_CD) AS ROW_COUNT ");
			sSql.Append("FROM ");
			sSql.Append("	T_MARKING, ( ");
			sSql.Append("	SELECT ");
			sSql.Append("		SITE_CD, ");
			sSql.Append("		USER_SEQ, ");
			sSql.Append("		USER_CHAR_NO, ");
			sSql.Append("		CONFIRM_MARKING_DATE ");
			sSql.Append("	FROM ");
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sSql.Append("		T_USER_MAN_CHARACTER_EX ");
			} else {
				sSql.Append("		T_CAST_CHARACTER_EX ");
			}
			sSql.Append("	WHERE ");
			sSql.Append("		SITE_CD			= :SITE_CD			AND ");
			sSql.Append("		USER_SEQ		= :USER_SEQ			AND ");
			sSql.Append("		USER_CHAR_NO	= :USER_CHAR_NO		) CONFIRM_DATE ");
			sSql.Append("WHERE ");
			sSql.Append("	T_MARKING.SITE_CD					= CONFIRM_DATE.SITE_CD			AND ");
			sSql.Append("	T_MARKING.PARTNER_USER_SEQ			= CONFIRM_DATE.USER_SEQ			AND ");
			sSql.Append("	T_MARKING.PARTNER_USER_CHAR_NO		= CONFIRM_DATE.USER_CHAR_NO		AND ");
			sSql.Append("	(CONFIRM_MARKING_DATE IS NULL OR CONFIRM_MARKING_DATE < MARKING_DATE)");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					dr = ds.Tables[0].Rows[0];
					if (ds.Tables[0].Rows.Count != 0) {
						iRecCount = int.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return iRecCount;
	}

	/// <summary>
	/// 足あとリストの確認日時を更新
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	public void UpdateConfirmMarkingDate(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd) {
		if (SessionObjs.Current.adminFlg) {
			return;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CONFIRM_MARKING_DATE");
			db.ProcedureInParm("pSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pSEX_CD",DbType.VARCHAR2,pSexCd);
			db.ProcedureOutParm("pSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
