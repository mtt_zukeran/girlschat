﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者着ボイス
--	Progaram ID		: CastVoice
--
--  Creation Date	: 2010.07.29
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class CastVoice : CastBase {
	public CastVoice() {}

	public int GetChargePoint(string pSiteCd,string pUserSeq,string pUserCharNo,string pVoiceSeq) {

		int iChargePoint = 0;

		string sSql = "SELECT NVL(CHARGE_POINT,0) FROM T_CAST_VOICE ";

		string sWhere = string.Empty;
		List<OracleParameter> oParameterList = new List<OracleParameter>();
		
		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhere);
		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhere);
		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhere);
		SysPrograms.SqlAppendWhere("VOICE_SEQ = :VOICE_SEQ",ref sWhere);
		
		oParameterList.Add(new OracleParameter("SITE_CD",pSiteCd));
		oParameterList.Add(new OracleParameter("USER_SEQ",pUserSeq));
		oParameterList.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		oParameterList.Add(new OracleParameter("VOICE_SEQ",pVoiceSeq));

		sSql = sSql+sWhere;

		try{
			conn = DbConnect("CastVoice.GetChargePoint");

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.AddRange(oParameterList.ToArray());
				
				iChargePoint = Convert.ToInt32(cmd.ExecuteScalar() as decimal? ?? decimal.Zero);
			}
		}finally{
			conn.Close();
		}
		return iChargePoint;
	}

	public int GetPageCount(string pSiteCd,string pCastLoginId,string pChastCharNo, bool pIncludeNotAprrove, bool pIncludeNotPublish, string pVoiceType, string pActCategorySeq, int pRecPerPage, out decimal pRecCount) {
		int iPages = 0;

		string sSql = "SELECT COUNT(*) FROM VW_CAST_VOICE00 P ";
		string sWhere = string.Empty;
		OracleParameter[] oParms = CreateWhere(string.Empty, pSiteCd, pCastLoginId, pChastCharNo, pIncludeNotAprrove, pIncludeNotPublish, pVoiceType, pActCategorySeq,false,ref sWhere);
		sSql = sSql + sWhere;
		
		try {
			conn = DbConnect("CastVoice.GetPageCount");
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.AddRange(oParms);
				
				// 総件数を取得

				pRecCount = (cmd.ExecuteScalar() as decimal?) ?? decimal.Zero;
				// ページ数を取得

				iPages = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollectionBySeq(string pVoiceSeq,bool pIncludeNotAprrove, bool pIncludeNotPublish,string pVoiceType ) {
		return this.GetPageCollectionBase(pVoiceSeq, string.Empty, string.Empty, string.Empty, string.Empty, pIncludeNotAprrove, pIncludeNotPublish, pVoiceType, string.Empty, string.Empty, 1, 1);
	}
	public DataSet GetPageCollection(string pSiteCd, string pUserSeq, string pCastLoginId, string pCastCharNo, bool pIncludeNotAprrove, bool pIncludeNotPublish, string pVoiceType, string pActCategorySeq, string pSortType, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(string.Empty, pSiteCd, pUserSeq, pCastLoginId, pCastCharNo, pIncludeNotAprrove, pIncludeNotPublish, pVoiceType, pActCategorySeq, pSortType, pPageNo, pRecPerPage);
	}

	private DataSet GetPageCollectionBase(string pVoiceSeq, string pSiteCd, string pUserSeq, string pCastLoginId, string pCastCharNo, bool pIncludeNotAprrove, bool pIncludeNotPublish, string pVoiceType, string pActCategorySeq, string pSortType, int pPageNo, int pRecPerPage) {
		int iStartRowIndex = (pPageNo - 1) * pRecPerPage;
		int iMaximumRows = pPageNo * pRecPerPage;
		string sWhere = string.Empty;
		string sOrder = string.Empty;

		// ソート条件
		if (pSortType.Equals(ViCommConst.SORT_ASC)) {
			sOrder = "ORDER BY P.SITE_CD, P.CHARGE_POINT, P.VOICE_SEQ DESC";
		} else if (pSortType.Equals(ViCommConst.SORT_DESC)) {
			sOrder = "ORDER BY P.SITE_CD, P.CHARGE_POINT DESC, P.VOICE_SEQ DESC";
		} else {
			sOrder = "ORDER BY P.SITE_CD, P.UPLOAD_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO, P.VOICE_SEQ DESC";
		}


		System.Text.StringBuilder sInnerSql = new System.Text.StringBuilder();
		sInnerSql.Append("SELECT * FROM( ");
		sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
		sInnerSql.Append("		SELECT " + CastBasicField() + ",");
		sInnerSql.Append("			P.VOICE_SEQ,										").AppendLine();
		sInnerSql.Append("			P.UNIQUE_VALUE,										").AppendLine();
		sInnerSql.Append("			P.VOICE_FILE_NM,									").AppendLine();
		sInnerSql.Append("			P.VOICE_TITLE,										").AppendLine();
		sInnerSql.Append("			P.VOICE_DOC,										").AppendLine();
		sInnerSql.Append("			P.CHARGE_POINT,										").AppendLine();
		sInnerSql.Append("			P.OBJ_NOT_APPROVE_FLAG,								").AppendLine();
		sInnerSql.Append("			P.OBJ_NOT_PUBLISH_FLAG,								").AppendLine();
		sInnerSql.Append("			P.UPLOAD_DATE,										").AppendLine();
		sInnerSql.Append("			P.VOICE_TYPE,										").AppendLine();
		sInnerSql.Append("			P.READING_COUNT,									").AppendLine();
		sInnerSql.Append("			P.PUBLISH_DATE,										").AppendLine();
		sInnerSql.Append("			T_OBJ_USED_HISTORY.USED_DATE						").AppendLine();
		sInnerSql.Append("		FROM 													").AppendLine();
		sInnerSql.Append("			VW_CAST_VOICE00 P									").AppendLine();
		sInnerSql.Append("		LEFT JOIN												").AppendLine();
		sInnerSql.Append("			T_OBJ_USED_HISTORY									").AppendLine();
		sInnerSql.Append("			ON													").AppendLine();
		sInnerSql.Append("				P.SITE_CD		= T_OBJ_USED_HISTORY.SITE_CD	").AppendLine();
		sInnerSql.Append("			AND :USER_SEQ		= T_OBJ_USED_HISTORY.USER_SEQ	").AppendLine();
		sInnerSql.Append("			AND P.VOICE_TYPE	= T_OBJ_USED_HISTORY.OBJ_TYPE	").AppendLine();
		sInnerSql.Append("			AND P.VOICE_SEQ		= T_OBJ_USED_HISTORY.OBJ_SEQ	").AppendLine();

		OracleParameter[] objParms = CreateWhere(pVoiceSeq, pSiteCd, pCastLoginId, pCastCharNo, pIncludeNotAprrove, pIncludeNotPublish, pVoiceType, pActCategorySeq,true,ref sWhere);
		sInnerSql.Append(sWhere).AppendLine();
		sInnerSql.Append(sOrder).AppendLine();

		if (pVoiceSeq.Equals(string.Empty)) {
			sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
		}
		else {
			sInnerSql.Append(")INNER ) ");
		}

		SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
		string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
		string sSql = oObj.sqlSyntax[sTemplate].ToString();

		StringBuilder sTables = new StringBuilder();

		StringBuilder sJoinTable = new StringBuilder();

		StringBuilder sJoinField = new StringBuilder();
		sJoinField.Append("TO_CHAR(T1.UPLOAD_DATE,'YYYY/MM/DD HH24:MI')	AS UPLOAD_DATE,").AppendLine();
		sJoinField.Append("CASE").AppendLine();
		sJoinField.Append("WHEN T1.OBJ_NOT_APPROVE_FLAG != 0 THEN '未認証'").AppendLine();
		sJoinField.Append("WHEN T1.OBJ_NOT_PUBLISH_FLAG = 0 THEN '公開'").AppendLine();
		sJoinField.Append("WHEN T1.OBJ_NOT_PUBLISH_FLAG = 1 THEN '非公開'").AppendLine();
		sJoinField.Append("ELSE NULL END AS UNAUTH_MARK,").AppendLine();
		sJoinField.Append("DECODE( T1.USED_DATE , null , 0 , 1 ) AS DOWNLOAD_FLAG	,");

		sSql = string.Format(sSql,sInnerSql.ToString(),sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect("CastVoice.GetPageCollectionByLoginId");

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.AddRange(objParms);
				if (pVoiceSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				}
				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs, "VW_CAST_VOICE00");
				}
			}
		} finally {
			conn.Close();
		}
		
		AppendAttr(oDs);

		return oDs;
	}

	private OracleParameter[] CreateWhere(string pVoiceSeq,string pSiteCd,string pCastLoginId,string pCastCharNo,bool pIncludeNotAprrove,bool pIncludeNotPublish,string pVoiceType,string pActCategorySeq,bool bAllInfo,ref string pWhere) {
		ArrayList oParameterList = new ArrayList();

		if (bAllInfo) {
			InnnerCondition(ref oParameterList);
		}

		SysPrograms.SqlAppendWhere("P.USER_STATUS = :USER_STATUS", ref pWhere);
		oParameterList.Add(new OracleParameter(":USER_STATUS", ViCommConst.USER_WOMAN_NORMAL));

		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG", ref pWhere);
		oParameterList.Add(new OracleParameter(":NA_FLAG", ViCommConst.FLAG_OFF));

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD", ref pWhere);
			oParameterList.Add(new OracleParameter(":SITE_CD", pSiteCd));
		}

		if (!string.IsNullOrEmpty(pCastLoginId) && !string.IsNullOrEmpty(pCastCharNo)) {
			SysPrograms.SqlAppendWhere("P.LOGIN_ID = :LOGIN_ID", ref pWhere);
			oParameterList.Add(new OracleParameter(":LOGIN_ID", pCastLoginId));
			SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :CAST_CHAR_NO", ref pWhere);
			oParameterList.Add(new OracleParameter(":CAST_CHAR_NO", pCastCharNo));
		}
		
		if (!string.IsNullOrEmpty(pVoiceSeq)) {
			SysPrograms.SqlAppendWhere("P.VOICE_SEQ = :VOICE_SEQ", ref pWhere);
			oParameterList.Add(new OracleParameter(":VOICE_SEQ", pVoiceSeq));
		}

		if (!pIncludeNotAprrove) {
			SysPrograms.SqlAppendWhere("P.OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG", ref pWhere);
			oParameterList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG", ViCommConst.FLAG_OFF));
		}

		if (!pIncludeNotPublish) {
			SysPrograms.SqlAppendWhere("P.OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG", ref pWhere);
			oParameterList.Add(new OracleParameter(":OBJ_NOT_PUBLISH_FLAG", ViCommConst.FLAG_OFF));
		}

		if (!string.IsNullOrEmpty(pVoiceType)) {
			SysPrograms.SqlAppendWhere("P.VOICE_TYPE = :VOICE_TYPE", ref pWhere);
			oParameterList.Add(new OracleParameter(":VOICE_TYPE", pVoiceType));
		}

		if (!string.IsNullOrEmpty(pActCategorySeq)) {
			SysPrograms.SqlAppendWhere("P.ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ", ref pWhere);
			oParameterList.Add(new OracleParameter("ACT_CATEGORY_SEQ", pActCategorySeq));
		}
		
		return (OracleParameter[])oParameterList.ToArray(typeof(OracleParameter));	
	}

	private void AppendAttr(DataSet pDS) {
		SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
		string sManUserSeq = "";
		if (oObj is SessionMan) {
			sManUserSeq = ((SessionMan)oObj).userMan.userSeq;
		}

		string sSql, sSql2;

		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD	AND " +
						"USER_SEQ		= :USER_SEQ	AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO	";

		sSql2 = "SELECT USED_DATE FROM T_OBJ_USED_HISTORY " +
					"WHERE " +
						"SITE_CD	= :SITE_CD	AND " +
						"USER_SEQ	= :USER_SEQ	AND " +
						"OBJ_TYPE  	= :OBJ_TYPE	AND " +
						"OBJ_SEQ	= :OBJ_SEQ ";

		DataColumn col;
		for (int i = 1; i <= ViCommConst.MAX_ATTR_COUNT; i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}", i), System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		col = new DataColumn("OBJ_DOWNLOAD_DATE", System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn("OBJ_DOWNLOAD_FLAG", System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			for (int i = 1; i <= ViCommConst.MAX_ATTR_COUNT; i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}", i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD", dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ", dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO", dr["USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub, "T_ATTR_VALUE");
					}
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}", drSub["ITEM_NO"].ToString())] = drSub["DISPLAY_VALUE"].ToString();
				}
			}

			dr["OBJ_DOWNLOAD_FLAG"] = "0";
			dr["OBJ_DOWNLOAD_DATE"] = "";
			if (!sManUserSeq.Equals("")) {
				using (DataSet dsSub = new DataSet()) {
					using (cmd = CreateSelectCommand(sSql2,conn)) {
						cmd.Parameters.Add("SITE_CD", dr["SITE_CD"].ToString());
						cmd.Parameters.Add("USER_SEQ", sManUserSeq);
						cmd.Parameters.Add("OBJ_TYPE", ViCommConst.ATTACHED_VOICE.ToString());
						cmd.Parameters.Add("OBJ_SEQ", dr["VOICE_SEQ"].ToString());
						using (da = new OracleDataAdapter(cmd)) {
							da.Fill(dsSub, "T_OBJ_USED_HISTORY");
						}
					}
					if (dsSub.Tables[0].Rows.Count > 0) {
						dr["OBJ_DOWNLOAD_FLAG"] = "1";
						dr["OBJ_DOWNLOAD_DATE"] = dsSub.Tables[0].Rows[0]["USED_DATE"].ToString();
					}
				}
			}
		}
	}


}
