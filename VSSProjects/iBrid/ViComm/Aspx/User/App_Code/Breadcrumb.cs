﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

[Serializable]
public class Breadcrumb {
	private string _key = null;
	private string _url = null;
	private string _title = null;

	public string Key {
		get { return this._key; }
		set { this._key = value; }
	}
	public string Url {
		get { return this._url; }
		set { this._url = value; }
	}
	public string Title {
		get { return this._title; }
		set { this._title = value; }
	}

	public Breadcrumb() { }
	public Breadcrumb(string pKey, string pUrl, string pTitle) {
		this._key = pKey;
		this._url = pUrl;
		this._title = pTitle;
	}
	
	public static string GenearteKey(SessionObjs pSessionObjs){
		string sHtmlDocNo = "-1";
		if(pSessionObjs.currentAspx.Equals("DisplayDoc.aspx")){
			sHtmlDocNo = iBridCommLib.iBridUtil.GetStringValue(pSessionObjs.requestQuery["doc"]);
		}
		return string.Format("{0}/{1}?doc={2}", pSessionObjs.currentProgramRoot, pSessionObjs.currentProgramId, sHtmlDocNo).ToLower();
	}

	public override bool Equals(object obj) {
		return this.Key.Equals(((Breadcrumb)obj).Key);
	}

	public override int GetHashCode() {
		return this.Key.GetHashCode();
	}
}

