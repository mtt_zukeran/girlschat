﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class Product :DbSession{


	public Product() { }
	
	public string ConvertProductId2Seq(string pProductId){
		string sProductSeq = null;
		
		DataSet oDataSet = this.GetOneByLoginId(pProductId);
		if( oDataSet.Tables[0].Rows.Count>0){
			sProductSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PRODUCT_SEQ"]);
		}
		
		return sProductSeq;
	}


	public int GetChargePoint(string pProductSeq) {

		int iChargePoint = 0;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine(" 	DECODE(                         ");
		oSqlBuilder.AppendLine(" 		SPECIAL_CHARGE_POINT	,   ");
		oSqlBuilder.AppendLine(" 		0						,   ");
		oSqlBuilder.AppendLine(" 		CHARGE_POINT			,   ");
		oSqlBuilder.AppendLine(" 		SPECIAL_CHARGE_POINT        ");
		oSqlBuilder.AppendLine(" 	) AS APPLY_CHARGE_POINT         ");
		oSqlBuilder.AppendLine(" FROM                               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT                       ");
		oSqlBuilder.AppendLine(" WHERE                              ");
		oSqlBuilder.AppendLine("   PRODUCT_SEQ = :PRODUCT_SEQ       ");

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);

				iChargePoint = Convert.ToInt32(cmd.ExecuteScalar() as decimal? ?? decimal.Zero);
			}
		} finally {
			conn.Close();
		}
		return iChargePoint;
	}
	
	public DataSet GetOneBySeq(string pProductSeq) {
		return this.GetOne(pProductSeq,null);
	}
	public DataSet GetOneByLoginId(string pProductId) {
		return this.GetOne(null,pProductId);
	}

	private DataSet GetOne(string pProductSeq,string pProductId){
		// 念のため引数チェック
		if(string.IsNullOrEmpty(pProductId) && string.IsNullOrEmpty(pProductSeq)) throw new ArgumentException();
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.SITE_CD				,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_AGENT_CD       ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_SEQ            ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_ID             ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_TYPE           ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_NM             ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_DISCRIPTION    ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PART_NUMBER            ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PUBLISH_START_DATE     ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PUBLISH_END_DATE       ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_SERIES_SUMMARY ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.CAST_SUMMARY           ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PRODUCT_MAKER_SUMMARY  ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.RELEASE_DATE           ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.GROSS_PRICE            ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.CHARGE_POINT           ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.SPECIAL_CHARGE_POINT   ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PUBLISH_FLAG           ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.DEL_FLAG               ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.REMARKS                ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.LINK_ID                ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.LINK_DATE              ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.UNEDITABLE_FLAG        ,   ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00.PICKUP_MASK                ");
		oSqlBuilder.AppendLine(" FROM                                       ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT00                            ");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhereClause = string.Empty;
		if (!string.IsNullOrEmpty(pProductSeq)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_SEQ = :PRODUCT_SEQ ", ref sWhereClause);
			oParamList.Add(new OracleParameter(":PRODUCT_SEQ", pProductSeq));
		}
		if (!string.IsNullOrEmpty(pProductId)) {
			SysPrograms.SqlAppendWhere(" VW_PRODUCT00.PRODUCT_ID = :PRODUCT_ID ", ref sWhereClause);
			oParamList.Add(new OracleParameter(":PRODUCT_ID", pProductId));
		}
		oSqlBuilder.AppendLine(sWhereClause);
	
	
		DataSet oDataSet = new DataSet();
		try{
			this.conn = this.DbConnect();

			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oParamList.ToArray());
				
				using(this.da = new OracleDataAdapter(this.cmd)){
					this.da.Fill(oDataSet);
				}
			}
			
		}finally{
			this.conn.Close();
		}
		return oDataSet;
	}
	
}
