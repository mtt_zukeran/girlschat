﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画
--	Progaram ID		: CastMovie
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Collections.Specialized;
using System.Collections.Generic;

[Serializable]
public class CastMovieSeekCondition:SeekConditionBase {
	public CastMovieSeekCondition()
		: this(new NameValueCollection()) {
	}

	public CastMovieSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
	}
}

[System.Serializable]
public class CastMovie:CastBase {

	public int chargePoint;

	public CastMovie()
		: base() {
	}
	public CastMovie(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}


	public string GetCastMovieInfo(string pMovieSeq,out string pLoginId,out string pUserSeq,out string pUserCharNo) {
		DataSet ds;
		DataRow dr;

		string sFileNm = string.Empty;
		pLoginId = string.Empty;
		pUserSeq = string.Empty;
		pUserCharNo = string.Empty;
		try {
			conn = DbConnect("CastMovie.GetCastMovieInfo");

			string sSql = "SELECT MOVIE_SEQ,CHARGE_POINT,LOGIN_ID,USER_SEQ,USER_CHAR_NO FROM VW_CAST_MOVIE00 " +
							"WHERE " +
							"MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE00");
					if (ds.Tables["VW_CAST_MOVIE00"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_MOVIE00"].Rows[0];
						pLoginId = dr["LOGIN_ID"].ToString();
						pUserSeq = dr["USER_SEQ"].ToString();
						pUserCharNo = dr["USER_CHAR_NO"].ToString();
						chargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sFileNm;
	}

	public bool GetCastSampleMovieFileNm(string pMovieSeq,out string pLoginId) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;

		pLoginId = "";
		try {
			conn = DbConnect("CastMovie.GetCastMovieFileNm");

			string sSql = "SELECT LPAD(SAMPLE_MOVIE_SEQ,15,'0') AS MOVIE_FILE_NM,CHARGE_POINT,LOGIN_ID FROM VW_CAST_MOVIE02 " +
							"WHERE " +
							" SAMPLE_MOVIE_SEQ = :SAMPLE_MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SAMPLE_MOVIE_SEQ",pMovieSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE02");
					if (ds.Tables["VW_CAST_MOVIE02"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_MOVIE02"].Rows[0];
						pLoginId = dr["LOGIN_ID"].ToString();
						chargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pMovieSeries,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,"","",pMovieSeries,false,new SeekCondition(),pRecPerPage,pUseMV,out pRecCount);
	}

	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,bool pOnlyUsed,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",pOnlyUsed,new SeekCondition(),pRecPerPage,pUseMV,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,bool pOnlyUsed,SeekCondition pCondition,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",pOnlyUsed,pCondition,pRecPerPage,pUseMV,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,new SeekCondition(),pRecPerPage,pUseMV,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,string pApproveStatus,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pApproveStatus,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,new SeekCondition(),pRecPerPage,pUseMV,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,string pApproveStatus,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,SeekCondition pCondition,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pApproveStatus,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,pCondition,pRecPerPage,pUseMV,out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,SeekCondition pCondition,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountBase(FlexibleQueryType.None,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,pCondition,pRecPerPage,pUseMV,out pRecCount);
	}

	public int GetPageCountBase(
		FlexibleQueryType pFlexibleQueryType,
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pMovieSeq,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pMovieType,
		string pActCategorySeq,
		string pCastMovieAttrTypeSeq,
		string pCastMovieAttrSeq,
		string pMovieSeries,
		bool pOnlyUsed,
		SeekCondition pCondition,
		int pRecPerPage,
		bool pUseMV,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = CreateFlexibleQueryTarget(pFlexibleQueryType,pCondition,pMovieType);

		try {
			conn = DbConnect("CastMovie.GetPageCountBase");

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastMovie"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			string sSql = string.Format("SELECT COUNT(*) AS ROW_COUNT FROM {0}_CAST_MOVIE00 P ",sSuffix);
			if (!string.IsNullOrEmpty(pCondition.pickupId)) {
				sSql += "	, VW_PICKUP_OBJS00	";
			}
			// いいねした動画のみ検索
			if (pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sSql += "	, T_CAST_MOVIE_LIKE CML ";
			}

			if (pCondition != null && pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						sSql += string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1);
					}
				}
			}

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pApproveStatus,pIncludeNotPublish,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,pMovieSeries,pOnlyUsed,pCondition,false,ref sWhere);
			sSql = sSql + sWhere;

			using ( pUseMV ? cmd = CreateSelectCommand(sSql,conn) : cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		if (pFlexibleQueryType == FlexibleQueryType.None && oFlexibleQueryTarget.HasTarget) {
			decimal dExcludeRecCount = decimal.Zero;
			GetPageCountBase(FlexibleQueryType.InScope,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pApproveStatus,pIncludeNotPublish,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,pMovieSeries,pOnlyUsed,pCondition,pRecPerPage,pUseMV,out dExcludeRecCount);
			iPages = CalcExcludeCount(ref pRecCount,dExcludeRecCount,pRecPerPage);
		}

		return iPages;
	}

	public void GetPrevNextMovieSeq(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pMovieSeq,
		bool pIncludeNotAprroveFlag,
		bool pIncludeNotPublish,
		string pMovieType,
		string pActCategorySeq,
		string pCastMovieAttrTypeSeq,
		string pCastMovieAttrSeq,
		string pMovieSeries,
		bool pOnlyUsed,
		string pSortVector,
		bool pUseMV,
		out string pPrevPicSeq,
		out string pNextPicSeq
	) {
		GetPrevNextMovieSeq(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag,pIncludeNotPublish,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,pMovieSeries,pOnlyUsed,new SeekCondition(),pSortVector,pUseMV,out pPrevPicSeq,out pNextPicSeq);
	}
	public void GetPrevNextMovieSeq(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pMovieSeq,
		bool pIncludeNotAprroveFlag,
		bool pIncludeNotPublish,
		string pMovieType,
		string pActCategorySeq,
		string pCastMovieAttrTypeSeq,
		string pCastMovieAttrSeq,
		string pMovieSeries,
		bool pOnlyUsed,
		SeekCondition pCondition,
		string pSortVector,
		bool pUseMV,
		out string pPrevPicSeq,
		out string pNextPicSeq
	) {
		pPrevPicSeq = "";
		pNextPicSeq = "";


		try {
			conn = DbConnect("CastMovie.GetPrevNextMovieSeq");
			DataSet ds = new DataSet();

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastMovie"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			string sOrder;
			GetOrderBy(false,false,string.Empty,pSortVector,pLoginId,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,pCondition,out sOrder);
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM(");
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		MOVIE_SEQ,").AppendLine();
			sSql.Append(string.Format("LAG(MOVIE_SEQ)	OVER ({0}) AS PREV_MOVIE_SEQ,",sOrder)).AppendLine();
			sSql.Append(string.Format("LEAD(MOVIE_SEQ)	OVER ({0}) AS NEXT_MOVIE_SEQ",sOrder));
			sSql.Append(string.Format("	FROM {0}_CAST_MOVIE00 P ",sSuffix)).AppendLine();

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope,pCondition,pMovieType),pSiteCd,pLoginId,pUserCharNo,"",ViCommConst.FLAG_OFF_STR,pIncludeNotPublish,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,pMovieSeries,pOnlyUsed,pCondition,false,ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(") WHERE MOVIE_SEQ =:MOVIE_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE00");
				}
			}
			if (ds.Tables[0].Rows.Count != 0) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (dr["MOVIE_SEQ"].ToString().Equals(pMovieSeq)) {
						pPrevPicSeq = dr["PREV_MOVIE_SEQ"].ToString();
						pNextPicSeq = dr["NEXT_MOVIE_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetPageCollectionBySeq(string pMovieSeq,bool pUseMV) {
		return GetPageCollectionBase("","","",pMovieSeq,string.Empty,false,"","","","","",false,new SeekCondition(),"",0,0,pUseMV);
	}

	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pMovieSeries,string pSortType,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionBase(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,"","",pMovieSeries,false,new SeekCondition(),pSortType,pPageNo,pRecPerPage,pUseMV);
	}

	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,string pSortType,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionBase(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,new SeekCondition(),pSortType,pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,string pApproveStatus,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,string pSortType,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionBase(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pApproveStatus,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,new SeekCondition(),pSortType,pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,string pApproveStatus,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,SeekCondition pCondition,string pSortType,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionBase(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pApproveStatus,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,pCondition,pSortType,pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,bool pOnlyObj,string pSortType,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionBase(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",pOnlyObj,new SeekCondition(),pSortType,pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,bool pOnlyObj,SeekCondition pCondition,string pSortType,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionBase(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",pOnlyObj,pCondition,pSortType,pPageNo,pRecPerPage,pUseMV);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,bool pIncludeNotAprroveFlag,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,SeekCondition pCondition,string pSortType,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionBase(pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pIncludeNotAprroveFlag ? string.Empty : ViCommConst.FLAG_OFF_STR,false,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,"",false,pCondition,pSortType,pPageNo,pRecPerPage,pUseMV);
	}


	public DataSet GetPageCollectionBase(
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		string pMovieSeq,
		string pApproveStatus,
		bool pIncludeNotPublish,
		string pMovieType,
		string pActCategorySeq,
		string pCastMovieAttrTypeSeq,
		string pCastMovieAttrSeq,
		string pMovieSeries,
		bool pOnlyUsed,
		SeekCondition pCondition,
		string sSortVector,
		int pPageNo,
		int pRecPerPage,
		bool pUseMV
	) {
		DataSet ds;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.None,pCondition,pMovieType);
		try {
			conn = DbConnect("CastMovie.GetPageCollectionByLoginId");
			ds = new DataSet();

			string sOrder;
			GetOrderBy(!string.IsNullOrEmpty(pCondition.pickupId),pCondition.directRandom == ViCommConst.FLAG_ON,pCondition.sortType,sSortVector,pLoginId,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,pCondition,out sOrder);

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastMovie"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder sInnerSql = new StringBuilder();

			sInnerSql.Append("SELECT * FROM( ");
			sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT 											").AppendLine();
			sInnerSql.Append("			P.*, 										").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			D.GOOD_POINT AS GOOD_POINT_DAILY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			W.GOOD_POINT AS GOOD_POINT_WEEKLY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			M.GOOD_POINT AS GOOD_POINT_MONTHLY,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
				sInnerSql.AppendLine("NVL(LW.LIKE_COUNT,0) AS LIKE_COUNT_WEEK,");
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
				sInnerSql.AppendLine("NVL(LM.LIKE_COUNT,0) AS LIKE_COUNT_MONTH,");
			}
			sInnerSql.Append("			NVL(T.REVIEW_COUNT,0) AS REVIEW_COUNT,		").AppendLine();
			sInnerSql.Append("			NVL(T.COMMENT_COUNT,0) AS COMMENT_COUNT,	").AppendLine();
			sInnerSql.Append("			NVL(T.GOOD_STAR_TOTAL,0) AS GOOD_STAR_TOTAL,").AppendLine();

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sInnerSql.AppendLine("		1 AS BOOKMARK_FLAG	");
				} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
				} else {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						sInnerSql.AppendLine("CASE WHEN EXISTS(");
						sInnerSql.AppendLine("SELECT 1 FROM T_OBJ_BOOKMARK BKM WHERE");
						sInnerSql.AppendLine(" BKM.SITE_CD = P.SITE_CD AND");
						sInnerSql.AppendLine(" BKM.OBJ_SEQ = P.MOVIE_SEQ AND");
						sInnerSql.AppendFormat(" BKM.USER_SEQ = {0} AND",sSelfUserSeq).AppendLine();
						sInnerSql.AppendFormat(" BKM.USER_CHAR_NO = {0}",sSelfUserCharNo).AppendLine();
						sInnerSql.AppendLine(") THEN 1 ELSE 0 END AS BOOKMARK_FLAG");
					} else {
						sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
					}
				}
			} else {
				sInnerSql.AppendLine("		0 AS BOOKMARK_FLAG	");
			}

			sInnerSql.Append("		FROM (											").AppendLine();
			sInnerSql.Append("		SELECT " + CastInfoFields() + ",");
			if (!string.IsNullOrEmpty(pCondition.pickupId)) {
				sInnerSql.Append("	VW_PICKUP_OBJS00.PICKUP_TITLE								,	").AppendLine();
				sInnerSql.Append("	VW_PICKUP_OBJS00.COMMENT_PICKUP		AS PICKUP_OBJ_COMMENT	,	").AppendLine();
				sInnerSql.Append("	VW_PICKUP_OBJS00.PICKUP_START_PUB_DAY		,	").AppendLine();
				sInnerSql.Append("	VW_PICKUP_OBJS00.PICKUP_END_PUB_DAY			,	").AppendLine();
			}
			sInnerSql.Append("			P.MOVIE_SEQ					,").AppendLine();
			sInnerSql.Append("			P.UNIQUE_VALUE				,").AppendLine();
			sInnerSql.Append("			P.MOVIE_TITLE				,").AppendLine();
			sInnerSql.Append("			P.MOVIE_DOC					,").AppendLine();
			sInnerSql.Append("			P.MOVIE_TYPE				,").AppendLine();
			sInnerSql.Append("			P.CHARGE_POINT				,").AppendLine();
			sInnerSql.Append("			P.UPLOAD_DATE				,").AppendLine();
			sInnerSql.Append("			P.OBJ_NOT_APPROVE_FLAG		,").AppendLine();
			sInnerSql.Append("			P.OBJ_NOT_PUBLISH_FLAG		,").AppendLine();
			sInnerSql.Append("			P.READING_COUNT				,").AppendLine();
			sInnerSql.Append("			P.PLAY_TIME					,").AppendLine();
			sInnerSql.Append("			P.MOVIE_SERIES				,").AppendLine();
			sInnerSql.Append("			P.SAMPLE_MOVIE_SEQ			,").AppendLine();
			sInnerSql.Append("			P.THUMBNAIL_PIC_SEQ			,").AppendLine();
			sInnerSql.Append("			P.CAST_MOVIE_ATTR_TYPE_SEQ	,").AppendLine();
			sInnerSql.Append("			P.CAST_MOVIE_ATTR_SEQ		,").AppendLine();
			sInnerSql.Append("			P.PLANNING_TYPE				,").AppendLine();
			sInnerSql.Append("			P.OBJ_NA_FLAG				,").AppendLine();
			sInnerSql.Append("			P.OBJ_RANKING_FLAG			,").AppendLine();
			sInnerSql.Append("			P.LIKE_COUNT				,").AppendLine();
			sInnerSql.Append("			P.COMMENT_COUNT AS CAST_MOVIE_COMMENT_COUNT").AppendLine();
			sInnerSql.Append(string.Format("FROM {0}_CAST_MOVIE00 P ",sSuffix)).AppendLine();
			if (!string.IsNullOrEmpty(pCondition.pickupId)) {
				sInnerSql.Append("	, VW_PICKUP_OBJS00	").AppendLine();
			}
			// いいねした動画のみ検索
			if (pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sInnerSql.AppendLine("	, T_CAST_MOVIE_LIKE CML");
			}

			if (pCondition != null && pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						sInnerSql.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
					}
				}
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pSiteCd,pLoginId,pUserCharNo,pMovieSeq,pApproveStatus,pIncludeNotPublish,pMovieType,pActCategorySeq,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,pMovieSeries,pOnlyUsed,pCondition,true,ref sWhere);
			sInnerSql.Append(sWhere).AppendLine();

			sInnerSql.Append("			) P,							").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_DAILY D,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_WEEKLY W,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			T_OBJ_REVIEW_MONTHLY M,			").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
				sInnerSql.AppendLine("T_CAST_MOVIE_LIKE_WEEK LW,");
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
				sInnerSql.AppendLine("T_CAST_MOVIE_LIKE_MONTH LM,");
			}
			sInnerSql.Append("			T_OBJ_REVIEW_TOTAL	T			").AppendLine();
			sInnerSql.Append("		WHERE								").AppendLine();
			if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
				sInnerSql.Append("			P.SITE_CD	= D.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.MOVIE_SEQ	= D.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
				sInnerSql.Append("			P.SITE_CD	= W.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.MOVIE_SEQ	= W.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
				sInnerSql.Append("			P.SITE_CD	= M.SITE_CD	(+) AND	").AppendLine();
				sInnerSql.Append("			P.MOVIE_SEQ	= M.OBJ_SEQ	(+) AND	").AppendLine();
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
				sInnerSql.AppendLine("P.SITE_CD = LW.SITE_CD (+) AND");
				sInnerSql.AppendLine("P.MOVIE_SEQ = LW.MOVIE_SEQ (+) AND");
			} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
				sInnerSql.AppendLine("P.SITE_CD = LM.SITE_CD (+) AND");
				sInnerSql.AppendLine("P.MOVIE_SEQ = LM.MOVIE_SEQ (+) AND");
			}
			sInnerSql.Append("			P.SITE_CD	= T.SITE_CD	(+) AND	").AppendLine();
			sInnerSql.Append("			P.MOVIE_SEQ	= T.OBJ_SEQ	(+)		").AppendLine();

			sInnerSql.Append(sOrder).AppendLine();

			if (pMovieSeq.Equals(string.Empty)) {
				sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sInnerSql.Append(")INNER ) ");
			}

			SessionObjs oObj = this.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			StringBuilder sTables = new StringBuilder();
			sTables.Append("T_CAST_MOVIE_ATTR_TYPE T_CAST_MOVIE_ATTR_TYPE1,").AppendLine();
			sTables.Append("T_CAST_MOVIE_ATTR_TYPE T_CAST_MOVIE_ATTR_TYPE2,").AppendLine();
			sTables.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE,");

			StringBuilder sJoinTable = new StringBuilder();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_MOVIE_ATTR_TYPE1.SITE_CD						(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_MOVIE_ATTR_TYPE_SEQ	= T_CAST_MOVIE_ATTR_TYPE1.CAST_MOVIE_ATTR_TYPE_SEQ		(+)	AND").AppendLine();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_MOVIE_ATTR_TYPE_VALUE.SITE_CD					(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_MOVIE_ATTR_TYPE_SEQ	= T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_TYPE_SEQ	(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_MOVIE_ATTR_SEQ		= T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_SEQ		(+)	AND").AppendLine();
			sJoinTable.Append("T1.SITE_CD					= T_CAST_MOVIE_ATTR_TYPE2.SITE_CD						(+)	AND").AppendLine();
			sJoinTable.Append("T1.MOVIE_SERIES				= T_CAST_MOVIE_ATTR_TYPE2.CAST_MOVIE_ATTR_TYPE_SEQ		(+)	AND");

			StringBuilder sJoinField = new StringBuilder();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開中', '非公開'), '申請中') AS UNAUTH_MARK,").AppendLine();
			sJoinField.Append("DECODE(T1.OBJ_NOT_APPROVE_FLAG, 0, DECODE(T1.OBJ_NOT_PUBLISH_FLAG, 0, '公開', '非公開'), '認証待ち')	AS UNAUTH_MARK_ADMIN,").AppendLine();
			sJoinField.Append("GET_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.THUMBNAIL_PIC_SEQ,T1.PROFILE_PIC_SEQ) AS THUMBNAIL_IMG_PATH,").AppendLine();
			sJoinField.Append("T_CAST_MOVIE_ATTR_TYPE1.CAST_MOVIE_ATTR_TYPE_NM	AS CAST_MOVIE_ATTR_TYPE_NM	,").AppendLine();
			sJoinField.Append("T_CAST_MOVIE_ATTR_TYPE_VALUE.CAST_MOVIE_ATTR_NM	AS CAST_MOVIE_ATTR_NM		,").AppendLine();
			sJoinField.Append("T_CAST_MOVIE_ATTR_TYPE1.CAST_MOVIE_ATTR_TYPE_NM	AS MOVIE_SERIES_NM			,");

			sSql = string.Format(sSql,sInnerSql.ToString(),sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				if (pMovieSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",(pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
				}
				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_CAST_MOVIE00");
				}

			}
		} finally {
			conn.Close();
		}

		ds = this.Exclude(ds,pRecPerPage,oFlexibleQueryTarget);

		if (pMovieType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())
			|| pMovieType.Equals(ViCommConst.ATTACHED_BBS.ToString())
			|| pMovieType.Equals(ViCommConst.ATTACHED_MOVIE.ToString())
			|| pMovieType.Equals(string.Empty)) {
			AppendAttr(ds);
			
			if (ds.Tables[0].Rows.Count > 0) {
				if (ds.Tables[0].Rows[0]["MOVIE_TYPE"].ToString().Equals(ViCommConst.ATTACHED_BBS.ToString())) {
					AppendBbsMovie(ds);
				} else {
					AppendProfileMovie(ds);
				}
			}
		}

		return ds;
	}

	private void GetOrderBy(bool pIsPickup,bool pDirectRandom,string sSortType,string pSortVector,string pLoginId,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,SeekCondition pCondition,out string pOrder) {
		if (sSortType.Equals("*"))
			sSortType = string.Empty;
		switch (sSortType) {
			case ViCommConst.SortType.NEW:				//新着順 
				pOrder = "ORDER BY P.MOVIE_SEQ DESC ";
				break;
			case ViCommConst.SortType.LAST_LOGIN_DATE:	//最終ﾛｸﾞｲﾝ日時順 
				pOrder = "ORDER BY P.LAST_LOGIN_DATE DESC, P.MOVIE_SEQ DESC";
				break;
			case ViCommConst.SortType.ONLINE_STATUS:	//オンライン優先 
				pOrder = "ORDER BY P.CHARACTER_ONLINE_STATUS DESC,P.LAST_ACTION_DATE DESC, P.MOVIE_SEQ DESC";
				break;
			case ViCommConst.SortType.PICKUP_START_PUB_DAY:	//ﾋﾟｯｸｱｯﾌﾟ開始日
				pOrder = "ORDER BY P.PICKUP_START_PUB_DAY DESC,P.PRIORITY ASC,P.SITE_CD,P.UPLOAD_DATE DESC,P.USER_SEQ,P.USER_CHAR_NO,P.MOVIE_SEQ DESC";
				break;
			case ViCommConst.SortType.LOGINED:			//ﾛｸﾞｲﾝ中
			case ViCommConst.SortType.WAITING:			//待機中
				pOrder = "ORDER BY P.LAST_ACTION_DATE DESC, P.MOVIE_SEQ DESC";
				break;
			case ViCommConst.SortType.READING_COUNT:	//閲覧回数順

				pOrder = "ORDER BY P.READING_COUNT DESC, P.MOVIE_SEQ DESC";
				break;
			default:
				if (pIsPickup) {
					if (pDirectRandom) {
						pOrder = "ORDER BY DBMS_RANDOM.RANDOM,P.PRIORITY ASC ";
					} else {
						pOrder = "ORDER BY P.PRIORITY ASC,P.SITE_CD,P.UPLOAD_DATE DESC,P.USER_SEQ,P.USER_CHAR_NO,P.MOVIE_SEQ DESC";
					}
				} else {
					string sOrderSpec = string.Empty;
					if (!pLoginId.Equals(string.Empty)) {
						sOrderSpec = sOrderSpec + "P.LOGIN_ID,";
					}
					if (!pCastMovieAttrTypeSeq.Equals(string.Empty)) {
						sOrderSpec = sOrderSpec + "P.CAST_MOVIE_ATTR_TYPE_SEQ,";
					}
					if (!pCastMovieAttrSeq.Equals(string.Empty)) {
						sOrderSpec = sOrderSpec + "P.CAST_MOVIE_ATTR_SEQ,";
					}
					if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.DAILY)) {
						sOrderSpec = sOrderSpec + "GOOD_POINT_DAILY DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.WEEKLY)) {
						sOrderSpec = sOrderSpec + "GOOD_POINT_WEEKLY DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.MONTHLY)) {
						sOrderSpec = sOrderSpec + "GOOD_POINT_MONTHLY DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_WEEKLY)) {
						sOrderSpec = sOrderSpec + "LIKE_COUNT_WEEK DESC,P.UPLOAD_DATE DESC NULLS LAST,";
					} else if (pCondition.rankType.Equals(PwViCommConst.ObjReviewRankType.LIKE_COUNT_MONTHLY)) {
						sOrderSpec = sOrderSpec + "LIKE_COUNT_MONTH DESC,P.UPLOAD_DATE DESC NULLS LAST,";
					}
					pOrder = string.Format("ORDER BY P.SITE_CD,P.NA_FLAG,P.OBJ_NOT_APPROVE_FLAG,P.OBJ_NOT_PUBLISH_FLAG,P.MOVIE_TYPE,{0}P.MOVIE_SEQ DESC ",sOrderSpec);

					//昇順と降順 
					if (pSortVector.Equals(ViCommConst.SORT_ASC)) {
						pOrder = "ORDER BY P.SITE_CD,";
						pOrder = string.Format("ORDER BY P.SITE_CD,P.NA_FLAG,P.OBJ_NA_FLAG,{0}P.CHARGE_POINT,P.MOVIE_SEQ",sOrderSpec);
					} else if (pSortVector.Equals(ViCommConst.SORT_DESC)) {
						pOrder = string.Format("ORDER BY P.SITE_CD,P.NA_FLAG,P.OBJ_NA_FLAG,{0}P.CHARGE_POINT DESC,P.MOVIE_SEQ DESC",sOrderSpec);
					}
				}
				break;
		}
	}


	private string CastInfoFields() {
		string sField = CastBasicField();

		sField = sField + ",P.ENABLED_BLOG_FLAG ";
		sField = sField + ",P.BLOG_SEQ ";
		sField = sField + ",P.BLOG_TITLE ";
		sField = sField + ",P.BLOG_DOC ";

		return sField;
	}

	private OracleParameter[] CreateWhere(FlexibleQueryTargetInfo pFlexibleQueryTarget,string pSiteCd,string pLoginId,string pUserCharNo,string pMovieSeq,string pApproveStatus,bool pIncludeNotPublish,string pMovieType,string pActCategorySeq,string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq,string pMovieSeries,bool pOnlyUsed,SeekCondition pCondition,bool bAllInfo,ref string pWhere) {
		if (pCastMovieAttrSeq.Equals("*")) {
			pCastMovieAttrSeq = string.Empty;
		}

		ArrayList list = new ArrayList();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		if (!pMovieSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.MOVIE_SEQ = :MOVIE_SEQ ",ref pWhere);
			list.Add(new OracleParameter("MOVIE_SEQ",pMovieSeq));
		} else {
			if (pSiteCd.Equals(string.Empty) == false) {
				SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD ",ref pWhere);
				list.Add(new OracleParameter("SITE_CD",pSiteCd));
			}
			SysPrograms.SqlAppendWhere("P.NA_FLAG =: NA_FLAG ",ref pWhere);
			list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));


			if (!string.IsNullOrEmpty(pApproveStatus) && pApproveStatus.Equals(ViCommConst.FLAG_OFF_STR) && pIncludeNotPublish == false) {
				SysPrograms.SqlAppendWhere("P.OBJ_NA_FLAG = :OBJ_NA_FLAG",ref pWhere);
				list.Add(new OracleParameter("OBJ_NA_FLAG",ViCommConst.FLAG_OFF));
			} else {
				if (!string.IsNullOrEmpty(pApproveStatus)) {
					SysPrograms.SqlAppendWhere("P.OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pApproveStatus));
				}
				if (pIncludeNotPublish == false) {
					SysPrograms.SqlAppendWhere("P.OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF));
				}
			}

			if (pMovieType.Equals(string.Empty) == false) {
				SysPrograms.SqlAppendWhere("P.MOVIE_TYPE = :MOVIE_TYPE ",ref pWhere);
				list.Add(new OracleParameter("MOVIE_TYPE",pMovieType));
			}

			if (pLoginId.Equals(string.Empty) == false) {
				SysPrograms.SqlAppendWhere("P.LOGIN_ID = :LOGIN_ID ",ref pWhere);
				SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO ",ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginId));
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}

			if (!string.IsNullOrEmpty(pCastMovieAttrTypeSeq)) {
				SysPrograms.SqlAppendWhere("P.CAST_MOVIE_ATTR_TYPE_SEQ = :CAST_MOVIE_ATTR_TYPE_SEQ ",ref pWhere);
				list.Add(new OracleParameter("CAST_MOVIE_ATTR_TYPE_SEQ",pCastMovieAttrTypeSeq));
			}

			if (!string.IsNullOrEmpty(pCastMovieAttrSeq)) {
				SysPrograms.SqlAppendWhere("P.CAST_MOVIE_ATTR_SEQ = :CAST_MOVIE_ATTR_SEQ ",ref pWhere);
				list.Add(new OracleParameter("CAST_MOVIE_ATTR_SEQ",pCastMovieAttrSeq));
			}

			if (!string.IsNullOrEmpty(pCondition.screenId)) {
				if (!string.IsNullOrEmpty(pCastMovieAttrSeq) && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
					using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
						if (oCastMovieAttrTypeValue.GetOne(pSiteCd,pCastMovieAttrSeq)) {
							SysPrograms.SqlAppendWhere("P.PLANNING_TYPE = :PLANNING_TYPE",ref pWhere);
							list.Add(new OracleParameter("PLANNING_TYPE",oCastMovieAttrTypeValue.planningType));
						}
					}
				} else {
					SysPrograms.SqlAppendWhere("P.PLANNING_TYPE > 0",ref pWhere);
				}
			} else if (string.IsNullOrEmpty(pCondition.pickupId)) {
				SysPrograms.SqlAppendWhere("P.PLANNING_TYPE = 0",ref pWhere);
			}

			if (pActCategorySeq.Equals(string.Empty) == false) {
				SysPrograms.SqlAppendWhere("P.ACT_CATEGORY_SEQ =: ACT_CATEGORY_SEQ ",ref pWhere);
				list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pActCategorySeq));
			}

			if (pMovieSeries.Equals(string.Empty) == false) {
				SysPrograms.SqlAppendWhere("P.MOVIE_SERIES = :MOVIE_SERIES ",ref pWhere);
				list.Add(new OracleParameter("MOVIE_SERIES",pMovieSeries));
			}

			// 拡張検索(一覧下部の検索フォームからの検索)の条件追加
			if (pCondition.hasExtend) {
				if (pCondition.onlineStatus == ViCommConst.SeekOnlineStatus.WAITING) {
					pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS = :EXT_ONLINE_STATUS_WAITING ";
					list.Add(new OracleParameter("EXT_ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				} else if (pCondition.onlineStatus == ViCommConst.SeekOnlineStatus.LOGINED) {
					pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS = :EXT_ONLINE_STATUS_LOGINED ";
					list.Add(new OracleParameter("EXT_ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				}
				if (!string.IsNullOrEmpty(pCondition.handleNm)) {
					pWhere = pWhere + " AND P.HANDLE_NM LIKE '%' || :EXT_HANDLE_NM || '%' ";
					list.Add(new OracleParameter("EXT_HANDLE_NM",pCondition.handleNm));
				}
				if (!string.IsNullOrEmpty(pCondition.ageFrom)) {
					pWhere = pWhere + " AND P.AGE >= :EXT_AGE_FROM ";
					list.Add(new OracleParameter("EXT_AGE_FROM",pCondition.ageFrom));
				}
				if (!string.IsNullOrEmpty(pCondition.ageTo)) {
					pWhere = pWhere + " AND P.AGE <= :EXT_AGE_TO ";
					list.Add(new OracleParameter("EXT_AGE_TO",pCondition.ageTo));
				}

				if (pCondition.newCastDay != 0) {
					pWhere = pWhere + " AND SYSDATE <= TO_DATE(P.START_PERFORM_DAY, 'YYYY/MM/DD') + :NEW_CAST_DAY ";
					list.Add(new OracleParameter("NEW_CAST_DAY",pCondition.newCastDay));
				}

				if (pCondition.attrValue.Count > 0) {
					this.CreateAttrQuery(pCondition,ref pWhere,ref list);
				}
			}

			if (!string.IsNullOrEmpty(pCondition.pickupId)) {
				// VW_PICKUP_OBJS00 × P
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.OBJ_SEQ		= P.MOVIE_SEQ ";
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.SITE_CD		= :SITE_CD	";
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.PICKUP_ID		= :PICKUP_ID ";
				pWhere = pWhere + " AND VW_PICKUP_OBJS00.PICKUP_FLAG	= :PICKUP_FLAG ";

				list.Add(new OracleParameter(":SITE_CD",pSiteCd));
				list.Add(new OracleParameter(":PICKUP_ID",pCondition.pickupId));
				list.Add(new OracleParameter(":PICKUP_FLAG",ViCommConst.FLAG_ON));

				if (!string.IsNullOrEmpty(pCondition.pickupStartPubDay)) {
					pWhere = pWhere + " AND VW_PICKUP_OBJS00.PICKUP_START_PUB_DAY	<= TO_DATE(:PICKUP_START_PUB_DAY,'YYYYMMDD') ";
					list.Add(new OracleParameter(":PICKUP_START_PUB_DAY",pCondition.pickupStartPubDay));
				}

				pWhere = pWhere + " AND P.PROFILE_PIC_SEQ IS NOT NULL ";
			}

			if (pOnlyUsed) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.MOVIE_SEQ			");
				oSubQueryBuilder.AppendLine(" ) ");
				pWhere = pWhere + oSubQueryBuilder.ToString();

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));

			} else if (pCondition.unusedFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				StringBuilder oSubQueryBuilder = new StringBuilder();
				oSubQueryBuilder.AppendLine(" AND NOT EXISTS( ");
				oSubQueryBuilder.AppendLine("	SELECT										");
				oSubQueryBuilder.AppendLine("		1										");
				oSubQueryBuilder.AppendLine("	FROM										");
				oSubQueryBuilder.AppendLine("		T_OBJ_USED_HISTORY USED					");
				oSubQueryBuilder.AppendLine("	WHERE										");
				oSubQueryBuilder.AppendLine("		USED.SITE_CD 	= :USED_SITE_CD		AND ");
				oSubQueryBuilder.AppendLine("		USED.USER_SEQ 	= :USED_USER_SEQ	AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_TYPE 	= :USED_OBJ_TYPE    AND ");
				oSubQueryBuilder.AppendLine("		USED.OBJ_SEQ	= P.MOVIE_SEQ			");
				oSubQueryBuilder.AppendLine(" ) ");
				pWhere = pWhere + oSubQueryBuilder.ToString();

				list.Add(new OracleParameter(":USED_SITE_CD",this.sessionObj.site.siteCd));
				list.Add(new OracleParameter(":USED_USER_SEQ",this.sessionObj.GetUserSeq()));
				list.Add(new OracleParameter(":USED_OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX));

			}

			if (!string.IsNullOrEmpty(pCondition.rankType)) {
				SysPrograms.SqlAppendWhere("P.OBJ_RANKING_FLAG = 1",ref pWhere);
			}

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN) && this.sessionObj.IsImpersonated == false) {
				if (pCondition.bookmarkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					SessionMan sessionMan = (SessionMan)sessionObj;
					string sSelfUserSeq = sessionMan.userMan.userSeq;
					string sSelfUserCharNo = sessionMan.userMan.userCharNo;

					if (!string.IsNullOrEmpty(sSelfUserSeq) && !string.IsNullOrEmpty(sSelfUserCharNo)) {
						StringBuilder oSubQueryBuilder = new StringBuilder();
						oSubQueryBuilder.AppendLine(" AND EXISTS(										");
						oSubQueryBuilder.AppendLine("	SELECT											");
						oSubQueryBuilder.AppendLine("		1											");
						oSubQueryBuilder.AppendLine("	FROM											");
						oSubQueryBuilder.AppendLine("		T_OBJ_BOOKMARK BKM							");
						oSubQueryBuilder.AppendLine("	WHERE											");
						oSubQueryBuilder.AppendLine("		BKM.SITE_CD 		= P.SITE_CD			AND	");
						oSubQueryBuilder.AppendLine("		BKM.OBJ_SEQ			= P.MOVIE_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_SEQ		= :BKM_USER_SEQ		AND	");
						oSubQueryBuilder.AppendLine("		BKM.USER_CHAR_NO	= :BKM_USER_CHAR_NO		");
						oSubQueryBuilder.AppendLine(" )													");
						pWhere = pWhere + oSubQueryBuilder.ToString();

						list.Add(new OracleParameter(":BKM_USER_SEQ",sSelfUserSeq));
						list.Add(new OracleParameter(":BKM_USER_CHAR_NO",sSelfUserCharNo));
					}
				}
			}

			// いいねした動画のみ検索
			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)
				&& pCondition.isLikedOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)
			) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;

				SysPrograms.SqlAppendWhere("P.SITE_CD = CML.SITE_CD",ref pWhere);
				SysPrograms.SqlAppendWhere("P.MOVIE_SEQ = CML.MOVIE_SEQ",ref pWhere);
				SysPrograms.SqlAppendWhere(":MAN_USER_SEQ = CML.MAN_USER_SEQ",ref pWhere);
				list.Add(new OracleParameter(":MAN_USER_SEQ",oSessionMan.userMan.userSeq));
			}
		}

		if (pCondition.enabledBlog.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere(" P.ENABLED_BLOG_FLAG = :ENABLED_BLOG_FLAG  ",ref pWhere);
			list.Add(new OracleParameter(":ENABLED_BLOG_FLAG",ViCommConst.FLAG_ON));
		}

		if (!string.IsNullOrEmpty(pCondition.lastTxMailDays)) {
			SysPrograms.SqlAppendWhere("P.LAST_TX_MAIL_DATE >= SYSDATE - :LAST_TX_MAIL_DAYS",ref pWhere);
			list.Add(new OracleParameter("LAST_TX_MAIL_DAYS",pCondition.lastTxMailDays));
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}

		if (pMovieType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {

			if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				SessionMan oSessionMan = (SessionMan)this.sessionObj;
				pWhere += SetFlexibleQuery(pFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list);
			}
		}

		switch (pCondition.sortType) {
			case ViCommConst.SortType.LOGINED:
				pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING) ";
				list.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;
			case ViCommConst.SortType.WAITING:
				pWhere = pWhere + " AND P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING) ";
				list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;

			default:
				break;
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private FlexibleQueryTargetInfo CreateFlexibleQueryTarget(FlexibleQueryType pFlexibleQueryType,SeekCondition pSeekCondition,string pMovieType) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(pFlexibleQueryType);
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			if (pMovieType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
				oFlexibleQueryTarget.JealousyBbs = true;
			}
			switch (pSeekCondition.onlineStatus) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					oFlexibleQueryTarget.JealousyWaitingStatus = true;
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
				case ViCommConst.SeekOnlineStatus.LOGINED:
					oFlexibleQueryTarget.JealousyLoginStatus = true;
					break;
			}
		}
		return oFlexibleQueryTarget;
	}

	public int GetCastMovieCharge(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieSeq) {
		DataSet ds;
		DataRow dr;

		int iChargePoint = 0;
		try {
			conn = DbConnect("CastMovie.GetCastMovieCharge");
			string sSql = "SELECT CHARGE_POINT FROM T_CAST_MOVIE " +
							"WHERE " +
							" SITE_CD		= :SITE_CD		AND " +
							" USER_SEQ		= :USER_SEQ		AND " +
							" USER_CHAR_NO	= :USER_CHAR_NO AND " +
							" MOVIE_SEQ		= :MOVIE_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE");
					if (ds.Tables["T_CAST_MOVIE"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_MOVIE"].Rows[0];
						iChargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iChargePoint;
	}

	public void GetChargeInfoByMovieSeq(string pSiteCd,string pMovieSeq,
		out int pUseIndividualChargeFlag,
		out int pViewerChargePoint,
		out int pUseIndividualPayFlag,
		out int pViewePayAmt
	) {
		DataSet ds;
		DataRow dr;

		pUseIndividualChargeFlag = 0;
		pViewerChargePoint = 0;
		pUseIndividualPayFlag = 0;
		pViewePayAmt = 0;

		string sValue = string.Empty;
		try {
			conn = DbConnect("CastMovie.GetChargeInfoByMovieSeq");
			string sSql = "SELECT USE_INDIVIDUAL_PAY_FLAG,VIEW_PAY_AMT,USE_INDIVIDUAL_CHARGE_FLAG,VIEWER_CHARGE_POINT FROM VW_CAST_MOVIE01 " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE01");
					if (ds.Tables["VW_CAST_MOVIE01"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_MOVIE01"].Rows[0];
						pUseIndividualChargeFlag = int.Parse(dr["USE_INDIVIDUAL_CHARGE_FLAG"].ToString());
						pViewerChargePoint = int.Parse(dr["VIEWER_CHARGE_POINT"].ToString());
						pUseIndividualPayFlag = int.Parse(dr["USE_INDIVIDUAL_PAY_FLAG"].ToString());
						pViewePayAmt = int.Parse(dr["VIEW_PAY_AMT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
	}


	public void UpdateProfileMovie(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pMovieSeq,
		string pMovieTitle,
		string pMovieDoc,
		int pDelFlag,
		int pMovieType,
		string pCastMovieAttrTypeSeq,
		string pCastMovieAttrSeq
	) {
		this.UpdateProfileMovie(pSiteCd,pUserSeq,pUserCharNo,pMovieSeq,pMovieTitle,pMovieDoc,pDelFlag,pMovieType,pCastMovieAttrTypeSeq,pCastMovieAttrSeq,1);
	}

	public void UpdateProfileMovie(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pMovieSeq,
		string pMovieTitle,
		string pMovieDoc,
		int pDelFlag,
		int pMovieType,
		string pCastMovieAttrTypeSeq,
		string pCastMovieAttrSeq,
		int? pObjRankingFlag
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_PROFILE_MOVIE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,pMovieSeq);
			db.ProcedureInParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2,pMovieTitle);
			db.ProcedureInParm("PMOVIE_DOC",DbSession.DbType.VARCHAR2,pMovieDoc);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PMOVIE_TYPE",DbSession.DbType.NUMBER,pMovieType);
			db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pCastMovieAttrTypeSeq);
			db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ",DbSession.DbType.VARCHAR2,pCastMovieAttrSeq);
			db.ProcedureInParm("POBJ_RANKING_FLAG",DbSession.DbType.NUMBER,pObjRankingFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	public string GetMovieTitleByMovieSeq(string pMovieSeq,string pSiteCd) {
		DataSet ds;
		DataRow dr;

		string sMovieTitle = "";
		try {
			conn = DbConnect("CastMovie.GetMovieTitleByMovieSeq");

			string sSql = "SELECT MOVIE_TITLE FROM T_CAST_MOVIE " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE");

					if (ds.Tables["T_CAST_MOVIE"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_MOVIE"].Rows[0];
						sMovieTitle = dr["MOVIE_TITLE"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sMovieTitle;
	}

	public string GetValueByMovieSeq(string pSiteCd,string pMovieSeq,string pValue) {
		DataSet ds;
		DataRow dr;

		string sValue = string.Empty;
		try {
			conn = DbConnect("CastMovie.GetValueByMovieSeq");

			string sSql = "SELECT MOVIE_TITLE,MOVIE_DOC,UPLOAD_DATE FROM T_CAST_MOVIE " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND MOVIE_SEQ = :MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE");

					if (ds.Tables["T_CAST_MOVIE"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_MOVIE"].Rows[0];
						sValue = dr[pValue].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sValue;
	}

	private void AppendAttr(DataSet pDS) {
		SessionObjs oObj = this.sessionObj;

		string sNewDays = oObj.site.newFaceDays.ToString();
		string sSql;

		if (sNewDays.Equals("")) {
			sNewDays = "0";
		}
		string sNewDay = DateTime.Now.AddDays(-1 * int.Parse(sNewDays)).ToString("yyyy/MM/dd");

		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD	AND " +
						"USER_SEQ		= :USER_SEQ	AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO	";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),System.Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();

			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"].ToString())] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	private void AppendBbsMovie(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_DATE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_FLAG",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_USED_SPECIAL_FREE_FLAG",Type.GetType("System.String"));

		string sSiteCd = string.Empty;
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			sSiteCd = oSessionMan.site.siteCd;
			sUserSeq = oSessionMan.userMan.userSeq;
			sUserCharNo = oSessionMan.userMan.userCharNo;
		}

		if (string.IsNullOrEmpty(sSiteCd) || string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sUserCharNo)) {
			return;
		}

		string sWhereClause = String.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		List<string> sObjSeqList = new List<string>();

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			sObjSeqList.Add(iBridUtil.GetStringValue(dr["MOVIE_SEQ"]));

			dr["OBJ_DOWNLOAD_FLAG"] = "0";
			dr["OBJ_DOWNLOAD_DATE"] = "";
			dr["SELF_USED_SPECIAL_FREE_FLAG"] = "0";
		}

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",sSiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",sUserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",sUserCharNo));

		SysPrograms.SqlAppendWhere("OBJ_TYPE = :OBJ_TYPE",ref sWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_TYPE",ViCommConst.ATTACH_MOVIE_INDEX.ToString()));

		SysPrograms.SqlAppendWhere(string.Format("OBJ_SEQ IN ({0})",string.Join(",",sObjSeqList.ToArray())),ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	OBJ_SEQ,");
		oSqlBuilder.AppendLine("	USED_DATE,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG,");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG2");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_OBJ_USED_HISTORY");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["MOVIE_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["OBJ_SEQ"]);

			pDR["OBJ_DOWNLOAD_FLAG"] = "1";
			pDR["OBJ_DOWNLOAD_DATE"] = subDR["USED_DATE"].ToString();

			if (subDR["SPECIAL_FREE_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR) || subDR["SPECIAL_FREE_FLAG2"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				pDR["SELF_USED_SPECIAL_FREE_FLAG"] = ViCommConst.FLAG_ON_STR;
			}
		}
	}

	private void AppendProfileMovie(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_DATE",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("OBJ_DOWNLOAD_FLAG",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add("SELF_USED_SPECIAL_FREE_FLAG",Type.GetType("System.String"));

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["OBJ_DOWNLOAD_FLAG"] = "0";
			dr["OBJ_DOWNLOAD_DATE"] = "";
			dr["SELF_USED_SPECIAL_FREE_FLAG"] = "0";
		}
	}

	public int GetNewlyArrived(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType,int pHour) {
		return this.GetNewlyArrived(pSiteCd,pUserSeq,pUserCharNo,pMovieType,pHour,string.Empty);
	}
	public int GetNewlyArrived(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType,int pHour,string pAttr) {
		DataSet ds;
		int iFlag = ViCommConst.FLAG_OFF;
		try {
			conn = DbConnect("CastMovie.GetNewlyArrived");
			ds = new DataSet();

			string sSql = "SELECT " +
							" COUNT(*) CNT " +
							"FROM " +
							" T_CAST_MOVIE " +
							"WHERE " +
							" SITE_CD		=  :SITE_CD			AND " +
							" USER_SEQ		=  :USER_SEQ		AND " +
							" USER_CHAR_NO	=  :USER_CHAR_NO	AND " +
							" MOVIE_TYPE	=  :MOVIE_TYPE		AND " +
							" OBJ_NOT_APPROVE_FLAG	=  :OBJ_NOT_APPROVE_FLAG		AND " +
							" OBJ_NOT_PUBLISH_FLAG	=  :OBJ_NOT_PUBLISH_FLAG		AND " +
							" UPLOAD_DATE	>= :UPLOAD_DATE		";

			if (!string.IsNullOrEmpty(pAttr)) {
				sSql += "AND  CAST_MOVIE_ATTR_SEQ	=  :CAST_MOVIE_ATTR_SEQ	";
			}

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("MOVIE_TYPE",pMovieType);
				cmd.Parameters.Add("OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("OBJ_NOT_PUBLISH_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("UPLOAD_DATE",OracleDbType.Date,DateTime.Now.AddHours(pHour * -1),ParameterDirection.Input);
				if (!string.IsNullOrEmpty(pAttr)) {
					cmd.Parameters.Add("CAST_MOVIE_ATTR_SEQ",pAttr);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow drSub = ds.Tables[0].Rows[0];
						iFlag = int.Parse(drSub["CNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return iFlag;
	}
	private void CreateAttrQuery(SeekCondition pCondition,ref string pWhere,ref ArrayList pList) {
		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				pWhere = pWhere + string.Format(" AND AT{0}.SITE_CD				= P.SITE_CD				",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_SEQ			= P.USER_SEQ			",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO		",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0}",i + 1);
				pList.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						pWhere += " AND (";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									pWhere += " OR ";
								}
								pWhere = pWhere + string.Format(" (AT{0}.CAST_ATTR_INPUT_VALUE	LIKE '%' || :CAST_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						pWhere += ")";
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');
					if (sValues.Length > 1) {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	IN (		",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0)
								pWhere = pWhere + " , ";
							pWhere = pWhere + string.Format(" :CAST_ATTR_SEQ{0}_{1}		",i + 1,j);

							pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						pWhere = pWhere + " ) ";
					} else {
						pWhere = pWhere + string.Format(" AND AT{0}.CAST_ATTR_SEQ	= :CAST_ATTR_SEQ{0}		",i + 1);
						pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					pWhere = pWhere + string.Format(" AND AT{0}.GROUPING_CD		= :GROUPING_CD{0}	",i + 1);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}
	}

	public int GetMovieCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT													");
		oSqlBuilder.AppendLine("	COUNT(T_CAST_MOVIE.MOVIE_SEQ)						");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE										");
		oSqlBuilder.AppendLine(" WHERE													");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.SITE_CD		= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.USER_CHAR_NO	= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.MOVIE_TYPE		= :MOVIE_TYPE		AND	");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.OBJ_NA_FLAG	= :OBJ_NA_FLAG		AND	");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.PLANNING_TYPE	= 0						");

		System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":MOVIE_TYPE",pMovieType));
		oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",ViCommConst.FLAG_OFF));

		return ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	public int GetMovieCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType,string pAttr,string pObjNaFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT													");
		oSqlBuilder.AppendLine("	COUNT(T_CAST_MOVIE.MOVIE_SEQ)						");
		oSqlBuilder.AppendLine(" FROM													");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE										");
		oSqlBuilder.AppendLine(" WHERE													");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.SITE_CD		= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE.USER_CHAR_NO	= :USER_CHAR_NO			");

		System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		if (!string.IsNullOrEmpty(pMovieType)) {
			oSqlBuilder.AppendLine("	AND	T_CAST_MOVIE.MOVIE_TYPE		= :MOVIE_TYPE");
			oParamList.Add(new OracleParameter(":MOVIE_TYPE",pMovieType));
		}
		if (!string.IsNullOrEmpty(pAttr)) {
			oSqlBuilder.AppendLine("	AND	T_CAST_MOVIE.CAST_MOVIE_ATTR_SEQ		= :CAST_MOVIE_ATTR_SEQ");
			oParamList.Add(new OracleParameter(":CAST_MOVIE_ATTR_SEQ",pAttr));
		}
		if (!string.IsNullOrEmpty(pObjNaFlag)) {
			oSqlBuilder.AppendLine("	AND	T_CAST_MOVIE.OBJ_NA_FLAG	= :OBJ_NA_FLAG");
			oParamList.Add(new OracleParameter(":OBJ_NA_FLAG",pObjNaFlag));
		}

		return ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	public string ModifyGameCastMovie(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieSeq,string pMovieDoc) {
		string sResult = string.Empty;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_GAME_CAST_MOVIE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.VARCHAR2,pMovieSeq);
			db.ProcedureInParm("pMOVIE_DOC",DbSession.DbType.VARCHAR2,pMovieDoc);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			sResult = db.GetStringValue("pRESULT");
		}

		return sResult;
	}

	public DataSet GetOneByMovieSeq(string pSiteCd,string pMovieSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	USER_SEQ					,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE					");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	MOVIE_SEQ	= :MOVIE_SEQ		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MOVIE_SEQ",pMovieSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
