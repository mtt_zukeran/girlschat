﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員写真

--	Progaram ID		: UserManPic
--
--  Creation Date	: 2009.08.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

[System.Serializable]
public class UserManPic:DbSession {

	public UserManPic() {
	}

	public int GetPageCountByLoginId(string pSiteCd,string pLoginId,string pPicType,string pNotApproveStatus,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("UserManPic.GetPageCountByLoginId");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_PIC01 ";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhereByLoginId(pSiteCd,pLoginId,pPicType,pNotApproveStatus,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollectionByLoginId(string pSiteCd,string pLoginId,string pPicType,string pNotApproveStatus,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try {
			conn = DbConnect("UserManPic.GetPageCollectionByLoginId");
			ds = new DataSet();

			string sOrder = "ORDER BY P.SITE_CD,P.USER_SEQ,P.DISPLAY_POSITION,P.PIC_SEQ DESC ";
			string sOrder2 = "ORDER BY SITE_CD,USER_SEQ,DISPLAY_POSITION,PIC_SEQ DESC ";

			string sSql = "SELECT * FROM " +
							"(" +
							"SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"LOGIN_ID				," +
								"DISPLAY_POSITION		," +
								"PIC_SEQ				," +
								"PHOTO_IMG_PATH			AS OBJ_PHOTO_IMG_PATH          ," +
								"SMALL_PHOTO_IMG_PATH	AS OBJ_SMALL_PHOTO_IMG_PATH    ," +
								"PROFILE_PIC_FLAG		," +
								"UPDATE_DATE			," +
								"UPLOAD_DATE			," +
								"PIC_TYPE				," +
								"OBJ_NOT_APPROVE_FLAG	," +
								"OBJ_NOT_PUBLISH_FLAG	," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							"FROM VW_USER_MAN_PIC01 P ";

			string sWhere = "";

			OracleParameter[] objParms = CreateWhereByLoginId(pSiteCd,pLoginId,pPicType,pNotApproveStatus,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder2;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_PIC01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhereByLoginId(string pSiteCd,string pLoginId,string pPicType,string pNotApproveStatus,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = " WHERE SITE_CD = :SITE_CD AND  LOGIN_ID = :LOGIN_ID ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("LOGIN_ID",pLoginId));

		if (!pPicType.Equals(string.Empty)) {
			pWhere += "AND PIC_TYPE = :PIC_TYPE ";
			list.Add(new OracleParameter("PIC_TYPE",pPicType));
		}

		if (!pNotApproveStatus.Equals(string.Empty)) {
			pWhere += "AND OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG ";
			list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveStatus));
		}
		pWhere += "AND OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG ";
		list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",ViComm.ViCommConst.FLAG_OFF_STR));
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public string GetValueByPicSeq(string pSiteCd,string pPicSeq,string pValue) {
		DataSet ds;
		DataRow dr;

		string sValue = string.Empty;
		try {
			conn = DbConnect("UserManPic.GetValueByPicSeq");

			string sSql = "SELECT UPLOAD_DATE,PHOTO_IMG_PATH FROM VW_USER_MAN_PIC01 " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND PIC_SEQ = :PIC_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PIC_SEQ",pPicSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_PIC01");

					if (ds.Tables["VW_USER_MAN_PIC01"].Rows.Count != 0) {
						dr = ds.Tables["VW_USER_MAN_PIC01"].Rows[0];
						sValue = dr[pValue].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sValue;
	}
}

