﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ライブカメラ
--	Progaram ID		: LiveCamera
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;


[System.Serializable]
public class LiveCamera:DbSession {

	public LiveCamera() {
	}

	public bool IsMainCameraActive(string pLiveSeq) {
		DataSet ds;
		DataRow dr;
		bool bRet = false;
		try{
			conn = DbConnect("LiveCamera.IsMainCameraActive");
			string sSql = "SELECT CONNECTED_CAMERA_FLAG FROM VW_LIVE_BROADCAST_CAMERA01 "+
								"WHERE LIVE_SEQ = :LIVE_SEQ AND MAIN_CAMERA_FLAG = :MAIN_CAMERA_FLAG";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("LIVE_SEQ", pLiveSeq);
				cmd.Parameters.Add("MAIN_CAMERA_FLAG", ViComm.ViCommConst.FLAG_ON);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"LIVE_CAMERA");
					if (ds.Tables["LIVE_CAMERA"].Rows.Count != 0) {
						dr = ds.Tables["LIVE_CAMERA"].Rows[0];
						if (dr["CONNECTED_CAMERA_FLAG"].ToString().Equals("1")) {
							bRet = true;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bRet;
	}

	public DataSet GetList(string pSiteCd,string pLiveSeq,bool pOnLineOnly) {
		DataSet ds;
		try{
			conn = DbConnect("LiveCamera.GetList");
			ds = new DataSet();

			string sSql = "SELECT " +
								"CAMERA_ID," +
								"LIVE_CAMERA_NM " +
							"FROM " +
							" VW_LIVE_BROADCAST_CAMERA01 " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND LIVE_SEQ = :LIVE_SEQ";

			if (pOnLineOnly) {
				sSql = sSql + " AND CONNECTED_CAMERA_FLAG != 0";
			}
			sSql = sSql + " ORDER BY SITE_CD,LIVE_SEQ,CAMERA_ID,MAIN_CAMERA_FLAG DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("LIVE_SEQ",pLiveSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}


}
