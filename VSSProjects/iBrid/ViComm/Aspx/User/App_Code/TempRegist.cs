﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 仮登録
--	Progaram ID		: TempRegist
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using ViComm;

[System.Serializable]
public class TempRegist:DbSession {

	public string adCd;
	public string siteCd;
	public string castLoginId;
	public string castHandleNm;
	public string affiliaterCd;
	public string registAffiliateCd;
	public string trackingUrl;
	public string trackingAdditionInfo;
	public string trackingJs;
	public string registStatus;
	public string mobileCarrierCd;
	public int authResultTransType;
	public string registIpAddr;
	public string terminalUniqueId;
	public string imodeId;

	public string prepaidId;
	public string prepaidPw;
	public string emailAddr;

	public TempRegist() {
		adCd = string.Empty;
		siteCd = string.Empty;
		castLoginId = string.Empty;
		castHandleNm = string.Empty;
		prepaidId = string.Empty;
		prepaidPw = string.Empty;
		emailAddr = string.Empty;
		trackingUrl = string.Empty;
		trackingAdditionInfo = string.Empty;
		trackingJs = string.Empty;
		registStatus = string.Empty;
		mobileCarrierCd = string.Empty;
		authResultTransType = 0;
		registIpAddr = string.Empty;
		terminalUniqueId = string.Empty;
		imodeId = string.Empty;
	}

	public bool GetOne(string pTempRegistId) {
		return GetOne(pTempRegistId,"");
	}

	public bool GetOneByUserSeq(string pUserSeq) {
		return GetOne("",pUserSeq);
	}

	private bool GetOne(string pTempRegistId,string pUserSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("TempRegist.GetOne");

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("AD_CD					,").AppendLine();
			sSql.Append("EMAIL_ADDR				,").AppendLine();
			sSql.Append("SITE_CD				,").AppendLine();
			sSql.Append("AD_CD					,").AppendLine();
			sSql.Append("TERMINAL_UNIQUE_ID		,").AppendLine();
			sSql.Append("IMODE_ID				,").AppendLine();
			sSql.Append("REGIST_IP_ADDR     	,").AppendLine();
			sSql.Append("MOBILE_CARRIER_CD		,").AppendLine();
			sSql.Append("MOBILE_TERMINAL_NM		,").AppendLine();
			sSql.Append("REGIST_APPLY_DATE		,").AppendLine();
			sSql.Append("REGIST_COMPLITE_DATE	,").AppendLine();
			sSql.Append("USER_SEQ				,").AppendLine();
			sSql.Append("REGIST_AFFILIATE_CD	,").AppendLine();
			sSql.Append("TRACKING_URL  			,").AppendLine();
			sSql.Append("TRACKING_ADDITION_INFO	,").AppendLine();
			sSql.Append("TRACKING_JS  			,").AppendLine();
			sSql.Append("AUTH_RESULT_TRANS_TYPE	,").AppendLine();
			sSql.Append("CAST_LOGIN_ID			,").AppendLine();
			sSql.Append("CAST_USER_SEQ			,").AppendLine();
			sSql.Append("PREPAID_ID				,").AppendLine();
			sSql.Append("PREPAID_PW				,").AppendLine();
			sSql.Append("EMAIL_ADDR				,").AppendLine();
			sSql.Append("CAST_HANDLE_NM			,").AppendLine();
			sSql.Append("REGIST_STATUS			,").AppendLine();
			sSql.Append("AUTH_RESULT_TRANS_TYPE	,").AppendLine();
			sSql.Append("AFFILIATER_CD			 ").AppendLine();
			sSql.Append("FROM					").AppendLine();
			sSql.Append("VW_TEMP_REGIST01		").AppendLine();
			sSql.Append("WHERE					").AppendLine();

			if (pTempRegistId.Equals(string.Empty) == false) {
				sSql.Append("TEMP_REGIST_ID = :KEY ");
			} else {
				sSql.Append("USER_SEQ = :KEY ");
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				if (pTempRegistId.Equals(string.Empty) == false) {
					cmd.Parameters.Add("KEY",pTempRegistId);
				} else {
					cmd.Parameters.Add("KEY",pUserSeq);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"TEMP_REGIST_ID");

					if (ds.Tables["TEMP_REGIST_ID"].Rows.Count != 0) {
						dr = ds.Tables["TEMP_REGIST_ID"].Rows[0];
						adCd = iBridCommLib.iBridUtil.GetStringValue(dr["AD_CD"]);
						siteCd = dr["SITE_CD"].ToString();
						castLoginId = dr["CAST_LOGIN_ID"].ToString();
						castHandleNm = dr["CAST_HANDLE_NM"].ToString();
						affiliaterCd = dr["AFFILIATER_CD"].ToString();
						registAffiliateCd = dr["REGIST_AFFILIATE_CD"].ToString();
						prepaidId = dr["PREPAID_ID"].ToString();
						prepaidPw = dr["PREPAID_PW"].ToString();
						emailAddr = dr["EMAIL_ADDR"].ToString();
						trackingUrl = dr["TRACKING_URL"].ToString();
						trackingAdditionInfo = dr["TRACKING_ADDITION_INFO"].ToString();
						trackingJs = dr["TRACKING_JS"].ToString();
						int iTransType;
						authResultTransType = int.TryParse(dr["AUTH_RESULT_TRANS_TYPE"].ToString(),out iTransType) ? iTransType : ViCommConst.AUTH_AFFILIATE_TRASN_NG;
						registStatus = dr["REGIST_STATUS"].ToString();
						mobileCarrierCd = dr["MOBILE_CARRIER_CD"].ToString();
						registIpAddr = dr["REGIST_IP_ADDR"].ToString();
						terminalUniqueId = dr["TERMINAL_UNIQUE_ID"].ToString();
						imodeId = dr["IMODE_ID"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void RegistUserManTemp(
			string pTerminalUniqueId,
			string pIModeId,
			string pIpAddr,
			string pTel,
			string pLoginPassword,
			string pHandleNm,
			string pBirthday,
			string pCastLoginId,
			string pCastCharNo,
			out string pTempRegistId,
			out string pResult,
			string[] pAttrTypeSeq,
			string[] pAttrSeq,
			string[] pAttrInputValue,
			int pMaxAttrCount,
			string[] pRegistSiteCd,
			int pGameRegistFlag,
			string pTwitterId
		) {
		using (DbSession db = new DbSession()) {

			SessionMan userObjs = (SessionMan)HttpContext.Current.Session["objs"];

			int iRegistSiteCount = 0;

			if (pRegistSiteCd != null) {
				iRegistSiteCount = pRegistSiteCd.Length;
			}
			db.PrepareProcedure("REGIST_USER_MAN_TEMP");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,userObjs.adCd);
			db.ProcedureInParm("PAFFILIATER_CD",DbSession.DbType.VARCHAR2,userObjs.affiliateCompany);
			db.ProcedureInParm("PREGIST_AFFILIATE_CD",DbSession.DbType.VARCHAR2,userObjs.affiliaterCd);
			db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,userObjs.carrier);
			db.ProcedureInParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,pCastLoginId);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureInParm("PREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,pIpAddr);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pLoginPassword);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,userObjs.mobileUserAgent);
			db.ProcedureInParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,userObjs.introducerFriendCd);
			db.ProcedureInParm("PPREPAID_ID",DbSession.DbType.VARCHAR2,userObjs.userMan.prepaidId);
			db.ProcedureInParm("PPREPAID_PW",DbSession.DbType.VARCHAR2,userObjs.userMan.prepaidPw);
			db.ProcedureInParm("POPEN_ID",DbSession.DbType.VARCHAR2,userObjs.tempOpenId);
			db.ProcedureInParm("POPEN_ID_TYPE",DbSession.DbType.VARCHAR2,userObjs.tempOpenIdType);
			db.ProcedureInParm("POUTSIDE_IF_TEMP_REGIST_FLAG",DbSession.DbType.NUMBER,ViComm.ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PGAME_REGIST_FLAG",DbSession.DbType.NUMBER,pGameRegistFlag);
			db.ProcedureInArrayParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrTypeSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrSeq);
			db.ProcedureInArrayParm("PMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,pMaxAttrCount,pAttrInputValue);
			db.ProcedureInParm("PMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER,pMaxAttrCount);
			db.ProcedureInArrayParm("pSYNC_REGIST_SITE_CD",DbSession.DbType.VARCHAR2,iRegistSiteCount,pRegistSiteCd);
			db.ProcedureInParm("PSYNC_REGIST_COUNT",DbSession.DbType.NUMBER,iRegistSiteCount);
			db.ProcedureOutParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEXIST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEXIST_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PTWITTER_ID",DbSession.DbType.VARCHAR2,pTwitterId);
			db.ExecuteProcedure();
			pTempRegistId = db.GetStringValue("PTEMP_REGIST_ID");
			pResult = db.GetStringValue("PRESULT");
		}
	}

	public void ApplyRegistUserPC(
		string pTempRegistId,
		string pMobileCarrierCd,
		string pTerminalUniqueId,
		string pIModeId,
		string pRegistIPAddr,
		string pMobileTerminalNm,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("APPLY_REGIST_USER_PC");
			db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,pTempRegistId);
			db.ProcedureInParm("pMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,pMobileCarrierCd);
			db.ProcedureInParm("pTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("pIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureInParm("pREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,pRegistIPAddr);
			db.ProcedureInParm("pMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
	
	public void RegistUserWomanTemp(
		   string pTerminalUniqueId,
		   string pIModeId,
		   string pIpAddr,
		   string pTel,
		   string pPassword,
		   string pCastNm,
		   string pCastKanaNm,
		   string pBirthday,
		   string pQuestionDoc,
		   string pHandleNm,
		   int pGameRegistFlag,
		   out string pTempRegistId,
		   out string pResult
	   ) {
		RegistUserWomanTemp(
			pTerminalUniqueId,
			pIModeId,
			pIpAddr,
			pTel,
			pPassword,
			pCastNm,
			pCastKanaNm,
			pBirthday,
			pQuestionDoc,
			pHandleNm,
			pGameRegistFlag,
			ViCommConst.FLAG_OFF,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			out pTempRegistId,
			out pResult
		);
	}

	public void RegistUserWomanTemp(
		string pTerminalUniqueId,
		string pIModeId,
		string pIpAddr,
		string pTel,
		string pPassword,
		string pCastNm,
		string pCastKanaNm,
		string pBirthday,
		string pQuestionDoc,
		string pHandleNm,
		int pGameRegistFlag,
		int pIdolRegistFlag,
		string pPrefectureCd,
		string pAddress1,
		string pGuardianNm,
		string pGuardianTel,
		out string pTempRegistId,
		out string pResult
	) {
		RegistUserWomanTemp(
			pTerminalUniqueId,
			pIModeId,
			pIpAddr,
			pTel,
			pPassword,
			pCastNm,
			pCastKanaNm,
			pBirthday,
			pQuestionDoc,
			pHandleNm,
			pGameRegistFlag,
			pIdolRegistFlag,
			pPrefectureCd,
			pAddress1,
			pGuardianNm,
			pGuardianTel,
			string.Empty,
			string.Empty,
			string.Empty,
			out pTempRegistId,
			out pResult
		);
	}

	public void RegistUserWomanTemp(
			string pTerminalUniqueId,
			string pIModeId,
			string pIpAddr,
			string pTel,
			string pPassword,
			string pCastNm,
			string pCastKanaNm,
			string pBirthday,
			string pQuestionDoc,
			string pHandleNm,
			int pGameRegistFlag,
			int pIdolRegistFlag,
			string pPrefectureCd,
			string pAddress1,
			string pGuardianNm,
			string pGuardianTel,
			string pIdolSexAttrSeq,
			string pIdolWishCastTypeAttrSeq,
			string pTwitterId,
			out string pTempRegistId,
			out string pResult
		) {
		using (DbSession db = new DbSession()) {

			SessionWoman userObjs = (SessionWoman)HttpContext.Current.Session["objs"];

			db.PrepareProcedure("REGIST_USER_WOMAN_TEMP");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,userObjs.site.siteCd);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,userObjs.adCd);
			db.ProcedureInParm("PAFFILIATER_CD",DbSession.DbType.VARCHAR2,userObjs.affiliateCompany);
			db.ProcedureInParm("PREGIST_AFFILIATE_CD",DbSession.DbType.VARCHAR2,userObjs.affiliaterCd);
			db.ProcedureInParm("PMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,userObjs.carrier);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureInParm("PREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,pIpAddr);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,pPassword);
			db.ProcedureInParm("PCAST_NM",DbSession.DbType.VARCHAR2,pCastNm);
			db.ProcedureInParm("PCAST_KANA_NM",DbSession.DbType.VARCHAR2,pCastKanaNm);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("PMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,userObjs.mobileUserAgent);
			db.ProcedureInParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,userObjs.introducerFriendCd);
			db.ProcedureInParm("PQUESTION_DOC",DbSession.DbType.VARCHAR2,pQuestionDoc);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("PGAME_REGIST_FLAG",DbSession.DbType.VARCHAR2,pGameRegistFlag);
			db.ProcedureOutParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PIDOL_REGIST_FLAG",DbSession.DbType.VARCHAR2,pIdolRegistFlag);
			db.ProcedureInParm("PPREFECTURE_CD",DbSession.DbType.VARCHAR2,pPrefectureCd);
			db.ProcedureInParm("PADDRESS1",DbSession.DbType.VARCHAR2,pAddress1);
			db.ProcedureInParm("PGUARDIAN_NM",DbSession.DbType.VARCHAR2,pGuardianNm);
			db.ProcedureInParm("PGUARDIAN_TEL",DbSession.DbType.VARCHAR2,pGuardianTel);
			db.ProcedureInParm("PIDOL_SEX_ATTR_SEQ",DbSession.DbType.VARCHAR2,pIdolSexAttrSeq);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PIDOL_WISH_CAST_TYPE_ATTR_SEQ",DbSession.DbType.VARCHAR2,pIdolWishCastTypeAttrSeq);
			db.ProcedureInParm("PTWITTER_ID",DbSession.DbType.VARCHAR2,pTwitterId);
			db.ExecuteProcedure();
			pTempRegistId = db.GetStringValue("PTEMP_REGIST_ID");
			pResult = db.GetStringValue("PRESULT");
		}
	}

	public void CompliteTrackingReport(string pUserSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TEMP_REGIST_STATUS");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void TrackingReport(string pSiteCd,string pUserSeq,string pReportTiming,string pSexCd) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TRACKING_REPORT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PREPORT_TIMING",DbSession.DbType.VARCHAR2,pReportTiming);
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
	public bool GetValue(string pTempRegistId,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	CAST_NM								, ").AppendLine();
			sSql.Append("	CAST_KANA_NM						, ").AppendLine();
			sSql.Append("	BIRTHDAY							, ").AppendLine();
			sSql.Append("	QUESTION_DOC						  ").AppendLine();
			sSql.Append("FROM									  ").AppendLine();
			sSql.Append("	T_TEMP_REGIST						  ").AppendLine();
			sSql.Append("WHERE									  ").AppendLine();
			sSql.Append("	TEMP_REGIST_ID = :TEMP_REGIST_ID	  ").AppendLine();
			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":TEMP_REGIST_ID",pTempRegistId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TEMP_REGIST");
					if (ds.Tables["T_TEMP_REGIST"].Rows.Count != 0) {
						dr = ds.Tables["T_TEMP_REGIST"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public bool GetLoginIdPass(string pTempRegistId,out string pLoginId,out string pLoginPassword) {

		DataSet ds;
		DataRow dr;
		bool bExist = false;

		pLoginId = string.Empty;
		pLoginPassword = string.Empty;
		
		try {
			conn = DbConnect("TempRegist.GetLoginIdPass");

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	LOGIN_ID		,	").AppendLine();
			sSql.Append("	LOGIN_PASSWORD		").AppendLine();
			sSql.Append("FROM					").AppendLine();
			sSql.Append("	VW_TEMP_REGIST04	").AppendLine();
			sSql.Append("WHERE					").AppendLine();
			sSql.Append("	TEMP_REGIST_ID = :TEMP_REGIST_ID ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("TEMP_REGIST_ID",pTempRegistId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);

					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						pLoginId = dr["LOGIN_ID"].ToString();
						pLoginPassword = dr["LOGIN_PASSWORD"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void UpdateCarrier(string pTempRegistId,string pCarrierCd) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TEMP_REGIST_CARRIER");
			db.ProcedureInParm("pKEY",DbSession.DbType.VARCHAR2,pTempRegistId);
			db.ProcedureInParm("pCARRIER_CD",DbSession.DbType.VARCHAR2,pCarrierCd);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void ApplyRegistCastPC(
		string pTempRegistId,
		string pMobileCarrierCd,
		string pTerminalUniqueId,
		string pIModeId,
		string pRegistIPAddr,
		string pMobileTerminalNm,
		out string pResult
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("APPLY_REGIST_CAST_PC");
			db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,pTempRegistId);
			db.ProcedureInParm("pMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,pMobileCarrierCd);
			db.ProcedureInParm("pTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("pIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureInParm("pREGIST_IP_ADDR",DbSession.DbType.VARCHAR2,pRegistIPAddr);
			db.ProcedureInParm("pMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
