/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: Cast PC
--	Title			: 男性会員キャラクター
--	Progaram ID		: UserManCharacter
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class UserManCharacter:DbSession {

	public UserManCharacter() {
	}

	public int GetOnlineCount(
		string pSiteCd
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		string sSql =
					" SELECT " +
						" COUNT(*) AS ROW_COUNT " +
					" FROM " +
						"VW_USER_MAN_CHARACTER03 " +
					" WHERE " +
						" SITE_CD					= :SITE_CD AND		" +
						" CHARACTER_ONLINE_STATUS	IN (" + GetOnlineCountWhere() + ") AND " +
						" USER_STATUS				IN (:USER_STATUS1,:USER_STATUS2,:USER_STATUS3,:USER_STATUS4,:USER_STATUS5,:USER_STATUS6)";
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_STATUS1",ViCommConst.USER_MAN_NORMAL);
				cmd.Parameters.Add("USER_STATUS2",ViCommConst.USER_MAN_TEL_NO_AUTH);
				cmd.Parameters.Add("USER_STATUS3",ViCommConst.USER_MAN_NEW);
				cmd.Parameters.Add("USER_STATUS4",ViCommConst.USER_MAN_NO_RECEIPT);
				cmd.Parameters.Add("USER_STATUS5",ViCommConst.USER_MAN_NO_USED);
				cmd.Parameters.Add("USER_STATUS6",ViCommConst.USER_MAN_DELAY);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	private string GetOnlineCountWhere() {
		StringBuilder sb = new StringBuilder();
		sb.Append(ViCommConst.USER_LOGINED).Append(",");
		sb.Append(ViCommConst.USER_TALKING);
		return sb.ToString();
	}

	public bool GetCharacterByPrioritySite(
		string pUserSeq,
		out string pHandleNm,
		out string pBirthday
	) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pHandleNm = string.Empty;
		pBirthday = string.Empty;

		StringBuilder sSql = new StringBuilder();
		sSql.AppendLine("SELECT			");
		sSql.AppendLine("	HANDLE_NM	,");
		sSql.AppendLine("	BIRTHDAY	");
		sSql.AppendLine("FROM			");
		sSql.AppendLine("	VW_USER_MAN_CHARACTER04	");
		sSql.AppendLine("WHERE			");
		sSql.AppendLine("	USER_SEQ	= :USER_SEQ	");
		sSql.AppendLine("ORDER BY		");
		sSql.AppendLine("	PRIORITY	");
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					bExist = true;
					pHandleNm = dr["HANDLE_NM"].ToString();
					pBirthday = dr["BIRTHDAY"].ToString();
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void CreateManCharacter(string pSiteCd,string pUserSeq,string pHandleNm,string pBirthday,string pAdCd) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CREATE_MAN_CHARACTER");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pHANDLE_NM",DbSession.DbType.VARCHAR2,pHandleNm);
			db.ProcedureInParm("pBIRTHDAY",DbSession.DbType.VARCHAR2,pBirthday);
			db.ProcedureInParm("pAD_CD",DbSession.DbType.VARCHAR2,pAdCd);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public bool IsActivated(string pSiteCd,string pUserSeq,string pUserCharNo) {

		DataSet ds;
		DataRow dr;
		bool bRet = false;
		StringBuilder sSql = new StringBuilder();
		sSql.AppendLine("SELECT					");
		sSql.AppendLine("	NA_FLAG				");
		sSql.AppendLine("FROM									");
		sSql.AppendLine("	T_USER_MAN_CHARACTER				");
		sSql.AppendLine("WHERE									");
		sSql.AppendLine("		SITE_CD			= :SITE_CD		");
		sSql.AppendLine("	AND	USER_SEQ		= :USER_SEQ		");
		sSql.AppendLine("	AND	USER_CHAR_NO	= :USER_CHAR_NO	");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count > 0) {
					dr = ds.Tables[0].Rows[0];
					if (dr["NA_FLAG"].ToString().Equals(ViCommConst.NA_CHAR_NONE)) {
						bRet = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bRet;
	}

	public string GetUserSeq(string pSiteCd,string pLoginId,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine(" 	USER_SEQ                    ");
		oSqlBuilder.AppendLine(" FROM                           ");
		oSqlBuilder.AppendLine(" 	VW_USER_MAN_CHARACTER00     ");
		oSqlBuilder.AppendLine(" WHERE                          ");
		oSqlBuilder.AppendLine(" 	SITE_CD		 = :SITE_CD			AND ");
		oSqlBuilder.AppendLine(" 	LOGIN_ID	 = :LOGIN_ID		AND ");
		oSqlBuilder.AppendLine(" 	USER_CHAR_NO = :USER_CHAR_NO		");

		string sUserSeq = string.Empty;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":LOGIN_ID",pLoginId);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				sUserSeq = iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return sUserSeq;
	}

	public void GetUserManData(string pSiteCd,string pUserSeq,string pUserCharNo,out string pHandelNm,out string pBalPoint,out string pAge,out string pPicSeq) {
		DataSet ds;
		DataRow dr;

		pHandelNm = string.Empty;
		pBalPoint = string.Empty;
		pAge = string.Empty;
		pPicSeq = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT			");
		oSqlBuilder.AppendLine(" 	HANDLE_NM	,	");
		oSqlBuilder.AppendLine(" 	BAL_POINT	,	");
		oSqlBuilder.AppendLine(" 	AGE			,	");
		oSqlBuilder.AppendLine(" 	PIC_SEQ			");
		oSqlBuilder.AppendLine(" FROM                           ");
		oSqlBuilder.AppendLine(" 	VW_USER_MAN_CHARACTER00     ");
		oSqlBuilder.AppendLine(" WHERE                          ");
		oSqlBuilder.AppendLine(" 	SITE_CD		 = :SITE_CD			AND ");
		oSqlBuilder.AppendLine(" 	USER_SEQ	 = :USER_SEQ		AND ");
		oSqlBuilder.AppendLine(" 	USER_CHAR_NO = :USER_CHAR_NO		");

		string sUserSeq = string.Empty;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count > 0) {
					dr = ds.Tables[0].Rows[0];
					pHandelNm = iBridUtil.GetStringValue(dr["HANDLE_NM"]);
					pBalPoint = iBridUtil.GetStringValue(dr["BAL_POINT"]);
					pAge = iBridUtil.GetStringValue(dr["AGE"]);
					pPicSeq = iBridUtil.GetStringValue(dr["PIC_SEQ"]);
				}
			}
		} finally {
			conn.Close();
		}
	}
	
	public DataSet GetOneByUserSeq(string pSiteCd,string pUserSeq,string pUserCharNo) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MOBILE_MAIL_RX_TIME_USE_FLAG		,	");
		oSqlBuilder.AppendLine("	MOBILE_MAIL_RX_START_TIME			,	");
		oSqlBuilder.AppendLine("	MOBILE_MAIL_RX_END_TIME				,	");
		oSqlBuilder.AppendLine("	MOBILE_MAIL_RX_TIME_USE_FLAG2		,	");
		oSqlBuilder.AppendLine("	MOBILE_MAIL_RX_START_TIME2			,	");
		oSqlBuilder.AppendLine("	MOBILE_MAIL_RX_END_TIME2				");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}

