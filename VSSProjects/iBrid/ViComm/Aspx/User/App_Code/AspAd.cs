﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ASP広告
--	Progaram ID		: AspAd
--
--  Creation Date	: 2010.09.27
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System.Collections;
using Oracle.DataAccess.Client;
using System.Data;
using System.Text;
using System;
using ViComm;

[System.Serializable]
public class AspAd:DbSession {
	public decimal recCount;

	public int GetPageCount(string pSiteCd,string pSexCd,string pAdPublicationTopic,string pAffiliatePointFrom,string pAffiliatePointTo,string pAdCategory,string pMonthlyFeeFrom,string pMonthlyFeeTo,string pCarrier,bool pExcludeSettled,string pUserSeq,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("AspAd.GetPageCount");
			ds = new DataSet();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pAdPublicationTopic,pAffiliatePointFrom,pAffiliatePointTo,pAdCategory,pMonthlyFeeFrom,pMonthlyFeeTo,pCarrier,pExcludeSettled,pUserSeq,ref sWhere);

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM VW_ASP_AD02 ");
			sSql.Append(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd)) {

				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				da.Fill(ds,"VW_ASP_AD02");

				if (ds.Tables["VW_ASP_AD02"].Rows.Count != 0) {
					dr = ds.Tables["VW_ASP_AD02"].Rows[0];
					recCount = Decimal.Parse(dr["ROW_COUNT"].ToString());
					pRecCount = recCount;
					iPages = (int)Math.Ceiling(recCount / pRecPerPage);
				}
			}
			conn.Close();
		} catch {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,string pAdPublicationTopic,string pAffiliatePointFrom,string pAffiliatePointTo,string pAdCategory,string pMonthlyFeeFrom,string pMonthlyFeeTo,string pCarrier,bool pExcludeSettled,string pUserSeq,bool pOrderByNew,string pOrderType,int pPageNo,int pRecPerPage) {

		DataSet ds;
		try {
			conn = DbConnect("AspAd.GetPageCollection");
			ds = new DataSet();

			string sOrder = " ORDER BY AFFILIATE_POINT DESC, APPLY_START_DAY DESC, ASP_AD_CD ";
			if (pOrderByNew) {
				sOrder = " ORDER BY APPLY_START_DAY DESC, AFFILIATE_POINT DESC, ASP_AD_CD ";
			} else if (pOrderType.Equals("rate")) {
				switch (pSexCd) {
					case ViCommConst.UsableSexCd.MAN:
						sOrder = " ORDER BY AFFILIATE_POINT_RATE DESC, APPLY_START_DAY DESC, ASP_AD_CD ";
						break;
					case ViCommConst.UsableSexCd.WOMAN:
						sOrder = " ORDER BY WOMAN_AFFILIATE_POINT_RATE DESC, APPLY_START_DAY DESC, ASP_AD_CD ";
						break;
					default:
						break;
				}
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pAdPublicationTopic,pAffiliatePointFrom,pAffiliatePointTo,pAdCategory,pMonthlyFeeFrom,pMonthlyFeeTo,pCarrier,pExcludeSettled,pUserSeq,ref sWhere);

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("	* ");
			sSql.Append("FROM ");
			sSql.Append("	(");
			sSql.Append("	SELECT ");
			sSql.Append("		SETTLE_COMPANY_CD			, ");
			sSql.Append("		ASP_AD_CD					, ");
			sSql.Append("		AD_NM						, ");
			sSql.Append("		AD_DESCRIPTION				, ");
			sSql.Append("		AD_DESCRIPTION_SUB			, ");
			sSql.Append("		MONTHLY_FEE					, ");
			sSql.Append("		AFFILIATE_POINT				, ");
			sSql.Append("		APPLY_START_DAY				, ");
			sSql.Append("		APPLY_DOCOMO_FLAG			, ");
			sSql.Append("		APPLY_SOFTBANK_FLAG			, ");
			sSql.Append("		APPLY_AU_FLAG				, ");
			sSql.Append("		AD_CATEGORY					, ");
			sSql.Append("		AD_PUBLICATION_TOPIC		, ");
			sSql.Append("		CP_ID_NO					, ");
			sSql.Append("		CP_PASSWORD					, ");
			sSql.Append("		SETTLE_URL					, ");
			sSql.Append("		BACK_URL					, ");
			sSql.Append("		CONTINUE_SETTLE_URL			, ");
			sSql.Append("		REGEX_LOGIN_ID				, ");
			sSql.Append("		REGEX_ASP_AD_CD				, ");
			sSql.Append("		REGEX_POINT					, ");
			sSql.Append("		REGEX_SETTLE_SEQ			, ");
			sSql.Append("		HOST_NM						, ");
			sSql.Append("		AFFILIATE_POINT_RATE		, ");
			sSql.Append("		WOMAN_AFFILIATE_POINT_RATE	, ");
			sSql.Append("		ROW_NUMBER() OVER (" + sOrder + ") AS RNUM ");
			sSql.Append("	FROM ");
			sSql.Append("		VW_ASP_AD02 ");
			sSql.Append(sWhere);
			sSql.Append("	) ");
			sSql.Append("WHERE ");
			sSql.Append("	RNUM >= :FIRST_ROW AND ");
			sSql.Append("	RNUM <= :LAST_ROW ");
			sSql.Append(sOrder);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_ASP_AD02");
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,string pAdPublicationTopic,string pAffiliatePointFrom,string pAffiliatePointTo,string pAdCategory,string pMonthlyFeeFrom,string pMonthlyFeeTo,string pCarrier,bool pExcludeSettled,string pUserSeq,ref string pWhere) {
		ArrayList list = new ArrayList();

		iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		string sDay = DateTime.Today.ToString("yyyy/MM/dd");
		iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_START_DAY <= :APPLY_START_DAY",ref pWhere);
		list.Add(new OracleParameter("APPLY_START_DAY",sDay));

		iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_END_DAY >= :APPLY_END_DAY",ref pWhere);
		list.Add(new OracleParameter("APPLY_END_DAY",sDay));

		iBridCommLib.SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhere);
		list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF_STR));

		if (!string.IsNullOrEmpty(pAdPublicationTopic)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("AD_PUBLICATION_TOPIC = :AD_PUBLICATION_TOPIC",ref pWhere);
			list.Add(new OracleParameter("AD_PUBLICATION_TOPIC",pAdPublicationTopic));
		}

		if (!string.IsNullOrEmpty(pAffiliatePointFrom) && !string.IsNullOrEmpty(pAffiliatePointTo)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("AFFILIATE_POINT >= :AFFILIATE_POINT_FROM AND AFFILIATE_POINT <= :AFFILIATE_POINT_TO",ref pWhere);
			list.Add(new OracleParameter("AFFILIATE_POINT_FROM",pAffiliatePointFrom));
			list.Add(new OracleParameter("AFFILIATE_POINT_TO",pAffiliatePointTo));
		}

		if (!string.IsNullOrEmpty(pAdCategory)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("AD_CATEGORY = :AD_CATEGORY",ref pWhere);
			list.Add(new OracleParameter("AD_CATEGORY",pAdCategory));
		}

		if (!string.IsNullOrEmpty(pMonthlyFeeFrom) && !string.IsNullOrEmpty(pMonthlyFeeTo)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("MONTHLY_FEE >= :MONTHLY_FEE_FROM AND MONTHLY_FEE <= :MONTHLY_FEE_TO",ref pWhere);
			list.Add(new OracleParameter("MONTHLY_FEE_FROM",pMonthlyFeeFrom));
			list.Add(new OracleParameter("MONTHLY_FEE_TO",pMonthlyFeeTo));
		}

		switch (pCarrier) {
			case ViCommConst.DOCOMO:
				iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_DOCOMO_FLAG = 1",ref pWhere);
				break;

			case ViCommConst.SOFTBANK:
				iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_SOFTBANK_FLAG = 1",ref pWhere);
				break;

			case ViCommConst.KDDI:
				iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_AU_FLAG = 1",ref pWhere);
				break;

			case ViCommConst.ANDROID:
				iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_ANDROID_FLAG = 1", ref pWhere);
				break;

			case ViCommConst.IPHONE:
				iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_IPHONE_FLAG = 1", ref pWhere);
				break;

			default:
				//PCの時は何も表示させない

				iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_DOCOMO_FLAG = :APPLY_DOCOMO_FLAG",ref pWhere);
				list.Add(new OracleParameter("APPLY_DOCOMO_FLAG",-1));
				break;
		}

		if (!string.IsNullOrEmpty(pSexCd)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("APPLY_SEX_CD IN (:APPLY_SEX_CD1,:APPLY_SEX_CD2)",ref pWhere);
			list.Add(new OracleParameter("APPLY_SEX_CD1",pSexCd));
			list.Add(new OracleParameter("APPLY_SEX_CD2",ViCommConst.UsableSexCd.COMMON));
		}

		if (pExcludeSettled) {
			iBridCommLib.SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT SETTLE_SEQ FROM T_SETTLE_LOG WHERE T_SETTLE_LOG.SITE_CD = VW_ASP_AD02.SITE_CD AND T_SETTLE_LOG.SETTLE_COMPANY_CD = VW_ASP_AD02.SETTLE_COMPANY_CD AND T_SETTLE_LOG.SETTLE_TYPE = VW_ASP_AD02.SETTLE_TYPE AND T_SETTLE_LOG.ASP_AD_CD = VW_ASP_AD02.ASP_AD_CD AND T_SETTLE_LOG.SETTLE_COMPLITE_FLAG = 1 AND SETTLE_STATUS = :SETTLE_STATUS AND T_SETTLE_LOG.USER_SEQ = :USER_SEQ)",ref pWhere);
			list.Add(new OracleParameter("SETTLE_STATUS",ViCommConst.SETTLE_STAT_OK));
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetCount(string pSiteCd,string pSexCd,string pAdPublicationTopic,string pAffiliatePointFrom,string pAffiliatePointTo,string pAdCategory,string pMonthlyFeeFrom,string pMonthlyFeeTo,string pCarrier,bool pSettledOnly,string pUserSeq) {
		DataSet ds;
		int iCnt = 0;
		try {
			conn = DbConnect("AspAd.GetCountAll");
			ds = new DataSet();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pAdPublicationTopic,pAffiliatePointFrom,pAffiliatePointTo,pAdCategory,pMonthlyFeeFrom,pMonthlyFeeTo,pCarrier,false,pUserSeq,ref sWhere);

			StringBuilder sSql = new StringBuilder();
			if (pSettledOnly) {
				sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM VW_ASP_AD03 ");
				sSql.Append(sWhere);
				sSql.Append(" AND SETTLE_STATUS = :SETTLE_STATUS AND SETTLE_COMPLITE_FLAG = 1 AND USER_SEQ = :USER_SEQ ");
			} else {
				sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM T_ASP_AD ");
				sSql.Append(sWhere);
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				if (pSettledOnly) {
					cmd.Parameters.Add("SETTLE_STATUS",ViCommConst.SETTLE_STAT_OK);
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ASP_AD");
				}
				if (ds.Tables["T_ASP_AD"].Rows.Count != 0) {
					iCnt = int.Parse(ds.Tables["T_ASP_AD"].Rows[0]["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iCnt;
	}
}