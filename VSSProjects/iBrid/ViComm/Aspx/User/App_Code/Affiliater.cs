/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: アフリエター
--	Progaram ID		: Affiliater
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using iBridCommLib;

[System.Serializable]
public class Affiliater:DbSession {

	public string trackingUrl;
	public int trackingHttpFlag;
	public string addOnInfo;

	public Affiliater() {
		trackingUrl = "";
		trackingHttpFlag = 0;
		addOnInfo = "";
	}

	public bool IsAffiliater(string pSiteCd,NameValueCollection pQuery,out string pAffiliate,out string pAffiliateCompany,out string pAdCd) {
		bool bFind = false;

		pAffiliateCompany = string.Empty;
		pAffiliate = string.Empty;
		pAdCd = string.Empty;


		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT                                            ").AppendLine();
		oSqlBuilder.Append(" 	AFFILIATER_CD     	,                          ").AppendLine();
		oSqlBuilder.Append(" 	AS_INFO_PREFIX    	,                          ").AppendLine();
		oSqlBuilder.Append(" 	AS_INFO_SUB_PREFIX	,                          ").AppendLine();
		oSqlBuilder.Append(" 	AS_INFO_SUB_VALUE 	,                          ").AppendLine();
		oSqlBuilder.Append(" 	AS_INFO_PREFIX_COUNT,						   ").AppendLine();
		oSqlBuilder.Append(" 	AD_CD                                          ").AppendLine();
		oSqlBuilder.Append(" FROM                                              ").AppendLine();
		oSqlBuilder.Append(" 	VW_AFFILIATER01                                ").AppendLine();
		oSqlBuilder.Append(" WHERE                                             ").AppendLine();
		oSqlBuilder.Append(" 	SITE_CD = :SITE_CD                             ").AppendLine();

		try {
			conn = DbConnect("Affiliater.IsAffiliater");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);

				using (OracleDataReader oReader = cmd.ExecuteReader()) {
					while (oReader.Read()) {
						decimal dAsInfoPrefixCount = (oReader["AS_INFO_PREFIX_COUNT"] as decimal?) ?? decimal.Zero;
						string sTmpAffiliate = iBridUtil.GetStringValue(pQuery[oReader["AS_INFO_PREFIX"].ToString()]);
						string sTmpSubAffiliate = iBridUtil.GetStringValue(oReader["AS_INFO_SUB_PREFIX"]);
						string sActualSubValue = iBridUtil.GetStringValue(pQuery[sTmpSubAffiliate]);
						string sExpectedSubValue = iBridUtil.GetStringValue(oReader["AS_INFO_SUB_VALUE"]);

						bFind = false;
						if (dAsInfoPrefixCount == 1M) {
							bFind = !string.IsNullOrEmpty(sTmpAffiliate);
						} else {
							bFind = !string.IsNullOrEmpty(sTmpAffiliate) && sExpectedSubValue.Equals(sActualSubValue);
						}

						if (bFind) {
							pAffiliate = sTmpAffiliate;
							pAffiliateCompany = oReader["AFFILIATER_CD"].ToString();
							pAdCd = oReader["AD_CD"].ToString();
							break;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bFind;
	}


	public bool GetOne(string pAffiliaterCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("Affiliater.GetOne");

			using (cmd = CreateSelectCommand("SELECT TRACKING_URL,TRACKING_HTTP_FLAG,ADD_ON_INFO FROM T_AFFILIATER WHERE AFFILIATER_CD = :AFFILIATER_CD ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("AFFILIATER_CD",pAffiliaterCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AFFILIATER");
					if (ds.Tables["T_AFFILIATER"].Rows.Count != 0) {
						dr = ds.Tables["T_AFFILIATER"].Rows[0];
						trackingUrl = dr["TRACKING_URL"].ToString();
						trackingHttpFlag = int.Parse(dr["TRACKING_HTTP_FLAG"].ToString());
						addOnInfo = dr["ADD_ON_INFO"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

}
