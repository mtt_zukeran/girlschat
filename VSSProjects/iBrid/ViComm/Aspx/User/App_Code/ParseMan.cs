﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using iBridCommLib;
using MobileLib;
using MobileLib.Pictograph;
using ViComm;

[Serializable]
public partial class ParseMan:ParseViComm {

	public const int GET_RANDOM = 1;
	public const int GET_PICKUP = 2;
	public const int GET_RECOMMEND = 3;
	public const int GET_PICKUP_OBJ = 4;

	public SessionMan sessionMan;

	public ParseMan()
		: base() {
	}
	public ParseMan(string pPattern,bool pIsCrawler,string pSessionId,string pDomain,string pBasePath,RegexOptions pOption,string pCarrierCd,SessionMan pSessionMan)
		: base(pPattern,pIsCrawler,pSessionId,pDomain,pBasePath,pOption,pCarrierCd,pSessionMan) {
		sessionMan = pSessionMan;
		divNestCount = 0;
	}

	public override byte[] Perser(string pTag,string pArgument) {
		string sValue = "";
		int iValue;
		bool bParsed = true;

		try {
			if (pTag.StartsWith("$x")) {
				return ConvertEmoji(pTag);
			} else {
				Encoding encSjis = Encoding.GetEncoding(932); // shift-jis
				switch (pTag) {
					#region ---------- セッション開始後利用可能・システム関連 ----------
					//CROSMILE連携
					case "$SELF_CAN_USE_CROSMILE":
						sValue = sessionMan.IsRegistedCrosmile(sessionMan.userMan.crosmileUri) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$SELF_CROSMILE_APP_RUN":
						sValue = GetCrosmileAppRunIntent();
						break;
					//動画システム連携 
					case "$MOVIE_SITE_URL":
						sValue = GetMovieSiteUrl();
						break;
					case "$ADD_BONUS_POINT":
						AddBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,pArgument);
						break;
					case "$ADD_OMIKUJI_POINT":
						AddOmikujiPoint();
						break;
					case "$SELF_ADD_OMIKUJI_POINT":
						sValue = sessionMan.userMan.getOmikujiPoint.ToString();
						break;
					case "$SELF_OMIKUJI_POINT":
						sValue = sessionMan.userMan.omikujiPoint.ToString();
						break;
					case "$SELF_OMIKUJI_NO":
						sValue = sessionMan.userMan.omikujiNo;
						break;
					case "$ADMIN_NEW_REPORT_DATE":
						int.TryParse(pArgument,out iValue);
						sValue = GetNewAdminReport(sessionMan.site.siteCd,ViCommConst.MAN,iValue,"ADMIN_REPORT_DATE");
						break;
					case "$ADMIN_NEW_REPORT_TITLE":
						int.TryParse(pArgument,out iValue);
						sValue = parseContainer.Parse(GetNewAdminReport(sessionMan.site.siteCd,ViCommConst.MAN,iValue,"DOC_TITLE"));
						break;
					case "$ASP_AD_COUNT":
						sValue = GetAspAdCount(pTag,pArgument);
						break;
					case "$DIV_IS_LOGINED":
						SetNoneDisplay(!(sessionMan.logined.Equals(true)));
						break;
					case "$DIV_IS_NOT_LOGINED":
						SetNoneDisplay((sessionMan.logined.Equals(true)));
						break;
					case "$MAIL_DOMAIN":
						sValue = sessionMan.site.mailHost;
						break;
					case "$POINT_PACK":
						sValue = CreatePackSelect(pTag,pArgument);
						break;
					case "$CAMPAIN_CD":
						sValue = GetCampainCd(pTag,pArgument);
						break;
					case "$RECEIPT_CAMPAIN":
						sValue = GetIsExistsReceiptCampain(pTag,pArgument);
						break;
					case "$SELF_LOGIN_FLAG":
						sValue = sessionMan.logined ? "1" : "0";
						break;
					case "$SUB_HOST":
						sValue = sessionMan.site.subHostNm;
						break;
					case "$SUPPORT_MONITOR_VOICE":
						sValue = sessionMan.site.supportVoiceMonitorFlag.ToString();
						break;
					case "$URL_PARENT_SYS_LOGIN":
						sValue = GetParentSysLoginUrl();
						break;
					case "$URL_PREPAID_TOP":
						sValue = GetPrepaidTopUrl();
						break;
					case "$SORT_NEW":
						sValue = ViCommConst.SortType.NEW;
						break;
					case "$SORT_ONLINE_STAUTS":
						sValue = ViCommConst.SortType.ONLINE_STATUS;
						break;
					case "$SORT_LAST_LOGIN_DATE":
						sValue = ViCommConst.SortType.LAST_LOGIN_DATE;
						break;
					case "$DIV_ IS_REGISTED":
						SetNoneDisplay(!IsRegistSite(pArgument));
						break;
					case "$DIV_IS_NOT_REGISTED":
						SetNoneDisplay(IsRegistSite(pArgument));
						break;
					case "$DIV_IS_ANY_OF_REGISTED":
						SetNoneDisplay(!IsRegistSite(string.Empty));
						break;
					case "$DIV_IS_NOT_ANY_OF_REGISTED":
						SetNoneDisplay(IsRegistSite(string.Empty));
						break;
					case "$DIV_IS_ADDABLE_BONUS_POINT":
						SetNoneDisplay(!this.CheckBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,pArgument).Equals(ViCommConst.UserDefPointResult.NOT_ADD_POINT));
						break;
					case "$DIV_IS_ADDABLE_BONUS_POINT_ANY": {
							bool bNonDisplayFlag = true;
							string[] sPointTypes = pArgument.Split(',');
							foreach (string sPointType in sPointTypes) {
								if (ViCommConst.UserDefPointResult.NOT_ADD_POINT.Equals(this.CheckBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sPointType))) {
									bNonDisplayFlag = false;
									break;
								}
							}
							SetNoneDisplay(bNonDisplayFlag);
						}
						break;
					case "$DIV_IS_ADDABLE_BONUS_POINT_ALL": {
							bool bNonDisplayFlag = false;
							string[] sPointTypes = pArgument.Split(',');
							foreach (string sPointType in sPointTypes) {
								if (!ViCommConst.UserDefPointResult.NOT_ADD_POINT.Equals(this.CheckBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sPointType))) {
									bNonDisplayFlag = true;
									break;
								}
							}
							SetNoneDisplay(bNonDisplayFlag);
						}
						break;
					case "$DIV_IS_NOT_ADDABLE_BONUS_POINT":
						SetNoneDisplay(this.CheckBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,pArgument).Equals(ViCommConst.UserDefPointResult.NOT_ADD_POINT));
						break;
					case "$DIV_IS_NOT_ADDABLE_BONUS_POINT_ANY": {
							bool bNonDisplayFlag = true;
							string[] sPointTypes = pArgument.Split(',');
							foreach (string sPointType in sPointTypes) {
								if (!ViCommConst.UserDefPointResult.NOT_ADD_POINT.Equals(this.CheckBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sPointType))) {
									bNonDisplayFlag = false;
									break;
								}
							}
							SetNoneDisplay(bNonDisplayFlag);
						}
						break;
					case "$DIV_IS_NOT_ADDABLE_BONUS_POINT_ALL": {
							bool bNonDisplayFlag = false;
							string[] sPointTypes = pArgument.Split(',');
							foreach (string sPointType in sPointTypes) {
								if (ViCommConst.UserDefPointResult.NOT_ADD_POINT.Equals(this.CheckBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sPointType))) {
									bNonDisplayFlag = true;
									break;
								}
							}
							SetNoneDisplay(bNonDisplayFlag);
						}
						break;

					// icloudメールユーザフラグ
					case "$ICLOUD_MAIL_USER_FLAG": {
							string sIcloudom = "icloud.com";
							string sMecom = "me.com";
							string sUserMail = sessionMan.userMan.emailAddr;
							// メールドメインの取得
							string[] sMailAddr = sUserMail.Split('@');
							// メアドなし又は取得できない場合
							if(sMailAddr.Length < 2 || string.IsNullOrEmpty(sUserMail)){
								// 表示しない
								sValue = "0";
							} 
							else {
								// icloudユーザ
								if (sMailAddr[1] == sIcloudom) {
									sValue = "1";
								}
								// me.comユーザ
								else if (sMailAddr[1] == sMecom) {
									sValue = "1";
								}
								// その他ユーザ
								else
									sValue = "0";
							}
						}
						break;

					// ezwebメールユーザフラグ
					case "$EZWEB_MAIL_USER_FLAG": {
							string sEzweb = "ezweb.ne.jp";
							string sUserMail = sessionMan.userMan.emailAddr;
							// メールドメインの取得
							string[] sMailAddr = sUserMail.Split('@');
							// メアドなし又は取得できない場合
							if (sMailAddr.Length < 2 || string.IsNullOrEmpty(sUserMail)) {
								// 表示しない
								sValue = "0";
							} else {
								// ezwebユーザ
								if (sMailAddr[1] == sEzweb) {
									sValue = "1";
								}
								// その他ユーザ
								else
									sValue = "0";
							}
						}
						break;

					#endregion

					#region ---------- PARTS ------------------------------------------
					case "$CAST_PARTS":
						sValue = GetListCast(pTag,pArgument);
						break;
					case "$ASP_AD_PARTS":
						sValue = GetListAspAd(pTag,pArgument);
						break;
					case "$RANDOM_CHOICE_PARTS":
						sValue = GetListRandomChoice(pTag,pArgument);
						break;

					#endregion --------------------------------------------------------


					#region ---------- セッション開始後利用可能・商品関連 ----------
					case "$PRODUCT_MOVIE_PARTS":
						sValue = GetListProduct(ViCommConst.INQUIRY_PRODUCT_MOVIE,pTag,pArgument);
						break;
					case "$PRODUCT_PIC_PARTS":
						sValue = GetListProduct(ViCommConst.INQUIRY_PRODUCT_PIC,pTag,pArgument);
						break;
					case "$PRODUCT_AUCTION_PARTS":
						sValue = GetListProduct(ViCommConst.INQUIRY_PRODUCT_AUCTION,pTag,pArgument);
						break;
					case "$PRODUCT_AUCTION_BID_LOG_PARTS":
						sValue = GetListProductAuctionBidLog(pTag,pArgument);
						break;
					#endregion

					#region ---------- セッション開始後利用可能・ピックアップ関連 ----------
					case "$PICKUP_PARTS":
						sValue = GetListPickup(pTag,pArgument);
						break;
					case "$PICKUP_COUNT":
						sValue = GetCountPickup(pTag,pArgument);
						break;
					#endregion

					#region ---------- セッション開始後利用可能・無料ギャラリー関連 ----------
					case "$CAST_PARTNER_PIC_PARTS":
						sValue = GetListCastPic(pTag,pArgument);
						break;
					#endregion

					#region ---------- セッション開始後利用可能・キャスト関連 ----------



					case "$ACT_CATEGORY_SEQ":
						sValue = GetActCategorySeq(pTag,pArgument);
						break;
					case "$CAST_LIST_PIC":
					case "$CAST_LIST_PIC_NONAME":
					case "$CAST_PICKUP_PIC":
					case "$CAST_PICKUP_PIC_NONAME":
					case "$CAST_RECOMMEND_LIST":
					case "$CAST_RECOMMEND_LIST_NONAME":
					case "$CAST_PICKUP_OBJ":
					case "$CAST_PICKUP_OBJ_NONAME":
						sValue = GetCastDisplayTag(pTag,pArgument);
						break;
					case "$CAST_LOGINED_COUNT":
					case "$CAST_NEW_COUNT":
					case "$CAST_ONLINE_COUNT":
					case "$CAST_TALKING_COUNT":
					case "$CAST_TVTEL_COUNT":
					case "$CAST_WAITING_COUNT":
					case "$CAST_TODAY_LOGINED_COUNT":
						sValue = GetCastCount(pTag,pArgument);
						break;
					case "$CAST_RANDOM_PIC":
						sValue = GetRandomCastPicture(pTag,pArgument);
						break;
					case "$CAST_LOGIN_HISTORY_COUNT":
						sValue = GetCastLoginHistoryCount(pTag,pArgument);
						break;
					case "$CAST_CROSMILE_FLAG":
						sValue = GetPartnerCrosmileFlag(pTag);
						break;
					case "$CAST_VOICEAPP_FLAG":
						sValue = GetPartnerVoiceappFlag(pTag);
						break;
					#endregion

					#region ---------- 男性会員 ----------
					case "$SELF_AD_CD":
						if (sessionMan.logined) {
							sValue = sessionMan.userMan.adCd;
						} else {
							sValue = sessionMan.adCd;
						}
						break;
					case "$SELF_AD_GROUP_CD":
						if (sessionMan.logined) {
							sValue = sessionMan.userMan.adGroupCd;
						} else {
							if (sessionMan.adGroup != null) {
								sValue = sessionMan.adGroup.adGroupCd;
							}
						}
						break;
					case "$SELF_AGE":
						sValue = sessionMan.userMan.age;
						break;
					case "$SELF_BAL_POINT":
						sValue = sessionMan.userMan.balPoint.ToString();
						break;
					case "$SELF_BILL_AMT":
						sValue = sessionMan.userMan.billAmt.ToString();
						break;
					case "$SELF_BIRTHDAY":
						sValue = sessionMan.userMan.birthday;
						break;
					case "$SELF_CAST_MAIL_UNREAD_COUNT":
						sValue = GetUnreadMailCount(ViCommConst.MAIL_BOX_USER);
						break;
					case "$SELF_MARKING_UNCONFIRM_COUNT":  // 足あとリストの未確認件数
						sValue = GetUnconfirmMarkingCount();
						break;
					case "$SELF_CREDIT_DEAL_FLAG":
						sValue = sessionMan.userMan.existCreditDealFlag.ToString();
						break;
					case "$SELF_EMAIL_ADDR":
						sValue = sessionMan.userMan.emailAddr;
						break;
					case "$SELF_EMAIL_INVALID_FLAG":
						sValue = sessionMan.userMan.nonExistEmailAddrFlag.ToString();
						break;
					case "$SELF_EXIST_PAYMENT_FLAG":
						sValue = (sessionMan.userMan.IsExistPayment(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,pArgument) == true) ? "1" : "0";
						break;
					case "$SELF_FREE_DIAL_FLAG":
						sValue = sessionMan.userMan.useFreeDialFlag.ToString();
						break;
					case "$SELF_WHITE_PLAN_FLAG":
						sValue = sessionMan.userMan.useWhitePlanFlag.ToString();
						break;
					case "$SELF_CROSMILE_FLAG":
						sValue = sessionMan.userMan.useCrosmileFlag.ToString();
						break;
					case "$SELF_VOICEAPP_FLAG":
						sValue = sessionMan.userMan.useVoiceappFlag.ToString();
						break;
					case "$SETUP_FREE_DIAL_URL":
						sValue = GetSetupFreeDialUrl(pTag,pArgument);
						break;
					case "$SETUP_WHITE_PLAN_URL":
						sValue = GetSetupWhitePlanUrl(pTag,pArgument);
						break;
					case "$SETUP_CROSMILE_URL":
						sValue = GetSetupCrosmileUrl(pTag,pArgument);
						break;
					case "$SETUP_VOICEAPP_URL":
						sValue = GetSetupVoiceappUrl(pTag,pArgument);
						break;
					case "$SELF_FRIEND_INTRO_CD":
						sValue = sessionMan.userMan.friendIntroCd;
						break;
					case "$SELF_HANDLE_NM":
						sValue = parseContainer.Parse(sessionMan.userMan.handleNm);
						break;
					case "$SELF_IMG_PATH":
						sValue = "../" + sessionMan.userMan.smallPhotoImgPath;
						break;
					case "$SELF_LARGE_IMG_PATH":
						sValue = "../" + sessionMan.userMan.photoImgPath;
						break;
					case "$SELF_INFO_MAIL_UNREAD_COUNT":
						sValue = GetUnreadMailCount(ViCommConst.MAIL_BOX_INFO);
						break;
					case "$SELF_ITEM":
						sValue = parseContainer.Parse(GetUserManAttrValue(pArgument));
						sValue = ReplaceBr(sValue);
						break;
					case "$SELF_ITEM_CD":
						sValue = GetUserManAttrValueCd(pArgument);
						break;
					case "$SELF_IS_NEW_MEMBER":
						sValue = (this.IsNewMember() == true) ? "1" : "0";
						break;
					case "$SELF_INVITE_POINT":
						sValue = sessionMan.userMan.inviteServicePoint.ToString();
						break;
					case "$SELF_LIMIT_POINT":
						sValue = sessionMan.userMan.limitPoint.ToString();
						break;
					case "$SELF_LOGIN_ID":
						sValue = sessionMan.userMan.loginId;
						break;
					case "$SELF_LOGIN_PASSWORD":
						sValue = sessionMan.userMan.loginPassword;
						break;
					case "$SELF_BEFORE_SYSTEM_ID":
						sValue = sessionMan.userMan.beforeSystemId;
						break;
					case "$SELF_POINT_AFFILIATE_TOTAL_AMT":
						sValue = sessionMan.userMan.mchaTotalReceiptAmt.ToString();
						break;
					case "$SELF_MODIFY_MAIL_ADDR":
						sValue = string.Format("modm{0:D2}{1}{2}{3}@{4}",sessionMan.userMan.userSeq.Length,sessionMan.userMan.userSeq,sessionMan.userMan.siteCd,sessionMan.userMan.loginPassword,sessionMan.site.mailHost);
						break;
					case "$SELF_MOVIE_COUNT":
						sValue = GetMovieCount();
						break;
					case "$SELF_NEW_INFO_MAIL_DATE":
						sValue = GetNewMailInfo(ViCommConst.MAIL_BOX_INFO,ViCommConst.RX,"CREATE_DATE");
						break;
					case "$SELF_NEW_INFO_MAIL_TITLE":
						sValue = GetNewMailInfo(ViCommConst.MAIL_BOX_INFO,ViCommConst.RX,"MAIL_TITLE");
						break;
					case "$SELF_PERSONAL_AD":
						sValue = sessionMan.personalAdCd;
						break;
					case "$SELF_PIC_COUNT":
						sValue = GetPicCount();
						break;
					case "$SELF_PROFILE_MODIFY":
						sValue = GetManAttrHtml(pTag,pArgument);
						break;
					case "$SELF_PROFILE_PIC_OK_FLAG":
						sValue = (sessionMan.userMan.profilePicSeq.Equals("")) ? "0" : "1";
						break;
					case "$SELF_RANK_NM":
						sValue = sessionMan.userMan.rankNm;
						break;
					case "$SELF_USER_RANK":
						sValue = sessionMan.userMan.userRankCd;
						break;
					case "$SELF_REAL_BAL_POINT":
						sValue = sessionMan.userMan.realBalPoint.ToString();
						break;
					case "$SELF_RECEIPT_LIMIT_DAY":
						sValue = sessionMan.userMan.receiptLimitDay;
						break;
					case "$SELF_RECORDING_COUNT":
						sValue = GetRecMessageCount();
						break;
					case "$SELF_REGISTED_PROFILE_FLAG":
						sValue = sessionMan.userMan.profileOkFlag.ToString();
						break;
					case "$SELF_REGIST_SERVICE_POINT_FLAG":
						sValue = sessionMan.userMan.registServicePointFlag.ToString();
						break;
					case "$SELF_SERVICE_POINT":
						sValue = sessionMan.userMan.servicePoint.ToString();
						break;
					case "$SELF_STOCK_MAIL_MOVIE_SEND_ADDR":
						sValue = GetMovieUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_MAIL);
						break;
					case "$SELF_STOCK_MAIL_PIC_SEND_ADDR":
						sValue = GetPictureUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_MAIL);
						break;
					case "$SELF_STOCK_PROFILE_PIC_SEND_ADDR":
						sValue = GetPictureUploadAddr(pTag,pArgument,ViCommConst.ATTACHED_PROFILE);
						break;
					case "$SELF_TEL":
						sValue = sessionMan.userMan.tel;
						break;
					case "$SELF_TEL_ATTESTED_FLAG":
						sValue = sessionMan.userMan.telAttestedFlag.ToString();
						break;
					case "$SELF_TERMINAL_UNIQUE_ID_FLAG":
						sValue = IsEqual(pTag,!sessionMan.userMan.terminalUniqueId.Equals(""));
						break;
					case "$SELF_GUID_FLAG":
						sValue = IsEqual(pTag,!sessionMan.userMan.iModeId.Equals(""));
						break;
					case "$SELF_TOTAL_RECEIPT_AMT":
						sValue = sessionMan.userMan.totalReceiptAmt.ToString();
						break;
					case "$SELF_TOTAL_RECEIPT_COUNT":
						sValue = sessionMan.userMan.totalReceiptCount.ToString();
						break;
					case "$SELF_MONTHLY_RECEIPT_AMT":
						sValue = sessionMan.userMan.monthlyReceiptAmt.ToString();
						break;
					case "$SELF_WAITING_FLAG":
						sValue = IsManWaiting();
						break;
					case "$SELF_WAITING_START_BUTTON":
						sValue = "<input name=\"cmdStartWait\" type=\"submit\" value=\"待機開始\"/>";
						break;
					case "$SELF_ZERO_SETTLE_OK_FLAG":
						sValue = sessionMan.userMan.zeroSettleOkFlag.ToString();
						break;
					case "$SELF_NON_USED_EASY_LOGIN_FLAG":
						sValue = sessionMan.userMan.nonUsedEasyLoginFlag.ToString();
						break;
					case "$SELF_ONLINE_STATUS":
						sValue = sessionMan.userMan.mobileOnlineStatusNm;
						break;
					case "$SELF_WAITING_TYPE":
						sValue = sessionMan.userMan.manPfWaitingTypeNm;
						break;
					case "$SELF_STATUS_WAITING_FLAG":
						sValue = (sessionMan.userMan.characterOnlineStatus == ViCommConst.USER_WAITING) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$SELF_TV_TALK_OK_FLAG":
						sValue = (sessionMan.userMan.characterOnlineStatus == ViCommConst.USER_WAITING && sessionMan.userMan.manPfWaitingType == ViCommConst.WAIT_TYPE_VIDEO) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$SELF_VOICE_TALK_OK_FLAG":
						sValue = (sessionMan.userMan.characterOnlineStatus == ViCommConst.USER_WAITING && sessionMan.userMan.manPfWaitingType == ViCommConst.WAIT_TYPE_VOICE) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$SELF_VOICE_TV_TALK_OK_FLAG":
						sValue = (sessionMan.userMan.characterOnlineStatus == ViCommConst.USER_WAITING && sessionMan.userMan.manPfWaitingType == ViCommConst.WAIT_TYPE_BOTH) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$SELF_PARTNER_MAIL_RX_TYPE":
						sValue = sessionMan.userMan.castMailRxType;
						break;
					case "$SELF_INFO_MAIL_RX_TYPE":
						sValue = sessionMan.userMan.InfoMailRxType;
						break;
					case "$SELF_TALK_COUNT":
						sValue = sessionMan.userMan.totalTalkCount.ToString();
						break;
					case "$SELF_RX_MAIL_COUNT":
						sValue = sessionMan.userMan.totalRxMailCount.ToString();
						break;
					case "$SELF_TX_MAIL_COUNT":
						sValue = sessionMan.userMan.totalTxMailCount.ToString();
						break;
					case "$SELF_USER_DEF_FLAG":
						sValue = IsValidMask(sessionMan.userMan.userDefineMask,pArgument);
						break;
					case "$HREF_SMART_PHONE_CERTIFY":
						sValue = GetHrefSmartPhoneCertify(pTag,pArgument);
						break;
					case "$SELF_REGIST_INCOMPLETE_FLAG":
						sValue = ViCommConst.FLAG_OFF == sessionMan.userMan.registIncompleteFlag ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR;
						break;
					case "$SELF_LAST_POINT_USED_DATE":
						sValue = sessionMan.userMan.lastPointUsedDate;
						break;
					case "$SELF_LAST_RECEIPT_DATE":
						sValue = sessionMan.userMan.lastReceiptDate;
						break;
					case "$SELF_LAST_RECEIPT_PAST_DAYS": {
							DateTime dtLastPeceipt;
							if (DateTime.TryParse(sessionMan.userMan.lastReceiptDate,out dtLastPeceipt)) {
								sValue = ((TimeSpan)(DateTime.Today - dtLastPeceipt.Date)).Days.ToString();
							} else {
								sValue = string.Empty;
							}
						}
						break;
					case "$SELF_REGIST_DATE":
						sValue = sessionMan.userMan.registDay;
						break;
					case "$ACTIVATED_FLAG":
						using (ManageCompany oInstance = new ManageCompany()) {
							if (oInstance.IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE)) {
								sValue = IsActivated(pTag,pArgument);
							}
						}
						break;
					case "$DIV_IS_BLACK": {
							bool bBlacked = false;
							using (UserManCharacterBlack oBlack = new UserManCharacterBlack()) {
								string sBlackType = parseContainer.Parse(pArgument);
								bBlacked = oBlack.IsBlack(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,pArgument);
							}
							SetNoneDisplay(!bBlacked);
						}
						break;
					case "$DIV_IS_NOT_BLACK": {
							bool bBlacked = false;
							using (UserManCharacterBlack oBlack = new UserManCharacterBlack()) {
								string sBlackType = parseContainer.Parse(pArgument);
								bBlacked = oBlack.IsBlack(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,pArgument);
							}
							SetNoneDisplay(bBlacked);
						}
						break;
					case "$POINT_REDUCTION_START_DATE": {
							using (PointReductionSchdule oPointReductionSchedule = new PointReductionSchdule()) {
								DataSet oDataSet = oPointReductionSchedule.GetOne(this.sessionMan.site.siteCd);
								if (oDataSet.Tables[0].Rows.Count > 0) {
									sValue = oDataSet.Tables[0].Rows[0]["APPLICATION_START_DATE"].ToString();
								}
							}
						}
						break;
					case "$POINT_REDUCTION_END_DATE": {
							using (PointReductionSchdule oPointReductionSchedule = new PointReductionSchdule()) {
								DataSet oDataSet = oPointReductionSchedule.GetOne(this.sessionMan.site.siteCd);
								if (oDataSet.Tables[0].Rows.Count > 0) {
									sValue = oDataSet.Tables[0].Rows[0]["APPLICATION_END_DATE"].ToString();
								}
							}
						}
						break;
					case "$SELF_REDUCTION_RECEIPT_SUM_AMT": {
							if (this.sessionMan.userMan.reductionReceiptSumAmt != null) {
								sValue = this.sessionMan.userMan.reductionReceiptSumAmt.ToString();
							}
						}
						break;
					case "$SELF_REDUCTION_POINT": {
							if (this.sessionMan.userMan.reductionReceiptSumAmt != null) {
								sValue = this.GetCurrentReductionPoint();
							}
						}
						break;
					case "$SELF_REDUCTION_RECEIPT_SUM_AMT_NEXT": {
							if (this.sessionMan.userMan.reductionReceiptSumAmt != null) {
								if (this.sessionMan.userMan.reductionPointType.Equals(ViCommConst.PointReductionType.InputValue)) {
									sValue = GetNextReductionReceiptSumAmt();
								}
							}
						}
						break;
					case "$SELF_REDUCTION_POINT_NEXT": {
							if (this.sessionMan.userMan.reductionReceiptSumAmt != null) {
								if (this.sessionMan.userMan.reductionPointType.Equals(ViCommConst.PointReductionType.InputValue)) {
									sValue = GetNextReductionPoint();
								}
							}
						}
						break;
					case "$SELF_REFRESH_SESSION": {
							if (sessionMan.logined) {
								sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionObjs);
							}
						}
						break;
					case "$SELF_NO_REPORT_AFFILIATE_FLAG":
						sValue = sessionMan.userMan.noReportAffiliateFlag.ToString();
						break;
					case "$SELF_EXIST_DEVICE_TOKEN_FLAG":
						sValue = iBridUtil.GetStringValue(sessionMan.userMan.existDeviceTokenFlag);
						break;
					case "$MILEAGE_SETTLE_ID":
						sValue = GetMileageSettleId(pTag,pArgument);
						break;
					// 退会処理完了
					case "$DIV_IS_WITHDRAWAL_STATUS_COMPLETE":
						SetNoneDisplay(!IsWithdrawal(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE));
						break;
					// 退会申請中
					case "$DIV_IS_WITHDRAWAL_STATUS_ACCEPT":
						SetNoneDisplay(!IsWithdrawal(ViCommConst.WITHDRAWAL_STATUS_ACCEPT));
						break;
					// 退会申請なし
					case "$DIV_IS_WITHDRAWAL_STATUS_NONE":
						SetNoneDisplay(!IsWithdrawal(string.Empty));
						break;
					// つぶやき無制限対象
					case "$DIV_IS_TWEET_UNLIMITED":
						using (UserMan oUserMan = new UserMan()) {
							SetNoneDisplay(!oUserMan.IsTargetTweetUnlimited(sessionMan.site.siteCd,sessionMan.userMan.userSeq));
						}
						break;
					// つぶやき無制限対象外
					case "$DIV_IS_NOT_TWEET_UNLIMITED":
						using (UserMan oUserMan = new UserMan()) {
							SetNoneDisplay(oUserMan.IsTargetTweetUnlimited(sessionMan.site.siteCd,sessionMan.userMan.userSeq));
						}
						break;

					#endregion

					#region ---------- プログラム依存・登録関連 ----------
					case "$AFFILIATE_COOKEI_IMG":
						sValue = GetAffiliateCookieImg();
						break;
					// 電話番号認証なし版 広告成果報告タグ出力
					case "$NONE_TEL_AUTH_AFFILICATE_TAG":
						sValue = GetAffiliateCookieImgNoneTelAuth(pArgument);
						break;
					case "$REGIST_CAST_HANDLE_NM":
						sValue = parseContainer.Parse(sessionMan.userMan.registCastHandleNm);
						break;
					case "$REGIST_CAST_LOGIN_ID":
						sValue = sessionMan.userMan.registCastLoginId;
						break;
					case "$REGIST_MAIL_ADDR":
						sValue = GetRegistMailAddr();
						break;
					case "$REGIST_MAIL_TEMP_ID":
						sValue = GetRegistMailTempId();
						break;
					case "$TEMP_REGIST_ID":
						sValue = sessionMan.userMan.tempRegistId;
						break;
					case "$COMPLETE_TRACKING_REPORT":
						CompliteTrackingReport();
						break;
					#endregion

					#region ---------- プログラム依存・メール書込み ----------
					case "$DIV_IS_ATTACHED_PIC":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["mailtype"]).Equals(ViCommConst.ATTACH_PIC_INDEX.ToString()));
						break;
					case "$DIV_IS_ATTACHED_MOVIE":
						SetNoneDisplay(!iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["mailtype"]).Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString()));
						break;
					case "$SELF_MAIL_ATTACHED_MOVIE_TITLE":
						sValue = GetAttachedValue("MOVIE_TITLE");
						break;
					case "$SELF_MAIL_ATTACHED_PIC_IMG_PATH":
						sValue = GetAttachedValue("PHOTO_IMG_PATH");
						break;
					case "$SELF_MAIL_ATTACHED_PIC_UPLOAD_DATE":
						sValue = GetAttachedValue("UPLOAD_DATE");
						break;
					case "$SELF_MAIL_ATTACHED_MOVIE_UPLOAD_DATE":
						sValue = GetAttachedValue("UPLOAD_DATE");
						break;
					case "$SELF_MAIL_WRITE_DOC":
						sValue = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,parseContainer.Parse(sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].mailDoc)));
						break;
					case "$SELF_MAIL_WRITE_TITLE":
						sValue = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,parseContainer.Parse(sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].mailTitle)));
						break;
					case "$SELF_MAIL_WRITE_TO_CAST_HANDLE_NM":
						if (sessionMan.currentAspx.Equals("TxMail.aspx") || sessionMan.currentAspx.Equals("ReturnMail.aspx")) {
							sValue = parseContainer.Parse(sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.userMan.mailDataSeq)].rxUserNm);
						} else {
							sValue = parseContainer.Parse(sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].rxUserNm);
						}
						break;
					case "$SELF_MAIL_ATTACHED_MAILER_TEMP_ID":
						if (sessionMan.userMan.mailData.ContainsKey(iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"]))) {
							sValue = sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].objTempId;
						}
						if (string.IsNullOrEmpty(sValue)) {
							string sRequestTxMailSeq;
							if (GetValue(pTag,ViCommConst.DATASET_MAIL,"REQUEST_TX_MAIL_SEQ",out sRequestTxMailSeq)) {
								using (MailLog oMailLog = new MailLog()) {
									sValue = oMailLog.GetObjTempId(sRequestTxMailSeq);
								}
							}
						}
						break;
					case "$SELF_MAIL_ATTACHED_OBJ_TYPE":
						sValue = parseContainer.Parse(sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].mailAttachType);
						break;
					case "$DIV_IS_RETURN_MAIL":
						SetNoneDisplay(!sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].isReturnMail);
						break;
					case "$DIV_IS_NOT_RETURN_MAIL":
						SetNoneDisplay(sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].isReturnMail);
						break;
					case "$SELF_MAIL_PRESENT_FLAG":
						sValue = parseContainer.Parse(sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].presentMailFlag.ToString());
						break;
					case "$SELF_MAIL_QUICK_RESPONSE_FLAG":
						if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"]))) {
							sValue = sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].quickResMailFlag.ToString();

						} else if (sessionMan.currentAspx.Equals("TxMail.aspx") || sessionMan.currentAspx.Equals("TxMail01.aspx") || sessionMan.currentAspx.Equals("ReturnMail.aspx") || sessionMan.currentAspx.Equals("ReturnMail.aspx") ||
								((sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) && sessionMan.currentAspx.Equals("Profile.aspx")))
						) {
							sValue = sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.userMan.mailDataSeq)].quickResMailFlag.ToString();
						}
						break;

					#endregion

					#region ---------- プログラム依存・決済関連 ----------
					case "$SETTLE_CVSDL_NAME":
						sValue = sessionMan.userMan.csvdlname;
						break;
					case "$SETTLE_CVSDL_RECEIPT_NO":
						sValue = sessionMan.userMan.csvdlreceiptno;
						break;
					case "$SETTLE_CVSDL_RECEIPT_LIMIT_DATE":
						sValue = sessionMan.userMan.csvdlreceiptLimitDate.ToString("yyyy/MM/dd HH:mm:ss");
						break;
					case "$SETTLE_CVSDL_RECEIPT_URL":
						sValue = sessionMan.userMan.csvdlreceiptUrl;
						break;
					case "$SETTLE_ID":
						sValue = sessionMan.userMan.settleId;
						break;
					case "$SETTLE_REQUEST_AMT":
						sValue = sessionMan.userMan.settleRequestAmt.ToString();
						break;
					case "$SETTLE_REQUEST_POINT":
						sValue = sessionMan.userMan.settleRequestPoint.ToString();
						break;
					case "$SETTLE_SERVICE_POINT":
						sValue = sessionMan.userMan.settleServicePoint.ToString();
						break;
					#endregion

					#region ---------- プログラム依存・IVP関連 ----------
					case "$CALL_TEL":
						if (sessionMan.errorMessage.Equals("")) {
							sValue = sessionMan.ivpRequest.dialNo;
						}
						break;
					case "$CALL_USE_CROSMILE":
						sValue = sessionMan.ivpRequest.useCrosmile ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
						break;
					case "$DIV_NOT_OPEN_SHARE_LIVE":
						SetNoneDisplay((sessionMan.GetShareLiveTalkKeyToAcceptNo(pArgument).Equals("") == false));
						break;
					case "$DIV_OPEN_SHARE_LIVE":
						SetNoneDisplay((sessionMan.GetShareLiveTalkKeyToAcceptNo(pArgument).Equals("")));
						break;
					case "$HREF_SHARE_LIVE":
						sValue = ParseShareLive(pArgument);
						break;
					case "$HREF_VCS_MONITOR":
						sValue = GetVcsNoPersonMenuUrl(ViCommConst.VCS_MENU_WIRETAPPING,ViCommConst.CHARGE_WIRETAPPING,pArgument);
						break;
					case "$HREF_VCS_PLAY_PV_MSG":
						sValue = GetVcsNoPersonMenuUrl(ViCommConst.VCS_MENU_PLAY_PV_MSG,ViCommConst.CHARGE_PLAY_PV_MSG,pArgument);
						break;
					case "$HREF_VIDEO_TEL":
						sValue = GetHrefVideoTel(pArgument);
						break;
					case "$HREF_VOICE_TEL":
						sValue = GetHrefVoiceTel(pArgument);
						break;
					#endregion

					#region ---------- プログラム依存・プロフ登録でポイントGET ----------
					case "$PROFILE_PIC_POINT_TYPE":
						sValue = sessionMan.userMan.profilePicPointType.ToString();
						break;
					case "$PROFILE_POINT_RESULT":
						sValue = sessionMan.userMan.profilePointResult;
						break;
					
					#endregion

					#region ---------- 楽天決済関連 ----------

					case "$RAKUTEN_AUTH_URL":
						// 楽天あんしん決済のURL
						string sBackToUrl = string.Format("http://{0}/User/Start.aspx",HttpContext.Current.Request.Url.Host.ToLower());

						sValue = new ViComm.Rakuten.UserAuth().GenerateUserAuthRequestUrl(false,sBackToUrl);
						break;

					case "$DIV_ENABLED_AUTH_RAKUTEN":
						SetNoneDisplay(ViComm.Rakuten.RakutenUtil.IsTMode && !sessionMan.rakutenDebug);
						break;

					case "$DIV_ENABLED_RAKUTEN": {
							bool bEnalbedRakutenOpenId = false;
							if (sessionMan.userMan != null) {
								bEnalbedRakutenOpenId = (sessionMan.userMan.rakutenOpenIdEnabled);
							}
							SetNoneDisplay(!bEnalbedRakutenOpenId);
							break;
						}
					case "$DIV_NOT_ENABLED_RAKUTEN": {
							bool bEnalbedRakutenOpenId = false;
							if (sessionMan.userMan != null) {
								bEnalbedRakutenOpenId = (sessionMan.userMan.rakutenOpenIdEnabled);
							}
							SetNoneDisplay(bEnalbedRakutenOpenId);
							break;
						}



					#endregion

					default:
						#region ---------- データセット処理 ----------
						switch (this.currentTableIdx) {
							case ViCommConst.DATASET_CAST:
								sValue = this.ParseCast(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_ADMIN_REPORT:
								sValue = this.ParseAdminReport(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_MAIL:
								sValue = this.ParseMail(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PARTNER_MOVIE:
								sValue = this.ParseCastMovie(pTag,pArgument,out bParsed);
								if (bParsed == false) {
									//ゲーム用動画の場合のみ、別口で変数をパースする。
									sValue = ParseGamePartnerMovie(pTag,pArgument,out bParsed);
								}
								break;
							case ViCommConst.DATASET_PARTNER_PIC:
								sValue = this.ParseCastPic(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PARTNER_DIARY:
								sValue = this.ParseCastDiary(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PARTNER_BBS_DOC:
								sValue = this.ParseCastBbsDoc(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PARTNER_VOICE:
								sValue = this.ParseCastVoice(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_SELF_MOVIE:
								sValue = this.ParseSelfMovie(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_SELF_PIC:
								sValue = this.ParseSelfPic(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_SELF_BBS_DOC:
								sValue = this.ParseSelfBbsDoc(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_TALK_HISTORY:
								sValue = this.ParseTalkHistory(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_REPUTATION:
								sValue = this.ParseReputation(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_ATTR_TYPE:
								sValue = this.ParseAttrType(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_TALK_MOVIE:
								sValue = this.ParseTalkMovie(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PARTNER_BBS_OBJ:
								sValue = this.ParseCastBbsObj(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_ASP_AD:
								sValue = this.ParseAspAd(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_GET_ASP_AD:
								sValue = this.ParseGetAspAd(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PRODUCT_MOVIE:
								sValue = this.ParseProduct(pTag,pArgument,out bParsed);
								sValue = (!bParsed) ? this.ParseProductMovie(pTag,pArgument,out bParsed) : sValue;
								break;
							case ViCommConst.DATASET_PRODUCT_PIC:
								sValue = this.ParseProduct(pTag,pArgument,out bParsed);
								sValue = (!bParsed) ? this.ParseProductPic(pTag,pArgument,out bParsed) : sValue;
								break;
							case ViCommConst.DATASET_RANKING:
								sValue = this.ParseManRanking(pTag,pArgument,out bParsed);
								break;
							case ViCommConst.DATASET_PRODUCT_AUCTION:
								sValue = this.ParseProduct(pTag,pArgument,out bParsed);
								sValue = (!bParsed) ? this.ParseProductAuction(pTag,pArgument,out bParsed) : sValue;
								sValue = (!bParsed) ? this.ParseProductMovie(pTag,pArgument,out bParsed) : sValue;
								break;
							case ViCommConst.DATASET_PRODUCT_AUCTION_BID_LOG:
								sValue = this.ParseProduct(pTag,pArgument,out bParsed);
								sValue = (!bParsed) ? this.ParseProductAuctionBidLog(pTag,pArgument,out bParsed) : sValue;
								break;
							case ViCommConst.DATASET_PRODUCT_THEME:
								sValue = this.ParseProduct(pTag,pArgument,out bParsed);
								//sValue = (!bParsed) ? this.ParseProductTheme(pTag,pArgument,out bParsed) : sValue;
								break;
							default:
								bParsed = false;
								break;
						}
						break;
						#endregion
				}

				if (bParsed == false) {
					sValue = PerserEx(pTag,pArgument,out bParsed);
				}
				if (bParsed == false) {
					sValue = new PwParseMan(this).Parse(pTag,pArgument,out bParsed);
				}

				if (bParsed == false) {
					if (pTag.StartsWith("$DIV_")) {
						ParseDiv(pTag,pArgument);
					} else if (pTag.Equals("$SELF_TO_CAST_USE_POINT")) {
						sValue = GetUsePoint(pTag,pArgument).ToString();
					} else {
						return base.Perser(pTag,pArgument);
					}
				}

				byte[] byteArray = encSjis.GetBytes(sValue ?? string.Empty);
				return byteArray;
			}
		} catch (Exception ex) {
			ViCommInterface.WriteIFError(ex,"ParseMan",pTag);
			this.parseErrorList.Add(pTag + " " + ex.Message);
			throw;
		}
	}

	protected override string ParseStartForm(string pTag) {
		string sValue = string.Format("{0}",pTag);
		Regex regex;

		switch (postAction) {
			case ViCommConst.POST_ACT_MAIL:
				postAction = 0;
				regex = new Regex(@"(action="".*?"")",RegexOptions.Compiled);
				sValue = regex.Replace(pTag,new MatchEvaluator(RegexMatchPostMailAction));
				break;
			default:
				sValue = base.ParseStartForm(pTag);
				break;
		}
		return sValue;
	}

	private string RegexMatchPostMailAction(Match match) {
		return string.Format("action=\"mailto:modm{0:D2}{1}{2}{3}@{4}\"",
			sessionMan.userMan.userSeq.Length,
			sessionMan.userMan.userSeq,
			sessionMan.userMan.siteCd,
			sessionMan.userMan.loginPassword,
			sessionMan.site.mailHost);
	}

	public override string ParseSiteTop() {
		return ParseSiteTop(true);
	}
	public string ParseSiteTop(bool pWithImpersonate) {
		if (pWithImpersonate) {
			if (sessionMan.IsImpersonated) {
				return ((ParseWoman)sessionMan.originalWomanObj.parseContainer.parseUser).ParseSiteTop(false);
			}
		}
		string sBaseUrl = sessionMan.root + sessionMan.sysType;

		string sAppendParam = string.Empty;
		if (sessionObjs.carrier.Equals(ViCommConst.KDDI)) {
			sAppendParam = "?" + this.GetAuRefresh();
		}

		if (sessionMan.logined) {
			return sessionMan.GetNavigateUrl(sBaseUrl,sessionMan.sessionId,sessionMan.site.userTopId + sAppendParam);
		} else {
			if (sessionMan.groupType == 0) {
				return sessionMan.GetNavigateUrl(sBaseUrl,sessionMan.sessionId,sessionMan.site.nonUserTopId + sAppendParam);
			} else {
				return sessionMan.GetNavigateUrl(sBaseUrl,sessionMan.sessionId,"PrepaidUserTop.aspx" + sAppendParam);
			}
		}
	}


	private string GetUserManAttrValue(string itemNo) {
		string ret = string.Empty;
		foreach (UserManAttr attr in this.sessionMan.userMan.attrList) {
			if (itemNo == attr.itemNo) {
				if (attr.inputType == ViCommConst.INPUT_TYPE_TEXT) {
					ret = attr.attrImputValue;
				} else {
					ret = attr.attrNm;
				}
				break;
			}
		}
		return ret;
	}
	private string GetUserManAttrValueCd(string itemNo) {
		string ret = string.Empty;
		foreach (UserManAttr attr in this.sessionMan.userMan.attrList) {
			if (itemNo == attr.itemNo) {
				ret = attr.itemCd;
				break;
			}
		}
		return ret;
	}

	private string ParseShareLive(string pText) {
		string[] sValue = pText.Split(',');
		if (sValue.Length != 4) {
			return "";
		}

		string sLabel = sValue[0];
		string sAccessKey;
		if (sValue[1].Equals("") == false) {
			sAccessKey = string.Format(" accesskey=\"{0}\" ",sValue[1]);
		} else {
			sAccessKey = "";
		}
		string sShKey = sValue[2];
		string sLoginId = sValue[3];
		string sCastUserSeq = GetUserSeqByLoginId(sLoginId);

		return string.Format("<a href=\"RequestIVP.aspx?request={0}&castseq={1}&chgtype={2}&shkey={3}\" {4}>{5}</a>",
						ViCommConst.REQUEST_VIEW_SHARE_LIVE_TALK,
						sCastUserSeq,
						ViCommConst.CHARGE_VIEW_LIVE,
						sShKey,
						sAccessKey,
						sLabel
						);
	}

	private string GetRecMessageCount() {
		int iRecCount = 0;
		using (Recording oRecording = new Recording()) {
			iRecCount = oRecording.GetRecordingCount(sessionMan.site.siteCd,"","",sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.REC_TYPE_MESSAGE);
		}
		return iRecCount.ToString();
	}

	private string GetUserSeqByLoginId(string pLoginId) {
		string sUserSeq = "";
		using (Cast oCast = new Cast(this.sessionObjs)) {
			sUserSeq = oCast.GetUserSeqByLoginId(pLoginId);
		}
		return sUserSeq;
	}

	private string GetPicCount() {
		int iPageCnt = 0;
		decimal iRecCnt = 0;
		using (UserManPic oUserManPic = new UserManPic()) {
			iPageCnt = oUserManPic.GetPageCountByLoginId(sessionMan.site.siteCd,sessionMan.userMan.loginId,string.Empty,ViCommConst.FLAG_OFF_STR,1,out iRecCnt);
		}
		return iRecCnt.ToString();
	}

	private string GetMovieCount() {
		int iPageCnt = 0;
		decimal iRecCnt = 0;
		using (UserManMovie oUserManMovie = new UserManMovie()) {
			iPageCnt = oUserManMovie.GetPageCountByLoginId(sessionMan.site.siteCd,sessionMan.userMan.loginId,string.Empty,string.Empty,ViCommConst.FLAG_OFF_STR,1,out iRecCnt);
		}
		return iRecCnt.ToString();
	}

	private string GetHtmlDocFull(string pTag) {
		string sValue = "";
		string sDoc1 = "",sDoc2 = "",sDoc3 = "",sDoc4 = "",sDoc5 = "";
		if (GetValue(pTag,"HTML_DOC1",out sDoc1) && GetValue(pTag,"HTML_DOC2",out sDoc2) && GetValue(pTag,"HTML_DOC3",out sDoc3) && GetValue(pTag,"HTML_DOC4",out sDoc4) && GetValue(pTag,"HTML_DOC5",out sDoc5)) {
			sValue = parseContainer.Parse(sDoc1 + sDoc2 + sDoc3 + sDoc4);
		}
		return sValue;
	}

	private string IsManWaiting() {
		string sValue;
		using (Recording oRecording = new Recording()) {
			if (oRecording.WaitingNow(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,"","","")) {
				sValue = ViCommConst.FLAG_ON.ToString();
			} else {
				sValue = ViCommConst.FLAG_OFF.ToString();
			}
		}
		return sValue;
	}

	private string GetUnreadMailCount(string pMailType) {
		int iRecCount = 0;
		if (sessionMan.logined) {
			using (MailBox oMailLog = new MailBox()) {
				iRecCount = (int)(oMailLog.GetMailCount(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN,pMailType,true));
			}
		}
		return iRecCount.ToString();
	}

	/// <summary>
	/// 足あとリストの未確認件数を取得
	/// </summary>
	/// <returns></returns>
	private string GetUnconfirmMarkingCount() {
		// 取得済みの場合
		if (!string.IsNullOrEmpty(sessionMan.userMan.characterEx.unconfirmMarkingCount)) {
			return sessionMan.userMan.characterEx.unconfirmMarkingCount;
		}

		int iRecCount = 0;
		if (sessionMan.logined) {
			using (Marking oMarking = new Marking()) {
				iRecCount = (int)(oMarking.GetUnconfirmMarkingCount(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN));
			}
		}

		// 取得した足あとの未確認件数をセッションに保持しておく
		sessionMan.userMan.characterEx.unconfirmMarkingCount = iRecCount.ToString();

		return iRecCount.ToString();
	}

	/// <summary>
	/// 広告の成果報告用Imageタグ出力(電話番号認証なし)
	/// </summary>
	/// <returns></returns>
	private string GetAffiliateCookieImgNoneTelAuth(string authAdCd) {
		string sTag = string.Empty;

		// スマホ以外の場合
		if (!sessionMan.userMan.registCarrierCd.Equals(ViCommConst.ANDROID)
			&& !sessionMan.userMan.registCarrierCd.Equals(ViCommConst.IPHONE)
		) {
			// 成果報告用のタグを出力しない
			return sTag;
		}

		bool bFlag = false;

		// 許可する広告コードと登録したときの広告コードが一致する
		if (!string.IsNullOrEmpty(authAdCd)
			&& sessionMan.adCd.Equals(authAdCd)
		) {
			bFlag = true;
		}

		if ((bFlag || sessionMan.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY))
			&& sessionMan.userMan.noReportAffiliateFlag == ViCommConst.FLAG_ON
			&& !sessionMan.adCd.Equals("")
		) {
			this.SetAffiliateCookieImg(ref sTag);
		}

		return sTag;
	}

	/// <summary>
	/// 広告の成果報告用Imageタグ出力
	/// </summary>
	/// <returns></returns>
	private string GetAffiliateCookieImg() {
		string sResult = "";
		if (sessionMan.userMan.registCarrierCd.Equals(ViCommConst.ANDROID) || sessionMan.userMan.registCarrierCd.Equals(ViCommConst.IPHONE)) {
			if ((sessionMan.userMan.telAttestedFlag == ViCommConst.FLAG_ON
					|| sessionMan.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY)
				)
				&& sessionMan.userMan.noReportAffiliateFlag == ViCommConst.FLAG_ON
				&& !sessionMan.adCd.Equals("")
			) {
				this.SetAffiliateCookieImg(ref sResult);
			}
		}

		return sResult;
	}

	/// <summary>
	/// 広告の成果報告用のImageタグ/JavaScriptタグの取得＆設定
	/// </summary>
	/// <param name="sTag"></param>
	/// <returns></returns>
	private void SetAffiliateCookieImg(ref string sTag) {
		using (TempRegist oTemp = new TempRegist()) {
			if (oTemp.GetOneByUserSeq(sessionMan.userMan.userSeq)) {
				string sTrackingUrl;
				switch (oTemp.authResultTransType) {
					case ViCommConst.AUTH_AFFILIATE_TRASN_IMAGE_TAG:

						string sUID;
						if (oTemp.mobileCarrierCd.Equals(ViCommConst.KDDI)) {
							sUID = oTemp.terminalUniqueId;
						} else {
							sUID = oTemp.imodeId;
						}
						sTrackingUrl = string.Format(oTemp.trackingUrl,oTemp.registAffiliateCd,sessionMan.userMan.loginId,oTemp.registIpAddr,sUID);
						sTrackingUrl += oTemp.trackingAdditionInfo;
						sTag = string.Format("<img src=\"{0}\" width=\"1\" height=\"1\" />",sTrackingUrl);
						sessionMan.userMan.noReportAffiliateFlag = 0;
						oTemp.CompliteTrackingReport(sessionMan.userMan.userSeq);
						break;
					case ViCommConst.AUTH_AFFILIATE_TRASN_JS_TAG:
						sTag = oTemp.trackingJs;
						sessionMan.userMan.noReportAffiliateFlag = 0;
						oTemp.CompliteTrackingReport(sessionMan.userMan.userSeq);
						break;
				}
			}
		}
	}

	private string GetMailObjTempId(string pTag) {
		string sObjTempId = string.Empty;
		if (sessionMan.userMan.mailData.ContainsKey(iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"]))) {
			sObjTempId = sessionMan.userMan.mailData[iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["data"])].objTempId;
		}
		if (string.IsNullOrEmpty(sObjTempId)) {
			string sRequestTxMailSeq;
			if (GetValue(pTag,ViCommConst.DATASET_MAIL,"REQUEST_TX_MAIL_SEQ",out sRequestTxMailSeq)) {
				using (MailLog oMailLog = new MailLog()) {
					sObjTempId = oMailLog.GetObjTempId(sRequestTxMailSeq);
				}
			}
		}
		return sObjTempId;
	}

	private string GetPictureUploadAddr(string pTag,string pArgument,int attachType) {
		string sObjTempId = string.Empty;
		if (attachType.Equals(ViCommConst.ATTACHED_MAIL)) {
			sObjTempId = GetMailObjTempId(pTag);
		}

		string sMailto = parseContainer.Parse(pArgument);
		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailto)) {
			sUrlHeader = ViCommConst.PHOTO_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailto,ViCommConst.PHOTO_URL_HEADER2);
		}

		return sUrlHeader +
				attachType.ToString() +
				sessionMan.site.siteCd + "_" +
				sessionMan.userMan.loginId + ViCommConst.MAIN_CHAR_NO + "_" +
				sObjTempId +
				"@" + sessionMan.site.mailHost;
	}

	private string GetMovieUploadAddr(string pTag,string pArgument,int attachType) {
		string sObjTempId = string.Empty;
		if (attachType.Equals(ViCommConst.ATTACHED_MAIL)) {
			sObjTempId = GetMailObjTempId(pTag);
		}

		string sMailto = parseContainer.Parse(pArgument);
		string sUrlHeader = string.Empty;
		if (string.IsNullOrEmpty(sMailto)) {
			sUrlHeader = ViCommConst.MOVIE_URL_HEADER;
		} else {
			sUrlHeader = string.Format("{0}:{1}",sMailto,ViCommConst.MOVIE_URL_HEADER2);
		}

		return sUrlHeader +
				attachType.ToString() +
				sessionMan.site.siteCd + "_" +
				sessionMan.userMan.loginId + ViCommConst.MAIN_CHAR_NO + "_" +
				sObjTempId +
				"@" + sessionMan.site.mailHost;
	}

	private string GetPrepaidTopUrl() {
		string sBaseUrl = sessionMan.root + sessionMan.sysType;
		if (sessionMan.logined) {
			return sessionMan.GetNavigateUrl(sBaseUrl,sessionMan.sessionId,sessionMan.site.userTopId);
		} else {
			return sessionMan.GetNavigateUrl(sBaseUrl,sessionMan.sessionId,"PrepaidUserTop.aspx");
		}
	}

	private string GetParentSysLoginUrl() {
		string sUrl = "";
		if (sessionMan.logined) {
			sUrl = string.Format(sessionMan.site.parentWebLoginUrl,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
		}
		return sUrl;
	}

	private string GetCastDisplayTag(string pTag,string pArgument) {
		string[] sValues = pArgument.Split(',');
		if (sValues.Length < 3) {
			return string.Format("Syntax Error Tag:{0}",pTag);
		}

		// category,count,large picture flag
		string sPickupId = sValues[0];
		int iCategoryIndex = int.Parse(sValues[0]);
		int iCastCount = int.Parse(sValues[1]);
		int iRequestCount = iCastCount;
		int iGetType = 0;
		bool bIsSmallImage,bIsFixed;

		if (int.Parse(sValues[2]) == 0) {
			bIsSmallImage = true;
			bIsFixed = false;
		} else {
			bIsSmallImage = false;
			bIsFixed = true;
		}

		if (pTag.StartsWith("$CAST_PICKUP_PIC")) {
			iGetType = GET_PICKUP;
		} else if (pTag.StartsWith("$CAST_LIST_PIC")) {
			iGetType = GET_RANDOM;
		} else if (pTag.StartsWith("$CAST_PICKUP_OBJ")) {
			iGetType = GET_PICKUP_OBJ;
		} else {
			iGetType = GET_RECOMMEND;
		}

		string[] sCastSeq = null;
		string[] sLoginId = null;
		string[] sCryptValue = null;
		string[] sHandleNm = null;
		string[] sImagePath = null;
		string[] sRecNum = null;
		string[] sObjSeq = null;
		string[] sActCategoryIdx = new string[iCastCount];
		string sActCategorySeq = string.Empty;
		if (iGetType != GET_RECOMMEND && iGetType != GET_PICKUP_OBJ) {
			sActCategorySeq = ((ActCategory)sessionMan.categoryList[iCategoryIndex]).actCategorySeq;
		}
		string sColor = "";

		if (sessionMan.currentViewMask == 0) {
			sColor = sessionMan.site.colorBack;

		} else if ((sessionMan.currentViewMask & ViCommConst.VIEW_MASK_ACT_CATEGORY) != 0) {
			sColor = ((ActCategory)sessionMan.categoryList[sessionMan.currentCategoryIndex]).colorBack;

		} else if ((sessionMan.currentViewMask & ViCommConst.VIEW_MASK_USER_RANK) != 0) {
			sColor = sessionMan.userRank.colorBack;

		} else if ((sessionMan.currentViewMask & ViCommConst.VIEW_MASK_AD_GROUP) != 0) {
			sColor = sessionMan.adGroup.colorBack;

		} else if ((sessionMan.currentViewMask & ViCommConst.VIEW_MASK_HTML_DOC_COLOR) != 0) {
			sColor = sessionMan.webFace.colorBack;
		}

		ulong ulSeekMode = 0;
		string sPickupType = string.Empty;


		switch (iGetType) {
			case GET_PICKUP:
				sessionMan.userMan.castCharacter.GetPickup(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sActCategorySeq,bIsSmallImage,out sCastSeq,out sCryptValue,out sLoginId,out sHandleNm,out sImagePath,out sRecNum);
				ulSeekMode = ViCommConst.INQUIRY_CAST_PICKUP;
				break;
			case GET_RECOMMEND:
				sessionMan.userMan.castCharacter.GetRandomRecommend(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sPickupId,bIsSmallImage,out sCastSeq,out sCryptValue,out sLoginId,out sHandleNm,out sImagePath,out sActCategoryIdx,out sRecNum);
				ulSeekMode = ViCommConst.INQUIRY_RANDOM_RECOMMEND;
				break;
			case GET_PICKUP_OBJ:
				using (Pickup oPickup = new Pickup()) {
					sPickupType = oPickup.GetPickupType(sessionMan.site.siteCd,sPickupId);
				}
				switch (sPickupType) {
					case ViCommConst.PicupTypes.CAST_CHARACTER:
						sessionMan.userMan.castCharacter.GetRandomCast(3,sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sActCategorySeq,bIsSmallImage,out sCastSeq,out sCryptValue,out sLoginId,out sHandleNm,out sImagePath,out sRecNum);
						break;
					case ViCommConst.PicupTypes.BBS_PIC:
					case ViCommConst.PicupTypes.PROFILE_PIC:
						sessionMan.userMan.castCharacter.GetRandomPic(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sPickupId,bIsSmallImage,out sCastSeq,out sCryptValue,out sLoginId,out sHandleNm,out sImagePath,out sActCategoryIdx,out sObjSeq,out sRecNum);
						break;
					case ViCommConst.PicupTypes.BBS_MOVIE:
					case ViCommConst.PicupTypes.PROFILE_MOVIE:
						sessionMan.userMan.castCharacter.GetRandomMovie(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sPickupId,bIsSmallImage,out sCastSeq,out sCryptValue,out sLoginId,out sHandleNm,out sImagePath,out sActCategoryIdx,out sObjSeq,out sRecNum);
						break;
				}
				break;
			default:
				sessionMan.userMan.castCharacter.GetRandomCast(3,sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sActCategorySeq,bIsSmallImage,out sCastSeq,out sCryptValue,out sLoginId,out sHandleNm,out sImagePath,out sRecNum);
				ulSeekMode = ViCommConst.INQUIRY_CAST_RECOMMEND;
				break;
		}


		if (iCastCount > sCastSeq.Length) {
			iCastCount = sCastSeq.Length;
		}

		int iWidth = (bIsFixed) ? (100 / iRequestCount) * iCastCount : 100;

		string sHtml = string.Format("<table bgcolor=\"{0}\" border=\"0\" width=\"{1}%\"><tr>",sColor,iWidth);
		string sCastQuery;
		for (int i = 0;i < iCastCount;i++) {
			sHandleNm[i] = parseContainer.Parse(sHandleNm[i]);
			if (iGetType == GET_RECOMMEND || iGetType == GET_PICKUP_OBJ) {
				if (string.IsNullOrEmpty(sActCategoryIdx[i])) {
					iCategoryIndex = 0;
				} else {
					iCategoryIndex = int.Parse(sActCategoryIdx[i]);
				}
			}

			switch (sPickupType) {
				case ViCommConst.PicupTypes.BBS_PIC:
					ulSeekMode = ViCommConst.INQUIRY_BBS_PIC;
					sCastQuery = string.Format("ViewBbsPic.aspx?seekmode={0}&category={1}&loginid={2}&userrecno={3}&{4}={5}&objseq={6}",
									ulSeekMode,iCategoryIndex,sLoginId[i],sRecNum[i],sessionMan.site.charNoItemNm,sCryptValue[i],sObjSeq[i]);
					break;
				case ViCommConst.PicupTypes.PROFILE_PIC:
					ulSeekMode = ViCommConst.INQUIRY_GALLERY;
					sCastQuery = string.Format("ViewGallery.aspx?seekmode={0}&category={1}&loginid={2}&userrecno={3}&{4}={5}&picseq={6}",
									ulSeekMode,iCategoryIndex,sLoginId[i],sRecNum[i],sessionMan.site.charNoItemNm,sCryptValue[i],sObjSeq[i]);
					break;
				case ViCommConst.PicupTypes.BBS_MOVIE:
					ulSeekMode = ViCommConst.INQUIRY_BBS_MOVIE;
					sCastQuery = string.Format("ViewBbsMovie.aspx?seekmode={0}&category={1}&loginid={2}&userrecno={3}&{4}={5}&objseq={6}",
									ulSeekMode,iCategoryIndex,sLoginId[i],sRecNum[i],sessionMan.site.charNoItemNm,sCryptValue[i],sObjSeq[i]);
					break;
				case ViCommConst.PicupTypes.PROFILE_MOVIE:
					ulSeekMode = ViCommConst.INQUIRY_CAST_MOVIE;
					sCastQuery = string.Format("ViewProfileMovie.aspx?seekmode={0}&category={1}&loginid={2}&userrecno={3}&{4}={5}&movieseq={6}",
									ulSeekMode,iCategoryIndex,sLoginId[i],sRecNum[i],sessionMan.site.charNoItemNm,sCryptValue[i],sObjSeq[i]);
					break;
				default:
					sCastQuery = string.Format("Profile.aspx?seekmode={0}&category={1}&loginid={2}&userrecno={3}&{4}={5}",
									ulSeekMode,iCategoryIndex,sLoginId[i],sRecNum[i],sessionMan.site.charNoItemNm,sCryptValue[i]);
					break;
			}

			sCastQuery = sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sCastQuery);

			if (pTag.EndsWith("NONAME")) {
				sHtml = sHtml + string.Format("<td bgcolor=\"{0}\" width=\"33%\"><center><a href=\"{2}\"><img src=\"{1}\" width=\"80%\"></a><br></center>",
						sColor,sImagePath[i],sCastQuery);
			} else {
				sHtml = sHtml + string.Format("<td bgcolor=\"{0}\" width=\"33%\"><center><img src=\"{1}\" width=\"80%\"><br>" +
						"<a href=\"{2}\"><font size=\"1\">{3}</font></a><br></center>",
						sColor,sImagePath[i],sCastQuery,sHandleNm[i]);
			}
		}
		sHtml = sHtml + "</tr></table>";
		return sHtml;
	}

	private string GetListProduct(ulong pSeekMode,string pTag,string pArgument) {

		DesignPartsArgument oArg = DesignPartsArgument.Parse(this,pTag,pArgument);

		ProductSeekCondition oCondition = new ProductSeekCondition(oArg.Query);
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.SeekMode = pSeekMode;
		oCondition.UserSeq = sessionMan.userMan.userSeq;
		oCondition.UserCharNo = sessionMan.userMan.userCharNo;

		DataSet oTmpDataSet = this.ParseCache.Get(pTag,pArgument) as DataSet;
		int iTmpTableIndex = -1;
		switch (pSeekMode) {
			case ViCommConst.INQUIRY_PRODUCT_MOVIE:
				if (oTmpDataSet == null) {
					using (ProductMovie oProductMovie = new ProductMovie(sessionMan)) {
						oTmpDataSet = oProductMovie.GetPageCollection(oCondition,1,oArg.NeedCount);
					}
				}
				iTmpTableIndex = ViCommConst.DATASET_PRODUCT_MOVIE;
				break;
			case ViCommConst.INQUIRY_PRODUCT_PIC:
				if (oTmpDataSet == null) {
					using (ProductPic oProductPic = new ProductPic(sessionMan)) {
						oTmpDataSet = oProductPic.GetPageCollection(oCondition,1,oArg.NeedCount);
					}
				}
				iTmpTableIndex = ViCommConst.DATASET_PRODUCT_PIC;
				break;
			case ViCommConst.INQUIRY_PRODUCT_AUCTION:
				if (oTmpDataSet == null) {

					using (ProductAuction oProductAuction = new ProductAuction(sessionMan)) {
						oTmpDataSet = oProductAuction.GetPageCollection(oCondition,1,oArg.NeedCount);
					}
				}
				iTmpTableIndex = ViCommConst.DATASET_PRODUCT_AUCTION;
				break;
			case ViCommConst.INQUIRY_PRODUCT_THEME:
				if (oTmpDataSet == null) {
					using (ProductTheme oProductTheme = new ProductTheme(sessionMan)) {
						oTmpDataSet = oProductTheme.GetPageCollection(oCondition,1,oArg.NeedCount);
					}
				}
				iTmpTableIndex = ViCommConst.DATASET_PRODUCT_THEME;
				break;
			default:
				oTmpDataSet = null;
				break;

		}
		this.ParseCache.Add(pTag,pArgument,oTmpDataSet);


		if (oTmpDataSet == null)
			return string.Empty;

		string iPrevSeekMode = sessionObjs.seekMode;
		ProductSeekCondition oPrevSeekCondition = sessionObjs.productSeekCondition;
		try {
			sessionObjs.seekMode = oCondition.SeekMode.ToString();
			sessionObjs.productSeekCondition = oCondition;
			return parseContainer.ParseDirect(oArg.HtmlTemplate,oTmpDataSet,iTmpTableIndex);
		} finally {
			sessionObjs.seekMode = iPrevSeekMode;
			sessionObjs.productSeekCondition = oPrevSeekCondition;
		}

	}

	private string GetListProductAuctionBidLog(string pTag,string pArgument) {
		DesignPartsArgument oArg = DesignPartsArgument.Parse(this,pTag,pArgument);


		ProductSeekCondition oCondition = new ProductSeekCondition(oArg.Query);
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.SeekMode = ViCommConst.INQUIRY_PRODUCT_AUCTION_BID_LOG;
		oCondition.UserSeq = sessionMan.userMan.userSeq;
		oCondition.UserCharNo = sessionMan.userMan.userCharNo;

		DataSet oTmpDataSet = null;
		int iTmpTableIndex = ViCommConst.DATASET_PRODUCT_AUCTION_BID_LOG;

		using (ProductAuctionBidLog oProductAuctionBidLog = new ProductAuctionBidLog(sessionMan)) {
			oTmpDataSet = oProductAuctionBidLog.GetPageCollection(oCondition,1,oArg.NeedCount);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		string iPrevSeekMode = sessionObjs.seekMode;
		ProductSeekCondition oPrevSeekCondition = sessionObjs.productSeekCondition;
		try {
			sessionObjs.seekMode = oCondition.SeekMode.ToString();
			sessionObjs.productSeekCondition = oCondition;
			return parseContainer.ParseDirect(oArg.HtmlTemplate,oTmpDataSet,iTmpTableIndex);
		} finally {
			sessionObjs.seekMode = iPrevSeekMode;
			sessionObjs.productSeekCondition = oPrevSeekCondition;
		}
	}

	private static readonly Regex _regexPict = new Regex(@"(?<pict>\$x.{4};)",RegexOptions.Compiled);

	private string GetManAttrHtml(string pTag,string pArgument) {
		string sText = "";
		string sValue = "";
		sText = parseContainer.Parse(pArgument);

		for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
			if (sText.Equals(sessionMan.userMan.attrList[i].itemNo)) {
				if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
					PictographConverter oPictConverter = PictographConverter.Create(sessionMan.carrier);
					oPictConverter.NeedConvert2Image = false;
					string sAttrInputValue = _regexPict.Replace(sessionMan.userMan.attrList[i].attrImputValue,new MatchEvaluator(delegate(Match pMatch) {
						return SysConst.EncodingShiftJIS.GetString(oPictConverter.ConvertTag2Pict(pMatch.Value,true));
					}));

					if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
						sValue = string.Format("<textarea name=\"txtAreaUserManItem{0}\" rows=\"{1}\" cols=\"24\" id=\"txtAreaUserManItem{0}\" {3}>{2}</textarea>",sessionMan.userMan.attrList[i].attrTypeSeq,sessionMan.userMan.attrList[i].rowCount,parseContainer.Parse(sAttrInputValue),this.IsCarrierContainsOfSmart() ? "class=\"emoji_enabled\"" : string.Empty);
					} else {
						sValue = string.Format("<input name=\"txtUserManItem{0}\" value=\"{1}\" size=\"12\" maxlength=\"300\" {2}/>",sessionMan.userMan.attrList[i].attrTypeSeq,parseContainer.Parse(sAttrInputValue),this.IsCarrierContainsOfSmart() ? "class=\"emoji_enabled\"" : string.Empty);
					}
				} else if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_LIST) {
					sValue = string.Format("<select name=\"lstUserManItem{0}\">",sessionMan.userMan.attrList[i].attrTypeSeq);
					using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
						DataSet ds = oAttrTypeValue.GetList(sessionMan.userMan.siteCd,sessionMan.userMan.attrList[i].attrTypeSeq);

						foreach (DataRow dr in ds.Tables[0].Rows) {
							sValue += string.Format("<option value=\"{0}\"",dr["MAN_ATTR_SEQ"]);
							if (dr["MAN_ATTR_SEQ"].ToString().Equals(sessionMan.userMan.attrList[i].attrSeq)) {
								sValue += string.Format(" selected>{0}\n",dr["MAN_ATTR_NM"]);
							} else {
								sValue += string.Format(">{0}\n",dr["MAN_ATTR_NM"]);
							}
						}
						sValue += "</select>";
					}
				} else {
					using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
						DataSet ds = oAttrTypeValue.GetList(sessionMan.userMan.siteCd,sessionMan.userMan.attrList[i].attrTypeSeq);

						foreach (DataRow dr in ds.Tables[0].Rows) {
							sValue += string.Format("<input type=\"radio\" name=\"rdoUserManItem{0}\" value=\"{1}\"",sessionMan.userMan.attrList[i].attrTypeSeq,dr["MAN_ATTR_SEQ"]);
							if (dr["MAN_ATTR_SEQ"].ToString().Equals(sessionMan.userMan.attrList[i].attrSeq)) {
								sValue += string.Format(" checked>{0}<br>\n",dr["MAN_ATTR_NM"]);
							} else {
								sValue += string.Format(">{0}<br>\n",dr["MAN_ATTR_NM"]);
							}
						}
					}
				}
			}
		}

		return sValue;
	}

	private string GetHrefVideoTel(string pLabel) {
		string sLabel = parseContainer.Parse(pLabel);
		if (sessionMan.errorMessage.Equals("")) {
			if (sessionMan.ivpRequest.useCrosmile) {
				return string.Format("<a href=\"{0}/MakeCall/?callType=2&userAgentKey={1}&userAgentVersion={2}&serviceCode={3}&uasUserId={4}&uasUserIdType={5}&screenPatternId={6}&extendKey={7}&accountableSecond={8}\">{9}</a>",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]),
					sessionMan.site.ivpSiteCd,
					"00.00.0",
					sessionMan.ivpRequest.crosmileDirectFlag ? iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartDirectServiceCode"]) : iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartServiceCode"]),
					sessionMan.ivpRequest.dialNo,
					"4",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmScreenPatternId"]),
					sessionMan.ivpRequest.acceptSeq,
					0,
					sLabel);
			} else {
				return string.Format("<a href=\"{0}{1}\">{2}</a>",sessionMan.ivpRequest.videoPrefix,sessionMan.ivpRequest.dialNo,sLabel);
			}
		} else {
			return "";
		}
	}

	private string GetHrefVoiceTel(string pLabel) {
		string sLabel = parseContainer.Parse(pLabel);
		if (sessionMan.errorMessage.Equals("")) {
			if (sessionMan.ivpRequest.useCrosmile) {
				return string.Format("<a href=\"{0}/MakeCall/?callType=1&userAgentKey={1}&userAgentVersion={2}&serviceCode={3}&uasUserId={4}&uasUserIdType={5}&screenPatternId={6}&extendKey={7}&accountableSecond={8}\">{9}</a>",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]),
					sessionMan.site.ivpSiteCd,
					"00.00.0",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmSmartDirectServiceCode"]),
					sessionMan.ivpRequest.dialNo,
					"4",
					iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmScreenPatternId"]),
					sessionMan.ivpRequest.acceptSeq,
					0,
					sLabel);
			} else if (sessionMan.ivpRequest.useVoiceapp) {
				string sData = "outgoing=1";
				sData = AEScryptHelper.Encrypt(sData);
				return string.Format("<a href=\"girlschatapp://call/?data={0}\">{1}</a>",sData,sLabel);
			} else {
				return string.Format("<a href=\"{0}{1}\">{2}</a>",sessionMan.ivpRequest.audioPrefix,sessionMan.ivpRequest.dialNo,sLabel);
			}
		} else {
			return "";
		}
	}

	private bool ParseImageArguement(string pTag,string pText,out int pIdx,out string pWidth,out string pAccessKey) {
		pIdx = 0;
		pWidth = "100%";
		pAccessKey = "";

		string[] sValue = pText.Split(',');
		if (sValue.Length < 1) {
			parseErrorList.Add(string.Format("Argument Invalid:{0} {1}",pTag,pText));
			return false;
		}
		pIdx = int.Parse(sValue[0]);
		if (sValue.Length >= 2) {
			pWidth = sValue[1];
		}
		if (sValue.Length >= 3) {
			pAccessKey = string.Format(" accesskey=\"{0}\" ",sValue[2]);
		}
		return true;
	}


	protected string GetCastCount(string pTag,string pArgument) {
		return GetCastCount(pTag,sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,GetActCategorySeq(pTag,pArgument),sessionMan.userMan.castCharacter);
	}

	private string GetRandomCastPicture(string pTag,string pArgument) {
		int iCategoryIndex = 1;
		if (!pArgument.Equals("")) {
			iCategoryIndex = int.Parse(pArgument);
		}

		string[] sCastSeq;
		string[] sCryptValue;
		string[] sLoginId;
		string[] sHandleNm;
		string[] sImagePath;
		string[] sRecNum;
		string sActCategorySeq = ((ActCategory)sessionMan.categoryList[iCategoryIndex]).actCategorySeq;

		sessionMan.userMan.castCharacter.GetRandomCast(1,sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sActCategorySeq,true,out sCastSeq,out sCryptValue,out sLoginId,out sHandleNm,out sImagePath,out sRecNum);
		if (sImagePath.Length > 0) {
			return sImagePath[0];
		} else {
			return "";
		}
	}

	private string GetRegistMailAddr() {
		string sCodeType = ViCommConst.REGIST_FROM_NONE;
		string sCode = "";

		if (!sessionMan.affiliaterCd.Equals("")) {
			sCodeType = sessionMan.affiliateCompany;
			sCode = sessionMan.affiliaterCd;
		} else if (!sessionMan.adCd.Equals("")) {
			sCodeType = ViCommConst.REGIST_FROM_AD;
			sCode = sessionMan.adCd;
		}
		return string.Format("regm{0}00{1}{2}{3}@{4}",
			sessionMan.site.siteCd,
			sessionMan.carrier,
			sCodeType,
			sCode,
			sessionMan.site.mailHost);
	}

	private string GetRegistMailTempId() {
		return string.Format("utnm{0}@{1}",sessionMan.userMan.tempRegistId,sessionMan.site.mailHost);
	}


	private string GetDoc(string pTag,string pFiledNm) {
		string sValue = "";
		if (GetValue(pTag,pFiledNm,out sValue)) {
			sValue = parseContainer.Parse(Regex.Replace(sValue.ToString(),"\r\n","<br />"));
		}
		return sValue;
	}

	private string GetNewMailInfo(string pMailType,string pTxRxType,string pField) {
		string sValue;
		using (MailBox oMailBox = new MailBox()) {
			DataSet ds = oMailBox.GetNewMail(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,ViCommConst.MAN,pMailType,pTxRxType);
			if (ds.Tables[0].Rows.Count != 0) {
				sValue = ds.Tables[0].Rows[0][pField].ToString();
			} else {
				sValue = "";
			}
		}
		return sValue;
	}

	private string GetAttachedValue(string sFieldNm) {
		string sValue = string.Empty;
		string sMovieSeq = iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["movieseq"]);
		string sPicSeq = iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["picseq"]);

		if (!sMovieSeq.Equals(string.Empty)) {
			using (UserManMovie oUserManMovie = new UserManMovie()) {
				sValue = oUserManMovie.GetValueByMovieSeq(sessionMan.userMan.siteCd,sMovieSeq,sFieldNm);
			}
		} else if (!sPicSeq.Equals(string.Empty)) {
			using (UserManPic oUserManPic = new UserManPic()) {
				sValue = oUserManPic.GetValueByPicSeq(sessionMan.userMan.siteCd,sPicSeq,sFieldNm);
			}
		}
		return sValue;
	}

	protected string GetCastLoginHistoryCount(string pTag,string pArgument) {
		int iDays,iLoginCount,iLoginUniqueCount;
		int.TryParse(pArgument,out iDays);
		using (Access oAccess = new Access()) {
			oAccess.GetCount(sessionMan.site.siteCd,ViCommConst.OPERATOR,iDays,out iLoginCount,out iLoginUniqueCount);
		}
		return iLoginCount.ToString();
	}

	protected string CreatePackSelect(string pTag,string pArgument) {
		int iFirstSettleEnableDays = 0;
		DateTime dtRegistDate = DateTime.Parse(sessionMan.userMan.registDay).Date;
		DateTime dtToday = DateTime.Today;
		TimeSpan tsDaysFromRegist = dtToday - dtRegistDate;
		bool bFirstSettleEnable;

		using (Site oSite = new Site()) {
			string sFirstSettleEnableDays = string.Empty;
			oSite.GetValue(sessionMan.site.siteCd,"FIRST_SETTLE_ENABLE_DAYS",ref sFirstSettleEnableDays);
			
			if (!int.TryParse(sFirstSettleEnableDays,out iFirstSettleEnableDays)) {
				iFirstSettleEnableDays = 0;
			}
		}

		if (iFirstSettleEnableDays == 0) {
			bFirstSettleEnable = true;
		} else if (iFirstSettleEnableDays >= tsDaysFromRegist.Days) {
			bFirstSettleEnable = true;
		} else {
			bFirstSettleEnable = false;
		}
		
		string sHtml = "<select name=\"*packamt\">\r\n";
		using (Pack oPack = new Pack()) {
			DataSet ds;
			if (sessionMan.userMan.totalReceiptAmt > 0 || !bFirstSettleEnable) {
				ds = oPack.GetNonFirstList(sessionMan.site.siteCd,pArgument,0,sessionMan.userMan.userSeq);
			} else {
				ds = oPack.GetList(sessionMan.site.siteCd,pArgument,0,sessionMan.userMan.userSeq);
			}
			//			oPack.CalcServicePoint(sessionMan.userMan.userSeq,ds);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				sHtml += string.Format("<option value=\"{0}\">{1}</option>\r\n",dr["SALES_AMT"].ToString(),dr["REMARKS"].ToString());
			}
		}
		return sHtml += "</select>";
	}

	protected string GetCampainCd(string pTag,string pArgument) {
		using (Campain oCampain = new Campain()) {
			return oCampain.GetCurrentCampainCd(sessionMan.site.siteCd);
		}
	}

	protected string GetIsExistsReceiptCampain(string pTag,string pArgument) {
		using (ReceiptServicePoint oReceiptServicePoint = new ReceiptServicePoint()) {
			return oReceiptServicePoint.IsExists(sessionMan.site.siteCd,pArgument) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
		}
	}

	protected string GetAspAdCount(string pTag,string pArgument) {
		const string TOPIC = "01";
		const string CATEGORY = "02";
		const string POINT = "03";
		const string MONTHLYFEE = "04";

		int iCnt = 0;
		string[] sValues = pArgument.Split(',');
		bool bSettleOnly = sValues[1].Equals(ViCommConst.FLAG_ON_STR);
		using (AspAd oAspAd = new AspAd()) {
			switch (sValues[0]) {
				case TOPIC:
					iCnt = oAspAd.GetCount(sessionMan.userMan.siteCd,ViCommConst.UsableSexCd.MAN,sValues[2],string.Empty,string.Empty,string.Empty,string.Empty,string.Empty,sessionMan.carrier,bSettleOnly,sessionMan.userMan.userSeq);
					break;
				case CATEGORY:
					iCnt = oAspAd.GetCount(sessionMan.userMan.siteCd,ViCommConst.UsableSexCd.MAN,string.Empty,string.Empty,string.Empty,sValues[2],string.Empty,string.Empty,sessionMan.carrier,bSettleOnly,sessionMan.userMan.userSeq);
					break;
				case POINT:
					iCnt = oAspAd.GetCount(sessionMan.userMan.siteCd,ViCommConst.UsableSexCd.MAN,string.Empty,sValues[2],sValues[3],string.Empty,string.Empty,string.Empty,sessionMan.carrier,bSettleOnly,sessionMan.userMan.userSeq);
					break;
				case MONTHLYFEE:
					iCnt = oAspAd.GetCount(sessionMan.userMan.siteCd,ViCommConst.UsableSexCd.MAN,string.Empty,string.Empty,string.Empty,string.Empty,sValues[2],sValues[3],sessionMan.carrier,bSettleOnly,sessionMan.userMan.userSeq);
					break;
			}
		}
		return iCnt.ToString();
	}

	private int GetUsePoint(string pTag,string pArgument) {
		string[] sValues;
		string sCastUserSeq,sActCategorySeq;
		sCastUserSeq = string.Empty;
		sActCategorySeq = string.Empty;

		if (this.currentTableIdx != 0) {
			if (GetValues(pTag,"USER_SEQ,ACT_CATEGORY_SEQ",out sValues)) {
				sCastUserSeq = sValues[0];
				sActCategorySeq = sValues[1];
			}
		}

		int iPoint,iUnitSec;
		sessionMan.userMan.GetCornerCharge(sessionMan.site.siteCd,sCastUserSeq,pArgument,sActCategorySeq,out iUnitSec,out iPoint);

		return iPoint;
	}

	private bool IsRegistSite(string pArgment) {
		string pUtn = MobileLib.Mobile.GetUtn(sessionMan.carrier,sessionMan.requestQuery);
		string pImodeId = MobileLib.Mobile.GetiModeId(sessionMan.carrier,sessionMan.requestQuery);
		string pUserSeq = string.Empty;
		bool bExist = false;

		if (!pUtn.Equals(string.Empty)) {
			using (User oUser = new User()) {
				bExist = oUser.RegistSiteByUtn(pArgment,pUtn,ViCommConst.MAN,out pUserSeq);
			}
		}
		if (bExist) {
			return bExist;
		}
		if (!pImodeId.Equals(string.Empty)) {
			using (User oUser = new User()) {
				bExist = oUser.RegistSiteByImodeId(pArgment,pImodeId,ViCommConst.MAN,out pUserSeq);
			}
		}
		return bExist;
	}

	private string IsActivated(string pTag,string pArgument) {

		string sUserSeq,sUserCharNo,sSiteCd;

		sSiteCd = this.sessionMan.site.siteCd;
		sUserSeq = this.sessionMan.userMan.userSeq;
		sUserCharNo = this.sessionMan.userMan.userCharNo;

		if (!string.IsNullOrEmpty(pArgument)) {
			sSiteCd = pArgument;
		}

		using (UserManCharacter oUser = new UserManCharacter()) {
			if (oUser.IsActivated(sSiteCd,sUserSeq,sUserCharNo)) {
				return ViCommConst.FLAG_ON_STR;
			} else {
				return ViCommConst.FLAG_OFF_STR;
			}
		}
	}

	private string GetHrefSmartPhoneCertify(string pTag,string pLabel) {
		string sLabel,sAccessKey;
		GetLableAndAccessKey(pLabel,out sLabel,out sAccessKey);
		return string.Format("<a href=\"RequestIVP.aspx?request={0}\" {1}>{2}</a>",ViCommConst.REQUEST_TEL_AUTH_LIGHT,sAccessKey,sLabel);
	}

	private string GetListPickup(string pTag,string pArgument) {
		DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		string sPickupId = iBridUtil.GetStringValue(oPartsArgs.Query["pickupid"]);
		string sSortType = iBridUtil.GetStringValue(oPartsArgs.Query["sorttype"]);
		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		string sRandom = iBridUtil.GetStringValue(oPartsArgs.Query["random"]);
		string sPickupStartDay = iBridUtil.GetStringValue(oPartsArgs.Query["pickupstartday"]);
		string sLastLoginDate = iBridUtil.GetStringValue(oPartsArgs.Query["lastlogin"]);
		string sLastTxMailDays = iBridUtil.GetStringValue(oPartsArgs.Query["lasttxmaildays"]);
		string sOnlineStatus = iBridUtil.GetStringValue(oPartsArgs.Query["online"]);
		string sHasProfilePic = iBridUtil.GetStringValue(oPartsArgs.Query["hasprofpic"]);


		if (string.IsNullOrEmpty(sPickupId)) {
			throw new ApplicationException("pickupid is empty.");
		}


		string sPickupType = string.Empty;
		using (Pickup oPickup = new Pickup()) {
			sPickupType = oPickup.GetPickupType(sessionObjs.site.siteCd,sPickupId);
		}
		if (string.IsNullOrEmpty(sPickupType)) {
			throw new ApplicationException("pickupid not found.id:" + sPickupId);
		}


		SeekCondition oSeekCondition = new SeekCondition();
		oSeekCondition.InitCondtition();
		oSeekCondition.pickupId = sPickupId;
		oSeekCondition.sortType = sSortType;
		oSeekCondition.pickupStartPubDay = sPickupStartDay;
		oSeekCondition.directRandom = sRandom.Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		oSeekCondition.lastLoginDate = sLastLoginDate;
		oSeekCondition.lastTxMailDays = sLastTxMailDays;
		int.TryParse(sOnlineStatus,out oSeekCondition.onlineStatus);
		oSeekCondition.hasProfilePic = sHasProfilePic;

		int iDataSetIndex = 0;
		DataSet oDataSet = null;
		switch (sPickupType) {
			case ViCommConst.PicupTypes.BBS_MOVIE:
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
				using (CastMovie oCastMovie = new CastMovie(sessionObjs)) {
					oDataSet = oCastMovie.GetPageCollection(sessionObjs.site.siteCd,string.Empty,string.Empty,string.Empty,false,string.Empty,sessionMan.actCategorySeq,sAttrTypeSeq,sAttrSeq,oSeekCondition,sSortType,1,oPartsArgs.NeedCount,true);
				}
				iDataSetIndex = ViCommConst.DATASET_PARTNER_MOVIE;
				break;
			case ViCommConst.PicupTypes.BBS_PIC:
				using (CastPic oCastPic = new CastPic(sessionObjs)) {
					oDataSet = oCastPic.GetPageCollection(sessionObjs.site.siteCd,string.Empty,string.Empty,false,ViCommConst.ATTACHED_BBS.ToString(),sessionMan.actCategorySeq,sAttrTypeSeq,sAttrSeq,oSeekCondition,1,oPartsArgs.NeedCount);
				}
				iDataSetIndex = ViCommConst.DATASET_PARTNER_PIC;
				break;
			case ViCommConst.PicupTypes.PROFILE_PIC:
				using (CastPic oCastPic = new CastPic(sessionObjs)) {
					oDataSet = oCastPic.GetPageCollection(sessionObjs.site.siteCd,string.Empty,string.Empty,false,ViCommConst.ATTACHED_PROFILE.ToString(),sessionMan.actCategorySeq,sAttrTypeSeq,sAttrSeq,oSeekCondition,1,oPartsArgs.NeedCount);
				}
				iDataSetIndex = ViCommConst.DATASET_PARTNER_PIC;
				break;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				oSeekCondition.siteCd = sessionObjs.site.siteCd;
				oSeekCondition.userSeq = sessionMan.userMan.userSeq;
				oSeekCondition.userCharNo = sessionMan.userMan.userCharNo;
				oSeekCondition.conditionFlag = ViCommConst.INQUIRY_RECOMMEND;
				oSeekCondition.newCastDay = 0;
				oSeekCondition.userRank = "";

				if (sessionMan.currentCategoryIndex >= 0) {
					oSeekCondition.categorySeq = ((ActCategory)sessionMan.categoryList[sessionMan.currentCategoryIndex]).actCategorySeq;
				} else {
					oSeekCondition.categorySeq = "";
				}

				using (Cast oCast = new Cast(sessionObjs)) {

					oDataSet = oCast.GetPageCollection(oSeekCondition,1,oPartsArgs.NeedCount);
				}

				iDataSetIndex = ViCommConst.DATASET_CAST;
				break;

		}

		if (oDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(oPartsArgs.HtmlTemplate,oDataSet,iDataSetIndex);
	}

	private string GetListCastPic(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		string sAttrTypeSeq = string.Empty;
		string sAttrSeq = string.Empty;
		string sPicType = string.Empty;

		sAttrTypeSeq = iBridUtil.GetStringValue(pPartsArgs.Query["attrtypeseq"]);
		sAttrSeq = iBridUtil.GetStringValue(pPartsArgs.Query["attrseq"]);
		sPicType = iBridUtil.GetStringValue(pPartsArgs.Query["pictype"]);

		DataSet oTmpDataSet = null;

		using (CastPic oCastPic = new CastPic(sessionObjs)) {
			oTmpDataSet = oCastPic.GetPageCollection(sessionMan.site.siteCd,string.Empty,string.Empty,false,sPicType,string.Empty,sAttrTypeSeq,sAttrSeq,1,pPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_PARTNER_PIC);
	}

	private string GetListRandomChoice(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		string sRandomChoiceType = pPartsArgs.Query["choicetype"];

		DataSet oTmpDataSet = new DataSet();
		using (Cast oCast = new Cast(sessionObjs))
		using (RandomChoise oRandomChoise = new RandomChoise()) {
			foreach (RandomChoise oRandomChoiseEntity in oRandomChoise.GetRandomChoise(sessionObjs.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.OPERATOR,sRandomChoiceType,pPartsArgs.NeedCount)) {

				oTmpDataSet.Merge(oCast.GetOne(oRandomChoiseEntity.siteCd,oRandomChoiseEntity.userSeq,oRandomChoiseEntity.userCharNo,1));
			}
		}

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_CAST);
	}


	private string GetListCast(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);


		DataSet oTmpDataSet = null;

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(pPartsArgs.Query["loginid"]))) {
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

			using (Cast oCast = new Cast(sessionObjs)) {
				oTmpDataSet = oCast.GetOneByLoginId(sessionMan.site.siteCd,iBridUtil.GetStringValue(pPartsArgs.Query["loginid"]),sCastCharNo,1);
			}

		} else {
			SeekCondition oCondition = new SeekCondition();
			oCondition.InitCondtition();
			oCondition.conditionFlag = ViCommConst.INQUIRY_CONDITION;

			oCondition.siteCd = sessionMan.site.siteCd;
			oCondition.userSeq = sessionMan.userMan.userSeq;
			oCondition.userCharNo = sessionMan.userMan.userCharNo;
			oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
			oCondition.userRank = string.Empty;
			oCondition.random = ViCommConst.FLAG_OFF;
			oCondition.orderAsc = iBridUtil.GetStringValue(pPartsArgs.Query["orderasc"]);
			oCondition.orderDesc = iBridUtil.GetStringValue(pPartsArgs.Query["orderdesc"]);
			oCondition.handleNm = iBridUtil.GetStringValue(pPartsArgs.Query["handle"]);
			oCondition.enabledBlog = iBridUtil.GetStringValue(pPartsArgs.Query["enabledblog"]);
			oCondition.hasProfilePic = iBridUtil.GetStringValue(pPartsArgs.Query["hasprofpic"]);
			oCondition.pickupStartPubDay = iBridUtil.GetStringValue(pPartsArgs.Query["pickupstartday"]);
			oCondition.userAdminFlag = iBridUtil.GetStringValue(pPartsArgs.Query["useradmin"]);

			int.TryParse(iBridUtil.GetStringValue(pPartsArgs.Query["online"]),out oCondition.onlineStatus);
			int.TryParse(iBridUtil.GetStringValue(pPartsArgs.Query["random"]),out oCondition.directRandom);

			using (Cast oCast = new Cast(sessionObjs)) {
				oTmpDataSet = oCast.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
			}
		}




		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_CAST);
	}

	private string GetListMoteKing(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);

		DataSet oTmpDataSet = null;

		SeekCondition oCondition = new SeekCondition();
		oCondition.InitCondtition();
		oCondition.conditionFlag = ViCommConst.INQUIRY_EXTENSION_MOTE_KING;
		oCondition.siteCd = sessionMan.site.siteCd;
		oCondition.newCastDay = 0;
		oCondition.userRank = string.Empty;
		oCondition.random = ViCommConst.FLAG_OFF;
		oCondition.listDisplay = iBridUtil.GetStringValue(pPartsArgs.Query["list"]);
		oCondition.orderAsc = iBridUtil.GetStringValue(pPartsArgs.Query["orderasc"]);
		oCondition.orderDesc = iBridUtil.GetStringValue(pPartsArgs.Query["orderdesc"]);
		oCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
		oCondition.rankingCtlSeq = iBridUtil.GetStringValue(pPartsArgs.Query["ctlseq"]);
		if (oCondition.rankingCtlSeq.Equals(string.Empty)) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				oCondition.rankingCtlSeq = oRankingCtl.GetLastSeq(sessionMan.site.siteCd,ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN);
			}
		}

		using (Man oMan = new Man(sessionObjs)) {
			oTmpDataSet = oMan.GetPageCollection(oCondition,1,pPartsArgs.NeedCount);
		}

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_RANKING);
	}

	private string GetListAspAd(string pTag,string pArgument) {
		DesignPartsArgument pPartsArgs = DesignPartsArgument.Parse(this,pTag,pArgument);
		int iPageCount = 0;
		decimal dRecCount = 0;

		DataSet oTmpDataSet = sessionMan.CreateAspAdUseApi(pPartsArgs.Query,1,pPartsArgs.NeedCount,out iPageCount,out dRecCount);
		;

		if (oTmpDataSet == null)
			return string.Empty;

		return parseContainer.ParseDirect(pPartsArgs.HtmlTemplate,oTmpDataSet,ViCommConst.DATASET_GET_ASP_AD);

	}

	private string GetCountPickup(string pTag,string pArgument) {

		//他のﾃﾞｻﾞｲﾝﾊﾟｰﾂのpArgumentと異なり、条件部分だけが渡される為、htmlと表示件数をﾀﾞﾐｰで入れる
		string sDummyArg = string.Format("dummy,1,{0}",pArgument);
		DesignPartsArgument oPartsArgs = DesignPartsArgument.Parse(this,pTag,sDummyArg);

		string sPickupId = iBridUtil.GetStringValue(oPartsArgs.Query["pickupid"]);
		string sSortType = iBridUtil.GetStringValue(oPartsArgs.Query["sorttype"]);
		string sAttrTypeSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrtypeseq"]);
		string sAttrSeq = iBridUtil.GetStringValue(oPartsArgs.Query["attrseq"]);
		string sRandom = iBridUtil.GetStringValue(oPartsArgs.Query["random"]);


		if (string.IsNullOrEmpty(sPickupId)) {
			throw new ApplicationException("pickupid is empty.");
		}


		string sPickupType = string.Empty;
		using (Pickup oPickup = new Pickup()) {
			sPickupType = oPickup.GetPickupType(sessionObjs.site.siteCd,sPickupId);
		}
		if (string.IsNullOrEmpty(sPickupType)) {
			throw new ApplicationException("pickupid not found.id:" + sPickupId);
		}


		SeekCondition oSeekCondition = new SeekCondition();
		oSeekCondition.InitCondtition();
		oSeekCondition.pickupId = sPickupId;
		oSeekCondition.sortType = sSortType;
		oSeekCondition.directRandom = sRandom.Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;



		decimal dRowCount = 0;
		switch (sPickupType) {
			case ViCommConst.PicupTypes.BBS_MOVIE:
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
				using (CastMovie oCastMovie = new CastMovie(sessionObjs)) {
					oCastMovie.GetPageCount(sessionObjs.site.siteCd,string.Empty,string.Empty,string.Empty,false,string.Empty,sessionMan.actCategorySeq,sAttrTypeSeq,sAttrSeq,oSeekCondition,oPartsArgs.NeedCount,true,out dRowCount);
				}
				break;
			case ViCommConst.PicupTypes.BBS_PIC:
				using (CastPic oCastPic = new CastPic(sessionObjs)) {
					oCastPic.GetPageCount(sessionObjs.site.siteCd,string.Empty,string.Empty,false,ViCommConst.ATTACHED_BBS.ToString(),sessionMan.actCategorySeq,sAttrTypeSeq,sAttrSeq,oSeekCondition,oPartsArgs.NeedCount,out dRowCount);
				}
				break;
			case ViCommConst.PicupTypes.PROFILE_PIC:
				using (CastPic oCastPic = new CastPic(sessionObjs)) {
					oCastPic.GetPageCount(sessionObjs.site.siteCd,string.Empty,string.Empty,false,ViCommConst.ATTACHED_PROFILE.ToString(),sessionMan.actCategorySeq,sAttrTypeSeq,sAttrSeq,oSeekCondition,oPartsArgs.NeedCount,out dRowCount);
				}
				break;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				oSeekCondition.siteCd = sessionObjs.site.siteCd;
				oSeekCondition.userSeq = sessionMan.userMan.userSeq;
				oSeekCondition.userCharNo = sessionMan.userMan.userCharNo;
				oSeekCondition.conditionFlag = ViCommConst.INQUIRY_RECOMMEND;
				oSeekCondition.newCastDay = 0;
				oSeekCondition.userRank = "";

				if (sessionMan.currentCategoryIndex >= 0) {
					oSeekCondition.categorySeq = ((ActCategory)sessionMan.categoryList[sessionMan.currentCategoryIndex]).actCategorySeq;
				} else {
					oSeekCondition.categorySeq = "";
				}

				using (Cast oCast = new Cast(sessionObjs)) {
					oCast.GetPageCount(oSeekCondition,oPartsArgs.NeedCount,out dRowCount);
				}
				break;

		}

		return dRowCount.ToString();
	}


	private bool IsNewMember() {
		bool bRet = false;
		string sNewDay = DateTime.Now.AddDays(-1 * sessionMan.site.manNewFaceDays).ToString("yyyy/MM/dd");

		if (sessionMan.userMan.registDay.Length >= 10) {
			if (sNewDay.CompareTo(sessionMan.userMan.registDay.Substring(0,10)) <= 0) {
				bRet = true;
			}
		}
		return bRet;
	}

	private string GetMovieCount(string pTag,string pAttachedType) {
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		string sBbsNonPublishFlag = ViCommConst.FLAG_OFF_STR;

		if (pAttachedType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			if (GetValue(pTag,"BBS_NON_PUBLISH_FLAG",out sBbsNonPublishFlag)) {
				if (sBbsNonPublishFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					return "0";
				}
			}
		}

		if (GetValue(pTag,"USER_SEQ",out sUserSeq) && GetValue(pTag,"USER_CHAR_NO",out sUserCharNo)) {
			using (CastMovie oCastMovie = new CastMovie()) {
				return oCastMovie.GetMovieCount(sessionMan.site.siteCd,sUserSeq,sUserCharNo,pAttachedType).ToString();
			}
		}
		return string.Empty;
	}

	private string GetPicCount(string pTag,string pAttachedType) {
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		string sBbsNonPublishFlag = ViCommConst.FLAG_OFF_STR;

		if (pAttachedType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			if (GetValue(pTag,"BBS_NON_PUBLISH_FLAG",out sBbsNonPublishFlag)) {
				if (sBbsNonPublishFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					return "0";
				}
			}
		}

		if (GetValue(pTag,"USER_SEQ",out sUserSeq) && GetValue(pTag,"USER_CHAR_NO",out sUserCharNo)) {
			using (CastPic oCastPic = new CastPic()) {
				return oCastPic.GetPicCount(sessionMan.site.siteCd,sUserSeq,sUserCharNo,pAttachedType).ToString();
			}
		}
		return string.Empty;
	}

	private string GetBbsWriteCount(string pTag) {
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		string sBbsNonPublishFlag = ViCommConst.FLAG_OFF_STR;

		if (GetValue(pTag,"BBS_NON_PUBLISH_FLAG",out sBbsNonPublishFlag)) {
			if (sBbsNonPublishFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				return "0";
			}
		}

		if (GetValue(pTag,"USER_SEQ",out sUserSeq) && GetValue(pTag,"USER_CHAR_NO",out sUserCharNo)) {
			using (CastBbs oCastBbs = new CastBbs()) {
				return oCastBbs.GetWriteCount(sessionMan.site.siteCd,sUserSeq,sUserCharNo).ToString();
			}
		}
		return string.Empty;
	}

	private string GetPicCountByAttr(string pTag,string pAttr) {
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (CastPic oCastPic = new CastPic(sessionObjs)) {
				return oCastPic.GetPicCount(sessionMan.site.siteCd,sValues[0],sValues[1],ViCommConst.ATTACHED_BBS.ToString(),pAttr,ViCommConst.FLAG_OFF_STR).ToString();
			}
		}
		return string.Empty;
	}

	private string GetMovieCountByAttr(string pTag,string pAttr) {
		string[] sValues;
		if (GetValues(pTag,"USER_SEQ,USER_CHAR_NO",out sValues)) {
			using (CastMovie oCastMovie = new CastMovie(sessionObjs)) {
				return oCastMovie.GetMovieCount(sessionMan.site.siteCd,sValues[0],sValues[1],ViCommConst.ATTACHED_BBS.ToString(),pAttr,ViCommConst.FLAG_OFF_STR).ToString();
			}
		}
		return string.Empty;
	}

	private void AddOmikujiPoint() {
		using (Omikuji oOmikuji = new Omikuji()) {
			oOmikuji.AddOmikujiPoint(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN,out sessionMan.userMan.omikujiNo,out sessionMan.userMan.getOmikujiPoint);
			sessionMan.userMan.omikujiPoint += sessionMan.userMan.getOmikujiPoint;
			if (sessionMan.userMan.omikujiPoint >= sessionMan.site.pointPrice) {
				sessionMan.userMan.balPoint += sessionMan.userMan.omikujiPoint / sessionMan.site.pointPrice;
				sessionMan.userMan.omikujiPoint = sessionMan.userMan.omikujiPoint % sessionMan.site.pointPrice;
			}
		}
	}

	private string GetCurrentReductionPoint() {
		using (PointReduction oPointReduction = new PointReduction()) {
			int? iReductionPoint = oPointReduction.GetCurrentReductionPoint(this.sessionMan.site.siteCd,this.sessionMan.userMan.reductionReceiptSumAmt);

			if (iReductionPoint == null) {
				return string.Empty;
			}

			if (this.sessionMan.userMan.reductionPointType == ViCommConst.PointReductionType.InputValue) {
				return iReductionPoint.ToString();
			} else {
				double? dTmp = (this.sessionMan.userMan.reductionReceiptSumAmt / this.sessionMan.site.pointPrice) * (iReductionPoint / 100.0);
				return dTmp.ToString();
			}
		}
	}

	private string GetNextReductionPoint() {
		using (PointReduction oPointReduction = new PointReduction()) {
			DataSet oDataSet = oPointReduction.GetNextReductionInfo(this.sessionMan.site.siteCd,this.sessionMan.userMan.reductionReceiptSumAmt);

			if (oDataSet == null) {
				return string.Empty;
			}
			int iReductionPoint = int.Parse(oDataSet.Tables[0].Rows[0]["REDUCTION_POINT"].ToString());
			if (this.sessionMan.userMan.reductionPointType == ViCommConst.PointReductionType.InputValue) {
				return iReductionPoint.ToString();
			} else {
				double? dTmp = (this.sessionMan.userMan.reductionReceiptSumAmt / this.sessionMan.site.pointPrice) * (iReductionPoint / 100.0);
				return dTmp.ToString();
			}
		}
	}

	private string GetNextReductionReceiptSumAmt() {
		using (PointReduction oPointReduction = new PointReduction()) {
			DataSet oDataSet = oPointReduction.GetNextReductionInfo(this.sessionMan.site.siteCd,this.sessionMan.userMan.reductionReceiptSumAmt);

			if (oDataSet == null) {
				return string.Empty;
			}

			return oDataSet.Tables[0].Rows[0]["RECEIPT_SUM_AMT"].ToString();
		}
	}

	private string GetMovieSiteUrl() {
		string sMovieSiteUrl = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(sessionMan.site.siteCd,"MOVIE_SITE_URL",ref sMovieSiteUrl);
		}
		return string.Format("{0}?memberid={1}&sid={2}&sex={3}&guid=ON",sMovieSiteUrl,sessionMan.userMan.ifKey,sessionMan.sessionId,sessionMan.sexCd);
	}

	private string GetLastLoginMin(string pTag) {
		DateTime dtLastLoginDate;
		GetValue(pTag,"LAST_ACTION_DATE",out dtLastLoginDate);
		TimeSpan tsLastLoginMin = DateTime.Now - dtLastLoginDate;
		if (tsLastLoginMin.TotalMinutes < 0) {
			//DBｻｰﾊﾞとWEBｻｰﾊﾞの時間差を考慮 
			return "0";
		} else {
			return tsLastLoginMin.TotalMinutes.ToString("0");
		}
	}

	private string GetCrosmileAppRunIntent() {
		if (sessionMan.userMan.useCrosmileFlag == 0) {
			return string.Empty;
		}

		int min = 0;
		using (Recording oRecording = new Recording()){
			min = oRecording.GetWaitingEndMin(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo);
		}

		if (min > 0) {
			return string.Format("{0}/Home/?waitingTime={1}",
						iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]),
						min
					);
		} else {
			return string.Empty;
		}
	}

	private string GetPartnerCrosmileFlag(string pTag) {
		string sUserSeq = string.Empty;
		if (GetValue(pTag,"USER_SEQ",out sUserSeq)) {
			using (User oUser = new User()) {
				if (oUser.GetOne(sUserSeq,ViCommConst.OPERATOR)) {
					return oUser.useCrosmileFlag;
				}
			}
		}
		return string.Empty;
	}

	private void CompliteTrackingReport() {
		if (sessionMan.userMan.registCarrierCd.Equals(ViCommConst.ANDROID) || sessionMan.userMan.registCarrierCd.Equals(ViCommConst.IPHONE)) {
			if ((sessionMan.userMan.telAttestedFlag == ViCommConst.FLAG_ON || sessionMan.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY)) && sessionMan.userMan.noReportAffiliateFlag == ViCommConst.FLAG_ON && !sessionMan.adCd.Equals("")) {
				using (TempRegist oTemp = new TempRegist()) {
					if (oTemp.GetOneByUserSeq(sessionMan.userMan.userSeq)) {
						sessionMan.userMan.noReportAffiliateFlag = 0;
						oTemp.CompliteTrackingReport(sessionMan.userMan.userSeq);
					}
				}
			}
		}
	}

	private string GetMileageSettleId(string pTag,string pArgument) {
		string sValue = "27";
		string sLastPointUsedDate = "1900/01/01 00:00:00";

		if (this.IsSameCarrier(ViCommConst.DOCOMO)) {
			if (sessionMan.userMan.totalReceiptAmt < 5000) {
				if (sessionMan.userMan.totalReceiptAmt >= 3000) {
					sValue = "95";
				}
			} else {
				if (!string.IsNullOrEmpty(sessionMan.userMan.lastPointUsedDate)) {
					sLastPointUsedDate = sessionMan.userMan.lastPointUsedDate;
				}

				if (DateTime.Compare(DateTime.Parse(sLastPointUsedDate),DateTime.Now.AddDays(-7)) > 0) {
					if (!this.IsNewMember()) {
						sValue = "97";
					}
				} else {
					sValue = "96";
				}
			}
		}

		return sValue;
	}

	private string GetSetupFreeDialUrl(string pTag,string pArgument) {
		string sValue = string.Empty;
		UrlBuilder oUrl = new UrlBuilder(Path.GetFileName(sessionMan.requestQuery.Url.AbsolutePath),sessionMan.requestQuery.QueryString);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (sessionMan.userMan.useFreeDialFlag == 1) {
			sValue = string.Format("SetupFreeDial.aspx?setup=0&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		} else {
			sValue = string.Format("SetupFreeDial.aspx?setup=1&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		}

		return sValue;
	}

	private string GetSetupWhitePlanUrl(string pTag,string pArgument) {
		string sValue = string.Empty;
		UrlBuilder oUrl = new UrlBuilder(Path.GetFileName(sessionMan.requestQuery.Url.AbsolutePath),sessionMan.requestQuery.QueryString);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (sessionMan.userMan.useWhitePlanFlag == 1) {
			sValue = string.Format("SetupWhitePlan.aspx?setup=0&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		} else {
			sValue = string.Format("SetupWhitePlan.aspx?setup=1&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		}

		return sValue;
	}

	private string GetSetupCrosmileUrl(string pTag,string pArgument) {
		string sValue = string.Empty;
		UrlBuilder oUrl = new UrlBuilder(Path.GetFileName(sessionMan.requestQuery.Url.AbsolutePath),sessionMan.requestQuery.QueryString);
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (sessionMan.userMan.useCrosmileFlag == 1) {
			sValue = string.Format("SetupTalkApp.aspx?setup=0&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		} else {
			sValue = string.Format("SetupTalkApp.aspx?setup=1&backurl={0}",HttpUtility.UrlEncode(oUrl.ToString(),enc));
		}

		return sValue;
	}

	private string GetSetupVoiceappUrl(string pTag,string pArgument) {
		string sValue = string.Empty;
		string sBackUrl = string.Empty;

		if (sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
			if (sessionMan.currentAspx.Equals("ModifyUserTel.aspx")) {
				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["backurl"]))) {
					sBackUrl = iBridUtil.GetStringValue(sessionMan.requestQuery.QueryString["backurl"]);
				}
			}
		}

		if (string.IsNullOrEmpty(sBackUrl)) {
			UrlBuilder oUrl = new UrlBuilder(Path.GetFileName(sessionMan.requestQuery.Url.AbsolutePath),sessionMan.requestQuery.QueryString);
			sBackUrl = oUrl.ToString();
		}

		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (sessionMan.userMan.useVoiceappFlag == 1) {
			sValue = string.Format("SetupVoiceApp.aspx?setup=0&backurl={0}",HttpUtility.UrlEncode(sBackUrl,enc));
		} else {
			sValue = string.Format("SetupVoiceApp.aspx?setup=1&backurl={0}",HttpUtility.UrlEncode(sBackUrl,enc));
		}

		return sValue;
	}

	private string GetPartnerVoiceappFlag(string pTag) {
		string sUserSeq = string.Empty;
		if (GetValue(pTag,"USER_SEQ",out sUserSeq)) {
			using (User oUser = new User()) {
				if (oUser.GetOne(sUserSeq,ViCommConst.OPERATOR)) {
					return oUser.useVoiceappFlag;
				}
			}
		}
		return string.Empty;
	}

	/// <summary>
	/// 退会申請状態を判定する
	/// </summary>
	/// <param name="pWithdrawalStatus"></param>
	/// <returns></returns>
	private bool IsWithdrawal(string pWithdrawalStatus) {
		User oUser = null;
		DataSet ds = null;
		DataRow dr = null;
		string userStatus = string.Empty;
		string existsAccept = string.Empty;

		// すでに取得済みの場合、セッションに保存している値を使う
		if (!string.IsNullOrEmpty(sessionMan.userMan.existAcceptWithdrawalFlag)) {
			userStatus = sessionMan.userMan.userStatus;
			existsAccept = sessionMan.userMan.existAcceptWithdrawalFlag;
		} else {
			oUser = new User();
			ds = oUser.GetWithdrawalStatus(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo);
			if (ds.Tables[0].Rows.Count <= 0) {
				// ユーザ情報が取得できなかった場合
				return false;
			}
			dr = ds.Tables[0].Rows[0];
			userStatus = dr["USER_STATUS"].ToString();
			existsAccept = dr["EXISTS_ACCEPT"].ToString();

			// セッションに保存
			sessionMan.userMan.userStatus = userStatus;
			sessionMan.userMan.existAcceptWithdrawalFlag = existsAccept;
		}

		// 退会処理完了かどうかを判定
		if (pWithdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE)) {
			if (userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)) {
				// 会員状態が退会の場合
				return true;
			}
			// 会員状態が退会以外の場合
			return false;
		}
		// 退会申請中かどうかを判定
		else if (pWithdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_ACCEPT)) {
			if (userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)) {
				// 会員状態が退会の場合
				return false;
			}
			if (existsAccept.Equals(ViCommConst.FLAG_ON_STR)) {
				// 退会申請中の場合
				return true;
			}
			// 退会申請中以外の場合
			return false;
		}
		// 退会申請なし(未遂、継続含む)かどうかを判定
		if (userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)) {
			// 会員状態が退会の場合
			return false;
		}
		if (existsAccept.Equals(ViCommConst.FLAG_ON_STR)) {
			// 退会申請中の場合
			return false;
		}
		return true;
	}
}
