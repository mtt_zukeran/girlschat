﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using MobileLib;
using iBridCommLib;
using ViComm;

public class Communication:DbSession {
	public Communication() {
	}

	public DataSet GetTypeAndDateList(string pSiteCd,string pUserSeq,string pPartnerUserSeq,string pPartnerUserCharNo,string pLikeMeFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	LAST_TALK_DATE,");
		oSqlBuilder.AppendLine("	LAST_TX_MAIL_DATE,");
		oSqlBuilder.AppendLine("	REGIST_FAVORITE_DATE_BY_MAN,");
		oSqlBuilder.AppendLine("	LAST_MARKING_DATE_BY_MAN,");
		oSqlBuilder.AppendLine("	LAST_MAIL_READ_DATE_BY_MAN");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_COMMUNICATION");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pPartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pPartnerUserCharNo));

		DataSet dsSelect = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		DataSet dsTemp = new DataSet();
		DataTable dt = new DataTable();
		dt.Columns.Add("COMM_TYPE",Type.GetType("System.String"));
		dt.Columns.Add("COMM_DATE",Type.GetType("System.String"));
		dsTemp.Tables.Add(dt);

		if (dsSelect.Tables[0].Rows.Count > 0) {
			if (!string.IsNullOrEmpty(dsSelect.Tables[0].Rows[0]["LAST_TALK_DATE"].ToString())) {
				DataRow dr01 = dsTemp.Tables[0].Rows.Add();
				dr01["COMM_TYPE"] = "TALK";
				dr01["COMM_DATE"] = dsSelect.Tables[0].Rows[0]["LAST_TALK_DATE"];
			}

			if (!string.IsNullOrEmpty(dsSelect.Tables[0].Rows[0]["LAST_TX_MAIL_DATE"].ToString())) {
				DataRow dr02 = dsTemp.Tables[0].Rows.Add();
				dr02["COMM_TYPE"] = "TX_MAIL";
				dr02["COMM_DATE"] = dsSelect.Tables[0].Rows[0]["LAST_TX_MAIL_DATE"];
			}

			if (!string.IsNullOrEmpty(dsSelect.Tables[0].Rows[0]["REGIST_FAVORITE_DATE_BY_MAN"].ToString()) && pLikeMeFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				DataRow dr03 = dsTemp.Tables[0].Rows.Add();
				dr03["COMM_TYPE"] = "FAVORITE_BY_MAN";
				dr03["COMM_DATE"] = dsSelect.Tables[0].Rows[0]["REGIST_FAVORITE_DATE_BY_MAN"];
			}

			if (!string.IsNullOrEmpty(dsSelect.Tables[0].Rows[0]["LAST_MARKING_DATE_BY_MAN"].ToString())) {
				DataRow dr04 = dsTemp.Tables[0].Rows.Add();
				dr04["COMM_TYPE"] = "MARKING_BY_MAN";
				dr04["COMM_DATE"] = dsSelect.Tables[0].Rows[0]["LAST_MARKING_DATE_BY_MAN"];
			}

			if (!string.IsNullOrEmpty(dsSelect.Tables[0].Rows[0]["LAST_MAIL_READ_DATE_BY_MAN"].ToString())) {
				DataRow dr05 = dsTemp.Tables[0].Rows.Add();
				dr05["COMM_TYPE"] = "MAIL_READ_BY_MAN";
				dr05["COMM_DATE"] = dsSelect.Tables[0].Rows[0]["LAST_MAIL_READ_DATE_BY_MAN"];
			}
		}

		DataRow[] oRows = dsTemp.Tables[0].Select(string.Empty,"COMM_DATE DESC");
		DataSet dsSorted = dsTemp.Clone();

		foreach (DataRow dr in oRows) {
			dsSorted.Tables[0].ImportRow(dr);
		}

		return dsSorted;
	}

	public DataSet GetOneByUserSeq(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet oDataSet = null;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	TOTAL_TX_MAIL_COUNT								,	");
		oSqlBuilder.AppendLine("	TOTAL_RX_MAIL_COUNT									");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	T_COMMUNICATION										");
		oSqlBuilder.AppendLine("WHERE													");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ				= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pPartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pPartnerUserCharNo));

		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}
}
