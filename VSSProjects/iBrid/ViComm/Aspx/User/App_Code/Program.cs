﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アクセス集計

--	Progaram ID		: Access
--
--  Creation Date	: 2010.10.28
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;


[System.Serializable]
public class Program :DbSession{
		
	public DataSet GetEnableOverrideList(){
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();
			
			System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
			oSqlBuilder.Append(" SELECT                                            ").AppendLine();
			oSqlBuilder.Append("   T_PROGRAM.PROGRAM_ROOT  ,                       ").AppendLine();
			oSqlBuilder.Append("   T_PROGRAM.PROGRAM_ID                            ").AppendLine();
			oSqlBuilder.Append(" FROM                                              ").AppendLine();
			oSqlBuilder.Append("   T_PROGRAM                                       ").AppendLine();
			oSqlBuilder.Append(" WHERE                                             ").AppendLine();
			oSqlBuilder.Append("   T_PROGRAM.ENABLE_OVERRIDE=:ENABLE_OVERRIDE      ").AppendLine();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":ENABLE_OVERRIDE", ViCommConst.FLAG_ON);
			
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(oDs,"T_PROGRAM");
				}
			}			
		}finally{
			conn.Close();
		}
		return oDs;
	}
}
