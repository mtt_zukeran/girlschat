﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls.WebParts;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

[System.Serializable]
public class SeekCondition:IDisposable {
	public string siteCd;
	public string userSeq;
	public string userCharNo;
	public string partnerLoginId;
	public int onlineStatus;
	public string categorySeq;
	public int findManElapasedDays;
	public int newCastDay;
	public int newManDay;
	public ulong conditionFlag;
	public string handleNm;
	public Int64 playMask;
	public string listDisplay;
	public string movie;
	public string waitType;
	public List<string> attrTypeSeq;
	public List<string> attrValue;
	public List<string> groupingFlag;
	public List<string> likeSearchFlag;
	public List<string> notEqualFlag;
	public List<string> rangeFlag;
	public List<string> seqRangeFlag;
	public string ageFrom;
	public string ageTo;
	public string birthDayFrom;
	public string birthDayTo;
	public string comment;
	public string userRank;
	public string pickupId;
	public string pickupStartPubDay;
	public string registDateFrom;
	public string registDateTo;
	public string findFavoriteType;
	public bool hasExtend = false;
	public string orderAsc;
	public string orderDesc;
	public string sortType;
	public int notSendBatchMailFlag;
	public int random = 0;
	public int directRandom = 0;
	public bool? hasCommunication = null;
	public bool withoutBatchMail = false;
	public string rankingCtlSeq;
	public string richinoRank;
	public bool? hasRecvMail = null;
	public string enabledBlog = string.Empty;
	public string hasProfilePic = string.Empty;
	public string lastLoginDate = string.Empty;
	public string sortExType = string.Empty;
	public string talkingType = string.Empty;
	public int monitorEnableFlag = 0;
	public string bingoTermSeq = string.Empty;
	public string hasPic = string.Empty;
	public string screenId = string.Empty;
	public string device = string.Empty;
	public string lastActionDate = string.Empty;
	public string unusedFlag = string.Empty;
	public string rankType = string.Empty;
	public string withoutProf = string.Empty;
	public string reportDay = string.Empty;
	public string subSeq = string.Empty;
	public string bookmarkFlag = string.Empty;
	public string mailSendFlag = string.Empty;
	public string favoritGroupSeq = string.Empty;
	public string mailSendCount = string.Empty;
	public string diaryUserSeq = string.Empty;
	public string diaryUserCharNo = string.Empty;
	public string userAdminFlag = string.Empty;
	public string targetMailMax = string.Empty;
	public string targetFlag = string.Empty;
	public string noTxMailDays = string.Empty;
	public string lastTxMailDays = string.Empty;
	/// <summary>いいねしたもののみを対象にする</summary>
	public string isLikedOnlyFlag = string.Empty;
	/// <summary>やきもち防止条件を含める</summary>
	public string containJealousyFlag = string.Empty;

	public SeekCondition() {
		attrTypeSeq = new List<string>();
		attrValue = new List<string>();
		groupingFlag = new List<string>();
		likeSearchFlag = new List<string>();
		notEqualFlag = new List<string>();
		rangeFlag = new List<string>();
		seqRangeFlag = new List<string>();
		InitCondtition();
	}


	public void InitCondtition() {
		siteCd = "";
		userSeq = "";
		userCharNo = "";
		partnerLoginId = "";
		onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE;
		categorySeq = "";
		findManElapasedDays = 0;
		newCastDay = 0;
		newManDay = 0;
		conditionFlag = 0;
		handleNm = "";
		playMask = 0;
		listDisplay = "";
		movie = "";
		waitType = "";
		attrTypeSeq.Clear();
		attrValue.Clear();
		groupingFlag.Clear();
		likeSearchFlag.Clear();
		notEqualFlag.Clear();
		rangeFlag.Clear();
		seqRangeFlag.Clear();
		ageFrom = "";
		ageTo = "";
		birthDayFrom = "";
		birthDayTo = "";
		comment = "";
		userRank = "";
		pickupId = string.Empty;
		pickupStartPubDay = string.Empty;
		registDateFrom = string.Empty;
		registDateTo = string.Empty;
		findFavoriteType = string.Empty;
		hasExtend = false;
		orderAsc = string.Empty;
		orderDesc = string.Empty;
		sortType = string.Empty;
		notSendBatchMailFlag = 0;
		random = 0;
		directRandom = 0;
		hasCommunication = null;
		withoutBatchMail = false;
		rankingCtlSeq = string.Empty;
		richinoRank = string.Empty;
		hasRecvMail = null;
		enabledBlog = string.Empty;
		hasProfilePic = string.Empty;
		lastLoginDate = string.Empty;
		sortExType = string.Empty;
		talkingType = string.Empty;
		monitorEnableFlag = 0;
		bingoTermSeq = string.Empty;
		hasPic = string.Empty;
		screenId = string.Empty;
		device = string.Empty;
		unusedFlag = string.Empty;
		rankType = string.Empty;
		withoutProf = string.Empty;
		reportDay = string.Empty;
		subSeq = string.Empty;
		bookmarkFlag = string.Empty;
		mailSendFlag = string.Empty;
		favoritGroupSeq = string.Empty;
		mailSendCount = string.Empty;
		diaryUserSeq = string.Empty;
		diaryUserCharNo = string.Empty;
		userAdminFlag = string.Empty;
		targetMailMax = string.Empty;
		targetFlag = string.Empty;
		noTxMailDays = string.Empty;
		lastTxMailDays = string.Empty;
		isLikedOnlyFlag = string.Empty;
		containJealousyFlag = string.Empty;
	}

	public string GetConditionQuery() {
		string sItemQuery = "";
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (onlineStatus != ViCommConst.SeekOnlineStatus.IGNORE) {
			sItemQuery += string.Format("&online={0}",onlineStatus);
		}

		if (!handleNm.Equals("")) {
			sItemQuery += string.Format("&handle={0}",HttpUtility.UrlEncode(handleNm,enc));
		}

		if (playMask != 0) {
			sItemQuery += string.Format("&play={0}",playMask);
		}

		if (newCastDay != 0) {
			sItemQuery += "&newcast=1";
		}
		if (newManDay != 0) {
			sItemQuery += "&newman=1";
		}
		if (movie.Equals("1")) {
			sItemQuery += "&movie=1";
		}
		if (!waitType.Equals("*") && (!waitType.Equals(""))) {
			sItemQuery += "&waittype=" + waitType;
		}
		if (!ageFrom.Equals("")) {
			sItemQuery += "&agefrom=" + ageFrom;
		}
		if (!ageTo.Equals("")) {
			sItemQuery += "&ageto=" + ageTo;
		}
		if (!birthDayFrom.Equals("")) {
			sItemQuery += "&birthfrom=" + birthDayFrom;
		}
		if (!birthDayTo.Equals("")) {
			sItemQuery += "&birthto=" + birthDayTo;
		}
		if (!comment.Equals("")) {
			sItemQuery += "&comment=" + HttpUtility.UrlEncode(comment,enc);
		}
		if (!userRank.Equals("")) {
			sItemQuery += "&userrank=" + userRank;
		}
		if (!registDateFrom.Equals(string.Empty)) {
			sItemQuery += "&registfrom=" + registDateFrom;
		}
		if (!registDateTo.Equals(string.Empty)) {
			sItemQuery += "&registto=" + registDateTo;
		}
		if (!pickupId.Equals(string.Empty)) {
			sItemQuery += "&pickupid=" + pickupId;
		}
		if (!pickupStartPubDay.Equals(string.Empty)) {
			sItemQuery += "&pickupstartday=" + pickupStartPubDay;
		}
		for (int i = 0;i < attrTypeSeq.Count;i++) {
			if ((attrValue[i] != null) && (!attrValue[i].Equals("")) && (!attrValue[i].Equals("*"))) {
				if (i != 0) {
					sItemQuery = sItemQuery + "&";
				}
				sItemQuery = sItemQuery + string.Format("&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}&range{0:D2}={6}&seqrange{0:D2}={7}",i + 1,HttpUtility.UrlEncode(attrValue[i],enc),attrTypeSeq[i],groupingFlag[i],likeSearchFlag[i],notEqualFlag[i],rangeFlag[i],seqRangeFlag[i]);
			}
		}
		if (!orderAsc.Equals(string.Empty)) {
			sItemQuery += "&orderasc=" + orderAsc;
		}
		if (!orderDesc.Equals(string.Empty)) {
			sItemQuery += "&orderdesc=" + orderDesc;
		}
		if (!sortType.Equals(string.Empty)) {
			sItemQuery += "&sorttype=" + sortType;
		}
		if (random == 1) {
			sItemQuery += "&random=1";
		}
		if (!richinoRank.Equals(string.Empty)) {
			sItemQuery += "&richinorank=" + richinoRank;
		}
		if (!hasRecvMail.Equals(string.Empty)) {
			sItemQuery += "&has_recvmail=" + hasRecvMail;
		}
		if (!string.IsNullOrEmpty(enabledBlog)) {
			sItemQuery += "&enabledblog=" + enabledBlog;
		}
		if (!string.IsNullOrEmpty(hasProfilePic)) {
			sItemQuery += "&hasprofpic=" + hasProfilePic;
		}
		if (!string.IsNullOrEmpty(lastLoginDate)) {
			sItemQuery += "&lastlogin=" + HttpUtility.UrlEncode(lastLoginDate,enc);
		}
		if (!string.IsNullOrEmpty(lastActionDate)) {
			sItemQuery += "&lastaction=" + HttpUtility.UrlEncode(lastActionDate,enc);
		}
		if (!string.IsNullOrEmpty(sortExType)) {
			sItemQuery += "&sortex=" + sortExType;
		}
		if (!string.IsNullOrEmpty(bingoTermSeq)) {
			sItemQuery += "&bingotermseq=" + bingoTermSeq;
		}
		if (!string.IsNullOrEmpty(hasPic)) {
			sItemQuery += "&haspic=" + hasPic;
		}
		if (!string.IsNullOrEmpty(reportDay)) {
			sItemQuery += "&reportday=" + reportDay;
		}
		if (!string.IsNullOrEmpty(subSeq)) {
			sItemQuery += "&subseq=" + subSeq;
		}
		if (!string.IsNullOrEmpty(mailSendFlag)) {
			sItemQuery += "&mailsend=" + mailSendFlag;
		}
		if (!string.IsNullOrEmpty(userAdminFlag)) {
			sItemQuery += "&useradmin=" + userAdminFlag;
		}
		if (!string.IsNullOrEmpty(targetMailMax)) {
			sItemQuery += "&target_mail_max=" + targetMailMax;
		}
		if (!string.IsNullOrEmpty(targetFlag)) {
			sItemQuery += "&targetflag=" + targetFlag;
		}
		if (!string.IsNullOrEmpty(noTxMailDays)) {
			sItemQuery += "&notxmaildays=" + noTxMailDays;
		}
		if (!string.IsNullOrEmpty(lastTxMailDays)) {
			sItemQuery += "&lasttxmaildays=" + lastTxMailDays;
		}
		return sItemQuery;
	}

	public virtual void Dispose() {
	}

}


[Serializable]
public abstract class SessionObjs {
	public static SessionObjs Current {
		get {
			if (HttpContext.Current == null || HttpContext.Current.Session == null)
				return null;
			return HttpContext.Current.Session["objs"] as SessionObjs;
		}
	}
	private IDictionary<string,DataSet> objObjAttrCache = new Dictionary<string,DataSet>();
	public IDictionary<string,DataSet> ObjObjAttrCache {
		get {
			return this.objObjAttrCache;
		}
	}

	public abstract SessionObjs GetOriginalObj();
	public bool IsImpersonated {
		get {
			bool bIsImpersonated = false;

			SessionObjs oOriginalObj = this.GetOriginalObj();
			if (oOriginalObj != null) {
				if (!this.logined && oOriginalObj.logined) {
					bIsImpersonated = true;
				}
			}
			return bIsImpersonated;
		}
	}

	public string[] arAdNo;
	public int iAdCnt;

	public bool alreadyWrittenLoginLog = false;
	public bool isNoPc = false;
	public string rankSign = string.Empty;
	public string root;
	public string sysType;
	public string carrier;
	public string browserId;
	public string modelName;
	public string mobileUserAgent;
	public string rowUrl;
	public string mainMenu;
	public string currentAspx;
	public string currentProgramRoot;
	public string currentProgramId;
	public int currentCategoryIndex;
	public int currentViewMask;
	public string auUtn;
	public string pageTitle;
	public string pageKeyword;
	public string pageDescription;
	//	public string cssFileNm;
	public int tableIndex;
	public string currentPageAgentType;
	public string[] htmlDoc;
	public SeekCondition seekCondition;
	public ProductSeekCondition productSeekCondition;
	public SeekConditionBase seekConditionEx;
	public string sessionId;
	public int startRNum;
	public int endRNum;
	public int maxPage;
	public int currentPage;
	public int rowCountOfPage;
	public bool enableViewPageControl;
	public decimal datasetRecCount;
	public decimal totalRowCount;
	public ArrayList categoryList;
	public string adCd;
	public string affiliateCompany;
	public string affiliaterCd;
	public string introducerFriendCd;	//紹介された時の紹介者コード 
	public bool logined;
	public string sexCd;
	public int groupType;
	public string personalAdCd;
	public string currentIModeId;
	public bool isCrawler;
	public bool cookieLess;
	public bool designDebug;
	/// <summary>OpenId</summary>
	public string tempOpenId = null;
	/// <summary>OpenType</summary>
	public string tempOpenIdType = null;
	public UserRank userRank;
	public AdGroup adGroup;
	public Site site;
	public IvpRequest ivpRequest;
	public NGWord ngWord;
	public WebFace webFace;

	[NonSerialized]
	public HttpRequest requestQuery;

	[NonSerialized]
	public HttpResponse responseQuery;

	public ParseHTML parseContainer;
	public string errorMessage;
	public string seekMode;
	public bool valid;
	public Hashtable sqlSyntax;
	public Hashtable sqlHint;

	// For Previous Version
	public string profUserSeq;
	public string profRecNo;
	public string profOnlineStatus;
	public string profCategorySeq;
	public string profNewCastDay;
	public string profConditionFlag;
	public ulong companyMask = 0;

	// SNS OAuth
	public string twitterAccessToken = string.Empty;
	public string twitterAccessTokenSecret = string.Empty;
	public string snsType = string.Empty;
	public string snsId = string.Empty;
	public string snsEmailAddr = string.Empty;
	
	// Page Access Log
	public int pageViewCount = 0;

	public bool adminFlg = false;

	public int? randomNum4Cast = null;
	public bool isFrameVw = false;

	public List<Breadcrumb> breadcrumbList = new List<Breadcrumb>();

	public BingoCard bingoCard;

	public string[] cssFileNmArray = new string[] { };
	public string[] javascriptFileNmArray = new string[] { };
	public string canonical = string.Empty;
	public int protectImageFlag = ViCommConst.FLAG_ON;
	public int emojiToolFlag = ViCommConst.FLAG_ON;
	public int noIndexFlag = ViCommConst.FLAG_OFF;
	public int noFollowFlag = ViCommConst.FLAG_OFF;
	public int noArchiveFlag = ViCommConst.FLAG_OFF;

	public string deviceToken = string.Empty;
	public string deviceUuid = string.Empty;
	public string deviceId = string.Empty;
	public string androidId = string.Empty;
	public string macAddress = string.Empty;
	public string deviceSerialNo = string.Empty;
	public string deviceTel = string.Empty;
	public string gcappVersion = string.Empty;
	
	public string accessMailTemplateNo = string.Empty;
	public string accessTxMailDay = string.Empty;

	#region FOR $RANDAM

	private IDictionary<string,int> _randomDict;

	public void InitRandomDictionary() {
		_randomDict = new Dictionary<string,int>();
	}

	public int GetRandomDictonaryValue(string key,int start,int end) {
		if (!_randomDict.ContainsKey(key)) {
			_randomDict.Add(key,new Random(Environment.TickCount).Next(start,end + 1));
		}
		return _randomDict[key];
	}

	#endregion



	public static string CurrentSexCd {
		get {
			if (HttpContext.Current.Session["sexcd"] == null) {
				HttpContext.Current.Session["sexcd"] = ViCommConst.MAN;
			}
			return (string)HttpContext.Current.Session["sexcd"];
		}
		set {
			HttpContext.Current.Session["sexcd"] = value;
		}
	}

	protected abstract DataSet CreateListDataSet(HttpRequest pRequest,ulong pMode,out int pRNum,out int pLineCnt,out int pPageCnt);
	public abstract string GetUserSeq();

	public SessionObjs() {
		requestQuery = HttpContext.Current.Request;
		responseQuery = HttpContext.Current.Response;
	}

	public SessionObjs(string pHost,string pSessionId,string pCarrierCd,string pBrowserId,string pUserAgent,string pAdCd,NameValueCollection pQuery,bool pIsCrawler,bool pCookieLess) {
		RefreshRandomNum4Cast();
		sessionId = pSessionId;
		carrier = pCarrierCd;
		browserId = pBrowserId;
		isCrawler = pIsCrawler;
		cookieLess = pCookieLess;
		mobileUserAgent = pUserAgent;
		htmlDoc = new string[10];
		seekCondition = new SeekCondition();
		rankSign = iBridUtil.GetStringValue(pQuery["ranksign"]);
		isNoPc = iBridUtil.GetStringValue(pQuery["nopc"]).Equals("1");
		isFrameVw = iBridUtil.GetStringValue(pQuery["framevw"]).Equals("1");
		deviceToken = iBridUtil.GetStringValue(pQuery["token"]);
		Regex rgx = new Regex(@"[\x00-\x1F]",RegexOptions.Compiled);

		string sMailTemplateAccessKey = iBridUtil.GetStringValue(pQuery["tn"]);
		
		if (!string.IsNullOrEmpty(sMailTemplateAccessKey)) {
			string[] sMailAccessArr = sMailTemplateAccessKey.Split('_');
			
			if (sMailAccessArr.Length == 2) {
				if (SysPrograms.Expression(@"\d{4,6}",sMailAccessArr[0])) {
					accessMailTemplateNo = sMailAccessArr[0];
				}
				
				DateTime dtTxMailDate;
				if (DateTime.TryParseExact(sMailAccessArr[1],"yyyyMMdd",System.Globalization.CultureInfo.InvariantCulture,System.Globalization.DateTimeStyles.None,out dtTxMailDate)) {
					accessTxMailDay = dtTxMailDate.ToString("yyyy/MM/dd");
				}
			}
		}
		
		if (!string.IsNullOrEmpty(deviceToken)) {
			if (AEScryptHelper.IsBase64(deviceToken)) {
				if (!AEScryptHelper.IsNotContainUpperChar(deviceToken)) {
					deviceToken = AEScryptHelper.Decrypt(deviceToken);
				}

				deviceToken = rgx.Replace(deviceToken,"");
			}
		}
		
		string sDeviceUCode = iBridUtil.GetStringValue(pQuery["ucode"]);

		if (!string.IsNullOrEmpty(sDeviceUCode)) {
			string sDecryptUCode = AEScryptHelper.Decrypt(sDeviceUCode);
			sDecryptUCode = rgx.Replace(sDecryptUCode,"");
			if (!string.IsNullOrEmpty(sDecryptUCode)) {
				string[] oUCode = sDecryptUCode.Split('|');

				if (carrier.Equals(ViCommConst.IPHONE) && oUCode.Length > 1) {
					deviceToken = oUCode[0];
					deviceUuid = oUCode[1];
				} else if (carrier.Equals(ViCommConst.ANDROID) && oUCode.Length > 6) {
					deviceToken = oUCode[0];
					deviceUuid = oUCode[1];
					deviceId = oUCode[2];
					androidId = oUCode[3];
					deviceTel = oUCode[4];
					deviceSerialNo = oUCode[5];
					macAddress = oUCode[6];
				}
			}
		}

		if (deviceToken.Equals("(null)")) {
			deviceToken = string.Empty;
		}

		errorMessage = "";
		currentCategoryIndex = -1;

		root = ConfigurationManager.AppSettings["Root"].ToString();
		sysType = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["Type"]);
		enableViewPageControl = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableViewPageControl"]).Equals("1");

		site = new Site();

		if (site.GetOneByJobOfferHost(pHost)) {
			if (sysType.Equals("")) {
				sysType = site.jobOfferSiteType;
			}
		} else if (site.GetOneByHost(pHost)) {
			if (sysType.Equals("")) {
				sysType = site.siteType;
			}
		} else {
			throw new ApplicationException("Host name not found " + pHost);
		}

		HttpContext.Current.Session["LinkColor"] = site.colorLink;

		ngWord = null;

		userRank = new UserRank();
		ivpRequest = new IvpRequest();
		webFace = new WebFace();
		adGroup = null;

		categoryList = new ArrayList();
		categoryList.Add(null);
		for (int i = 1;i <= 3;i++) {
			categoryList.Add(new ActCategory(site.siteCd,i));
		}

		adCd = "";
		affiliaterCd = "";
		affiliateCompany = "";

		if (!pAdCd.Equals("")) {
			SetAdInfo(pAdCd);
		} else {
			SetAffiliateInfo(pQuery);
		}

		using (ModelDiscriminant modelDiscriminant = new ModelDiscriminant()) {
			modelName = modelDiscriminant.GetModelName(carrier,pUserAgent);
		}

		sqlSyntax = new Hashtable();
		sqlHint = new Hashtable();

		bingoCard = new BingoCard();
		try {
			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				string[] filelist = System.IO.Directory.GetFiles(site.webPhisicalDir + "\\TEMPLATE","*.SQL",System.IO.SearchOption.TopDirectoryOnly);
				foreach (string sFileNm in filelist) {
					StringBuilder sSql = new StringBuilder();
					using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
						string sLine;
						while ((sLine = sr.ReadLine()) != null) {
							sSql.Append(sLine).AppendLine();
						}
					}
					sqlSyntax.Add(sFileNm.Substring(sFileNm.LastIndexOf("\\") + 1).Replace(".SQL",""),sSql.ToString());
				}

				string[] hintlist = System.IO.Directory.GetFiles(site.webPhisicalDir + "\\TEMPLATE","*.HINT",System.IO.SearchOption.TopDirectoryOnly);
				foreach (string sFileNm in hintlist) {
					StringBuilder sHint = new StringBuilder();
					using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
						string sLine;
						while ((sLine = sr.ReadLine()) != null) {
							sHint.Append(sLine).AppendLine();
						}
					}
					sqlHint.Add(sFileNm.Substring(sFileNm.LastIndexOf("\\") + 1).Replace(".HINT",""),sHint.ToString());
				}
			}
		} catch {
			EventLogWriter.Force("TEMPLATE ACCESS FAILED. ({0})",site.webPhisicalDir);
			throw;
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			string sMask = string.Empty;
			if (oManageCompany.GetValue("COMPANY_MASK",out sMask)) {
				companyMask = ulong.Parse(sMask);
			} else {
				companyMask = 0;
			}
		}

		if (carrier.Equals(ViCommConst.IPHONE) || carrier.Equals(ViCommConst.ANDROID)) {
			Match match = Regex.Match(pUserAgent,@" GirlsChatApp\/([0-9.]+)");

			if (match.Success) {
				gcappVersion = match.Groups[1].Value;
			}
		}
	}

	public void SetAdInfo(string pAdCd) {
		if (!pAdCd.Equals("")) {
			string sAdNm = "";
			using (Ad oAd = new Ad()) {
				if (oAd.IsExist(pAdCd,ref sAdNm)) {
					adCd = pAdCd;
				} else {
					using (ManageCompany oManageCompany = new ManageCompany()) {
						if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_REGIST_TEMP_AD)) {
							oAd.RegistTempAd(site.siteCd,pAdCd);
							adCd = pAdCd;
						}
					}
				}
			}

			if (!adCd.Equals("")) {
				using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
					string sGroupCd;
					if (oSiteAdGroup.GetAdGroupCd(site.siteCd,adCd,out sGroupCd,out groupType)) {
						adGroup = new AdGroup(site.siteCd,sGroupCd);
					}
				}
			}
		}
	}

	public void SetAffiliateInfo(NameValueCollection pQuery) {
		using (Affiliater oAffiliater = new Affiliater()) {
			if (oAffiliater.IsAffiliater(site.siteCd,pQuery,out affiliaterCd,out affiliateCompany,out adCd)) {
			}
		}
		if (!adCd.Equals("")) {
			using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
				string sGroupCd;
				if (oSiteAdGroup.GetAdGroupCd(site.siteCd,adCd,out sGroupCd,out groupType)) {
					adGroup = new AdGroup(site.siteCd,sGroupCd);
				}
			}
		}
	}


	public HtmlFilter InitScreen(Stream pFilter,System.Web.UI.MobileControls.Form pForm,HttpRequest pRequest,StateBag pViewState,string pUserRank,string pAdGroupCd) {
		requestQuery = pRequest;
		if (HttpContext.Current != null) {
			responseQuery = HttpContext.Current.Response;
		}

		string sAspx = "";
		Regex rgx = new Regex(@"(?<page_name>[^\/]+)(?<extension>.aspx)",RegexOptions.Compiled);
		Match rgxMatch = rgx.Match(pRequest.Path);
		if (rgxMatch.Success) {
			string sScreenId = iBridUtil.GetStringValue(pRequest.QueryString["scrid"]);
			sAspx = string.Format("{0}{1}{2}",rgxMatch.Groups["page_name"].Value,sScreenId,rgxMatch.Groups["extension"].Value);
		}

		return InitScreen(pFilter,pForm,pRequest,sAspx,pViewState,pUserRank,pAdGroupCd);
	}

	public void ClearCategory() {
		currentCategoryIndex = -2;
	}

	public HtmlFilter InitScreen(Stream pFilter,System.Web.UI.MobileControls.Form pForm,HttpRequest pRequest,string pCurrentAspx,StateBag pViewState,string pUserRank,string pAdGroupCd) {
		requestQuery = pRequest;

		if (HttpContext.Current != null) {
			responseQuery = HttpContext.Current.Response;
			if (this.logined == false || iBridUtil.GetStringValue(pRequest.QueryString["_no_chace"]).Equals(ViCommConst.FLAG_ON_STR)) {
				responseQuery.Cache.SetNoStore();
				responseQuery.Cache.SetCacheability(HttpCacheability.NoCache);
			}
		}

		string sCategory = iBridUtil.GetStringValue(pRequest.QueryString["category"]);
		string sDoc = iBridUtil.GetStringValue(pRequest.QueryString["doc"]);

		if (currentCategoryIndex != -2) {
			if (!sCategory.Equals("")) {
				currentCategoryIndex = int.Parse(sCategory);
				pViewState["CATEGORY_INDEX"] = sCategory;
			} else {
				if (!iBridUtil.GetStringValue(pViewState["CATEGORY_INDEX"]).ToString().Equals("")) {
					currentCategoryIndex = int.Parse(iBridUtil.GetStringValue(pViewState["CATEGORY_INDEX"]).ToString());
				} else {
					currentCategoryIndex = -1;
				}
			}
		} else {
			currentCategoryIndex = -1;
			pViewState["CATEGORY_INDEX"] = "";
		}

		string sCategorySeq = "";
		if (currentCategoryIndex >= 0) {
			sCategorySeq = ((ActCategory)categoryList[currentCategoryIndex]).actCategorySeq;
		}

		if (logined && !string.IsNullOrEmpty(pAdGroupCd)) {
			string[] sDisabledLoginnedDesignAdCdGroupList = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["DisableLoginnedDesignAdCdGroupList"]).Split(',');
			foreach (string sDisabledLoginnedDesignAdGroupCd in sDisabledLoginnedDesignAdCdGroupList) {
				if (pAdGroupCd.Trim().ToUpper().Equals(sDisabledLoginnedDesignAdGroupCd.Trim().ToUpper())) {
					pAdGroupCd = string.Empty;
					break;
				}
			}
		}



		currentAspx = pCurrentAspx;
		currentProgramRoot = sysType;
		currentProgramId = currentAspx;
		using (UserView oView = new UserView()) {
			oView.GetView(
					site.siteCd,
					sysType,
					currentAspx,
					carrier,
					pAdGroupCd,
					pUserRank,
					sCategorySeq,
					sDoc,
					designDebug,
					out pageTitle,
					out pageKeyword,
					out pageDescription,
					out tableIndex,
					ref this.htmlDoc,
					out currentViewMask,
					out currentPageAgentType,
					out cssFileNmArray,
					out javascriptFileNmArray,
					out canonical,
					out protectImageFlag,
					out emojiToolFlag,
					out noIndexFlag,
					out noFollowFlag,
					out noArchiveFlag,
					ref webFace
			);
		}

		if (!pageTitle.Equals("")) {
			site.pageTitle = pageTitle;
		} else {
			site.pageTitle = site.pageTitleOriginal;
		}

		if (!pageKeyword.Equals("")) {
			site.pageKeyword = pageKeyword;
		} else {
			site.pageKeyword = site.pageKeywordOriginal;
		}

		if (!pageDescription.Equals("")) {
			site.pageDescription = pageDescription;
		} else {
			site.pageDescription = site.pageDescriptionOriginal;
		}

		switch (carrier) {
			case ViCommConst.DOCOMO:
				if (cssFileNmArray.Length > 0) {
					site.cssArrayDocomo = cssFileNmArray;
				} else {
					site.cssArrayDocomo = new string[] { site.cssDocomoOriginal };
				}
				break;
			case ViCommConst.KDDI:
				if (cssFileNmArray.Length > 0) {
					site.cssArrayAu = cssFileNmArray;
				} else {
					site.cssArrayAu = new string[] { site.cssAuOriginal };
				}
				break;
			case ViCommConst.SOFTBANK:
				if (cssFileNmArray.Length > 0) {
					site.cssArraySoftbank = cssFileNmArray;
				} else {
					site.cssArraySoftbank = new string[] { site.cssSoftbankOriginal };
				}
				break;
			case ViCommConst.IPHONE:
				if (cssFileNmArray.Length > 0) {
					site.cssArrayiPhone = cssFileNmArray;
				} else {
					site.cssArrayiPhone = new string[] { site.cssiPhoneOriginal };
				}
				break;
			case ViCommConst.ANDROID:
				if (cssFileNmArray.Length > 0) {
					site.cssArrayAndroid = cssFileNmArray;
				} else {
					site.cssArrayAndroid = new string[] { site.cssAndroidOriginal };
				}
				break;
			case ViCommConst.CARRIER_OTHERS:
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableDisplaySPDesignToPCUser"]).Equals(ViCommConst.FLAG_ON_STR)) {
					if (cssFileNmArray.Length > 0) {
						site.cssArrayAndroid = cssFileNmArray;
					} else {
						site.cssArrayAndroid = new string[] { site.cssAndroidOriginal };
					}
				}
				break;
		}

		ParseHTML oParseViComm = SessionObjs.Current.parseContainer;
		oParseViComm.parseUser.currentTableIdx = tableIndex;
		return new HtmlFilter(pFilter,carrier,oParseViComm);
	}

	public virtual bool ControlList(
		HttpRequest pRequest,
		ulong pMode,
		Form pActiveForm,
		out DataSet pDS
	) {
		string[] sSplitUrl = pRequest.Path.Split('/');
		currentAspx = sSplitUrl[sSplitUrl.Length - 1];

		bool bFind = false;

		totalRowCount = -1;

		pDS = CreateListDataSet(pRequest,pMode,out currentPage,out rowCountOfPage,out maxPage);

		datasetRecCount = pDS.Tables[0].Rows.Count;
		if (totalRowCount == -1) {
			totalRowCount = pDS.Tables[0].Rows.Count;
		}

		seekMode = pMode.ToString();

		if (currentPage > maxPage) {
			currentPage = maxPage;
		}

		ParseHTML oParseViComm = SessionObjs.Current.parseContainer;
		int iTableIdx = oParseViComm.parseUser.currentTableIdx;
		oParseViComm.loopCount[iTableIdx] = pDS.Tables[0].Rows.Count;
		oParseViComm.parseUser.dataTable[iTableIdx] = pDS.Tables[0];

		startRNum = (currentPage - 1) * rowCountOfPage + 1;
		endRNum = currentPage * rowCountOfPage;

		iBMobileLiteralText tagNoDataFound = (iBMobileLiteralText)(pActiveForm.FindControl("tagNoDataFound"));
		iBMobileLiteralText tagList = (iBMobileLiteralText)(pActiveForm.FindControl("tagList"));
		iBMobileLiteralText tagDetail = (iBMobileLiteralText)(pActiveForm.FindControl("tagDetail"));
		string sList = iBridUtil.GetStringValue(pRequest.QueryString["list"]);

		if (sList.Equals("1")) {
			if (tagList != null)
				tagList.Visible = true;

			if (tagDetail != null)
				tagDetail.Visible = false;

		} else {
			if (tagList != null)
				tagList.Visible = false;

			if (tagDetail != null)
				tagDetail.Visible = true;
		}


		if (pDS.Tables[0].Rows.Count > 0) {
			oParseViComm.currentRecPos[iTableIdx] = 0;
			oParseViComm.parseUser.SetDataRow(iTableIdx,oParseViComm.parseUser.dataTable[iTableIdx].Rows[oParseViComm.currentRecPos[iTableIdx]]);
			bFind = true;
			if (tagNoDataFound != null) {
				tagNoDataFound.Visible = false;
			}
		} else {
			oParseViComm.currentRecPos[iTableIdx] = -1;
			oParseViComm.parseUser.SetDataRow(iTableIdx,null);
			bFind = false;

			if (tagNoDataFound != null) {
				tagNoDataFound.Visible = true;
			}
		}

		return bFind;
	}

	public string GetNextPrevUrl(int pPageNo,HttpRequest pRequest,DataTable pTable,bool pIsPrevious) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sUrl;
		string sList = iBridUtil.GetStringValue(pRequest.QueryString["list"]);
		if (sList.Equals("1")) {
			sList = "&list=1";
		} else {
			sList = "";
		}
		sList += string.Format("&lrows={0}&drows={1}&scrid={2}",iBridUtil.GetStringValue(pRequest.QueryString["lrows"]),iBridUtil.GetStringValue(pRequest.QueryString["drows"]),iBridUtil.GetStringValue(pRequest.QueryString["scrid"]));

		// 拡張検索のクエリも引き継ぐ
		foreach (string sKey in pRequest.QueryString) {
			if (string.IsNullOrEmpty(sKey))
				continue;
			if (!sKey.StartsWith("ext_"))
				continue;
			sList += string.Format("&{0}={1}",sKey,HttpUtility.UrlDecode(pRequest.QueryString[sKey]));
		}
		// メール検索のクエリも引き継ぐ
		if (!string.IsNullOrEmpty(pRequest.QueryString["search_key"])) {
			sList += string.Format("&search_key={0}",HttpUtility.UrlDecode(pRequest.QueryString["search_key"]));
		}
		bool bSeekExFlag = false;
		switch (currentAspx) {
			case "Profile.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&site={4}&prevuserseq={5}&prevcharno={6}&order={7}",
									currentAspx,
									currentCategoryIndex,
									pPageNo,
									seekMode,
									site.siteCd,
									pTable.Rows[0]["USER_SEQ"].ToString(),
									pTable.Rows[0]["USER_CHAR_NO"].ToString(),
									pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT);
				sUrl = sUrl + AddUniqueQuery(pTable.Rows[0],pRequest);
				break;

			case "ViewProfileMovie.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&site={4}&prevuserseq={5}&prevcharno={6}&loginid={7}&{8}={9}&userrecno={10}&order={11}",
									currentAspx,
									currentCategoryIndex,
									pPageNo,
									seekMode,
									site.siteCd,
									pTable.Rows[0]["USER_SEQ"].ToString(),
									pTable.Rows[0]["USER_CHAR_NO"].ToString(),
									pTable.Rows[0]["LOGIN_ID"].ToString(),
									site.charNoItemNm,
									pTable.Rows[0]["CRYPT_VALUE"].ToString(),
									iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),
									pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT);
				sUrl = sUrl + AddUniqueQuery(pTable.Rows[0],pRequest);
				break;

			case "ListUserMailBox.aspx":
			case "ListTxMailBox.aspx":
			case "ListMailHistory.aspx":
				//sUrl = string.Format("{0}?&pageno={1}&site={2}&loginid={3}&{4}={5}&withbatch={6}",currentAspx,pPageNo,site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["withbatch"]));
				sUrl = string.Format("{0}?&pageno={1}&site={2}&loginid={3}&{4}={5}&withbatch={6}&read_flag={7}&return_flag={8}&prt_flg={9}&obj_flag={10}&tx_only={11}",currentAspx,pPageNo,site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["withbatch"]),iBridUtil.GetStringValue(pRequest.QueryString["read_flag"]),iBridUtil.GetStringValue(pRequest.QueryString["return_flag"]),iBridUtil.GetStringValue(pRequest.QueryString["prt_flg"]),iBridUtil.GetStringValue(pRequest.QueryString["obj_flag"]),iBridUtil.GetStringValue(pRequest.QueryString["tx_only"]));
				break;

			case "ViewUserMail.aspx":
			case "ViewTxMail.aspx":
			case "ViewMailHistory.aspx":
				sUrl = string.Format("{0}?&pageno={1}&site={2}&loginid={3}&{4}={5}&mailseq={6}&order={7}",currentAspx,pPageNo,site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),pTable.Rows[0]["MAIL_SEQ"].ToString(),pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT);
				break;
			case "ListInfoMailBox.aspx": {
					string sData = iBridUtil.GetStringValue(pRequest.QueryString["data"]);
					string sMailType = iBridUtil.GetStringValue(pRequest.QueryString["mailtype"]);
					sUrl = string.Format("{0}?&pageno={1}&site={2}&data={3}&mailtype={4}&read_flag={5}",currentAspx,pPageNo,site.siteCd,sData,sMailType,iBridUtil.GetStringValue(pRequest.QueryString["read_flag"]));
					break;
				}
			case "ListBlogMailBox.aspx": {
					string sData = iBridUtil.GetStringValue(pRequest.QueryString["data"]);
					string sMailType = iBridUtil.GetStringValue(pRequest.QueryString["mailtype"]);
					sUrl = string.Format("{0}?&pageno={1}&site={2}&data={3}&mailtype={4}&read_flag={5}",currentAspx,pPageNo,site.siteCd,sData,sMailType,iBridUtil.GetStringValue(pRequest.QueryString["read_flag"]));
					break;
				}
			case "ListAdminReport.aspx":
			case "ListCastRefuse.aspx":
			case "ListCastRefuseMe.aspx":
			case "ListManRefuse.aspx":
			case "ListMailMovie.aspx":
			case "ListMailMovieConf.aspx":
			case "ViewMailMovieConf.aspx":
			case "ListBonusLog.aspx":
			case "ListBatchMailLog.aspx":
			case "ViewBatchMailLog.aspx":
			case "ListMessage.aspx":
			case "ListWrittenMessage.aspx":
			case "ViewMessage.aspx":
			case "ViewProfilePicConf.aspx":
			case "ListPaymentHistory.aspx": {
					string sData = iBridUtil.GetStringValue(pRequest.QueryString["data"]);
					string sMailType = iBridUtil.GetStringValue(pRequest.QueryString["mailtype"]);
					string sWithoutHide = iBridUtil.GetStringValue(pRequest.QueryString["withouthide"]);
					string sHideonly = iBridUtil.GetStringValue(pRequest.QueryString["hideonly"]);

					sUrl = string.Format("{0}?&pageno={1}&site={2}&data={3}&mailtype={4}&withouthide={5}&hideonly={6}",currentAspx,pPageNo,site.siteCd,sData,sMailType,sWithoutHide,sHideonly);
					break;
				}
			case "ListProfilePicConf.aspx": {
					string sWithoutHide = iBridUtil.GetStringValue(pRequest.QueryString["withouthide"]);
					string sHideonly = iBridUtil.GetStringValue(pRequest.QueryString["hideonly"]);
					string sAttrTypeSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]);
					string sAttrSeq = iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]);

					sUrl = string.Format("{0}?&pageno={1}&site={2}&withouthide={3}&hideonly={4}&attrtypeseq={5}&attrseq={6}",currentAspx,pPageNo,site.siteCd,sWithoutHide,sHideonly,sAttrTypeSeq,sAttrSeq);
					break;
				}
			case "ListManageBatchMail.aspx":

				sUrl = string.Format("{0}?&pageno={1}",currentAspx,pPageNo);
				break;

			case "ListAllTalkHistory.aspx":
				sUrl = string.Format("{0}?&pageno={1}&site={2}&loginid={3}",currentAspx,pPageNo,site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["loginid"]));
				break;

			case "ListBbsPicConf.aspx":
			case "ViewBbsPicConf.aspx":
			case "ListBbsMovieConf.aspx":
			case "ViewBbsMovieConf.aspx":
			case "ListBbsObjConf.aspx":
			case "ViewBbsObjConf.aspx":
				string sUnAuth = iBridUtil.GetStringValue(pRequest.QueryString["unauth"]);
				sUrl = string.Format("{0}?&pageno={1}&site={2}&attrtypeseq={3}&attrseq={4}&unauth={5}",currentAspx,pPageNo,site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]),sUnAuth);
				break;
			case "ListBbsThread.aspx":
				sUrl = string.Format("{0}?pageno={1}&sort={2}&searchKey={3}&searchadmintype={4}",currentAspx,pPageNo,iBridUtil.GetStringValue(requestQuery.QueryString["sort"]),iBridUtil.GetStringValue(requestQuery.QueryString["searchKey"]),iBridUtil.GetStringValue(requestQuery.QueryString["searchadmintype"]));
				break;
			case "ListBbsRes.aspx":
				sUrl = string.Format("{0}?pageno={1}&bbsthreadseq={2}&sort={3}&searchKey={4}",currentAspx,pPageNo,iBridUtil.GetStringValue(requestQuery.QueryString["bbsthreadseq"]),iBridUtil.GetStringValue(requestQuery.QueryString["sort"]),iBridUtil.GetStringValue(requestQuery.QueryString["searchKey"]));
				break;
			case "ViewBlogMail.aspx":
			case "ViewInfoMail.aspx":
				sUrl = string.Format("{0}?&pageno={1}&site={2}&mailseq={3}&order={4}",currentAspx,pPageNo,site.siteCd,pTable.Rows[0]["MAIL_SEQ"].ToString(),pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT);
				break;
			case "ListSaleMovie.aspx":
			case "ViewSaleMovie.aspx":
				sUrl = string.Format("{0}?&pageno={1}&site={2}&userrecno={3}&sort={4}&loginid={5}&{6}={7}&movieseries={8}",currentAspx,pPageNo,site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),iBridUtil.GetStringValue(pRequest.QueryString["sort"]),iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.siteCd,site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["movieseries"]));
				break;
			case "ViewCastBbs.aspx":
			case "ViewUserBbs.aspx":
			case "ViewCastBbsConf.aspx":
			case "ViewUserBbsConf.aspx":
				sUrl = string.Format("{0}?&pageno={1}&site={2}&userrecno={3}&bbsseq={4}&order={5}&loginid={6}",currentAspx,pPageNo,site.siteCd,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),pTable.Rows[0]["BBS_SEQ"],pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT,iBridUtil.GetStringValue(pRequest.QueryString["loginid"]));
				break;

			case "ViewGallery.aspx":
				sUrl = string.Format("{0}?&pageno={1}&site={2}&userrecno={3}&picseq={4}&order={5}",
							currentAspx,
							pPageNo,
							site.siteCd,
							iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),
							pTable.Rows[0]["PIC_SEQ"],
							pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT
				);
				break;

			case "ListPersonalLongMovie.aspx":
			case "ListPersonalDiary.aspx":
			case "ListPersonalProfileMovie.aspx":
			case "ViewDiary.aspx":
			case "ViewPic.aspx":
			case "ListCastBbs.aspx":
			case "ListUserBbs.aspx":
			case "ListCastBbsConf.aspx":
			case "ListUserBbsConf.aspx":
			case "ListPersonalProfilePic.aspx":
			case "ViewPersonalProfilePic.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}",
				currentAspx,currentCategoryIndex,pPageNo,seekMode,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.siteCd,site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]));
				break;

			case "ViewPersonalProfileMovie.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&order={9}&movieseq={10}",
							currentAspx,
							currentCategoryIndex,
							pPageNo,
							seekMode,
							iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),
							iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),
							site.siteCd,
							site.charNoItemNm,
							iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),
							pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT,
							pTable.Rows[0]["MOVIE_SEQ"].ToString());
				break;

			case "ViewAdminReport.aspx":
				sUrl = string.Format("{0}?pageno={1}&site={2}&order={3}&docseq={4}",
							currentAspx,
							pPageNo,
							site.siteCd,
							pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT,
							pTable.Rows[0]["DOC_SEQ"].ToString());
				break;

			case "ListPersonalBbsPic.aspx":
			case "ViewPersonalBbsPic.aspx":
			case "ListPersonalBbsMovie.aspx":
			case "ViewPersonalBbsMovie.aspx":
			case "ListBbsPic.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}&used={11}&unused={12}&ranktype={13}&bkm={14}",
							currentAspx,currentCategoryIndex,pPageNo,seekMode,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.siteCd,site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]),iBridUtil.GetStringValue(pRequest.QueryString["used"]),iBridUtil.GetStringValue(pRequest.QueryString["unused"]),iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]),iBridUtil.GetStringValue(pRequest.QueryString["bkm"]));
				break;

			case "ViewBbsPic.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}&order={11}&objseq={12}",
							currentAspx,
							currentCategoryIndex,
							pPageNo,
							seekMode,
							iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),
							iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),
							site.siteCd,
							site.charNoItemNm,
							iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),
							iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),
							iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]),
							pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT,
							pTable.Rows[0]["PIC_SEQ"].ToString());
				break;

			case "ViewBbsMovie.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}&order={11}&objseq={12}",
							currentAspx,
							currentCategoryIndex,
							pPageNo,
							seekMode,
							iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),
							iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),
							site.siteCd,
							site.charNoItemNm,
							iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),
							iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),
							iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]),
							pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT,
							pTable.Rows[0]["MOVIE_SEQ"].ToString());
				break;

			case "ListPersonalBbsObj.aspx":
			case "ViewPersonalBbsObj.aspx":
			case "ListBbsObj.aspx":
			case "ViewBbsObj.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}&allbbs={11}&used={12}&unused={13}&ranktype={14}&bkm={15}",
							currentAspx,currentCategoryIndex,pPageNo,seekMode,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.siteCd,site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]),iBridUtil.GetStringValue(pRequest.QueryString["allbbs"]),iBridUtil.GetStringValue(pRequest.QueryString["used"]),iBridUtil.GetStringValue(pRequest.QueryString["unused"]),iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]),iBridUtil.GetStringValue(pRequest.QueryString["bkm"]));
				break;

			case "ListRequestHistory.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&findtype={3}&loginid={4}",GetCallFromAspx(),currentCategoryIndex,pPageNo,iBridUtil.GetStringValue(requestQuery.QueryString["findtype"]),iBridUtil.GetStringValue(requestQuery.QueryString["loginid"]));
				break;
			case "ListPersonalSaleMovie.aspx":
			case "ViewPersonalSaleMovie.aspx": {
					System.Text.StringBuilder oUrlBuilder = new System.Text.StringBuilder();
					oUrlBuilder.AppendFormat("{0}?",currentAspx);
					bool bHasPageNo = false;
					foreach (string sKey in pRequest.QueryString.Keys) {
						if (string.IsNullOrEmpty(sKey))
							continue;
						if (sKey.ToLower().Equals("pageno")) {
							oUrlBuilder.AppendFormat("{0}={1}&",sKey,pPageNo);
							bHasPageNo = true;
						} else {
							oUrlBuilder.AppendFormat("{0}={1}&",sKey,pRequest.QueryString[sKey]);
						}
					}
					if (!bHasPageNo) {
						oUrlBuilder.AppendFormat("pageno={0}",pPageNo);
					}
					sUrl = oUrlBuilder.ToString();
				}
				break;
			case "ListCastRecommend.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}",GetCallFromAspx(),currentCategoryIndex,pPageNo);
				break;
			case "ListAspAd.aspx":
				string sTopic = iBridUtil.GetStringValue(pRequest.QueryString["adtopic"]);
				string sPointFrom = iBridUtil.GetStringValue(pRequest.QueryString["affpointfrom"]);
				string sPointTo = iBridUtil.GetStringValue(pRequest.QueryString["affpointto"]);
				string sCategory = iBridUtil.GetStringValue(pRequest.QueryString["adcategory"]);
				string sFeeFrom = iBridUtil.GetStringValue(pRequest.QueryString["monthlyfeefrom"]);
				string sFeeTo = iBridUtil.GetStringValue(pRequest.QueryString["monthlyfeeto"]);
				string sTitle = iBridUtil.GetStringValue(pRequest.QueryString["aspadtitle"]);
				string sExcludeSettled = iBridUtil.GetStringValue(pRequest.QueryString["excludesettled"]);
				string sOrderByNew = iBridUtil.GetStringValue(pRequest.QueryString["neworder"]);
				string sOrderByType = iBridUtil.GetStringValue(pRequest.QueryString["order"]);

				sUrl = string.Format("{0}?&pageno={1}&site={2}&adtopic={3}&affpointfrom={4}&affpointto={5}&adcategory={6}&monthlyfeefrom={7}&monthlyfeeto={8}&aspadtitle={9}&excludesettled={10}&neworder={11}&order={12}",
							currentAspx,pPageNo,site.siteCd,sTopic,sPointFrom,sPointTo,sCategory,sFeeFrom,sFeeTo,sTitle,sExcludeSettled,sOrderByNew,sOrderByType);
				break;
			case "ListGetAspAd.aspx":
				string sId = iBridUtil.GetStringValue(pRequest.QueryString["settleid"]);
				string sTId = iBridUtil.GetStringValue(pRequest.QueryString["t_id"]);
				string sCtId = iBridUtil.GetStringValue(pRequest.QueryString["ct_id"]);
				string sPtMin = iBridUtil.GetStringValue(pRequest.QueryString["pt_min"]);
				string sPtMax = iBridUtil.GetStringValue(pRequest.QueryString["pt_max"]);
				string sOrder = iBridUtil.GetStringValue(pRequest.QueryString["order"]);
				string sLimit = "99999";
				string sPage = "0";
				string sMbFlg = iBridUtil.GetStringValue(pRequest.QueryString["mb_flg"]);
				string sRcFlg = iBridUtil.GetStringValue(pRequest.QueryString["rc_flg"]);
				string sCustomFlg1 = iBridUtil.GetStringValue(pRequest.QueryString["custom_flg_1"]);
				string sCustomFlg2 = iBridUtil.GetStringValue(pRequest.QueryString["custom_flg_2"]);
				string sCustomFlg3 = iBridUtil.GetStringValue(pRequest.QueryString["custom_flg_3"]);
				string sCustomFlg4 = iBridUtil.GetStringValue(pRequest.QueryString["custom_flg_4"]);

				sUrl = string.Format("{0}?pageno={1}&t_id={2}&ct_id={3}&pt_min={4}&pt_max={5}&order={6}&limit={7}&page={8}&mb_flg={9}&rc_flg={10}&custom_flg_1={11}&custom_flg_2={12}&custom_flg_3={13}&custom_flg_4={14}&settleid={15}",
								currentAspx,pPageNo,sTId,sCtId,sPtMin,sPtMax,sOrder,sLimit,sPage,sMbFlg,sRcFlg,sCustomFlg1,sCustomFlg2,sCustomFlg3,sCustomFlg4,sId);
				break;
			case "ListPickup.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}",
							currentAspx,currentCategoryIndex,pPageNo,seekMode,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.siteCd,site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]));
				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]))) {
					sUrl = sUrl + string.Format("&ranktype={0}",iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]));
				}
				break;
			case "ListProdMov.aspx":
			case "ListProdMovBuyHist.aspx":
			case "ListProdMovFavorite.aspx":
			case "ListProdMovRankingMonthly.aspx":
			case "ListProdMovRankingWeekly.aspx":
			case "ListProdPic.aspx":
			case "ListProdPicBuyHist.aspx":
			case "ListProdPicFavorite.aspx":
			case "ListProdPicRankingMonthly.aspx":
			case "ListProdPicRankingWeekly.aspx":
			case "ListProdAuction.aspx":
			case "ListProdAuctionFavorite.aspx":
			case "ListProdAuctionBidLog.aspx":
			case "ListProdAuctionEntry.aspx":
			case "ListProdAuctionWin.aspx":
			case "ListProdTheme.aspx":
			case "ListProdThemeBuyHist.aspx": {
					UrlBuilder oUrlBuilder = new UrlBuilder(currentAspx,productSeekCondition.GetQuery());
					oUrlBuilder.AddParameter("pageno",pPageNo.ToString());
					oUrlBuilder.AddParameter("seekmode",seekMode);
					sUrl = oUrlBuilder.ToString(true);
					bSeekExFlag = true;
				}
				break;
			case "ListBbsViewHistory.aspx":
				UrlBuilder oUrlBuilder2 = new UrlBuilder(currentAspx);
				oUrlBuilder2.AddParameter("pageno",pPageNo.ToString());
				oUrlBuilder2.AddParameter("seekmode",seekMode);
				oUrlBuilder2.AddParameter("loginid",iBridUtil.GetStringValue(pRequest.QueryString["loginid"]));
				oUrlBuilder2.AddParameter("objseq",iBridUtil.GetStringValue(pRequest.QueryString["objseq"]));
				sUrl = oUrlBuilder2.ToString();
				break;

			case "ListMarkingRanking.aspx": {
					sUrl = string.Format("{0}?pageno={1}&ctlseq={2}&new={3}",GetCallFromAspx(),pPageNo,iBridUtil.GetStringValue(pRequest.QueryString["ctlseq"]),iBridUtil.GetStringValue(pRequest.QueryString["new"]));
				}
				break;

			case "ListGallery.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}&ranktype={11}",
				currentAspx,currentCategoryIndex,pPageNo,seekMode,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.siteCd,site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]),iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]));

				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["hide_box"]))) {
					sUrl = string.Format("{0}&hide_box={1}",sUrl,iBridUtil.GetStringValue(pRequest.QueryString["hide_box"]));
				}
				break;
			case "ListBbsMovie.aspx":
				sUrl = string.Format("{0}?category={1}&pageno={2}&seekmode={3}&userrecno={4}&loginid={5}&site={6}&{7}={8}&attrtypeseq={9}&attrseq={10}&used={11}&unused={12}&ranktype={13}&bkm={14}",
							currentAspx,currentCategoryIndex,pPageNo,seekMode,iBridUtil.GetStringValue(pRequest.QueryString["userrecno"]),iBridUtil.GetStringValue(pRequest.QueryString["loginid"]),site.siteCd,site.charNoItemNm,iBridUtil.GetStringValue(pRequest.QueryString[site.charNoItemNm]),iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]),iBridUtil.GetStringValue(pRequest.QueryString["used"]),iBridUtil.GetStringValue(pRequest.QueryString["unused"]),iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]),iBridUtil.GetStringValue(pRequest.QueryString["bkm"]));

				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(pRequest.QueryString["hide_box"]))) {
					sUrl = string.Format("{0}&hide_box={1}",sUrl,iBridUtil.GetStringValue(pRequest.QueryString["hide_box"]));
				}
				break;
			case "SetFavoritGroup.aspx":
				sUrl = string.Format("SetFavoritGroup.aspx?grpseq={0}&setgrpseq={1}&pageno={2}",iBridUtil.GetStringValue(pRequest.QueryString["grpseq"]),iBridUtil.GetStringValue(pRequest.QueryString["setgrpseq"]),pPageNo);
				break;
			case "ListFavorit.aspx":
				sUrl = string.Format("ListFavorit.aspx?grpseq={0}&pageno={1}",iBridUtil.GetStringValue(pRequest.QueryString["grpseq"]),pPageNo);
				break;
			case "ListFindResult.aspx":
				sUrl = string.Format("ListFindResult.aspx?category={0}&pageno={1}&mailsendcnt={2}",currentCategoryIndex,pPageNo,iBridUtil.GetStringValue(pRequest.QueryString["mailsendcnt"]));
				break;
			case "ListMailUserRx.aspx":
				sUrl = string.Format("ListMailUserRx.aspx?pageno={0}",pPageNo);
				break;
			case "ListMailUserTx.aspx":
				sUrl = string.Format("ListMailUserTx.aspx?pageno={0}",pPageNo);
				break;
			case "ListFanClubStatus.aspx":
				sUrl = string.Format("ListFanClubStatus.aspx?pageno={0}&memberrank={1}&item01={2}&typeseq01={3}",pPageNo,iBridUtil.GetStringValue(pRequest.QueryString["memberrank"]),iBridUtil.GetStringValue(pRequest.QueryString["item01"]),iBridUtil.GetStringValue(pRequest.QueryString["typeseq01"]));
				break;
			case "ListProfileMovieObj.aspx":
				sUrl = string.Format("ListProfileMovieObj.aspx?pageno={0}&ranktype={1}",pPageNo,iBridUtil.GetStringValue(pRequest.QueryString["ranktype"]));
				break;
			case "ListMailPic.aspx":
				sUrl = string.Format("ListMailPic.aspx?pageno={0}&data={1}",pPageNo,iBridUtil.GetStringValue(pRequest.QueryString["data"]));
				break;
			case "ListCastPicMyLike.aspx":
				sUrl = string.Format("{0}?pageno={1}&attrtypeseq={2}&attrseq={3}",currentAspx,pPageNo,iBridUtil.GetStringValue(pRequest.QueryString["attrtypeseq"]),iBridUtil.GetStringValue(pRequest.QueryString["attrseq"]));
				break;
			default:
				if (seekConditionEx != null) {
					UrlBuilder oUrlBuilder = new UrlBuilder(currentAspx,seekConditionEx.GetQuery());
					oUrlBuilder.AddParameter("pageno",pPageNo.ToString());
					oUrlBuilder.AddParameter("seekmode",seekMode);
					oUrlBuilder.AddParameter("order",pIsPrevious ? ViCommConst.ORDER_PREVIOUS : ViCommConst.ORDER_NEXT);
					oUrlBuilder.RemoveAddParameter("sort",HttpUtility.UrlEncode(seekConditionEx.Sort,enc));
					sUrl = oUrlBuilder.ToString();
					bSeekExFlag = true;
				} else {
					sUrl = string.Format("{0}?category={1}&pageno={2}",GetCallFromAspx(),currentCategoryIndex,pPageNo);
				}
				break;
		}
		if (bSeekExFlag) {
			return sUrl + sList;
		}
		return sUrl + seekCondition.GetConditionQuery() + sList;
	}

	private string GetCallFromAspx() {
		string sCallFrom;
		if (seekMode != null) {
			sCallFrom = this.GetCallFromAspx(ulong.Parse(seekMode));
		} else {
			sCallFrom = "ListOnlineUser.aspx";
		}
		return sCallFrom;
	}

	public string GetCallFromAspx(ulong pSeekMode) {
		string sCallFrom;

		switch (pSeekMode) {
			case ViCommConst.INQUIRY_ONLINE:
			case ViCommConst.INQUIRY_CAST_RECOMMEND:
				sCallFrom = "ListOnlineUser.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_ONLINE_TV_PHONE:
				sCallFrom = "ListOnlineTVPhone.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_TV_PHONE:
				sCallFrom = "ListTVPhone.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_MONITOR:
				sCallFrom = "ListTalking.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_NEW_FACE:
				sCallFrom = "ListNewCast.aspx";
				break;
			case ViCommConst.INQUIRY_CAST_PICKUP:
				sCallFrom = "ListPickupCast.aspx";
				break;

			case ViCommConst.INQUIRY_ONLINE_CAST_NEW_FACE:
				sCallFrom = "ListOnlineNewCast.aspx";
				break;

			case ViCommConst.INQUIRY_FAVORIT:
				sCallFrom = "ListFavorit.aspx";
				break;

			case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
				sCallFrom = "ListFavoritAndLikeMe.aspx";
				break;

			case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
				sCallFrom = "ListLoveList.aspx";
				break;

			case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
				sCallFrom = "ListPetitLoveList.aspx";
				break;
			case ViCommConst.INQUIRY_EXTENSION_MOTE_KING:
				sCallFrom = "ListMoteKing.aspx";
				break;

			case ViCommConst.INQUIRY_EXTENSION_MOTE_CON:
				sCallFrom = "ListMoteCon.aspx";
				break;

			case ViCommConst.INQUIRY_LIKE_ME:
				sCallFrom = "ListLikeMe.aspx";
				break;

			case ViCommConst.INQUIRY_TALK_HISTORY:
				sCallFrom = "ListTalkHistory.aspx";
				break;

			case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_GROUP_MAN:
			case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_ALL:
			case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_SUCCESS:
				sCallFrom = "ListRequestHistory.aspx";
				break;

			case ViCommConst.INQUIRY_MARKING:
				sCallFrom = "ListMarking.aspx";
				break;

			case PwViCommConst.INQUIRY_SELF_MARKING:
				sCallFrom = "ListSelfMarking.aspx";
				break;

			case ViCommConst.INQUIRY_ROOM_MONITOR:
				sCallFrom = "ListRoomMonitor.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2:
				sCallFrom = "ListDiaryAccessRankingWeekly2.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_MONTHLY2:
				sCallFrom = "ListDiaryAccessRankingMonthly2.aspx";
				break;

			case ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING:
				sCallFrom = "ListMarkingRanking.aspx";
				break;

			case ViCommConst.INQUIRY_CONDITION:
				sCallFrom = "ListFindResult.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_MOVIE:
				sCallFrom = "ListProfileMovie.aspx";
				break;

			case ViCommConst.INQUIRY_TALK_MOVIE:
				sCallFrom = "ListLongMovie.aspx";
				break;

			case ViCommConst.INQUIRY_TALK_MOVIE_PICKUP:
				sCallFrom = "ListLongMoviePickup.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_DIARY:
				sCallFrom = "ListDiary.aspx";
				break;

			case ViCommConst.INQUIRY_ADMIN_REPORT:
				sCallFrom = "ListAdminReport.aspx";
				break;

			case ViCommConst.INQUIRY_MAN_NEW:
				sCallFrom = currentAspx;
				break;

			case ViCommConst.INQUIRY_MAN_LOGINED:
				sCallFrom = "ListLoginedUser.aspx";
				break;

			case ViCommConst.INQUIRY_MAN_LOGINED_NEW:
				sCallFrom = "ListLoginedNewUser.aspx";
				break;

			case ViCommConst.INQUIRY_CAST_LOGINED:
				sCallFrom = "ListLoginedUser.aspx";
				break;

			case ViCommConst.INQUIRY_FORWARD:
				sCallFrom = "ListForward.aspx";
				break;

			case ViCommConst.INQUIRY_WAITING_MAN:
				sCallFrom = "ListOnlineUser.aspx";
				break;

			case ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE:
				sCallFrom = "ListPersonalSaleMovie.aspx";
				break;

			case ViCommConst.INQUIRY_SALE_MOVIE:
				sCallFrom = "ListSaleMovie.aspx";
				break;

			case ViCommConst.INQUIRY_PERSONAL_SALE_VOICE:
				sCallFrom = "ListPersonalSaleVoice.aspx";
				break;

			case ViCommConst.INQUIRY_SALE_VOICE:
				sCallFrom = "ListSaleVoice.aspx";
				break;

			case ViCommConst.INQUIRY_RECOMMEND:
				sCallFrom = "ListCastRecommend.aspx";
				break;

			case ViCommConst.INQUIRY_BBS_OBJ:
				sCallFrom = "ListBbsObj.aspx";
				break;

			case ViCommConst.INQUIRY_ASP_AD:
				sCallFrom = "ListAspAd.aspx";
				break;

			case ViCommConst.INQUIRY_GET_ASP_AD:
				sCallFrom = "ListGetAspAd.aspx";
				break;
			case ViCommConst.INQUIRY_PICKUP_PIC:
			case ViCommConst.INQUIRY_PICKUP_MOVIE:
				sCallFrom = "ListPickup.aspx";
				break;
			case ViCommConst.INQUIRY_MEMO:
				sCallFrom = "ListMemo.aspx";
				break;
			case ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE:
				sCallFrom = "ListQuickResponse.aspx";
				break;
			case ViCommConst.INQUIRY_MAN_ADD_POINT:
				sCallFrom = "ListManAddPoint.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_MOVIE:
				sCallFrom = "ListProdMov.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_PIC:
				sCallFrom = "ListProdPic.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_MOVIE_FAVORITE:
				sCallFrom = "ListProdMovFavorite.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_PIC_FAVORITE:
				sCallFrom = "ListProdPicFavorite.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_MOVIE_WEELKY_RANKING:
				sCallFrom = "ListProdMovRankingWeekly.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_MOVIE_MONTHLY_RANKING:
				sCallFrom = "ListProdMovRankingMonthly.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_PIC_WEELKY_RANKING:
				sCallFrom = "ListProdPicRankingWeekly.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_PIC_MONTHLY_RANKING:
				sCallFrom = "ListProdPicRankingMonthly.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_AUCTION:
				sCallFrom = "ListProdAuction.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_AUCTION_FAVORITE:
				sCallFrom = "ListProdAuctionFavorite.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_AUCTION_ENTRY:
				sCallFrom = "ListProdAuctionEntry.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_AUCTION_WIN:
				sCallFrom = "ListProdAuctionWin.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_AUCTION_BID_LOG:
				sCallFrom = "ListProdAuctionBidLog.aspx";
				break;
			case ViCommConst.INQUIRY_BBS_VIEW_HISTORY:
				sCallFrom = "ListBbsViewHistory.aspx";
				break;
			case ViCommConst.INQUIRY_PRODUCT_THEME:
				sCallFrom = "ListProdTheme.aspx";
				break;
			case ViCommConst.INQUIRY_DIARY_LIKE:
				sCallFrom = "ListPersonalDiaryLike.aspx";
				break;
			case ViCommConst.INQUIRY_EXTENSION_GAME:
				sCallFrom = "ListGame.aspx";
				break;

			case ViCommConst.INQUIRY_EXTENSION_BEAUTY_CLOCK:
				sCallFrom = "ListBeautyClock.aspx";
				break;
			case ViCommConst.INQUIRY_EXTENSION_BBS_THREAD:
				sCallFrom = "ListBbsThread.aspx";
				break;
			case ViCommConst.INQUIRY_EXTENSION_RICHINO_USER:
				sCallFrom = "ListRichinoUser.aspx";
				break;
			case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
				sCallFrom = "ListBingoEntryWinner.aspx";
				break;
			case PwViCommConst.INQUIRY_CAST_CAN_MAIL_REQUEST:
				sCallFrom = "ListCanMailRequest.aspx";
				break;
			default:
				sCallFrom = "ListOnlineUser.aspx";
				break;
		}
		return sCallFrom;
	}

	public string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl) {
		HttpRequest request = HttpContext.Current.Request;
		string sScheme = request.IsSecureConnection ? "https" : "http";
		return string.Format("{0}://{1}:{2}{3}",sScheme,request.Url.Host,request.Url.Port,relativeUrl);
	}

	public string GetNavigateAbsoluteUrl(string pUrl) {
		string sNavicateUrl = this.GetNavigateUrl(pUrl);
		return ConvertRelativeUrlToAbsoluteUrl(sNavicateUrl);
	}

	public string GetNavigateUrl(string pBaseUrl,string pSessionId,string pUrl) {
		if (cookieLess) {
			return SysPrograms.ChangeQueryToPathInfo(pBaseUrl + "/" + pUrl,true);
		} else {
			return pBaseUrl + "/(S(" + pSessionId + "))/" + pUrl;
		}
	}
	public string GetNavigateUrl(string pUrl) {
		return GetNavigateUrl(string.Concat(this.root,this.sysType),this.sessionId,pUrl);
	}

	public string GetNavigateUrl(string pSessionId,string pUrl) {
		return GetNavigateUrl(string.Concat(this.root,this.sysType),pSessionId,pUrl);
	}

	public string GetNavigateUrlNoSession(string pUrl) {
		return ConfigurationManager.AppSettings["Root"] + "/" + pUrl;
	}

	public string GetNavigateErrorUrl(string pSessionId,string pUrl) {
		if (cookieLess) {
			return SysPrograms.ChangeQueryToPathInfo(ConfigurationManager.AppSettings["Root"] + "/error/" + pUrl,true);
		} else {
			return ConfigurationManager.AppSettings["Root"] + "/error/(S(" + pSessionId + "))/" + pUrl;
		}
	}

	public string AddUniqueQuery(DataRow pRow,HttpRequest pRequest) {
		string sUrl = "";
		ulong iSeekMode;
		if (ulong.TryParse(seekMode,out iSeekMode)) {
			switch (iSeekMode) {
				case ViCommConst.INQUIRY_CAST_DIARY:
					if (pRow.Table.Columns.Contains("DIARY_DAY")) {
						sUrl = string.Format("&conditem={0}&condvalue={1}","DIARY_DAY",pRow["DIARY_DAY"].ToString());
					}
					break;
				case ViCommConst.INQUIRY_MARKING:
					if (pRow.Table.Columns.Contains("MARKING_DAY")) {
						sUrl = string.Format("&conditem={0}&condvalue={1}","MARKING_DAY",pRow["MARKING_DAY"].ToString());
					}
					break;


				case ViCommConst.INQUIRY_BBS_PIC:
				case ViCommConst.INQUIRY_BBS_MOVIE:
				case ViCommConst.INQUIRY_BBS_DOC:
				case ViCommConst.INQUIRY_GALLERY:
				case ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE:
					if (pRow.Table.Columns.Contains("UNIQUE_VALUE")) {
						sUrl = string.Format("&conditem={0}&condvalue={1}","UNIQUE_VALUE",pRow["UNIQUE_VALUE"].ToString());
					}
					break;

				case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_ALL:
				case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_SUCCESS:
					if (pRow.Table.Columns.Contains("UNIQUE_VALUE")) {
						sUrl = string.Format("&conditem={0}&condvalue={1}&partnerid={2}","UNIQUE_VALUE",pRow["UNIQUE_VALUE"].ToString(),iBridUtil.GetStringValue(pRequest.QueryString["partnerid"]));
					}
					break;

				case ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_GROUP_MAN:
					if (pRow.Table.Columns.Contains("UNIQUE_VALUE")) {
						sUrl = string.Format("&conditem={0}&condvalue={1}","UNIQUE_VALUE",pRow["UNIQUE_VALUE"].ToString());
					}
					break;

				case ViCommConst.INQUIRY_SALE_MOVIE:
					if (pRow.Table.Columns.Contains("UNIQUE_VALUE")) {
						sUrl = string.Format("&conditem={0}&condvalue={1}&sort={2}","UNIQUE_VALUE",pRow["UNIQUE_VALUE"].ToString(),iBridUtil.GetStringValue(pRequest.QueryString["sort"]));
					}
					break;

				case ViCommConst.INQUIRY_TALK_HISTORY:
				case ViCommConst.INQUIRY_ALL_TALK_HISTORY:
					if (pRow.Table.Columns.Contains("UNIQUE_VALUE")) {
						sUrl = string.Format("&conditem={0}&condvalue={1}","UNIQUE_VALUE",pRow["UNIQUE_VALUE"].ToString());
					}
					break;
			}
		}
		return sUrl;
	}

	//	public int ChangeToDataSetIndex(int pPreviousTableIdx) {
	//		ParseUser oParseUser = this.parseContainer.parseUser;
	//		int iTableIdx;
	//		if (oParseUser.tableToDatasetDictionary.TryGetValue(pPreviousTableIdx,out iTableIdx)) {
	//			return iTableIdx;
	//		} else {
	//			return -1;
	//		}
	//	}

	protected string GetRowValue(string pFieldNm,int pTableIdx) {
		ParseUser oParseUser = this.parseContainer.parseUser;
		DataRow dr = null;
		if (oParseUser.datasetDictionary.ContainsKey(pTableIdx)) {
			dr = oParseUser.GetDataRow(pTableIdx);
		} else {
			oParseUser.parseErrorList.Add(string.Format("GetRowValue Convert Error Index:{0} Field:{1}",pTableIdx,pFieldNm));
			return "";
		}
		if (dr != null) {
			if (dr.Table.Columns.Contains(pFieldNm)) {
				return dr[pFieldNm].ToString();
			} else {
				oParseUser.parseErrorList.Add(string.Format("GetRowValue Failed Table:{0} Field{1}",oParseUser.datasetDictionary[pTableIdx],pFieldNm));
				return "";
			}
		} else {

			oParseUser.parseErrorList.Add(string.Format("GetRowValue Failed datarow is null Table:{0}",pTableIdx));
			return "";
		}
	}

	/// <summary>
	/// 指定されたﾚｺｰﾄﾞのﾌｨｰﾙﾄﾞ値を返す
	/// </summary>
	/// <param name="pFieldNm">ﾌｨｰﾙﾄﾞ名</param>
	/// <param name="pTableIdx">ﾃｰﾌﾞﾙｲﾝﾃﾞｯｸｽ(新)</param>
	/// <param name="bExist">値が取得できたらTrue ※現在あるGetRowValue(↑)と差別する為に付けた引数です</param>
	/// <returns></returns>
	protected string GetRowValue(string pFieldNm,int pTableIdx,out bool bExist) {
		ParseUser oParseUser = this.parseContainer.parseUser;
		DataRow dr = null;
		bExist = false;
		dr = oParseUser.GetDataRow(pTableIdx);
		if (dr != null) {
			if (dr.Table.Columns.Contains(pFieldNm)) {
				bExist = true;
				return dr[pFieldNm].ToString();
			} else {
				oParseUser.parseErrorList.Add(string.Format("GetRowValue Failed Table:{0} Field{1}",oParseUser.datasetDictionary[pTableIdx],pFieldNm));
				return string.Empty;
			}
		} else {
			oParseUser.parseErrorList.Add(string.Format("GetRowValue Failed datarow is null Table:{0}",pTableIdx));
			return string.Empty;
		}
	}

	protected void SetRowValue(string pFieldNm,string pValue,int pTableIdx) {
		ParseUser oParseUser = this.parseContainer.parseUser;
		DataRow dr = null;
		if (oParseUser.datasetDictionary.ContainsKey(pTableIdx)) {
			dr = oParseUser.GetDataRow(pTableIdx);
		} else {
			oParseUser.parseErrorList.Add(string.Format("SetRowValue Convert Error Index:{0} Field:{1}",pTableIdx,pFieldNm));
			return;
		}
		if (dr != null) {
			if (dr.Table.Columns.Contains(pFieldNm)) {
				dr[pFieldNm] = pValue;
			} else {
				oParseUser.parseErrorList.Add(string.Format("SetRowValue Failed Table:{0} Field{1}",oParseUser.datasetDictionary[pTableIdx],pFieldNm));
			}
		} else {
			oParseUser.parseErrorList.Add(string.Format("SetRowValue Failed datarow is null Table:{0}",pTableIdx));
		}
	}

	protected int GetRowCount(int pTableIdx) {
		int iResult = 0;
		ParseUser oParseUser = this.parseContainer.parseUser;
		if (oParseUser.datasetDictionary.ContainsKey(pTableIdx)) {
			if (oParseUser.dataTable[pTableIdx] != null) {
				iResult = oParseUser.dataTable[pTableIdx].Rows.Count;
			} else {
				oParseUser.parseErrorList.Add(string.Format("GetRowCount dataset is null:{0}",pTableIdx));
			}
		} else {
			oParseUser.parseErrorList.Add(string.Format("GetRowCount Convert Error Index:{0}",pTableIdx));
		}
		return iResult;
	}

	protected int AdjustRecPosition(HttpRequest pRequest,int pRNum,int pPageCnt) {
		string sOrder = iBridUtil.GetStringValue(pRequest.QueryString["order"]);
		if (!string.IsNullOrEmpty(sOrder)) {
			if (sOrder == ViCommConst.ORDER_PREVIOUS) {
				pRNum--;
			} else {
				pRNum++;
			}
		}
		if (pRNum < 1) {
			pRNum = 1;
		} else if (pRNum > pPageCnt) {
			pRNum = pPageCnt;
		}
		return pRNum;
	}

	public int NextRandomNum(int? pCurrent,int pMax) {
		Random oRandom = new Random(Environment.TickCount);
		pCurrent = pCurrent ?? 0;
		int iNewNum = 0;
		do {
			iNewNum = oRandom.Next(0,pMax * 100) % pMax + 1;
		} while (pCurrent == iNewNum);
		return iNewNum;
	}

	public void RefreshRandomNum4Cast() {
		randomNum4Cast = NextRandomNum(randomNum4Cast,ViCommConst.MAX_OF_RANDOM_NUM_4_CAST);
	}

	public void ActivateImpUser(string sSiteCd,string sUserSeq,string sUserCharNo,string pCarrierCd,string pMobileTerminalNm,string pImodeId) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACTIVATE_IMPORT_USER");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sUserCharNo);
			db.ProcedureInParm("pMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,pCarrierCd);
			db.ProcedureInParm("pMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureInParm("pIMODE_ID",DbSession.DbType.VARCHAR2,pImodeId);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public bool IsRegistedCrosmile(string pUri) {
		bool bRegisted = false;

		if (!pUri.Equals(string.Empty)) {
			string sIfUrl = string.Empty;
			using (Sys oSys = new Sys()) {
				oSys.GetValue("CROSMILE_SIPURI_EXIST_IF_URL",out sIfUrl);
			}
			if (!sIfUrl.Equals(string.Empty)) {

				WebRequest req = WebRequest.Create(string.Format(sIfUrl + "?sipuri={0}",pUri));
				req.Timeout = 30000;
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st)) {
					string sRes = sr.ReadToEnd();
					NameValueCollection objCol = HttpUtility.ParseQueryString(sRes);
					sr.Close();
					st.Close();
					string sResult = iBridUtil.GetStringValue(objCol["result"]);
					if (sResult.Equals("0")) {
						bRegisted = true;
					}
				}
			}
		}
		return bRegisted;
	}

	public bool GetCrosmileSipUriByRegistKey(string pRegistKey,out string pSipUri,out string pLastVer) {
		bool bRegisted = false;
		pSipUri = string.Empty;
		pLastVer = string.Empty;

		if (!pRegistKey.Equals(string.Empty)) {
			string sIfUrl = string.Empty;
			using (Sys oSys = new Sys()) {
				oSys.GetValue("CROSMILE_SIPURI_EXIST_IF_URL",out sIfUrl);
			}
			if (!sIfUrl.Equals(string.Empty)) {

				WebRequest req = WebRequest.Create(string.Format(sIfUrl + "?registKey={0}",pRegistKey));
				req.Timeout = 30000;
				WebResponse res = req.GetResponse();

				Stream st = res.GetResponseStream();
				using (StreamReader sr = new StreamReader(st)) {
					string sRes = sr.ReadToEnd();
					NameValueCollection objCol = HttpUtility.ParseQueryString(sRes);
					sr.Close();
					st.Close();
					string sResult = iBridUtil.GetStringValue(objCol["result"]);
					pSipUri = iBridUtil.GetStringValue(objCol["sipUri"]);
					pLastVer = iBridUtil.GetStringValue(objCol["lastVer"]);
					if (sResult.Equals("0")) {
						bRegisted = true;
					}
				}
			}
		}
		return bRegisted;
	}

	public void ModifyUserCrosmileUri(string pSiteCd,string pUserSeq,string pSexCd,string pSipUri,string pOtherRegistKey,string pLastVer) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_USER_CROSMILE_URI");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("pCROSMILE_URI",DbSession.DbType.VARCHAR2,pSipUri);
			db.ProcedureInParm("pCROSMILE_REGIST_KEY",DbSession.DbType.VARCHAR2,pOtherRegistKey);
			db.ProcedureInParm("pCROSMILE_LAST_USED_VERSION",DbSession.DbType.VARCHAR2,pLastVer);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
	public bool RegistCrosmileSipUri(string pSiteCd,string pUserSeq,string pSexCd,string pTel,int pTelAttestedFlag,ref string pOtherRegistKey) {	
		bool bOK = false;
		string sSipUri = string.Empty;
		string sIfUrl = string.Empty;
		
		using (Sys oSys = new Sys()) {
			oSys.GetValue("CROSMILE_SIPURI_GET_IF_URL",out sIfUrl);
		}
		if (!sIfUrl.Equals(string.Empty)) {
			WebRequest req = WebRequest.Create(string.Format("{0}?otherRegistKey={1}&userAgentKey={2}",sIfUrl,pOtherRegistKey,site.ivpSiteCd));
			req.Timeout = 30000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				NameValueCollection objCol = HttpUtility.ParseQueryString(sRes);
				sr.Close();
				st.Close();
				sSipUri = iBridUtil.GetStringValue(objCol["sipuri"]);
				pOtherRegistKey = iBridUtil.GetStringValue(objCol["otherRegistKey"]);
				bOK = true;
				ModifyUserCrosmileUri(pSiteCd,pUserSeq,pSexCd,sSipUri,pOtherRegistKey,string.Empty);
			}
		}

		return bOK;
	}

	public bool IsValidMask(int pCurrentMask,string pIndex) {
		int iIdx,iValue;
		int.TryParse(pIndex,out iIdx);
		if (iIdx > 0) {
			iValue = 1 << (iIdx - 1);
			if ((iValue & pCurrentMask) > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

