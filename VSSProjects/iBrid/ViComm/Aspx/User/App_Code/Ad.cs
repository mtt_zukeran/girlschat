/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: User
--	Title			: 広告コード

--	Progaram ID		: Ad
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

[System.Serializable]
public class Ad:DbSession {

	public string adCd = string.Empty;
	public string adNm = string.Empty;
	public string castRegistReportTiming = string.Empty;
	public string manRegistReportTiming = string.Empty;

	public Ad() {
	}

	public bool GetOne(string pAdCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine(" 	AD_CD						,   ");
		oSqlBuilder.AppendLine(" 	AD_NM                       ,   ");
		oSqlBuilder.AppendLine(" 	CAST_REGIST_REPORT_TIMING   ,   ");
		oSqlBuilder.AppendLine(" 	MAN_REGIST_REPORT_TIMING        ");
		oSqlBuilder.AppendLine(" FROM                               ");
		oSqlBuilder.AppendLine(" 	T_AD                            ");
		oSqlBuilder.AppendLine(" WHERE                              ");
		oSqlBuilder.AppendLine(" 	AD_CD = :AD_CD                  ");

		bool bIsExists = false;
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add("AD_CD",pAdCd);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();
				if (oDataReader.Read()) {
					bIsExists = true;

					adCd = iBridUtil.GetStringValue(oDataReader["AD_CD"]);
					adNm = iBridUtil.GetStringValue(oDataReader["AD_NM"]);
					castRegistReportTiming = iBridUtil.GetStringValue(oDataReader["CAST_REGIST_REPORT_TIMING"]);
					manRegistReportTiming = iBridUtil.GetStringValue(oDataReader["MAN_REGIST_REPORT_TIMING"]);
				}
			}
		} finally {
			this.conn.Close();
		}
		return bIsExists;
	}

	public bool IsExist(string pAdCd,ref string pAdNm) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pAdNm = "";
		try {
			conn = DbConnect("Ad.IsExist");

			using (cmd = CreateSelectCommand("SELECT AD_NM FROM T_AD WHERE AD_CD =:AD_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("AD_CD",pAdCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AD");
					if (ds.Tables["T_AD"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						pAdNm = dr["AD_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void RegistTempAd(string pSiteCd,string pAdCd) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REGIST_TEMP_AD");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,pAdCd);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}

