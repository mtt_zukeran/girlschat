﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールBOX
--	Progaram ID		: MailBox
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応
  2010/07/14  Koyanagi		送受信メール検索対応


-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using iBridCommLib;
using ViComm;
using System.Text;
using System.Configuration;

[System.Serializable]
public class ExParam:Object {
	// 3分以内メール返信ボーナス
	public string mailResBonusSeq;  // 期間SEQ
	public string mailResBonusLimitSec1;  // 1通目の返信期限秒数
	public string mailResBonusLimitSec2;  // 2通目以降の返信期限秒数
	public string mailResBonusMaxNum;  // ボーナス付与最大返信数
	public string mailResBonusUnreceivedDays;  // 男性会員からの未受信日数

	public ExParam() {
		mailResBonusSeq = string.Empty;
		mailResBonusLimitSec1 = string.Empty;
		mailResBonusLimitSec2 = string.Empty;
		mailResBonusMaxNum = string.Empty;
		mailResBonusUnreceivedDays = string.Empty;
	}
}

[System.Serializable]
public class MailBox:DbSession {
	/// <summary>追加パラメータ</summary>
	public ExParam exParam;
	public MailBox() {
		exParam = new ExParam();
	}

	// 大量データでは使用することができない。（パフォーマンスが非常に悪い） 
	// 実装するためにはManやCastと同様に移動が発生した時点でMAIL_SEQのRNUMを取得し
	// RNUM+1,RNUM-1のPageCollectionを実行する。 
	public void GetPrevNextMailSeq(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pSexCd,
		string pMailBoxType,
		string pTxRxType,
		bool pWithBatchMail,
		string pMailSeq,
		out string pPrevMailSeq,
		out string pNextMailSeq) {
		DataSet ds;
		pPrevMailSeq = "";
		pNextMailSeq = "";
		try {
			conn = DbConnect("MailBox.GetPrevNextMailSeq");
			ds = new DataSet();

			string sOrder;

			if (pTxRxType.Equals(ViCommConst.RX)) {
				if (pPartnerLoginId.Equals(string.Empty)) {
					sOrder = "ORDER BY P.RX_SITE_CD,P.RX_USER_SEQ,P.RX_USER_CHAR_NO,P.RX_MAIL_BOX_TYPE,P.WAIT_TX_FLAG,P.RX_DEL_FLAG,P.MAIL_SEQ DESC";
				} else {
					sOrder = "ORDER BY P.RX_SITE_CD,P.RX_USER_SEQ,P.RX_USER_CHAR_NO,P.RX_MAIL_BOX_TYPE,P.WAIT_TX_FLAG,P.RX_DEL_FLAG,P.TX_USER_SEQ,P.TX_USER_CHAR_NO,P.MAIL_SEQ DESC";
				}
			} else if (pTxRxType.Equals(ViCommConst.TX)) {
				if (pPartnerLoginId.Equals(string.Empty)) {
					sOrder = "ORDER BY TX_SITE_CD,TX_USER_SEQ,TX_USER_CHAR_NO,TX_MAIL_BOX_TYPE,BATCH_MAIL_FLAG,TX_DEL_FLAG,MAIL_SEQ DESC";
				} else {
					sOrder = "ORDER BY TX_SITE_CD,TX_USER_SEQ,TX_USER_CHAR_NO,TX_MAIL_BOX_TYPE,BATCH_MAIL_FLAG,TX_DEL_FLAG,RX_USER_SEQ,RX_USER_CHAR_NO,MAIL_SEQ DESC";
				}
			} else {
				sOrder = "ORDER BY MAN_USER_SITE_CD,MAN_USER_SEQ,MAN_USER_CHAR_NO,MAN_MAIL_BOX_TYPE,CAST_SITE_CD,CAST_USER_SEQ,CAST_CHAR_NO,CAST_MAIL_BOX_TYPE,WAIT_TX_FLAG,MAN_MAIL_DEL_FLAG,CAST_MAIL_DEL_FLAG,BATCH_MAIL_FLAG,CREATE_DATE DESC";
			}

			string sViewNm;

			if (pTxRxType.Equals(ViCommConst.RX)) {
				sViewNm = "VW_MAIL_BOX_RX00";
			} else if (pTxRxType.Equals(ViCommConst.TX)) {
				sViewNm = "VW_MAIL_BOX_TX00";
			} else {
				sViewNm = "T_MAIL_LOG";
			}

			StringBuilder sSql = new StringBuilder();
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sSql.Append("SELECT T_CAST_CHARACTER.START_PERFORM_DAY, P.* FROM(").AppendLine();
			}

			sSql.Append("SELECT * FROM(SELECT ").AppendLine();
			if (pTxRxType.Equals(ViCommConst.TXRX)) {
				if (pSexCd.Equals(ViCommConst.MAN)) {
					sSql.Append("	MAN_USER_SITE_CD	AS SITE_CD				,").AppendLine();
					sSql.Append("	MAN_USER_SEQ		AS USER_SEQ				,").AppendLine();
					sSql.Append("	MAN_MAIL_BOX_TYPE	AS MAIL_BOX_TYPE		,").AppendLine();
				} else {
					sSql.Append("	CAST_SITE_CD		AS SITE_CD				,").AppendLine();
					sSql.Append("	CAST_USER_SEQ		AS USER_SEQ				,").AppendLine();
					sSql.Append("	CAST_MAIL_BOX_TYPE	AS MAIL_BOX_TYPE		,").AppendLine();
				}
			} else {
				sSql.Append("	SITE_CD				,").AppendLine();
				sSql.Append("	USER_SEQ			,").AppendLine();
				sSql.Append("	MAIL_BOX_TYPE		,").AppendLine();
			}
			sSql.Append("	MAIL_SEQ			,").AppendLine();
			sSql.Append("	TX_USER_SEQ			,").AppendLine();
			sSql.Append("	TX_USER_CHAR_NO		,").AppendLine();
			sSql.Append("	TX_MAIL_BOX_TYPE	,").AppendLine();
			sSql.Append("	RX_SITE_CD			,").AppendLine();
			sSql.Append("	RX_USER_SEQ			,").AppendLine();
			sSql.Append("	RX_USER_CHAR_NO		,").AppendLine();
			sSql.Append("	RX_MAIL_BOX_TYPE	,").AppendLine();
			sSql.Append("	CAST_SITE_CD		,").AppendLine();
			sSql.Append("	CAST_USER_SEQ		,").AppendLine();
			sSql.Append("	CAST_CHAR_NO		,").AppendLine();
			sSql.Append(string.Format("	LAG(MAIL_SEQ) OVER({0})		AS PREV_SEQ, ",sOrder)).AppendLine();
			sSql.Append(string.Format("	LEAD(MAIL_SEQ) OVER({0})	AS NEXT_SEQ, ",sOrder)).AppendLine();
			sSql.Append(string.Format("RNUM FROM(SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0} P",sViewNm,sOrder)).AppendLine();
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pMailBoxType,pTxRxType,"",pWithBatchMail,string.Empty,pSexCd,false,ref sWhere);

			sSql.Append(sWhere);
			if (pTxRxType.Equals(ViCommConst.RX)) {
				sSql.Append(")WHERE RX_DEL_FLAG = :DEL_FLAG ");
			} else if (pTxRxType.Equals(ViCommConst.TX)) {
				sSql.Append(")WHERE TX_DEL_FLAG = :DEL_FLAG ");
			} else {
				if (pSexCd.Equals(ViCommConst.MAN)) {
					sSql.Append(")WHERE MAN_MAIL_DEL_FLAG = :DEL_FLAG ");
				} else {
					sSql.Append(")WHERE CAST_MAIL_DEL_FLAG = :DEL_FLAG ");
				}
			}
			sSql.Append(sOrder);
			sSql.Append(") WHERE MAIL_SEQ=:MAIL_SEQ ");
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sSql.Append(")P,T_CAST_CHARACTER").AppendLine();
				sSql.Append("WHERE").AppendLine();
				sSql.Append("P.CAST_SITE_CD	 = T_CAST_CHARACTER.SITE_CD		(+) AND	").AppendLine();
				sSql.Append("P.CAST_USER_SEQ = T_CAST_CHARACTER.USER_SEQ	(+) AND	").AppendLine();
				sSql.Append("P.CAST_CHAR_NO	 = T_CAST_CHARACTER.USER_CHAR_NO(+) 	").AppendLine();
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("DEL_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("MAIL_SEQ",pMailSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_BOX01");
				}
			}
		} finally {
			conn.Close();
		}

		if (ds.Tables[0].Rows.Count != 0) {
			DataColumn col;
			col = new DataColumn("TXRX_TYPE",Type.GetType("System.String"));
			ds.Tables[0].Columns.Add(col);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (pUserSeq.Equals(dr["TX_USER_SEQ"].ToString())) {
					dr["TXRX_TYPE"] = ViCommConst.TX;
				} else {
					dr["TXRX_TYPE"] = ViCommConst.RX;
				}
				if (dr["MAIL_SEQ"].ToString().Equals(pMailSeq)) {
					pNextMailSeq = dr["NEXT_SEQ"].ToString();
					pPrevMailSeq = dr["PREV_SEQ"].ToString();
				}
			}
		}
		AppendAttr(pSexCd,pTxRxType,ds);

		return;
	}

	public decimal GetMailCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		string pMailBoxType,
		bool pUnreadOnly
	) {
		decimal iCount;
		GetPageCount(pSiteCd,pUserSeq,pUserCharNo,string.Empty,string.Empty,pSexCd,pMailBoxType,ViCommConst.RX,true,string.Empty,pUnreadOnly,1,out iCount);
		return iCount;
	}

	public int GetPageCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pSexCd,
		string pMailBoxType,
		string pTxRxType,
		bool pWithBatchMail,
		string pMailSearchKey,
		int pRecPerPage,
		out decimal pRecCount
	) {
		return GetPageCount(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pSexCd,pMailBoxType,pTxRxType,pWithBatchMail,pMailSearchKey,false,pRecPerPage,out pRecCount);
	}

	private int GetPageCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pSexCd,
		string pMailBoxType,
		string pTxRxType,
		bool pWithBatchMail,
		string pMailSearchKey,
		bool pUnreadOnly,
		int pRecPerPage,
		out decimal pRecCount
	) {
		int? iReadType = null;
		if (pUnreadOnly) {
			iReadType = ViCommConst.FLAG_OFF;
		}
		return GetPageCount(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pSexCd,pMailBoxType,pTxRxType,pWithBatchMail,pMailSearchKey,iReadType,null,null,null,null,pRecPerPage,out pRecCount);
	}

	public int GetPageCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pSexCd,
		string pMailBoxType,
		string pTxRxType,
		bool pWithBatchMail,
		string pMailSearchKey,
		int? pReadType,
		int? pReturnFlag,
		string pDelProtectFlag,
		string pExistObjFlag,
		string pTxOnlyFlag,
		int pRecPerPage,
		out decimal pRecCount
	) {
		return GetPageCountBase(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pSexCd,pMailBoxType,pTxRxType,pWithBatchMail,pMailSearchKey,pReadType,pReturnFlag,pDelProtectFlag,pExistObjFlag,pTxOnlyFlag,pRecPerPage,out pRecCount);
	}

	private int GetPageCountBase(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pSexCd,
		string pMailBoxType,
		string pTxRxType,
		bool pWithBatchMail,
		string pMailSearchKey,
		int? pReadType,
		int? pReturnFlag,
		string pDelProtectFlag,
		string pExistObjFlag,
		string pTxOnlyFlag,
		int pRecPerPage,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		int iMailSearchLimit = ViCommConst.DEFAULT_MAIL_SEARCH_LIMIT;
		decimal[] iCnt = new decimal[2];

		using (Site oSite = new Site()) {
			string sMailSearcheLimit = null;
			oSite.GetValue(pSiteCd,"MAIL_SEARCH_LIMIT",ref sMailSearcheLimit);
			if (!int.TryParse(sMailSearcheLimit,out iMailSearchLimit)) {
				iMailSearchLimit = ViCommConst.DEFAULT_MAIL_SEARCH_LIMIT;
			}
		}

		conn = DbConnect("MailBox.GetPageCount");

		string sViewNm;
		string sHint = "";
		int iLoop;

		if (pTxRxType.Equals(ViCommConst.TXRX)) {
			iLoop = 1;
		} else {
			iLoop = 1;
		}
		string sTxRxType = pTxRxType;

		for (int j = 0;j < iLoop;j++) {
			iCnt[j] = 0;
			if (sTxRxType.Equals(ViCommConst.RX)) {
				sViewNm = "VW_MAIL_BOX_RX00";
			} else if (pTxRxType.Equals(ViCommConst.TX)) {
				sViewNm = "VW_MAIL_BOX_TX00";
			} else {
				sViewNm = "T_MAIL_LOG";
			}

			StringBuilder sSql = new StringBuilder(string.Format("SELECT {1} COUNT(*) AS ROW_COUNT FROM {0} P",sViewNm,sHint));
			sSql.AppendLine();
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pMailBoxType,pTxRxType,"",pWithBatchMail,pMailSearchKey,pSexCd,pReadType,pReturnFlag,pDelProtectFlag,pExistObjFlag,pTxOnlyFlag,ref sWhere);
			sSql.Append(sWhere);

			try {
				using (cmd = CreateSelectCommand(sSql.ToString(),conn))
				using (da = new OracleDataAdapter(cmd))
				using (ds = new DataSet()) {
					for (int i = 0;i < oParms.Length;i++) {
						cmd.Parameters.Add(oParms[i]);
					}

					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						iCnt[j] = decimal.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			} finally {
				conn.Close();
			}
			pRecCount += iCnt[j];
		}

		if (!string.IsNullOrEmpty(pMailSearchKey)) {
			if (pRecCount > iMailSearchLimit) {
				if (SessionObjs.Current != null) {
					// 表示するメール検索上限超過エラーページを性別で分ける
					string sPage = "DisplayDoc.aspx?doc=";
					if (pSexCd.Equals(ViCommConst.MAN)) {
						sPage += ViCommConst.ERR_FRAME_OVER_SEARCH_LIMIT_MAN;
					} else {
						sPage += ViCommConst.ERR_FRAME_OVER_SEARCH_LIMIT_WOMAN;
					}

					string sRedirectUrl = SessionObjs.Current.GetNavigateUrl(SessionObjs.Current.root + SessionObjs.Current.sysType,SessionObjs.Current.sessionId,sPage);
					HttpContext.Current.Response.Redirect(sRedirectUrl,true);
				}
			}
		}

		iPages = (int)Math.Ceiling(pRecCount / pRecPerPage);
		return iPages;
	}

	public int GetPageCountUserRx(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		int pRecPerPage,
		out decimal pRecCount
	) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,"","",ViCommConst.MAIL_BOX_USER,ViCommConst.RX,"",false,"",pSexCd,null,null,"",null,null,ref sWhere);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(MAIL_SEQ)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		MAX(MAIL_SEQ) AS MAIL_SEQ");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		VW_MAIL_BOX_RX00 P");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("	GROUP BY");
		oSqlBuilder.AppendLine("		TX_SITE_CD,");
		oSqlBuilder.AppendLine("		TX_USER_SEQ,");
		oSqlBuilder.AppendLine("		TX_USER_CHAR_NO");
		oSqlBuilder.AppendLine("	) MAIL");

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParms);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public int GetPageCountUserTx(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		int pRecPerPage,
		out decimal pRecCount
	) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,"","",ViCommConst.MAIL_BOX_USER,ViCommConst.TX,"",false,"",pSexCd,null,null,"",null,null,ref sWhere);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(MAIL_SEQ)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		MAX(MAIL_SEQ) AS MAIL_SEQ");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		VW_MAIL_BOX_TX00 P");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("	GROUP BY");
		oSqlBuilder.AppendLine("		RX_SITE_CD,");
		oSqlBuilder.AppendLine("		RX_USER_SEQ,");
		oSqlBuilder.AppendLine("		RX_USER_CHAR_NO");
		oSqlBuilder.AppendLine("	) MAIL");

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParms);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollectionBySeq(string pMailSeq,string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,string pMailBoxType,string pTxRxType) {
		return GetPageCollectionBase(pSiteCd,pUserSeq,pUserCharNo,"","",pSexCd,pMailBoxType,pTxRxType,true,pMailSeq,"",null,null,true,null,null,null,0,0);
	}

	public DataSet GetPageCollection(
			string pSiteCd,
			string pUserSeq,
			string pUserCharNo,
			string pPartnerLoginId,
			string pPartnerUserCharNo,
			string pSexCd,
			string pMailBoxType,
			string pTxRxType,
			bool pWithBatchMail,
			string pMailSearchKey,
			bool pAppendAttr,
			int pPageNo,
			int pRecPerPage
	) {
		return GetPageCollectionBase(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pSexCd,pMailBoxType,pTxRxType,pWithBatchMail,"",pMailSearchKey,null,null,pAppendAttr,null,null,null,pPageNo,pRecPerPage);
	}

	public DataSet GetPageCollection(
				string pSiteCd,
				string pUserSeq,
				string pUserCharNo,
				string pPartnerLoginId,
				string pPartnerUserCharNo,
				string pSexCd,
				string pMailBoxType,
				string pTxRxType,
				bool pWithBatchMail,
				string pMailSearchKey,
				int? pMailReadType,
				int? pMailReturnFlag,
				bool pAppendAttr,
				string pDelProtectFlag,
				string pExistObjFlag,
				string pTxOnlyFlag,
				int pPageNo,
				int pRecPerPage
		) {
		return GetPageCollectionBase(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pSexCd,pMailBoxType,pTxRxType,pWithBatchMail,"",pMailSearchKey,pMailReadType,pMailReturnFlag,pAppendAttr,pDelProtectFlag,pExistObjFlag,pTxOnlyFlag,pPageNo,pRecPerPage);
	}

	public DataSet GetPageCollectionBase(
			string pSiteCd,
			string pUserSeq,
			string pUserCharNo,
			string pPartnerLoginId,
			string pPartnerUserCharNo,
			string pSexCd,
			string pMailBoxType,
			string pTxRxType,
			bool pWithBatchMail,
			string pMailSeq,
			string pMailSearchKey,
			int? pMailReadType,
			int? pMailReturnFlag,
			bool pAppendAttr,
			string pDelProtectFlag,
			string pExistObjFlag,
			string pTxOnlyFlag,
			int pPageNo,
			int pRecPerPage
		) {
		DataSet ds;
		try {
			conn = DbConnect("MailBox.GetPageCollectionBase");
			ds = new DataSet();

			string sOrder = "";
			if (pTxRxType.Equals(ViCommConst.RX)) {
				if (pPartnerLoginId.Equals(string.Empty)) {
					sOrder = "ORDER BY P.RX_SITE_CD,P.RX_USER_SEQ,P.RX_USER_CHAR_NO,P.RX_MAIL_BOX_TYPE,P.WAIT_TX_FLAG,P.RX_DEL_FLAG,P.MAIL_SEQ DESC";
				} else {
					sOrder = "ORDER BY P.RX_SITE_CD,P.RX_USER_SEQ,P.RX_USER_CHAR_NO,P.RX_MAIL_BOX_TYPE,P.WAIT_TX_FLAG,P.RX_DEL_FLAG,P.TX_USER_SEQ,P.TX_USER_CHAR_NO,P.MAIL_SEQ DESC";
				}
			} else if (pTxRxType.Equals(ViCommConst.TX)) {
				if (pPartnerLoginId.Equals(string.Empty)) {
					sOrder = "ORDER BY P.TX_SITE_CD,P.TX_USER_SEQ,P.TX_USER_CHAR_NO,P.TX_MAIL_BOX_TYPE,P.BATCH_MAIL_FLAG,P.TX_DEL_FLAG,MAIL_SEQ DESC";
				} else {
					sOrder = "ORDER BY P.TX_SITE_CD,P.TX_USER_SEQ,P.TX_USER_CHAR_NO,P.TX_MAIL_BOX_TYPE,P.BATCH_MAIL_FLAG,P.TX_DEL_FLAG,P.RX_USER_SEQ,P.RX_USER_CHAR_NO,P.MAIL_SEQ DESC";
				}
			} else {
				if (string.IsNullOrEmpty(pMailSeq)) {
					sOrder = "ORDER BY P.MAN_USER_SITE_CD,P.MAN_USER_SEQ,P.MAN_USER_CHAR_NO,P.MAN_MAIL_BOX_TYPE,P.CAST_SITE_CD,P.CAST_USER_SEQ,P.CAST_CHAR_NO,P.CAST_MAIL_BOX_TYPE,P.WAIT_TX_FLAG,P.CREATE_DATE DESC";
				} else {
					sOrder = "";
				}
			}

			string sViewNm;
			string sHint = string.Empty;
			if (pTxRxType.Equals(ViCommConst.RX)) {
				sViewNm = "VW_MAIL_BOX_RX00";
			} else if (pTxRxType.Equals(ViCommConst.TX)) {
				sViewNm = "VW_MAIL_BOX_TX00";
			} else {
				sViewNm = "T_MAIL_LOG";
			}
			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT MAIL.*,").AppendLine();
			sSql.Append("	T_REQ_TX_MAIL.MOVIE_SEQ				,").AppendLine();
			sSql.Append("	T_REQ_TX_MAIL.PIC_SEQ				,").AppendLine();
			sSql.Append("	T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE		,").AppendLine();
			sSql.Append("	CASE									").AppendLine();
			sSql.Append("		WHEN (MAIL.TX_SEX_CD = '1') THEN	").AppendLine();
			sSql.Append("			GET_MAN_PHOTO_IMG_PATH	(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ,0)	").AppendLine();
			sSql.Append("		ELSE								").AppendLine();
			sSql.Append("			GET_PHOTO_IMG_PATH		(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ)	").AppendLine();
			sSql.Append("	END		AS OBJ_PHOTO_IMG_PATH			,").AppendLine();
			sSql.Append("	CASE									").AppendLine();
			sSql.Append("		WHEN (MAIL.TX_SEX_CD = '1') THEN	").AppendLine();
			sSql.Append("			GET_MAN_SMALL_PHOTO_IMG_PATH	(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ,0)	").AppendLine();
			sSql.Append("		ELSE								").AppendLine();
			sSql.Append("			GET_SMALL_PHOTO_IMG_PATH		(MAIL.TX_SITE_CD,MAIL.TX_LOGIN_ID,T_REQ_TX_MAIL.PIC_SEQ)	").AppendLine();
			sSql.Append("	END		AS OBJ_SMALL_PHOTO_IMG_PATH		,").AppendLine();

			if (pSexCd.Equals(ViCommConst.MAN)) {
				sSql.Append("	T_ACT_CATEGORY.PRIORITY		AS ACT_CATEGORY_IDX		,").AppendLine();
				sSql.Append("	T_ACT_CATEGORY.ACT_CATEGORY_NM		,").AppendLine();
				sSql.Append("	T_ACT_CATEGORY.ACT_CATEGORY_SEQ		,").AppendLine();
				sSql.Append("	T_EX_CHAR_NO.CRYPT_VALUE			,").AppendLine();
				sSql.Append("	T_CAST_CHARACTER.AGE				,").AppendLine();
				sSql.Append("	T_CAST_CHARACTER.START_PERFORM_DAY	,").AppendLine();
				if (pTxRxType.Equals(ViCommConst.TXRX) == false) {
					sSql.Append("	GET_SMALL_PHOTO_IMG_PATH(MAIL.PARTNER_SITE_CD,MAIL.PARTNER_LOGIN_ID,T_CAST_CHARACTER.PIC_SEQ)	AS SMALL_PHOTO_IMG_PATH	").AppendLine();
				} else {
					sSql.Append("	GET_SMALL_PHOTO_IMG_PATH(MAIL.CAST_SITE_CD,MAIL.CAST_LOGIN_ID,T_CAST_CHARACTER.PIC_SEQ)	AS SMALL_PHOTO_IMG_PATH	").AppendLine();
				}
			} else {
				sSql.Append("	T_USER_MAN_CHARACTER.AGE			,").AppendLine();
				sSql.Append("	T_USER_MAN_CHARACTER.NG_SKULL_COUNT	,").AppendLine();
				if (pTxRxType.Equals(ViCommConst.TXRX) == false) {
					sSql.Append("	GET_MAN_SMALL_PHOTO_IMG_PATH(MAIL.PARTNER_SITE_CD,MAIL.PARTNER_LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0)	AS SMALL_PHOTO_IMG_PATH	").AppendLine();
				} else {
					sSql.Append("	GET_MAN_SMALL_PHOTO_IMG_PATH(MAIL.MAN_USER_SITE_CD,MAIL.MAN_LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0)	AS SMALL_PHOTO_IMG_PATH	").AppendLine();
				}
				// 3分以内メール返信ボーナス対象
				if (!string.IsNullOrEmpty(exParam.mailResBonusSeq)) {
					sSql.Append(",	MRBL.IS_MAIL_RES_BONUS_1 ").AppendLine();
					sSql.Append(",	MRBL.IS_MAIL_RES_BONUS_2 ").AppendLine();
				}
			}

			sSql.Append("FROM(").AppendLine();
			sSql.Append("SELECT * FROM(").AppendLine();
			sSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (").AppendLine();
			sSql.Append("	SELECT " + sHint).AppendLine();
			if (pTxRxType.Equals(ViCommConst.TXRX) == false) {
				sSql.Append("		P.SITE_CD						,").AppendLine();
				sSql.Append("		P.USER_SEQ						,").AppendLine();
				sSql.Append("		P.MAIL_BOX_TYPE					,").AppendLine();
				sSql.Append("		P.HANDLE_NM						,").AppendLine();
				sSql.Append("		P.PARTNER_SITE_CD				,").AppendLine();
				sSql.Append("		P.PARTNER_USER_SEQ				,").AppendLine();
				sSql.Append("		P.PARTNER_USER_CHAR_NO			,").AppendLine();
				sSql.Append("		P.PARTNER_LOGIN_ID				,").AppendLine();
				sSql.Append("		P.PARTNER_LOGIN_ID AS LOGIN_ID	,").AppendLine();
			} else {
				if (pSexCd.Equals(ViCommConst.MAN)) {
					sSql.Append("		P.MAN_USER_SITE_CD		AS SITE_CD				,").AppendLine();
					sSql.Append("		P.MAN_USER_SEQ			AS USER_SEQ				,").AppendLine();
					sSql.Append("		P.MAN_MAIL_BOX_TYPE		AS MAIL_BOX_TYPE		,").AppendLine();
					sSql.Append("		P.CAST_SITE_CD			AS PARTNER_SITE_CD		,").AppendLine();
					sSql.Append("		P.CAST_USER_SEQ			AS PARTNER_USER_SEQ		,").AppendLine();
					sSql.Append("		P.CAST_CHAR_NO			AS PARTNER_USER_CHAR_NO	,").AppendLine();
					sSql.Append("		P.CAST_HANDLE_NM		AS HANDLE_NM			,").AppendLine();
					sSql.Append("		P.CAST_LOGIN_ID			AS PARTNER_LOGIN_ID		,").AppendLine();
					sSql.Append("		P.CAST_LOGIN_ID			AS LOGIN_ID				,").AppendLine();
				} else {
					sSql.Append("		P.CAST_SITE_CD			AS SITE_CD				,").AppendLine();
					sSql.Append("		P.CAST_USER_SEQ			AS USER_SEQ				,").AppendLine();
					sSql.Append("		P.CAST_MAIL_BOX_TYPE	AS MAIL_BOX_TYPE		,").AppendLine();
					sSql.Append("		P.MAN_USER_SITE_CD		AS PARTNER_SITE_CD		,").AppendLine();
					sSql.Append("		P.MAN_USER_SEQ			AS PARTNER_USER_SEQ		,").AppendLine();
					sSql.Append("		P.MAN_USER_CHAR_NO		AS PARTNER_USER_CHAR_NO	,").AppendLine();
					sSql.Append("		P.MAN_HANDLE_NM			AS HANDLE_NM			,").AppendLine();
					sSql.Append("		P.MAN_LOGIN_ID			AS PARTNER_LOGIN_ID		,").AppendLine();
					sSql.Append("		P.MAN_LOGIN_ID			AS LOGIN_ID				,").AppendLine();
				}
				sSql.Append("		P.MAN_USER_SITE_CD	,").AppendLine();
				sSql.Append("		P.MAN_USER_SEQ		,").AppendLine();
				sSql.Append("		P.MAN_USER_CHAR_NO	,").AppendLine();
				sSql.Append("		P.MAN_MAIL_BOX_TYPE	,").AppendLine();
				sSql.Append("		P.MAN_LOGIN_ID		,").AppendLine();
				sSql.Append("		P.CAST_SITE_CD		,").AppendLine();
				sSql.Append("		P.CAST_USER_SEQ		,").AppendLine();
				sSql.Append("		P.CAST_CHAR_NO		,").AppendLine();
				sSql.Append("		P.CAST_LOGIN_ID		,").AppendLine();
			}
			sSql.Append("		P.MAIL_SEQ						,").AppendLine();
			sSql.Append("		P.MAIL_TYPE						,").AppendLine();
			sSql.Append("		P.CREATE_DATE					,").AppendLine();
			sSql.Append("		P.MAIL_TITLE					,").AppendLine();
			sSql.Append("		P.MAIL_DOC1						,").AppendLine();
			sSql.Append("		P.MAIL_DOC2						,").AppendLine();
			sSql.Append("		P.MAIL_DOC3						,").AppendLine();
			sSql.Append("		P.MAIL_DOC4						,").AppendLine();
			sSql.Append("		P.MAIL_DOC5						,").AppendLine();
			sSql.Append("		P.REQUEST_TX_MAIL_SEQ			,").AppendLine();
			sSql.Append("		P.TEXT_MAIL_FLAG				,").AppendLine();
			sSql.Append("		P.READ_FLAG						,").AppendLine();
			sSql.Append("		P.TX_DEL_FLAG					,").AppendLine();
			sSql.Append("		P.RX_DEL_FLAG					,").AppendLine();
			sSql.Append("		P.TX_SITE_CD					,").AppendLine();
			sSql.Append("		P.TX_USER_SEQ					,").AppendLine();
			sSql.Append("		P.TX_USER_CHAR_NO				,").AppendLine();
			sSql.Append("		P.TX_MAIL_BOX_TYPE				,").AppendLine();
			sSql.Append("		P.TX_LOGIN_ID					,").AppendLine();
			sSql.Append("		P.TX_SEX_CD						,").AppendLine();
			sSql.Append("		P.RX_SITE_CD					,").AppendLine();
			sSql.Append("		P.RX_USER_SEQ					,").AppendLine();
			sSql.Append("		P.RX_USER_CHAR_NO				,").AppendLine();
			sSql.Append("		P.RX_MAIL_BOX_TYPE				,").AppendLine();
			sSql.Append("		P.RX_SEX_CD						,").AppendLine();
			sSql.Append("		P.SERVICE_POINT					,").AppendLine();
			sSql.Append("		P.SERVICE_POINT_EFFECTIVE_DATE	,").AppendLine();
			sSql.Append("		P.SERVICE_POINT_TRANSFER_FLAG	,").AppendLine();
			sSql.Append("		P.BATCH_MAIL_LOG_SEQ			,").AppendLine();
			sSql.Append("		P.ATTACHED_OBJ_OPEN_FLAG		,").AppendLine();
			sSql.Append("		P.RETURN_MAIL_FLAG				,").AppendLine();
			sSql.Append("		P.DEL_PROTECT_FLAG				,").AppendLine();
			sSql.Append("		P.OBJ_SEQ						,").AppendLine();
			sSql.Append("		P.MAIL_SEQ AS UNIQUE_VALUE		,").AppendLine();
			sSql.Append("		CASE													").AppendLine();
			sSql.Append("			WHEN QRM.QUICK_RES_SEQ IS NOT NULL					").AppendLine();
			sSql.Append("			THEN 1												").AppendLine();
			sSql.Append("			ELSE 0												").AppendLine();
			sSql.Append("		END	AS QUICK_RES_MAIL_FLAG							,	").AppendLine();
			sSql.Append("		P.PRESENT_MAIL_ITEM_SEQ								,	").AppendLine();
			sSql.Append("		P.PRESENT_RETURN_FLAG									").AppendLine();
			sSql.Append("	FROM " + sViewNm + " P ,T_QUICK_RES_MAIL QRM ").AppendLine();

			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pMailBoxType,pTxRxType,pMailSeq,pWithBatchMail,pMailSearchKey,pSexCd,pMailReadType,pMailReturnFlag,pDelProtectFlag,pExistObjFlag,pTxOnlyFlag,ref sWhere);

			SysPrograms.SqlAppendWhere(" P.MAIL_SEQ = QRM.MAIL_SEQ (+) ",ref sWhere);

			sSql.Append(sWhere).AppendLine();
			sSql.Append(sOrder).AppendLine();

			if (pMailSeq.Equals(string.Empty)) {
				sSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ) MAIL");
			} else {
				sSql.Append(")INNER ");
				sSql.Append(")) MAIL ");
			}
			// 3分以内メール返信ボーナス対象
			string sAddTable = string.Empty;
			string sAddWhere = string.Empty;
			if (!string.IsNullOrEmpty(exParam.mailResBonusSeq)) {
				StringBuilder oSqlBuilder = new StringBuilder();
				oSqlBuilder.AppendLine(", (SELECT");
				oSqlBuilder.AppendLine("	IMRBL3.*,");
				oSqlBuilder.AppendLine("	CASE WHEN IMRBL3.MAIL_RES_NUM = 1");
				oSqlBuilder.Append("		AND IMRBL3.MAN_MAIL_SEND_DATE >= (SYSDATE - NUMTODSINTERVAL(");
				oSqlBuilder.Append(exParam.mailResBonusLimitSec1);
				oSqlBuilder.AppendLine(", 'SECOND'))");
				oSqlBuilder.AppendLine("		AND IMRBL3.CAST_MAIL_SEND_DATE IS NULL THEN 1 ELSE 0 END AS IS_MAIL_RES_BONUS_1,");
				oSqlBuilder.Append("	CASE WHEN IMRBL3.MAIL_RES_NUM BETWEEN 2 AND ");
				oSqlBuilder.Append(exParam.mailResBonusMaxNum);
				oSqlBuilder.Append("		AND IMRBL3.MAN_MAIL_SEND_DATE >= (SYSDATE - NUMTODSINTERVAL(");
				oSqlBuilder.Append(exParam.mailResBonusLimitSec2);
				oSqlBuilder.AppendLine(", 'SECOND'))");
				oSqlBuilder.AppendLine("		AND IMRBL3.CAST_MAIL_SEND_DATE IS NULL THEN 1 ELSE 0 END AS IS_MAIL_RES_BONUS_2");
				oSqlBuilder.AppendLine("FROM");
				oSqlBuilder.AppendLine("	(");
				oSqlBuilder.AppendLine("		SELECT");
				oSqlBuilder.AppendLine("			IMRBL2.*");
				oSqlBuilder.AppendLine("		FROM");
				oSqlBuilder.AppendLine("			(");
				oSqlBuilder.AppendLine("				SELECT");
				oSqlBuilder.AppendLine("					SITE_CD,");
				oSqlBuilder.AppendLine("					MAIL_RES_BONUS_SEQ,");
				oSqlBuilder.AppendLine("					MAX(MAIL_RES_NUM) AS MAIL_RES_NUM,");
				oSqlBuilder.AppendLine("					MAN_USER_SEQ,");
				oSqlBuilder.AppendLine("					CAST_USER_SEQ");
				oSqlBuilder.AppendLine("				FROM");
				oSqlBuilder.AppendLine("					T_MAIL_RES_BONUS_LOG2");
				oSqlBuilder.AppendLine("				WHERE");
				oSqlBuilder.Append("					MAIL_RES_BONUS_SEQ=");
				oSqlBuilder.AppendLine(exParam.mailResBonusSeq);
				oSqlBuilder.Append("					AND SITE_CD='");
				oSqlBuilder.Append(pSiteCd);
				oSqlBuilder.AppendLine("'");
				oSqlBuilder.AppendLine("				GROUP BY");
				oSqlBuilder.AppendLine("					SITE_CD, MAIL_RES_BONUS_SEQ, MAN_USER_SEQ, CAST_USER_SEQ");
				oSqlBuilder.AppendLine("			) IMRBL1,");
				oSqlBuilder.AppendLine("			T_MAIL_RES_BONUS_LOG2 IMRBL2");
				oSqlBuilder.AppendLine("		WHERE");
				oSqlBuilder.AppendLine("			IMRBL1.SITE_CD = IMRBL2.SITE_CD");
				oSqlBuilder.AppendLine("			AND IMRBL1.MAIL_RES_BONUS_SEQ = IMRBL2.MAIL_RES_BONUS_SEQ");
				oSqlBuilder.AppendLine("			AND IMRBL1.MAIL_RES_NUM = IMRBL2.MAIL_RES_NUM");
				oSqlBuilder.AppendLine("			AND IMRBL1.MAN_USER_SEQ = IMRBL2.MAN_USER_SEQ");
				oSqlBuilder.AppendLine("			AND IMRBL1.CAST_USER_SEQ = IMRBL2.CAST_USER_SEQ");
				oSqlBuilder.AppendLine("	) IMRBL3");
				oSqlBuilder.AppendLine(") MRBL ");
				sAddTable = oSqlBuilder.ToString();
				sAddWhere = " AND MAIL.TX_USER_SEQ = MRBL.MAN_USER_SEQ (+) AND MAIL.RX_USER_SEQ = MRBL.CAST_USER_SEQ (+) ";
			}

			if (pTxRxType.Equals(ViCommConst.TXRX) == false) {
				if (pSexCd.Equals(ViCommConst.MAN)) {
					sSql.Append(",T_REQ_TX_MAIL,T_CAST_CHARACTER,T_ACT_CATEGORY,T_EX_CHAR_NO").AppendLine();
					sSql.Append("WHERE").AppendLine();
					sSql.Append("MAIL.REQUEST_TX_MAIL_SEQ			= T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ		AND").AppendLine();
					sSql.Append("MAIL.PARTNER_SITE_CD				= T_CAST_CHARACTER.SITE_CD			(+)	AND").AppendLine();
					sSql.Append("MAIL.PARTNER_USER_SEQ				= T_CAST_CHARACTER.USER_SEQ			(+)	AND").AppendLine();
					sSql.Append("MAIL.PARTNER_USER_CHAR_NO			= T_CAST_CHARACTER.USER_CHAR_NO		(+)	AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.SITE_CD			= T_ACT_CATEGORY.SITE_CD			(+) AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.ACT_CATEGORY_SEQ	= T_ACT_CATEGORY.ACT_CATEGORY_SEQ	(+) AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.SITE_CD			= T_EX_CHAR_NO.SITE_CD				(+) AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.USER_CHAR_NO		= T_EX_CHAR_NO.USER_CHAR_NO			(+) AND").AppendLine();
					sSql.Append(":LATEST_FLAG						= T_EX_CHAR_NO.LATEST_FLAG			(+) ").AppendLine();
				} else {
					sSql.Append(",T_REQ_TX_MAIL,T_USER_MAN_CHARACTER").AppendLine();
					sSql.Append(sAddTable).AppendLine();
					sSql.Append("WHERE").AppendLine();
					sSql.Append("MAIL.REQUEST_TX_MAIL_SEQ	= T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ		AND").AppendLine();
					sSql.Append("MAIL.PARTNER_SITE_CD		= T_USER_MAN_CHARACTER.SITE_CD		(+)	AND").AppendLine();
					sSql.Append("MAIL.PARTNER_USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ		(+)	AND").AppendLine();
					sSql.Append("MAIL.PARTNER_USER_CHAR_NO	= T_USER_MAN_CHARACTER.USER_CHAR_NO	(+)	").AppendLine();
					sSql.Append(sAddWhere).AppendLine();
				}
			} else {
				if (pSexCd.Equals(ViCommConst.MAN)) {
					sSql.Append(",T_REQ_TX_MAIL,T_CAST_CHARACTER,T_ACT_CATEGORY,T_EX_CHAR_NO").AppendLine();
					sSql.Append("WHERE").AppendLine();
					sSql.Append("MAIL.REQUEST_TX_MAIL_SEQ			= T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ		AND").AppendLine();
					sSql.Append("MAIL.CAST_SITE_CD					= T_CAST_CHARACTER.SITE_CD			(+)	AND").AppendLine();
					sSql.Append("MAIL.CAST_USER_SEQ					= T_CAST_CHARACTER.USER_SEQ			(+)	AND").AppendLine();
					sSql.Append("MAIL.CAST_CHAR_NO					= T_CAST_CHARACTER.USER_CHAR_NO		(+)	AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.SITE_CD			= T_ACT_CATEGORY.SITE_CD			(+) AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.ACT_CATEGORY_SEQ	= T_ACT_CATEGORY.ACT_CATEGORY_SEQ	(+) AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.SITE_CD			= T_EX_CHAR_NO.SITE_CD				(+) AND").AppendLine();
					sSql.Append("T_CAST_CHARACTER.USER_CHAR_NO		= T_EX_CHAR_NO.USER_CHAR_NO			(+) AND").AppendLine();
					sSql.Append(":LATEST_FLAG						= T_EX_CHAR_NO.LATEST_FLAG			(+) ").AppendLine();
				} else {
					sSql.Append(",T_REQ_TX_MAIL,T_USER_MAN_CHARACTER").AppendLine();
					sSql.Append(sAddTable).AppendLine();
					sSql.Append("WHERE").AppendLine();
					sSql.Append("MAIL.REQUEST_TX_MAIL_SEQ	= T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ		AND").AppendLine();
					sSql.Append("MAIL.MAN_USER_SITE_CD		= T_USER_MAN_CHARACTER.SITE_CD		(+)	AND").AppendLine();
					sSql.Append("MAIL.MAN_USER_SEQ			= T_USER_MAN_CHARACTER.USER_SEQ		(+)	AND").AppendLine();
					sSql.Append("MAIL.MAN_USER_CHAR_NO		= T_USER_MAN_CHARACTER.USER_CHAR_NO	(+)	").AppendLine();
					sSql.Append(sAddWhere).AppendLine();
				}
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				if (pMailSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				}
				if (pSexCd.Equals(ViCommConst.MAN)) {
					cmd.Parameters.Add("LATEST_FLAG",1);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_BOX00");
				}
			}
		} finally {
			conn.Close();
		}

		if (pTxRxType.Equals(ViCommConst.TXRX)) {
			DataColumn col;
			col = new DataColumn("TXRX_TYPE",Type.GetType("System.String"));
			ds.Tables[0].Columns.Add(col);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (pUserSeq.Equals(dr["TX_USER_SEQ"].ToString())) {
					dr["TXRX_TYPE"] = ViCommConst.TX;
				} else {
					dr["TXRX_TYPE"] = ViCommConst.RX;
				}
			}
		}
		if (pAppendAttr) {
			AppendAttr(pSexCd,pTxRxType,ds);
		}

		// ﾌﾟﾛﾌ画像やきもち設定
		if (pSexCd.Equals(ViCommConst.MAN)) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_PROFILE_PIC,2)) {
					this.SetProfilePicNonPublish(pSiteCd,ds);
				}
			}
		}

		return ds;
	}

	public DataSet GetPageCollectionUserRx(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		int pPageNo,
		int pRecPerPage
	) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,"","",ViCommConst.MAIL_BOX_USER,ViCommConst.RX,"",false,"",pSexCd,null,null,"",null,null,ref sWhere);
		oParamList.AddRange(oParms);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAX(MAIL_SEQ) AS MAIL_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAIL_BOX_RX00 P");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	TX_SITE_CD,");
		oSqlBuilder.AppendLine("	TX_USER_SEQ,");
		oSqlBuilder.AppendLine("	TX_USER_CHAR_NO");

		string sSortExpression = "ORDER BY MAIL_SEQ DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			AppendMailUserRx(oDataSet);
		}

		return oDataSet;
	}

	public DataSet GetPageCollectionUserTx(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pSexCd,
		int pPageNo,
		int pRecPerPage
	) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,"","",ViCommConst.MAIL_BOX_USER,ViCommConst.TX,"",false,"",pSexCd,null,null,"",null,null,ref sWhere);
		oParamList.AddRange(oParms);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAX(MAIL_SEQ) AS MAIL_SEQ");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAIL_BOX_TX00 P");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	RX_SITE_CD,");
		oSqlBuilder.AppendLine("	RX_USER_SEQ,");
		oSqlBuilder.AppendLine("	RX_USER_CHAR_NO");

		string sSortExpression = "ORDER BY MAIL_SEQ DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			AppendMailUserTx(oDataSet);
		}

		return oDataSet;
	}

	private void AppendMailUserRx(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		string sWhereClause = String.Empty;
		List<string> sMailSeqList = new List<string>();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("HANDLE_NM"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("LOGIN_ID"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_TITLE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("CREATE_DATE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("READ_FLAG"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("RETURN_MAIL_FLAG"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("DEL_PROTECT_FLAG"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC1"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC2"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC3"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC4"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC5"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("REQUEST_TX_MAIL_SEQ"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("TX_SEX_CD"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_TYPE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("TEXT_MAIL_FLAG"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("SMALL_PHOTO_IMG_PATH"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("ATTACHED_OBJ_TYPE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("AGE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("NG_SKULL_COUNT"),System.Type.GetType("System.String")));

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			sMailSeqList.Add(pDR["MAIL_SEQ"].ToString());
		}

		SysPrograms.SqlAppendWhere(string.Format("MAIL_SEQ IN ({0})",string.Join(",",sMailSeqList.ToArray())),ref sWhereClause);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAIL.MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAIL.HANDLE_NM,");
		oSqlBuilder.AppendLine("	MAIL.LOGIN_ID,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_TITLE,");
		oSqlBuilder.AppendLine("	MAIL.CREATE_DATE,");
		oSqlBuilder.AppendLine("	MAIL.READ_FLAG,");
		oSqlBuilder.AppendLine("	MAIL.RETURN_MAIL_FLAG,");
		oSqlBuilder.AppendLine("	MAIL.DEL_PROTECT_FLAG,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC1,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC2,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC3,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC4,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC5,");
		oSqlBuilder.AppendLine("	MAIL.REQUEST_TX_MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAIL.TX_SEX_CD,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_TYPE,");
		oSqlBuilder.AppendLine("	MAIL.TEXT_MAIL_FLAG,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(MAIL.MAN_USER_SITE_CD,MAIL.LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.AGE,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.NG_SKULL_COUNT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		MAIL_SEQ,");
		oSqlBuilder.AppendLine("		HANDLE_NM,");
		oSqlBuilder.AppendLine("		PARTNER_LOGIN_ID AS LOGIN_ID,");
		oSqlBuilder.AppendLine("		MAIL_TITLE,");
		oSqlBuilder.AppendLine("		CREATE_DATE,");
		oSqlBuilder.AppendLine("		READ_FLAG,");
		oSqlBuilder.AppendLine("		RETURN_MAIL_FLAG,");
		oSqlBuilder.AppendLine("		DEL_PROTECT_FLAG,");
		oSqlBuilder.AppendLine("		MAIL_DOC1,");
		oSqlBuilder.AppendLine("		MAIL_DOC2,");
		oSqlBuilder.AppendLine("		MAIL_DOC3,");
		oSqlBuilder.AppendLine("		MAIL_DOC4,");
		oSqlBuilder.AppendLine("		MAIL_DOC5,");
		oSqlBuilder.AppendLine("		TX_SEX_CD,");
		oSqlBuilder.AppendLine("		MAIL_TYPE,");
		oSqlBuilder.AppendLine("		TEXT_MAIL_FLAG,");
		oSqlBuilder.AppendLine("		MAN_USER_SITE_CD,");
		oSqlBuilder.AppendLine("		REQUEST_TX_MAIL_SEQ,");
		oSqlBuilder.AppendLine("		PARTNER_SITE_CD,");
		oSqlBuilder.AppendLine("		PARTNER_USER_SEQ,");
		oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		VW_MAIL_BOX_RX00");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) MAIL,");
		oSqlBuilder.AppendLine("	T_REQ_TX_MAIL,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	MAIL.REQUEST_TX_MAIL_SEQ = T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ AND").AppendLine();
		oSqlBuilder.AppendLine("	MAIL.PARTNER_SITE_CD = T_USER_MAN_CHARACTER.SITE_CD (+) AND").AppendLine();
		oSqlBuilder.AppendLine("	MAIL.PARTNER_USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ (+) AND").AppendLine();
		oSqlBuilder.AppendLine("	MAIL.PARTNER_USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO (+)").AppendLine();

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["MAIL_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["MAIL_SEQ"]);
			pDR["HANDLE_NM"] = subDR["HANDLE_NM"];
			pDR["LOGIN_ID"] = subDR["LOGIN_ID"];
			pDR["MAIL_TITLE"] = subDR["MAIL_TITLE"];
			pDR["CREATE_DATE"] = subDR["CREATE_DATE"];
			pDR["READ_FLAG"] = subDR["READ_FLAG"];
			pDR["RETURN_MAIL_FLAG"] = subDR["RETURN_MAIL_FLAG"];
			pDR["DEL_PROTECT_FLAG"] = subDR["DEL_PROTECT_FLAG"];
			pDR["MAIL_DOC1"] = subDR["MAIL_DOC1"];
			pDR["MAIL_DOC2"] = subDR["MAIL_DOC2"];
			pDR["MAIL_DOC3"] = subDR["MAIL_DOC3"];
			pDR["MAIL_DOC4"] = subDR["MAIL_DOC4"];
			pDR["MAIL_DOC5"] = subDR["MAIL_DOC5"];
			pDR["REQUEST_TX_MAIL_SEQ"] = subDR["REQUEST_TX_MAIL_SEQ"];
			pDR["TX_SEX_CD"] = subDR["TX_SEX_CD"];
			pDR["MAIL_TYPE"] = subDR["MAIL_TYPE"];
			pDR["TEXT_MAIL_FLAG"] = subDR["TEXT_MAIL_FLAG"];
			pDR["SMALL_PHOTO_IMG_PATH"] = subDR["SMALL_PHOTO_IMG_PATH"];
			pDR["ATTACHED_OBJ_TYPE"] = subDR["ATTACHED_OBJ_TYPE"];
			pDR["AGE"] = subDR["AGE"];
			pDR["NG_SKULL_COUNT"] = subDR["NG_SKULL_COUNT"];
		}
	}

	private void AppendMailUserTx(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		string sWhereClause = String.Empty;
		List<string> sMailSeqList = new List<string>();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("HANDLE_NM"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("LOGIN_ID"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_TITLE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("CREATE_DATE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("READ_FLAG"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("RETURN_MAIL_FLAG"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC1"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC2"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC3"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC4"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_DOC5"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("REQUEST_TX_MAIL_SEQ"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("TX_SEX_CD"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("MAIL_TYPE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("TEXT_MAIL_FLAG"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("SMALL_PHOTO_IMG_PATH"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("ATTACHED_OBJ_TYPE"),System.Type.GetType("System.String")));
		pDS.Tables[0].Columns.Add(new DataColumn(string.Format("AGE"),System.Type.GetType("System.String")));

		foreach (DataRow pDR in pDS.Tables[0].Rows) {
			sMailSeqList.Add(pDR["MAIL_SEQ"].ToString());
		}

		SysPrograms.SqlAppendWhere(string.Format("MAIL_SEQ IN ({0})",string.Join(",",sMailSeqList.ToArray())),ref sWhereClause);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	MAIL.MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAIL.HANDLE_NM,");
		oSqlBuilder.AppendLine("	MAIL.LOGIN_ID,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_TITLE,");
		oSqlBuilder.AppendLine("	MAIL.CREATE_DATE,");
		oSqlBuilder.AppendLine("	MAIL.READ_FLAG,");
		oSqlBuilder.AppendLine("	MAIL.RETURN_MAIL_FLAG,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC1,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC2,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC3,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC4,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_DOC5,");
		oSqlBuilder.AppendLine("	MAIL.REQUEST_TX_MAIL_SEQ,");
		oSqlBuilder.AppendLine("	MAIL.TX_SEX_CD,");
		oSqlBuilder.AppendLine("	MAIL.MAIL_TYPE,");
		oSqlBuilder.AppendLine("	MAIL.TEXT_MAIL_FLAG,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(MAIL.MAN_USER_SITE_CD,MAIL.LOGIN_ID,T_USER_MAN_CHARACTER.PIC_SEQ,0) AS SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	T_REQ_TX_MAIL.ATTACHED_OBJ_TYPE,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.AGE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(SELECT");
		oSqlBuilder.AppendLine("		MAIL_SEQ,");
		oSqlBuilder.AppendLine("		HANDLE_NM,");
		oSqlBuilder.AppendLine("		PARTNER_LOGIN_ID AS LOGIN_ID,");
		oSqlBuilder.AppendLine("		MAIL_TITLE,");
		oSqlBuilder.AppendLine("		CREATE_DATE,");
		oSqlBuilder.AppendLine("		READ_FLAG,");
		oSqlBuilder.AppendLine("		RETURN_MAIL_FLAG,");
		oSqlBuilder.AppendLine("		MAIL_DOC1,");
		oSqlBuilder.AppendLine("		MAIL_DOC2,");
		oSqlBuilder.AppendLine("		MAIL_DOC3,");
		oSqlBuilder.AppendLine("		MAIL_DOC4,");
		oSqlBuilder.AppendLine("		MAIL_DOC5,");
		oSqlBuilder.AppendLine("		TX_SEX_CD,");
		oSqlBuilder.AppendLine("		MAIL_TYPE,");
		oSqlBuilder.AppendLine("		TEXT_MAIL_FLAG,");
		oSqlBuilder.AppendLine("		MAN_USER_SITE_CD,");
		oSqlBuilder.AppendLine("		REQUEST_TX_MAIL_SEQ,");
		oSqlBuilder.AppendLine("		PARTNER_SITE_CD,");
		oSqlBuilder.AppendLine("		PARTNER_USER_SEQ,");
		oSqlBuilder.AppendLine("		PARTNER_USER_CHAR_NO");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		VW_MAIL_BOX_TX00");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) MAIL,");
		oSqlBuilder.AppendLine("	T_REQ_TX_MAIL,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	MAIL.REQUEST_TX_MAIL_SEQ = T_REQ_TX_MAIL.REQUEST_TX_MAIL_SEQ AND").AppendLine();
		oSqlBuilder.AppendLine("	MAIL.PARTNER_SITE_CD = T_USER_MAN_CHARACTER.SITE_CD (+) AND").AppendLine();
		oSqlBuilder.AppendLine("	MAIL.PARTNER_USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ (+) AND").AppendLine();
		oSqlBuilder.AppendLine("	MAIL.PARTNER_USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO (+)").AppendLine();

		DataSet subDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		pDS.Tables[0].PrimaryKey = new DataColumn[] { pDS.Tables[0].Columns["MAIL_SEQ"] };

		foreach (DataRow subDR in subDS.Tables[0].Rows) {
			DataRow pDR = pDS.Tables[0].Rows.Find(subDR["MAIL_SEQ"]);
			pDR["HANDLE_NM"] = subDR["HANDLE_NM"];
			pDR["LOGIN_ID"] = subDR["LOGIN_ID"];
			pDR["MAIL_TITLE"] = subDR["MAIL_TITLE"];
			pDR["CREATE_DATE"] = subDR["CREATE_DATE"];
			pDR["READ_FLAG"] = subDR["READ_FLAG"];
			pDR["RETURN_MAIL_FLAG"] = subDR["RETURN_MAIL_FLAG"];
			pDR["MAIL_DOC1"] = subDR["MAIL_DOC1"];
			pDR["MAIL_DOC2"] = subDR["MAIL_DOC2"];
			pDR["MAIL_DOC3"] = subDR["MAIL_DOC3"];
			pDR["MAIL_DOC4"] = subDR["MAIL_DOC4"];
			pDR["MAIL_DOC5"] = subDR["MAIL_DOC5"];
			pDR["REQUEST_TX_MAIL_SEQ"] = subDR["REQUEST_TX_MAIL_SEQ"];
			pDR["TX_SEX_CD"] = subDR["TX_SEX_CD"];
			pDR["MAIL_TYPE"] = subDR["MAIL_TYPE"];
			pDR["TEXT_MAIL_FLAG"] = subDR["TEXT_MAIL_FLAG"];
			pDR["SMALL_PHOTO_IMG_PATH"] = subDR["SMALL_PHOTO_IMG_PATH"];
			pDR["ATTACHED_OBJ_TYPE"] = subDR["ATTACHED_OBJ_TYPE"];
			pDR["AGE"] = subDR["AGE"];
		}
	}

	private OracleParameter[] CreateWhere(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pMailBoxType,
		string pTxRxType,
		string pMailSeq,
		bool pWithBatchMail,
		string pMailSearchKey,
		string pSexCd,
		bool pUnreadOnly,
		ref string pWhere
	) {
		int? iReadType = null;
		if (pUnreadOnly) {
			iReadType = ViCommConst.FLAG_OFF;
		}
		return CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pMailBoxType,pTxRxType,pMailSeq,pWithBatchMail,pMailSearchKey,pSexCd,iReadType,null,null,null,null,ref pWhere);
	}
	private OracleParameter[] CreateWhere(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pMailBoxType,
		string pTxRxType,
		string pMailSeq,
		bool pWithBatchMail,
		string pMailSearchKey,
		string pSexCd,
		int? pReadType,
		int? pReturnFlag,
		string pDelProtectFlag,
		string pExistObjFlag,
		string pTxOnlyFlag,
		ref string pWhere
	) {
		ArrayList list = new ArrayList();

		string sPartnerUserSeq = string.Empty;

		if (!string.IsNullOrEmpty(pPartnerLoginId)) {
			if (pSexCd.Equals(ViCommConst.MAN)) {
				// 男性の場合、相手は女性
				using (Cast oCast = new Cast()) {
					sPartnerUserSeq = oCast.GetUserSeq(pSiteCd,pPartnerLoginId,pPartnerUserCharNo);
				}
			} else {
				// 女性の場合、相手は男性
				using (UserManCharacter oUserMan = new UserManCharacter()) {
					sPartnerUserSeq = oUserMan.GetUserSeq(pSiteCd,pPartnerLoginId,pPartnerUserCharNo);
				}
			}
		}


		if (pTxRxType.Equals(ViCommConst.RX)) {
			pWhere = CreateRxMailWhere(pMailSeq,pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pSexCd,sPartnerUserSeq,pPartnerUserCharNo,ref list);
		} else if (pTxRxType.Equals(ViCommConst.TX)) {
			pWhere = CreateTxMailWhere(pMailSeq,pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pSexCd,sPartnerUserSeq,pPartnerUserCharNo,pWithBatchMail,ref list);
		} else {
			pWhere = CreateBothMailWhere(pMailSeq,pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pSexCd,sPartnerUserSeq,pPartnerUserCharNo,pTxOnlyFlag,ref list);
		}

		StringBuilder oWhere = new StringBuilder(pWhere);

		//if (pMailBoxType.Equals(ViCommConst.MAIL_BOX_BLOG)) {
		//    // マテビューのタイムラグ対策




		//    using(BlogArticle oBlogArticle = new BlogArticle()){
		//        oWhere.AppendLine(" AND EXISTS ( ");
		//        oWhere.AppendFormat("		SELECT 1 FROM {0} BA ", oBlogArticle.GetMainTableName()).AppendLine();
		//        oWhere.AppendLine("		WHERE   ");
		//        oWhere.AppendLine("		 BA.BLOG_ARTICLE_SEQ = OBJ_SEQ AND   ");
		//        oWhere.AppendLine(" ) ");
		//    }
		//}


		if (!string.IsNullOrEmpty(pMailSearchKey)) {
			oWhere.Append(" AND (");
			oWhere.Append("	P.MAIL_TITLE		LIKE '%' || :FUZZY_MAIL_TITLE || '%'	OR ");
			if (pTxRxType.Equals(ViCommConst.TXRX)) {
				if (pSexCd.Equals(ViCommConst.MAN)) {
					oWhere.Append("	P.CAST_HANDLE_NM		LIKE '%' || :FUZZY_HANDLE_NM || '%'		OR ");
				} else {
					oWhere.Append("	P.MAN_HANDLE_NM		LIKE '%' || :FUZZY_HANDLE_NM || '%'		OR ");
				}
			} else {
				oWhere.Append("	P.HANDLE_NM		LIKE '%' || :FUZZY_HANDLE_NM || '%'		OR ");
			}

			oWhere.Append("	P.MAIL_DOC1		LIKE '%' || :FUZZY_MAIL_DOC1 || '%'		OR ");
			oWhere.Append("	P.MAIL_DOC2		LIKE '%' || :FUZZY_MAIL_DOC2 || '%'		OR ");
			oWhere.Append("	P.MAIL_DOC3		LIKE '%' || :FUZZY_MAIL_DOC3 || '%'		OR ");
			oWhere.Append("	P.MAIL_DOC4		LIKE '%' || :FUZZY_MAIL_DOC4 || '%'		OR ");
			oWhere.Append("	P.MAIL_DOC5		LIKE '%' || :FUZZY_MAIL_DOC5 || '%'");
			oWhere.Append(" ) ");
			list.Add(new OracleParameter(":FUZZY_MAIL_TITLE",pMailSearchKey));
			list.Add(new OracleParameter(":FUZZY_HANDLE_NM",pMailSearchKey));
			list.Add(new OracleParameter(":FUZZY_MAIL_DOC1",pMailSearchKey));
			list.Add(new OracleParameter(":FUZZY_MAIL_DOC2",pMailSearchKey));
			list.Add(new OracleParameter(":FUZZY_MAIL_DOC3",pMailSearchKey));
			list.Add(new OracleParameter(":FUZZY_MAIL_DOC4",pMailSearchKey));
			list.Add(new OracleParameter(":FUZZY_MAIL_DOC5",pMailSearchKey));
		}

		//if (pUnreadOnly) {
		if (pReadType != null) {
			oWhere.Append(" AND P.READ_FLAG = :READ_FLAG ");
			list.Add(new OracleParameter("READ_FLAG",pReadType));
		}

		if (pReturnFlag != null) {
			oWhere.Append(" AND P.RETURN_MAIL_FLAG = :RETURN_MAIL_FLAG ");
			list.Add(new OracleParameter("RETURN_MAIL_FLAG",pReturnFlag));
		}

		if (ViCommConst.MAIL_BOX_INFO.Equals(pMailBoxType)) {
			oWhere.Append("	AND P.MAIL_DOC1 IS NOT NULL ");
		}

		if (!string.IsNullOrEmpty(pDelProtectFlag)) {
			oWhere.Append(" AND P.DEL_PROTECT_FLAG = :DEL_PROTECT_FLAG ");
			list.Add(new OracleParameter("DEL_PROTECT_FLAG",pDelProtectFlag));
		}
		
		if (!string.IsNullOrEmpty(pExistObjFlag) && pExistObjFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oWhere.Append(" AND  EXISTS(SELECT 1 FROM T_REQ_TX_MAIL WHERE REQUEST_TX_MAIL_SEQ = P.REQUEST_TX_MAIL_SEQ AND ATTACHED_OBJ_TYPE IS NOT NULL)");
		}

		pWhere = oWhere.ToString();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	string CreateRxMailWhere(string pMailSeq,string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pSexCd,string pPartnerUserSeq,string pPartnerUserCharNo,ref ArrayList pParmList) {
		StringBuilder sWhere = new StringBuilder();

		if (pMailSeq.Equals(string.Empty)) {
			sWhere.Append("WHERE").AppendLine();
			sWhere.Append("P.RX_SITE_CD		= :RX_SITE_CD			AND	").AppendLine();
			sWhere.Append("P.RX_USER_SEQ		= :RX_USER_SEQ			AND	").AppendLine();
			sWhere.Append("P.RX_USER_CHAR_NO	= :RX_USER_CHAR_NO		AND ").AppendLine();
			sWhere.Append("P.RX_MAIL_BOX_TYPE	= :RX_MAIL_BOX_TYPE		AND ").AppendLine();
			sWhere.Append("P.WAIT_TX_FLAG		= :WAIT_TX_FLAG			AND ");
			sWhere.Append("P.RX_DEL_FLAG		= :RX_DEL_FLAG			 ").AppendLine();
			pParmList.Add(new OracleParameter("RX_SITE_CD",pSiteCd));
			pParmList.Add(new OracleParameter("RX_USER_SEQ",pUserSeq));
			pParmList.Add(new OracleParameter("RX_USER_CHAR_NO",pUserCharNo));
			pParmList.Add(new OracleParameter("RX_MAIL_BOX_TYPE",pMailBoxType));
			pParmList.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));
			pParmList.Add(new OracleParameter("RX_DEL_FLAG",ViCommConst.FLAG_OFF));

			if (!pPartnerUserSeq.Equals(string.Empty) && !pPartnerUserCharNo.Equals(string.Empty)) {
				sWhere.Append("AND P.TX_USER_SEQ		=:TX_USER_SEQ		").AppendLine();
				sWhere.Append("AND P.TX_USER_CHAR_NO	=:TX_USER_CHAR_NO	").AppendLine();
				pParmList.Add(new OracleParameter("TX_USER_SEQ",pPartnerUserSeq));
				pParmList.Add(new OracleParameter("TX_USER_CHAR_NO",pPartnerUserCharNo));
			}
		} else {
			sWhere.Append("WHERE P.MAIL_SEQ =:MAIL_SEQ  AND P.RX_USER_SEQ =: RX_USER_SEQ");
			pParmList.Add(new OracleParameter("MAIL_SEQ",pMailSeq));
			pParmList.Add(new OracleParameter("RX_USER_SEQ",pUserSeq));
		}
		return sWhere.ToString();
	}

	string CreateTxMailWhere(string pMailSeq,string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pSexCd,string pPartnerUserSeq,string pPartnerUserCharNo,bool pWithBatchMail,ref ArrayList pParmList) {
		StringBuilder sWhere = new StringBuilder();
		if (pMailSeq.Equals("")) {
			sWhere.Append("WHERE").AppendLine();
			sWhere.Append("P.TX_SITE_CD			= :TX_SITE_CD			AND	").AppendLine();
			sWhere.Append("P.TX_USER_SEQ		= :TX_USER_SEQ			AND	").AppendLine();
			sWhere.Append("P.TX_USER_CHAR_NO	= :TX_USER_CHAR_NO		AND ").AppendLine();
			sWhere.Append("P.TX_MAIL_BOX_TYPE	= :TX_MAIL_BOX_TYPE		AND ").AppendLine();

			// メール一括送信を含めない場合はBATCH_MAIL_FLAGが0のもののみを対象とする。 
			if (pSexCd.Equals(ViCommConst.MAN) || pWithBatchMail == false) {
				sWhere.Append("P.BATCH_MAIL_FLAG = :BATCH_MAIL_FLAG AND").AppendLine();
			} else {
				sWhere.Append("(P.BATCH_MAIL_FLAG IN (:BATCH_MAIL_FLAG_OFF,:BATCH_MAIL_FLAG_ON)) AND").AppendLine();
			}

			sWhere.Append("P.TX_DEL_FLAG	= :DEL_FLAG ");
			pParmList.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
			pParmList.Add(new OracleParameter("TX_USER_SEQ",pUserSeq));
			pParmList.Add(new OracleParameter("TX_USER_CHAR_NO",pUserCharNo));
			pParmList.Add(new OracleParameter("TX_MAIL_BOX_TYPE",pMailBoxType));
			if (pSexCd.Equals(ViCommConst.MAN) || pWithBatchMail == false) {
				pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG",ViCommConst.FLAG_OFF));
			} else {
				pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG_OFF",ViCommConst.FLAG_OFF));
				pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG_ON",ViCommConst.FLAG_ON));
			}
			pParmList.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));

			if (!pPartnerUserSeq.Equals("") && !pPartnerUserCharNo.Equals("")) {
				sWhere.Append("AND P.RX_USER_SEQ		=:RX_USER_SEQ  ").AppendLine();
				sWhere.Append("AND P.RX_USER_CHAR_NO	=:RX_USER_CHAR_NO	").AppendLine();
				pParmList.Add(new OracleParameter("RX_USER_SEQ",pPartnerUserSeq));
				pParmList.Add(new OracleParameter("RX_USER_CHAR_NO",pPartnerUserCharNo));
			}
		} else {
			sWhere.Append("WHERE P.MAIL_SEQ =:MAIL_SEQ  AND P.TX_USER_SEQ =: TX_USER_SEQ");
			pParmList.Add(new OracleParameter("MAIL_SEQ",pMailSeq));
			pParmList.Add(new OracleParameter("TX_USER_SEQ",pUserSeq));
		}
		return sWhere.ToString();
	}

	string CreateBothMailWhere(string pMailSeq,string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pSexCd,string pPartnerUserSeq,string pPartnerUserCharNo,string pTxOnlyFlag,ref ArrayList pParmList) {
		StringBuilder sWhere = new StringBuilder();

		if (pMailSeq.Equals(string.Empty)) {
			sWhere.Append(" WHERE ").AppendLine();
			sWhere.Append("	P.MAN_USER_SITE_CD	= :MAN_USER_SITE_CD		AND ").AppendLine();
			sWhere.Append("	P.MAN_USER_SEQ		= :MAN_USER_SEQ			AND ").AppendLine();
			sWhere.Append("	P.MAN_USER_CHAR_NO	= :MAN_USER_CHAR_NO		AND ").AppendLine();
			sWhere.Append("	P.MAN_MAIL_BOX_TYPE	= :MAN_MAIL_BOX_TYPE	AND ").AppendLine();
			sWhere.Append("	P.CAST_SITE_CD		= :CAST_SITE_CD			AND ").AppendLine();
			sWhere.Append("	P.CAST_USER_SEQ		= :CAST_USER_SEQ		AND ").AppendLine();
			sWhere.Append("	P.CAST_CHAR_NO		= :CAST_CHAR_NO			AND ").AppendLine();
			sWhere.Append("	P.CAST_MAIL_BOX_TYPE	= :CAST_MAIL_BOX_TYPE	AND ").AppendLine();
			sWhere.Append("	P.WAIT_TX_FLAG		= :WAIT_TX_FLAG			 ").AppendLine();

			if (pSexCd.Equals(ViCommConst.MAN)) {
				pParmList.Add(new OracleParameter("MAN_USER_SITE_CD",pSiteCd));
				pParmList.Add(new OracleParameter("MAN_USER_SEQ",pUserSeq));
				pParmList.Add(new OracleParameter("MAN_USER_CHAR_NO",pUserCharNo));
				pParmList.Add(new OracleParameter("MAN_MAIL_BOX_TYPE",pMailBoxType));
				pParmList.Add(new OracleParameter("CAST_USER_SITE_CD",pSiteCd));
				pParmList.Add(new OracleParameter("CAST_USER_SEQ",pPartnerUserSeq));
				pParmList.Add(new OracleParameter("CAST_CHAR_NO",pPartnerUserCharNo));
				pParmList.Add(new OracleParameter("CAST_MAIL_BOX_TYPE",pMailBoxType));
				pParmList.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));
			} else {
				pParmList.Add(new OracleParameter("MAN_USER_SITE_CD",pSiteCd));
				pParmList.Add(new OracleParameter("MAN_USER_SEQ",pPartnerUserSeq));
				pParmList.Add(new OracleParameter("MAN_USER_CHAR_NO",pPartnerUserCharNo));
				pParmList.Add(new OracleParameter("MAN_MAIL_BOX_TYPE",pMailBoxType));
				pParmList.Add(new OracleParameter("CAST_USER_SITE_CD",pSiteCd));
				pParmList.Add(new OracleParameter("CAST_USER_SEQ",pUserSeq));
				pParmList.Add(new OracleParameter("CAST_CHAR_NO",pUserCharNo));
				pParmList.Add(new OracleParameter("CAST_MAIL_BOX_TYPE",pMailBoxType));
				pParmList.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));
			}

			sWhere.AppendLine(" AND ");
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sWhere.Append("	P.MAN_MAIL_DEL_FLAG	= :MAN_MAIL_DEL_FLAG	 ").AppendLine();
				pParmList.Add(new OracleParameter("MAN_MAIL_DEL_FLAG",ViCommConst.FLAG_OFF));
			} else {
				sWhere.Append("	P.CAST_MAIL_DEL_FLAG	= :CAST_MAIL_DEL_FLAG	 ").AppendLine();
				pParmList.Add(new OracleParameter("CAST_MAIL_DEL_FLAG",ViCommConst.FLAG_OFF));
			}

			if (!string.IsNullOrEmpty(pTxOnlyFlag) && pTxOnlyFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sWhere.Append(" AND	P.TX_USER_SEQ			= :TX_USER_SEQ			 ").AppendLine();
				sWhere.Append(" AND	P.TX_USER_CHAR_NO		= :TX_USER_CHAR_NO		 ").AppendLine();
				pParmList.Add(new OracleParameter("TX_USER_SEQ",pUserSeq));
				pParmList.Add(new OracleParameter("TX_USER_CHAR_NO",pUserCharNo));
			}


		} else {
			sWhere.Append("WHERE P.MAIL_SEQ =:MAIL_SEQ  AND (P.MAN_USER_SEQ =: MAN_USER_SEQ OR P.CAST_USER_SEQ =: CAST_USER_SEQ) ");
			pParmList.Add(new OracleParameter("MAIL_SEQ",pMailSeq));
			pParmList.Add(new OracleParameter("MAN_USER_SEQ",pUserSeq));
			pParmList.Add(new OracleParameter("CAST_USER_SEQ",pUserSeq));
		}
		return sWhere.ToString();
	}

	public void UpdateMailBatchDel(string pSiteCd,string pUserSeq,string pUserCharNo,string pTxRxType,string pMailBoxType,int pReadOnlyFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_BATCH_DEL");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PTX_RX_TYPE",DbType.VARCHAR2,pTxRxType);
			db.ProcedureInParm("PMAIL_BOX_TYPE",DbType.VARCHAR2,pMailBoxType);
			db.ProcedureInParm("PREAD_MAIL_ONLY_FLAG",DbType.NUMBER,pReadOnlyFlag);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateMailDel(string pMailSeq,string pTxRxType) {
		string[] mailSeqList = new string[] { pMailSeq };
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL");
			db.ProcedureInArrayParm("PMAIL_SEQ",DbType.VARCHAR2,mailSeqList.Length,mailSeqList);
			db.ProcedureInParm("PMAIL_SEQ_COUNT",DbType.NUMBER,mailSeqList.Length);
			db.ProcedureInParm("PTX_RX_TYPE",DbType.VARCHAR2,pTxRxType);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateMailDel(string[] pMailSeqList,string pTxRxType) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL");
			db.ProcedureInArrayParm("PMAIL_SEQ",DbType.VARCHAR2,pMailSeqList.Length,pMailSeqList);
			db.ProcedureInParm("PMAIL_SEQ_COUNT",DbType.NUMBER,pMailSeqList.Length);
			db.ProcedureInParm("PTX_RX_TYPE",DbType.VARCHAR2,pTxRxType);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateMailDelTxRx(string pSiteCd,string pUserSeq,string pUserCharNo,string[] pMailSeqList) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL_TX_RX");
			db.ProcedureInParm("pSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInArrayParm("pMAIL_SEQ",DbType.VARCHAR2,pMailSeqList.Length,pMailSeqList);
			db.ProcedureOutParm("pSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateMailReadFlag(string pMailSeq) {
		if (SessionObjs.Current.adminFlg) {
			return;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_READ_FLAG");
			db.ProcedureInParm("PMAIL_SEQ",DbType.VARCHAR2,pMailSeq);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateMailReadFlagBatch(string[] pMailSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_READ_FLAG_BATCH");
			db.ProcedureInArrayParm("PMAIL_SEQ",DbType.VARCHAR2,pMailSeq.Length,pMailSeq);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateMailAttaChedOpenFlag(string pMailSeq) {
		if (SessionObjs.Current.adminFlg) {
			return;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_ATTACHED_FLAG");
			db.ProcedureInParm("PMAIL_SEQ",DbType.VARCHAR2,pMailSeq);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private void AppendAttr(string pSexCd,string pTxRxType,DataSet pDS) {
		SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
		string sNewDays = oObj.site.newFaceDays.ToString();
		string sNewMark = oObj.site.newFaceMark;
		if (sNewDays.Equals("")) {
			sNewDays = "0";
		}
		string sNewDay = DateTime.Now.AddDays(-1 * int.Parse(sNewDays)).ToString("yyyy/MM/dd");

		string sSql,sField;
		if (pSexCd.Equals(ViCommConst.MAN)) {
			// ﾒｰﾙの相手は女性
			sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
							"WHERE " +
								"SITE_CD		= :SITE_CD	AND " +
								"USER_SEQ		= :USER_SEQ	AND " +
								"USER_CHAR_NO	= :USER_CHAR_NO  ";
			sField = "CAST_ATTR_VALUE";
		} else {
			// ﾒｰﾙの相手は男性
			sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_USER_MAN_ATTR_VALUE01 " +
							"WHERE " +
								"SITE_CD		= :SITE_CD	AND " +
								"USER_SEQ		= :USER_SEQ	AND " +
								"USER_CHAR_NO	= :USER_CHAR_NO  ";
			sField = "MAN_ATTR_VALUE";
		}

		string sRefuseSql = "SELECT NVL(COUNT(*),0) AS CNT FROM T_REFUSE " +
					"WHERE " +
						"SITE_CD				= :SITE_CD			AND " +
						"USER_SEQ				= :USER_SEQ			AND " +
						"USER_CHAR_NO			= :USER_CHAR_NO		AND " +
						"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ	AND	" +
						"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	";

		string sLikeMeSql = "SELECT NVL(COUNT(*),0) AS CNT FROM T_FAVORIT " +
					"WHERE " +
						"SITE_CD				= :SITE_CD			AND " +
						"USER_SEQ				= :USER_SEQ			AND " +
						"USER_CHAR_NO			= :USER_CHAR_NO		AND " +
						"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ	AND " +
						"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ";


		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("{0}{1:D2}",sField,i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}
		col = new DataColumn("REFUSE_FLAG",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);
		col = new DataColumn("LIKE_ME_FLAG",Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		if (pSexCd.Equals(ViCommConst.MAN)) {
			col = new DataColumn("NEW_CAST_SIGN",Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("{0}{1:D2}",sField,i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());

					if ((pTxRxType.Equals(ViCommConst.RX)) || ((pTxRxType.Equals(ViCommConst.TXRX)) && (dr["TXRX_TYPE"].ToString().Equals(ViCommConst.RX)))) {
						cmd.Parameters.Add("USER_SEQ",dr["TX_USER_SEQ"].ToString());
						cmd.Parameters.Add("USER_CHAR_NO",dr["TX_USER_CHAR_NO"].ToString());
					} else {
						cmd.Parameters.Add("USER_SEQ",dr["RX_USER_SEQ"].ToString());
						cmd.Parameters.Add("USER_CHAR_NO",dr["RX_USER_CHAR_NO"].ToString());
					}
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}
				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("{0}{1}",sField,drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
			using (DataSet dsSub = new DataSet())
			using (cmd = CreateSelectCommand(sRefuseSql,conn)) {
				if ((pTxRxType.Equals(ViCommConst.RX)) || ((pTxRxType.Equals(ViCommConst.TXRX)) && (dr["TXRX_TYPE"].ToString().Equals(ViCommConst.RX)))) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["RX_USER_SEQ"]);
					cmd.Parameters.Add("USER_CHAR_NO",dr["RX_USER_CHAR_NO"]);
					cmd.Parameters.Add("PARTNER_USER_SEQ",dr["TX_USER_SEQ"].ToString());
					cmd.Parameters.Add("PARTNER_USER_CHAR_NO",dr["TX_USER_CHAR_NO"].ToString());
				} else {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["TX_USER_SEQ"]);
					cmd.Parameters.Add("USER_CHAR_NO",dr["TX_USER_CHAR_NO"]);
					cmd.Parameters.Add("PARTNER_USER_SEQ",dr["RX_USER_SEQ"].ToString());
					cmd.Parameters.Add("PARTNER_USER_CHAR_NO",dr["RX_USER_CHAR_NO"].ToString());
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub,"T_REFUSE");
					if (dsSub.Tables[0].Rows.Count != 0) {
						DataRow drSub = dsSub.Tables[0].Rows[0];
						dr["REFUSE_FLAG"] = drSub["CNT"].ToString();
					}
				}
			}
			using (DataSet dsSub = new DataSet())
			using (cmd = CreateSelectCommand(sLikeMeSql,conn)) {
				if ((pTxRxType.Equals(ViCommConst.RX)) || ((pTxRxType.Equals(ViCommConst.TXRX)) && (dr["TXRX_TYPE"].ToString().Equals(ViCommConst.RX)))) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["TX_USER_SEQ"]);
					cmd.Parameters.Add("USER_CHAR_NO",dr["TX_USER_CHAR_NO"]);
					cmd.Parameters.Add("PARTNER_USER_SEQ",dr["RX_USER_SEQ"].ToString());
					cmd.Parameters.Add("PARTNER_USER_CHAR_NO",dr["RX_USER_CHAR_NO"].ToString());
				} else {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["RX_USER_SEQ"]);
					cmd.Parameters.Add("USER_CHAR_NO",dr["RX_USER_CHAR_NO"]);
					cmd.Parameters.Add("PARTNER_USER_SEQ",dr["TX_USER_SEQ"].ToString());
					cmd.Parameters.Add("PARTNER_USER_CHAR_NO",dr["TX_USER_CHAR_NO"].ToString());
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub,"T_FAVORIT");
					if (dsSub.Tables[0].Rows.Count != 0) {
						DataRow drSub = dsSub.Tables[0].Rows[0];
						dr["LIKE_ME_FLAG"] = drSub["CNT"].ToString();
					}
				}
			}

			if (pSexCd.Equals(ViCommConst.MAN)) {
				dr["NEW_CAST_SIGN"] = "";
				if (dr["START_PERFORM_DAY"].ToString().Equals("") == false) {
					if (sNewDay.CompareTo(dr["START_PERFORM_DAY"].ToString()) <= 0) {
						dr["NEW_CAST_SIGN"] = sNewMark;
					} else {
						dr["NEW_CAST_SIGN"] = "";
					}
				} else {
				}
			}
		}
	}

	public string TxManToCastMail(
		string pSiteCd,
		string pUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		string pPicSeq,
		string pMovieSeq,
		int pQuickResMailFlag,
		string pPresentMailItemSeq
	) {
		string sObjTempId = string.Empty;
		return TxManToCastMail(pSiteCd,pUserSeq,pCastUserSeq,pCastCharNo,pMailTitle,pMailDoc,pAttachedObjType,pPicSeq,pMovieSeq,"",ViCommConst.ATTACHED_STOCK,pQuickResMailFlag,pPresentMailItemSeq,out sObjTempId);
	}

	public string TxManToCastReturnMail(
		string pSiteCd,
		string pUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		string pPicSeq,
		string pMovieSeq,
		string pPartnerMailSeq,
		string pPresentMailItemSeq,
		int pQuickResMailFlag
	) {
		string sObjTempId = string.Empty;
		return TxManToCastMail(pSiteCd,pUserSeq,pCastUserSeq,pCastCharNo,pMailTitle,pMailDoc,pAttachedObjType,pPicSeq,pMovieSeq,pPartnerMailSeq,ViCommConst.ATTACHED_STOCK,pQuickResMailFlag,pPresentMailItemSeq,out sObjTempId);
	}

	public string TxManToCastAttachedMailer(
		string pSiteCd,
		string pUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		int pQuickResMailFlag,
		string pPresentMailItemSeq,
		out string pObjTempId
	) {
		return TxManToCastMail(pSiteCd,pUserSeq,pCastUserSeq,pCastCharNo,pMailTitle,pMailDoc,pAttachedObjType,null,null,null,ViCommConst.ATTACHED_MAILER,pQuickResMailFlag,pPresentMailItemSeq,out pObjTempId);
	}
	public string TxManToCastAttachedMailerReturnMail(
		string pSiteCd,
		string pUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		string pPartnerMailSeq,
		string pPresentMailItemSeq,
		out string pObjTempId,
		int pQuickResMailFlag
	) {
		return TxManToCastMail(pSiteCd,pUserSeq,pCastUserSeq,pCastCharNo,pMailTitle,pMailDoc,pAttachedObjType,null,null,pPartnerMailSeq,ViCommConst.ATTACHED_MAILER,ViCommConst.FLAG_OFF,pPresentMailItemSeq,out pObjTempId);
	}

	private string TxManToCastMail(
		string pSiteCd,
		string pUserSeq,
		string pCastUserSeq,
		string pCastCharNo,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		string pPicSeq,
		string pMovieSeq,
		string pPartnerMailSeq,
		string pMailAttachedMethod,
		int pQuickResMailFlag,
		string pPresentMailItemSeq,
		out string pObjTempId
	) {
		string sMailSeq = string.Empty;
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(pMailDoc,ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_MAN_TO_CAST_MAIL");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PCAST_USER_SEQ",DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInParm("PMAIL_TITLE",DbType.VARCHAR2,pMailTitle);
			db.ProcedureInArrayParm("PMAIL_DOC",DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PMAIL_DOC_COUNT",DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PATTACHED_OBJ_TYPE",DbType.VARCHAR2,pAttachedObjType);
			db.ProcedureInParm("PPIC_SEQ",DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PMOVIE_SEQ",DbType.VARCHAR2,pMovieSeq);
			db.ProcedureInParm("PPARTNER_MAIL_SEQ",DbType.VARCHAR2,pPartnerMailSeq);
			db.ProcedureInParm("PMAIL_ATTACHED_METHOD",DbType.VARCHAR2,pMailAttachedMethod);
			db.ProcedureInParm("PQUICK_RES_MAIL_FLAG",DbType.VARCHAR2,pQuickResMailFlag);
			db.ProcedureInParm("PRESENT_MAIL_ITEM_SEQ",DbType.VARCHAR2,pPresentMailItemSeq);
			db.ProcedureOutParm("PMAIL_SEQ",DbType.VARCHAR2);
			db.ProcedureOutParm("POBJ_TEMP_ID",DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
			pObjTempId = db.GetStringValue("POBJ_TEMP_ID");
			sMailSeq = db.GetStringValue("PMAIL_SEQ");
		}
		return sMailSeq;
	}

	public string TxCastToManMail(
		string pSiteCd,
		string pMailTemplateNo,
		string pCastUserSeq,
		string pCastCharNo,
		string[] pManUserSeq,
		int pServicePoint,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		string pPicSeq,
		string pMovieSeq,
		int pBatchMailFlag,
		string[] pSearchCndNmList,
		string[] pSearchCndValueList,
		out string pObjTempId) {
		return TxCastToManMail(pSiteCd,pMailTemplateNo,pCastUserSeq,pCastCharNo,pManUserSeq,pServicePoint,pMailTitle,pMailDoc,pAttachedObjType,pPicSeq,pMovieSeq,pBatchMailFlag,pSearchCndNmList,pSearchCndValueList,"",out pObjTempId);
	}

	public string TxCastToManReturnMail(
			string pSiteCd,
			string pMailTemplateNo,
			string pCastUserSeq,
			string pCastCharNo,
			string[] pManUserSeq,
			int pServicePoint,
			string pMailTitle,
			string pMailDoc,
			string pAttachedObjType,
			string pPicSeq,
			string pMovieSeq,
			int pBatchMailFlag,
			string[] pSearchCndNmList,
			string[] pSearchCndValueList,
			string pPartnerSeq,
			out string pObjTempId) {
		return TxCastToManMail(pSiteCd,pMailTemplateNo,pCastUserSeq,pCastCharNo,pManUserSeq,pServicePoint,pMailTitle,pMailDoc,pAttachedObjType,pPicSeq,pMovieSeq,pBatchMailFlag,pSearchCndNmList,pSearchCndValueList,pPartnerSeq,out pObjTempId);
	}


	private string TxCastToManMail(
		string pSiteCd,
		string pMailTemplateNo,
		string pCastUserSeq,
		string pCastCharNo,
		string[] pManUserSeq,
		int pServicePoint,
		string pMailTitle,
		string pMailDoc,
		string pAttachedObjType,
		string pPicSeq,
		string pMovieSeq,
		int pBatchMailFlag,
		string[] pSearchCndNmList,
		string[] pSearchCndValueList,
		string pPartnerMailSeq,
		out string pObjTempId
	) {
		string[] sDoc;
		int iDocCount;
		string sMailSeq = string.Empty;

		SysPrograms.SeparateHtml(pMailDoc,ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_CAST_TO_MAN_MAIL");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbType.VARCHAR2,pMailTemplateNo);
			db.ProcedureInParm("PCAST_USER_SEQ",DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PCAST_CHAR_NO",DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInArrayParm("PMAN_USER_SEQ",DbType.VARCHAR2,pManUserSeq.Length,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_COUNT",DbType.NUMBER,pManUserSeq.Length);
			db.ProcedureInParm("PSERVICE_POINT",DbType.NUMBER,pServicePoint);
			db.ProcedureInParm("PORIGINAL_TITLE",DbType.VARCHAR2,pMailTitle);
			db.ProcedureInArrayParm("PORIGINAL_DOC",DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PATTACHED_OBJ_TYPE",DbType.VARCHAR2,pAttachedObjType);
			db.ProcedureInParm("PPIC_SEQ",DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PMOVIE_SEQ",DbType.VARCHAR2,pMovieSeq);
			db.ProcedureInParm("PBATCH_MAIL_FLAG",DbType.NUMBER,pBatchMailFlag);

			if (pSearchCndNmList != null && pSearchCndValueList != null) {
				db.ProcedureInArrayParm("PSEARCH_CND_NM",DbType.VARCHAR2,pSearchCndNmList.Length,pSearchCndNmList);
				db.ProcedureInArrayParm("PSEARCH_CND_VALUE",DbType.VARCHAR2,pSearchCndNmList.Length,pSearchCndValueList);
				db.ProcedureInParm("PSERACH_CND_COUNT",DbType.NUMBER,pSearchCndNmList.Length);
			} else {
				db.ProcedureInArrayParm("PSEARCH_CND_NM",DbType.VARCHAR2,0,null);
				db.ProcedureInArrayParm("PSEARCH_CND_VALUE",DbType.VARCHAR2,0,null);
				db.ProcedureInParm("PSERACH_CND_COUNT",DbType.NUMBER,0);
			}
			db.ProcedureInParm("PPARTNER_MAIL_SEQ",DbType.VARCHAR2,pPartnerMailSeq);
			db.ProcedureOutParm("PMAIL_SEQ",DbType.VARCHAR2);
			db.ProcedureOutParm("POBJ_TEMP_ID",DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();

			pObjTempId = db.GetStringValue("POBJ_TEMP_ID");
			sMailSeq = db.GetStringValue("PMAIL_SEQ");
		}
		return sMailSeq;
	}

	public void BatchMailMarking(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		string[] pManUserSeq,
		string pMarkingDate
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BATCH_MAIL_MARKING");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbType.VARCHAR2,pCastUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO  ",DbType.VARCHAR2,pCastCharNo);
			db.ProcedureInArrayParm("PMAN_USER_SEQ",DbType.VARCHAR2,pManUserSeq.Length,pManUserSeq);
			db.ProcedureInParm("PMAN_USER_COUNT",DbType.NUMBER,pManUserSeq.Length);
			db.ProcedureInParm("PMARKING_DATE",DbType.VARCHAR2,pMarkingDate);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}


	public int GetMailCount(string pTxRxType,string pSiteCd,string pRxUserSeq,string pRxUserCharNo,string pTxUserSeq,string pTxUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iRecCount = 0;
		try {
			conn = DbConnect("MailBox.GetMailCount");
			StringBuilder sSql = new StringBuilder();


			sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM T_MAIL_LOG ").AppendLine();
			sSql.Append("WHERE ").AppendLine();
			if (pTxRxType.Equals(ViCommConst.RX)) {
				sSql.Append("RX_SITE_CD			= :RX_SITE_CD		AND ").AppendLine();
				sSql.Append("RX_USER_SEQ		= :RX_USER_SEQ		AND ").AppendLine();
				sSql.Append("RX_USER_CHAR_NO	= :RX_USER_CHAR_NO	AND ").AppendLine();
				sSql.Append("RX_MAIL_BOX_TYPE	= :RX_MAIL_BOX_TYPE AND ").AppendLine();
				sSql.Append("WAIT_TX_FLAG		= :WAIT_TX_FLAG		AND	").AppendLine();
				sSql.Append("RX_DEL_FLAG		= :RX_DEL_FLAG		AND ").AppendLine();
				sSql.Append("TX_USER_SEQ		= :TX_USER_SEQ		AND ").AppendLine();
				sSql.Append("TX_USER_CHAR_NO	= :TX_USER_CHAR_NO	 ").AppendLine();
			} else {
				sSql.Append("TX_SITE_CD			= :TX_SITE_CD		AND ").AppendLine();
				sSql.Append("TX_USER_SEQ		= :TX_USER_SEQ		AND ").AppendLine();
				sSql.Append("TX_USER_CHAR_NO	= :TX_USER_CHAR_NO	AND ").AppendLine();
				sSql.Append("TX_MAIL_BOX_TYPE	= :TX_MAIL_BOX_TYPE AND ").AppendLine();
				sSql.Append("BATCH_MAIL_FLAG	IN(:BATCH_MAIL_FLAG_OFF,:BATCH_MAIL_FLAG_ON) AND	").AppendLine();
				sSql.Append("TX_DEL_FLAG		= :TX_DEL_FLAG		AND ").AppendLine();
				sSql.Append("RX_USER_SEQ		= :RX_USER_SEQ		AND ").AppendLine();
				sSql.Append("RX_USER_CHAR_NO	= :RX_USER_CHAR_NO	 ").AppendLine();
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				if (pTxRxType.Equals(ViCommConst.RX)) {
					cmd.Parameters.Add("RX_SITE_CD",pSiteCd);
					cmd.Parameters.Add("RX_USER_SEQ",pRxUserSeq);
					cmd.Parameters.Add("RX_USER_CHAR_NO",pRxUserCharNo);
					cmd.Parameters.Add("RX_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER);
					cmd.Parameters.Add("WAIT_TX_FLAG",ViCommConst.FLAG_OFF);
					cmd.Parameters.Add("RX_DEL_FLAG",ViCommConst.FLAG_OFF);
					cmd.Parameters.Add("TX_USER_SEQ",pTxUserSeq);
					cmd.Parameters.Add("TX_USER_CHAR_NO",pTxUserCharNo);
				} else {
					cmd.Parameters.Add("TX_SITE_CD",pSiteCd);
					cmd.Parameters.Add("TX_USER_SEQ",pTxUserSeq);
					cmd.Parameters.Add("TX_USER_CHAR_NO",pTxUserCharNo);
					cmd.Parameters.Add("TX_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER);
					cmd.Parameters.Add("BATCH_MAIL_FLAG_OFF",ViCommConst.FLAG_OFF);
					cmd.Parameters.Add("BATCH_MAIL_FLAG_ON",ViCommConst.FLAG_ON);
					cmd.Parameters.Add("TX_DEL_FLAG",ViCommConst.FLAG_OFF);
					cmd.Parameters.Add("RX_USER_SEQ",pRxUserSeq);
					cmd.Parameters.Add("RX_USER_CHAR_NO",pRxUserCharNo);
				}
				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					dr = ds.Tables[0].Rows[0];
					if (ds.Tables[0].Rows.Count != 0) {
						iRecCount = int.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iRecCount;
	}

	public DataSet GetNewMail(string pSiteCd,string pUserSeq,string pUserCharNo,string pSexCd,string pMailBoxType,string pTxRxType) {
		DataSet ds;
		ds = GetPageCollection(
				pSiteCd,
				pUserSeq,
				pUserCharNo,
				"",			// PartnerLoginId,
				"",			// PartnerUserCharNo,
				pSexCd,
				pMailBoxType,
				pTxRxType,
				true,		// WithBacthMail		
				"",			// MailSearchKey,
				false,
				1,			// PageNo,
				1			// RecPerPage
		);
		return ds;
	}

	public bool IsSameMailSent(string pSiteCd,string pTxUserSeq,string pTxUserCharNo,string pRxUserSeq,string pRxUserCharNo,string pMailTitle,string pMailDoc) {
		string[] sDoc;
		int iDocCount;

		pMailDoc = pMailDoc.Replace(System.Environment.NewLine,"<br />");
		SysPrograms.SeparateHtml(pMailDoc,ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CHECK_SAME_MAIL");
			oDbSession.ProcedureInParm("pTX_SITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pTX_USER_SEQ",DbSession.DbType.VARCHAR2,pTxUserSeq);
			oDbSession.ProcedureInParm("pTX_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pTxUserCharNo);
			oDbSession.ProcedureInParm("pRX_SITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pRX_USER_SEQ",DbSession.DbType.VARCHAR2,pRxUserSeq);
			oDbSession.ProcedureInParm("pRX_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pRxUserCharNo);
			oDbSession.ProcedureInParm("pMAIL_TITLE",DbSession.DbType.VARCHAR2,pMailTitle);
			oDbSession.ProcedureInArrayParm("pMAIL_DOC",DbSession.DbType.ARRAY_VARCHAR2,sDoc);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			if (ViCommConst.SendMailStatus.SAME.Equals(oDbSession.GetStringValue("pRESULT"))) {
				return true;
			}
		}

		return false;
	}

	public bool IsQuickResMail(string pMailSeq) {
		DataSet ds;
		bool bIsExist = false;

		try {
			conn = DbConnect("MailBox.IsQuickResMail");
			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT MAIL_SEQ FROM T_QUICK_RES_MAIL ").AppendLine();
			sSql.Append("WHERE ").AppendLine();
			sSql.Append("	MAIL_SEQ = :MAIL_SEQ	").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("MAIL_SEQ",pMailSeq);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						bIsExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bIsExist;
	}

	private void SetProfilePicNonPublish(string pSiteCd,DataSet pDs) {
		string sQuery = "SELECT GET_SMALL_PHOTO_IMG_PATH(:SITE_CD,NULL,NULL) FROM DUAL";
		string sSmallNoPicImgPath = string.Empty;
		using (DbSession db = new DbSession()) {
			using (db.conn = db.DbConnect())
			using (db.cmd = db.CreateSelectCommand(sQuery,db.conn)) {
				db.cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				sSmallNoPicImgPath = iBridUtil.GetStringValue(db.cmd.ExecuteScalar());
			}
		}

		using (Favorit oFavorit = new Favorit()) {
			foreach (DataRow dr in pDs.Tables[0].Rows) {				
				if (oFavorit.GetProfilePicNonPublishFlag(pSiteCd,dr["PARTNER_USER_SEQ"].ToString(),dr["PARTNER_USER_CHAR_NO"].ToString(),dr["USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO).Equals("2")) {
					dr["SMALL_PHOTO_IMG_PATH"] = sSmallNoPicImgPath;
				}
			}
		}
	
	}
}



