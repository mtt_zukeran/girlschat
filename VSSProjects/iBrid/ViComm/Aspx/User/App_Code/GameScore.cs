﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｶﾞﾙﾁｬGAME
--	Progaram ID		: Game
--
--  Creation Date	: 2011.03.29
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[Serializable]
public class GameScoreSeekCondition {
	private NameValueCollection query = new NameValueCollection();

	public ulong SeekMode {
		get { return ulong.Parse(this.query["seekmode"] ?? "-1"); }
		set { this.query["seekmode"] = value.ToString(); }
	}
	public string SiteCd {
		get { return this.query["site_cd"]; }
		set { this.query["site_cd"] = value; }
	}
	public string GameType {
		get { return this.query["game_type"]; }
		set { this.query["game_type"] = value; }
	}
	public string SexCd {
		get { return this.query["sex_cd"]; }
		set {
			this.query["sex_cd"] = value.Equals(ViCommConst.WOMAN) ? ViCommConst.OPERATOR : value;
		}
	}

	public GameScoreSeekCondition() : this(new NameValueCollection()) { }
	public GameScoreSeekCondition(NameValueCollection pQuery) {
		this.query["seekmode"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["seekmode"]));
		this.query["site_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site_cd"]));
		this.query["game_type"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["game_type"]));
		this.query["sex_cd"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["sex_cd"]));
	}

	public NameValueCollection GetQuery() { return this.query; }

}
[Serializable]
public class GameScore : DbSession {
	public GameScore() {}
	
	public DataSet GetLatestScoreInfo(string pSiteCd,string pGameType,string pSexCd,string pUserSeq,string pUserCharNo){
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT																									");
		oSqlBuilder.AppendLine(" 	TMP.SITE_CD      	,                                                                               ");
		oSqlBuilder.AppendLine(" 	TMP.GAME_TYPE     	,                                                                               ");
		oSqlBuilder.AppendLine(" 	TMP.SEX_CD        	,                                                                               ");
		oSqlBuilder.AppendLine(" 	TMP.GAME_SCORE     	AS GAME_LATEST_SCORE,                                                           ");
		oSqlBuilder.AppendLine(" 	(																		                            ");
		oSqlBuilder.AppendLine(" 		SELECT																                            ");
		oSqlBuilder.AppendLine(" 			COUNT(*) + 1													                            ");
		oSqlBuilder.AppendLine(" 		FROM																                            ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE													                            ");
		oSqlBuilder.AppendLine(" 		WHERE																                            ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SITE_CD 		= TMP.SITE_CD 				AND		                            ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_TYPE  	= TMP.GAME_TYPE         	AND		                            ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SEX_CD     	= TMP.SEX_CD            	AND		                            ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_SCORE 	> TMP.GAME_SCORE				                                ");
		oSqlBuilder.AppendLine(" 	) AS GAME_LATEST_RANK                                                                               ");
		oSqlBuilder.AppendLine(" FROM                                                                                                   ");
		oSqlBuilder.AppendLine(" 	T_GAME_SCORE TMP                                                                                    ");
		oSqlBuilder.AppendLine(" WHERE                                                                                                  ");
		oSqlBuilder.AppendLine(" 	TMP.SITE_CD 		= :SITE_CD 			AND                                                         ");
		oSqlBuilder.AppendLine(" 	TMP.GAME_TYPE 		= :GAME_TYPE 		AND                                                         ");
		oSqlBuilder.AppendLine(" 	TMP.SEX_CD 			= :SEX_CD			AND                                                         ");
		oSqlBuilder.AppendLine(" 	TMP.USER_SEQ 		= :USER_SEQ 		AND                                                         ");
		oSqlBuilder.AppendLine(" 	TMP.USER_CHAR_NO 	= :USER_CHAR_NO 	AND                                                         ");
		oSqlBuilder.AppendLine(" 	TMP.GAME_SCORE_SEQ 	=                                                                               ");
		oSqlBuilder.AppendLine(" 	(                                                                                                   ");
		oSqlBuilder.AppendLine(" 		SELECT                                                                                          ");
		oSqlBuilder.AppendLine(" 			MAX(T_GAME_SCORE.GAME_SCORE_SEQ)							                                ");
		oSqlBuilder.AppendLine(" 		FROM                                                                                            ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE                                                                                ");
		oSqlBuilder.AppendLine(" 		WHERE                                                                                           ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SITE_CD 		= TMP.SITE_CD 		AND                                         ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_TYPE 		= TMP.GAME_TYPE 	AND                                         ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SEX_CD 		= TMP.SEX_CD		AND                                         ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.USER_SEQ 		= TMP.USER_SEQ 		AND                                         ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.USER_CHAR_NO 	= TMP.USER_CHAR_NO  	                                        ");
		oSqlBuilder.AppendLine(" 		GROUP BY                                                                                        ");
		oSqlBuilder.AppendLine(" 			TMP.SITE_CD, TMP.GAME_TYPE, TMP.SEX_CD,T_GAME_SCORE.USER_SEQ ,T_GAME_SCORE.USER_CHAR_NO     ");
		oSqlBuilder.AppendLine(" 	) 												                                                    ");

		oParamList.Add(new OracleParameter(":SITE_CD", pSiteCd));
		oParamList.Add(new OracleParameter(":GAME_TYPE", pGameType));
		oParamList.Add(new OracleParameter(":SEX_CD", pSexCd));
		oParamList.Add(new OracleParameter(":USER_SEQ", pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO", pUserCharNo));

		return base.ExecuteSelectQueryBase(oSqlBuilder, oParamList.ToArray());

	}
	
	public DataSet GetHighScoreInfo(string pSiteCd,string pGameType,string pSexCd,string pUserSeq,string pUserCharNo){
	
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
	
		oSqlBuilder.AppendLine(" SELECT																						");
		oSqlBuilder.AppendLine(" 	TMP.SITE_CD      		,                                                               ");
		oSqlBuilder.AppendLine(" 	TMP.GAME_TYPE     	    ,                                                               ");
		oSqlBuilder.AppendLine(" 	TMP.SEX_CD        	    ,                                                               ");
		oSqlBuilder.AppendLine(" 	TMP.GAME_HIGH_SCORE     ,                                                               ");
		oSqlBuilder.AppendLine(" 	(																						");
		oSqlBuilder.AppendLine(" 		SELECT																				");
		oSqlBuilder.AppendLine(" 			COUNT(*) + 1																	");
		oSqlBuilder.AppendLine(" 		FROM																				");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE																	");
		oSqlBuilder.AppendLine(" 		WHERE																				");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SITE_CD 		= TMP.SITE_CD 				AND						");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_TYPE  	= TMP.GAME_TYPE         	AND						");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SEX_CD     	= TMP.SEX_CD            	AND						");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_SCORE 	> TMP.GAME_HIGH_SCORE								");
		oSqlBuilder.AppendLine("	) AS GAME_HIGH_RANK                                                                     ");
		oSqlBuilder.AppendLine(" FROM                                                                                       ");
		oSqlBuilder.AppendLine(" 	(                                                                                       ");
		oSqlBuilder.AppendLine(" 		SELECT                                                                              ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SITE_CD      			,                                           ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_TYPE       		,                                           ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SEX_CD          		,                                           ");
		oSqlBuilder.AppendLine(" 			MAX(T_GAME_SCORE.GAME_SCORE) AS GAME_HIGH_SCORE                                 ");
		oSqlBuilder.AppendLine(" 		FROM                                                                                ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE                                                                    ");
		oSqlBuilder.AppendLine(" 		WHERE                                                                               ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SITE_CD 		= :SITE_CD 			AND                             ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_TYPE 		= :GAME_TYPE 		AND                             ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SEX_CD 		= :SEX_CD			AND                             ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.USER_SEQ 		= :USER_SEQ 		AND                             ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.USER_CHAR_NO 	= :USER_CHAR_NO 	                                ");
		oSqlBuilder.AppendLine(" 		GROUP BY                                                                            ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SITE_CD		,                                                   ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.GAME_TYPE		,                                                   ");
		oSqlBuilder.AppendLine(" 			T_GAME_SCORE.SEX_CD			                                                    ");
		oSqlBuilder.AppendLine(" 	) TMP																					");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":GAME_TYPE", pGameType));
		oParamList.Add(new OracleParameter(":SEX_CD", pSexCd));
		oParamList.Add(new OracleParameter(":USER_SEQ", pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO", pUserCharNo));
		
		return base.ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
	
	}

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(GameScoreSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_GAME_SCORE	");
		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder, oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(GameScoreSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion, pPageNo, pRecPerPage);
	}
	private DataSet GetPageCollectionBase(GameScoreSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT															");
		oSqlBuilder.AppendLine(" 	T_GAME_SCORE.SITE_CD		,                               ");
		oSqlBuilder.AppendLine(" 	T_GAME_SCORE.GAME_TYPE		,                               ");
		oSqlBuilder.AppendLine(" 	T_GAME_SCORE.SEX_CD			,                               ");
		oSqlBuilder.AppendLine(" 	T_GAME_SCORE.GAME_SCORE		,                               ");
		oSqlBuilder.AppendLine(" 	DENSE_RANK() OVER (ORDER BY T_GAME_SCORE.GAME_SCORE DESC) AS GAME_RANK  ");
		oSqlBuilder.AppendLine(" FROM                                                           ");
		oSqlBuilder.AppendLine(" 	T_GAME_SCORE                                                ");
  		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion, ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);

		StringBuilder oOuterBuilder = new StringBuilder();
		oOuterBuilder.AppendLine(" SELECT					");
		oOuterBuilder.AppendLine(" 	TMP.*                   ");
		oOuterBuilder.AppendLine(" FROM                     ");
		oOuterBuilder.AppendLine(" 	(                       ");
		oOuterBuilder.AppendFormat(" 		{0}             ",oSqlBuilder).AppendLine();
		oOuterBuilder.AppendLine(" 	) TMP                   ");
		oOuterBuilder.AppendLine(" GROUP BY                 ");
		oOuterBuilder.AppendLine(" 	TMP.SITE_CD			,   ");
		oOuterBuilder.AppendLine(" 	TMP.GAME_TYPE		,   ");
		oOuterBuilder.AppendLine(" 	TMP.SEX_CD			,   ");
		oOuterBuilder.AppendLine(" 	TMP.GAME_SCORE		,   ");
		oOuterBuilder.AppendLine(" 	TMP.GAME_RANK           ");

		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oOuterBuilder.ToString(), sSortExpression, iStartIndex, pRecPerPage, out sExecSql));

		return ExecuteSelectQueryBase(sExecSql, oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(GameScoreSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" SEX_CD		= :SEX_CD", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" GAME_TYPE		= :GAME_TYPE", ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD", pCondition.SexCd));
		oParamList.Add(new OracleParameter(":GAME_TYPE", pCondition.GameType));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(GameScoreSeekCondition pCondition) {
		return " ORDER BY GAME_RANK  ";
	}

	#endregion ========================================================================================================

}
