﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 利用履歴
--	Progaram ID		: BbsUsedHistory
--
--  Creation Date	: 2011.02.01
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class BbsUsedHistory:ManBase {

	public BbsUsedHistory() {
	}

	public int GetPageCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pObjSeq,
		int pRecPerPage,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT							").AppendLine();
		oSqlBuilder.Append("	COUNT(P.SITE_CD) AS ROW_COUNT").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		oSqlBuilder.Append("	VW_OBJ_USED_HISTORY01	O,	").AppendLine();
		oSqlBuilder.Append("	VW_USER_MAN_CHARACTER00	P	").AppendLine();

		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pObjSeq,false,ref sWhere);

		oSqlBuilder.Append(sWhere);

		try {
			conn = DbConnect("TalkHistory.GetPageCount");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pObjSeq,
		string pOrderAsc,
		string pOrderDesc,
		int pPageNo,
		int pRecPerPage
	) {
		DataSet ds;
		try {
			conn = DbConnect("TalkHistory.GetPageCollection");
			ds = new DataSet();

			string sOrder = string.Empty;

			sOrder = "ORDER BY O.USED_DATE DESC,P.SITE_CD,P.USER_SEQ,P.USER_CHAR_NO ";
			if (!pOrderAsc.Equals(string.Empty) || !pOrderDesc.Equals(string.Empty)) {
				sOrder = " ORDER BY ";
				if (!pOrderAsc.Equals(string.Empty)) {
					sOrder += pOrderAsc + " ASC, ";
				}
				if (!pOrderDesc.Equals(string.Empty)) {
					sOrder += pOrderDesc + " DESC, ";
				}
				sOrder += " P.SITE_CD,P.USER_SEQ,P.USER_CHAR_NO ";
			}

			StringBuilder sInnerSql = new StringBuilder();

			sInnerSql.AppendLine("SELECT	");
			sInnerSql.AppendLine("	*		");
			sInnerSql.AppendLine("FROM		");
			sInnerSql.AppendLine("	(SELECT	");
			sInnerSql.AppendLine("		ROWNUM AS RNUM	,");
			sInnerSql.AppendLine("		INNER.*			");
			sInnerSql.AppendLine("	FROM (	");
			sInnerSql.AppendLine("		SELECT " + ManBasicField() + ",");
			sInnerSql.AppendLine("			O.CAST_USER_SEQ			,");
			sInnerSql.AppendLine("			O.CAST_USER_CHAR_NO		,");
			sInnerSql.AppendLine("			O.MAN_USER_SEQ			,");
			sInnerSql.AppendLine("			O.MAN_USER_CHAR_NO		,");
			sInnerSql.AppendLine("			O.OBJ_SEQ				,");
			sInnerSql.AppendLine("			O.OBJ_KIND				,");
			sInnerSql.AppendLine("			O.OBJ_TITLE				,");
			sInnerSql.AppendLine("			O.OBJ_DOC				,");
			sInnerSql.AppendLine("			O.OBJ_NOT_PUBLISH_FLAG			,");
			sInnerSql.AppendLine("			O.READING_COUNT			,");
			sInnerSql.AppendLine("			O.UPLOAD_DATE			,");
			sInnerSql.AppendLine("			O.OBJ_ATTR_TYPE_SEQ		,");
			sInnerSql.AppendLine("			O.OBJ_ATTR_SEQ			,");
			sInnerSql.AppendLine("			O.USED_DATE				");
			sInnerSql.AppendLine("		FROM	");
			sInnerSql.AppendLine("			VW_USER_MAN_CHARACTER00 P,	");
			sInnerSql.AppendLine("			VW_OBJ_USED_HISTORY01	O	");

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pObjSeq,true,ref sWhere);
			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
			string sSql = oObj.sqlSyntax["MAN"].ToString();

			StringBuilder sTables = new StringBuilder();
			StringBuilder sJoinTable = new StringBuilder();
			StringBuilder sJoinField = new StringBuilder();

			//sTables
			sTables.Append("T_USER	,	").AppendLine();
			sTables.Append("T_CAST_MOVIE				T_OBJ_MOVIE	,").AppendLine();
			//sJoinTable
			sJoinTable.Append("T1.OBJ_SEQ		= T_OBJ_MOVIE.MOVIE_SEQ	(+)	AND").AppendLine();
			sJoinTable.Append("T1.CAST_USER_SEQ	= T_USER.USER_SEQ		(+)	AND").AppendLine();
			//sJoinField
			sJoinField.AppendLine("GET_PHOTO_IMG_PATH		(T1.SITE_CD,T_USER.LOGIN_ID,T1.OBJ_SEQ) AS OBJ_PHOTO_IMG_PATH		,");
			sJoinField.AppendLine("GET_SMALL_PHOTO_IMG_PATH	(T1.SITE_CD,T_USER.LOGIN_ID,T1.OBJ_SEQ) AS OBJ_SMALL_PHOTO_IMG_PATH	,");
			sJoinField.AppendLine("GET_PHOTO_IMG_PATH		(T1.SITE_CD,T1.LOGIN_ID,T_OBJ_MOVIE.THUMBNAIL_PIC_SEQ) AS THUMBNAIL_IMG_PATH,");
			sJoinField.AppendLine("T_OBJ_MOVIE.MOVIE_SEQ	,");

			sSql = string.Format(sSql,sInnerSql.ToString(),sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);

				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_OBJ_USED_HISTORY01");
				}
			}
		} finally {
			conn.Close();
		}

		AppendAttr(ds);
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pObjSeq,bool bAllInfo,ref string pWhere) {
		ArrayList list = new ArrayList();
		StringBuilder sWhere = new StringBuilder();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		sWhere.Append("	WHERE ").AppendLine();
		sWhere.Append("		O.SITE_CD				= P.SITE_CD				AND	").AppendLine();
		sWhere.Append("		O.MAN_USER_SEQ			= P.USER_SEQ			AND	").AppendLine();
		sWhere.Append("		O.MAN_USER_CHAR_NO		= P.USER_CHAR_NO		AND	").AppendLine();

		sWhere.Append("		O.SITE_CD				= :SITE_CD				AND	").AppendLine();
		sWhere.Append(" 	O.CAST_USER_SEQ			= :USER_SEQ				AND	").AppendLine();
		sWhere.Append("		O.CAST_USER_CHAR_NO		= :USER_CHAR_NO			AND	").AppendLine();
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("CAST_USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("CAST_USER_CHAR_NO",pUserCharNo));

		if (!pPartnerLoginId.Equals(string.Empty) && !pPartnerUserCharNo.Equals(string.Empty)) {
			sWhere.Append("	P.LOGIN_ID				= :LOGIN_ID				AND	").AppendLine();
			sWhere.Append("	P.USER_CHAR_NO			= :PARTNER_USER_CHAR_NO	AND	").AppendLine();
			list.Add(new OracleParameter("LOGIN_ID",pPartnerLoginId));
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pPartnerUserCharNo));
		}

		if (!pObjSeq.Equals(string.Empty)) {
			sWhere.Append("	O.OBJ_SEQ				= :OBJ_SEQ				AND").AppendLine();
			list.Add(new OracleParameter("OBJ_SEQ",pObjSeq));
		}

		sWhere.Append("		P.USER_STATUS	IN (:USER_STATUS1,:USER_STATUS2,:USER_STATUS3,:USER_STATUS4,:USER_STATUS5,:USER_STATUS6)	").AppendLine();
		list.Add(new OracleParameter("USER_STATUS1",ViCommConst.USER_MAN_NORMAL));
		list.Add(new OracleParameter("USER_STATUS2",ViCommConst.USER_MAN_TEL_NO_AUTH));
		list.Add(new OracleParameter("USER_STATUS3",ViCommConst.USER_MAN_NEW));
		list.Add(new OracleParameter("USER_STATUS4",ViCommConst.USER_MAN_NO_RECEIPT));
		list.Add(new OracleParameter("USER_STATUS5",ViCommConst.USER_MAN_NO_USED));
		list.Add(new OracleParameter("USER_STATUS6",ViCommConst.USER_MAN_DELAY));


		pWhere = sWhere.ToString();

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void AppendAttr(DataSet pDS) {
		string sSql;
		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_USER_MAN_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD				AND " +
						"USER_SEQ		= :PARTNER_USER_SEQ		AND " +
						"USER_CHAR_NO	= :PARTNER_USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("MAN_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("MAN_ATTR_VALUE{0:D2}",i)] = "";
			}

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["MAN_USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["MAN_USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}
				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("MAN_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}
}

