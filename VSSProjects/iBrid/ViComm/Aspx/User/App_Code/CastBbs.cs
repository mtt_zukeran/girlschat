﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャスト掲示板
--	Progaram ID		: CastBbs
--  Creation Date	: 2010.04.20
--  Creater			: i-Brid
--
**************************************************************************/
#region using
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;
#endregion

/// <summary>
/// キャスト掲示板
/// </summary>
[System.Serializable]
public class CastBbs:CastBase {
	public CastBbs() {
	}

	public void GetPrevNextBbsSeq(
		string pBbsSeq,
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		bool pUseMV,
		out string pPrevPicSeq,
		out string pNextPicSeq) {

		pPrevPicSeq = "";
		pNextPicSeq = "";

		try {
			conn = DbConnect("CastPic.GetPrevNextBbsSeq");
			DataSet ds = new DataSet();

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastBbs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM(");
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		BBS_SEQ,").AppendLine();
			sSql.Append("		LAG(BBS_SEQ)	OVER (ORDER BY BBS_SEQ DESC) AS PREV_BBS_SEQ,").AppendLine();
			sSql.Append("		LEAD(BBS_SEQ)   OVER (ORDER BY BBS_SEQ DESC) AS NEXT_BBS_SEQ");
			sSql.Append(string.Format("	FROM {0}_CAST_BBS00 P ",sSuffix)).AppendLine();
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(this.CreateFlexibleQueryTarget(FlexibleQueryType.NotInScope),"",pSiteCd,pLoginId,pUserCharNo,false, new SeekCondition(), ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(") WHERE BBS_SEQ =:BBS_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("BBS_SEQ",pBbsSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_BBS00");
				}
			}
			if (ds.Tables[0].Rows.Count != 0) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (dr["BBS_SEQ"].ToString().Equals(pBbsSeq)) {
						pPrevPicSeq = dr["PREV_BBS_SEQ"].ToString();
						pNextPicSeq = dr["NEXT_BBS_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
	}
	
	
	#region GetPageCount ==============================================================================================
	
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,int pRecPerPage,bool pUseMV,out decimal pRecCount){
		return GetPageCountBase(FlexibleQueryType.None, pSiteCd, pLoginId, pUserCharNo, pRecPerPage, pUseMV, out pRecCount, new SeekCondition());
	}
	public int GetPageCount(string pSiteCd, string pLoginId, string pUserCharNo, int pRecPerPage, bool pUseMV, out decimal pRecCount, SeekCondition pCondition) {
		return GetPageCountBase(FlexibleQueryType.None, pSiteCd, pLoginId, pUserCharNo, pRecPerPage, pUseMV, out pRecCount, pCondition);
	}

	private int GetPageCountBase(FlexibleQueryType pFlexibleQueryType, string pSiteCd, string pLoginId, string pUserCharNo, int pRecPerPage, bool pUseMV, out decimal pRecCount, SeekCondition pCondition) {
		pUseMV = true;

		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;

		FlexibleQueryTargetInfo oFlexibleQueryTarget = CreateFlexibleQueryTarget(pFlexibleQueryType);

		try {
			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastBbs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			conn = DbConnect("CastBbs.GetPageCount");
			string sSql = string.Format("SELECT COUNT(BBS_SEQ) AS ROW_COUNT FROM {0}_CAST_BBS00 P ",sSuffix);
			for (int i = 0; i < pCondition.attrValue.Count; i++) {
				if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
					sSql += string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ", i + 1);
				}
			}
			
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget, "", pSiteCd, pLoginId, pUserCharNo, false, pCondition, ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		
		if(pFlexibleQueryType == FlexibleQueryType.None && oFlexibleQueryTarget.HasTarget){
			decimal dExcludeRecCount = decimal.Zero;
			GetPageCountBase(FlexibleQueryType.InScope, pSiteCd, pLoginId, pUserCharNo, pRecPerPage, pUseMV, out dExcludeRecCount, pCondition);
			iPages = CalcExcludeCount(ref pRecCount, dExcludeRecCount, pRecPerPage);
		}
		return iPages;
	}
	
	#endregion ========================================================================================================

	private void GetOrder(string pLoginId,out string pOrder) {
		string sOrderSpec = string.Empty;
		if (!pLoginId.Equals(string.Empty)) {
			sOrderSpec = sOrderSpec + "P.LOGIN_ID,";
		}
		pOrder = string.Format("ORDER BY P.SITE_CD,P.NA_FLAG,P.DEL_FLAG,{0}P.BBS_SEQ DESC ",sOrderSpec);
	}

	public DataSet GetPageCollectionBySeq(string pBbsSeq,bool pUseMV) {
		return GetPageCollectionBase(pBbsSeq,"","","",0,0,pUseMV, new SeekCondition());
	}

	public DataSet GetPageCollection(string pSiteCd, string pLoginId, string pUserCharNo, int pPageNo, int pRecPerPage, bool pUseMV) {
		return GetPageCollectionBase("", pSiteCd, pLoginId, pUserCharNo, pPageNo, pRecPerPage, pUseMV, new SeekCondition());
	}

	public DataSet GetPageCollection(string pSiteCd, string pLoginId, string pUserCharNo, int pPageNo, int pRecPerPage, bool pUseMV, SeekCondition pSeekCondition) {
		return GetPageCollectionBase("", pSiteCd, pLoginId, pUserCharNo, pPageNo, pRecPerPage, pUseMV, pSeekCondition);
	}

	public DataSet GetPageCollectionBase(string pBbsSeq,string pSiteCd,string pLoginId,string pUserCharNo,int pPageNo,int pRecPerPage,bool pUseMV,SeekCondition pCondition) {
		pUseMV = true;
		DataSet ds;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.None);
		
		try {
			conn = DbConnect("CastBbs.GetPageCollectionBase");
			ds = new DataSet();

			string sOrder;
			GetOrder(pLoginId,out sOrder);

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewCastBbs"]).Equals("1") && sessionObj.sexCd.Equals(ViCommConst.MAN)) {
				sSuffix = "MV";
			}

			StringBuilder sInnerSql = new StringBuilder();
			sInnerSql.Append("SELECT  * FROM( ");
			sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT " + CastBasicField() + ",").AppendLine();
			sInnerSql.Append("			BBS_SEQ				,").AppendLine();
			sInnerSql.Append("			UNIQUE_VALUE		,").AppendLine();
			sInnerSql.Append("			BBS_TITLE			,").AppendLine();
			sInnerSql.Append("			BBS_DOC				,").AppendLine();
			sInnerSql.Append("			READING_COUNT		,").AppendLine();
			sInnerSql.Append("			CREATE_DATE			").AppendLine();
			sInnerSql.Append(string.Format("	FROM {0}_CAST_BBS00 P ",sSuffix));

			for (int i = 0; i < pCondition.attrValue.Count; i++) {
				if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
					sInnerSql.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ", i + 1));
				}
			}

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget, pBbsSeq, pSiteCd, pLoginId, pUserCharNo, true, pCondition, ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			if (pBbsSeq.Equals(string.Empty)) {
				sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sInnerSql.Append(")INNER ) ");
			}

			SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();
			sSql = string.Format(sSql,sInnerSql,"","","");

			string sHint = string.Empty;
			if (oObj.sqlHint.ContainsKey("CAST")) {
				sHint = oObj.sqlHint["CAST"].ToString();
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				if (pBbsSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW", (pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
					cmd.Parameters.Add("FIRST_ROW", (pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW", (pPageNo * pRecPerPage) + this.GetFlexibleQueryBuffer());
				}
				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_BBS");

				}
			}
		} finally {
			conn.Close();
		}
		ds = this.Exclude(ds, pRecPerPage, oFlexibleQueryTarget);

		AppendAttr(ds);
		
		return ds;
	}

	private void AppendAttr(DataSet pDS) {
		SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
		string sManUserSeq = "";
		if (oObj is SessionMan) {
			sManUserSeq = ((SessionMan)oObj).userMan.userSeq;
		}
		string sNewDays = oObj.site.newFaceDays.ToString();
		string sNewMark = oObj.site.newFaceMark;
		string sSql;
		if (sNewDays.Equals("")) {
			sNewDays = "0";
		}
		string sNewDay = DateTime.Now.AddDays(-1 * int.Parse(sNewDays)).ToString("yyyy/MM/dd");

		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD		AND " +
						"USER_SEQ		= :USER_SEQ		AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();
			
			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	private OracleParameter[] CreateWhere(FlexibleQueryTargetInfo pFlexibleQueryTarget, string pBbsSeq, string pSiteCd, string pLoginId, string pUserCharNo, bool bAllInfo, SeekCondition pCondition, ref string pWhere) {		
		ArrayList list = new ArrayList();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}
		if (!pBbsSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.BBS_SEQ = :BBS_SEQ",ref pWhere);
			list.Add(new OracleParameter("BBS_SEQ",pBbsSeq));

			SysPrograms.SqlAppendWhere("P.DEL_FLAG = :BBS_SEDEL_FLAGQ",ref pWhere);
			list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));
		} else {
			if (pSiteCd.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
				list.Add(new OracleParameter("SITE_CD",pSiteCd));
			}
			SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG",ref pWhere);
			list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));

			SysPrograms.SqlAppendWhere("P.DEL_FLAG = :DEL_FLAG",ref pWhere);
			list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));

			if (pLoginId.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.LOGIN_ID = :LOGIN_ID",ref pWhere);
				SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginId));
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		using (ManageCompany oManageCompany = new ManageCompany())　{
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}
		
		if (this.sessionObj.logined && this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			pWhere += SetFlexibleQuery(pFlexibleQueryTarget,oSessionMan.site.siteCd, oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list);
		}
		
		CreateAttrQuery(pCondition, ref pWhere, ref list);

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void CreateAttrQuery(SeekCondition pCondition, ref string pWhere, ref ArrayList pList) {
		string sQuery = string.Empty;
		for (int i = 0; i < pCondition.attrValue.Count; i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.SITE_CD			= P.SITE_CD					", i + 1), ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_SEQ			= P.USER_SEQ				", i + 1), ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO			", i + 1), ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0}	", i + 1), ref pWhere);
				pList.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}", i + 1), pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						sQuery = " ( ";
						for (int k = 0; k < sValue.Length; k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									sQuery += " OR ";
								}
								sQuery += string.Format(" (AT{0}.CAST_ATTR_INPUT_VALUE	LIKE '%' || :CAST_ATTR_INPUT_VALUE{0}_{1} || '%')	", i + 1, k);
								pList.Add(new OracleParameter(string.Format("CAST_ATTR_INPUT_VALUE{0}_{1}", i + 1, k), sValue[k]));
							}
						}
						sQuery += ")";
						SysPrograms.SqlAppendWhere(sQuery, ref pWhere);
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');

					if (sValues.Length > 1) {
						sQuery = string.Format(" AT{0}.CAST_ATTR_SEQ IN ( ", i + 1);
						for (int j = 0; j < sValues.Length; j++) {
							if (j != 0) {
								sQuery += " , ";
							}
							sQuery += string.Format(" :CAST_ATTR_SEQ{0}_{1}	", i + 1, j);
							pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}_{1}", i + 1, j), sValues[j]));
						}
						sQuery += " ) ";
						SysPrograms.SqlAppendWhere(sQuery, ref pWhere);
					} else {
						if (pCondition.notEqualFlag.Count > i && pCondition.notEqualFlag[i].Equals("1")) {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ <> :CAST_ATTR_SEQ{0} ", i + 1), ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.CAST_ATTR_SEQ = :CAST_ATTR_SEQ{0} ", i + 1), ref pWhere);
						}
						pList.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}", i + 1), pCondition.attrValue[i]));
					}
				} else {
					SysPrograms.SqlAppendWhere(string.Format(" AT{0}.GROUPING_CD	= :GROUPING_CD{0}	", i + 1), ref pWhere);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}", i + 1), pCondition.attrValue[i]));
				}
			}
		}
	}

	private FlexibleQueryTargetInfo CreateFlexibleQueryTarget(FlexibleQueryType pFlexibleQueryType) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(pFlexibleQueryType);
		if (this.sessionObj.logined && this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			oFlexibleQueryTarget.JealousyBbs = true;
		}
		return oFlexibleQueryTarget;
	}



	public void WriteBbs(string pSiteCd,string pUserSeq,string pUserCharNo,string pBbsTitle,string pBbsDoc) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_CAST_BBS");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PBBS_TITLE",DbSession.DbType.VARCHAR2,pBbsTitle);
			db.ProcedureInParm("PBBS_DOC",DbSession.DbType.VARCHAR2,pBbsDoc);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void DeleteBbs(string pSiteCd,string pUserSeq,string pUserCharno,string pBbsSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_CAST_BBS");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharno);
			db.ProcedureInParm("PBBS_SEQ",DbSession.DbType.VARCHAR2,pBbsSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateReadingCount(string pSiteCd,string pUserSeq,string pUserCharno,string pBbsSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_BBS_COUNT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharno);
			db.ProcedureInParm("PBBS_SEQ",DbSession.DbType.VARCHAR2,pBbsSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public int GetNewlyArrived(string pSiteCd,string pUserSeq,string pUserCharNo,int pHour) {
		DataSet ds;
		int iFlag = ViCommConst.FLAG_OFF;
		try {
			conn = DbConnect("CastBbs.GetNewlyArrived");
			ds = new DataSet();

			string sSql = "SELECT " +
							" COUNT(*) CNT " +
							"FROM " +
							" T_CAST_BBS " +
							"WHERE " +
							" SITE_CD		=  :SITE_CD			AND " +
							" USER_SEQ		=  :USER_SEQ		AND " +
							" USER_CHAR_NO	=  :USER_CHAR_NO	AND " +
							" CREATE_DATE	>= :CREATE_DATE		AND" +
							" DEL_FLAG		=  :DEL_FLAG		";


			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("CREATE_DATE",OracleDbType.Date,DateTime.Now.AddHours(pHour * -1),ParameterDirection.Input);
				cmd.Parameters.Add("DEL_FLAG",ViCommConst.FLAG_OFF);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_BBS");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow drSub = ds.Tables[0].Rows[0];
						iFlag = int.Parse(drSub["CNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return iFlag;
	}
	public DateTime? GetLastWriteDate(string pSiteCd, string pUserSeq, string pUserCharNo) {
		return GetLastWriteDate(pSiteCd, pUserSeq, pUserCharNo, true);
	}
	public DateTime? GetLastWriteDate(string pSiteCd,string pUserSeq,string pUserCharNo,bool pNotDelFlag) {
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT													").AppendLine();
		oSqlBuilder.Append(" 	MAX(T_CAST_BBS.CREATE_DATE) AS LAST_WRITE_DATE		").AppendLine();
		oSqlBuilder.Append(" FROM													").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS											").AppendLine();
		oSqlBuilder.Append(" WHERE													").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.SITE_CD			=	:SITE_CD		AND ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_SEQ			=	:USER_SEQ		AND	").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_CHAR_NO		=	:USER_CHAR_NO		").AppendLine();
		if (pNotDelFlag) {
			oSqlBuilder.Append(" AND	T_CAST_BBS.DEL_FLAG			=	:DEL_FLAG			").AppendLine();
			oSqlBuilder.Append(" AND	T_CAST_BBS.ADMIN_DEL_FLAG	=	:ADMIN_DEL_FLAG		").AppendLine();
		}
		oSqlBuilder.Append(" GROUP BY                                      ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.SITE_CD			,              ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_SEQ			,              ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_CHAR_NO                    ").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO", pUserCharNo);
				if (pNotDelFlag) {
					cmd.Parameters.Add(":DEL_FLAG", ViCommConst.FLAG_OFF_STR);
					cmd.Parameters.Add(":ADMIN_DEL_FLAG", ViCommConst.FLAG_OFF_STR);
				}

				return (DateTime?)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}
	}

	public int GetWriteCntEachDay(string pSiteCd,string pUserSeq,string pUserCharNo,DateTime pTargetDate) {
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT												").AppendLine();
		oSqlBuilder.Append(" 	COUNT(*) AS TODAY_WRITE_CNT						").AppendLine();
		oSqlBuilder.Append(" FROM												").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS										").AppendLine();
		oSqlBuilder.Append(" WHERE												").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.SITE_CD		=	:SITE_CD					AND ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_SEQ		=	:USER_SEQ					AND ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_CHAR_NO	=	:USER_CHAR_NO				AND ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.CREATE_DATE 	>= 	TRUNC(:TARGET_DATE)			AND ").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.CREATE_DATE 	< 	TRUNC(:TARGET_DATE) + 1").AppendLine();
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add(":TARGET_DATE",OracleDbType.Date,pTargetDate,ParameterDirection.Input);

				return int.Parse(cmd.ExecuteScalar().ToString());
			}
		} finally {
			conn.Close();
		}
	}

	public int GetWriteCount(string pSiteCd, string pUserSeq, string pUserCharNo) {
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT													").AppendLine();
		oSqlBuilder.Append(" 	COUNT(*) AS ROWS_COUNT								").AppendLine();
		oSqlBuilder.Append(" FROM													").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS											").AppendLine();
		oSqlBuilder.Append(" WHERE													").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.SITE_CD			=	:SITE_CD		AND	").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_SEQ			=	:USER_SEQ		AND	").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.USER_CHAR_NO		=	:USER_CHAR_NO	AND	").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.DEL_FLAG			=	:DEL_FLAG		AND	").AppendLine();
		oSqlBuilder.Append(" 	T_CAST_BBS.ADMIN_DEL_FLAG	=	:ADMIN_DEL_FLAG		").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":USER_SEQ", pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO", pUserCharNo);
				cmd.Parameters.Add(":DEL_FLAG", ViCommConst.FLAG_OFF_STR);
				cmd.Parameters.Add(":ADMIN_DEL_FLAG", ViCommConst.FLAG_OFF_STR);

				return int.Parse(cmd.ExecuteScalar().ToString());
			}
		} finally {
			conn.Close();
		}
	}

}
