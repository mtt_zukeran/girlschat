﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using MobileLib;
using ViComm;

[System.Serializable]
public class User:DbSession {
	public string loginId;
	public string loginPassword;
	public string sexCd;
	public string userStatus;
	public string adminUserFlag;
	public string useCrosmileFlag;
	public string mobileCarrierCd;
	public string gcappLastLoginVersion;
	public string useVoiceappFlag;

	/// <summary>自動退会処理開始日時</summary>
	private const string START_AUTO_WITHDRAWAL_DATE = "2017-01-09 10:00:00";
	private const string START_AUTO_WITHDRAWAL_DATE_FORMAT = "YYYY-MM-DD HH24:MI:SS";

	public User() {
		loginId = "";
		loginPassword = "";
		sexCd = "";
		userStatus = "";
		adminUserFlag = ViCommConst.FLAG_OFF_STR;
		gcappLastLoginVersion = string.Empty;
	}

	public bool GetOne(string pUserSeq,string pSexCd) {
		bool bExist = false;
		int iUserSeq,iSexCd;
		DataSet ds;
		DataRow dr;

		if (string.IsNullOrEmpty(pUserSeq) || string.IsNullOrEmpty(pSexCd)) {
			return false;
		}

		if (!int.TryParse(pUserSeq,out iUserSeq) || !int.TryParse(pSexCd,out iSexCd)) {
			return false;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT							").AppendLine();
		oSqlBuilder.Append("	SEX_CD					,	").AppendLine();
		oSqlBuilder.Append("	USER_STATUS				,	").AppendLine();
		oSqlBuilder.Append("	LOGIN_ID				,	").AppendLine();
		oSqlBuilder.Append("	LOGIN_PASSWORD			,	").AppendLine();
		oSqlBuilder.Append("	MOBILE_CARRIER_CD		,	").AppendLine();
		oSqlBuilder.Append("	USE_CROSMILE_FLAG		,	").AppendLine();
		oSqlBuilder.Append("	ADMIN_FLAG				,	").AppendLine();
		oSqlBuilder.Append("	GCAPP_LAST_LOGIN_VERSION,	").AppendLine();
		oSqlBuilder.Append("	USE_VOICEAPP_FLAG			").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		oSqlBuilder.Append("     T_USER						").AppendLine();
		oSqlBuilder.Append(" WHERE							").AppendLine();
		oSqlBuilder.Append("     USER_SEQ	= :USER_SEQ AND  ").AppendLine();
		oSqlBuilder.Append("     SEX_CD		= :SEX_CD   ").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						loginId = dr["LOGIN_ID"].ToString();
						loginPassword = dr["LOGIN_PASSWORD"].ToString();
						sexCd = dr["SEX_CD"].ToString();
						userStatus = dr["USER_STATUS"].ToString();
						mobileCarrierCd = dr["MOBILE_CARRIER_CD"].ToString();
						adminUserFlag = dr["ADMIN_FLAG"].ToString();
						useCrosmileFlag = dr["USE_CROSMILE_FLAG"].ToString();
						gcappLastLoginVersion = dr["GCAPP_LAST_LOGIN_VERSION"].ToString();
						useVoiceappFlag = dr["USE_VOICEAPP_FLAG"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public void UpdateMobileInfo(string pUserSeq,string pCarrierCd,string pMobileTerminalNm) {
		if (SessionObjs.Current.adminFlg)
			return;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_USER_MOBILE_INFO");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pMOBILE_CARRIER_CD",DbSession.DbType.VARCHAR2,pCarrierCd);
			db.ProcedureInParm("pMOBILE_TERMINAL_NM",DbSession.DbType.VARCHAR2,pMobileTerminalNm);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

	}

	public bool GetUserSexByIModeId(string pImodeId,string pSiteCd) {
		bool bExist = false;
		DataSet ds;
		DataRow dr;

		if (pImodeId.Equals(string.Empty)) {
			return bExist;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT							").AppendLine();
		oSqlBuilder.Append("	SEX_CD					,	").AppendLine();
		oSqlBuilder.Append("	USER_STATUS				,	").AppendLine();
		oSqlBuilder.Append("	LOGIN_ID				,	").AppendLine();
		oSqlBuilder.Append("	LOGIN_PASSWORD			,	").AppendLine();
		oSqlBuilder.Append("	ADMIN_FLAG					").AppendLine();
		oSqlBuilder.Append("FROM							").AppendLine();
		oSqlBuilder.Append("	VW_SITE_USER01				").AppendLine();
		oSqlBuilder.Append("WHERE							").AppendLine();
		oSqlBuilder.Append("	IMODE_ID			= :IMODE_ID	AND	").AppendLine();
		oSqlBuilder.Append("	SITE_CD				= :SITE_CD	AND	").AppendLine();
		oSqlBuilder.Append("	NOT_USED_EASY_LOGIN	= :NOT_USED_EASY_LOGIN	").AppendLine();
		oSqlBuilder.Append("ORDER BY SEX_CD,USER_STATUS").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add(":IMODE_ID",pImodeId);
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":NOT_USED_EASY_LOGIN",ViCommConst.FLAG_OFF);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE_USER01");
					if (ds.Tables["VW_SITE_USER01"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						loginId = dr["LOGIN_ID"].ToString();
						loginPassword = dr["LOGIN_PASSWORD"].ToString();
						sexCd = dr["SEX_CD"].ToString();
						userStatus = dr["USER_STATUS"].ToString();
						adminUserFlag = dr["ADMIN_FLAG"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		if (bExist && !sexCd.Equals(ViCommConst.MAN)) {
			sexCd = ViCommConst.WOMAN;
		}

		return bExist;
	}


	public bool RegistSiteByImodeId(string pSiteCd,string pImodeId,string pSexCd,out string pUserSeq) {
		bool bExist = false;
		pUserSeq = string.Empty;
		DataSet ds;
		DataRow dr;

		if (pImodeId.Equals(string.Empty)) {
			return bExist;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT							").AppendLine();
		oSqlBuilder.Append("    SEX_CD						,").AppendLine();
		oSqlBuilder.Append("    USER_SEQ					").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		oSqlBuilder.Append("    VW_SITE_USER01				").AppendLine();
		oSqlBuilder.Append(" WHERE							").AppendLine();
		if (!pSiteCd.Equals(string.Empty)) {
			oSqlBuilder.Append("SITE_CD		= :SITE_CD	AND	").AppendLine();
		}
		oSqlBuilder.Append("    IMODE_ID	= :IMODE_ID	AND	").AppendLine();
		oSqlBuilder.Append("    SEX_CD		= :SEX_CD	AND	").AppendLine();
		oSqlBuilder.Append("	USER_STATUS	!=:USER_STATUS	").AppendLine();
		oSqlBuilder.Append(" ORDER BY SEX_CD,USER_STATUS	").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				if (!pSiteCd.Equals(string.Empty)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}
				cmd.Parameters.Add("IMODE_ID",pImodeId);
				cmd.Parameters.Add("SEX_CD",pSexCd);
				if (pSexCd.Equals(ViCommConst.MAN)) {
					cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_MAN_HOLD);
				} else {
					cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_WOMAN_HOLD);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						pUserSeq = dr["USER_SEQ"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	public bool RegistSiteByUtn(string pSiteCd,string pUtn,string pSexCd,out string pUserSeq) {
		bool bExist = false;
		pUserSeq = string.Empty;
		DataSet ds;
		DataRow dr;

		if (pUtn.Equals(string.Empty)) {
			return bExist;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT				").AppendLine();
		oSqlBuilder.Append("    SEX_CD			,").AppendLine();
		oSqlBuilder.Append("    USER_SEQ		").AppendLine();
		oSqlBuilder.Append(" FROM				").AppendLine();
		oSqlBuilder.Append("    VW_SITE_USER01	").AppendLine();
		oSqlBuilder.Append(" WHERE				").AppendLine();
		if (!pSiteCd.Equals(string.Empty)) {
			oSqlBuilder.Append("SITE_CD		= :SITE_CD	AND	").AppendLine();
		}
		oSqlBuilder.Append("    TERMINAL_UNIQUE_ID	= :TERMINAL_UNIQUE_ID	AND	").AppendLine();
		oSqlBuilder.Append("    SEX_CD				= :SEX_CD				AND	").AppendLine();
		oSqlBuilder.Append("	USER_STATUS			!=:USER_STATUS				").AppendLine();
		oSqlBuilder.Append(" ORDER BY SEX_CD,USER_STATUS	").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				if (!pSiteCd.Equals(string.Empty)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}
				cmd.Parameters.Add("TERMINAL_UNIQUE_ID",pUtn);
				cmd.Parameters.Add("SEX_CD",pSexCd);
				if (pSexCd.Equals(ViCommConst.MAN)) {
					cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_MAN_HOLD);
				} else {
					cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_WOMAN_HOLD);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						pUserSeq = dr["USER_SEQ"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	public void ResetMailNg(string pUserSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RESET_MAIL_NG");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public string GetLoginIdByBeforeSystemId(string pBeforeSystemId,string pSexCd) {
		string sLoginId = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT			").AppendLine();
		oSqlBuilder.Append("    LOGIN_ID	").AppendLine();
		oSqlBuilder.Append(" FROM			").AppendLine();
		oSqlBuilder.Append("    T_USER		").AppendLine();
		oSqlBuilder.Append(" WHERE			").AppendLine();
		oSqlBuilder.Append("    BEFORE_SYSTEM_ID	= :BEFORE_SYSTEM_ID	AND	").AppendLine();
		oSqlBuilder.Append("    SEX_CD				= :SEX_CD				").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("BEFORE_SYSTEM_ID",pBeforeSystemId);
				cmd.Parameters.Add("SEX_CD",pSexCd);
				
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sLoginId = dr["LOGIN_ID"].ToString();

					}
				}
			}
		} finally {
			conn.Close();
		}
		return sLoginId;
	}

	public bool IsAdminUserByLoginID(string pSiteCd, string pLoginID, string pPassword) {
		DataSet ds;
		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT 1 FROM DUAL WHERE EXISTS (SELECT * FROM VW_SITE_USER01 WHERE SITE_CD = :SITE_CD AND LOGIN_ID = :LOGIN_ID AND LOGIN_PASSWORD = :LOGIN_PASSWORD AND ADMIN_FLAG = :ADMIN_FLAG)");

		bool bExist = false;
		conn = DbConnect();

		using (cmd = CreateSelectCommand(sSql.ToString(), conn))
		using (ds = new DataSet()) {
			cmd.BindByName = true;
			cmd.Parameters.Add(":SITE_CD", pSiteCd);
			cmd.Parameters.Add(":LOGIN_ID", pLoginID);
			cmd.Parameters.Add(":LOGIN_PASSWORD", pPassword);
			cmd.Parameters.Add(":ADMIN_FLAG", ViCommConst.FLAG_ON_STR);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "VW_SITE_USER01");
				bExist = (ds.Tables["VW_SITE_USER01"].Rows.Count != 0);
			}
		}
		conn.Close();
		return bExist;
	}


	public bool IsAdminUserByTel(string pSiteCd, string pTel, string pPassword) {
		DataSet ds;
		StringBuilder sSql = new StringBuilder();
		sSql.Append("SELECT 1 FROM DUAL WHERE EXISTS (SELECT * FROM VW_SITE_USER01 WHERE SITE_CD = :SITE_CD AND TEL = :TEL AND TEL_ATTESTED_FLAG = :TEL_ATTESTED_FLAG AND LOGIN_PASSWORD = :LOGIN_PASSWORD AND ADMIN_FLAG = :ADMIN_FLAG)");

		bool bExist = false;
		conn = DbConnect();

		using (cmd = CreateSelectCommand(sSql.ToString(), conn))
		using (ds = new DataSet()) {
			cmd.BindByName = true;
			cmd.Parameters.Add(":SITE_CD", pSiteCd);
			cmd.Parameters.Add(":TEL", pTel);
			cmd.Parameters.Add(":TEL_ATTESTED_FLAG", ViCommConst.FLAG_ON_STR);
			cmd.Parameters.Add(":LOGIN_PASSWORD", pPassword);
			cmd.Parameters.Add(":ADMIN_FLAG", ViCommConst.FLAG_ON_STR);
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds, "VW_SITE_USER01");
				bExist = (ds.Tables["VW_SITE_USER01"].Rows.Count != 0);
			}
		}
		conn.Close();
		return bExist;
	}

	public string GetUseTerminalType(string pUserSeq) {
		string sTerminalType = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT					").AppendLine();
		oSqlBuilder.Append("    USE_TERMINAL_TYPE	").AppendLine();
		oSqlBuilder.Append(" FROM					").AppendLine();
		oSqlBuilder.Append("    T_USER				").AppendLine();
		oSqlBuilder.Append(" WHERE					").AppendLine();
		oSqlBuilder.Append("    USER_SEQ	= :USER_SEQ	").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sTerminalType = dr["USE_TERMINAL_TYPE"].ToString();

					}
				}
			}
		} finally {
			conn.Close();
		}
		return sTerminalType;
	}

	public string GetUseCrosmileFlag(string pUserSeq) {
		string sUseCrosmileFlag = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT					").AppendLine();
		oSqlBuilder.Append("    USE_CROSMILE_FLAG	").AppendLine();
		oSqlBuilder.Append(" FROM					").AppendLine();
		oSqlBuilder.Append("    T_USER				").AppendLine();
		oSqlBuilder.Append(" WHERE					").AppendLine();
		oSqlBuilder.Append("    USER_SEQ	= :USER_SEQ	").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sUseCrosmileFlag = dr["USE_CROSMILE_FLAG"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sUseCrosmileFlag;
	}

	public string GetCrosmileLastUsedVersion(string pUserSeq) {
		string sLastVersion = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT							").AppendLine();
		oSqlBuilder.Append("    CROSMILE_LAST_USED_VERSION	").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		oSqlBuilder.Append("    T_USER						").AppendLine();
		oSqlBuilder.Append(" WHERE							").AppendLine();
		oSqlBuilder.Append("    USER_SEQ	= :USER_SEQ		").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sLastVersion = dr["CROSMILE_LAST_USED_VERSION"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sLastVersion;
	}

	public string GetTalkDirectSmartNotMonitor(string pUserSeq) {
		string sValue = string.Empty;
		DataSet ds;
		DataRow dr;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT								").AppendLine();
		oSqlBuilder.Append("    TALK_SMART_DIRECT_NOT_MONITOR	").AppendLine();
		oSqlBuilder.Append(" FROM								").AppendLine();
		oSqlBuilder.Append("    T_USER							").AppendLine();
		oSqlBuilder.Append(" WHERE								").AppendLine();
		oSqlBuilder.Append("    USER_SEQ	= :USER_SEQ			").AppendLine();

		try {
			conn = this.DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						sValue = dr["TALK_SMART_DIRECT_NOT_MONITOR"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sValue;
	}

	public void CheckCastUtnExist(
		string pTerminalUniqueId,
		string pIModeId,
		out int pExistFlag
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_CAST_UTN_EXIST");
			db.ProcedureInParm("pTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,pTerminalUniqueId);
			db.ProcedureInParm("pIMODE_ID",DbSession.DbType.VARCHAR2,pIModeId);
			db.ProcedureOutParm("pEXIST_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			pExistFlag = db.GetIntValue("pEXIST_FLAG");
		}
	}
	
	public DataSet GetOneByTwitterId (string pTwitterId) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	LOGIN_ID					,	");
		oSqlBuilder.AppendLine("	LOGIN_PASSWORD				,	");
		oSqlBuilder.AppendLine("	SEX_CD							");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_USER							");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	TWITTER_ID	= :TWITTER_ID		");
		
		oParamList.Add(new OracleParameter(":TWITTER_ID",pTwitterId));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public void UpdateDeviceToken(string pUserSeq,string pDeviceToken) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_DEVICE_TOKEN");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pDEVICE_TOKEN",DbSession.DbType.VARCHAR2,pDeviceToken);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}
	
	public void CertifyNotificationApp(string pSiteCd,string pUserSeq,string pDeviceUuid) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CERTIFY_NOTIFICATION_APP");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pDEVICE_UUID",DbSession.DbType.VARCHAR2,pDeviceUuid);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pDeviceUuid = db.GetStringValue("pDEVICE_UUID");
		}
	}
	
	public void UpdateSmartPhoneTel(string pSiteCd,string pUserSeq,string pTel) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_SMART_PHONE_TEL");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,pTel);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	public DataSet GetOneBySnsAccountId(string pSnsAccountId,string pSnsType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	LOGIN_ID							,	");
		oSqlBuilder.AppendLine("	LOGIN_PASSWORD						,	");
		oSqlBuilder.AppendLine("	SEX_CD									");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_USER									");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SNS_ACCOUNT_ID	= :SNS_ACCOUNT_ID	AND	");
		oSqlBuilder.AppendLine("	SNS_TYPE		= :SNS_TYPE				");

		oParamList.Add(new OracleParameter(":SNS_ACCOUNT_ID",pSnsAccountId));
		oParamList.Add(new OracleParameter(":SNS_TYPE",pSnsType));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public string CheckEmailAddrDuplication(string pSiteCd,string pEmailAddr) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_EMAIL_ADDR_DUPLICATION");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,pEmailAddr);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
			
			return db.GetStringValue("pRESULT");
		}
	}

	/// <summary>
	/// 電話番号、性別CDを元にログインパスワードを取得する
	/// </summary>
	/// <param name="pTel"></param>
	/// <param name="pSexCd"></param>
	/// <returns></returns>
	public DataSet GetOneByTel(string pTel,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	LOGIN_PASSWORD,");
		oSqlBuilder.AppendLine("	USER_STATUS");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	TEL = :TEL");
		oSqlBuilder.AppendLine("	AND SEX_CD = :SEX_CD");

		oParamList.Add(new OracleParameter(":TEL",pTel));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	/// <summary>
	/// 会員状態、退会申請状態を取得する
	/// </summary>
	/// <param name="pUserSeq"></param>
	/// <param name="pSexCd"></param>
	/// <returns></returns>
	public DataSet GetWithdrawalStatus(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sStatus = ViCommConst.WITHDRAWAL_STATUS_ACCEPT;

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	U.USER_STATUS,");
		oSqlBuilder.AppendLine("	U.LAST_WITHDRAWAL_DETAIN_DATE,");
		oSqlBuilder.AppendLine("	NVL(T.FLAG, 0) AS EXISTS_ACCEPT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER U LEFT JOIN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			USER_SEQ,");
		oSqlBuilder.AppendLine("			1 AS FLAG");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_ACCEPT_WITHDRAWAL");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("			AND USER_SEQ = :USER_SEQ");
		oSqlBuilder.AppendLine("			AND USER_CHAR_NO = :USER_CHAR_NO");
		oSqlBuilder.AppendLine("			AND WITHDRAWAL_STATUS = :WITHDRAWAL_STATUS");
		oSqlBuilder.AppendLine("			AND NVL(MANUAL_WITHDRAWAL_FLAG, 0) = 0");
		oSqlBuilder.AppendLine("			AND UPDATE_DATE >= TO_DATE(:UPDATE_DATE, :UPDATE_DATE_FORMAT)");
		oSqlBuilder.AppendLine("	) T ON U.USER_SEQ = T.USER_SEQ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":WITHDRAWAL_STATUS",sStatus));
		oParamList.Add(new OracleParameter(":UPDATE_DATE",START_AUTO_WITHDRAWAL_DATE));
		oParamList.Add(new OracleParameter(":UPDATE_DATE_FORMAT",START_AUTO_WITHDRAWAL_DATE_FORMAT));

		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	U.USER_SEQ = :USER_SEQ");

		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
