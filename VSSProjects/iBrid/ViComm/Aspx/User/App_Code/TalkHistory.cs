﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会話履歴
--	Progaram ID		: TalkHistory
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class TalkHistory:DbSession {

	public TalkHistory() {
	}

	public int GetTalkCount(string pSiteCd,string pManUserSeq,string pManUserChar,string pCastUserSeq,string pCastCharNo) {
		DataSet ds;
		DataRow dr;
		int iRecCount = 0;
		try {
			conn = DbConnect("TalkHistory.GetTalkCount");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_TALK_HISTORY " +
							"WHERE " +
								"SITE_CD				= :SITE_CD			AND " +
								"USER_SEQ				= :USER_SEQ			AND " +
								"USER_CHAR_NO			= :USER_CHAR_NO		AND " +
								"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ AND " +
								"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pManUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pManUserChar);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pCastUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pCastCharNo);

				using (ds = new DataSet())
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					dr = ds.Tables[0].Rows[0];
					if (ds.Tables[0].Rows.Count != 0) {
						iRecCount = int.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iRecCount;
	}

	public void ModifyTalkHistoryMemo(string pTalkSeq,string pMemo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_TALK_HISTORY_MEMO");
			db.ProcedureInParm("PTALK_SEQ",DbSession.DbType.VARCHAR2,pTalkSeq);
			db.ProcedureInParm("PMEMO",DbSession.DbType.VARCHAR2,pMemo);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void DeleteTalkHistory(string pTalkSeq,string pUserSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TALK_HISTORY_DEL");
			db.ProcedureInParm("PTALK_SEQ",DbSession.DbType.VARCHAR2,pTalkSeq);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void DeleteTalkAllHistory(string pTalkSeq,string pUserSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TALK_HISTORY_DEL_ALL");
			db.ProcedureInParm("PTALK_SEQ",DbSession.DbType.VARCHAR2,pTalkSeq);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}

