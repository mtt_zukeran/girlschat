﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ポイント還元スケジュール


--	Progaram ID		: PointReductionSchdule
--
--  Creation Date	: 2011.09.29
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class PointReductionSchdule : DbSession {
	public PointReductionSchdule() {
	}

	public DataSet GetOne(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	PRS.SITE_CD						,");
		oSqlBuilder.AppendLine("	PRS.APPLICATION_START_DATE		,");
		oSqlBuilder.AppendLine("	PRS.APPLICATION_END_DATE		,");
		oSqlBuilder.AppendLine("	PRS.POINT_REDUCTION_TYPE		 ");
		oSqlBuilder.AppendLine("FROM								 ");
		oSqlBuilder.AppendLine("	T_POINT_REDUCTION_SCHDULE	PRS	 ");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE								 ");
		oWhereBuilder.AppendLine("	PRS.SITE_CD	= :SITE_CD			 ");

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(string.Format("{0}{1}", oSqlBuilder.ToString(), oWhereBuilder.ToString()), conn)) {
				cmd.BindByName = true;

				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}

		return oDataSet;
		
	}

	public void GetReductionInfo(string pSiteCd, string pUserSeq, out int? pReductionReceiptSumAmt,out string pReductionPointType) {
		pReductionReceiptSumAmt = null;
		pReductionPointType = string.Empty;
		DataSet oDataset = this.GetOne(pSiteCd);
		
		if (oDataset.Tables[0].Rows.Count <= 0) { return; }
		pReductionReceiptSumAmt = 0;
		DataRow oDatarow = oDataset.Tables[0].Rows[0];

		pReductionPointType = oDatarow["POINT_REDUCTION_TYPE"].ToString();

		DateTime oStartDate = DateTime.Parse(oDatarow["APPLICATION_START_DATE"].ToString());
		DateTime oEndDate = DateTime.Parse(oDatarow["APPLICATION_END_DATE"].ToString());
		if (oStartDate.CompareTo(DateTime.Now) < 0 && DateTime.Now.CompareTo(oEndDate) < 0) {
			pReductionReceiptSumAmt = this.GetReceiptAmtBetweenReductionSchdule(pSiteCd, pUserSeq, oStartDate, oEndDate);

		}
	}

	private int GetReceiptAmtBetweenReductionSchdule(string pSiteCd, string pUserSeq, DateTime pStartDate, DateTime pEndDate) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	NVL(SUM(RECEIPT_AMT),0) AS SUM_AMT	");
		oSqlBuilder.AppendLine("FROM									 ");
		oSqlBuilder.AppendLine("	T_RECEIPT							 ");
		oSqlBuilder.AppendLine("WHERE									 ");
		oSqlBuilder.AppendLine("	SITE_CD		 = :SITE_CD			AND	 ");
		oSqlBuilder.AppendLine("	USER_SEQ	 = :USER_SEQ		AND	 ");
		oSqlBuilder.AppendLine("	CREATE_DATE	>= :START_DATE		AND	 ");
		oSqlBuilder.AppendLine("	CREATE_DATE	<= :END_DATE			 ");


		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;

				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter(":USER_SEQ", pUserSeq));
				cmd.Parameters.Add(new OracleParameter(":START_DATE", OracleDbType.Date, pStartDate, ParameterDirection.Input));
				cmd.Parameters.Add(new OracleParameter(":END_DATE", OracleDbType.Date, pEndDate, ParameterDirection.Input));

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					if (oDataSet.Tables[0].Rows.Count > 0) {
						return int.Parse(oDataSet.Tables[0].Rows[0]["SUM_AMT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return 0;
	}
}
