﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 演出カテゴリー
--	Progaram ID		: ActCategory
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using ViComm;

[System.Serializable]
public class ActCategory:DbSession {

	public string categoryNm;
	public string webFaceSeq;
	public string actCategorySeq;
	public string colorBack;
	public string colorChar;
	public string colorLine;
	public string colorLink;
	public string colorALink;
	public string colorVLink;
	public int sizeLine;
	public int sizeChar;

	public ActCategory() {
		categoryNm = string.Empty;
		webFaceSeq = string.Empty;
		actCategorySeq = string.Empty;
	}

	public ActCategory(string pSiteCd,int pPriority) {
		categoryNm = string.Empty;
		webFaceSeq = string.Empty;
		actCategorySeq = string.Empty;
		colorBack = string.Empty;
		colorChar = string.Empty;
		colorLine = string.Empty;
		colorLink = string.Empty;
		colorALink = string.Empty;
		colorVLink = string.Empty;
		sizeLine = 0;
		sizeChar = 0;
		GetOne(pSiteCd, pPriority);
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect("ActCategory.GetList");
			ds = new DataSet();

			string sSql = "SELECT " +
								"SITE_CD			," +
								"ACT_CATEGORY_SEQ	," +
								"ACT_CATEGORY_NM	," +
								"PRIORITY			," +
								"HTML_DOC			," +
								"WEB_FACE_SEQ		," +
								"DISPLAY_FLAG " +
							"FROM " +
								"T_ACT_CATEGORY " +
							"WHERE " +
								"SITE_CD = :SITE_CD AND NA_FLAG = :NA_FLAG AND ACT_CATEGORY_SEQ != :ACT_CATEGORY_SEQ";

			sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.Add("NA_FLAG", ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ", ViCommConst.DEFAULT_ACT_CATEGORY_SEQ);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public bool GetOne(string pSiteCd,string pCategorySeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect("ActCategory.GetOne");

			string sSql = "SELECT ACT_CATEGORY_SEQ,WEB_FACE_SEQ,ACT_CATEGORY_NM FROM T_ACT_CATEGORY WHERE SITE_CD = :SITE_CD AND ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",pCategorySeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ACT_CATEGORY");

					if (ds.Tables["T_ACT_CATEGORY"].Rows.Count != 0) {
						dr = ds.Tables["T_ACT_CATEGORY"].Rows[0];
						categoryNm = dr["ACT_CATEGORY_NM"].ToString();
						webFaceSeq = dr["WEB_FACE_SEQ"].ToString();
						actCategorySeq = dr["ACT_CATEGORY_SEQ"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public bool GetOne(string pSiteCd,int pPriority) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect("ActCategory.GetOne2");

			string sSql = "SELECT " +
							"ACT_CATEGORY_SEQ	," +
							"WEB_FACE_SEQ		," +
							"COLOR_LINE			," +
							"COLOR_INDEX		," +
							"COLOR_CHAR			," +
							"COLOR_BACK			," +
							"COLOR_LINK			," +
							"COLOR_ALINK		," +
							"COLOR_VLINK		," +
							"SIZE_LINE			," +
							"SIZE_CHAR			," +
							"COLOR_LINE_WOMAN	," +
							"COLOR_INDEX_WOMAN	," +
							"COLOR_CHAR_WOMAN	," +
							"COLOR_BACK_WOMAN	," +
							"COLOR_LINK_WOMAN	," +
							"COLOR_ALINK_WOMAN	," +
							"COLOR_VLINK_WOMAN	," +
							"SIZE_LINE_WOMAN	," +
							"SIZE_CHAR_WOMAN	," +
							"ACT_CATEGORY_NM	 " +
						"FROM " +
							"VW_ACT_CATEGORY01 " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND PRIORITY = :PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PRIORITY",pPriority);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ACT_CATEGORY");

					if (ds.Tables["T_ACT_CATEGORY"].Rows.Count != 0) {
						dr = ds.Tables["T_ACT_CATEGORY"].Rows[0];
						categoryNm = dr["ACT_CATEGORY_NM"].ToString();
						webFaceSeq = dr["WEB_FACE_SEQ"].ToString();
						actCategorySeq = dr["ACT_CATEGORY_SEQ"].ToString();
						if(SessionObjs.CurrentSexCd.Equals(ViComm.ViCommConst.MAN)){
							colorBack = dr["COLOR_BACK"].ToString();
							colorChar = dr["COLOR_CHAR"].ToString();
							colorLine = dr["COLOR_LINE"].ToString();
							colorLink = dr["COLOR_LINK"].ToString();
							colorALink = dr["COLOR_ALINK"].ToString();
							colorVLink = dr["COLOR_VLINK"].ToString();
							sizeChar = int.Parse(dr["SIZE_CHAR"].ToString());
							sizeLine = int.Parse(dr["SIZE_LINE"].ToString());
						}else{
							colorBack = dr["COLOR_BACK_WOMAN"].ToString();
							colorChar = dr["COLOR_CHAR_WOMAN"].ToString();
							colorLine = dr["COLOR_LINE_WOMAN"].ToString();
							colorLink = dr["COLOR_LINK_WOMAN"].ToString();
							colorALink = dr["COLOR_ALINK_WOMAN"].ToString();
							colorVLink = dr["COLOR_VLINK_WOMAN"].ToString();
							sizeChar = int.Parse(dr["SIZE_CHAR_WOMAN"].ToString());
							sizeLine = int.Parse(dr["SIZE_LINE_WOMAN"].ToString());
						}
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public bool GetCategorySeq(string pSiteCd,int pPriority) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect("ActCategory.GetOne2");

			string sSql = "SELECT " +
							"ACT_CATEGORY_SEQ " +
						"FROM " +
							"T_ACT_CATEGORY " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND PRIORITY = :PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PRIORITY",pPriority);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ACT_CATEGORY");

					if (ds.Tables["T_ACT_CATEGORY"].Rows.Count != 0) {
						dr = ds.Tables["T_ACT_CATEGORY"].Rows[0];
						actCategorySeq = dr["ACT_CATEGORY_SEQ"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public string GetPrimaryCategorySeq(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		string sSeq = "0";
		try{
			conn = DbConnect("ActCategory.GetPrimaryCategorySeq");

			string sSql = "SELECT ACT_CATEGORY_SEQ FROM T_ACT_CATEGORY WHERE SITE_CD = :SITE_CD AND ACT_CATEGORY_SEQ != :ACT_CATEGORY_SEQ ORDER BY PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",ViCommConst.DEFAULT_ACT_CATEGORY_SEQ);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ACT_CATEGORY");
					if (ds.Tables["T_ACT_CATEGORY"].Rows.Count != 0) {
						dr = ds.Tables["T_ACT_CATEGORY"].Rows[0];
						sSeq = dr["ACT_CATEGORY_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sSeq;
	}

}
