﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 録音
--	Progaram ID		: Recording
--
--  Creation Date	: 2010.02.23
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using ViComm;

[System.Serializable]
public class Recording:DbSession {

	public Recording() {
	}

	public int GetRecordingCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pRecType) {
		DataSet ds;

		string sSql = "SELECT USER_SEQ FROM T_RECORDING ";
		try{
			conn = DbConnect("Recording.GetRecordingCount");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerUserSeq,pPartnerUserCharNo,pRecType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				da.Fill(ds,"T_RECORDING");
			}
		} finally {
			conn.Close();
		}
		return ds.Tables["T_RECORDING"].Rows.Count;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pRecType,ref string pWhere) {
		ArrayList list = new ArrayList();

		iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}

		if (!pUserCharNo.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		}

		if (!pPartnerUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pPartnerUserSeq));
		}

		if (!pPartnerUserCharNo.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pPartnerUserCharNo));
		}

		if (!pRecType.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("REC_TYPE = :REC_TYPE",ref pWhere);
			list.Add(new OracleParameter("REC_TYPE",pRecType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void RecordingMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo,string pRecType,string pFowardTelNo,int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECORDING_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureInParm("PREC_SEQ",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PREC_TYPE",DbSession.DbType.NUMBER,int.Parse(pRecType));
			db.ProcedureInParm("PFOWARD_TEL_NO",DbSession.DbType.VARCHAR2,pFowardTelNo);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public string GetLastComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerSiteCd,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		string sComment;

		string sSql = "SELECT WAITING_COMMENT FROM T_RECORDING ";
		try{
			conn = DbConnect("Recording.GetRecordingCount");

			sSql += "WHERE SITE_CD		= :SITE_CD		AND " +
						" USER_SEQ		= :USER_SEQ		AND " +
						" USER_CHAR_NO	= :USER_CHAR_NO AND " +
						" REC_TYPE		= :REC_TYPE ";

			if (pPartnerSiteCd.Equals("")) {
				sSql += " AND PARTNER_SITE_CD IS NULL ";
			} else {
				sSql += " AND PARTNER_SITE_CD = :PARTNER_SITE ";
			}

			if (pPartnerUserSeq.Equals("")) {
				sSql += " AND PARTNER_USER_SEQ IS NULL ";
			} else {
				sSql += " AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ ";
			}

			if (pPartnerUserCharNo.Equals("")) {
				sSql += " AND PARTNER_USER_CHAR_NO IS NULL ";
			} else {
				sSql += " AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO ";
			}

			sSql += " ORDER BY REC_SEQ DESC ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("REC_TYPE",ViCommConst.REC_TYPE_MAN_WAITING);

				if (!pPartnerSiteCd.Equals("")) {
					cmd.Parameters.Add("PARTNER_SITE_CD",pPartnerSiteCd);
				}
				if (!pPartnerUserSeq.Equals("")) {
					cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				}
				if (!pPartnerUserCharNo.Equals("")) {
					cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);
				}
				this.Fill(da,ds,"T_RECORDING");
			}
		} finally {
			conn.Close();
		}
		if (ds.Tables["T_RECORDING"].Rows.Count > 0) {
			sComment = ds.Tables["T_RECORDING"].Rows[0]["WAITING_COMMENT"].ToString();
		} else {
			sComment = "";
		}

		return sComment;
	}

	public bool WaitingNow(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerSiteCd,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		bool bExist = false;

		string sSql = "SELECT REC_SEQ FROM T_RECORDING ";
		try{
			conn = DbConnect("Recording.GetRecordingCount");

			sSql += "WHERE SITE_CD		= :SITE_CD		AND " +
						" USER_SEQ		= :USER_SEQ		AND " +
						" USER_CHAR_NO	= :USER_CHAR_NO AND " +
						" REC_TYPE		= :REC_TYPE ";

			if (pPartnerSiteCd.Equals("")) {
				sSql += " AND PARTNER_SITE_CD IS NULL ";
			} else {
				sSql += " AND PARTNER_SITE_CD = :PARTNER_SITE ";
			}

			if (pPartnerUserSeq.Equals("")) {
				sSql += " AND PARTNER_USER_SEQ IS NULL ";
			} else {
				sSql += " AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ ";
			}

			if (pPartnerUserCharNo.Equals("")) {
				sSql += " AND PARTNER_USER_CHAR_NO IS NULL ";
			} else {
				sSql += " AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO ";
			}

			sSql += " AND FOWARD_TERM_DATE > :FOWARD_TERM_DATE ";

			sSql += " ORDER BY REC_SEQ DESC ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("REC_TYPE",ViCommConst.REC_TYPE_MAN_WAITING);

				if (!pPartnerSiteCd.Equals("")) {
					cmd.Parameters.Add("PARTNER_SITE_CD",pPartnerSiteCd);
				}
				if (!pPartnerUserSeq.Equals("")) {
					cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				}
				if (!pPartnerUserCharNo.Equals("")) {
					cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);
				}
				cmd.Parameters.Add("FOWARD_TERM_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				da.Fill(ds,"T_RECORDING");
			}
		} finally {
			conn.Close();
		}
		if (ds.Tables["T_RECORDING"].Rows.Count > 0) {
			bExist = true;
		}

		return bExist;
	}
	
	//後何分で待機終了
	public int GetWaitingEndMin(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int min = 0;
		DataSet ds;

		try {
			conn = DbConnect("Recording.GetWaitingEndMin");

			StringBuilder sql = new StringBuilder();
			sql.AppendLine("SELECT	");
			sql.AppendLine("	TRUNC((FOWARD_TERM_DATE - SYSDATE) * 1440) AS WAITING_MIN");
			sql.AppendLine("FROM	");
			sql.AppendLine("	T_RECORDING		");
			sql.AppendLine("WHERE	");
			sql.AppendLine("	SITE_CD			= :SITE_CD		AND	");
			sql.AppendLine("	USER_SEQ		= :USER_SEQ		AND ");
			sql.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO AND ");
			sql.AppendLine("	REC_TYPE		= :REC_TYPE		AND	");
			sql.AppendLine("	FOWARD_TERM_DATE> :FOWARD_TERM_DATE ");
			sql.AppendLine("ORDER BY");
			sql.AppendLine("	REC_SEQ DESC	");

			using (cmd = CreateSelectCommand(sql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("REC_TYPE",ViCommConst.REC_TYPE_MAN_WAITING);
				cmd.Parameters.Add("FOWARD_TERM_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input);
				da.Fill(ds,"T_RECORDING");
			}
		} finally {
			conn.Close();
		}
		if (ds.Tables["T_RECORDING"].Rows.Count > 0) {
			int.TryParse(ds.Tables["T_RECORDING"].Rows[0]["WAITING_MIN"].ToString(),out min);
		}

		return min;
	}

}
