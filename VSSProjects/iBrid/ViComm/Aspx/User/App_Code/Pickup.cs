﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップ
--	Progaram ID		: Pickup
--
--  Creation Date	: 2010.10.26
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;

using ViComm;
using iBridCommLib;

[System.Serializable]
public class Pickup:DbSession {

	public string siteCd = string.Empty;
	public string pickupId = string.Empty;
	public long pickupMask = 0;
	public bool pickupFlag = false;
	public string pickupType = string.Empty;
	public DateTime publishStartDate;
	public DateTime publishEndDate;

	public Pickup() {
	}

	public string GetTopPagePickupId(string pSiteCd) {
		string sPickupId = string.Empty;
		DataSet ds;

		StringBuilder sSql = new StringBuilder();

		sSql.Append("SELECT											").AppendLine();
		sSql.Append("	PICKUP_ID									").AppendLine();
		sSql.Append("FROM											").AppendLine();
		sSql.Append("	T_PICKUP									").AppendLine();
		sSql.Append("WHERE											").AppendLine();
		sSql.Append("	SITE_CD				= :SITE_CD			AND	").AppendLine();
		sSql.Append("	PICKUP_FLAG			= :PICKUP_FLAG		AND	").AppendLine();
		sSql.Append("	USE_TOP_PAGE_FLAG	= :USE_TOP_PAGE_FLAG	").AppendLine();

		try {
			conn = DbConnect("Pickup.GetTopPagePickupId");


			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PICKUP_FLAG", ViCommConst.FLAG_ON_STR);
				cmd.Parameters.Add("USE_TOP_PAGE_FLAG",ViCommConst.FLAG_ON);

				da.Fill(ds,"T_PICKUP");
				if (ds.Tables["T_PICKUP"].Rows.Count != 0) {
					sPickupId = ds.Tables["T_PICKUP"].Rows[0]["PICKUP_ID"].ToString();
				}
			}
		} finally {
			conn.Close();
		}
		return sPickupId;
	}
	
	public string GetPickupType(string pSiteCd,string sPickupId){
		string sPickupType = string.Empty;
	
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT                                            ").AppendLine();
		oSqlBuilder.Append(" 	PICKUP_TYPE                                    ").AppendLine();
		oSqlBuilder.Append(" FROM                                              ").AppendLine();
		oSqlBuilder.Append(" 	T_PICKUP                                       ").AppendLine();
		oSqlBuilder.Append(" WHERE                                             ").AppendLine();
		oSqlBuilder.Append(" 	T_PICKUP.SITE_CD 	= :SITE_CD 		AND        ").AppendLine();
		oSqlBuilder.Append(" 	T_PICKUP.PICKUP_ID 	= :PICKUP_ID               ").AppendLine();
		
		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PICKUP_ID", sPickupId);
				
				sPickupType = iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		}finally{
			conn.Close();
		}
		
		return sPickupType;
	}


	public bool GetOne(string pSiteCd, string sPickupId) {
		bool bExists = false;
		
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT                                            ").AppendLine();
		oSqlBuilder.Append(" 	SITE_CD					,                      ").AppendLine();
		oSqlBuilder.Append(" 	PICKUP_ID				,		               ").AppendLine();
		oSqlBuilder.Append(" 	PICKUP_MASK				,	                   ").AppendLine();
		oSqlBuilder.Append(" 	PICKUP_FLAG				,	                   ").AppendLine();
		oSqlBuilder.Append(" 	PICKUP_TYPE				,	                   ").AppendLine();
		oSqlBuilder.Append(" 	PUBLISH_START_DATE		,	                   ").AppendLine();
		oSqlBuilder.Append(" 	PUBLISH_END_DATE			                   ").AppendLine();
		oSqlBuilder.Append(" FROM                                              ").AppendLine();
		oSqlBuilder.Append(" 	T_PICKUP                                       ").AppendLine();
		oSqlBuilder.Append(" WHERE                                             ").AppendLine();
		oSqlBuilder.Append(" 	T_PICKUP.SITE_CD 	= :SITE_CD 		AND        ").AppendLine();
		oSqlBuilder.Append(" 	T_PICKUP.PICKUP_ID 	= :PICKUP_ID               ").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PICKUP_ID", sPickupId);
				
				using(OracleDataReader oReader = cmd.ExecuteReader()){
					if(oReader.Read()){
						bExists = true;
						siteCd = iBridUtil.GetStringValue(oReader["SITE_CD"]);
						pickupId = iBridUtil.GetStringValue(oReader["PICKUP_ID"]);
						pickupMask = long.Parse(iBridUtil.GetStringValue(oReader["PICKUP_MASK"]));
						pickupFlag = iBridUtil.GetStringValue(oReader["PICKUP_FLAG"]).Equals("1");
						pickupType = iBridUtil.GetStringValue(oReader["PICKUP_TYPE"]);
						publishStartDate = DateTime.Parse(oReader["PUBLISH_START_DATE"].ToString());
						publishEndDate = DateTime.Parse(oReader["PUBLISH_END_DATE"].ToString());
						
					}
				}
				
			}
		} finally {
			conn.Close();
		}

		return bExists;
	}
}