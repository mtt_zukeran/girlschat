﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員
--	Progaram ID		: Man
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class Man:ManBase {

	public decimal recCount;

	public Man()
		: base() {
	}
	public Man(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public int GetPageCount(
		SeekCondition pCondition,
		int pRecPerPage,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("Man.GetPageCount");
			ds = new DataSet();

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewMan"]).Equals("1")) {
				sSuffix = "MV";
			}

			string sSql = string.Format("SELECT COUNT(*) AS ROW_COUNT FROM {0}_USER_MAN_CHARACTER00 P ",sSuffix);

			switch (pCondition.conditionFlag) {
				case ViCommConst.INQUIRY_FAVORIT:
					sSql = sSql + ",T_FAVORIT F,T_SITE_MANAGEMENT SM,T_CAST_CHARACTER CC,T_FAVORIT_GROUP FG ";
					break;
					
				case ViCommConst.INQUIRY_LIKE_ME:
					sSql = sSql + ",T_FAVORIT F ";
					break;

				case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
					sSql = sSql + ",T_FAVORIT F1,T_FAVORIT F2 ";
					break;

				case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
				case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
					sSql = sSql + ",T_FAVORIT LL ";
					break;

				case ViCommConst.INQUIRY_EXTENSION_MOTE_KING:
					sSql = sSql + ",T_RANKING_EX MK ";
					break;

				case ViCommConst.INQUIRY_MARKING:
					sSql = sSql + ",T_MARKING MK,T_MARKING_REFERER MR";
					break;

				case ViCommConst.INQUIRY_REFUSE:
				case ViCommConst.INQUIRY_REFUSE_ME:
					sSql = sSql + ",T_REFUSE RF ";
					break;

				case ViCommConst.INQUIRY_WAITING_MAN:
					sSql = sSql + ",VW_RECORDING03 RW ";
					break;

				case ViCommConst.INQUIRY_EXTENSION_RICHINO_USER:
					sSql = sSql + ",T_USER_MAN_CHARACTER_EX EX ";
					break;
				case ViCommConst.INQUIRY_MEMO:
					sSql = sSql + ",T_MEMO ME ";
					break;
				case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
					sSql = sSql + ",T_BINGO_ENTRY BE ";
					break;
			}


			if (pCondition.conditionFlag == ViCommConst.INQUIRY_CONDITION || pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						sSql = sSql + string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1);
					}
				}
			}

			if ((pCondition.findFavoriteType.Equals(ViCommConst.FIND_FAVORITE_ONLY))) {
				sSql = sSql + ",T_FAVORIT F ";
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pCondition,false,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				this.Fill(da,ds,"VW_USER_MAN_CHARACTER00");

				if (ds.Tables["VW_USER_MAN_CHARACTER00"].Rows.Count != 0) {
					dr = ds.Tables["VW_USER_MAN_CHARACTER00"].Rows[0];
					recCount = Decimal.Parse(dr["ROW_COUNT"].ToString());
					pRecCount = recCount;
					iPages = (int)Math.Ceiling(recCount / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(
		SeekCondition pCondition,
		int pPageNo,
		int pRecPerPage) {
		return GetPageCollectionBase(pCondition,pPageNo,pRecPerPage,true,"","","","");
	}

	public DataSet GetPageCollectionByNoDetailInfo(
		SeekCondition pCondition,
		int pPageNo,
		int pRecPerPage) {
		return GetPageCollectionBase(pCondition,pPageNo,pRecPerPage,false,"","","","");
	}

	public int GetRNumByUserSeq(
		SeekCondition pCondition,
		string pUserSeq,
		string pUserCharNo
	) {
		DataSet ds = GetPageCollectionBase(pCondition,1,1,false,pUserSeq,pUserCharNo,"","");
		if (ds.Tables[0].Rows.Count > 0) {
			return int.Parse(ds.Tables[0].Rows[0]["RNUM"].ToString());
		} else {
			return 0;
		}
	}

	public int GetRNumByUserSeqAndCondtion(
		SeekCondition pCondition,
		string pUserSeq,
		string pUserCharNo,
		string pConditionFieldNm,
		string pConditionFieldValue
	) {
		DataSet ds = GetPageCollectionBase(pCondition,1,1,false,pUserSeq,pUserCharNo,pConditionFieldNm,pConditionFieldValue);
		if (ds.Tables[0].Rows.Count > 0) {
			return int.Parse(ds.Tables[0].Rows[0]["RNUM"].ToString());
		} else {
			return 0;
		}
	}

	public DataSet GetOneByUserSeqAndCondtion(
		SeekCondition pCondition,
		string pUserSeq,
		string pUserCharNo,
		string pConditionFieldNm,
		string pConditionFieldValue,
		int pRNum
	) {
		DataSet ds = GetPageCollectionBase(pCondition,1,1,true,pUserSeq,pUserCharNo,pConditionFieldNm,pConditionFieldValue);
		if (ds.Tables[0].Rows.Count > 0) {
			ds.Tables[0].Rows[0]["RNUM"] = pRNum;
		}
		return ds;
	}

	public DataSet GetPageCollectionBase(
		SeekCondition pCondition,
		int pPageNo,
		int pRecPerPage,
		bool pGetDetailInfo,
		string pUserSeq,
		string pUserCharNo,
		string pConditionFieldNm,
		string pConditionFieldValue) {
		DataSet ds;
		try {
			conn = DbConnect("Man.GetPageCollection");
			ds = new DataSet();

			string sSuffix = "VW";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewMan"]).Equals("1")) {
				sSuffix = "MV";
			}

			StringBuilder sInnerSql = new StringBuilder();
			sInnerSql.Append("SELECT * FROM ").AppendLine();
			sInnerSql.Append("(SELECT ROWNUM AS RNUM,INNER.* FROM (").AppendLine();
			sInnerSql.Append(string.Format("SELECT " + ManInfoFields(pCondition.conditionFlag) + " FROM {0}_USER_MAN_CHARACTER00 P ",sSuffix));

			string sOrder = " ORDER BY P.SITE_CD,P.LAST_ACTION_DATE DESC ,P.USER_SEQ ";

			switch (pCondition.conditionFlag) {
				case ViCommConst.INQUIRY_FAVORIT:
					sInnerSql.Append(",T_FAVORIT F ");
					sInnerSql.Append(",T_SITE_MANAGEMENT SM ");
					sInnerSql.Append(",T_CAST_CHARACTER CC");
					sInnerSql.Append(",T_FAVORIT_GROUP FG");
					if ("1".Equals(pCondition.sortExType)) {
						sOrder = "ORDER BY P.SITE_CD, F.LAST_COMM_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
					} else if ("2".Equals(pCondition.sortExType)) {
						sOrder = "ORDER BY P.SITE_CD, F.REGIST_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
					} else if ("3".Equals(pCondition.sortExType)) {
						sOrder = "ORDER BY P.SITE_CD, P.LAST_LOGIN_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
					}
					break;

				case ViCommConst.INQUIRY_LIKE_ME:
					sInnerSql.Append(",T_FAVORIT F ");
					if ("1".Equals(pCondition.sortExType)) {
						sOrder = "ORDER BY P.SITE_CD, F.REGIST_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
					} else if ("2".Equals(pCondition.sortExType)) {
						sOrder = "ORDER BY P.SITE_CD, F.LAST_COMM_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
					} else if ("3".Equals(pCondition.sortExType)) {
						sOrder = "ORDER BY P.SITE_CD, P.LAST_LOGIN_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
					}
					break;

				case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
					sInnerSql.Append(",T_FAVORIT F1,T_FAVORIT F2 ");
					if ("1".Equals(pCondition.sortExType)) {
						sOrder = "ORDER BY P.SITE_CD, F1.LAST_COMM_DATE DESC, P.USER_SEQ, P.USER_CHAR_NO";
					}
					break;

				case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
				case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
					sInnerSql.Append(",T_FAVORIT LL ");
					sOrder = " ORDER BY P.SITE_CD,LL.LAST_VIEW_PROFILE_DATE DESC,P.USER_SEQ ";
					break;

				case ViCommConst.INQUIRY_EXTENSION_MOTE_KING:
					sInnerSql.Append(",T_RANKING_EX MK ");
					sOrder = " ORDER BY MK.SITE_CD,MK.SUMMARY_COUNT DESC,MK.USER_SEQ ";
					break;

				case ViCommConst.INQUIRY_MARKING:
					sInnerSql.Append(",T_MARKING MK,T_MARKING_REFERER MR");
					sOrder = " ORDER BY P.SITE_CD,MK.MARKING_DATE DESC,P.USER_SEQ ";
					break;

				case ViCommConst.INQUIRY_REFUSE:
				case ViCommConst.INQUIRY_REFUSE_ME:
					sInnerSql.Append(",T_REFUSE RF ");
					sOrder = " ORDER BY P.SITE_CD,RF.REGIST_DATE DESC,P.USER_SEQ ";
					break;

				case ViCommConst.INQUIRY_WAITING_MAN:
					sInnerSql.Append(",VW_RECORDING03 RW ");
					sOrder = " ORDER BY P.SITE_CD,RW.RECORDING_DATE DESC,P.USER_SEQ ";
					break;

				case ViCommConst.INQUIRY_EXTENSION_RICHINO_USER:
					sInnerSql.Append(",T_USER_MAN_CHARACTER_EX EX ");
					sOrder = " ORDER BY P.SITE_CD,P.LAST_ACTION_DATE ASC ,P.USER_SEQ ";
					//ソート条件が最終日付の昇順なのは仕様 
					break;

				case ViCommConst.INQUIRY_MAN_NEW:
				case ViCommConst.INQUIRY_MAN_LOGINED_NEW:
					sOrder = " ORDER BY P.SITE_CD,P.REGIST_DATE DESC,P.USER_SEQ";
					break;

				case ViCommConst.INQUIRY_MEMO:
					sInnerSql.Append(",T_MEMO ME ");
					sOrder = " ORDER BY P.SITE_CD,ME.UPDATE_DATE DESC,P.USER_SEQ ";
					break;

				case ViCommConst.INQUIRY_MAN_ADD_POINT:
					sOrder = " ORDER BY P.SITE_CD,P.LAST_RECEIPT_DATE DESC,P.USER_SEQ ";
					break;

				case ViCommConst.INQUIRY_CONDITION:
					sOrder = " ORDER BY P.SITE_CD,P.CHARACTER_ONLINE_STATUS DESC,P.LAST_ACTION_DATE DESC ,P.USER_SEQ";
					break;

				case ViCommConst.INQUIRY_MAN_LOGINED:
					if ("1".Equals(pCondition.sortExType)) {
						sOrder = " ORDER BY P.SITE_CD,P.LAST_LOGIN_DATE DESC,P.USER_SEQ ";
					}
					break;

				case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
					sInnerSql.Append(",T_BINGO_ENTRY BE ");
					sOrder = " ORDER BY P.SITE_CD,BE.BINGO_APPLICATION_DATE DESC,P.USER_SEQ ";
					break;
			}
			if (pCondition.conditionFlag == ViCommConst.INQUIRY_CONDITION || pCondition.hasExtend) {
				for (int i = 0;i < pCondition.attrValue.Count;i++) {
					if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
						sInnerSql.Append(string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1));
					}
				}

			}
			//ORDER BYのパラメータがきた場合をパラメータによりORDER BY文を生成する
			if (!pCondition.orderAsc.Equals(string.Empty) || !pCondition.orderDesc.Equals(string.Empty)) {
				sOrder = " ORDER BY P.SITE_CD ";
				if (!pCondition.orderAsc.Equals(string.Empty)) {
					sOrder += "," + pCondition.orderAsc + " ASC ";
				}
				if (!pCondition.orderDesc.Equals(string.Empty)) {
					sOrder += "," + pCondition.orderDesc + " DESC ";
				}
				sOrder += "," + " P.USER_SEQ ";
			}

			if (pCondition.random.Equals(ViCommConst.FLAG_ON)) {
				sOrder = " ORDER BY DBMS_RANDOM.RANDOM";
			}

			if ((pCondition.findFavoriteType.Equals(ViCommConst.FIND_FAVORITE_ONLY))) {
				sInnerSql.Append(",T_FAVORIT F ");
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pCondition,true,ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			if (pUserSeq.Equals(string.Empty)) {
				sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sInnerSql.Append(")INNER ");
				sInnerSql.Append(")WHERE USER_SEQ = :USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO ");
				if (!pConditionFieldNm.Equals("")) {
					sInnerSql.Append(string.Format(" AND {0} = :{0} ",pConditionFieldNm));
				}
			}

			SessionObjs oObj = this.sessionObj;
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "MAN";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			StringBuilder sTables = new StringBuilder();
			StringBuilder sJoinTable = new StringBuilder();
			StringBuilder sJoinField = new StringBuilder();

			sSql = string.Format(sSql,sInnerSql.ToString(),sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				if (pUserSeq.Equals("")) {
					cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				} else {
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
					if (!pConditionFieldNm.Equals("")) {
						cmd.Parameters.Add(pConditionFieldNm,pConditionFieldValue);
					}
				}
				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_USER_MAN_CHARACTER00");
				}
			}
		} finally {
			conn.Close();
		}
		if (pGetDetailInfo == true) {
			AppendManAttr(ds);
		}

		return ds;
	}

	public DataSet GetOne(string pSiteCd,string pUserSeq,int pRecNo) {
		DataSet ds;
		try {
			conn = DbConnect("Man.GetOne");
			ds = new DataSet();

			string sInnerSql = "SELECT " +
									ManInfoFields(0) +
								",'" + pRecNo + "' RNUM " +
							"FROM " +
								"VW_USER_MAN_CHARACTER00 P " +
							"WHERE " +
								"P.SITE_CD		= :SITE_CD	AND " +
								"P.USER_SEQ		= :USER_SEQ	AND	" +
								"P.USER_CHAR_NO	= :USER_CHAR_NO ";

			SessionObjs oObj = this.sessionObj;
			string sSql = oObj.sqlSyntax["MAN"].ToString();
			sSql = string.Format(sSql,sInnerSql,"","","");

			ArrayList list = new ArrayList();
			InnnerCondition(ref list);
			OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);

				// Join Condition
				list.Clear();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER00");
				}
			}
		} finally {
			conn.Close();
		}
		AppendManAttr(ds);

		return ds;
	}

	public DataSet GetOneByLoginId(string pSiteCd,string pLoginId,int pRecNo) {
		DataSet ds;
		try {
			conn = DbConnect("Man.GetOneByLoginId");
			ds = new DataSet();

			string sInnerSql = "SELECT " +
								ManInfoFields(0) +
								"," + pRecNo + " RNUM " +
							"FROM " +
								"VW_USER_MAN_CHARACTER00 P " +
							"WHERE " +
								"P.SITE_CD		= :SITE_CD	AND " +
								"P.LOGIN_ID		= :LOGIN_ID	";

			SessionObjs oObj = this.sessionObj;
			string sSql = oObj.sqlSyntax["MAN"].ToString();
			sSql = string.Format(sSql,sInnerSql,"","","");

			ArrayList list = new ArrayList();
			InnnerCondition(ref list);
			OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("LOGIN_ID",pLoginId);

				// Join Condition
				list.Clear();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER00");
				}
			}
		} finally {
			conn.Close();
		}

		AppendManAttr(ds);

		return ds;
	}

	public bool IsTelephoneRegistered(string pSiteCd,string pTelephoneNo) {
		DataSet ds;
		bool bExist = false;

		try {
			conn = DbConnect("Man.IsTelephoneNoRegistered");
			ds = new DataSet();

			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT		");
			oSql.AppendLine("	TEL			,");
			oSql.AppendLine("	USER_STATUS	");
			oSql.AppendLine("FROM		");
			oSql.AppendLine("	VW_USER_MAN_CHARACTER00 P	");
			oSql.AppendLine("WHERE		");
			if (!pSiteCd.Equals(string.Empty)) {
				oSql.AppendLine("	P.SITE_CD	= :SITE_CD	AND ");
			}
			oSql.AppendLine("P.TEL		= :TEL	");

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				if (!pSiteCd.Equals(string.Empty)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}
				cmd.Parameters.Add("TEL",pTelephoneNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER00");
				}
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	private string ManInfoFields(ulong pConditionFlag) {
		StringBuilder sField = new StringBuilder(ManBasicField());

		sField.Append(",P.RICHINO_RANK	").AppendLine();
		sField.AppendLine(",P.NG_SKULL_COUNT");

		switch (pConditionFlag) {
			case ViCommConst.INQUIRY_MARKING:
				sField.Append(",MK.MARKING_DATE ").AppendLine();
				sField.Append(",MK.REPORT_DAY	MARKING_DAY ");
				sField.Append(",MR.DISPLAY_NM	MARKING_REFERER_DISPLAY_NM");
				break;

			case ViCommConst.INQUIRY_REFUSE:
			case ViCommConst.INQUIRY_REFUSE_ME:
				sField.Append(",RF.REGIST_DATE REFUSE_REGIST_DATE,").AppendLine();
				sField.Append("RF.REFUSE_COMMENT ");
				break;

			case ViCommConst.INQUIRY_FAVORIT:
				sField.Append(",F.FAVORIT_COMMENT					").AppendLine();
				sField.Append(",F.LOGIN_VIEW_STATUS					").AppendLine();
				sField.Append(",F.WAITING_VIEW_STATUS				").AppendLine();
				sField.Append(",F.BBS_NON_PUBLISH_FLAG				").AppendLine();
				sField.Append(",F.RANKING_NON_PUBLISH_FLAG			").AppendLine();
				sField.Append(",F.NOT_SEND_BATCH_MAIL_FLAG			").AppendLine();
				sField.Append(",F.REGIST_DATE FAVORIT_REGIST_DATE	").AppendLine();
				sField.Append(",F.BLOG_NON_PUBLISH_FLAG				").AppendLine();
				sField.Append(",F.DIARY_NON_PUBLISH_FLAG			").AppendLine();
				sField.Append(",F.PROFILE_PIC_NON_PUBLISH_FLAG		").AppendLine();
				sField.Append(",F.USE_COMMENT_DETAIL_FLAG			").AppendLine();
				sField.AppendLine(",F.OFFLINE_LAST_UPDATE_DATE");
				sField.AppendLine(",CASE");
				sField.AppendLine("		WHEN (F.LOGIN_VIEW_STATUS = 1) AND (F.WAITING_VIEW_STATUS = 1) AND (F.OFFLINE_LAST_UPDATE_DATE < SYSDATE) THEN");
				sField.AppendLine("			NVL(F.OFFLINE_LAST_UPDATE_DATE,CC.LAST_ACTION_DATE) - SM.JEALOUSY_EFFECT_DELAY_MIN / 1440");
				sField.AppendLine("		WHEN (F.OFFLINE_LAST_UPDATE_DATE > SYSDATE) THEN");
				sField.AppendLine("			F.OFFLINE_LAST_UPDATE_DATE - SM.JEALOUSY_EFFECT_DELAY_MIN / 1440");
				sField.AppendLine("		ELSE");
				sField.AppendLine("			CC.LAST_ACTION_DATE");
				sField.AppendLine("		END");
				sField.AppendLine("	AS SELF_LAST_ACTION_DATE");
				sField.AppendLine(",F.FAVORIT_GROUP_SEQ		");
				sField.AppendLine(",FG.FAVORIT_GROUP_NM		");
				break;

			case ViCommConst.INQUIRY_LIKE_ME:
				sField.AppendLine(",F.REGIST_DATE AS FAVORIT_REGIST_DATE");
				break;

			case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
			case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
				sField.Append(",LL.LAST_VIEW_PROFILE_DATE	").AppendLine();
				break;

			case ViCommConst.INQUIRY_EXTENSION_MOTE_KING:
				sField.Append(",ROWNUM AS RANKING, MK.SUMMARY_COUNT ").AppendLine();
				break;

			case ViCommConst.INQUIRY_WAITING_MAN:
				sField.Append(",RW.RECORDING_DATE		").AppendLine();	//待機開始時間 
				sField.Append(",RW.FOWARD_TERM_SCH_DATE	").AppendLine();	//待機終了予定時間 
				sField.Append(",RW.WAITING_COMMENT		").AppendLine();	//待機コメント 
				sField.Append(",RW.WAITING_TYPE_NM		");					//待機種別 
				break;

			case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
				sField.Append(",BE.BINGO_APPLICATION_DATE	").AppendLine();
				sField.Append(",BE.BINGO_ELECTION_AMT		").AppendLine();
				sField.Append(",BE.BINGO_BALL_FLAG			").AppendLine();
				break;
		}

		return sField.ToString();
	}

	private OracleParameter[] CreateWhere(SeekCondition pCondition,bool bAllInfo,ref string pWhere) {

		ArrayList list = new ArrayList();
		if (bAllInfo) {
			InnnerCondition(ref list);
		}

		if (pCondition.conditionFlag != 0) {

			switch (pCondition.conditionFlag) {
				case ViCommConst.INQUIRY_FAVORIT:
					SysPrograms.SqlAppendWhere("F.SITE_CD		= :SITE_CD				 ",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_SEQ		= :USER_SEQ				 ",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_CHAR_NO	= :USER_CHAR_NO			 ",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= F.SITE_CD				 ",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= F.PARTNER_USER_SEQ	 ",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= F.PARTNER_USER_CHAR_NO ",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= SM.SITE_CD			 ",ref pWhere);
					SysPrograms.SqlAppendWhere("F.SITE_CD		= CC.SITE_CD			 ",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_SEQ		= CC.USER_SEQ			 ",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_CHAR_NO	= CC.USER_CHAR_NO		 ",ref pWhere);
					if (!string.IsNullOrEmpty(pCondition.favoritGroupSeq)) {
						if (pCondition.favoritGroupSeq.Equals("null")) {
							SysPrograms.SqlAppendWhere("F.FAVORIT_GROUP_SEQ	IS NULL ",ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere("F.FAVORIT_GROUP_SEQ = :FAVORIT_GROUP_SEQ ",ref pWhere);
						}
					}
					SysPrograms.SqlAppendWhere("F.SITE_CD			= FG.SITE_CD (+)			",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_SEQ			= FG.USER_SEQ (+)			",ref pWhere);
					SysPrograms.SqlAppendWhere("F.USER_CHAR_NO		= FG.USER_CHAR_NO (+)		",ref pWhere);
					SysPrograms.SqlAppendWhere("F.FAVORIT_GROUP_SEQ	= FG.FAVORIT_GROUP_SEQ (+)	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					if (!string.IsNullOrEmpty(pCondition.favoritGroupSeq) && !pCondition.favoritGroupSeq.Equals("null")) {
						list.Add(new OracleParameter("FAVORIT_GROUP_SEQ",pCondition.favoritGroupSeq));
					}
					break;

				case ViCommConst.INQUIRY_LIKE_ME:
					SysPrograms.SqlAppendWhere("F.SITE_CD				= :SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("F.PARTNER_USER_SEQ		= :USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("F.PARTNER_USER_CHAR_NO	= :USER_CHAR_NO	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= F.SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= F.USER_SEQ	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= F.USER_CHAR_NO",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					using (ManageCompany oManageCompany = new ManageCompany()) {
						if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_LIKE_ME,2)) {
							SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = F.SITE_CD AND USER_SEQ = F.PARTNER_USER_SEQ AND USER_CHAR_NO = F.PARTNER_USER_CHAR_NO AND PARTNER_USER_SEQ = F.USER_SEQ AND PARTNER_USER_CHAR_NO = F.USER_CHAR_NO)",ref pWhere);
						}
					}
					break;

				case ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST:
					SysPrograms.SqlAppendWhere("F1.SITE_CD				= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("F1.USER_SEQ				= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("F1.USER_CHAR_NO			= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= F1.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= F1.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= F1.PARTNER_USER_CHAR_NO	",ref pWhere);
					SysPrograms.SqlAppendWhere("F2.SITE_CD				= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("F2.PARTNER_USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("F2.PARTNER_USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= F2.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= F2.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= F2.USER_CHAR_NO			",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_EXTENSION_LOVE_LIST:
					SysPrograms.SqlAppendWhere("LL.SITE_CD					= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_SEQ			= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_CHAR_NO		= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD					= LL.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ					= LL.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO				= LL.USER_CHAR_NO			",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.LAST_VIEW_PROFILE_DATE	> :LAST_VIEW_PROFILE_DATE	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					list.Add(new OracleParameter("LAST_VIEW_PROFILE_DATE",OracleDbType.Date,DateTime.Now.AddDays(int.Parse(sessionObj.site.loveListProfileDays) * -1),ParameterDirection.Input));
					break;

				case ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST:
					SysPrograms.SqlAppendWhere("LL.SITE_CD					= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_SEQ			= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.PARTNER_USER_CHAR_NO		= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD					= LL.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ					= LL.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO				= LL.USER_CHAR_NO			",ref pWhere);
					SysPrograms.SqlAppendWhere("LL.LAST_VIEW_PROFILE_DATE	<= :LAST_VIEW_PROFILE_DATE	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					list.Add(new OracleParameter("LAST_VIEW_PROFILE_DATE",OracleDbType.Date,DateTime.Now.AddDays(int.Parse(sessionObj.site.loveListProfileDays) * -1),ParameterDirection.Input));
					break;

				case ViCommConst.INQUIRY_EXTENSION_MOTE_KING:
					SysPrograms.SqlAppendWhere("MK.SITE_CD			= P.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.SUMMARY_TYPE		= :SUMMARY_TYPE			",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.USER_SEQ			= P.USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.USER_CHAR_NO		= P.USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.RANKING_CTL_SEQ	= :RANKING_CTL_SEQ		",ref pWhere);
					list.Add(new OracleParameter("SUMMARY_TYPE",ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN));
					list.Add(new OracleParameter("RANKING_CTL_SEQ",pCondition.rankingCtlSeq));
					break;

				case ViCommConst.INQUIRY_MARKING:
					SysPrograms.SqlAppendWhere("MK.SITE_CD				= :PARTNER_SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.PARTNER_USER_SEQ		= :PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= MK.SITE_CD			",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= MK.USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= MK.USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.SITE_CD				= MR.SITE_CD(+)				",ref pWhere);
					SysPrograms.SqlAppendWhere("MK.MARKING_REFERER_SEQ	= MR.MARKING_REFERER_SEQ(+)	",ref pWhere);
					
					list.Add(new OracleParameter("PARTNER_SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("PARTNER_USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_MAN_NEW:
				case ViCommConst.INQUIRY_MAN_LOGINED_NEW:
					if (pCondition.newManDay != 0) {
						DateTime Dt = DateTime.Today.AddDays(pCondition.newManDay * -1);
						SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE",OracleDbType.Date,Dt,ParameterDirection.Input));
					}
					break;

				case ViCommConst.INQUIRY_REFUSE:
					SysPrograms.SqlAppendWhere("RF.SITE_CD		= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("RF.USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("RF.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= RF.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= RF.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= RF.PARTNER_USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_REFUSE_ME:
					SysPrograms.SqlAppendWhere("RF.SITE_CD				= :SITE_CD			",ref pWhere);
					SysPrograms.SqlAppendWhere("RF.PARTNER_USER_SEQ		= :USER_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("RF.PARTNER_USER_CHAR_NO	= :USER_CHAR_NO		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD				= RF.SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ				= RF.USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO			= RF.USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_WAITING_MAN:
					SysPrograms.SqlAppendWhere("RW.SITE_CD			= :SITE_CD			",ref pWhere);
					SysPrograms.SqlAppendWhere("RW.REC_TYPE			= :REC_TYPE			",ref pWhere);
					SysPrograms.SqlAppendWhere("RW.FOWARD_TERM_DATE	> :FOWARD_TERM_DATE	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD			= RW.SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ			= RW.USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO		= RW.USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("REC_TYPE",ViCommConst.REC_TYPE_MAN_WAITING));
					list.Add(new OracleParameter("FOWARD_TERM_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input));
					break;

				case ViCommConst.INQUIRY_EXTENSION_RICHINO_USER:
					SysPrograms.SqlAppendWhere("EX.RICHINO_RANK		= :RICHINO_RANK		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD			= EX.SITE_CD		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ			= EX.USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO		= EX.USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("RICHINO_RANK",pCondition.richinoRank));
					break;

				case ViCommConst.INQUIRY_MEMO:
					SysPrograms.SqlAppendWhere("ME.SITE_CD		= :SITE_CD					",ref pWhere);
					SysPrograms.SqlAppendWhere("ME.USER_SEQ		= :USER_SEQ					",ref pWhere);
					SysPrograms.SqlAppendWhere("ME.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD		= ME.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ		= ME.PARTNER_USER_SEQ		",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO	= ME.PARTNER_USER_CHAR_NO	",ref pWhere);
					list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
					list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
					list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
					break;

				case ViCommConst.INQUIRY_MAN_ADD_POINT:
					SysPrograms.SqlAppendWhere("P.LAST_RECEIPT_DATE > :LAST_RECEIPT_DATE		",ref pWhere);
					System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
					oSqlBuilder.Append(" AND EXISTS(								      ").AppendLine();
					oSqlBuilder.Append(" SELECT 1 FROM T_COMMUNICATION                     ").AppendLine();
					oSqlBuilder.Append(" WHERE                                             ").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.SITE_CD					= P.SITE_CD 					AND ").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ					AND").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO				AND").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.PARTNER_USER_SEQ		= :PARTNER_USER_SEQ_ADD_P		AND").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO_ADD_P	AND").AppendLine();
					oSqlBuilder.Append(" (	T_COMMUNICATION.LAST_TALK_DATE			> :LAST_TALK_DATE				OR").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.LAST_TX_MAIL_DATE		> :LAST_TX_MAIL_DATE			)").AppendLine();
					oSqlBuilder.Append(" )	                                                ").AppendLine();

					pWhere += oSqlBuilder.ToString();
					SessionObjs oObj = this.sessionObj;
					int iBuyPointMailCommDays = 0;
					int.TryParse(oObj.site.buyPointMailCommDays,out iBuyPointMailCommDays);

					DateTime DtLastReceipt = DateTime.Today.AddDays(-1);
					DateTime DtLastCommunication = DateTime.Now.AddDays(iBuyPointMailCommDays * -1);
					list.Add(new OracleParameter("LAST_RECEIPT_DATE",OracleDbType.Date,DtLastReceipt,ParameterDirection.Input));
					list.Add(new OracleParameter(":PARTNER_USER_SEQ_ADD_P",pCondition.userSeq));
					list.Add(new OracleParameter(":PARTNER_USER_CHAR_NO_ADD_P",pCondition.userCharNo));
					list.Add(new OracleParameter("LAST_TALK_DATE",OracleDbType.Date,DtLastCommunication,ParameterDirection.Input));
					list.Add(new OracleParameter("LAST_TX_MAIL_DATE",OracleDbType.Date,DtLastCommunication,ParameterDirection.Input));
					break;

				case ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER:
					SysPrograms.SqlAppendWhere("BE.BINGO_TERM_SEQ			= :BINGO_TERM_SEQ			",ref pWhere);
					SysPrograms.SqlAppendWhere("BE.BINGO_APPLICATION_FLAG	= :BINGO_APPLICATION_FLAG	",ref pWhere);
					SysPrograms.SqlAppendWhere("P.SITE_CD					= BE.SITE_CD				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_SEQ					= BE.USER_SEQ				",ref pWhere);
					SysPrograms.SqlAppendWhere("P.USER_CHAR_NO				= BE.USER_CHAR_NO			",ref pWhere);
					list.Add(new OracleParameter("BINGO_TERM_SEQ",pCondition.bingoTermSeq));
					list.Add(new OracleParameter("BINGO_APPLICATION_FLAG",ViCommConst.FLAG_ON));
					break;

				case ViCommConst.INQUIRY_CONDITION:
					if (!pCondition.handleNm.Equals("")) {
						SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE '%' || :HANDLE_NM || '%' ",ref pWhere);
						list.Add(new OracleParameter("HANDLE_NM",pCondition.handleNm));
					}
					if (pCondition.newManDay != 0) {
						DateTime Dt = DateTime.Today.AddDays(pCondition.newManDay * -1);
						SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE	",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE",OracleDbType.Date,Dt,ParameterDirection.Input));
					}
					if (!pCondition.ageFrom.Equals("")) {
						SysPrograms.SqlAppendWhere("P.AGE >= :AGE_FROM ",ref pWhere);
						list.Add(new OracleParameter("AGE_FROM",pCondition.ageFrom));
					}
					if (!pCondition.ageTo.Equals("")) {
						SysPrograms.SqlAppendWhere("P.AGE <= :AGE_TO ",ref pWhere);
						list.Add(new OracleParameter("AGE_TO",pCondition.ageTo));
					}
					if (!pCondition.birthDayFrom.Equals("")) {
						SysPrograms.SqlAppendWhere("P.BIRTHDAY_MMDD >= :BIRTHDAY_MMDD_FROM",ref pWhere);
						list.Add(new OracleParameter("BIRTHDAY_MMDD_FROM",pCondition.birthDayFrom));
					}
					if (!pCondition.birthDayTo.Equals("")) {
						SysPrograms.SqlAppendWhere("P.BIRTHDAY_MMDD <= :BIRTHDAY_MMDD_TO ",ref pWhere);
						list.Add(new OracleParameter("BIRTHDAY_MMDD_TO",pCondition.birthDayTo));
					}
					if (!pCondition.registDateFrom.Equals(string.Empty)) {
						DateTime dtFrom = DateTime.Parse(pCondition.registDateFrom + " 00:00:00");
						SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE_FROM",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
					}
					if (!pCondition.registDateTo.Equals(string.Empty)) {
						DateTime dtTo = DateTime.Parse(pCondition.registDateTo + " 23:59:59").AddSeconds(1);
						SysPrograms.SqlAppendWhere(" P.REGIST_DATE < :REGIST_DATE_TO",ref pWhere);
						list.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
					}

					if (pCondition.notSendBatchMailFlag == 1) {
						SysPrograms.SqlAppendWhere("(P.NON_EXIST_MAIL_ADDR_FLAG = 0 OR P.LAST_LOGIN_DATE > SYSDATE-60)",ref pWhere);
					} else {
						SysPrograms.SqlAppendWhere("(P.NON_EXIST_MAIL_ADDR_FLAG = 0 OR P.LAST_LOGIN_DATE > SYSDATE-90)",ref pWhere);
					}
					
					CreateAttrQuery(pCondition,ref pWhere,ref list);
					break;
			}
		}

		if (!string.IsNullOrEmpty(pCondition.lastLoginDate)) {
			SysPrograms.SqlAppendWhere("P.LAST_LOGIN_DATE >= :LAST_LOGIN_DATE ",ref pWhere);
			list.Add(new OracleParameter("LAST_LOGIN_DATE",OracleDbType.Date,DateTime.Parse(pCondition.lastLoginDate),ParameterDirection.Input));
		}
		if (!string.IsNullOrEmpty(pCondition.targetMailMax)) {
			SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_MAN_RX_CAST_MAIL_COUNT WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND (YESTERDAY_COUNT > :YESTERDAY_COUNT OR TODAY_COUNT > :TODAY_COUNT)) ",ref pWhere);
			list.Add(new OracleParameter("YESTERDAY_COUNT",pCondition.targetMailMax));
			list.Add(new OracleParameter("TODAY_COUNT",pCondition.targetMailMax));
		}
		if (!string.IsNullOrEmpty(pCondition.targetFlag) && pCondition.targetFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("(P.LAST_LOGIN_DATE >= SYSDATE - 1 OR (P.NON_EXIST_MAIL_ADDR_FLAG = :NON_EXIST_MAIL_ADDR_FLAG AND P.CAST_MAIL_RX_TYPE = :CAST_MAIL_RX_TYPE)) ",ref pWhere);
			int iNonExistMailAddrFlag = 0;
			list.Add(new OracleParameter("NON_EXIST_MAIL_ADDR_FLAG",iNonExistMailAddrFlag));
			list.Add(new OracleParameter("CAST_MAIL_RX_TYPE",ViCommConst.MAIL_RX_BOTH));
		}
		if ((pCondition.findFavoriteType.Equals(ViCommConst.FIND_FAVORITE_ONLY))) {
			SysPrograms.SqlAppendWhere(" F.SITE_CD		= :SITE_CD				 ",ref pWhere);
			SysPrograms.SqlAppendWhere(" F.USER_SEQ		= :USER_SEQ				 ",ref pWhere);
			SysPrograms.SqlAppendWhere(" F.USER_CHAR_NO	= :USER_CHAR_NO			 ",ref pWhere);
			SysPrograms.SqlAppendWhere(" P.SITE_CD		= F.SITE_CD				 ",ref pWhere);
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= F.PARTNER_USER_SEQ	 ",ref pWhere);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= F.PARTNER_USER_CHAR_NO ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
			list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
			list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));

		} else if ((pCondition.findFavoriteType.Equals(ViCommConst.FIND_NOT_FAVORITE_ONLY))) {
			SysPrograms.SqlAppendWhere(" ((SELECT COUNT(*) AS COUNT FROM T_FAVORIT WHERE P.SITE_CD = T_FAVORIT.SITE_CD AND P.USER_SEQ = T_FAVORIT.PARTNER_USER_SEQ AND P.USER_CHAR_NO	= T_FAVORIT.PARTNER_USER_CHAR_NO AND T_FAVORIT.USER_SEQ = :USER_SEQ AND T_FAVORIT.USER_CHAR_NO = :USER_CHAR_NO ) = 0) ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pCondition.userSeq));
			list.Add(new OracleParameter("USER_CHAR_NO",pCondition.userCharNo));
		}

		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewMan"]).Equals("1")) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD AND P.USER_OK_FLAG = :USER_OK_FLAG ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
			list.Add(new OracleParameter("USER_OK_FLAG",ViCommConst.FLAG_ON));
			switch (pCondition.onlineStatus) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					SysPrograms.SqlAppendWhere("P.USER_WAITING_FLAG = :USER_WAITING_FLAG",ref pWhere);
					list.Add(new OracleParameter("USER_WAITING_FLAG",ViCommConst.FLAG_ON));
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
					SysPrograms.SqlAppendWhere("P.USER_LOGINED_FLAG = :USER_LOGINED_FLAG",ref pWhere);
					list.Add(new OracleParameter("USER_LOGINED_FLAG",ViCommConst.FLAG_ON));
					break;
			}
		} else {
			switch (pCondition.onlineStatus) {
				case ViCommConst.SeekOnlineStatus.WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhere);
					list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					break;
				case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
					SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) ",ref pWhere);
					list.Add(new OracleParameter("ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
					list.Add(new OracleParameter("ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
					list.Add(new OracleParameter("ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
					break;
			}

			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD AND P.NA_FLAG = :NA_FLAG ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pCondition.siteCd));
			list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
		}

		if (!pCondition.partnerLoginId.Equals("")) {
			SysPrograms.SqlAppendWhere("P.LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pCondition.partnerLoginId));
		}

		if (!pCondition.userSeq.Equals("") && pCondition.conditionFlag != ViCommConst.INQUIRY_REFUSE && pCondition.conditionFlag != ViCommConst.INQUIRY_REFUSE_ME) {
			SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE P.SITE_CD = T_REFUSE.SITE_CD AND P.USER_SEQ = T_REFUSE.USER_SEQ AND P.USER_CHAR_NO = T_REFUSE.USER_CHAR_NO AND T_REFUSE.PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND T_REFUSE.PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO) ",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pCondition.userSeq));
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pCondition.userCharNo));

			if (pCondition.notSendBatchMailFlag == 1) {
				// ===========================
				// 通話、メール履歴(男性から女性にメールを送ったことがある・会話をしたことがある場合除外)  
				// ===========================
				{
					System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
					oSqlBuilder.Append(" AND NOT EXISTS(                                   ").AppendLine();
					oSqlBuilder.Append(" SELECT 1 FROM T_COMMUNICATION                     ").AppendLine();
					oSqlBuilder.Append(" WHERE                                             ").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.SITE_CD					= P.SITE_CD 					AND ").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ					AND").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO				AND").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.PARTNER_USER_SEQ		= :BM_PARTNER_USER_SEQ_01 		AND").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :BM_PARTNER_USER_CHAR_NO_01	AND").AppendLine();
					oSqlBuilder.Append(" (	T_COMMUNICATION.TOTAL_TALK_COUNT		> :TOTAL_TALK_COUNT				OR").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.TOTAL_TX_MAIL_COUNT		> :TOTAL_TX_MAIL_COUNT			OR").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.TOTAL_MAIL_REQUEST_COUNT	> :TOTAL_MAIL_REQUEST_COUNT	OR").AppendLine();
					oSqlBuilder.Append(" 	T_COMMUNICATION.LAST_RX_MAIL_DATE		> :LAST_RX_MAIL_DATE			)").AppendLine();
					oSqlBuilder.Append(" )	                                                ").AppendLine();

					pWhere += oSqlBuilder.ToString();

					SessionObjs oObj = this.sessionObj;

					list.Add(new OracleParameter(":BM_PARTNER_USER_SEQ_01",pCondition.userSeq));
					list.Add(new OracleParameter(":BM_PARTNER_USER_CHAR_NO_01",pCondition.userCharNo));
					list.Add(new OracleParameter(":TOTAL_TALK_COUNT","0"));
					list.Add(new OracleParameter(":TOTAL_TX_MAIL_COUNT","0"));
					list.Add(new OracleParameter(":TOTAL_MAIL_REQUEST_COUNT","0"));
					list.Add(new OracleParameter(":LAST_RX_MAIL_DATE",OracleDbType.Date,DateTime.Now.AddHours(-1 * oObj.site.reBatchMailNaHour),ParameterDirection.Input));
				}
				// ===========================
				// お気に入り 
				// ===========================	
				{
					System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
					oSqlBuilder.Append(" AND NOT EXISTS(                                   ").AppendLine();
					oSqlBuilder.Append(" SELECT 1 FROM T_FAVORIT                           ").AppendLine();
					oSqlBuilder.Append(" WHERE                                             ").AppendLine();
					oSqlBuilder.Append(" 	T_FAVORIT.SITE_CD				= P.SITE_CD 					AND ").AppendLine();
					oSqlBuilder.Append(" 	T_FAVORIT.USER_SEQ				= :BM_PARTNER_USER_SEQ_03 		AND ").AppendLine();
					oSqlBuilder.Append(" 	T_FAVORIT.USER_CHAR_NO			= :BM_PARTNER_USER_CHAR_NO_03	AND ").AppendLine();
					oSqlBuilder.Append(" 	T_FAVORIT.PARTNER_USER_SEQ		= P.USER_SEQ 					AND ").AppendLine();
					oSqlBuilder.Append(" 	T_FAVORIT.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO				").AppendLine();
					oSqlBuilder.Append(" )                                                 ").AppendLine();

					pWhere += oSqlBuilder.ToString();

					list.Add(new OracleParameter(":BM_PARTNER_USER_SEQ_03",pCondition.userSeq));
					list.Add(new OracleParameter(":BM_PARTNER_USER_CHAR_NO_03",pCondition.userCharNo));
				}
				// ===========================
				// 拒否している 
				// ===========================	
				{
					System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
					oSqlBuilder.Append(" AND NOT EXISTS(                                   ").AppendLine();
					oSqlBuilder.Append(" SELECT 1 FROM T_REFUSE                            ").AppendLine();
					oSqlBuilder.Append(" WHERE                                             ").AppendLine();
					oSqlBuilder.Append(" 	T_REFUSE.SITE_CD				= P.SITE_CD 					AND ").AppendLine();
					oSqlBuilder.Append(" 	T_REFUSE.USER_SEQ				= :BM_PARTNER_USER_SEQ_04 		AND ").AppendLine();
					oSqlBuilder.Append(" 	T_REFUSE.USER_CHAR_NO			= :BM_PARTNER_USER_CHAR_NO_04	AND ").AppendLine();
					oSqlBuilder.Append(" 	T_REFUSE.PARTNER_USER_SEQ		= P.USER_SEQ 					AND ").AppendLine();
					oSqlBuilder.Append(" 	T_REFUSE.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO				").AppendLine();
					oSqlBuilder.Append(" )                                                 ").AppendLine();

					pWhere += oSqlBuilder.ToString();

					list.Add(new OracleParameter(":BM_PARTNER_USER_SEQ_04", pCondition.userSeq));
					list.Add(new OracleParameter(":BM_PARTNER_USER_CHAR_NO_04", pCondition.userCharNo));
				}
			}
		}

		if (pCondition.hasExtend) {
			if (pCondition.hasCommunication != null) {
				System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
				if (pCondition.hasCommunication.Value) {
					oSqlBuilder.AppendLine(" AND EXISTS(		");
				} else {
					oSqlBuilder.AppendLine(" AND NOT EXISTS(	");
				}
				oSqlBuilder.AppendLine(" SELECT 1 FROM T_COMMUNICATION                                              ");
				oSqlBuilder.AppendLine(" WHERE                                                                      ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.SITE_CD					= P.SITE_CD 				AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.USER_SEQ					= P.USER_SEQ 				AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.USER_CHAR_NO				= P.USER_CHAR_NO 			AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.PARTNER_USER_SEQ 		= :PARTNER_USER_SEQ_04 		AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.PARTNER_USER_CHAR_NO 	= :PARTNER_USER_CHAR_NO_04 	    ");
				oSqlBuilder.AppendLine(" )                                                                          ");

				pWhere += oSqlBuilder.ToString();

				list.Add(new OracleParameter(":PARTNER_USER_SEQ_04",pCondition.userSeq));
				list.Add(new OracleParameter(":PARTNER_USER_CHAR_NO_04",pCondition.userCharNo));
			}

			if (pCondition.withoutBatchMail) {
				System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
				oSqlBuilder.AppendLine(" AND NOT EXISTS(															");
				oSqlBuilder.AppendLine(" SELECT 1 FROM T_COMMUNICATION                                              ");
				oSqlBuilder.AppendLine(" WHERE                                                                      ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.SITE_CD					= P.SITE_CD 				AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.USER_SEQ					= P.USER_SEQ 				AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.USER_CHAR_NO				= P.USER_CHAR_NO 			AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.PARTNER_USER_SEQ 		= :PARTNER_USER_SEQ_05 		AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.PARTNER_USER_CHAR_NO 	= :PARTNER_USER_CHAR_NO_05 	AND ");
				oSqlBuilder.AppendLine("   T_COMMUNICATION.LAST_RX_BATCH_MAIL_DATE 	> :LAST_RX_BATCH_MAIL_DATE_05   ");
				oSqlBuilder.AppendLine(" )                                                                          ");

				pWhere += oSqlBuilder.ToString();

				list.Add(new OracleParameter(":PARTNER_USER_SEQ_05",pCondition.userSeq));
				list.Add(new OracleParameter(":PARTNER_USER_CHAR_NO_05",pCondition.userCharNo));
				list.Add(new OracleParameter(":LAST_RX_BATCH_MAIL_DATE_05",DateTime.Now.AddMonths(-1)));
			}

			if (!string.IsNullOrEmpty(pCondition.handleNm)) {
				SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE '%' || :EXT_HANDLE_NM || '%' ",ref pWhere);
				list.Add(new OracleParameter("EXT_HANDLE_NM",pCondition.handleNm));
			}
			if (!string.IsNullOrEmpty(pCondition.ageFrom)) {
				SysPrograms.SqlAppendWhere("P.AGE >= :EXT_AGE_FROM ",ref pWhere);
				list.Add(new OracleParameter("EXT_AGE_FROM",pCondition.ageFrom));
			}
			if (!string.IsNullOrEmpty(pCondition.ageTo)) {
				SysPrograms.SqlAppendWhere("P.AGE <= :EXT_AGE_TO ",ref pWhere);
				list.Add(new OracleParameter("EXT_AGE_TO",pCondition.ageTo));
			}

			if (pCondition.attrValue.Count > 0) {
				this.CreateAttrQuery(pCondition,ref pWhere,ref list);
			}
		}

		if (!string.IsNullOrEmpty(pCondition.device)) {
			switch (pCondition.device) {
				case ViCommConst.DEVICE_3G:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD IN (:MOBILE_CARRIER_CD1,:MOBILE_CARRIER_CD2,:MOBILE_CARRIER_CD3) ",ref pWhere);
					list.Add(new OracleParameter("MOBILE_CARRIER_CD1",ViCommConst.DOCOMO));
					list.Add(new OracleParameter("MOBILE_CARRIER_CD2",ViCommConst.KDDI));
					list.Add(new OracleParameter("MOBILE_CARRIER_CD3",ViCommConst.SOFTBANK));
					break;
				case ViCommConst.DEVICE_SMART_PHONE:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD IN (:MOBILE_CARRIER_CD1,:MOBILE_CARRIER_CD2) ",ref pWhere);
					list.Add(new OracleParameter("MOBILE_CARRIER_CD1",ViCommConst.ANDROID));
					list.Add(new OracleParameter("MOBILE_CARRIER_CD2",ViCommConst.IPHONE));
					break;
				case ViCommConst.DEVICE_ANDROID:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD = :MOBILE_CARRIER_CD ",ref pWhere);
					list.Add(new OracleParameter("MOBILE_CARRIER_CD",ViCommConst.ANDROID));
					break;
				case ViCommConst.DEVICE_IPHONE:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD = :MOBILE_CARRIER_CD ",ref pWhere);
					list.Add(new OracleParameter("MOBILE_CARRIER_CD",ViCommConst.IPHONE));
					break;
				case "-" + ViCommConst.DEVICE_IPHONE:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD <> :MOBILE_CARRIER_CD ",ref pWhere);
					list.Add(new OracleParameter("MOBILE_CARRIER_CD",ViCommConst.IPHONE));
					break;
			}
		}

		if (!string.IsNullOrEmpty(pCondition.mailSendFlag) && !string.IsNullOrEmpty(pCondition.userSeq) && !string.IsNullOrEmpty(pCondition.userCharNo)) {
			System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();

			if (pCondition.mailSendFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				oSqlBuilder.AppendLine(" AND EXISTS(");
			} else {
				oSqlBuilder.AppendLine(" AND NOT EXISTS(");
			}

			oSqlBuilder.AppendLine(" SELECT 1 FROM T_COMMUNICATION");
			oSqlBuilder.AppendLine(" WHERE");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.SITE_CD					= P.SITE_CD 				AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ 				AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO			AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_SEQ		= :COM_PARTNER_USER_SEQ		AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :COM_PARTNER_USER_CHAR_NO	AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.TOTAL_RX_MAIL_COUNT		> 0");
			oSqlBuilder.AppendLine(" )");

			pWhere += oSqlBuilder.ToString();

			list.Add(new OracleParameter(":COM_PARTNER_USER_SEQ",pCondition.userSeq));
			list.Add(new OracleParameter(":COM_PARTNER_USER_CHAR_NO",pCondition.userCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.mailSendCount) && !string.IsNullOrEmpty(pCondition.userSeq) && !string.IsNullOrEmpty(pCondition.userCharNo)) {
			System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
			oSqlBuilder.AppendLine(" AND EXISTS(");
			oSqlBuilder.AppendLine(" SELECT 1 FROM T_COMMUNICATION");
			oSqlBuilder.AppendLine(" WHERE");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.SITE_CD					= P.SITE_CD 					AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ 					AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO				AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_SEQ		= :COM_PARTNER_USER_SEQ2		AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :COM_PARTNER_USER_CHAR_NO2	AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.TOTAL_RX_MAIL_COUNT		= :TOTAL_RX_MAIL_COUNT");
			oSqlBuilder.AppendLine(" )");
			pWhere += oSqlBuilder.ToString();
			list.Add(new OracleParameter(":COM_PARTNER_USER_SEQ2",pCondition.userSeq));
			list.Add(new OracleParameter(":COM_PARTNER_USER_CHAR_NO2",pCondition.userCharNo));
			list.Add(new OracleParameter(":TOTAL_RX_MAIL_COUNT",pCondition.mailSendCount));
		}

		if (!string.IsNullOrEmpty(pCondition.noTxMailDays) && !string.IsNullOrEmpty(pCondition.userSeq) && !string.IsNullOrEmpty(pCondition.userCharNo)) {
			System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
			oSqlBuilder.AppendLine(" AND NOT EXISTS(");
			oSqlBuilder.AppendLine(" SELECT 1 FROM T_COMMUNICATION");
			oSqlBuilder.AppendLine(" WHERE");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.SITE_CD					= P.SITE_CD 						AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ 						AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO					AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_SEQ		= :COM_PARTNER_USER_SEQ3			AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :COM_PARTNER_USER_CHAR_NO3		AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.LAST_RX_MAIL_DATE		> SYSDATE - :LAST_RX_MAIL_DATE			");
			oSqlBuilder.AppendLine(" )");
			pWhere += oSqlBuilder.ToString();
			list.Add(new OracleParameter(":COM_PARTNER_USER_SEQ3",pCondition.userSeq));
			list.Add(new OracleParameter(":COM_PARTNER_USER_CHAR_NO3",pCondition.userCharNo));
			list.Add(new OracleParameter(":LAST_RX_MAIL_DATE",pCondition.noTxMailDays));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetFavoritCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iCount = 0;
		string sFavoritSql = "SELECT NVL(COUNT(*),0) AS CNT FROM T_FAVORIT " +
				   "WHERE " +
						"SITE_CD				= :SITE_CD			AND " +
						"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ	AND " +
						"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ";
		try {
			conn = DbConnect("Cast.GetFavoritCount");

			using (DataSet ds = new DataSet())
			using (cmd = CreateSelectCommand(sFavoritSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pUserCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_FAVORIT");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow dr = ds.Tables[0].Rows[0];
						iCount = int.Parse(dr["CNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return iCount;
	}

	public bool IsNotEixstTermId(string pTerminalUniqueId,string piModeId,string pSiteCd,string pUserSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("Man.EixstTermId");

			string sSql = "SELECT " +
								"USER_SEQ			," +
								"USER_STATUS		 " +
							"FROM " +
							" VW_USER_MAN01 ";

			string sWhere = "";

			ArrayList list = new ArrayList();
			sWhere = "WHERE	" +
						"		SITE_CD	= :SITE_CD	";
			list.Add(new OracleParameter("SITE_CD",pSiteCd));

			string sWhereId = string.Empty;
			if (!pTerminalUniqueId.Equals("")) {
				sWhereId = " ( TERMINAL_UNIQUE_ID = :TERMINAL_UNIQUE_ID	";
				list.Add(new OracleParameter("TERMINAL_UNIQUE_ID",pTerminalUniqueId));
			}
			if (!piModeId.Equals("")) {
				if (!sWhereId.Equals(string.Empty)) {
					sWhereId += " OR ";
				} else {
					sWhereId = " ( ";
				}
				sWhereId += "	IMODE_ID = :IMODE_ID	";
				list.Add(new OracleParameter("IMODE_ID",piModeId));
			}
			if (!sWhereId.Equals(string.Empty)) {
				sWhereId += " ) ";
				sWhere += " AND " + sWhereId;
			}
			sWhere += "	AND	SEX_CD	= :SEX_CD	";
			list.Add(new OracleParameter("SEX_CD",ViCommConst.MAN));

			OracleParameter[] objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));

			sSql += sWhere;
			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count == 0) {
						bExist = true;
					} else {
						bool bTmp = true;
						for (int j = 0;j < ds.Tables["T_USER"].Rows.Count;j++) {
							dr = ds.Tables["T_USER"].Rows[j];
							if (!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
								if (!dr["USER_SEQ"].ToString().Equals(pUserSeq)) {
									bTmp = false;
								}
							}
						}
						bExist = bTmp;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsBlackUser(string pNoType,string pTermIdOrTel) {
		DataSet ds;

		bool bBlack = false;
		try {
			conn = DbConnect("Man.IsBlackUser");

			string sSql = "SELECT 1 " +
							"FROM " +
							" T_UNABLE_REGIST_NO " +
							"WHERE " +
							" NO_TYPE					= :NO_TYPE AND " +
							" TERMINAL_UNIQUE_ID_OR_TEL	= :TERMINAL_UNIQUE_ID_OR_TEL ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("NO_TYPE",pNoType);
				cmd.Parameters.Add("TERMINAL_UNIQUE_ID_OR_TEL",pTermIdOrTel);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
				if (ds.Tables[0].Rows.Count > 0) {
					bBlack = true;
				}
			}
		} finally {
			conn.Close();
		}
		return bBlack;
	}

	public string GetUserSeqByLoginId(string pLoginId) {
		DataSet ds;
		DataRow dr;
		string sUserSeq = "";
		try {
			conn = DbConnect("Cast.GetUserSeqByLoginId");

			string sSql = "SELECT " +
								"USER_SEQ " +
							"FROM " +
							"	T_USER " +
							"WHERE " +
							"	LOGIN_ID = :LOGIN_ID ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("LOGIN_ID",pLoginId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER");
					if (ds.Tables["T_USER"].Rows.Count > 0) {
						dr = ds.Tables["T_USER"].Rows[0];
						sUserSeq = dr["USER_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sUserSeq;
	}

	public DataSet GetPageCollectionBatchMail(SeekCondition pCondition,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pCondition,false,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	P.USER_SEQ");
		oSqlBuilder.AppendLine("FROM");

		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewMan"]).Equals("1")) {
			oSqlBuilder.AppendLine("	MV_USER_MAN_CHARACTER00 P");
		} else {
			oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P");
		}

		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				oSqlBuilder.AppendFormat(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1).AppendLine();
			}
		}

		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY DBMS_RANDOM.RANDOM";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,iStartIndex,pRecPerPage,out sExecSql));
		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetListBatchMail(SeekCondition pCondition,int pLimitCount) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.siteCd));
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));
		oParamList.AddRange(this.CreateWhereBatchMail(pCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT USER_SEQ FROM (");
		oSqlBuilder.AppendLine("SELECT P.USER_SEQ FROM (");
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.SITE_CD,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_SEQ,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.HANDLE_NM,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.REGIST_DATE,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.AGE,");
		oSqlBuilder.AppendLine("	SUBSTR(T_USER_MAN_CHARACTER.BIRTHDAY,6,5) AS BIRTHDAY_MMDD,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.CHARACTER_ONLINE_STATUS,");
		oSqlBuilder.AppendLine("	T_USER.MOBILE_CARRIER_CD");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER ON (T_USER_MAN_CHARACTER.USER_SEQ = T_USER.USER_SEQ)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.NA_FLAG = :NA_FLAG AND");

		using (ManageCompany oManageCompany = new ManageCompany())　{
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_BATCH_MAIL_RECENT_ACTIVE,2)) {
				oSqlBuilder.AppendLine("	T_USER.NON_EXIST_MAIL_ADDR_FLAG = 0 AND");
				oSqlBuilder.AppendLine("	T_USER.LAST_LOGIN_DATE > SYSDATE - 7 AND");
				oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.CAST_MAIL_RX_TYPE = :CAST_MAIL_RX_TYPE_BOTH ");
				oParamList.Add(new OracleParameter(":CAST_MAIL_RX_TYPE_BOTH",ViCommConst.MAIL_RX_BOTH));
			} else {
				oSqlBuilder.AppendLine("	(T_USER.NON_EXIST_MAIL_ADDR_FLAG = 0 OR T_USER.LAST_LOGIN_DATE > SYSDATE-60)");
			}
		}
		
		oSqlBuilder.AppendLine(") P");

		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				oSqlBuilder.AppendFormat(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1).AppendLine();
			}
		}

		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY DBMS_RANDOM.RANDOM");
		oSqlBuilder.AppendFormat(") WHERE ROWNUM <= {0}",pLimitCount).AppendLine();

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhereBatchMail(SeekCondition pCondition,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pCondition.handleNm.Equals("")) {
			SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE '%' || :HANDLE_NM || '%'",ref pWhere);
			list.Add(new OracleParameter(":HANDLE_NM",pCondition.handleNm));
		}
		if (pCondition.newManDay != 0) {
			DateTime Dt = DateTime.Today.AddDays(pCondition.newManDay * -1);
			SysPrograms.SqlAppendWhere("P.REGIST_DATE >= :REGIST_DATE",ref pWhere);
			list.Add(new OracleParameter(":REGIST_DATE",OracleDbType.Date,Dt,ParameterDirection.Input));
		}
		if (!pCondition.ageFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("P.AGE >= :AGE_FROM",ref pWhere);
			list.Add(new OracleParameter(":AGE_FROM",pCondition.ageFrom));
		}
		if (!pCondition.ageTo.Equals("")) {
			SysPrograms.SqlAppendWhere("P.AGE <= :AGE_TO",ref pWhere);
			list.Add(new OracleParameter(":AGE_TO",pCondition.ageTo));
		}
		if (!pCondition.birthDayFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("P.BIRTHDAY_MMDD >= :BIRTHDAY_MMDD_FROM",ref pWhere);
			list.Add(new OracleParameter(":BIRTHDAY_MMDD_FROM",pCondition.birthDayFrom));
		}
		if (!pCondition.birthDayTo.Equals("")) {
			SysPrograms.SqlAppendWhere("P.BIRTHDAY_MMDD <= :BIRTHDAY_MMDD_TO",ref pWhere);
			list.Add(new OracleParameter(":BIRTHDAY_MMDD_TO",pCondition.birthDayTo));
		}

		CreateAttrQuery(pCondition,ref pWhere,ref list);

		switch (pCondition.onlineStatus) {
			case ViCommConst.SeekOnlineStatus.WAITING:
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING))",ref pWhere);
				list.Add(new OracleParameter(":ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter(":ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;
			case ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING:
				SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:ONLINE_STATUS_LOGINED,:ONLINE_STATUS_WAITING,:ONLINE_STATUS_TALKING)) ",ref pWhere);
				list.Add(new OracleParameter(":ONLINE_STATUS_LOGINED",ViCommConst.USER_LOGINED));
				list.Add(new OracleParameter(":ONLINE_STATUS_WAITING",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter(":ONLINE_STATUS_TALKING",ViCommConst.USER_TALKING));
				break;
		}

		SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE P.SITE_CD = T_REFUSE.SITE_CD AND P.USER_SEQ = T_REFUSE.USER_SEQ AND P.USER_CHAR_NO = T_REFUSE.USER_CHAR_NO AND T_REFUSE.PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND T_REFUSE.PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO) ",ref pWhere);
		list.Add(new OracleParameter(":PARTNER_USER_SEQ",pCondition.userSeq));
		list.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pCondition.userCharNo));

		// ===========================
		// 通話、メール履歴(男性から女性にメールを送ったことがある・会話をしたことがある場合除外)  
		// ===========================
		{
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("NOT EXISTS(SELECT 1 FROM T_COMMUNICATION WHERE");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.SITE_CD					= P.SITE_CD 					AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ					AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO				AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_SEQ		= :BM_PARTNER_USER_SEQ_01 		AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :BM_PARTNER_USER_CHAR_NO_01	AND");
			oSqlBuilder.AppendLine("	(T_COMMUNICATION.TOTAL_TALK_COUNT		> :TOTAL_TALK_COUNT				OR");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.TOTAL_TX_MAIL_COUNT		> :TOTAL_TX_MAIL_COUNT			OR");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.TOTAL_MAIL_REQUEST_COUNT	> :TOTAL_MAIL_REQUEST_COUNT	OR");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.LAST_RX_MAIL_DATE		> :LAST_RX_MAIL_DATE)");
			oSqlBuilder.AppendLine(")");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhere);
			SessionObjs oObj = this.sessionObj;
			list.Add(new OracleParameter(":BM_PARTNER_USER_SEQ_01",pCondition.userSeq));
			list.Add(new OracleParameter(":BM_PARTNER_USER_CHAR_NO_01",pCondition.userCharNo));
			list.Add(new OracleParameter(":TOTAL_TALK_COUNT","0"));
			list.Add(new OracleParameter(":TOTAL_TX_MAIL_COUNT","0"));
			list.Add(new OracleParameter(":TOTAL_MAIL_REQUEST_COUNT","0"));
			list.Add(new OracleParameter(":LAST_RX_MAIL_DATE",OracleDbType.Date,DateTime.Now.AddHours(-1 * oObj.site.reBatchMailNaHour),ParameterDirection.Input));
		}
		// ===========================
		// お気に入り 
		// ===========================	
		{
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("NOT EXISTS(SELECT 1 FROM T_FAVORIT WHERE");
			oSqlBuilder.AppendLine("	T_FAVORIT.SITE_CD				= P.SITE_CD 					AND");
			oSqlBuilder.AppendLine("	T_FAVORIT.USER_SEQ				= :BM_PARTNER_USER_SEQ_03 		AND");
			oSqlBuilder.AppendLine("	T_FAVORIT.USER_CHAR_NO			= :BM_PARTNER_USER_CHAR_NO_03	AND");
			oSqlBuilder.AppendLine("	T_FAVORIT.PARTNER_USER_SEQ		= P.USER_SEQ 					AND");
			oSqlBuilder.AppendLine("	T_FAVORIT.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO");
			oSqlBuilder.AppendLine(")");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhere);
			list.Add(new OracleParameter(":BM_PARTNER_USER_SEQ_03",pCondition.userSeq));
			list.Add(new OracleParameter(":BM_PARTNER_USER_CHAR_NO_03",pCondition.userCharNo));
		}
		// ===========================
		// 拒否している 
		// ===========================	
		{
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE");
			oSqlBuilder.AppendLine("	T_REFUSE.SITE_CD				= P.SITE_CD 					AND");
			oSqlBuilder.AppendLine("	T_REFUSE.USER_SEQ				= :BM_PARTNER_USER_SEQ_04 		AND");
			oSqlBuilder.AppendLine("	T_REFUSE.USER_CHAR_NO			= :BM_PARTNER_USER_CHAR_NO_04	AND");
			oSqlBuilder.AppendLine("	T_REFUSE.PARTNER_USER_SEQ		= P.USER_SEQ 					AND");
			oSqlBuilder.AppendLine("	T_REFUSE.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO");
			oSqlBuilder.AppendLine(")");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhere);
			list.Add(new OracleParameter(":BM_PARTNER_USER_SEQ_04",pCondition.userSeq));
			list.Add(new OracleParameter(":BM_PARTNER_USER_CHAR_NO_04",pCondition.userCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.device)) {
			switch (pCondition.device) {
				case ViCommConst.DEVICE_3G:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD IN (:MOBILE_CARRIER_CD1,:MOBILE_CARRIER_CD2,:MOBILE_CARRIER_CD3)",ref pWhere);
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD1",ViCommConst.DOCOMO));
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD2",ViCommConst.KDDI));
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD3",ViCommConst.SOFTBANK));
					break;
				case ViCommConst.DEVICE_SMART_PHONE:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD IN (:MOBILE_CARRIER_CD1,:MOBILE_CARRIER_CD2)",ref pWhere);
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD1",ViCommConst.ANDROID));
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD2",ViCommConst.IPHONE));
					break;
				case ViCommConst.DEVICE_ANDROID:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD = :MOBILE_CARRIER_CD",ref pWhere);
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD",ViCommConst.ANDROID));
					break;
				case ViCommConst.DEVICE_IPHONE:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD = :MOBILE_CARRIER_CD",ref pWhere);
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD",ViCommConst.IPHONE));
					break;
				case "-" + ViCommConst.DEVICE_IPHONE:
					SysPrograms.SqlAppendWhere("P.MOBILE_CARRIER_CD <> :MOBILE_CARRIER_CD",ref pWhere);
					list.Add(new OracleParameter(":MOBILE_CARRIER_CD",ViCommConst.IPHONE));
					break;
			}
		}

		if (!string.IsNullOrEmpty(pCondition.mailSendFlag) && !string.IsNullOrEmpty(pCondition.userSeq) && !string.IsNullOrEmpty(pCondition.userCharNo)) {
			StringBuilder oSqlBuilder = new StringBuilder();

			if (pCondition.mailSendFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				oSqlBuilder.AppendLine("EXISTS(");
			} else {
				oSqlBuilder.AppendLine("NOT EXISTS(");
			}

			oSqlBuilder.AppendLine("SELECT 1 FROM T_COMMUNICATION WHERE");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.SITE_CD					= P.SITE_CD 				AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ 				AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO			AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_SEQ		= :COM_PARTNER_USER_SEQ		AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :COM_PARTNER_USER_CHAR_NO	AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.TOTAL_RX_MAIL_COUNT		> 0");
			oSqlBuilder.AppendLine(")");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhere);
			list.Add(new OracleParameter(":COM_PARTNER_USER_SEQ",pCondition.userSeq));
			list.Add(new OracleParameter(":COM_PARTNER_USER_CHAR_NO",pCondition.userCharNo));
		}

		if (!string.IsNullOrEmpty(pCondition.mailSendCount) && !string.IsNullOrEmpty(pCondition.userSeq) && !string.IsNullOrEmpty(pCondition.userCharNo)) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("EXISTS(SELECT 1 FROM T_COMMUNICATION WHERE");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.SITE_CD					= P.SITE_CD 					AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_SEQ				= P.USER_SEQ 					AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.USER_CHAR_NO			= P.USER_CHAR_NO				AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_SEQ		= :COM_PARTNER_USER_SEQ2		AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.PARTNER_USER_CHAR_NO	= :COM_PARTNER_USER_CHAR_NO2	AND");
			oSqlBuilder.AppendLine("	T_COMMUNICATION.TOTAL_RX_MAIL_COUNT		= :TOTAL_RX_MAIL_COUNT");
			oSqlBuilder.AppendLine(")");

			SysPrograms.SqlAppendWhere(oSqlBuilder.ToString(),ref pWhere);
			list.Add(new OracleParameter(":COM_PARTNER_USER_SEQ2",pCondition.userSeq));
			list.Add(new OracleParameter(":COM_PARTNER_USER_CHAR_NO2",pCondition.userCharNo));
			list.Add(new OracleParameter(":TOTAL_RX_MAIL_COUNT",pCondition.mailSendCount));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public string GetManHandleNmByLoginId (string pSiteCd,string pLoginId) {
		string sValue = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.HANDLE_NM							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER P			,	");
		oSqlBuilder.AppendLine("	T_USER U							");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	P.USER_SEQ	= U.USER_SEQ		AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD	= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID	= :LOGIN_ID				");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":LOGIN_ID",pLoginId));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			sValue = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["HANDLE_NM"]);
		}
		
		return sValue;
	}
	
	public int GetOnlineStatusByUserSeq(string pSiteCd,string pUserSeq,string pUserCharNo) {
		int iValue = 0;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT										");
		oSqlBuilder.AppendLine("	P.CHARACTER_ONLINE_STATUS				");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER P					");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	P.SITE_CD		= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ		= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO	= :USER_CHAR_NO			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			int.TryParse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CHARACTER_ONLINE_STATUS"]),out iValue);
		}

		return iValue;
	}
}
