﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾞﾛｸﾞｵﾌﾞｼﾞｪｸﾄ
--	Progaram ID		: BlogObj
--
--  Creation Date	: 2011.04.05
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

#region □■□ Condition □■□ =======================================================================================
[Serializable]
public class BlogObjSeekCondition : SeekConditionBase {
	private string blogFileType = string.Empty;
	private string blogArticleStatus = string.Empty;

	public string SiteCd {
		get { return this.Query["site"]; }
		set { this.Query["site"] = value; }
	}
	public string UserSeq {
		get { return this.Query["user_seq"]; }
		set { this.Query["user_seq"] = value; }
	}
	public string UserCharNo {
		get { return this.Query["user_char_no"]; }
		set { this.Query["user_char_no"] = value; }
	}
	public string BlogSeq {
		get { return this.Query["blog_seq"]; }
		set { this.Query["blog_seq"] = value; }
	}
	public string ObjSeq {
		get { return this.Query["obj_seq"]; }
		set { this.Query["obj_seq"] = value; }
	}
	public string UsedFlag {
		get { return this.Query["used_flag"]; }
		set { this.Query["used_flag"] = value; }
	}
	public string BlogArticleSeq {
		get { return this.Query["blog_article_seq"]; }
		set { this.Query["blog_article_seq"] = value; }
	}
	protected string AppendBlogArticleSeq {
		get { return this.Query["append_blog_article_seq"]; }
		set { this.Query["append_blog_article_seq"] = value; }
	}
	public string FileType {
		get { return blogFileType; }
		set { blogFileType = value; }
	}
	public string BlogArticleStatus {
		get { return this.blogArticleStatus; }
		set { this.blogArticleStatus = value; }
	}


	public BlogObjSeekCondition() : this(new NameValueCollection()) { }
	public BlogObjSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
		this.Query["blog_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_seq"]));
		this.Query["obj_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["obj_seq"]));
		this.Query["used_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["used_flag"]));
		this.Query["blog_article_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_article_seq"]));
		this.Query["append_blog_article_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["append_blog_article_seq"]));
	}
}
#endregion ============================================================================================================

[Serializable]
public class BlogObj : CastBase {
	public BlogObj() : base() { }
	public BlogObj(SessionObjs pSessionObj) : base(pSessionObj) { }


	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(BlogObjSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendFormat("	{0} P			", GetMainTableName()).AppendLine();

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder, oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(BlogObjSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion, pPageNo, pRecPerPage);
	}
	private DataSet GetPageCollectionBase(BlogObjSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		if (!string.IsNullOrEmpty(pCondtion.ObjSeq)) {
			pPageNo = 1;
			pRecPerPage = 1;
		}

		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();

		// select
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	" + CastBasicField() + ",   ");
		if(!string.IsNullOrEmpty(pCondtion.ObjSeq)){
			// 注意)必ず主処理のSORT条件と合わせる
			oSqlBuilder.AppendFormat("	LEAD(P.OBJ_SEQ,1)	OVER ({0}) AS NEXT_SEQ	,	", sSortExpression).AppendLine();
			oSqlBuilder.AppendFormat("	LAG (P.OBJ_SEQ,1)	OVER ({0}) AS PREV_SEQ	,	", sSortExpression).AppendLine();
			oSqlBuilder.AppendFormat("	RANK()				OVER ({0}) AS RNUM		,	", sSortExpression).AppendLine();			
		}
		oSqlBuilder.AppendLine("	P.OBJ_SEQ				,	");
		oSqlBuilder.AppendLine("	P.BLOG_FILE_TYPE		,	");
		oSqlBuilder.AppendLine("	P.BLOG_PIC_USED_FLAG	,	");
		oSqlBuilder.AppendLine("	P.REGIST_DATE			,	");
		oSqlBuilder.AppendLine("	P.BLOG_SEQ				,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_SEQ		,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_OBJS_REGIST_DATE	,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_TITLE	,	");
		oSqlBuilder.AppendLine("	P.READING_COUNT			,	");
		oSqlBuilder.AppendLine("	0 AS COMMENT_COUNT			");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendFormat("	{0} P			", GetMainTableName()).AppendLine();

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion, ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);


		string sExecSql = oSqlBuilder.ToString();
		if(!string.IsNullOrEmpty(pCondtion.ObjSeq)){
			oParamList.AddRange(WrapSeqQuery(sExecSql, pCondtion, out sExecSql));
		}else{
			oParamList.AddRange(ViCommPrograms.CreatePagingSql(sExecSql, iStartIndex, pRecPerPage, out sExecSql));
		}

		// =================================================
		//  TEMPLATE 適用
		// =================================================
		StringBuilder oOuterFields = new StringBuilder();
		// PIC
		oOuterFields.AppendLine("	T1.OBJ_SEQ															AS PIQ_SEQ							, ");
		oOuterFields.AppendLine("	GET_PHOTO_IMG_PATH				(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_PHOTO_IMG_PATH				, ");
		oOuterFields.AppendLine("	GET_SMALL_PHOTO_IMG_PATH		(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_SMALL_PHOTO_IMG_PATH			, ");
		oOuterFields.AppendLine("	GET_BLUR_PHOTO_IMG_PATH			(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_BLUR_PHOTO_IMG_PATH			, ");
		oOuterFields.AppendLine("	GET_SMALL_BLUR_PHOTO_IMG_PATH	(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)	AS OBJ_SMALL_BLUR_PHOTO_IMG_PATH	, ");
		// MOVIE
		oOuterFields.AppendLine("	T1.OBJ_SEQ															AS MOVIE_SEQ						, ");
		oOuterFields.AppendLine("	T1.OBJ_SEQ															AS THUMBNAIL_PIC_SEQ				, ");
		oOuterFields.AppendLine("	GET_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.OBJ_SEQ)				AS THUMBNAIL_IMG_PATH				, ");

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql, oParamList, string.Empty, string.Empty, oOuterFields.ToString());

		AppendCommentCount(oDataSet);
		
		return oDataSet;

	}

	private OracleParameter[] CreateWhere(BlogObjSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD", ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD", pCondition.SiteCd));

		SysPrograms.SqlAppendWhere(" P.USER_STATUS = :USER_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter(":USER_STATUS",ViCommConst.USER_WOMAN_NORMAL));

		SysPrograms.SqlAppendWhere(" P.NA_FLAG = :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.FLAG_OFF));

		if (!string.IsNullOrEmpty(pCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(" P.USER_SEQ		= :USER_SEQ", ref pWhereClause);
			SysPrograms.SqlAppendWhere(" P.USER_CHAR_NO	= :USER_CHAR_NO", ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ", pCondition.UserSeq));
			oParamList.Add(new OracleParameter(":USER_CHAR_NO", pCondition.UserCharNo));
		}
		if (!string.IsNullOrEmpty(pCondition.BlogSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_SEQ		= :BLOG_SEQ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_SEQ", pCondition.BlogSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.UsedFlag)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_PIC_USED_FLAG	= :BLOG_PIC_USED_FLAG", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_PIC_USED_FLAG", pCondition.UsedFlag));
		}
		if (!string.IsNullOrEmpty(pCondition.FileType)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_FILE_TYPE	= :BLOG_FILE_TYPE", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_FILE_TYPE", pCondition.FileType));
		}
		if (!string.IsNullOrEmpty(pCondition.BlogSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_SEQ	= :BLOG_SEQ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_SEQ", pCondition.BlogSeq));
		}
		if (!string.IsNullOrEmpty(pCondition.BlogArticleSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_ARTICLE_SEQ	= :BLOG_ARTICLE_SEQ", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_ARTICLE_SEQ", pCondition.BlogArticleSeq));
		}
		if (!string.IsNullOrEmpty(pCondition.BlogArticleStatus)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_ARTICLE_STATUS	= :BLOG_ARTICLE_STATUS", ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_ARTICLE_STATUS", pCondition.BlogArticleStatus));
		}

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(FlexibleQueryType.NotInScope);
			oFlexibleQueryTarget.JealousyBlog = true;

			ArrayList oTmpList = new ArrayList();
			pWhereClause += SetFlexibleQuery(oFlexibleQueryTarget, oSessionMan.site.siteCd, oSessionMan.userMan.userSeq, oSessionMan.userMan.userCharNo, ref oTmpList);
			foreach (OracleParameter oParam in oTmpList) {
				oParamList.Add(oParam);
			}
			
			SysPrograms.SqlAppendWhere(" NOT EXISTS(SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :SELF_USER_SEQ AND PARTNER_USER_CHAR_NO = :SELF_USER_CHAR_NO)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SELF_USER_SEQ",oSessionMan.userMan.userSeq));
			oParamList.Add(new OracleParameter(":SELF_USER_CHAR_NO",oSessionMan.userMan.userCharNo));
		}

		SysPrograms.SqlAppendWhere(" P.DEL_FLAG	= :DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(BlogObjSeekCondition pCondition) {
		string sSortExpression = string.Empty;
		if (string.IsNullOrEmpty(pCondition.Sort)) {
			sSortExpression = " ORDER BY OBJ_SEQ DESC ";
		} else {
			sSortExpression = string.Format("ORDER BY {0} ", pCondition.Sort);
		}
		return sSortExpression;
	}

	public OracleParameter[] WrapSeqQuery(string pSql, BlogObjSeekCondition pCondtion, out string pSeqQuery) {
		if (string.IsNullOrEmpty(pCondtion.ObjSeq)) {
			pSeqQuery = pSql;
			return new OracleParameter[] { };
		}

		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT	/*+ FIRST_ROWS(1) */	");
		oSqlBuilder.AppendLine("	SEQ.*						");
		oSqlBuilder.AppendLine(" FROM (							");
		oSqlBuilder.AppendFormat(" {0} ", pSql).AppendLine();
		oSqlBuilder.AppendLine(" ) SEQ					");
		oSqlBuilder.AppendLine(" WHERE							");
		oSqlBuilder.AppendLine("	SEQ.OBJ_SEQ = :OBJ_SEQ AND	");
		oSqlBuilder.AppendLine("	ROWNUM <= 1					");

		oParamList.Add(new OracleParameter(":OBJ_SEQ", pCondtion.ObjSeq));

		pSeqQuery = oSqlBuilder.ToString();
		return oParamList.ToArray();
	}

	private string GetMainTableName() {
		string sPrefix = "VW";
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableMateViewBlogObjs"]).Equals(ViCommConst.FLAG_ON_STR)
			&& sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			sPrefix = "MV";
		}
		return string.Format("{0}_BLOG_OBJS00", sPrefix);
	}

	private void AppendCommentCount(DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return;
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			if (!string.IsNullOrEmpty(dr["BLOG_ARTICLE_SEQ"].ToString())) {
				BlogCommentSeekCondition oCondition = new BlogCommentSeekCondition();
				oCondition.SiteCd = dr["SITE_CD"].ToString();
				oCondition.BlogArticleSeq = dr["BLOG_ARTICLE_SEQ"].ToString();
				decimal dRecCount = 0;

				using (BlogComment oBlogComment = new BlogComment(sessionObj)) {
					oBlogComment.GetPageCount(oCondition,1,out dRecCount);
				}

				dr["COMMENT_COUNT"] = dRecCount.ToString();
			}
		}
	}

	#endregion ========================================================================================================


	public void DeleteBlogObj(string pObjSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_BLOG_OBJS");
			oDbSession.ProcedureInParm("pOBJ_SEQ", DbType.NUMBER, pObjSeq);
			oDbSession.ProcedureOutParm("pSTATUS", DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}



}
