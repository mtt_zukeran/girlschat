﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: オブジェクト利用履歴
--	Progaram ID		: ObjUsedHistory
--
--  Creation Date	: 2009.09.02
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class ObjUsedHistory:DbSession {

	public ObjUsedHistory() {
	}

	public void AccessBbsObj(string pSiteCd,string pUserSeq,string pObjType,string pObjSeq,int pSpecialFreeFlag,int pSpecialFreeFlag2) {
		AccessBbsObjBase(pSiteCd,pUserSeq,pObjType,pObjSeq,pSpecialFreeFlag,pSpecialFreeFlag2);
	}

	public void AccessBbsObj(string pSiteCd,string pUserSeq,string pObjType,string pObjSeq) {
		AccessBbsObjBase(pSiteCd,pUserSeq,pObjType,pObjSeq,ViCommConst.FLAG_OFF,ViCommConst.FLAG_OFF);
	}

	private void AccessBbsObjBase(string pSiteCd,string pUserSeq,string pObjType,string pObjSeq,int pSpecialFreeFlag,int pSpecialFreeFlag2) {
		if (SessionObjs.Current.adminFlg)
			return;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACCESS_BBS_OBJ");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("POBJ_TYPE",DbSession.DbType.VARCHAR2,pObjType);
			db.ProcedureInParm("POBJ_SEQ",DbSession.DbType.VARCHAR2,pObjSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PSPECIAL_FREE_FLAG",DbSession.DbType.NUMBER,pSpecialFreeFlag);
			db.ProcedureInParm("PSPECIAL_FREE_FLAG2",DbSession.DbType.NUMBER,pSpecialFreeFlag2);
			db.ExecuteProcedure();
		}
	}

	public bool CheckRichinoObjFreeView(string pSiteCd,string pUserSeq,string pObjType) {
		bool bOk = false;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CHECK_RICHINO_OBJ_FREE_VIEW");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("POBJ_TYPE",DbSession.DbType.VARCHAR2,pObjType);
			db.ProcedureOutParm("PVIEW_OK_FLAG",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			bOk = (db.GetStringValue("PVIEW_OK_FLAG") == ViCommConst.FLAG_ON_STR);
		}
		return bOk;
	}
	
	public bool GetOne(string pSiteCd,string pUserSeq,string pObjType,string pObjSeq) {
		DataSet ds;
		try {
			conn = DbConnect("ObjUsedHistory.GetOne");
			ds = new DataSet();

			string sSql = " SELECT " +
								" NVL(COUNT(*),0) AS CNT " +
							" FROM " +
								" T_OBJ_USED_HISTORY " +
							" WHERE " +
								" SITE_CD = :SITE_CD AND " +
								" USER_SEQ = :USER_SEQ AND " +
								" OBJ_TYPE = :OBJ_TYPE AND " +
								" OBJ_SEQ = :OBJ_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("OBJ_TYPE",pObjType);
				cmd.Parameters.Add("OBJ_SEQ",pObjSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_OBJ_USED_HISTORY");
				}
			}
		} finally {
			conn.Close();
		}

		if (ds.Tables["T_OBJ_USED_HISTORY"].Rows[0]["CNT"].ToString().Equals("0")) {
			return false;
		} else {
			return true;
		}
	}

	public DataSet GetOneDataSet(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pObjSeq,
		string pSpecialFreeFlag
	) {
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref sWhereClause);
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		SysPrograms.SqlAppendWhere("OBJ_SEQ = :OBJ_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));

		if (!string.IsNullOrEmpty(pSpecialFreeFlag)) {
			SysPrograms.SqlAppendWhere("SPECIAL_FREE_FLAG = :SPECIAL_FREE_FLAG",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SPECIAL_FREE_FLAG",pSpecialFreeFlag));
		}

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	USED_DATE,			");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG,	");
		oSqlBuilder.AppendLine("	SPECIAL_FREE_FLAG2	");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_OBJ_USED_HISTORY	");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
	}
}
