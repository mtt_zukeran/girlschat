﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会話履歴(相手は男性)
--	Progaram ID		: ManTalkHistory
--
--  Creation Date	: 2010.11.24
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Text;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class ManTalkHistory:ManBase {

	public ManTalkHistory() {
	}

	public int GetPageCount(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pFindType,	//発信結果検索種類 
		int pRecPerPage,
		out decimal pRecCount
	) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT							").AppendLine();
		// NULL許容の列だと件数が正しく取得できないのでTALK_SEQからSITE_CDに変更。 
		// コストベースの場合はindexの有無は関係ない。構文解析時のオーバーヘッドを押さえるために*ではなくNOT NULLの列を指定 
		oSqlBuilder.Append("	COUNT(P.SITE_CD) AS ROW_COUNT").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		switch (pFindType) {
			case ViCommConst.FIND_CALL_RESULT_GROUP_MAN:
				oSqlBuilder.Append(" VW_TALK_HISTORY07 T	").AppendLine();
				break;
			case ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING:
				oSqlBuilder.Append(" VW_TALK_HISTORY06 T	").AppendLine();
				break;
			case ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING:
				oSqlBuilder.Append(" VW_TALK_HISTORY04 T	").AppendLine();
				break;
			default:
				oSqlBuilder.Append(" VW_TALK_HISTORY05 T	").AppendLine();
				break;
		}
		oSqlBuilder.Append("	,VW_USER_MAN_CHARACTER00 P		").AppendLine();

		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pFindType,false,ref sWhere);

		oSqlBuilder.Append(sWhere);

		try {
			conn = DbConnect("TalkHistory.GetPageCount");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pFindType,
		int pPageNo,
		int pRecPerPage
	) {
		DataSet ds;
		try {
			conn = DbConnect("TalkHistory.GetPageCollection");
			ds = new DataSet();

			string sOrder = string.Empty;
			if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) ||
				pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) ||
				pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {

				sOrder = "ORDER BY T.TALK_START_DATE DESC,T.USER_SEQ,T.USER_CHAR_NO ";
			} else {
				sOrder = "ORDER BY T.ACTION_DATE DESC,T.USER_SEQ,T.USER_CHAR_NO ";
			}
			StringBuilder sInnerSql = new StringBuilder();

			sInnerSql.Append("SELECT * FROM ");
			sInnerSql.Append("(SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("	SELECT " + ManBasicField() + ",");
			sInnerSql.Append("	P.NG_SKULL_COUNT			").AppendLine();
			sInnerSql.Append(" ,T.TALK_START_DATE			").AppendLine();
			sInnerSql.Append(" ,T.PARTNER_USER_SEQ			").AppendLine();
			sInnerSql.Append(" ,T.PARTNER_USER_CHAR_NO		").AppendLine();
			sInnerSql.Append(" ,T.TALK_SEQ					").AppendLine();
			sInnerSql.Append(" ,T.UNIQUE_VALUE				").AppendLine();
			if (!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) &&
				!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) &&
				!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {

				sInnerSql.Append(" ,T.IVP_REQUEST_SEQ							").AppendLine();
				sInnerSql.Append(" ,T.REQUEST_ID								").AppendLine();
				sInnerSql.Append(" ,T.MENU_ID									").AppendLine();
				sInnerSql.Append(" ,T.REQUEST_DATE								").AppendLine();
				sInnerSql.Append(" ,T.CONNECT_TYPE								").AppendLine();
				sInnerSql.Append(" ,T.TALK_END_DATE								").AppendLine();
				//結合後にCASE文にてCALL_RESULTを求める 
				sInnerSql.Append(" ,NVL(T.CALL_RESULT,'00') AS TALK_HIS_CALL_RESULT		").AppendLine();
				sInnerSql.Append(" ,T.CHARGE_TYPE								").AppendLine();
				sInnerSql.Append(" ,T.ACTION_DATE								").AppendLine();
				sInnerSql.Append(" ,T.IVP_ACCEPT_SEQ AS TALK_HIS_IVP_ACCEPT_SEQ	").AppendLine();
			}
			sInnerSql.Append("FROM VW_USER_MAN_CHARACTER00 P	 ").AppendLine();

			if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN)) {
				sInnerSql.Append(" ,VW_TALK_HISTORY07 T	").AppendLine();
			} else if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING)) {
				sInnerSql.Append(" ,VW_TALK_HISTORY06 T	").AppendLine();
			} else if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {
				sInnerSql.Append(" ,VW_TALK_HISTORY04 T	").AppendLine();
			} else {
				sInnerSql.Append(" ,VW_TALK_HISTORY05 T	").AppendLine();
			}

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pFindType,true,ref sWhere);
			OracleParameter[] objJoinFieldParms = null;
			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
			string sSql = oObj.sqlSyntax["MAN"].ToString();

			StringBuilder sTables = new StringBuilder();
			StringBuilder sJoinTable = new StringBuilder();
			StringBuilder sJoinField = new StringBuilder();

			string sManNm = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["talkHistoryManNm"]);
			string sCastNm = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["talkHistoryCastNm"]);
			if (sManNm.Equals(string.Empty)) {
				sManNm = "男性";
			}
			if (sCastNm.Equals(string.Empty)) {
				sCastNm = "貴女";
			}

			if (!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) &&
				!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) &&
				!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {
				//sTables
				sTables.Append("T_CODE_DTL T_CODE_DTL6	,	");
				sTables.Append("T_CODE_DTL T_CODE_DTL7	,	");
				sTables.Append("T_CHARGE_TYPE			,	");
				//sJoinTable
				sJoinTable.Append("T1.CHARGE_TYPE			= T_CHARGE_TYPE.CHARGE_TYPE	(+) AND").AppendLine();
				sJoinTable.Append(":CODE_TYPE6				= T_CODE_DTL6.CODE_TYPE		(+)	AND").AppendLine();
				sJoinTable.Append("T1.CONNECT_TYPE			= T_CODE_DTL6.CODE			(+)	AND").AppendLine();
				sJoinTable.Append(":CODE_TYPE7				= T_CODE_DTL7.CODE_TYPE		(+)	AND").AppendLine();
				sJoinTable.Append("T1.TALK_HIS_CALL_RESULT	= T_CODE_DTL7.CODE			(+)	AND").AppendLine();
				//sJoinField
				sJoinField.Append("T_CODE_DTL6.CODE_NM CONNECT_TYPE_NM	,").AppendLine();
				sJoinField.Append("T_CHARGE_TYPE.CHARGE_TYPE_NM			,").AppendLine();
				sJoinField.Append("T_CODE_DTL7.CODE_NM CALL_RESULT_NM	,").AppendLine();
				//
				sJoinField.Append("CASE").AppendLine();
				sJoinField.Append("WHEN (T1.IVP_REQUEST_SEQ IS NULL AND T1.IVP_ACCEPT_SEQ != T1.TALK_HIS_IVP_ACCEPT_SEQ) OR").AppendLine();
				sJoinField.Append("		(T1.IVP_REQUEST_SEQ IS NULL AND APP_COMM.CALC_SECOND(T1.REQUEST_DATE,SYSDATE) >= 120) THEN").AppendLine();
				sJoinField.Append("		'01'").AppendLine();
				sJoinField.Append("WHEN T1.IVP_REQUEST_SEQ IS NULL THEN").AppendLine();
				sJoinField.Append("		'00'").AppendLine();
				sJoinField.Append("ELSE").AppendLine();
				sJoinField.Append("		T1.TALK_HIS_CALL_RESULT").AppendLine();
				sJoinField.Append("END AS CALL_RESULT,").AppendLine();
				//
				sJoinField.Append("CASE").AppendLine();
				sJoinField.Append("WHEN (T1.CHARGE_TYPE IN(:CHARGE_TYPE1,:CHARGE_TYPE2,:CHARGE_TYPE3,:CHARGE_TYPE4) AND T1.TALK_HIS_CALL_RESULT IN(:CALL_RESULT1,:CALL_RESULT2)) THEN ").AppendLine();
				sJoinField.Append("		(SELECT REPLACE(T_ERROR.ERROR_DTL,'{0}','" + sCastNm + "') FROM T_ERROR WHERE T_ERROR.SITE_CD = T1.SITE_CD AND 'CR' || T1.TALK_HIS_CALL_RESULT = T_ERROR.ERROR_CD )").AppendLine();
				sJoinField.Append("WHEN (T1.CHARGE_TYPE IN(:CHARGE_TYPE5,:CHARGE_TYPE6,:CHARGE_TYPE7,:CHARGE_TYPE8) AND T1.TALK_HIS_CALL_RESULT IN(:CALL_RESULT3,:CALL_RESULT4,:CALL_RESULT5,:CALL_RESULT11)) THEN ").AppendLine();
				sJoinField.Append("		(SELECT REPLACE(T_ERROR.ERROR_DTL,'{0}','" + sManNm + "') FROM T_ERROR WHERE T_ERROR.SITE_CD = T1.SITE_CD AND 'CR' || T1.TALK_HIS_CALL_RESULT = T_ERROR.ERROR_CD )").AppendLine();
				sJoinField.Append("WHEN T1.TALK_HIS_CALL_RESULT IN(:CALL_RESULT6,:CALL_RESULT7) THEN ").AppendLine();
				sJoinField.Append("		(SELECT REPLACE(T_ERROR.ERROR_DTL,'{0}','" + sManNm + "') FROM T_ERROR WHERE T_ERROR.SITE_CD = T1.SITE_CD AND 'CR' || T1.TALK_HIS_CALL_RESULT = T_ERROR.ERROR_CD )").AppendLine();
				sJoinField.Append("WHEN T1.TALK_HIS_CALL_RESULT IN(:CALL_RESULT8,:CALL_RESULT9,:CALL_RESULT10,:CALL_RESULT12) THEN ").AppendLine();
				sJoinField.Append("		(SELECT REPLACE(T_ERROR.ERROR_DTL,'{0}','" + sCastNm + "') FROM T_ERROR WHERE T_ERROR.SITE_CD = T1.SITE_CD AND 'CR' || T1.TALK_HIS_CALL_RESULT = T_ERROR.ERROR_CD )").AppendLine();
				sJoinField.Append("WHEN (T1.IVP_REQUEST_SEQ IS NULL AND T1.IVP_ACCEPT_SEQ != T1.TALK_HIS_IVP_ACCEPT_SEQ) OR").AppendLine();
				sJoinField.Append("		(T1.IVP_REQUEST_SEQ IS NULL AND APP_COMM.CALC_SECOND(T1.REQUEST_DATE,SYSDATE) >= 120) THEN").AppendLine();
				sJoinField.Append("		(SELECT T_ERROR.ERROR_DTL FROM T_ERROR WHERE T_ERROR.SITE_CD = T1.SITE_CD AND 'CR01' = T_ERROR.ERROR_CD )").AppendLine();
				sJoinField.Append("WHEN T1.IVP_REQUEST_SEQ IS NULL THEN").AppendLine();
				sJoinField.Append("		(SELECT T_ERROR.ERROR_DTL FROM T_ERROR WHERE T_ERROR.SITE_CD = T1.SITE_CD AND 'CR00' = T_ERROR.ERROR_CD )").AppendLine();
				sJoinField.Append("ELSE").AppendLine();
				sJoinField.Append("		(SELECT T_ERROR.ERROR_DTL FROM T_ERROR WHERE T_ERROR.SITE_CD = T1.SITE_CD AND 'CR' || T1.TALK_HIS_CALL_RESULT = T_ERROR.ERROR_CD )").AppendLine();
				sJoinField.Append("END AS CALL_RESULT_DETAIL, ").AppendLine();
				ArrayList listJoinField = new ArrayList();
				JoinFieldCondition(ref listJoinField);
				objJoinFieldParms = (OracleParameter[])listJoinField.ToArray(typeof(OracleParameter));
			}

			sSql = string.Format(sSql,sInnerSql.ToString(),sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				// JoinField Condition
				cmd.BindByName = true;
				ArrayList list = new ArrayList();
				if (objJoinFieldParms != null) {
					for (int i = 0;i < objJoinFieldParms.Length;i++) {
						cmd.Parameters.Add(objJoinFieldParms[i]);
					}
				}
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);

				// Join Condition
				if (!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) &&
					!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) &&
					!pFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {
					list.Add(new OracleParameter("CODE_TYPE6","43"));
					list.Add(new OracleParameter("CODE_TYPE7","46"));
				}
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"VW_TALK_HISTORY01");
				}
			}
		} finally {
			conn.Close();
		}

		AppendAttr(ds);
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pFindType,bool bAllInfo,ref string pWhere) {
		ArrayList list = new ArrayList();
		StringBuilder sWhere = new StringBuilder();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}
		sWhere.AppendLine();
		sWhere.Append("	WHERE ").AppendLine();
		sWhere.Append("		T.SITE_CD		= :SITE_CD			").AppendLine();
		sWhere.Append(" AND	T.USER_SEQ		= :USER_SEQ			").AppendLine();
		sWhere.Append("	AND	T.USER_CHAR_NO	= :USER_CHAR_NO		").AppendLine();
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));

		if (!pPartnerLoginId.Equals(string.Empty) && !pPartnerUserCharNo.Equals(string.Empty)) {
			sWhere.Append("	AND	P.LOGIN_ID				= :LOGIN_ID				").AppendLine();
			sWhere.Append("	AND	T.PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO	").AppendLine();
			list.Add(new OracleParameter("LOGIN_ID",pPartnerLoginId));
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pPartnerUserCharNo));
		}


		if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_INCOMMING) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_OUTGOING)) {

			sWhere.Append(" AND T.CALL_RESULT IN (:CALL_RESULT1, :CALL_RESULT2, :CALL_RESULT3) ").AppendLine();
			list.Add(new OracleParameter("CALL_RESULT1",ViCommConst.CALL_RESULT_REQUESTER_DISCONN));
			list.Add(new OracleParameter("CALL_RESULT2",ViCommConst.CALL_RESULT_PARTNER_DISCONN));
			list.Add(new OracleParameter("CALL_RESULT3",ViCommConst.CALL_RESULT_POINT_DISC));
		}

		if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_INCOMMING) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_OUTGOING) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_INCOMMING) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_OUTGOING)) {

			sWhere.Append(" AND ((T.REQUEST_ID IN (:REQ1,:REQ2,:REQ3,:REQ4,:REQ5,:REQ6)) OR (T.REQUEST_ID IN (:REQ7) AND T.MENU_ID IN (:MENU1,:MENU2))) ").AppendLine();
			list.Add(new OracleParameter("REQ1",ViCommConst.REQUEST_TALK));
			list.Add(new OracleParameter("REQ2",ViCommConst.REQUEST_TALK_REGULAR));
			list.Add(new OracleParameter("REQ3",ViCommConst.REQUEST_TALK_LIGHT));
			list.Add(new OracleParameter("REQ4",ViCommConst.REQUEST_TALK_PREMIUM));
			list.Add(new OracleParameter("REQ5",ViCommConst.REQUEST_TALK_SMART_IVP));
			list.Add(new OracleParameter("REQ6",ViCommConst.REQUEST_TALK_SMART_DIRECT));
			list.Add(new OracleParameter("REQ7",ViCommConst.REQUEST_VCS_MENU));
			list.Add(new OracleParameter("MENU1",ViCommConst.VCS_MENU_DIRECT_TALK));
			list.Add(new OracleParameter("MENU2",ViCommConst.VCS_MENU_GPF_TALK));

		}

		if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_OUTGOING) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_OUTGOING)) {

			sWhere.Append(" AND ((T.CHARGE_TYPE IN (:TYPE1,:TYPE2,:TYPE3,:TYPE4))) ").AppendLine();
			list.Add(new OracleParameter("TYPE1",ViCommConst.CHARGE_CAST_TALK_PUBLIC));
			list.Add(new OracleParameter("TYPE2",ViCommConst.CHARGE_CAST_TALK_WSHOT));
			list.Add(new OracleParameter("TYPE3",ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT));
			list.Add(new OracleParameter("TYPE4",ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC));
		} else if (pFindType.Equals(ViCommConst.FIND_CALL_RESULT_ALL_INCOMMING) ||
			pFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_INCOMMING)) {

			sWhere.Append(" AND ((T.CHARGE_TYPE IN (:TYPE1,:TYPE2,:TYPE3,:TYPE4,:TYPE5,:TYPE6,:TYPE7,:TYPE8))) ").AppendLine();
			list.Add(new OracleParameter("TYPE1",ViCommConst.CHARGE_TALK_PUBLIC));
			list.Add(new OracleParameter("TYPE2",ViCommConst.CHARGE_TALK_WSHOT));
			list.Add(new OracleParameter("TYPE3",ViCommConst.CHARGE_TALK_VOICE_WSHOT));
			list.Add(new OracleParameter("TYPE4",ViCommConst.CHARGE_VIEW_LIVE));
			list.Add(new OracleParameter("TYPE5",ViCommConst.CHARGE_VIEW_TALK));
			list.Add(new OracleParameter("TYPE6",ViCommConst.CHARGE_VIEW_ONLINE));
			list.Add(new OracleParameter("TYPE7",ViCommConst.CHARGE_WIRETAPPING));
			list.Add(new OracleParameter("TYPE8",ViCommConst.CHARGE_TALK_VOICE_PUBLIC));
		}

		sWhere.Append("	AND	T.SITE_CD				= P.SITE_CD			").AppendLine();
		sWhere.Append("	AND	T.PARTNER_USER_SEQ		= P.USER_SEQ		").AppendLine();
		sWhere.Append("	AND	T.PARTNER_USER_CHAR_NO	= P.USER_CHAR_NO	").AppendLine();

		pWhere = sWhere.ToString();

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void AppendAttr(DataSet pDS) {
		string sSql;
		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_USER_MAN_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD				AND " +
						"USER_SEQ		= :PARTNER_USER_SEQ		AND " +
						"USER_CHAR_NO	= :PARTNER_USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("MAN_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("MAN_ATTR_VALUE{0:D2}",i)] = "";
			}

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
					cmd.Parameters.Add("USER_SEQ",dr["PARTNER_USER_SEQ"].ToString());
					cmd.Parameters.Add("USER_CHAR_NO",dr["PARTNER_USER_CHAR_NO"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub,"T_ATTR_VALUE");
					}
				}
				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("MAN_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	protected void JoinFieldCondition(ref ArrayList pParmList) {
		pParmList.Add(new OracleParameter("CHARGE_TYPE1",ViCommConst.CHARGE_CAST_TALK_PUBLIC));
		pParmList.Add(new OracleParameter("CHARGE_TYPE2",ViCommConst.CHARGE_CAST_TALK_WSHOT));
		pParmList.Add(new OracleParameter("CHARGE_TYPE3",ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT));
		pParmList.Add(new OracleParameter("CHARGE_TYPE4",ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC));
		pParmList.Add(new OracleParameter("CALL_RESULT1",ViCommConst.CALL_RESULT_REQUESTER_DISCONN));
		pParmList.Add(new OracleParameter("CALL_RESULT2",ViCommConst.CALL_RESULT_NO_MACH));
		pParmList.Add(new OracleParameter("CHARGE_TYPE5",ViCommConst.CHARGE_CAST_TALK_PUBLIC));
		pParmList.Add(new OracleParameter("CHARGE_TYPE6",ViCommConst.CHARGE_CAST_TALK_WSHOT));
		pParmList.Add(new OracleParameter("CHARGE_TYPE7",ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT));
		pParmList.Add(new OracleParameter("CHARGE_TYPE8",ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC));
		pParmList.Add(new OracleParameter("CALL_RESULT3",ViCommConst.CALL_RESULT_PARTNER_DISCONN));
		pParmList.Add(new OracleParameter("CALL_RESULT4",ViCommConst.CALL_RESULT_NO_ANSWER));
		pParmList.Add(new OracleParameter("CALL_RESULT5",ViCommConst.CALL_RESULT_BUSY));
		pParmList.Add(new OracleParameter("CALL_RESULT11",ViCommConst.CALL_RESULT_REFUSE));
		pParmList.Add(new OracleParameter("CALL_RESULT6",ViCommConst.CALL_RESULT_REQUESTER_DISCONN));
		pParmList.Add(new OracleParameter("CALL_RESULT7",ViCommConst.CALL_RESULT_NO_MACH));
		pParmList.Add(new OracleParameter("CALL_RESULT8",ViCommConst.CALL_RESULT_PARTNER_DISCONN));
		pParmList.Add(new OracleParameter("CALL_RESULT9",ViCommConst.CALL_RESULT_NO_ANSWER));
		pParmList.Add(new OracleParameter("CALL_RESULT10",ViCommConst.CALL_RESULT_BUSY));
		pParmList.Add(new OracleParameter("CALL_RESULT12",ViCommConst.CALL_RESULT_REFUSE));
	}

}

