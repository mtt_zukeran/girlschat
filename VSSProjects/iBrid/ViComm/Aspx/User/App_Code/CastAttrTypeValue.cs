﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者属性値
--	Progaram ID		: CastAttrTypeValue
--
--  Creation Date	: 2009.07.16
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections;
using ViComm;
using iBridCommLib;

[System.Serializable]
public class CastAttrTypeValue:DbSession {

	public CastAttrTypeValue() {
	}

	public string GetSeqByItemNo(string pSiteCd, string pAttrTypeSeq, string pItemCd) {

		System.Text.StringBuilder objSqlBuilder = new System.Text.StringBuilder();
		objSqlBuilder.Append(" SELECT                                                                          ").AppendLine();
		objSqlBuilder.Append("     CAST_ATTR_SEQ                                                               ").AppendLine();
		objSqlBuilder.Append(" FROM                                                                            ").AppendLine();
		objSqlBuilder.Append("     T_CAST_ATTR_TYPE_VALUE                                                      ").AppendLine();
		objSqlBuilder.Append(" WHERE                                                                           ").AppendLine();
		objSqlBuilder.Append("         SITE_CD            = :SITE_CD    AND                                    ").AppendLine();
		objSqlBuilder.Append("         CAST_ATTR_TYPE_SEQ = :CAST_ATTR_TYPE_SEQ    AND                         ").AppendLine();
		objSqlBuilder.Append("         ITEM_CD            = :ITEM_CD                                           ").AppendLine();

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":CAST_ATTR_TYPE_SEQ", pAttrTypeSeq);
				cmd.Parameters.Add(":ITEM_CD", pItemCd);

				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
	}
	public DataSet GetList(string pSiteCd,string pCastAttrTypeSeq) {
		DataSet ds;
		try{
			conn = DbConnect("CastAttrTypeValue.GetList");

			string sSql = "SELECT " +
								"CAST_ATTR_SEQ			, " +
								"CAST_ATTR_NM			, " +
								"GROUPING_CD			, " +
								"OMIT_SEEK_CONTION_FLAG	  " +
							"FROM " +
								"T_CAST_ATTR_TYPE_VALUE " +
							"WHERE " +
								"SITE_CD			= :SITE_CD				AND " +
								"CAST_ATTR_TYPE_SEQ = :CAST_ATTR_TYPE_SEQ		" +
							"ORDER BY " +
								"PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_ATTR_TYPE_SEQ",pCastAttrTypeSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_ATTR_TYPE_VALUE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetPageCount(string pSiteCd,string pItemNo,string pGroupingCd,int pRecPerPage,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try{
			conn = DbConnect("CastAttrTypeValue.GetPageCount");

			string sSql = "SELECT COUNT(*) AS ROW_COUNT " +
								"FROM " +
									"VW_CAST_ATTR_TYPE_VALUE01 ";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pItemNo,pGroupingCd,ViCommConst.FLAG_OFF.ToString(),ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pItemNo,string pGroupingCd,int pPageNo,int pRecPerPage) {
		DataSet ds;
		try{
			conn = DbConnect("CastAttrTypeValue.GetPageCollection");
			ds = new DataSet();

			string sSql = "SELECT * FROM " +
							"(" +
								"SELECT " +
									"CAST_ATTR_TYPE_SEQ		," +
									"CAST_ATTR_SEQ			," +
									"CAST_ATTR_NM			," +
									"PRIORITY				," +
									"ROW_NUMBER() OVER (ORDER BY PRIORITY) AS RNUM " +
								"FROM " +
									"VW_CAST_ATTR_TYPE_VALUE01 ";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pItemNo,pGroupingCd,ViCommConst.FLAG_OFF.ToString(),ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + "ORDER BY PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_ATTR_TYPE_VALUE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pItemNo,string pGroupingCd,string pOmitSeekContionFlag,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pItemNo.Equals("")) {
			SysPrograms.SqlAppendWhere("ITEM_NO = :ITEM_NO ",ref pWhere);
			list.Add(new OracleParameter("ITEM_NO",pItemNo));
		}

		if (!pGroupingCd.Equals("")) {
			SysPrograms.SqlAppendWhere("GROUPING_CD = :GROUPING_CD ",ref pWhere);
			list.Add(new OracleParameter("GROUPING_CD",pGroupingCd));
		}

		if (!pOmitSeekContionFlag.Equals("")) {
			SysPrograms.SqlAppendWhere("OMIT_SEEK_CONTION_FLAG = :OMIT_SEEK_CONTION_FLAG ",ref pWhere);
			list.Add(new OracleParameter("OMIT_SEEK_CONTION_FLAG",pOmitSeekContionFlag));
		}
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
	
	public DataTable GetAttrDataTable(string pSiteCd,string pUserSeq,string pUserCharNo) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT					").AppendLine();
		oSqlBuilder.Append("	DISPLAY_VALUE	,	").AppendLine();
		oSqlBuilder.Append("	ITEM_NO				").AppendLine();
		oSqlBuilder.Append(" FROM								").AppendLine();
		oSqlBuilder.Append("	VW_CAST_ATTR_VALUE01		").AppendLine();
		oSqlBuilder.Append(" WHERE								").AppendLine();
		oSqlBuilder.Append("	SITE_CD			= :SITE_CD		AND		").AppendLine();
		oSqlBuilder.Append("	USER_SEQ		= :USER_SEQ		AND		").AppendLine();
		oSqlBuilder.Append("	USER_CHAR_NO	= :USER_CHAR_NO			").AppendLine();
		oSqlBuilder.Append(" ORDER BY				").AppendLine();
		oSqlBuilder.Append("	CAST_ATTR_TYPE_PRIORITY	").AppendLine();


		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);

				DataTable oAttrDataTable = new DataTable();

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oAttrDataTable);
				}

				return oAttrDataTable;
			}
		} finally {
			conn.Close();
		}
	}
}