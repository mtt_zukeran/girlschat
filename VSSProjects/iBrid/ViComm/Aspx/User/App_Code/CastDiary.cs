﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者日記

--	Progaram ID		: CastDiary
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Configuration;
using MobileLib;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class CastDiary:CastBase {

	public int recNo;
	public string year;
	public string month;
	public string day;
	public string reportDay;
	public string subSeq;
	public string diaryTitle;
	public string diaryDoc;
	public string picSeq;
	public string photoPath;
	public string handleNm;
	public string castSeq;
	public string castDiaryAttrSeq;

	public CastDiary() {
	}
	public CastDiary(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect("CastDiary.GetOne");

			string sSql = "SELECT " +
								"*	" +
							"FROM " +
								"T_CAST_DIARY " +
							"WHERE " +
								" SITE_CD		= :SITE_CD		AND " +
								" USER_SEQ		= :USER_SEQ		AND " +
								" USER_CHAR_NO	= :USER_CHAR_NO	AND " +
								" REPORT_DAY	= :REPORT_DAY ";
			if (!string.IsNullOrEmpty(pSubSeq)) {
				sSql += " AND CAST_DIARY_SUB_SEQ = :CAST_DIARY_SUB_SEQ ";
			}
			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("REPORT_DAY",pReportDay);
				if (!string.IsNullOrEmpty(pSubSeq)) {
					cmd.Parameters.Add("CAST_DIARY_SUB_SEQ",pSubSeq);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_DIARY");

					if (ds.Tables["T_CAST_DIARY"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_DIARY"].Rows[0];
						year = dr["REPORT_DAY"].ToString().Substring(0,4);
						month = dr["REPORT_DAY"].ToString().Substring(5,2);
						day = dr["REPORT_DAY"].ToString().Substring(8,2);
						subSeq = dr["CAST_DIARY_SUB_SEQ"].ToString();
						diaryTitle = dr["DIARY_TITLE"].ToString();
						diaryDoc = string.Format("{0}{1}{2}{3}",dr["HTML_DOC1"].ToString(),dr["HTML_DOC2"].ToString(),dr["HTML_DOC3"].ToString(),dr["HTML_DOC4"].ToString());
						picSeq = dr["PIC_SEQ"].ToString();
						castDiaryAttrSeq = dr["CAST_DIARY_ATTR_SEQ"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCountByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,bool pNewDiaryOnlyFlag,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountByLoginId(pSiteCd,pLoginId,pUserCharNo,pNewDiaryOnlyFlag,new SeekCondition(),pRecPerPage,pUseMV,out pRecCount);
	}

	public int GetPageCountByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,bool pNewDiaryOnlyFlag,SeekCondition pCondition,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		return GetPageCountByLoginId(pSiteCd,pLoginId,pUserCharNo,pNewDiaryOnlyFlag,false,pCondition,pRecPerPage,pUseMV,out pRecCount);
	}

	public int GetPageCountByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,bool pNewDiaryOnlyFlag,bool pDiarySwitchScreenFlag,SeekCondition pCondition,int pRecPerPage,bool pUseMV,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.None);
		try {
			conn = DbConnect("CastDiary.GetPageCountByLoginId");

			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_DIARY00 P ");
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pSiteCd,pLoginId,pUserCharNo,pNewDiaryOnlyFlag,pDiarySwitchScreenFlag,false,pCondition,ref sWhere);
			oSql.AppendLine(sWhere);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollectionByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,bool pNewDiaryOnlyFlag,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionByLoginId(pSiteCd,pLoginId,pUserCharNo,pNewDiaryOnlyFlag,new SeekCondition(),pPageNo,pRecPerPage,pUseMV);
	}

	public DataSet GetPageCollectionByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,bool pNewDiaryOnlyFlag,SeekCondition pCondition,int pPageNo,int pRecPerPage,bool pUseMV) {
		return GetPageCollectionByLoginId(pSiteCd,pLoginId,pUserCharNo,pNewDiaryOnlyFlag,false,pCondition,pPageNo,pRecPerPage,pUseMV);
	}

	public DataSet GetPageCollectionByLoginId(string pSiteCd,string pLoginId,string pUserCharNo,bool pNewDiaryOnlyFlag,bool pDiarySwitchScreenFlag,SeekCondition pCondition,int pPageNo,int pRecPerPage,bool pUseMV) {
		DataSet ds;
		FlexibleQueryTargetInfo oFlexibleQueryTarget = this.CreateFlexibleQueryTarget(FlexibleQueryType.None);
		try {
			conn = DbConnect("CastDiary.GetPageCollectionByLoginId");

			ds = new DataSet();

			string sOrder;
			sOrder = "ORDER BY P.DIARY_CREATE_DATE DESC";

			System.Text.StringBuilder sInnerSql = new System.Text.StringBuilder();
			sInnerSql.Append("SELECT * FROM( ");
			sInnerSql.Append("	SELECT ROWNUM AS RNUM,INNER.* FROM (");
			sInnerSql.Append("		SELECT " + CastBasicField() + ",");
			sInnerSql.Append("			P.REPORT_DAY,			").AppendLine();
			sInnerSql.Append("			P.CAST_DIARY_SUB_SEQ,	").AppendLine();
			sInnerSql.Append("			P.REPORT_DAY DIARY_DAY,	").AppendLine();
			sInnerSql.Append("			P.UNIQUE_VALUE,			").AppendLine();
			sInnerSql.Append("			P.DIARY_HEADER_TITLE,	").AppendLine();
			sInnerSql.Append("			P.DIARY_TITLE,			").AppendLine();
			sInnerSql.Append("			P.HTML_DOC1	,			").AppendLine();
			sInnerSql.Append("			P.HTML_DOC2	,			").AppendLine();
			sInnerSql.Append("			P.HTML_DOC3	,			").AppendLine();
			sInnerSql.Append("			P.HTML_DOC4	,			").AppendLine();
			sInnerSql.Append("			P.PIC_SEQ	,			").AppendLine();
			sInnerSql.Append("			P.DIARY_UPDATE_DATE	,	").AppendLine();
			sInnerSql.Append("			P.DIARY_CREATE_DATE	,	").AppendLine();
			sInnerSql.Append("			P.ATTACHED_OBJ_TYPE	,	").AppendLine();
			sInnerSql.Append("			P.DEL_FLAG			,	").AppendLine();
			sInnerSql.Append("			P.ADMIN_DEL_FLAG	,	").AppendLine();
			sInnerSql.Append("			P.READING_COUNT		,	").AppendLine();
			sInnerSql.Append("			P.SCREEN_ID			,	").AppendLine();
			sInnerSql.Append("			P.LIKE_COUNT			").AppendLine();
			sInnerSql.Append("		FROM 						").AppendLine();
			sInnerSql.Append("			VW_CAST_DIARY00 P		").AppendLine();
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(oFlexibleQueryTarget,pSiteCd,pLoginId,pUserCharNo,pNewDiaryOnlyFlag,pDiarySwitchScreenFlag,true,pCondition,ref sWhere);

			sInnerSql.Append(sWhere).AppendLine();
			sInnerSql.Append(sOrder).AppendLine();

			sInnerSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sInnerSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
			string sTemplate = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TemplateSuffix"]) + "CAST";
			string sSql = oObj.sqlSyntax[sTemplate].ToString();

			StringBuilder sTables = new StringBuilder();
			StringBuilder sJoinTable = new StringBuilder();
			StringBuilder sJoinField = new StringBuilder();

			sJoinField.Append("GET_PHOTO_IMG_PATH		(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,T1.PROFILE_PIC_SEQ)	AS OBJ_PHOTO_IMG_PATH,").AppendLine();
			sJoinField.Append("GET_SMALL_PHOTO_IMG_PATH	(T1.SITE_CD,T1.LOGIN_ID,T1.PIC_SEQ,T1.PROFILE_PIC_SEQ)	AS OBJ_SMALL_PHOTO_IMG_PATH,").AppendLine();

			sSql = string.Format(sSql,sInnerSql.ToString(),sTables.ToString(),sJoinTable.ToString(),sJoinField.ToString());

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
				cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
				cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				// Join Condition
				ArrayList list = new ArrayList();
				JoinCondition(ref list);
				objParms = (OracleParameter[])list.ToArray(typeof(OracleParameter));
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_DIARY00");
				}
			}
		} finally {
			conn.Close();
		}
		ds = this.Exclude(ds,pRecPerPage,oFlexibleQueryTarget);

		AppendAttr(ds);

		return ds;
	}

	private FlexibleQueryTargetInfo CreateFlexibleQueryTarget(FlexibleQueryType pFlexibleQueryType) {
		FlexibleQueryTargetInfo oFlexibleQueryTarget = new FlexibleQueryTargetInfo(pFlexibleQueryType);
		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			oFlexibleQueryTarget.JealousyDiary = true;
		}
		return oFlexibleQueryTarget;
	}

	private void AppendAttr(DataSet pDS) {
		SessionObjs oObj = (SessionObjs)HttpContext.Current.Session["objs"];
		string sSql;

		sSql = "SELECT DISPLAY_VALUE,ITEM_NO FROM VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD		AND " +
						"USER_SEQ		= :USER_SEQ		AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO		";

		DataColumn col;
		for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
			col = new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}",i),Type.GetType("System.String"));
			pDS.Tables[0].Columns.Add(col);
		}

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			dr["CHARACTER_ONLINE_STATUS"] = dr["FAKE_ONLINE_STATUS"].ToString();
			dr["LAST_ACTION_DATE"] = dr["FAKE_LAST_ACTION_DATE"].ToString();

			for (int i = 1;i <= ViCommConst.MAX_ATTR_COUNT;i++) {
				dr[string.Format("CAST_ATTR_VALUE{0:D2}",i)] = "";
			}
			using (DataSet dsSub = new DataSet()) {
				try {
					using (cmd = CreateSelectCommand(sSql,conn)) {
						cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
						cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
						cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());

						using (da = new OracleDataAdapter(cmd)) {
							da.Fill(dsSub,"T_ATTR_VALUE");
						}
					}
				} finally {
					conn.Close();
				}

				foreach (DataRow drSub in dsSub.Tables[0].Rows) {
					dr[string.Format("CAST_ATTR_VALUE{0}",drSub["ITEM_NO"])] = drSub["DISPLAY_VALUE"].ToString();
				}
			}
		}
	}

	public int CollectionRNumByReportDay(string pSiteCd,string pLoginId,string pUserCharNo,string pReportDay,string pSubSeq,bool pUseMV) {
		DataSet ds;
		ds = new DataSet();
		int iRNum = 0;

		try {
			conn = DbConnect("CastDiary.CollectionRNumByReportDay");

			string sOrder = "ORDER BY D.USER_SEQ,D.USER_CHAR_NO,D.REPORT_DAY DESC,D.DIARY_CREATE_DATE DESC ";
			string sOrder2 = "ORDER BY USER_SEQ,USER_CHAR_NO,REPORT_DAY DESC,DIARY_CREATE_DATE DESC ";

			string sSql = "SELECT * FROM " +
							"(" +
							"SELECT " +
								"USER_SEQ			," +
								"USER_CHAR_NO		," +
								"REPORT_DAY			," +
								"CAST_DIARY_SUB_SEQ	," +
								"DIARY_UPDATE_DATE	," +
								"DIARY_CREATE_DATE	," +
								"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM " +
							"FROM VW_CAST_DIARY00 D ";

			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pLoginId,pUserCharNo,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE REPORT_DAY = :REPORT_DAY ";
			if (!string.IsNullOrEmpty(pSubSeq)) {
				sSql = sSql + " AND CAST_DIARY_SUB_SEQ = :CAST_DIARY_SUB_SEQ ";
			}
			sSql = sSql + sOrder2;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				// ReplaceはCrawler対応 
				cmd.Parameters.Add("REPORT_DAY",pReportDay.Replace("-","/"));
				if (!string.IsNullOrEmpty(pSubSeq)) {
					cmd.Parameters.Add("CAST_DIARY_SUB_SEQ",pSubSeq);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_DIARY00");

					if (ds.Tables["VW_CAST_DIARY00"].Rows.Count != 0) {
						DataRow dr = ds.Tables["VW_CAST_DIARY00"].Rows[0];
						iRNum = int.Parse(dr["RNUM"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iRNum;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pLoginId,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();
		if (!pLoginId.Equals("")) {
			pWhere = " WHERE SITE_CD = :SITE_CD AND LOGIN_ID = :LOGIN_ID AND USER_CHAR_NO = :USER_CHAR_NO AND REPORT_DAY <= :REPORT_DAY ";
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
			list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			list.Add(new OracleParameter("REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));
		} else {
			pWhere = " WHERE SITE_CD = :SITE_CD AND  REPORT_DAY <= :REPORT_DAY ";
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
			list.Add(new OracleParameter("REPORT_DAY",DateTime.Now.ToString("yyyy/MM/dd")));
		}
		pWhere += " AND NA_FLAG = :NA_FLAG ";
		list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
		pWhere += " AND WAIT_TX_FLAG = :WAIT_TX_FLAG ";
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));
		SysPrograms.SqlAppendWhere("DEL_FLAG = :DEL_FLAG ",ref pWhere);
		list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));
		SysPrograms.SqlAppendWhere("ADMIN_DEL_FLAG = :ADMIN_DEL_FLAG ",ref pWhere);
		list.Add(new OracleParameter("ADMIN_DEL_FLAG",ViCommConst.FLAG_OFF));

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = D.SITE_CD AND USER_SEQ = D.USER_SEQ AND USER_CHAR_NO = D.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}


		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private OracleParameter[] CreateWhere(FlexibleQueryTargetInfo pFlexibleQueryTarget,string pSiteCd,string pLoginId,string pUserCharNo,bool pNewDiaryOnlyFlag,bool pDiarySwitchScreenFlag,bool bAllInfo,SeekCondition pCondition,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (bAllInfo) {
			InnnerCondition(ref list);
		}
		
		if (pDiarySwitchScreenFlag) {
			if (string.IsNullOrEmpty(pCondition.screenId)) {
				SysPrograms.SqlAppendWhere("P.SCREEN_ID IS NULL",ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("P.SCREEN_ID = :SCREEN_ID",ref pWhere);
				list.Add(new OracleParameter("SCREEN_ID",pCondition.screenId));
			}
		}
		if (!pLoginId.Equals("")) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD AND P.LOGIN_ID = :LOGIN_ID AND P.USER_CHAR_NO = :USER_CHAR_NO ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
			list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		} else {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pCondition.hasPic)) {
			if (ViCommConst.FLAG_ON_STR.Equals(pCondition.hasPic)) {
				SysPrograms.SqlAppendWhere("P.PIC_SEQ IS NOT NULL",ref pWhere);
			} else if (ViCommConst.FLAG_OFF_STR.Equals(pCondition.hasPic)) {
				SysPrograms.SqlAppendWhere("P.ATTACHED_OBJ_TYPE = :ATTACHED_OBJ_TYPE",ref pWhere);
				list.Add(new OracleParameter("ATTACHED_OBJ_TYPE",ViCommConst.DiaryObjType.DOC));
			}
		}
		SysPrograms.SqlAppendWhere("P.NA_FLAG = :NA_FLAG ",ref pWhere);
		list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
		SysPrograms.SqlAppendWhere("P.WAIT_TX_FLAG = :WAIT_TX_FLAG ",ref pWhere);
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));
		SysPrograms.SqlAppendWhere("P.DEL_FLAG = :DEL_FLAG ",ref pWhere);
		list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));
		SysPrograms.SqlAppendWhere("P.ADMIN_DEL_FLAG = :ADMIN_DEL_FLAG ",ref pWhere);
		list.Add(new OracleParameter("ADMIN_DEL_FLAG",ViCommConst.FLAG_OFF));

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (this.sessionObj != null) {
					if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
						SessionMan oSessionMan = (SessionMan)this.sessionObj;
						sUserSeq = oSessionMan.userMan.userSeq;
						sUserCharNo = oSessionMan.userMan.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}

		if (this.sessionObj.sexCd.Equals(ViCommConst.MAN)) {
			SessionMan oSessionMan = (SessionMan)this.sessionObj;
			pWhere += SetFlexibleQuery(pFlexibleQueryTarget,oSessionMan.site.siteCd,oSessionMan.userMan.userSeq,oSessionMan.userMan.userCharNo,ref list);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void WriteDiary(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pCastDiarySubSeq,string pDiaryTitle,string pCastDiaryAttrSeq,string pDiaryDoc,bool pWaitFlag,string pScreenId,out string pUploadKey) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_DIARY");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PREPORT_DAY",DbSession.DbType.VARCHAR2,pReportDay);
			db.ProcedureInParm("PCAST_DIARY_SUB_SEQ",DbSession.DbType.VARCHAR2,pCastDiarySubSeq);
			db.ProcedureInParm("PDIARY_TITLE",DbSession.DbType.VARCHAR2,pDiaryTitle);
			db.ProcedureInParm("PCAST_DIARY_ATTR_SEQ",DbSession.DbType.NUMBER,int.Parse(pCastDiaryAttrSeq));
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.ARRAY_VARCHAR2,SysPrograms.SplitBytes(Encoding.UTF8,pDiaryDoc,2500));
			db.ProcedureInParm("PWAIT_FLAG",DbSession.DbType.NUMBER,pWaitFlag);
			db.ProcedureOutParm("PUPLOAD_KEY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PSCREEN_ID",DbSession.DbType.VARCHAR2,pScreenId);
			db.ExecuteProcedure();

			pUploadKey = db.GetStringValue("PUPLOAD_KEY");
		}
	}

	public void DeleteDiary(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq,out string pFileNm) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_DIARY");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PREPORT_DAY",DbSession.DbType.VARCHAR2,pReportDay);
			db.ProcedureInParm("PCAST_DIARY_SUB_SEQ",DbSession.DbType.VARCHAR2,pSubSeq);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON_STR);
			db.ProcedureInParm("PADMIN_DEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF_STR);
			db.ProcedureOutParm("PPIC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pFileNm = db.GetStringValue("PPIC_SEQ");
		}
	}

	public void UpdateReadingCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("UPDATE_CAST_DIARY_COUNT");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pREPORT_DAY",DbSession.DbType.VARCHAR2,pReportDay);
			oDbSession.ProcedureInParm("pCAST_DIARY_SUB_SEQ",DbSession.DbType.VARCHAR2,pSubSeq);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}

	public void UpdateLikeCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq,string pPartnerUserSeq,string pPartnerUserCharNo,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_DIARY_LIKE_COUNT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pREPORT_DAY",DbSession.DbType.VARCHAR2,pReportDay);
			db.ProcedureInParm("pCAST_DIARY_SUB_SEQ",DbSession.DbType.VARCHAR2,pSubSeq);
			db.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pResult = db.GetStringValue("pRESULT");
		}
	}

	public DataSet GetOneVwByReportDay(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq) {
		DataSet oDataSet = null;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	" + CastBasicField() + "							,	");
		oSqlBuilder.AppendLine("	P.REPORT_DAY										,	");
		oSqlBuilder.AppendLine("	P.CAST_DIARY_SUB_SEQ								,	");
		oSqlBuilder.AppendLine("	P.REPORT_DAY DIARY_DAY								,	");
		oSqlBuilder.AppendLine("	P.UNIQUE_VALUE										,	");
		oSqlBuilder.AppendLine("	P.DIARY_HEADER_TITLE								,	");
		oSqlBuilder.AppendLine("	P.DIARY_TITLE										,	");
		oSqlBuilder.AppendLine("	P.HTML_DOC1											,	");
		oSqlBuilder.AppendLine("	P.HTML_DOC2											,	");
		oSqlBuilder.AppendLine("	P.HTML_DOC3											,	");
		oSqlBuilder.AppendLine("	P.HTML_DOC4											,	");
		oSqlBuilder.AppendLine("	P.PIC_SEQ											,	");
		oSqlBuilder.AppendLine("	P.DIARY_UPDATE_DATE									,	");
		oSqlBuilder.AppendLine("	P.DIARY_CREATE_DATE									,	");
		oSqlBuilder.AppendLine("	P.ATTACHED_OBJ_TYPE									,	");
		oSqlBuilder.AppendLine("	P.DEL_FLAG											,	");
		oSqlBuilder.AppendLine("	P.ADMIN_DEL_FLAG									,	");
		oSqlBuilder.AppendLine("	P.READING_COUNT										,	");
		oSqlBuilder.AppendLine("	P.LIKE_COUNT										,	");
		oSqlBuilder.AppendLine("	GET_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,P.PIC_SEQ,P.PROFILE_PIC_SEQ)	AS OBJ_PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("	GET_SMALL_PHOTO_IMG_PATH(P.SITE_CD,P.LOGIN_ID,P.PIC_SEQ,P.PROFILE_PIC_SEQ)	AS OBJ_SMALL_PHOTO_IMG_PATH		");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	VW_CAST_DIARY00 P										");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	P.SITE_CD				= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ				= :USER_SEQ					AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO			= :USER_CHAR_NO				AND	");
		oSqlBuilder.AppendLine("	P.REPORT_DAY			= :REPORT_DAY				AND	");
		oSqlBuilder.AppendLine("	P.CAST_DIARY_SUB_SEQ	= :CAST_DIARY_SUB_SEQ		AND	");
		oSqlBuilder.AppendLine("	P.WAIT_TX_FLAG			= :WAIT_TX_FLAG				AND	");
		oSqlBuilder.AppendLine("	P.DEL_FLAG				= :DEL_FLAG					AND	");
		oSqlBuilder.AppendLine("	P.ADMIN_DEL_FLAG		= :ADMIN_DEL_FLAG				");
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":REPORT_DAY",pReportDay.Replace("-","/")));
		oParamList.Add(new OracleParameter(":CAST_DIARY_SUB_SEQ",pSubSeq));
		oParamList.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));
		oParamList.Add(new OracleParameter("ADMIN_DEL_FLAG",ViCommConst.FLAG_OFF));

		// order by
		string sSortExpression = " ORDER BY P.DIARY_UPDATE_DATE DESC";

		string sExecSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,0,1,out sExecSql));

		oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;
	}
}
