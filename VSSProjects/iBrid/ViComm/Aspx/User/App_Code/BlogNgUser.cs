﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾞﾛｸﾞｺﾒﾝﾄ制限ﾕｰｻﾞｰ
--	Progaram ID		: BlogNgUser
--
--  Creation Date	: 2012.12.26
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[Serializable]
public class BlogNgUserSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}
	
	public string UserSeq {
		get {
			return this.Query["user_seq"];
		}
		set {
			this.Query["user_seq"] = value;
		}
	}
	
	public string UserCharNo {
		get {
			return this.Query["user_char_no"];
		}
		set {
			this.Query["user_char_no"] = value;
		}
	}

	public BlogNgUserSeekCondition()
		: this(new NameValueCollection()) {
	}
	public BlogNgUserSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["user_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_seq"]));
		this.Query["user_char_no"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["user_char_no"]));
	}
}
#endregion ============================================================================================================

public class BlogNgUser:ManBase {

	public BlogNgUser()
		: base() {
	}

	public BlogNgUser(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(BlogNgUserSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P	,	");
		oSqlBuilder.AppendLine("	T_BLOG_NG_USER BNG				");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(BlogNgUserSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "			");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00 P	,	");
		oSqlBuilder.AppendLine("	T_BLOG_NG_USER BNG				");

		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);

		string sExecSql = oSqlBuilder.ToString();

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(sExecSql,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;

	}

	private OracleParameter[] CreateWhere(BlogNgUserSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null)
			throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("BNG.SITE_CD			= :SITE_CD					",ref pWhereClause);
		SysPrograms.SqlAppendWhere("BNG.USER_SEQ		= :USER_SEQ					",ref pWhereClause);
		SysPrograms.SqlAppendWhere("BNG.USER_CHAR_NO	= :USER_CHAR_NO				",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.SITE_CD			= BNG.SITE_CD				",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_SEQ			= BNG.PARTNER_USER_SEQ		",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.USER_CHAR_NO		= BNG.PARTNER_USER_CHAR_NO	",ref pWhereClause);
		oParamList.Add(new OracleParameter("SITE_CD",pCondition.SiteCd));
		oParamList.Add(new OracleParameter("USER_SEQ",pCondition.UserSeq));
		oParamList.Add(new OracleParameter("USER_CHAR_NO",pCondition.UserCharNo));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(BlogNgUserSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		sSortExpression = " ORDER BY BNG.CREATE_DATE DESC";

		return sSortExpression;
	}

	#endregion ========================================================================================================

	public void RegistBlogNgUser(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("REGIST_BLOG_NG_USER");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.NUMBER,pPartnerUserSeq);
			oDbSession.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	public void DeleteBlogNgUser(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_BLOG_NG_USER");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pPARTNER_USER_SEQ",DbSession.DbType.NUMBER,pPartnerUserSeq);
			oDbSession.ProcedureInParm("pPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}
	
	public bool CheckExistBlogNgUserByBlogArticleSeq(string pSiteCd,string pBlogArticleSeq,string pPartnerUserSeq,string pPartnerUserCharNo) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	1														");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_BLOG_NG_USER BNU										");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD					AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ		= :PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	EXISTS													");
		oSqlBuilder.AppendLine("	(														");
		oSqlBuilder.AppendLine("		SELECT												");
		oSqlBuilder.AppendLine("			*												");
		oSqlBuilder.AppendLine("		FROM												");
		oSqlBuilder.AppendLine("			T_BLOG_ARTICLE									");
		oSqlBuilder.AppendLine("		WHERE												");
		oSqlBuilder.AppendLine("			SITE_CD				= BNU.SITE_CD			AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= BNU.USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= BNU.USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("			BLOG_ARTICLE_SEQ	= :BLOG_ARTICLE_SEQ			");
		oSqlBuilder.AppendLine("	)														");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":BLOG_ARTICLE_SEQ",pBlogArticleSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pPartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pPartnerUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public bool CheckExistBlogNgUser(
		string pSiteCd,
		string pCastUserSeq,
		string pCastCharNo,
		string pManUserSeq,
		string pManUserCharNo
	) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_BLOG_NG_USER");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND");
		oSqlBuilder.AppendLine("	PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pCastUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pCastCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pManUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pManUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
