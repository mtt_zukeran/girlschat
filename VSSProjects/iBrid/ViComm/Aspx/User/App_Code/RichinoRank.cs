/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: User
--	Title			: リッチーノランク


--	Progaram ID		: RichinoRank
--
--  Creation Date	: 2011.04.18
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class RichinoRank:DbSession {

	public RichinoRank() {
	}

	public DataSet GetRichinoRankList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	*	").AppendLine();
			sSql.Append("FROM	").AppendLine();
			sSql.Append("	T_RICHINO_RANK	").AppendLine();
			sSql.Append("WHERE	").AppendLine();
			sSql.Append("SITE_CD =:SITE_CD	").AppendLine();
			sSql.Append("ORDER BY SITE_CD,RICHINO_RANK	").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_RICHINO_RANK");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetValue(string pSiteCd,string pRichinoRank,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"*	" +
							"FROM " +
								"T_RICHINO_RANK " +
							"WHERE " +
								"SITE_CD		= :SITE_CD		AND	" +
								"RICHINO_RANK	= :RICHINO_RANK	";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("RICHINO_RANK",pRichinoRank);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_RICHINO_RANK");
					if (ds.Tables["T_RICHINO_RANK"].Rows.Count != 0) {
						dr = ds.Tables["T_RICHINO_RANK"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
	
	public DataSet GetUserRichinoRank(string pSiteCd,string pUserSeq) {
		DataSet oDataSet = null;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	R.MAN_TWEET_DAILY_COUNT					,	");
		oSqlBuilder.AppendLine("	R.MAN_TWEET_INTERVAL_MIN					");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER_EX U				,	");
		oSqlBuilder.AppendLine("	T_RICHINO_RANK R							");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	U.SITE_CD		= R.SITE_CD				AND	");
		oSqlBuilder.AppendLine("	U.RICHINO_RANK	= R.RICHINO_RANK		AND	");
		oSqlBuilder.AppendLine("	U.SITE_CD		= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	U.USER_SEQ		= :USER_SEQ					");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		
		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

	public DataSet GetSelfRichinoRankKeepAmt(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet oDataSet = null;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																														");
		oSqlBuilder.AppendLine("	RR.RICHINO_RANK_KEEP_AMT - UM.MONTHLY_RECEIPT_AMT AS REST_RICHINO_RANK_KEEP_AMT										,	");
		oSqlBuilder.AppendLine("	TO_DATE(TO_CHAR(LAST_DAY(SYSDATE),'YYYY/MM/DD') || ' 23:59:59','YYYY/MM/DD HH24:MI:SS') AS RANK_KEEP_CLOSE_DATE			");
		oSqlBuilder.AppendLine("FROM																														");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER_EX UMC																							,	");
		oSqlBuilder.AppendLine("	T_USER_MAN UM																										,	");
		oSqlBuilder.AppendLine("	T_RICHINO_RANK RR																										");
		oSqlBuilder.AppendLine("WHERE																														");
		oSqlBuilder.AppendLine("	UMC.USER_SEQ					= UM.USER_SEQ																		AND	");
		oSqlBuilder.AppendLine("	UMC.RICHINO_RANK				= RR.RICHINO_RANK																	AND	");
		oSqlBuilder.AppendLine("	UMC.SITE_CD						= :SITE_CD																			AND	");
		oSqlBuilder.AppendLine("	UMC.USER_SEQ					= :USER_SEQ																			AND	");
		oSqlBuilder.AppendLine("	UMC.USER_CHAR_NO				= :USER_CHAR_NO																		AND	");
		oSqlBuilder.AppendLine("	UMC.LAST_RICHINO_RANK_UPD_DATE	< TO_DATE(TO_CHAR(SYSDATE,'YYYY/MM')||'/01 00:00:00','YYYY/MM/DD HH24:MI:SS')		AND	");
		oSqlBuilder.AppendLine("	UM.MONTHLY_RECEIPT_AMT			< RR.RICHINO_RANK_KEEP_AMT																");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}

