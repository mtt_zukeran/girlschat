﻿/*************************************************************************
--	System			: ViCOMM
--	Sub System Name	: 
--	Title			: ピックアップオブジェ 投票
--	Progaram ID		: PickupObjsVote
--
--  Creation Date	: 2013.05.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class PickupObjsVote:DbSession {
	public PickupObjsVote() {
	}

	public string CheckExistVoteByDay(string pSiteCd,string pUserSeq,string pUserCharNo,string pPickupId,string pObjSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	1											");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_PICKUP_OBJS_VOTE							");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	PICKUP_ID			= :PICKUP_ID		AND	");
		oSqlBuilder.AppendLine("	OBJ_SEQ				= :OBJ_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ			AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO		AND	");
		oSqlBuilder.AppendLine("	REPORT_DAY			= TO_CHAR(SYSDATE,'YYYY/MM/DD')");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":PICKUP_ID",pPickupId));
		oParamList.Add(new OracleParameter(":OBJ_SEQ",pObjSeq));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			return ViCommConst.FLAG_ON_STR;
		} else {
			return ViCommConst.FLAG_OFF_STR;
		}
	}
	
	public void VotePickupObj(string pSiteCd,string pUserSeq,string pUserCharNo,string pPickupId,string pObjSeq,out string pResult) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("VOTE_PICKUP_OBJ");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pPICKUP_ID",DbSession.DbType.VARCHAR2,pPickupId);
			db.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,pObjSeq);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pResult = db.GetStringValue("pRESULT");
		}
	}
}
