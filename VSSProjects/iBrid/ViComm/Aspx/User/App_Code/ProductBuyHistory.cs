﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

[System.Serializable]
public class ProductBuyHistory :DbSession {
	public ProductBuyHistory() {}
	
	public void CreateHistory(string pSiteCd,string pProductAgentCd,string pProductSeq,string pUserSeq,string pUserCharNo,string pChargeType){
	
		using(DbSession oDbSession = new DbSession()){
			oDbSession.PrepareProcedure("CREATE_PRODUCT_BUY_HISTORY");
			oDbSession.ProcedureInParm("pSITE_CD", DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbType.VARCHAR2, pProductAgentCd);
			oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbType.VARCHAR2, pProductSeq);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbType.VARCHAR2, pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbType.VARCHAR2, pUserCharNo);
			oDbSession.ProcedureInParm("pCHARGE_TYPE", DbType.VARCHAR2, pChargeType);
			oDbSession.ProcedureInParm("pOBJ_TYPE", DbType.VARCHAR2, ViCommConst.ATTACHED_PRODUCT);
			oDbSession.ProcedureOutParm("pSTATUS", DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;			
			oDbSession.ExecuteProcedure();
		}
	}
}
