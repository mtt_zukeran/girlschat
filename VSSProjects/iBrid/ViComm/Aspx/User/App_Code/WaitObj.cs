﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 写真・動画待機

--	Progaram ID		: Access
--
--  Creation Date	: 2008.10.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

[System.Serializable]
public class WaitObj:DbSession {
	public WaitObj() {
	}

	public void WriteWaitObj(string pSiteCd,string pUserSeq,string pUserCharNo,string pAttachedObjType,string pObjTitle,string pObjDoc,string pObjType,string pObjAttrTypeSeq,string pObjAttrSeq,int pObjModifyRequestFlag,string pObjModifyRequestSpec,int pProfilePicFlag,out string pObjTempId){
		this.WriteWaitObj(pSiteCd,pUserSeq,pUserCharNo,pAttachedObjType,pObjTitle,pObjDoc,pObjType,pObjAttrTypeSeq,pObjAttrSeq,pObjModifyRequestFlag,pObjModifyRequestSpec,pProfilePicFlag,1,out pObjTempId);
	}

	public void WriteWaitObj(string pSiteCd,string pUserSeq,string pUserCharNo,string pAttachedObjType,string pObjTitle,string pObjDoc,string pObjType,string pObjAttrTypeSeq,string pObjAttrSeq,int pObjModifyRequestFlag,string pObjModifyRequestSpec,int pProfilePicFlag,int pObjRankingFlag,out string pObjTempId){
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_WAIT_OBJ");
			// [in]
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pATTACHED_OBJ_TYPE",DbSession.DbType.VARCHAR2,pAttachedObjType);
			db.ProcedureInParm("pOBJ_TITLE",DbSession.DbType.VARCHAR2,pObjTitle);
			db.ProcedureInParm("pOBJ_DOC",DbSession.DbType.VARCHAR2,pObjDoc);
			db.ProcedureInParm("pOBJ_TYPE",DbSession.DbType.VARCHAR2,pObjType);
			db.ProcedureInParm("pOBJ_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pObjAttrTypeSeq);
			db.ProcedureInParm("pOBJ_ATTR_SEQ",DbSession.DbType.VARCHAR2,pObjAttrSeq);
			db.ProcedureInParm("pOBJ_MODIFY_REQUEST_FLAG",DbSession.DbType.NUMBER,pObjModifyRequestFlag);
			db.ProcedureInParm("pOBJ_MODIFY_REQUEST_SPEC",DbSession.DbType.VARCHAR2,pObjModifyRequestSpec);
			db.ProcedureInParm("pPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			// [out]
			db.ProcedureOutParm("pOBJ_TEMP_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);

			db.ProcedureInParm("pOBJ_RANKING_FLAG",DbSession.DbType.NUMBER,pObjRankingFlag);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();

			pObjTempId = db.GetStringValue("pOBJ_TEMP_ID");

		}
	}
}
