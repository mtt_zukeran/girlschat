﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画属性
--	Progaram ID		: CastMoiveAttrType
--
--  Creation Date	: 2010.05.25
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System.Data;
using System.Text;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using ViComm;

/// <summary>
/// 出演者動画属性
/// </summary>
[System.Serializable]
public class CastMovieAttrType:DbSession {
	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect("CastMovieAttrType.GetList");

			string sSql = "SELECT " +
								"SITE_CD					, " +
								"CAST_MOVIE_ATTR_TYPE_SEQ	, " +
								"CAST_MOVIE_ATTR_SEQ		, " +
								"TYPE_NM					, " +
								"PRIORITY1					, " +
								"PRIORITY2					  " +
							"FROM " +
								"VW_CAST_MOVIE_ATTR_TYPE_VALUE2 " +
							"WHERE " +
								"SITE_CD				   = :SITE_CD					AND " +
								"CAST_MOVIE_ATTR_TYPE_SEQ != :CAST_MOVIE_ATTR_TYPE_SEQ		" +
							"ORDER BY " +
								"PRIORITY1, " +
								"PRIORITY2  ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_MOVIE_ATTR_TYPE_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE_ATTR_TYPE_VALUE2");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public string GetAttrTypeNm(string pSiteCd,string pAttrTypeSeq) {
		DataSet ds;
		DataRow dr;
		string sAttrTypeNm = string.Empty;

		try {
			conn = DbConnect("CastMovieAttrType.GetAttrTypeNm");

			string sSql = "SELECT " +
								"CAST_MOVIE_ATTR_TYPE_NM	" +
							"FROM " +
								"T_CAST_MOVIE_ATTR_TYPE " +
							"WHERE " +
								"SITE_CD					= :SITE_CD					AND " +
								"CAST_MOVIE_ATTR_TYPE_SEQ	= :CAST_MOVIE_ATTR_TYPE_SEQ		";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_MOVIE_ATTR_TYPE_SEQ",pAttrTypeSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE_ATTR_TYPE");
					if (ds.Tables["T_CAST_MOVIE_ATTR_TYPE"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_MOVIE_ATTR_TYPE"].Rows[0];
						sAttrTypeNm = dr["CAST_MOVIE_ATTR_TYPE_NM"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sAttrTypeNm;
	}

	public DataSet GetListForComboBox(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_SEQ,");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE_ATTR_TYPE");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_SEQ != :CAST_MOVIE_ATTR_TYPE_SEQ");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	PRIORITY ASC");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_MOVIE_ATTR_TYPE_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
