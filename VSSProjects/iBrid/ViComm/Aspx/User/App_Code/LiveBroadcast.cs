﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ライブ配信
--	Progaram ID		: LiveBroadcast
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;


[System.Serializable]
public class LiveBroadcast:DbSession {

	public LiveBroadcast() {
	}

	public DataSet GetLiveInfo(string pSiteCd,string pWebScreenId) {
		DataSet ds;
		try{
			conn = DbConnect("LiveBroadcast.GetLiveInfo");
			ds = new DataSet();

			string sSql = "SELECT " +
								"LIVE_SEQ," +
								"CAST_USER_SEQ," +
								"HTML_DOC1," +
								"HTML_DOC2," +
								"HTML_DOC3," +
								"HTML_DOC4," +
								"PUBLISH_FLAG," +
								"NA_HTML_DOC1," +
								"NA_HTML_DOC2," +
								"NA_HTML_DOC3," +
								"NA_HTML_DOC4 " +
							"FROM " +
							" T_LIVE_BROADCAST " +
							"WHERE " +
							" SITE_CD = :SITE_CD AND WEB_SCREEN_ID = :WEB_SCREEN_ID";

			sSql = sSql + " ORDER BY SITE_CD,LIVE_SEQ DESC";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("WEB_SCREEN_ID",pWebScreenId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetLiveCharge(string pSiteCd,string pLiveSeq) {
		DataSet ds;
		DataRow dr;
		int iChargePoint = 0;
		try{
			conn = DbConnect("LiveBroadcast.GetLiveCharge");
			ds = new DataSet();

			string sSql = "SELECT CHARGE_POINT FROM T_LIVE_BROADCAST WHERE SITE_CD = :SITE_CD AND LIVE_SEQ = :LIVE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("LIVE_SEQ",pLiveSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"LIVE_BROADCAST");
					if (ds.Tables["LIVE_BROADCAST"].Rows.Count != 0) {
						dr = ds.Tables["LIVE_BROADCAST"].Rows[0];
						iChargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return iChargePoint;
	}
}
