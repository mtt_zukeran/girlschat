﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品ｵｰｸｼｮﾝ入札履歴

--	Progaram ID		: ProductAuctionBidLog
--
--  Creation Date	: 2011.06.21
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using System.Collections.Generic;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using ViComm;
using iBridCommLib;

[System.Serializable]
public class ProductAuctionBidLog : ProductBase {
	public ProductAuctionBidLog() : base() { }
	public ProductAuctionBidLog(SessionObjs pSessionObj) : base(pSessionObj) { }

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(ProductSeekCondition pCondtion, int pRecPerPage, out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine(" SELECT	");
		oSqlBuilder.AppendLine(base.CreateCountFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = CreateSelectCommand(oSqlBuilder.ToString(), this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(oWhereParams);

				pRecCount = decimal.Parse(this.cmd.ExecuteScalar().ToString());
				iPageCount = (int)Math.Ceiling(pRecCount / pRecPerPage);
			}
		} finally {
			this.conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionBySeq(string pProductSeq) {
		ProductSeekCondition oCondtion = new ProductSeekCondition();
		oCondtion.ProductSeq = pProductSeq;
		return this.GetPageCollectionBase(oCondtion, 0, 0);
	}
	public DataSet GetPageCollection(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {
		return this.GetPageCollectionBase(pCondtion, pPageNo, pRecPerPage);
	}
	private DataSet GetPageCollectionBase(ProductSeekCondition pCondtion, int pPageNo, int pRecPerPage) {

		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = string.Empty;
		string sWhereClause = string.Empty;

		StringBuilder oSqlBuilder = new StringBuilder();
		


		// select
		oSqlBuilder.AppendLine(" SELECT																	");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_BID_LOG01.BID_AMT							,	");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_BID_LOG01.REGIST_DATE	AS BID_DATE			,	");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_BID_LOG01.HANDLE_NM		AS BIDDER_HANDLE_NM	,	");
		oSqlBuilder.Append(base.CreateFieldBase(pCondtion));
		// from
		oSqlBuilder.AppendLine(this.CreateFrom(pCondtion));
		// where		
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion, ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		// order by
		sSortExpression = this.CreateOrderExpresionBase(pCondtion);

		return ExecutePageCollection(oSqlBuilder.ToString(), pCondtion, oWhereParams, sSortExpression, iStartIndex, pRecPerPage);
	}

	private OracleParameter[] CreateWhere(ProductSeekCondition pCondition, ref string pWhereClause) {
		if (pCondition == null) throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;

		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 商品共通のWHERE条件を作成
		oParamList.AddRange(base.CreateWhereBase(pCondition, ref pWhereClause));

		SysPrograms.SqlAppendWhere(" VW_PRODUCT_AUCTION_BID_LOG01.SITE_CD			= VW_PRODUCT00.SITE_CD				", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" VW_PRODUCT_AUCTION_BID_LOG01.PRODUCT_AGENT_CD	= VW_PRODUCT00.PRODUCT_AGENT_CD		", ref pWhereClause);
		SysPrograms.SqlAppendWhere(" VW_PRODUCT_AUCTION_BID_LOG01.PRODUCT_SEQ		= VW_PRODUCT00.PRODUCT_SEQ			", ref pWhereClause);
		return oParamList.ToArray();
	}

	private string CreateFrom(ProductSeekCondition pCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" FROM								");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_AUCTION_BID_LOG01,	");
		oSqlBuilder.AppendLine(base.CreateFromBase(pCondition));
		return oSqlBuilder.ToString();
	}

	protected override string CreateOrderExpresionBase(ProductSeekCondition pCondition) {
		if(string.IsNullOrEmpty(pCondition.Sort)){
			return " ORDER BY BID_AMT DESC,BID_DATE ASC ";
		}else{
			return base.CreateOrderExpresionBase(pCondition);
		}
	}

	#endregion ========================================================================================================

	protected override string GetProductType(ProductSeekCondition pCondition) {
		return ProductHelper.GetAuctionProductType(pCondition.AdultFlag);
	}	
}
