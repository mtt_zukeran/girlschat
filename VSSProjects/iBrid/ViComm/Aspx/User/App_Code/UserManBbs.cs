﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性掲示板
--	Progaram ID		: UserManBbs
--  Creation Date	: 2010.04.20
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Collections;
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

/// <summary>
/// 男性掲示板クラス
/// </summary>
[System.Serializable]
public class UserManBbs:DbSession {

	public UserManBbs() {
	}

	public void GetPrevNextBbsSeq(
		string pBbsSeq,
		string pSiteCd,
		string pLoginId,
		string pUserCharNo,
		out string pPrevPicSeq,
		out string pNextPicSeq) {

		pPrevPicSeq = "";
		pNextPicSeq = "";

		try {
			conn = DbConnect("UserManBbs.GetPrevNextBbsSeq");
			DataSet ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM(");
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		P.BBS_SEQ,").AppendLine();
			sSql.Append("		LAG(P.BBS_SEQ)	OVER (ORDER BY P.BBS_SEQ DESC) AS PREV_BBS_SEQ,").AppendLine();
			sSql.Append("		LEAD(P.BBS_SEQ) OVER (ORDER BY P.BBS_SEQ DESC) AS NEXT_BBS_SEQ");
			sSql.Append("	FROM VW_USER_MAN_BBS01 P ").AppendLine();
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere("",pSiteCd,pLoginId,pUserCharNo,new SeekCondition(),ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(") WHERE BBS_SEQ =:BBS_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("BBS_SEQ",pBbsSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_BBS01");
				}
			}
			if (ds.Tables[0].Rows.Count != 0) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (dr["BBS_SEQ"].ToString().Equals(pBbsSeq)) {
						pPrevPicSeq = dr["PREV_BBS_SEQ"].ToString();
						pNextPicSeq = dr["NEXT_BBS_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
	}

	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,int pRecPerPage,out decimal pRecCount) {
		return GetPageCount(pSiteCd,pLoginId,pUserCharNo,pRecPerPage,new SeekCondition(),out pRecCount);
	}
	public int GetPageCount(string pSiteCd,string pLoginId,string pUserCharNo,int pRecPerPage,SeekCondition pCondition,out decimal pRecCount) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		pRecCount = 0;
		try {
			conn = DbConnect("UserManBbs.GetPageCount");

			string sSql = "SELECT COUNT(P.BBS_SEQ) AS ROW_COUNT FROM VW_USER_MAN_BBS01 P ";
			for (int i = 0;i < pCondition.attrValue.Count;i++) {
				if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
					sSql += string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1);
				}
			}
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere("",pSiteCd,pLoginId,pUserCharNo,pCondition,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pRecCount = decimal.Parse(dr["ROW_COUNT"].ToString());
					iPages = (int)Math.Ceiling(decimal.Parse(dr["ROW_COUNT"].ToString()) / pRecPerPage);
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}


	private void GetOrder(out string pOrder) {
		pOrder = "ORDER BY P.BBS_SEQ DESC ";
	}

	public DataSet GetPageCollectionBySeq(string pBbsSeq) {
		return GetPageCollectionBase(pBbsSeq,"","","",0,0,new SeekCondition(),string.Empty,string.Empty);
	}

	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,int pPageNo,int pRecPerPage) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pPageNo,pRecPerPage,new SeekCondition(),string.Empty,string.Empty);
	}
	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pUserCharNo,int pPageNo,int pRecPerPage,SeekCondition pCondition,string pSelfUserSeq,string pSelfUserCharNo) {
		return GetPageCollectionBase("",pSiteCd,pLoginId,pUserCharNo,pPageNo,pRecPerPage,pCondition,pSelfUserSeq,pSelfUserCharNo);
	}

	public DataSet GetPageCollectionBase(
				string pBbsSeq,string pSiteCd,string pLoginId,string pUserCharNo,int pPageNo,int pRecPerPage,SeekCondition pCondition,string pSelfUserSeq,string pSelfUserCharNo) {
		DataSet ds;
		try {
			conn = DbConnect("UserManBbs.GetPageCollectionBase");
			ds = new DataSet();

			string sOrder;
			GetOrder(out sOrder);

			//時間がある時にTEMPLATEを利用するやり方に変更する必要有
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM( ").AppendLine();
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		ROWNUM AS RNUM	,").AppendLine();
			sSql.Append("		INNER.*			,").AppendLine();
			sSql.Append("		GET_MAN_PHOTO_IMG_PATH		(INNER.SITE_CD,INNER.LOGIN_ID,INNER.PIC_SEQ,0)	AS PHOTO_IMG_PATH		,").AppendLine();
			sSql.Append("		GET_MAN_SMALL_PHOTO_IMG_PATH(INNER.SITE_CD,INNER.LOGIN_ID,INNER.PIC_SEQ,0)	AS SMALL_PHOTO_IMG_PATH	,").AppendLine();
			sSql.Append("		(SELECT NVL(COUNT(USER_SEQ),0) AS CNT FROM T_FAVORIT											").AppendLine();
			sSql.Append("			WHERE																						").AppendLine();
			sSql.Append("				SITE_CD					= INNER.SITE_CD			AND										").AppendLine();
			sSql.Append("				USER_SEQ				= INNER.USER_SEQ		AND										").AppendLine();
			sSql.Append("				USER_CHAR_NO			= INNER.USER_CHAR_NO	AND										").AppendLine();
			sSql.Append("				PARTNER_USER_SEQ		= :PARTNER_USER_SEQ1	AND										").AppendLine();
			sSql.Append("				PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO1 ) 						AS LIKE_ME_FLAG,").AppendLine();
			sSql.Append("		(SELECT NVL(COUNT(*),0) AS CNT FROM T_FAVORIT													").AppendLine();
			sSql.Append("			WHERE																						").AppendLine();
			sSql.Append("				SITE_CD					= INNER.SITE_CD				AND									").AppendLine();
			sSql.Append("				USER_SEQ				= :PARTNER_USER_SEQ2		AND									").AppendLine();
			sSql.Append("				USER_CHAR_NO			= :PARTNER_USER_CHAR_NO2	AND									").AppendLine();
			sSql.Append("				PARTNER_USER_SEQ		= INNER.USER_SEQ			AND									").AppendLine();
			sSql.Append("				PARTNER_USER_CHAR_NO	= INNER.USER_CHAR_NO ) 							AS FAVORIT_FLAG,").AppendLine();
			sSql.Append("		(SELECT NVL(COUNT(*),0) AS CNT FROM T_REFUSE													").AppendLine();
			sSql.Append("			WHERE																						").AppendLine();
			sSql.Append("				SITE_CD					= INNER.SITE_CD				AND									").AppendLine();
			sSql.Append("				USER_SEQ				= :PARTNER_USER_SEQ3		AND									").AppendLine();
			sSql.Append("				USER_CHAR_NO			= :PARTNER_USER_CHAR_NO3	AND									").AppendLine();
			sSql.Append("				PARTNER_USER_SEQ		= INNER.USER_SEQ			AND									").AppendLine();
			sSql.Append("				PARTNER_USER_CHAR_NO	= INNER.USER_CHAR_NO ) 							AS REFUSE_FLAG,").AppendLine();
			sSql.Append("		CASE																							").AppendLine();
			sSql.Append("			WHEN INNER.CHARACTER_ONLINE_STATUS = 0 THEN													").AppendLine();
			sSql.Append("				INNER.OFFLINE_GUIDANCE																	").AppendLine();
			sSql.Append("			WHEN INNER.CHARACTER_ONLINE_STATUS = 1 THEN													").AppendLine();
			sSql.Append("				INNER.LOGINED_GUIDANCE																	").AppendLine();
			sSql.Append("			WHEN INNER.CHARACTER_ONLINE_STATUS = 2 THEN													").AppendLine();
			sSql.Append("				INNER.WAITING_GUIDANCE																	").AppendLine();
			sSql.Append("			WHEN INNER.CHARACTER_ONLINE_STATUS = 3 THEN													").AppendLine();
			sSql.Append("				INNER.TALKING_WSHOT_GUIDANCE															").AppendLine();
			sSql.Append("		END																	AS MOBILE_ONLINE_STATUS_NM	").AppendLine();
			sSql.Append("	FROM( ").AppendLine();
			sSql.Append("		SELECT ").AppendLine();
			sSql.Append("			P.SITE_CD					,").AppendLine();
			sSql.Append("			P.USER_SEQ					,").AppendLine();
			sSql.Append("			P.USER_CHAR_NO				,").AppendLine();
			sSql.Append("			P.BBS_SEQ					,").AppendLine();
			sSql.Append("			P.UNIQUE_VALUE				,").AppendLine();
			sSql.Append("			P.BBS_TITLE					,").AppendLine();
			sSql.Append("			P.BBS_DOC					,").AppendLine();
			sSql.Append("			P.READING_COUNT				,").AppendLine();
			sSql.Append("			P.CREATE_DATE				,").AppendLine();
			sSql.Append("			P.LOGIN_ID					,").AppendLine();
			sSql.Append("			P.HANDLE_NM					,").AppendLine();
			sSql.Append("			P.BIRTHDAY					,").AppendLine();
			sSql.Append("			P.AGE						,").AppendLine();
			sSql.Append("			P.PIC_SEQ					,").AppendLine();
			sSql.Append("			P.CHARACTER_ONLINE_STATUS	,").AppendLine();
			sSql.Append("			P.ONLINE_STATUS_NM 			,").AppendLine();
			sSql.Append("			P.OFFLINE_GUIDANCE			,").AppendLine();
			sSql.Append("			P.LOGINED_GUIDANCE			,").AppendLine();
			sSql.Append("			P.WAITING_GUIDANCE			,").AppendLine();
			sSql.Append("			P.TALKING_WSHOT_GUIDANCE	,").AppendLine();
			sSql.Append("			P.NG_SKULL_COUNT			").AppendLine();
			sSql.Append("		FROM VW_USER_MAN_BBS01 P		").AppendLine();
			for (int i = 0;i < pCondition.attrValue.Count;i++) {
				if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
					sSql.Append(string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1));
				}
			}
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pBbsSeq,pSiteCd,pLoginId,pUserCharNo,pCondition,ref sWhere);

			sSql.Append(sWhere).AppendLine();
			sSql.Append(sOrder).AppendLine();

			if (pBbsSeq.Equals(string.Empty)) {
				sSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
				sSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");
			} else {
				sSql.Append(")INNER ) ");
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("PARTNER_USER_SEQ1",pSelfUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO1",pSelfUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ2",pSelfUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO2",pSelfUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ3",pSelfUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO3",pSelfUserCharNo);

				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				if (pBbsSeq.Equals(string.Empty)) {
					cmd.Parameters.Add("MAX_ROW",pPageNo * pRecPerPage);
					cmd.Parameters.Add("FIRST_ROW",(pPageNo - 1) * pRecPerPage + 1);
					cmd.Parameters.Add("LAST_ROW",pPageNo * pRecPerPage);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_BBS01");

				}
			}
		} finally {
			conn.Close();
		}
		// 男性の属性を付与する

		using (UserManAttrTypeValue oUserManAttrTypeValue = new UserManAttrTypeValue()) {
			oUserManAttrTypeValue.AppendAttr2DataSet(ds,"SITE_CD","USER_SEQ","USER_CHAR_NO");
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pBbsSeq,string pSiteCd,string pLoginId,string pUserCharNo,SeekCondition pCondition, ref string pWhere) {
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("P.DEL_FLAG = :DEL_FLAG",ref pWhere);
		list.Add(new OracleParameter("P.DEL_FLAG",ViCommConst.FLAG_OFF));
		if (!pBbsSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("P.BBS_SEQ = :BBS_SEQ",ref pWhere);
			list.Add(new OracleParameter("P.BBS_SEQ",pBbsSeq));
		} else {
			if (!pSiteCd.Equals(string.Empty)) {
				SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
				list.Add(new OracleParameter("SITE_CD",pSiteCd));
			}

			if (!pLoginId.Equals(string.Empty)) {
				SysPrograms.SqlAppendWhere("P.LOGIN_ID = :LOGIN_ID",ref pWhere);
				SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginId));
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_NO_DISP_REFUSE_OBJS, 2)) {
				string sUserSeq = string.Empty;
				string sUserCharNo = string.Empty;
				if (SessionObjs.Current != null) {
					if (!SessionObjs.Current.sexCd.Equals(ViCommConst.MAN)) {
						SessionWoman oSessionWoman = (SessionWoman)SessionObjs.Current;
						sUserSeq = oSessionWoman.userWoman.userSeq;
						sUserCharNo = oSessionWoman.userWoman.CurCharacter.userCharNo;
					}
				}
				if (!sUserSeq.Equals(string.Empty)) {
					SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND PARTNER_USER_SEQ = :PARTNER_USER_SEQ3 AND PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO3)", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_SEQ3", sUserSeq));
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO3", sUserCharNo));
				}
			}
		}


		CreateAttrQuery(pCondition,ref pWhere,ref list);
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public void WriteBbs(string pSiteCd,string pUserSeq,string pUserCharNo,string pBbsTitle,string pBbsDoc) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WRITE_USER_BBS");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PBBS_TITLE",DbSession.DbType.VARCHAR2,pBbsTitle);
			db.ProcedureInParm("PBBS_DOC",DbSession.DbType.VARCHAR2,pBbsDoc);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void DeleteBbs(string pSiteCd,string pUserSeq,string pUserCharno,string pBbsSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_USER_MAN_BBS");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharno);
			db.ProcedureInParm("PBBS_SEQ",DbSession.DbType.VARCHAR2,pBbsSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void UpdateReadingCount(string pSiteCd,string pUserSeq,string pUserCharno,string pBbsSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_USER_MAN_BBS_COUNT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharno);
			db.ProcedureInParm("PBBS_SEQ",DbSession.DbType.VARCHAR2,pBbsSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public DateTime? GetLastWriteDate(string pSiteCd,string pUserSeq,string pUserCharNo) {
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT                                            ").AppendLine();
		oSqlBuilder.Append(" 	MAX(T_USER_MAN_BBS.CREATE_DATE) AS LAST_WRITE_DATE").AppendLine();
		oSqlBuilder.Append(" FROM                                              ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS                                 ").AppendLine();
		oSqlBuilder.Append(" WHERE                                             ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.SITE_CD		=	:SITE_CD		AND         ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.USER_SEQ		=	:USER_SEQ		AND			").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.USER_CHAR_NO	=	:USER_CHAR_NO				").AppendLine();
		oSqlBuilder.Append(" GROUP BY                                          ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.SITE_CD			,              ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.USER_SEQ			,              ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.USER_CHAR_NO                    ").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);

				return (DateTime?)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}
	}

	public int GetWriteCntEachDay(string pSiteCd,string pUserSeq, string pUserCharNo, DateTime pTargetDate) {
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT                                            ").AppendLine();
		oSqlBuilder.Append(" 	COUNT(*) AS TODAY_WRITE_CNT                    ").AppendLine();
		oSqlBuilder.Append(" FROM                                              ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS                                 ").AppendLine();
		oSqlBuilder.Append(" WHERE                                             ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.SITE_CD		=	:SITE_CD					AND ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.USER_SEQ		=	:USER_SEQ					AND ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.USER_CHAR_NO	=	:USER_CHAR_NO				AND ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.CREATE_DATE 	>= 	TRUNC(:TARGET_DATE)			AND ").AppendLine();
		oSqlBuilder.Append(" 	T_USER_MAN_BBS.CREATE_DATE 	< 	TRUNC(:TARGET_DATE) + 1").AppendLine();
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add(":TARGET_DATE",OracleDbType.Date,pTargetDate,ParameterDirection.Input);

				return int.Parse(cmd.ExecuteScalar().ToString());
			}
		} finally {
			conn.Close();
		}
	}

	private void CreateAttrQuery(SeekCondition pCondition,ref string pWhere, ref ArrayList pList) {
		string sQuery = string.Empty;
		for (int i = 0;i < pCondition.attrValue.Count;i++) {
			if ((!pCondition.attrValue[i].Equals("")) && (!pCondition.attrValue[i].Equals("*"))) {
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.SITE_CD			= P.SITE_CD					",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_SEQ			= P.USER_SEQ				",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO			",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ{0}	",i + 1),ref pWhere);
				pList.Add(new OracleParameter(string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1),pCondition.attrTypeSeq[i]));
				if (pCondition.likeSearchFlag[i].Equals("1")) {
					if (pCondition.attrValue[i].Trim() != "") {
						string[] sValue = pCondition.attrValue[i].Trim().Split(' ');
						sQuery = " ( ";
						for (int k = 0;k < sValue.Length;k++) {
							if (sValue[k] != "") {
								if (k != 0) {
									sQuery += " OR ";
								}
								sQuery += string.Format(" (AT{0}.MAN_ATTR_INPUT_VALUE	LIKE '%' || :MAN_ATTR_INPUT_VALUE{0}_{1} || '%')	",i + 1,k);
								pList.Add(new OracleParameter(string.Format("MAN_ATTR_INPUT_VALUE{0}_{1}",i + 1,k),sValue[k]));
							}
						}
						sQuery += ")";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					}
				} else if (pCondition.groupingFlag[i].Equals("1") == false) {
					string[] sValues = pCondition.attrValue[i].Trim().Split(',');

					if (sValues.Length > 1) {
						sQuery = string.Format(" AT{0}.MAN_ATTR_SEQ IN ( ",i + 1);
						for (int j = 0;j < sValues.Length;j++) {
							if (j != 0) {
								sQuery += " , ";
							}
							sQuery += string.Format(" :MAN_ATTR_SEQ{0}_{1}	",i + 1,j);
							pList.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}_{1}",i + 1,j),sValues[j]));
						}
						sQuery += " ) ";
						SysPrograms.SqlAppendWhere(sQuery,ref pWhere);
					} else {
						if (pCondition.notEqualFlag.Count > i && pCondition.notEqualFlag[i].Equals("1")) {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_SEQ <> :MAN_ATTR_SEQ{0} ",i + 1),ref pWhere);
						} else {
							SysPrograms.SqlAppendWhere(string.Format(" AT{0}.MAN_ATTR_SEQ = :MAN_ATTR_SEQ{0} ",i + 1),ref pWhere);
						}
						pList.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}",i + 1),pCondition.attrValue[i]));
					}
				} else {
					SysPrograms.SqlAppendWhere(string.Format(" AT{0}.GROUPING_CD	= :GROUPING_CD{0}	",i + 1),ref pWhere);
					pList.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),pCondition.attrValue[i]));
				}
			}
		}
	}

	public int GetNewlyArrived(string pSiteCd,string pUserSeq,string pUserCharNo,int pHour) {
		DataSet ds;
		int iFlag = ViCommConst.FLAG_OFF;
		try {
			conn = DbConnect("UserManBbs.GetNewlyArrived");
			ds = new DataSet();

			string sSql = "SELECT " +
							" COUNT(*) CNT " +
							"FROM " +
							" T_USER_MAN_BBS " +
							"WHERE " +
							" SITE_CD		=  :SITE_CD			AND " +
							" USER_SEQ		=  :USER_SEQ		AND " +
							" USER_CHAR_NO	=  :USER_CHAR_NO	AND " +
							" CREATE_DATE	>= :CREATE_DATE		AND" +
							" DEL_FLAG		=  :DEL_FLAG		";


			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("CREATE_DATE",OracleDbType.Date,DateTime.Now.AddHours(pHour * -1),ParameterDirection.Input);
				cmd.Parameters.Add("DEL_FLAG",ViCommConst.FLAG_OFF);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_MAN_BBS");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow drSub = ds.Tables[0].Rows[0];
						iFlag = int.Parse(drSub["CNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}

		return iFlag;
	}

}
