﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者属性値
--	Progaram ID		: CastAttrValue
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

[System.Serializable]
public class CastAttrValue:DbSession {

	public CastAttrValue() {
	}

	public DataSet GetList(string pSiteCd,string pUserSeq,string pUserCharNo) {

		DataSet ds = new DataSet();
		string sSql;
		try{
			conn = DbConnect("CasrAttrTypeValue.GetList");

			sSql = "SELECT " +
						"CAST_ATTR_TYPE_SEQ		," +
						"CAST_ATTR_TYPE_NM		," +
						"CAST_ATTR_SEQ			," +
						"CAST_ATTR_NM			," +
						"CAST_ATTR_INPUT_VALUE	," +
						"INPUT_TYPE				," +
						"PRIORITY				," +
						"ROW_COUNT				," +
						"ITEM_CD				," +
						"ITEM_NO				" +
					"FROM " +
						"VW_CAST_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD		AND	" +
						"USER_SEQ		= :USER_SEQ		AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO	AND	" +
						"NA_FLAG		= 0	" +
					"ORDER BY " +
						"SITE_CD	," +
						"USER_SEQ   ," +
						"CAST_ATTR_TYPE_PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}