﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾞﾛｸﾞｺﾒﾝﾄ
--	Progaram ID		: BlogComment
--
--  Creation Date	: 2012.12.26
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

#region □■□ Condition □■□ =======================================================================================
[Serializable]
public class BlogCommentSeekCondition:SeekConditionBase {

	public string SiteCd {
		get {
			return this.Query["site"];
		}
		set {
			this.Query["site"] = value;
		}
	}

	public string BlogArticleSeq {
		get {
			return this.Query["blog_article_seq"];
		}
		set {
			this.Query["blog_article_seq"] = value;
		}
	}
	
	public string DelFlag {
		get {
			return this.Query["del_flag"];
		}
		set {
			this.Query["del_flag"] = value;
		}
	}
	
	public string BlogCommentSeq {
		get {
			return this.Query["blog_comment_seq"];
		}
		set {
			this.Query["blog_comment_seq"] = value;
		}
	}

	public BlogCommentSeekCondition() : this(new NameValueCollection()) {
	}
	
	public BlogCommentSeekCondition(NameValueCollection pQuery)
		: base(pQuery) {
		this.Query["site"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["site"]));
		this.Query["blog_article_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_article_seq"]));
		this.Query["del_flag"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["del_flag"]));
		this.Query["blog_comment_seq"] = HttpUtility.UrlDecode(iBridUtil.GetStringValue(pQuery["blog_comment_seq"]));
	}
}
#endregion ============================================================================================================

public class BlogComment:ManBase {

	public BlogComment() : base() {
	}

	public BlogComment(SessionObjs pSessionObj)
		: base(pSessionObj) {
	}

	#region □■□ PagingQuery □■□ =================================================================================

	public int GetPageCount(BlogCommentSeekCondition pCondtion,int pRecPerPage,out decimal pRecCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine(" FROM					");
		oSqlBuilder.AppendLine("	VW_PW_BLOG_COMMENT00 P	");

		// where
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pCondtion,ref sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		pRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oWhereParams);
		return (int)Math.Ceiling(pRecCount / pRecPerPage);
	}

	public DataSet GetPageCollection(BlogCommentSeekCondition pCondtion,int pPageNo,int pRecPerPage) {
		
		int iStartIndex = (pPageNo - 1) * pRecPerPage;

		string sSortExpression = this.CreateOrderExpresion(pCondtion);
		string sWhereClause = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		StringBuilder oSqlBuilder = new StringBuilder();
		// select
		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	" + ManBasicField() + "	,	");
		oSqlBuilder.AppendLine("	P.BLOG_COMMENT_SEQ		,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_SEQ		,	");
		oSqlBuilder.AppendLine("	P.COMMENT_DOC			,	");
		oSqlBuilder.AppendLine("	P.CREATE_DATE			,	");
		oSqlBuilder.AppendLine("	P.NG_SKULL_COUNT		,	");
		oSqlBuilder.AppendLine("	P.BLOG_ARTICLE_REGIST_DATE	");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_PW_BLOG_COMMENT00 P		");
		
		// where		
		oParamList.AddRange(this.CreateWhere(pCondtion,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);

		string sExecSql = oSqlBuilder.ToString();

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(sExecSql,iStartIndex,pRecPerPage,out sExecSql));

		DataSet oDataSet = ExecuteTemplateQuery(sExecSql,oParamList,string.Empty,string.Empty,string.Empty);

		return oDataSet;

	}

	private OracleParameter[] CreateWhere(BlogCommentSeekCondition pCondition,ref string pWhereClause) {
		if (pCondition == null)
			throw new ArgumentNullException();
		pWhereClause = pWhereClause ?? string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		SysPrograms.SqlAppendWhere(" P.SITE_CD	= :SITE_CD",ref pWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pCondition.SiteCd));
		
		if (!string.IsNullOrEmpty(pCondition.BlogArticleSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_ARTICLE_SEQ	= :BLOG_ARTICLE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_ARTICLE_SEQ",pCondition.BlogArticleSeq));
		}

		if (!string.IsNullOrEmpty(pCondition.BlogCommentSeq)) {
			SysPrograms.SqlAppendWhere(" P.BLOG_COMMENT_SEQ	= :BLOG_COMMENT_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":BLOG_COMMENT_SEQ",pCondition.BlogCommentSeq));
		}

		SysPrograms.SqlAppendWhere(" P.DEL_FLAG	= :DEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":DEL_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere(" P.NA_FLAG		= :NA_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter(":NA_FLAG",ViCommConst.NaFlag.OK));

		return oParamList.ToArray();
	}

	protected string CreateOrderExpresion(BlogCommentSeekCondition pCondition) {
		string sSortExpression = string.Empty;

		switch (pCondition.Sort) {
			case(PwViCommConst.BlogCommentSort.CREATE_DATE_DESC):
				sSortExpression = " ORDER BY P.CREATE_DATE DESC,P.BLOG_COMMENT_SEQ DESC";
				break;
			case(PwViCommConst.BlogCommentSort.CREATE_DATE_ASC):
				sSortExpression = " ORDER BY P.CREATE_DATE ASC,P.BLOG_COMMENT_SEQ ASC";
				break;
			default:
				sSortExpression = " ORDER BY P.CREATE_DATE DESC,P.BLOG_COMMENT_SEQ DESC";
				break;
		}
		
		return sSortExpression;
	}

	#endregion ========================================================================================================

	public void WriteBlogComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pBlogArticleSeq,string pCommentDoc) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("WRITE_BLOG_COMMENT");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ",DbSession.DbType.NUMBER,pBlogArticleSeq);
			oDbSession.ProcedureInParm("pCOMMENT_DOC",DbSession.DbType.VARCHAR2,pCommentDoc);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	public void DeleteBlogComment(string pSiteCd,string pUserSeq,string pUserCharNo,string pBlogCommentSeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_BLOG_COMMENT");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pBLOG_COMMENT_SEQ",DbSession.DbType.NUMBER,pBlogCommentSeq);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}
	
	public bool CheckBlogCommentLimitTime(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT															");
		oSqlBuilder.AppendLine("	1															");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_BLOG_COMMENT COM											");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD								AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO							AND	");
		oSqlBuilder.AppendLine("	CREATE_DATE		> SYSDATE -1							AND	");
		oSqlBuilder.AppendLine("	EXISTS														");
		oSqlBuilder.AppendLine("	(															");
		oSqlBuilder.AppendLine("		SELECT													");
		oSqlBuilder.AppendLine("			*													");
		oSqlBuilder.AppendLine("		FROM													");
		oSqlBuilder.AppendLine("			T_BLOG_ARTICLE										");
		oSqlBuilder.AppendLine("		WHERE													");
		oSqlBuilder.AppendLine("			SITE_CD				= COM.SITE_CD				AND	");
		oSqlBuilder.AppendLine("			BLOG_ARTICLE_SEQ	= COM.BLOG_ARTICLE_SEQ		AND	");
		oSqlBuilder.AppendLine("			USER_SEQ			= :PARTNER_USER_SEQ			AND	");
		oSqlBuilder.AppendLine("			USER_CHAR_NO		= :PARTNER_USER_CHAR_NO			");
		oSqlBuilder.AppendLine("	)															");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PARTNER_USER_SEQ",pPartnerUserSeq));
		oParamList.Add(new OracleParameter(":PARTNER_USER_CHAR_NO",pPartnerUserCharNo));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
