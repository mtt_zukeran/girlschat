<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendProfileMovie.aspx.cs" Inherits="ViComm_woman_SendProfileMovie" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Assembly="MobileLib" Namespace="MobileLib"  %>
<%@ Register TagPrefix="ibrid" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01; 
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		$PGM_HTML03;
		<ibrid:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24" EmojiPaletteEnabled="true"></ibrid:iBMobileTextBox>
		$PGM_HTML04;
		<ibrid:TextArea ID="txtDoc" runat="server" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</ibrid:TextArea>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
