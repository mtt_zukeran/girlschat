/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�޲����݈ꗗ
--	Progaram ID		: ListGamePartTimeJobPlan
--
--  Creation Date	: 2011.07.27
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGamePartTimeJobPlan:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_GAME_PART_TIME_JOB_PLAN,this.ActiveForm);
		} else {
			string sPartTimeJobSeq = string.Empty;
			
			for (int i = 1;i <= 10;i++) {
				if (Request.Form[ViCommConst.BUTTON_GOTO_LINK + "_" + i] != null) {
					sPartTimeJobSeq = iBridUtil.GetStringValue(Request.Params["part_time_job_seq" + i.ToString()]);
				}
			}
			
			if (!string.IsNullOrEmpty(sPartTimeJobSeq)) {
				string sResult = GetPartTimeJob(sPartTimeJobSeq);
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				oParameters.Add("result",sResult);
				oParameters.Add("part_time_job_seq",sPartTimeJobSeq);
				RedirectToGameDisplayDoc(PwViCommConst.SCR_CAST_GAME_PART_TIME_JOB_RESULT,oParameters);
			}
		}
	}

	private string GetPartTimeJob(string sPartTimeJobSeq) {
		string sResult = string.Empty;
		
		using (PartTimeJobPlan oPartTimeJobPlan = new PartTimeJobPlan()) {
			sResult = oPartTimeJobPlan.GetPartTimeJob(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.CurCharacter.userCharNo,
				sPartTimeJobSeq
			);
		}
		
		return sResult;
	}
}
