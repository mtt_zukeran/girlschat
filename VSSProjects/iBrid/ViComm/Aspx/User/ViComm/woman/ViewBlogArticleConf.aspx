<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewBlogArticleConf.aspx.cs" Inherits="ViComm_woman_ViewBlogArticleConf" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile"	%>
<%@ Register TagPrefix="ibrid"	Namespace="MobileLib"						Assembly="MobileLib"			%>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>    			
		<ibrid:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
