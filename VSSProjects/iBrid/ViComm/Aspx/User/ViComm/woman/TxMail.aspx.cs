/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信
--	Progaram ID		: woman_TxMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/08/13  i-Brid(Y.Inoue)

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_TxMail:MobileMailWomanPage {

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
		MailTemplate = lstTemplate;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {
		string sBatch = iBridUtil.GetStringValue(Request.QueryString["batch"]);
		string sLayOutType = iBridUtil.GetStringValue(Request.QueryString["layouttype"]);

		if (!sLayOutType.Equals(ViCommConst.TxMailLayOutType.MEETING_MAIL)) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				bool bIsAvailable = oManageCompany.IsAvailableService(ViCommConst.RELEASE_TX_MAIL_DISABLE_TEMPLATE);
				if (bIsAvailable) {
					this.SelectNomalMail();
				}
				this.pnlTemplate.Visible = !bIsAvailable;
			}

			if (sBatch.Equals("1")) {
				using (ManageCompany oManageCompany = new ManageCompany()) {
					bool bIsAvailable = oManageCompany.IsAvailableService(ViCommConst.RELEASE_TX_MAIL_BATCH_DISABLE_TEMPLATE);
					if (bIsAvailable) {
						this.SelectNomalMail();
					}
					this.pnlTemplate.Visible = !bIsAvailable;
				}
			}
		}

		if (sBatch.Equals("1") || sLayOutType.Equals(ViCommConst.TxMailLayOutType.MEETING_MAIL)) {
			this.tagAttachedFileOption.Visible = false;
		} else {
			this.tagAttachedFileOption.Visible = true;
		}

		if (!IsPostBack) {
			if (iBridUtil.GetStringValue(this.Request.QueryString["protect"]).Equals(ViCommConst.FLAG_ON_STR)) {
				this.ProtectRxMail();
			}
			
			if (IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL, 2)) {
				lblTitle.Visible = false;
				txtTitle.Visible = false;
			}
			sessionWoman.batchMailFlag = sBatch.Equals(ViCommConst.FLAG_ON_STR);

			string sMailDataSeq = iBridUtil.GetStringValue(Request.QueryString["data"]);
			if (!sMailDataSeq.Equals(string.Empty)) {
				sLayOutType = sessionWoman.userWoman.mailData[sMailDataSeq].layOutType;
			}
			
			string sPageKey = "TxMail.aspx";
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["scrid"]))) {
				sPageKey = string.Format("TxMail{0}.aspx",iBridUtil.GetStringValue(this.Request.QueryString["scrid"]));
			}
			
			if (sLayOutType.Equals(ViCommConst.TxMailLayOutType.MEETING_MAIL)) {
				sPageKey = "TxMeetingMail.aspx";
				lblDoc.Text = "一言ﾒｯｾｰｼﾞ";
			}
			ViewState["PAGE_KEY"] = sPageKey;
			ViewState["LAY_OUT_TYPE"] = sLayOutType;

			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,sPageKey,ViewState,IsPostBack);
			SetMailTemplate(false,sBatch.Equals("1") ? ViCommConst.TxMailLayOutType.NORMAL_MAIL : sLayOutType);

			IsVisibleTitle();
			if (sBatch.Equals("1")) {
				if (sessionWoman.BatchMailUserList.Count < 1) {
					// メール送信ユーザーがいない場合は検索画面に戻ります。
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"FindBatchMailUser.aspx"));
				}
			}
			
			string sUserStatus = string.Empty;
			if (sMailDataSeq.Equals(string.Empty)) {
				// セッションにメールデータをいれておく入れ物を作ります。
				sMailDataSeq = iBridUtil.GetStringValue(sessionWoman.userWoman.NextMailDataSeq());
				sessionWoman.userWoman.mailData.Add(sMailDataSeq,new MailInfo());
				if (!sBatch.Equals("1")) {
					DataRow dr;
					int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

					if (sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),iRecNo,out dr) == false) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
					}

					string sUserSeq = iBridUtil.GetStringValue(dr["USER_SEQ"]);
					string sUserCharNo = iBridUtil.GetStringValue(dr["USER_CHAR_NO"]);
					sUserStatus = iBridUtil.GetStringValue(dr["USER_STATUS"]);

					sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq = sUserSeq;
					sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo = sUserCharNo;
					sessionWoman.userWoman.mailData[sMailDataSeq].rxUserNm = iBridUtil.GetStringValue(dr["HANDLE_NM"]);
					sessionWoman.userWoman.mailData[sMailDataSeq].layOutType = sLayOutType;
					sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);

					if (sessionWoman.logined) {
						// 相手からの拒否チェック
						CheckRefuse(sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo);
						
						using (Favorit oFavorit = new Favorit()) {
							//相手から見た時にオフライン、または会話中の場合、足あとをつけない
							int iCharacterOnlineStatus = ViCommConst.USER_OFFLINE;
							if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
								iCharacterOnlineStatus = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterOnlineStatus;
							}

							int iIsSeenOnlineStatus = oFavorit.GetIsSeenOnlineStatus(
										sessionWoman.site.siteCd,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,sUserSeq,
										sUserCharNo,
										iCharacterOnlineStatus);

							if (iIsSeenOnlineStatus != ViCommConst.USER_OFFLINE && iIsSeenOnlineStatus != ViCommConst.USER_TALKING) {

								using (Marking oMarking = new Marking()) {
									oMarking.MarkingMainte(
										sessionWoman.site.siteCd,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										sUserSeq,
										sUserCharNo,
										ViCommConst.MAN,
										0
									);
								}
							}
						}
					}

				}

				sessionWoman.userWoman.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_OFF;

			} else {
				// TxMailConfirmから戻るボタンで戻った場合、以前作ったセッションを使いまわしてTextBoxを復元させます。
				txtTitle.Text = sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle;
				txtDoc.Text = sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc;

				if (!sBatch.Equals("1")) {
					if (sessionWoman.SetManDataSetByUserSeq(sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,1) == false) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
					}
					DataRow oDataRow = sessionWoman.parseContainer.parseUser.dataTable[ViCommConst.DATASET_MAN].Rows[0];

					sUserStatus = iBridUtil.GetStringValue(oDataRow["USER_STATUS"]);
				}
			}

			if (sUserStatus.Equals(ViCommConst.USER_MAN_HOLD) || sUserStatus.Equals(ViCommConst.USER_MAN_RESIGNED) || sUserStatus.Equals(ViCommConst.USER_MAN_STOP) || sUserStatus.Equals(ViCommConst.USER_MAN_BLACK)) {
				RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_MAN);
			}
			if (!sBatch.Equals("1")) {
				// 自分が拒否しているチェック
				using (Refuse oRefuse = new Refuse()) {
					MailInfo oMailInfo = sessionWoman.userWoman.mailData[sMailDataSeq];
					if (oRefuse.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,oMailInfo.rxUserSeq,oMailInfo.rxUserCharNo)) {
						this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_MAN);
					}
				}
			}

			//メールアタック(新人)対象チェック
			sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag = ViCommConst.FLAG_OFF;
			using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
				string sResult = oMailAttackNew.RegistMailAttackNewTx(
													sessionWoman.site.siteCd,
													sessionWoman.userWoman.userSeq,
													sessionWoman.userWoman.curCharNo,
													sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,
													sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo,
													string.Empty,
													ViCommConst.FLAG_ON
												);
				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag = ViCommConst.FLAG_ON;
				}
			}
			sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackNewFlag = sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag;

			//メールアタック(ログイン)対象チェック
			sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag = ViCommConst.FLAG_OFF;
			using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
				string sResult = oMailAttackLogin.RegistMailAttackLoginTx(
													sessionWoman.site.siteCd,
													sessionWoman.userWoman.userSeq,
													sessionWoman.userWoman.curCharNo,
													sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,
													sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo,
													string.Empty,
													ViCommConst.FLAG_ON
												);
				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag = ViCommConst.FLAG_ON;
				}
			}
			sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackLoginFlag = sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag;

			// 3分以内メール返信ボーナス用の値を取得
			MailResBonus2 oMailResBonus2 = new MailResBonus2();
			sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus = oMailResBonus2.SetEventData(ref sessionWoman.userWoman);
			// メール返信ボーナス対象チェック
			sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag = ViCommConst.FLAG_OFF_STR;
			sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag = ViCommConst.FLAG_OFF_STR;
			if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus)) {
				MailResBonusLog2 oMailResBonusLog2 = new MailResBonusLog2();
				string pMainFlag;
				string pSubFlag;
				oMailResBonusLog2.CheckUser(
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusSeq,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec1,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec2,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusMaxNum,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusUnreceivedDays,
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,
					sessionWoman.userWoman.userSeq,
					out pMainFlag,
					out pSubFlag
				);
				if (pMainFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag = ViCommConst.FLAG_ON_STR;
				}
				if (pSubFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag = ViCommConst.FLAG_ON_STR;
				}
			}
			if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag)
				|| ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag)
			) {
				sessionWoman.userWoman.mailData[sMailDataSeq].mailResBonus2Flag = ViCommConst.FLAG_ON_STR;
			} else {
				sessionWoman.userWoman.mailData[sMailDataSeq].mailResBonus2Flag = ViCommConst.FLAG_OFF_STR;
			}

			ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;
			CheckOnlineStatus(sMailDataSeq,sBatch.Equals(ViCommConst.FLAG_ON_STR));

		} else {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState["PAGE_KEY"].ToString(),ViewState,IsPostBack);
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_CONFIRM] != null) {
				cmdConfirm_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_REGIST] != null) {
				cmdRegist_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (ViewState["LAY_OUT_TYPE"].Equals(ViCommConst.TxMailLayOutType.MEETING_MAIL)) {
			if (sessionWoman.carrier.Equals(ViCommConst.DOCOMO) || sessionWoman.carrier.Equals(ViCommConst.KDDI) || sessionWoman.carrier.Equals(ViCommConst.SOFTBANK)) {
				if (sessionWoman.userWoman.useVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
					sessionWoman.userWoman.SetupVoiceapp(sessionWoman.userWoman.userSeq,ViCommConst.FLAG_OFF);
				}
			}
		}

		bool bIsBatch = iBridUtil.GetStringValue(Request.QueryString["batch"]).Equals("1");
		Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false,bIsBatch,ViCommConst.SCR_TX_MAIL_COMPLITE_CAST);
	}

	private void SelectNomalMail() {
		foreach (MobileListItem oMobileListItem in this.lstTemplate.Items) {
			if (ViCommConst.MailTemplateType.NORMAL.Equals(oMobileListItem.Value)) {
				this.lstTemplate.SelectedIndex = oMobileListItem.Index;
				break;
			}
		}
	}

	//IMPACTのOVERRIDEで利用
	protected virtual void IsVisibleTitle() {
		// 何もしない。aspxで処理を追記する場合に使用 
	}

	protected void cmdConfirm_Click(object sender,EventArgs e) {
		Confirm(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false);
	}

	protected void cmdRegist_Click(object sender,EventArgs e) {
		MainteDraftMail(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false);
	}

	private void cmdDelete_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDelTxRx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,mailSeqList.ToArray());
			}
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("mail_delete_count",mailSeqList.Count.ToString());
			oParam.Add("loginid",iBridUtil.GetStringValue(this.Request.QueryString["loginid"]));
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_DELETED_MAIL_HISTORY,oParam);
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "TxMail.aspx?" + Request.QueryString));
		}
	}

	private void ProtectRxMail() {
		int iProtectFlag;
		int.TryParse(iBridUtil.GetStringValue(this.Request.QueryString["protectflag"]),out iProtectFlag);
		iProtectFlag = iProtectFlag == 1 ? 0 : 1;

		string sProtectMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["protectmailseq"]);
		string sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);
		string sListNo = iBridUtil.GetStringValue(this.Request.QueryString["listno"]);
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sProtectMailSeq,iProtectFlag);
			sessionWoman.SetMailValue("DEL_PROTECT_FLAG",iProtectFlag.ToString());
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   string.Format("TxMail.aspx?direct=1&scrid=01&stay=1&loginid={0}&userrecno=1&listno={1}#{2}",sLoginId,sListNo,sProtectMailSeq)));
	}
}
