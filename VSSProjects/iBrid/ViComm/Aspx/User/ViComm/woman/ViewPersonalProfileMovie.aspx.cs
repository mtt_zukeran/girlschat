/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフィール動画詳細表示
--	Progaram ID		: ViewPersonalProfileMovie
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2010/06/15  M.Horie    動画コメント入力欄追加対応

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewPersonalProfileMovie:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
				txtTitle.Text = sessionWoman.GetMoiveValue("MOVIE_TITLE");
				txtDoc.Text = sessionWoman.GetMoiveValue("MOVIE_DOC");
				ViewState["MOVIE_SEQ"] = sessionWoman.GetMoiveValue("MOVIE_SEQ");

				using (ManageCompany oManageCompany = new ManageCompany()) {
					txtDoc.Visible = oManageCompany.IsAvailableService(ViCommConst.RELEASE_PROFILE_MOVIE_INPUT_DOC);
				}

				if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
					using (CastMovie oMovie = new CastMovie()) {
						oMovie.UpdateProfileMovie(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							iBridUtil.GetStringValue(ViewState["MOVIE_SEQ"].ToString()),
							"",
							null,
							1,
							ViCommConst.ATTACHED_PROFILE,
							ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString(),
							ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString()
						);

						if (sMovieSeq.Equals("") == false) {
							DeleteMovie(iBridUtil.GetStringValue(ViewState["MOVIE_SEQ"].ToString()));
						}
					}
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_PROFILE_MOVIE) + "&site=" + sessionWoman.site.siteCd);
				} else if (!sMovieSeq.Equals("") && sPlayMovie.Equals("1")) {
					bDownload = MovieHelper.Download(sMovieSeq,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
				};

			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListPersonalProfileMovie.aspx") + "?site=" + sessionWoman.site.siteCd);
			}
		}
		if (bDownload == false) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			if (IsPostBack) {
				if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender,e);
				}
			}
		}
	}

	private void DeleteMovie(string pFileSeq) {
		string sFullPath = sessionWoman.site.webPhisicalDir +
							ViCommConst.MOVIE_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
							string.Format("{0:D15}",int.Parse(pFileSeq));
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + "s" + ViCommConst.MOVIE_FOODER)) {
				System.IO.File.Delete(sFullPath + "s" + ViCommConst.MOVIE_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER2)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER2);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER3)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER3);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		txtTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
		txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

		bool bOk = true;
		bOk = ValidText();
		int? iObjRankingFlag = null;

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]))) {
			iObjRankingFlag = int.Parse(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]));
		}

		if (bOk == false) {
			return;
		} else {
			string sTitle = Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text);
			string sDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
			if (sTitle.Length > 30) {
				sTitle = SysPrograms.Substring(sTitle,30);
			}
			if (sDoc.Length > 1000) {
				sDoc = SysPrograms.Substring(sDoc,1000);
			}
			using (CastMovie oMovie = new CastMovie()) {
				oMovie.UpdateProfileMovie(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					iBridUtil.GetStringValue(ViewState["MOVIE_SEQ"]),
					sTitle,
					sDoc,
					0,
					ViCommConst.ATTACHED_PROFILE,
					ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString(),
					ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString(),
					iObjRankingFlag
				);
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListPersonalProfileMovie.aspx") + "?site=" + sessionWoman.site.siteCd);
		}
	}

	private bool ValidText() {
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		bool bOk = true;
		if (txtTitle.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("") && txtDoc.Visible) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		return bOk;
	}
}
