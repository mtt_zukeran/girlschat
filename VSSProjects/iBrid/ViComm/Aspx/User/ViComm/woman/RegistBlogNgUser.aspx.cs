/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ��۸޺��Đ����o�^
--	Progaram ID		: RegistBlogNgUser
--
--  Creation Date	: 2012.12.26
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistBlogNgUser:MobileWomanPageBase {
	private string sLoginId;
	private string sManUserSeq;
	private string sManUserCharNo;

	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);

		if (string.IsNullOrEmpty(sLoginId)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		DataSet dsMan;

		if (!sessionWoman.ControlList(Request,ViCommConst.INQUIRY_DIRECT,ActiveForm,out dsMan)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		sManUserSeq = iBridUtil.GetStringValue(dsMan.Tables[0].Rows[0]["USER_SEQ"]);
		sManUserCharNo = iBridUtil.GetStringValue(dsMan.Tables[0].Rows[0]["USER_CHAR_NO"]);

		if (!this.IsPostBack) {
			pnlConfirm.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}			
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		using (BlogNgUser oBlogBlogNgUser = new BlogNgUser()) {
			oBlogBlogNgUser.RegistBlogNgUser(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sManUserSeq,
				sManUserCharNo
			);
		}

		pnlConfirm.Visible = false;
		pnlComplete.Visible = true;
	}
}