﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 銀行口座情報変更
--	Progaram ID		: ModifyCastBank
--
--  Creation Date	: 2010.02.03
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using Microsoft.VisualBasic;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ModifyCastBank:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			txtBankNm.Text = sessionWoman.userWoman.bankNm;
			txtBankOfficeNm.Text = sessionWoman.userWoman.bankOfficeNm;
			txtBankOfficeKanaNm.Text = sessionWoman.userWoman.bankOfficeKanaNm;
			txtBankAccountType.Text = sessionWoman.userWoman.bankAccountType;
			if (sessionWoman.userWoman.bankAccountType2.Equals(ViCommConst.BankAccountType.NORMAL_ACCOUNT)) {
				for (int i = 0;i < this.rdoBankAccountType.Items.Count;i++) {
					if (this.rdoBankAccountType.Items[i].Value.Equals(ViCommConst.BankAccountType.NORMAL_ACCOUNT)) {
						this.rdoBankAccountType.Items[i].Selected = true;
					} else {
						this.rdoBankAccountType.Items[i].Selected = false;
					}
				}
			} else if (sessionWoman.userWoman.bankAccountType2.Equals(ViCommConst.BankAccountType.CHECKING_ACCOUNT)) {
				for (int i = 0;i < this.rdoBankAccountType.Items.Count;i++) {
					if (this.rdoBankAccountType.Items[i].Value.Equals(ViCommConst.BankAccountType.CHECKING_ACCOUNT)) {
						this.rdoBankAccountType.Items[i].Selected = true;
					} else {
						this.rdoBankAccountType.Items[i].Selected = false;
					}
				}
			} else if (sessionWoman.userWoman.bankAccountType2.Equals(ViCommConst.BankAccountType.SAVINGS_ACCOUNTS)) {
				for (int i = 0;i < this.rdoBankAccountType.Items.Count;i++) {
					if (this.rdoBankAccountType.Items[i].Value.Equals(ViCommConst.BankAccountType.SAVINGS_ACCOUNTS)) {
						this.rdoBankAccountType.Items[i].Selected = true;
					} else {
						this.rdoBankAccountType.Items[i].Selected = false;
					}
				}
			}
			if (IsAvailableService(ViCommConst.RELEASE_CAST_INPUT_BANK_CD,2)) {
				pnlBankCd.Visible = true;
				pnlBankOfficeCd.Visible = true;
			} else {
				pnlBankCd.Visible = false;
				pnlBankOfficeCd.Visible = false;
			}
			txtBankCd.Text = sessionWoman.userWoman.bankCd;
			txtBankOfficeCd.Text = sessionWoman.userWoman.bankOfficeCd;
			txtBankAccountHolderNm.Text = sessionWoman.userWoman.bankAccountHolderNm;
			txtBankAccountNo.Text = sessionWoman.userWoman.bankAccountNo;

			if (IsAvailableService(ViCommConst.RELEASE_SUPPORT_BANK_OFFICE_NO)) {
				txtBankOfficeNm.Numeric = true;
				txtBankOfficeNm.MaxLength = 3;
				txtBankOfficeNm.Size = 10;
				lblBankOfficeNm.Text = "支店番号";
			}
			if (IsAvailableService(ViCommConst.RELEASE_BANK_TYPE_RADIO)) {
				txtBankAccountType.Visible = false;
				rdoBankAccountType.Visible = true;
			} else {
				txtBankAccountType.Visible = true;
				rdoBankAccountType.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		txtBankNm.Text = HttpUtility.HtmlEncode(txtBankNm.Text);
		txtBankCd.Text = HttpUtility.HtmlEncode(txtBankCd.Text);
		txtBankOfficeNm.Text = HttpUtility.HtmlEncode(txtBankOfficeNm.Text);
		txtBankOfficeCd.Text = HttpUtility.HtmlEncode(txtBankOfficeCd.Text);
		txtBankAccountType.Text = HttpUtility.HtmlEncode(txtBankAccountType.Text);
		txtBankAccountNo.Text = HttpUtility.HtmlEncode(txtBankAccountNo.Text);
		
		string sBankAccountType2 = string.Empty;
		bool bOk = true;

		lblErrorMessage.Text = "";

		//各入力項目へ合わせて変換 
		InputTextConvert();

		if (txtBankNm.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_NM);
		}

		using (ManageCompany oCompany = new ManageCompany()) {
			if (!oCompany.IsAvailableService(ViCommConst.RELEASE_SUPPORT_BANK_OFFICE_NO)) {
				if (txtBankOfficeNm.Text.Equals("")) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_OFFICE_NM);
				}
			} else {
				// 支店番号 
				if (txtBankOfficeNm.Text.Equals("")) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_OFFICE_NO);
				}
				if (bOk) {
					if (!SysPrograms.Expression(@"\d{3}",txtBankOfficeNm.Text)) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_OFFICE_NO);
					}
				}
			}
		}
		if (txtBankOfficeKanaNm.Visible) {
			if (txtBankOfficeKanaNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_OFFICE_KANA_NM);
			} else {
				//全角カナチェック 
				if (!SysPrograms.Expression(@"^[ァ-ー]+$",txtBankOfficeKanaNm.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_VALIDATE_OFFICE_KANA_NM);
				}
			}
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (IsAvailableService(ViCommConst.RELEASE_BANK_TYPE_RADIO)) {
				if (this.rdoBankAccountType.Selection.Value.Equals(ViCommConst.BankAccountType.NORMAL_ACCOUNT)) {
					sBankAccountType2 = ViCommConst.BankAccountType.NORMAL_ACCOUNT;
				} else if (this.rdoBankAccountType.Selection.Value.Equals(ViCommConst.BankAccountType.CHECKING_ACCOUNT)) {
					sBankAccountType2 = ViCommConst.BankAccountType.CHECKING_ACCOUNT;
				} else if (this.rdoBankAccountType.Selection.Value.Equals(ViCommConst.BankAccountType.SAVINGS_ACCOUNTS)) {
					sBankAccountType2 = ViCommConst.BankAccountType.SAVINGS_ACCOUNTS;
				}

				if (sBankAccountType2.Equals(string.Empty)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_ACCOUNT_TYPE);
				}
			} else {
				if (txtBankAccountType.Text.Equals("")) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_ACCOUNT_TYPE);
				}
			}
		}
		if (txtBankAccountNo.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_ACCOUNT_NO);
		} else {
			//数字チェック
			if (!SysPrograms.Expression(@"^\d+$",txtBankAccountNo.Text)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_VALIDATE_BANK_ACCOUNT_NO);
			}
		}
		if (txtBankAccountHolderNm.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BANK_ACCOUNT_HOLDER_NM);
		} else {
			//全角カナチェック
			if (!SysPrograms.Expression(@"^[ァ-ー]+　[ァ-ー]+$",txtBankAccountHolderNm.Text)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_VALIDATE_BANK_ACCOUNT_HOLDER_NM);
			}
		}

		if (txtBankCd.Visible) {
			if (!txtBankCd.Text.Equals("")) {
				if (!SysPrograms.Expression(@"\d{4}",txtBankCd.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_VALIDATE_BANK_CD);
				}
			}
		}
		if (txtBankOfficeCd.Visible) {
			if (!txtBankOfficeCd.Text.Equals("")) {
				if (!SysPrograms.Expression(@"\d{3}",txtBankOfficeCd.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_VALIDATE_BANK_OFFICE_CD);
				}
			}
		}
		if (bOk == false) {
			return;
		} else {
			sessionWoman.userWoman.ModifyBank(
				sessionWoman.userWoman.userSeq,
				txtBankNm.Text,
				txtBankCd.Text,
				txtBankOfficeNm.Text,
				txtBankOfficeKanaNm.Text,
				txtBankOfficeCd.Text,
				txtBankAccountType.Text,
				sBankAccountType2,
				txtBankAccountHolderNm.Text,
				txtBankAccountNo.Text
			);
		}
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_BANK_COMPLITE) + "&site=" + sessionWoman.site.siteCd);
	}

	private void InputTextConvert() {
		//口座番号は全角→半角 
		txtBankAccountNo.Text = Strings.StrConv(txtBankAccountNo.Text,VbStrConv.Narrow,0);
		//支店カナ、口座名義は半角→全角 
		txtBankOfficeKanaNm.Text = Strings.StrConv(txtBankOfficeKanaNm.Text,VbStrConv.Katakana | VbStrConv.Wide,0);
		txtBankAccountHolderNm.Text = Strings.StrConv(txtBankAccountHolderNm.Text,VbStrConv.Katakana | VbStrConv.Wide,0);
	}
}
