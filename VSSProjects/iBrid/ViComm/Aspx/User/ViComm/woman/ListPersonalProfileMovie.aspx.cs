/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 個人別プロフィール動画一覧
--	Progaram ID		: ListPersonalProfileMovie
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListPersonalProfileMovie:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;

		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sMovieSeq) && sPlayMovie.Equals("1")) {
				bDownload = MovieHelper.Download(sMovieSeq, sessionWoman.userWoman.userSeq, sessionWoman.userWoman.curCharNo);
			};
			if (bDownload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
				sessionWoman.ControlList(Request, ViCommConst.INQUIRY_NONE_DISP_PROFILE, ActiveForm);

			}
		}else{
			if (IsPostBack) {
				if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender, e);
				}
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListPersonalProfileMovie.aspx") + "?site=" + sessionWoman.site.siteCd);
	}



}
