/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: クイックレスポンスルーム
--	Progaram ID		: QuickResponseRoom
--
--  Creation Date	: 2011.06.02
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_QuickResponseRoom:MobileWomanPageBase {

	string sInOut;
	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sInOut = iBridUtil.GetStringValue(Request.QueryString["inout"]);

		if (!IsPostBack) {
			sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);

			bool bInOutFlag;

			if (sInOut.Equals(ViCommConst.QuickResponse.IN)) {
				//INパラメータできたが、レスポンスリクエストルームにいる場合は何もしない。 
				if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.quickResFlag == ViCommConst.FLAG_ON &&
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.quickResOutSchTime > DateTime.Now) {

					return;
				}
				bInOutFlag = true;
			} else if (sInOut.Equals(ViCommConst.QuickResponse.EXTENSION)) {
				bInOutFlag = true;
			} else {
				bInOutFlag = false;
			}
			QuickResponseRoom(bInOutFlag);
		}
	}

	private void QuickResponseRoom(bool pInOutFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("QUICK_RESPONSE_ROOM");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sessionWoman.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.curCharNo);
			db.ProcedureInParm("PQUICK_RESPONSE_ROOM",DbSession.DbType.VARCHAR2,pInOutFlag ? ViCommConst.QuickResponse.IN : ViCommConst.QuickResponse.OUT);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
			
			string sResult = db.GetStringValue("PRESULT");
			if (sResult.Equals("0")) {
				//正常終了
				if (sInOut.Equals(ViCommConst.QuickResponse.OUT)) {
					RedirectToDisplayDoc(ViCommConst.SCR_QUICK_RESPONSE_OUT);
				}
			} else {
				//BLACK
				RedirectToDisplayDoc(ViCommConst.SCR_QUICK_RESPONSE_BLACK);
			}
		}
	}
}
