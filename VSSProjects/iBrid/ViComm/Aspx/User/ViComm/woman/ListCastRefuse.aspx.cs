/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���ۃ��X�g�ꗗ
--	Progaram ID		: ListCastRefuse
--
--  Creation Date	: 2009.08.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using System.Web.UI.MobileControls;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListCastRefuse:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionWoman.ClearCategory();
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_REFUSE,
					ActiveForm);
		}
	}
	
}
