﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド更新
--	Progaram ID		: ModifyBbsThread
--
--  Creation Date	: 2011.04.13
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;

public partial class ViComm_woman_ModifyBbsThread:MobileBbsExWomanBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {

			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,ActiveForm)) {
				string sback = iBridUtil.GetStringValue(Request.QueryString["back"]);
				if (sback.Equals(ViCommConst.FLAG_ON_STR)) {
					txtDoc.Text = sessionWoman.userWoman.bbsInfo.bbsThreadDoc;
				} else {
					txtDoc.Text = sessionWoman.GetBbsThreadValue("BBS_THREAD_DOC1") +
									sessionWoman.GetBbsThreadValue("BBS_THREAD_DOC2") +
									sessionWoman.GetBbsThreadValue("BBS_THREAD_DOC3") +
									sessionWoman.GetBbsThreadValue("BBS_THREAD_DOC4");
				}
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBbsThread.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {

		if (!ValidateText()) {
			return;
		} else {
			sessionWoman.userWoman.bbsInfo.bbsThreadDoc = HttpUtility.HtmlEncode(txtDoc.Text);

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ConfirmModifyBbsThread.aspx?bbsthreadseq={0}",iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]))));
		}
	}

	private bool ValidateText() {
		bool bOk = true;
		string sNgWord = string.Empty;
		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		lblErrorMessage.Text = string.Empty;

		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_THREAD_DOC_EMPTY);
		}
		if (txtDoc.Text.Length > 4000) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_THREAD_DOC_LENGTH);
		}
		if (!sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNgWord)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(string.Format(ViCommConst.ERR_STATUS_BBS_THREAD_DOC_NG,sNgWord));
		}

		return bOk;
	}
}
