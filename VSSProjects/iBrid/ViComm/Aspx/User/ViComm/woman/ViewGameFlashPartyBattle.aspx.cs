/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����ٹްїpFlash(�߰è����)
--	Progaram ID		: ViewGameFlashBattle
--
--  Creation Date	: 2011.11.14
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_ViewGameFlashPartyBattle:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sBattleLogSeq = this.Request.QueryString["battle_log_seq"];

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(sBattleLogSeq));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		string sLevelUpFlag = Request.QueryString["level_up_flag"];
		string sAddForceCount = Request.QueryString["add_force_count"];
		string sPreExp = Request.QueryString["pre_exp"];
		string sTreasureCompleteFlag = iBridUtil.GetStringValue(Request.QueryString["treasure_complete_flag"]);
		string sComePoliceFlag = iBridUtil.GetStringValue(Request.QueryString["come_police_flag"]);
		string sTrapUsedFlag = iBridUtil.GetStringValue(Request.QueryString["trap_used_flag"]);
		string sPartnerUserSeq = string.Empty;
		string sPartnerUserCharNo = string.Empty;
		string sAttackWinFlag = string.Empty;
		string sDefenceWinFlag = string.Empty;
		string sPartnerGameCharacterType = string.Empty;

		this.GetBattleLogData(
			sBattleLogSeq,
			out sPartnerUserSeq,
			out sPartnerUserCharNo,
			out sAttackWinFlag,
			out sDefenceWinFlag,
			out sPartnerGameCharacterType
		);

		string sSelfPartyMemberMsk = this.GetPartyMemberMsk(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
		string sPartnerPartyMemberMsk = this.GetPartyMemberMsk(sPartnerUserSeq,sPartnerUserCharNo);

		if (string.IsNullOrEmpty(sPartnerGameCharacterType)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		string sFileNm = string.Empty;
		string sNextPageNm;
		string sGetParamStr = string.Empty;

		if (sTreasureCompleteFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sNextPageNm = "ViewGameFlashComplete.aspx";
		} else {
			if (sLevelUpFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sNextPageNm = "ViewGameFlashLevelUp.aspx";
			} else {
				if (sAttackWinFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sNextPageNm = "ViewGameBattleResultWinParty.aspx";
				} else {
					sNextPageNm = "ViewGameBattleResultLoseParty.aspx";
				}
			}
		}

		if (sAttackWinFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sFileNm = "w_party_win.swf";
		} else {
			sFileNm = "w_party_lose.swf";
		}

		if (this.Request.QueryString.Count > 0) {
			string[] keys = this.Request.QueryString.AllKeys;
			string value = string.Empty;
			List<string> oGetParamList = new List<string>();

			foreach (string key in keys) {
				oGetParamList.Add(key + "=" + iBridUtil.GetStringValue(this.Request.QueryString[key]));
			}

			sGetParamStr = string.Join("&",oGetParamList.ToArray());
		}

		NameValueCollection oSwfParam = new NameValueCollection();
		oSwfParam.Add("char1",sessionWoman.userWoman.CurCharacter.gameCharacter.gameCharacterType);
		oSwfParam.Add("char2",sPartnerGameCharacterType);
		if (sSelfPartyMemberMsk.Equals("0")) {
			sSelfPartyMemberMsk = "8";
		}
		if (sPartnerPartyMemberMsk.Equals("0")) {
			sPartnerPartyMemberMsk = "8";
		}
		oSwfParam.Add("party1",sSelfPartyMemberMsk);
		oSwfParam.Add("party2",sPartnerPartyMemberMsk);
		GameFlashLiteHelper.ResponseFlashSocialGame(sFileNm,sNextPageNm,sGetParamStr,oSwfParam);
	}

	private void GetBattleLogData(
		string sBattleLogSeq,
		out string sPartnerUserSeq,
		out string sPartnerUserCharNo,
		out string sAttackWinFlag,
		out string sDefenceWinFlag,
		out string sPartnerGameCharacterType
	) {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.BattleLogSeq = sBattleLogSeq;

		DataSet pDS = null;
		using (BattleLog oBattleLog = new BattleLog()) {
			pDS = oBattleLog.GetOneSimpleData(oCondition);
		}

		sPartnerUserSeq = pDS.Tables[0].Rows[0]["PARTNER_USER_SEQ"].ToString();
		sPartnerUserCharNo = pDS.Tables[0].Rows[0]["PARTNER_USER_CHAR_NO"].ToString();
		sAttackWinFlag = pDS.Tables[0].Rows[0]["ATTACK_WIN_FLAG"].ToString();
		sDefenceWinFlag = pDS.Tables[0].Rows[0]["DEFENCE_WIN_FLAG"].ToString();
		sPartnerGameCharacterType = pDS.Tables[0].Rows[0]["PARTNER_GAME_CHARACTER_TYPE"].ToString();
	}

	private string GetPartyMemberMsk(string sUserSeq,string sUserCharNo) {
		string sPartyMemberMsk = string.Empty;

		GamePartyBattleMemberSeekCondition oCondition = new GamePartyBattleMemberSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sUserSeq;
		oCondition.UserCharNo = sUserCharNo;

		using (GamePartyBattleMember oGamePartyBattleMember = new GamePartyBattleMember()) {
			sPartyMemberMsk = oGamePartyBattleMember.GetPartyMemberMask(oCondition);
		}

		return sPartyMemberMsk;
	}
}