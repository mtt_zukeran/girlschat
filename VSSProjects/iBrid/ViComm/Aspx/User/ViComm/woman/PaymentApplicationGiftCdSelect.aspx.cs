﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 精算申請(ギフト券) 枚数選択
--	Progaram ID		: PaymentApplicationGiftCdSelect
--
--  Creation Date	: 2017.04.10
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_PaymentApplicationGiftCdSelect:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		// パラメータチェック
		string sGiftCodeType = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["id"]);
		string sPaymentAmt = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["amt"]);
		if (string.IsNullOrEmpty(sGiftCodeType)
			|| string.IsNullOrEmpty(sPaymentAmt)
		) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("PaymentApplicationGiftCd.aspx"));
		}

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_EXTENSION_PAYMENT_GIFT_SELECT,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdTopPage"] != null) {
				cmdTopPage_Click(sender,e);
			}
		}
	}

	/// <summary>
	/// 「清算申請確認」ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sPaymentNum = iBridUtil.GetStringValue(sessionWoman.requestQuery.Params["gift_payment_num"]);
		string sGiftCodeType = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["id"]);
		string sPaymentAmt = iBridUtil.GetStringValue(sessionWoman.requestQuery.QueryString["amt"]);
		string sResult = string.Empty;

		// ギフト券交換判定
		GiftCode oGiftCode = new GiftCode();
		oGiftCode.PaymentGiftCode(
			sessionWoman.site.siteCd
			,sessionWoman.userWoman.userSeq
			,sessionWoman.userWoman.loginId
			,sGiftCodeType
			,sPaymentAmt
			,sPaymentNum
			,ViCommConst.FLAG_ON_STR
			,out sResult
		);

		// 共通パラメータ
		string sParam = string.Format(
			"?id={0}&amt={1}&payment_num={2}"
			,sGiftCodeType
			,sPaymentAmt
			,sPaymentNum
		);

		// エラーなし
		if (sResult.Equals("0")) {
			// ギフト券交換確認ページ
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("PaymentApplicationGiftCdConfirm.aspx" + sParam));
		}
		// エラーあり
		else {
			// ギフト券枚数選択ページ
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format(
				"PaymentApplicationGiftCdSelect.aspx{0}&eno={1}"
				,sParam
				,sResult
			)));
		}
	}

	protected void cmdTopPage_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("UserTop.aspx"));
	}
}
