/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ 会員のつぶやきコメント一覧
--	Progaram ID		: ListManTweetComment
--
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListManTweetComment:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			sessionWoman.errorMessage = string.Empty;
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_TWEET_COMMENT,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		
		string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.Form["userseq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.Form["usercharno"]);
		string sManTweetSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetseq"]);
		
		int iTxMailFlag;
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["tx_mail_flag"]),out iTxMailFlag);
		
		//自分が拒否している場合
		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sPartnerUserSeq,sPartnerUserCharNo)) {
				this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_MAN);
			}
		}
		
		int iLimitCount = 0;
		string sLimitCount = iBridUtil.GetStringValue(this.Request.Form["limitCount"]);
		int.TryParse(sLimitCount,out iLimitCount);

		int iMinLength = 0;
		string sMinLength = iBridUtil.GetStringValue(this.Request.Form["minLength"]);
		int.TryParse(sMinLength,out iMinLength);
		
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);

		bool bOk = true;
		
		if (iLimitCount != 0) {
			decimal dCommentCount = 0;
			
			using (ManTweetComment oManTweetComment = new ManTweetComment()) {
				dCommentCount = oManTweetComment.GetManTweetCommentCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sManTweetSeq);
			}
			
			if (iLimitCount <= (int)dCommentCount) {
				sessionWoman.errorMessage += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_TWEET_COMMENT_COUNT_OVER_LIMIT),iLimitCount);
				return;
			}
		}

		if (txtComment.Text.Equals("")) {
			bOk = false;
			//本文を入力して下さい。 
			sessionWoman.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}
		if (iMaxLength != 0 && txtComment.Text.Length > iMaxLength) {
			bOk = false;
			sessionWoman.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"ｺﾒﾝﾄ");
		}
		if (iMinLength != 0 && txtComment.Text.Length < iMinLength) {
			bOk = false;
			sessionWoman.errorMessage += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_TWEET_COMMENT_TOO_SHORT);
		}

		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			string sNGWord;
			if (sessionWoman.ngWord.VaidateDoc(txtComment.Text,out sNGWord) == false) {
				bOk = false;
				sessionWoman.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		string sSiteCd = sessionWoman.site.siteCd;
		string sUserSeq = sessionWoman.userWoman.userSeq;
		string sUserCharNo = sessionWoman.userWoman.curCharNo;
		string sCommentText = string.Empty;
		string sResult = string.Empty;

		if (bOk == false) {
			return;
		} else {
			using (ManTweetComment oManTweetComment = new ManTweetComment()) {
				sCommentText = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtComment.Text));
				oManTweetComment.WriteManTweetComment(sSiteCd,sUserSeq,sUserCharNo,sManTweetSeq,sCommentText,iTxMailFlag,out sResult);
			}

			string sTweetUserSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetuserseq"]);
			string sTweetUserCharNo = iBridUtil.GetStringValue(this.Request.QueryString["tweetusercharno"]);

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListManTweetComment.aspx?tweetseq={0}&tweetuserseq={1}&tweetusercharno={2}&pageno={3}",sManTweetSeq,sTweetUserSeq,sTweetUserCharNo,iBridUtil.GetStringValue(this.Request.QueryString["pageno"]))));
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}
}