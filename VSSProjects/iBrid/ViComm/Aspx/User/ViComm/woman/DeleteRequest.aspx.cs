/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい削除/復活
--	Progaram ID		: DeleteRequest
--
--  Creation Date	: 2012.07.03
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteRequest:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		string sRequestSeq = iBridUtil.GetStringValue(Request.QueryString["req_seq"]);

		if (string.IsNullOrEmpty(sRequestSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!this.checkAccess()) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			pnlConfirm.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sRequestSeq);
			}
		}
	}

	private bool checkAccess() {
		if (sessionWoman.IsValidMask(sessionWoman.userWoman.CurCharacter.userDefineMask,PwViCommConst.CastUserDefFlag.REQUEST_ADMIN)) {
			return true;
		}

		return false;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pRequestSeq) {
		string sResult = string.Empty;

		using (PwRequest oPwRequest = new PwRequest()) {
			sResult = oPwRequest.DeleteRequest(
				sessionWoman.site.siteCd,
				pRequestSeq,
				ViCommConst.FLAG_ON_STR
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}