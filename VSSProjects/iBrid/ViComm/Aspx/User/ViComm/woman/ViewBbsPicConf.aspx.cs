/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �f���摜�Ǘ�
--	Progaram ID		: ViewBbsPicConf
--
--  Creation Date	: 2009.08.24
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web.UI.MobileControls;

public partial class ViComm_woman_ViewBbsPicConf:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
				if (sessionWoman.GetBbsPicValue("PLANNING_TYPE").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
					txtTitle.Visible = false;
					txtDoc.Visible = false;
				}
				SetDropDownList(sessionWoman.GetBbsPicValue("CAST_PIC_ATTR_TYPE_SEQ"),sessionWoman.GetBbsPicValue("CAST_PIC_ATTR_SEQ"));
				txtTitle.Text = iBridUtil.GetStringValue(sessionWoman.GetBbsPicValue("PIC_TITLE")).Replace("\r\n",string.Empty).Replace("\n",string.Empty).Replace("\r",string.Empty);
				txtDoc.Text = sessionWoman.GetBbsPicValue("PIC_DOC");
				string sObjRankingFlag = sessionWoman.GetBbsPicValue("OBJ_RANKING_FLAG");
				foreach (MobileListItem item in lstRankingFlag.Items) {
					if (item.Value == sObjRankingFlag) {
						item.Selected = true;
						break;
					}
				}
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListBbsPicConf.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sFileSeq = sessionWoman.GetBbsPicValue("PIC_SEQ");
		bool bUnauthFlag = sessionWoman.GetBbsPicValue("OBJ_NOT_APPROVE_FLAG").Equals("1");
		bool bUpdate = false;

		if (IsAvailableService(ViCommConst.RELEASE_DISABLE_BBS_MODIFY_LIST)) {
			bUpdate = true;
		} else if (lstSetUp.SelectedIndex == 0) {
			bUpdate = true;
		}

		if (bUpdate) {
			txtTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
			txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

			bool bOk = sessionWoman.GetBbsPicValue("PLANNING_TYPE").ToString().Equals(ViCommConst.FLAG_ON_STR) | ValidText();
			if (bOk == false) {
				return;
			} else {
				UpdateProfilePicture(sFileSeq,"0","0",bUnauthFlag);
				if (sessionWoman.GetBbsPicValue("PLANNING_TYPE").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.SCR_UPDATED_CAST_PLANNING_PIC) + "&site=" + sessionWoman.site.siteCd);
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.SCR_UPDATED_CAST_BBS_PIC) + "&site=" + sessionWoman.site.siteCd);
				}
			}
		} else {
			DeletePicture(sFileSeq,bUnauthFlag);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_BBS_PIC) + "&site=" + sessionWoman.site.siteCd);
		}
	}

	protected void cmdDelete_Click(object sender,EventArgs e) {
		if (!this.IsDeletable()) {
			return;
		}
		string sFileSeq = sessionWoman.GetBbsPicValue("PIC_SEQ");
		bool bUnauthFlag = sessionWoman.GetBbsPicValue("OBJ_NOT_APPROVE_FLAG").Equals("1");
		DeletePicture(sFileSeq,bUnauthFlag);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_BBS_PIC) + "&site=" + sessionWoman.site.siteCd);
	}

	private void UpdateProfilePicture(string pPicSeq,string pDelFlag,string pProfilePicFlag,bool pNotApproveFlag) {
		string sCastPicAttrTypeSeq;
		string sCastPicAttrSeq;
		if (lstAttrSeq.Visible) {
			string[] sCastPicAttr = lstAttrSeq.Selection.Value.Split(':');
			sCastPicAttrTypeSeq = sCastPicAttr[0];
			sCastPicAttrSeq = sCastPicAttr[1];
		} else {
			sCastPicAttrTypeSeq = ViewState["ATTR_TYPE_SEQ"].ToString();
			sCastPicAttrSeq = ViewState["ATTR_SEQ"].ToString();
		}

		string sTitle = Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text);
		string sDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
		if (sTitle.Length > 30) {
			sTitle = SysPrograms.Substring(sTitle,30);
		}
		if (sDoc.Length > 1000) {
			sDoc = SysPrograms.Substring(sDoc,1000);
		}
		int iRankingFlag;
		if (!int.TryParse(lstRankingFlag.Items[lstRankingFlag.SelectedIndex].Value,out iRankingFlag)) {
			iRankingFlag = 1;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sessionWoman.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.curCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_BBS);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,pNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,sTitle);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,sDoc);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,sCastPicAttrTypeSeq);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,sCastPicAttrSeq);
			db.ProcedureInParm("POBJ_RANKING_FLAG",DbSession.DbType.NUMBER,iRankingFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}

		string sRotateType = iBridUtil.GetStringValue(this.Request.Params["rotate_type"]);
		RotateFlipType oRoateType = ImageUtil.Convert2RotateFlipType(sRotateType);

		string sWebPhisicalDir = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(sessionWoman.site.siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}
		string sCastPicDir = ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sessionWoman.site.siteCd,sessionWoman.userWoman.loginId);
		ImageUtil.RotateFilp(sCastPicDir,pPicSeq,oRoateType);

	}

	private void DeletePicture(string pFileSeq,bool pNotApproveFlag) {
		UpdateProfilePicture(pFileSeq,"1","0",pNotApproveFlag);
		string sFullPath = sessionWoman.site.webPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
							string.Format("{0:D15}",int.Parse(pFileSeq));
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath + ViCommConst.PIC_FOODER)) {
				System.IO.File.Delete(sFullPath + ViCommConst.PIC_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.PIC_FOODER_SMALL)) {
				System.IO.File.Delete(sFullPath + ViCommConst.PIC_FOODER_SMALL);
			}
		}
	}

	private void SetDropDownList(string pCastPicAttrTypeSeq,string pCastPicAttrSeq) {
		if (IsAvailableService(ViCommConst.RELEASE_DISABLE_BBS_MODIFY_LIST)) {
			lstSetUp.Visible = false;
		} else {
			lstSetUp.Items.Add(new MobileListItem("�C������","1"));
			lstSetUp.Items.Add(new MobileListItem("�폜����","2"));
		}
		int iIdx = 0;

		using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
			DataSet ds = oCastPicAttrTypeValue.GetList(sessionWoman.site.siteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstAttrSeq.Items.Add(new MobileListItem(dr["CAST_PIC_ATTR_TYPE_NM"].ToString() + ":" + dr["CAST_PIC_ATTR_NM"].ToString(),dr["CAST_PIC_ATTR_TYPE_SEQ"].ToString() + ":" + dr["CAST_PIC_ATTR_SEQ"].ToString()));
				if (dr["CAST_PIC_ATTR_TYPE_SEQ"].ToString().Equals(pCastPicAttrTypeSeq) && dr["CAST_PIC_ATTR_SEQ"].ToString().Equals(pCastPicAttrSeq)) {
					lstAttrSeq.SelectedIndex = iIdx;
				}
				iIdx++;
			}
		}

		if (sessionWoman.site.bbsPicAttrFlag != ViCommConst.FLAG_OFF) {
			lstAttrSeq.Visible = true;
			if (sessionWoman.site.supportChgObjCatFlag == ViCommConst.FLAG_OFF) {
				lstAttrSeq.Visible = false;
			}
		} else {
			lstAttrSeq.Visible = false;
		}

		if (!lstAttrSeq.Visible) {
			ViewState["ATTR_TYPE_SEQ"] = pCastPicAttrTypeSeq;
			ViewState["ATTR_SEQ"] = pCastPicAttrSeq;
		}
	}

	protected virtual bool ValidText() {
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		bool bOk = true;
		if (txtTitle.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		return bOk;
	}

	private bool IsDeletable(){
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		bool bOk = true;

		using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
			if (!oCastPicAttrTypeValue.IsDeletable(sessionWoman.site.siteCd,iBridUtil.GetStringValue(ViewState["ATTR_SEQ"]))) {
				this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_POSTER_DEL_NA);
				bOk = false;
			}
		}
		return bOk;
	}
}
