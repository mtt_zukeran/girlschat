/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 固体識別利用再ﾛｸﾞｲﾝ
--	Progaram ID		: ReLoginUser
--
--  Creation Date	: 2010.02.09
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ReLoginUser:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			ParseHTML oParseWoman = sessionWoman.parseContainer;
			oParseWoman.parseUser.postAction = ViCommConst.POST_ACT_UTN;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUtn = Mobile.GetUtn(sessionWoman.carrier,Request);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("LoginUser.aspx?utn={0}&useutn=1",sUtn)));
	}
}
