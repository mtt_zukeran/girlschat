/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ(一次)実行
--	Progaram ID		: GetMailLottery
--
--  Creation Date	: 2015.01.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_GetMailLottery:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["mailseq"]);
		string sParterMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["partnermailseq"]);
		
		string sMailDataSeq = iBridUtil.GetStringValue(this.Request.QueryString["data"]);

		if (string.IsNullOrEmpty(sMailSeq) || string.IsNullOrEmpty(sParterMailSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
		
		string sMailLotteryRateSeq = string.Empty;
		string sLoseFlag = string.Empty;
		string sResult = string.Empty;
		
		using (MailLottery oMailLottery = new MailLottery()) {
			oMailLottery.GetMailLottery(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR,
				sMailSeq,
				sParterMailSeq,
				ViCommConst.FLAG_OFF,
				out sMailLotteryRateSeq,
				out sLoseFlag,
				out sResult
			);
		}

		if (!sResult.Equals(PwViCommConst.GetMailLotteryResult.RESULT_NG)) {
			string sNextUrl = string.Format("DisplayDoc.aspx?doc={0}&result={1}",PwViCommConst.SCR_CAST_MAIL_LOTTERY_COMPLETE,sResult);

			if (sResult.Equals(PwViCommConst.GetMailLotteryResult.RESULT_OK)) {
				sNextUrl = string.Format("{0}&lottery={1}&lose={2}",sNextUrl,sMailLotteryRateSeq,sLoseFlag);
				
				if (!sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
					if (IsAvailableService(ViCommConst.RELEASE_MAIL_LOTTERY_MOVIE,2)) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ViewFlashMailLottery.aspx?next_url={0}&lose={1}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)),sLoseFlag)));
					} else {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sNextUrl));
					}
				} else {
					if (IsAvailableService(ViCommConst.RELEASE_MAIL_LOTTERY_MOVIE_IPHONE,2)) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ViewFlashMailLottery.aspx?next_url={0}&lose={1}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)),sLoseFlag)));
					} else {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sNextUrl));
					}
				}
			} else if (sResult.Equals(PwViCommConst.GetMailLotteryResult.RESULT_NO_ATTACH)) {
				string sValue;
				using (MailTemplate oTemplate = new MailTemplate()) {
					oTemplate.GetValue(sessionWoman.site.siteCd,sessionWoman.userWoman.mailData[sMailDataSeq].mailTemplateNo,"MAIL_ATTACHED_OBJ_TYPE",out sValue);
				}

				if (sValue.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&partnermailseq={3}&lotteryreturn=1",ViCommConst.SCR_WOMAN_PIC_RETURN_MAILER_COMPLITE,sMailDataSeq,sMailSeq,sParterMailSeq)));
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&partnermailseq={3}&lotteryreturn=1",ViCommConst.SCR_WOMAN_MOVIE_RETURN_MAILER_COMPLITE,sMailDataSeq,sMailSeq,sParterMailSeq)));
				}
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sNextUrl));
			}
			
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}
