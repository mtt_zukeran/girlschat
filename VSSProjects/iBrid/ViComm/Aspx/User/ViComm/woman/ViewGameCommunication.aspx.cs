/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女性との記録
--	Progaram ID		: ViewGameCommunication
--
--  Creation Date	: 2011.11.15
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameCommunication:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_COMMUNICATION,ActiveForm);
	}
}