/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 自身の出演者画像コメント一覧
--	Progaram ID		: ListSelfCastPicComment
--  Creation Date	: 2013.12.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListSelfCastPicComment:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			string sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);

			if (string.IsNullOrEmpty(sPicSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_CAST_PIC_COMMENT,this.ActiveForm);
		}
	}
}
