/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o���҉摜�R�����g�폜
--	Progaram ID		: DeleteCastPicComment
--  Creation Date	: 2013.12.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteCastPicComment:MobileWomanPageBase {
	private string sCastPicCommentSeq;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		sCastPicCommentSeq = iBridUtil.GetStringValue(Request.QueryString["commentseq"]);

		if (string.IsNullOrEmpty(sCastPicCommentSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_CAST_PIC_COMMENT,this.ActiveForm)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			pnlConfirm.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult = string.Empty;

		using (CastPicComment oCastPicComment = new CastPicComment()) {
			oCastPicComment.DeleteCastPicComment(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sCastPicCommentSeq,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.DeleteCastPicCommentResult.RESULT_OK)) {
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}
	}
}
