﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾋﾞﾝｺﾞｶｰﾄﾞ詳細
--	Progaram ID		: ViewBingoCard
--
--  Creation Date	: 2011.07.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewBingoCard : MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		string sResult = this.sessionObj.bingoCard.GetBingoCard(
												this.sessionWoman.site.siteCd, 
												this.sessionWoman.userWoman.userSeq, 
												this.sessionWoman.userWoman.curCharNo, 
												ViCommConst.OPERATOR);

		if (ViCommConst.GetBingoCardResult.NOT_HAVE_CARD.Equals(sResult)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"EntryBingo.aspx"));
		} 
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["bingo"])) {
				this.RedirectResultPage();
			}

			switch (this.sessionObj.bingoCard.bingoStatus) {
				case ViCommConst.BingoStatus.BINGO:
					this.tagBingo.Visible = true;
					this.tagBingoDenied.Visible = false;
					break;
				case ViCommConst.BingoStatus.BINGO_DENIED:
					this.tagBingo.Visible = false;
					this.tagBingoDenied.Visible = true;
					break;
				case ViCommConst.BingoStatus.BINGO_APPLIED:
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"EntryBingo.aspx"));
					break;
				default:
					this.tagBingo.Visible = false;
					this.tagBingoDenied.Visible = false;
					break;
			}
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER,
					ActiveForm);
		}
	}

	private void RedirectResultPage() {
		string sBingoStatus;
		string sBingoElectionPoint;
		string sBingoElectionAmt;
		string sBingoCardNo;
		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			oMailDeBingo.DealJackpot(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR,
				out sBingoStatus,
				out sBingoElectionAmt,
				out sBingoElectionPoint,
				out sBingoCardNo);
		}

		switch (sBingoStatus) {
			case ViCommConst.BingoStatus.BINGO_DENIED: {
					// 先にビンゴした人がいる
					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("bingo",ViCommConst.FLAG_OFF_STR);
					oParameters.Add("bingocardno",sBingoCardNo);
					RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_BINGO_APPLICATION,oParameters);
				}
				break;
			case ViCommConst.BingoStatus.BINGO_COMPLETE: {
					// 申請完了
					string sToMailAddress = this.sessionWoman.site.infoEmailAddr;
					string sFromMailAddress = this.sessionWoman.userWoman.emailAddr;
					string sTitle = "【女性】メールdeビンゴ賞金獲得";
					string sDoc = string.Format("会員ID：{0}\r\nﾊﾝﾄﾞﾙﾈｰﾑ：{1}\r\n付与pt：{2}\r\n会員ﾒｰﾙｱﾄﾞﾚｽ：{3}\r\nｶｰﾄﾞ枚数：{4}枚目\r\n",
						this.sessionWoman.userWoman.loginId,this.sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNm,sBingoElectionPoint,this.sessionWoman.userWoman.emailAddr,sBingoCardNo);
					//メールアドレスの形式をチェック
					try {
						new System.Net.Mail.MailAddress(sFromMailAddress);
					}
					catch (FormatException) {
						sFromMailAddress = sToMailAddress;
					}

					this.SendMail(sFromMailAddress,sToMailAddress,sTitle,sDoc);

					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("bingo",ViCommConst.FLAG_ON_STR);
					oParameters.Add("bingocardno",sBingoCardNo);
					RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_BINGO_APPLICATION,oParameters);
				}
				break;
			case ViCommConst.BingoStatus.BINGO_APPLIED:
				// 申請済み
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"EntryBingo.aspx"));
				break;
			case ViCommConst.BingoStatus.BINGO:
				// ビンゴ達成（ポイント付与失敗）
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				break;
			default:
				break;
		}
	}
}
