/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE���ߎ��s
--	Progaram ID		: ViewGameTownClearMissionNG
--
--  Creation Date	: 2011.09.28
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_woman_ViewGameTownClearMissionNG:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
	
		string sStageSeq = iBridUtil.GetStringValue(this.Request.QueryString["stage_seq"]);
		string sTownSeq = iBridUtil.GetStringValue(this.Request.QueryString["town_seq"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sStageSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		rgxMatch = rgx.Match(sTownSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_TOWN,ActiveForm);
	}
}