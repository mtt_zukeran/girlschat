/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾊｸﾞされた一覧
--	Progaram ID		: ListGameCommunicationPartnerHug
--
--  Creation Date	: 2011.09.29
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameCommunicationPartnerHug:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		using (GameCharacter oGameCharacter = new GameCharacter()) {
			oGameCharacter.UpdateLastHugLogCheck(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.CurCharacter.userCharNo
			);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_COMMUNICATION_PARTNER_HUG,ActiveForm);
	}
}
