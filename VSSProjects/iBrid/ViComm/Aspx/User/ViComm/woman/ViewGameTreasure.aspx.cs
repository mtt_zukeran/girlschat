/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女性用お宝詳細
--	Progaram ID		: ViewGameTreasure
--
--  Creation Date	: 2011.10.7
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameTreasure:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sCastTreasureSeq = iBridUtil.GetStringValue(this.Request.Params["cast_treasure_seq"]);

		if (sCastTreasureSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.cmdNextLink_Click(sender,e);
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE,ActiveForm);
			}
		}
	}

	protected void cmdNextLink_Click(object sender,EventArgs e) {
		int iDoubleTrapFlag = 0;
		int iUsedTrapCount = 0;
		string sTreasureSeq = this.Request.Form["cast_treasure_seq"].ToString();
		int.TryParse(this.Request.Form["double_trap_flag"].ToString(),out iDoubleTrapFlag);
		int.TryParse(this.Request.Params["possessionItemNum"],out iUsedTrapCount);
		string sResult = string.Empty;

		using (GameItem oGameItem = new GameItem()) {
			oGameItem.UseTrap(
				this.sessionWoman.userWoman.CurCharacter.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.CurCharacter.userCharNo,
				ViCommConst.OPERATOR,
				sTreasureSeq,
				iUsedTrapCount,
				iDoubleTrapFlag,
				out sResult
			);

			if (sResult.Equals(PwViCommConst.GameUseTrapResult.RESULT_OK)) {
				this.RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("{0}?cast_treasure_seq={1}",this.Request.Params["NEXTLINK"].ToString(),sTreasureSeq)));
			} else {
				string sErrerCd = string.Empty;
				string sErrerMsg = string.Empty;
				switch (sResult) {
					case PwViCommConst.GameUseTrapResult.RESULT_NOT_HAVE_ITEM:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_NOT_HAVE_OTHER;
						sErrerMsg = "所持しているﾜﾅ";
						break;
					case PwViCommConst.GameUseTrapResult.RESULT_NOT_HAVE_TREASURE:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_NOT_HAVE_OTHER;
						sErrerMsg = "所持しているお宝";
						break;
					case PwViCommConst.GameUseTrapResult.RESULT_OVER_USE_TRAP:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_LIMIT_OVER;
						sErrerMsg = "仕掛けられるﾜﾅの合計は50回";
						break;
					default:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_NOT_HAVE_OTHER;
						sErrerMsg = "ﾜﾅ設定";
						break;
				}

				if (!string.IsNullOrEmpty(sErrerCd)) {
					lblErrorMsg.Text = string.Format(this.GetErrorMessage(sErrerCd),sErrerMsg);
				}
			}
		}
	}
}
