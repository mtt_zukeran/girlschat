﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール一括送信検索条件指定

--	Progaram ID		: FindBatchMailUser
--
--  Creation Date	: 2010.03.17
--  Creater			: i-Brid(Nakano)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_FindBatchMailUser:MobileWomanPageBase {
	private const string ONLINE_STATUS = "FindBatchMailUserOnlineStatus";
	private const string NEWMAN_FLG = "FindBatchMailUserNewManFlg";
	private const string HANDLE_NAME = "FindBatchMailUserHandleNm";
	private const string BIRTHDAY_FROM = "FindBatchMailUserBirthdayFrom";
	private const string BIRTHDAY_TO = "FindBatchMailUserBirthdayTo";
	private const string MAN_ITEM = "FindBatchMailUserManItem";
	private const string AGE = "FindBatchMailUserAge";
	private const string DEVICE = "FindBatchMailDevice";
	private const string MAIL_SEND_COUNT = "FindBatchMailUserMailSendCount";

	protected string sAreaAttrTypeSeq;

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		int iIdx = 0;

		if (!IsPostBack) {
			int iLimitCount = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castToManBatchMailLimit - sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].batchMailCount;

			if (iLimitCount <= 0) {
				lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_CAST_TO_MAN_MAIL_LIMIT_OVER,false);
			}

			using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
				DataSet dsAttrType = oUserManAttrType.GetList(sessionWoman.site.siteCd);
				SelectionList lstUserManItem;
				iBMobileLabel lblUserManItem;
				iBMobileTextBox txtUserManItem;
				Dictionary<string,string> manAttrList = (Dictionary<string,string>)Session[MAN_ITEM];

				foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
					if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",iIdx + 1));
						lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iIdx + 1));
						txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iIdx + 1));

						lblUserManItem.Text = drAttrType["MAN_ATTR_TYPE_FIND_NM"].ToString();

						if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
							lstUserManItem.Visible = false;

							// 入力値の復元を行います。

							if (manAttrList != null) {
								if (manAttrList.ContainsKey(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString())) {
									string val = manAttrList[drAttrType["MAN_ATTR_TYPE_SEQ"].ToString()];
									txtUserManItem.Text = val;
								}
							}
						} else {
							txtUserManItem.Visible = false;

							//lstUserManItem.Items.Add(new MobileListItem("指定なし","*"));
							if (string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {

								using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
									DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionWoman.site.siteCd,drAttrType["MAN_ATTR_TYPE_SEQ"].ToString());
									foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
										if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
											lstUserManItem.Items.Add(new MobileListItem(drAttrTypeValue["MAN_ATTR_NM"].ToString(),drAttrTypeValue["MAN_ATTR_SEQ"].ToString()));
										}
									}
								}
							} else {
								using (CodeDtl oCodeDtl = new CodeDtl()) {
									DataSet dsGroup = oCodeDtl.GetList(drAttrType["GROUPING_CATEGORY_CD"].ToString());
									foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
										lstUserManItem.Items.Add(new MobileListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
									}
								}
							}
							if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_RADIO)) {
								lstUserManItem.SelectType = ListSelectType.Radio;
								if (manAttrList == null) {
									lstUserManItem.SelectedIndex = 0;
								}
							}

							// 入力値の復元を行います。

							if (manAttrList != null) {
								if (manAttrList.ContainsKey(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString())) {
									string val = manAttrList[drAttrType["MAN_ATTR_TYPE_SEQ"].ToString()];
									foreach (MobileListItem item in lstUserManItem.Items) {
										if (item.Value == val) {
											item.Selected = true;
										}
									}
								}
							}
						}
						iIdx++;
					}
				}
			}
			for (int i = iIdx;i < ViCommConst.MAX_ATTR_COUNT;i++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i + 1));
				pnlUserManItem.Visible = false;
			}
			int iOnlineFlag;
			int iNewFaceFlag;
			int.TryParse(iBridUtil.GetStringValue(Session[ONLINE_STATUS]),out iOnlineFlag);
			int.TryParse(iBridUtil.GetStringValue(Session[NEWMAN_FLG]),out iNewFaceFlag);
			if (iOnlineFlag == this.GetSeekOnlineStatus()) {
				for (int iRdoManStatus = 0;iRdoManStatus < this.rdoManStatus.Items.Count;iRdoManStatus++) {
					this.rdoManStatus.Items[iRdoManStatus].Selected = false;
				}
				foreach(MobileListItem oItem in rdoManStatus.Items){
					if(oItem.Value.Equals("1")){
						oItem.Selected = true;
					}
				}
			} else if (iNewFaceFlag == 1) {
				for (int iRdoManStatus = 0;iRdoManStatus < this.rdoManStatus.Items.Count;iRdoManStatus++) {
					this.rdoManStatus.Items[iRdoManStatus].Selected = false;
				}
				foreach (MobileListItem oItem in rdoManStatus.Items) {
					if (oItem.Value.Equals("2")) {
						oItem.Selected = true;
					}
				}
			}

			txtHandelNm.Text = iBridUtil.GetStringValue(Session[HANDLE_NAME]);

			if (Session[MAIL_SEND_COUNT] != null) {
				string sMailSendCount = iBridUtil.GetStringValue(Session[MAIL_SEND_COUNT]);

				foreach (MobileListItem oItem in chkMailSendCount.Items) {
					oItem.Selected = false;
				}
				foreach (MobileListItem oItem in chkMailSendCount.Items) {
					if (oItem.Value.Equals(sMailSendCount)) {
						oItem.Selected = true;
					}
				}
			}

			string sBirthdayFrom = iBridUtil.GetStringValue(Session[BIRTHDAY_FROM]);
			string sBirthdayTo = iBridUtil.GetStringValue(Session[BIRTHDAY_TO]);
			string sAge = iBridUtil.GetStringValue(Session[AGE]);

			SysPrograms.SetupFromToDay(null,lstBirthdayFromMM,lstBirthdayFromDD,null,lstBirthdayToMM,lstBirthdayToDD,true);
			if (!sBirthdayFrom.Equals(string.Empty)) {
				string[] sBirthday = sBirthdayFrom.Split('/');
				foreach (MobileListItem item in lstBirthdayFromMM.Items) {
					if (item.Value == sBirthday[0]) {
						item.Selected = true;
					}
				}
				foreach (MobileListItem item in lstBirthdayFromDD.Items) {
					if (item.Value == sBirthday[1]) {
						item.Selected = true;
					}
				}
			}
			if (!sBirthdayTo.Equals(string.Empty)) {
				string[] sBirthday = sBirthdayTo.Split('/');
				foreach (MobileListItem item in lstBirthdayToMM.Items) {
					if (item.Value == sBirthday[0]) {
						item.Selected = true;
					}
				}
				foreach (MobileListItem item in lstBirthdayToDD.Items) {
					if (item.Value == sBirthday[1]) {
						item.Selected = true;
					}
				}
			}
			if (!sAge.Equals(string.Empty)) {
				foreach (MobileListItem item in lstAge.Items) {
					if (item.Value == sAge) {
						item.Selected = true;
					}
				}
			}
			for (int i = 0;i < this.rdoManStatus.Items.Count;i++) {
				this.rdoManStatus.Items[i].Text = DisplayWordUtil.Replace(this.rdoManStatus.Items[i].Text);
			}

			lblDevice.Visible = true;
			lstDevice.Visible = true;
			tagLinesDevice.Visible = true;
			string sDevice = iBridUtil.GetStringValue(Session[DEVICE]);
			if (!string.IsNullOrEmpty(sDevice)) {
				foreach (MobileListItem oItem in lstDevice.Items) {
					if (oItem.Value.Equals(sDevice)) {
						oItem.Selected = true;
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = "";
		string sHandleNm = txtHandelNm.Text;
		string sAgeFrom = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[0]);
		string sAgeTo = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[1]);
		string sBirthdayFrom = "";
		string sBirthdayTo = "";
		string sMailSendCount = "";

		if (sessionWoman.userWoman.CurCharacter.castToManBatchMailNaFlag == ViCommConst.FLAG_ON) {
			lblErrorMessage.Text = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_CAST_TO_MAN_BATCH_MAIL_NO_AUTH,false);
			return;
		}

		if (chkMailSendCount.SelectedIndex != -1) {
			sMailSendCount = chkMailSendCount.Items[chkMailSendCount.SelectedIndex].Value;
		}

		// 検索条件をセッションに保存します。 
		sessionWoman.BatchMailSearchConditions = new Dictionary<string,string>();

		if (rdoManStatus.SelectedIndex >= 0) {
			sessionWoman.BatchMailSearchConditions.Add("待機状態",rdoManStatus.Items[rdoManStatus.SelectedIndex].Text);
		}
		if (!string.IsNullOrEmpty(sMailSendCount)) {
			sessionWoman.BatchMailSearchConditions.Add("送信回数",chkMailSendCount.Items[chkMailSendCount.SelectedIndex].Text);
		}
		if (!string.IsNullOrEmpty(sHandleNm)) {
			sessionWoman.BatchMailSearchConditions.Add("名前",sHandleNm);
		}
		if (!string.IsNullOrEmpty(sAgeFrom) || !string.IsNullOrEmpty(sAgeTo)) {
			sessionWoman.BatchMailSearchConditions.Add("年齢",lstAge.Selection.Text);
		}

		// 画面の項目を取得する 
		int iOnlineStatus = 0;
		int iNewFaceFlag = 0;

		for (int iRdoManStatus = 0;iRdoManStatus < this.rdoManStatus.Items.Count;iRdoManStatus++) {
			if (this.rdoManStatus.Items[iRdoManStatus].Selected) {
				if (this.rdoManStatus.Items[iRdoManStatus].Value.ToString().Equals("1")) {
					iOnlineStatus = this.GetSeekOnlineStatus();
				} else if (rdoManStatus.Items[iRdoManStatus].Value.ToString().Equals("2")) {
					iNewFaceFlag = ViCommConst.FLAG_ON;
				}
			}
		}

		string from = "";
		string to = "";

		if (!lstBirthdayFromMM.Selection.Value.Equals("")) {
			sBirthdayFrom = lstBirthdayFromMM.Selection.Value + "/";
			if (!lstBirthdayFromDD.Selection.Value.Equals("")) {
				sBirthdayFrom += lstBirthdayFromDD.Selection.Value;
			} else {
				sBirthdayFrom += "01";
			}
			from = string.Format("{0}から",sBirthdayFrom);
		}
		if (!lstBirthdayToMM.Selection.Value.Equals("")) {
			sBirthdayTo = lstBirthdayToMM.Selection.Value + "/";
			if (!lstBirthdayToDD.Selection.Value.Equals("")) {
				sBirthdayTo += lstBirthdayToDD.Selection.Value;
			} else {
				sBirthdayTo += "31";
			}
			to = string.Format("{0}まで",sBirthdayTo);
		}

		string sDevice = string.Empty;
		if (lstDevice.Visible) {
			sDevice = lstDevice.Selection.Value;
			sessionWoman.batchMailSeekDevice = sDevice;
		}

		// 検索条件をセッションに保存します。 
		if (!string.IsNullOrEmpty(from) || !string.IsNullOrEmpty(to)) {
			sessionWoman.BatchMailSearchConditions.Add("誕生日",string.Format("{0}{1}",from,to));
		}

		SelectionList lstUserManItem;
		iBMobileTextBox txtUserManItem;
		Panel pnlUserManItem;
		List<string> valueList = new List<string>();
		List<string> typeList = new List<string>();
		List<string> groupList = new List<string>();
		List<string> likeList = new List<string>();
		List<string> notEqualList = new List<string>();
		Dictionary<string,string> manAttrList = new Dictionary<string,string>();
		int i = 1;
		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet dsAttrType = oUserManAttrType.GetList(sessionWoman.site.siteCd);
			foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
				if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
					pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i));

					if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i));
						if ((pnlUserManItem != null) && (txtUserManItem != null) && (pnlUserManItem.Visible)) {
							valueList.Add(txtUserManItem.Text);
							typeList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString());
							groupList.Add("0");
							likeList.Add("1");
							notEqualList.Add("0");

							// 検索条件をセッションに保存します。 
							if (!string.IsNullOrEmpty(txtUserManItem.Text)) {
								sessionWoman.BatchMailSearchConditions.Add(drAttrType["MAN_ATTR_TYPE_NM"].ToString(),txtUserManItem.Text);
							}
							// 入力値復元用の値保存 
							manAttrList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),txtUserManItem.Text);
						}
					} else {
						lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i));
						string sGroupingFlag = "";
						if (!string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
							sGroupingFlag = "1";
						}
						if ((pnlUserManItem != null) && (lstUserManItem != null) && (pnlUserManItem.Visible)) {
							valueList.Add(lstUserManItem.Items[lstUserManItem.SelectedIndex].Value);
							typeList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString());
							groupList.Add(sGroupingFlag);
							likeList.Add("0");
							notEqualList.Add("0");

							// 検索条件をセッションに保存します。 
							if (!lstUserManItem.Selection.Text.Equals(GetListEmptyItemText())) {
								sessionWoman.BatchMailSearchConditions.Add(drAttrType["MAN_ATTR_TYPE_NM"].ToString(),lstUserManItem.Selection.Text);
							}
							// 入力値復元用の値保存 
							manAttrList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),lstUserManItem.Items[lstUserManItem.SelectedIndex].Value);
						}
					}
					i++;
				}
			}
		}

		this.AppendSearchCondtion(valueList,typeList,groupList,likeList,notEqualList);

		int iLimitCount = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castToManBatchMailLimit - sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].batchMailCount;

		if (iLimitCount <= 0) {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_CAST_TO_MAN_MAIL_LIMIT_OVER,false);
			return;
		}

		decimal iTotalCount = 0;

		// 選択項目からデータセット検索 
		DataSet ds = sessionWoman.SearchManListBatch(iOnlineStatus,iNewFaceFlag,sHandleNm,sAgeFrom,sAgeTo,sBirthdayFrom,sBirthdayTo,valueList,typeList,groupList,likeList,notEqualList,iLimitCount,sDevice,sMailSendCount,out iTotalCount);

		// 0件の場合、エラー 
		if (iTotalCount == 0) {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MAN_NOT_FOUND,false);
			return;
		}

		// 選択した値をセッションにいれておきます。 
		Session[ONLINE_STATUS] = iOnlineStatus;
		Session[NEWMAN_FLG] = iNewFaceFlag;
		Session[HANDLE_NAME] = txtHandelNm.Text;
		Session[BIRTHDAY_FROM] = sBirthdayFrom;
		Session[BIRTHDAY_TO] = sBirthdayTo;
		Session[AGE] = lstAge.Selection.Value;
		Session[MAN_ITEM] = manAttrList;
		Session[DEVICE] = sDevice;
		Session[MAIL_SEND_COUNT] = sMailSendCount;

		if (lblErrorMessage.Text.Equals("")) {
			// USER_SEQをセッションに確保 
			List<string> userSeqList = new List<string>();
			foreach (DataRow dr in ds.Tables[0].Rows) {
				userSeqList.Add(dr["USER_SEQ"].ToString());
			}
			sessionWoman.BatchMailUserList = userSeqList;

			// メール送信画面に移動 
			string sUrl = string.Format("TxMail.aspx?site={0}&batch=1",sessionWoman.site.siteCd);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sUrl));
		}
	}

	protected virtual void AppendSearchCondtion(List<string> valueList,List<string> typeList,List<string> groupList,List<string> likeList,List<string> notEqualList) {
		// 何もしない。aspxで処理を追記する場合に使用 
	}

	protected virtual int GetSeekOnlineStatus() {
		return ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;	//2 LOGINED,WAITING,TALKING;
	}

	protected virtual string GetListEmptyItemText() {
		return "指定なし";
	}
}
