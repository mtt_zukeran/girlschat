/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・仲間申請取り下げ
--	Progaram ID		: RegistGameCancelApplicationFellow
--
--  Creation Date	: 2011.09.28
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_RegistGameCancelApplicationFellow:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		} else {
			if (this.CheckOnlineStatusGame(sPartnerUserSeq,sPartnerUserCharNo)) {
				if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo);
				}
			} else {
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo) {
		using (GameFellow oGameFellow = new GameFellow()) {
			string sResult = oGameFellow.CancelApplicationFellow(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.CurCharacter.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo
			);

			IDictionary<string,string> oParameters = new Dictionary<string,string>();

			oParameters.Add("result",sResult);
			oParameters.Add("partner_user_seq",pPartnerUserSeq);
			oParameters.Add("partner_user_char_no",pPartnerUserCharNo);

			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_CANCEL_APPLY_FELLOW_RESULT,oParameters);
		}
	}
}