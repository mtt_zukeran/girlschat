﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FindBatchMailUser.aspx.cs" Inherits="ViComm_woman_FindBatchMailUser" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server"><cc1:iBMobileLiteralText ID="tag01" runat="server" /> 
	   $PGM_HTML01; 
	   <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
	   <mobile:SelectionList ID="rdoManStatus" Runat="server" SelectType="Radio" Alignment="Left">
		   <Item Text="全ての男性" Value="0" Selected="true" />
		   <Item Text="オンラインの男性のみ" Value="1" Selected="False" />
		   <Item Text="新人の男性のみ" Value="2" Selected="False" />
	   </mobile:SelectionList>
	   $PGM_HTML02;

		<mobile:SelectionList ID="chkMailSendCount" Runat="server" SelectType="CheckBox" Alignment="Left">
			<Item Text="未送信の会員のみ" Value="0" Selected="true" />
		</mobile:SelectionList>

		<cc1:iBMobileLiteralText ID="tagLinesOnline" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLabel ID="lblHandelNm" runat="server" BreakAfter="true">名前</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLinesHandelNm" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLabel ID="lblAge" runat="server" BreakAfter="true">年齢</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstAge" Runat="server" SelectType="DropDown">
			<Item Value=":" Text="指定なし">
			</Item>
			<Item Value="18:20" Text="18～20歳">
			</Item>
			<Item Value="21:24" Text="21～24歳">
			</Item>
			<Item Value="25:29" Text="25～29歳">
			</Item>
			<Item Value="30:34" Text="30～34歳">
			</Item>
			<Item Value="35:39" Text="35～39歳">
			</Item>
			<Item Value="40:" Text="40歳以上">
			</Item>
		</mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagLinesAge" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">誕生日</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayFromMM" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel1" runat="server" BreakAfter="false">月</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayFromDD" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel2" runat="server" BreakAfter="true">日から</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayToMM" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel3" runat="server" BreakAfter="false">月</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayToDD" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel4" runat="server" BreakAfter="false">日まで</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagLinesBirthday" runat="server" Text="$PGM_HTML03;" />
		<mobile:Panel ID="pnlUserManItem1" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem1" runat="server" BreakAfter="true">userManItemm01</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem1" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem1" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem2" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem2" runat="server" BreakAfter="true">userManItemm02</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem2" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem2" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines2" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem3" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem3" runat="server" BreakAfter="true">userManItemm03</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem3" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem3" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines3" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem4" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem4" runat="server" BreakAfter="true">userManItemm04</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem4" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem4" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines4" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem5" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem5" runat="server" BreakAfter="true">userManItemm05</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem5" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem5" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines5" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem6" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem6" runat="server" BreakAfter="true">userManItemm06</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem6" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem6" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines6" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem7" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem7" runat="server" BreakAfter="true">userManItemm07</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem7" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem7" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines7" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem8" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem8" runat="server" BreakAfter="true">userManItemm08</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem8" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem8" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines8" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem9" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem9" runat="server" BreakAfter="true">userManItemm09</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem9" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem9" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines9" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem10" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem10" runat="server" BreakAfter="true">userManItemm10</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem10" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem10" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem11" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem11" runat="server" BreakAfter="true">userManItemm11</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem11" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem11" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem12" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem12" runat="server" BreakAfter="true">userManItemm12</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem12" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem12" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem13" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem13" runat="server" BreakAfter="true">userManItemm13</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem13" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem13" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem14" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem14" runat="server" BreakAfter="true">userManItemm14</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem14" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem14" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem15" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem15" runat="server" BreakAfter="true">userManItemm15</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem15" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem15" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem16" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem16" runat="server" BreakAfter="true">userManItemm16</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem16" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem16" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem17" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem17" runat="server" BreakAfter="true">userManItemm17</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem17" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem17" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem18" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem18" runat="server" BreakAfter="true">userManItemm18</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem18" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem18" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem19" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem19" runat="server" BreakAfter="true">userManItemm19</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem19" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem19" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem20" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem20" runat="server" BreakAfter="true">userManItemm20</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem20" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem20" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<cc1:iBMobileLabel ID="lblDevice" runat="server" BreakAfter="true">携帯種別</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstDevice" Runat="server" SelectType="DropDown" Alignment="Left">
			<Item Text="--問わない--" Value="" Selected="True" />
			<Item Text="携帯のみ" Value="1" />
			<Item Text="スマホのみ" Value="2" />
		</mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagLinesDevice" runat="server" Text="$PGM_HTML03;" />
		<br />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
