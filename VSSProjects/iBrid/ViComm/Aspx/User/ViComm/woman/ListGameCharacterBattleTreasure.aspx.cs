/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ相手一覧(お宝所持)
--	Progaram ID		: ListGameCharacterBattleTreasure
--
--  Creation Date	: 2011.09.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_woman_ListGameCharacterBattleTreasure:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["cast_treasure_seq"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_TREASURE_LIST,ActiveForm);
		}
	}
}
