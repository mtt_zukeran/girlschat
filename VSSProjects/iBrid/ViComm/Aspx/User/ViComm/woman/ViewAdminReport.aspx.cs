/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 管理者連絡詳細表示
--	Progaram ID		: ViewAdminReport
--
--  Creation Date	: 2009.07.23
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewAdminReport:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {


		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_ADMIN_REPORT,ActiveForm)) {
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListAdminReport.aspx"));
			}
		}
	}
}
