/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: リッチーノ一覧
--	Progaram ID		: ListRichinoUser
--
--  Creation Date	: 2011.04.18
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListRichinoUser:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		CheckEnabledRichino();
		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_RICHINO_USER,
					ActiveForm);
			CreateComboBox();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string selectedValue = lstRankSelect.Items[lstRankSelect.SelectedIndex].Value;
		UrlBuilder oUrlBuilder;
		ListSearchAction(true,out oUrlBuilder);
		oUrlBuilder.AddParameter("richinorank",selectedValue);

		oUrlBuilder.RemoveParameter("notxmaildays");
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Form["notxmaildays"]))) {
			oUrlBuilder.AddParameter("notxmaildays",iBridUtil.GetStringValue(this.Request.Form["notxmaildays"]));
		}

		RedirectToMobilePage(oUrlBuilder.ToString(true));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&richinorank=[A-Z]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}

	private void CreateComboBox() {
		using (RichinoRank oRichinoRank = new RichinoRank()) {
			DataSet ds = oRichinoRank.GetRichinoRankList(sessionWoman.site.siteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstRankSelect.Items.Add(new MobileListItem(iBridUtil.GetStringValue(dr["RICHINO_RANK_NM"]),iBridUtil.GetStringValue(dr["RICHINO_RANK"])));
			}
		}

		string sSelectRichinoRank = iBridUtil.GetStringValue(Request.QueryString["richinorank"]);
		if (!sSelectRichinoRank.Equals(string.Empty)) {
			foreach (MobileListItem item in lstRankSelect.Items) {
				if (item.Value == sSelectRichinoRank) {
					lstRankSelect.SelectedIndex = item.Index;
				}
			}
		}
	}

	private void CheckEnabledRichino() {
		if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.enabledRichinoFlag == ViCommConst.FLAG_OFF) {
			RedirectToDisplayDoc(ViCommConst.SCR_CANT_USE_RICHINO);
		}
	}
}
