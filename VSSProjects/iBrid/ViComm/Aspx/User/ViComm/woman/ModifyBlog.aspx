<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyBlog.aspx.cs" Inherits="ViComm_woman_ModifyBlog" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile"	%>
<%@ Register TagPrefix="ibrid"	Namespace="MobileLib"						Assembly="MobileLib"			%>
<%@ Register TagPrefix="ibrid"	TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>    
		$PGM_HTML02;
		<ibrid:iBMobileTextBox ID="txtBlogTitle" runat="server"  BreakAfter="true" Size="20" MaxLength="20" ></ibrid:iBMobileTextBox>
		$PGM_HTML03;
		<ibrid:TextArea ID="txtBlogDoc" runat="server" BreakBefore="false" BrerakAfter="true" Columns="20" Rows="6">
		</ibrid:TextArea>		
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
