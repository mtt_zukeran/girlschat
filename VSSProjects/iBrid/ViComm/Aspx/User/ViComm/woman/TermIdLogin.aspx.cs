/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 固体識別番号ログイン
--	Progaram ID		: TermIdLogin
--
--  Creation Date	: 2010.04.28
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_TermIdLogin:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE) || sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				string sLoginId = string.Empty;
				string sLoginPassword = string.Empty;
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCookie"]).Equals(ViCommConst.FLAG_ON_STR)) {
					if (Request.Cookies["maqia"] != null) {
						using (User oUser = new User()) {
							if (oUser.GetOne(iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiauid"]),iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiasex"]))) {
									sLoginId = oUser.loginId;
									sLoginPassword = oUser.loginPassword;
							}
						}
					}
					if (string.IsNullOrEmpty(sLoginId) || string.IsNullOrEmpty(sLoginPassword)) {
						if (IsAvailableService(ViCommConst.RELEASE_ENABLE_EASYLOGIN_ERROR,2)) {
							RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_SMART_PHONE_EASYLOGIN_NG);
						}
					}
				}
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("LoginUser.aspx?loginid={0}&password={1}&goto=SelectSite.aspx",sLoginId,sLoginPassword)));
			} else {
				string sId = Mobile.GetiModeId(sessionWoman.carrier,Request);
				string sUtn = Mobile.GetUtn(sessionWoman.carrier,Request);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("LoginUser.aspx?utn={0}&id={1}",sUtn,sId)));
			}
		}
	}
}
