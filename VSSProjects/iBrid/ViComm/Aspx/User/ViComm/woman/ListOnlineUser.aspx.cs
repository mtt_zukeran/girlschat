/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: オンライン一覧
--	Progaram ID		: ListOnlineUser
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListOnlineUser:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			// 待機+会話中
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_ONLINE,ActiveForm);
		}
	}
}
