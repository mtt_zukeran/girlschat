/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: トップメニュー
--	Progaram ID		: UserTop
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_UserTop:MobileWomanPageBase {

	protected override bool NeedAccessLog {
		get {
			return true;
		}
	}

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		using (User oUser = new User()) {
			oUser.UpdateMobileInfo(sessionWoman.userWoman.userSeq,sessionWoman.carrier,sessionWoman.mobileUserAgent);
		}

		if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE) || (sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionWoman.adminFlg)) {
			HttpCookie cookie = new HttpCookie("maqia");
			cookie.Values["maqiauid"] = sessionWoman.userWoman.userSeq;
			cookie.Values["maqiapw"] = sessionWoman.userWoman.loginPassword;
			cookie.Values["maqiasex"] = ViCommConst.OPERATOR;
			cookie.Expires = DateTime.Now.AddDays(90);
			cookie.Domain = sessionWoman.site.hostNm;
			Response.Cookies.Add(cookie);
		}

		if (!IsPostBack) {
			sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);

			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].registIncompleteFlag == 1 || sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].profileResettingFlag == 1) {
				string[] sRequest = Request.Path.Split('/');
				if (!sRequest[sRequest.Length - 1].Equals("ModifyUserProfile.aspx")) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ModifyUserProfile.aspx?site=" + sessionWoman.site.siteCd + "&charno=" + sessionWoman.userWoman.curCharNo));
				}
			}
			CheckBulkImpUser();

			// ﾛｸﾞｲﾝﾛｸﾞを書き込む
			if (!sessionWoman.alreadyWrittenLoginLog) {
				this.AccessPage(ViCommConst.ProgramAction.LOGIN);
				sessionWoman.alreadyWrittenLoginLog = true;
			}

			if (!sessionWoman.userWoman.loginCharacter.ContainsKey(sessionWoman.userWoman.curKey)) {
				sessionWoman.userWoman.loginCharacter.Add(sessionWoman.userWoman.curKey,false);
			}
			if (!sessionWoman.userWoman.loginCharacter[sessionWoman.userWoman.curKey]) {
				sessionWoman.userWoman.SetLastLoginDate(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.currentIModeId);
				sessionWoman.userWoman.loginCharacter[sessionWoman.userWoman.curKey] = true;
			}
			sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
			//女性は各ｻｲﾄのUserTopにてﾄｯﾌﾟﾍﾟｰｼﾞｶｳﾝﾄ数のｶｳﾝﾄｱｯﾌﾟをする。  
			using (Access oAccess = new Access()) {
				oAccess.AccessTopPage(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].adCd,
						ViCommConst.OPERATOR
				);
			}

			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCastUserTopDataSet"]).Equals("1")) {
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_USER_TOP,ActiveForm);
			}

			// 足あとの未確認件数を初期化
			sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.unconfirmMarkingCount = string.Empty;
		}
	}

	private void CheckBulkImpUser() {
		if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].bulkImpNonUseFlag == 1) {
			if (sessionWoman.site.impCastFirstLoginAct.Equals(ViCommConst.ImpFirstLoginActType.USER_TOP)) {
				sessionWoman.ActivateImpUser(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.carrier,sessionWoman.mobileUserAgent,sessionWoman.currentIModeId);
			} else if (sessionWoman.site.impCastFirstLoginAct.Equals(ViCommConst.ImpFirstLoginActType.MODIFY_HANDLE_NM)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ModifyUserHandleNm.aspx"));
			} else if (sessionWoman.site.impManFirstLoginAct.Equals(ViCommConst.ImpFirstLoginActType.MODIFY_PROFILE)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ModifyUserProfile.aspx"));
			}
		}
	}
}
