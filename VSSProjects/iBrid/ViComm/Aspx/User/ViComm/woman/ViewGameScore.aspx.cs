/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｹﾞｰﾑ結果
--	Progaram ID		: ViewGameScore
--
--  Creation Date	: 2011.03.29
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewGameScore : MobileWomanPageBase {
	protected const string PARAM_NM_GAME_TYPE = "game_type";

	/// <summary>基底クラスでﾛｸﾞｲﾝのﾁｪｯｸを行う</summary>
	protected override bool NeedLogin { get { return true; } }

	protected void Page_Load(object sender, EventArgs e) {

		if (!IsPostBack) {
			string sGameType = iBridUtil.GetStringValue(Request.QueryString[PARAM_NM_GAME_TYPE]);
			string sCurrentAspxName = string.Format("ViewGameScore{0}.aspx", sGameType);

			Response.Filter = sessionWoman.InitScreen(Response.Filter, frmMain, Request, sCurrentAspxName, ViewState);
			sessionWoman.ControlList(Request, ViCommConst.INQUIRY_EXTENSION_GAME, ActiveForm);
		}
	}
}
