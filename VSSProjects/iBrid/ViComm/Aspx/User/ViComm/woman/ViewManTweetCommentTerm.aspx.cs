/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやきコメントランキングTOP
--	Progaram ID		: ViewManTweetCommentTerm
--
--  Creation Date	: 2013.03.27
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewManTweetCommentTerm:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_TWEET_COMMENT_TERM,this.ActiveForm);
	}
}
