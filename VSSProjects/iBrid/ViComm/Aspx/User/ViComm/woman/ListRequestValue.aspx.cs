/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい評価一覧
--	Progaram ID		: ListRequestValue
--
--  Creation Date	: 2012.07.03
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListRequestValue:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sSearchValue = string.Empty;

			if (iBridUtil.GetStringValue(this.Request.QueryString["cmt_flg"]).Equals(ViCommConst.FLAG_ON_STR)) {
				switch (iBridUtil.GetStringValue(this.Request.QueryString["val_no"])) {
					case "":
						sSearchValue = "1";//ｺﾒﾝﾄあり
						break;
					case PwViCommConst.RequestValue.GOOD:
						sSearchValue = "2";//良いｺﾒﾝﾄのみ
						break;
					case PwViCommConst.RequestValue.BAD:
						sSearchValue = "3";//悪いｺﾒﾝﾄのみ
						break;
					default:
						sSearchValue = "1";
						break;
				}
			}

			foreach (MobileListItem item in lstSearch.Items) {
				if (item.Value.Equals(sSearchValue)) {
					item.Selected = true;
				}
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_REQUEST_VALUE,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		UrlBuilder oUrlBuilder = new UrlBuilder("ListRequestValue.aspx");
		oUrlBuilder.Parameters.Add("req_seq",iBridUtil.GetStringValue(this.Request.QueryString["req_seq"]));

		switch (lstSearch.Items[lstSearch.SelectedIndex].Value) {
			case "1":
				oUrlBuilder.Parameters.Add("cmt_flg",ViCommConst.FLAG_ON_STR);
				break;
			case "2":
				oUrlBuilder.Parameters.Add("cmt_flg",ViCommConst.FLAG_ON_STR);
				oUrlBuilder.Parameters.Add("val_no",PwViCommConst.RequestValue.GOOD);
				break;
			case "3":
				oUrlBuilder.Parameters.Add("cmt_flg",ViCommConst.FLAG_ON_STR);
				oUrlBuilder.Parameters.Add("val_no",PwViCommConst.RequestValue.BAD);
				break;
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrlBuilder.ToString()));
	}
}
