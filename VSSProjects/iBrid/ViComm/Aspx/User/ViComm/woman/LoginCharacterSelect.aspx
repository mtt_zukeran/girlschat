<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginCharacterSelect.aspx.cs" Inherits="ViComm_woman_LoginCharacterSelect" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:SelectionList ID="lstWaitTime" Runat="server" Visible="False"></mobile:SelectionList>
		$PGM_HTML02;
		
		<mobile:Panel ID="pnlLoginCharacter0" Runat="server">
			<mobile:SelectionList ID="chkLogin0" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo0" runat="server" BreakAfter="False">CharNo0</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm0" runat="server" BreakAfter="True">HandleNm0</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter1" Runat="server">
			<mobile:SelectionList ID="chkLogin1" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo1" runat="server" BreakAfter="False">CharNo1</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm1" runat="server" BreakAfter="True">HandleNm1</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter2" Runat="server">
			<mobile:SelectionList ID="chkLogin2" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo2" runat="server" BreakAfter="False">CharNo2</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm2" runat="server" BreakAfter="True">HandleNm2</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter3" Runat="server">
			<mobile:SelectionList ID="chkLogin3" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo3" runat="server" BreakAfter="False">CharNo3</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm3" runat="server" BreakAfter="True">HandleNm3</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter4" Runat="server">
			<mobile:SelectionList ID="chkLogin4" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo4" runat="server" BreakAfter="False">CharNo4</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm4" runat="server" BreakAfter="True">HandleNm4</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter5" Runat="server">
			<mobile:SelectionList ID="chkLogin5" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo5" runat="server" BreakAfter="False">CharNo5</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm5" runat="server" BreakAfter="True">HandleNm5</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter6" Runat="server">
			<mobile:SelectionList ID="chkLogin6" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo6" runat="server" BreakAfter="False">CharNo6</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm6" runat="server" BreakAfter="True">HandleNm6</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter7" Runat="server">
			<mobile:SelectionList ID="chkLogin7" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo7" runat="server" BreakAfter="False">CharNo7</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm7" runat="server" BreakAfter="True">HandleNm7</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter8" Runat="server">
			<mobile:SelectionList ID="chkLogin8" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo8" runat="server" BreakAfter="False">CharNo8</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm8" runat="server" BreakAfter="True">HandleNm8</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter9" Runat="server">
			<mobile:SelectionList ID="chkLogin9" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo9" runat="server" BreakAfter="False">CharNo9</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm9" runat="server" BreakAfter="True">HandleNm9</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter10" Runat="server">
			<mobile:SelectionList ID="chkLogin10" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo10" runat="server" BreakAfter="False">CharNo10</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm10" runat="server" BreakAfter="True">HandleNm10</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter11" Runat="server">
			<mobile:SelectionList ID="chkLogin11" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo11" runat="server" BreakAfter="False">CharNo11</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm11" runat="server" BreakAfter="True">HandleNm11</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter12" Runat="server">
			<mobile:SelectionList ID="chkLogin12" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo12" runat="server" BreakAfter="False">CharNo12</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm12" runat="server" BreakAfter="True">HandleNm12</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter13" Runat="server">
			<mobile:SelectionList ID="chkLogin13" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo13" runat="server" BreakAfter="False">CharNo13</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm13" runat="server" BreakAfter="True">HandleNm13</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter14" Runat="server">
			<mobile:SelectionList ID="chkLogin14" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo14" runat="server" BreakAfter="False">CharNo14</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm14" runat="server" BreakAfter="True">HandleNm14</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter15" Runat="server">
			<mobile:SelectionList ID="chkLogin15" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo15" runat="server" BreakAfter="False">CharNo15</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm15" runat="server" BreakAfter="True">HandleNm15</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter16" Runat="server">
			<mobile:SelectionList ID="chkLogin16" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo16" runat="server" BreakAfter="False">CharNo16</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm16" runat="server" BreakAfter="True">HandleNm16</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter17" Runat="server">
			<mobile:SelectionList ID="chkLogin17" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo17" runat="server" BreakAfter="False">CharNo17</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm17" runat="server" BreakAfter="True">HandleNm17</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter18" Runat="server">
			<mobile:SelectionList ID="chkLogin18" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo18" runat="server" BreakAfter="False">CharNo18</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm18" runat="server" BreakAfter="True">HandleNm18</cc1:iBMobileLabel>
		</mobile:Panel>
		<mobile:Panel ID="pnlLoginCharacter19" Runat="server">
			<mobile:SelectionList ID="chkLogin19" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="" Value="1" Selected="false" />
			</mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblUserCharNo19" runat="server" BreakAfter="False">CharNo19</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblHandleNm19" runat="server" BreakAfter="True">HandleNm19</cc1:iBMobileLabel>
		</mobile:Panel>
		<br />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
