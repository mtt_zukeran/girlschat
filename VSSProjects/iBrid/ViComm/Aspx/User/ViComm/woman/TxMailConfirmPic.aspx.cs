/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信確認
--	Progaram ID		: TxMailConfirmPic
--
--  Creation Date	: 2015.10.14
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_TxMailConfirmPic:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["data"]))) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
		
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["picseq"]))) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ListMailPic.aspx?data={0}",iBridUtil.GetStringValue(Request.QueryString["data"]))));
		}

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_CAST_MAIL_PIC_STOCK,ActiveForm)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ListMailPic.aspx?data={0}",iBridUtil.GetStringValue(Request.QueryString["data"]))));
			}
			
			ParseHTML oParse = sessionWoman.parseContainer;
			ViewState["MAIL_DATA_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["data"]);
			
			if (iBridUtil.GetStringValue(Request.QueryString["send"]).Equals(ViCommConst.FLAG_ON_STR)) {
				cmdSubmit_Click(sender,e);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		ParseHTML oParse = sessionWoman.parseContainer;
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		string sRxUserSeq = string.Empty;
		string sRxUserNm = string.Empty;
		string sObjTempId = string.Empty;
		string sMailSeq = string.Empty;
		bool bOk = true;
		
		if (bOk == true) {
			sRxUserSeq = sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq;
			sRxUserNm = sessionWoman.userWoman.mailData[sMailDataSeq].rxUserNm;
			string sMailTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle) : sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle));
			string sMailDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc) : sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc));
			string sQuereyString = sessionWoman.userWoman.mailData[sMailDataSeq].queryString;
			string sReturnMailSeq = sessionWoman.userWoman.mailData[sMailDataSeq].returnMailSeq;
			bool bIsReturnMail = sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail;
			string sMailTemplate = sessionWoman.userWoman.mailData[sMailDataSeq].mailTemplateNo;
			string sMailType = ViCommConst.ATTACH_PIC_INDEX.ToString();
			string sPicSeq = string.Empty;
			
			sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
			
			using (MailBox oMailBox = new MailBox()) {
				sMailSeq = oMailBox.TxCastToManMail(
					sessionWoman.site.siteCd,
					sMailTemplate,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					new string[] { sRxUserSeq },
					0,
					sMailTitle,
					sMailDoc,
					sMailType,
					sPicSeq,
					string.Empty,
					ViCommConst.FLAG_OFF,
					null,
					null,
					out sObjTempId
				);
			}

			DeleteDraftMail(sMailDataSeq);

			using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
				oMailRequestLog.UpdateMailRequestLog(sessionWoman.site.siteCd,sRxUserSeq,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sMailSeq);
			}

			IDictionary<string,string> oParam = new Dictionary<string,string>();

			oParam.Add("mseq",sMailSeq);
			
			if (sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackLoginFlag == ViCommConst.FLAG_ON) {
				string sMailAttackLoginTxLogResult;
				using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
					sMailAttackLoginTxLogResult = oMailAttackLogin.RegistMailAttackLoginTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sRxUserSeq,ViCommConst.MAIN_CHAR_NO,sMailSeq,ViCommConst.FLAG_OFF);
				}

				oParam.Add("attackloginresult",sMailAttackLoginTxLogResult);
			}

			if (sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackNewFlag == ViCommConst.FLAG_ON) {
				string sMailAttackNewTxLogResult;
				using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
					sMailAttackNewTxLogResult = oMailAttackNew.RegistMailAttackNewTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sRxUserSeq,ViCommConst.MAIN_CHAR_NO,sMailSeq,ViCommConst.FLAG_OFF);
				}

				oParam.Add("attacknewresult",sMailAttackNewTxLogResult);
			}

			if (CheckMailResBonusEnable(sMailSeq)) {
				oParam.Add("mailresbonusflag",ViCommConst.FLAG_ON_STR);
			}

			// メール返信ボーナス　ポイント付与対象チェック
			if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.mailData[sMailDataSeq].mailResBonus2Flag)) {
				MailResBonusLog2 oMailResBonusLog2 = new MailResBonusLog2();
				string pMainFlag;
				string pSubFlag;
				oMailResBonusLog2.CheckSendMail(
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusSeq,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec1,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec2,
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusMaxNum,
					sessionWoman.site.siteCd,
					sRxUserSeq,
					sessionWoman.userWoman.userSeq,
					sMailSeq,
					out pMainFlag,
					out pSubFlag
				);
				if (pMainFlag.Equals(ViCommConst.FLAG_ON_STR) || pSubFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					oParam.Add("mailresbonusflag",ViCommConst.FLAG_ON_STR);
					oParam.Add("v","2");
				}
			}

			RedirectToDisplayDoc(ViCommConst.SCR_TX_MAIL_COMPLITE_CAST,oParam);
		}
	}

	private void DeleteDraftMail(string pMailDataSeq) {
		string sDraftMailSeq = iBridUtil.GetStringValue(sessionWoman.userWoman.mailData[pMailDataSeq].draftMailSeq);
		if (!string.IsNullOrEmpty(sDraftMailSeq)) {
			using (DraftMail oDraftMail = new DraftMail()) {
				oDraftMail.DeleteDraftMail(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,new string[] { sDraftMailSeq });
			}
		}
	}

	/// <summary>
	/// メール返信ボーナスポイント付与チェック
	/// </summary>
	/// <param name="pMailSeq"></param>
	/// <returns></returns>
	private bool CheckMailResBonusEnable(string pMailSeq) {
		bool bOK = false;
		if (IsAvailableService(ViCommConst.RELEASE_MAIL_RES_BONUS,2)) {
			using (MailResBonus oMailResBonus = new MailResBonus()) {
				string sAddPoint;
				string sResult;
				oMailResBonus.AddMailResBonusPoint(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					pMailSeq,
					ViCommConst.FLAG_ON,
					out sAddPoint,
					out sResult
				);
				
				bOK = sResult.Equals(PwViCommConst.AddMailResBonusPoint.RESULT_OK);
			}
		}

		return bOK;
	}
}
