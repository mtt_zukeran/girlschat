/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・プロフ情報入力
--	Progaram ID		: ModifyGameUserProfile
--
--  Creation Date	: 2012.03.16
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ModifyGameUserProfile:MobileSocialGameWomanBase {
	private string[] attrTypeSeq;
	private string[] attrSeq;
	private string[] attrInputValue;
	private Int64 okMask;

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			using (TempRegist oTempRegist = new TempRegist()) {
				if (oTempRegist.GetOneByUserSeq(sessionWoman.userWoman.userSeq)) {
					if (oTempRegist.registStatus.Equals(PwViCommConst.RegistStatus.REGIST_RX_EMAIL)) {
						if (iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("01")) {
							IDictionary<string,string> oParameters = new Dictionary<string,string>();
							oParameters.Add("gc_acc","1");
							RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_ID_PIC_MAIL,oParameters);
						} else {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_ID_PIC_MAIL);
						}
					}
				}
			}
			
			DisplayInitData();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	virtual protected void DisplayInitData() {
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
				CreateList();
			} else {
				this.pnlBirthday.Visible = false;
			}
		}
		
		txtHandelNm.Text = HttpUtility.HtmlDecode(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNm);

		bool bDupliFlag = false;
		if (IsAvailableService(ViCommConst.RELEASE_CHECK_HANDLE_NM_DUPLI)) {
			using (UserWoman oUserWoman = new UserWoman()) {
				bDupliFlag = oUserWoman.CheckCastHandleNmDupli(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,Mobile.EmojiToCommTag(sessionWoman.carrier,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNm));
			}
		}
		if (txtHandelNm.Text.Equals(string.Empty) || bDupliFlag) {
			pnlHandleNm.Visible = true;
		}
		
		txtAreaCommentList.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].commentList;
		txtAreaCommentDetail.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].commentDetail;

		SelectionList lstAgeMainteHint = (SelectionList)frmMain.FindControl(string.Format("lstAgeMainteHint")) as SelectionList;
		if (lstAgeMainteHint != null) {
			int iStartAge = 18;
			int iEndAge = 39;
			DateTime dtNow = DateTime.Now;
			string sText = string.Empty;
			for (int age = iStartAge;age <= iEndAge;age++) {
				sText = string.Format("{0}歳={1}〜",age,dtNow.AddYears((age + 1) * -1).AddDays(1).ToString("yyyy/MM/dd"));
				lstAgeMainteHint.Items.Add(new MobileListItem(sText,age.ToString()));
			}
		}
		
		int iIdx = 0;
		int i = 0;

		for (i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			SelectionList lstUserWomanItem = (SelectionList)frmMain.FindControl(string.Format("lstUserWomanItem{0}",i + 1)) as SelectionList;
			iBMobileTextBox txtUserWomanItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserWomanItem{0}",i + 1)) as iBMobileTextBox;
			iBMobileLabel lblUserWomanItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserWomanItem{0}",i + 1)) as iBMobileLabel;
			TextArea txtAreaUserWomanItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserWomanItem{0}",i + 1)) as TextArea;

			lblUserWomanItem.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm;
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
				if (int.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].rowCount) > 1) {
					txtAreaUserWomanItem.Visible = true;
					txtUserWomanItem.Visible = false;
					txtAreaUserWomanItem.Rows = int.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].rowCount);
					txtAreaUserWomanItem.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrImputValue;
				} else {
					txtAreaUserWomanItem.Visible = false;
					txtUserWomanItem.Visible = true;
					txtUserWomanItem.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrImputValue;
				}
				lstUserWomanItem.Visible = false;
			} else {
				txtAreaUserWomanItem.Visible = false;
				txtUserWomanItem.Visible = false;
				lstUserWomanItem.Visible = true;
				iIdx = 0;
				using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue()) {
					DataSet ds = oCastAttrTypeValue.GetList(sessionWoman.site.siteCd,sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeSeq);
					if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].inputType == ViCommConst.INPUT_TYPE_LIST) {
						lstUserWomanItem.Items.Add(new MobileListItem("未選択",string.Empty));
					}
					foreach (DataRow dr in ds.Tables[0].Rows) {
						lstUserWomanItem.Items.Add(new MobileListItem(dr["CAST_ATTR_NM"].ToString(),dr["CAST_ATTR_SEQ"].ToString()));
						if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrSeq.Equals(dr["CAST_ATTR_SEQ"].ToString())) {
							lstUserWomanItem.Items.Remove(new MobileListItem("未選択",string.Empty));
							lstUserWomanItem.SelectedIndex = iIdx;
						}
						iIdx++;
					}
				}
				if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].inputType == ViCommConst.INPUT_TYPE_RADIO) {
					lstUserWomanItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
					if (lstUserWomanItem.SelectedIndex == -1) {
						lstUserWomanItem.SelectedIndex = 0;
					}
				}
			}
		}

		for (i = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
			Panel pnlUserWoman = (Panel)frmMain.FindControl(string.Format("pnlUserWoman{0}",i + 1)) as Panel;
			pnlUserWoman.Visible = false;
		}

		using (OkPlayList oPlayList = new OkPlayList()) {
			DataSet ds = oPlayList.GetList(sessionWoman.site.siteCd);
			int iCnt = 0;
			Int64 iOkMask = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].okPlayMask;
			int iMaskValue = 1;

			foreach (DataRow dr in ds.Tables[0].Rows) {
				ViewState["OK" + iCnt.ToString()] = dr["OK_PLAY"].ToString();
				SelectionList chkOk = (SelectionList)pnlOkList.FindControl(string.Format("chkOK{0}",iCnt)) as SelectionList;
				chkOk.Items.Add(new MobileListItem(dr["OK_PLAY_NM"].ToString(),dr["OK_PLAY"].ToString()));
				chkOk.Visible = true;
				if ((iOkMask & iMaskValue) != 0) {
					chkOk.Items[0].Selected = true;
				}
				iMaskValue = iMaskValue << 1;
				iCnt++;
			}
			ViewState["OK_PLAY_RECORD_COUNT"] = iCnt;
			for (i = iCnt;i < ViCommConst.MAX_PLAY_COUNT;i++) {
				SelectionList chkOk = (SelectionList)pnlOkList.FindControl(string.Format("chkOK{0}",i)) as SelectionList;
				chkOk.Visible = false;
			}
			if (ds.Tables[0].Rows.Count == 0) {
				pnlOkList.Visible = false;
			}
		}
		this.lblCommentList.Text = DisplayWordUtil.ReplaceWordCommentList(this.lblCommentList.Text);
		this.IBMobileLabel1.Text = DisplayWordUtil.ReplaceOkList(this.IBMobileLabel1.Text);
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (CheckInputValue() == false) {
			return;
		} else {
			UpdateResult(UpdateProfile());
		}
	}

	virtual protected bool CheckInputValue() {
		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}
		
		string sNGWord = string.Empty;
		bool bOk = true;
		okMask = 0;
		Int64 iMaskValue = 1;
		int iMaskCount = int.Parse(ViewState["OK_PLAY_RECORD_COUNT"].ToString());
		
		for (int i = 0;i < iMaskCount;i++) {
			SelectionList chkOk = (SelectionList)pnlOkList.FindControl(string.Format("chkOK{0}",i)) as SelectionList;
			if (chkOk.Items[0].Selected) {
				okMask = okMask + iMaskValue;
			}
			iMaskValue = iMaskValue << 1;
		}

		lblErrorMessage.Text = "";

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				//ﾊﾝﾄﾞﾙﾈｰﾑを入力して下さい。 
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}

		}

		attrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		attrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		attrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
				if (int.Parse(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].rowCount) > 1) {
					TextArea txtAreaUserWomanItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserWomanItem{0}",i + 1)) as TextArea;
					attrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtAreaUserWomanItem.Text));
					if (attrInputValue[i].Length > 300) {
						attrInputValue[i] = SysPrograms.Substring(attrInputValue[i],300);
					}
				} else {
					iBMobileTextBox txtUserWomanItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserWomanItem{0}",i + 1)) as iBMobileTextBox;
					attrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtUserWomanItem.Text));
				}
				if (sessionWoman.ngWord.VaidateDoc(attrInputValue[i],out sNGWord) == false) {
					bOk = false;
					lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
				if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].profileReqItemFlag) {
					if (attrInputValue[i].Equals(string.Empty)) {
						lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm);
						bOk = false;
					}
				}
			} else {
				SelectionList lstUserWomanItem = (SelectionList)frmMain.FindControl(string.Format("lstUserWomanItem{0}",i + 1)) as SelectionList;
				if (lstUserWomanItem.Selection != null) {
					if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].profileReqItemFlag && lstUserWomanItem.Selection.Value.Equals(string.Empty)) {
						lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm);
						bOk = false;
					} else {
						attrSeq[i] = lstUserWomanItem.Selection.Value;
					}
				} else {
					if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].profileReqItemFlag) {
						bOk = false;
						lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm);
					}
				}
			}
			attrTypeSeq[i] = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeSeq;
		}
		
		if (IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
			string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
			int iAge = ViCommPrograms.Age(sDate);
			if (iAge == 0) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
			} else if (iAge < 18) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18_MODIFY);
			}
		}

		if (txtAreaCommentDetail.Visible) {
			if (sessionWoman.ngWord.VaidateDoc(txtAreaCommentDetail.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		if (txtAreaCommentList.Visible) {
			if (sessionWoman.ngWord.VaidateDoc(txtAreaCommentList.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		return bOk & this.CheckOther();
	}

	protected void UpdateResult(string pResult) {
		if (pResult.Equals("0")) {
			if (iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("01")) {
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				oParameters.Add("gc_acc","1");
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_ID_PIC_MAIL,oParameters);
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_ID_PIC_MAIL);
			}
		} else {
			switch (int.Parse(pResult)) {
				case ViCommConst.REG_USER_RST_HANDLE_NM_EXIST:
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
					break;
				default:
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
					break;
			}
		}
	}

	virtual protected string UpdateProfile() {
		string sCommentList;
		string sCommentDetail;
		string sResult;

		if (txtAreaCommentList.Text.Length > 1000) {
			sCommentList = SysPrograms.Substring(txtAreaCommentList.Text,1000);
		} else {
			sCommentList = txtAreaCommentList.Text;
		}

		if (txtAreaCommentDetail.Text.Length > 1000) {
			sCommentDetail = SysPrograms.Substring(txtAreaCommentDetail.Text,1000);
		} else {
			sCommentDetail = txtAreaCommentDetail.Text;
		}
		
		string sBirthday = "";
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
				sBirthday = string.Format("{0}/{1}/{2}",lstYear.Selection.Value,lstMonth.Selection.Value,lstDay.Selection.Value);
			}
		}

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			oCastGameCharacter.ModifyGameUserWomanProf(
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				txtHandelNm.Visible ? HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtHandelNm.Text)) : string.Empty,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,sCommentList)),
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,sCommentDetail)),
				okMask,
				sBirthday,
				attrTypeSeq,
				attrSeq,
				attrInputValue,
				sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count,
				out sResult
			);
		}
		
		return sResult;
	}
	
	private void CreateList() {
		string[] sBirthday = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].birthday.Split('/');
		int iYear = DateTime.Today.Year - 18;
		bool bSelectedIndex = false;

		for (int i = 0;i < 28;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			if ((sBirthday.Length == 3) && (sBirthday[0].Equals(iYear.ToString("d4")))) {
				lstYear.SelectedIndex = i;
				bSelectedIndex = true;
			}
			iYear -= 1;
		}

		if (!bSelectedIndex) {
			if (sBirthday.Length == 3) {
				for (int i = 28;i < 85;i++) {
					lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
					if ((sBirthday.Length == 3) && (sBirthday[0].Equals(iYear.ToString("d4")))) {
						lstYear.SelectedIndex = i;
						bSelectedIndex = true;
						break;
					}
					iYear -= 1;
				}
			} else {
				lstYear.SelectedIndex = 7;
				lstMonth.SelectedIndex = 0;
				lstDay.SelectedIndex = 0;
			}
		}

		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
			if ((sBirthday.Length == 3) && (sBirthday[1].Equals(i.ToString("d2")))) {
				lstMonth.SelectedIndex = i - 1;
			}
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
			if ((sBirthday.Length == 3) && (sBirthday[2].Equals(i.ToString("d2")))) {
				lstDay.SelectedIndex = i - 1;
			}
		}

		if (this.lstYear.Selection == null || this.lstMonth.Selection == null || this.lstDay.Selection == null) {
			return;
		}

		int iBirthDay = int.Parse(string.Concat(this.lstYear.Selection.Text,this.lstMonth.Selection.Text,this.lstDay.Selection.Text));
		int iToday = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
		string sAge = ((int)Math.Floor((double)((iToday - iBirthDay) / 10000))).ToString();
		int iCounter = 0;
		foreach (MobileListItem oItem in this.lstAgeMainteHint.Items) {
			if (oItem.Value.Equals(sAge)) {
				this.lstAgeMainteHint.SelectedIndex = iCounter;
				return;
			}
			iCounter++;
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
