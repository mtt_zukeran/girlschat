/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 登録申請(固体識別事前通知型)
--	Progaram ID		: RegistUserRequestByTermId
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_RegistUserRequestByTermId:MobileWomanPageBase {

	private const string PARAM_NEXT_DOC_NO = "*nextdoc";

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		ParseHTML oParseWoman = sessionWoman.parseContainer;

		if (sessionWoman.getTermIdFlag) {
			oParseWoman.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
		} else {
			oParseWoman.parseUser.postAction = ViCommConst.POST_ACT_GUID;
		}

		if (!IsPostBack) {
			CreateList();
			DispAgree();
			DisplayInitData();
			if (IsAvailableService(ViCommConst.RELEASE_CAST_TEL_INPUT_AFTER)) {
				lblTel.Visible = false;
				txtTel.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		txtTel.Text = HttpUtility.HtmlEncode(txtTel.Text);
		txtCastNm.Text = HttpUtility.HtmlEncode(txtCastNm.Text);
		txtCastKanaNm.Text = HttpUtility.HtmlEncode(txtCastKanaNm.Text);
		txtPassword.Text = HttpUtility.HtmlEncode(txtPassword.Text);

		string sUtn = Mobile.GetUtn(sessionWoman.carrier,Request);
		string siModeId = Mobile.GetiModeId(sessionWoman.carrier,Request);

		string sResult;
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (Mobile.IsFeaturePhone(sessionWoman.carrier)) {
			if (bOk) {
				if (sessionWoman.getTermIdFlag) {
					if (sUtn.Equals(string.Empty)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
						return;
					}
				} else {
					if (siModeId.Equals(string.Empty)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
						return;
					}
				}
			}
		}

		if (!IsAvailableService(ViCommConst.RELEASE_CAST_TEL_INPUT_AFTER)) {
			if (txtTel.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
			} else {
				using (Man oMan = new Man()) {
					if (oMan.IsBlackUser(ViCommConst.NO_TYPE_TERMINAL_UNIQUE_ID,sUtn)) {
						//ﾌﾞﾗｯｸﾕｰｻﾞｰとして登録されています。ご利用になれません。
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
						bOk = false;
						return;
					}
					if (oMan.IsBlackUser(ViCommConst.NO_TYPE_TEL,txtTel.Text)) {
						//ﾌﾞﾗｯｸﾕｰｻﾞｰとして登録されています。ご利用になれません。
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
						bOk = false;
						return;
					}
				}

				if (txtTel.Text.Length > 11) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
				} else if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
				} else {
					using (UserWoman oWoman = new UserWoman()) {
						if (oWoman.IsTelephoneRegistered(txtTel.Text)) {
							//携帯電話番号は既に登録されています。
							lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
							bOk = false;
						}
					}
				}
			}
		}

		if (txtCastNm.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NM_KANJI);
		}

		if (txtCastKanaNm.Visible) {
			if (txtCastKanaNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NM_KANA);
			}

			//カタカナチェック
			if (!SysPrograms.Expression(@"^[ｦ-ﾟァ-ー]+$",txtCastKanaNm.Text)) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_VALIDATE_KANA_FIELD),"氏名カナ");
			}
		}
		if ((lstYear.Selection.Value.Equals("")) || (lstMonth.Selection.Value.Equals("")) || (lstDay.Selection.Value.Equals(""))) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY);
		} else if (lstYear.Selection.Value.Length != 4) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_YYYY_NG);
		} else {
			if (bOk) {
				string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
				int iAge = ViCommPrograms.Age(sDate);
				if (iAge < 18) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18);
				} else if (iAge > 99) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
				}
			}
		}

		if (txtPassword.Visible) {
			if (txtPassword.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD);
				bOk = false;
			} else {
				if (!SysPrograms.Expression(@"^[a-zA-Z0-9]{4,8}$",txtPassword.Text)) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD_NG);
					bOk = false;
				}
			}
		} else {
			Random rnd = new Random();
			txtPassword.Text = string.Format("{0:D4}",rnd.Next(10000));
		}

		if (bOk) {
			if (pnlAgreement.Visible) {
				if (!chkAgreement.Items[0].Selected) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NOT_AGREE);
				}
			}
		}

		bOk = bOk & CheckOther();

		if (bOk == false) {
			//RedirectToMobilePage(ViCommPrograms.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_WOMAN_REGIST));
			return;
		}

		using (TempRegist oRegist = new TempRegist()) {
			string sBirthday = "";
			DateTime dtBiathday = new DateTime(int.Parse(lstYear.Selection.Value),int.Parse(lstMonth.Selection.Value),int.Parse(lstDay.Selection.Value));
			sBirthday = dtBiathday.ToString("yyyyMMdd");

			oRegist.RegistUserWomanTemp(
				sUtn,
				siModeId,
				Request.UserHostAddress,
				txtTel.Text,
				txtPassword.Text,
				txtCastNm.Text,
				txtCastKanaNm.Text,
				sBirthday,
				txtQuestionDoc.Text,
				string.Empty,
				ViCommConst.FLAG_OFF,
				out sessionWoman.userWoman.tempRegistId,out sResult);

			switch (int.Parse(sResult)) {
				case ViCommConst.REG_USER_RST_UNT_EXIST:
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
					return;
			}
		}

		if (sResult.Equals("0")) {
			string sNextDoc = iBridUtil.GetStringValue(Request.Params[PARAM_NEXT_DOC_NO]);
			if (string.IsNullOrEmpty(sNextDoc)) {
				sNextDoc = ViCommConst.SCR_REGIST_TEMP_WOMAN_COMPLITE;
			}

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + sNextDoc));
		} else {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
		}
	}

	protected virtual void CreateList() {
		int iYear = DateTime.Today.Year - 18;

		for (int i = 0;i < 85;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear -= 1;
		}
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}

	private void DispAgree() {
		pnlAgreement.Visible = false;
		using (Site oSite = new Site()) {
			oSite.GetOne(sessionWoman.site.siteCd);
			if (oSite.castRegistNeedAgreeFlag == ViCommConst.FLAG_ON) {
				pnlAgreement.Visible = true;
			}
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}

	protected virtual void DisplayInitData() {
	}
}
