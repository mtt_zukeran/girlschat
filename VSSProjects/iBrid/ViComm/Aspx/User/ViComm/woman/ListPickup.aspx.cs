/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップ一覧
--	Progaram ID		: ListPickup
--  Creation Date	: 2014.06.04
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListPickup:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPickupId = iBridUtil.GetStringValue(this.Request.QueryString["pickupid"]);
		Response.Filter = sessionWoman.InitScreen(Response.Filter,frmMain,Request,string.Format("ListPickup{0}.aspx",sPickupId),ViewState);

		if (!this.IsPostBack) {
			ulong pMode = 0;
			string sPickupType = string.Empty;
			
			using(Pickup oPickup = new Pickup()){
				sPickupType = oPickup.GetPickupType(sessionWoman.site.siteCd,sPickupId);
			}

			switch(sPickupType){
				case ViCommConst.PicupTypes.PROFILE_MOVIE:
					pMode = ViCommConst.INQUIRY_PICKUP_MOVIE;
					break;
				default:
					throw new NotSupportedException(string.Format("想定外のピックアップ区分です。{0}",sPickupType));
			}

			sessionWoman.ControlList(Request,pMode,ActiveForm);
		}
	}
}
