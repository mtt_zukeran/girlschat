/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやき購読解除
--	Progaram ID		: DeleteManTweetRead
--
--  Creation Date	: 2013.03.21
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteManTweetRead:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		string sManUserSeq = iBridUtil.GetStringValue(this.Request.QueryString["manuserseq"]);

		if (!string.IsNullOrEmpty(sManUserSeq)) {
			if (sessionWoman.SetManDataSetByUserSeq(sManUserSeq,1)) {
				string sResult = string.Empty;

				using (ManTweetRead oManTweetRead = new ManTweetRead()) {
					oManTweetRead.DeleteManTweetRead(
						sessionWoman.site.siteCd,
						sManUserSeq,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						out sResult
					);
				}
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}
