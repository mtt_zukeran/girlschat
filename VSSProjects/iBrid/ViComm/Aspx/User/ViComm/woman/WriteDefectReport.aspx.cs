/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 不具合報告投稿
--	Progaram ID		: WriteDefectReport
--
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_WriteDefectReport:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;
			pnlResult.Visible = false;

			this.setLstCategory();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	private void setLstCategory() {
		using (DefectReport oDefectReport = new DefectReport()) {
			string sBlogFlag = (sessionWoman.userWoman.CurCharacter.characterEx.enabledBlogFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
			string sRichFlag = (sessionWoman.userWoman.CurCharacter.characterEx.enabledRichinoFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
			
			DataSet dsCategory = oDefectReport.GetDefectReportCategory(sessionWoman.site.siteCd,ViCommConst.OPERATOR,sBlogFlag,sRichFlag);

			foreach (DataRow drCategory in dsCategory.Tables[0].Rows) {
				lstCategory.Items.Add(new MobileListItem(drCategory["DEFECT_REPORT_CATEGORY_NM"].ToString(),drCategory["DEFECT_REPORT_CATEGORY_SEQ"].ToString()));
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;

		if (txtTitle.Text.Equals(string.Empty)) {
			sessionWoman.errorMessage = "ﾀｲﾄﾙを入力して下さい";
			return;
		} else {
			if (txtTitle.Text.Length > 42) {
				sessionWoman.errorMessage = "ﾀｲﾄﾙが長すぎます";
				return;
			}
		}

		if (lstCategory.Items[lstCategory.SelectedIndex].Value.Equals(string.Empty)) {
			sessionWoman.errorMessage = "ｶﾃｺﾞﾘを選択して下さい";
			return;
		}

		if (txtText.Text.Equals(string.Empty)) {
			sessionWoman.errorMessage = "報告内容を入力して下さい";
			return;
		} else {
			if (txtText.Text.Length > 1000) {
				sessionWoman.errorMessage = "報告内容が長すぎます";
				return;
			}
		}

		string sNGWord = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (!sessionWoman.ngWord.VaidateDoc(txtTitle.Text,out sNGWord)) {
			sessionWoman.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			return;
		}

		if (!sessionWoman.ngWord.VaidateDoc(txtText.Text,out sNGWord)) {
			sessionWoman.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			return;
		}

		ViewState["TITLE"] = txtTitle.Text;
		ViewState["DEFECT_REPORT_CATEGORY_SEQ"] = lstCategory.Items[lstCategory.SelectedIndex].Value;
		ViewState["TEXT"] = txtText.Text;

		lblTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
		lblCategory.Text = lstCategory.Items[lstCategory.SelectedIndex].Text;
		lblText.Text = HttpUtility.HtmlEncode(txtText.Text).Replace(System.Environment.NewLine,"<br>");

		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
	}

	protected void cmdTx_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		string sTitle = iBridUtil.GetStringValue(ViewState["TITLE"]);
		string sDefectReportCategorySeq = iBridUtil.GetStringValue(ViewState["DEFECT_REPORT_CATEGORY_SEQ"]);
		string sText = iBridUtil.GetStringValue(ViewState["TEXT"]);
		string sDefectReportSeq;
		string sResult = string.Empty;

		using (DefectReport oDefectReport = new DefectReport()) {
			sResult = oDefectReport.WriteDefectReport(
				sessionWoman.site.siteCd,
				ViCommConst.OPERATOR,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sTitle)),
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sText)),
				sDefectReportCategorySeq,
				out sDefectReportSeq
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			pnlWrite.Visible = false;
			pnlConfirm.Visible = false;
			pnlResult.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
		pnlResult.Visible = false;
	}
}