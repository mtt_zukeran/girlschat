/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����ٹްїpFlash(Boss��)
--	Progaram ID		: ViewGameFlashBattle
--
--  Creation Date	: 2012.01.05
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_ViewGameFlashBossBattle:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sStageSeq = iBridUtil.GetStringValue(Request.QueryString["stage_seq"]);
		string sResult = iBridUtil.GetStringValue(Request.QueryString["result"]);

		if (sResult.Equals(PwViCommConst.GameStageClear.RESULT_CLEARED) || sResult.Equals(PwViCommConst.GameStageClear.RESULT_NOT_CLEAR_TOWN)) {
			IDictionary<string,string> oParameters = new Dictionary<string,string>();
			oParameters.Add("result",sResult);
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_BOSS_BATTLE_NO_FINISH,oParameters);
		} else if (sResult.Equals(PwViCommConst.GameStageClear.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(sStageSeq));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		rgx = new Regex("^[0-9]+$");
		rgxMatch = rgx.Match(iBridUtil.GetStringValue(sResult));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		string sPreMoney = iBridUtil.GetStringValue(Request.QueryString["pre_money"]);
		string sLevelUpFlag = iBridUtil.GetStringValue(Request.QueryString["level_up_flag"]);
		string sAddForceCount = iBridUtil.GetStringValue(Request.QueryString["add_force_count"]);
		string sStageClearPointFlag = iBridUtil.GetStringValue(Request.QueryString["stage_clear_point_flag"]);
		string sAreaBossType = iBridUtil.GetStringValue(Request.QueryString["area_boss_type"]);

		string sFileNm = string.Empty;
		string sNextPageNm = string.Empty;
		string sGetParamStr = string.Empty;
		string sResultStr = string.Empty;
		string sBossLevel = string.Empty;

		sBossLevel = this.GetBossLevel(sStageSeq);

		if (sResult.Equals(PwViCommConst.GameStageClear.RESULT_OK)) {
			sResultStr = "win";
		} else {
			sResultStr = "lose";
		}

		sFileNm = string.Format("boss_battle_{0}_{1}w.swf",sResultStr,sBossLevel);

		if (sLevelUpFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sNextPageNm = "ViewGameFlashLevelup.aspx";
		} else {
			sNextPageNm = "ViewGameStageBossBattleResult.aspx";
		}

		if (this.Request.QueryString.Count > 0) {
			string[] keys = this.Request.QueryString.AllKeys;
			string value = string.Empty;
			List<string> oGetParamList = new List<string>();

			foreach (string key in keys) {
				oGetParamList.Add(key + "=" + iBridUtil.GetStringValue(this.Request.QueryString[key]));
			}

			sGetParamStr = string.Join("&",oGetParamList.ToArray());
		}

		NameValueCollection oSwfParam = new NameValueCollection();
		oSwfParam.Add("char",sessionWoman.userWoman.CurCharacter.gameCharacter.gameCharacterType);
		string sSelfPartyMemberMsk = this.GetPartyMemberMsk(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
		if (sSelfPartyMemberMsk.Equals("0")) {
			sSelfPartyMemberMsk = "8";
		}
		oSwfParam.Add("party",sSelfPartyMemberMsk);
		GameFlashLiteHelper.ResponseFlashSocialGame(sFileNm,sNextPageNm,sGetParamStr,oSwfParam);
	}

	private string GetBossLevel(string sStageSeq) {
		string sBossLevel = string.Empty;

		string sSiteCd = sessionWoman.site.siteCd;
		string sSexCd = ViCommConst.OPERATOR;

		using (Stage oStage = new Stage()) {
			sBossLevel = oStage.GetStageLevelInStageTypeByStageSeq(sSiteCd,sStageSeq,sSexCd);
		}

		return sBossLevel;
	}

	private string GetPartyMemberMsk(string sUserSeq,string sUserCharNo) {
		string sPartyMemberMsk = string.Empty;

		GamePartyBattleMemberSeekCondition oCondition = new GamePartyBattleMemberSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sUserSeq;
		oCondition.UserCharNo = sUserCharNo;

		using (GamePartyBattleMember oGamePartyBattleMember = new GamePartyBattleMember()) {
			sPartyMemberMsk = oGamePartyBattleMember.GetPartyMemberMask(oCondition);
		}

		return sPartyMemberMsk;
	}
}
