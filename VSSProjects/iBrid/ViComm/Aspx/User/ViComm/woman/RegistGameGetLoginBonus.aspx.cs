/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����۸޲��ްŽSP
--	Progaram ID		: RegistGameGetLoginBonus
--
--  Creation Date	: 2011.08.09
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_woman_RegistGameGetLoginBonus:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionWoman.logined == false) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermIdGame.aspx"));
			return;
		}

		this.GetLoginBonusExecute();
	}

	private void GetLoginBonusExecute() {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		int iDailyBonusFlag;
		int iMonthlyBonusFlag;
		string sResult;

		using (GameLoginBonusMonthly oGameLoginBonusMontly = new GameLoginBonusMonthly()) {
			oGameLoginBonusMontly.GetLoginBonus(
				sSiteCd,
				sUserSeq,
				sUserCharNo,
				out iDailyBonusFlag,
				out iMonthlyBonusFlag,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.GameGetLoginBonusResult.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		} else {
			string query = string.Empty;

			if (sResult.Equals(PwViCommConst.GameGetLoginBonusResult.RESULT_OK)) {
				query = string.Format("?daily_flag={0}&monthly_flag={1}",iDailyBonusFlag.ToString(),iMonthlyBonusFlag.ToString());
			}

			string sRedirectUrl = "ListGameLoginBonusMonthly.aspx" + query;

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sRedirectUrl));
		}
	}
}