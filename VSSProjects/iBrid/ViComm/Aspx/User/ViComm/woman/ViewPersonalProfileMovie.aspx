<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewPersonalProfileMovie.aspx.cs" Inherits="ViComm_woman_ViewPersonalProfileMovie" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
		$PGM_HTML05;
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="20"></cc1:iBMobileTextBox>
		$PGM_HTML06;
		<tx:TextArea ID="txtDoc" runat="server" BreakBefore="false" BreakAfter="true" Rows="6" Columns="24">
		</tx:TextArea>
		$PGM_HTML07;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
