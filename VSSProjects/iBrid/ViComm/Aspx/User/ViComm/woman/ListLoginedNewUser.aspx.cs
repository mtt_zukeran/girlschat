/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 新規ﾛｸﾞｲﾝ中男性会員一覧
--	Progaram ID		: ListLoginedNewUser
--
--  Creation Date	: 2010.09.13
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListLoginedNewUser:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_MAN_LOGINED_NEW,ActiveForm);
		}
	}
}
