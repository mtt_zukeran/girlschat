/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 音声通話アプリ利用設定
--	Progaram ID		: SetupVoiceApp
--  Creation Date	: 2015.03.24
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_SetupVoiceApp:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sSetupFlag = iBridUtil.GetStringValue(Request.QueryString["setup"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));

			if (!string.IsNullOrEmpty(sSetupFlag)) {
				this.SetupVoiceapp(sSetupFlag.Equals(ViCommConst.FLAG_ON_STR),sBackUrl);
			} else {
				pnlInput.Visible = true;
				pnlError.Visible = false;

				if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
					chkUseVoiceapp.Items[0].Text = "音声通話アプリを利用する";
				}

				if (sessionWoman.userWoman.useVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
					chkUseVoiceapp.Items[0].Selected = true;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		this.SetupVoiceapp(chkUseVoiceapp.Items[0].Selected,"");
	}

	private void SetupVoiceapp(bool pSetupFlag,string pBackUrl) {
		string sResult = sessionWoman.userWoman.SetupVoiceapp(
			sessionWoman.userWoman.userSeq,
			(pSetupFlag) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF
		);

		if (sResult.Equals("0")) {
			if (!string.IsNullOrEmpty(pBackUrl)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(pBackUrl));
				return;
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("SetupVoiceAppComplete.aspx"));
				return;
			}
		} else {
			pnlInput.Visible = false;
			pnlError.Visible = true;
		}
	}
}
