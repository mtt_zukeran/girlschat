/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・お宝画像詳細
--	Progaram ID		: ViewGameTreasureManagement
--
--  Creation Date	: 2011.10.13
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameTreasureManagement:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		string sCastGamePicSeq = iBridUtil.GetStringValue(this.Request.QueryString["cast_game_pic_seq"]);

		if (sCastGamePicSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			DataSet oDataSet;
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE_MANAGEMENT,ActiveForm,out oDataSet);

			if (oDataSet.Tables[0].Rows.Count > 0) {
				txtDoc.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_DOC"]);
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sCastGamePicSeq);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pCastGamePicSeq) {
		txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

		if (ValidateInput()) {
			string sDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
			string sResult = string.Empty;

			using (ManTreasure oManTreasure = new ManTreasure()) {
				sResult = oManTreasure.ModifyManTreasure(
					this.sessionWoman.site.siteCd,
					this.sessionWoman.userWoman.userSeq,
					this.sessionWoman.userWoman.curCharNo,
					pCastGamePicSeq,
					sDoc
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				string sRotateType = iBridUtil.GetStringValue(this.Request.Params["rotate_type"]);
				RotateFlipType oRoateType = ImageUtil.Convert2RotateFlipType(sRotateType);

				string sWebPhisicalDir = string.Empty;
				using (Site oSite = new Site()) {
					oSite.GetValue(sessionWoman.site.siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
				}
				string sCastPicDir = ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sessionWoman.site.siteCd,sessionWoman.userWoman.loginId);
				ImageUtil.RotateFilp(sCastPicDir,pCastGamePicSeq,oRoateType);

				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				foreach (string sKey in this.Request.QueryString.AllKeys) {
					oParameters.Add(sKey,iBridUtil.GetStringValue(this.Request.QueryString[sKey]));
				}
				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_PIC_UPDATE_COMPLETE,oParameters);
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	private bool ValidateInput() {
		bool bOk = true;
		string sNGWord = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄを入力して下さい。";
		} else if (txtDoc.Text.Length > 750) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄが長すぎます。";
		} else if (sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
		}

		return bOk;
	}
}