/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 日記詳細表示
--	Progaram ID		: ViewDiary
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewDiary:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
				if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
					using (CastDiary oDiary = new CastDiary()) {
						string sPicFileNm;
						oDiary.DeleteDiary(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sessionWoman.GetDiaryValue("REPORT_DAY"),
							sessionWoman.GetDiaryValue("CAST_DIARY_SUB_SEQ"),
							out sPicFileNm
						);
						if (sPicFileNm.Equals("") == false) {
							DeletePicture(sPicFileNm);
						}
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_DIARY) + "&site=" + sessionWoman.site.siteCd);
					}
				}
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListPersonalDiary.aspx") + "?site=" + sessionWoman.site.siteCd);
			}
		}
	}

	private void DeletePicture(string pFileSeq) {
		string sFullPath = sessionWoman.site.webPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
							string.Format("{0:D15}",int.Parse(pFileSeq)) + ViCommConst.PIC_FOODER;
		
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}

			sFullPath = sessionWoman.site.webPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
								string.Format("{0:D15}",int.Parse(pFileSeq)) + ViCommConst.PIC_FOODER_SMALL;


			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
	}

}
