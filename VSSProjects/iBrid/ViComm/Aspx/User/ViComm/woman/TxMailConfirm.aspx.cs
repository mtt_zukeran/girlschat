/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信確認
--	Progaram ID		: TxMailConfirm
--
--  Creation Date	: 2010.09.07
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_TxMailConfirm:MobileWomanPageBase {

	private string sBatch;

	virtual protected void Page_Load(object sender,EventArgs e) {
		sBatch = iBridUtil.GetStringValue(Request.QueryString["batch"]);

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			ParseHTML oParse = sessionWoman.parseContainer;
			ViewState["MAIL_DATA_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["data"]);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdBack"] != null) {
				cmdBack_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = string.Empty;
		ParseHTML oParse = sessionWoman.parseContainer;
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		string sRxUserSeq = string.Empty;
		string sRxUserNm = string.Empty;
		string sObjTempId = string.Empty;
		string sMailer = iBridUtil.GetStringValue(Request.QueryString["mailer"]);
		bool bOk = true;
		if (sBatch.Equals("1")) {
			if ((sessionWoman.BatchMailUserList.Count + sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].batchMailCount) > sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].castToManBatchMailLimit) {
				bOk = false;
				lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_CAST_TO_MAN_MAIL_LIMIT_OVER);
			}
		}
		if (sBatch.Equals(ViCommConst.FLAG_ON_STR) && sessionWoman.userWoman.mailData[sMailDataSeq].isSent) {
			bOk = false;
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_SENT_MAIL);
		}
		if (bOk == true) {
			if (sBatch != "1") {
				sRxUserSeq = sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq;
				sRxUserNm = sessionWoman.userWoman.mailData[sMailDataSeq].rxUserNm;
			}
			string sMailTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle) : sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle));
			string sMailDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionWoman.ngWord.ReplaceMask(sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc) : sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc));
			string sQuereyString = sessionWoman.userWoman.mailData[sMailDataSeq].queryString;
			string sReturnMailSeq = sessionWoman.userWoman.mailData[sMailDataSeq].returnMailSeq;
			bool bIsReturnMail = sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail;
			string sMailTemplate = sessionWoman.userWoman.mailData[sMailDataSeq].mailTemplateNo;
			string sMailType = iBridUtil.GetStringValue(Request.QueryString["mailtype"]);
			string sPicSeq = string.Empty;
			string sMovieSeq = string.Empty;

			if (sMailType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
				sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			} else if (sMailType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
				sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
			} else {
				sMailType = string.Empty;
			}
			using (MailBox oMailBox = new MailBox()) {
				if (sBatch == "1") {
					string[] keys = new string[sessionWoman.BatchMailSearchConditions.Keys.Count];
					string[] values = new string[sessionWoman.BatchMailSearchConditions.Values.Count];
					sessionWoman.BatchMailSearchConditions.Keys.CopyTo(keys,0);
					sessionWoman.BatchMailSearchConditions.Values.CopyTo(values,0);
					sMailTitle = MailHelper.ConvertVarForSend(sMailTitle);
					sMailDoc = MailHelper.ConvertVarForSend(sMailDoc);

					oMailBox.BatchMailMarking(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						sessionWoman.BatchMailUserList.ToArray(),
						sessionWoman.BatchMailListCreateDate.ToString("yyyyMMddHHmmss")
					);


					if (sMailer.Equals(ViCommConst.FLAG_ON_STR)) {
						oMailBox.TxCastToManMail(
							sessionWoman.site.siteCd,
							sMailTemplate,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sessionWoman.BatchMailUserList.ToArray(),
							0,
							sMailTitle,
							sMailDoc,
							sMailType,
							null,
							null,
							ViCommConst.FLAG_ON,
							keys,
							values,
							out sObjTempId
						);
						sessionWoman.userWoman.mailData[sMailDataSeq].objTempId = sObjTempId;
					} else {
						oMailBox.TxCastToManMail(
							sessionWoman.site.siteCd,
							sMailTemplate,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sessionWoman.BatchMailUserList.ToArray(),
							0,
							sMailTitle,
							sMailDoc,
							sMailType,
							sPicSeq,
							sMovieSeq,
							ViCommConst.FLAG_ON,
							keys,
							values,
							out sObjTempId
						);
					}
					sessionWoman.userWoman.mailData[sMailDataSeq].isSent = true;
					sessionWoman.userWoman.UpdateBatchMailCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.BatchMailUserList.Count);
				} else {
					oMailBox.TxCastToManReturnMail(
						sessionWoman.site.siteCd,
						sMailTemplate,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						new string[] { sRxUserSeq },
						0,
						sMailTitle,
						sMailDoc,
						sMailType,
						sPicSeq,
						sMovieSeq,
						ViCommConst.FLAG_OFF,
						null,
						null,
						sReturnMailSeq,
						out sObjTempId
					);
				}
			}
			if (sBatch.Equals(ViCommConst.FLAG_ON_STR)) {
				if (sMailer.Equals(ViCommConst.FLAG_ON_STR)) {
					if (sMailType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
						if (bIsReturnMail) {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}",ViCommConst.SCR_WOMAN_PIC_RETURN_MAILER_COMPLITE,sMailDataSeq)));
						} else {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}",ViCommConst.SCR_WOMAN_PIC_TX_MAILER_COMPLITE,sMailDataSeq)));
						}
					} else {
						if (bIsReturnMail) {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}",ViCommConst.SCR_WOMAN_MOVIE_RETURN_MAILER_COMPLITE,sMailDataSeq)));
						} else {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}",ViCommConst.SCR_WOMAN_MOVIE_TX_MAILER_COMPLITE,sMailDataSeq)));
						}
					}
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_BATCH_MAIL_COMPLETE_CAST + "&" + sQuereyString));
				}
			} else {
				if (bIsReturnMail) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_REMAIL_COMPLITE_CAST + "&" + sQuereyString));
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_MAIL_COMPLITE_CAST + "&" + sQuereyString));
				}
			}
		}
	}

	protected void cmdBack_Click(object sender,EventArgs e) {
		string sMailType = iBridUtil.GetStringValue(Request.QueryString["mailtype"]);
		string sMailDataSeq = ViewState["MAIL_DATA_SEQ"].ToString();

		if (sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ReturnMail.aspx?data={0}",sMailDataSeq)));
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("TxMail.aspx?data={0}&batch={1}",sMailDataSeq,sBatch)));
		}
	}

}
