<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyGameUser.aspx.cs" Inherits="ViComm_woman_ModifyGameUser" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
	    <ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		$PGM_HTML02;
		<ibrid:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true" BreakAfter="true"></ibrid:iBMobileTextBox>
		$PGM_HTML03;
		<ibrid:iBMobileTextBox ID="txtCastNm" runat="server" MaxLength="12" Size="18" BreakAfter="true"></ibrid:iBMobileTextBox>
		$PGM_HTML04;
		<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<ibrid:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">�N</ibrid:iBMobileLabel>
		<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<ibrid:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">��</ibrid:iBMobileLabel>
		<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<ibrid:iBMobileLabel ID="lblDay" runat="server">��</ibrid:iBMobileLabel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
