/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・お宝動画詳細
--	Progaram ID		: ViewGameCastMovie
--
--  Creation Date	: 2011.10.26
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameCastMovie:MobileSocialGameWomanBase {
	virtual protected void Page_Load(object sender,EventArgs e) {
		
		string sCastUserMovieSeq = iBridUtil.GetStringValue(this.Request.Params["cast_user_movie_seq"]);
		string sMovieSeq = iBridUtil.GetStringValue(this.Request.Params["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(this.Request.Params["playmovie"]);

		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sMovieSeq) && sPlayMovie.Equals(ViCommConst.FLAG_ON_STR)) {
				MovieHelper.Download(sMovieSeq,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			} else {
				if (string.IsNullOrEmpty(sCastUserMovieSeq)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}

				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

				DataSet oDataSet;
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CAST_MOVIE_PLAY,ActiveForm,out oDataSet);

				if (oDataSet.Tables[0].Rows.Count > 0) {
					txtDoc.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["MOVIE_DOC"]);					
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sCastUserMovieSeq);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pCastUserMovieSeq) {
		txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

		if (ValidateInput()) {
			string sDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
			string sResult = string.Empty;

			using (CastMovie oCastMovie = new CastMovie()) {
				sResult = oCastMovie.ModifyGameCastMovie(
					this.sessionWoman.site.siteCd,
					this.sessionWoman.userWoman.userSeq,
					this.sessionWoman.userWoman.curCharNo,
					pCastUserMovieSeq,
					sDoc
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				foreach (string sKey in this.Request.QueryString.AllKeys) {
					oParameters.Add(sKey,iBridUtil.GetStringValue(this.Request.QueryString[sKey]));
				}

				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_MOVIE_UPDATE_COMPLETE,oParameters);	
			}
		}
	}

	private bool ValidateInput() {
		bool bOk = true;
		string sNGWord = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄを入力して下さい。";
		} else if (txtDoc.Text.Length > 750) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄが長すぎます。";
		} else if (sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
		}

		return bOk;
	}
}
