/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE������ھ��Ċm�F
--	Progaram ID		: ResitGameOnceDayPresent
--
--  Creation Date	: 2012.03.30
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistGameOnceDayPresent:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		string sGameItemSeq = iBridUtil.GetStringValue(Request.QueryString["game_item_seq"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty) || sGameItemSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		} else {
			if (this.CheckOnlineStatusGame(sPartnerUserSeq,sPartnerUserCharNo)) {
				if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sGameItemSeq);
				}
			} else {
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo,string pGameItemSeq) {
		string sResult = string.Empty;
		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		
		using (OnceDayPresent oOnceDayPresent = new OnceDayPresent()) {
			sResult = oOnceDayPresent.ExecOnceDayPresent(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.CurCharacter.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				pGameItemSeq
			);
		}
		
		if (sResult.Equals(PwViCommConst.OnceDayPresentResult.RESULT_OK)) {
			oParameters.Add("partner_user_seq",pPartnerUserSeq);
			oParameters.Add("partner_user_char_no",pPartnerUserCharNo);
			oParameters.Add("game_item_seq",pGameItemSeq);
			oParameters.Add("pre_cooperation_point",sessionWoman.userWoman.CurCharacter.gameCharacter.cooperationPoint.ToString());
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ONCE_DAY_PRESENT_RESULT,oParameters);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}
}
