/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 自身の出演者動画コメント一覧
--	Progaram ID		: ListSelfCastMovieComment
--  Creation Date	: 2013.12.26
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListSelfCastMovieComment:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);

			if (string.IsNullOrEmpty(sMovieSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_CAST_MOVIE_COMMENT,this.ActiveForm);
		}
	}
}
