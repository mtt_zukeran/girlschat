﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板返信レス書き込み確認
--	Progaram ID		: ConfirmWriteBbsReturnRes
--
--  Creation Date	: 2011.04.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ConfirmWriteBbsReturnRes : MobileBbsExWomanBase {
	private string BbsResDoc {
		get { return iBridUtil.GetStringValue(this.ViewState["BbsResDoc"]); }
		set { this.ViewState["BbsResDoc"] = value; }
	}
	private string BbsResHandleNm {
		get {
			return iBridUtil.GetStringValue(this.ViewState["BbsResHandleNm"]);
		}
		set {
			this.ViewState["BbsResHandleNm"] = value;
		}
	}

	protected void Page_Load(object sender, EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,ActiveForm)) {
				this.BbsResHandleNm = sessionWoman.userWoman.bbsInfo.bbsResHandleNm;
				lblResHandleNm.Text = this.BbsResHandleNm;
				this.BbsResDoc = sessionWoman.userWoman.bbsInfo.bbsResDoc;
				lblResDoc.Text = this.BbsResDoc.Replace(System.Environment.NewLine,"<br>");
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBbsThread.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender, e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender, e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender, EventArgs e) {
		string sNewBbsThreadSubSeq = string.Empty;
		string sAnonymousFlag;
		if (this.chkAnonymous.Selection == null) {
			sAnonymousFlag = ViCommConst.FLAG_OFF_STR;
		} else {
			sAnonymousFlag = ViCommConst.FLAG_ON_STR;
		}

		using (BbsRes oBbsRes = new BbsRes()) {
			oBbsRes.WriteBbsRes(
				sessionWoman.site.siteCd,
				Request.QueryString["bbsthreadseq"],
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				Mobile.EmojiToCommTag(sessionObj.carrier,this.BbsResDoc),
				Request.QueryString["bbsthreadseq"],
				Request.QueryString["subseq"],
				sAnonymousFlag,
				Mobile.EmojiToCommTag(sessionObj.carrier,this.BbsResHandleNm),
				out sNewBbsThreadSubSeq);
		}
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("bbsthreadseq",Request.QueryString["bbsthreadseq"]);
		RedirectToDisplayDoc(ViCommConst.SCR_BBS_EX_RETURN_RES_COMPLETE,oParam);
	}

	protected void cmdReturn_Click(object sender, EventArgs e) {
		sessionWoman.userWoman.bbsInfo.bbsResDoc = this.BbsResDoc;
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("WriteBbsReturnRes.aspx?back=1&" + Request.QueryString.ToString()));
	}

	private string GetHandleNm() {
		string sBbsResHandleNm = sessionWoman.GetBbsResValue("BBS_RES_HANDLE_NM");
		string sHandleNm = sessionWoman.GetBbsResValue("HANDLE_NM");
		string sAnonymousFlag = sessionWoman.GetBbsResValue("ANONYMOUS_FLAG");

		if (ViCommConst.FLAG_ON_STR.Equals(sAnonymousFlag)) {
			return "匿名";
		} else {
			return !string.IsNullOrEmpty(sBbsResHandleNm) ? sBbsResHandleNm : sHandleNm;
		}
	}
}
