/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾒｯｾｰｼﾞ書き込み
--	Progaram ID		: WriteMessage
--
--  Creation Date	: 2010.07.23
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;

using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_woman_WriteMessage:MobileWomanPageBase {
	

	private string loginid = null;
	private int recNo;

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		loginid = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
		recNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

		if (!this.IsPostBack) {
			if (!sessionWoman.logined) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				}
				else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}
			DataRow dr;
			if (sessionWoman.SetManDataSetByLoginId(loginid,recNo,out dr) == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {

		// 入力チェック
		if (!this.ValidateForSubmit())
			return;

		string sMessageDoc = this.txtMessageDoc.Text.Trim();
		sMessageDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,sMessageDoc));
		sMessageDoc = MobileLib.Mobile.GetInputValue(sMessageDoc,4000,System.Text.Encoding.UTF8);

		DataRow dr;
		sessionWoman.SetManDataSetByLoginId(loginid,recNo,out dr);

		// 登録処理
		using (Message oMessage = new Message()) {
			oMessage.WriteMessage(
				sessionWoman.site.siteCd,
				ViCommConst.OPERATOR,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				iBridUtil.GetStringValue(dr["USER_SEQ"]),
				iBridUtil.GetStringValue(dr["USER_CHAR_NO"]),
				sMessageDoc);
		}

		// 完了画面へリダイレクト
		RedirectToMobilePage(
			sessionWoman.GetNavigateUrl(
				sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_WRITE_MESSAGE_COMPLITE + "&" + Request.QueryString));
	}

	/// <summary>
	/// ﾒｯｾｰｼﾞ登録前の入力検証を行う。
	/// </summary>
	/// <returns>すべての検証に成功した場合はtrue。それ以外はfalse。</returns>
	private bool ValidateForSubmit() {
		bool bIsValid = true;
		string sNGWord;
		lblErrorMessage.Text = string.Empty;

		string sMessageDoc = this.txtMessageDoc.Text.Trim();
		if (string.IsNullOrEmpty(sMessageDoc)) {
			bIsValid = false;
			//本文を入力して下さい。
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		if (sMessageDoc.Length > 300) {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MESSAGE_OVER_LEN);
			bIsValid = false;
		}

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (sessionWoman.ngWord.VaidateDoc(sMessageDoc,out sNGWord) == false) {
			bIsValid = false;
			//「{0}」の単語が不正です。
			lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
		}
		return bIsValid;
	}

}
