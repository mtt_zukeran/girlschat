/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サブアタックリスト
--	Progaram ID		: ListMailAttackSub
--
--  Creation Date	: 2013.04.05
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListMailAttackSub:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAIL_ATTACK_SUB,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSort = iBridUtil.GetStringValue(this.Request.Form["sort"]);
		string sNorxmail24h = iBridUtil.GetStringValue(this.Request.QueryString["norxmail24h"]);
		
		string sRedirectUrl = string.Format("ListMailAttackSub.aspx?sort={0}",sSort);
		
		if (!string.IsNullOrEmpty(sNorxmail24h)) {
			sRedirectUrl = sRedirectUrl + "&norxmail24h=" + sNorxmail24h;
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectUrl));
	}
}
