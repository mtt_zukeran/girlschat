/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフィール画像画面
--	Progaram ID		: VireCastRefuse
--
--  Creation Date	: 2010/10/29
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewCastRefuse : MobileWomanPageBase {

	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);

		if(!this.IsPostBack){
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_REFUSE,ActiveForm);
		}
	}
}
