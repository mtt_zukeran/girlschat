/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����̕�VRanking
--	Progaram ID		: ListYesterdayCastReport
--
--  Creation Date	: 2012.07.10
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListYesterdayCastReport:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_YESTERDAY_CAST_REPORT,this.ActiveForm);
		}
	}
}
