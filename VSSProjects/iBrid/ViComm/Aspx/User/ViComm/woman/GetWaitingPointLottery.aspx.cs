/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 待機ポイント抽選
--	Progaram ID		: GetWaitingPointLottery
--
--  Creation Date	: 2011.04.04
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_GetWaitingPointLottery:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sAddPointId = string.Empty;
			string sResult = string.Empty;

			using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
				oUserDefPoint.AddWaitingPoint(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,out sAddPointId,out sResult);
			}

			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("addpointid",System.Web.HttpUtility.UrlEncode(sAddPointId));
			oParam.Add("result",System.Web.HttpUtility.UrlEncode(sResult));

			//ポイント正常追加
			if (sResult.Equals(ViCommConst.UserDefPointResult.OK)) {
				RedirectToDisplayDoc(ViCommConst.SCR_GET_WAITING_POINT_SUCCESS,oParam);
			//ブラックリスト
			} else if (sResult.Equals(ViCommConst.UserDefPointResult.BLACKLIST)) {
				RedirectToDisplayDoc(ViCommConst.SCR_GET_WAITING_POINT_BLACK_LIST,oParam);
			//追加済み
			} else if (sResult.Equals(ViCommConst.UserDefPointResult.ADDED)) {
				RedirectToDisplayDoc(ViCommConst.SCR_GET_WAITING_POINT_ADDED,oParam);
			//待機してない
			} else if (sResult.Equals(ViCommConst.UserDefPointResult.NOT_WAITING)) {
				RedirectToDisplayDoc(ViCommConst.SCR_GET_WAITING_POINT_NO_WAIT,oParam);
			//その他設定がないなど
			} else {
				RedirectToDisplayDoc(ViCommConst.SCR_GET_WAITING_POINT_SYS_ERROR,oParam);
			}
		}
	}
}
