/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板写真・動画確認一覧
--	Progaram ID		: ListBbsObjConf
--
--  Creation Date	: 2010.09.10
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;

public partial class ViComm_woman_ListBbsObjConf:MobileObjWomanPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm);

			if (IsAvailableService(ViCommConst.RELEASE_CAST_BBS_OBJ_FIND) && sessionWoman.site.bbsMovieAttrFlag == ViCommConst.FLAG_ON) {
				CreateObjAttrComboBox(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,string.Empty,string.Empty,ViCommConst.ATTACHED_BBS.ToString(),true);
			} else {
				pnlSearchList.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 選択された値をセッションに設定して検索します。
		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;

		string[] selectedValueList = selectedValue.Split(',');

		string sAddQuery = string.Format("&attrtypeseq={0}&attrseq={1}",selectedValueList[0],selectedValueList[1]);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBbsObjConf.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&attrtypeseq=[0-9]*|&attrseq=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}
