﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewPaymentHistoryGiftCd.aspx.cs" Inherits="ViComm_woman_ViewPaymentHistoryGiftCd" %>

<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		$PGM_HTML02;
		$PGM_HTML03;
		$PGM_HTML04;
		$PGM_HTML05;
		$PGM_HTML06;
		$PGM_HTML07;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
