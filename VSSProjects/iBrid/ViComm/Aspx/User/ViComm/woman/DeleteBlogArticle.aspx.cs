/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ��۸ދL���폜
--	Progaram ID		: DeleteBlogArticle
--
--  Creation Date	: 2012.12.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteBlogArticle:MobileBlogWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		bool bHasData = sessionWoman.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE,ActiveForm);

		if(bHasData) {
			if (!IsPostBack) {
			} else {
				string sBlogArticleSeq = sessionWoman.GetBlogArticleValue("BLOG_ARTICLE_SEQ");
				if (IsPostAction(PwViCommConst.BUTTON_DELETE)) {
					using (BlogArticle oBlogArticle = new BlogArticle()) {
						oBlogArticle.DeleteBlogArticle(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sBlogArticleSeq
						);
					}
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DeleteBlogArticle.aspx?blog_article_seq={0}&delete=1",sBlogArticleSeq)));
				} else if (IsPostAction(ViCommConst.BUTTON_RETURN)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ViewBlogArticleConf.aspx?blog_article_seq={0}",sBlogArticleSeq)));
				}
			}
		}
	}
}