/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ギャラリー写真確認一覧(リスト表示)
--	Progaram ID		: ListProfilePicConf
--
--  Creation Date	: 2010.09.13
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListProfilePicConf:MobileObjWomanPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm);
			if (IsAvailableService(ViCommConst.RELEASE_CAST_BBS_OBJ_FIND) && sessionWoman.site.bbsPicAttrFlag == ViCommConst.FLAG_ON && IsAvailableService(ViCommConst.RELEASE_CAST_PIC_GALLERY_ATTR)) {
				string sPicType = ViCommConst.ATTACHED_PROFILE.ToString();
				if(iBridUtil.GetStringValue(Request.QueryString["hideonly"]).Equals(ViCommConst.FLAG_ON_STR)){
					sPicType = ViCommConst.ATTACHED_HIDE.ToString();
				}
				CreateObjAttrComboBox(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,string.Empty,ViCommConst.BbsObjType.PIC,sPicType,true);
			} else {
				pnlSearchList.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 選択された値をセッションに設定して検索します。
		if (lstGenreSelect.Visible) {
			string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
			string[] selectedValueList = selectedValue.Split(',');
			string sAddQuery = string.Format("&attrtypeseq={0}&attrseq={1}",selectedValueList[0],selectedValueList[1]);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListProfilePicConf.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListProfilePicConf.aspx?" + initializePageNo(Request.QueryString.ToString())));
		}
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&attrtypeseq=[0-9]*|&attrseq=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}
