/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ オーディションエントリー実行
--	Progaram ID		: RegistElectionEntryExecute
--
--  Creation Date	: 2013.12.11
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_RegistElectionEntryExecute:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		string sResult = string.Empty;
		
		using (ElectionEntry oElectionEntry = new ElectionEntry()) {
			oElectionEntry.RegistElectionEntry(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,out sResult);
		}
		
		if (sResult.Equals(PwViCommConst.ElectionEntryResult.RESULT_OK)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_REGIST_ELECTION_ENTRY_COMPLETE);
		} else if (sResult.Equals(PwViCommConst.ElectionEntryResult.RESULT_NO_PROFILE_PIC)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ELECTION_ENTRY_NG_NO_PROFILE_PIC);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}