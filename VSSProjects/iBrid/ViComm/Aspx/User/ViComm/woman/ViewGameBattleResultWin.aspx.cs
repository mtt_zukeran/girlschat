/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@�ʏ����ُ���
--	Progaram ID		: ViewGameBattleResultWin
--
--  Creation Date	: 2011.09.23
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_woman_ViewGameBattleResultWin:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.checkRecentBattleLog();

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_WIN,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	private void checkRecentBattleLog() {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition(this.Request.QueryString);
		oCondition.SiteCd = this.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.sessionWoman.userWoman.curCharNo;
		oCondition.AttackWinFlag = ViCommConst.FLAG_ON_STR;

		using (BattleLog oBattleLog = new BattleLog()) {
			if (!oBattleLog.CheckRecentBattleLog(oCondition)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}
}
