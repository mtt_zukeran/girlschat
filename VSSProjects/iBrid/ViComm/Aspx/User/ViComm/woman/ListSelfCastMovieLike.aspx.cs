/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 自身の出演者動画イイネ一覧
--	Progaram ID		: ListSelfCastMovieLike
--  Creation Date	: 2013.12.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListSelfCastMovieLike:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);

			if (string.IsNullOrEmpty(sMovieSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			// 新着いいね通知 確認
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["pageno"])) ||
				iBridUtil.GetStringValue(Request.QueryString["pageno"]).Equals("1")) {
				using (CastLikeNotice oCastLikeNotice = new CastLikeNotice()) {
					oCastLikeNotice.CastLikeNoticeConfirm(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_MOVIE,
						sMovieSeq,
						string.Empty,
						string.Empty
					);
				}
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_CAST_MOVIE_LIKE,this.ActiveForm);
		}
	}
}
