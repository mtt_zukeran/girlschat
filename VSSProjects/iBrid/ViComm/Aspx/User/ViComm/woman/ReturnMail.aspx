<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReturnMail.aspx.cs" Inherits="ViComm_woman_ReturnMail" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
	    <mobile:Panel ID="pnlTemplate" Runat="server" BreakAfter="False">
			<cc1:iBMobileLabel ID="lblTemplate" runat="server">Ұ�����ڰ�</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstTemplate" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<cc1:iBMobileLabel ID="lblTitle" runat="server">����</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24" EmojiPaletteEnabled="true" ></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblDoc" runat="server">�{��</cc1:iBMobileLabel>
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true" >
		</tx:TextArea>
		<br />
		$PGM_HTML05;
		$PGM_HTML06;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
