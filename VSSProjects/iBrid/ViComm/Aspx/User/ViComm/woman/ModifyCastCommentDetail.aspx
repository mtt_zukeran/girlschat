<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyCastCommentDetail.aspx.cs" Inherits="ViComm_woman_ModifyCastCommentDetail" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlWrite" Runat="server">
			$PGM_HTML02;
			<tx:TextArea ID="txtCommentDetail" runat="server" Row="10" Rows="6" Columns="24" EmojiPaletteEnabled="true" BreakBefore="false" BrerakAfter="false"></tx:TextArea>
			$PGM_HTML03;
		</mobile:Panel>
		<mobile:Panel ID="pnlConfirm" Runat="server">
			$PGM_HTML04;
			<ibrid:iBMobileLabel ID="lblCommentDetail" runat="server"></ibrid:iBMobileLabel>
			$PGM_HTML05;
		</mobile:Panel>
		<mobile:Panel ID="pnlComplete" Runat="server">
			$PGM_HTML06;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
