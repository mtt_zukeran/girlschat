/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・贈り先仲間一覧
--	Progaram ID		: ListGameCharacterFellowPresent
--
--  Creation Date	: 2011.09.27
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameCharacterFellowPresent:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		if (!sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
			oParameters.Add("action",PwViCommConst.GameWomanLiveChatNoApplyAction.PRESENT);
			RedirectToGameDisplayDoc(ViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY,oParameters);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_PRESENT_LIST,ActiveForm);
	}
}
