﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザー掲示板詳細表示
--	Progaram ID		: ViewUserBbs
--
--  Creation Date	: 2010.04.22
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewUserBbs:MobileWomanPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_BBS_DOC,ActiveForm)) {

				string loginId = sessionWoman.GetManBbsDocValue("LOGIN_ID");

				// プロフィール表示クエリ用に男性のデータを検索します。

				sessionWoman.SetManDataSetByLoginId(loginId,1);

				// 閲覧数を更新します。

				using (UserManBbs oUserManBbs = new UserManBbs()) {
					oUserManBbs.UpdateReadingCount(
						sessionWoman.site.siteCd,
						sessionWoman.GetManBbsDocValue("USER_SEQ"),
						sessionWoman.GetManBbsDocValue("USER_CHAR_NO"),
						sessionWoman.GetManBbsDocValue("BBS_SEQ")
					);
				}
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListUserBbs.aspx"));
			}
		}
	}
}
