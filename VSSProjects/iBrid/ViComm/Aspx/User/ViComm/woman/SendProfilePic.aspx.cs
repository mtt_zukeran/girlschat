﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフ画像送信設定
--	Progaram ID		: SendProfilePic
--
--  Creation Date	: 2010.09.24
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;
using System.Web;

public partial class ViComm_woman_SendProfilePic:MobileObjWomanPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm);

			if (!IsAvailableService(ViCommConst.RELEASE_BLUR_MODIFY_REQUEST)) {
				pnlBlur.Visible = false;
			}
			if (!IsAvailableService(ViCommConst.RELEASE_CAST_PIC_GALLERY_ATTR)) {
				pnlSearchList.Visible = false;
			} else if (!iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]).Equals(string.Empty) && !iBridUtil.GetStringValue(Request.QueryString["attrseq"]).Equals(string.Empty)) {
				//前画面よりリンクにてカテゴリ選択した場合リストボックスは表示させない

				pnlSearchList.Visible = false;
			} else {
				CreateObjAttrComboBox(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,string.Empty,ViCommConst.BbsObjType.PIC,ViCommConst.ATTACHED_PROFILE.ToString(),false);
			}
			if (IsAvailableService(ViCommConst.RELEASE_PROFILE_PIC_AUTH)) {
				pnlBlur.Visible = false;
				chkModifyRequestFlag.Items[0].Selected = true;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (ValidateInput()) {
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text));
			string sModifyRequestSpec = string.Empty;
			string sAttrTypeSeq = ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString();
			string sAttrSeq = ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString();
			string sObjTempId = string.Empty;
			string sPicType = string.Empty;
			int iProfileFlag = ViCommConst.FLAG_OFF;
			int iModifyRequestFlag = ViCommConst.FLAG_OFF;
			int iObjRankingFlag = ViCommConst.FLAG_ON;

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]))) {
				int.TryParse(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]),out iObjRankingFlag);
			}


			if (rdoPicType.Items[0].Selected) {
				//ｷﾞｬﾗﾘｰ用
				sPicType = ViCommConst.ATTACHED_PROFILE.ToString();
			} else if (rdoPicType.Items[1].Selected) {
				//ﾌﾟﾛﾌ用
				sPicType = ViCommConst.ATTACHED_PROFILE.ToString();
				iProfileFlag = ViCommConst.FLAG_ON;
			} else {
				//非表示
				sPicType = ViCommConst.ATTACHED_HIDE.ToString();
			}

			if (chkModifyRequestFlag.Items[0].Selected) {
				iModifyRequestFlag = ViCommConst.FLAG_ON;
				sModifyRequestSpec = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtModifyRequestSpec.Text));
			}

			if (pnlSearchList.Visible) {
				string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
				string[] selectedValueList = selectedValue.Split(',');

				sAttrTypeSeq = selectedValueList[0];
				sAttrSeq = selectedValueList[1];
			} else {
				string sQueryAttrTypeSeq = iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]);
				string sQueryAttrSeq = iBridUtil.GetStringValue(Request.QueryString["attrseq"]);

				if (!sQueryAttrTypeSeq.Equals(string.Empty)) {
					sAttrTypeSeq = sQueryAttrTypeSeq;
				}
				if (!sQueryAttrSeq.Equals(string.Empty)) {
					sAttrSeq = sQueryAttrSeq;
				}
			}

			using (WaitObj oWaitObj = new WaitObj()) {
				oWaitObj.WriteWaitObj(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					ViCommConst.ATTACH_PIC_INDEX.ToString(),
					sTitle,
					sDoc,
					sPicType,
					sAttrTypeSeq,
					sAttrSeq,
					iModifyRequestFlag,
					sModifyRequestSpec,
					iProfileFlag,
					iObjRankingFlag,
					out sObjTempId
				);
			}

			sessionWoman.userWoman.ObjTempId = sObjTempId;
			RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_PIC_PROFILE_MAILER_COMPLITE);
		}
	}

	virtual protected bool ValidateInput() {
		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}
		string sNGWord = string.Empty;


		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (txtTitle.Visible && txtTitle.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE) + "<br />";
		}
		if (txtDoc.Visible && txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC) + "<br />";
		}
		if (sessionWoman.ngWord.VaidateDoc(string.Concat(txtDoc.Visible ? txtDoc.Text : string.Empty,txtTitle.Visible ? txtTitle.Text : string.Empty),out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
		}
		if (pnlSearchList.Visible) {
			string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
			string[] selectedValueList = selectedValue.Split(',');

			string sAttrTypeSeq = selectedValueList[0];
			string sAttrSeq = selectedValueList[1];

			if (sAttrTypeSeq.Equals(string.Empty) || sAttrSeq.Equals(string.Empty)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_OBJ_ATTR) + "<br />";
			}
		}
		return bOk;
	}
}
