/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 報酬確認カレンダー
--	Progaram ID		: ListPerformanceDaily
--
--  Creation Date	: 2014.10.20
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListPerformanceDaily:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		string sReportYear = iBridUtil.GetStringValue(this.Request.QueryString["year"]);
		string sReportMonth = iBridUtil.GetStringValue(this.Request.QueryString["month"]);
		
		if (!(string.IsNullOrEmpty(sReportYear) && string.IsNullOrEmpty(sReportMonth))) {
			DateTime dtReportMonth;
			
			if (string.IsNullOrEmpty(sReportYear) || string.IsNullOrEmpty(sReportMonth)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			} else if (!DateTime.TryParse(string.Format("{0}/{1}/01",sReportYear,sReportMonth),out dtReportMonth)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_PERFORMANCE_DAILY,this.ActiveForm);
	}
}