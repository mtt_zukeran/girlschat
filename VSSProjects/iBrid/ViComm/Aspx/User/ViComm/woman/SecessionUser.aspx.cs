/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 退会
--	Progaram ID		: SecessionUser
--
--  Creation Date	: 2010.10.14
--  Creater			: i-Brid(H.Kiyotou)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Collections;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_SecessionUser:MobileWomanPageBase {

	private string withdrawalSeq;

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		// 退会済み会員をリダイレクトさせる
		RedirectWithdrawalUser();

		if (!IsPostBack) {
			// 退会済み以外＆退会申請中以外の場合、退会申請レコードを生成する
			// (選択された理由の登録)
			if (!sessionWoman.userWoman.userStatus.Equals(ViCommConst.USER_WOMAN_RESIGNED)
				&& !sessionWoman.userWoman.existAcceptWithdrawalFlag.Equals(ViCommConst.FLAG_ON_STR)
			) {
				withdrawalSeq = string.Empty;
				sessionWoman.userWoman.WithdrawalMainte(ref withdrawalSeq,iBridUtil.GetStringValue(Request.QueryString["reason"]));
				ViewState["WITHDRAWAL_SEQ"] = withdrawalSeq;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				// 退会済み以外＆退会申請中以外の場合、退会申請処理を行う
				if (!sessionWoman.userWoman.userStatus.Equals(ViCommConst.USER_WOMAN_RESIGNED)
					&& !sessionWoman.userWoman.existAcceptWithdrawalFlag.Equals(ViCommConst.FLAG_ON_STR)
				) {
					cmdSubmit_Click(sender,e);
				}
				// 上記以外の場合、退会申請完了画面へ遷移させる
				else {
					RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_ACCEPT_WITHDRAWAL_COMPLETE);
				}
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = string.Empty;
		string sResult = string.Empty;
		string url = string.Empty;
		string sDrawalDoc = string.Empty;
		bool bSendMail = false;

		string sSendMailAddr = string.Empty;
		ArrayList list = new ArrayList();

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.Equals("*mail")) {
				sSendMailAddr = Request.Form[sKey];
			} else if (sKey.StartsWith("*withdrawaldoc")) {
				list.Add(Request.Form[sKey]);
				sDrawalDoc = Request.Form[sKey];
				if (!this.CheckDrawalDoc(sDrawalDoc)) {
					return;
				}
			} else if (sKey.StartsWith("*")) {
				list.Add(Request.Form[sKey]);
			}
		}

		if (iBridUtil.GetStringValue(Request.QueryString["onlyaccept"]).Equals(ViCommConst.FLAG_ON_STR)) {
			withdrawalSeq = iBridUtil.GetStringValue(ViewState["WITHDRAWAL_SEQ"]);
			sessionWoman.userWoman.WithdrawalMainte(ref withdrawalSeq,sDrawalDoc);
			sResult = "0";
		} else {
			sessionWoman.userWoman.SecessionUser(out sResult);
		}

		if (sResult.Equals("0")) {
			// 退会申請中フラグをたてる
			sessionWoman.userWoman.existAcceptWithdrawalFlag = ViCommConst.FLAG_ON_STR;

			// メール送信の有無を判定(手動で退会処理を行うかの判定)
			bSendMail = IsSendMail();
			if (bSendMail) {
				// 手動対応フラグをたてる
				sessionWoman.userWoman.UpdateManualWithdrawal(withdrawalSeq);

				// 送信先のメールアドレスがあればメールを送信する
				if (!sSendMailAddr.Equals(string.Empty)) {
					string sDoc = string.Empty;
					sDoc += string.Format("退会者ログインID:{0}\n",sessionWoman.userWoman.loginId);
					sDoc += string.Format("退会者メールアドレス：{0}\n",sessionWoman.userWoman.emailAddr);
					sDoc += string.Format("退会者ハンドルネーム：{0}\n\n",sessionWoman.userWoman.castNm);
					string[] sDocList = (string[])list.ToArray(typeof(string));
					sDoc += string.Join("\n\n",sDocList);

					string sCastNm = string.Empty;
					sCastNm = "出演者";

					string sSubject;
					if (iBridUtil.GetStringValue(Request.QueryString["onlyaccept"]).Equals(ViCommConst.FLAG_ON_STR)) {
						sSubject = string.Format("{0}退会申請",sCastNm);
					} else {
						sSubject = string.Format("{0}が退会しました",sCastNm);
					}
					SendMail(sSendMailAddr,sSendMailAddr,sSubject,sDoc);
				}
			}
			if (!iBridUtil.GetStringValue(Request.QueryString["onlyaccept"]).Equals(ViCommConst.FLAG_ON_STR)) {
				url = sessionWoman.GetNavigateUrlNoSession("Start.aspx?goto=DisplayDoc.aspx&doc=" + ViCommConst.SCR_WOMAN_SECESSION_COMPLITE +
						"&tran=secession&sc=" + sessionWoman.site.siteCd +
						"&host=" + Request.Url.Host);
				Session.Abandon();
				RedirectToMobilePage(url);
			} else {
				RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_ACCEPT_WITHDRAWAL_COMPLETE);
			}
		} else {
			lblErrorMessage.Text += GetErrorMessage(sResult);
		}
	}

	protected virtual bool CheckDrawalDoc(string pDrawalDoc) {
		if (pDrawalDoc.Equals(string.Empty)) {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_WITHDRAWAL_REASON_DOC_EMPTY);
			return false;
		}

		return true;
	}

	/// <summary>
	/// 退会済みの会員の場合のリダイレクト処理
	/// </summary>
	private void RedirectWithdrawalUser() {
		string url = string.Empty;
		string userStatus = string.Empty;
		string existsAccept = string.Empty;
		string detainDate = string.Empty;
		User oUser = new User();
		DataSet ds = oUser.GetWithdrawalStatus(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
		DataRow dr = null;
		if (ds.Tables[0].Rows.Count <= 0) {
			// ユーザ情報が取得できなかった場合
			sessionWoman.userWoman.existAcceptWithdrawalFlag = string.Empty;
			sessionWoman.userWoman.lastWithdrawalDetainDate = string.Empty;
			return;
		}
		dr = ds.Tables[0].Rows[0];
		userStatus = dr["USER_STATUS"].ToString();
		existsAccept = dr["EXISTS_ACCEPT"].ToString();
		detainDate = dr["LAST_WITHDRAWAL_DETAIN_DATE"].ToString();

		// セッションに保存（パース処理での判定時に利用）
		sessionWoman.userWoman.userStatus = userStatus;
		sessionWoman.userWoman.existAcceptWithdrawalFlag = existsAccept;
		sessionWoman.userWoman.lastWithdrawalDetainDate = detainDate;

		// 出演者ステータスが退会の場合
		if (userStatus.Equals(ViCommConst.USER_WOMAN_RESIGNED)) {
			url = sessionWoman.GetNavigateUrlNoSession("Start.aspx");
			Session.Abandon();
			RedirectToMobilePage(url);
		}
	}

	/// <summary>
	/// メール送信の有無を判定(手動で退会処理を行うかの判定)
	/// </summary>
	/// <returns></returns>
	private bool IsSendMail() {
		// この娘を探せの当日以降のピックアップ対象かどうかを取得
		InvestigateTargetSeekCondition oCondition = new InvestigateTargetSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
		oCondition.IncludingExecutionDayOrLaterFlag = ViCommConst.FLAG_ON_STR;
		InvestigateTarget oInvestigateTarget = new InvestigateTarget();
		DataSet dsIT = oInvestigateTarget.GetOne(oCondition);
		if (dsIT.Tables[0].Rows.Count > 0) {
			return true;
		}

		// 最終退会引きとめ処理日時が存在する場合
		if (!string.IsNullOrEmpty(sessionWoman.userWoman.lastWithdrawalDetainDate)) {
			return false;
		}

		// 清算履歴を取得
		PaymentHistory oPaymentHistory = new PaymentHistory();
		DateTime paymentDate = new DateTime();
		bool bExist = oPaymentHistory.GetLastPaymentDate(sessionWoman.userWoman.userSeq,out paymentDate);
		if (bExist) {
			// 清算履歴が存在する場合
			return false;
		}

		// 未清算ポイントを取得
		Cast oCast = new Cast();
		int iPaymentAmt = 0;
		int iPaymentPoint = 0;
		DataSet dsC = oCast.GetNotPayment(sessionWoman.userWoman.userSeq);
		if (dsC.Tables[0].Rows.Count > 0) {
			DataRow drC = dsC.Tables[0].Rows[0];
			iPaymentAmt = int.Parse(drC["NOT_PAYMENT_AMT"].ToString());
			iPaymentPoint = int.Parse(drC["NOT_PAYMENT_POINT"].ToString());
		}
		if (iPaymentAmt >= 2000) {
			return false;
		}

		// 3日以内のメール受信履歴を取得
		MailLog oMailLog = new MailLog();
		string sFromDate = DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd HH:mm:ss");
		DataSet dsML = oMailLog.GetTermRxMail(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.FLAG_OFF_STR,sFromDate);
		if (dsML.Tables[0].Rows.Count > 0) {
			return true;
		}

		// 3日以内の通話履歴を取得
		UsedLog oUsedLog = new UsedLog();
		DataSet dsUL = oUsedLog.GetTermTalkLog(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sFromDate);
		if (dsUL.Tables[0].Rows.Count > 0) {
			return true;
		}
		return false;
	}
}
