/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエストエントリー実行
--	Progaram ID		: RegistGameQuestEntryExecute
--
--  Creation Date	: 2012.07.16
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_woman_RegistGameQuestEntryExecute:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["quest_seq"]);
		string sQuestType = iBridUtil.GetStringValue(this.Request.QueryString["quest_type"]);
		string sLevelQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["level_quest_seq"]);
		string sLittleQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["little_quest_seq"]);
		string sOtherQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["other_quest_seq"]);
		string sEntryLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["entry_log_seq"]);
		string sRetireFlag = iBridUtil.GetStringValue(this.Request.QueryString["retire_flag"]);
		string sScrid = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);
		string sEntryClearFlag = string.Empty;
		string sLevelQuesrClearFlag = string.Empty;
		string sResult = string.Empty;

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sQuestSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
		
		rgxMatch = rgx.Match(sQuestType);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		if (sQuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
			rgxMatch = rgx.Match(sLevelQuestSeq);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}

			rgxMatch = rgx.Match(sLittleQuestSeq);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else if (sQuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
			rgxMatch = rgx.Match(sLevelQuestSeq);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else if (sQuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
			rgxMatch = rgx.Match(sOtherQuestSeq);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}

		using (Quest oQuest = new Quest()) {
			oQuest.QuestEntryExe(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR,
				sQuestSeq,
				sQuestType,
				sLevelQuestSeq,
				sLittleQuestSeq,
				sOtherQuestSeq,
				sRetireFlag,
				ref sEntryLogSeq,
				out sEntryClearFlag,
				out sLevelQuesrClearFlag,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.GameQuestEntryResult.RESULT_OK)) {

			if (sEntryClearFlag.Equals(ViCommConst.FLAG_ON_STR)) {

				if (sLevelQuesrClearFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					using (Quest oQuest = new Quest()) {
						oQuest.QuestEntryExe(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							ViCommConst.OPERATOR,
							sQuestSeq,
							PwViCommConst.GameQuestType.EX_QUEST,
							sLevelQuestSeq,
							string.Empty,
							string.Empty,
							ViCommConst.FLAG_OFF_STR,
							ref sEntryLogSeq,
							out sEntryClearFlag,
							out sLevelQuesrClearFlag,
							out sResult
						);
					}
				}
				
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,string.Format("ListGameQuestRewardGet.aspx?quest_seq={0}&scrid={1}",sQuestSeq,sScrid)));
			}
			
			IDictionary<string,string> oParameters = new Dictionary<string,string>();
			oParameters.Add("quest_seq",sQuestSeq);
			oParameters.Add("quest_type",sQuestType);
			oParameters.Add("level_quest_seq",sLevelQuestSeq);
			oParameters.Add("little_quest_seq",sLittleQuestSeq);
			oParameters.Add("other_quest_seq",sOtherQuestSeq);
			oParameters.Add("entry_log_seq",sEntryLogSeq);
			oParameters.Add("retire_flag",sRetireFlag);

			RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_WOMAN_QUEST_ENTRY_RESULT,oParameters);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}
}
