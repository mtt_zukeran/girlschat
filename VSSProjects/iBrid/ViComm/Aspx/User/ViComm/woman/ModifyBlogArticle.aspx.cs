/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾞﾛｸﾞ記事 更新
--	Progaram ID		: ModifyBlogArticle
--
--  Creation Date	: 2011.04.05
--  Creater			: K.Itoh
--
**************************************************************************/

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;


using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_woman_ModifyBlogArticle : MobileBlogWomanBase {
	protected override bool NeedBlogRegistPermission { get { return true; } }


	protected const string PARAM_NM_OBJ_SEQ = "obj_seq";
	protected const string PARAM_NM_BLOG_ARTICLE_SEQ = "blog_article_seq";

	#region □■□ Properties □■□ ==================================================================================
	public string BlogArticleSeq {
		get { return this.ViewState["BlogArticleSeq"] as string; }
		set { this.ViewState["BlogArticleSeq"] = value; }
	}
	public string ObjSeq {
		get { return this.ViewState["ObjSeq"] as string; }
		set { this.ViewState["ObjSeq"] = value; }
	}
	#endregion ========================================================================================================


	#region □■□ Page Event Handler □■□ ==========================================================================

	protected virtual void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);

		string sObjSeq = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_OBJ_SEQ]);
		string sBlogArticleSeq = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_BLOG_ARTICLE_SEQ]);
		
		if(string.IsNullOrEmpty(sObjSeq) && string.IsNullOrEmpty(sBlogArticleSeq)){
			throw new ArgumentException();
		}
		if (!IsPostBack) {
			if(!string.IsNullOrEmpty(sBlogArticleSeq)){
				if(!string.IsNullOrEmpty(sObjSeq)){
					AppendObj(sBlogArticleSeq,sObjSeq);
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ModifyBlogArticle.aspx?blog_article_seq={0}",sBlogArticleSeq)));
				}
				// 編集モード
				this.DisplayEditMode(sBlogArticleSeq);
			}else{
				// 新規モード
				this.DisplayNewMode(sObjSeq);
			}
		}else{
			if (IsPostAction(ViCommConst.BUTTON_GOTO_MOVIE)) {
				if (!this.ValidateForModfiy()) return;
				sBlogArticleSeq = this.Modify();
				if (string.IsNullOrEmpty(sBlogArticleSeq)) return;
				RedirectToMobilePage(sessionObj.GetNavigateUrl(string.Format("ListBlogMovieConf.aspx?append_blog_article_seq={0}", sBlogArticleSeq)));
			} else if (IsPostAction(ViCommConst.BUTTON_GOTO_PIC)) {
				if (!this.ValidateForModfiy()) return;
				sBlogArticleSeq = this.Modify();
				if (string.IsNullOrEmpty(sBlogArticleSeq)) return;
				RedirectToMobilePage(sessionObj.GetNavigateUrl(string.Format("ListBlogPicConf.aspx?append_blog_article_seq={0}", sBlogArticleSeq)));
			} else if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				if (!this.ValidateForModfiy()) return;
				sBlogArticleSeq = this.Modify();
				if (string.IsNullOrEmpty(sBlogArticleSeq)) return;
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ViewBlogArticleConf.aspx?blog_article_seq={0}", sBlogArticleSeq)));
			} 
		}

		sessionWoman.ControlList(this.Request, ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ, this.ActiveForm);
	}

	#endregion ========================================================================================================
	
	protected void AppendObj(string pBlogArticleSeq, string pObjSeq){		
		using(BlogArticle oBlogArticle = new BlogArticle()){
			oBlogArticle.AppendObj(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sessionWoman.userWoman.CurCharacter.characterEx.blogSeq,
				pBlogArticleSeq,
				pObjSeq				
			);
		} 
	}

	public string Modify(){
		string sBlogArticleTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier, this.txtBlogArticleTitle.Text.Trim()));
		string sBlogArticleDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier, this.txtBlogArticleDoc.Text.Trim()));
		if (sBlogArticleDoc.Length > 10000) {
			sBlogArticleDoc = SysPrograms.Substring(sBlogArticleDoc, 10000);
		}
		
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BLOG_ARTICLE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sessionWoman.site.siteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, sessionWoman.userWoman.userSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, sessionWoman.userWoman.curCharNo);
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.NUMBER, sessionWoman.userWoman.CurCharacter.characterEx.blogSeq);
			oDbSession.ProcedureBothParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.NUMBER, this.BlogArticleSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_TITLE", DbSession.DbType.VARCHAR2, sBlogArticleTitle);
			oDbSession.ProcedureInArrayParm("pBLOG_ARTICLE_BODY", DbSession.DbType.VARCHAR2, SysPrograms.SplitBytes(Encoding.UTF8, sBlogArticleDoc, 2500));
			oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.VARCHAR2, this.ObjSeq);
			oDbSession.ProcedureOutParm("pRESULT", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			switch (oDbSession.GetStringValue("pRESULT")) {
				case ViCommConst.ModifyBlogArticleResult.RESULT_OBJ_ALREADY_USED:
					this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_OBJ_ALREADY_USED);
					return string.Empty;
			}

			return oDbSession.GetStringValue("pBLOG_ARTICLE_SEQ");
		}		
	}
	
	protected bool ValidateForModfiy(){
		bool bIsValid = true;
		StringBuilder oErrMsgBuilder = new StringBuilder();

		string sBlogArticleTitle = this.txtBlogArticleTitle.Text.Trim();
		string sBlogArticleDoc = this.txtBlogArticleDoc.Text.Trim();

		// 必須入力チェック
		if (string.IsNullOrEmpty(sBlogArticleTitle.Replace(Environment.NewLine, string.Empty))) {
			bIsValid = false;
			oErrMsgBuilder.Append(GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_ARTICLE_TITLE_EMPTY));
		}
		if (string.IsNullOrEmpty(sBlogArticleDoc.Replace(Environment.NewLine, string.Empty))) {
			bIsValid = false;
			oErrMsgBuilder.Append(GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_ARTICLE_BODY_EMPTY));
		}
		// 文字長チェック
		if (bIsValid) {
			if (sBlogArticleTitle.Length > 50) {
				bIsValid = false;
				oErrMsgBuilder.Append(string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_TITLE_LENGTH), "50文字"));
			}
		}
		// NGワードチェック
		if (bIsValid) {
			string sNGWord;
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}

			if (!sessionWoman.ngWord.VaidateDoc(sBlogArticleTitle + sBlogArticleDoc, out sNGWord)) {
				bIsValid = false;
				oErrMsgBuilder.Append(string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD), sNGWord));

			}
		}
		if(!bIsValid){
			this.lblErrorMessage.Text = oErrMsgBuilder.ToString();
		}
		
		return bIsValid;
	}
	
	public void DisplayEditMode(string pBlogArticleSeq){
		using(DbSession oDbSession = new DbSession()){
			oDbSession.PrepareProcedure("BLOG_ARTICLE_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sessionWoman.site.siteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, sessionWoman.userWoman.userSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, sessionWoman.userWoman.curCharNo);
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.NUMBER, sessionWoman.userWoman.CurCharacter.characterEx.blogSeq);
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.NUMBER, pBlogArticleSeq);
			oDbSession.ProcedureOutParm("pBLOG_ARTICLE_TITLE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pBLOG_ARTICLE_BODY", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pOBJ_SEQ", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			if (oDbSession.GetDecimalValue("pRECORD_COUNT") > decimal.Zero) {
				this.BlogArticleSeq = pBlogArticleSeq;

				this.txtBlogArticleTitle.Text = oDbSession.GetStringValue("pBLOG_ARTICLE_TITLE");
				this.txtBlogArticleDoc.Text = oDbSession.GetArrayStringConcatValue("pBLOG_ARTICLE_BODY");
				this.ObjSeq = oDbSession.GetStringValue("pOBJ_SEQ");
			}else{
				throw new ApplicationException(string.Format("BLOG_ARTICLE Not Found.blog_article_seq:{0}",pBlogArticleSeq));
			}			
		}


		BlogObjSeekCondition oCondition = new BlogObjSeekCondition();
		oCondition.SeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ;
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.BlogSeq = sessionWoman.userWoman.CurCharacter.characterEx.blogSeq;
		oCondition.BlogArticleSeq = this.BlogArticleSeq;
		using (BlogObj oBlogObj = new BlogObj()) {
			using (DataSet oBlogObjDataSet = oBlogObj.GetPageCollection(oCondition, 1, int.MaxValue)) {
			
				List<int> oMatchIndexList = new List<int>();
				foreach(Match oMatch in BlogHelper.RegexBlogArticleTag.Matches(this.txtBlogArticleDoc.Text)){
					oMatchIndexList.Add(int.Parse(oMatch.Groups["index"].Value) - 1);					
				}
				
				for(int i=0;i<oBlogObjDataSet.Tables[0].Rows.Count;i++){
					if(!oMatchIndexList.Contains(i)){
						this.txtBlogArticleDoc.Text += string.Format("{1}＃＃＃{0}＃＃＃", i + 1, Environment.NewLine);
					}
				}
			}
		}
	}
	
	public void DisplayNewMode(string pObjSeq){
		this.ObjSeq = pObjSeq;
		this.txtBlogArticleDoc.Text = "＃＃＃1＃＃＃";
	}
	

}
