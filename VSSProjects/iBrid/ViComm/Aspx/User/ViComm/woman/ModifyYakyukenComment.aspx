<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyYakyukenComment.aspx.cs" Inherits="ViComm_woman_ModifyYakyukenComment" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
	    <ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		$PGM_HTML02;
		<tx:TextArea ID="txtCommentText" runat="server" Row="4" BreakBefore="false" BrerakAfter="true" Rows="4" Columns="24" MaxLength="100" EmojiPaletteEnabled="true">
		</tx:TextArea>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
