﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板レス書き込み確認
--	Progaram ID		: ConfirmWriteBbsRes
--
--  Creation Date	: 2011.04.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ConfirmWriteBbsRes:MobileBbsExWomanBase {
	private string BbsResDoc {
		get {
			return iBridUtil.GetStringValue(this.ViewState["BbsResDoc"]);
		}
		set {
			this.ViewState["BbsResDoc"] = value;
		}
	}
	private string BbsResHandleNm {
		get {
			return iBridUtil.GetStringValue(this.ViewState["BbsResHandleNm"]);
		}
		set {
			this.ViewState["BbsResHandleNm"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,ActiveForm)) {
				this.BbsResHandleNm = sessionWoman.userWoman.bbsInfo.bbsResHandleNm;
				lblResHandleNm.Text = this.BbsResHandleNm;
				this.BbsResDoc = sessionWoman.userWoman.bbsInfo.bbsResDoc;
				lblResDoc.Text = sessionWoman.userWoman.bbsInfo.bbsResDoc.Replace(System.Environment.NewLine,"<br>");
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBbsThread.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sNewBbsThreadSubSeq = string.Empty;
		string sAnonymousFlag;
		if (this.chkAnonymous.Selection == null) {
			sAnonymousFlag = ViCommConst.FLAG_OFF_STR;
		} else {
			sAnonymousFlag = ViCommConst.FLAG_ON_STR;
		}

		using (BbsRes oBbsRes = new BbsRes()) {
			oBbsRes.WriteBbsRes(
				sessionWoman.site.siteCd,
				Request.QueryString["bbsthreadseq"],
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				Mobile.EmojiToCommTag(sessionObj.carrier,this.BbsResDoc),
				string.Empty,
				string.Empty,
				sAnonymousFlag,
				Mobile.EmojiToCommTag(sessionObj.carrier,this.BbsResHandleNm),
				out sNewBbsThreadSubSeq);
		}
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("bbsthreadseq",Request.QueryString["bbsthreadseq"]);
		RedirectToDisplayDoc(ViCommConst.SCR_BBS_EX_RES_COMPLETE,oParam);
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionWoman.userWoman.bbsInfo.bbsResDoc = this.BbsResDoc;
		sessionWoman.userWoman.bbsInfo.bbsResHandleNm = this.BbsResHandleNm;
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("WriteBbsRes.aspx?back=1&" + Request.QueryString.ToString()));
	}
}
