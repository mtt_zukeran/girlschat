/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・報酬獲得ニュース
--	Progaram ID		: ListGamePaymentLogDetail
--
--  Creation Date	: 2012.10.09
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_woman_ListGamePaymentLogDetail:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		string sGamePaymentType = iBridUtil.GetStringValue(this.Request.QueryString["game_payment_type"]);

		if(!string.IsNullOrEmpty(sGamePaymentType)) {
			Regex rgx = new Regex("^[0-9]+$");
			Match rgxMatch = rgx.Match(sGamePaymentType);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
		
		int iGamePaymentType;
		int.TryParse(sGamePaymentType,out iGamePaymentType);
		
		if (!IsPostBack) {
			if(iGamePaymentType <= 4) {
				lstGamePaymentType.SelectedIndex = iGamePaymentType;
			} else {
				lstGamePaymentType.SelectedIndex = 0;
			}
			
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_PAYMENT_LOG_DETAIL,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e) {
		string sGamePaymentType = HttpUtility.HtmlEncode(lstGamePaymentType.Selection.Value);

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,String.Format("ListGamePaymentLogDetail.aspx?game_payment_type={0}",sGamePaymentType)));
	}
}