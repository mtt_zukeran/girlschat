/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 添付動画選択一覧
--	Progaram ID		: ListMailMovie
--
--  Creation Date	: 2009.08.13
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListMailMovie:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;

		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sMovieSeq) && sPlayMovie.Equals("1")) {
				bDownload = MovieHelper.Download(sMovieSeq,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			};

			if (bDownload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_CAST_MAIL_MOVIE_STOCK,ActiveForm);
			}
		}
	}
}