/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���[���폜
--	Progaram ID		: DeleteMail
--
--  Creation Date	: 2010.07.29
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_DeleteMail:MobileWomanPageBase {
	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sDeleteMailType = iBridUtil.GetStringValue(Request.QueryString["mailtype"]);
			string sReadOnly = iBridUtil.GetStringValue(Request.QueryString["readonly"]);

			ViewState["DELETE_MAIL_TYPE"] = sDeleteMailType;
			ViewState["READ_ONLY"] = sReadOnly;
			
			string sMessageCd = "";

			if (sDeleteMailType.Equals(ViCommConst.DELETE_MAIL_TX)) {
				sMessageCd = ViCommConst.CONFIRM_STATUS_DELETE_TX_MAIL_LOG;
			} else if (sDeleteMailType.Equals(ViCommConst.DELETE_MAIL_RX)) {
				sMessageCd = ViCommConst.CONFIRM_STATUS_DELETE_RX_MAIL_LOG;
			} else if (sDeleteMailType.Equals(ViCommConst.DELETE_MAIL_TXRX)) {
				sMessageCd = ViCommConst.CONFIRM_STATUS_DELETE_TXRX_MAIL_LOG;
			} else if (sDeleteMailType.Equals(ViCommConst.DELETE_MAIL_INFO)) {
				sMessageCd = ViCommConst.CONFIRM_STATUS_DELETE_INFO_MAIL_LOG;
			}

			lblErrorMessage.Text = GetErrorMessage(sMessageCd);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sDeleteMailType = iBridUtil.GetStringValue(ViewState["DELETE_MAIL_TYPE"]);
		int iReadOnly;
		int.TryParse(iBridUtil.GetStringValue(ViewState["READ_ONLY"]),out iReadOnly);

		string sMailType;
		string sRxTx;

		if (sDeleteMailType.Equals(ViCommConst.DELETE_MAIL_TX)) {
			sMailType = ViCommConst.MAIL_BOX_USER;
			sRxTx = ViCommConst.TX;
		} else if (sDeleteMailType.Equals(ViCommConst.DELETE_MAIL_RX)) {
			sMailType = ViCommConst.MAIL_BOX_USER;
			sRxTx = ViCommConst.RX;
		} else if (sDeleteMailType.Equals(ViCommConst.DELETE_MAIL_TXRX)) {
			sMailType = ViCommConst.MAIL_BOX_USER;
			sRxTx = ViCommConst.TXRX;
		} else {
			sMailType = ViCommConst.MAIL_BOX_INFO;
			sRxTx = ViCommConst.RX;
		}

		using (MailBox oMailBox = new MailBox()) {
			oMailBox.UpdateMailBatchDel(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sRxTx,sMailType,iReadOnly);
		}

		RedirectToDisplayDoc(ViCommConst.SCR_CAST_BATCH_MAIL_DEL);
	}
}
