/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信確認
--	Progaram ID		: TxMailConfirm
--
--  Creation Date	: 2010.09.07
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Text;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_MailFormConfirm:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		string sScreenId = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);
		Response.Filter = sessionWoman.InitScreen(Response.Filter,frmMain,Request,string.Format("MailFormConfirm{0}.aspx",sScreenId),ViewState);

		if (!IsPostBack) {
			ErrorCheck();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdBack"] != null) {
				cmdBack_Click(sender,e);
			}
		}
	}

	private string CreateDocument() {
		string sParamsInput = string.Empty;

		StringBuilder sb = new StringBuilder();

		string callfrom = iBridUtil.GetStringValue(Request.QueryString["callfrom"]);
		if (callfrom.Equals("ModifyUserHandleNm.aspx")) {
			string oldHandleNm = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNm;
			string newHandleNm = iBridUtil.GetStringValue(Request.QueryString["handlenm"]);
			sb.AppendFormat("旧ハンドルネーム：{0}\r\nから\r\n新ハンドルネーム：{1}\r\nへの変更依頼\r\nログインID:{2}",oldHandleNm,newHandleNm,sessionWoman.userWoman.loginId);
		} else {
			string sAfterDefaultBody = iBridUtil.GetStringValue(Request.Params["afterdefaultbody"]);
			if (!sAfterDefaultBody.Equals(ViCommConst.FLAG_ON_STR)) {
				AppendDefaultBody(false,ref sb);
			}
			string sBbsThreadSeq = iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]);
			if (!sBbsThreadSeq.Equals(string.Empty)) {
				using (BbsThread oBbsThread = new BbsThread()) {
					string sBbsThreadTitle = string.Empty;
					if (oBbsThread.GetValue(sessionWoman.site.siteCd,sBbsThreadSeq,"BBS_THREAD_TITLE",ref sBbsThreadTitle)) {
						sb.AppendFormat("トピック名:{0}\r\n",sBbsThreadTitle);
					}
				}
			}
			foreach (string sParams in Request.QueryString.AllKeys) {
				if (sParams.StartsWith("*")) {
					sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("*",string.Empty)]);
					sb.AppendFormat("{0}:{1}\r\n",iBridUtil.GetStringValue(Request.QueryString[sParams]),sParamsInput);
				} else if (sParams.StartsWith("?")) {
					sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("?",string.Empty)]);
					sb.AppendFormat("{0}:{1}\r\n",iBridUtil.GetStringValue(Request.QueryString[sParams]),sParamsInput);
				}
			}
			if (sAfterDefaultBody.Equals(ViCommConst.FLAG_ON_STR)) {
				AppendDefaultBody(true,ref sb);
			}
		}

		return sb.ToString();
	}

	private void ErrorCheck() {
		string sParamsInput = string.Empty;
		string sParamsName = string.Empty;
		bool bOk = true;
		StringBuilder sb = new StringBuilder();

		foreach (string sParams in Request.QueryString.AllKeys) {
			if (sParams.StartsWith("*")) {
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("*",string.Empty)]);
				sParamsName = iBridUtil.GetStringValue(Request.QueryString[sParams]);

				if (!sParamsInput.Equals("")) {
					lblConfirm.Text += string.Format("{0}:{1}<BR />",sParamsName.Replace("\r\n","<BR>"),sParamsInput.Replace("\r\n","<BR>"));
				} else {
					bOk = false;
					sb.AppendFormat("{0}が入力されていません<BR />",sParamsName);
				}
			} else if (sParams.StartsWith("?")) {
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("?",string.Empty)]);
				sParamsName = iBridUtil.GetStringValue(Request.QueryString[sParams]);

				if (!sParamsInput.Equals("")) {
					lblConfirm.Text += string.Format("{0}:{1}<BR />",sParamsName.Replace("\r\n","<BR>"),sParamsInput.Replace("\r\n","<BR>"));
				}
			}
		}
		if (bOk == false) {
			sessionWoman.errorMessage = sb.ToString();
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			string sBbsThreadSeq = iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]);
			if (!sBbsThreadSeq.Equals(string.Empty)) {
				oParam.Add("bbsthreadseq",sBbsThreadSeq);
			}
			string sCommentNo = iBridUtil.GetStringValue(Request.QueryString["commentno"]);
			if (!string.IsNullOrEmpty(sCommentNo)) {
				oParam.Add("commentno", sCommentNo);
			}
			if (!string.IsNullOrEmpty(sBbsThreadSeq) || !string.IsNullOrEmpty(sCommentNo)) {
				oParam.Add("errormessage", "1");
			}

			Encoding enc = Encoding.GetEncoding(932);
			string sMail = iBridUtil.GetStringValue(Request.QueryString["mail"]);
			if (!string.IsNullOrEmpty(sMail)) {
				oParam.Add("mail", HttpUtility.UrlEncode(sMail, enc));
			}
			string sName = iBridUtil.GetStringValue(Request.QueryString["name"]);
			if (!string.IsNullOrEmpty(sName)) {
				oParam.Add("name", HttpUtility.UrlEncode(sName, enc));
			}
			string sBody = iBridUtil.GetStringValue(Request.QueryString["body"]);
			if (!string.IsNullOrEmpty(sBody)) {
				oParam.Add("body", HttpUtility.UrlEncode(sBody, enc));
			}
			RedirectToDisplayDoc(iBridUtil.GetStringValue(Request.QueryString["error"]),oParam);
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sToMailAddress = iBridUtil.GetStringValue(Request.QueryString["send"]);
		string sFromMailAddress = iBridUtil.GetStringValue(Request.QueryString["from"]);
		//fromがない場合ｾｯｼｮﾝのﾒｰﾙｱﾄﾞﾚｽを利用する。
		if (sFromMailAddress.Equals(string.Empty)) {
			sFromMailAddress = sessionWoman.userWoman.emailAddr;
		}

		string sTitle = iBridUtil.GetStringValue(Request.QueryString["title"]);
		string sDoc = CreateDocument();
		//メールアドレスの形式をチェック
		try {
			new System.Net.Mail.MailAddress(sFromMailAddress);
		} catch (FormatException) {
			sFromMailAddress = sToMailAddress;
		}

		bool bUtf8Flag = false;
		if (iBridUtil.GetStringValue(Request.QueryString["callfrom"]).Equals("ModifyUserHandleNm.aspx")) {
			if (iBridUtil.GetStringValue(System.Configuration.ConfigurationManager.AppSettings["HandleNmMailUtf8Flag"]).Equals(ViCommConst.FLAG_ON_STR)) {
				bUtf8Flag = true;
			}
		}
		
		SendMail(sFromMailAddress,sToMailAddress,sTitle,sDoc,bUtf8Flag);

		IDictionary<string,string> oParam = new Dictionary<string,string>();
		string sBbsThreadSeq = iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]);
		if (!sBbsThreadSeq.Equals(string.Empty)) {
			oParam.Add("bbsthreadseq",sBbsThreadSeq);
		}
		RedirectToDisplayDoc(iBridUtil.GetStringValue(Request.QueryString["success"]),oParam);
	}

	protected void cmdBack_Click(object sender,EventArgs e) {
		string sKey,sParamsInput;
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		StringBuilder sb = new StringBuilder();

		foreach (string sParams in Request.QueryString.AllKeys) {
			if (sParams.StartsWith("*")) {
				sKey = sParams.Replace("*",string.Empty);
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sKey]);
				sb.AppendFormat("&{0}={1}",sKey,HttpUtility.UrlEncode(sParamsInput,enc));
			} else if (sParams.StartsWith("?")) {
				sKey = sParams.Replace("?",string.Empty);
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sKey]);
				sb.AppendFormat("&{0}={1}",sKey,HttpUtility.UrlEncode(sParamsInput,enc));
			}
		}
		string doc = iBridUtil.GetStringValue(Request.QueryString["doc"]);
		if (!string.IsNullOrEmpty(doc)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("DisplayDoc.aspx?doc={0}{1}",Request.QueryString["doc"],sb.ToString())));
		} else {
			string callfrom = iBridUtil.GetStringValue(Request.QueryString["callfrom"]);
			if (callfrom.Equals("ModifyUserHandleNm.aspx")) {
				sb.AppendFormat("&{0}={1}","sendmail",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["sendmail"]),enc));
				sb.AppendFormat("&{0}={1}","send",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["send"]),enc));
				sb.AppendFormat("&{0}={1}","title",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["title"]),enc));
				sb.AppendFormat("&{0}={1}","success",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["success"]),enc));
				sb.AppendFormat("&{0}={1}","handlenm",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["handlenm"]),enc));
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("{0}?{1}",callfrom,sb.ToString())));
		}
	}

	private void AppendDefaultBody(bool pAfter,ref StringBuilder pBody) {
		if (pAfter) {
			pBody.Append("\r\n");
		}
		pBody.AppendFormat("ﾛｸﾞｲﾝID:{0}\r\n",sessionWoman.userWoman.loginId);
		pBody.AppendFormat("登録ﾒｰﾙｱﾄﾞﾚｽ:{0}",sessionWoman.userWoman.emailAddr);
		if (!pAfter) {
			pBody.Append("\r\n\r\n");
		}
	}
}