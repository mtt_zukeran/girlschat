/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹް� ���ًL�^�ꗗ
--	Progaram ID		: ListGameBattleLog
--
--  Creation Date	: 2011.11.10
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameBattleLog:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sBattleType = iBridUtil.GetStringValue(Request.QueryString["battle_type"]);
		if (sBattleType.Equals(PwViCommConst.GameBattleType.SUPPORT)) {
			this.UpdateSupportRequestCheck();
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_LOG,ActiveForm);
	}

	private void UpdateSupportRequestCheck() {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sResult = string.Empty;

		using (SupportRequest oSupportRequest = new SupportRequest()) {
			oSupportRequest.UpdateSupportRequestCheck(sSiteCd,sUserSeq,sUserCharNo,string.Empty,out sResult);
		}
	}
}
