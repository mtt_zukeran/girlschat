﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewBeautyClock.aspx.cs" Inherits="ViComm_woman_ViewBeautyClock" %>
<%@ Register TagPrefix="ibrid"		Namespace="MobileLib"						Assembly="MobileLib"%>
<%@ Register TagPrefix="mobile"		Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<ibrid:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
		<ibrid:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
