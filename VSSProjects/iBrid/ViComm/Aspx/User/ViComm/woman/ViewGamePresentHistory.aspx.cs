﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾌﾟﾚｾﾞﾝﾄ詳細
--	Progaram ID		: ViewGamePresentHistory
--
--  Creation Date	: 2012.01.25
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGamePresentHistory:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPresentHistorySeq = iBridUtil.GetStringValue(Request.QueryString["present_history_seq"]);

		if (!sPresentHistorySeq.Equals(string.Empty)) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_MAN_PRESENT_HISTORY,ActiveForm);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}
}