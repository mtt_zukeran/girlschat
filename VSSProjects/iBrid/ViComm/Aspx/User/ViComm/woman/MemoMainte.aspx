<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MemoMainte.aspx.cs" Inherits="ViComm_woman_MemoMainte" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Assembly="MobileLib" Namespace="MobileLib" %>
<%@ Register TagPrefix="ibrid" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>    
		$PGM_HTML02;
		<ibrid:TextArea ID="txtMemoDoc" runat="server" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24">
		</ibrid:TextArea>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
