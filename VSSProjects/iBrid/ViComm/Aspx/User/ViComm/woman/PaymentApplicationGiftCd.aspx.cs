﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 精算申請(ギフト券)
--	Progaram ID		: PaymentApplicationGiftCd
--
--  Creation Date	: 2017.03.21
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_PaymentApplicationGiftCd:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_EXTENSION_PAYMENT_GIFT,ActiveForm);
		} else {
			if (Request.Params["cmdTopPage"] != null) {
				cmdTopPage_Click(sender,e);
			}
		}
	}

	protected void cmdTopPage_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("UserTop.aspx"));
	}
}
