/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ログインキャラクター選択
--	Progaram ID		: LoginCharacterSelect
--
--  Creation Date	: 2010.01.21
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_LoginCharacterSelect:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		int iIdx = 0;

		if (!IsPostBack) {
			DataSet ds;
			using (CodeDtl oCodeDtl = new CodeDtl()) {
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_WAIT_TIME);
			}
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstWaitTime.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
			}

			using (Cast oCast = new Cast()) {
				DataSet dsCastLoginCharacter = oCast.GetLoginCharacterList(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);

				SelectionList chkLogin;
				iBMobileLabel lblUserCharNo;
				iBMobileLabel lblHandleNm;

				foreach (DataRow dr in dsCastLoginCharacter.Tables[0].Rows) {
					chkLogin = (SelectionList)frmMain.FindControl(string.Format("chkLogin{0}",iIdx)) as SelectionList;
					lblUserCharNo = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserCharNo{0}",iIdx)) as iBMobileLabel;
					lblHandleNm = (iBMobileLabel)frmMain.FindControl(string.Format("lblHandleNm{0}",iIdx)) as iBMobileLabel;

					string sConnectType = iBridUtil.GetStringValue(dr["CONNECT_TYPE"]);

					if (!iBridUtil.GetStringValue(dr["CHARACTER_ONLINE_STATUS"]).Equals(ViCommConst.USER_WAITING.ToString())) {
						chkLogin.Items[0].Selected = true;
					} else {
						chkLogin.Items[0].Selected = false;
					}
					lblUserCharNo.Text = iBridUtil.GetStringValue(dr["USER_CHAR_NO"]);
					lblHandleNm.Text = iBridUtil.GetStringValue(dr["HANDLE_NM"]);

					if (sConnectType.Equals(ViCommConst.WAIT_TYPE_VIDEO) || sConnectType.Equals(ViCommConst.WAIT_TYPE_BOTH)) {
						Panel pnlLoginCharacter = (Panel)frmMain.FindControl(string.Format("pnlLoginCharacter{0}",iIdx)) as Panel;
						pnlLoginCharacter.Visible = false;
					}
					iIdx++;
				}
			}

			for (int i = iIdx;i < ViComm.ViCommConst.MAX_CHARACTER_COUNT;i++) {

				Panel pnlLoginCharacter = (Panel)frmMain.FindControl(string.Format("pnlLoginCharacter{0}",i)) as Panel;
				pnlLoginCharacter.Visible = false;

			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iLoginRecCount = 0;
		int iLogoffRecCount = 0;
		ArrayList alLoginUserChar = new ArrayList();
		ArrayList alLogoffUserChar = new ArrayList();
		ArrayList alLoginEndDate = new ArrayList();
		DateTime dtLoginEndDate = sessionWoman.userWoman.endDate;

		for (int i = 0;i < ViCommConst.MAX_CHARACTER_COUNT;i++) {
			SelectionList chkLogin = (SelectionList)frmMain.FindControl(string.Format("chkLogin{0}",i)) as SelectionList;
			iBMobileLabel lblUserCharNo = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserCharNo{0}",i)) as iBMobileLabel;
			iBMobileLabel lblHandleNm = (iBMobileLabel)frmMain.FindControl(string.Format("lblHandleNm{0}",i)) as iBMobileLabel;

			if (chkLogin.Items[0].Selected) {
				iLoginRecCount++;
				alLoginUserChar.Add(lblUserCharNo.Text);
				alLoginEndDate.Add(dtLoginEndDate.ToString("yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo));
			} else {
				iLogoffRecCount++;
				alLogoffUserChar.Add(lblUserCharNo.Text);
			}
		}
		string[] sLoginUserCharNo = (string[])alLoginUserChar.ToArray(typeof(string));
		string[] sLogoffUserCharNo = (string[])alLogoffUserChar.ToArray(typeof(string));
		string[] sLoginEndDate = (string[])alLoginEndDate.ToArray(typeof(string));

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARACTER_LOGOFF");
			db.ProcedureInParm("SITE_CD",DbSession.DbType.VARCHAR2,sessionWoman.site.siteCd);
			db.ProcedureInParm("USER_SEQ",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.userSeq);
			db.ProcedureInArrayParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,iLogoffRecCount,sLogoffUserCharNo);
			db.ProcedureInParm("PUSER_CHAR_RECORD_COUNT",DbSession.DbType.NUMBER,iLogoffRecCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARACTER_STANDBY");
			db.ProcedureInParm("SITE_CD",DbSession.DbType.VARCHAR2,sessionWoman.site.siteCd);
			db.ProcedureInParm("USER_SEQ",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.userSeq);
			db.ProcedureInArrayParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,iLoginRecCount,sLoginUserCharNo);
			db.ProcedureInArrayParm("PSTANDBY_END_DATE",DbSession.DbType.VARCHAR2,iLoginRecCount,sLoginEndDate);
			db.ProcedureInParm("PUSER_CHAR_RECORD_COUNT",DbSession.DbType.NUMBER,iLoginRecCount);
			db.ProcedureInParm("PLOGIN_USED_TEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PLOGIN_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_CAST_WAITING_START));
	}
}
