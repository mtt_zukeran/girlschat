﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ゲームオブジェクト送信

--	Progaram ID		: SendGameObj
--
--  Creation Date	: 2011.07.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Collections.Generic;
using System.Web;

public partial class ViComm_woman_SendGameObj:MobileSocialGameWomanBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (ValidateInput()) {
			string sObjType = iBridUtil.GetStringValue(Request.QueryString["objtype"]);//1(写真)or2(動画)
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text));
			string sAttrSeq = iBridUtil.GetStringValue(Request.QueryString["attrseq"]);
			string sObjTempId = string.Empty;

			using (WaitObj oWaitObj = new WaitObj()) {
				oWaitObj.WriteWaitObj(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sObjType,
					string.Empty,
					sDoc,
					ViCommConst.ATTACHED_SOCIAL_GAME.ToString(),
					string.Empty,
					sAttrSeq,
					ViCommConst.FLAG_OFF,
					string.Empty,
					ViCommConst.FLAG_OFF,
					out sObjTempId
				);
			}

			sessionWoman.userWoman.ObjTempId = sObjTempId;

			if (sObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_PIC_GAME_MAILER_COMPLITE);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_MOVIE_GAME_MAILER_COMPLITE);
			}
		}
	}

	private bool ValidateInput() {
		bool bOk = true;
		string sNGWord = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄを入力して下さい。";
		} else if (txtDoc.Text.Length > 750) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄが長すぎます。";
		} else if (sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
		}

		return bOk;
	}
}
