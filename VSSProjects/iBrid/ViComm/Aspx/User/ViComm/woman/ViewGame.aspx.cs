/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｹﾞｰﾑ詳細
--	Progaram ID		: ViewGame
--
--  Creation Date	: 2011.03.24
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewGame : MobileWomanPageBase {

	protected const string PARAM_NM_GAME_TYPE = "game_type";
	protected const string PARAM_NM_PLAY_FLAG = "play_flag";
	protected const string PARAM_NM_SCORE = "score";
	
	/// <summary>基底クラスでﾛｸﾞｲﾝのﾁｪｯｸを行う</summary>
	protected override bool NeedLogin { get { return true; } }

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			string sGameType = iBridUtil.GetStringValue(Request.Params[PARAM_NM_GAME_TYPE]);
			string sScore = iBridUtil.GetStringValue(Request.Params[PARAM_NM_SCORE]);
			bool bPlayFlag = iBridUtil.GetStringValue(Request.Params[PARAM_NM_PLAY_FLAG]).Equals(ViCommConst.FLAG_ON_STR);
			string sCurrentAspxName = string.Format("ViewGame{0}.aspx", sGameType);
			
			if(bPlayFlag){
				FlashLiteHelper.ResponseFlashGame(sGameType, sessionObj.sexCd, sessionWoman.userWoman.loginId, sessionWoman.userWoman.loginPassword);
			}else if(!string.IsNullOrEmpty(sScore)){
				RegistScore(sGameType,sScore);
			} else {
				Response.Filter = sessionWoman.InitScreen(Response.Filter, frmMain, Request, sCurrentAspxName, ViewState);
				sessionWoman.ControlList(Request, ViCommConst.INQUIRY_EXTENSION_GAME, ActiveForm);
			}
		}
	}
	
	private void RegistScore(string pGameType,string pScore){
		decimal dGameScoreSeq = decimal.Zero;
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("REGIST_GAME_SCORE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sessionWoman.site.siteCd);
			oDbSession.ProcedureInParm("pGAME_TYPE", DbSession.DbType.VARCHAR2, pGameType);
			oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, sessionWoman.sexCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, sessionWoman.userWoman.userSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, ViCommConst.MAIN_CHAR_NO);
			oDbSession.ProcedureInParm("pGAME_SCORE", DbSession.DbType.NUMBER, pScore);
			oDbSession.ProcedureOutParm("pGAME_SCORE_SEQ", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			dGameScoreSeq = oDbSession.GetDecimalValue("pGAME_SCORE_SEQ");
		}
		
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ViewGameScore.aspx?seq={0}&game_type={1}",dGameScoreSeq,pGameType)));
	}
}
