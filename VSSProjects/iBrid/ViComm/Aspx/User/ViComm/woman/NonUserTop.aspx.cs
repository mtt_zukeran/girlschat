/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o�^�O�g�b�v
--	Progaram ID		: NonUserTop
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_NonUserTop:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionWoman.site.GetOne(iBridUtil.GetStringValue(Session["BASIC_SITE"]));
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
//			if (!iBridUtil.GetStringValue(Request.QueryString["guid"]).Equals(string.Empty) && sessionWoman.usedEasyLoginFlag) {
//				cmdSubmit_Click(sender,e);
//			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (!sessionWoman.usedEasyLoginFlag) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"LoginUser.aspx"));
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("LoginUser.aspx?id={0}",sessionWoman.currentIModeId)));
	}
}
