/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 受信メール設定
--	Progaram ID		: SetupRxMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_SetupRxMail:MobileWomanPageBase {
	private const string PARAM_NM_DIRECT_SETUP = "direct";
	private const string PARAM_NM_MAN_MAIL_RX_TYPE = "manmail";
	private const string PARAM_NM_INFO_MAIL_RX_TYPE = "infomail";
	private const string PARAM_NM_CLEAR_MAIL_ADDR_NG = "clearng";
	private const string PARAM_NEXT_DOC_NO = "*nextdoc";
	
	protected SelectionList buyPointMailRxType;			// ﾎﾟｲﾝﾄ購入ﾒｰﾙ受信区分	

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		bool bDirectSetup = iBridUtil.GetStringValue(Request.Params[PARAM_NM_DIRECT_SETUP]).Equals(ViCommConst.FLAG_ON_STR);
		string sManMailRxType = iBridUtil.GetStringValue(Request.Params[PARAM_NM_MAN_MAIL_RX_TYPE]);
		string sInfoMailRxType = iBridUtil.GetStringValue(Request.Params[PARAM_NM_INFO_MAIL_RX_TYPE]);
		string sNextDocNo = iBridUtil.GetStringValue(Request.Params[PARAM_NEXT_DOC_NO]);
		int iClearNg;
		if(!int.TryParse(iBridUtil.GetStringValue(Request.Params[PARAM_NM_CLEAR_MAIL_ADDR_NG]),out iClearNg)){
			iClearNg = ViCommConst.FLAG_OFF;
		}
		
		

		buyPointMailRxType = (SelectionList)frmMain.FindControl("lstBuyPointMailRxType") as SelectionList;

		if (!IsPostBack) {
			if(bDirectSetup){
				this.DirectSetup(sManMailRxType, sInfoMailRxType,iClearNg, sNextDocNo);
				return;
			}
		
		
			UserWomanCharacter oWomanCharacter = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey];

			if (sessionWoman.site.castTalkEndSendMailFlag == ViCommConst.FLAG_OFF) {
				pnlTalkEndMailRxFlag.Visible = false;
			}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds;
				int iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_MAIL_RX_TYPE);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstManMailRxType.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
					if (oWomanCharacter.manMailRxType.Equals(dr["CODE"].ToString())) {
						lstManMailRxType.SelectedIndex = iIdx;
					}
					iIdx++;
				}

				iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_INFO_MAIL_RX_TYPE);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstInfoMailRxType.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
					if (oWomanCharacter.infoMailRxType.Equals(dr["CODE"].ToString())) {
						lstInfoMailRxType.SelectedIndex = iIdx;
					}
					iIdx++;
				}

				if (buyPointMailRxType is SelectionList) {
					iIdx = 0;
					ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_BUY_POINT_MAIL_RX_TYPE);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						buyPointMailRxType.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
						if (oWomanCharacter.characterEx.buyPointMailRxType.Equals(dr["CODE"].ToString())) {
							buyPointMailRxType.SelectedIndex = iIdx;
						}
						iIdx++;
					}
				}

				iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TIME_PART);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstMobileMailRxStartTime.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));
					lstMobileMailRxEndTime.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));

					if (oWomanCharacter.mobileMailRxStartTime.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxStartTime.SelectedIndex = iIdx;
					}
					if (oWomanCharacter.mobileMailRxEndTime.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxEndTime.SelectedIndex = iIdx;
					}
					iIdx++;
				}

				iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_TALK_END_MAIL_RX_TYPE);
				foreach (DataRow dr in ds.Tables[0].Rows) {

					lstTalkEndMailRxFlag.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));

					if (oWomanCharacter.talkEndMailRxFlag.ToString().Equals(dr["CODE"].ToString())) {
						lstTalkEndMailRxFlag.SelectedIndex = iIdx;
					}
					iIdx++;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	private void DirectSetup(string pManMailRxType, string pInfoMailRxType,int pClearMailAddrNgFlag, string pNextDoc){
		if(string.IsNullOrEmpty(pManMailRxType)){
			pManMailRxType = sessionWoman.userWoman.CurCharacter.manMailRxType;
		}
		if(string.IsNullOrEmpty(pInfoMailRxType)){
			pInfoMailRxType = sessionWoman.userWoman.CurCharacter.infoMailRxType;
		}
		if(string.IsNullOrEmpty(pNextDoc)){
			pNextDoc = ViCommConst.SCR_WOMAN_SETUP_RX_MAIL_LIGHT_COMPLETE;
		}
		
	
		sessionWoman.userWoman.SetupRxMail(
			sessionWoman.site.siteCd,
			sessionWoman.userWoman.userSeq,
			sessionWoman.userWoman.curCharNo,
			pManMailRxType,
			pInfoMailRxType,
			sessionWoman.userWoman.CurCharacter.talkEndMailRxFlag,
			sessionWoman.userWoman.CurCharacter.mobileMailRxStartTime,
			sessionWoman.userWoman.CurCharacter.mobileMailRxEndTime,
			pClearMailAddrNgFlag
		);
		
		RedirectToDisplayDoc(pNextDoc);		
	}

	virtual protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iTalkEndMailRxFlag = 0;
		int.TryParse(lstTalkEndMailRxFlag.Items[lstTalkEndMailRxFlag.SelectedIndex].Value,out iTalkEndMailRxFlag);
		sessionWoman.userWoman.SetupRxMail(
			sessionWoman.site.siteCd,
			sessionWoman.userWoman.userSeq,
			sessionWoman.userWoman.curCharNo,
			lstManMailRxType.Items[lstManMailRxType.SelectedIndex].Value,
			lstInfoMailRxType.Items[lstInfoMailRxType.SelectedIndex].Value,
			iTalkEndMailRxFlag,
			lstMobileMailRxStartTime.Items[lstMobileMailRxStartTime.SelectedIndex].Value,
			lstMobileMailRxEndTime.Items[lstMobileMailRxEndTime.SelectedIndex].Value,
			ViCommConst.FLAG_OFF
		);

		if (buyPointMailRxType is SelectionList) {
			sessionWoman.userWoman.SetupRxMailEx(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				buyPointMailRxType.Items[buyPointMailRxType.SelectedIndex].Value
			);
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SETUP_MAIL_COMPLITE_CAST) + "&site=" + sessionWoman.site.siteCd);
	}
}
