/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 日記書込
--	Progaram ID		: WiteDiary
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_WriteDiary:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {

			this.ExecuteControlList();

			//未使用のためｺﾒﾝﾄｱｳﾄ
			//sessionWoman.userWoman.GetCharacterList(sessionWoman.site.siteCd);

			int iYear = DateTime.Today.Year;
			string sReportDay = string.Empty;
			string sSubSeq = string.Empty;
			string sCastDiaryAttrSeq = ViCommConst.DEFAULT_CAST_DIARY_ATTR_SEQ.ToString();

			RunMode iMode = (RunMode)int.Parse((string)Request.QueryString["mode"]);

			for (int i = 0;i < 3;i++) {
				lstYear.Items.Add(new MobileListItem(iYear.ToString(),iYear.ToString()));
				iYear -= 1;
			}

			for (int i = 1;i <= 12;i++) {
				lstMonth.Items.Add(new MobileListItem(i.ToString(),i.ToString("d2")));
			}

			for (int i = 1;i <= 31;i++) {
				lstDay.Items.Add(new MobileListItem(i.ToString(),i.ToString("d2")));
			}

			switch (iMode) {
				case RunMode.Update:
					sReportDay = Request.QueryString["reportday"].ToString();
					sSubSeq = iBridUtil.GetStringValue(Request.QueryString["subseq"]);
					using (CastDiary oDiary = new CastDiary()) {
						if (oDiary.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sReportDay,sSubSeq)) {
							txtTitle.Text = oDiary.diaryTitle;
							txtDoc.Text = oDiary.diaryDoc;
							sCastDiaryAttrSeq = oDiary.castDiaryAttrSeq;
						}
					}
					break;

				default:
					sReportDay = DateTime.Today.ToShortDateString();
					break;
			}

			for (int i = 0;i < lstYear.Items.Count;i++) {
				if (lstYear.Items[i].Value.Equals(sReportDay.Substring(0,4))) {
					lstYear.SelectedIndex = i;
					break;
				}
			}

			for (int i = 0;i < lstMonth.Items.Count;i++) {
				if (lstMonth.Items[i].Value.Equals(sReportDay.Substring(5,2))) {
					lstMonth.SelectedIndex = i;
					break;
				}
			}

			for (int i = 0;i < lstDay.Items.Count;i++) {
				if (lstDay.Items[i].Value.Equals(sReportDay.Substring(8,2))) {
					lstDay.SelectedIndex = i;
					break;
				}
			}
			if (IsAvailableService(ViCommConst.RELEASE_DISABLE_DIARY_DAY,2)) {
				lstYear.Visible = false;
				lblYear.Visible = false;
				lstMonth.Visible = false;
				lblMonth.Visible = false;
				lstDay.Visible = false;
			}
			using (CastDiaryAttrValue oCastDiaryAttrValue = new CastDiaryAttrValue()) {
				using (DataSet oDataSet = oCastDiaryAttrValue.GetList(this.sessionObj.site.siteCd)) {
					DataRow dr;
					int iSelectedIdx = 0;
					if (oDataSet.Tables[0].Rows.Count > 0) {

						for (int i = 0;i < oDataSet.Tables[0].Rows.Count;i++) {
							dr = oDataSet.Tables[0].Rows[i];
							this.lstCastDiaryAttrValue.Items.Add(new MobileListItem(dr["CAST_DIARY_ATTR_NM"].ToString(),dr["CAST_DIARY_ATTR_SEQ"].ToString()));

							if (dr["CAST_DIARY_ATTR_SEQ"].ToString().Equals(sCastDiaryAttrSeq)) {
								iSelectedIdx = i;
							}
						}
					} else {
						this.lstCastDiaryAttrValue.Items.Add(new MobileListItem(string.Empty,ViCommConst.DEFAULT_CAST_DIARY_ATTR_SEQ.ToString()));
					}

					this.lstCastDiaryAttrValue.Items[iSelectedIdx].Selected = true;
				}
			}
			if (IsAvailableService(ViCommConst.RELEASE_USE_DIARY_ATTR,2)) {
				pnlCastDiaryAttrValue.Visible = true;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdDelete"] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = "";

		if (txtTitle.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (bOk & CheckOther()) {
			string sUploadKey = string.Empty;
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text));
			string sReportDay = lstYear.Items[lstYear.SelectedIndex].Value + "/" + lstMonth.Items[lstMonth.SelectedIndex].Value + "/" + lstDay.Items[lstDay.SelectedIndex].Value;
			string sCastDiaryAttrSeq = lstCastDiaryAttrValue.Items[lstCastDiaryAttrValue.SelectedIndex].Value;
			string sSubSeq = iBridUtil.GetStringValue(Request.QueryString["subseq"]);
			bool sWaitFlag = iBridUtil.GetStringValue(Request.Form["wait"]).Equals(ViCommConst.FLAG_ON_STR);
			
			string sScreenId = this.GetDiaryScreenID();
			bool bDiarySwitchScreenFlag = this.IsDiarySwitchScreen();

			using (CastDiary oDiary = new CastDiary()) {
				oDiary.WriteDiary(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sReportDay,sSubSeq,sTitle,sCastDiaryAttrSeq,sDoc,sWaitFlag,sScreenId,out sUploadKey);
			}
			if (sWaitFlag) {
				if (bDiarySwitchScreenFlag) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WRITE_DIARY_COMPLITE_CAST_PIC_SWITCH) + "&site=" + sessionWoman.site.siteCd + "&uploadkey=" + sUploadKey);
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WRITE_DIARY_COMPLITE_CAST_PIC) + "&site=" + sessionWoman.site.siteCd + "&uploadkey=" + sUploadKey);
				}
			} else {
				if (bDiarySwitchScreenFlag) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WRITE_DIARY_COMPLITE_CAST_SWITCH) + "&site=" + sessionWoman.site.siteCd);
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WRITE_DIARY_COMPLITE_CAST) + "&site=" + sessionWoman.site.siteCd);
				}
			}
		}
	}

	private void cmdDelete_Click(object sender,EventArgs e) {
		using (CastDiary oDiary = new CastDiary()) {
			string sPicFileNm;
			oDiary.DeleteDiary(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sessionWoman.GetDiaryValue("REPORT_DAY"),
				sessionWoman.GetDiaryValue("CAST_DIARY_SUB_SEQ"),
				out sPicFileNm
			);
			if (sPicFileNm.Equals("") == false) {
				DeletePicture(sPicFileNm);
			}

			bool bDiarySwitchScreenFlag = this.IsDiarySwitchScreen();
			if (bDiarySwitchScreenFlag) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_DIARY_SWITCH) + "&site=" + sessionWoman.site.siteCd);
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_DIARY) + "&site=" + sessionWoman.site.siteCd);
			}
		}
	}

	private void DeletePicture(string pFileSeq) {
		string sFullPath = sessionWoman.site.webPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
							string.Format("{0:D15}",int.Parse(pFileSeq)) + ViCommConst.PIC_FOODER;

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}

			sFullPath = sessionWoman.site.webPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
								string.Format("{0:D15}",int.Parse(pFileSeq)) + ViCommConst.PIC_FOODER_SMALL;


			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
	}

	private string GetDiaryScreenID() {
		string sScreenId = iBridUtil.GetStringValue(Request.QueryString["scrid"]);

		return sScreenId;
	}

	private bool IsDiarySwitchScreen() {		
		bool bDiarySwitchScreenFlag = false;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bDiarySwitchScreenFlag = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DIARY_SWITCH_SCREEN,2);
		}

		return bDiarySwitchScreenFlag && !string.IsNullOrEmpty(this.GetDiaryScreenID());
	}

	protected virtual bool CheckOther() {
		return true;
	}

	protected virtual bool ExecuteControlList() {
		return true;
	}
}
