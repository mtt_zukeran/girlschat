/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ相手の所持お宝一覧
--	Progaram ID		: ListGameTreasureBattle
--
--  Creation Date	: 2011.09.21
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_woman_ListGameTreasureBattle:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

			Regex rgx = new Regex("^[0-9]+$");
			Match rgxMatch = rgx.Match(sPartnerUserSeq);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}

			rgx = new Regex("^[0-9]+$");
			rgxMatch = rgx.Match(sPartnerUserCharNo);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}

			if (sessionWoman.userWoman.userSeq.Equals(sPartnerUserSeq) &&
				sessionWoman.userWoman.curCharNo.Equals(sPartnerUserCharNo)
			) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
			
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE_BATTLE_LIST,ActiveForm);
		}
	}
}
