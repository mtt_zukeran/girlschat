/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者ログインスケジュール登録
--	Progaram ID		: RegistLoginPlan
--
--  Creation Date	: 2014.12.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_woman_RegistLoginPlan:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			using (LoginPlan oLoginPlan = new LoginPlan()) {
				oLoginPlan.DeleteOverLoginPlan(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			}
			sessionWoman.errorMessage = string.Empty;
			pnlRegistForm.Visible = true;
			lblDeleteForm.Visible = false;
			this.CreateList();

			if (sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_LOGIN_PLAN,ActiveForm)) {
				pnlRegistForm.Visible = false;
				lblDeleteForm.Visible = true;
			} else {
				string sYear = string.Empty;
				DateTime dt = new DateTime();
				string sLoginStartTime = string.Empty;
				if (DateTime.Now.Minute < 30) {
					sLoginStartTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),"30");
				} else {
					sLoginStartTime = string.Format("{0}:{1}",DateTime.Now.AddHours(1).ToString("yyyy/MM/dd HH"),"00");
				}

				dt = DateTime.Parse(sLoginStartTime);
				foreach (MobileListItem item in lstDate.Items) {
					if (item.Value == dt.ToString("yyyy/MM/dd")) {
						item.Selected = true;
					}
				}

				foreach (MobileListItem item in lstTime.Items) {
					if (item.Value == dt.ToString("HH:mm")) {
						item.Selected = true;
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		DateTime dt;
		if (DateTime.TryParse(string.Format("{0} {1}",lstDate.Selection.Value,lstTime.Selection.Value),out dt)) {
			if (dt <= DateTime.Now) {
				sessionWoman.errorMessage = "過去は入力できません。";
				return;
			} else {
				using (LoginPlan oLoginPlan = new LoginPlan()) {
					oLoginPlan.LoginPlanMainte(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,string.Empty,dt,ViCommConst.FLAG_OFF);
				}
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("RegistLoginPlan.aspx"));
			}
		} else {
			sessionWoman.errorMessage = "日時を正しく入力してください。";
			return;
		}
	}

	protected void cmdDelete_Click(object sender,EventArgs e) {
		using (LoginPlan oLoginPlan = new LoginPlan()) {
			oLoginPlan.LoginPlanMainte(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,iBridUtil.GetStringValue(this.Request.Form["loginscheduleseq"]),DateTime.Now,ViCommConst.FLAG_ON);
		}
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("RegistLoginPlan.aspx"));
	}

	private void CreateList() {
		string[] sWeekArr = { "日","月","火","水","木","金","土" };
		string sMaxLoginPlanDaysSelectCount = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MaxWaitScheduleDaysSelectCount"]);
		int iMaxLoginPlanDaysSelectCount;
		if (!int.TryParse(sMaxLoginPlanDaysSelectCount,out iMaxLoginPlanDaysSelectCount)) {
			iMaxLoginPlanDaysSelectCount = 15;
		}

		DateTime dt = DateTime.Now;

		for (int i = 0;i < iMaxLoginPlanDaysSelectCount;i++) {
			lstDate.Items.Add(new MobileListItem(string.Format("{0} ({1})",dt.ToString("MM/dd"),sWeekArr[int.Parse(dt.DayOfWeek.ToString("d"))]),dt.ToString("yyyy/MM/dd")));
			dt = dt.AddDays(1);
		}

		for (int i = 0;i <= 23;i++) {
			lstTime.Items.Add(new MobileListItem(string.Format("{0}:00",i.ToString("d2")),string.Format("{0}:00",i.ToString("d2"))));
			lstTime.Items.Add(new MobileListItem(string.Format("{0}:30",i.ToString("d2")),string.Format("{0}:30",i.ToString("d2"))));
		}
	}
}