/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ 会員のつぶやき詳細
--	Progaram ID		: ViewManTweet
--
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ViewManTweet:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_TWEET,this.ActiveForm);
	}
}