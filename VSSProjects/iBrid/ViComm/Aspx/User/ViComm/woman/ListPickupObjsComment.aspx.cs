/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップオブジェ コメント一覧
--	Progaram ID		: ListPickupObjsComment
--
--  Creation Date	: 2013.05.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListPickupObjsComment:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_PICKUP_OBJS_COMMENT,ActiveForm);
		}
	}
}