/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 未送信メール編集リダイレクト
--	Progaram ID		: RedirectDraftMail.aspx
--
--  Creation Date	: 2013.09.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_RedirectDraftMail:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		string sDraftMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["draftmailseq"]);

		if (string.IsNullOrEmpty(sDraftMailSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		DataSet oDataSet = null;

		ManDraftMailSeekCondition oCondition = new ManDraftMailSeekCondition();
		string sSiteCd = sessionWoman.site.siteCd;
		string sTxUserSeq = sessionWoman.userWoman.userSeq;
		string sTxUserCharNo = sessionWoman.userWoman.curCharNo;

		using (ManDraftMail oManDraftMail = new ManDraftMail(sessionObj)) {
			oDataSet = oManDraftMail.GetOne(sSiteCd,sTxUserSeq,sTxUserCharNo,sDraftMailSeq);
		}

		if (oDataSet.Tables[0].Rows.Count == 0) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		string sMailDataSeq = iBridUtil.GetStringValue(sessionWoman.userWoman.NextMailDataSeq());
		sessionWoman.userWoman.mailData.Add(sMailDataSeq,new MailInfo());

		bool bReturnMailFlag = oDataSet.Tables[0].Rows[0]["RETURN_MAIL_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR) ? true : false;

		sessionWoman.userWoman.mailData[sMailDataSeq].draftMailSeq = sDraftMailSeq;
		sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
		sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		sessionWoman.userWoman.mailData[sMailDataSeq].rxUserNm = oDataSet.Tables[0].Rows[0]["HANDLE_NM"].ToString();
		sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle = oDataSet.Tables[0].Rows[0]["MAIL_TITLE"].ToString();
		sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc = GetDraftMailDoc(oDataSet);
		sessionWoman.userWoman.mailData[sMailDataSeq].mailTemplateNo = oDataSet.Tables[0].Rows[0]["MAIL_TEMPLATE_NO"].ToString();
		sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail = bReturnMailFlag;
		sessionWoman.userWoman.mailData[sMailDataSeq].returnMailSeq = oDataSet.Tables[0].Rows[0]["RETURN_ORG_MAIL_SEQ"].ToString();
		sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId = oDataSet.Tables[0].Rows[0]["LOGIN_ID"].ToString();

		string sRedirectUrl = string.Empty;
		if (bReturnMailFlag) {
			if (iBridUtil.GetStringValue(this.Request.QueryString["chat"]).Equals(ViCommConst.FLAG_ON_STR)) {
				sRedirectUrl = string.Format("ReturnMail.aspx?data={0}&direct=1&loginid={1}&scrid=01&stay=1#btm",sMailDataSeq,sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId);
			} else {
				sRedirectUrl = string.Format("ReturnMail.aspx?data={0}&direct=1&mailseq={1}",sMailDataSeq,sessionWoman.userWoman.mailData[sMailDataSeq].returnMailSeq);
			}
			
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sRedirectUrl));
		} else {
			if (iBridUtil.GetStringValue(this.Request.QueryString["chat"]).Equals(ViCommConst.FLAG_ON_STR)) {
				sRedirectUrl = string.Format("TxMail.aspx?data={0}&loginid={1}&scrid=01&stay=1#btm",sMailDataSeq,sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId);
			} else {
				sRedirectUrl = string.Format("TxMail.aspx?data={0}",sMailDataSeq);
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sRedirectUrl));
		}
	}

	private string GetDraftMailDoc(DataSet pDS) {
		string sValue = "";

		sValue = pDS.Tables[0].Rows[0]["MAIL_DOC1"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC2"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC3"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC4"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC5"].ToString();

		return sValue;
	}
}