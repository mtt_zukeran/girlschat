/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ管理
--	Progaram ID		: ViewMailLotteryTicketCount
--
--  Creation Date	: 2015.01.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ViewMailLotteryTicketCount:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAIL_LOTTERY_TICKET_COUNT,this.ActiveForm);
	}
}