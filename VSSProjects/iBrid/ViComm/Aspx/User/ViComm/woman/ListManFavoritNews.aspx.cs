/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り会員ニュース一覧
--	Progaram ID		: ListManFavoritNews
--
--  Creation Date	: 2013.09.09
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListManFavoritNews:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_FAVORIT_NEWS,this.ActiveForm);
		}
	}
}
