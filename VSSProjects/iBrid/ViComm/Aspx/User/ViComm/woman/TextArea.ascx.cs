using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using MobileLib;

public partial class TextArea:System.Web.UI.MobileControls.MobileUserControl, IEmojiInputControl {
	bool breakBefore = true;
	bool breakAfter = true;
	private bool emojiPaletteEnabled = false;
	
	protected override void OnPreRender(EventArgs e) {
		base.OnPreRender(e);
		if (Page is IEarlyParseEmoji) {
			((IEarlyParseEmoji)Page).EarlyParseEmoji(this.TextBox1);
		}
	}
	
	//ユーザーコントロールの Text プロパティを定義
	public string Text {
		get {
			return TextBox1.Text;

		}
		set {
			TextBox1.Text = Server.HtmlDecode(value);
		}

	}

	public int Rows {
		get {
			return TextBox1.Rows;

		}
		set {
			TextBox1.Rows = value;
		}

	}

	public int Columns {
		get {
			return TextBox1.Columns;

		}
		set {
			TextBox1.Columns = value;
		}

	}

	public int MaxLength {
		get {
			return TextBox1.MaxLength;

		}
		set {
			TextBox1.MaxLength = value;
		}

	}

	//ユーザーコントロールの BrerakAfter プロパティを定義
	public bool BrerakAfter {
		get {
			return breakAfter;
		}
		set {
			breakAfter = value;
			Literal1.Text = (breakAfter) ? "<br />" : "";
		}
	}
	public bool BreakBefore {
		get {
			return breakBefore;
		}
		set {
			breakBefore = value;
			Literal2.Text = (breakBefore) ? "<br />" : "";
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Literal1.Text = (breakAfter) ? "<br />" : "";
		Literal2.Text = (breakBefore) ? "<br />" : "";
		this._setTextBoxCssClass();
	}

	private void _setTextBoxCssClass()
	{
		string cssClassName = "emoji_enabled";

		TextBox1.CssClass.Replace(cssClassName, "").Trim();
		if (emojiPaletteEnabled)
		{
			if (TextBox1.CssClass.Length > 0)
				TextBox1.CssClass += " " + cssClassName;
			else
				TextBox1.CssClass = cssClassName;
		}
	}

	#region IEmojiInputControlの実装

	public bool EmojiPaletteEnabled
	{
		get
		{
			return emojiPaletteEnabled;
		}
		set
		{
			emojiPaletteEnabled = value;
			this._setTextBoxCssClass();
		}
	}
	#endregion
}

