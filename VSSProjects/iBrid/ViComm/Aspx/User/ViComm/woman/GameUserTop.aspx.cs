/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްщ��TOP
--	Progaram ID		: GameUserTop
--
--  Creation Date	: 2011.07.21
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_GameUserTop:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermIdGame.aspx"));
			return;
		}

		if (sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_ONLY)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_NON_USER_TOP_FOR_CHAT_USER);
			return;
		}

		using (User oUser = new User()) {
			oUser.UpdateMobileInfo(sessionWoman.userWoman.userSeq,sessionWoman.carrier,sessionWoman.mobileUserAgent);
		}

		if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE) || (sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionWoman.adminFlg)) {
			HttpCookie cookie = new HttpCookie("maqia");
			cookie.Values["maqiauid"] = sessionWoman.userWoman.userSeq;
			cookie.Values["maqiapw"] = sessionWoman.userWoman.loginPassword;
			cookie.Values["maqiasex"] = ViCommConst.OPERATOR;
			cookie.Expires = DateTime.Now.AddDays(90);
			cookie.Domain = sessionWoman.site.hostNm;
			Response.Cookies.Add(cookie);
		}

		if (!IsPostBack) {
			if (!sessionWoman.userWoman.loginCharacter.ContainsKey(sessionWoman.userWoman.curKey)) {
				sessionWoman.userWoman.loginCharacter.Add(sessionWoman.userWoman.curKey,false);
			}
			if (!sessionWoman.userWoman.loginCharacter[sessionWoman.userWoman.curKey]) {
				sessionWoman.userWoman.SetLastLoginDate(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.currentIModeId);
			}
		}
	}
}
