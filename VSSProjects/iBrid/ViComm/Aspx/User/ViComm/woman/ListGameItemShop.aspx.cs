/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���Ѽ���� ���шꗗ
--	Progaram ID		: ListGameItemShop
--
--  Creation Date	: 2011.07.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameItemShop:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		string sPageId = iBridUtil.GetStringValue(this.Request.Params["page_id"]);
		string sStageSeq = iBridUtil.GetStringValue(this.Request.Params["stage_seq"]);

		if (!sPageId.Equals(string.Empty)) {
			Session["PageId"] = sPageId;
		}

		if (!sStageSeq.Equals(string.Empty)) {
			Session["StageSeq"] = sStageSeq;
		}
		
		if (!IsPostBack) {
			sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_SHOP_LIST,ActiveForm);
		} else {
			string RNum;
			if (this.getRNum(out RNum)) {
				string itemSeq = this.Request.Params["GOTOLINK_" + RNum];
				string buyNum = this.Request.Params["buyItemNum_" + RNum];
				
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,String.Format("ViewGameItemShop.aspx?item_seq={0}&buy_num={1}",itemSeq,buyNum)));
			}
		}
	}

	private bool getRNum(out string RNum) {
		string index;

		for (int i = 1;i <= 10;i++) {
			index = i.ToString();
			if (this.Request.Params[ViCommConst.BUTTON_GOTO_LINK + "_" + index] != null) {
				RNum = index;
				return true;
			}
		}
		RNum = null;
		return false;
	}
}
