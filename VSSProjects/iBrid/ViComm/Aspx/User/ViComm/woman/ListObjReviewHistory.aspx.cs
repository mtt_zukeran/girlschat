/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝レビュー一覧
--	Progaram ID		: ListObjReviewHistory
--
--  Creation Date	: 2012.04.30
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListObjReviewHistory:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY,this.ActiveForm);
		}
	}
}
