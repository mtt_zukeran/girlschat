/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ��۸޺��Đ�������
--	Progaram ID		: DeleteBlogNgUser
--
--  Creation Date	: 2012.12.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteBlogNgUser:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.QueryString["partner_user_char_no"]);
		string sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);

		if (!this.IsPostBack) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_DIRECT,ActiveForm);
		} else {
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				string sSiteCd = sessionWoman.site.siteCd;
				string sUserSeq = sessionWoman.userWoman.userSeq;
				string sUserCharNo = sessionWoman.userWoman.curCharNo;
				using (BlogNgUser oBlogBlogNgUser = new BlogNgUser()) {
					oBlogBlogNgUser.DeleteBlogNgUser(sSiteCd,sUserSeq,sUserCharNo,sPartnerUserSeq,sPartnerUserCharNo);
				}

				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DeleteBlogNgUser.aspx?delete=1&partner_user_seq={0}&partner_user_char_no={1}&loginid={2}",sPartnerUserSeq,sPartnerUserCharNo,sLoginId)));
			} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBlogNgUser.aspx"));
			}
		}
	}
}