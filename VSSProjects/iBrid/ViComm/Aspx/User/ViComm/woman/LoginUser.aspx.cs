/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーログイン
--	Progaram ID		: LoginUser
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_LoginUser:MobileWomanPageBase {

	private string utn;
	private string id;
	private string loginId;
	private string loginPassword;
	private string snsType;
	private string snsId;
	private bool loginOk;
	private bool isPreviousLogin = false;

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			// 偽装状態の場合、偽造終了画面へ転送
			if (sessionWoman.IsImpersonated) {
				sessionWoman.parseContainer.parseUser.postAction = 0;
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + "/ViComm/woman",Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_RETURN_TO_ORIGINAL_MENU));
			}

			utn = this.GetStringValue(Request.QueryString["utn"]);
			id = this.GetStringValue(Request.QueryString["id"]);
			snsType = sessionObj.snsType;
			snsId = sessionObj.snsId;
			string sGoto = this.GetStringValue(Request.QueryString["goto"]);

			if (utn.Equals("") && id.Equals("") && snsType.Equals("") && snsId.Equals("")) {
				txtLoginId.Text = this.GetStringValue(Request.QueryString["uid"]);
				txtPassword.Text = this.GetStringValue(Request.QueryString["pw"]);
				if (txtLoginId.Text.Equals("") || txtPassword.Text.Equals("")) {
					txtLoginId.Text = this.GetStringValue(Request.QueryString["loginid"]);
					txtPassword.Text = this.GetStringValue(Request.QueryString["password"]);
				}

				if ((!txtLoginId.Text.Equals("")) && (!txtPassword.Text.Equals("")) && (!sGoto.Equals(""))) {
					cmdSubmit_Click(sender,e);
					return;
				}

			} else if (!string.IsNullOrEmpty(snsType) && !string.IsNullOrEmpty(snsId)) {
				using (User oUser = new User()) {
					if (snsType.Equals(PwViCommConst.SnsType.TWITTER)) {
						DataSet oDataSet = oUser.GetOneByTwitterId(snsId);
						if (oDataSet.Tables[0].Rows.Count > 0) {
							txtLoginId.Text = this.GetStringValue(oDataSet.Tables[0].Rows[0]["LOGIN_ID"]);
							txtPassword.Text = this.GetStringValue(oDataSet.Tables[0].Rows[0]["LOGIN_PASSWORD"]);
							cmdSubmit_Click(sender,e);
						}
					}
				}
			} else {
				if (this.GetStringValue(Request.QueryString["useutn"]).Equals("1")) {
					if (sessionWoman.userWoman.UsedTermId(utn,"",out loginId,out loginPassword)) {
						txtLoginId.Text = loginId;
						txtPassword.Text = loginPassword;
						cmdSubmit_Click(sender,e);
					}
				} else {
					if (sessionWoman.userWoman.UsedTermId("",id,out loginId,out loginPassword)) {
						txtLoginId.Text = loginId;
						txtPassword.Text = loginPassword;
						cmdSubmit_Click(sender,e);
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = "";

		if (sessionObj.isCrawler) {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				Session.Abandon();
				return;
			} else {
				return;
			}
		}

		if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
			loginOk = sessionWoman.userWoman.GetOneByTel(txtLoginId.Text,txtPassword.Text);
		} else {
			loginOk = sessionWoman.userWoman.GetOne(txtLoginId.Text,txtPassword.Text);
			if (loginOk == false && sessionWoman.userWoman.loginErrMsgCd.Equals(ViCommConst.ERR_STATUS_INVALID_ID_PW)) {
				loginOk = sessionWoman.userWoman.GetOneByPreviousId(txtLoginId.Text,txtPassword.Text);
				isPreviousLogin = true;
			}
		}

		if (loginOk) {
			using (User oUser = new User()) {
				oUser.GetOne(sessionWoman.userWoman.userSeq, ViCommConst.OPERATOR);
				if (oUser.adminUserFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sessionWoman.currentIModeId = string.Empty;
				}
			}
			sessionWoman.alreadyWrittenLoginLog = false;

			if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE) || (sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionWoman.adminFlg)) {
				HttpCookie cookie = new HttpCookie("maqia");
				cookie.Values["maqiauid"] = sessionWoman.userWoman.userSeq;
				cookie.Values["maqiapw"] = sessionWoman.userWoman.loginPassword;
				cookie.Values["maqiasex"] = ViCommConst.OPERATOR;
				cookie.Expires = DateTime.Now.AddDays(90);
				cookie.Domain = sessionWoman.site.hostNm;
				Response.Cookies.Add(cookie);
			}


			string sGoto = isPreviousLogin ? string.Empty : this.GetStringValue(Request.QueryString["goto"]);
			bool bImpersonate = this.GetStringValue(Request.QueryString["impersonate"]).Equals(ViCommConst.FLAG_ON_STR);
			string sCharNo;
			sCharNo = this.GetStringValue(Request.QueryString["charno"]);

			if (sCharNo.Equals("")) {
				sCharNo = ViCommConst.MAIN_CHAR_NO;
			}

			// Loginの確定により男性偽装分を破棄
			sessionWoman.originalManObj = null;

			// 2010.11.22 ATTENTION K.TABEI
			// ﾏﾙﾁｷｬﾗｸﾀのﾛｸﾞｲﾝ制御を行う場合で
			// SelectSite -> UserTop以外でもｷｬｽﾄをﾛｸﾞｲﾝ状態とする場合、下記のCASE毎に
			// SetLastLoginDateを実行する。
			// 現在はﾏﾙﾁｷｬﾗｸﾀのﾛｸﾞｲﾝ制御を行わず、SetLastActionDateで全てのｷｬﾗｸﾀをLogin状態に設定している。
			// ﾏﾙﾁｷｬﾗｸﾀのﾛｸﾞｲﾝ制御を行う場合はSetLastActionDateにSITE/CHAR_NOをParamとして渡す

			sessionWoman.userWoman.SetLastActionDate(sessionWoman.userWoman.userSeq);

			bool bMailAppAccess = false;
			if (sessionWoman.adminFlg == false) {
				using (CastNews oCastNews = new CastNews()) {
					oCastNews.LogCastNews(PwViCommConst.MAIN_SITE_CD,sessionWoman.userWoman.userSeq,sCharNo,"1");
				}

				if (!string.IsNullOrEmpty(sessionWoman.deviceToken)) {
					using (User oUser = new User()) {
						oUser.UpdateDeviceToken(sessionWoman.userWoman.userSeq,sessionWoman.deviceToken);
					}
					
					bMailAppAccess = true;

					sessionWoman.deviceToken = string.Empty;
				}
				
				if (IsAvailableService(ViCommConst.RELEASE_CERTIFY_NOTIFICATION_APP,2)) {
					if (!string.IsNullOrEmpty(sessionWoman.deviceUuid)) {
						sessionWoman.deviceUuid = string.Empty;
						sessionWoman.deviceId = string.Empty;
						sessionWoman.androidId = string.Empty;
						sessionWoman.macAddress = string.Empty;
						sessionWoman.deviceSerialNo = string.Empty;
						sessionWoman.deviceTel = string.Empty;
					}
				}

				if (!string.IsNullOrEmpty(sessionWoman.accessMailTemplateNo) && !string.IsNullOrEmpty(sessionWoman.accessTxMailDay)) {
					if (IsAvailableService(ViCommConst.RELEASE_ACCESS_MAIL_COUNT,2)) {
						using (AccessMailLog oAccessMailLog = new AccessMailLog()) {
							oAccessMailLog.RegistAccessMailLog(
								sessionWoman.site.siteCd,
								sessionWoman.userWoman.userSeq,
								sCharNo,
								sessionWoman.accessMailTemplateNo,
								ViCommConst.OPERATOR,
								sessionWoman.accessTxMailDay
							);
						}
					}
				}
				sessionWoman.accessMailTemplateNo = string.Empty;
				sessionWoman.accessTxMailDay = string.Empty;
			}

			if (bImpersonate) {
				UrlBuilder oUrlBuilder = new UrlBuilder(sGoto,this.Request.QueryString);
				oUrlBuilder.RemoveParameter("goto");
				oUrlBuilder.RemoveParameter("urlsex");
				oUrlBuilder.RemoveParameter("loginid");
				oUrlBuilder.RemoveParameter("password");
				oUrlBuilder.RemoveParameter("impersonate");
				oUrlBuilder.RemoveParameter("castid");

				oUrlBuilder.AddParameter("loginid",this.GetStringValue(this.Request.QueryString["castid"]));

				RedirectToMobilePage(sessionObj.root + string.Format("/ViComm/man/(S({0}))/{1}",sessionObj.sessionId,oUrlBuilder.ToString()));
			} else {
				switch (sGoto) {
					case "ReturnMail.aspx": {
							string sRxSiteCd,sRxMailSexCd,sTxUserSeq,sTxUserCharNo,sTxLoginId;
							using (MailLog oMailLog = new MailLog()) {
								oMailLog.GetRxMailInfo(this.GetStringValue(Request.QueryString["mailseq"]),out sRxSiteCd,out sRxMailSexCd,out sTxUserSeq,out sTxUserCharNo,out sTxLoginId);
							}
							sessionWoman.logined = true;
							CheckProfileComplite(sRxSiteCd,sCharNo);
							Dictionary<string,string> oParams = new Dictionary<string,string>();
							oParams.Add("mailseq",this.GetStringValue(Request.QueryString["mailseq"]));
							oParams.Add("direct",ViCommConst.FLAG_ON_STR);
							string sLinkName = string.Empty;
							
							if (IsAvailableService(ViCommConst.RELEASE_SP_LOGIN_RETURN_MAIL_CHAT,2)) {
								if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
									oParams.Add("loginid",sTxLoginId);
									oParams.Add("scrid","01");
									oParams.Add("stay",ViCommConst.FLAG_ON_STR);
									sLinkName = "#btm";
								}
							}
							
							if (bMailAppAccess) {
								if (IsAvailableService(ViCommConst.RELEASE_MAIL_APP_ACCESS_BONUS,2)) {
									string sResult = string.Empty;
									using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
										oUserDefPoint.AddBonusPoint(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,"CAST_ACCESS_MAIL_APP",out sResult);
									}
									
									if (sResult.Equals(ViCommConst.UserDefPointResult.OK)) {
										oParams.Add("appbonus",ViCommConst.FLAG_ON_STR);
									}
								}
							}
							
							oParams.Add("site",sessionWoman.site.siteCd);
							oParams.Add("charno",sCharNo);

							RedirectToMobilePage(sessionWoman.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString() + sLinkName));
						}
						break;

					case "ViewUserMail.aspx":
						sessionWoman.logined = true;
						sessionWoman.site.siteCd = this.GetStringValue(Request.QueryString["site"]);
						CheckProfileComplite(sessionWoman.site.siteCd,sCharNo);
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ViewUserMail.aspx?direct=1&mailseq=" + this.GetStringValue(Request.QueryString["mailseq"])) + "&site=" + sessionWoman.site.siteCd + "&charno=" + sCharNo);
						break;

					case "DisplayDoc.aspx":
						sessionWoman.logined = true;
						sessionWoman.site.siteCd = this.GetStringValue(Request.QueryString["site"]);
						CheckProfileComplite(sessionWoman.site.siteCd,sCharNo);
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + this.GetStringValue(Request.QueryString["doc"]) + "&site=" + sessionWoman.site.siteCd + "&charno=" + sCharNo));
						break;

					case "ViewGame.aspx": {
							string sGameType = this.GetStringValue(Request.QueryString["game_type"]);
							string sScore = this.GetStringValue(Request.QueryString["score"]);
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("ViewGame.aspx?game_type={0}&score={1}",sGameType,sScore)));
						}
						break;
					case "Profile.aspx": {
							sessionWoman.logined = true;
							sessionWoman.site.siteCd = this.GetStringValue(Request.QueryString["site"]);
							CheckProfileComplite(sessionWoman.site.siteCd,sCharNo);

							string sManId = this.GetStringValue(Request.QueryString["manid"]);

							Dictionary<string,string> oParams = new Dictionary<string,string>();
							oParams.Add("loginid",sManId);
							oParams.Add("direct",ViCommConst.FLAG_ON_STR);
							oParams.Add("site",sessionWoman.site.siteCd);
							oParams.Add("charno",sCharNo);
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));


						}
						break;
					case "SelectSite.aspx": {
							sessionWoman.logined = true;
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"SelectSite.aspx"));
						}
						break;

					case "RequestTop.aspx": {
							sessionWoman.logined = true;
							Dictionary<string,string> oParams = new Dictionary<string,string>();
							if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["prg_seq"]))) {
								oParams.Add("prg_seq",this.GetStringValue(Request.QueryString["prg_seq"]));
							}
							if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["ctg_seq"]))) {
								oParams.Add("ctg_seq",this.GetStringValue(Request.QueryString["ctg_seq"]));
							}
							oParams.Add("site",sessionWoman.site.siteCd);
							oParams.Add("charno",sCharNo);
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,new UrlBuilder(sGoto,oParams).ToString()));
						}
						break;

					case "DefectReportTop.aspx": {
							sessionWoman.logined = true;
							Dictionary<string,string> oParams = new Dictionary<string,string>();
							if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["drows"]))) {
								oParams.Add("drows",this.GetStringValue(Request.QueryString["drows"]));
							}
							oParams.Add("site",sessionWoman.site.siteCd);
							oParams.Add("charno",sCharNo);
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,new UrlBuilder(sGoto,oParams).ToString()));
						}
						break;
					
					default:
						sessionWoman.logined = true;
						if (string.IsNullOrEmpty(sGoto) || sGoto.Equals("LoginUser.aspx")) {		// Goto=LoginUser.aspxでID,PWが設定されていない場合
							if (isPreviousLogin) {
								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"SelectSite.aspx?oldloginid=1"));
							}
							
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"SelectSite.aspx"));
						} else {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sGoto + "?site=" + sessionWoman.site.siteCd + "&charno=" + sCharNo));
						}
						break;
				}
			}
		} else {
			if (sessionWoman.userWoman.loginErrMsgCd.Equals(ViCommConst.ERR_STATUS_CAST_LOGIN_RESIGNED)) {
				string sResult = string.Empty;
				sessionWoman.userWoman.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_LOGINID,sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword,out sResult);
				if (sResult.Equals("0")) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("LoginUser.aspx?loginid={0}&password={1}&goto=SelectSite.aspx",sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword)));
				}
			}
			lblErrorMessage.Text = GetErrorMessage(sessionWoman.userWoman.loginErrMsgCd);
		}
	}

	private void CheckProfileComplite(string pSiteCd,string pUserCharNo) {
		string sKey = pSiteCd + pUserCharNo;
		if (sessionWoman.userWoman.characterList.ContainsKey(sKey)) {
			if (sessionWoman.userWoman.characterList[sKey].registIncompleteFlag == 1 || sessionWoman.userWoman.characterList[sKey].profileResettingFlag == 1) {
				string[] sRequest = Request.Path.Split('/');
				if (!sRequest[sRequest.Length - 1].Equals("ModifyUserProfile.aspx")) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ModifyUserProfile.aspx?site=" + pSiteCd + "&charno=" + pUserCharNo));
				}
			}
		}
	}

	//マルチスレッド問題回避
	private string GetStringValue(object value) {
		if (value == null) {
			return "";
		} else {
			return value.ToString();
		}
	}
}
