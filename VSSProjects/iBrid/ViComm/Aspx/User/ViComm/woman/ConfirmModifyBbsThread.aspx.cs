﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド編集確認
--	Progaram ID		: ConfirmModifyBbsThread
--
--  Creation Date	: 2011.04.08
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;

public partial class ViComm_woman_ConfirmModifyBbsThread:MobileBbsExWomanBase {

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,ActiveForm)) {
				ViewState["DOC"] = sessionWoman.userWoman.bbsInfo.bbsThreadDoc;
				lblThreadDoc.Text = sessionWoman.userWoman.bbsInfo.bbsThreadDoc.Replace("\r\n","<BR />");
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBbsThread.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sNewBbsThreadSeq = string.Empty;

		using (BbsThread oBbsThread = new BbsThread()) {
			oBbsThread.WriteBbsThread(sessionWoman.site.siteCd,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]),
										Mobile.EmojiToCommTag(sessionWoman.carrier,iBridUtil.GetStringValue(ViewState["HANDLE_NM"])),
										string.Empty,
										Mobile.EmojiToCommTag(sessionWoman.carrier,iBridUtil.GetStringValue(ViewState["DOC"])),
										out sNewBbsThreadSeq);
		}
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("bbsthreadseq",sNewBbsThreadSeq);
		RedirectToDisplayDoc(ViCommConst.SCR_BBS_EX_MODIFY_THREAD_COMPLETE,oParam);
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionWoman.userWoman.bbsInfo.bbsThreadTitle = iBridUtil.GetStringValue(ViewState["TITLE"]);
		sessionWoman.userWoman.bbsInfo.bbsThreadDoc = iBridUtil.GetStringValue(ViewState["DOC"]);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ModifyBbsThread.aspx?back=1&bbsthreadseq={0}",iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]))));
	}
}
