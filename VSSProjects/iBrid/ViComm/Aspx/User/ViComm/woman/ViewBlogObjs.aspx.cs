﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ 写真・動画詳細
--	Progaram ID		: ViewBlogObj
--
--  Creation Date	: 2011.04.07
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web.UI.MobileControls;

public partial class ViComm_woman_ViewBlogObjs:MobileBlogWomanBase {
	protected const string PARAM_NM_OBJ_MOVIE = "movieseq";
	protected const string PARAM_NM_PLAY_MOVIE = "playmovie";

	protected void Page_Load(object sender,EventArgs e) {
		string sMovieSeq = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_OBJ_MOVIE]);
		string sPlayMovie = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_PLAY_MOVIE]);
		bool bDownload = false;

		if (!string.IsNullOrEmpty(sMovieSeq) && sPlayMovie.Equals(ViCommConst.FLAG_ON_STR)) {
			bDownload = Download(sMovieSeq);
		}

		if (!bDownload) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		}
	}
	private bool Download(string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (lSize > 0) {
				MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}
}
