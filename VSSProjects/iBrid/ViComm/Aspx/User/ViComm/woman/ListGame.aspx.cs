/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ������ްшꗗ
--	Progaram ID		: ListGame--
--  Creation Date	: 2011.03.29
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListGame : MobileWomanPageBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		sessionWoman.ControlList(
				this.Request,
				ViCommConst.INQUIRY_EXTENSION_GAME,
				this.ActiveForm);

	}
}
