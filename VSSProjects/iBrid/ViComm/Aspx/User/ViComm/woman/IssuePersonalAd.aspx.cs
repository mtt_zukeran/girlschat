/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 口コミコード発行
--	Progaram ID		: IssuePersonalAd
--
--  Creation Date	: 2010.12.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_IssuePersonalAd:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.errorMessage = string.Empty;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		string sResult = "";
		string sPersonalad = iBridUtil.GetStringValue(this.Request.Form["personalad"]);

		if (sPersonalad.Equals("")) {
			sessionWoman.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_PERSONAL_INVALID);
		} else {
			if (!SysPrograms.Expression(@"^[0-9]{4,6}$",sPersonalad)) {
				sessionWoman.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_PERSONAL_INVALID);
			}
		}
		if (!sessionWoman.errorMessage.Equals(string.Empty)) {
			return;
		}

		using (PersonalAd oPersonalAd = new PersonalAd()) {
			sResult = oPersonalAd.IssuePersonalAd(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sPersonalad);
		}
		switch (sResult) {
			case "A":
				sessionWoman.personalAdCd = sPersonalad;
				sessionWoman.errorMessage += string.Format(GetErrorMessage(ViCommConst.INFO_STATUS_ISSUE_PERSONAL_AD),sPersonalad);
				break;
			case "X":
				sessionWoman.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_PERSONAL_AD_USED),sPersonalad);
				break;
			default:
				sessionWoman.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_PERSONAL_AD_USED);
				break;
		}
	}
}
