/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド一覧
--	Progaram ID		: ListBbsThread
--
--  Creation Date	: 2010.04.11
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListBbsThread:MobileBbsExWomanBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sBbsThreadSeq = iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]);
			using (BbsThread oBbsThread = new BbsThread()) {
				oBbsThread.AccessBbsThread(sBbsThreadSeq);
			}
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,
					ActiveForm);
		}
	}

}
