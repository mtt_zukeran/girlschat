/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルつぶやき書き込み/完了
--	Progaram ID		: WriteCastTweet
--
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_WriteCastTweet:MobileWomanPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.errorMessage = string.Empty;
			tagTweetWrite.Visible = true;
			txtTweet.Visible = true;
			tagTweetWriteSub.Visible = true;
			tagTweetComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);

		bool bOk = true;

		if (txtTweet.Text.Equals("")) {
			bOk = false;
			//本文を入力して下さい。 
			sessionWoman.errorMessage += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_TWEET);
		}
		if (iMaxLength != 0 && txtTweet.Text.Length > iMaxLength) {
			bOk = false;
			sessionWoman.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"つぶやき");
		}

		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			string sNGWord;
			if (sessionWoman.ngWord.VaidateDoc(txtTweet.Text,out sNGWord) == false) {
				bOk = false;
				sessionWoman.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		string sSiteCd = sessionWoman.site.siteCd;
		string sUserSeq = sessionWoman.userWoman.userSeq;
		string sUserCharNo = sessionWoman.userWoman.curCharNo;
		string sTweetText = txtTweet.Text;
		string sCastTweetSeq = string.Empty;
		string sResult = string.Empty;

		if (bOk == false) {
			return;
		} else {
			sTweetText = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtTweet.Text));
			using (CastTweet oCastTweet = new CastTweet()) {
				oCastTweet.WriteCastTweet(sSiteCd,sUserSeq,sUserCharNo,sTweetText,out sCastTweetSeq,out sResult);
			}
			
			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				tagTweetWrite.Visible = false;
				txtTweet.Visible = false;
				tagTweetWriteSub.Visible = false;
				tagTweetComplete.Visible = true;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}
}