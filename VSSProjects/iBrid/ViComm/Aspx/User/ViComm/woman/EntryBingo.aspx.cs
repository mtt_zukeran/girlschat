﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ビンゴ参加
--	Progaram ID		: EntryBingo
--
--  Creation Date	: 2011.07.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_woman_EntryBingo : MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {

			if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["entry"])) {
				this.RedirectResultPage();
			}
		}
	}

	private void RedirectResultPage() {
		string sResult;
		using (BingoEntry oBingoEntry = new BingoEntry()) {
			sResult = oBingoEntry.EntryBingo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.OPERATOR);
		}

		IDictionary<string, string> oParameters = new Dictionary<string, string>();
		oParameters.Add("result", sResult);
		RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_BINGO_ENTRY_RESULT,oParameters);
	}

}
