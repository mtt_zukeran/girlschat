﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール返信ボーナスポイント付与実行
--	Progaram ID		: ExecuteAddMailResBonusPoint
--
--  Creation Date	: 2016.07.28
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ExecuteAddMailResBonusPoint:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["mailseq"]);
		
		string sParterMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["partnermailseq"]);
		string sMailDataSeq = iBridUtil.GetStringValue(this.Request.QueryString["data"]);
		string sVersion = iBridUtil.GetStringValue(this.Request.QueryString["version"]);

		if (string.IsNullOrEmpty(sMailSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		string sAddPoint = string.Empty;
		string sResult = string.Empty;
		string sParamVersion = string.Empty;

		// 3分/10分以内メール返信ボーナス
		if (sVersion.Equals("2")) {
			MailResBonusLog2 oMailResBonusLog2 = new MailResBonusLog2();
			oMailResBonusLog2.AddMailResBonusPoint(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sMailSeq,
				out sAddPoint,
				out sResult
			);
			sParamVersion = "&v=2";
		}
		// 3分以内メール返信ボーナス
		else {
			using (MailResBonus oMailResBonus = new MailResBonus()) {
				oMailResBonus.AddMailResBonusPoint(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sMailSeq,
					ViCommConst.FLAG_OFF,
					out sAddPoint,
					out sResult
				);
			}
		}

		if (!sResult.Equals(PwViCommConst.AddMailResBonusPoint.RESULT_NG)) {
			IDictionary<string,string> oParam = new Dictionary<string,string>();

			if (sResult.Equals(PwViCommConst.AddMailResBonusPoint.RESULT_NOT_SEND_MAIL)) {
				if (string.IsNullOrEmpty(sParterMailSeq) || string.IsNullOrEmpty(sMailDataSeq)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}
				
				string sValue;
				using (MailTemplate oTemplate = new MailTemplate()) {
					oTemplate.GetValue(sessionWoman.site.siteCd,sessionWoman.userWoman.mailData[sMailDataSeq].mailTemplateNo,"MAIL_ATTACHED_OBJ_TYPE",out sValue);
				}
				
				if (sValue.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&partnermailseq={3}&resbonusreturn=1{4}",ViCommConst.SCR_WOMAN_PIC_RETURN_MAILER_COMPLITE,sMailDataSeq,sMailSeq,sParterMailSeq,sParamVersion)));
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&partnermailseq={3}&resbonusreturn=1{4}",ViCommConst.SCR_WOMAN_MOVIE_RETURN_MAILER_COMPLITE,sMailDataSeq,sMailSeq,sParterMailSeq,sParamVersion)));
				}
				
			} else {
				oParam.Add("result",sResult);

				if (sResult.Equals(PwViCommConst.AddMailResBonusPoint.RESULT_OK)) {
					oParam.Add("add",sAddPoint);
				}
			}

			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_MAIL_RES_BONUS_COMPLETE,oParam);

		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}
