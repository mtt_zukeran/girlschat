/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログコメント一覧
--	Progaram ID		: ListBlogComment
--
--  Creation Date	: 2012.12.26
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListBlogComment:MobileBlogWomanBase {

	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_BLOG_COMMENT,this.ActiveForm);
	}
}
