/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール未送信BOX
--	Progaram ID		: ListDraftMail
--
--  Creation Date	: 2013.09.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListDraftMail:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_DRAFT_MAIL,this.ActiveForm);
	}
}