/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �f���ʐ^�E����Ǘ�
--	Progaram ID		: ViewBbsObjConf
--
--  Creation Date	: 2010.09.10
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web.UI.MobileControls;

public partial class ViComm_woman_ViewBbsObjConf:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;

		if (!string.IsNullOrEmpty(sMovieSeq)) {
			bDownload = Download(sMovieSeq);
		}

		if (!IsPostBack) {
			if (bDownload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
				
				if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
					string sTitle = sessionWoman.GetBbsObjValue("OBJ_TITLE");
					string sDoc = sessionWoman.GetBbsObjValue("OBJ_DOC");
					string sTypeSeq = sessionWoman.GetBbsObjValue("CAST_OBJ_ATTR_TYPE_SEQ");
					string sAttrSeq = sessionWoman.GetBbsObjValue("CAST_OBJ_ATTR_SEQ");
					string sObjKind = sessionWoman.GetBbsObjValue("OBJ_KIND");
					string sObjRankingFlag = sessionWoman.GetBbsObjValue("OBJ_RANKING_FLAG");

					txtTitle.Text = iBridUtil.GetStringValue(sTitle).Replace("\r\n",string.Empty).Replace("\n",string.Empty).Replace("\r",string.Empty);
					txtDoc.Text = sDoc;
					ViewState["ATTR_TYPE_SEQ"] = sTypeSeq;
					ViewState["ATTR_SEQ"] = sAttrSeq;

					SetDropDownList(ViewState["ATTR_TYPE_SEQ"].ToString(),ViewState["ATTR_SEQ"].ToString());

					foreach (MobileListItem item in lstRankingFlag.Items) {
						if (item.Value == sObjRankingFlag) {
							item.Selected = true;
							break;
						}
					}
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListBbsObjConf.aspx"));
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sObjKind = sessionWoman.GetBbsObjValue("OBJ_KIND");
		switch (sObjKind) {
			case ViCommConst.BbsObjType.PIC:
				SubmitPic();
				break;
			case ViCommConst.BbsObjType.MOVIE:
				SubmitMovie();
				break;
			default:
				break;
		}
	}

	protected void cmdDelete_Click(object sender,EventArgs e) {
		if (!this.IsDeletable()) {
			return;
		}
		string sObjKind = sessionWoman.GetBbsObjValue("OBJ_KIND");
		switch (sObjKind) {
			case ViCommConst.BbsObjType.PIC:
				string sPicSeq = sessionWoman.GetBbsObjValue("PIC_SEQ");
				DeletePic(sPicSeq);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_BBS_PIC) + "&site=" + sessionWoman.site.siteCd);
				break;
			case ViCommConst.BbsObjType.MOVIE:
				string sMovieSeq = sessionWoman.GetBbsObjValue("MOVIE_SEQ");
				DeleteMovie(sMovieSeq);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_BBS_MOVIE) + "&site=" + sessionWoman.site.siteCd);
				break;
			default:
				break;
		}
	}

	#region ----------------------------------------------------------PIC
	protected void SubmitPic() {
		string sPicSeq = sessionWoman.GetBbsObjValue("PIC_SEQ");
		bool bUpdate = false;

		if (IsAvailableService(ViCommConst.RELEASE_DISABLE_BBS_MODIFY_LIST)) {
			bUpdate = true;
		} else if (lstSetUp.SelectedIndex == 0) {
			bUpdate = true;
		}

		if (bUpdate) {
			txtTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
			txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

			bool bOk = ValidText();
			if (bOk == false) {
				return;
			} else {
				UpdatePic(sPicSeq,ViCommConst.FLAG_OFF);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_UPDATED_CAST_BBS_PIC) + "&site=" + sessionWoman.site.siteCd);
			}
		} else {
			DeletePic(sPicSeq);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_BBS_PIC) + "&site=" + sessionWoman.site.siteCd);
		}
	}

	private void UpdatePic(string pPicSeq,int pDelFlag) {
		bool bUnauthFlag = sessionWoman.GetBbsObjValue("OBJ_NOT_APPROVE_FLAG").Equals("1");
		string sCastPicAttrTypeSeq;
		string sCastPicAttrSeq;
		if (lstAttrSeq.Visible) {
			string[] sCastPicAttr = lstAttrSeq.Selection.Value.Split(':');
			sCastPicAttrTypeSeq = sCastPicAttr[0];
			sCastPicAttrSeq = sCastPicAttr[1];
		} else {
			sCastPicAttrTypeSeq = ViewState["ATTR_TYPE_SEQ"].ToString();
			sCastPicAttrSeq = ViewState["ATTR_SEQ"].ToString();
		}
		string sTitle = Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text);
		string sDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
		if (sTitle.Length > 30) {
			sTitle = SysPrograms.Substring(sTitle,30);
		}
		if (sDoc.Length > 1000) {
			sDoc = SysPrograms.Substring(sDoc,1000);
		}
		int iRankingFlag;
		if (!int.TryParse(lstRankingFlag.Items[lstRankingFlag.SelectedIndex].Value,out iRankingFlag)) {
			iRankingFlag = 1;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sessionWoman.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.curCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_BBS);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,bUnauthFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,sTitle);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,sDoc);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,sCastPicAttrTypeSeq);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,sCastPicAttrSeq);
			db.ProcedureInParm("POBJ_RANKING_FLAG",DbSession.DbType.NUMBER,iRankingFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}

		string sRotateType = iBridUtil.GetStringValue(this.Request.Params["rotate_type"]);
		RotateFlipType oRoateType = ImageUtil.Convert2RotateFlipType(sRotateType);

		string sWebPhisicalDir = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(sessionWoman.site.siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}
		string sCastPicDir = ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sessionWoman.site.siteCd,sessionWoman.userWoman.loginId);
		ImageUtil.RotateFilp(sCastPicDir,pPicSeq,oRoateType);


	}

	private void DeletePic(string pPicSeq) {
		UpdatePic(pPicSeq,ViCommConst.FLAG_ON);
		string sFullPath = sessionWoman.site.webPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
							string.Format("{0:D15}",int.Parse(pPicSeq));
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath + ViCommConst.PIC_FOODER)) {
				System.IO.File.Delete(sFullPath + ViCommConst.PIC_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.PIC_FOODER_SMALL)) {
				System.IO.File.Delete(sFullPath + ViCommConst.PIC_FOODER_SMALL);
			}
		}
	}
	#endregion -------------------------------------------------------PIC

	#region ----------------------------------------------------------MOVIE
	protected void SubmitMovie() {
		string sMovieSeq = sessionWoman.GetBbsObjValue("MOVIE_SEQ");
		bool bUpdate = false;

		if (IsAvailableService(ViCommConst.RELEASE_DISABLE_BBS_MODIFY_LIST)) {
			bUpdate = true;
		} else if (lstSetUp.SelectedIndex == 0) {
			bUpdate = true;
		}

		if (bUpdate) {
			txtTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
			txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

			bool bOk = ValidText();
			if (bOk == false) {
				return;
			} else {
				UpdateMovie(sMovieSeq,ViCommConst.FLAG_OFF);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_UPDATED_CAST_BBS_MOVIE) + "&site=" + sessionWoman.site.siteCd);
			}
		} else {
			DeleteMovie(sMovieSeq);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_BBS_MOVIE) + "&site=" + sessionWoman.site.siteCd);
		}
	}

	private void UpdateMovie(string sMovieSeq,int pDelFlag) {
		string sAttrTypeSeq = "";
		string sAttrSeq = "";

		if (lstAttrSeq.Visible) {
			string[] sAttr = lstAttrSeq.Selection.Value.Split(':');
			sAttrTypeSeq = sAttr[0];
			sAttrSeq = sAttr[1];
		} else {
			sAttrTypeSeq = ViewState["ATTR_TYPE_SEQ"].ToString();
			sAttrSeq = ViewState["ATTR_SEQ"].ToString();
		}
		string sTitle = Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text);
		string sDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
		if (sTitle.Length > 30) {
			sTitle = SysPrograms.Substring(sTitle,30);
		}
		if (sDoc.Length > 1000) {
			sDoc = SysPrograms.Substring(sDoc,1000);
		}
		int iRankingFlag;
		if (!int.TryParse(lstRankingFlag.Items[lstRankingFlag.SelectedIndex].Value,out iRankingFlag)) {
			iRankingFlag = 1;
		}
		using (CastMovie oMovie = new CastMovie()) {
			oMovie.UpdateProfileMovie(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sMovieSeq,
				sTitle,
				sDoc,
				pDelFlag,
				ViCommConst.ATTACHED_BBS,
				sAttrTypeSeq,
				sAttrSeq,
				iRankingFlag
			);
		}
	}

	private void DeleteMovie(string sMovieSeq) {
		UpdateMovie(sMovieSeq,ViCommConst.FLAG_ON);
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string sFullPath = sessionWoman.site.webPhisicalDir +
								ViCommConst.MOVIE_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
								string.Format("{0:D15}",int.Parse(sMovieSeq));

			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + "s" + ViCommConst.MOVIE_FOODER)) {
				System.IO.File.Delete(sFullPath + "s" + ViCommConst.MOVIE_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER2)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER2);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER3)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER3);
			}			
		}
	}

	private bool Download(string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}
	#endregion -------------------------------------------------------MOVIE

	private void SetDropDownList(string pCastPicAttrTypeSeq,string pCastPicAttrSeq) {
		if (IsAvailableService(ViCommConst.RELEASE_DISABLE_BBS_MODIFY_LIST)) {
			lstSetUp.Visible = false;
		} else {
			lstSetUp.Items.Add(new MobileListItem("�C������","1"));
			lstSetUp.Items.Add(new MobileListItem("�폜����","2"));
		}

		int iIdx = 0;

		using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
			DataSet ds = oCastPicAttrTypeValue.GetList(sessionWoman.site.siteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstAttrSeq.Items.Add(new MobileListItem(dr["CAST_PIC_ATTR_TYPE_NM"].ToString() + ":" + dr["CAST_PIC_ATTR_NM"].ToString(),dr["CAST_PIC_ATTR_TYPE_SEQ"].ToString() + ":" + dr["CAST_PIC_ATTR_SEQ"].ToString()));
				if (dr["CAST_PIC_ATTR_TYPE_SEQ"].ToString().Equals(pCastPicAttrTypeSeq) && dr["CAST_PIC_ATTR_SEQ"].ToString().Equals(pCastPicAttrSeq)) {
					lstAttrSeq.SelectedIndex = iIdx;
				}
				iIdx++;
			}
		}

		if (sessionWoman.site.bbsPicAttrFlag != ViCommConst.FLAG_OFF) {
			lstAttrSeq.Visible = true;
			if (sessionWoman.site.supportChgObjCatFlag == ViCommConst.FLAG_OFF) {
				lstAttrSeq.Visible = false;
			}
		} else {
			lstAttrSeq.Visible = false;
		}
	}

	private bool ValidText() {
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		bool bOk = true;
		if (txtTitle.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		return bOk;
	}

	private bool IsDeletable() {
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		bool bOk = true;

		using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
			if (!oCastPicAttrTypeValue.IsDeletable(sessionWoman.site.siteCd,iBridUtil.GetStringValue(ViewState["ATTR_SEQ"]))) {
				this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_POSTER_DEL_NA);
				bOk = false;
			}
		}
		return bOk;
	}
}
