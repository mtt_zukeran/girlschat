/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女性用お宝一覧
--	Progaram ID		: ListGameTreasure
--
--  Creation Date	: 2011.10.17
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameTreasure:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sStageGroupType = iBridUtil.GetStringValue(this.Request.Params["stage_group_type"]);

		if (sStageGroupType.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE,ActiveForm);
	}
}
