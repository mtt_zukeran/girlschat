/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳画像登録
--	Progaram ID		: RegistYakyukenPicTmp
--
--  Creation Date	: 2013.04.30
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistYakyukenPicTmp:MobileWomanPageBase {
	private string sYakyukenGameSeq;
	private string sYakyukenType;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		sYakyukenGameSeq = iBridUtil.GetStringValue(Request.QueryString["ygameseq"]);
		sYakyukenType = iBridUtil.GetStringValue(Request.QueryString["ytype"]);

		if (string.IsNullOrEmpty(sYakyukenGameSeq) || string.IsNullOrEmpty(sYakyukenType)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (CheckInputValue()) {
			string sYakyukenPicTmpSeq = string.Empty;
			string sResult = string.Empty;

			using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
				oYakyukenPic.RegistYakyukenPicTmp(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sYakyukenGameSeq,
					sYakyukenType,
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtCommentText.Text)),
					out sYakyukenPicTmpSeq,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK) && !string.IsNullOrEmpty(sYakyukenPicTmpSeq)) {
				IDictionary<string,string> oParam = new Dictionary<string,string>();
				oParam.Add("ypictmpseq",sYakyukenPicTmpSeq);
				oParam.Add("ygameseq",sYakyukenGameSeq);
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_REGIST_YAKYUKEN_PIC_TMP_COMPLETE,oParam);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}

	protected bool CheckInputValue() {
		bool bOk = true;
		string sNGWord;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtCommentText.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄを入力して下さい。";
		} else if (txtCommentText.Text.Length < 10) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は10文字以上です。";
		} else if (txtCommentText.Text.Length > 50) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は50文字以内です。";
		} else if (sessionWoman.ngWord.VaidateDoc(txtCommentText.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄに禁止語句が含まれています。";
		}

		return bOk;
	}
}
