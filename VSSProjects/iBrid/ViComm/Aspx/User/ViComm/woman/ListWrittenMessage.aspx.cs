/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ү���ވꗗ
--	Progaram ID		: ListWrittenMessage 
--
--  Creation Date	: 2010.07.23
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListWrittenMessage:MobileWomanPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!sessionWoman.logined) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			} else {
				sessionWoman.ControlList(Request,0,ActiveForm);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> oMessageSeqList = new List<string>();

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.StartsWith("checkbox")) {
				oMessageSeqList.Add(Request.Form[sKey]);
			}
		}
		if (oMessageSeqList.Count > 0) {
			using (Message oMessage = new Message()) {
				oMessage.Delete(oMessageSeqList.ToArray());
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_WOMAN_MESSAGE + "&msg_delete_count=" + oMessageSeqList.Count));
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "ListWrittenMessage.aspx?" + Request.QueryString));
		}
	}
}
