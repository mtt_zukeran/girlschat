/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい投稿
--	Progaram ID		: WriteRequest
--
--  Creation Date	: 2012.07.03
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_WriteRequest:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;

			this.setLstCategory();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	private void setLstCategory() {
		using (PwRequest oPwRequest = new PwRequest()) {
			string sBlogFlag = (sessionWoman.userWoman.CurCharacter.characterEx.enabledBlogFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
			string sRichFlag = (sessionWoman.userWoman.CurCharacter.characterEx.enabledRichinoFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;

			DataSet dsCategory = oPwRequest.GetRequestCategory(sessionWoman.site.siteCd,ViCommConst.OPERATOR,sBlogFlag,sRichFlag);

			foreach (DataRow drCategory in dsCategory.Tables[0].Rows) {
				lstCategory.Items.Add(new MobileListItem(drCategory["REQUEST_CATEGORY_NM"].ToString(),drCategory["REQUEST_CATEGORY_SEQ"].ToString()));
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;

		if (txtTitle.Text.Equals(string.Empty)) {
			sessionWoman.errorMessage = "ﾀｲﾄﾙを入力して下さい";
			return;
		} else {
			if (txtTitle.Text.Length > 42) {
				sessionWoman.errorMessage = "ﾀｲﾄﾙが長すぎます";
				return;
			}
		}

		if (lstCategory.Items[lstCategory.SelectedIndex].Value.Equals(string.Empty)) {
			sessionWoman.errorMessage = "ｶﾃｺﾞﾘを選択して下さい";
			return;
		}

		if (txtText.Text.Equals(string.Empty)) {
			sessionWoman.errorMessage = "提案内容を入力して下さい";
			return;
		} else {
			if (txtText.Text.Length > 1000) {
				sessionWoman.errorMessage = "提案内容が長すぎます";
				return;
			}

			string sNGWord = string.Empty;

			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}

			if (!sessionWoman.ngWord.VaidateDoc(txtText.Text,out sNGWord)) {
				sessionWoman.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		ViewState["TITLE"] = txtTitle.Text;
		ViewState["REQUEST_CATEGORY_SEQ"] = lstCategory.Items[lstCategory.SelectedIndex].Value;
		ViewState["TEXT"] = txtText.Text;

		lblTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
		lblCategory.Text = lstCategory.Items[lstCategory.SelectedIndex].Text;
		lblText.Text = HttpUtility.HtmlEncode(txtText.Text).Replace(System.Environment.NewLine,"<br>");

		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
	}

	protected void cmdTx_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		string sTitle = iBridUtil.GetStringValue(ViewState["TITLE"]);
		string sRequestCategorySeq = iBridUtil.GetStringValue(ViewState["REQUEST_CATEGORY_SEQ"]);
		string sText = iBridUtil.GetStringValue(ViewState["TEXT"]);
		string sRequestSeq;
		string sResult = string.Empty;

		using (PwRequest oPwRequest = new PwRequest()) {
			sResult = oPwRequest.WriteRequest(
				sessionWoman.site.siteCd,
				ViCommConst.OPERATOR,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sTitle)),
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sText)),
				sRequestCategorySeq,
				out sRequestSeq
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("req_seq",sRequestSeq);
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_WRITE_REQUEST_COMPLETE,oParam);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
	}
}