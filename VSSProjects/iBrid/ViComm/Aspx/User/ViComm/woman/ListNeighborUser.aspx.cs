/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 24h以内にログインした近所の男性一覧
--	Progaram ID		: ListNeighborUser
--
--  Creation Date	: 2015.02.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListNeighborUser:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_NEIGHBOR_USER,this.ActiveForm);
	}
}