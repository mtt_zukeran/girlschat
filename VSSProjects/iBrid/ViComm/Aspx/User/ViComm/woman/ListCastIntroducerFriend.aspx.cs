﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 友達紹介一覧(女性)
--	Progaram ID		: ListCastIntroducerFriend
--
--  Creation Date	: 2011.03.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListCastIntroducerFriend: MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		sessionWoman.ClearCategory();
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_INTRODUCER_FRIEND,
					ActiveForm);
		}

	}
}
