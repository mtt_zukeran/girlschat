/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ つぶやき削除
--	Progaram ID		: DeleteCastTweet
--
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_DeleteCastTweet:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_CAST_TWEET,this.ActiveForm);
		} else {
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				string sSiteCd = sessionWoman.site.siteCd;
				string sUserSeq = sessionWoman.userWoman.userSeq;
				string sUserCharNo = sessionWoman.userWoman.curCharNo;
				string sCastTweetSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetseq"]);
				string sResult = string.Empty;
				
				using (CastTweet oCastTweet = new CastTweet()) {
					oCastTweet.DeleteCastTweet(sSiteCd,sUserSeq,sUserCharNo,sCastTweetSeq,out sResult);
				}
				
				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_DELETE_TWEET_COMPLETE);
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}
			} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListSelfCastTweet.aspx"));
			}
		}
	}
}