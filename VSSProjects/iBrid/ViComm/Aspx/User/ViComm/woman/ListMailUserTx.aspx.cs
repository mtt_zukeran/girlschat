/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信者一覧
--	Progaram ID		: ListMailUserTx
--
--  Creation Date	: 2012.11.30
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Web;
using System.Collections.Generic;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListMailUserTx:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_MAIL_USER_TX,ActiveForm);
		}
	}
}
