/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ｱｲﾃﾑ使用してﾅﾝﾊﾟ
--	Progaram ID		: RegistGameClearMissionRecoveryForce
--
--  Creation Date	: 2011.10.01
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_RegistGameClearMissionRecoveryForce:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sStageSeq = iBridUtil.GetStringValue(this.Request.QueryString["stage_seq"]);
		string sTownSeq = iBridUtil.GetStringValue(this.Request.QueryString["town_seq"]);

		if (string.IsNullOrEmpty(sStageSeq) || string.IsNullOrEmpty(sTownSeq)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CLEAR_MISSION_RECOVERY_FORCE,ActiveForm);
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				string sNoRecoveryItemFlag = iBridUtil.GetStringValue(this.Request.Params["NEXTLINK"]);
				string sPreCastGamePoint = sessionWoman.userWoman.CurCharacter.gameCharacter.castGamePoint.ToString();
				string sPreMissionForceCount = sessionWoman.userWoman.CurCharacter.gameCharacter.missionForceCount.ToString();
				string sPreAttackForceCount = sessionWoman.userWoman.CurCharacter.gameCharacter.attackForceCount.ToString();
				string sPreDefenceForceCount = sessionWoman.userWoman.CurCharacter.gameCharacter.defenceForceCount.ToString();

				this.RecoveryForce(sNoRecoveryItemFlag);

				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				oParameters.Add("stage_seq",sStageSeq);
				oParameters.Add("town_seq",sTownSeq);
				oParameters.Add("no_recovery_item_flag",sNoRecoveryItemFlag);
				oParameters.Add("pre_point",sPreCastGamePoint);
				oParameters.Add("pre_mission",sPreMissionForceCount);
				oParameters.Add("pre_attack",sPreAttackForceCount);
				oParameters.Add("pre_defence",sPreDefenceForceCount);
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_RECOVERY_FORCE_COMP,oParameters);
			}
		}
	}

	private void RecoveryForce(string sNoRecoveryItemFlag) {
		string sGameItemSeq;
		string sRecoveryForceResult;

		using (GameItem oGameItem = new GameItem()) {
			sGameItemSeq = oGameItem.GetGameItem(
				this.sessionWoman.site.siteCd,
				ViCommConst.OPERATOR,
				PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE,
				"GAME_ITEM_SEQ"
			);

			if (sNoRecoveryItemFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				string sBuyGameItemResult;

				oGameItem.BuyGameItem(
					this.sessionWoman.site.siteCd,
					this.sessionWoman.userWoman.userSeq,
					this.sessionWoman.userWoman.curCharNo,
					sGameItemSeq,
					"1",
					out sBuyGameItemResult
				);

				if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
					sessionWoman.userWoman.CurCharacter.gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);
				} else if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_NO_MONEY)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_NO_POINT);
				} else if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_NG)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
			}
		}

		using (RecoveryForce oRecoveryForce = new RecoveryForce()) {
			oRecoveryForce.RecoveryForceFull(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				0,
				int.Parse(sGameItemSeq),
				PwViCommConst.GameMskForce.MSK_ALL_FORCE,
				out sRecoveryForceResult
			);
		}

		if (!sRecoveryForceResult.Equals(PwViCommConst.GameRecoveryForceFullStatus.RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}
}