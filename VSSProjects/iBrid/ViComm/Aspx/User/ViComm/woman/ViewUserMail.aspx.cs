/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーメール詳細表示
--	Progaram ID		: ViewUserMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewUserMail:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		bool bDownload = false;

		if (!IsPostBack) {
			string sUserManSeq = iBridUtil.GetStringValue(Request.QueryString["usermanseq"]);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);

			if (!sMovieSeq.Equals("") && sPlayMovie.Equals("1")) {
				bDownload = Download(sUserManSeq,ViCommConst.MAIN_CHAR_NO,sMovieSeq);
			}
			if (bDownload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

				if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm)) {
					if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailDel(sessionWoman.GetMailValue("MAIL_SEQ"),ViCommConst.RX);
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_MAN_MAIL + "&mail_delete_count=1"));
						}
					} else {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailReadFlag(sessionWoman.GetMailValue("MAIL_SEQ"));
						}
					}
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListUserMailBox.aspx"));
				}
			}
		} else {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdProtect"] != null) {
				cmdProtect_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm);
		sessionWoman.errorMessage = "";

		if (!sessionWoman.GetMailValue("ATTACHED_OBJ_OPEN_FLAG").Equals("1")) {
			if (sessionWoman.GetMailValue("ATTACHED_OBJ_TYPE").Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailAttaChedOpenFlag(sessionWoman.GetMailValue("MAIL_SEQ"));
					sessionWoman.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","1");
				}
			}
		}
	}

	protected void cmdProtect_Click(object sender,EventArgs e) {
		sessionWoman.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm);
		int iProtectFlag = int.Parse(sessionWoman.GetMailValue("DEL_PROTECT_FLAG")) == 1 ? 0 : 1;
		string sMailSeq = sessionWoman.GetMailValue("MAIL_SEQ");
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sMailSeq,iProtectFlag);
			sessionWoman.SetMailValue("DEL_PROTECT_FLAG",iProtectFlag.ToString());
		}
	}

	private bool Download(string pUserManSeq,string pUserCharNo,string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.MAN,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				if (sessionWoman.GetMailValue("ATTACHED_OBJ_TYPE").Equals("1") == false) {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailAttaChedOpenFlag(sessionWoman.GetMailValue("MAIL_SEQ"));
					}
					sessionWoman.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","1");
				}

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.MAN);

				return true;
			} else {
				return false;
			}
		}
	}
}

