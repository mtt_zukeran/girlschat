/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����(�����)
--	Progaram ID		: ViewGameLotteryNotComp
--
--  Creation Date	: 2012.06.16
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public partial class ViComm_woman_ViewGameLotteryNotComp:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_LOTTERY_NOT_COMP,ActiveForm);
		} else {

			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,int.Parse(iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"])),0);
			} else if (Request.Params[PwViCommConst.BUTTON_SUBMIT_SET_1] != null) {
				cmdSubmit_Click(sender,e,int.Parse(iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"])),1);
			} else if (Request.Params[PwViCommConst.BUTTON_SUBMIT_SET_2] != null) {
				cmdSubmit_Click(sender,e,int.Parse(iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"])),2);
			} else if (Request.Params[PwViCommConst.BUTTON_SUBMIT_SET_3] != null) {
				cmdSubmit_Click(sender,e,int.Parse(iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"])),3);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e,int iLotterySeq,int iLotteryPattern) {

		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sSexCd = ViCommConst.OPERATOR;
		int iSetLotteryItemGetLogSeq = 0;
		int iFirstBuyFreeFlag = 0;
		string sResult = string.Empty;
		string sPrePoint = this.sessionWoman.userWoman.CurCharacter.gameCharacter.castGamePoint.ToString();
		string sPreMaxAttackPower = this.sessionWoman.userWoman.CurCharacter.gameCharacter.maxAttackPower.ToString();
		string sPreMaxDefencePower = this.sessionWoman.userWoman.CurCharacter.gameCharacter.maxDefencePower.ToString();

		using (Lottery oLottery = new Lottery()) {
			oLottery.GetLotteryNotComp(
				sSiteCd,
				sUserSeq,
				sUserCharNo,
				sSexCd,
				iLotterySeq,
				iLotteryPattern,
				out iSetLotteryItemGetLogSeq,
				out iFirstBuyFreeFlag,
				out sResult
			);
		}

		if (!sResult.Equals(PwViCommConst.GameLotteryResult.RESULT_NG)) {
			StringBuilder oUrl = new StringBuilder();

			if (sResult.Equals(PwViCommConst.GameLotteryResult.RESULT_OK) && iFirstBuyFreeFlag == ViCommConst.FLAG_ON) {
				this.CheckQuestClear();
			}

			if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
				if (sResult.Equals(PwViCommConst.GameLotteryResult.RESULT_OK)) {
					IDictionary<string,string> oParameters = new Dictionary<string,string>();

					oParameters.Add("lottery_seq",iLotterySeq.ToString());
					oParameters.Add("set_lottery_item_get_log_seq",iSetLotteryItemGetLogSeq.ToString());
					oParameters.Add("lottery_pattern",iLotteryPattern.ToString());
					oParameters.Add("result",sResult);
					oParameters.Add("pre_point",sPrePoint);
					oParameters.Add("pre_max_attack_power",sPreMaxAttackPower);
					oParameters.Add("pre_max_defence_power",sPreMaxDefencePower);

					RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_LOTTERY_SP,oParameters);
				} else {

					if (iLotteryPattern == 0) {
						oUrl.Append("ViewGameLotteryGetItemResultNotComp.aspx");
					} else {
						oUrl.Append("ListGameLotteryGetItemResultNotComp.aspx");
					}
					oUrl.AppendFormat("?lottery_seq={0}",iLotterySeq.ToString());
					oUrl.AppendFormat("&set_lottery_item_get_log_seq={0}",iSetLotteryItemGetLogSeq.ToString());
					oUrl.AppendFormat("&lottery_pattern={0}",iLotteryPattern.ToString());
					oUrl.AppendFormat("&result={0}",sResult);
					oUrl.AppendFormat("&pre_point={0}",sPrePoint);
					oUrl.AppendFormat("&pre_max_attack_power={0}",sPreMaxAttackPower);
					oUrl.AppendFormat("&pre_max_defence_power={0}",sPreMaxDefencePower);

					string sRedirectUrl = oUrl.ToString();

					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectUrl));
				}
			} else {
				if (sResult.Equals(PwViCommConst.GameLotteryResult.RESULT_OK)) {
					oUrl.Append("ViewGameFlashLotteryNotComp.aspx");
				} else {
					if (iLotteryPattern == 0) {
						oUrl.Append("ViewGameLotteryGetItemResultNotComp.aspx");
					} else {
						oUrl.Append("ListGameLotteryGetItemResultNotComp.aspx");
					}
				}
				oUrl.AppendFormat("?lottery_seq={0}",iLotterySeq.ToString());
				oUrl.AppendFormat("&set_lottery_item_get_log_seq={0}",iSetLotteryItemGetLogSeq.ToString());
				oUrl.AppendFormat("&lottery_pattern={0}",iLotteryPattern.ToString());
				oUrl.AppendFormat("&result={0}",sResult);
				oUrl.AppendFormat("&pre_point={0}",sPrePoint);
				oUrl.AppendFormat("&pre_max_attack_power={0}",sPreMaxAttackPower);
				oUrl.AppendFormat("&pre_max_defence_power={0}",sPreMaxDefencePower);

				string sRedirectUrl = oUrl.ToString();

				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectUrl));
			}
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				PwViCommConst.GameQuestTrialCategory.LOTTERY,
				PwViCommConst.GameQuestTrialCategoryDetail.LOTTERY,
				out sQuestClearFlag,
				out sResult,
				ViCommConst.OPERATOR
			);
		}
	}
}
