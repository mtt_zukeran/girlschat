/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �싅���Q�[���ǉ�
--	Progaram ID		: RegistYakyukenGame
--
--  Creation Date	: 2013.04.29
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistYakyukenGame:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			string sYakyukenGameSeq = string.Empty;
			string sResult = string.Empty;

			using (YakyukenGame oYakyukenGame = new YakyukenGame()) {
				oYakyukenGame.RegistYakyukenGame(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					out sYakyukenGameSeq,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK) && !string.IsNullOrEmpty(sYakyukenGameSeq)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListYakyukenPic.aspx?ygameseq={0}",sYakyukenGameSeq)));
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}
}
