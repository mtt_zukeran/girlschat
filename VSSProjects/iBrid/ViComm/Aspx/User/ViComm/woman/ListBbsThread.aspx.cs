/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド一覧
--	Progaram ID		: ListBbsThread
--
--  Creation Date	: 2010.04.11
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListBbsThread:MobileBbsExWomanBase {
	const string THREAD_SEARCH = "1";
	const string RES_SEARCH = "2";

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,
					ActiveForm);

			lblErrorMessage.Text = string.Empty;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sSearchType = iBridUtil.GetStringValue(Request.Form["rdoSearchType"]);
		string sSearchKey = HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["txtSearchKey"]),enc);
		if (sSearchKey.Equals(string.Empty)) {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_BBS_SEARCH_KEY_EMPTY);
		} else {
			string sDrows = string.Empty;
			string sLrows = string.Empty;
			string sList = string.Empty;
			string sScrid = string.Empty;
			if (sSearchType.Equals(THREAD_SEARCH)) {
				sDrows = iBridUtil.GetStringValue(Request.Form["threadDrows"]);
				sLrows = iBridUtil.GetStringValue(Request.Form["threadLrows"]);
				sList = iBridUtil.GetStringValue(Request.Form["threadList"]);
				sScrid = iBridUtil.GetStringValue(Request.Form["threadScrid"]);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListBbsThread.aspx?scrid={0}&list={1}&drows={2}&lrows={3}&searchKey={4}&searchadmintype={5}",sScrid,sList,sDrows,sLrows,sSearchKey,ViCommConst.BbsThreadSearchAdminType.ALL)));
			} else if (sSearchType.Equals(RES_SEARCH)) {
				sDrows = iBridUtil.GetStringValue(Request.Form["resDrows"]);
				sLrows = iBridUtil.GetStringValue(Request.Form["resLrows"]);
				sList = iBridUtil.GetStringValue(Request.Form["resList"]);
				sScrid = iBridUtil.GetStringValue(Request.Form["resScrid"]);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListBbsRes.aspx?scrid={0}&list={1}&drows={2}&lrows={3}&searchKey={4}",sScrid,sList,sDrows,sLrows,sSearchKey)));
			}
		}
	}
}
