<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TxInviteMail.aspx.cs" Inherits="ViComm_woman_TxInviteMail" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblTemplate" runat="server">ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstTemplate" Runat="server"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblTitle" runat="server">ﾀｲﾄﾙ</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblDoc" runat="server">本文</cc1:iBMobileLabel>
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		<cc1:iBMobileLabel ID="lblServicePoint" runat="server">招待分数</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstServicePoint" Runat="server"></mobile:SelectionList>
		$PGM_HTML02;
		<br />
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
