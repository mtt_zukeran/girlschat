/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーログイン(Twitterアカウント利用)
--	Progaram ID		: LoginTwitter
--
--  Creation Date	: 2014.01.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_LoginTwitter:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			sessionObj.snsType = string.Empty;
			sessionObj.snsId = string.Empty;
			sessionObj.twitterAccessToken = string.Empty;
			sessionObj.twitterAccessTokenSecret = string.Empty;
			string sRequestTokenUrl = string.Empty;
			string sConsumerKey = string.Empty;
			string sConsumerSecret = string.Empty;
			string sAuthrizeUrl = string.Empty;

			using (SnsOAuth oSnsOAuth = new SnsOAuth()) {
				DataSet oDataSet = oSnsOAuth.GetOneBySnsType(sessionWoman.site.siteCd,PwViCommConst.SnsType.TWITTER);

				if (oDataSet.Tables[0].Rows.Count == 0) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}

				sRequestTokenUrl = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REQUEST_TOKEN_URL"]);
				sConsumerKey = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CONSUMER_KEY"]);
				sConsumerSecret = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CONSUMER_SECRET"]);
				sAuthrizeUrl = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AUTHORIZE_URL"]);
			}

			Guid guidValue = Guid.NewGuid();
			string sNonce = guidValue.ToString("N");
			string sSignatureMethod = "HMAC-SHA1";
			string sTimestamp = DateTimeHelper.GetUnixTimeStamp(DateTime.Now).ToString();
			string sOAuthVersion = "1.0";

			SortedDictionary<string,string> oParamDictionary = new SortedDictionary<string,string>();
			oParamDictionary.Add("oauth_callback",string.Format("http://{0}{1}",sessionWoman.site.jobOfferSiteHostNm,sessionWoman.GetNavigateUrl("LoginUserByTwitterId.aspx")));
			oParamDictionary.Add("oauth_consumer_key",sConsumerKey);
			oParamDictionary.Add("oauth_nonce",sNonce);
			oParamDictionary.Add("oauth_signature_method",sSignatureMethod);
			oParamDictionary.Add("oauth_timestamp",sTimestamp);
			oParamDictionary.Add("oauth_version",sOAuthVersion);
			string sSignature = OAuthHelper.Twitter.GenerateSignature(oParamDictionary,sRequestTokenUrl,sConsumerSecret,string.Empty);

			oParamDictionary.Add("oauth_signature",sSignature);

			string sParams = OAuthHelper.Twitter.GenerateParameta(oParamDictionary);

			string sResponseStr = OAuthHelper.Twitter.PostRequest(sRequestTokenUrl,sParams);

			Dictionary<string,string> oTokenDictionary = OAuthHelper.Twitter.GetToken(sResponseStr);

			string sToken = oTokenDictionary["oauth_token"];

			RedirectToMobilePage(string.Format("{0}?oauth_token={1}",sAuthrizeUrl,sToken));
		}
	}
}
