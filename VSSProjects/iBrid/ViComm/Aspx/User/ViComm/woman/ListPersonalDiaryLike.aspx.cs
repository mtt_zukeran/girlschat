﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 個人別日記いいね押下男性一覧
--	Progaram ID		: ListPersonalDiaryLike
--
--  Creation Date	: 2012.06.15
--  Creater			: i-Brid
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListPersonalDiaryLike : MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sReportDay = iBridUtil.GetStringValue(Request.QueryString["reportday"]);
			string sCastDiarySubSeq = iBridUtil.GetStringValue(Request.QueryString["subseq"]);

			if (string.IsNullOrEmpty(sReportDay) || string.IsNullOrEmpty(sCastDiarySubSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_DIARY_LIKE,ActiveForm);
		}
	}

}
