/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り解除
--	Progaram ID		: ReleaseFavorit
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ReleaseFavorit:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			DataRow dr;
			if (sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),iRecNo,out dr)) {
				
				string sUserStatus = dr["USER_STATUS"].ToString();
				if (sUserStatus.Equals(ViCommConst.USER_MAN_HOLD) || sUserStatus.Equals(ViCommConst.USER_MAN_RESIGNED) || sUserStatus.Equals(ViCommConst.USER_MAN_STOP) || sUserStatus.Equals(ViCommConst.USER_MAN_BLACK)) {
					RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_MAN);
				}
				using (Favorit oFavorit = new Favorit()) {
					oFavorit.FavoritMainte(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						dr["USER_SEQ"].ToString(),
						dr["USER_CHAR_NO"].ToString(),
						string.Empty,
						ViCommConst.FLAG_ON,
						ViCommConst.FLAG_OFF);
				}
			}
		}
	}

}
