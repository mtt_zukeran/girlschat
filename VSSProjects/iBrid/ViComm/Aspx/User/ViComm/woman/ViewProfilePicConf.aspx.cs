/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ギャラリー写真確認詳細(リスト表示)
--	Progaram ID		: ViewProfilePicConf
--
--  Creation Date	: 2010.09.13
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewProfilePicConf:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
				SetDropDownList();
				txtTitle.Text = sessionWoman.GetPicValue("PIC_TITLE");
				txtDoc.Text = sessionWoman.GetPicValue("PIC_DOC");
				using (ManageCompany oManageCompany = new ManageCompany()) {
					bool bIsAvailableService = oManageCompany.IsAvailableService(ViCommConst.RELEASE_GALLERY_PIC_INPUT_TITLE_AND_DOC);
					txtTitle.Visible = bIsAvailableService;
					txtDoc.Visible = bIsAvailableService;
				}
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	private void SetDropDownList() {
		int iProfilePicFlag;
		string sPicType = string.Empty;
		string sCastPicAttrTypeSeq = string.Empty;
		string sCastPicAttrSeq = string.Empty;
		int iIdx = 0;

		int.TryParse(sessionWoman.GetPicValue("CUR_PIC_IS_PROFILE"),out iProfilePicFlag);
		sPicType = sessionWoman.GetPicValue("PIC_TYPE");
		sCastPicAttrTypeSeq = sessionWoman.GetPicValue("CAST_PIC_ATTR_TYPE_SEQ");
		sCastPicAttrSeq = sessionWoman.GetPicValue("CAST_PIC_ATTR_SEQ");

		using (CodeDtl oCodeDtl = new CodeDtl()) {
			DataSet ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_MODIFY_PROFILE_PIC_TYPE);
			foreach (DataRow row in ds.Tables[0].Rows) {
				if (!iBridUtil.GetStringValue(row["CODE_NM"]).Equals("削除")) {
					lstPicType.Items.Add(new MobileListItem(row["CODE_NM"].ToString(),row["CODE"].ToString()));
				}
			}
		}

		if (iProfilePicFlag == ViCommConst.ATTACHED_PROFILE) {
			foreach (MobileListItem item in lstPicType.Items) {
				if (item.Value == ViCommConst.ProfilePicType.PROFILE) {
					item.Selected = true;
				}
			}
		} else if (sPicType.Equals(ViCommConst.ATTACHED_HIDE.ToString())) {
			foreach (MobileListItem item in lstPicType.Items) {
				if (item.Value == ViCommConst.ProfilePicType.HIDE) {
					item.Selected = true;
				}
			}
		} else {
			foreach (MobileListItem item in lstPicType.Items) {
				if (item.Value == ViCommConst.ProfilePicType.GALLERY) {
					item.Selected = true;
				}
			}
		}

		//写真カテゴリ設定
		if (IsAvailableService(ViCommConst.RELEASE_CAST_PIC_GALLERY_ATTR)) {
			lstAttrSeq.Visible = true;
			if (ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString().Equals(sCastPicAttrTypeSeq) && ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString().Equals(sCastPicAttrSeq)) {
				lstAttrSeq.Items.Add(new MobileListItem("未分類",ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString() + ":" + ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString()));
			}
			using (CastPicAttrTypeValue oCastPicAttrTypeValue = new CastPicAttrTypeValue()) {
				DataSet ds = oCastPicAttrTypeValue.GetList(sessionWoman.site.siteCd);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstAttrSeq.Items.Add(new MobileListItem(dr["CAST_PIC_ATTR_TYPE_NM"].ToString() + ":" + dr["CAST_PIC_ATTR_NM"].ToString(),dr["CAST_PIC_ATTR_TYPE_SEQ"].ToString() + ":" + dr["CAST_PIC_ATTR_SEQ"].ToString()));
					if (dr["CAST_PIC_ATTR_TYPE_SEQ"].ToString().Equals(sCastPicAttrTypeSeq) && dr["CAST_PIC_ATTR_SEQ"].ToString().Equals(sCastPicAttrSeq)) {
						lstAttrSeq.SelectedIndex = iIdx;
					}
					iIdx++;
				}
			}
		} else {
			lstAttrSeq.Visible = false;
			ViewState["ATTR_TYPE_SEQ"] = sCastPicAttrTypeSeq;
			ViewState["ATTR_SEQ"] = sCastPicAttrSeq;
		}
	}

	protected void cmdDelete_Click(object sender,EventArgs e) {
		string picSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
		DeletePicture(picSeq);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_PIC) + "&site=" + sessionWoman.site.siteCd);
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		txtTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
		txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

		string picSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
		string sCastPicAttrTypeSeq = string.Empty;
		string sCastPicAttrSeq = string.Empty;
		int iClosedFlag;
		int iUnauthFlag;
		int iProfilePicFlag;
		int? iObjRankingFlag = null;
		string sClosedFlag = sessionWoman.GetPicValue("OBJ_NOT_PUBLISH_FLAG");

		int.TryParse(sessionWoman.GetPicValue("OBJ_NOT_PUBLISH_FLAG"),out iClosedFlag);
		int.TryParse(sessionWoman.GetPicValue("OBJ_NOT_APPROVE_FLAG"),out iUnauthFlag);
		int.TryParse(sessionWoman.GetPicValue("CUR_PIC_IS_PROFILE"),out iProfilePicFlag);

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]))) {
			iObjRankingFlag = int.Parse(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]));
		}

		if (lstPicType.Selection.Value == "4") {
			DeletePicture(picSeq);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_PIC) + "&site=" + sessionWoman.site.siteCd);
		}

		bool bOk = ValidText();
		if (bOk == false) {
			return;
		} else {
			string sRotateType = iBridUtil.GetStringValue(this.Request.Params["rotate_type"]);
			RotateFlipType oRoateType = ImageUtil.Convert2RotateFlipType(sRotateType);

			string sWebPhisicalDir = string.Empty;
			using (Site oSite = new Site()) {
				oSite.GetValue(sessionWoman.site.siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}
			string sCastPicDir = ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sessionWoman.site.siteCd,sessionWoman.userWoman.loginId);

			if (lstAttrSeq.Visible) {
				string[] sCastPicAttr = lstAttrSeq.Selection.Value.Split(':');
				sCastPicAttrTypeSeq = sCastPicAttr[0];
				sCastPicAttrSeq = sCastPicAttr[1];
			} else {
				sCastPicAttrTypeSeq = ViewState["ATTR_TYPE_SEQ"].ToString();
				sCastPicAttrSeq = ViewState["ATTR_SEQ"].ToString();
			}

			string sTitle = Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text);
			string sDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
			if (sTitle.Length > 30) {
				sTitle = SysPrograms.Substring(sTitle,30);
			}
			if (sDoc.Length > 1000) {
				sDoc = SysPrograms.Substring(sDoc,1000);
			}
			if (lstPicType.Selection.Value == ViCommConst.ATTACHED_PROFILE.ToString()) {
				//ﾌﾟﾛﾌ写真にする
				sessionWoman.userWoman.UpdateProfilePicture(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					picSeq,
					ViCommConst.FLAG_OFF,
					ViCommConst.FLAG_ON,
					ViCommConst.ATTACHED_PROFILE.ToString(),
					iUnauthFlag,
					iClosedFlag,
					sTitle,
					sDoc,
					sCastPicAttrTypeSeq,
					sCastPicAttrSeq,
					iObjRankingFlag
				);

				string sDecomailServerType = "";
				using (Sys oSys = new Sys()) {
					oSys.GetValue("DECOMAIL_SERVER_TYPE",out sDecomailServerType);
				}
				if (sDecomailServerType.Equals(ViCommConst.DECO_SRV_OLD)) {
					FtpPicture(picSeq);
				}

				ImageUtil.RotateFilp(sCastPicDir,picSeq,oRoateType);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_PROFILE_PIC_COMPLITE) + "&site=" + sessionWoman.site.siteCd);
			} else if (lstPicType.Selection.Value == "2") {
				//ｷﾞｬﾗﾘｰ画像にする
				sessionWoman.userWoman.UpdateProfilePicture(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					picSeq,
					ViCommConst.FLAG_OFF,
					ViCommConst.FLAG_OFF,
					ViCommConst.ATTACHED_PROFILE.ToString(),
					iUnauthFlag,
					iClosedFlag,
					sTitle,
					sDoc,
					sCastPicAttrTypeSeq,
					sCastPicAttrSeq,
					iObjRankingFlag
				);

				ImageUtil.RotateFilp(sCastPicDir,picSeq,oRoateType);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_GALLERY_PIC_COMPLITE) + "&site=" + sessionWoman.site.siteCd);

			} else if (lstPicType.Selection.Value.Equals("3")) {
				//一時的に非表示にする
				sessionWoman.userWoman.UpdateProfilePicture(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					picSeq,
					ViCommConst.FLAG_OFF,
					iProfilePicFlag,
					ViCommConst.ATTACHED_HIDE.ToString(),
					iUnauthFlag,
					iClosedFlag,
					sTitle,
					sDoc,
					sCastPicAttrTypeSeq,
					sCastPicAttrSeq,
					iObjRankingFlag
				);

				ImageUtil.RotateFilp(sCastPicDir,picSeq,oRoateType);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_PIC_CLOSED_COMPLITE) + "&site=" + sessionWoman.site.siteCd);
			}
		}
	}

	private void FtpPicture(string pFileSeq) {
		string sWebPhisicalDir = "";
		string sMailerIP = "";
		string sMailerFtpId = "";
		string sMailerFtpPw = "";
		string sFileNm = "";

		using (Site oSite = new Site()) {
			oSite.GetValue(sessionWoman.site.siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(sessionWoman.site.siteCd,"TX_HI_MAILER_IP",ref sMailerIP);
			oSite.GetValue(sessionWoman.site.siteCd,"MAILER_FTP_ID",ref sMailerFtpId);
			oSite.GetValue(sessionWoman.site.siteCd,"MAILER_FTP_PW",ref sMailerFtpPw);
		}
		using (CastPic oPic = new CastPic()) {
			sFileNm = oPic.GetPictureFileNm(pFileSeq);
		}

		string sFullPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
							sFileNm + ViCommConst.PIC_FOODER;

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			SysInterface.FTPUpload(sFullPath,sMailerIP,sessionWoman.site.siteCd + "_" + sessionWoman.userWoman.loginId + sessionWoman.userWoman.curCharNo + ".jpg",sMailerFtpId,sMailerFtpPw);
		}
	}


	private void DeletePicture(string pFileSeq) {
		string sFileNm = "";
		string sWebPhisicalDir = "";


		using (Site oSite = new Site()) {
			oSite.GetValue(sessionWoman.site.siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		using (CastPic oPic = new CastPic()) {
			sFileNm = oPic.GetPictureFileNm(pFileSeq);
		}

		string sFullPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
							sFileNm + ViCommConst.PIC_FOODER;

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}


			sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
								sFileNm + ViCommConst.PIC_FOODER_SMALL;

			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
		sessionWoman.userWoman.UpdateProfilePicture(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,pFileSeq,1,0,"0",0,0,"","","","",0);
	}


	virtual protected bool ValidText() {
		bool bIsAvailableService = false;
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		using (ManageCompany oManageCompany = new ManageCompany()) {
			bIsAvailableService = oManageCompany.IsAvailableService(ViCommConst.RELEASE_GALLERY_PIC_INPUT_TITLE_AND_DOC);
		}

		bool bOk = true;

		if (bIsAvailableService) {
			if (txtTitle.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
			}
			if (txtDoc.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
			}

			string sNGWord;
			if (bOk) {
				if (sessionWoman.ngWord == null) {
					sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
				}
				if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
					bOk = false;
					lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				}
			}
		}
		return bOk;
	}
}
