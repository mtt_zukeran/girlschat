/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性検索
--	Progaram ID		: FindGameCharacter
--
--  Creation Date	: 2011.10.4
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_FindGameCharacter:MobileSocialGameWomanBase {
	private const string ONLINE_STATUS = "FindUserOnlineStatus";
	private const string NEWMAN_FLG = "FindUserNewManFlg";
	private const string HANDLE_NAME = "FindUserHandleNm";
	private const string BIRTHDAY_FROM = "FindUserBirthdayFrom";
	private const string BIRTHDAY_TO = "FindUserBirthdayTo";
	private const string MAN_ITEM = "FindUserManItem";
	private const string AGE = "FindUserAge";
	private const string AREA = "FindArea";
	private const string RELATION = "FindGameCharacterRelation";
	private const string GAME_TYPE = "FindGameCharacterGameType";

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		int iIdx = 0;

		if (!IsPostBack) {

			GetQueryParm();

			// 地域
			using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
				DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionWoman.site.siteCd,"400");

				using (CodeDtl oCodeDtl = new CodeDtl()) {
					DataSet dsCodeDtl = oCodeDtl.GetList("81");
					foreach (DataRow drCodeDtl in dsCodeDtl.Tables[0].Rows) {
						string sExp = string.Format("GROUPING_CD={0}",drCodeDtl["CODE"]);

						string sValues = string.Empty;
						foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Select(sExp)) {
							if (!string.IsNullOrEmpty(sValues))
								sValues += ",";
							sValues += drAttrTypeValue["MAN_ATTR_SEQ"];
						}

						lstUserManItemEx1.Items.Add(new MobileListItem(drCodeDtl["CODE_NM"].ToString(),sValues));

					}
				}

				foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
					if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lstUserManItemEx1.Items.Add(new MobileListItem(drAttrTypeValue["MAN_ATTR_NM"].ToString(),drAttrTypeValue["MAN_ATTR_SEQ"].ToString()));
					}
				}
			}

			// 地域検索条件の保持
			int? iUserManItemEx1 = Session[AREA] as int?;
			if (iUserManItemEx1 != null) {
				this.lstUserManItemEx1.SelectedIndex = iUserManItemEx1.Value;
			}

			using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
				DataSet dsAttrType = oUserManAttrType.GetList(sessionWoman.site.siteCd);
				SelectionList lstUserManItem;
				iBMobileLabel lblUserManItem;
				iBMobileTextBox txtUserManItem;
				Dictionary<string,string> manAttrList = (Dictionary<string,string>)Session[MAN_ITEM];

				foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
					if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",iIdx + 1)) as iBMobileLabel;
						lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iIdx + 1)) as SelectionList;
						txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iIdx + 1)) as iBMobileTextBox;

						lblUserManItem.Text = drAttrType["MAN_ATTR_TYPE_FIND_NM"].ToString();

						if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
							lstUserManItem.Visible = false;

							// 入力値の復元を行います。
							if (manAttrList != null) {
								if (manAttrList.ContainsKey(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString())) {
									string val = manAttrList[drAttrType["MAN_ATTR_TYPE_SEQ"].ToString()];
									txtUserManItem.Text = val;
								}
							}
						} else {
							txtUserManItem.Visible = false;

							//lstUserManItem.Items.Add(new MobileListItem("指定なし","*"));
							if (string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {

								using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
									DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionWoman.site.siteCd,drAttrType["MAN_ATTR_TYPE_SEQ"].ToString());
									foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
										if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
											lstUserManItem.Items.Add(new MobileListItem(drAttrTypeValue["MAN_ATTR_NM"].ToString(),drAttrTypeValue["MAN_ATTR_SEQ"].ToString()));
										}
									}
								}
							} else {
								using (CodeDtl oCodeDtl = new CodeDtl()) {
									DataSet dsGroup = oCodeDtl.GetList(drAttrType["GROUPING_CATEGORY_CD"].ToString());
									foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
										lstUserManItem.Items.Add(new MobileListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
									}
								}
							}
							if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_RADIO)) {
								lstUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
								if (manAttrList == null) {
									lstUserManItem.SelectedIndex = 0;
								}
							}

							// 入力値の復元を行います。
							if (manAttrList != null) {
								if (manAttrList.ContainsKey(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString())) {
									string val = manAttrList[drAttrType["MAN_ATTR_TYPE_SEQ"].ToString()];
									foreach (MobileListItem item in lstUserManItem.Items) {
										if (item.Value == val) {
											item.Selected = true;
										}
									}
								}
							}
						}
						iIdx++;
					}
				}
			}
			for (int i = iIdx;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i + 1)) as Panel;
				pnlUserManItem.Visible = false;
			}

			int iOnlineFlag;
			int.TryParse(iBridUtil.GetStringValue(Session[ONLINE_STATUS]),out iOnlineFlag);
			if (iOnlineFlag == ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING) {
				// ﾛｸﾞｲﾝ中
				rdoStatus.Items[0].Selected = false;
				rdoStatus.Items[1].Selected = true;
			} else if (iOnlineFlag == ViCommConst.SeekOnlineStatus.WAITING) {
				// 待機中
				rdoStatus.Items[0].Selected = false;
				rdoStatus.Items[2].Selected = true;
			} else {
				// 全て
				rdoStatus.Items[0].Selected = true;
			}

			if (iBridUtil.GetStringValue(Session[NEWMAN_FLG]) == "1") {
				chkNewCast.Items[0].Selected = true;
			}

			txtHandelNm.Text = iBridUtil.GetStringValue(Session[HANDLE_NAME]);
			string sBirthdayFrom = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Session[BIRTHDAY_FROM]));
			string sBirthdayTo = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Session[BIRTHDAY_TO]));
			string sAge = iBridUtil.GetStringValue(Session[AGE]);
			string sRelation = iBridUtil.GetStringValue(Session[RELATION]);
			string sGameType = iBridUtil.GetStringValue(Session[GAME_TYPE]);

			SysPrograms.SetupFromToDay(null,lstBirthdayFromMM,lstBirthdayFromDD,null,lstBirthdayToMM,lstBirthdayToDD,true);
			if (!sBirthdayFrom.Equals(string.Empty)) {
				string[] sBirthday = sBirthdayFrom.Split('/');
				foreach (MobileListItem item in lstBirthdayFromMM.Items) {
					if (item.Value == sBirthday[0]) {
						item.Selected = true;
					}
				}
				foreach (MobileListItem item in lstBirthdayFromDD.Items) {
					if (item.Value == sBirthday[1]) {
						item.Selected = true;
					}
				}
			}
			if (!sBirthdayTo.Equals(string.Empty)) {
				string[] sBirthday = sBirthdayTo.Split('/');
				foreach (MobileListItem item in lstBirthdayToMM.Items) {
					if (item.Value == sBirthday[0]) {
						item.Selected = true;
					}
				}
				foreach (MobileListItem item in lstBirthdayToDD.Items) {
					if (item.Value == sBirthday[1]) {
						item.Selected = true;
					}
				}
			}
			if (!sAge.Equals(string.Empty)) {
				foreach (MobileListItem item in lstAge.Items) {
					if (item.Value == sAge) {
						item.Selected = true;
					}
				}
			}
			if (!sRelation.Equals(string.Empty)) {
				foreach (MobileListItem item in lstRelation.Items) {
					if (item.Value == sRelation) {
						item.Selected = true;
					}
				}
			}
			if (!sGameType.Equals(string.Empty)) {
				foreach (MobileListItem item in lstGameType.Items) {
					if (item.Value == sGameType) {
						item.Selected = true;
					}
				}
			}
			this.rdoStatus.Items[0].Text = DisplayWordUtil.Replace(this.rdoStatus.Items[0].Text);
		} else {

			Session[AREA] = lstUserManItemEx1.SelectedIndex;
		
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sHandleNm = HttpUtility.UrlEncode(HttpUtility.HtmlEncode(txtHandelNm.Text),enc);
		string sAgeFrom = HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[0]);
		string sAgeTo = HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[1]);
		string sBirthdayFrom = string.Empty;
		string sBirthdayTo = string.Empty;
		string sUrl = string.Empty;
		int i;
		string sRelation = string.Empty;
		string sStartLevel = string.Empty;
		string sEndLevel = string.Empty;
		string sGameType = string.Empty;

		int iOnlineFlag = 0;
		int iNewManFlag = 0;
		bool bOk = true;

		txtHandelNm.Text = HttpUtility.HtmlEncode(txtHandelNm.Text);

		sStartLevel = txtStartLevel.Text;
		txtStartLevel.Text = HttpUtility.HtmlEncode(txtStartLevel.Text);

		sEndLevel = txtEndLevel.Text;
		txtEndLevel.Text = HttpUtility.HtmlEncode(txtEndLevel.Text);

		if (!txtStartLevel.Text.Equals("")) {
			if (!SysPrograms.Expression(@"^\d+$",txtStartLevel.Text)) {
				lblErrorMessage.Text = string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_VALIDATE_NUMERIC_FIELD),"開始ﾚﾍﾞﾙ");
				bOk = false;
			}
		}

		if (!txtEndLevel.Text.Equals("")) {
			if (!SysPrograms.Expression(@"^\d+$",txtEndLevel.Text)) {
				lblErrorMessage.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_VALIDATE_NUMERIC_FIELD),"終了ﾚﾍﾞﾙ");
				bOk = false;
			}
		}

		if (bOk == false) {
			return;
		}

		if (rdoStatus.Items[1].Selected) {
			iOnlineFlag = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
		} else if (rdoStatus.Items[2].Selected) {
			iOnlineFlag = ViCommConst.SeekOnlineStatus.WAITING;
		}
		
		if (chkNewCast.SelectedIndex != -1) {
			iNewManFlag = 1;
		}
		
		if (!lstBirthdayFromMM.Selection.Value.Equals("")) {
			sBirthdayFrom = lstBirthdayFromMM.Selection.Value + "/";
			if (!lstBirthdayFromDD.Selection.Value.Equals("")) {
				sBirthdayFrom += lstBirthdayFromDD.Selection.Value;
			} else {
				sBirthdayFrom += "01";
			}
			sBirthdayFrom = HttpUtility.UrlEncode(sBirthdayFrom,enc);
		}
		if (!lstBirthdayToMM.Selection.Value.Equals("")) {
			sBirthdayTo = lstBirthdayToMM.Selection.Value + "/";
			if (!lstBirthdayToDD.Selection.Value.Equals("")) {
				sBirthdayTo += lstBirthdayToDD.Selection.Value;
			} else {
				sBirthdayTo += "31";
			}
			sBirthdayTo = HttpUtility.UrlEncode(sBirthdayTo,enc);
		}

		sRelation = lstRelation.Selection.Value;

		sGameType = lstGameType.Selection.Value;

		sUrl = string.Format(
						"ListFindGameCharacter.aspx?category={0}&online={1}&handle={2}&play={3}&site={4}&agefrom={5}&ageto={6}&birthfrom={7}&birthto={8}&newman={9}&relation={10}&start_level={11}&end_level={12}&game_type={13}",
						sessionWoman.currentCategoryIndex,
						iOnlineFlag,
						sHandleNm,
						"0",
						sessionWoman.site.siteCd,
						sAgeFrom,
						sAgeTo,
						sBirthdayFrom,
						sBirthdayTo,
						iNewManFlag,
						sRelation,
						sStartLevel,
						sEndLevel,
						sGameType
						);

		// 選択した値をセッションにいれておきます。 
		Session[ONLINE_STATUS] = iOnlineFlag;
		Session[NEWMAN_FLG] = iNewManFlag;
		Session[HANDLE_NAME] = txtHandelNm.Text;
		Session[BIRTHDAY_FROM] = sBirthdayFrom;
		Session[BIRTHDAY_TO] = sBirthdayTo;
		Session[AGE] = lstAge.Selection.Value;
		Session[RELATION] = lstRelation.Selection.Value;
		Session[GAME_TYPE] = lstGameType.Selection.Value;
		
		SelectionList lstUserManItem;
		iBMobileTextBox txtUserManItem;
		Panel pnlUserManItem;

		i = 1;
		string sGroupingFlag;
		Dictionary<string,string> manAttrList = new Dictionary<string,string>();

		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet dsAttrType = oUserManAttrType.GetList(sessionWoman.site.siteCd);
			foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
				if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
					pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i)) as Panel;

					if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i)) as iBMobileTextBox;
						if ((pnlUserManItem != null) && (txtUserManItem != null) && (pnlUserManItem.Visible)) {
							sUrl = sUrl + string.Format(
												"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}",
												i,
												HttpUtility.UrlEncode(txtUserManItem.Text,enc),
												drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),
												"0",
												"1");
							// 入力値復元用の値保存 
							manAttrList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),txtUserManItem.Text);

						}
					} else {
						lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i)) as SelectionList;
						sGroupingFlag = "";
						if (!string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
							sGroupingFlag = "1";
						}
						if ((pnlUserManItem != null) && (lstUserManItem != null) && (pnlUserManItem.Visible)) {
							sUrl = sUrl + string.Format(
												"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}",
												i,
												lstUserManItem.Items[lstUserManItem.SelectedIndex].Value,
												drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),
												sGroupingFlag,
												"0");
							// 入力値復元用の値保存 
							manAttrList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),lstUserManItem.Items[lstUserManItem.SelectedIndex].Value);

						}
					}
					i++;
				}

			}
		}
		Session[MAN_ITEM] = manAttrList;
		// 地域
		sUrl += string.Format("&item{0:D2}={1}&typeseq{0:D2}=400&group{0:D2}=&like{0:D2}=0",i,lstUserManItemEx1.Items[lstUserManItemEx1.SelectedIndex].Value);

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sUrl));
	}

	private void GetQueryParm() {
		txtStartLevel.Text = iBridUtil.GetStringValue(this.Request.QueryString["start_level"]);
		txtEndLevel.Text = iBridUtil.GetStringValue(this.Request.QueryString["end_level"]);
	}
}
