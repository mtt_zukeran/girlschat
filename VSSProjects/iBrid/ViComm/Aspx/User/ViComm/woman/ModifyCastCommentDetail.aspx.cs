/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 特殊コメント編集
--	Progaram ID		: ModifyCastCommentDetail
--
--  Creation Date	: 2013.05.16
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ModifyCastCommentDetail:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.errorMessage = string.Empty;

			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = false;

			txtCommentDetail.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].commentDetail;

			if (string.IsNullOrEmpty(txtCommentDetail.Text)) {
				txtCommentDetail.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].commentList;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (txtCommentDetail.Text.IndexOf("$") != -1) {
			txtCommentDetail.Text = txtCommentDetail.Text.Replace("$","");
			sessionWoman.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_INPUT_DOLLAR_MARK);
			return;
		}
		
		string sNGWord = string.Empty;
		sessionWoman.errorMessage = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (string.IsNullOrEmpty(txtCommentDetail.Text)) {
			sessionWoman.errorMessage = "特殊ｺﾒﾝﾄを入力して下さい";
			return;
		} else if (sessionWoman.ngWord.VaidateDoc(txtCommentDetail.Text,out sNGWord) == false) {
			sessionWoman.errorMessage = "特殊ｺﾒﾝﾄに禁止語句が含まれています";
			return;
		}

		ViewState["COMMENT_DETAIL"] = txtCommentDetail.Text;
		lblCommentDetail.Text = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,txtCommentDetail.Text)).Replace(System.Environment.NewLine,"<br>");

		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
		pnlComplete.Visible = false;
	}

	protected void cmdTx_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		string sResult = string.Empty;
		string sCommentDetail = iBridUtil.GetStringValue(ViewState["COMMENT_DETAIL"]);

		if (sCommentDetail.Length > 1000) {
			sCommentDetail = SysPrograms.Substring(sCommentDetail,1000);
		}

		sessionWoman.userWoman.ModifyCastCommentDetail(
			sessionWoman.site.siteCd,
			sessionWoman.userWoman.userSeq,
			sessionWoman.userWoman.curCharNo,
			HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sCommentDetail)),
			out sResult
		);

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			pnlWrite.Visible = false;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
		pnlComplete.Visible = false;
	}
}
