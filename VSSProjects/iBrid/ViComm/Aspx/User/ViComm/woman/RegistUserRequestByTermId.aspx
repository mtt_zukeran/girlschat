<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistUserRequestByTermId.aspx.cs" Inherits="ViComm_woman_RegistUserRequestByTermId" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		 $PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="DivS01" runat="server" Text="<font size='1'>" />
		<cc1:iBMobileLabel ID="lblTel" runat="server" BreakAfter="true">電話番号</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true" BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagTel" runat="server" Text="<br/>" />
		<cc1:iBMobileLiteralText ID="DivE01" runat="server" Text="</font>" />
		<cc1:iBMobileLiteralText ID="DivS02" runat="server" Text="<font size='1'>" />
		<cc1:iBMobileLabel ID="lblCastNm" runat="server" BreakAfter="true">氏名(漢字)</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtCastNm" runat="server" MaxLength="12" Size="18" BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagNm" runat="server" Text="<br/>" />
		<cc1:iBMobileLiteralText ID="DivE02" runat="server" Text="</font>" />
		<cc1:iBMobileLiteralText ID="DivS04" runat="server" Text="<font size='1'>" />
		<cc1:iBMobileLabel ID="lblCastKanaNm" runat="server" BreakAfter="true">氏名(ﾌﾘｶﾞﾅ)</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtCastKanaNm" runat="server" MaxLength="15" Size="18" BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagKanaNm" runat="server" Text="<br/>" />
		<cc1:iBMobileLiteralText ID="DivE04" runat="server" Text="</font>" />
		<cc1:iBMobileLiteralText ID="DivS03" runat="server" Text="<font size='1'>" />
		<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">生年月日</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">年</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">月</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblDay" runat="server">日</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagBirthday" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblQuestionDoc" runat="server">ご質問事項</cc1:iBMobileLabel>
		<tx:TextArea ID="txtQuestionDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" MaxLength="300">
		</tx:TextArea>
		<cc1:iBMobileLiteralText ID="DivE03" runat="server" Text="</font>" />
		<mobile:Panel ID="pnlAgreement" Runat="server">
			<mobile:SelectionList ID="chkAgreement" Runat="server" SelectType="CheckBox">
				<Item Text="$PGM_HTML03;">
				</Item>
			</mobile:SelectionList>
			$PGM_HTML04;
		</mobile:Panel>
		$PGM_HTML02;
		$PGM_HTML09;
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="12" Numeric="true" Visible="false"></cc1:iBMobileTextBox>
	</mobile:Form>
</body>
</html>
