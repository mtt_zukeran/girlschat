/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ꊇ�����M���[���Ǘ�
--	Progaram ID		: ListManageBatchMail
--
--  Creation Date	: 2011.01.21
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListManageBatchMail:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_TX_BATCH_MAIL,ActiveForm);
			if (sessionWoman.totalRowCount == 0) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
														   "FindBatchMailUser.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();
		List<string> mailCount = new List<string>();
		string[] sValue;

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				sValue = Request.Form[key].Split(',');
				mailSeqList.Add(sValue[0]);
				mailCount.Add(sValue[1]);
			}
		}

		if (mailSeqList.Count > 0) {
			int iCancelCount = 0;
			using (BatchMail oBatchMail = new BatchMail()) {
				iCancelCount = oBatchMail.CancelBatchMail(mailSeqList.ToArray(),mailCount.ToArray());
			}
			sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
			if (iCancelCount > 0) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
																   "DisplayDoc.aspx?doc=" + ViCommConst.SCR_CANCEL_BATCH_MAIL_COMPLITE + "&mail_delete_count=" + mailSeqList.Count));
			}
		}
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
														   "ListManageBatchMail.aspx?" + Request.QueryString));
	}
}
