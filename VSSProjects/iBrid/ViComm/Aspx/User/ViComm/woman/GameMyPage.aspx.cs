/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсEϲ�߰��
--	Progaram ID		: GameMyPage
--
--  Creation Date	: 2011.08.02
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_GameMyPage:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);

		if (sessionWoman.logined == false) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermIdGame.aspx"));
			return;
		}

		if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE) || (sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionWoman.adminFlg)) {
			HttpCookie cookie = new HttpCookie("maqia");
			cookie.Values["maqiauid"] = sessionWoman.userWoman.userSeq;
			cookie.Values["maqiapw"] = sessionWoman.userWoman.loginPassword;
			cookie.Values["maqiasex"] = ViCommConst.OPERATOR;
			cookie.Expires = DateTime.Now.AddDays(90);
			cookie.Domain = sessionWoman.site.hostNm;
			Response.Cookies.Add(cookie);
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.userWoman.CurCharacter.gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);
	}
}