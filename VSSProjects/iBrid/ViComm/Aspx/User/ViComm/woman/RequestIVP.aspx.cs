/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: IVP申請
--	Progaram ID		: RequestIVP
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_RequestIVP:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		

		if (!IsPostBack) {
			int iRecNo;
			int.TryParse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]),out iRecNo);
			if (iRecNo == 0) {
				iRecNo = 1;
			}
			string sManUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);

			if (sessionWoman.SetManDataSetByUserSeq(sManUserSeq, iRecNo)) {
				using (Refuse oRefuse = new Refuse()) {
					DataRow oDataRow = sessionWoman.parseContainer.parseUser.dataTable[ViCommConst.DATASET_MAN].Rows[0];
					
					// 拒否登録している
					if (oRefuse.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]))) {
						this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_MAN);
					}

					// 拒否登録されている
					this.CheckRefuse(iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]));
				}
			}
			sessionWoman.ContorIVP(this,Request);

			string sPageKey = "";
			string sChargeType = iBridUtil.GetStringValue(Request.QueryString["chgtype"]);
			switch (sChargeType) {
				case ViCommConst.CHARGE_CAST_TALK_WSHOT:
					sPageKey = "RequestWShotTalk.aspx";
					break;
				case ViCommConst.CHARGE_CAST_TALK_PUBLIC:
					sPageKey = "RequestPublicTalk.aspx";
					break;
				case ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT:
				case ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC:
					sPageKey = "RequestVoiceTalk.aspx";
					break;
			}
			Response.Filter = sessionWoman.InitScreen(Response.Filter,frmMain,Request,sPageKey,ViewState);
		}
	}
}
