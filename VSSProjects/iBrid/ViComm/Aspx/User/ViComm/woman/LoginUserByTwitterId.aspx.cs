/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーログイン・登録振り分け(Twitterアカウント利用)
--	Progaram ID		: LoginUserByTwitterId
--
--  Creation Date	: 2014.01.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_LoginUserByTwitterId:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["denied"]))) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("LoginUser.aspx"));
			}

			string sAccessTokenUrl = string.Empty;
			string sConsumerKey = string.Empty;
			string sConsumerSecret = string.Empty;

			using (SnsOAuth oSnsOAuth = new SnsOAuth()) {
				DataSet oDataSet = oSnsOAuth.GetOneBySnsType(sessionWoman.site.siteCd,PwViCommConst.SnsType.TWITTER);

				if (oDataSet.Tables[0].Rows.Count == 0) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}

				sAccessTokenUrl = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ACCESS_TOKEN_URL"]);
				sConsumerKey = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CONSUMER_KEY"]);
				sConsumerSecret = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CONSUMER_SECRET"]);
			}

			string sOAuthToken = iBridUtil.GetStringValue(Request.QueryString["oauth_token"]);
			string sOAuthVerifier = iBridUtil.GetStringValue(Request.QueryString["oauth_verifier"]);

			Guid guidValue = Guid.NewGuid();
			string sNonce = guidValue.ToString("N");
			string sSignatureMethod = "HMAC-SHA1";
			string sTimestamp = DateTimeHelper.GetUnixTimeStamp(DateTime.Now).ToString();
			string sOAuthVersion = "1.0";

			SortedDictionary<string,string> oParamDictionary = new SortedDictionary<string,string>();
			oParamDictionary.Add("oauth_consumer_key",sConsumerKey);
			oParamDictionary.Add("oauth_nonce",sNonce);
			oParamDictionary.Add("oauth_signature_method",sSignatureMethod);
			oParamDictionary.Add("oauth_timestamp",sTimestamp);
			oParamDictionary.Add("oauth_token",sOAuthToken);
			oParamDictionary.Add("oauth_verifier",sOAuthVerifier);
			oParamDictionary.Add("oauth_version",sOAuthVersion);
			string sSignature = OAuthHelper.Twitter.GenerateSignature(oParamDictionary,sAccessTokenUrl,sConsumerSecret,string.Empty);

			oParamDictionary.Add("oauth_signature",sSignature);

			string sParams = OAuthHelper.Twitter.GenerateParameta(oParamDictionary);

			string sResponseStr = OAuthHelper.Twitter.PostRequest(sAccessTokenUrl,sParams);

			Dictionary<string,string> oTokenDictionary = OAuthHelper.Twitter.GetToken(sResponseStr);

			string sAccessToken = oTokenDictionary["oauth_token"];
			string sAccessTokenSecret = oTokenDictionary["oauth_token_secret"];
			string sTwitterId = oTokenDictionary["user_id"];

			bool bExistFlag = false;
			string sSexCd = string.Empty;

			using (User oUser = new User()) {
				DataSet oDataSet;
				oDataSet = oUser.GetOneByTwitterId(sTwitterId);

				if (oDataSet.Tables[0].Rows.Count > 0) {
					bExistFlag = true;

					sSexCd = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["SEX_CD"]);
				}
			}

			sessionObj.twitterAccessToken = sAccessToken;
			sessionObj.twitterAccessTokenSecret = sAccessTokenSecret;
			sessionObj.snsType = PwViCommConst.SnsType.TWITTER;
			sessionObj.snsId = sTwitterId;

			if (!bExistFlag) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("RegistUserRequestByTermIdIdol.aspx"));
			} else if (!sSexCd.Equals(ViCommConst.MAN)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("LoginUser.aspx"));
			} else {
				RedirectToMobilePage(string.Format("http://{0}{1}{2}/(S({3}))/LoginUser.aspx",sessionWoman.site.hostNm,sessionWoman.root,sessionWoman.site.siteType,sessionWoman.sessionId));
			}
		}
	}
}