/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�f�[�g����
--	Progaram ID		: ListGameDateLog
--
--  Creation Date	: 2012.10.08
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameDateLog:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			using (GameCastNews oGameCastNews = new GameCastNews()) {
				oGameCastNews.DeleteGameCastNews(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					PwViCommConst.GameCastNewsType.DATE_CHARGE
				);
			}

			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_DATE_LOG,ActiveForm);
		}
	}
}