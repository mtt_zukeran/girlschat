<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistLoginPlan.aspx.cs" Inherits="ViComm_woman_RegistLoginPlan" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlRegistForm" runat="server">
			$PGM_HTML02;
			<mobile:SelectionList ID="lstDate" Runat="server" BreakAfter="false"></mobile:SelectionList>
			$PGM_HTML05;
			<mobile:SelectionList ID="lstTime" Runat="server" BreakAfter="false"></mobile:SelectionList>
			$PGM_HTML03;
		</mobile:Panel>
		<cc1:iBMobileLiteralText ID="lblDeleteForm" runat="server" Text="$PGM_HTML04;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
