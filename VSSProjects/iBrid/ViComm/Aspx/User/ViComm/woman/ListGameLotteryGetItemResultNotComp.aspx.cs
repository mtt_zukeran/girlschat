/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����ؽČ���(�����)
--	Progaram ID		: ListGameLotteryGetItemResultNotComp
--
--  Creation Date	: 2012.06.16
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public partial class ViComm_woman_ListGameLotteryGetItemResultNotComp:MobileSocialGameWomanBase {
	private NameValueCollection query = new NameValueCollection();

	protected void Page_Load(object sender,EventArgs e) {
		if (iBridUtil.GetStringValue(this.Request.QueryString["result"]).Equals(PwViCommConst.GameLotteryResult.RESULT_OK)) {
			Regex rgx = new Regex("^[0-9]+$");
			Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["set_lottery_item_get_log_seq"]));

			Regex rgx2 = new Regex("^[0-9]+$");
			Match rgxMatch2 = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"]));

			if (!rgxMatch.Success || !rgxMatch2.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT_NOT_COMP,ActiveForm);
		}
	}
}
