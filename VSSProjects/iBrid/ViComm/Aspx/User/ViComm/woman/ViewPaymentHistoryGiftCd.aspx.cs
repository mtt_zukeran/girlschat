﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ギフト券交換履歴詳細
--	Progaram ID		: ViewPaymentHistoryGiftCd
--
--  Creation Date	: 2017.04.12
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewPaymentHistoryGiftCd:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_EXTENSION_PAYMENT_HISTORY_GIFT_DTL,ActiveForm);
		}
	}
}
