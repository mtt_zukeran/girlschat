﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 美人時計詳細
--	Progaram ID		: ViewBeautyClock
--
--  Creation Date	: 2011.03.28
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewBeautyClock:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		ulong lSeekMode = ulong.Parse(this.Request.QueryString["seekmode"]);
		if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			lSeekMode = ViCommConst.INQUIRY_EXTENSION_BEAUTY_CLOCK;
		}
		
		if (!IsPostBack) {
			bool bControlList = sessionWoman.ControlList(this.Request,lSeekMode,this.ActiveForm);
			
			if (!bControlList) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListBeautyClock.aspx"));
			}
		}
	}
}
