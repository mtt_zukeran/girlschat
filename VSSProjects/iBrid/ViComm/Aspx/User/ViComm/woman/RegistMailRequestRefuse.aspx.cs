/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���[�����˂��苑�ۓo�^
--	Progaram ID		: RegistMailRequestRefuse
--
--  Creation Date	: 2015.04.01
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistMailRequestRefuse:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			string sNowFlag = iBridUtil.GetStringValue(Request.QueryString["now"]);
			string sPrevPath = iBridUtil.GetStringValue(Request.QueryString["prev_path"]);
			
			string sResult;
			string sSiteCd = sessionWoman.site.siteCd;
			string sUserSeq = sessionWoman.userWoman.userSeq;
			string sUserCharNo = sessionWoman.userWoman.curCharNo;

			int iRegistFlag = sNowFlag.Equals(ViCommConst.FLAG_OFF_STR) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;

			sessionWoman.userWoman.RegistMailRequestRefuse(sSiteCd,sUserSeq,sUserCharNo,iRegistFlag,out sResult);
			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				sessionWoman.userWoman.CurCharacter.characterEx.mailRequestRefuseFlag = iRegistFlag;

				string sRedirectPath = !string.IsNullOrEmpty(sPrevPath) ? sPrevPath : "ListMailRequestLog.aspx";

				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectPath));
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}
}
