/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹް� ���ًL�^�ڍ�
--	Progaram ID		: ViewGameBattleLog
--
--  Creation Date	: 2011.11.16
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_woman_ViewGameBattleLog:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(this.Request.QueryString["battle_log_seq"]);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		this.UpdateSupportRequestCheck();
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_LOG,ActiveForm);
	}

	private void UpdateSupportRequestCheck() {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sBattleLogSeq = iBridUtil.GetStringValue(Request.QueryString["battle_log_seq"]);
		string sResult = string.Empty;

		using (SupportRequest oSupportRequest = new SupportRequest()) {
			oSupportRequest.UpdateSupportRequestCheck(sSiteCd,sUserSeq,sUserCharNo,sBattleLogSeq,out sResult);
		}
	}
}
