/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �Q�[���o�^�t�H�[��(�ő̎��ʒʒm�o�^��)
--	Progaram ID		: RegistAddOnInfoByGame
--
--  Creation Date	: 2011.07.21
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Net;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistAddOnInfoByGame:MobileSocialGameWomanBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			ViewState["UTN"] = iBridUtil.GetStringValue(Request.QueryString["utn"]);
			ViewState["GUID"] = iBridUtil.GetStringValue(Request.QueryString["id"]);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = string.Empty;

		string sLoginId = string.Empty;
		string sLoginPassword = string.Empty;

		if (sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS) ||
			sessionWoman.carrier.Equals(ViCommConst.IPHONE) ||
			sessionWoman.carrier.Equals(ViCommConst.ANDROID)) {

			using (TempRegist oTempRegist = new TempRegist()) {
				oTempRegist.GetLoginIdPass(sessionWoman.userWoman.tempRegistId,out sLoginId,out sLoginPassword);
			}
		} else {
			if (sessionWoman.getTermIdFlag) {
				sessionWoman.userWoman.UsedTermId(ViewState["UTN"].ToString(),string.Empty,out sLoginId,out sLoginPassword);
			} else {
				sessionWoman.userWoman.UsedTermId(string.Empty,ViewState["GUID"].ToString(),out sLoginId,out sLoginPassword);
			}
		}

		bool loginOk = sessionWoman.userWoman.GetOne(sLoginId,sLoginPassword);

		if (loginOk) {
			sessionWoman.userWoman.SetLastActionDate(sessionWoman.userWoman.userSeq);
			sessionWoman.logined = true;
			string sCharNo;
			sCharNo = iBridUtil.GetStringValue(Request.QueryString["charno"]);

			if (sCharNo.Equals("")) {
				sCharNo = ViCommConst.MAIN_CHAR_NO;
			}
			
			string sListCharacterIndex = string.Format("{0}{1}",sessionWoman.site.siteCd,sCharNo);

			sessionWoman.userWoman.curCharNo = sCharNo;
			sessionWoman.userWoman.curKey = sListCharacterIndex;
			sessionWoman.userWoman.characterList[sListCharacterIndex].gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);
			
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"GameDisplayDoc.aspx?doc=" + PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_SELECT + "&site=" + sessionWoman.site.siteCd + "&charno=" + ViCommConst.MAIN_CHAR_NO));
		} else {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_LOGIN);
		}
	}
}
