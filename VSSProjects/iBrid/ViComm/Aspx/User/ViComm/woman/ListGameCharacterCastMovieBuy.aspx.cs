/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・動画を閲覧した男性
--	Progaram ID		: ListGameCharacterCastMovieBuy
--
--  Creation Date	: 2011.10.25
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameCharacterCastMovieBuy:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sObjSeq = iBridUtil.GetStringValue(this.Request.Params["obj_seq"]);

		if (sObjSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CAST_MOVIE_BUY,ActiveForm);
	}
}
