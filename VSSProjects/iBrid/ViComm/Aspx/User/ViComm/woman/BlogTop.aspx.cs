/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ��۸�į��
--	Progaram ID		: BlogTop
--
--  Creation Date	: 2011.04.05
--  Creater			: K.Itoh
--
**************************************************************************/
#region using
using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;


using iBridCommLib;
using MobileLib;
using ViComm;
#endregion

public partial class ViComm_woman_BlogTop : MobileBlogWomanBase {
	protected override bool NeedBlogRegistPermission { get { return true; } }
	
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
	}
}
