/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���[���Y�t�摜�Ǘ�
--	Progaram ID		: ViewMailPicConf
--
--  Creation Date	: 2009.08.12
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewMailPicConf:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_CAST_MAIL_PIC_STOCK,ActiveForm)) {
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListMailPicConf.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sFileSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
		DeletePicture(sFileSeq);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListMailPicConf.aspx") + "?site=" + sessionWoman.site.siteCd);
	}

	private void UpdateProfilePicture(string pPicSeq,int pDelFlag,int pProfilePicFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sessionWoman.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.userSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sessionWoman.userWoman.curCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	private void DeletePicture(string pFileSeq) {
		UpdateProfilePicture(pFileSeq,1,0);
	}
}