/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 画像レビュー一覧
--	Progaram ID		: ListObjReviewHistoryPic
--
--  Creation Date	: 2013.09.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListObjReviewHistoryPic:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY_PIC,this.ActiveForm);
		}
	}
}
