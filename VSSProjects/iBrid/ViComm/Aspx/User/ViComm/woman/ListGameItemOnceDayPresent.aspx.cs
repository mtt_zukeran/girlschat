/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE������ھ��� ���шꗗ
--	Progaram ID		: ListGameItemOnceDayPresent
--
--  Creation Date	: 2012.03.30
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameItemOnceDayPresent:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_GAME_ITEM_ONCE_DAY_PRESENT,this.ActiveForm);
		}
	}
}
