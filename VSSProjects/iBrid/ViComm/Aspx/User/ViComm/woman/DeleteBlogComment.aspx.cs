/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ動画 一覧
--	Progaram ID		: DeleteBlogComment
--
--  Creation Date	: 2011.04.05
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteBlogComment:MobileBlogWomanBase {

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		bool bHasData = sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_BLOG_COMMENT,ActiveForm);
		
		if (bHasData) {
			if (!IsPostBack) {
			} else {
				string sBlogArticleSeq = iBridUtil.GetStringValue(this.Request.QueryString["blog_article_seq"]);
				string sBlogCommentSeq = sessionWoman.GetBlogCommentValue("BLOG_COMMENT_SEQ");
				
				if (IsPostAction(PwViCommConst.BUTTON_DELETE)) {
					string sSiteCd = sessionWoman.site.siteCd;
					string sUserSeq = sessionWoman.userWoman.userSeq;
					string sUserCharNo = sessionWoman.userWoman.curCharNo;
					using (BlogComment oBlogComment = new BlogComment()) {
						oBlogComment.DeleteBlogComment(sSiteCd,sUserSeq,sUserCharNo,sBlogCommentSeq);
					}

					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DeleteBlogComment.aspx?delete=1&blog_article_seq={0}&blog_comment_seq={1}",sBlogArticleSeq,sBlogCommentSeq)));
				} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListBlogComment.aspx?blog_article_seq={0}",sBlogArticleSeq)));
				}
			}
		}
	}
}
