/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・持ち物贈り物一覧
--	Progaram ID		: ListGameItemPossessionPresent
--
--  Creation Date	: 2011.09.26
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameItemPossessionPresent:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		if (!sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
			oParameters.Add("action",PwViCommConst.GameWomanLiveChatNoApplyAction.PRESENT);
			RedirectToGameDisplayDoc(ViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY,oParameters);
		}

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_PRESENT,ActiveForm);
	}
}
