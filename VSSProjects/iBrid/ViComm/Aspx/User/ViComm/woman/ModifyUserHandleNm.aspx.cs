/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ハンドルネーム変更
--	Progaram ID		: ModifyUserHandleNm
--
--  Creation Date	: 2010.11.08
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ModifyUserHandleNm:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].bulkImpNonUseFlag == ViCommConst.FLAG_OFF) {
				if (!iBridUtil.GetStringValue(Request.QueryString["handlenm"]).Equals(string.Empty)) {
					txtHandelNm.Text = iBridUtil.GetStringValue(Request.QueryString["handlenm"]);
				} else {
					txtHandelNm.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNm;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (CheckInputValue() == false) {
			return;
		} else {
			if (!iBridUtil.GetStringValue(Request.QueryString["sendmail"]).Equals("1")) {
				string sResult;

				sessionWoman.userWoman.ModifyUserHandleNm(
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtHandelNm.Text)),
					out sResult
				);

				if (sResult.Equals("0")) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_WOMAN_PROFILE_COMPLITE));
				} else if (sResult.Equals(ViCommConst.REG_USER_RST_HANDLE_NM_EXIST.ToString())) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
				} else if (sResult.Equals(ViCommConst.REG_USER_RST_UNT_EXIST.ToString())) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
				} else if (sResult.Equals(ViCommConst.REG_USER_RST_SUB_SCR_NO_IS_NULL.ToString())) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
				} else {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
				}
			} else {
				bool iDupliFlag = false;
				if (IsAvailableService(ViCommConst.RELEASE_CHECK_HANDLE_NM_DUPLI)) {
					using (UserWoman oUserWoman = new UserWoman()) {
						iDupliFlag = oUserWoman.CheckCastHandleNmDupli(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,Mobile.EmojiToCommTag(sessionWoman.carrier,txtHandelNm.Text));
					}
				}
				if (iDupliFlag == false) {
					System.Text.Encoding enc = System.Text.Encoding.GetEncoding("Shift_JIS");
					string send = HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["send"]),enc);
					string title = HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["title"]),enc);
					string success = HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["success"]),enc);
					string handleNm = HttpUtility.UrlEncode(txtHandelNm.Text,enc);
					string url = string.Format("MailFormConfirm.aspx?callfrom=ModifyUserHandleNm.aspx&sendmail=1&send={0}&title={1}&success={2}&handlenm={3}",send,title,success,handleNm);
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,url));
				} else {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
					return;
				}
			}
		}
	}

	virtual protected bool CheckInputValue() {
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			}
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		return bOk;
	}
}
