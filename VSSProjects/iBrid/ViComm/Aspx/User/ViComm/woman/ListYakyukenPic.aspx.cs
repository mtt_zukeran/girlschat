/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �싅���摜�ꗗ
--	Progaram ID		: ListYakyukenPic
--
--  Creation Date	: 2013.04.30
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListYakyukenPic:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["ygameseq"]))) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_YAKYUKEN_PIC,this.ActiveForm);
		}
	}
}
