<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewBbsPicConf.aspx.cs" Inherits="ViComm_woman_ViewBbsPicConf" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML04;
		$PGM_HTML05;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24"></cc1:iBMobileTextBox>
		$PGM_HTML06;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24">
		</tx:TextArea>
		$PGM_HTML07;
		<mobile:SelectionList ID="lstRankingFlag" Runat="server" SelectType="Radio" Alignment="Left">
		   <Item Text="参加する" Value="1" />
		   <Item Text="参加しない" Value="0" />
		</mobile:SelectionList>
		<mobile:SelectionList ID="lstAttrSeq" Runat="server" BreakAfter="true"> </mobile:SelectionList>
		$PGM_HTML08;
		<mobile:SelectionList ID="lstSetUp" Runat="server" BreakAfter="true"> </mobile:SelectionList>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
