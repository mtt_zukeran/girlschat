/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 電話番号変更
--	Progaram ID		: ModifyUserTel
--
--  Creation Date	: 2010.10.28
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ModifyUserTel:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			txtTel.Text = string.Empty;
		}
		else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (txtTel.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
		}
		else {
			if (!sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
				}
				else {
					using (UserWoman oWoman = new UserWoman()) {
						if (oWoman.IsTelephoneRegistered(txtTel.Text)) {
							//携帯電話番号は既に登録されています。
							lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
							bOk = false;
						}
					}
				}
			}
		}

		if (bOk == false) {
			return;
		}
		else {
			string sResult;

			sessionWoman.userWoman.ModifyUserTel(
				sessionWoman.userWoman.userSeq,
				txtTel.Text,
                ViCommConst.CAST_SITE_CD,
				out sResult
			);

			if (sResult.Equals("0")) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_MODIFY_TEL_COMPLETE));
			}
			else {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
			}
		}
	}

}
