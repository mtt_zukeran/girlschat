/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE��ھ��Ċm�F
--	Progaram ID		: ViewGameCharacterPresent
--
--  Creation Date	: 2011.10.5
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_woman_ViewGameCharacterPresent:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		string sItemSeq = iBridUtil.GetStringValue(Request.QueryString["item_seq"]);

		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		if (!sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
			oParameters.Add("action",PwViCommConst.GameWomanLiveChatNoApplyAction.PRESENT);
			RedirectToGameDisplayDoc(ViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY,oParameters);
		}

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty) || sItemSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
		this.CheckGameRefusePartner(sPartnerUserSeq,sPartnerUserCharNo);
		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		} else {
			if (this.CheckOnlineStatusGame(sPartnerUserSeq,sPartnerUserCharNo)) {
				if (Request.Params[ViCommConst.BUTTON_DEFAULT] != null) {
					cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sItemSeq,ViCommConst.FLAG_OFF_STR);
				} else if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sItemSeq,ViCommConst.FLAG_ON_STR);
				}
			} else {
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo,string pItemSeq,string purchase_flg) {
		string sResult = string.Empty;
		string sUrl = string.Empty;
		
		string sPreCooperationPoint = sessionWoman.userWoman.CurCharacter.gameCharacter.cooperationPoint.ToString();
		string sPrePoint = sessionWoman.userWoman.CurCharacter.gameCharacter.castGamePoint.ToString();

		if (purchase_flg.Equals(ViCommConst.FLAG_ON_STR)) {
			BuyGameItem(pItemSeq);
		}

		sResult = this.PresentGameItem(pPartnerUserSeq,pPartnerUserCharNo,pItemSeq);

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			this.CheckQuestClear(this.sessionWoman.userWoman.userSeq,this.sessionWoman.userWoman.curCharNo,ViCommConst.OPERATOR);
			this.CheckQuestClear(pPartnerUserSeq,pPartnerUserCharNo,ViCommConst.MAN);
		}

		if (sResult == PwViCommConst.PresentGameItemResult.RESULT_OK) {
			sUrl = string.Format(
				"ViewGameCharacterPresentResult.aspx?partner_user_seq={0}&partner_user_char_no={1}&item_seq={2}&purchase_flg={3}&pre_cooperation_point={4}&pre_point={5}",
				pPartnerUserSeq,
				pPartnerUserCharNo,
				pItemSeq,
				purchase_flg,
				sPreCooperationPoint,
				sPrePoint
			);

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sUrl));
		} else if (sResult == PwViCommConst.PresentGameItemResult.RESULT_NG_ITEM) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_PRESENT_GAME_ITEM_INJUSTICE);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}

	private void BuyGameItem(string pItem_seq) {
		string sSiteCode = sessionWoman.userWoman.CurCharacter.siteCd;
		string sUserSeq = sessionWoman.userWoman.userSeq;
		string sUserCharNo = sessionWoman.userWoman.CurCharacter.userCharNo;
		string sBuyNum = ViCommConst.FLAG_ON_STR;
		string result = this.BuyGameItemExeCute(sSiteCode,sUserSeq,sUserCharNo,pItem_seq,sBuyNum);

		if (!result.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		sessionWoman.userWoman.CurCharacter.gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);
	}

	private string BuyGameItemExeCute(string sSiteCode,string sUserSeq,string sUserCharNo,string sItemSeq,string sBuyNum) {
		string sResult = "";

		GameItem oGameItem = new GameItem();
		oGameItem.BuyGameItem(sSiteCode,sUserSeq,sUserCharNo,sItemSeq,sBuyNum,out sResult);

		return sResult;
	}

	private string PresentGameItem(string pPartnerUserSeq,string pPartnerUserCharNo,string pItemSeq) {
		string sResult = string.Empty;
		
		using (GameItem oGameItem = new GameItem()) {
			sResult = oGameItem.PresentGameItem(
				sessionWoman.userWoman.CurCharacter.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.CurCharacter.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				ViCommConst.OPERATOR,
				pItemSeq
			);
		}
		
		return sResult;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo,string pSexCd) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionWoman.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				pSexCd
			);
		}
	}
}