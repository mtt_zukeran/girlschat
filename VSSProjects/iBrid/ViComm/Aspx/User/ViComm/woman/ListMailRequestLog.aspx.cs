﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールおねだり履歴一覧
--	Progaram ID		: ListMailRequestLog
--
--  Creation Date	: 2014.12.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListMailRequestLog:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAIL_REQUEST_LOG,this.ActiveForm);
	}
}