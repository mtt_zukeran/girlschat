﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド作成確認
--	Progaram ID		: ConfirmWriteBbsThread
--
--  Creation Date	: 2011.04.08
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;

public partial class ViComm_woman_ConfirmWriteBbsThread:MobileBbsExWomanBase {

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			ViewState["HANDLE_NM"] = sessionWoman.userWoman.bbsInfo.bbsThreadHandleNm;
			ViewState["TITLE"] = sessionWoman.userWoman.bbsInfo.bbsThreadTitle;
			ViewState["DOC"] = sessionWoman.userWoman.bbsInfo.bbsThreadDoc;
			lblThreadHandleNm.Text = sessionWoman.userWoman.bbsInfo.bbsThreadHandleNm;
			lblThreadTitle.Text = sessionWoman.userWoman.bbsInfo.bbsThreadTitle;
			lblThreadDoc.Text = sessionWoman.userWoman.bbsInfo.bbsThreadDoc.Replace("\r\n","<BR />");
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sNewBbsThreadSeq = string.Empty;
		
		using (BbsThread oBbsThread = new BbsThread()) {
			oBbsThread.WriteBbsThread(	sessionWoman.site.siteCd,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										string.Empty,
										Mobile.EmojiToCommTag(sessionWoman.carrier,iBridUtil.GetStringValue(ViewState["HANDLE_NM"])),
										Mobile.EmojiToCommTag(sessionWoman.carrier,iBridUtil.GetStringValue(ViewState["TITLE"])),
										Mobile.EmojiToCommTag(sessionWoman.carrier,iBridUtil.GetStringValue(ViewState["DOC"])),
										out sNewBbsThreadSeq);
		}
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("bbsthreadseq",sNewBbsThreadSeq);
		RedirectToDisplayDoc(ViCommConst.SCR_BBS_EX_CREATE_THREAD_COMPLETE,oParam);
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionWoman.userWoman.bbsInfo.bbsThreadHandleNm = iBridUtil.GetStringValue(ViewState["HANDLE_NM"]);
		sessionWoman.userWoman.bbsInfo.bbsThreadTitle = iBridUtil.GetStringValue(ViewState["TITLE"]);
		sessionWoman.userWoman.bbsInfo.bbsThreadDoc = iBridUtil.GetStringValue(ViewState["DOC"]);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("WriteBbsThread.aspx?back=1"));
	}
}
