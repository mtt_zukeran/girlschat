/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエスト報酬受取実行
--	Progaram ID		: RegistGameQuestRewardGetExecute
--
--  Creation Date	: 2012.07.17
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_woman_RegistGameQuestRewardGetExecute:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["quest_seq"]);
		string sQuestType = iBridUtil.GetStringValue(this.Request.QueryString["quest_type"]);
		string sLevelQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["level_quest_seq"]);
		string sEntryLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["entry_log_seq"]);
		string sLittleQuestSeq = string.Empty;
		string sOtherQuestSeq = string.Empty;
		string sResult = string.Empty;

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sQuestSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		if (sQuestType.Equals(PwViCommConst.GameQuestType.LEVEL_QUEST)) {
			rgxMatch = rgx.Match(sLevelQuestSeq);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			rgxMatch = rgx.Match(sEntryLogSeq);
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}

		using (QuestReward oQuestReward = new QuestReward()) {
			oQuestReward.QuestRewardGetExe(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sQuestSeq,
				sQuestType,
				sEntryLogSeq,
				ref sLevelQuestSeq,
				out sLittleQuestSeq,
				out sOtherQuestSeq,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.GameQuestRewardGetResult.RESULT_OK)) {
			IDictionary<string,string> oParameters = new Dictionary<string,string>();
			oParameters.Add("quest_seq",sQuestSeq);
			oParameters.Add("quest_type",sQuestType);
			oParameters.Add("level_quest_seq",sLevelQuestSeq);
			oParameters.Add("little_quest_seq",sLittleQuestSeq);
			oParameters.Add("other_quest_seq",sOtherQuestSeq);
			oParameters.Add("entry_log_seq",sEntryLogSeq);

			RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_WOMAN_QUEST_REWARD_GET_RESULT,oParameters);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}
}
