/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフィール画像画面
--	Progaram ID		: ViewProfilePic
--
--  Creation Date	: 2009.07.31
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewProfilePic:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
			sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
	}
}
