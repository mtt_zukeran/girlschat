/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 足あとランキング
--	Progaram ID		: ListMarkingRanking
--
--  Creation Date	: 2011.04.05
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListMarkingRanking:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_CAST_MARKING_RANKING,
					ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 選択された値をセッションに設定して検索します。
		string sCtlSeq = string.Empty;
		if (this.Request.Form["ctlseq"] != null) {
			sCtlSeq = iBridUtil.GetStringValue(this.Request.Form["ctlseq"]);
		}
		string sAddQuery = string.Format("&ctlseq={0}",sCtlSeq);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListMarkingRanking.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&ctlseq=[0-9]*|new=[1]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}
