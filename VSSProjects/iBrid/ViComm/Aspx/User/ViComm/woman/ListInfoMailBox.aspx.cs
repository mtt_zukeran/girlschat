﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お知らせメール受信BOX
--	Progaram ID		: ListInfoMailBox
--
--  Creation Date	: 2010.05.25
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Web.UI.MobileControls;
using ViComm;

public partial class ViComm_woman_ListInfoMailBox:MobileWomanPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		sessionWoman.ClearCategory();
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_RX_INFO_MAIL_BOX,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDel(mailSeqList.ToArray(),ViCommConst.RX);
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_INFO_MAIL + "&mail_delete_count=" + mailSeqList.Count));
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "ListInfoMailBox.aspx?" + Request.QueryString));
		}
	}
}
