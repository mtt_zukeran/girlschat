/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���Ѽ���� ���яڍ�
--	Progaram ID		: ViewGameItemShop
--
--  Creation Date	: 2011.07.28
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_woman_ViewGameItemShop:MobileSocialGameWomanBase {
	private NameValueCollection query = new NameValueCollection();

	protected void Page_Load(object sender,EventArgs e) {

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["item_seq"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["buy_num"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_SHOP_VIEW,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.BuyGameItem();
			}
		}
	}

	private void BuyGameItem() {
		string sSiteCode = sessionWoman.userWoman.CurCharacter.siteCd;
		string sUserSeq = sessionWoman.userWoman.userSeq;
		string sUserCharNo = sessionWoman.userWoman.CurCharacter.userCharNo;
		string[] sValues = this.Request.Params["NEXTLINK"].Split('_');
		string sItemSeq = sValues[0];
		string sBuyNum = sValues[1];
		string sGetCd = sValues[2];
		string sCastTreasureSeq = iBridUtil.GetStringValue(this.Request.Params["cast_treasure_seq"]);
		string sItemCategory = iBridUtil.GetStringValue(this.Request.Params["item_category"]);
		string sPageId = iBridUtil.GetStringValue(Session["PageId"]);
		string sStageSeq = iBridUtil.GetStringValue(Session["StageSeq"]);
		string sBattleLogSeq = iBridUtil.GetStringValue(Session["BattleLogSeq"]);
		string sSaleFlag = iBridUtil.GetStringValue(this.Request.Params["sale"]);

		string sPreMoney = this.sessionWoman.userWoman.CurCharacter.gameCharacter.gamePoint.ToString();
		string sPreCastGamePoint = this.sessionWoman.userWoman.CurCharacter.gameCharacter.castGamePoint.ToString();
		string sPreAttackPower = this.GetAttackPower();
		string sPreDefencePower = this.GetDefencePower();
		
		string result = this.BuyGameItemExeCute(sSiteCode,sUserSeq,sUserCharNo,sItemSeq,sBuyNum);
		
		if (result.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
			if (sGetCd.Equals(PwViCommConst.GameItemGetCd.CHARGE)) {
				sessionWoman.userWoman.CurCharacter.gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);
			}

			IDictionary<string,string> oParameters = new Dictionary<string,string>();

			if (sPageId == PwViCommConst.PageId.TRAP) {
				oParameters.Add("buy_num",sBuyNum);
				oParameters.Add("cast_treasure_seq",sCastTreasureSeq);
				oParameters.Add("item_category",sItemCategory);
				oParameters.Add("page_id",sPageId);
			} else if (sPageId == PwViCommConst.PageId.BOSS_BATTLE) {
				oParameters.Add("buy_num",sBuyNum);
				oParameters.Add("stage_seq",sStageSeq);
				oParameters.Add("page_id",sPageId);
			} else if (sPageId == PwViCommConst.PageId.SUPPORT_BATTLE) {
				oParameters.Add("buy_num",sBuyNum);
				oParameters.Add("battle_log_seq",sBattleLogSeq);
				oParameters.Add("page_id",sPageId);
			} else {
				oParameters.Add("buy_num",sBuyNum);
			}

			if(sGetCd.Equals(PwViCommConst.GameItemGetCd.CHARGE)) {
				oParameters.Add("pre_point",sPreCastGamePoint);
			} else {
				oParameters.Add("pre_money",sPreMoney);
			}
			oParameters.Add("pre_attack_power",sPreAttackPower);
			oParameters.Add("pre_defence_power",sPreDefencePower);
			oParameters.Add("sale",sSaleFlag);

			Session["PageId"] = null;
			Session["BattleLogSeq"] = null;
			
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ITEM_BUY_COMPLETE,oParameters);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}

	private string BuyGameItemExeCute(string sSiteCode,string sUserSeq,string sUserCharNo,string sItemSeq,string sBuyNum) {
		string sResult = "";
		GameItem oGameItem = new GameItem();
		oGameItem.BuyGameItem(sSiteCode,sUserSeq,sUserCharNo,sItemSeq,sBuyNum,out sResult);
		return sResult;
	}

	private string GetAttackPower() {
		string sValue = "0";

		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sForceCount = this.sessionWoman.userWoman.CurCharacter.gameCharacter.attackMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameAttackPowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}

	private string GetDefencePower() {
		string sValue = "0";

		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sForceCount = this.sessionWoman.userWoman.CurCharacter.gameCharacter.defenceMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameDefencePowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}
}
