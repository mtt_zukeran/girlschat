/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り一覧
--	Progaram ID		: ListFavorit
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListFavorit:MobileWomanPageBase {
	private string sMailSendFlag = string.Empty;

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		switch (iBridUtil.GetStringValue(Request.QueryString["mailsend"])) {
			case ViCommConst.FLAG_ON_STR:
				sMailSendFlag = ViCommConst.FLAG_ON_STR;
				break;
			case ViCommConst.FLAG_OFF_STR:
				sMailSendFlag = ViCommConst.FLAG_OFF_STR;
				break;
			default:
				sMailSendFlag = string.Empty;
				break;
		}

		if (!IsPostBack) {
			string sGrpSeq = iBridUtil.GetStringValue(Request.QueryString["grpseq"]);
			SetSelectBox(sGrpSeq);
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_FAVORIT,
					ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DEFAULT] != null) {
				cmdDefault_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_SEARCH] != null) {
				cmdSearch_Click(sender,e);
			}
		}
	}

	private void SetSelectBox(string pGrpSeq) {
		int iAllUserCount = 0;
		int iNullUserCount = 0;
		string sListText = string.Empty;

		using (Favorit oFavorit = new Favorit()) {
			iAllUserCount = oFavorit.GetSelfFavoritCount(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR
			);
		}

		iNullUserCount = iAllUserCount;

		DataSet oDataSet = GetFavoritGroup();
		Regex regex2 = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			iNullUserCount = iNullUserCount - int.Parse(dr["USER_COUNT"].ToString());
			sListText = string.Format("{0}({1})",regex2.Replace(iBridUtil.GetStringValue(dr["FAVORIT_GROUP_NM"]),string.Empty),iBridUtil.GetStringValue(dr["USER_COUNT"]));
			lstFavoritGroup.Items.Add(new MobileListItem(sListText,iBridUtil.GetStringValue(dr["FAVORIT_GROUP_SEQ"])));
		}

		lstFavoritGroup.Items.Insert(0,new MobileListItem(string.Format("すべて({0})",iAllUserCount.ToString()),string.Empty));
		lstFavoritGroup.Items.Insert(1,new MobileListItem(string.Format("ｸﾞﾙｰﾌﾟ未設定({0})",iNullUserCount.ToString()),"null"));

		foreach (MobileListItem item in lstFavoritGroup.Items) {
			if (item.Value.Equals(pGrpSeq)) {
				item.Selected = true;
			}
		}
	}

	private DataSet GetFavoritGroup() {
		DataSet oDataSet;
		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oDataSet = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,1,99);
		}

		return oDataSet;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iLoginViewStatus = 0;
		int iWaitingViewStatus = 0;
		int iBbsNonPublishFlag = 0;
		int iRankingNonPublishFlag = 0;
		int iNotSendBatchMailFlag = 0;
		int iBlogNonPublishFlag = 0;
		int iDiaryNonPublishFlag = 0;
		int iProfilePicNonPublishFlag = 0;
		int iUseCommentDetailFlag = 0;

		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_login"]),out iLoginViewStatus);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_waiting"]),out iWaitingViewStatus);
		if (this.Request.Form["jealousy_waiting"] == null) {
			iWaitingViewStatus = iLoginViewStatus;
		}
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_bbs"]),out iBbsNonPublishFlag);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_ranking"]),out iRankingNonPublishFlag);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_batch"]),out iNotSendBatchMailFlag);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_blog"]),out iBlogNonPublishFlag);

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_DIARY,2)) {
				int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_diary"]),out iDiaryNonPublishFlag);
			}
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_PROFILE_PIC,2)) {
				int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_profile_pic"]),out iProfilePicNonPublishFlag);
			}
		}
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_comment_detail"]),out iUseCommentDetailFlag);

		bool bOk = true;

		//やきもち状態ﾁｪｯｸ(片方だけｵﾌﾗｲﾝに見せている場合ｴﾗｰではじく) 
		//待機：待機0 ﾛｸﾞｲﾝ2 ｵﾌﾗｲﾝ1 
		//ﾛｸﾞｲﾝ：ﾛｸﾞｲﾝ0 ｵﾌﾗｲﾝ1 

		// 一方でもｵﾌﾗｲﾝの場合、両方ｵﾌﾗｲﾝに設定
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_JEALOUSY_OFFLINE_ANY,2)) {
				if (iWaitingViewStatus == 1 || iLoginViewStatus == 1) {
					iWaitingViewStatus = 1;
					iLoginViewStatus = 1;
				}
			}
		}

		if ((iWaitingViewStatus == 0 || iWaitingViewStatus == 2) && iLoginViewStatus == 1) {
			bOk = false;
		}
		if (iWaitingViewStatus == 1 && iLoginViewStatus == 0) {
			bOk = false;
		}

		if (bOk) {
			using (Favorit oFavorit = new Favorit()) {
				oFavorit.JealousyMainte(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,string.Empty,string.Empty,iLoginViewStatus,iWaitingViewStatus,iBbsNonPublishFlag,iRankingNonPublishFlag,iNotSendBatchMailFlag,iBlogNonPublishFlag,iDiaryNonPublishFlag,iProfilePicNonPublishFlag,iUseCommentDetailFlag,lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value,this.sMailSendFlag);
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListFavorit.aspx" + Request.Url.Query.Replace("&update=1",string.Empty) + "&update=1"));
		} else {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_JEALOUSY);
		}
	}

	protected void cmdDefault_Click(object sender,EventArgs e) {
		using (Favorit oFavorit = new Favorit()) {
			oFavorit.JealousyMainte(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,string.Empty,string.Empty,0,0,0,0,0,0,0,0,0,lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value,this.sMailSendFlag);
		}
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListFavorit.aspx?sortex=1&drows=5&update=1"));
	}

	protected void cmdSearch_Click(object sender,EventArgs e) {
		string sGoto = iBridUtil.GetStringValue(Request.Form["goto"]);
		string sFavoritGroupSeq = lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value;
		string sMailSendFlag = iBridUtil.GetStringValue(Request.QueryString["mailsend"]);
		string sList = iBridUtil.GetStringValue(Request.QueryString["list"]);

		if (sGoto.Contains("ListFavorit.aspx")) {
			if (!string.IsNullOrEmpty(sFavoritGroupSeq)) {
				if (sGoto.Contains("?")) {
					sGoto += "&grpseq=" + sFavoritGroupSeq;
				} else {
					sGoto += "?grpseq=" + sFavoritGroupSeq;
				}
			}

			if (sMailSendFlag != "") {
				if (sGoto.Contains("?")) {
					sGoto += "&mailsend=" + sMailSendFlag;
				} else {
					sGoto += "?mailsend=" + sMailSendFlag;
				}
			}

			if (sList != "") {
				if (sGoto.Contains("?")) {
					sGoto += "&list=" + sList;
				} else {
					sGoto += "?list=" + sList;
				}
			}
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sGoto));
	}

	private string RemoveSortEx(string pQueryString) {
		Regex regex = new Regex("(&|\\?)sortex=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(pQueryString,string.Empty);
	}
}
