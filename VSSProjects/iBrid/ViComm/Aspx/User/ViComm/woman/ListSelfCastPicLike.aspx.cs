/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 自身の出演者画像イイネ一覧
--	Progaram ID		: ListSelfCastPicLike
--  Creation Date	: 2013.12.23
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListSelfCastPicLike:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			string sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);

			if (string.IsNullOrEmpty(sPicSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}
			
			// 新着いいね通知 確認
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["pageno"])) ||
				iBridUtil.GetStringValue(Request.QueryString["pageno"]).Equals("1")) {
				using (CastLikeNotice oCastLikeNotice = new CastLikeNotice()) {	
					oCastLikeNotice.CastLikeNoticeConfirm(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_PIC,
						sPicSeq,
						string.Empty,
						string.Empty
					);
				}
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_CAST_PIC_LIKE,this.ActiveForm);
		}
	}
}
