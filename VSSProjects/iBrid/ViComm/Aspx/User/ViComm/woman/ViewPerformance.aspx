<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewPerformance.aspx.cs" Inherits="ViComm_woman_ViewPerformance" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
	    <mobile:SelectionList ID="lstFromYear" Runat="server" BreakAfter="false"></mobile:SelectionList>年
	    <mobile:SelectionList ID="lstFromMonth" Runat="server" BreakAfter="false"></mobile:SelectionList>月
	    <mobile:SelectionList ID="lstFromDay" Runat="server" BreakAfter="false"></mobile:SelectionList>日から
	    <br />
	    <mobile:SelectionList ID="lstToYear" Runat="server" BreakAfter="false"></mobile:SelectionList>年
	    <mobile:SelectionList ID="lstToMonth" Runat="server" BreakAfter="false"></mobile:SelectionList>月
	    <mobile:SelectionList ID="lstToDay" Runat="server" BreakAfter="false"></mobile:SelectionList>日まで
	    <br />
	    <mobile:SelectionList ID="chkNotPaymentOnlyFlag" Runat="server" SelectType="CheckBox">
			<Item Text="未精算分のみ表示する"></Item>
	    </mobile:SelectionList>
		$PGM_HTML02;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
