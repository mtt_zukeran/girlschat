/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ作戦設定
--	Progaram ID		: RegistGameBattleStart
--
--  Creation Date	: 2011.09.22
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_RegistGameBattleStart:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		int iTutorialFlag = ViCommConst.FLAG_OFF;

		if (!string.IsNullOrEmpty(sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus)) {
			iTutorialFlag = ViCommConst.FLAG_ON;
		} 
		
		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_START,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				string sTreasureSeq = iBridUtil.GetStringValue(Request.Params["NEXTLINK"]);
				int iAttackForceCount;
				int.TryParse(iBridUtil.GetStringValue(this.Request.Params["attack_force_count"]),out iAttackForceCount);

				if (iAttackForceCount > 0) {
					
					this.BattleStart(sPartnerUserSeq,sPartnerUserCharNo,sTreasureSeq,iAttackForceCount,iTutorialFlag);
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
			}
		}
	}

	private void BattleStart(string sPartnerUserSeq,string sPartnerUserCharNo,string sTreasureSeq,int iAttackForceCount,int iTutorialFlag) {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sSupportUserSeq = string.Empty;
		string sSupportUserCharNo = string.Empty;
		string sSupportBattleLogSeq = string.Empty;
		string sSupportRequestSubSeq = string.Empty;
		string sBattleType = PwViCommConst.GameBattleType.SINGLE;
		int iSurelyWinFlag = (iTutorialFlag.Equals(ViCommConst.FLAG_ON)) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		int iLevelUpFlag;
		int iAddForceCount;
		int iTreasureCompleteFlag;
		int iBonusGetFlag;
		int iWinFlag;
		int iComePoliceFlag;
		int iTrapUsedFlag;
		string sBattleLogSeq;
		string[] BreakGameItemSeq;
		string sResult;
		string sPreExp = this.sessionWoman.userWoman.CurCharacter.gameCharacter.exp.ToString();
		
		Battle oBattle = new Battle();
		oBattle.BattleStart(
			sSiteCd,
			sUserSeq,
			sUserCharNo,
			sPartnerUserSeq,
			sPartnerUserCharNo,
			sSupportUserSeq,
			sSupportUserCharNo,
			sSupportBattleLogSeq,
			sSupportRequestSubSeq,
			sBattleType,
			sTreasureSeq,
			iSurelyWinFlag,
			iAttackForceCount,
			out iLevelUpFlag,
			out iAddForceCount,
			out iTreasureCompleteFlag,
			out iBonusGetFlag,
			out iWinFlag,
			out iComePoliceFlag,
			out iTrapUsedFlag,
			out sBattleLogSeq,
			out BreakGameItemSeq,
			out sResult
		);

		string sRedirectUrl = string.Empty;

		if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_OK)) {
			this.CheckQuestClear(sUserSeq,sUserCharNo);
			this.CheckQuestClear(sPartnerUserSeq,sPartnerUserCharNo);
			
			if (iTutorialFlag.Equals(ViCommConst.FLAG_ON)) {

				string sGameItemSeq;

				int iNextTutorialStatus = int.Parse(sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus) + 1;
				//ﾁｭｰﾄﾘｱﾙ状態更新
				using (GameCharacter oGameCharacter = new GameCharacter()) {
					oGameCharacter.SetupTutorialStatus(
						this.sessionWoman.site.siteCd,
						this.sessionWoman.userWoman.userSeq,
						this.sessionWoman.userWoman.curCharNo,
						iNextTutorialStatus.ToString()
					);
				}

				//ﾊﾞｲｱｹﾞﾗを付与
				using (GameItem oGameItem = new GameItem()) {
					sGameItemSeq = oGameItem.GetGameItem(
						this.sessionWoman.site.siteCd,
						ViCommConst.OPERATOR,
						PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE,
						"GAME_ITEM_SEQ"
					);
				}

				using (GameItem oGameItem = new GameItem()) {
					oGameItem.GamePossessionItemMainte(
						this.sessionWoman.site.siteCd,
						this.sessionWoman.userWoman.userSeq,
						this.sessionWoman.userWoman.curCharNo,
						sGameItemSeq
					);
				}
			}
			
			string sBreakItemSeqList = string.Join("_",BreakGameItemSeq);
			
			if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
				string sPartnerGameCharacterType = this.GetGameCharacterType(sPartnerUserSeq,sPartnerUserCharNo);

				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				if (iWinFlag.Equals(ViCommConst.FLAG_ON)) {
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("treasure_complete_flag",iTreasureCompleteFlag.ToString());
					oParameters.Add("break_item_seq",sBreakItemSeqList);
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("tutorial_flag",iTutorialFlag.ToString());
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
				} else {
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("come_police_flag",iComePoliceFlag.ToString());
					oParameters.Add("trap_used_flag",iTrapUsedFlag.ToString());
					oParameters.Add("break_item_seq",sBreakItemSeqList);
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
				}
				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_BATTLE_SP,oParameters);
			} else {
				if (iWinFlag == ViCommConst.FLAG_ON) {
					sRedirectUrl = String.Format(
						"ViewGameFlashBattle.aspx?battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&treasure_complete_flag={3}&break_item_seq={4}&pre_exp={5}&tutorial_flag={6}",
						sBattleLogSeq,
						iLevelUpFlag.ToString(),
						iAddForceCount.ToString(),
						iTreasureCompleteFlag.ToString(),
						sBreakItemSeqList,
						sPreExp,
						iTutorialFlag
					);
				} else {
					sRedirectUrl = String.Format(
						"ViewGameFlashBattle.aspx?&battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&come_police_flag={3}&trap_used_flag={4}&break_item_seq={5}&pre_exp={6}",
						sBattleLogSeq,
						iLevelUpFlag.ToString(),
						iAddForceCount.ToString(),
						iComePoliceFlag.ToString(),
						iTrapUsedFlag.ToString(),
						sBreakItemSeqList,
						sPreExp
					);
				}

				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectUrl));
			}
		} else if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		} else {
			NameValueCollection query = new NameValueCollection();
			query["result"] = sResult;
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_BATTLE_NO_FINISH,query);
		}
	}

	private string GetCastTreasureLogCreate(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sGetTreasureLogSeq;
		string sStatus;
		string sTutorialBattleCastStageGroupType = null;
		string sTutorialBattleCastStageCastTreasureSeq = null;

		DataSet oDataSetCastTreasure;
		using (CastTreasure oCastTreasure = new CastTreasure()) {
			oDataSetCastTreasure = oCastTreasure.GetCastTreasureSeqData(this.sessionWoman.site.siteCd);
		}

		foreach (DataRow drCastTreasure in oDataSetCastTreasure.Tables[0].Rows) {
			sTutorialBattleCastStageGroupType = drCastTreasure["STAGE_GROUP_TYPE"].ToString();
			sTutorialBattleCastStageCastTreasureSeq = drCastTreasure["CAST_TREASURE_SEQ"].ToString();
		}
		
		using (GetCastTreasureLog oGetCastTreasureLog = new GetCastTreasureLog()) {
			oGetCastTreasureLog.GetCastTreasureLogCreate(
				sessionWoman.site.siteCd,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				sTutorialBattleCastStageGroupType,
				sTutorialBattleCastStageCastTreasureSeq,
				out sGetTreasureLogSeq,
				out sStatus
			);
		}

		return sGetTreasureLogSeq;
	}

	private string GetGameCharacterType(string sPartnerUserSeq,string sPartnerUserCharNo) {
		string sGameCharacterType = string.Empty;

		GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.PartnerUserSeq = sPartnerUserSeq;
		oCondition.PartnerUserCharNo = sPartnerUserCharNo;

		using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
			DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);

			if (ds.Tables[0].Rows.Count > 0) {
				sGameCharacterType = ds.Tables[0].Rows[0]["GAME_CHARACTER_TYPE"].ToString();
			}
		}

		return sGameCharacterType;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionWoman.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				ViCommConst.OPERATOR
			);
		}
	}
}
