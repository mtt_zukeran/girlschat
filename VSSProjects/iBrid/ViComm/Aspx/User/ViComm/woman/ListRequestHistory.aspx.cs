/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会話要求一覧
--	Progaram ID		: ListRequestHistory
--
--  Creation Date	: 2009.07.22
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;

public partial class ViComm_woman_ListRequestHistory:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_FIND_CALL_REQUEST);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					string sCode = iBridUtil.GetStringValue(dr["CODE"]);
					if (!sCode.Equals("1") && !sCode.Equals("4") && !sCode.Equals("7")) {
						lstFindSelect.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
					}
				}
			}
			ulong ulSel = ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_ALL;
			string sFindType = iBridUtil.GetStringValue(Request.QueryString["findtype"]);

			lstFindSelect.SelectedIndex = 0;
			foreach (MobileListItem item in lstFindSelect.Items) {
				if (item.Value == sFindType) {
					lstFindSelect.SelectedIndex = item.Index;
				}
			}
			if (sFindType.Equals("")) {
				sFindType = lstFindSelect.Items[lstFindSelect.SelectedIndex].Value;
			}
			
			sessionWoman.userWoman.talkRequestFindType = sFindType;
			
			if (sFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS) ||
				sFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_INCOMMING) ||
				sFindType.Equals(ViCommConst.FIND_CALL_RESULT_SUCCESS_OUTGOING)) {
				ulSel = ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_SUCCESS;
			} else if (sFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN) ||
				sFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_INCOMMING) ||
				sFindType.Equals(ViCommConst.FIND_CALL_RESULT_GROUP_MAN_OUTGOING)) {
				ulSel = ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_GROUP_MAN;
			} else {
				ulSel = ViCommConst.INQUIRY_TALK_REQUEST_HISTORY_ALL;
			}

			sessionWoman.ControlList(
					Request,
					ulSel,
					ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sAddQuery = string.Format("&findtype={0}",lstFindSelect.Items[lstFindSelect.SelectedIndex].Value);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
												   "ListRequestHistory.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&findtype=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}
