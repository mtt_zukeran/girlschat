<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginReminder.aspx.cs" Inherits="ViComm_woman_LoginReminder" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="vicomm" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		$PGM_HTML02;
	    <vicomm:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></vicomm:iBMobileLabel>
		$PGM_HTML05;
		<vicomm:iBMobileTextBox ID="txtMailAddr" runat="server" size="16" MaxLength="256"></vicomm:iBMobileTextBox>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
