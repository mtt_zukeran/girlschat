/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画新着いいね一覧
--	Progaram ID		: ListCastLikeNoticeMovie
--
--  Creation Date	: 2016.10.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListCastLikeNoticeMovie:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}
		
		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;
		
		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sMovieSeq) && sPlayMovie.Equals("1")) {
				bDownload = MovieHelper.Download(sMovieSeq, sessionWoman.userWoman.userSeq, sessionWoman.userWoman.curCharNo);
			};
			if (bDownload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
				sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_LIKE_NOTICE_MOVIE,this.ActiveForm);
			}
		}
	}
}