/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���̖���T���^�[�Q�b�g���ۓo�^
--	Progaram ID		: RegistWantedTargetRefuse
--
--  Creation Date	: 2015.03.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistWantedTargetRefuse:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
		} else {
			string sResult;
			string sSiteCd = sessionWoman.site.siteCd;
			string sUserSeq = sessionWoman.userWoman.userSeq;
			string sUserCharNo = sessionWoman.userWoman.curCharNo;
			
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				sessionWoman.userWoman.RegistWantedTargetRefuse(sSiteCd,sUserSeq,sUserCharNo,ViCommConst.FLAG_ON,out sResult);
				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					sessionWoman.userWoman.CurCharacter.characterEx.wantedTargetRefuseFlag = ViCommConst.FLAG_ON;
				}
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				sessionWoman.userWoman.RegistWantedTargetRefuse(sSiteCd,sUserSeq,sUserCharNo,ViCommConst.FLAG_OFF,out sResult);
				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					sessionWoman.userWoman.CurCharacter.characterEx.wantedTargetRefuseFlag = ViCommConst.FLAG_OFF;
				}
			}
		}

		if (sessionWoman.userWoman.CurCharacter.characterEx.wantedTargetRefuseFlag == ViCommConst.FLAG_OFF) {
			tagRegist.Visible = true;
			tagDelete.Visible = false;
		} else {
			tagRegist.Visible = false;
			tagDelete.Visible = true;
		}
	}
}
