﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャスト掲示板書込詳細
--	Progaram ID		: ViewCastBbsConf
--
--  Creation Date	: 2010.04.22
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web.UI.MobileControls;

public partial class ViComm_woman_ViewCastBbsConf:MobileWomanPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListCastBbsConf.aspx"));
			}
				
			if (this.IsGetAction(ViCommConst.ACTION_DELETE)) {
				cmdSubmit_Click(sender, e);
			}
		} else {
			if (this.IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		using (CastBbs oCastBbs = new CastBbs()) {
			oCastBbs.DeleteBbs(
				sessionWoman.site.siteCd,
				sessionWoman.GetBbsDocValue("USER_SEQ"),
				sessionWoman.GetBbsDocValue("USER_CHAR_NO"),
				sessionWoman.GetBbsDocValue("BBS_SEQ")
			);
		}
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETE_CAST_BBS_COMPLETE) + "&site=" + sessionWoman.site.siteCd);
	}
}
