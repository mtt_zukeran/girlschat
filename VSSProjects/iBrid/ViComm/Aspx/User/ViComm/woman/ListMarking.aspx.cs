/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: あしあと一覧
--	Progaram ID		: ListMarking
--
--  Creation Date	: 2009.07.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListMarking:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			// 足あとリストの確認日時を更新
			using (Marking oMarking = new Marking()) {
				oMarking.UpdateConfirmMarkingDate(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,ViCommConst.WOMAN);
			}

			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_MARKING,ActiveForm);
		}
	}
}
