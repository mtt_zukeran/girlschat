/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾌﾟﾚｾﾞﾝﾄされた一覧
--	Progaram ID		: ListGamePresentHistory
--
--  Creation Date	: 2011.09.23
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGamePresentHistory:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			using (GameCharacter oGameCharacter = new GameCharacter()) {
					oGameCharacter.UpdateLastPresentLogCheck(
					this.sessionWoman.site.siteCd,
					this.sessionWoman.userWoman.userSeq,
					this.sessionWoman.userWoman.CurCharacter.userCharNo
				);
			}

			this.Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_MAN_PRESENT_HISTORY,ActiveForm);
		} else {
			string sPresentHistorySeq = string.Empty;

			for (int i = 1;i <= 10;i++) {
				if (Request.Form[ViCommConst.BUTTON_GOTO_LINK + "_" + i] != null) {
					sPresentHistorySeq = iBridUtil.GetStringValue(Request.Params["present_history_seq" + i.ToString()]);
				}
			}

			if (!sPresentHistorySeq.Equals(string.Empty)) {
				string sResult = string.Empty;

				using (GameCharacterPresentHistory oGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
					sResult = oGameCharacterPresentHistory.GetPresentGameItem(
						this.sessionWoman.site.siteCd,
						sPresentHistorySeq
					);
				}

				if (sResult == PwViCommConst.GetPresentGameItemResult.RESULT_OK) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(
						sessionWoman.root + sessionWoman.sysType,
						sessionWoman.sessionId,
						string.Format("ViewGamePresentHistory.aspx?get_flag=1&present_history_seq={0}",sPresentHistorySeq)
					));
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
			}
		}
	}
}
