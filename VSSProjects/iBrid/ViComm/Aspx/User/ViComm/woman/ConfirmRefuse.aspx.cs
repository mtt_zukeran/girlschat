﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 拒否登録確認
--	Progaram ID		: ConfirmRefuse
--
--  Creation Date	: 2011.01.05
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ConfirmRefuse : MobileWomanPageBase {
	virtual protected void Page_Load(object sender, EventArgs e) {

		if (this.sessionWoman.parseContainer.parseUser.currentTableIdx != ViCommConst.DATASET_MAN) {
			DataRow dr;
			sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),1,out dr);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
	}
}
