<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StartWaitting.aspx.cs" Inherits="ViComm_woman_StartWaitting" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:SelectionList ID="lstWaitTime" Runat="server"></mobile:SelectionList>
		$PGM_HTML02;
		<mobile:SelectionList ID="lstConnectType" Runat="server"></mobile:SelectionList>
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML03;
		<tx:TextArea ID="txtWaitComment" runat="server" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" MaxLength="300" EmojiPaletteEnabled="true">
		</tx:TextArea>	
		$PGM_HTML04;
		<mobile:SelectionList ID="chkMonitorEnableFlag" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML05;" />
		</mobile:SelectionList>		
		$PGM_HTML06;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
