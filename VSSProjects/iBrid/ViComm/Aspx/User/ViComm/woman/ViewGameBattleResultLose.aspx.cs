/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@�ʏ����ٔs�k
--	Progaram ID		: ViewGameBattleResultLose
--
--  Creation Date	: 2011.09.23
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_woman_ViewGameBattleResultLose:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.checkRecentBattleLog();

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			if (!string.IsNullOrEmpty(this.Request.Params["cmdNext"])) {
				int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
				if (iCharacterOnlineStatus != ViCommConst.USER_TALKING && iCharacterOnlineStatus != ViCommConst.USER_OFFLINE) {
					string sBattleLogSeq = this.Request.Params["NEXTLINK"].ToString();
					string sResult = string.Empty;

					this.SupportRequest(sBattleLogSeq,out sResult);

					if (sResult.Equals(PwViCommConst.GameSupportRequestResult.RESULT_OK)) {
						string sRedirectUrl = string.Format("ListGameSupportRequest.aspx?battle_log_seq={0}",sBattleLogSeq);
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectUrl));
					} else {
						RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
					}
				} else {
					if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE,ActiveForm)) {
						RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
					}
				}
			}
		}
	}

	private void checkRecentBattleLog() {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition(this.Request.QueryString);
		oCondition.SiteCd = this.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.sessionWoman.userWoman.curCharNo;
		oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

		using (BattleLog oBattleLog = new BattleLog()) {
			if (!oBattleLog.CheckRecentBattleLog(oCondition)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	private void SupportRequest(string sBattleLogSeq,out string sResult) {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sSexCd = ViCommConst.OPERATOR;

		using (SupportRequest oSupportRequest = new SupportRequest()) {
			oSupportRequest.ExecuteSupportRequest(sSiteCd,sUserSeq,sUserCharNo,sSexCd,sBattleLogSeq,out sResult);
		}
	}
}
