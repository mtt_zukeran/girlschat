/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーメール受信BOX
--	Progaram ID		: ListUserMailBox
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Collections.Generic;
using ViComm;

public partial class ViComm_woman_ListUserMailBox:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionWoman.ClearCategory();
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			// 3分以内メール返信ボーナス用の値を取得
			MailResBonus2 oMailResBonus2 = new MailResBonus2();
			sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus = oMailResBonus2.SetEventData(ref sessionWoman.userWoman);

			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_SEARCH] != null) {
			
				string sSearchKey = this.Request.Form["search_key"];

				string sBaseUrl = sessionWoman.GetNavigateUrl(ViCommPrograms.GetCurrentAspx());
				UrlBuilder oUrlBuilder = new UrlBuilder(sBaseUrl, sessionWoman.requestQuery.QueryString);
				oUrlBuilder.Parameters.Remove("pageno");
				oUrlBuilder.Parameters.Remove("search_key");
				if (!string.IsNullOrEmpty(sSearchKey)) {
					oUrlBuilder.Parameters.Add("search_key", sSearchKey);
				}
				RedirectToMobilePage(oUrlBuilder.ToString(true));
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDel(mailSeqList.ToArray(),ViCommConst.RX);
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_MAN_MAIL + "&mail_delete_count=" + mailSeqList.Count));
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "ListUserMailBox.aspx?" + Request.QueryString));
		}
	}
}
