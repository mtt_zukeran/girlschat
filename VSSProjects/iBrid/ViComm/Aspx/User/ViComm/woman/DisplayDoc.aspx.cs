/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サイト文章表示
--	Progaram ID		: DisplayDoc
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Text;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Configuration;

public partial class ViComm_woman_DisplayDoc:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		if (IsAvailableService(ViCommConst.RELEASE_UPDATE_SESSION)) {
			if (sessionWoman.logined) {
				sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
				sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
			}
		}
		if (iBridUtil.GetStringValue(Request.Params[ViCommConst.ACTION_REDIRECT]).Equals(ViCommConst.FLAG_ON_STR)) {
			cmdNextLink_Click(sender,e);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			using (SiteHtmlDoc oDoc = new SiteHtmlDoc()) {
				System.Collections.Generic.List<string> oSexCdList = new System.Collections.Generic.List<string>();
				if (ViCommConst.FLAG_ON == sessionObj.site.siteHtmlDocSexCheckFlag) {
					oSexCdList.Add("3");
					if (sessionObj.logined) {
						oSexCdList.Add(sessionObj.sexCd);
					}
				}
				string sHtml;
				if (iBridUtil.GetStringValue(Request.QueryString["sc"]) == "") {
					oDoc.GetOne(sessionWoman.site.siteCd,iBridUtil.GetStringValue(Request.QueryString["doc"]),sessionWoman.currentPageAgentType,ViCommConst.DEFUALT_PUB_DAY,oSexCdList.ToArray(),out sHtml);
				} else {
					oDoc.GetOne(iBridUtil.GetStringValue(Request.QueryString["sc"]),iBridUtil.GetStringValue(Request.QueryString["doc"]),sessionWoman.currentPageAgentType,ViCommConst.DEFUALT_PUB_DAY,oSexCdList.ToArray(),out sHtml);
				}
				if (string.IsNullOrEmpty(sHtml)) {
					sHtml = iBridUtil.GetStringValue(Request.QueryString["doc"]);
				}
				tag01.Text = sHtml;
			}
			string sAction = iBridUtil.GetStringValue(this.Request.QueryString["frmaction"]);
			if (!string.IsNullOrEmpty(sAction)) {
				this.frmMain.Action = sAction;
				this.EnableViewState = false;
			}
			string sMethod = iBridUtil.GetStringValue(this.Request.QueryString["frmmethod"]);
			if (sMethod.ToLower().Equals("get")) {
				this.frmMain.Method = System.Web.UI.MobileControls.FormMethod.Get;
			}

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (!string.IsNullOrEmpty(Request.Params[ViCommConst.BUTTON_FINDUSER])) {
				this.FindUser();
			} else if (Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				cmdNextLink_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RESET_MAIL_NG] != null) {
				this.ResetMailNg();
			}
		}
	}

	private void FindUser() {
		string sRegistFrom = iBridUtil.GetStringValue(this.Request.Params["registfrom"]);
		string sRegistTo = iBridUtil.GetStringValue(this.Request.Params["registto"]);

		UrlBuilder oUrlBuilder = new UrlBuilder("ListFindResult.aspx");
		oUrlBuilder.Parameters.Add("registfrom",sRegistFrom);
		oUrlBuilder.Parameters.Add("registto",sRegistTo);
		RedirectToMobilePage(SessionObjs.Current.GetNavigateUrl(oUrlBuilder.ToString()));
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		StringBuilder oRequest = new StringBuilder();
		string sKeyInput = string.Empty;

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.StartsWith("*")) {
				sKeyInput = sKey.Replace("*",string.Empty);
				oRequest.Append(string.Format("&{0}={1}&{2}={3}",sKey,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKey]),enc),sKeyInput,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKeyInput]),enc)));
			} else if (sKey.StartsWith("?")) {
				sKeyInput = sKey.Replace("?",string.Empty);
				oRequest.Append(string.Format("&{0}={1}&{2}={3}",sKey,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKey]),enc),sKeyInput,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKeyInput]),enc)));
			}
		}
		string sScreenId = iBridUtil.GetStringValue(this.Request.Params["scrid"]);
		if (!sScreenId.Equals(string.Empty)) {
			oRequest.Append(string.Format("&scrid={0}",sScreenId));
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("MailFormConfirm.aspx?doc={0}&error={1}&success={2}&send={3}&title={4}{5}",
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["doc"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["error"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["success"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["send"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["title"]),enc),
																																oRequest.ToString())));
	}

	protected void cmdNextLink_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sNextUrl = iBridUtil.GetStringValue(Request.Params["NEXTLINK"]);
		string sAddPointId = iBridUtil.GetStringValue(Request.Params["ADD_POINT_ID"]);
		string sKeyInput = string.Empty;
		StringBuilder oRequest = new StringBuilder();

		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableRedirectAddBonusPoint"]).Equals(ViCommConst.FLAG_ON_STR)) {
			if (sessionObj.logined && !string.IsNullOrEmpty(sAddPointId)) {
				using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
					string sResult = string.Empty;
					oUserDefPoint.AddBonusPoint(sessionObj.site.siteCd,sessionObj.GetUserSeq(),sAddPointId,out sResult);
				}
			}
		}

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.StartsWith("*")) {
				oRequest.Append(string.Format("&{0}={1}",sKey.Replace("*",string.Empty),HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKey]),enc)));
			}
		}

		string sSep = sNextUrl.Contains("?") ? string.Empty : "?";

		if (sNextUrl.StartsWith("http://") || sNextUrl.StartsWith("https://")) {
			RedirectToMobilePage(sNextUrl + sSep + oRequest.ToString());
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sNextUrl + sSep + oRequest.ToString()));
		}
	}

	private void ResetMailNg() {
		using (User oUser = new User()) {
			oUser.ResetMailNg(sessionWoman.userWoman.userSeq);
		}
		RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_RESET_MAIL_NG_COMPLETE);
	}
}
