<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResignedReturn.aspx.cs" Inherits="ViComm_woman_ResignedReturn" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
	    $PGM_HTML02;
		<br />
		$PGM_HTML05;
		<cc1:iBMobileTextBox ID="txtLoginId" runat="server" MaxLength="12" Size="16" Numeric="true"></cc1:iBMobileTextBox>
		<br />
		$PGM_HTML06;
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="12" Size="16" Numeric="true"></cc1:iBMobileTextBox>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
