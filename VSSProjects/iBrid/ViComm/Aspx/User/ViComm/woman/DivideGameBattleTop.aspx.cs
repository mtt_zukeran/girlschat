/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ バトルTOP 宝石コンプ状況によるページ切替
--	Progaram ID		: DivideGameBattleTop
--
--  Creation Date	: 2012.09.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DivideGameBattleTop:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sStageGroupType = string.Empty;
		string sRedirectUrl = string.Empty;

		sStageGroupType = this.GetAimStageGroupType(sSiteCd,sUserSeq,sUserCharNo);

		if (!string.IsNullOrEmpty(sStageGroupType)) {
			sRedirectUrl = string.Format("ListGameTreasure.aspx?scrid=01&stage_group_type={0}",sStageGroupType);
		} else {
			sRedirectUrl = "ListGameCharacterBattle.aspx?drows=5";
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectUrl));
	}

	public string GetAimStageGroupType(string sSiteCd,string sUserSeq,string sUserCharNo) {
		string sStageGroupType = string.Empty;
		string sResult = string.Empty;

		using (StageGroup oStageGroup = new StageGroup()) {
			oStageGroup.GetAimStageGroupType(sSiteCd,sUserSeq,sUserCharNo,out sStageGroupType,out sResult);
		}

		return sStageGroupType;
	}
}
