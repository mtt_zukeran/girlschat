<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyRequest.aspx.cs" Inherits="ViComm_woman_ModifyRequest" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlWrite" Runat="server">
			$PGM_HTML02;
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>¶ĂșȚŰIđF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>JeSIđF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<mobile:SelectionList ID="lstCategory" Runat="server" SelectType="DropDown" BreakAfter="true">
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>i»ó”F</font><br />" />
			<mobile:SelectionList ID="lstProgress" Runat="server" SelectType="DropDown" BreakAfter="true">
			</mobile:SelectionList>

			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>ÇÒșÒĘÄF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>ÇÒRgF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<tx:TextArea ID="txtAdminComment" runat="server" Row="10" Rows="6" Columns="24" EmojiPaletteEnabled="true" BrerakAfter="true" BreakBefore="false"></tx:TextArea>
			$PGM_HTML03;
		</mobile:Panel>
		<mobile:Panel ID="pnlConfirm" Runat="server">
			$PGM_HTML04;
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>¶ĂșȚŰIđF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>JeSIđF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<ibrid:iBMobileLabel ID="lblCategory" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>
			<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>i»ó”F</font><br />" />
			<ibrid:iBMobileLabel ID="lblProgress" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>
			
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>ÇÒșÒĘÄF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>ÇÒRgF</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<ibrid:iBMobileLabel ID="lblAdminComment" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>
			$PGM_HTML05;
		</mobile:Panel>
		<mobile:Panel ID="pnlComplete" Runat="server">
			$PGM_HTML06;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
