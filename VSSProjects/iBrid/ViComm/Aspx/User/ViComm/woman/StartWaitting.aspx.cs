/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 待機設定
--	Progaram ID		: StartWaitting
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_StartWaitting:MobileWomanPageBase {
	protected virtual int MaxWaitCommentLength {
		get {
			return 300;
		}
	}
	protected string StandbyTimeType {
		get {
			return this.lstWaitTime.SelectedIndex < 0 ? string.Empty : iBridUtil.GetStringValue(this.lstWaitTime.SelectedIndex);
		}
	}

	string sGotoPage;
	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sGotoPage = iBridUtil.GetStringValue(Request.QueryString["gotopage"]);
		if (!IsPostBack) {

			int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);

			using (Cast oCast = new Cast()) {
				using (DataSet oDs = oCast.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,1)) {
					if (oDs.Tables[0].Rows.Count > 0) {
						DataRow oDr = oDs.Tables[0].Rows[0];
						this.txtWaitComment.Text = iBridUtil.GetStringValue(oDr["WAITING_COMMENT"]);
					}
				}
			}

			if (!((iCharacterOnlineStatus.Equals(ViCommConst.USER_OFFLINE)) || (iCharacterOnlineStatus.Equals(ViCommConst.USER_LOGINED)))) {
				bool sWaitEnd = iBridUtil.GetStringValue(this.Request.QueryString["waitend"]).Equals(ViCommConst.FLAG_ON_STR);
				if (sWaitEnd) {
					using (ManageCompany oManageCompany = new ManageCompany()) {
						if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ERR_WAIT_BY_TALKING,2)) {
							if (iCharacterOnlineStatus.Equals(ViCommConst.USER_TALKING) && sessionWoman.userWoman.dummyTalkFlag.Equals(ViCommConst.FLAG_OFF)) {
								RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_WAIT_BY_TALKING));
							}
						}
					}

					sessionWoman.userWoman.StartWaitting(DateTime.Now.Add(TimeSpan.FromHours(-1)),string.Empty,false,sessionWoman.mobileUserAgent,ViCommConst.FLAG_OFF,txtWaitComment.Text,"",this.StandbyTimeType,sessionWoman.adminFlg);
					sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
					if (sGotoPage.Equals(string.Empty)) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_CAST_WAITING_END));
					} else {
						RedirectToGotoPage();
					}
				}
			}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
				if (!((iCharacterOnlineStatus.Equals(ViCommConst.USER_OFFLINE)) || (iCharacterOnlineStatus.Equals(ViCommConst.USER_LOGINED)))) {
					lstWaitTime.Items.Add(new MobileListItem("待機終了","-1"));
				}

				DataSet ds;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_WAIT_TIME);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstWaitTime.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
				}

				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_WAIT_TYPE);
				int iIdx = 0;
				bool bOk;
				foreach (DataRow dr in ds.Tables[0].Rows) {
					bOk = true;

					if (dr["CODE"].Equals(ViCommConst.WAIT_TYPE_BOTH) || dr["CODE"].Equals(ViCommConst.WAIT_TYPE_VIDEO)) {
						if (sessionWoman.carrier.Equals(ViCommConst.KDDI)) {
							bOk = false;
						} else if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
							if (sessionWoman.userWoman.useCrosmileFlag == ViCommConst.FLAG_OFF) {
								bOk = false;
							}
						}
					}
					if (bOk) {
						lstConnectType.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
						if (sessionWoman.userWoman.connectType.Equals(dr["CODE"].ToString())) {
							lstConnectType.SelectedIndex = iIdx;
						}
						iIdx++;
					}
				}
			}

			chkMonitorEnableFlag.Items[0].Selected = (sessionWoman.userWoman.monitorEnableFlag != 0);
			chkMonitorEnableFlag.Visible = (sessionWoman.site.castCanSelectMonitorFlag != 0);

			lstConnectType.Visible = (sessionWoman.site.castCanSelectConnectType != 0);

		} else {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ERR_WAIT_BY_TALKING,2)) {
					int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
					if (iCharacterOnlineStatus.Equals(ViCommConst.USER_TALKING) && sessionWoman.userWoman.dummyTalkFlag.Equals(ViCommConst.FLAG_OFF)) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_WAIT_BY_TALKING));
					}
				}
			}
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdDummyWait"] != null) {
				cmdDummyWait_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		double dWaitHour;
		double.TryParse(lstWaitTime.Items[lstWaitTime.SelectedIndex].Value,out dWaitHour);

		if (!this.ValidateInput())
			return;

		string sWaitingComment = txtWaitComment.Text;


		string isDummyCall = iBridUtil.GetStringValue(this.Request.Params["is_dummy"]);
		if (isDummyCall.Equals("1")) {
			this.CallDummy(dWaitHour * 60,sWaitingComment);
		} else {
			if (sessionWoman.carrier.Equals(ViCommConst.DOCOMO) || sessionWoman.carrier.Equals(ViCommConst.KDDI) || sessionWoman.carrier.Equals(ViCommConst.SOFTBANK)) {
				if (sessionWoman.userWoman.useCrosmileFlag.Equals(ViCommConst.FLAG_ON)) {
					sessionWoman.userWoman.SetupCrosmile(sessionWoman.userWoman.userSeq,false);
				}

				if (sessionWoman.userWoman.useVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
					sessionWoman.userWoman.SetupVoiceapp(sessionWoman.userWoman.userSeq,ViCommConst.FLAG_OFF);
				}
			}

			sessionWoman.userWoman.StartWaitting(
				DateTime.Now.Add(TimeSpan.FromHours(dWaitHour)),
				lstConnectType.Items[lstConnectType.SelectedIndex].Value,
				chkMonitorEnableFlag.Items[0].Selected,
				sessionWoman.mobileUserAgent,
				ViCommConst.FLAG_OFF,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,sWaitingComment)),
				iBridUtil.GetStringValue(Request.Params["rdoChangeRank"]),
				this.StandbyTimeType,
				sessionWoman.adminFlg
			);

			if (sessionWoman.userWoman.useCrosmileFlag == ViCommConst.FLAG_ON) {
				string sLastVer = string.Empty;
				sessionWoman.GetCrosmileSipUriByRegistKey(sessionWoman.userWoman.crosmileRegistKey,out sessionWoman.userWoman.crosmileUri,out sLastVer);
				sessionWoman.ModifyUserCrosmileUri(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,ViCommConst.OPERATOR,sessionWoman.userWoman.crosmileUri,string.Empty,sLastVer);
			}
			sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);

			if (lstWaitTime.Items[lstWaitTime.SelectedIndex].Value != "-1") {
				DataSet dsSite = sessionWoman.userWoman.GetCharacterList("");
				using (MailLog oMailLog = new MailLog()) {
					foreach (DataRow dr in dsSite.Tables[0].Rows) {
						if (dr["USER_CHAR_NO"].ToString().Equals(ViCommConst.MAIN_CHAR_NO)) {
							oMailLog.TxLoginMail(dr["SITE_CD"].ToString(),sessionWoman.userWoman.userSeq,ViCommConst.MAIN_CHAR_NO);
						}
					}
				}

				using (CastNews oCastNews = new CastNews()) {
					oCastNews.LogCastNews(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,"2");
				}
				
				if (sGotoPage.Equals(string.Empty)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_CAST_WAITING_START));
				} else {
					RedirectToGotoPage();
				}
			} else {
				if (sGotoPage.Equals(string.Empty)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_CAST_WAITING_END));
				} else {
					RedirectToGotoPage();
				}
			}
		}
	}

	protected void cmdDummyWait_Click(object sender,EventArgs e) {
		int iWaitMin;
		int.TryParse(iBridUtil.GetStringValue(Request.Params["lstDummyWaitTime"]),out iWaitMin);

		if (!this.ValidateInput())
			return;

		string sWaitingComment = txtWaitComment.Text;

		this.CallDummy(iWaitMin,sWaitingComment);
	}

	private bool ValidateInput() {
		string sWaitingComment = txtWaitComment.Text;

		//ｺﾒﾝﾄに$ﾏｰｸがある場合ｴﾗｰﾒｯｾｰｼﾞ表示
		if (txtWaitComment.Text.IndexOf("$") != -1) {
			txtWaitComment.Text = txtWaitComment.Text.Replace("$","");
			lblErrorMessage.Text = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_INPUT_DOLLAR_MARK);
			return false;
		}
		
		//ｺﾒﾝﾄにNGﾜｰﾄﾞがある場合ｴﾗｰﾒｯｾｰｼﾞ表示
		string sNGWord;
		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}
		if (!sessionWoman.ngWord.VaidateDoc(sWaitingComment,out sNGWord)) {
			//「{0}」の単語が不正です。
			lblErrorMessage.Text = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			return false;
		}

		//文字数チェック
		if (Encoding.GetEncoding(932).GetByteCount(txtWaitComment.Text) > 600) {
			lblErrorMessage.Text = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_LETTER_COUNT),"全角300");
			return false;
		}

		return true & this.CheckOther();
	}

	private void CallDummy(double pWaitMin,string pWaitComment) {

		sessionWoman.userWoman.StartWaitting(
			DateTime.Now.AddMinutes(pWaitMin),
			lstConnectType.Items[lstConnectType.SelectedIndex].Value,
			chkMonitorEnableFlag.Items[0].Selected,
			sessionWoman.mobileUserAgent,
			ViCommConst.FLAG_ON,
			HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,pWaitComment)),
			"",
			this.StandbyTimeType,
			sessionWoman.adminFlg
		);

		if (sessionWoman.site.dummyTalkTxLoginMailFlag == 1 && lstWaitTime.Items[lstWaitTime.SelectedIndex].Value != "-1") {
			DataSet dsSite = sessionWoman.userWoman.GetCharacterList("");
			using (MailLog oMailLog = new MailLog()) {
				foreach (DataRow dr in dsSite.Tables[0].Rows) {
					if (dr["USER_CHAR_NO"].ToString().Equals(ViCommConst.MAIN_CHAR_NO)) {
						oMailLog.TxLoginMail(dr["SITE_CD"].ToString(),sessionWoman.userWoman.userSeq,ViCommConst.MAIN_CHAR_NO);
					}
				}
			}
		}
		sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
		if (sGotoPage.Equals(string.Empty)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_CAST_WAITING_START));
		} else {
			RedirectToGotoPage();
		}
	}

	private void RedirectToGotoPage() {
		UrlBuilder oUrlBuilder = new UrlBuilder(sGotoPage,Request.QueryString);
		oUrlBuilder.RemoveParameter("gotopage");
		oUrlBuilder.RemoveParameter("waitend");
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrlBuilder.ToString()));
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
