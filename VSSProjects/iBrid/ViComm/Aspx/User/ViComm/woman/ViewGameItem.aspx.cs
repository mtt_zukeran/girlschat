/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���Ѽ���� ���яڍ�
--	Progaram ID		: ViewGameItem
--
--  Creation Date	: 2011.07.29
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_woman_ViewGameItem:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(this.Request.QueryString["item_seq"]);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_VIEW,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				string sItemSeq = iBridUtil.GetStringValue(this.Request.Params["NEXTLINK"]);
				string sBuyNum = iBridUtil.GetStringValue(this.Request.Params["buyItemNum"]);
				string sCastTreasureSeq = iBridUtil.GetStringValue(this.Request.Params["cast_treasure_seq"]);
				string sItemCategory = iBridUtil.GetStringValue(this.Request.Params["item_category"]);
				string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.Params["battle_log_seq"]);
				string sPageId = iBridUtil.GetStringValue(this.Request.Params["page_id"]);
				string sSaleFlag = iBridUtil.GetStringValue(this.Request.Params["sale"]);

				if (iBridUtil.GetStringValue(Session["PageId"]) != PwViCommConst.PageId.BOSS_BATTLE) {
					Session["PageId"] = sPageId;
					Session["BattleLogSeq"] = sBattleLogSeq;
				}
				
				if (sPageId == PwViCommConst.PageId.TRAP) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(
					sessionWoman.root + 
					sessionWoman.sysType,
					sessionWoman.sessionId,
					String.Format("ViewGameItemShop.aspx?item_seq={0}&buy_num={1}&cast_treasure_seq={2}&item_category={3}&page_id={4}&sale={5}",
					sItemSeq,
					sBuyNum,
					sCastTreasureSeq,
					sItemCategory,
					sPageId,
					sSaleFlag)));
				}else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,String.Format("ViewGameItemShop.aspx?item_seq={0}&buy_num={1}&sale={2}",sItemSeq,sBuyNum,sSaleFlag)));
				}
				
			}
		}
	}
}
