/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE���L���`�h�~�ݒ�ڍ�
--	Progaram ID		: ViewGameJealousyMainte
--
--  Creation Date	: 2012.10.29
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameJealousyMainte:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		string sUserSeq = iBridUtil.GetStringValue(this.Request.QueryString["user_seq"]);
		string sUserCharNo = iBridUtil.GetStringValue(this.Request.QueryString["user_char_no"]);
		
		if(string.IsNullOrEmpty(sUserSeq) || string.IsNullOrEmpty(sUserCharNo)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					PwViCommConst.INQUIRY_GAME_JEALOUSY_MAINTE,
					ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sUserSeq,sUserCharNo);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pUserSeq,string pUserCharNo) {
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"RegistGameJealousyMainte.aspx" + Request.Url.Query.Replace("&update=1",string.Empty)));
	}
}
