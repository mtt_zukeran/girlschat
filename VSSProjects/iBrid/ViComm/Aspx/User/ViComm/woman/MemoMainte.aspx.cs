/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メモ更新
--	Progaram ID		: MemoMainte
--
--  Creation Date	: 2010.12.02
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_MemoMainte:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			DataRow dr;
			if (!sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),1,out dr)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
			}
			txtMemoDoc.Text = sessionWoman.GetManValue("MEMO_DOC");
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sReviews = string.Empty;
		bool bOk = true;
		this.lblErrorMessage.Text = string.Empty;

		DataRow dr;
		if (!sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),1,out dr)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
		}
		
		if (this.Request.Form["reviews"] != null) {
			sReviews = iBridUtil.GetStringValue(this.Request.Form["reviews"]);
			if (sReviews.Equals(string.Empty)) {
				bOk = false;
				lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_REVIEWS);
			}
		}

		if (bOk & CheckOther()) {
			string sMemoDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtMemoDoc.Text));
			sMemoDoc = Mobile.GetInputValue(sMemoDoc,4000,System.Text.Encoding.UTF8);

			using (Memo oMemo = new Memo()) {
				oMemo.MemoMainte(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sessionWoman.GetManValue("USER_SEQ"),
					sessionWoman.GetManValue("USER_CHAR_NO"),
					sReviews,
					sMemoDoc,
					ViCommConst.FLAG_OFF);
			}
			
			//完了ﾍﾟｰｼﾞへのリダイレクトをさせない
			//RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_CAST_MODIFY_MEMO));

			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MODIFY_COMMENT_COMPLATE);
		}
	}


	protected void cmdDelete_Click(object sender,EventArgs e) {
		DataRow dr;
		if (!sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),1,out dr)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
		}
		
		using (Memo oMemo = new Memo()) {
			oMemo.MemoMainte(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sessionWoman.GetManValue("USER_SEQ"),
				sessionWoman.GetManValue("USER_CHAR_NO"),
				string.Empty,
				string.Empty,
				ViCommConst.FLAG_ON);
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_CAST_DELETE_MEMO));
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
