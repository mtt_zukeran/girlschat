/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 不具合報告一覧
--	Progaram ID		: ListDefectReport
--
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListDefectReport:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		this.setLstCategory();

		if (!IsPostBack) {
			txtKeyword.Text = HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.QueryString["kwd"]));

			foreach (MobileListItem item in lstCategory.Items) {
				if (item.Value == iBridUtil.GetStringValue(this.Request.QueryString["ctgseq"])) {
					item.Selected = true;
				}
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_DEFECT_REPORT,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void setLstCategory() {

		using (DefectReport oDefectReport = new DefectReport()) {
			string sBlogFlag = (sessionWoman.userWoman.CurCharacter.characterEx.enabledBlogFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
			string sRichFlag = (sessionWoman.userWoman.CurCharacter.characterEx.enabledRichinoFlag.Equals(ViCommConst.FLAG_ON)) ? null : ViCommConst.FLAG_OFF_STR;
			
			DataSet dsCategory = oDefectReport.GetDefectReportCategory(sessionWoman.site.siteCd,ViCommConst.OPERATOR,sBlogFlag,sRichFlag);

			foreach (DataRow drCategory in dsCategory.Tables[0].Rows) {
				lstCategory.Items.Add(new MobileListItem(drCategory["DEFECT_REPORT_CATEGORY_NM"].ToString(),drCategory["DEFECT_REPORT_CATEGORY_SEQ"].ToString()));
			}

		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		UrlBuilder oUrlBuilder = new UrlBuilder("ListDefectReport.aspx");

		if (!txtKeyword.Text.Equals(string.Empty)) {
			if (txtKeyword.Text.Length > 30) {
				sessionWoman.errorMessage = "ｷｰﾜｰﾄﾞが長すぎます";
				return;
			}

			Encoding enc = Encoding.GetEncoding("Shift_JIS");
			oUrlBuilder.Parameters.Add("kwd",HttpUtility.UrlEncode(txtKeyword.Text,enc));
		}

		if (!lstCategory.Items[lstCategory.SelectedIndex].Value.Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("ctgseq",lstCategory.Items[lstCategory.SelectedIndex].Value);
		}

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["prgseq"]))) {
			oUrlBuilder.Parameters.Add("prgseq",iBridUtil.GetStringValue(this.Request.QueryString["prgseq"]));
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrlBuilder.ToString()));
	}
}