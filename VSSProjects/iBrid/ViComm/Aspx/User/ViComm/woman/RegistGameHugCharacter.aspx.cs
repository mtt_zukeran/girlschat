/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсEʸގ��s
--	Progaram ID		: RegistGameHugCharacter
--
--  Creation Date	: 2011.09.30
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistGameHugCharacter:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		if (!sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
			oParameters.Add("action",PwViCommConst.GameWomanLiveChatNoApplyAction.HUG);
			RedirectToGameDisplayDoc(ViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY,oParameters);
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
			string sResult = string.Empty;
			string sPreCooperationPoint = string.Empty;

			this.CheckGameRefusePartner(sPartnerUserSeq,sPartnerUserCharNo);
			this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

			if (!sPartnerUserSeq.Equals(string.Empty) && !sPartnerUserCharNo.Equals(string.Empty)) {
				sPreCooperationPoint = sessionWoman.userWoman.CurCharacter.gameCharacter.cooperationPoint.ToString();
				if (this.CheckOnlineStatusGame(sPartnerUserSeq,sPartnerUserCharNo)) {
					sResult = HugGameCharacter(sPartnerUserSeq,sPartnerUserCharNo);
				} else {
					sResult = PwViCommConst.HugGameCharacterResult.RESULT_CANNOT_STATUS;
				}
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,string.Format("ViewGameCharacterHugResult.aspx?result={0}&partner_user_seq={1}&partner_user_char_no={2}&pre_cooperation_point={3}",sResult,sPartnerUserSeq,sPartnerUserCharNo,sPreCooperationPoint)));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	private string HugGameCharacter(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult = string.Empty;

		using (GameCommunication oGameCommunication = new GameCommunication()) {
			sResult = oGameCommunication.HugGameCharacter(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.CurCharacter.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			this.CheckQuestClear(this.sessionWoman.userWoman.userSeq,this.sessionWoman.userWoman.curCharNo,PwViCommConst.GameQuestTrialCategoryDetail.HUG,ViCommConst.OPERATOR);
			this.CheckQuestClear(pPartnerUserSeq,pPartnerUserCharNo,PwViCommConst.GameQuestTrialCategoryDetail.PARTNER_HUG,ViCommConst.MAN);
		}

		return sResult;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo,string pTrialCategoryDetail,string pSexCd) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionWoman.site.siteCd,
				pUserSeq,
				pUserCharNo,
				PwViCommConst.GameQuestTrialCategory.HUG,
				pTrialCategoryDetail,
				out sQuestClearFlag,
				out sResult,
				pSexCd
			);
		}
	}
}
