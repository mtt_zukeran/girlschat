<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WriteRequest.aspx.cs" Inherits="ViComm_woman_WriteRequest" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlWrite" Runat="server">
			$PGM_HTML02;
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>ﾀｲﾄﾙ：(※42文字以内)</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>タイトル：(※42文字以内)</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<ibrid:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="84" Size="30" BreakAfter="true"></ibrid:iBMobileTextBox>

			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>ｶﾃｺﾞﾘ選択：</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>カテゴリ選択：</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			
			<mobile:SelectionList ID="lstCategory" Runat="server" SelectType="DropDown" BreakAfter="true">
				<Item Text="--ｶﾃｺﾞﾘ--" Value="" />
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>提案内容：</font><br />" />
			<tx:TextArea ID="txtText" runat="server" Row="10" Rows="6" Columns="24" EmojiPaletteEnabled="true" BrerakAfter="true" BreakBefore="false"></tx:TextArea>
			$PGM_HTML03;
		</mobile:Panel>
		<mobile:Panel ID="pnlConfirm" Runat="server">
			$PGM_HTML04;
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>ﾀｲﾄﾙ：</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>タイトル：</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<ibrid:iBMobileLabel ID="lblTitle" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>

			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>ｶﾃｺﾞﾘ選択：</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>カテゴリ選択：</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<ibrid:iBMobileLabel ID="lblCategory" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>
			<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>提案内容：</font><br />" />
			<ibrid:iBMobileLabel ID="lblText" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>
			$PGM_HTML05;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
