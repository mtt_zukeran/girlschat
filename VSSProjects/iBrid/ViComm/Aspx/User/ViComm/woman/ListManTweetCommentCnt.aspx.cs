/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやきコメントランキング一覧
--	Progaram ID		: ListManTweetCommentCnt
--
--  Creation Date	: 2013.03.27
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListManTweetCommentCnt:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			SetLstTerm();
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_TWEET_COMMENT_CNT,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void SetLstTerm() {
		string sTermSeq = iBridUtil.GetStringValue(Request.QueryString["termseq"]);

		ManTweetCommentTermSeekCondition oCondition = new ManTweetCommentTermSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.Finished = ViCommConst.FLAG_ON_STR;
		
		using (ManTweetCommentTerm oManTweetCommentTerm = new ManTweetCommentTerm()) {
			DataSet oDataSet = oManTweetCommentTerm.GetPageCollection(oCondition,1,999);

			foreach (DataRow dr in oDataSet.Tables[0].Rows) {
				DateTime dtEndDate = DateTime.Parse(dr["END_DATE"].ToString());
				lstTerm.Items.Add(new MobileListItem(dtEndDate.ToString("yyyy/MM/dd"),dr["TERM_SEQ"].ToString()));
			}
		}

		foreach (MobileListItem oItem in lstTerm.Items) {
			if (object.Equals(oItem.Value,sTermSeq)) {
				lstTerm.SelectedIndex = oItem.Index;
				break;
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sTermSeq = lstTerm.Selection.Value;

		if (!string.IsNullOrEmpty(sTermSeq)) {
			UrlBuilder oUrl = new UrlBuilder("ListManTweetCommentCnt.aspx",this.Request.QueryString);
			oUrl.RemoveParameter("pageno");
			oUrl.AddParameter("termseq",sTermSeq);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrl.ToString()));
		}
	}
}
