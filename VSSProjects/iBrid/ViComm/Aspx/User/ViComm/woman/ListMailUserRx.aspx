<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListMailUserRx.aspx.cs" Inherits="ViComm_woman_ListMailUserRx" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="cc1" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		$DATASET_LOOP_START10;
			<cc1:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END10;
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
