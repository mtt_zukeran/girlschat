/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 音声通話アプリ利用設定完了
--	Progaram ID		: SetupVoiceAppComplete
--  Creation Date	: 2015.03.24
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_SetupVoiceAppComplete:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.userWoman.useVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
				pnlVoiceAppOn.Visible = true;
				pnlVoiceAppOff.Visible = false;
			} else {
				pnlVoiceAppOn.Visible = false;
				pnlVoiceAppOff.Visible = true;
			}
		}
	}
}
