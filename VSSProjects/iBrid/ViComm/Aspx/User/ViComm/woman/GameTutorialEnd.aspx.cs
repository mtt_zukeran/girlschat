/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE����ري���
--	Progaram ID		: GameTutorialEnd
--
--  Creation Date	: 2012.02.06
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_GameTutorialEnd:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		//����رُ�ԍX�V
		using (GameCharacter oGameCharacter = new GameCharacter()) {
			oGameCharacter.SetupTutorialStatus(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				null
			);
		}

		sessionWoman.userWoman.CurCharacter.gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);

		string sResult = string.Empty;

		using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
			sResult = oGameIntroLog.LogGameIntro(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR
			);
		}

		if (sResult == PwViCommConst.LogGameIntroResult.RESULT_OK) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}
}