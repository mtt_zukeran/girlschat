/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���ߕK�v���эw��
--	Progaram ID		: ListGameClearTownUsedItemShortage
--
--  Creation Date	: 2012.02.22
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_woman_ListGameClearTownUsedItemShortage:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sStageSeq = iBridUtil.GetStringValue(this.Request.QueryString["stage_seq"]);
		string sTownSeq = iBridUtil.GetStringValue(this.Request.QueryString["town_seq"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sStageSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		rgxMatch = rgx.Match(sTownSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CLEAR_TOWN_USED_ITEM_SHORTAGE_LIST,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			string sRNum;
			if (this.getRNum(out sRNum)) {
				if (sRNum.Equals("P")) {
					int iPreTotalPrice = int.Parse(this.Request.Params["GOTOLINK_P"]);
					this.BuyClearTownUsedItemPlural(sStageSeq,sTownSeq,iPreTotalPrice);
				} else {
					string sBuyItemData = this.Request.Params["GOTOLINK_" + sRNum];
					string[] buyItemArr = sBuyItemData.Split('_');
					string sGameItemSeq = buyItemArr[0];
					string sBuyNum = buyItemArr[1];

					this.BuyClearTownUsedItem(sStageSeq,sTownSeq,sGameItemSeq,sBuyNum);
				}

			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	private void BuyClearTownUsedItem(string sStageSeq,string sTownSeq,string sGameItemSeq,string sBuyNum) {
		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		int iUsedItemTotalPrice = 0;

		string sPreMoney = this.sessionWoman.userWoman.CurCharacter.gameCharacter.gamePoint.ToString();
		string sPreAttackPower = this.GetAttackPower();
		string sPreDefencePower = this.GetDefencePower();

		GameItemSeekCondition oCondition = new GameItemSeekCondition();
		oCondition.SiteCd = this.sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.ItemSeq = sGameItemSeq;

		using (GameItem oGameItem = new GameItem()) {
			DataSet ds = oGameItem.GetPageCollection(oCondition,1,1);
			iUsedItemTotalPrice = int.Parse(ds.Tables[0].Rows[0]["PRICE"].ToString()) * int.Parse(sBuyNum);
		}

		if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.gamePoint < iUsedItemTotalPrice) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_BUY_USED_ITEM_FAIL_NO_MONEY);
		}

		string sResult = this.BuyGameItemExeCute(sGameItemSeq,sBuyNum);

		if (!sResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		string sBuyGameItemSeqData = sGameItemSeq;
		string sBuyGameItemNumData = sBuyNum;

		oParameters.Add("stage_seq",sStageSeq);
		oParameters.Add("town_seq",sTownSeq);
		oParameters.Add("get_game_item_seq",sBuyGameItemSeqData);
		oParameters.Add("get_game_item_count",sBuyGameItemNumData);
		oParameters.Add("get_record_count","1");
		oParameters.Add("pre_money",sPreMoney);
		oParameters.Add("pre_attack_power",sPreAttackPower);
		oParameters.Add("pre_defence_power",sPreDefencePower);

		RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_BUY_USED_ITEM_COMPLETE,oParameters);
	}

	private void BuyClearTownUsedItemPlural(string sStageSeq,string sTownSeq,int iPreTotalPrice) {
		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		DataSet ds;

		string sPreMoney = this.sessionWoman.userWoman.CurCharacter.gameCharacter.gamePoint.ToString();
		string sPreAttackPower = this.GetAttackPower();
		string sPreDefencePower = this.GetDefencePower();

		ClearTownUsedItemSeekCondition oCondition = new ClearTownUsedItemSeekCondition();
		oCondition.SiteCd = this.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.GameItemGetCd = PwViCommConst.GameItemGetCd.NOT_CHARGE;
		oCondition.StageSeq = sStageSeq;
		oCondition.TownSeq = sTownSeq;

		using (ClearTownUsedItem oClearTownUsedItem = new ClearTownUsedItem()) {
			ds = oClearTownUsedItem.GetPageCollectionUsedItemShortage(oCondition);
		}

		int iUsedItemTotalPrice = 0;

		if (ds.Tables[0].Rows.Count > 0) {
			List<string> oBuyGameItemSeqArr = new List<string>();
			List<string> oBuyNumArr = new List<string>();

			foreach (DataRow dr in ds.Tables[0].Rows) {
				iUsedItemTotalPrice += int.Parse(dr["USED_ITEM_TOTAL_PRICE"].ToString());
			}

			if (iPreTotalPrice != iUsedItemTotalPrice) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			} else if (this.sessionWoman.userWoman.CurCharacter.gameCharacter.gamePoint < iUsedItemTotalPrice) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_BUY_USED_ITEM_FAIL_NO_MONEY);
			}

			string sBuyGameItemSeq;
			string sBuyGameItemNum;

			foreach (DataRow dr in ds.Tables[0].Rows) {
				sBuyGameItemSeq = dr["GAME_ITEM_SEQ"].ToString();
				sBuyGameItemNum = dr["USED_ITEM_SHORTAGE"].ToString();
				this.BuyGameItemExeCute(sBuyGameItemSeq,sBuyGameItemNum);

				oBuyGameItemSeqArr.Add(sBuyGameItemSeq);
				oBuyNumArr.Add(sBuyGameItemNum);
			}

			string sBuyGameItemSeqData = string.Join("_",oBuyGameItemSeqArr.ToArray());
			string sBuyGameItemNumData = string.Join("_",oBuyNumArr.ToArray());

			oParameters.Add("stage_seq",sStageSeq);
			oParameters.Add("town_seq",sTownSeq);
			oParameters.Add("get_game_item_seq",sBuyGameItemSeqData);
			oParameters.Add("get_game_item_count",sBuyGameItemNumData);
			oParameters.Add("get_record_count",ds.Tables[0].Rows.Count.ToString());
			oParameters.Add("pre_money",sPreMoney);
			oParameters.Add("pre_attack_power",sPreAttackPower);
			oParameters.Add("pre_defence_power",sPreDefencePower);

			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_BUY_USED_ITEM_COMPLETE,oParameters);

		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}

	private bool getRNum(out string sRNum) {
		string index;

		for (int i = 1;i <= 10;i++) {
			index = i.ToString();
			if (this.Request.Params[ViCommConst.BUTTON_GOTO_LINK + "_" + index] != null) {
				sRNum = index;
				return true;
			}
		}

		if (this.Request.Params[ViCommConst.BUTTON_GOTO_LINK + "_P"] != null) {
			sRNum = "P";
			return true;
		} else {
			sRNum = null;
			return false;
		}
	}

	private string BuyGameItemExeCute(string sItemSeq,string sBuyNum) {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;

		string sResult = "";
		GameItem oGameItem = new GameItem();
		oGameItem.BuyGameItem(sSiteCd,sUserSeq,sUserCharNo,sItemSeq,sBuyNum,out sResult);
		return sResult;
	}

	private string GetAttackPower() {
		string sValue = "0";

		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sForceCount = this.sessionWoman.userWoman.CurCharacter.gameCharacter.attackMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameAttackPowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}

	private string GetDefencePower() {
		string sValue = "0";

		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sForceCount = this.sessionWoman.userWoman.CurCharacter.gameCharacter.defenceMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameDefencePowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}
}
