/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹް� ���Ѽ���� ��Ѿ�� ���шꗗ
--	Progaram ID		: ListGameItem
--
--  Creation Date	: 2011.08.11
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameItemSale:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_SALE_LIST,ActiveForm);
		} else {
			string RNum;
			if (this.getRNum(out RNum)) {
				string itemSeq = this.Request.Params["GOTOLINK_" + RNum];
				string buyNum = this.Request.Params["buyItemNum_" + RNum];
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,String.Format("ViewGameItemShop.aspx?item_seq={0}&buy_num={1}&sale={2}",itemSeq,buyNum,ViCommConst.FLAG_ON_STR)));
			}
		}
	}

	private bool getRNum(out string RNum) {
		string index;

		for (int i = 1;i <= 10;i++) {
			index = i.ToString();
			if (this.Request.Params[ViCommConst.BUTTON_GOTO_LINK + "_" + index] != null) {
				RNum = index;
				return true;
			}
		}
		RNum = null;
		return false;
	}
}
