﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistMinkuru.aspx.cs" Inherits="ViComm_woman_RegistMinkuru" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red" Visible="false"></cc1:iBMobileLabel>
		$PGM_HTML02;
		<cc1:iBMobileTextBox ID="txtHandleNm" runat="server" MaxLength="20" Size="15"></cc1:iBMobileTextBox>
		$PGM_HTML03;
		<cc1:iBMobileLabel ID="lblTagStart1" runat="server"></cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
		年
		<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
		月
		<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
		日
		<cc1:iBMobileLiteralText ID="tagLinesAge" runat="server" Text="$PGM_HTML08;">
		</cc1:iBMobileLiteralText>
		$PGM_HTML04;
		<mobile:SelectionList ID="lstLocation" Runat="server" BreakAfter="False">
			<Item Text="▼選択" Value="*" />
		</mobile:SelectionList>
        <cc1:iBMobileLiteralText ID="tagLinesLocation" runat="server" Text="$PGM_HTML08;">
		</cc1:iBMobileLiteralText>
		<br />
        $PGM_HTML05;
		<cc1:iBMobileLiteralText ID="tabBloodA" runat="server" Text="<input type=&quot;radio&quot; name=&quot;radioBlood&quot; value=&quot;1&quot; checked=&quot;checked&quot;>A" />
		<cc1:iBMobileLiteralText ID="tabBloodB" runat="server" Text="<input type=&quot;radio&quot; name=&quot;radioBlood&quot; value=&quot;2&quot;>B" />
		<cc1:iBMobileLiteralText ID="tabBloodO" runat="server" Text="<input type=&quot;radio&quot; name=&quot;radioBlood&quot; value=&quot;3&quot;>O" />
		<cc1:iBMobileLiteralText ID="tabBloodAB" runat="server" Text="<input type=&quot;radio&quot; name=&quot;radioBlood&quot; value=&quot;4&quot;>AB" />
		<cc1:iBMobileLiteralText ID="tagLinesBlood" runat="server" Text="$PGM_HTML08;" />      
        <br />
		$PGM_HTML06;
		<mobile:SelectionList ID="chkAgreement" Runat="server" SelectType="CheckBox">
			<Item Text="$PGM_HTML07;" Selected="true">
			</Item>
		</mobile:SelectionList>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
