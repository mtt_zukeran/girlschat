/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �f������Ǘ�
--	Progaram ID		: ViewBbsMovieConf
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web.UI.MobileControls;

public partial class ViComm_woman_ViewBbsMovieConf:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;

		if (!sMovieSeq.Equals("") && sPlayMovie.Equals("1")) {
			bDownload = Download(sMovieSeq);
		}

		if (bDownload == false) {
			if (!IsPostBack) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
				
				if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
					string sTitle = sessionWoman.GetBbsMovieValue("MOVIE_TITLE");
					string sDoc = sessionWoman.GetBbsMovieValue("MOVIE_DOC");
					string sSeq = sessionWoman.GetBbsMovieValue("MOVIE_SEQ");
					string sTypeSeq = sessionWoman.GetBbsMovieValue("CAST_MOVIE_ATTR_TYPE_SEQ");
					string sAttrSeq = sessionWoman.GetBbsMovieValue("CAST_MOVIE_ATTR_SEQ");
					string sObjRankingFlag = sessionWoman.GetBbsMovieValue("OBJ_RANKING_FLAG");

					if (sessionWoman.GetBbsMovieValue("PLANNING_TYPE").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
						txtTitle.Visible = false;
						txtDoc.Visible = false;
					}
					txtTitle.Text = iBridUtil.GetStringValue(sTitle).Replace("\r\n",string.Empty).Replace("\n",string.Empty).Replace("\r",string.Empty);
					txtDoc.Text = sDoc;
					ViewState["MOVIE_SEQ"] = sSeq;
					ViewState["ATTR_TYPE_SEQ"] = sTypeSeq;
					ViewState["ATTR_SEQ"] = sAttrSeq;

					foreach (MobileListItem item in lstRankingFlag.Items) {
						if (item.Value == sObjRankingFlag) {
							item.Selected = true;
							break;
						}
					}

					if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
						cmdDelete_Click(sender,e);
					}
					SetDropDownList(ViewState["ATTR_TYPE_SEQ"].ToString(),ViewState["ATTR_SEQ"].ToString());

				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListBbsMovieConf.aspx"));
				}
			} else {
				if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender,e);
				} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
					cmdDelete_Click(sender,e);
				}
			}
		}
	}

	private bool Download(string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		txtTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
		txtDoc.Text = HttpUtility.HtmlEncode(txtDoc.Text);

		string sSeq = ViewState["MOVIE_SEQ"].ToString();
		bool bUpdate = false;

		if (IsAvailableService(ViCommConst.RELEASE_DISABLE_BBS_MODIFY_LIST)) {
			bUpdate = true;
		} else if (lstSetUp.SelectedIndex == 0) {
			bUpdate = true;
		}

		if (bUpdate) {
			bool bOk = ValidText();
			if (bOk == false) {
				return;
			} else {
				string sAttrTypeSeq = "";
				string sAttrSeq = "";

				if (lstAttrSeq.Visible) {
					string[] sAttr = lstAttrSeq.Selection.Value.Split(':');
					sAttrTypeSeq = sAttr[0];
					sAttrSeq = sAttr[1];
				} else {
					sAttrTypeSeq = ViewState["ATTR_TYPE_SEQ"].ToString();
					sAttrSeq = ViewState["ATTR_SEQ"].ToString();
				}
				int iRankingFlag;
				if (!int.TryParse(lstRankingFlag.Items[lstRankingFlag.SelectedIndex].Value,out iRankingFlag)) {
					iRankingFlag = 1;
				}
				using (CastMovie oMovie = new CastMovie()) {
					string sMovieTitle = Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text);
					string sMovieDoc = Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text);
					if (sMovieTitle.Length > 30) {
						sMovieTitle = SysPrograms.Substring(sMovieTitle,30);
					}
					if (sMovieDoc.Length > 1000) {
						sMovieDoc = SysPrograms.Substring(sMovieDoc,1000);
					}
					oMovie.UpdateProfileMovie(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						sSeq,
						sMovieTitle,
						sMovieDoc,
						0,
						ViCommConst.ATTACHED_BBS,
						sAttrTypeSeq,
						sAttrSeq,
						iRankingFlag
					);
				}
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_UPDATED_CAST_BBS_MOVIE) + "&site=" + sessionWoman.site.siteCd);
			}
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ViewBbsMovieConf.aspx?pageno=" + iBridUtil.GetStringValue(Request.QueryString["pageno"]) + "&del=1") + "&site=" + sessionWoman.site.siteCd + "&unauth=" + iBridUtil.GetStringValue(Request.QueryString["unauth"]));
		}
	}

	protected void cmdDelete_Click(object sender,EventArgs e) {
		if (!this.IsDeletable()) {
			return;
		}
		string sTitle = sessionWoman.GetBbsMovieValue("MOVIE_TITLE");
		string sSeq = sessionWoman.GetBbsMovieValue("MOVIE_SEQ");
		sTitle = iBridUtil.GetStringValue(sTitle).Replace("\r\n",string.Empty).Replace("\n",string.Empty).Replace("\r",string.Empty);
		using (CastMovie oMovie = new CastMovie()) {
			oMovie.UpdateProfileMovie(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sSeq,
				sTitle,
				null,
				1,
				ViCommConst.ATTACHED_BBS,
				ViewState["ATTR_TYPE_SEQ"].ToString(),
				ViewState["ATTR_SEQ"].ToString());
		}
		DeleteMovie(sSeq);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_BBS_MOVIE) + "&site=" + sessionWoman.site.siteCd);
	}

	private void DeleteMovie(string pFileSeq) {
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string sFullPath = sessionWoman.site.webPhisicalDir +
								ViCommConst.MOVIE_DIRECTRY + "\\" + sessionWoman.site.siteCd + "\\Operator\\" + sessionWoman.userWoman.loginId + "\\" +
								string.Format("{0:D15}",int.Parse(pFileSeq));

			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + "s" + ViCommConst.MOVIE_FOODER)) {
				System.IO.File.Delete(sFullPath + "s" + ViCommConst.MOVIE_FOODER);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER2)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER2);
			}
			if (System.IO.File.Exists(sFullPath + ViCommConst.MOVIE_FOODER3)) {
				System.IO.File.Delete(sFullPath + ViCommConst.MOVIE_FOODER3);
			}
		}
	}

	private void SetDropDownList(string pCastMovieAttrTypeSeq,string pCastMovieAttrSeq) {
		if (IsAvailableService(ViCommConst.RELEASE_DISABLE_BBS_MODIFY_LIST)) {
			lstSetUp.Visible = false;
		} else {
			lstSetUp.Items.Add(new MobileListItem("�C������","1"));
			lstSetUp.Items.Add(new MobileListItem("�폜����","2"));
		}

		int iIdx = 0;

		using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
			DataSet ds = oCastMovieAttrTypeValue.GetList(sessionWoman.site.siteCd);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstAttrSeq.Items.Add(new MobileListItem(dr["CAST_MOVIE_ATTR_TYPE_NM"].ToString() + ":" + dr["CAST_MOVIE_ATTR_NM"].ToString(),dr["CAST_MOVIE_ATTR_TYPE_SEQ"].ToString() + ":" + dr["CAST_MOVIE_ATTR_SEQ"].ToString()));
				if (dr["CAST_MOVIE_ATTR_TYPE_SEQ"].ToString().Equals(pCastMovieAttrTypeSeq) && dr["CAST_MOVIE_ATTR_SEQ"].ToString().Equals(pCastMovieAttrSeq)) {
					lstAttrSeq.SelectedIndex = iIdx;
				}
				iIdx++;
			}
		}

		if (sessionWoman.site.bbsMovieAttrFlag != ViCommConst.FLAG_OFF) {
			lstAttrSeq.Visible = true;
			if (sessionWoman.site.supportChgObjCatFlag == ViCommConst.FLAG_OFF) {
				lstAttrSeq.Visible = false;
			}
		} else {
			lstAttrSeq.Visible = false;
		}

	}

	protected virtual bool ValidText() {
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		bool bOk = true;
		if (txtTitle.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		return bOk;
	}

	private bool IsDeletable() {
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		bool bOk = true;

		using (CastMovieAttrTypeValue oCastMovieAttrTypeValue = new CastMovieAttrTypeValue()) {
			if (!oCastMovieAttrTypeValue.IsDeletable(sessionWoman.site.siteCd,iBridUtil.GetStringValue(ViewState["ATTR_SEQ"]))) {
				this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_POSTER_DEL_NA);
				bOk = false;
			}
		}
		return bOk;
	}
}
