/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップオブジェ コメント削除
--	Progaram ID		: DeletePickupObjsComment
--
--  Creation Date	: 2013.05.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_DeletePickupObjsComment:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			tagDeleteConfirm.Visible = true;
			tagDeleteComplete.Visible = false;
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_PICKUP_OBJS_COMMENT,this.ActiveForm);
		} else {
			string sPickupId = iBridUtil.GetStringValue(this.Request.QueryString["pickupid"]);
			string sObjSeq = iBridUtil.GetStringValue(this.Request.QueryString["objseq"]);
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				string sSiteCd = sessionWoman.site.siteCd;
				string sUserSeq = sessionWoman.userWoman.userSeq;
				string sUserCharNo = sessionWoman.userWoman.curCharNo;
				string sCommentSeq = iBridUtil.GetStringValue(this.Request.QueryString["commentseq"]);

				string sResult = string.Empty;

				using (PickupObjsComment oPickupObjsComment = new PickupObjsComment()) {
					oPickupObjsComment.DeletePickupObjsComment(sSiteCd,sUserSeq,sUserCharNo,sCommentSeq,out sResult);
				}

				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					tagDeleteConfirm.Visible =false;
					tagDeleteComplete.Visible = true;
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}
			} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListPickupObjsComment.aspx?pickupid={0}&objseq={1}",sPickupId,sObjSeq)));
			}
		}
	}
}