/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�~������ؽēo�^�A����
--	Progaram ID		: RegistGameWantedItemMainte
--
--  Creation Date	: 2011.11.18
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistGameWantedItemMainte:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			string sGameItemSeq = iBridUtil.GetStringValue(Request.QueryString["game_item_seq"]);
			string sWantedItemSeq = iBridUtil.GetStringValue(Request.QueryString["wanted_item_seq"]);
			string sDelFlag = iBridUtil.GetStringValue(Request.QueryString["del_flag"]);
			int iViewOfflineRecCount = 0;
			string sResult = string.Empty;

			int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			using (ManGameCharacterFavorite oManGameCharacterFavorite = new ManGameCharacterFavorite()) {
				iViewOfflineRecCount = oManGameCharacterFavorite.GetFavoritViewStatusOfflineCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			}

			if (iCharacterOnlineStatus == ViCommConst.USER_TALKING || iCharacterOnlineStatus == ViCommConst.USER_OFFLINE || iViewOfflineRecCount > 0) {
				sResult = PwViCommConst.WantedItemManteResult.RESULT_CANNOT_STATUS;
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				oParameters.Add("result",sResult);
				oParameters.Add("del_flag",sDelFlag);
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_WANTED_ITEM_MAINTE_RESULT,oParameters);
			}

			if ((!sGameItemSeq.Equals(string.Empty) && sDelFlag.Equals(ViCommConst.FLAG_OFF_STR)) || (!sWantedItemSeq.Equals(string.Empty) && sDelFlag.Equals(ViCommConst.FLAG_ON_STR))) {
				if (sDelFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sGameItemSeq = GetGameItemSeq(sWantedItemSeq);
				}
				sResult = WantedItemMainte(sWantedItemSeq,sGameItemSeq,sDelFlag);
				if(!sResult.Equals(PwViCommConst.WantedItemManteResult.RESULT_NG)) {
					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("result",sResult);
					oParameters.Add("game_item_seq",sGameItemSeq);
					oParameters.Add("del_flag",sDelFlag);
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_WANTED_ITEM_MAINTE_RESULT,oParameters);
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	private string GetGameItemSeq(string pWantedItemSeq) {
		string sGameItemSeq = string.Empty;
		using (WantedItem oWantedItem = new WantedItem()) {
			sGameItemSeq = oWantedItem.GetGameItemSeq(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,pWantedItemSeq);
		}
		return sGameItemSeq;
	}

	private string WantedItemMainte(string pWantedItemSeq,string pGameItemSeq,string pDelFlag) {
		string sResult = string.Empty;

		using (WantedItem oWantedItem = new WantedItem()) {
			sResult = oWantedItem.WantedItemMainte(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR,
				pWantedItemSeq,
				pGameItemSeq,
				pDelFlag
			);
		}

		return sResult;
	}
}
