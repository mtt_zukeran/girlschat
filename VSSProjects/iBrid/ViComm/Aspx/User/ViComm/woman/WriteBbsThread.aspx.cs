﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド作成
--	Progaram ID		: WriteBbsThread
--
--  Creation Date	: 2011.04.08
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_WriteBbsThread:MobileBbsExWomanBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sback = iBridUtil.GetStringValue(Request.QueryString["back"]);
			if (sback.Equals(ViCommConst.FLAG_ON_STR)) {
				txtTitle.Text = sessionWoman.userWoman.bbsInfo.bbsThreadTitle;
				txtDoc.Text = sessionWoman.userWoman.bbsInfo.bbsThreadDoc;
				txtBbsThreadHandleNm.Text = sessionWoman.userWoman.bbsInfo.bbsThreadHandleNm;
			} else {
				txtBbsThreadHandleNm.Text = sessionWoman.userWoman.CurCharacter.handleNm;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {

		if (!ValidateText()) {
			return;
		} else {
			string sCheckHandleNmFlag = iBridUtil.GetStringValue(this.Request.Form["check_handle"]);
			
			sessionWoman.userWoman.bbsInfo.bbsThreadTitle = HttpUtility.HtmlEncode(txtTitle.Text);
			sessionWoman.userWoman.bbsInfo.bbsThreadDoc = HttpUtility.HtmlEncode(txtDoc.Text);
			if (sCheckHandleNmFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sessionWoman.userWoman.bbsInfo.bbsThreadHandleNm = HttpUtility.HtmlEncode(txtBbsThreadHandleNm.Text);
			}

			RedirectToMobilePage(sessionWoman.GetNavigateUrl("ConfirmWriteBbsThread.aspx"));
		}
	}

	private bool ValidateText() {
		bool bOk = true;
		string sNgWord = string.Empty;
		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}
		string sCheckHandleNmFlag = iBridUtil.GetStringValue(this.Request.Form["check_handle"]);

		lblErrorMessage.Text = string.Empty;
		
		if (sCheckHandleNmFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			if (txtBbsThreadHandleNm.Text.Equals(string.Empty)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_STATUS_BBS_THREAD_HANDLE_NM);
			}
			if (txtBbsThreadHandleNm.Text.Length > 20) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_STATUS_BBS_THREAD_HANDLE_LENGTH);
			}
			if (!sessionWoman.ngWord.VaidateDoc(txtBbsThreadHandleNm.Text,out sNgWord)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_STATUS_BBS_THREAD_HANDLE_NG);
			}
		}

		if (txtTitle.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_THREAD_TITLE_EMPTY);
		}
		if (txtTitle.Text.Length > 255) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_THREAD_TITLE_LENGTH);
		}
		if (!sessionWoman.ngWord.VaidateDoc(txtTitle.Text,out sNgWord)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(string.Format(ViCommConst.ERR_STATUS_BBS_THREAD_TITLE_NG,sNgWord));
		}
		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_THREAD_DOC_EMPTY);
		}
		if (txtDoc.Text.Length > 4000) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_THREAD_DOC_LENGTH);
		}
		if (!sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNgWord)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(string.Format(ViCommConst.ERR_STATUS_BBS_THREAD_DOC_NG,sNgWord));
		}

		return bOk;
	}
}
