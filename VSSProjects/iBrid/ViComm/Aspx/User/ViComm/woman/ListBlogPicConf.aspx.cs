/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ写真 一覧
--	Progaram ID		: ListBlogPicConf
--
--  Creation Date	: 2011.04.05
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListBlogPicConf : MobileBlogWomanBase {
	protected override bool NeedBlogRegistPermission { get { return true; } }

	protected const string PARAM_NM_OBJ_SEQ = "obj_seq";
	protected const string PARAM_NM_APPEND_MODE = "append";

	protected void Page_Load(object sender, EventArgs e) {
		string sObjSeq = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_OBJ_SEQ]);
		
		if (IsPostAction(ViCommConst.BUTTON_DELETE)) {
			this.Delete(sObjSeq);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
		sessionWoman.ControlList(
				this.Request,
				ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ,
				this.ActiveForm);
	}
	private void Delete(string pObjSeq) {
		using (BlogObj oBlogObj = new BlogObj()) {
			oBlogObj.DeleteBlogObj(pObjSeq);
		}
		
		NameValueCollection oNewParamList = new NameValueCollection();
		foreach (string sKey in this.Request.QueryString) {
			if (string.IsNullOrEmpty(sKey)) continue;

			if (sKey.ToLower().Equals("obj_seq")) continue;
			if (sKey.ToLower().Equals(ViCommConst.BUTTON_DELETE.ToLower())) continue;

			oNewParamList.Add(sKey, this.Request.QueryString[sKey]);
		}
		UrlBuilder oUrlBuilder = new UrlBuilder(sessionWoman.currentAspx, oNewParamList);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrlBuilder.ToString()));
	}
}
