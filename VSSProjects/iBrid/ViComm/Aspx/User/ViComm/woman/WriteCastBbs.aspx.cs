﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャスト掲示板書き込み
--	Progaram ID		: WriteCastBbs
--
--  Creation Date	: 2010.04.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_WriteCastBbs:MobileWomanPageBase {
	
	
	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				}
				else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);

		bool bOk = true;

		lblErrorMessage.Text = "";

		if (txtTitle.Text.Equals("")) {
			bOk = false;
			//タイトルを入力して下さい。 
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("")) {
			bOk = false;
			//本文を入力して下さい。 
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}
		if (iMaxLength != 0 && txtDoc.Text.Length > iMaxLength) {
			bOk = false;
			int iOverLength = txtDoc.Text.Length - iMaxLength;
			lblErrorMessage.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iOverLength.ToString(),iMaxLength.ToString());
		}

		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			string sNGWord;
			if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		
		string sSiteCd = sessionWoman.site.siteCd;
		string sUserSeq = sessionWoman.userWoman.userSeq;
		string sUserCharNo = sessionWoman.userWoman.curCharNo;

		using (Site oSite = new Site())
		using (CastBbs oCastBbs = new CastBbs()) {

			oSite.GetOne(sessionWoman.site.siteCd);

			// 書き込み件数のチェック
			if (oSite.castWriteBbsLimit >= 0) {
				int iWriteCnt = oCastBbs.GetWriteCntEachDay(sSiteCd, sUserSeq, sUserCharNo, DateTime.Now.Date);
				if (iWriteCnt >= oSite.castWriteBbsLimit) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_CAST_WRITE_BBS_LIMIT);
				}
			}

			// 書き込み間隔のチェック
			DateTime? oLastWriteDate = oCastBbs.GetLastWriteDate(sSiteCd, sUserSeq, sUserCharNo,false);
			if ((oLastWriteDate != null)
				&& (oLastWriteDate.Value.AddMinutes(oSite.castWriteIntervalMin) >= DateTime.Now)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_CAST_WRITE_BBS_INTERVAL_SEC);
			}
		}

        bOk = bOk & CheckOther();

		if (bOk == false) {
			return;
		} else {
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text));
			if (sDoc.Length > 1000) {
				sDoc = SysPrograms.Substring(sDoc,1000);
			}

			using (CastBbs oCastBbs = new CastBbs()) {
				oCastBbs.WriteBbs(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sTitle,sDoc);
			}

			// TODO:男性掲示板一覧に戻る処理を追加する
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WRITE_CAST_BBS_COMPLETE + "&" + Request.QueryString));

		}
	}

    protected virtual bool CheckOther()
    {
        return true;
    }
}
