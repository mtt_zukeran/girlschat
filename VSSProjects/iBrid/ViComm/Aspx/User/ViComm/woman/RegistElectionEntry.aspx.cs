/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ オーディションエントリー
--	Progaram ID		: RegistElectionEntry
--
--  Creation Date	: 2013.12.13
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistElectionEntry:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {

		if (!this.IsPostBack) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_ELECTION_PERIOD,ActiveForm);
		}
	}
}
