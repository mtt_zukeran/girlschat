<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListGamePaymentLogDetail.aspx.cs" Inherits="ViComm_woman_ListGamePaymentLogDetail" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<mobile:SelectionList ID="lstGamePaymentType" Runat="server" SelectType="DropDown" BreakAfter="False">
			<Item Value="" Text="全て"></Item>
			<Item Value="1" Text="動画ﾁｹｯﾄ(有料)使用"></Item>
			<Item Value="2" Text="ﾓｻﾞ消しﾂｰﾙ使用"></Item>
			<Item Value="3" Text="ﾌﾟﾚｾﾞﾝﾄGET"></Item>
			<Item Value="4" Text="ﾃﾞｰﾄ指名"></Item>
		</mobile:SelectionList><br />
		$PGM_HTML02;
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList"		runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail"	runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
