/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ポイント追加男性一覧
--	Progaram ID		: ListManAddPoint
--
--  Creation Date	: 2011.03.31
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListManAddPoint:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {


		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_MAN_ADD_POINT,
					ActiveForm);
		}
	}
}
