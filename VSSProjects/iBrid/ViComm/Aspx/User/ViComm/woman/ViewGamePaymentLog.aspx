<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewGamePaymentLog.aspx.cs" Inherits="ViComm_woman_ViewGamePaymentLog" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<br />
	    <mobile:SelectionList ID="lstStartYear" Runat="server" BreakAfter="false"></mobile:SelectionList>年<!--
	    --><mobile:SelectionList ID="lstStartMonth" Runat="server" BreakAfter="false"></mobile:SelectionList>月<!--
	    --><mobile:SelectionList ID="lstStartDay" Runat="server" BreakAfter="false"></mobile:SelectionList>日〜
	    <br />
	    <mobile:SelectionList ID="lstEndYear" Runat="server" BreakAfter="false"></mobile:SelectionList>年<!--
	    --><mobile:SelectionList ID="lstEndMonth" Runat="server" BreakAfter="false"></mobile:SelectionList>月<!--
	    --><mobile:SelectionList ID="lstEndDay" Runat="server" BreakAfter="false"></mobile:SelectionList>日
	    <br />
			<ibrid:iBMobileLiteralText ID="tagList"		runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail"	runat="server" Text="$PGM_HTML08;" />
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
