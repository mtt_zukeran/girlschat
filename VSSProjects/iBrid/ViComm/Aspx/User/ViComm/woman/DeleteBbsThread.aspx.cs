﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板スレッド削除確認
--	Progaram ID		: DeleteBbsThread
--
--  Creation Date	: 2011.04.08
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;

public partial class ViComm_woman_DeleteBbsThread:MobileBbsExWomanBase {

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,ActiveForm)) {
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBbsThread.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		using (BbsThread oBbsThread = new BbsThread()) {
			oBbsThread.DeleteBbsThread(	sessionWoman.site.siteCd,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]));
		}
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("bbsthreadseq",iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]));
		RedirectToDisplayDoc(ViCommConst.SCR_BBS_EX_DELETE_THREAD_COMPLETE,oParam);
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListBbsRes.aspx?bbsthreadseq={0}",iBridUtil.GetStringValue(Request.QueryString["bbsthreadseq"]))));
	}
}
