
/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾊﾞﾄﾙ相手詳細(応援ﾊﾞﾄﾙ)
--	Progaram ID		: ViewGameCharacterBattleSupport
--
--  Creation Date	: 2012.01.11
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameCharacterBattleSupport:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sSiteCd = this.sessionWoman.userWoman.CurCharacter.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.CurCharacter.userCharNo;
		string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["battle_log_seq"]);

		if (string.IsNullOrEmpty(sBattleLogSeq)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		using (GameCharacterBattleSupport oGameCharacterBattleSupport = new GameCharacterBattleSupport()) {
			if (!oGameCharacterBattleSupport.CheckExistsPartnerWoman(sSiteCd,sUserSeq,sUserCharNo,sBattleLogSeq)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}

			if (!oGameCharacterBattleSupport.CheckExistsSupportWoman(sSiteCd,sUserSeq,sUserCharNo,sBattleLogSeq)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}

		}

		if (!IsPostBack) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_START_SUPPORT,ActiveForm);
		}
	}
}