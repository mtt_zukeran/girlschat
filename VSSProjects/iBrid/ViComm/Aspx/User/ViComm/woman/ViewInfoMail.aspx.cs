﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お知らせメール詳細表示
--	Progaram ID		: ViewInfoMail
--
--  Creation Date	: 2010.05.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using iBridCommLib;
using ViComm;

public partial class ViComm_woman_ViewInfoMail:MobileWomanPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {

			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_RX_INFO_MAIL_BOX,ActiveForm)) {
				if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
					using (MailBox oMailBox = new MailBox()) {
						if (sessionWoman.GetMailCount() > 0) {
							oMailBox.UpdateMailDel(sessionWoman.GetMailValue("MAIL_SEQ"),ViCommConst.RX);
						}
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_INFO_MAIL + "&mail_delete_count=1"));
					}
				} else {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailReadFlag(sessionWoman.GetMailValue("MAIL_SEQ"));
					}
				}
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListInfoMailBox.aspx"));
			}
		}
	}
}
