/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 拒否られリスト一覧
--	Progaram ID		: ListCastRefuseMe
--
--  Creation Date	: 2011.04.07
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using System.Web.UI.MobileControls;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListCastRefuseMe:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionWoman.ClearCategory();
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_REFUSE_ME,
					ActiveForm);
		}
	}
	
}
