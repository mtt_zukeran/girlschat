/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳ジャンケン実行プレビュー
--	Progaram ID		: RegistYakyukenProgressCast
--
--  Creation Date	: 2013.05.07
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistYakyukenProgressCast:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sYakyukenGameSeq = iBridUtil.GetStringValue(Request.QueryString["ygameseq"]);
		string sManJyankenType = iBridUtil.GetStringValue(Request.QueryString["jtype"]);

		if (string.IsNullOrEmpty(sYakyukenGameSeq) || string.IsNullOrEmpty(sManJyankenType)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			string sCastJyankenType;
			string sWinStatus;
			string sYakyukenType;
			string sResult;

			using (YakyukenProgressCast oYakyukenProgressCast = new YakyukenProgressCast()) {
				oYakyukenProgressCast.RegistYakyukenProgressCast(
					sessionWoman.site.siteCd,
					sYakyukenGameSeq,
					sManJyankenType,
					out sCastJyankenType,
					out sWinStatus,
					out sYakyukenType,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				UrlBuilder oUrlBuilder = new UrlBuilder("PreviewYakyukenGame.aspx");
				oUrlBuilder.AddParameter("scrid","01");
				oUrlBuilder.AddParameter("ygameseq",sYakyukenGameSeq);
				oUrlBuilder.AddParameter("ytype",sYakyukenType);
				oUrlBuilder.AddParameter("winstatus",sWinStatus);
				oUrlBuilder.AddParameter("manjtype",sManJyankenType);
				oUrlBuilder.AddParameter("castjtype",sCastJyankenType);

				RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrlBuilder.ToString()));
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}
}
