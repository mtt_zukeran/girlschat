<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WriteRequestValue.aspx.cs" Inherits="ViComm_woman_WriteRequestValue" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlWrite" Runat="server">
			$PGM_HTML02;
			<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>▼評価</font><br />" />
			<mobile:SelectionList ID="rdoValue" Runat="server" SelectType="Radio" Alignment="Left" BreakAfter="true">
				<Item Text="良い" Value="1" Selected="true" />
				<Item Text="悪い" Value="2" Selected="false" />
			</mobile:SelectionList>

			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>▼ｺﾒﾝﾄの入力(※100文字以内)</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>▼コメントの入力(※100文字以内)</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<tx:TextArea ID="txtComment" runat="server" Row="10" Rows="4" MaxLength="200" Columns="24" EmojiPaletteEnabled="true" BrerakAfter="true" BreakBefore="false"></tx:TextArea>
			$PGM_HTML03;
		</mobile:Panel>
		<mobile:Panel ID="pnlConfirm" Runat="server">
			$PGM_HTML04;
			<ibrid:iBMobileLiteralText runat="server" Text="<font color='#0000ff'>▼評価</font><br />" />
			<ibrid:iBMobileLabel ID="lblValue" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>

			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>▼ｺﾒﾝﾄ</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="<div id=$DIV_IS_SMART_PHONE;>" />
				<ibrid:iBMobileLiteralText runat="server" Text="<br /><font color='#0000ff'>▼コメント</font><br />" />
			<ibrid:iBMobileLiteralText runat="server" BreakAfter="false" Text="</div>" />

			<ibrid:iBMobileLabel ID="lblComment" runat="server" BreakAfter="true"></ibrid:iBMobileLabel>
			<mobile:Panel ID="pnlConfirmNoComment" Runat="server">
				$PGM_HTML05;
			</mobile:Panel>
			<mobile:Panel ID="pnlConfirmComment" Runat="server">
				$PGM_HTML06;
			</mobile:Panel>
		</mobile:Panel>
		<mobile:Panel ID="pnlComplete" Runat="server">
			<mobile:Panel ID="pnlCompleteNoComment" Runat="server">
				$PGM_HTML07;
			</mobile:Panel>
			<mobile:Panel ID="pnlCompleteComment" Runat="server">
				$PGM_HTML08;
			</mobile:Panel>
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
