/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ(二次)Flash表示
--	Progaram ID		: ViewFlashMailLotterySecond
--
--  Creation Date	: 2015.01.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_woman_ViewFlashMailLotterySecond:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionWoman.carrier.Equals(ViCommConst.ANDROID)) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else if (sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else {
			string sNextUrl = iBridUtil.GetStringValue(this.Request.QueryString["next_url"]);

			string sFlashFileNm = string.Empty;
			sFlashFileNm = "mail_lottery_second.swf";

			if (string.IsNullOrEmpty(sNextUrl)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}

			string sFlashDir = Path.Combine(sessionWoman.site.webPhisicalDir,PwViCommConst.MAIL_LOTTERY_DIR);
			string sSwfFilePath = Path.Combine(sFlashDir,sFlashFileNm);
			byte[] bufTempSwf;

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!File.Exists(sSwfFilePath)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}

				bufTempSwf = FileHelper.GetFileBuffer(sSwfFilePath);
			}

			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("next_url",sNextUrl);
			oParam.Add("lose",ViCommConst.FLAG_OFF_STR);
			byte[] oSwfBuffer = GameFlashLiteHelper.RewriteSwfBuffer(bufTempSwf,oParam);
			GameFlashLiteHelper.OutputSwf(oSwfBuffer);
		}
	}
}
