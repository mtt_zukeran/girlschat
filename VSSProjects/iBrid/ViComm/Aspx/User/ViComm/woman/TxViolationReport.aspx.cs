﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 違反報告メール送信
--	Progaram ID		: TxViolationReport
--
--  Creation Date	: 2010.09.13
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
  yyyy/MM/dd  xxxxxxxxx
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using ViComm;

public partial class ViComm_woman_TxViolationReport:MobileWomanPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			DataRow dr;
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			if (sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),iRecNo,out dr) == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
			}

			// 違反報告内容のコンボボックス作成
			lstViolationContents.Items.Add(new MobileListItem("報告内容",""));
			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_VIOLATION_TYPE_WOMAN);
				foreach (DataRow row in ds.Tables[0].Rows) {
					lstViolationContents.Items.Add(new MobileListItem(row["CODE_NM"].ToString(),row["CODE_NM"].ToString()));
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = "";

		if (lstViolationContents.SelectedIndex == -1) {
			bOk = false;
			lblErrorMessage.Text += string.Format("{0}<br />", GetErrorMessage(ViCommConst.ERR_STATUS_VIOLATION_NOT_SELECTED));
		} else if (string.IsNullOrEmpty(lstViolationContents.Items[lstViolationContents.SelectedIndex].Value)) {
			bOk = false;
			lblErrorMessage.Text += string.Format("{0}<br />", GetErrorMessage(ViCommConst.ERR_STATUS_VIOLATION_NOT_SELECTED));
		}
		if (txtDoc.Text.Equals("")) {
			if (!IsAvailableService(ViCommConst.RELEASE_PERMIT_NO_BODY_REPORT)) {
				bOk = false;
				lblErrorMessage.Text += string.Format("{0}<br />",GetErrorMessage(ViCommConst.ERR_STATUS_DOC));
			}
		}

		if (bOk == false) {
			return;
		}

		// 絵文字変換処理 
		string sTitle = lstViolationContents.Items[lstViolationContents.SelectedIndex].Value;
		string sDoc = CreateDocument(txtDoc.Text);

		// 報告先のメールアドレス取得
		string sMailAddress;
		using (Site oSite = new Site()) {
			oSite.GetOne(sessionWoman.site.siteCd);
			sMailAddress = oSite.reportEmailAddr;
		}

		// 違反送信先にメールを送る
		SendMail(sMailAddress,sMailAddress,sTitle,sDoc);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_REPORT_MAIL_COMPLETED_WOMAN + "&" + Request.QueryString));
	}

	private string CreateDocument(string doc) {
		StringBuilder sb = new StringBuilder();
		sb.AppendFormat("・ID（コード）：{0}-{1}-{2}\r\n",sessionWoman.GetManValue("SITE_CD"),sessionWoman.GetManValue("LOGIN_ID"),sessionWoman.GetManValue("USER_CHAR_NO"));
		sb.AppendFormat("・ハンドル名：{0}\r\n",sessionWoman.GetManValue("HANDLE_NM"));
		sb.AppendFormat("・送信者：{0}({1}-{2})\r\n",sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].handleNm,sessionWoman.site.siteCd,sessionWoman.userWoman.loginId);
		sb.AppendFormat("・{0}頃送信\r\n",DateTime.Now.ToString("yyyy/MM/dd hh:mm"));
		sb.Append("通報内容\r\n");
		sb.Append(doc);

		return sb.ToString();
	}
}
