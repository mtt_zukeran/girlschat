﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 一括メール送信履歴一覧
--	Progaram ID		: ListBatchMailLog
--
--  Creation Date	: 2010.05.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using ViComm;

public partial class ViComm_woman_ListBatchMailLog:MobileWomanPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack)
		{
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm);
		}
	}
}
