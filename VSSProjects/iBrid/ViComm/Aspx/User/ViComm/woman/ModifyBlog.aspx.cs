/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾌﾞﾛｸﾞ更新
--	Progaram ID		: ModifyBlog
--
--  Creation Date	: 2011.04.04
--  Creater			: K.Itoh
--
**************************************************************************/

using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;


using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_woman_ModifyBlog : MobileBlogWomanBase {
	protected override bool NeedBlogRegistPermission { get { return true; } }
	protected override bool AllowRedirect2ModifyBlog { get { return false; } }
	
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);

		if (!IsPostBack) {
			this.txtBlogTitle.Text = sessionWoman.userWoman.CurCharacter.characterEx.blogTitle;
			this.txtBlogDoc.Text = sessionWoman.userWoman.CurCharacter.characterEx.blogDoc;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				if (!this.ValidateForModify()) return;
				this.Modify();
			}
		}
	}
	
	private void Modify(){

		string sBlogTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier, Mobile.EmojiToCommTag(sessionWoman.carrier, this.txtBlogTitle.Text)));
		string sBlogDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier, Mobile.EmojiToCommTag(sessionWoman.carrier, this.txtBlogDoc.Text)));

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BLOG_MAINTE");
			
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sessionWoman.site.siteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, sessionWoman.userWoman.userSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, sessionWoman.userWoman.curCharNo);
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.NUMBER, sessionWoman.userWoman.CurCharacter.characterEx.blogSeq);
			oDbSession.ProcedureInParm("pBLOG_TITLE", DbSession.DbType.VARCHAR2, sBlogTitle);
			oDbSession.ProcedureInParm("pBLOG_DOC", DbSession.DbType.VARCHAR2, sBlogDoc);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
		sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("BlogTop.aspx"));
		
	}
	
	private bool ValidateForModify(){
		bool bIsValid = true;
		StringBuilder oErrMsgBuilder = new StringBuilder();
		
		string sBlogTitle = this.txtBlogTitle.Text.Trim();
		string sBlogDoc = this.txtBlogDoc.Text.Trim();


		if(string.IsNullOrEmpty(sBlogTitle.Replace(Environment.NewLine,string.Empty))){
			bIsValid = false;
			oErrMsgBuilder.Append(GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_TITLE_EMPTY));
		}
		if (string.IsNullOrEmpty(sBlogDoc.Replace(Environment.NewLine, string.Empty))) {
			bIsValid = false;
			oErrMsgBuilder.Append(GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_DOC_EMPTY));
		}
		
		if(bIsValid){
			if (sBlogTitle.Length > 20) {
				bIsValid = false;
				oErrMsgBuilder.Append(string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_TITLE_LENGTH),"20文字"));				
			}

			if (sBlogDoc.Length > 100) {
				bIsValid = false;
				oErrMsgBuilder.Append(string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_BLOG_DOC_LENGTH), "100文字"));
			}
		}

		if (bIsValid) {
			string sNGWord;
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			
			if (!sessionWoman.ngWord.VaidateDoc(sBlogTitle + sBlogDoc, out sNGWord)) {
				bIsValid = false;
				oErrMsgBuilder.Append(string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD), sNGWord));

			}
		}
		
		this.lblErrorMessage.Text = oErrMsgBuilder.ToString();		
		return bIsValid;
	}
}
