/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �T�C�g�I��
--	Progaram ID		: SelectSite
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_SelectSite:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		sessionWoman.site.GetOne(iBridUtil.GetStringValue(Session["BASIC_SITE"]));
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
//			sessionWoman.userWoman.UpdateUserInfo(sessionWoman.userWoman.userSeq);

			sessionWoman.ControlList(
					Request,
					0,
					ActiveForm);

			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_BIND_SELECT_SITE)) {
					return;
				}
			}

			if (sessionWoman.GetSiteCount() == 1) {

				string sSiteCd = sessionWoman.GetSiteValue("SITE_CD");
				string sUserCharNo = sessionWoman.GetSiteValue("USER_CHAR_NO");

				DataSet dsCharacter;
				using (Cast oCast = new Cast()) {
					dsCharacter = oCast.GetLoginCharacterList(sSiteCd,sessionWoman.userWoman.userSeq);
				}
				if (dsCharacter.Tables[0].Rows.Count == 1) {
					if (!sSiteCd.Equals("") && !sUserCharNo.Equals("")) {
						if (sessionWoman.userWoman.characterList[sSiteCd + sUserCharNo].registIncompleteFlag == 1 || sessionWoman.userWoman.characterList[sSiteCd + sUserCharNo].profileResettingFlag == 1) {
							string[] sRequest = Request.Path.Split('/');
							if (!sRequest[sRequest.Length - 1].Equals("ModifyUserProfile.aspx")) {
								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ModifyUserProfile.aspx?site=" + sSiteCd + "&charno=" + sUserCharNo));
							}
						}
					}
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"UserTop.aspx?site=" + sSiteCd + "&charno=" + sUserCharNo + "&" + Request.QueryString.ToString()));
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"StartWaitting.aspx"));
	}
}
