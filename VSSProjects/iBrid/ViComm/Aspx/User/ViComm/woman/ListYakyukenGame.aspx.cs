/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �싅���Q�[���ꗗ
--	Progaram ID		: ListYakyukenGame
--
--  Creation Date	: 2013.04.29
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListYakyukenGame:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_YAKYUKEN_GAME,this.ActiveForm);
		}
	}
}
