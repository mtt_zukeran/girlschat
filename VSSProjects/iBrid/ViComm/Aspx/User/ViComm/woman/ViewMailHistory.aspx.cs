/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール履歴詳細表示
--	Progaram ID		: ViewMailHistory
--
--  Creation Date	: 2010.07.15
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.IO;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewMailHistory:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		bool bDownload = false;

		if (!IsPostBack) {
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);

			if (!sMovieSeq.Equals("") && sPlayMovie.Equals("1")) {
				bDownload = Download(sMovieSeq);
			}
			if (bDownload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

				if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_MAIL_HISTORY,ActiveForm)) {
					if (sessionWoman.GetMailValue("TXRX_TYPE").Equals(ViCommConst.RX)) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailReadFlag(sessionWoman.GetMailValue("MAIL_SEQ"));
						}
					}
				} else {
					NameValueCollection oQeury = new NameValueCollection(Request.QueryString);
					oQeury.Remove("pageno");
					oQeury.Remove("mailseq");
					oQeury.Remove("seekmode");
					UrlBuilder oUrlBuilder = new UrlBuilder(sessionWoman.GetNavigateUrl("ListMailHistory.aspx"));

					foreach (string sKey in oQeury.Keys) {
						if (sKey != null) {
							oUrlBuilder.AddParameter(sKey,HttpUtility.UrlEncodeUnicode(oQeury[sKey]));
						}
					}
					RedirectToMobilePage(oUrlBuilder.ToString());
				}
			}
		} else {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			if (Request.Params["cmdProtect"] != null) {
				cmdProtect_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.ControlList(Request,ViCommConst.INQUIRY_MAIL_HISTORY,ActiveForm);
		sessionWoman.errorMessage = "";

		if (!sessionWoman.GetMailValue("ATTACHED_OBJ_OPEN_FLAG").Equals("1") && sessionWoman.GetMailValue("TXRX_TYPE").Equals(ViCommConst.RX)) {
			if (sessionWoman.GetMailValue("ATTACHED_OBJ_TYPE").Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailAttaChedOpenFlag(sessionWoman.GetMailValue("MAIL_SEQ"));
					sessionWoman.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","1");
				}
			}
		}
	}

	private bool Download(string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;

		string sTxRxType = sessionWoman.GetMailValue("TXRX_TYPE");

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize;
		if (sTxRxType.Equals(ViCommConst.TX)) {
			lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		} else {
			lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.MAN,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		}
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				//受信時のみ処理を行う
				if (sTxRxType.Equals(ViCommConst.RX)) {
					if (sessionWoman.GetMailValue("ATTACHED_OBJ_TYPE").Equals("1") == false) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailAttaChedOpenFlag(sessionWoman.GetMailValue("MAIL_SEQ"));
						}
						sessionWoman.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","1");
					}
				}

				if (sTxRxType.Equals(ViCommConst.TX)) {
					MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				} else {
					MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.MAN);
				}

				return true;
			} else {
				return false;
			}
		}
	}

	protected void cmdProtect_Click(object sender,EventArgs e) {
		sessionWoman.ControlList(Request,ViCommConst.INQUIRY_MAIL_HISTORY,ActiveForm);
		int iProtectFlag = int.Parse(sessionWoman.GetMailValue("DEL_PROTECT_FLAG")) == 1 ? 0 : 1;
		string sMailSeq = sessionWoman.GetMailValue("MAIL_SEQ");
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sMailSeq,iProtectFlag);
			sessionWoman.SetMailValue("DEL_PROTECT_FLAG",iProtectFlag.ToString());
		}
	}
}

