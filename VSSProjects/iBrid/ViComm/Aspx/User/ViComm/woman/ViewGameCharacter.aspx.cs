/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・男性詳細
--	Progaram ID		: ViewGameCharacter.aspx
--
--  Creation Date	: 2011.09.27
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameCharacter:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

			if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}

			string sNaFlag = this.GetNaFlag(sPartnerUserSeq,sPartnerUserCharNo);
			if (!sNaFlag.Equals(ViCommConst.NaFlag.OK.ToString()) && !sNaFlag.Equals(ViCommConst.NaFlag.NO_AUTH.ToString())) {
				RedirectToGameDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_MAN);
			}

			this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

			if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["favorite_ins"])) {
				GameFavoriteMainte(sPartnerUserSeq,sPartnerUserCharNo,0);
			} else if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["favorite_del"])) {
				GameFavoriteMainte(sPartnerUserSeq,sPartnerUserCharNo,1);
			} else {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
				sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
			}
		}
	}

	private void GameFavoriteMainte(string pPartnerUserSeq,string pPartnerUserCharNo,int pDelFlag) {
		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		if (!sessionWoman.userWoman.CurCharacter.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
			oParameters.Add("action",PwViCommConst.GameWomanLiveChatNoApplyAction.ADD_FAVORITE);
			RedirectToGameDisplayDoc(ViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY,oParameters);
		}
		
		using (GameFavorite oGameFavorite = new GameFavorite()) {
			oGameFavorite.GameFavoriteMainte(
				sessionWoman.userWoman.CurCharacter.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.CurCharacter.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				pDelFlag
			);
		}
		
		oParameters.Add("del_flag",pDelFlag.ToString());
		oParameters.Add("partner_user_seq",pPartnerUserSeq);
		oParameters.Add("partner_user_char_no",pPartnerUserCharNo);
		RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_FAVORITE_COMPLETE,oParameters);
	}

	private string GetNaFlag(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sNaFlag = string.Empty;
		DataSet oDataSet;
		using (ManCharacter oManCharacter = new ManCharacter()) {
			oDataSet = oManCharacter.GetOne(sessionWoman.userWoman.CurCharacter.siteCd,pPartnerUserSeq,pPartnerUserCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sNaFlag = oDataSet.Tables[0].Rows[0]["NA_FLAG"].ToString();
		}
		return sNaFlag;
	}
}