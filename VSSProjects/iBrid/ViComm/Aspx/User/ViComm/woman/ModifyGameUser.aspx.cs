/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・登録情報入力
--	Progaram ID		: ModifyGameUser
--
--  Creation Date	: 2012.03.15
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ModifyGameUser:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			using (TempRegist oTempRegist = new TempRegist()) {
				if (oTempRegist.GetOneByUserSeq(sessionWoman.userWoman.userSeq)) {
					if (oTempRegist.registStatus.Equals(PwViCommConst.RegistStatus.REGIST_RX_EMAIL)) {
						if(iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("01")) {
							IDictionary<string,string> oParameters = new Dictionary<string,string>();
							oParameters.Add("gc_acc","1");
							RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_ID_PIC_MAIL,oParameters);
						} else {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_ID_PIC_MAIL);
						}
					}
				}
			}

			if (!sessionWoman.userWoman.castNm.Equals(string.Empty) && !sessionWoman.userWoman.tel.Equals(string.Empty) && !sessionWoman.userWoman.CurCharacter.birthday.Equals(string.Empty)) {
				string sRedirectPath = "ModifyGameUserProfile.aspx";
				if(!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["scrid"]))) {
					sRedirectPath = sRedirectPath + "?scrid=" + iBridUtil.GetStringValue(Request.QueryString["scrid"]);
				}
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sRedirectPath));
			}
			
			CreateList();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult;
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;
		
		if (txtTel.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
		} else {
			txtTel.Text = System.Web.HttpUtility.HtmlEncode(txtTel.Text);
			using (Man oMan = new Man()) {
				if (oMan.IsBlackUser(ViCommConst.NO_TYPE_TEL,txtTel.Text)) {
					//ﾌﾞﾗｯｸﾕｰｻﾞｰとして登録されています。ご利用になれません。
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
					bOk = false;
					return;
				}
			}

			if (!sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
				} else {
					using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
						if (oCastGameCharacter.IsTelDupli(sessionWoman.userWoman.userSeq,txtTel.Text)) {
							//携帯電話番号は既に登録されています。
							lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
							bOk = false;
						}
					}
				}
			}
		}
		
		if (txtCastNm.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NM_KANJI);
		}
		
		txtCastNm.Text = System.Web.HttpUtility.HtmlEncode(txtCastNm.Text);
		
		if ((lstYear.Selection.Value.Equals("")) || (lstMonth.Selection.Value.Equals("")) || (lstDay.Selection.Value.Equals(""))) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY);
		} else if (lstYear.Selection.Value.Length != 4) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_YYYY_NG);
		} else {
			if (bOk) {
				string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
				int iAge = ViCommPrograms.Age(sDate);
				if (iAge < 18) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18);
				} else if (iAge > 99) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
				}
			}
		}
		
		if (bOk == false) {
			return;
		}

		string sBirthday = "";
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
				sBirthday = string.Format("{0}/{1}/{2}",lstYear.Selection.Value,lstMonth.Selection.Value,lstDay.Selection.Value);
			}
		}

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			oCastGameCharacter.ModifyGameUserWoman(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				txtCastNm.Text,
				txtTel.Text,
				sBirthday,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.ModifyGameUserWoman.RESULT_OK)) {
			sessionWoman.userWoman.GetOne(sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword);
			sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);

			string sRedirectPath = "ModifyGameUserProfile.aspx";
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["scrid"]))) {
				sRedirectPath = sRedirectPath + "?scrid=" + iBridUtil.GetStringValue(Request.QueryString["scrid"]);
			}

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sRedirectPath));
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}
	
	protected virtual void CreateList() {
		int iYear = DateTime.Today.Year - 18;

		for (int i = 0;i < 85;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear -= 1;
		}
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}
}
