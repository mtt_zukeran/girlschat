/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���ۃ����ҏW
--	Progaram ID		: ModifyRefuseComment
--
--  Creation Date	: 2010.05.26
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ModifyRefuseComment:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
			DataRow dr;
			if (sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),iRecNo,out dr)) {

				ViewState["MAN_USER_SEQ"] = dr["USER_SEQ"].ToString();
				ViewState["MAN_USER_CHAR_NO"] = dr["USER_CHAR_NO"].ToString();

				using (Refuse oRefuse = new Refuse()) {
					if (oRefuse.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,ViCommConst.MAIN_CHAR_NO,ViewState["MAN_USER_SEQ"].ToString(),ViewState["MAN_USER_CHAR_NO"].ToString())) {
						txtAreaRefuseComment.Text = oRefuse.refuseComment;
					}
				}
				pnlFoundDate.Visible = true;
				pnlNotFoundDate.Visible = false;
			} else {
				pnlFoundDate.Visible = false;
				pnlNotFoundDate.Visible = true;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);
		if (iMaxLength < 1) {
			iMaxLength = 300;
		}
		if (txtAreaRefuseComment.Text.Length > iMaxLength) {
			lblModifyComplate.Text = GetErrorMessage(ViCommConst.ERR_STATUS_LENGTH_OVER_REF_MEMO);
		} else {
			using (Refuse oRefuse = new Refuse()) {
				oRefuse.RefuseMainte(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					ViCommConst.OPERATOR,
					ViewState["MAN_USER_SEQ"].ToString(),
					ViewState["MAN_USER_CHAR_NO"].ToString(),
					"9",
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtAreaRefuseComment.Text)),
					0);
			}
			lblModifyComplate.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MODIFY_COMMENT_COMPLATE);
		}
	}
}
