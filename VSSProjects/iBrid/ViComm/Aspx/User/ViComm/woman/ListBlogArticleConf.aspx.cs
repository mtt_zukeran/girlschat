/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ投稿 確認一覧
--	Progaram ID		: ListBlogArticleConf
--
--  Creation Date	: 2011.04.07
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListBlogArticleConf : MobileBlogWomanBase {
	protected override bool NeedBlogRegistPermission { get { return true; } }
	
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
		sessionWoman.ControlList(
				this.Request,
				ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE,
				this.ActiveForm);

	}
}
