<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ModifyBlogArticle.aspx.cs" Inherits="ViComm_woman_ModifyBlogArticle" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile"	%>
<%@ Register TagPrefix="ibrid"	Namespace="MobileLib"						Assembly="MobileLib"			%>
<%@ Register TagPrefix="ibrid"	TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="ViComm" %>
<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		string sObjSeq = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_OBJ_SEQ]);
		string sBlogArticleSeq = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_BLOG_ARTICLE_SEQ]);

		if (string.IsNullOrEmpty(sObjSeq) && string.IsNullOrEmpty(sBlogArticleSeq)) {
			throw new ArgumentException();
		}
		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sBlogArticleSeq)) {
				if (!string.IsNullOrEmpty(sObjSeq)) {
					AppendObj(sBlogArticleSeq,sObjSeq);
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ModifyBlogArticle.aspx?blog_article_seq={0}",sBlogArticleSeq)));
				}
				// 編集モード
				this.DisplayEditMode(sBlogArticleSeq);
			} else {
				// 新規モード
				this.DisplayNewMode(sObjSeq);
			}
		} else {
			if (IsPostAction(ViCommConst.BUTTON_GOTO_MOVIE)) {
				if (!this.ValidateForModfiy())
					return;
				sBlogArticleSeq = this.Modify();
				if (string.IsNullOrEmpty(sBlogArticleSeq))
					return;
				RedirectToMobilePage(sessionObj.GetNavigateUrl(string.Format("ListBlogMovieUnused.aspx?append_blog_article_seq={0}",sBlogArticleSeq)));
			} else if (IsPostAction(ViCommConst.BUTTON_GOTO_PIC)) {
				if (!this.ValidateForModfiy())
					return;
				sBlogArticleSeq = this.Modify();
				if (string.IsNullOrEmpty(sBlogArticleSeq))
					return;
				RedirectToMobilePage(sessionObj.GetNavigateUrl(string.Format("ListBlogPicUnused.aspx?append_blog_article_seq={0}",sBlogArticleSeq)));
			} else if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				if (!this.ValidateForModfiy())
					return;
				sBlogArticleSeq = this.Modify();
				if (string.IsNullOrEmpty(sBlogArticleSeq))
					return;
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ViewBlogArticleConf.aspx?blog_article_seq={0}",sBlogArticleSeq)));
			}
		}

		sessionWoman.ControlList(this.Request,ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ,this.ActiveForm);
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>    
		$PGM_HTML02;
		<ibrid:iBMobileTextBox ID="txtBlogArticleTitle" runat="server"  BreakAfter="true" Size="25" MaxLength="50" ></ibrid:iBMobileTextBox>
		$PGM_HTML03;
		<ibrid:TextArea ID="txtBlogArticleDoc" runat="server" BreakBefore="false" BrerakAfter="true" Columns="20" Rows="6">
		</ibrid:TextArea>		
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList"		runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail"	runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;		
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
