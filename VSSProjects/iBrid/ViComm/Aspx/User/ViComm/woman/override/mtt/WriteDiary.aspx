<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../WriteDiary.aspx.cs" Inherits="ViComm_woman_WriteDiary" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">�N</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"> </mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">��</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="true"> </mobile:SelectionList>
		<mobile:Panel ID="pnlCastDiaryAttrValue" Runat="server" visible="False">
		    <cc1:iBMobileLabel ID="lblDiaryAttrValue" runat="server" Text='�ú��' ></cc1:iBMobileLabel>
		    <mobile:SelectionList ID="lstCastDiaryAttrValue" Runat="server" ></mobile:SelectionList>
		</mobile:Panel>
		<cc1:iBMobileLabel ID="lblTitle" runat="server" Visible="false">����</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24" EmojiPaletteEnabled="true" Visible="false" Text="DEFAULT TITLE"></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblDoc" runat="server" Visible="false">�{��</cc1:iBMobileLabel>
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		<br />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
