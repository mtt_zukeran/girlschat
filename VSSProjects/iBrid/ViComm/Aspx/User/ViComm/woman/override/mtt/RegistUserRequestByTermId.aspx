﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../RegistUserRequestByTermId.aspx.cs"
    Inherits="ViComm_woman_RegistUserRequestByTermId" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>

<script runat="server">
    protected override bool CheckOther() {
        bool bResult = true;
        System.Text.Encoding enc = System.Text.Encoding.GetEncoding(932);
        if (enc.GetByteCount(txtCastNm.Text) > 24) {
            this.lblErrorMessage.Text += string.Format(GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"氏名(漢字)");
            bResult = false;
        }
        if (enc.GetByteCount(txtCastKanaNm.Text) > 30) {
            this.lblErrorMessage.Text += string.Format(GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"氏名(ﾌﾘｶﾞﾅ)");
            bResult = false;
        }
        return bResult;
    }

    protected override void CreateList() {
        int iYear = DateTime.Today.Year - 18;

        for (int i = 0; i < 28; i++) {
            lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"), iYear.ToString("d4")));
            iYear -= 1;
        }
        lstYear.SelectedIndex = 7;
        for (int i = 1; i <= 12; i++)
        {
            lstMonth.Items.Add(new MobileListItem(i.ToString("d2"), i.ToString("d2")));
        }
        for (int i = 1; i <= 31; i++)
        {
            lstDay.Items.Add(new MobileListItem(i.ToString("d2"), i.ToString("d2")));
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <mobile:Form ID="frmMain" Runat="server">
		 $PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
	    <cc1:iBMobileLiteralText ID="tagDivS01" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<cc1:iBMobileLiteralText ID="DivS01" runat="server" Text="<font size='1'>" />
		<cc1:iBMobileLiteralText ID="tagDivS02" runat="server" BreakAfter="false" Text="</div>" />
		<cc1:iBMobileLabel ID="lblTel" runat="server" BreakAfter="true">$xE688;電話番号</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true"
            BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagTel" runat="server" Text="<br/>" />
		<cc1:iBMobileLiteralText ID="tagDivS03" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<cc1:iBMobileLiteralText ID="DivE01" runat="server" Text="</font>" />
		<cc1:iBMobileLiteralText ID="DivS02" runat="server" Text="<font size='1'>" />
		<cc1:iBMobileLiteralText ID="tagDivS04" runat="server" BreakAfter="false" Text="</div>" />
		<cc1:iBMobileLabel ID="lblCastNm" runat="server" BreakAfter="true">$xE684;氏名(漢字)</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtCastNm" runat="server" MaxLength="24" Size="18" BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagNm" runat="server" Text="<br/>" />
		<cc1:iBMobileLiteralText ID="tagDivS05" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<cc1:iBMobileLiteralText ID="DivE02" runat="server" Text="</font>" />
		<cc1:iBMobileLiteralText ID="DivS04" runat="server" Text="<font size='1'>" Visible="false" />
		<cc1:iBMobileLiteralText ID="tagDivS06" runat="server" BreakAfter="false" Text="</div>" />
		<cc1:iBMobileLabel ID="lblCastKanaNm" runat="server" BreakAfter="true" Visible="false">$xE684;氏名(ﾌﾘｶﾞﾅ)</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtCastKanaNm" runat="server" MaxLength="30" Size="18" BreakAfter="true"
            Visible="false"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagKanaNm" runat="server" Text="<br/>" Visible="false" />
		<cc1:iBMobileLiteralText ID="tagDivS07" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<cc1:iBMobileLiteralText ID="DivE04" runat="server" Text="</font>" Visible="false" />
		<cc1:iBMobileLiteralText ID="DivS03" runat="server" Text="<font size='1'>" />
		<cc1:iBMobileLiteralText ID="tagDivS08" runat="server" BreakAfter="false" Text="</div>" />
		<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">$xE686;生年月日</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">年</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">月</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblDay" runat="server">日</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagBirthday" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblQuestionDoc" runat="server" Visible="false">$xE689;ご質問事項</cc1:iBMobileLabel>
		<tx:TextArea ID="txtQuestionDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true"
            Rows="6" Columns="24" MaxLength="300" Visible="false"></tx:TextArea>
		<cc1:iBMobileLiteralText ID="tagDivS09" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<cc1:iBMobileLiteralText ID="DivE03" runat="server" Text="</font>" />
		<cc1:iBMobileLiteralText ID="tagDivS10" runat="server" BreakAfter="false" Text="</div>" />
		<mobile:Panel ID="pnlAgreement" Runat="server">
			<mobile:SelectionList ID="chkAgreement" Runat="server" SelectType="CheckBox">
                <Item Text="$PGM_HTML03;"></Item>
            </mobile:SelectionList>
			$PGM_HTML04;
		</mobile:Panel>
		$PGM_HTML02;
		$PGM_HTML09;
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="12" Numeric="true"
            Visible="false"></cc1:iBMobileTextBox>
	</mobile:Form>
</body>
</html>
