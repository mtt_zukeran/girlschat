﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ModifyCastBank.aspx.cs" Inherits="ViComm_woman_ModifyCastBank" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<cc1:iBMobileLabel ID="lblBankAccountHolderNm" runat="server">$xE741;口座名義</cc1:iBMobileLabel>
		$PGM_HTML07;
		<cc1:iBMobileTextBox ID="txtBankAccountHolderNm" runat="server" MaxLength="40" Size="20"></cc1:iBMobileTextBox>
		<br />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblBankNm" runat="server">$xE741;銀行</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtBankNm" runat="server" MaxLength="20" Size="20"></cc1:iBMobileTextBox>
		<mobile:Panel ID="pnlBankCd" Runat="server">
			<br />
			<cc1:iBMobileLabel ID="lblBankCd" runat="server">金融機関ｺｰﾄﾞ</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtBankCd" runat="server" MaxLength="4" Size="4"></cc1:iBMobileTextBox>
		</mobile:Panel>
		$PGM_HTML02;
		<br />
		<cc1:iBMobileLabel ID="lblBankOfficeNm" runat="server">$xE741;支店名</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtBankOfficeNm" runat="server" MaxLength="20" Size="20"></cc1:iBMobileTextBox>
		$PGM_HTML03;
		<br />
		<cc1:iBMobileLabel ID="lblBankOfficeKanaNm" runat="server">$xE741;支店名ﾌﾘｶﾞﾅ</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtBankOfficeKanaNm" runat="server" MaxLength="32" Size="20" ZenkakuKana="true"></cc1:iBMobileTextBox>
		<mobile:Panel ID="pnlBankOfficeCd" Runat="server">
			<br />
			<cc1:iBMobileLabel ID="lblBankOfficeCd" runat="server">支店ｺｰﾄﾞ</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtBankOfficeCd" runat="server" MaxLength="3" Size="3"></cc1:iBMobileTextBox>
		</mobile:Panel>
		$PGM_HTML04;
		<br />
		<cc1:iBMobileLabel ID="lblBankAccountType" runat="server">$xE741;口座種別</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtBankAccountType" runat="server" MaxLength="4" Size="10"></cc1:iBMobileTextBox>
		<mobile:SelectionList ID="rdoBankAccountType" Runat="server" SelectType="Radio" Alignment="Left">
			<Item Text="普通" Value="1" Selected="false" />
			<Item Text="当座" Value="2" Selected="false" />
		</mobile:SelectionList>
		$PGM_HTML05;
		<br />
		<cc1:iBMobileLabel ID="lblBankAccountNo" runat="server">$xE741;口座番号</cc1:iBMobileLabel>
		$PGM_HTML06;
		<cc1:iBMobileTextBox ID="txtBankAccountNo" runat="server" MaxLength="7" Size="20" Numeric="true"></cc1:iBMobileTextBox>
		<br />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
