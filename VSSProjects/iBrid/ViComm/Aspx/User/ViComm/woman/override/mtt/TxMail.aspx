<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../TxMail.aspx.cs" Inherits="ViComm_woman_TxMail" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<script runat="server">
    protected override bool CheckOther() {
        if (System.Text.Encoding.GetEncoding(932).GetByteCount(txtTitle.Text) > 30) {
            this.lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_LENGTH_OVER_TITLE);
            return false;
        }
        return true;
    }

    protected override void IsVisibleTitle() {
        if (ViewState["LAY_OUT_TYPE"].ToString().Equals(ViComm.ViCommConst.TxMailLayOutType.MEETING_MAIL)) {
            txtTitle.Visible = false;
            lblTitle.Visible = false;
            lblDoc.Visible = false;
            lblDocTitle.Visible = true;
        } else {
			if (IsAvailableService(ViComm.ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2)) {
				txtTitle.Visible = false;
				lblTitle.Visible = false;
			} else {
				txtTitle.Visible = true;
				lblTitle.Visible = true;
			}
			
            lblDoc.Visible = true;
            lblDocTitle.Visible = false;
        }
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		<!-- 宛先が一人の時に表示されるヘッダー -->
		<cc1:iBMobileLiteralText ID="tagHeaderSingle" runat="server" Text="$PGM_HTML01;" />
		<cc1:iBMobileLiteralText ID="tagToSingle" runat="server" Text="$PGM_HTML02;" />
		<!-- 宛先が複数の時に表示されるヘッダー -->
		<cc1:iBMobileLiteralText ID="tagHeaderBatch" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLiteralText ID="tagToBatch" runat="server" Text="$PGM_HTML04;" />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblManNbr" runat="server"></cc1:iBMobileLabel>
		<mobile:Panel ID="pnlTemplate" Runat="server" BreakAfter="False">
			<cc1:iBMobileLabel ID="lblTemplate" runat="server">$xE6CE;ｽﾀﾝﾊﾞｲ種別</cc1:iBMobileLabel>
            <mobile:SelectionList ID="lstTemplate" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<cc1:iBMobileLabel ID="lblTitle" runat="server" Text="件名：<font color=red>※全角15文字まで</font>" Visible="false"></cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="30" Size="24" EmojiPaletteEnabled="true" ></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblDoc" runat="server" Visible="false">本文：</cc1:iBMobileLabel>
        <cc1:iBMobileLiteralText ID="lblDocTitle" runat="server" Text="<font color='#FF0099'>ﾒｯｾｰｼﾞ</font>$xE6EF;"></cc1:iBMobileLiteralText>
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="9" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		$PGM_HTML05;
		<cc1:iBMobileLiteralText ID="tagAttachedFileOption" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML08;
		<!-- 宛先が一人の時に表示されるフッター -->
		<cc1:iBMobileLiteralText ID="tagHooterSingle" runat="server" Text="$PGM_HTML07;" />
		<!-- 宛先が複数の時に表示されるフッター -->
		<cc1:iBMobileLiteralText ID="tagHooterBatch" runat="server" Text="$PGM_HTML09;" />
	</mobile:Form>
</body>
</html>
