<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ModifyUserProfile.aspx.cs" Inherits="ViComm_woman_ModifyUserProfile" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<%@ Import Namespace="System.Data" %>

<script runat="server">
    protected override void DisplayInitData() {
        base.DisplayInitData();

        lstYear.Items.Clear();
        int iYear = DateTime.Today.Year - 10;


		string[] sBirthday;
        
        using (Cast oCast = new Cast()) {
			sBirthday = oCast.GetBirthDayByUserSeq(sessionWoman.userWoman.userSeq).ToString().Split('/');
        }

        bool bSelectedIndex = false;
        for (int i = 0; i < 28; i++) {
            lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"), iYear.ToString("d4")));
            if ((sBirthday.Length == 3) && (sBirthday[0].Equals(iYear.ToString("d4")))) {
                lstYear.SelectedIndex = i;
                bSelectedIndex = true;
            }
            iYear -= 1;
        }


		for (int i = 1;i <= 12;i++) {
			if ((sBirthday.Length == 3) && (sBirthday[1].Equals(i.ToString("d2")))) {
				lstMonth.SelectedIndex = i - 1;
			}
		}
		for (int i = 1;i <= 31;i++) {
			if ((sBirthday.Length == 3) && (sBirthday[2].Equals(i.ToString("d2")))) {
				lstDay.SelectedIndex = i - 1;
			}
		}

        if (!bSelectedIndex) {
            if (sBirthday.Length == 3) {
                for (int i = 0; i < 85; i++) {
                    lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"), iYear.ToString("d4")));
                    if ((sBirthday.Length == 3) && (sBirthday[0].Equals(iYear.ToString("d4")))) {
                        lstYear.SelectedIndex = i;
                        bSelectedIndex = true;
                        break;
                    }
                    iYear -= 1;
                }
            } else {
                lstYear.SelectedIndex = 7;
                lstMonth.SelectedIndex = 0;
                lstDay.SelectedIndex = 0;
            }
        }

		bool bBustSizeDisplay = true;
		
		DataSet oDataSet = null;
		
		using (Cast oCast = new Cast()) {
			oDataSet = oCast.GetIdolAttrSeq(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
		}

        for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
				iBMobileTextBox txtUserWomanItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserWomanItem{0}",i + 1)) as iBMobileTextBox;
				switch (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm) {
					case "愛称":
						txtUserWomanItem.Size = 20;
						break;
					case "職業":
						txtUserWomanItem.Size = 25;
						break;
					case "身長":
					case "ﾊﾞｽﾄ":
					case "ｳｴｽﾄ":
					case "ﾋｯﾌﾟ":
						txtUserWomanItem.Size = 4;
						break;
					case "趣味":
					case "特技":
						txtUserWomanItem.Size = 25;
						break;
				}
			} else {
				SelectionList lstUserWomanItem = (SelectionList)frmMain.FindControl(string.Format("lstUserWomanItem{0}",i + 1)) as SelectionList;
				switch (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm) {
					
					case "性別":
						if (lstUserWomanItem.SelectedIndex == -1) {
							string sIdolSexAttrSeq = string.Empty;

							sIdolSexAttrSeq = oDataSet.Tables[0].Rows[0]["IDOL_SEX_ATTR_SEQ"].ToString();

							if (!string.IsNullOrEmpty(sIdolSexAttrSeq)) {
								foreach (MobileListItem oItem in lstUserWomanItem.Items) {
									if (oItem.Value.Equals(sIdolSexAttrSeq)) {
										oItem.Selected = true;
										break;
									}
								}
							}

							if (lstUserWomanItem.SelectedIndex == -1) {
								foreach (MobileListItem oItem in lstUserWomanItem.Items) {
									if (oItem.Text.Equals("女性")) {
										oItem.Selected = true;
										break;
									}
								}
							}
						}

						lblSex.Text = lstUserWomanItem.Selection.Text;

						if (lstUserWomanItem.Selection.Text.Equals("男性")) {
							bBustSizeDisplay = false;
						}
						break;
					case "志望ｼﾞｬﾝﾙ":
						if (lstUserWomanItem.SelectedIndex == -1) {
							string sIdolWishCastTypeAttrSeq = string.Empty;

							sIdolWishCastTypeAttrSeq = oDataSet.Tables[0].Rows[0]["IDOL_WISH_CAST_TYPE_ATTR_SEQ"].ToString();

							if (!string.IsNullOrEmpty(sIdolWishCastTypeAttrSeq)) {
								foreach (MobileListItem oItem in lstUserWomanItem.Items) {
									if (oItem.Value.Equals(sIdolWishCastTypeAttrSeq)) {
										oItem.Selected = true;
										break;
									}
								}
							}
						}
						break;
				}
			}
        }

		if (bBustSizeDisplay == false) {
	        for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
				if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm.Equals("ﾊﾞｽﾄｻｲｽﾞ")) {
					System.Web.UI.MobileControls.Panel pnlUserWoman = (System.Web.UI.MobileControls.Panel)frmMain.FindControl(string.Format("pnlUserWoman{0}",i + 1)) as System.Web.UI.MobileControls.Panel;
					pnlUserWoman.Visible = false;

					SelectionList lstUserWomanItem = (SelectionList)frmMain.FindControl(string.Format("lstUserWomanItem{0}",i + 1)) as SelectionList;

					foreach (MobileListItem oItem in lstUserWomanItem.Items) {
						if (!oItem.Text.Equals("未選択")) {
							oItem.Selected = true;
							break;
						}
					}
				}
			}
		}

        if (this.lstYear.Selection == null || this.lstMonth.Selection == null || this.lstDay.Selection == null) {
            return;
        }
	}

	protected override bool CheckInputValue() {

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}
		string sNGWord = string.Empty;

		bool bOk = true;
		okMask = 0;
		Int64 iMaskValue = 1;

		int iMaskCount = int.Parse(ViewState["OK_PLAY_RECORD_COUNT"].ToString());
		for (int i = 0;i < iMaskCount;i++) {
			SelectionList chkOk = (SelectionList)pnlOkList.FindControl(string.Format("chkOK{0}",i)) as SelectionList;
			if (chkOk.Items[0].Selected) {
				okMask = okMask + iMaskValue;
			}
			iMaskValue = iMaskValue << 1;
		}

		lblErrorMessage.Text = "";

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += "芸名を入力して下さい。<br />";
			} else {
				//芸名に絵文字が含まれる場合にエラーメッセージを表示させる
				string sCheckHandleNm = Mobile.EmojiToCommTag(sessionWoman.carrier,txtHandelNm.Text);
				if (!txtHandelNm.Text.Equals(sCheckHandleNm)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_HANDLE_NM_NG);
				}
			}
			if (sessionWoman.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += "芸名に禁止語句が含まれています。<br />";
			}
		}

		if (txtTel.Visible) {
			if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
			} else {
				using (UserWoman oWoman = new UserWoman()) {
					if (oWoman.IsTelephoneRegistered(txtTel.Text)) {
						//携帯電話番号は既に登録されています。 
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
						bOk = false;
					}
				}
			}
		}

		attrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		attrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		attrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm.Equals("愛称")) {
				CheckAttrValue(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i],i,ref bOk);
				break;
			}
		}

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm.Equals("出身地")) {
				CheckAttrValue(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i],i,ref bOk);
				break;
			}
		}

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm.Equals("性別")) {
				CheckAttrValue(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i],i,ref bOk);
				break;
			}
		}

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			switch (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm) {
				case "志望ｼﾞｬﾝﾙ":
				case "職業":
				case "身長":
				case "ﾊﾞｽﾄ":
				case "ｳｴｽﾄ":
				case "ﾋｯﾌﾟ":
					CheckAttrValue(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i],i,ref bOk);
					break;
			}
		}

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm.Equals("ﾊﾞｽﾄｻｲｽﾞ")) {
				CheckAttrValue(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i],i,ref bOk);
				break;
			}
		}

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			switch (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm) {
				case "血液型":
				case "趣味":
				case "特技":
					CheckAttrValue(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i],i,ref bOk);
					break;
			}
		}

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeNm.Equals("出演時間")) {
				CheckAttrValue(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i],i,ref bOk);
				break;
			}
		}

		for (int i = 0;i < sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList.Count;i++) {
			attrTypeSeq[i] = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].attrList[i].attrTypeSeq;
		}

		if (pnlBirthday.Visible) {
			if (IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
				string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
				int iAge = ViCommPrograms.Age(sDate);
				if (iAge == 0) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
				} else if (iAge < 10) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_STATUS_UNDER_14);
				}
			}
		}

		if (txtAreaCommentDetail.Visible) {
			if (sessionWoman.ngWord.VaidateDoc(txtAreaCommentDetail.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		if (txtAreaCommentList.Visible) {
			if (string.IsNullOrEmpty(txtAreaCommentList.Text)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_COMMENT_LIST);
			}else if (sessionWoman.ngWord.VaidateDoc(txtAreaCommentList.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format("自己PRに禁止語句が含まれています。<br />");
			}
		}

		return bOk & this.CheckOther();
	}

	private void CheckAttrValue(UserWomanAttr pAttr,int i,ref bool bOk) {
		string sNGWord = string.Empty;

		if (pAttr.inputType == ViCommConst.INPUT_TYPE_TEXT) {
			if (int.Parse(pAttr.rowCount) > 1) {
				TextArea txtAreaUserWomanItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserWomanItem{0}",i + 1)) as TextArea;
				attrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtAreaUserWomanItem.Text));
				if (attrInputValue[i].Length > 300) {
					attrInputValue[i] = SysPrograms.Substring(attrInputValue[i],300);
				}
			} else {
				iBMobileTextBox txtUserWomanItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserWomanItem{0}",i + 1)) as iBMobileTextBox;
				attrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtUserWomanItem.Text));
			}
			if (sessionWoman.ngWord.VaidateDoc(attrInputValue[i],out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format("{0}に禁止語句が含まれています。<br />",pAttr.attrTypeNm);
			}
			if (pAttr.profileReqItemFlag) {
				if (attrInputValue[i].Equals(string.Empty)) {
					lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),pAttr.attrTypeNm);
					bOk = false;
				}
			}
			if (!attrInputValue[i].Equals(string.Empty)) {
				switch (pAttr.attrTypeNm) {
					case "身長":
					case "ﾊﾞｽﾄ":
					case "ｳｴｽﾄ":
					case "ﾋｯﾌﾟ":
						if (!SysPrograms.Expression(@"^[0-9]+$",attrInputValue[i])) {
							lblErrorMessage.Text += string.Format("{0}は半角数字で入力してください。<br />",pAttr.attrTypeNm);
							bOk = false;
						}
						break;
				}
			}
		} else {
			SelectionList lstUserWomanItem = (SelectionList)frmMain.FindControl(string.Format("lstUserWomanItem{0}",i + 1)) as SelectionList;
			if (lstUserWomanItem.Selection != null) {
				if (pAttr.profileReqItemFlag && lstUserWomanItem.Selection.Value.Equals(string.Empty)) {
					lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),pAttr.attrTypeNm);
					bOk = false;
				} else {
					attrSeq[i] = lstUserWomanItem.Selection.Value;
				}
			} else {
				if (pAttr.profileReqItemFlag) {
					bOk = false;
					lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),pAttr.attrTypeNm);
				}
			}
		}
	}
	
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<mobile:Panel ID="pnlHandleNm" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLiteralText runat="server" BreakAfter="true" Text="芸名 <font color='#ff0000'>※絵文字は使用不可です</font>" />
			<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="20"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines01" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlTel" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblTel" runat="server">電話番号</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true" BreakAfter="true"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines26" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlBirthday" Runat="server" Visible="false">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">生年月日</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagBirthdayHint1" runat="server" Text="<font color=#FF6666>※男性へは年齢のみ表示。</font><br>" />
			<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">年</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">月</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblDay" runat="server">日</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagBirthdayHint2" runat="server" Text="$NBSP;<font color=#FF6666>▼</font><font color=gray>設定する年齢のﾋﾝﾄです♪</font><br>$NBSP;" />
			<mobile:SelectionList ID="lstAgeMainteHint" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines02" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>

		<mobile:Panel ID="pnlUserWoman18" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem18" runat="server">UserWoman18</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem18" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem18" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem18" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines22" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman11" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem11" runat="server" Visible="true">UserWoman11</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem11" runat="server" MaxLength="341" Size="12" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem11" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem11" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman1" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem1" runat="server" Visible="true">UserWoman01</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem1" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem1" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem1" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines05" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman2" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserWomanItem2" runat="server" Visible="true">UserWoman02</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem2" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem2" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem2" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines06" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman3" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserWomanItem3" runat="server" Visible="true">UserWoman03</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem3" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem3" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem3" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines07" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman12" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem12" runat="server" Visible="true">UserWoman12</cc1:iBMobileLabel>
			<mobile:Panel ID="pnlSexInput" Runat="server" Visible="false">
				<cc1:iBMobileTextBox ID="txtUserWomanItem12" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
				<tx:TextArea ID="txtAreaUserWomanItem12" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
				</tx:TextArea>
				<mobile:SelectionList ID="lstUserWomanItem12" Runat="server"></mobile:SelectionList>
			</mobile:Panel>
			<cc1:iBMobileLabel ID="lblSex" runat="server"></cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman4" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem4" runat="server" Visible="true">UserWoman04</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem4" runat="server" MaxLength="341" Size="12" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem4" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem4" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines08" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman6" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem6" runat="server" Visible="true">UserWoman06</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem6" runat="server" MaxLength="3" Size="12" Numeric="true" BreakAfter="false"></cc1:iBMobileTextBox>cm<br />
			<tx:TextArea ID="txtAreaUserWomanItem6" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem6" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman13" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem13" runat="server" Visible="true">UserWoman13</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem13" runat="server" MaxLength="3" Size="12" Numeric="true" BreakAfter="false"></cc1:iBMobileTextBox>cm<br />
			<tx:TextArea ID="txtAreaUserWomanItem13" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem13" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman14" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem14" runat="server" Visible="true">UserWoman14</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem14" runat="server" MaxLength="3" Size="12" Numeric="true" BreakAfter="false"></cc1:iBMobileTextBox>cm<br />
			<tx:TextArea ID="txtAreaUserWomanItem14" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem14" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman15" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem15" runat="server" Visible="true">UserWoman15</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem15" runat="server" MaxLength="3" Size="12" Numeric="true" BreakAfter="false"></cc1:iBMobileTextBox>cm<br />
			<tx:TextArea ID="txtAreaUserWomanItem15" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem15" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman5" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem5" runat="server" Visible="true">UserWoman05</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem5" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem5" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem5" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines09" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman7" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem7" runat="server" Visible="true">UserWoman07</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem7" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem7" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem7" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman16" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem16" runat="server" Visible="true">UserWoman16</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem16" runat="server" MaxLength="341" Size="12" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem16" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem16" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman17" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem17" runat="server" Visible="true">UserWoman17</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem17" runat="server" MaxLength="341" Size="12" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem17" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem17" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines21" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman8" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
			<cc1:iBMobileLabel ID="lblUserWomanItem8" runat="server" Visible="true">UserWoman08</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem8" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem8" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem8" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman9" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserWomanItem9" runat="server" Visible="true">UserWoman09</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem9" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem9" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem9" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman10" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserWomanItem10" runat="server" Visible="true">UserWoman10</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem10" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem10" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem10" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>

		<mobile:Panel ID="pnlCommentList" runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<font color='#e69118'>■</font>" />
		    <cc1:iBMobileLabel ID="lblCommentList" runat="server" Visible="true">自己PR</cc1:iBMobileLabel>
		    <tx:TextArea ID="txtAreaCommentList" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" MaxLength="1333" EmojiPaletteEnabled="true">
		    </tx:TextArea>
		    <cc1:iBMobileLiteralText ID="tagLines03" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCommentDetail" Runat="server" Visible="false">
		    <cc1:iBMobileLabel ID="lblCommentDetail" runat="server">詳細用コメント</cc1:iBMobileLabel>
		    <tx:TextArea ID="txtAreaCommentDetail" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" MaxLength="1333" EmojiPaletteEnabled="true">
		    </tx:TextArea>
		    <cc1:iBMobileLiteralText ID="tagLines04" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>

		<mobile:Panel ID="pnlUserWoman19" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem19" runat="server">UserWoman19</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem19" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem19" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem19" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines23" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman20" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem20" runat="server">UserWoman20</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem20" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem20" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem20" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines24" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlOkList" Runat="server">
			<cc1:iBMobileLabel ID="IBMobileLabel1" runat="server" BreakAfter="true" Font-Size="Small">OKﾘｽﾄ</cc1:iBMobileLabel>
			<mobile:SelectionList ID="chkOK0" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK1" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK2" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK3" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK4" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK5" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK6" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK7" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK8" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK9" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK10" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK11" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK12" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK13" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK14" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK15" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK16" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK17" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK18" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK19" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK20" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK21" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK22" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK23" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK24" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK25" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK26" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK27" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK28" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK29" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK30" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK31" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK32" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK33" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK34" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK35" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK36" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK37" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK38" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK39" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines25" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		$PGM_HTML02;
		$PGM_HTML03;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
