﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../SendProfilePic.aspx.cs" Inherits="ViComm_woman_SendProfilePic" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="ViComm" %>

<script runat="server">
	protected override bool ValidateInput() {
		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		bool bOk = true;
		string sNGWord = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (txtTitle.Visible && txtTitle.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += "ﾀｲﾄﾙを入力して下さい。<br />";
		} else if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text += "ﾀｲﾄﾙに禁止語句が含まれています。<br />";
		}

		if (txtDoc.Visible && txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += "ｺﾒﾝﾄを入力して下さい。<br />";
		} else if (sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text += "ｺﾒﾝﾄに禁止語句が含まれています。<br />";
		}

		return bOk;
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server"><cc1:iBMobileLiteralText ID="tag01" runat="server" /> 
		$PGM_HTML01; 
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML02;
		<mobile:Panel ID="pnlSearchList" Runat="server" Visible="false">
			<mobile:SelectionList ID="lstGenreSelect" Runat="server" SelectType="DropDown"></mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML03;
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24"></cc1:iBMobileTextBox>
		$PGM_HTML04;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24">
		</tx:TextArea>
		$PGM_HTML05;
		<mobile:SelectionList ID="rdoPicType" Runat="server" SelectType="Radio" Alignment="Left">
			<Item Text="ｷﾞｬﾗﾘｰ用" Value="0" Selected="true" />
			<Item Text="ﾌﾟﾛﾌ用" Value="1" Selected="False" />
			<Item Text="ひとまず非表示" Value="2" Selected="False" />
		</mobile:SelectionList>
		$PGM_HTML06;
		<mobile:Panel ID="pnlBlur" Runat="server" Visible="false">
			<mobile:SelectionList ID="chkModifyRequestFlag" Runat="server" SelectType="CheckBox" >
				<Item Text=$PGM_HTML07;></Item>
			</mobile:SelectionList>
			$PGM_HTML08;
			<tx:TextArea ID="txtModifyRequestSpec" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="2" Columns="24">
			</tx:TextArea>
		</mobile:Panel>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
