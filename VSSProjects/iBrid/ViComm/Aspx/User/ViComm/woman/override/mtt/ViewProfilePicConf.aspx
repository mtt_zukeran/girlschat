<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ViewProfilePicConf.aspx.cs" Inherits="ViComm_woman_ViewProfilePicConf" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="ViComm" %>

<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		base.Page_Load(sender,e);
		
		lstAttrSeq.Visible = false;

		if (lstPicType.Selection.Value.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			lstPicType.Visible = false;
		}

		ViewState["ATTR_TYPE_SEQ"] = "-1";
		ViewState["ATTR_SEQ"] = "-1";
	}

	protected override bool ValidText() {
		bool bIsAvailableService = false;
		sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		using (ManageCompany oManageCompany = new ManageCompany()) {
			bIsAvailableService = oManageCompany.IsAvailableService(ViCommConst.RELEASE_GALLERY_PIC_INPUT_TITLE_AND_DOC);
		}

		bool bOk = true;

		if (bIsAvailableService) {
			string sNGWord;
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}

			if (txtTitle.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += "ﾀｲﾄﾙを入力して下さい。<br />";
			} else if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += "ﾀｲﾄﾙに禁止語句が含まれています。<br />";
			}

			if (txtDoc.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += "ｺﾒﾝﾄを入力して下さい。<br />";
			} else if (sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += "ｺﾒﾝﾄに禁止語句が含まれています。<br />";
			}
		}

		return bOk;
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="false" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML04;
		<mobile:SelectionList ID="lstAttrSeq" Runat="server" SelectType="DropDown"></mobile:SelectionList>
		$PGM_HTML05;
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24"></cc1:iBMobileTextBox>
		$PGM_HTML06;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24">
		</tx:TextArea>
		$PGM_HTML07;
		<mobile:SelectionList ID="lstPicType" Runat="server" Alignment="Left"></mobile:SelectionList>
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
