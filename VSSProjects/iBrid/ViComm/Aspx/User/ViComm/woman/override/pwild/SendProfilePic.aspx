﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../SendProfilePic.aspx.cs" Inherits="ViComm_woman_SendProfilePic" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>

<script runat="server">
    protected override bool ValidateInput() {
        bool bResult = base.ValidateInput();
        
        if (System.Text.Encoding.GetEncoding(932).GetByteCount(txtModifyRequestSpec.Text) > 20) {
            this.lblErrorMessage.Text += string.Format(GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_OVER_LETTER_COUNT), "ﾓｻﾞｲｸの指定などは全角10");
            bResult = bResult & false;
        }

        return bResult;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server"><cc1:iBMobileLiteralText ID="tag01" runat="server" /> 
		$PGM_HTML01; 
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML02;
		<mobile:Panel ID="pnlSearchList" Runat="server">
			<mobile:SelectionList ID="lstGenreSelect" Runat="server" SelectType="DropDown"></mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML03;
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24"></cc1:iBMobileTextBox>
		$PGM_HTML04;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24">
		</tx:TextArea>
		$PGM_HTML05;
		<mobile:SelectionList ID="rdoPicType" Runat="server" SelectType="Radio" Alignment="Left">
			<Item Text="ｷﾞｬﾗﾘｰ用" Value="0" Selected="true" />
			<Item Text="ﾌﾟﾛﾌ用" Value="1" Selected="False" />
			<Item Text="ひとまず非表示" Value="2" Selected="False" />
		</mobile:SelectionList>
		$PGM_HTML06;
		<mobile:Panel ID="pnlBlur" Runat="server">
			<mobile:SelectionList ID="chkModifyRequestFlag" Runat="server" SelectType="CheckBox" >
				<Item Text=$PGM_HTML07;></Item>
			</mobile:SelectionList>
			$PGM_HTML08;
			<tx:TextArea ID="txtModifyRequestSpec" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="2" Columns="24">
			</tx:TextArea>
		</mobile:Panel>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
