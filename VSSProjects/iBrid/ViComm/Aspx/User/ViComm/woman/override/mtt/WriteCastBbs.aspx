﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../WriteCastBbs.aspx.cs" Inherits="ViComm_woman_WriteCastBbs" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblTitle" runat="server" Visible="false">ﾀｲﾄﾙ</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="ltrTitle" runat="server" Text="タイトル：<font color=red>※20文字程度</font>"></cc1:iBMobileLiteralText>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24" EmojiPaletteEnabled="true" ></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblDoc" runat="server" Visible="false">本文</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="ltrDoc" runat="server" Text="本文：<font color=red>※100文字以内</font>"></cc1:iBMobileLiteralText>
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		<br />
		$PGM_HTML06;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
