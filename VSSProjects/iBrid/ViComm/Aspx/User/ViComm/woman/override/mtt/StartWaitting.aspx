﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../StartWaitting.aspx.cs"
    Inherits="ViComm_woman_StartWaitting" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>

<script runat="server">
    protected override void OnLoad(EventArgs e) {
        base.OnLoad(e);

        if (!IsPostBack) {
            foreach (MobileListItem oItem in lstConnectType.Items) {
                switch (oItem.Value) {
                    case "1":
                        oItem.Text = "TV電話のみ";
                        break;
                    case "2":
                        oItem.Text = "音声通話のみ";
                        break;
                    case "3":
                        oItem.Text = "TV電話/音声の両方";
                        break;
                }
            }

            MobileListItem[] oItemArray = this.lstConnectType.Items.GetAll();
            this.lstConnectType.Items.Clear();
            foreach (MobileListItem oItem in oItemArray) {
                if (oItem.Value == "3") {
                    this.lstConnectType.Items.Insert(0, oItem);
                } else {
                    this.lstConnectType.Items.Add(oItem);
                }
            }
            
            int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq, sessionWoman.userWoman.curCharNo);

            if (!((iCharacterOnlineStatus.Equals(ViComm.ViCommConst.USER_OFFLINE)) || (iCharacterOnlineStatus.Equals(ViComm.ViCommConst.USER_LOGINED)))) {
                lstWaitTime.Items.Remove(new MobileListItem("待機終了", "-1"));

                int iIndex;
                if (int.TryParse(sessionWoman.userWoman.CurCharacter.standbyTimeType, out iIndex)) {
                    lstWaitTime.SelectedIndex = iIndex;
                }
            }
        }
    }
    
	protected override void Page_Load(object sender,EventArgs e) {
		base.Page_Load(sender,e);
		
		chkMonitorEnableFlag.Visible = false;
		
		chkMonitorEnableFlag.Items[0].Selected = true;
	}

	protected override bool CheckOther() {
		if (int.Parse(sessionWoman.userWoman.CurCharacter.age) < 18) {
			double dWaitHour;
			double.TryParse(lstWaitTime.Items[lstWaitTime.SelectedIndex].Value,out dWaitHour);

			if (DateTime.Now.AddMinutes(dWaitHour * 60) > DateTime.Parse(string.Format("{0} 22:00:00",DateTime.Now.ToString("yyyy/MM/dd")))) {
				lblErrorMessage.Text = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_START_WAITTING_UNDER_18);
				return false;
			}
		}
		return true;
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:SelectionList ID="lstWaitTime" Runat="server" BreakAfter="false">
        </mobile:SelectionList>
		$PGM_HTML02;
		<mobile:SelectionList ID="lstConnectType" Runat="server" BreakAfter="false">
        </mobile:SelectionList>
		$PGM_HTML03;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="false" ForeColor="Red"></cc1:iBMobileLabel>
		<tx:TextArea ID="txtWaitComment" runat="server" BreakBefore="false" BrerakAfter="false"
            Rows="6" Columns="24" MaxLength="300" EmojiPaletteEnabled="true"></tx:TextArea>	
		$PGM_HTML04;
		<mobile:SelectionList ID="chkMonitorEnableFlag" Runat="server" SelectType="CheckBox"
            Font-Size="Small" Alignment="Left">
            <Item Text="$PGM_HTML05;" />
        </mobile:SelectionList>		
		$PGM_HTML06;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
