<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../LoginReminder.aspx.cs" Inherits="ViComm_woman_LoginReminder" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="vicomm" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Import Namespace="Oracle.DataAccess.Client" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<script runat="server">
	protected override void OnLoad(EventArgs e) {
		base.OnLoad(e);
		if (this.IsPostBack) {
			lblTelErrorMessage.Text = string.Empty;
			if (!string.IsNullOrEmpty(this.Request.Params["cmdSendSMS"])) {
				lblErrorMessage.Text = string.Empty;

				// SMS送信ボタンクリック
				cmdSendSMS_Click(this,EventArgs.Empty);
			}
		}
	}

	/// <summary>
	/// SMS送信ボタンクリック
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public void cmdSendSMS_Click(object sender,EventArgs e) {
		bool bOk = true;
		string sTel = string.Empty;
		string sPassword = string.Empty;
		double dTel = 0;

		lblTelErrorMessage.Text = string.Empty;

		// 電話番号の入力チェック
		if (string.IsNullOrEmpty(this.txtTel.Text)) {
			lblTelErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
			bOk = false;
		}
		sTel = txtTel.Text.Replace("-","");
		if (bOk && !double.TryParse(sTel,out dTel)) {
			lblTelErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
			bOk = false;
		}
		if (bOk == false) {
			return;
		}

		DataSet ds = null;
		DataRow dr = null;
		using (DbSession db = new DbSession()) {
			// 入力された電話番号によりパスワードを取得
			User oUser = new User();
			ds = oUser.GetOneByTel(sTel, ViCommConst.OPERATOR);
			if (ds.Tables[0].Rows.Count == 0) {
				// 電話番号未登録
				lblTelErrorMessage.Text = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_STATUS_NOT_EXISTS_TEL_NO);
				return;
			}
			dr = ds.Tables[0].Rows[0];
			sPassword = dr["LOGIN_PASSWORD"].ToString();

			// サイト構成拡張設定からログインパスワード送信SMS本文を取得
			string sBodyText = string.Empty;
			SiteManagementEx oSiteManagementEx = new SiteManagementEx();
			if (!oSiteManagementEx.GetOne(sessionObj.site.siteCd)) {
				// サイト構成拡張設定未登録
				lblTelErrorMessage.Text = "内部エラー";
				return;
			}
			sBodyText = oSiteManagementEx.loginReminderSmsDoc;

			// システム設定からTwilio情報を取得
			Sys oSys = new Sys();
			if (!oSys.GetOne()) {
				// システム設定未登録
				lblTelErrorMessage.Text = "内部エラー";
				return;
			}

			// SMS送信APIのURLを生成
			string sUrl = string.Format("https://api.twilio.com/2010-04-01/Accounts/{0}/Messages.json",oSys.twilioAccountSid);

			try {
				// POSTパラメータ
				System.Collections.Specialized.NameValueCollection ps = new System.Collections.Specialized.NameValueCollection();
				ps.Add("To",string.Format("+81{0}", dTel.ToString()));
				ps.Add("From",string.Format("+{0}", oSys.twilioSmsTelNo));
				ps.Add("Body",string.Format(sBodyText, sPassword));
				// SMS送信APIにPOST送信
				System.Net.WebClient wc = new System.Net.WebClient();
				wc.Credentials = new System.Net.NetworkCredential(oSys.twilioAccountSid,oSys.twilioAuthToken);
				byte[] resData = wc.UploadValues(sUrl,ps);
				wc.Dispose();
				// 送信完了画面へリダイレクト
				IDictionary<string,string> oParam = new Dictionary<string,string>();
				oParam.Add("tel",sTel);
				RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_REMINDER_OK,oParam);
			} catch (Exception ee) {
				// エラー時
				ViCommInterface.WriteIFError(ee,"Send SMS",sUrl);
			}
		}
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		$PGM_HTML02;
	    <vicomm:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></vicomm:iBMobileLabel>
		$PGM_HTML03;
		<vicomm:iBMobileTextBox ID="txtMailAddr" runat="server" size="16" MaxLength="256" Alphameric="true"></vicomm:iBMobileTextBox>
		$PGM_HTML04;
		$PGM_HTML05;
	    <vicomm:iBMobileLabel ID="lblTelErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></vicomm:iBMobileLabel>
		$PGM_HTML06;
		<vicomm:iBMobileTextBox ID="txtTel" runat="server" size="16" MaxLength="15" Numeric="true"></vicomm:iBMobileTextBox>
		$PGM_HTML07;
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
