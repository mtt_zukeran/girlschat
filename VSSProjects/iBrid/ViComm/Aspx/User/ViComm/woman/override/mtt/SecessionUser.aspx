﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../SecessionUser.aspx.cs" Inherits="ViComm_woman_SecessionUser" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<script runat="server">
    protected override bool CheckDrawalDoc(string pDrawalDoc) {
        return true;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="false" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
