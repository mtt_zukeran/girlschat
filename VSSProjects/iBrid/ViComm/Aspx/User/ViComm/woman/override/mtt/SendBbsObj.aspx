<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../SendBbsObj.aspx.cs" Inherits="ViComm_woman_SendBbsObj" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		base.Page_Load(sender,e);

		if (iBridUtil.GetStringValue(Request.QueryString["objtype"]).Equals(ViCommConst.BbsObjType.MOVIE)) {
			txtTitle.Visible = false;
		}
	}

	protected override bool ValidateInput() {
		bool bOk = true;
		string sNGWord = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtTitle.Visible) {
			string sEmojiTitle = Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text);

			if (txtTitle.Text.Equals(string.Empty)) {
				bOk = false;
				lblErrorMessage.Text += "ｶﾞﾁｬｺﾒﾝﾄを入力して下さい。<br />";
			} else if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += "ｶﾞﾁｬｺﾒﾝﾄに禁止語句が含まれています。<br />";
			} else if (!txtTitle.Text.Equals(sEmojiTitle)) {
				bOk = false;
				lblErrorMessage.Text += "ｶﾞﾁｬｺﾒﾝﾄに絵文字は利用できません。<br />";
			} else if (txtTitle.Text.Length > 30) {
				bOk = false;
				lblErrorMessage.Text += "ｶﾞﾁｬｺﾒﾝﾄが長すぎます。<br />";
			}
		}

		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += "説明文を入力して下さい。<br />";
		} else if (sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text += "説明文に禁止語句が含まれています。<br />";
		}

		if (pnlSearchList.Visible) {
			string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
			string[] selectedValueList = selectedValue.Split(',');

			string sAttrTypeSeq = selectedValueList[0];
			string sAttrSeq = selectedValueList[1];

			if (sAttrTypeSeq.Equals(string.Empty) || sAttrSeq.Equals(string.Empty)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_OBJ_ATTR) + "<br />";
			}
		}

		return bOk;
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server"><cc1:iBMobileLiteralText ID="tag01" runat="server" /> 
		$PGM_HTML01; 
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML02;
		<mobile:SelectionList ID="rdoBbsObj" Runat="server" SelectType="Radio" Alignment="Left">
		   <Item Text="$PGM_HTML03;" Value="0" Selected="true" />
		   <Item Text="$PGM_HTML04;" Value="1" Selected="False" />
		</mobile:SelectionList>
		$PGM_HTML05;
		<mobile:Panel ID="pnlSearchList" Runat="server">
			<mobile:SelectionList ID="lstGenreSelect" Runat="server" SelectType="DropDown"></mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML06;
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24"></cc1:iBMobileTextBox>
		$PGM_HTML07;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		$PGM_HTML08;
		<mobile:SelectionList ID="lstRankingFlag" Runat="server" SelectType="Radio" Alignment="Left" Visible="false">
		   <Item Text="参加する" Value="1" Selected="true" />
		   <Item Text="参加しない" Value="0" Selected="False" />
		</mobile:SelectionList>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
