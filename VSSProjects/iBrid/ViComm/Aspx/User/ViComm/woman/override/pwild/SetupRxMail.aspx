<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../SetupRxMail.aspx.cs" Inherits="ViComm_woman_SetupRxMail" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>

<script runat="server">
	protected override void OnLoad(EventArgs e) {
		base.OnLoad(e);
		if (!this.IsPostBack) {
			UserWomanCharacter oWomanCharacter = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey];

			if (oWomanCharacter.mobileMailRxTimeUseFlag == ViCommConst.FLAG_ON) {
				chkMobileMailRxTimeUseFlag.SelectedIndex = 0;
			}
			if (oWomanCharacter.mobileMailRxTimeUseFlag2 == ViCommConst.FLAG_ON) {
				chkMobileMailRxTimeUseFlag2.SelectedIndex = 0;
			}
			
			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds;
				int iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TIME_PART);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstMobileMailRxStartTime2.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));
					lstMobileMailRxEndTime2.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));

					if (oWomanCharacter.mobileMailRxStartTime2.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxStartTime2.SelectedIndex = iIdx;
					}
					if (oWomanCharacter.mobileMailRxEndTime2.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxEndTime2.SelectedIndex = iIdx;
					}
					iIdx++;
				}
			}
		}
	}
	
	protected override void cmdSubmit_Click(object sender,EventArgs e) {
		int iTalkEndMailRxFlag = 0;
		int.TryParse(lstTalkEndMailRxFlag.Items[lstTalkEndMailRxFlag.SelectedIndex].Value,out iTalkEndMailRxFlag);
		int iMobileMailRxTimeUseFlag;
		int iMobileMailRxTimeUseFlag2;
		string sMobileMailRxStartTime2;
		string sMobileMailRxEndTime2;

		string sMobileMailRxStartTime = lstMobileMailRxStartTime.Items[lstMobileMailRxStartTime.SelectedIndex].Value;
		string sMobileMailRxEndTime = lstMobileMailRxEndTime.Items[lstMobileMailRxEndTime.SelectedIndex].Value;
		
		if (!iBridUtil.GetStringValue(this.Request.Form["double"]).Equals(ViCommConst.FLAG_ON_STR)) {
			iMobileMailRxTimeUseFlag = ViCommConst.FLAG_ON;
			iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_OFF;
			sMobileMailRxStartTime2 = "00:00";
			sMobileMailRxEndTime2 = "00:00";
		} else {
			iMobileMailRxTimeUseFlag = ViCommConst.FLAG_OFF;
			if (chkMobileMailRxTimeUseFlag.Selection == null) {
				iMobileMailRxTimeUseFlag = ViCommConst.FLAG_OFF;
			} else {
				iMobileMailRxTimeUseFlag = ViCommConst.FLAG_ON;
			}
			iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_OFF;
			if (chkMobileMailRxTimeUseFlag2.Selection == null) {
				iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_OFF;
			} else {
				iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_ON;
			}
			sMobileMailRxStartTime2 = lstMobileMailRxStartTime2.Items[lstMobileMailRxStartTime2.SelectedIndex].Value;
			sMobileMailRxEndTime2 = lstMobileMailRxEndTime2.Items[lstMobileMailRxEndTime2.SelectedIndex].Value;

			bool bOk = false;

			if (iMobileMailRxTimeUseFlag == ViCommConst.FLAG_ON && iMobileMailRxTimeUseFlag2 == ViCommConst.FLAG_ON) {
				int iMobileMailRxStartTime;
				int iMobileMailRxEndTime;
				int iMobileMailRxStartTime2;
				int iMobileMailRxEndTime2;

				iMobileMailRxStartTime = int.Parse(sMobileMailRxStartTime.Replace(":",""));
				iMobileMailRxEndTime = int.Parse(sMobileMailRxEndTime.Replace(":",""));

				iMobileMailRxStartTime2 = int.Parse(sMobileMailRxStartTime2.Replace(":",""));
				iMobileMailRxEndTime2 = int.Parse(sMobileMailRxEndTime2.Replace(":",""));

				if (iMobileMailRxStartTime < iMobileMailRxEndTime && iMobileMailRxStartTime2 < iMobileMailRxEndTime2) {
					if (iMobileMailRxStartTime <= iMobileMailRxStartTime2 && iMobileMailRxEndTime <= iMobileMailRxStartTime2) {
						bOk = true;
					} else if (iMobileMailRxStartTime2 <= iMobileMailRxStartTime && iMobileMailRxEndTime2 <= iMobileMailRxStartTime) {
						bOk = true;
					}
				} else if (iMobileMailRxStartTime < iMobileMailRxEndTime && iMobileMailRxStartTime2 >= iMobileMailRxEndTime2) {
					if (iMobileMailRxStartTime >= iMobileMailRxEndTime2 && iMobileMailRxEndTime <= iMobileMailRxStartTime2) {
						bOk = true;
					}
				} else if (iMobileMailRxStartTime >= iMobileMailRxEndTime && iMobileMailRxStartTime2 < iMobileMailRxEndTime2) {
					if (iMobileMailRxStartTime2 >= iMobileMailRxEndTime && iMobileMailRxEndTime2 <= iMobileMailRxStartTime) {
						bOk = true;
					}
				} else {
					bOk = false;
				}
			} else {
				bOk = true;
			}

			if (bOk == false) {
				sessionWoman.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_RX_TIME);
				return;
			}
		}
		
		sessionWoman.userWoman.SetupRxMailDouble(
			sessionWoman.site.siteCd,
			sessionWoman.userWoman.userSeq,
			sessionWoman.userWoman.curCharNo,
			lstManMailRxType.Items[lstManMailRxType.SelectedIndex].Value,
			lstInfoMailRxType.Items[lstInfoMailRxType.SelectedIndex].Value,
			iTalkEndMailRxFlag,
			iMobileMailRxTimeUseFlag,
			sMobileMailRxStartTime,
			sMobileMailRxEndTime,
			iMobileMailRxTimeUseFlag2,
			sMobileMailRxStartTime2,
			sMobileMailRxEndTime2,
			ViCommConst.FLAG_OFF
		);

		if (buyPointMailRxType is SelectionList) {
			sessionWoman.userWoman.SetupRxMailEx(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				buyPointMailRxType.Items[buyPointMailRxType.SelectedIndex].Value
			);
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SETUP_MAIL_COMPLITE_CAST) + "&site=" + sessionWoman.site.siteCd);
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<cc1:iBMobileLiteralText runat="server" Text="<font color=#FF6666 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblInfoMailRxType" runat="server">サイトからのお知らせメールを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstInfoMailRxType" Runat="server"></mobile:SelectionList>
		<cc1:iBMobileLiteralText runat="server" Text="<hr width=90% align=center color=#FF0099 size=1 />" />
		<cc1:iBMobileLiteralText runat="server" Text="<font color=#FF6666 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblManMailRxType" runat="server">男性からのﾒｰﾙを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstManMailRxType" Runat="server"></mobile:SelectionList>
		<mobile:Panel ID="pnlTalkEndMailRxFlag" Runat="server">
			<cc1:iBMobileLabel ID="lblTalkEndMailRxFlag" Runat="server">通話切断理由ﾒｰﾙを</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstTalkEndMailRxFlag" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML02;
		<mobile:Panel ID="pnlMobileMailRxTime" Runat="server">
		<cc1:iBMobileLiteralText runat="server" Text="<hr width=90% align=center color=#FF0099 size=1 />" />
		<cc1:iBMobileLiteralText runat="server" Text="<font color=#FF6666 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblMobileMailRxTime" runat="server">受信時間帯</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText runat="server" Text="<font color=#FF0099 size=2>※</font>設定時間以外は携帯へ転送されません<br />" />
		$PGM_HTML06;
		<mobile:SelectionList ID="chkMobileMailRxTimeUseFlag" Runat="server" SelectType="CheckBox" Font-Size="Small" BreakAfter="false" Visible="true"><Item Text="" Value="1" /></mobile:SelectionList>
		$PGM_HTML07;
		<mobile:SelectionList ID="lstMobileMailRxStartTime" Runat="server" BreakAfter="false" ></mobile:SelectionList>〜
		<mobile:SelectionList ID="lstMobileMailRxEndTime" Runat="server" ></mobile:SelectionList>
		$PGM_HTML06;
		<mobile:SelectionList ID="chkMobileMailRxTimeUseFlag2" Runat="server" SelectType="CheckBox" Font-Size="Small" BreakAfter="false" Visible="true"><Item Text="" Value="1" /></mobile:SelectionList>
		$PGM_HTML07;
		$PGM_HTML06;
		<mobile:SelectionList ID="lstMobileMailRxStartTime2" Runat="server" BreakAfter="false"></mobile:SelectionList>〜
		<mobile:SelectionList ID="lstMobileMailRxEndTime2" Runat="server" BreakAfter="false"></mobile:SelectionList>
		$PGM_HTML07;
		</mobile:Panel>
		$PGM_HTML05;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
