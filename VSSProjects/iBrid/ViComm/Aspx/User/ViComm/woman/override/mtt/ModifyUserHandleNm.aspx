<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ModifyUserHandleNm.aspx.cs" Inherits="ViComm_woman_ModifyUserHandleNm" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>

<script runat="server">
	protected override bool CheckInputValue() {
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += "芸名を入力して下さい。<br />";
			}
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += "芸名に禁止語句が含まれています。<br />";
			}
		}

		string sCheckHandleNm = Mobile.EmojiToCommTag(sessionWoman.carrier,txtHandelNm.Text);
		if (!txtHandelNm.Text.Equals(sCheckHandleNm)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_HANDLE_NM_NG);
		}

		return bOk;
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText runat="server" BreakAfter="true" Text="芸名 <font color='#ff0000'>※絵文字は使用不可です</font>" />
		<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="20" BreakAfter="true"></cc1:iBMobileTextBox>
		$PGM_HTML02;
		$PGM_HTML03;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
