<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../RegistRefuse.aspx.cs" Inherits="ViComm_woman_RegistRefuse" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		if (IsFanClubMember()) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_REFUSE_NG_FANCLUB_MEMBER);
		} else {
			base.Page_Load(sender,e);
		}
	}

	private bool IsFanClubMember() {
		string sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);
		int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
		string sMemberRank = "0";
		DataRow dr = null;
		if (sessionWoman.SetManDataSetByLoginId(sLoginId,iRecNo,out dr)) {
			using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
				sMemberRank = oFanClubStatus.GetMyFanClubMemberRank(
					sessionWoman.site.siteCd,
					dr["USER_SEQ"].ToString(),
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo
				);
			}
		}

		if (sMemberRank.Equals("0")) {
			return false;
		} else {
			return true;
		}
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		$DATASET_LOOP_START1;
			<cc1:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END1;
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
