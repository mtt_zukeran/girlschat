﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ModifyUserHandleNm.aspx.cs" Inherits="ViComm_woman_ModifyUserHandleNm" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
	    <cc1:iBMobileLiteralText ID="tagDivS01" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<cc1:iBMobileLiteralText ID="tag01s" runat="server" Text="<font size=1>" />
		<cc1:iBMobileLiteralText ID="tagDivS02" runat="server" BreakAfter="false" Text="</div>" />
		<cc1:iBMobileLabel ID="lblHandelNm" runat="server">名前/ﾆｯｸﾈｰﾑ</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12" BreakAfter="false"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagDivS03" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<br />
		<cc1:iBMobileLiteralText ID="tagDivS04" runat="server" BreakAfter="false" Text="</div>" />
		<cc1:iBMobileLiteralText ID="tagLines01" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLiteralText ID="tagDivS05" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<br />
		<cc1:iBMobileLiteralText ID="tagDivS06" runat="server" BreakAfter="false" Text="</div>" />
		$PGM_HTML02;
		<cc1:iBMobileLiteralText ID="tagDivS07" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<cc1:iBMobileLiteralText ID="tag01e" runat="server" Text="</font>" />
		<cc1:iBMobileLiteralText ID="tagDivS08" runat="server" BreakAfter="false" Text="</div>" />
		$PGM_HTML03;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
