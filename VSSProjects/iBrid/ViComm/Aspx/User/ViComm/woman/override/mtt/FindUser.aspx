<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../FindUser.aspx.cs" Inherits="ViComm_woman_FindUser" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<script runat="server">

	protected override void OnLoad(EventArgs e) {
		if (!this.IsPostBack) {
			// 地域
			using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
				DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionWoman.site.siteCd, "400");

				using (CodeDtl oCodeDtl = new CodeDtl()) {
					DataSet dsCodeDtl = oCodeDtl.GetList("81");
					foreach (DataRow drCodeDtl in dsCodeDtl.Tables[0].Rows) {
						string sExp = string.Format("GROUPING_CD={0}", drCodeDtl["CODE"]);

						string sValues = string.Empty;
						foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Select(sExp)) {
							if (!string.IsNullOrEmpty(sValues)) sValues += ",";
							sValues += drAttrTypeValue["MAN_ATTR_SEQ"];
						}

						lstUserManItemEx1.Items.Add(new MobileListItem(drCodeDtl["CODE_NM"].ToString(), sValues));

					}
				}

				foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
					if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lstUserManItemEx1.Items.Add(new MobileListItem(drAttrTypeValue["MAN_ATTR_NM"].ToString(), drAttrTypeValue["MAN_ATTR_SEQ"].ToString()));
					}
				}
			}
			// 検索条件の復帰
			int? iUserManItemEx1 = Session["ViComm_man_FindUser.lblUserManItemEx1"] as int?;
			if (iUserManItemEx1 != null) {
				this.lstUserManItemEx1.SelectedIndex = iUserManItemEx1.Value;
			}

			if (sessionWoman.IsValidMask(sessionWoman.userWoman.CurCharacter.userDefineMask,PwViCommConst.CastUserDefFlag.LOGIN_USER_DISABLED)) {
				rdoStatus.Items.Remove(rdoStatus.Items[1]);
			}
		} else {
			Session["ViComm_man_FindUser.lblUserManItemEx1"] = lstUserManItemEx1.SelectedIndex;
		}
		base.OnLoad(e);
	}
	protected override void AppendSearchCondtion(ref string pUrl, int pIndex) {
		// 地域
		pUrl += string.Format("&item{0:D2}={1}&typeseq{0:D2}=400&group{0:D2}=&like{0:D2}=0", pIndex, lstUserManItemEx1.Items[lstUserManItemEx1.SelectedIndex].Value);
	}
	
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblOnline" runat="server" BreakAfter="true">
		</cc1:iBMobileLabel>
		<mobile:SelectionList ID="rdoStatus" Runat="server" SelectType="Radio" Alignment="Left">
			<Item Text="全ての会員" Value="0" />
			<Item Text="$PGM_HTML02;" Value="2" />
		</mobile:SelectionList>
		<mobile:SelectionList ID="chkNewCast" Runat="server" SelectType="CheckBox" Alignment="Left">
			<Item Text="$PGM_HTML05;" Value="1" Selected="false" />
		</mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagLinesHandelNm" runat="server" Text="$PGM_HTML03;" />

		<cc1:iBMobileLabel ID="lblMailSendCount" runat="server" BreakAfter="true">ﾒｰﾙ送信回数</cc1:iBMobileLabel>
		<mobile:SelectionList ID="chkMailSendCount" runat="server" SelectType="CheckBox" Alignment="Left">
			<Item Text="未送信の会員のみ" Value="0" Selected="false" />
		</mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagLinesMailSendCount" runat="server" Text="$PGM_HTML03;" />

		<mobile:SelectionList ID="lstAge" Runat="server" SelectType="DropDown">
			<Item Value=":" Text="--年齢--"></Item>
			<Item Value=":17" Text="18歳未満"></Item>
			<Item Value="18:20" Text="18〜20歳"></Item>
			<Item Value="21:24" Text="21〜24歳"></Item>
			<Item Value="25:29" Text="25〜29歳"></Item>
			<Item Value="30:34" Text="30〜34歳"></Item>
			<Item Value="35:39" Text="35〜39歳"></Item>
			<Item Value="40:44" Text="40〜44歳"></Item>
			<Item Value="45:49" Text="45〜49歳"></Item>
			<Item Value="50:" Text="50歳以上"></Item>
		</mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagLinesAge" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">誕生日</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayFromMM" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel1" runat="server" BreakAfter="false">月</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayFromDD" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel2" runat="server" BreakAfter="true">日から</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayToMM" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel3" runat="server" BreakAfter="false">月</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstBirthdayToDD" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="IBMobileLabel4" runat="server" BreakAfter="false">日まで</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagLinesBirthday" runat="server" Text="$PGM_HTML03;" />
		<mobile:Panel ID="pnlUserManItem1" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem1" runat="server" BreakAfter="true">userManItemm01</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem1" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem1" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItemEx1" Runat="server">
			<mobile:SelectionList ID="lstUserManItemEx1" Runat="server">
				<Item Text="--地域--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLinesEx1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem15" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem15" runat="server" BreakAfter="true" Visible="false">userManItemm15</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem15" Runat="server">
				<Item Text="--性別--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem15" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem2" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem2" runat="server" BreakAfter="true" Visible="false">userManItemm02</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem2" Runat="server">
				<Item Text="--血液型--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem2" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines2" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem4" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem4" runat="server" BreakAfter="true" Visible="false">userManItemm04</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem4" Runat="server">
				<Item Text="--年収--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem4" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines4" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem3" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem3" runat="server" BreakAfter="true" Visible="false">userManItemm03</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem3" Runat="server">
				<Item Text="--仕事--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem3" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines3" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem5" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem5" runat="server" BreakAfter="true" Visible="false">userManItemm05</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem5" Runat="server">
				<Item Text="--休日--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem5" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines5" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem6" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem6" runat="server" BreakAfter="true" Visible="false">userManItemm06</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem6" Runat="server">
				<Item Text="--趣味--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem6" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines6" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem7" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem7" runat="server" BreakAfter="true" Visible="false">userManItemm07</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem7" Runat="server">
				<Item Text="--好みのｱｲﾄﾞﾙの年齢--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem7" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines7" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem8" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem8" runat="server" BreakAfter="true" Visible="false">userManItemm08</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem8" Runat="server">
				<Item Text="--好きなｽﾀｲﾙ--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem8" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines8" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem9" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem9" runat="server" BreakAfter="true" Visible="false">userManItemm09</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem9" Runat="server">
				<Item Text="--好きなﾀｲﾌﾟ--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem9" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines9" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem10" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem10" runat="server" BreakAfter="true" Visible="false">userManItemm10</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem10" Runat="server">
				<Item Text="--女性としたい事--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem10" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem11" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem11" runat="server" BreakAfter="true" Visible="false">userManItemm11</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem11" Runat="server">
				<Item Text="--恋人いない歴--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem11" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem12" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem12" runat="server" BreakAfter="true" Visible="false">userManItemm12</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem12" Runat="server">
				<Item Text="--sex経験--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem12" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem13" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem13" runat="server" BreakAfter="true" Visible="false">userManItemm13</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem13" Runat="server">
				<Item Text="--ｻｲﾄをよく利用する時間帯--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem13" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastNm" Runat="server">
			<cc1:iBMobileLabel ID="lblHandelNm" runat="server" BreakAfter="true">名前に</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12" BreakAfter="false"></cc1:iBMobileTextBox>
			を含む
		<cc1:iBMobileLiteralText ID="tagLinesOnline" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem14" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem14" runat="server" BreakAfter="true" Visible="false">userManItemm14</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagEmojiUserManItem10" runat="server" Text="ﾌﾘｰﾜｰﾄﾞ検索<br/>" />
			<mobile:SelectionList ID="lstUserManItem14" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem14" runat="server" MaxLength="300" Size="12" BreakAfter="false"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagUserManItem10" runat="server" Text="を含む<br/>↑会員のﾌﾟﾛﾌｺﾒﾝﾄより検索<br/>" />
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem16" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem16" runat="server" BreakAfter="true">userManItemm16</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem16" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem16" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem17" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem17" runat="server" BreakAfter="true">userManItemm17</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem17" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem17" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem18" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem18" runat="server" BreakAfter="true">userManItemm18</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem18" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem18" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem19" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem19" runat="server" BreakAfter="true">userManItemm19</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem19" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem19" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem20" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem20" runat="server" BreakAfter="true">userManItemm20</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstUserManItem20" Runat="server">
				<Item Text="指定なし" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtUserManItem20" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<br />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
