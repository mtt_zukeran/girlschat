﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendBbsObj.aspx.cs" Inherits="ViComm_woman_SendBbsObj" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server"><cc1:iBMobileLiteralText ID="tag01" runat="server" /> 
		$PGM_HTML01; 
		<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML02;
		<mobile:SelectionList ID="rdoBbsObj" Runat="server" SelectType="Radio" Alignment="Left">
		   <Item Text="$PGM_HTML03;" Value="0" Selected="true" />
		   <Item Text="$PGM_HTML04;" Value="1" Selected="False" />
		</mobile:SelectionList>
		$PGM_HTML05;
		<mobile:Panel ID="pnlSearchList" Runat="server">
			<mobile:SelectionList ID="lstGenreSelect" Runat="server" SelectType="DropDown"></mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML06;
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
		$PGM_HTML07;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		$PGM_HTML08;
		<mobile:SelectionList ID="lstRankingFlag" Runat="server" SelectType="Radio" Alignment="Left">
		   <Item Text="参加する" Value="1" Selected="true" />
		   <Item Text="参加しない" Value="0" Selected="False" />
		</mobile:SelectionList>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
