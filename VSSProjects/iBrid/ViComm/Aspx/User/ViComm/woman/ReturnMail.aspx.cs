/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 返信メール送信
--	Progaram ID		: ReturnMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/08/14  i-Brid(Y.Inoue)

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text.RegularExpressions;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ReturnMail:MobileMailWomanPage {
	private static readonly Regex oReg = new Regex("<(\"[^\"]*\"|'[^']*'|[^'\">])*(>)",RegexOptions.IgnoreCase | RegexOptions.Compiled);

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
		MailTemplate = lstTemplate;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.pnlTemplate.Visible = !oManageCompany.IsAvailableService(ViCommConst.RELEASE_TX_MAIL_DISABLE_TEMPLATE);
		}

		if (!IsPostBack) {
			if (iBridUtil.GetStringValue(this.Request.QueryString["protect"]).Equals(ViCommConst.FLAG_ON_STR)) {
				this.ProtectRxMail();
			} else {
				if (IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL, 2)) {
					lblTitle.Visible = false;
					txtTitle.Visible = false;
				}
				txtTitle.Text = GetMailTitle();
				if (IsAvailableService(ViCommConst.RELEASE_RETURN_MAIL_USE_QUOTE)) {
					txtTitle.Text += sessionWoman.GetMailValue("MAIL_TITLE");
					string sMailDoc = sessionWoman.GetMailValue("MAIL_DOC1") + sessionWoman.GetMailValue("MAIL_DOC2") + sessionWoman.GetMailValue("MAIL_DOC3") + sessionWoman.GetMailValue("MAIL_DOC4") + sessionWoman.GetMailValue("MAIL_DOC5");

					sMailDoc = oReg.Replace(sMailDoc,string.Empty);

					txtDoc.Text += "\r\n\r\n>" + sMailDoc.Replace("\r\n","\r\n>");
				}
				SetMailTemplate(true,ViCommConst.TxMailLayOutType.NORMAL_MAIL);

				//			未使用のためｺﾒﾝﾄｱｳﾄ
				//			sessionWoman.userWoman.GetCharacterList(sessionWoman.site.siteCd);

				string sMailDataSeq = iBridUtil.GetStringValue(Request.QueryString["data"]);
				if (sMailDataSeq.Equals(string.Empty)) {
					sessionWoman.seekMode = ViCommConst.INQUIRY_RX_USER_MAIL_BOX.ToString();
					sMailDataSeq = iBridUtil.GetStringValue(sessionWoman.userWoman.NextMailDataSeq());

					sessionWoman.userWoman.mailData.Add(sMailDataSeq,new MailInfo());
					
					string sMailSeq = string.Empty;
					string sLoginId = string.Empty;

					if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["loginid"]))) {
						sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
						if (sessionWoman.SetMailDataSetByRecentMail(sLoginId) == false) {
							if (sessionWoman.logined) {
								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sessionWoman.site.userTopId));
							} else {
								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sessionWoman.site.nonUserTopId));
							}
						}
						sMailSeq = sessionWoman.GetMailValue("MAIL_SEQ");
					} else {
						sMailSeq = iBridUtil.GetStringValue(Request.QueryString["mailseq"]);
						if (sessionWoman.SetMailDataSetByMailSeq(sMailSeq) == false) {
							if (sessionWoman.logined) {
								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sessionWoman.site.userTopId));
							} else {
								RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sessionWoman.site.nonUserTopId));
							}
						}
					}

					string sUserSeq = sessionWoman.GetMailValue("TX_USER_SEQ");

					// ﾒｰﾙからのﾀﾞｲﾚｸﾄ返信
					sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq = sUserSeq;
					sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo = ViCommConst.MAIN_CHAR_NO;
					sessionWoman.userWoman.mailData[sMailDataSeq].returnMailSeq = sMailSeq;
					sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId = sessionWoman.GetMailValue("LOGIN_ID");

					string sReadFlag = sessionWoman.GetMailValue("READ_FLAG");
					if (sReadFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailReadFlag(sMailSeq);
						}
					}
				} else {
					txtTitle.Text = sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle;
					txtDoc.Text = sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc;

					sessionWoman.SetMailDataSetByMailSeq(sessionWoman.userWoman.mailData[sMailDataSeq].returnMailSeq);
				}
				ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;

				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["loginid"]))) {
					sessionWoman.userWoman.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_ON;
				} else {
					sessionWoman.userWoman.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_OFF;
				}

				// 相手からの拒否チェック
				CheckRefuse(sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo);

				// 自分が拒否しているチェック
				using (Refuse oRefuse = new Refuse()) {
					if (oRefuse.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo)) {
						this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_MAN);
					}
				}

				using (Man oMan = new Man()) {
					using (DataSet ds = oMan.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,1)) {
						if (ds.Tables[0].Rows.Count != 0) {
							string sUserStatus = ds.Tables[0].Rows[0]["USER_STATUS"].ToString();
							if (sUserStatus.Equals(ViCommConst.USER_MAN_HOLD) || sUserStatus.Equals(ViCommConst.USER_MAN_RESIGNED) || sUserStatus.Equals(ViCommConst.USER_MAN_STOP) || sUserStatus.Equals(ViCommConst.USER_MAN_BLACK)) {
								RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_MAN);
							}
							sessionWoman.userWoman.mailData[sMailDataSeq].rxUserNm = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["HANDLE_NM"]);
						}
					}
				}
				CheckOnlineStatus(sMailDataSeq,false);

				//メールアタック(新人)対象チェック
				sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag = ViCommConst.FLAG_OFF;
				using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
					string sResult = oMailAttackNew.RegistMailAttackNewTx(
														sessionWoman.site.siteCd,
														sessionWoman.userWoman.userSeq,
														sessionWoman.userWoman.curCharNo,
														sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,
														sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo,
														string.Empty,
														ViCommConst.FLAG_ON
													);
					if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
						sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag = ViCommConst.FLAG_ON;
					}
				}
				sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackNewFlag = sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag;

				//メールアタック(ログイン)対象チェック
				sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag = ViCommConst.FLAG_OFF;
				using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
					string sResult = oMailAttackLogin.RegistMailAttackLoginTx(
														sessionWoman.site.siteCd,
														sessionWoman.userWoman.userSeq,
														sessionWoman.userWoman.curCharNo,
														sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,
														sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo,
														string.Empty,
														ViCommConst.FLAG_ON
													);
					if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
						sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag = ViCommConst.FLAG_ON;
					}
				}
				sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackLoginFlag = sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag;

				// 3分以内メール返信ボーナス用の値を取得
				MailResBonus2 oMailResBonus2 = new MailResBonus2();
				sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus = oMailResBonus2.SetEventData(ref sessionWoman.userWoman);
				// メール返信ボーナス対象チェック
				sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag = ViCommConst.FLAG_OFF_STR;
				sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag = ViCommConst.FLAG_OFF_STR;
				if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus)) {
					MailResBonusLog2 oMailResBonusLog2 = new MailResBonusLog2();
					string pMainFlag;
					string pSubFlag;
					oMailResBonusLog2.CheckUser(
						sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusSeq,
						sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec1,
						sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec2,
						sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusMaxNum,
						sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusUnreceivedDays,
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,
						sessionWoman.userWoman.userSeq,
						out pMainFlag,
						out pSubFlag
					);
					if (pMainFlag.Equals(ViCommConst.FLAG_ON_STR)) {
						sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag = ViCommConst.FLAG_ON_STR;
					}
					if (pSubFlag.Equals(ViCommConst.FLAG_ON_STR)) {
						sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag = ViCommConst.FLAG_ON_STR;
					}
				}
				if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag)
					|| ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag)
				) {
					sessionWoman.userWoman.mailData[sMailDataSeq].mailResBonus2Flag = ViCommConst.FLAG_ON_STR;
				} else {
					sessionWoman.userWoman.mailData[sMailDataSeq].mailResBonus2Flag = ViCommConst.FLAG_OFF_STR;
				}
			}

		} else {
			if (iBridUtil.GetStringValue(Request.QueryString["direct"]).Equals("1")) {
				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["loginid"]))) {
					sessionWoman.SetMailDataSetByRecentMail(iBridUtil.GetStringValue(Request.QueryString["loginid"]));
				} else {
					sessionWoman.SetMailDataSetByMailSeq(iBridUtil.GetStringValue(Request.QueryString["mailseq"]));
				}
			}
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_CONFIRM] != null) {
				cmdConfirm_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_REGIST] != null) {
				cmdRegist_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true,false,ViCommConst.SCR_TX_REMAIL_COMPLITE_CAST);
	}

	virtual protected string GetMailTitle() {
		return "Re:";
	}

	protected void cmdConfirm_Click(object sender,EventArgs e) {
		Confirm(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true);
	}

	protected void cmdRegist_Click(object sender,EventArgs e) {
		MainteDraftMail(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true);
	}

	private void cmdDelete_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDelTxRx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,mailSeqList.ToArray());
			}
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("mail_delete_count",mailSeqList.Count.ToString());
			oParam.Add("loginid",iBridUtil.GetStringValue(this.Request.QueryString["loginid"]));
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_DELETED_MAIL_HISTORY,oParam);
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "ReturnMail.aspx?" + Request.QueryString));
		}
	}

	private void ProtectRxMail() {
		int iProtectFlag;
		int.TryParse(iBridUtil.GetStringValue(this.Request.QueryString["protectflag"]),out iProtectFlag);
		iProtectFlag = iProtectFlag == 1 ? 0 : 1;
		
		string sProtectMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["protectmailseq"]);
		string sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);
		string sListNo = iBridUtil.GetStringValue(this.Request.QueryString["listno"]);
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sProtectMailSeq,iProtectFlag);
			sessionWoman.SetMailValue("DEL_PROTECT_FLAG",iProtectFlag.ToString());
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   string.Format("ReturnMail.aspx?direct=1&scrid=01&stay=1&loginid={0}&listno={1}#{2}",sLoginId,sListNo,sProtectMailSeq)));
	}
}
