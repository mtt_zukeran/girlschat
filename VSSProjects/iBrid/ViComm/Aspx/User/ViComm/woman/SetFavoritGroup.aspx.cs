/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入りグループ設定
--	Progaram ID		: SetFavoritGroup
--
--  Creation Date	: 2012.10.06
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_SetFavoritGroup:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sGrpSeq = iBridUtil.GetStringValue(Request.QueryString["grpseq"]);
			string sSetGrpSeq = iBridUtil.GetStringValue(Request.QueryString["setgrpseq"]);
			SetSelectBox(sGrpSeq,sSetGrpSeq);
			sessionWoman.ControlList(this.Request,ViCommConst.INQUIRY_FAVORIT,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_SEARCH] != null) {
				cmdSearch_Click(sender,e);
			}
		}
	}

	private void SetSelectBox(string pGrpSeq,string pSetGrpSeq) {
		int iAllUserCount = 0;
		int iNullUserCount = 0;
		string sListText = string.Empty;

		using (Favorit oFavorit = new Favorit()) {
			iAllUserCount = oFavorit.GetSelfFavoritCount(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR
			);
		}

		iNullUserCount = iAllUserCount;

		DataSet oDataSet = GetFavoritGroup();
		Regex regex2 = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			iNullUserCount = iNullUserCount - int.Parse(dr["USER_COUNT"].ToString());
			sListText = string.Format("{0}({1})",regex2.Replace(iBridUtil.GetStringValue(dr["FAVORIT_GROUP_NM"]),string.Empty),iBridUtil.GetStringValue(dr["USER_COUNT"]));
			lstFavoritGroupSearch.Items.Add(new MobileListItem(sListText,iBridUtil.GetStringValue(dr["FAVORIT_GROUP_SEQ"])));
			lstFavoritGroupSet.Items.Add(new MobileListItem(sListText,iBridUtil.GetStringValue(dr["FAVORIT_GROUP_SEQ"])));
		}

		lstFavoritGroupSearch.Items.Insert(0,new MobileListItem(string.Format("すべて({0})",iAllUserCount.ToString()),string.Empty));
		lstFavoritGroupSearch.Items.Insert(1,new MobileListItem(string.Format("ｸﾞﾙｰﾌﾟ未設定({0})",iNullUserCount.ToString()),"null"));
		lstFavoritGroupSet.Items.Insert(0,new MobileListItem(string.Format("ｸﾞﾙｰﾌﾟ未設定({0})",iNullUserCount.ToString()),string.Empty));

		foreach (MobileListItem item in lstFavoritGroupSearch.Items) {
			if (item.Value.Equals(pGrpSeq)) {
				item.Selected = true;
			}
		}

		foreach (MobileListItem item in lstFavoritGroupSet.Items) {
			if (item.Value.Equals(pSetGrpSeq)) {
				item.Selected = true;
			}
		}
	}

	private DataSet GetFavoritGroup() {
		DataSet oDataSet;
		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oDataSet = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,1,99);
		}

		return oDataSet;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUserSeq;
		List<string> sPartnerUserSeq = new List<string>();
		List<string> sPartnerUserCharNo = new List<string>();

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.StartsWith("chkRnum") && Request.Form[sKey] != null) {
				sUserSeq = sKey.Replace("chkRnum","");

				if (Request.Form["user_char_no" + sUserSeq] != null) {
					sPartnerUserSeq.Add(sUserSeq);
					sPartnerUserCharNo.Add(Request.Form["user_char_no" + sUserSeq]);
				}
			}
		}

		if (sPartnerUserSeq.Count > 0 && sPartnerUserCharNo.Count > 0) {
			string sResult;

			using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
				sResult = oFavoritGroup.SetFavoritGroup(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sPartnerUserSeq.ToArray(),
					sPartnerUserCharNo.ToArray(),
					lstFavoritGroupSet.Items[lstFavoritGroupSet.SelectedIndex].Value
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				IDictionary<string,string> oParam = new Dictionary<string,string>();
				oParam.Add("count",sPartnerUserSeq.Count.ToString());
				oParam.Add("grpseq",lstFavoritGroupSet.Items[lstFavoritGroupSet.SelectedIndex].Value);
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_SET_FAVORIT_GROUP_COMPLETE,oParam);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}

	protected void cmdSearch_Click(object sender,EventArgs e) {
		UrlBuilder oUrlBuilder = new UrlBuilder("SetFavoritGroup.aspx");
		oUrlBuilder.Parameters.Add("grpseq",lstFavoritGroupSearch.Items[lstFavoritGroupSearch.SelectedIndex].Value);
		oUrlBuilder.Parameters.Add("setgrpseq",iBridUtil.GetStringValue(Request.QueryString["setgrpseq"]));
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrlBuilder.ToString()));
	}
}
