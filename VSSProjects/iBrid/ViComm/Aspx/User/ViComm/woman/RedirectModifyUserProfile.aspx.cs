/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフ設定リダイレクト
--	Progaram ID		: RedirectModifyUserProfile
--  Creation Date	: 2014.01.15
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RedirectModifyUserProfile:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!sessionWoman.logined) {
			if (!string.IsNullOrEmpty(sessionWoman.userWoman.tempRegistId)) {
				bool bExist = false;
				string sLoginId;
				string sLoginPassword;

				using (TempRegist oTempRegist = new TempRegist()) {
					bExist = oTempRegist.GetLoginIdPass(sessionWoman.userWoman.tempRegistId,out sLoginId,out sLoginPassword);
				}

				if (bExist) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("LoginUser.aspx?loginid={0}&password={1}&goto=ModifyUserProfile.aspx",sLoginId,sLoginPassword)));
					return;
				}
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("ModifyUserProfile.aspx"));
			return;
		}
	}
}
