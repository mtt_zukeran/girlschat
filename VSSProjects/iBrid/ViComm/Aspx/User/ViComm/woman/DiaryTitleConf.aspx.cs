/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ハンドルネーム変更
--	Progaram ID		: DiaryTitleConf
--
--  Creation Date	: 2010.11.08
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_DiaryTitleConf : MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
					txtDiaryTitle.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].diaryTitle;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

        if (txtDiaryTitle.Visible) {
            if (txtDiaryTitle.Text.Equals(string.Empty)) {
				bOk = false;
                lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),"ﾌﾞﾛｸﾞﾀｲﾄﾙ");
			}
		}

		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
            if (sessionWoman.ngWord.VaidateDoc(txtDiaryTitle.Text, out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (bOk == false) {
			return;
		} else {
			string sResult;

            sessionWoman.userWoman.UpdateDiaryTitle(
                sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtDiaryTitle.Text)),
				out sResult
			);
			if (sResult.Equals("0")) {
                RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType, Session.SessionID, "ListPersonalDiary.aspx"));
			} else {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
			}
		}
	}

}
