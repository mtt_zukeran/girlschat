<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyGameUserProfile.aspx.cs" Inherits="ViComm_woman_ModifyGameUserProfile" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="cc1" Namespace="MobileLib" Assembly="MobileLib" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<mobile:Panel ID="pnlHandleNm" Runat="server">
			<cc1:iBMobileLabel ID="lblHandelNm" runat="server">ﾊﾝﾄﾞﾙ名</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="20"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines01" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlBirthday" Runat="server">
			<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">$xE686;生年月日</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagBirthdayHint1" runat="server" Text="<font color=#FF6666>※男性へは年齢のみ表示。</font><br>" />
			<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">年</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">月</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblDay" runat="server">日</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagBirthdayHint2" runat="server" Text="$NBSP;<font color=#FF6666>▼</font><font color=gray>設定する年齢のﾋﾝﾄです♪</font><br>$NBSP;" />
			<mobile:SelectionList ID="lstAgeMainteHint" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines02" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman1" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem1" runat="server">UserWoman01</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem1" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem1" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem1" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines05" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman2" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem2" runat="server">UserWoman02</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem2" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem2" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem2" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines06" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman3" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem3" runat="server">UserWoman03</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem3" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem3" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem3" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines07" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman4" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem4" runat="server">UserWoman04</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem4" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem4" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem4" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines08" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman5" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem5" runat="server">UserWoman05</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem5" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem5" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem5" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines09" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman6" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem6" runat="server">UserWoman06</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem6" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem6" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem6" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman7" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem7" runat="server">UserWoman07</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem7" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem7" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem7" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman8" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem8" runat="server">UserWoman08</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem8" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem8" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem8" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman9" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem9" runat="server">UserWoman09</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem9" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem9" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem9" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman10" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem10" runat="server">UserWoman10</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem10" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem10" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem10" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman11" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem11" runat="server">UserWoman11</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem11" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem11" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem11" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman12" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem12" runat="server">UserWoman12</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem12" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem12" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem12" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman13" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem13" runat="server">UserWoman13</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem13" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem13" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem13" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman14" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem14" runat="server">UserWoman14</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem14" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem14" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem14" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman15" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem15" runat="server">UserWoman15</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem15" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem15" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem15" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman16" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem16" runat="server">UserWoman16</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem16" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem16" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem16" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman17" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem17" runat="server">UserWoman17</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem17" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem17" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem17" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines21" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman18" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem18" runat="server">UserWoman18</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem18" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem18" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem18" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines22" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman19" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem19" runat="server">UserWoman19</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem19" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem19" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem19" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines23" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserWoman20" Runat="server">
			<cc1:iBMobileLabel ID="lblUserWomanItem20" runat="server">UserWoman20</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserWomanItem20" runat="server" MaxLength="341" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserWomanItem20" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="341">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserWomanItem20" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines24" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCommentList" runat="server">
		    <cc1:iBMobileLabel ID="lblCommentList" runat="server" Visible="false">一覧用コメント</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagCommentList" runat="server" Text="$xE691;一言ｺﾒﾝﾄ(150文字以内)<br>$xE737;ｺﾒﾝﾄを記入すればﾓﾃ度は確実にup！誹謗中傷は書込禁止。<br>" />
		    <tx:TextArea ID="txtAreaCommentList" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" MaxLength="1333" EmojiPaletteEnabled="true">
		    </tx:TextArea>
		    <cc1:iBMobileLiteralText ID="tagLines03" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCommentDetail" Runat="server" Visible="false">
		    <cc1:iBMobileLabel ID="lblCommentDetail" runat="server">詳細用コメント</cc1:iBMobileLabel>
		    <tx:TextArea ID="txtAreaCommentDetail" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" MaxLength="1333" EmojiPaletteEnabled="true">
		    </tx:TextArea>
		    <cc1:iBMobileLiteralText ID="tagLines04" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlOkList" Runat="server">
			<cc1:iBMobileLabel ID="IBMobileLabel1" runat="server" BreakAfter="true" Font-Size="Small">OKﾘｽﾄ</cc1:iBMobileLabel>
			<mobile:SelectionList ID="chkOK0" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK1" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK2" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK3" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK4" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK5" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK6" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK7" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK8" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK9" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK10" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK11" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK12" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK13" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK14" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK15" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK16" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK17" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK18" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK19" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK20" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK21" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK22" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK23" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK24" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK25" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK26" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK27" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK28" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK29" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK30" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK31" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK32" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK33" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK34" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK35" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK36" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK37" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK38" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK39" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="False"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines25" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		$PGM_HTML02;
		$PGM_HTML03;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
