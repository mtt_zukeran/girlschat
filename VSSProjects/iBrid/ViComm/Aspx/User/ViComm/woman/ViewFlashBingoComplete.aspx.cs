/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: BINGO�B��Flash�\��
--	Progaram ID		: ViewFlashBingoComplete
--
--  Creation Date	: 2013.04.12
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_woman_ViewFlashBingoComplete:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionWoman.carrier.Equals(ViCommConst.ANDROID)) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else if (sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else {
			string sNextUrl = iBridUtil.GetStringValue(this.Request.QueryString["next_url"]);

			if (string.IsNullOrEmpty(sNextUrl)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}

			string sFlashDir = Path.Combine(sessionWoman.site.webPhisicalDir,PwViCommConst.MAIL_DE_BINGO_DIR);
			string sSwfFilePath = Path.Combine(sFlashDir,GetBingoCompleteSwfFileNm());
			byte[] bufTempSwf;

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!File.Exists(sSwfFilePath)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}

				bufTempSwf = FileHelper.GetFileBuffer(sSwfFilePath);
			}

			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("next_url",sNextUrl);
			byte[] oSwfBuffer = GameFlashLiteHelper.RewriteSwfBuffer(bufTempSwf,oParam);
			GameFlashLiteHelper.OutputSwf(oSwfBuffer);
		}
	}

	private string GetBingoCompleteSwfFileNm() {
		string sFileNm = string.Empty;

		Random oRandom = new System.Random();
		int iResult = oRandom.Next(1,6);

		sFileNm = string.Format("bingo_complete{0:d2}.swf",iResult);

		return sFileNm;
	}
}
