/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ お宝ガチャFlashプレビュー
--	Progaram ID		: ViewFlashCastPicGacha
--
--  Creation Date	: 2013.01.15
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_woman_ViewFlashCastPicGacha:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionWoman.carrier.Equals(ViCommConst.ANDROID)) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else if (sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else {
			string sNextUrl = string.Empty;
			string sPicSeq = iBridUtil.GetStringValue(this.Request.QueryString["pic_seq"]);
			string sWebPhisicalDir = sessionWoman.site.webPhisicalDir;
			string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.FLASH_DIR);
			string sXmlPath = Path.Combine(sFlashDir,"get_photo.xml");
			string sObjPhotoImgPath = string.Empty;
			string sObjPicDoc = string.Empty;
			DataSet oDataSet = null;
			
			using (CastPic oCastPic = new CastPic()) {
				oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);
			}
			
			if (oDataSet.Tables[0].Rows.Count > 0) {
				if (!oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString().Equals(sessionWoman.userWoman.userSeq) || !oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString().Equals(sessionWoman.userWoman.curCharNo)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}

				sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]));
				sObjPicDoc = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]);
			}

			string sXmlData = string.Empty;
			string sObjPhotoImgBase64 = string.Empty;

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!File.Exists(sObjPhotoImgPath)) {
					sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,PwViCommConst.NoPicJpgPath);
				}

				//XMLファイル読み込み
				using (StreamReader srXml = new StreamReader(sXmlPath)) {
					sXmlData = srXml.ReadToEnd();
				}

				//お宝写真読み込み
				byte[] bufObjPhotoImg = FileHelper.GetFileBuffer(sObjPhotoImgPath);
				sObjPhotoImgBase64 = Convert.ToBase64String(bufObjPhotoImg);

				//コメントの改行コード変換
				sObjPicDoc = sObjPicDoc.Replace("\r\n","&#13;");

				sXmlData = sXmlData.Replace("%IMAGE%",sObjPhotoImgBase64);
				sXmlData = sXmlData.Replace("%COMMENT%",sObjPicDoc);
			}

			string sTempXml = Path.GetTempFileName();
			string sTempSwf = Path.GetTempFileName();

			StreamWriter sw = new StreamWriter(sTempXml,false,Encoding.UTF8);
			sw.Write(sXmlData);
			sw.Close();

			using (Process prExe = new Process()) {
				prExe.StartInfo.FileName = "D:\\swfmill\\swfmill.exe";
				prExe.StartInfo.Arguments = string.Format("-e cp932 xml2swf {0} {1}",sTempXml,sTempSwf);
				prExe.StartInfo.CreateNoWindow = true;//コンソールウィンドウを開かない
				prExe.StartInfo.UseShellExecute = false;//シェル機能を使用しない
				prExe.Start();
				prExe.WaitForExit(3000);
			}

			byte[] bufTempSwf = FileHelper.GetFileBuffer(sTempSwf);
			File.Delete(sTempXml);
			File.Delete(sTempSwf);

			IDictionary<string,string> oParam = this.CreateParam(this.Request.QueryString);

			byte[] oSwfBuffer = GameFlashLiteHelper.RewriteSwfBuffer(bufTempSwf,oParam);
			GameFlashLiteHelper.OutputSwf(oSwfBuffer);
		}
	}

	private IDictionary<string,string> CreateParam(NameValueCollection pQueryString) {
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		string sNextUrl = iBridUtil.GetStringValue(pQueryString["next_url"]);

		oParam.Add("nextUrl",sNextUrl);

		return oParam;
	}
}
