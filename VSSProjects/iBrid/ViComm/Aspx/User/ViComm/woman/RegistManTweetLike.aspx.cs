/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ 会員のつぶやきイイネ登録
--	Progaram ID		: RegistManTweetLike
--
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_RegistManTweetLike:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		string sManTweetSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetseq"]);

		if (sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_TWEET,this.ActiveForm)) {
			DataSet oDataSet;
			
			using (ManTweet oManTweet = new ManTweet()) {
				oDataSet = oManTweet.GetOne(sessionWoman.site.siteCd,sManTweetSeq);
			}
			
			if (oDataSet.Tables[0].Rows.Count == 0) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
			
			DataRow oDataRow = oDataSet.Tables[0].Rows[0];
			
			//自分が拒否している場合
			using (Refuse oRefuse = new Refuse()) {
				if (oRefuse.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]))) {
					this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_MAN);
				}
			}

			using (Favorit oFavorit = new Favorit()) {
				if (oFavorit.CheckJealousyDialy(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]))) {
					this.RedirectToDisplayDoc(PwViCommConst.SCR_JEAROUSY_MAN_TWEET_LIKE);
				}
			}

			int iCharacterOnlineStatus = sessionWoman.userWoman.GetOnlineStatus(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
			int iIsSeenOnlineStatus;
			using (Favorit oFavorit = new Favorit()) {
				iIsSeenOnlineStatus = oFavorit.GetIsSeenOnlineStatus(
										sessionWoman.site.siteCd,
										sessionWoman.userWoman.userSeq,
										sessionWoman.userWoman.curCharNo,
										iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),
										iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]),
										iCharacterOnlineStatus);
			}
			
			// 自分がトーク中
			if (iCharacterOnlineStatus == ViCommConst.USER_TALKING) {
				if (iIsSeenOnlineStatus != ViCommConst.USER_LOGINED) {
					this.RedirectToDisplayDoc(PwViCommConst.SCR_JEAROUSY_MAN_TWEET_LIKE);
				}
			}
			
			// 自分が相手から見てオフライン
			//自分が普通にオフライン（直ログインのときありえる） 
			if (iCharacterOnlineStatus == ViCommConst.USER_OFFLINE) {
				this.RedirectToDisplayDoc(PwViCommConst.SCR_JEAROUSY_MAN_TWEET_LIKE);
			}
			
			if (iIsSeenOnlineStatus == ViCommConst.USER_OFFLINE) {
				this.RedirectToDisplayDoc(PwViCommConst.SCR_JEAROUSY_MAN_TWEET_LIKE);
			}
			
			using (ManTweet oManTweet = new ManTweet()) {
				string sSiteCd = sessionWoman.site.siteCd;
				string sUserSeq = sessionWoman.userWoman.userSeq;
				string sUserCharNo = sessionWoman.userWoman.curCharNo;
				string sResult = string.Empty;
				oManTweet.RegistManTweetLike(sSiteCd,sUserSeq,sUserCharNo,sManTweetSeq,out sResult);
			}
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}