﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板レス返信書き込み
--	Progaram ID		: WriteBbsReturnRes
--
--  Creation Date	: 2011.04.12
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_WriteBbsReturnRes:MobileBbsExWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BBS_THREAD,ActiveForm)) {
				string sback = iBridUtil.GetStringValue(Request.QueryString["back"]);
				if (sback.Equals(ViCommConst.FLAG_ON_STR)) {
					txtBbsResHandleNm.Text = sessionWoman.userWoman.bbsInfo.bbsResHandleNm;
					txtDoc.Text = sessionWoman.userWoman.bbsInfo.bbsResDoc;
				} else {
					txtBbsResHandleNm.Text = sessionWoman.userWoman.CurCharacter.handleNm;
				}
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBbsThread.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {

		if (!ValidateText()) {
			return;
		} else {
			string sCheckHandleNmFlag = iBridUtil.GetStringValue(this.Request.Form["check_handle"]);
			if (sCheckHandleNmFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sessionWoman.userWoman.bbsInfo.bbsResHandleNm = HttpUtility.HtmlEncode(txtBbsResHandleNm.Text);
			}
			sessionWoman.userWoman.bbsInfo.bbsResDoc = HttpUtility.HtmlEncode(txtDoc.Text);
			string sUrl = string.Format("ConfirmWriteBbsReturnRes.aspx?bbsthreadseq={0}&subseq={1}",Request.QueryString["bbsthreadseq"],Request.QueryString["subseq"]);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sUrl));
		}
	}

	private bool ValidateText() {
		bool bOk = true;
		string sNgWord = string.Empty;
		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}
		string sCheckHandleNmFlag = iBridUtil.GetStringValue(this.Request.Form["check_handle"]);

		lblErrorMessage.Text = string.Empty;

		if (sCheckHandleNmFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			if (string.IsNullOrEmpty(txtBbsResHandleNm.Text)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			}
			if (txtBbsResHandleNm.Text.Length > 20) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_STATUS_BBS_RES_HANDLE_LENGTH);
			}
			if (!sessionWoman.ngWord.VaidateDoc(txtBbsResHandleNm.Text,out sNgWord)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_STATUS_BBS_RES_HANDLE_NG);
			}
		}

		if (string.IsNullOrEmpty(txtDoc.Text)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_RES_EMPTY);
		}
		if (txtDoc.Text.Length > 16000) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BBS_RES_LENGTH);
		}
		if (!sessionWoman.ngWord.VaidateDoc(txtDoc.Text,out sNgWord)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(string.Format(ViCommConst.ERR_STATUS_BBS_RES_NG,sNgWord));
		}

		return bOk;
	}
}
