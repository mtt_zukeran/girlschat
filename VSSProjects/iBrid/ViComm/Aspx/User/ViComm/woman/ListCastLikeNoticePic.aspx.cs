/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者画像新着いいね一覧
--	Progaram ID		: ListCastLikeNoticePic
--
--  Creation Date	: 2016.10.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListCastLikeNoticePic:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_LIKE_NOTICE_PIC,this.ActiveForm);
	}
}