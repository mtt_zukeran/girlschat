/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �f���ʐ^�E����{�������ꗗ
--	Progaram ID		: ListBbsViewHistory
--
--  Creation Date	: 2010.09.10
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListBbsViewHistory:MobileWomanPageBase {
	virtual protected void Page_Load(object sender,EventArgs e) {
		bool bDwonload = false;

		if (!IsPostBack) {
			string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);

			if (!sMovieSeq.Equals("")) {
				bDwonload = Download(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sMovieSeq);
			}
			if (bDwonload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
				sessionWoman.ControlList(Request,ViCommConst.INQUIRY_BBS_VIEW_HISTORY,ActiveForm);
			}
		}
	}

	private bool Download(string pUserSeq,string pUserCharNo,string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.MAN);
				return true;
			} else {
				return false;
			}
		}
	}
}
