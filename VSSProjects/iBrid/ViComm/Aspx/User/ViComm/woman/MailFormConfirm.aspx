<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MailFormConfirm.aspx.cs" Inherits="ViComm_woman_MailFormConfirm" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblConfirm" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
