/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 送信済みメール詳細表示
--	Progaram ID		: ViewTxMail
--
--  Creation Date	: 2010.05.26
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ViewTxMail:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		bool bDownload = false;

		if (!IsPostBack) {
			string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);

			if (!sMovieSeq.Equals("")) {
				bDownload = Download(sUserSeq,ViCommConst.MAIN_CHAR_NO,sMovieSeq);
			}
			if (bDownload == false) {
				Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

				if (sessionWoman.ControlList(Request,ViCommConst.INQUIRY_TX_USER_MAIL_BOX,ActiveForm)) {
					if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailDel(sessionWoman.GetMailValue("MAIL_SEQ"),ViCommConst.TX);
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_TX_MAIL + "&mail_delete_count=1"));
						}
					}
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListTxMailBox.aspx"));
				}
			}
		} else {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		}
	}

	private bool Download(string pUserSeq,string pUserCharNo,string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;
		
		string sFileNm, sFullPath, sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if(lSize==ViCommConst.GET_MOVIE_ERROR_VALUE){
			return false;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (lSize > 0) {
				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);

				return true;
			} else {
				return false;
			}
		}
	}
}
