/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り詳細
--	Progaram ID		: ViewFavorit
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListFavorit:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		DataSet oDataSet;
		string sPartnerUserSeq = string.Empty;
		string sPartnerUserCharNo = string.Empty;
		string sFavoritGroupSeq = string.Empty;

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,ViCommConst.INQUIRY_FAVORIT,ActiveForm,out oDataSet);

		if (oDataSet.Tables[0].Rows.Count != 0) {
			sPartnerUserSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["USER_SEQ"]);
			sPartnerUserCharNo = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"]);
			sFavoritGroupSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["FAVORIT_GROUP_SEQ"]);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			SetSelectBox(sFavoritGroupSeq);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DEFAULT] != null) {
				cmdDefault_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_EDIT] != null) {
				cmdEdit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo);
			}
		}
	}

	private void SetSelectBox(string sFavoritGroupSeq) {
		int iNullUserCount = 0;
		string sListText = string.Empty;

		lstFavoritGroup.Items.Clear();

		using (Favorit oFavorit = new Favorit()) {
			iNullUserCount = oFavorit.GetSelfFavoritCount(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				ViCommConst.OPERATOR
			);
		}

		DataSet oDataSet = GetFavoritGroup();
		Regex regex2 = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			iNullUserCount = iNullUserCount - int.Parse(dr["USER_COUNT"].ToString());
			sListText = string.Format("{0}({1})",regex2.Replace(iBridUtil.GetStringValue(dr["FAVORIT_GROUP_NM"]),string.Empty),iBridUtil.GetStringValue(dr["USER_COUNT"]));
			lstFavoritGroup.Items.Add(new MobileListItem(sListText,iBridUtil.GetStringValue(dr["FAVORIT_GROUP_SEQ"])));
		}

		lstFavoritGroup.Items.Insert(0,new MobileListItem(string.Format("ｸﾞﾙｰﾌﾟ未設定({0})",iNullUserCount.ToString()),string.Empty));

		foreach (MobileListItem item in lstFavoritGroup.Items) {
			if (item.Value.Equals(sFavoritGroupSeq)) {
				item.Selected = true;
			}
		}
	}

	private DataSet GetFavoritGroup() {
		DataSet oDataSet;
		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.SexCd = ViCommConst.OPERATOR;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oDataSet = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,1,99);
		}

		return oDataSet;
	}

	protected void cmdEdit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult;
		string[] sPartnerUserSeq = new string[] { pPartnerUserSeq };
		string[] sPartnerUserCharNo = new string[] { pPartnerUserCharNo };

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			sResult = oFavoritGroup.SetFavoritGroup(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sPartnerUserSeq,
				sPartnerUserCharNo,
				lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			string sFavoritGroupNm = lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Text;
			sFavoritGroupNm = System.Text.RegularExpressions.Regex.Replace(sFavoritGroupNm,"[(][0-9]+[)]","");
			sFavoritGroupNm = HttpUtility.UrlEncode(sFavoritGroupNm,Encoding.GetEncoding(932));
			UrlBuilder oUrlBuilder = new UrlBuilder("ViewFavorit.aspx");

			foreach (string sKey in Request.QueryString.AllKeys) {
				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString[sKey])) && !sKey.Equals("edit") && !sKey.Equals("grpnm")) {
					oUrlBuilder.AddParameter(sKey,iBridUtil.GetStringValue(Request.QueryString[sKey]));
				}
			}

			oUrlBuilder.AddParameter("edit","1");
			oUrlBuilder.AddParameter("grpnm",sFavoritGroupNm);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(oUrlBuilder.ToString() + "#no_jealousy"));
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iLoginViewStatus = 0;
		int iWaitingViewStatus = 0;
		int iBbsNonPublishFlag = 0;
		int iRankingNonPublishFlag = 0;
		int iNotSendBatchMailFlag = 0;
		int iBlogNonPublishFlag = 0;
		int iDiaryNonPublishFlag = 0;
		int iProfilePicNonPublishFlag = 0;
		int iUseCommentDetailFlag = 0;

		string sPartnerUserSeq = sessionWoman.GetManValue("USER_SEQ");
		string sPartnerUserCharNo = sessionWoman.GetManValue("USER_CHAR_NO");

		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_login"]),out iLoginViewStatus);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_waiting"]),out iWaitingViewStatus);
		if (this.Request.Form["jealousy_waiting"] == null) {
			iWaitingViewStatus = iLoginViewStatus;
		}
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_bbs"]),out iBbsNonPublishFlag);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_ranking"]),out iRankingNonPublishFlag);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_batch"]),out iNotSendBatchMailFlag);
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_blog"]),out iBlogNonPublishFlag);

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_DIARY,2)) {
				int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_diary"]),out iDiaryNonPublishFlag);
			}
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_ENABLED_JEALOUSY_PROFILE_PIC,2)) {
				int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_profile_pic"]),out iProfilePicNonPublishFlag);
			}
		}
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["jealousy_comment_detail"]),out iUseCommentDetailFlag);

		bool bOk = true;
		//やきもち状態ﾁｪｯｸ(片方だけｵﾌﾗｲﾝに見せている場合ｴﾗｰではじく) 
		//待機：待機0 ﾛｸﾞｲﾝ2 ｵﾌﾗｲﾝ1 
		//ﾛｸﾞｲﾝ：ﾛｸﾞｲﾝ0 ｵﾌﾗｲﾝ1 

		// 一方でもｵﾌﾗｲﾝの場合、両方ｵﾌﾗｲﾝに設定
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_JEALOUSY_OFFLINE_ANY,2)) {
				if (iWaitingViewStatus == 1 || iLoginViewStatus == 1) {
					iWaitingViewStatus = 1;
					iLoginViewStatus = 1;
				}
			}
		}

		if ((iWaitingViewStatus == 0 || iWaitingViewStatus == 2) && iLoginViewStatus == 1) {
			bOk = false;
		}
		if (iWaitingViewStatus == 1 && iLoginViewStatus == 0) {
			bOk = false;
		}

		if (bOk) {
			using (Favorit oFavorit = new Favorit()) {
				oFavorit.JealousyMainte(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sPartnerUserSeq,sPartnerUserCharNo,iLoginViewStatus,iWaitingViewStatus,iBbsNonPublishFlag,iRankingNonPublishFlag,iNotSendBatchMailFlag,iBlogNonPublishFlag,iDiaryNonPublishFlag,iProfilePicNonPublishFlag,iUseCommentDetailFlag,null,string.Empty);
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ViewFavorit.aspx" + Request.Url.Query.Replace("&update=1",string.Empty) + "&update=1"));
		} else {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_JEALOUSY);
		}
	}

	protected void cmdDefault_Click(object sender,EventArgs e) {
		string sPartnerUserSeq = sessionWoman.GetManValue("USER_SEQ");
		string sPartnerUserCharNo = sessionWoman.GetManValue("USER_CHAR_NO");

		using (Favorit oFavorit = new Favorit()) {
			oFavorit.JealousyMainte(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sPartnerUserSeq,sPartnerUserCharNo,0,0,0,0,0,0,0,0,0,null,string.Empty);
		}
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ViewFavorit.aspx" + Request.Url.Query.Replace("&update=1",string.Empty) + "&update=1"));
	}
}
