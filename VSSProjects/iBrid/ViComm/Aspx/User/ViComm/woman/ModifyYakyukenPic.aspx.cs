/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳画像編集
--	Progaram ID		: ModifyYakyukenPic
--
--  Creation Date	: 2013.05.01
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ModifyYakyukenPic:MobileWomanPageBase {
	private string sYakyukenPicSeq;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		sYakyukenPicSeq = iBridUtil.GetStringValue(Request.QueryString["ypicseq"]);

		if (string.IsNullOrEmpty(sYakyukenPicSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			DataSet dsYakyukenPic;
			if (sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_YAKYUKEN_PIC_VIEW,this.ActiveForm,out dsYakyukenPic)) {
				if (iBridUtil.GetStringValue(dsYakyukenPic.Tables[0].Rows[0]["AUTH_FLAG"]).Equals(ViCommConst.FLAG_OFF_STR)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ViewYakyukenPic.aspx?ypicseq={0}",sYakyukenPicSeq)));
				}

				ViewState["YAKYUKEN_GAME_SEQ"] = iBridUtil.GetStringValue(dsYakyukenPic.Tables[0].Rows[0]["YAKYUKEN_GAME_SEQ"]);
				ViewState["PIC_SEQ"] = iBridUtil.GetStringValue(dsYakyukenPic.Tables[0].Rows[0]["PIC_SEQ"]);
				txtCommentText.Text = iBridUtil.GetStringValue(dsYakyukenPic.Tables[0].Rows[0]["COMMENT_TEXT"]);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sYakyukenGameSeq = iBridUtil.GetStringValue(ViewState["YAKYUKEN_GAME_SEQ"]);
		string sPicSeq = iBridUtil.GetStringValue(ViewState["PIC_SEQ"]);

		if (string.IsNullOrEmpty(sYakyukenGameSeq) || string.IsNullOrEmpty(sPicSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (CheckInputValue()) {
			string sResult = string.Empty;

			using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
				oYakyukenPic.ModifyYakyukenPic(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sYakyukenPicSeq,
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtCommentText.Text)),
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				string sRotateType = iBridUtil.GetStringValue(this.Request.Params["rotate_type"]);
				RotateFlipType oRoateType = ImageUtil.Convert2RotateFlipType(sRotateType);
				string sWebPhisicalDir = string.Empty;
				using (Site oSite = new Site()) {
					oSite.GetValue(sessionWoman.site.siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
				}
				string sCastPicDir = ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sessionWoman.site.siteCd,sessionWoman.userWoman.loginId);
				ImageUtil.RotateFilp(sCastPicDir,sPicSeq,oRoateType);

				IDictionary<string,string> oParam = new Dictionary<string,string>();
				oParam.Add("ygameseq",sYakyukenGameSeq);
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_MODIFY_YAKYUKEN_PIC_COMPLETE,oParam);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}

	protected bool CheckInputValue() {
		bool bOk = true;
		string sNGWord;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtCommentText.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄを入力して下さい。";
		} else if (txtCommentText.Text.Length < 10) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は10文字以上です。";
		} else if (txtCommentText.Text.Length > 50) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は50文字以内です。";
		} else if (sessionWoman.ngWord.VaidateDoc(txtCommentText.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄに禁止語句が含まれています。";
		}

		return bOk;
	}
}
