/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者無料動画詳細
--	Progaram ID		: ViewCastProfileMovie
--  Creation Date	: 2014.06.05
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewCastProfileMovie:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;

		if (!IsPostBack) {
			DataSet dsCastMovie;

			if (sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_CAST_MOVIE_VIEW,ActiveForm,out dsCastMovie)) {
				string sUserSeq = iBridUtil.GetStringValue(dsCastMovie.Tables[0].Rows[0]["USER_SEQ"]);
				string sUserCharNo = iBridUtil.GetStringValue(dsCastMovie.Tables[0].Rows[0]["USER_CHAR_NO"]);
				string sMovieType = iBridUtil.GetStringValue(dsCastMovie.Tables[0].Rows[0]["MOVIE_TYPE"]);

				if (!sMovieType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
					return;
				}

				if (sPlayMovie.Equals(ViCommConst.FLAG_ON_STR) && !string.IsNullOrEmpty(sMovieSeq) && !string.IsNullOrEmpty(sUserSeq) && !string.IsNullOrEmpty(sUserCharNo)) {
					bDownload = MovieHelper.Download(sMovieSeq,sUserSeq,sUserCharNo);
				}
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}
		}

		if (bDownload == false) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		}
	}
}
