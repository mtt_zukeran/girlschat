/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ｿｰｼｬﾙｹﾞｰﾑ用Flash(ﾅﾝﾊﾟ時お宝GET女性用)
--	Progaram ID		: ViewGameFlashBattle
--
--  Creation Date	: 2012.01.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_ViewGameFlashGetCastTreasure:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sTreasureCompleteFlag = iBridUtil.GetStringValue(this.Request.QueryString["treasure_complete_flag"]);
		string sTownFirstClearFlag = iBridUtil.GetStringValue(this.Request.QueryString["town_first_clear_flag"]);
		string sLevelUpFlag = iBridUtil.GetStringValue(this.Request.QueryString["level_up_flag"]);

		string sFileNm = "get_cast_treasure.swf";
		string sNextPageNm = string.Empty;
		string sGetParamStr = string.Empty;

		if (sTreasureCompleteFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sNextPageNm = "ViewGameFlashComplete.aspx";
		} else if (sTownFirstClearFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sNextPageNm = "ViewGameFlashMokuhyo.aspx";
		} else if (sLevelUpFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sNextPageNm = "ViewGameFlashLevelup.aspx";
		} else {
			sNextPageNm = "ViewGameTownClearMissionOK.aspx";
		}

		if (this.Request.QueryString.Count > 0) {
			string[] keys = this.Request.QueryString.AllKeys;
			string value = string.Empty;
			List<string> oGetParamList = new List<string>();

			foreach (string key in keys) {
				oGetParamList.Add(key + "=" + iBridUtil.GetStringValue(this.Request.QueryString[key]));
			}

			sGetParamStr = string.Join("&",oGetParamList.ToArray());
		}

		NameValueCollection oSwfParam = new NameValueCollection();
		GameFlashLiteHelper.ResponseFlashSocialGame(sFileNm,sNextPageNm,sGetParamStr,oSwfParam);
	}
}
