/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ްٽ����ēo�^�҂̹ްі��o�^�y�[�W
--	Progaram ID		: GameHandleNmRegist
--
--  Creation Date	: 2011.07.21
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_woman_GameHandleNmRegist:MobileSocialGameWomanBase {

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermIdGame.aspx"));
				return;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e) {
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);

		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (txtCastNm.Text.Equals("")) {
			lblErrorMessage.Text = GetErrorMessage(PwViCommConst.PwErrerCode.GAME_HANDLE_NM_ERR_STATUS_CAST);
			bOk = false;
		} else if (!SysPrograms.IsWithinByteCount(HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtCastNm.Text)),60)) {
			bOk = false;
			lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"�����Ȱ�");
		}

		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			string sNGWord;
			if (sessionWoman.ngWord.VaidateDoc(txtCastNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (bOk == false) {
			return;
		}

		string sSiteCd = sessionWoman.site.siteCd;
		string sUserSeq = sessionWoman.userWoman.userSeq;
		string sCurCharNo = sessionWoman.userWoman.curCharNo;
		string sSexCd = ViCommConst.OPERATOR;

		string sName = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtCastNm.Text));

		string sResult;
		sResult = sessionWoman.userWoman.CurCharacter.gameCharacter.RegistNm(sSiteCd,sUserSeq,sCurCharNo,sSexCd,sName);
		if (sResult.Equals(PwViCommConst.GameCharCreate.GAME_CHAR_CREATE_RESULT_NG)) {
			bOk = false;
			lblErrorMessage.Text = string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.GAME_CHAR_CREATE));
		}

		if (bOk == false) {
			return;
		}

		sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);

		// ����������ʂɑJ�ڂ��܂��B 
		RedirectToGameDisplayDoc(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_SELECT);
	}
}
