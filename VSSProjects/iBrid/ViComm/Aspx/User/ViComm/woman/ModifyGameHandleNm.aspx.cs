/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ゲームハンドルネーム変更
--	Progaram ID		: ModifyGameHandleNm
--
--  Creation Date	: 2012.10.16
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ModifyGameHandleNm:MobileSocialGameWomanBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			txtGameHandleNm.Text = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].gameCharacter.gameHandleNm;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = string.Empty;
		txtGameHandleNm.Text = HttpUtility.HtmlEncode(txtGameHandleNm.Text);

		if (string.IsNullOrEmpty(txtGameHandleNm.Text)) {
			lblErrorMessage.Text = GetErrorMessage(PwViCommConst.PwErrerCode.GAME_HANDLE_NM_ERR_STATUS_CAST,false);
			return;
		} else if (txtGameHandleNm.Text.Length > 256) {
			lblErrorMessage.Text = "源氏名が長すぎます。";
			return;
		} else {
			string sNGWord;

			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}

			if (sessionWoman.ngWord.VaidateDoc(txtGameHandleNm.Text,out sNGWord) == false) {
				lblErrorMessage.Text = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD,false),sNGWord);
				return;
			}
		}

		string sResult;

		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			oCastGameCharacter.ModifyGameHandleNm(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				txtGameHandleNm.Text,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_WOMAN_MODIFY_GAME_HANDLE_NM_COMPLETED);
		} else {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
			return;
		}
	}
}
