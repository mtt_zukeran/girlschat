/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE��׸�����وꗗ
--	Progaram ID		: ListGameCharacterBattle
--
--  Creation Date	: 2011.09.30
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameCharacterBattle:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_LIST,ActiveForm);
	}
}
