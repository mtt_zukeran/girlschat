<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetupFreeDial.aspx.cs" Inherits="ViComm_woman_SetupFreeDial" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:SelectionList ID="chkUseFreeDiall" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML03;" Value="1" />
		</mobile:SelectionList>		
		$PGM_HTML02;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
