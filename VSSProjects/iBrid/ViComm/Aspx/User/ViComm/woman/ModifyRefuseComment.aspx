<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyRefuseComment.aspx.cs" Inherits="ViComm_woman_ModifyRefuseComment" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<mobile:Panel ID="pnlFoundDate" Runat="server">
			<cc1:iBMobileLiteralText ID="tagHeader" runat="server" Text="$PGM_HTML01;" />
			<cc1:iBMobileLabel ID="lblModifyComplate" runat="server" />
			<cc1:iBMobileLiteralText ID="tagHeader2" runat="server" Text="$PGM_HTML02;" />
			<tx:TextArea ID="txtAreaRefuseComment" runat="server" Row="4" BreakBefore="false" BrerakAfter="true" Rows="4" Columns="24" MaxLength="500">
			</tx:TextArea>
			<cc1:iBMobileLiteralText ID="tagFooter" runat="server" Text="$PGM_HTML08;" />
			<cc1:iBMobileLiteralText ID="tagFooter2" runat="server" Text="$PGM_HTML09;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlNotFoundDate" Runat="server">
			<cc1:iBMobileLiteralText ID="tagNotFoundDate" runat="server" Text="$PGM_HTML06;" />
		</mobile:Panel>
	</mobile:Form>
</body>
</html>
