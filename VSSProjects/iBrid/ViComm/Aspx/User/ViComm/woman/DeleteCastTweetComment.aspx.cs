/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ つぶやきコメント削除
--	Progaram ID		: DeleteCastTweetComment
--
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_DeleteCastTweetComment:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_TWEET_COMMENT,this.ActiveForm);
		} else {
			string sCastTweetSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetseq"]);
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				string sSiteCd = sessionWoman.site.siteCd;
				string sUserSeq = sessionWoman.userWoman.userSeq;
				string sUserCharNo = sessionWoman.userWoman.curCharNo;
				string sCastTweetCommentSeq = iBridUtil.GetStringValue(this.Request.QueryString["commentseq"]);
				
				string sResult = string.Empty;

				using (CastTweetComment oCastTweetComment = new CastTweetComment()) {
					oCastTweetComment.DeleteCastTweetComment(sSiteCd,sUserSeq,sUserCharNo,sCastTweetCommentSeq,out sResult);
				}

				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("tweetseq",sCastTweetSeq);
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_DELETE_TWEET_COMMENT_COMPLETE,oParameters);
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}
			} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ListCastTweetComment.aspx?tweetseq={0}",sCastTweetSeq)));
			}
		}
	}
}