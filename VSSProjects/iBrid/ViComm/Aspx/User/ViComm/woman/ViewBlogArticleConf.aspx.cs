/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ��۸ޓ��e �m�F�ڍ�
--	Progaram ID		: ViewBlogArticleConf
--
--  Creation Date	: 2011.03.24
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewBlogArticleConf : MobileBlogWomanBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
		
		bool bHasData = sessionWoman.ControlList(Request, ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE, ActiveForm);
		
		if(bHasData){
			if(!this.IsPostBack){
			}else{
				string sBlogArticleSeq = sessionWoman.GetBlogArticleValue("BLOG_ARTICLE_SEQ");

				if (Request.Params[ViCommConst.BUTTON_EDIT] != null) {
					//
					// �ĕҏW
					//

					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("ModifyBlogArticle.aspx?blog_article_seq={0}", sBlogArticleSeq)));				
				}else if (this.IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
					//
					// ���J�\��
					//
														
					using(BlogArticle oBlogArticle = new BlogArticle()){
						oBlogArticle.PreferPublish(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sessionWoman.userWoman.CurCharacter.characterEx.blogSeq,
							sBlogArticleSeq
						);
							
					}
										
					IDictionary<string,string> oParam = new Dictionary<string,string>();
					oParam.Add("blor_article_seq",sBlogArticleSeq);
					RedirectToDisplayDoc(ViCommConst.SCR_BLOG_PREFER_PUBLISH,oParam);					
				}else if (this.IsPostAction(PwViCommConst.BUTTON_PUBLISH)) {
					using (BlogArticle oBlogArticle = new BlogArticle()) {
						oBlogArticle.PublishBlogArticle(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sessionWoman.userWoman.CurCharacter.characterEx.blogSeq,
							sBlogArticleSeq
						);
					}
					RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListBlogArticleConf.aspx"));
					
				}else if (this.IsPostAction(PwViCommConst.BUTTON_DELETE)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(string.Format("DeleteBlogArticle.aspx?blog_article_seq={0}",sBlogArticleSeq)));
				}
			}	
		}else{
			throw new ApplicationException("Data Not Found.");
		}	
	}
	
}
