/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ記事イイネ登録者一覧
--	Progaram ID		: ListBlogArticleLike
--
--  Creation Date	: 2014.03.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListBlogArticleLike:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_BLOG_ARTICLE_LIKE,this.ActiveForm);
	}
}