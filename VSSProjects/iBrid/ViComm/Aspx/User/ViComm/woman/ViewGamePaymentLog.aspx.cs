/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ゲーム内獲得報酬ポイント詳細
--	Progaram ID		: ViewGamePaymentLog
--
--  Creation Date	: 2012.10.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGamePaymentLog:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			int iGameStartYear = this.GetGameStartYear();
			this.SetupFromToDay(iGameStartYear);
			string sStartDate = iBridUtil.GetStringValue(this.Request.QueryString["start_date"]);
			string sEndDate = iBridUtil.GetStringValue(this.Request.QueryString["end_date"]);
			
			int iStartYear;
			int iStartMonth;
			int iStartDay;
			int iEndYear;
			int iEndMonth;
			int iEndDay;
			
			if (!string.IsNullOrEmpty(sStartDate)) {
				string[] sStartDateArr = sStartDate.Split('/');
				
				if(sStartDateArr.Length != 3) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
				
				if (!int.TryParse(sStartDateArr[0],out iStartYear)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}

				if (!int.TryParse(sStartDateArr[1],out iStartMonth)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}

				if (!int.TryParse(sStartDateArr[2],out iStartDay)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
			} else {
				iStartYear = int.Parse(DateTime.Now.ToString("yyyy"));
				iStartMonth = int.Parse(DateTime.Now.ToString("MM"));
				iStartDay = 1;
			}
			
			if (!string.IsNullOrEmpty(sEndDate)) {
				string[] sEndDateArr = sEndDate.Split('/');
				
				if (sEndDateArr.Length != 3) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}

				if (!int.TryParse(sEndDateArr[0],out iEndYear)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}

				if (!int.TryParse(sEndDateArr[1],out iEndMonth)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}

				if (!int.TryParse(sEndDateArr[2],out iEndDay)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
			} else {
				iEndYear = int.Parse(DateTime.Now.ToString("yyyy"));
				iEndMonth = int.Parse(DateTime.Now.ToString("MM"));
				iEndDay = lstEndDay.Items.Count;
			}

			lstStartYear.SelectedIndex = int.Parse(DateTime.Now.ToString("yyyy")) - iStartYear;
			lstStartMonth.SelectedIndex = iStartMonth - 1;
			lstStartDay.SelectedIndex = iStartDay - 1;
			lstEndYear.SelectedIndex = int.Parse(DateTime.Now.ToString("yyyy")) - iEndYear;
			lstEndMonth.SelectedIndex = iEndMonth - 1;
			lstEndDay.SelectedIndex = iEndDay - 1;
			
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_PAYMENT_LOG_VIEW,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e) {
		string sStartDate = HttpUtility.HtmlEncode(string.Format("{0}/{1}/{2}",lstStartYear.Selection.Value,lstStartMonth.Selection.Value,lstStartDay.Selection.Value));
		string sEndDate = HttpUtility.HtmlEncode(string.Format("{0}/{1}/{2}",lstEndYear.Selection.Value,lstEndMonth.Selection.Value,lstEndDay.Selection.Value));

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,String.Format("ViewGamePaymentLog.aspx?start_date={0}&end_date={1}",sStartDate,sEndDate)));
	}

	private void SetupFromToDay(int iGameStartYear) {
		int iCurYear = int.Parse(DateTime.Now.ToString("yyyy"));
		lstStartYear.Items.Clear();
		lstEndYear.Items.Clear();
		
		int iYearCount = iCurYear - iGameStartYear;

		for (int i = 0;i <= iYearCount;i++) {
			lstStartYear.Items.Add(new MobileListItem(iCurYear.ToString()));
			lstEndYear.Items.Add(new MobileListItem(iCurYear.ToString()));
			iCurYear--;
		}

		lstStartMonth.Items.Clear();
		lstEndMonth.Items.Clear();

		for (int i = 1;i <= 12;i++) {
			lstStartMonth.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
			lstEndMonth.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
		}

		lstStartDay.Items.Clear();
		lstEndDay.Items.Clear();

		for (int i = 1;i <= 31;i++) {
			lstStartDay.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
			lstEndDay.Items.Add(new MobileListItem(string.Format("{0:D2}",i)));
		}
	}
	
	private int GetGameStartYear() {
		int iValue;
		DateTime oGameStartDate;
		
		string sSiteCd = this.sessionWoman.site.siteCd;
		
		using (SocialGame oSocialGame = new SocialGame()) {
			oGameStartDate = oSocialGame.GetGameStartDate(sSiteCd);
		}
		
		int.TryParse(oGameStartDate.ToString("yyyy"),out iValue);
		
		return iValue;
	}
}