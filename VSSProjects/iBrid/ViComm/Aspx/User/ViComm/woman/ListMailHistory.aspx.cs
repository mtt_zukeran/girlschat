/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: [ðBOX
--	Progaram ID		: ListMailHistory
--
--  Creation Date	: 2010.07.14
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListMailHistory:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		sessionWoman.ClearCategory();
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_MAIL_HISTORY,ActiveForm);

			ParseHTML oParseWoman = sessionWoman.parseContainer;
			if (oParseWoman.currentRecPos[ViCommConst.DATASET_MAIL] == -1) {
				string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
				sessionWoman.SetManDataSetByLoginId(sLoginId,1);
				oParseWoman.parseUser.currentTableIdx = ViCommConst.DATASET_MAN;
			}

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDelTxRx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,mailSeqList.ToArray());
			}
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("mail_delete_count",mailSeqList.Count.ToString());
			oParam.Add("loginid",iBridUtil.GetStringValue(this.Request.QueryString["loginid"]));
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_DELETED_MAIL_HISTORY,oParam);
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
															   "ListMailHistory.aspx?" + Request.QueryString));
		}
	}
}
