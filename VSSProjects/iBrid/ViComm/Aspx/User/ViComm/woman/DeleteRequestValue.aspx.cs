/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���˂����]���폜/����
--	Progaram ID		: DeleteRequestValue
--
--  Creation Date	: 2012.07.03
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteRequestValue:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		string sRequestValueSeq = iBridUtil.GetStringValue(Request.QueryString["val_seq"]);

		if (string.IsNullOrEmpty(sRequestValueSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			pnlConfirm.Visible = true;
			pnlComplete.Visible = false;

			DataSet dsRequestValue = getRequestValue(sRequestValueSeq);

			if (dsRequestValue.Tables[0].Rows.Count > 0) {
				DataRow drRequestValue = dsRequestValue.Tables[0].Rows[0];

				if (!this.checkAccess(drRequestValue)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sRequestValueSeq);
			}
		}
	}

	private DataSet getRequestValue(string pRequestValueSeq) {
		DataSet dsRequestValue;
		RequestValueSeekCondition oCondition = new RequestValueSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.SexCd = ViCommConst.OPERATOR;
		oCondition.RequestValueSeq = pRequestValueSeq;
		using (RequestValue oRequestValue = new RequestValue()) {
			dsRequestValue = oRequestValue.GetPageCollection(oCondition,1,1);
		}
		return dsRequestValue;
	}

	private bool checkAccess(DataRow drRequestValue) {
		if (sessionWoman.IsValidMask(sessionWoman.userWoman.CurCharacter.userDefineMask,PwViCommConst.CastUserDefFlag.REQUEST_ADMIN)) {
			return true;
		} else if (sessionWoman.userWoman.userSeq.Equals(iBridUtil.GetStringValue(drRequestValue["USER_SEQ"])) && sessionWoman.userWoman.curCharNo.Equals(iBridUtil.GetStringValue(drRequestValue["USER_CHAR_NO"]))) {
			return true;
		}

		return false;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pRequestValueSeq) {
		string sResult = string.Empty;

		using (RequestValue oRequestValue = new RequestValue()) {
			sResult = oRequestValue.DeleteRequestValue(
				sessionWoman.site.siteCd,
				pRequestValueSeq,
				ViCommConst.FLAG_ON_STR
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}