/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްє���TOP
--	Progaram ID		: GameNonUserTop
--
--  Creation Date	: 2011.07.21
--  Creater			: PW K.Miyazato
--
**************************************************************************/
#region using
using System;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;


using iBridCommLib;
using MobileLib;
using ViComm;
#endregion

public partial class ViComm_woman_GameNonUserTop:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			using (Access oAccess = new Access()) {
				oAccess.AccessTopPage(
					sessionObj.site.siteCd,
					string.Empty,
					string.Empty,
					sessionObj.adCd,
					ViCommConst.OPERATOR
				);
			}
		}
	}
}
