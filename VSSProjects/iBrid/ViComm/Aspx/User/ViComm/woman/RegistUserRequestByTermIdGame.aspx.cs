/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ゲーム登録申請(固体識別事前通知型)
--	Progaram ID		: RegistUserRequestByTermIdGame
--
--  Creation Date	: 2011.07.21
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistUserRequestByTermIdGame:MobileSocialGameWomanBase {

	private const string PARAM_NEXT_DOC_NO = "*nextdoc";

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		ParseHTML oParseWoman = sessionWoman.parseContainer;

		if (sessionWoman.getTermIdFlag) {
			oParseWoman.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
		} else {
			oParseWoman.parseUser.postAction = ViCommConst.POST_ACT_GUID;
		}

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUtn = Mobile.GetUtn(sessionWoman.carrier,Request);
		string siModeId = Mobile.GetiModeId(sessionWoman.carrier,Request);

		string sResult;
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (Mobile.IsFeaturePhone(sessionWoman.carrier)) {
			if (bOk) {
				if (sessionWoman.getTermIdFlag) {
					if (sUtn.Equals(string.Empty)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
						return;
					}
				} else {
					if (siModeId.Equals(string.Empty)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
						return;
					}
				}
			}
		}

		using (Man oMan = new Man()) {
			if (oMan.IsBlackUser(ViCommConst.NO_TYPE_TERMINAL_UNIQUE_ID,sUtn)) {
				//ﾌﾞﾗｯｸﾕｰｻﾞｰとして登録されています。ご利用になれません。
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
				bOk = false;
				return;
			}
		}

		string sAge = iBridUtil.GetStringValue(this.Request.Params["checkAge"]);

		if (string.IsNullOrEmpty(sAge)) {
			lblErrorMessage.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.GAME_CAN_NOT_REGISTER),"18歳未満の方は");
			bOk = false;
		}

		string sHandleNm = System.Web.HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtHandleNm.Text));
		txtHandleNm.Text = System.Web.HttpUtility.HtmlEncode(txtHandleNm.Text);

		if (sHandleNm.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.GAME_HANDLE_NM_ERR_STATUS_CAST);
		}
		
		Random rnd = new Random();
		string sPassWord = string.Format("{0:D4}",rnd.Next(10000));

		if (bOk == false) {
			return;
		}

		string sBirthDay = string.Format("{0}0101",DateTime.Now.Year - 25);
		
		using (TempRegist oRegist = new TempRegist()) {
			oRegist.RegistUserWomanTemp(
				sUtn,
				siModeId,
				Request.UserHostAddress,
				string.Empty,
				sPassWord,
				string.Empty,
				string.Empty,
				sBirthDay,
				string.Empty,
				sHandleNm,
				ViCommConst.FLAG_ON,
				out sessionWoman.userWoman.tempRegistId,out sResult);

			switch (int.Parse(sResult)) {
				case ViCommConst.REG_USER_RST_UNT_EXIST:
					lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.GAME_STATUS_TERM_ID_ALREADY_EXIST);
					return;
			}
		}

		if (sResult.Equals("0")) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("RegistAddOnInfoByGame.aspx?utn={0}&id={1}",sUtn,siModeId)));
		} else {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
