/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 退会者トップ
--	Progaram ID		: ResignedUserTop.aspx
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ResignedUserTop:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (iBridUtil.GetStringValue(Request.QueryString["resignedreturn"]).Equals(ViCommConst.FLAG_ON_STR)) {
				ResignedReturnUser();
			}		
		}
	}
	
	private void ResignedReturnUser() {
		string sResult = string.Empty;
		using (User oUser = new User()) {
			if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE) || sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCookie"]).Equals(ViCommConst.FLAG_ON_STR)) {
					if (Request.Cookies["maqia"] != null) {
						oUser.GetOne(iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiauid"]), iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiasex"]));
					}
				}
			} else {
				oUser.GetUserSexByIModeId(sessionWoman.currentIModeId, sessionWoman.site.siteCd);
			}
			if (oUser.userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)) {
				sessionWoman.userWoman.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_LOGINID,oUser.loginId,oUser.loginPassword,out sResult);
			}
		}
		if (sResult.Equals("0")) {
			Response.Redirect(sessionWoman.site.url);
		} else {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.site.nonUserTopId));
		}
	}
}
