/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: «PCo^ót
--	Progaram ID		: ApplyRegistCastPC
--
--  Creation Date	: 2013.05.24
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ApplyRegistCastPC:MobileWomanPageBase {
	private string sTempRegistId;
	private string sUtn;
	private string sIModeId;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sTempRegistId = iBridUtil.GetStringValue(Request.QueryString["trid"]);

			if (string.IsNullOrEmpty(sTempRegistId)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			sUtn = Mobile.GetUtn(sessionWoman.carrier,Request);
			sIModeId = Mobile.GetiModeId(sessionWoman.carrier,Request);

			if (!CheckUtn()) {
				return;
			}

			string sResult;

			using (TempRegist oTempRegist = new TempRegist()) {
				oTempRegist.ApplyRegistCastPC(
					sTempRegistId,
					sessionWoman.carrier,
					sUtn,
					sIModeId,
					Request.UserHostAddress,
					sessionWoman.mobileUserAgent,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.ApplyRegistCastPCResult.RESULT_OK)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_SEND_ID_MAIL);
				return;
			} else if (sResult.Equals(PwViCommConst.ApplyRegistCastPCResult.RESULT_DONE)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_SEND_ID_MAIL);
				return;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}
		}
	}

	private bool CheckUtn() {
		bool bOk = true;
		sessionWoman.errorMessage = string.Empty;

		if (Mobile.IsFeaturePhone(sessionWoman.carrier)) {
			if (sessionWoman.getTermIdFlag) {
				if (sUtn.Equals(string.Empty)) {
					sessionWoman.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			} else {
				if (sIModeId.Equals(string.Empty)) {
					sessionWoman.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			}

			if (bOk) {
				int iUtnExistFlag;

				using (User oUser = new User()) {
					oUser.CheckCastUtnExist(sUtn,sIModeId,out iUtnExistFlag);
				}

				if (iUtnExistFlag.Equals(ViCommConst.FLAG_ON)) {
					sessionWoman.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
					bOk = false;
				}
			}
		}

		return bOk;
	}
}
