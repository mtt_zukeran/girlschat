<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistUserRequestByTermIdIdol.aspx.cs" Inherits="ViComm_woman_RegistUserRequestByTermIdIdol" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <mobile:Form ID="frmMain" Runat="server">
		 $PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblWishCastType" runat="server" BreakAfter="true">$PGM_HTML03;�u�]�W������</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstWishCastType" Runat="server" BreakAfter="true"></mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagWishCastType" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblTel" runat="server" BreakAfter="true">$PGM_HTML03;�g�єԍ�$PGM_HTML07;</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true"
            BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagTel" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblCastNm" runat="server" BreakAfter="true">$PGM_HTML03;�{��(����)$PGM_HTML07;</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtCastNm" runat="server" MaxLength="24" Size="18" BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagNm" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblCastKanaNm" runat="server" BreakAfter="true" Visible="true">$PGM_HTML03;�{��(�t���K�i)$PGM_HTML07;</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtCastKanaNm" runat="server" MaxLength="30" Size="18" BreakAfter="true" Visible="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagKanaNm" runat="server" Text="<br/>" Visible="true" />
		<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">$PGM_HTML03;���N����$PGM_HTML07;</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">�N</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">��</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblDay" runat="server">��</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagBirthday" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblPrefecture" runat="server" BreakAfter="true">$PGM_HTML03;���Z��(�s���{��)$PGM_HTML07;</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstPrefecture" Runat="server" BreakAfter="true"></mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagPrefecture" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblSex" runat="server" BreakAfter="true">$PGM_HTML03;����$PGM_HTML07;</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstSex" Runat="server" BreakAfter="true"></mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="tagSex" runat="server" Text="<br/>" />
		$PGM_HTML05;
		<cc1:iBMobileLabel ID="lblGuardianNm" runat="server" BreakAfter="true">�ی�Ҏ���</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtGuardianNm" runat="server" MaxLength="24" Size="18" BreakAfter="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagGuardianNm" runat="server" Text="<br/>" />
		<cc1:iBMobileLabel ID="lblGuardianTel" runat="server" BreakAfter="true">�ی�҂̌g�єԍ�</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtGuardianTel" runat="server" MaxLength="11" Size="18" Numeric="true" BreakAfter="true"></cc1:iBMobileTextBox>
		$PGM_HTML06;
		<cc1:iBMobileLabel ID="lblQuestionDoc" runat="server" Visible="false">�����⎖��</cc1:iBMobileLabel>
		<tx:TextArea ID="txtQuestionDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true"
            Rows="6" Columns="24" MaxLength="300" Visible="false"></tx:TextArea>
		<mobile:Panel ID="pnlAgreement" Runat="server">
			<mobile:SelectionList ID="chkAgreement" Runat="server" SelectType="CheckBox">
                <Item Text="$PGM_HTML03;"></Item>
            </mobile:SelectionList>
			$PGM_HTML04;
		</mobile:Panel>
		$PGM_HTML02;
		$PGM_HTML09;
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="12" Numeric="true"
            Visible="false"></cc1:iBMobileTextBox>
	</mobile:Form>
</body>
</html>
