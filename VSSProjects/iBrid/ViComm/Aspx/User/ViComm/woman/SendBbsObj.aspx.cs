﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板動画・画像送信設定

--	Progaram ID		: SendBbsObj
--
--  Creation Date	: 2010.09.24
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;
using System.Web;

public partial class ViComm_woman_SendBbsObj:MobileObjWomanPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	protected virtual void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm);

			string sObjType = iBridUtil.GetStringValue(Request.QueryString["objtype"]);
			//objtypeが指定された場合ラジオボタン非表示
			if (sObjType.Equals(ViCommConst.BbsObjType.PIC)) {
				rdoBbsObj.Items[0].Selected = true;
				rdoBbsObj.Items[1].Selected = false;
				rdoBbsObj.Visible = false;
			} else if (sObjType.Equals(ViCommConst.BbsObjType.MOVIE)) {
				rdoBbsObj.Items[0].Selected = false;
				rdoBbsObj.Items[1].Selected = true;
				rdoBbsObj.Visible = false;
			}
			if (sessionWoman.site.bbsMovieAttrFlag == ViCommConst.FLAG_OFF) {
				pnlSearchList.Visible = false;
			} else if (!iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]).Equals(string.Empty) && !iBridUtil.GetStringValue(Request.QueryString["attrseq"]).Equals(string.Empty)) {
				//前画面よりリンクにてカテゴリ選択した場合リストボックスは表示させない 
				pnlSearchList.Visible = false;
			} else {
				CreateObjAttrComboBox(sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,string.Empty,sObjType,ViCommConst.ATTACHED_BBS.ToString(),false);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (ValidateInput()) {
			string sAttachedObjType = string.Empty;
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text));
			string sAttrTypeSeq = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString();
			string sAttrSeq = ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString();
			string sObjTempId = string.Empty;
			int iRankingFlag = 1;

			if (rdoBbsObj.Items[0].Selected) {
				sAttachedObjType = ViCommConst.ATTACH_PIC_INDEX.ToString();
			} else if (rdoBbsObj.Items[1].Selected) {
				sAttachedObjType = ViCommConst.ATTACH_MOVIE_INDEX.ToString();
			}

			if (pnlSearchList.Visible) {
				string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
				string[] selectedValueList = selectedValue.Split(',');

				sAttrTypeSeq = selectedValueList[0];
				sAttrSeq = selectedValueList[1];
			} else {
				string sQueryAttrTypeSeq = iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]);
				string sQueryAttrSeq = iBridUtil.GetStringValue(Request.QueryString["attrseq"]);

				if (!sQueryAttrTypeSeq.Equals(string.Empty)) {
					sAttrTypeSeq = sQueryAttrTypeSeq;
				}
				if (!sQueryAttrSeq.Equals(string.Empty)) {
					sAttrSeq = sQueryAttrSeq;
				}
			}

			iRankingFlag = int.Parse(lstRankingFlag.Items[lstRankingFlag.SelectedIndex].Value);

			using (WaitObj oWaitObj = new WaitObj()) {
				oWaitObj.WriteWaitObj(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sAttachedObjType,
					sTitle,
					sDoc,
					ViCommConst.ATTACHED_BBS.ToString(),
					sAttrTypeSeq,
					sAttrSeq,
					ViCommConst.FLAG_OFF,
					string.Empty,
					ViCommConst.FLAG_OFF,
					iRankingFlag,
					out sObjTempId
				);
			}

			sessionWoman.userWoman.ObjTempId = sObjTempId;
			if (sAttachedObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
				RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_PIC_BBS_MAILER_COMPLITE);
			} else {
				RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_MOVIE_BBS_MAILER_COMPLITE);
			}
		}
	}

	protected virtual bool ValidateInput() {
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}
		string sNGWord = string.Empty;

		if (txtTitle.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE) + "<br />";
		}
		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC) + "<br />";
		}
		if (sessionWoman.ngWord.VaidateDoc(txtDoc.Text + txtTitle.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
		}

		if (pnlSearchList.Visible) {
			string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
			string[] selectedValueList = selectedValue.Split(',');

			string sAttrTypeSeq = selectedValueList[0];
			string sAttrSeq = selectedValueList[1];

			if (sAttrTypeSeq.Equals(string.Empty) || sAttrSeq.Equals(string.Empty)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_OBJ_ATTR) + "<br />";
			}
		}
		return bOk;
	}
}
