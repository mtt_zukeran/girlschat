<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfirmWriteBbsThread.aspx.cs" Inherits="ViComm_woman_ConfirmWriteBbsThread" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLabel ID="lblThreadHandleNm" runat="server" BreakAfter="true"></cc1:iBMobileLabel>
		$PGM_HTML02;
	    <cc1:iBMobileLabel ID="lblThreadTitle" runat="server" BreakAfter="true"></cc1:iBMobileLabel>
	    $PGM_HTML03;
	    <cc1:iBMobileLabel ID="lblThreadDoc" runat="server" BreakAfter="true"></cc1:iBMobileLabel>
	    $PGM_HTML04;
		$PGM_HTML06;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
