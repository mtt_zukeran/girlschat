/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾛｸﾞｲﾝ中の男性(メール送信済)一覧
--	Progaram ID		: ListLoginedUserMailSend.aspx
--
--  Creation Date	: 2016.05.10
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_woman_ListLoginedUserMailSend:MobileWomanPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		if (!IsPostBack) {
			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_LOGINED_MAIL_SEND,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		UrlBuilder oUrlBuilder = new UrlBuilder(SessionObjs.Current.GetNavigateUrl(ViCommPrograms.GetCurrentAspx()));

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["sort"]))) {
			oUrlBuilder.AddParameter("sort",iBridUtil.GetStringValue(this.Request.QueryString["sort"]));
		}

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Form["notxmaildays"]))) {
			oUrlBuilder.AddParameter("notxmaildays",iBridUtil.GetStringValue(this.Request.Form["notxmaildays"]));
		}
		RedirectToMobilePage(oUrlBuilder.ToString());
	}
}