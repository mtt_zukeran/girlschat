<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetupRxMail.aspx.cs" Inherits="ViComm_woman_SetupRxMail" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<cc1:iBMobileLabel ID="lblManMailRxType" runat="server">男性からのﾒｰﾙを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstManMailRxType" Runat="server"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblInfoMailRxType" runat="server">お知らせﾒｰﾙを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstInfoMailRxType" Runat="server"></mobile:SelectionList>
		<mobile:Panel ID="pnlTalkEndMailRxFlag" Runat="server">
			<cc1:iBMobileLabel ID="lblTalkEndMailRxFlag" Runat="server">通話切断理由ﾒｰﾙを</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstTalkEndMailRxFlag" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML02;
		<mobile:SelectionList ID="lstMobileMailRxStartTime" Runat="server" BreakAfter="false"></mobile:SelectionList>〜
		<mobile:SelectionList ID="lstMobileMailRxEndTime" Runat="server"></mobile:SelectionList>
		$PGM_HTML05;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
