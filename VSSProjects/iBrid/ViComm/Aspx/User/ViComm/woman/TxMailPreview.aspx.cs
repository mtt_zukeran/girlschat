/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信プレビュー
--	Progaram ID		: TxMailPreview
--
--  Creation Date	: 2013.08.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_TxMailPreview:MobileMailWomanPage {

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
		MailTemplate = lstTemplate;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {
		string sLayOutType = iBridUtil.GetStringValue(Request.QueryString["layouttype"]);
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			ParseHTML oParse = sessionWoman.parseContainer;
			string sMailDataSeq = iBridUtil.GetStringValue(Request.QueryString["data"]);
			ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;
			ViewState["LAY_OUT_TYPE"] = sLayOutType;
			bool isReturnMail = sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail;

			SetMailTemplate(isReturnMail,isReturnMail == true ? ViCommConst.TxMailLayOutType.NORMAL_MAIL : sLayOutType);

			if (sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail == false) {
				using (ManageCompany oManageCompany = new ManageCompany()) {
					bool bIsAvailable = oManageCompany.IsAvailableService(ViCommConst.RELEASE_TX_MAIL_DISABLE_TEMPLATE);
					if (bIsAvailable) {
						this.SelectNomalMail();
					}
				}
			}

			txtTitle.Text = sessionWoman.userWoman.mailData[sMailDataSeq].mailTitle;
			txtDoc.Text = sessionWoman.userWoman.mailData[sMailDataSeq].mailDoc;

			if (sessionWoman.SetManDataSetByUserSeq(sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq,1) == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdBack"] != null) {
				cmdBack_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_REGIST] != null) {
				cmdRegist_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		bool isReturnMail = sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail;
		string pSuccessPageNo;
		if (isReturnMail == true) {
			pSuccessPageNo = ViCommConst.SCR_TX_REMAIL_COMPLITE_CAST;
		} else {
			pSuccessPageNo = ViCommConst.SCR_TX_MAIL_COMPLITE_CAST;
		}
		Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),isReturnMail,false,pSuccessPageNo);
	}

	protected void cmdBack_Click(object sender,EventArgs e) {
		string sMailType = iBridUtil.GetStringValue(Request.QueryString["mailtype"]);
		string sMailDataSeq = ViewState["MAIL_DATA_SEQ"].ToString();
		string sRedirectUrl = string.Empty;

		if (sessionWoman.userWoman.mailData[sMailDataSeq].isReturnMail) {
			sRedirectUrl = string.Format("ReturnMail.aspx?data={0}",sMailDataSeq);
			if (sessionWoman.userWoman.mailData[sMailDataSeq].chatMailFlag == ViCommConst.FLAG_ON) {
				sRedirectUrl = sRedirectUrl + string.Format("&loginid={0}&scrid=01&stay=1#btm",sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId);
			}
			
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sRedirectUrl));
		} else {
			sRedirectUrl = string.Format("TxMail.aspx?data={0}",sMailDataSeq);
			if (sessionWoman.userWoman.mailData[sMailDataSeq].chatMailFlag == ViCommConst.FLAG_ON) {
				sRedirectUrl = sRedirectUrl + string.Format("&loginid={0}&userrecno=1&scrid=01&stay=1#btm",sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId);
			}
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sRedirectUrl));
		}
	}

	private void SelectNomalMail() {
		foreach (MobileListItem oMobileListItem in this.lstTemplate.Items) {
			if (ViCommConst.MailTemplateType.NORMAL.Equals(oMobileListItem.Value)) {
				this.lstTemplate.SelectedIndex = oMobileListItem.Index;
				break;
			}
		}
	}

	protected void cmdRegist_Click(object sender,EventArgs e) {
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		MainteDraftMail(sMailDataSeq,false);
	}
}
