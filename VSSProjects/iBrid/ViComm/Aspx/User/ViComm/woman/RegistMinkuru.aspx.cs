﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ミンクル登録
--	Progaram ID		: RegistMinkuru
--
--  Creation Date	: 2011.05.02
--  Creater			: i-Brid
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Net;
using System.IO;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text;

public partial class ViComm_woman_RegistMinkuru:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
//		lblStartSpanTag.Text = "<span style=font-size:medium;>";
//		lblEndSpanTag.Text = "</span>";
		string sErrorMessage;
		if (!this.IsCorrectPageLoad()) {
			this.RedirectToMobilePage(this.sessionWoman.site.url);
		} else if (!this.IsCorrectResponseStatus(out sErrorMessage)) {
		
			Encoding enc = Encoding.GetEncoding("Shift_JIS");
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("errormessage",HttpUtility.UrlEncode(sErrorMessage,enc));
			this.RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_COLOMOBA_REQUEST_NG,oParam);
		}

		this.Response.Filter = this.sessionWoman.InitScreen(this.Response.Filter,this,this.Request,this.ViewState,this.IsPostBack);

		if (!IsPostBack) {
			this.CreateList();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				this.cmdSubmit_Click(null,new EventArgs());
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (!this.IsCorrectInput()) {
			return;
		}
		Encoding sjis = Encoding.GetEncoding(932);

		string sLocationName = HttpUtility.UrlEncode(this.lstLocation.Selection.Text,sjis);
		string sHandleName = HttpUtility.UrlEncode(this.txtHandleNm.Text,sjis);

		this.RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,
			string.Format("ConfirmRegistMinkuru.aspx?year={0}&month={1}&day={2}&location={3}&locationname={4}&blood={5}&bloodname={6}&handlename={7}",
				this.lstYear.Selection.Value,this.lstMonth.Selection.Value,this.lstDay.Selection.Value,this.lstLocation.Selection.Value,sLocationName,
				Request.Form.Get("radioBlood"),this.GetBloodType(this.Request.Form.Get("radioBlood")),sHandleName)));
	}

	private void CreateList() {
		int iYear = DateTime.Today.Year - 18;
		string[] sBirthday = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].birthday.Split('/');

		for (int i = 0;i < 85;i++) {
			this.lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			if ((sBirthday.Length == 3) && (sBirthday[0].Equals(iYear.ToString("d4")))) {
				this.lstYear.SelectedIndex = i;
			}
			iYear -= 1;
		}
		for (int i = 1;i <= 12;i++) {
			this.lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
			if ((sBirthday.Length == 3) && (sBirthday[1].Equals(i.ToString("d2")))) {
				this.lstMonth.SelectedIndex = i - 1;
			}
		}
		for (int i = 1;i <= 31;i++) {
			this.lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
			if ((sBirthday.Length == 3) && (sBirthday[2].Equals(i.ToString("d2")))) {
				this.lstDay.SelectedIndex = i - 1;
			}
		}

		this.lstLocation.Items.Add(new MobileListItem("北海道","1"));
		this.lstLocation.Items.Add(new MobileListItem("青森県","2"));
		this.lstLocation.Items.Add(new MobileListItem("岩手県","3"));
		this.lstLocation.Items.Add(new MobileListItem("宮城県","4"));
		this.lstLocation.Items.Add(new MobileListItem("秋田県","5"));
		this.lstLocation.Items.Add(new MobileListItem("山形県","6"));
		this.lstLocation.Items.Add(new MobileListItem("福島県","7"));
		this.lstLocation.Items.Add(new MobileListItem("茨城県","8"));
		this.lstLocation.Items.Add(new MobileListItem("栃木県","9"));
		this.lstLocation.Items.Add(new MobileListItem("群馬県","10"));
		this.lstLocation.Items.Add(new MobileListItem("埼玉県","11"));
		this.lstLocation.Items.Add(new MobileListItem("千葉県","12"));
		this.lstLocation.Items.Add(new MobileListItem("東京都","13"));
		this.lstLocation.Items.Add(new MobileListItem("神奈川県","14"));
		this.lstLocation.Items.Add(new MobileListItem("新潟県","15"));
		this.lstLocation.Items.Add(new MobileListItem("富山県","16"));
		this.lstLocation.Items.Add(new MobileListItem("石川県","17"));
		this.lstLocation.Items.Add(new MobileListItem("福井県","18"));
		this.lstLocation.Items.Add(new MobileListItem("山梨県","19"));
		this.lstLocation.Items.Add(new MobileListItem("長野県","20"));
		this.lstLocation.Items.Add(new MobileListItem("岐阜県","21"));
		this.lstLocation.Items.Add(new MobileListItem("静岡県","22"));
		this.lstLocation.Items.Add(new MobileListItem("愛知県","23"));
		this.lstLocation.Items.Add(new MobileListItem("三重県","24"));
		this.lstLocation.Items.Add(new MobileListItem("滋賀県","25"));
		this.lstLocation.Items.Add(new MobileListItem("京都府","26"));
		this.lstLocation.Items.Add(new MobileListItem("大阪府","27"));
		this.lstLocation.Items.Add(new MobileListItem("兵庫県","28"));
		this.lstLocation.Items.Add(new MobileListItem("奈良県","29"));
		this.lstLocation.Items.Add(new MobileListItem("和歌山県","30"));
		this.lstLocation.Items.Add(new MobileListItem("鳥取県","31"));
		this.lstLocation.Items.Add(new MobileListItem("島根県","32"));
		this.lstLocation.Items.Add(new MobileListItem("岡山県","33"));
		this.lstLocation.Items.Add(new MobileListItem("広島県","34"));
		this.lstLocation.Items.Add(new MobileListItem("山口県","35"));
		this.lstLocation.Items.Add(new MobileListItem("徳島県","36"));
		this.lstLocation.Items.Add(new MobileListItem("香川県","37"));
		this.lstLocation.Items.Add(new MobileListItem("愛媛県","38"));
		this.lstLocation.Items.Add(new MobileListItem("高知県","39"));
		this.lstLocation.Items.Add(new MobileListItem("福岡県","40"));
		this.lstLocation.Items.Add(new MobileListItem("佐賀県","41"));
		this.lstLocation.Items.Add(new MobileListItem("長崎県","42"));
		this.lstLocation.Items.Add(new MobileListItem("熊本県","43"));
		this.lstLocation.Items.Add(new MobileListItem("大分県","44"));
		this.lstLocation.Items.Add(new MobileListItem("宮崎県","45"));
		this.lstLocation.Items.Add(new MobileListItem("鹿児島県","46"));
		this.lstLocation.Items.Add(new MobileListItem("沖縄県","47"));
	}

	private bool IsCorrectPageLoad() {
		return sessionWoman.logined && !sessionWoman.IsImpersonated && this.IsCarrierContainsOfFeature();
	}

	private bool IsCarrierContainsOfFeature() {
		switch (this.sessionWoman.carrier) {
			case ViCommConst.DOCOMO:
			case ViCommConst.KDDI:
			case ViCommConst.SOFTBANK:
				return true;
			default:
				return false;
		}
	}

	private string GetCarrierType(string pCarrier) {
		switch (pCarrier) {
			case ViCommConst.DOCOMO:
				return "1";
			case ViCommConst.KDDI:
				return "2";
			case ViCommConst.SOFTBANK:
				return "3";
			default:
				return string.Empty;
		}
	}

	private bool IsCorrectResponseStatus(out string pErrorMessage) {
		string sUrl = "http://minkuru.tv/tel2colo.html";//TODO: colomoba.jp から minkuru.tv へドメイン変更

		string sGId = string.Empty;
		if (this.sessionWoman.carrier.Equals(ViCommConst.KDDI)) {
			sGId = Mobile.GetUtn(this.sessionWoman.carrier,this.Request);
		} else {
			sGId = Mobile.GetiModeId(this.sessionWoman.carrier,this.Request);
		}
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		string sData = string.Format("?id={0}&em={1}&uid={2}&ca={3}",
			HttpUtility.UrlEncode(this.sessionWoman.userWoman.loginId,enc),
			HttpUtility.UrlEncode(this.sessionWoman.userWoman.emailAddr,enc),
			HttpUtility.UrlEncode(sGId,enc),
			HttpUtility.UrlEncode(this.GetCarrierType(sessionWoman.carrier),enc));

		/*
				string sReturn = string.Empty;
				using (System.Net.WebClient oWebClient = new System.Net.WebClient()) {
					oWebClient.Encoding = System.Text.Encoding.GetEncoding(932);
					oWebClient.Headers.Add("Content-Type","application/x-www-form-urlencoded");
					sReturn = oWebClient.UploadString(sUrl,sData);
				}

				return "0".Equals(sReturn);
		*/
		return TransCVS(sUrl + sData,out pErrorMessage);
	}

	private bool IsCorrectInput() {
		if (string.IsNullOrEmpty(this.txtHandleNm.Text)) {
			this.lblErrorMessage.Visible = true;
			this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MINKURU_HANDLE_NM_EMPTY);
			return false;
		}
		if (this.txtHandleNm.Text.Length < 2 || 10 < this.txtHandleNm.Text.Length) {
			this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MINKURU_HANDLE_NM_LENGTH);
			return false;
		}

		if (this.lstLocation.SelectedIndex == 0) {
			this.lblErrorMessage.Visible = true;
			this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MINKURU_AREA_EMPTY);
			return false;
		}

		if (this.GetAge() < 18) {
			this.lblErrorMessage.Visible = true;
			this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MINKURU_AGE_CHECK);
			return false;
		}
		if (this.chkAgreement.Selection == null) {
			this.lblErrorMessage.Visible = true;
			this.lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MINKURU_AGREEMENT);
			return false;
		}

		return true;
	}

	private int GetAge() {
		int iBirthDay = int.Parse(string.Concat(this.lstYear.Selection.Text,this.lstMonth.Selection.Text,this.lstDay.Selection.Text));
		int iToday = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
		return (int)Math.Floor((double)((iToday - iBirthDay) / 10000));
	}

	private string GetBloodType(string pBlood) {
		switch (pBlood) {
			case "1":
				return "A";
			case "2":
				return "B";
			case "3":
				return "O";
			case "4":
				return "AB";

			default:
				return string.Empty;
		}
	}

	public bool TransCVS(string pUrl,out string pErrorMessage) {
		pErrorMessage = string.Empty;
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 20000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st,Encoding.GetEncoding("Shift_JIS"))) {
				string sRes = sr.ReadToEnd();
				string[] sItem = Regex.Split(sRes,"\r\n");
				sr.Close();
				st.Close();
				if (sItem.Length > 0) {
					if (sItem.Length > 1) {
						pErrorMessage = sItem[1];
					}
					return sItem[0].Equals("result=0");
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransCVS",pUrl);
			return false;
		}
	}
}
