<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TxMailPreview.aspx.cs" Inherits="ViComm_woman_TxMailPreview" ValidateRequest="False" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="false" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML02;
	    <cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="24" Visible="false" ></cc1:iBMobileTextBox>
	    $PGM_HTML03;
	    <tx:TextArea ID="txtDoc" runat="server" Row="10" Rows="6" Columns="24" Visible="false">
		</tx:TextArea>
		<mobile:SelectionList ID="lstTemplate" Runat="server" Visible="false"></mobile:SelectionList>
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
