/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑﾛｸﾞｲﾝ
--	Progaram ID		: GameLoginUser
--
--  Creation Date	: 2011.07.22
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_GameLoginUser:MobileSocialGameWomanBase {

	private string utn;
	private string id;
	private string loginId;
	private string loginPassword;
	private bool loginOk;

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {

			utn = this.GetStringValue(Request.QueryString["utn"]);
			id = this.GetStringValue(Request.QueryString["id"]);

			if (utn.Equals("") && id.Equals("")) {
				txtLoginId.Text = this.GetStringValue(Request.QueryString["uid"]);
				txtPassword.Text = this.GetStringValue(Request.QueryString["pw"]);
				if (txtLoginId.Text.Equals("") || txtPassword.Text.Equals("")) {
					txtLoginId.Text = this.GetStringValue(Request.QueryString["loginid"]);
					txtPassword.Text = this.GetStringValue(Request.QueryString["password"]);
				}

				if ((!txtLoginId.Text.Equals("")) && (!txtPassword.Text.Equals(""))) {
					cmdSubmit_Click(sender,e);
					return;
				}

			} else {
				if (this.GetStringValue(Request.QueryString["useutn"]).Equals("1")) {
					if (sessionWoman.userWoman.UsedTermId(utn,"",out loginId,out loginPassword)) {
						txtLoginId.Text = loginId;
						txtPassword.Text = loginPassword;
						cmdSubmit_Click(sender,e);
					}
				} else {
					if (sessionWoman.userWoman.UsedTermId("",id,out loginId,out loginPassword)) {
						txtLoginId.Text = loginId;
						txtPassword.Text = loginPassword;
						cmdSubmit_Click(sender,e);
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = "";
		string sGoto = this.GetStringValue(Request.QueryString["goto"]);

		if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
			loginOk = sessionWoman.userWoman.GetOneByTel(txtLoginId.Text,txtPassword.Text);
		} else {
			loginOk = sessionWoman.userWoman.GetOne(txtLoginId.Text,txtPassword.Text);
			if (loginOk == false && sessionWoman.userWoman.loginErrMsgCd.Equals(ViCommConst.ERR_STATUS_INVALID_ID_PW)) {
				loginOk = sessionWoman.userWoman.GetOneByPreviousId(txtLoginId.Text,txtPassword.Text);
			}
		}

		if (loginOk) {
			sessionWoman.alreadyWrittenLoginLog = false;

			if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE) || (sessionWoman.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionWoman.adminFlg)) {
				HttpCookie cookie = new HttpCookie("maqia");
				cookie.Values["maqiauid"] = sessionWoman.userWoman.userSeq;
				cookie.Values["maqiapw"] = sessionWoman.userWoman.loginPassword;
				cookie.Values["maqiasex"] = ViCommConst.OPERATOR;
				cookie.Expires = DateTime.Now.AddDays(90);
				cookie.Domain = sessionWoman.site.hostNm;
				Response.Cookies.Add(cookie);
			}
			
			DataSet pDS = sessionWoman.userWoman.GetCharacterList("");
			
			string sSiteCd = pDS.Tables[0].Rows[0]["SITE_CD"].ToString();
			string sCharNo = pDS.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
			
			sessionWoman.logined = true;
			sessionWoman.userWoman.SetLastActionDate(sessionWoman.userWoman.userSeq);
			string sListCharacterIndex = string.Format("{0}{1}",sSiteCd,sCharNo);
			
			sessionWoman.userWoman.curCharNo = sCharNo;
			sessionWoman.userWoman.curKey = sListCharacterIndex;
			sessionWoman.userWoman.characterList[sListCharacterIndex].gameCharacter.GetCurrentInfo(sSiteCd,sessionWoman.userWoman.userSeq);

			if (sessionWoman.userWoman.characterList[sListCharacterIndex].gameCharacter.gameLogined == true) {

				int iTutorialStatus;

				int.TryParse(sessionWoman.userWoman.characterList[sListCharacterIndex].gameCharacter.tutorialStatus,out iTutorialStatus);

				if (sGoto.Equals(string.Format("GameDisplayDoc.aspx?doc={0}",PwViCommConst.SCR_WOMAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS))) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sGoto + "&site=" + sSiteCd + "&charno=" + sCharNo));
				} else if (sGoto.Equals("GameDisplayDoc.aspx")) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"GameDisplayDoc.aspx?doc=" + this.GetStringValue(Request.QueryString["doc"]) + "&site=" + sessionWoman.site.siteCd + "&charno=" + sCharNo));
				} else if (!string.IsNullOrEmpty(sGoto)) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sGoto));
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"GameUserTop.aspx" + "?site=" + sSiteCd + "&charno=" + sCharNo));
				}
			} else {
				if (sessionWoman.userWoman.characterList[sListCharacterIndex].registIncompleteFlag == 1 || sessionWoman.userWoman.characterList[sListCharacterIndex].profileResettingFlag == 1) {
					string[] sRequest = Request.Path.Split('/');
					if (!sRequest[sRequest.Length - 1].Equals("ModifyUserProfile.aspx")) {
						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ModifyUserProfile.aspx?site=" + sSiteCd + "&charno=" + sCharNo));
					}
				}
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				oParameters.Add("site",sSiteCd);
				oParameters.Add("charno",sCharNo);
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_NON_USER_TOP_FOR_CHAT_USER,oParameters);
			}
			
		} else {
			if (sessionWoman.userWoman.loginErrMsgCd.Equals(ViCommConst.ERR_STATUS_CAST_LOGIN_RESIGNED)) {
				string sResult = string.Empty;
				sessionWoman.userWoman.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_LOGINID,sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword,out sResult);
				if (sResult.Equals("0")) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,string.Format("GameLoginUser.aspx?loginid={0}&password={1}",sessionWoman.userWoman.loginId,sessionWoman.userWoman.loginPassword)));
				}
			}
			txtLoginId.Text = HttpUtility.HtmlEncode(txtLoginId.Text);
			txtPassword.Text = HttpUtility.HtmlEncode(txtPassword.Text);
			
			lblErrorMessage.Text = GetErrorMessage(sessionWoman.userWoman.loginErrMsgCd);
		}
	}

	//マルチスレッド問題回避
	private string GetStringValue(object value) {
		if (value == null) {
			return "";
		} else {
			return value.ToString();
		}
	}
}
