/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールフォーム
--	Progaram ID		: MailForm
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Text;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text.RegularExpressions;

public partial class ViComm_woman_MailForm:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.errorMessage = string.Empty;
			tagMailWrite.Visible = true;
			tagMailConfirm.Visible = false;
			tagMailComplete.Visible = false;
			ViewState["SUBJECT"] = string.Empty;
			ViewState["BODY"] = string.Empty;
			ViewState["FROM_ADDR"] = string.Empty;
			ViewState["TO_ADDR"] = string.Empty;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sFromAddr = iBridUtil.GetStringValue(Request.Form["from"]);
		if (sFromAddr.Equals(string.Empty)) {
			sessionWoman.errorMessage = "ﾒｰﾙｱﾄﾞﾚｽを入力してください<BR />";
			return;
		} else {
			Regex rgx = new Regex(@"[0-9A-Za-z\._-]+@[a-zA-Z0-9_-]+\.[a-zA-Z0-9\._-]+");
			Match rgxMatch = rgx.Match(sFromAddr);
			if (!rgxMatch.Success) {
				sessionWoman.errorMessage = "ﾒｰﾙｱﾄﾞﾚｽを正しい形式で入力してください<BR />";
				return;
			}
		}

		string sParamsInput = string.Empty;
		string sParamsName = string.Empty;
		StringBuilder sb = new StringBuilder();
		foreach (string sParams in Request.Form.AllKeys) {
			if (sParams.StartsWith("*")) {
				sParamsInput = iBridUtil.GetStringValue(Request.Form[sParams.Replace("*",string.Empty)]);
				sParamsName = iBridUtil.GetStringValue(Request.Form[sParams]);

				if (!sParamsInput.Equals("")) {
					sb.Append(string.Format("{0}:{1}",sParamsName,sParamsInput)).AppendLine();
				} else {
					sessionWoman.errorMessage = string.Format("{0}を入力してください<BR />",sParamsName);
					return;
				}
			}
		}

		AppendDefaultBody(ref sb);
		ViewState["SUBJECT"] = iBridUtil.GetStringValue(Request.Form["subject"]);
		ViewState["BODY"] = sb.ToString();
		ViewState["FROM_ADDR"] = sFromAddr;
		//送信先は自己責任
		ViewState["TO_ADDR"] = iBridUtil.GetStringValue(Request.Form["to"]);
		tagMailWrite.Visible = false;
		tagMailConfirm.Visible = true;
		tagMailComplete.Visible = false;
	}

	protected void cmdTx_Click(object sender,EventArgs e) {
		string sToMailAddress = iBridUtil.GetStringValue(ViewState["TO_ADDR"]);
		string sFromMailAddress = iBridUtil.GetStringValue(ViewState["FROM_ADDR"]);
		string sTitle = iBridUtil.GetStringValue(ViewState["SUBJECT"]);
		string sDoc = iBridUtil.GetStringValue(ViewState["BODY"]);
		//メールアドレスの形式をチェック
		try {
			new Agiletech.Net.Mail.MailAddress(sFromMailAddress);
		} catch (FormatException) {
			sFromMailAddress = sToMailAddress;
		}

		SendMail(sFromMailAddress,sToMailAddress,sTitle,sDoc);
		tagMailWrite.Visible = false;
		tagMailConfirm.Visible = false;
		tagMailComplete.Visible = true;
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		tagMailWrite.Visible = true;
		tagMailConfirm.Visible = false;
		tagMailComplete.Visible = false;
	}

	private void AppendDefaultBody(ref StringBuilder pBody) {
		pBody.Append("\r\n------------------------------").AppendLine();
		pBody.Append("送信者情報").AppendLine();
		if (sessionWoman.logined) {
			pBody.AppendFormat("ﾛｸﾞｲﾝID:{0}",sessionWoman.userWoman.loginId).AppendLine();
			pBody.AppendFormat("登録ﾒｰﾙｱﾄﾞﾚｽ:{0}",sessionWoman.userWoman.emailAddr).AppendLine();
		} else {
			pBody.Append("未ﾛｸﾞｲﾝ").AppendLine();
		}
		pBody.AppendFormat("USER-AGENT:{0}",sessionWoman.mobileUserAgent).AppendLine();
		if (!sessionWoman.currentIModeId.Equals(string.Empty)) {
			pBody.AppendFormat("GUID:{0}",sessionWoman.currentIModeId).AppendLine();
		}
	}
}