﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ミンクル確認
--	Progaram ID		: ConfirmRegistMinkuru
--
--  Creation Date	: 2011.05.02
--  Creater			: i-Brid
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ConfirmRegistMinkuru : MobileWomanPageBase {
	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsCorrectPageLoad()) {
			this.RedirectToMobilePage(this.sessionWoman.site.url);
		}

		this.Response.Filter = this.sessionWoman.InitScreen(this.Response.Filter, this, this.Request, this.ViewState, this.IsPostBack);

		if (IsPostBack) {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				this.cmdSubmit_Click(null, new EventArgs());
			}
		}

	}

	protected void cmdSubmit_Click(object sender, EventArgs e) {
		Encoding sjis = Encoding.GetEncoding(932);

		Response.Redirect(string.Format("http://minkuru.tv/tel2colo.html?guid=on&page=regist&id={0}&hname={1}&ymd={2}&ar={3}&bl={4}",
										HttpUtility.UrlEncode(sessionWoman.userWoman.loginId,sjis),
										HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["handlename"]),sjis),
										HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["year"]) + iBridUtil.GetStringValue(Request.QueryString["month"]) + iBridUtil.GetStringValue(Request.QueryString["day"]),sjis),
										HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["location"]),sjis),
										HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["blood"]),sjis)));
	}

	private bool IsCorrectPageLoad() {
		return sessionWoman.logined && !sessionWoman.IsImpersonated && this.IsCarrierContainsOfFeature();
	}

	private bool IsCarrierContainsOfFeature() {
		switch (this.sessionWoman.carrier) {
			case ViCommConst.DOCOMO:
			case ViCommConst.KDDI:
			case ViCommConst.SOFTBANK:
				return true;
			default:
				return false;
		}
	}

}
