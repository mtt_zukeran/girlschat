/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾅﾝﾊﾟ
--	Progaram ID		: RegistGameClearMission.aspx
--
--  Creation Date	: 2011.09.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistGameClearMission:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sStageSeq = iBridUtil.GetStringValue(Request.QueryString["stage_seq"]);
			string sTownSeq = iBridUtil.GetStringValue(Request.QueryString["town_seq"]);
			string sRecoveryFlag = iBridUtil.GetStringValue(Request.QueryString["recovery_flag"]);
			string sNoRecoveryItemFlag = iBridUtil.GetStringValue(Request.QueryString["no_recovery_item_flag"]);

			if (!sStageSeq.Equals(string.Empty) && !sTownSeq.Equals(string.Empty)) {
				int iTutorialFlag = ViCommConst.FLAG_OFF;
				int iLevelUpFlag;			//レベルアップしたら1
				int iTownFirstClearFlag;	//初めてステージクリアしたら1
				int iAddForceCount;			//レベルアップや、街クリアで増えた部隊数
				string sGetTreasureLogSeq;	//獲得したお宝履歴SEQ なしの場合NULL
				string[] sGetGameItemSeq;	//獲得したアイテム
				string[] sGetGameItemCount;	//獲得したアイテム数
				int iGetRecordCount;		//獲得したアイテム種類数
				string[] sBreakGameItemSeq;	//壊れた所持アイテム
				int iBreakRecordCount;		//壊れた所持アイテム種類数
				string sResult;
				int iTreasureCompleteFlag = ViCommConst.FLAG_OFF;	//お宝をコンプリートしたら1(既にコンプリート済みの場合は2)
				int iBonusGetFlag = ViCommConst.FLAG_OFF;
				string sGetTreasureResult = PwViCommConst.GameGetTreasureResult.RESULT_OK;
				string sBuyItemResult = PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK;
				int? iItemAbsoluteGetFlag = null;
				int? iTreasureAbsoluteGetFlag = null;
				string sPreExp = this.sessionWoman.userWoman.CurCharacter.gameCharacter.exp.ToString();
				string sPreGamePoint = this.sessionWoman.userWoman.CurCharacter.gameCharacter.gamePoint.ToString();
				string sPreMissionForceCount = this.sessionWoman.userWoman.CurCharacter.gameCharacter.missionForceCount.ToString();

				if (sRecoveryFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.RecoveryForce(sNoRecoveryItemFlag);
				}

				if (sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("1")) {
					iItemAbsoluteGetFlag = 0;
					iTreasureAbsoluteGetFlag = 0;
					iTutorialFlag = ViCommConst.FLAG_ON;
				} else if (sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("2")) {
					iItemAbsoluteGetFlag = 0;
					iTreasureAbsoluteGetFlag = 0;
					iTutorialFlag = ViCommConst.FLAG_ON;
				} else if (sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus.Equals("3")) {
					iItemAbsoluteGetFlag = 0;
					iTreasureAbsoluteGetFlag = 1;
					iTutorialFlag = ViCommConst.FLAG_ON;
				}

				using (Town oTown = new Town()) {
					oTown.ClearMission(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						sStageSeq,
						sTownSeq,
						iItemAbsoluteGetFlag,
						iTreasureAbsoluteGetFlag,
						out iLevelUpFlag,
						out iTownFirstClearFlag,
						out iAddForceCount,
						out sGetTreasureLogSeq,
						out sGetGameItemSeq,
						out sGetGameItemCount,
						out iGetRecordCount,
						out sBreakGameItemSeq,
						out iBreakRecordCount,
						out sResult
					);
				}

				string sGetGameItemSeqList = string.Join("_",sGetGameItemSeq);
				string sGetGameItemCountList = string.Join("_",sGetGameItemCount);
				string sBreakGameItemSeqList = string.Join("_",sBreakGameItemSeq);

				StringBuilder oStrBuilder = new StringBuilder();

				if (sResult.Equals(PwViCommConst.GameClearMissionResult.RESULT_NG)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				} else if (sResult.Equals(PwViCommConst.GameClearMissionResult.RESULT_OK)) {

					if (!string.IsNullOrEmpty(sGetTreasureLogSeq)) {
						using (Town oTown = new Town()) {
							oTown.GetTreasure(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							sGetTreasureLogSeq,
							out iTreasureCompleteFlag,
							out iBonusGetFlag,
							out sGetTreasureResult
						);

							if (!sGetTreasureResult.Equals(PwViCommConst.GameGetTreasureResult.RESULT_OK)) {
								RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
							}

						}
					}

					if (iTutorialFlag.Equals(ViCommConst.FLAG_ON)) {
						int iNextTutorialStatus = int.Parse(sessionWoman.userWoman.CurCharacter.gameCharacter.tutorialStatus) + 1;

						using (GameCharacter oGameCharacter = new GameCharacter()) {
							oGameCharacter.SetupTutorialStatus(
								sessionWoman.site.siteCd,
								sessionWoman.userWoman.userSeq,
								sessionWoman.userWoman.curCharNo,
								iNextTutorialStatus.ToString()
							);
						}
					}

					this.CheckQuestClear();

					string sPartnerUserSeq = string.Empty;
					string sPartnerUserCharNo = string.Empty;
					string sNextPageNm = string.Empty;
					
					using (UserManGameCharacter oUserManGameCharacter = new UserManGameCharacter()) {
						string sAllFlag = iBridUtil.GetStringValue(this.Request.QueryString["all_flag"]);
						oUserManGameCharacter.GetSeqForMission(this.sessionWoman.site.siteCd,this.sessionWoman.userWoman.userSeq,this.sessionWoman.userWoman.curCharNo,sAllFlag,out sPartnerUserSeq,out sPartnerUserCharNo);
					}

					oStrBuilder.AppendFormat("?result={0}",sResult);
					oStrBuilder.AppendFormat("&stage_seq={0}",sStageSeq);
					oStrBuilder.AppendFormat("&town_seq={0}",sTownSeq);
					oStrBuilder.AppendFormat("&partner_user_seq={0}",sPartnerUserSeq);
					oStrBuilder.AppendFormat("&partner_user_char_no={0}",sPartnerUserCharNo);
					oStrBuilder.AppendFormat("&level_up_flag={0}",iLevelUpFlag.ToString());
					oStrBuilder.AppendFormat("&town_first_clear_flag={0}",iTownFirstClearFlag.ToString());
					oStrBuilder.AppendFormat("&add_force_count={0}",iAddForceCount.ToString());
					oStrBuilder.AppendFormat("&get_game_item_seq={0}",sGetGameItemSeqList);
					oStrBuilder.AppendFormat("&get_game_item_count={0}",sGetGameItemCountList);
					oStrBuilder.AppendFormat("&get_record_count={0}",iGetRecordCount.ToString());
					oStrBuilder.AppendFormat("&break_game_item_seq={0}",sBreakGameItemSeqList);
					oStrBuilder.AppendFormat("&break_record_count={0}",iBreakRecordCount.ToString());
					oStrBuilder.AppendFormat("&get_treasure_log_seq={0}",sGetTreasureLogSeq);
					oStrBuilder.AppendFormat("&pre_exp={0}",sPreExp);
					oStrBuilder.AppendFormat("&game_point={0}",sPreGamePoint);
					oStrBuilder.AppendFormat("&pre_mission_force_count={0}",sPreMissionForceCount);

					if (!string.IsNullOrEmpty(sGetTreasureLogSeq)) {
						oStrBuilder.AppendFormat("&treasure_complete_flag={0}",iTreasureCompleteFlag.ToString());
						oStrBuilder.AppendFormat("&bonus_get_flag={0}",iBonusGetFlag.ToString());
						oStrBuilder.AppendFormat("&get_treasure_result={0}",sGetTreasureResult);
					}

					if(sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
						IDictionary<string,string> oParameters = new Dictionary<string,string>();

						oParameters.Add("result",sResult);
						oParameters.Add("stage_seq",sStageSeq);
						oParameters.Add("town_seq",sTownSeq);
						oParameters.Add("partner_user_seq",sPartnerUserSeq);
						oParameters.Add("partner_user_char_no",sPartnerUserCharNo);
						oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
						oParameters.Add("town_first_clear_flag",iTownFirstClearFlag.ToString());
						oParameters.Add("add_force_count",iAddForceCount.ToString());
						oParameters.Add("get_game_item_seq",sGetGameItemSeqList);
						oParameters.Add("get_game_item_count",sGetGameItemCountList);
						oParameters.Add("get_record_count",iGetRecordCount.ToString());
						oParameters.Add("break_game_item_seq",sBreakGameItemSeqList);
						oParameters.Add("break_record_count",iBreakRecordCount.ToString());
						oParameters.Add("get_treasure_log_seq",sGetTreasureLogSeq);
						oParameters.Add("pre_exp",sPreExp);
						oParameters.Add("game_point",sPreGamePoint);
						oParameters.Add("pre_mission_force_count",sPreMissionForceCount);

						if (!string.IsNullOrEmpty(sGetTreasureLogSeq)) {
							oParameters.Add("treasure_complete_flag",iTreasureCompleteFlag.ToString());
							oParameters.Add("bonus_get_flag",iBonusGetFlag.ToString());
							oParameters.Add("get_treasure_result",sGetTreasureResult);
						}

						if (!string.IsNullOrEmpty(sGetTreasureLogSeq)) {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_GET_MAN_TREASURE_SP,oParameters);
						} else if (iTownFirstClearFlag == ViCommConst.FLAG_ON) {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_MOKUHYOU_SP,oParameters);
						} else if (iLevelUpFlag == ViCommConst.FLAG_ON) {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_LEVELUP_SP,oParameters);
						} else {
							sNextPageNm = "ViewGameTownClearMissionOK.aspx";
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sNextPageNm + oStrBuilder.ToString()));
						}
					} else {
						if (!string.IsNullOrEmpty(sGetTreasureLogSeq)) {
							sNextPageNm = "ViewGameFlashGetCastTreasure.aspx";
						} else if (iTreasureCompleteFlag == ViCommConst.FLAG_ON) {
							sNextPageNm = "ViewGameFlashComplete.aspx";
						} else if (iTownFirstClearFlag == ViCommConst.FLAG_ON) {
							sNextPageNm = "ViewGameFlashMokuhyo.aspx";
						} else if (iLevelUpFlag == ViCommConst.FLAG_ON) {
							sNextPageNm = "ViewGameFlashLevelup.aspx";
						} else {
							sNextPageNm = "ViewGameTownClearMissionOK.aspx";
						}

						RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sNextPageNm + oStrBuilder.ToString()));
					}
				} else if (sResult.Equals(PwViCommConst.GameClearMissionResult.RESULT_NO_ITEM)) {

					oStrBuilder.Append("ListGameClearTownUsedItemShortage.aspx");
					oStrBuilder.AppendFormat("?stage_seq={0}",sStageSeq);
					oStrBuilder.AppendFormat("&town_seq={0}",sTownSeq);
					oStrBuilder.AppendFormat("&mission_result={0}",sResult);
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,oStrBuilder.ToString()));
				} else {
					oStrBuilder.Append("ViewGameTownClearMissionNG.aspx");
					oStrBuilder.AppendFormat("?result={0}",sResult);
					oStrBuilder.AppendFormat("&stage_seq={0}",sStageSeq);
					oStrBuilder.AppendFormat("&town_seq={0}",sTownSeq);
					oStrBuilder.AppendFormat("&buy_item_result={0}",sBuyItemResult);

					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,oStrBuilder.ToString()));
				}
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	private void RecoveryForce(string sNoRecoveryItemFlag) {
		string sGameItemSeq;
		string sRecoveryForceResult;

		using (GameItem oGameItem = new GameItem()) {
			sGameItemSeq = oGameItem.GetGameItem(
				this.sessionWoman.site.siteCd,
				ViCommConst.OPERATOR,
				PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE,
				"GAME_ITEM_SEQ"
			);

			if (sNoRecoveryItemFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				string sBuyGameItemResult;

				oGameItem.BuyGameItem(
					this.sessionWoman.site.siteCd,
					this.sessionWoman.userWoman.userSeq,
					this.sessionWoman.userWoman.curCharNo,
					sGameItemSeq,
					"1",
					out sBuyGameItemResult
				);

				if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
					sessionWoman.userWoman.CurCharacter.gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);
				} else if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_NO_MONEY)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_NO_POINT);
				} else if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_NG)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
				}
			}
		}

		using (RecoveryForce oRecoveryForce = new RecoveryForce()) {
			oRecoveryForce.RecoveryForceFull(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				0,
				int.Parse(sGameItemSeq),
				PwViCommConst.GameMskForce.MSK_ALL_FORCE,
				out sRecoveryForceResult
			);
		}

		if (!sRecoveryForceResult.Equals(PwViCommConst.GameRecoveryForceFullStatus.RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionWoman.site.siteCd,
				this.sessionWoman.userWoman.userSeq,
				this.sessionWoman.userWoman.curCharNo,
				PwViCommConst.GameQuestTrialCategory.MISSION,
				PwViCommConst.GameQuestTrialCategoryDetail.MISSION,
				out sQuestClearFlag,
				out sResult,
				ViCommConst.OPERATOR
			);
		}
	}
}
