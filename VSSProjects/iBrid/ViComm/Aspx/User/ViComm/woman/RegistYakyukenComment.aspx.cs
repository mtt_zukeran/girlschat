/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳コメント追加
--	Progaram ID		: RegistYakyukenComment
--
--  Creation Date	: 2013.04.29
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_RegistYakyukenComment:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (CheckInputValue()) {
			string sResult = string.Empty;

			using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
				oYakyukenComment.RegistYakyukenComment(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtCommentText.Text)),
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListYakyukenComment.aspx"));
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}

	protected bool CheckInputValue() {
		bool bOk = true;
		string sNGWord;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtCommentText.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄを入力して下さい。";
		} else if (txtCommentText.Text.Length < 10) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は10文字以上です。";
		} else if (txtCommentText.Text.Length > 50) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は50文字以内です。";
		} else if (sessionWoman.ngWord.VaidateDoc(txtCommentText.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄに禁止語句が含まれています。";
		}

		return bOk;
	}
}
