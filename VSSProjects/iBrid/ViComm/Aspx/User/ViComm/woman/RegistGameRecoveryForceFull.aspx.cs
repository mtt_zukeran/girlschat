/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹް� ������
--	Progaram ID		: RegistGameRecoveryForceFull
--
--  Creation Date	: 2011.09.21
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_woman_RegistGameRecoveryForceFull:MobileSocialGameWomanBase {
	private NameValueCollection query = new NameValueCollection();

	protected void Page_Load(object sender,EventArgs e) {
		string sItemSeq = iBridUtil.GetStringValue(this.Request.QueryString["item_seq"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sItemSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		if (!this.CheckPossessionRecoveryItem(sItemSeq)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_RECOVERY_FORCE_FULL,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.RecoveryForceFull();
			}
		}
	}

	private void RecoveryForceFull() {
		string[] sValues = this.Request.Params["NEXTLINK"].Split('_');
		string sGameItemSeq = sValues[0];
		string sGameItemPossessionCount = sValues[1];
		string sGameItemNm = sValues[2];
		string result = this.RecoveryForceFullExecute(sGameItemSeq);
		string sPageId = iBridUtil.GetStringValue(this.Request.QueryString["page_id"]);
		string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.Params["battle_log_seq"]);
		
		if (result.Equals(PwViCommConst.GameRecoveryForceFullStatus.RESULT_OK)) {
			this.query["item_seq"] = sGameItemSeq;
			this.query["item_nm"] = sGameItemNm;
			int iItemCount = int.Parse(sGameItemPossessionCount) - 1;
			this.query["item_count"] = iItemCount.ToString();

			if (sPageId == PwViCommConst.PageId.SUPPORT_BATTLE){
				this.query["page_id"] = sPageId;
				this.query["battle_log_seq"] = sBattleLogSeq;
			}

			Session["PageId"] = null;
			Session["BattleLogSeq"] = null;
			
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_RECOVERY_FORCE_FULL_COMPLETE,this.query);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
	}

	private string RecoveryForceFullExecute(string sGameItemSeq) {
		string sResult = "";
		int iGameItemSeq = int.Parse(sGameItemSeq);
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		int iUseCoopPointFlag = 0;
		int iMskForce = PwViCommConst.GameMskForce.MSK_ALL_FORCE;

		RecoveryForce oRecoveryForce = new RecoveryForce();
		oRecoveryForce.RecoveryForceFull(sSiteCd,sUserSeq,sUserCharNo,iUseCoopPointFlag,iGameItemSeq,iMskForce,out sResult);

		return sResult;
	}

	private bool CheckPossessionRecoveryItem(string sGameItemSeq) {
		PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition();
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_RECOVERY_FORCE_FULL;
		oCondition.SiteCd = this.sessionWoman.site.siteCd;
		oCondition.UserSeq = this.sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = this.sessionWoman.userWoman.curCharNo;
		oCondition.ItemSeq = sGameItemSeq;
		oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE;
		oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.SexCd = ViCommConst.OPERATOR;

		DataSet oDataSet = null;

		using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			oDataSet = oPossessionGameItem.GetPossessionItem(oCondition);
		}

		int iPossessionCount = 0;

		int.TryParse(oDataSet.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString(),out iPossessionCount);

		if (iPossessionCount > 0) {
			return true;
		} else {
			return false;
		}
	}
}