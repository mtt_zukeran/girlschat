/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���ٍ��ݒ�(�߰è�����)
--	Progaram ID		: RegistGameBattleStartParty
--
--  Creation Date	: 2011.09.22
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_RegistGameBattleStartParty:MobileSocialGameWomanBase {
	private NameValueCollection query = new NameValueCollection();

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

			if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}

			if (!sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_START_PARTY,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		} else {
			if (!string.IsNullOrEmpty(this.Request.Params["cmdNext"])) {

				string[] textArr = this.Request.Params["NEXTLINK"].ToString().Split('_');

				string sPartnerUserSeq = textArr[0];
				string sPartnerUserCharNo = textArr[1];
				string sTreasureSeq = textArr[2];

				int iAttackForceCount;
				iAttackForceCount = this.sessionWoman.userWoman.CurCharacter.gameCharacter.attackMaxForceCount;

				this.BattleStart(sPartnerUserSeq,sPartnerUserCharNo,sTreasureSeq,iAttackForceCount);
			}
		}
	}

	private void BattleStart(string sPartnerUserSeq,string sPartnerUserCharNo,string sTreasureSeq,int iAttackForceCount) {
		string sSiteCd = this.sessionWoman.site.siteCd;
		string sUserSeq = this.sessionWoman.userWoman.userSeq;
		string sUserCharNo = this.sessionWoman.userWoman.curCharNo;
		string sSupportUserSeq = string.Empty;
		string sSupportUserCharNo = string.Empty;
		string sSupportBattleLogSeq = string.Empty;
		string sSupportRequestSubSeq = string.Empty;
		string sBattleType = PwViCommConst.GameBattleType.PARTY;
		int iSurelyWinFlag = 0;
		int iLevelUpFlag;
		int iAddForceCount;
		int iTreasureCompleteFlag;
		int iBonusGetFlag;
		int iWinFlag;
		int iComePoliceFlag;
		int iTrapUsedFlag;
		string sBattleLogSeq;
		string[] BreakGameItemSeq;
		string sResult;

		string sPreExp = this.sessionWoman.userWoman.CurCharacter.gameCharacter.exp.ToString();

		Battle oBattle = new Battle();
		oBattle.BattleStart(
			sSiteCd,
			sUserSeq,
			sUserCharNo,
			sPartnerUserSeq,
			sPartnerUserCharNo,
			sSupportUserSeq,
			sSupportUserCharNo,
			sSupportBattleLogSeq,
			sSupportRequestSubSeq,
			sBattleType,
			sTreasureSeq,
			iSurelyWinFlag,
			iAttackForceCount,
			out iLevelUpFlag,
			out iAddForceCount,
			out iTreasureCompleteFlag,
			out iBonusGetFlag,
			out iWinFlag,
			out iComePoliceFlag,
			out iTrapUsedFlag,
			out sBattleLogSeq,
			out BreakGameItemSeq,
			out sResult
		);

		string sRedirectUrl = string.Empty;

		if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_OK)) {
			this.CheckQuestClear(sUserSeq,sUserCharNo);
			this.CheckQuestClear(sPartnerUserSeq,sPartnerUserCharNo);
			
			if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
				string sPartnerGameCharacterType = this.GetGameCharacterType(sPartnerUserSeq,sPartnerUserCharNo);
				string sPartnerGamePartyMsk = this.GetPartyMemberMsk(sPartnerUserSeq,sPartnerUserCharNo);

				IDictionary<string,string> oParameters = new Dictionary<string,string>();

				if (iWinFlag == ViCommConst.FLAG_ON) {
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("treasure_complete_flag",iTreasureCompleteFlag.ToString());
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
					oParameters.Add("partner_party_msk",sPartnerGamePartyMsk);
				} else {
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("come_police_flag",iComePoliceFlag.ToString());
					oParameters.Add("trap_used_flag",iTrapUsedFlag.ToString());
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
					oParameters.Add("partner_party_msk",sPartnerGamePartyMsk);
				}
				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_PARTY_BATTLE_SP,oParameters);
			} else {
				if (iWinFlag == ViCommConst.FLAG_ON) {
					sRedirectUrl = String.Format(
										"ViewGameFlashPartyBattle.aspx?battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&treasure_complete_flag={3}&pre_exp={4}&battle_type={5}",
										sBattleLogSeq,
										iLevelUpFlag.ToString(),
										iAddForceCount.ToString(),
										iTreasureCompleteFlag,
										sPreExp,
										PwViCommConst.GameBattleType.PARTY
									);
				} else {
					sRedirectUrl = String.Format(
										"ViewGameFlashPartyBattle.aspx?battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&come_police_flag={3}&trap_used_flag={4}&pre_exp={5}&battle_type={6}",
										sBattleLogSeq,
										iLevelUpFlag.ToString(),
										iAddForceCount.ToString(),
										iComePoliceFlag.ToString(),
										iTrapUsedFlag.ToString(),
										sPreExp,
										PwViCommConst.GameBattleType.PARTY
									);
				}

				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sRedirectUrl));
			}
		} else if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		} else {
			this.query["result"] = sResult;
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_BATTLE_NO_FINISH,this.query);
		}
	}

	private string GetGameCharacterType(string sPartnerUserSeq,string sPartnerUserCharNo) {
		string sGameCharacterType = string.Empty;

		GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.PartnerUserSeq = sPartnerUserSeq;
		oCondition.PartnerUserCharNo = sPartnerUserCharNo;

		using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
			DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);

			if (ds.Tables[0].Rows.Count > 0) {
				sGameCharacterType = ds.Tables[0].Rows[0]["GAME_CHARACTER_TYPE"].ToString();
			}
		}

		return sGameCharacterType;
	}

	private string GetPartyMemberMsk(string sUserSeq,string sUserCharNo) {
		string sPartyMemberMsk = string.Empty;

		GamePartyBattleMemberSeekCondition oCondition = new GamePartyBattleMemberSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sUserSeq;
		oCondition.UserCharNo = sUserCharNo;

		using (GamePartyBattleMember oGamePartyBattleMember = new GamePartyBattleMember()) {
			sPartyMemberMsk = oGamePartyBattleMember.GetPartyMemberMask(oCondition);
		}

		if (sPartyMemberMsk.Equals("0")) {
			sPartyMemberMsk = "8";
		}

		return sPartyMemberMsk;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionWoman.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				ViCommConst.OPERATOR
			);
		}
	}
}
