/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �싅���摜�ڍ�
--	Progaram ID		: ViewYakyukenPic
--
--  Creation Date	: 2013.05.01
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewYakyukenPic:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["ypicseq"]))) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			if (!sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_YAKYUKEN_PIC_VIEW,this.ActiveForm)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}
}
