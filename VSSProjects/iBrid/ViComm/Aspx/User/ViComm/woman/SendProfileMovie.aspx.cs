/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフ動画送信設定

--	Progaram ID		: SendProfileMovie
--
--  Creation Date	: 2010.10.28
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;
using System.Web;

public partial class ViComm_woman_SendProfileMovie : MobileWomanPageBase {

	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender, e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender, EventArgs e) {
		if (!ValidateInput()) return;
		
		string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier, txtTitle.Text));
		string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier, txtDoc.Text));
		string sObjTempId = string.Empty;
		int iObjRankingFlag = ViCommConst.FLAG_ON;

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]))) {
			int.TryParse(iBridUtil.GetStringValue(this.Request.Form["lstRankingFlag"]),out iObjRankingFlag);
		}

		using (WaitObj oWaitObj = new WaitObj()) {
			oWaitObj.WriteWaitObj(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
				sTitle,
				sDoc,
				ViCommConst.ATTACHED_PROFILE.ToString(),
				ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString(),
				ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString(),
				ViCommConst.FLAG_OFF,
				string.Empty,
				ViCommConst.FLAG_OFF,
				iObjRankingFlag,
				out sObjTempId
			);
		}

		sessionWoman.userWoman.ObjTempId = sObjTempId;
		RedirectToDisplayDoc(ViCommConst.SCR_WOMAN_MOVIE_PROFILE_MAILER_COMPLITE);
		
	}

	private bool ValidateInput() {
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (txtTitle.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE) + "<br />";
		}
		if (txtDoc.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC) + "<br />";
		}
		return bOk;
	}

}
