﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザー掲示板一覧
--	Progaram ID		: ListUserBbs
--
--  Creation Date	: 2010.04.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListUserBbs:MobileWomanPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			} else {
				sessionWoman.ControlList(Request,ViCommConst.INQUIRY_BBS_DOC,ActiveForm);
			}
		}
	}
}
