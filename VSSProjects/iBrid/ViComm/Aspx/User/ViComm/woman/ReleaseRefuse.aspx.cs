/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���ۃ��X�g����
--	Progaram ID		: ReleaseRefuse
--
--  Creation Date	: 2009.08.07
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ReleaseRefuse:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			DataRow dr;
			if (sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),iRecNo,out dr)) {
				string sUserStatus = dr["USER_STATUS"].ToString();
				if (sUserStatus.Equals(ViCommConst.USER_MAN_HOLD) || sUserStatus.Equals(ViCommConst.USER_MAN_RESIGNED) || sUserStatus.Equals(ViCommConst.USER_MAN_STOP) || sUserStatus.Equals(ViCommConst.USER_MAN_BLACK)) {
					RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_MAN);
				}
				using (Refuse oRefuse = new Refuse()) {
					oRefuse.RefuseMainte(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						ViCommConst.OPERATOR,
						dr["USER_SEQ"].ToString(),
						dr["USER_CHAR_NO"].ToString(),
						"9",
						"",
						1);
				}
			}
		}
	}

}
