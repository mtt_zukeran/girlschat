/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����ٹްїpFlash(����)
--	Progaram ID		: ViewGameFlashLottery
--
--  Creation Date	: 2011.11.14
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_ViewGameFlashLottery:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["battle_log_seq"]);

		string sFileNm = "lottery.swf";
		string sNextPageNm = string.Empty;
		string sGetParamStr = string.Empty;

		sNextPageNm = "ViewGameLotteryGetItemResult.aspx";

		if (this.Request.QueryString.Count > 0) {
			string[] keys = this.Request.QueryString.AllKeys;
			string value = string.Empty;
			List<string> oGetParamList = new List<string>();

			foreach (string key in keys) {
				oGetParamList.Add(key + "=" + iBridUtil.GetStringValue(this.Request.QueryString[key]));
			}

			sGetParamStr = string.Join("&",oGetParamList.ToArray());
		}

		NameValueCollection oSwfParam = new NameValueCollection();
		GameFlashLiteHelper.ResponseFlashSocialGame(sFileNm,sNextPageNm,sGetParamStr,oSwfParam);
	}

	private void GetBattleLogData(
		string sBattleLogSeq,
		out string sAttackWinFlag,
		out string sDefenceWinFlag,
		out string sBattleType
	) {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.BattleLogSeq = sBattleLogSeq;

		DataSet pDS = null;
		using (BattleLog oBattleLog = new BattleLog()) {
			pDS = oBattleLog.GetOneSimpleData(oCondition);
		}

		sAttackWinFlag = pDS.Tables[0].Rows[0]["ATTACK_WIN_FLAG"].ToString();
		sDefenceWinFlag = pDS.Tables[0].Rows[0]["DEFENCE_WIN_FLAG"].ToString();
		sBattleType = pDS.Tables[0].Rows[0]["BATTLE_TYPE"].ToString();
	}
}