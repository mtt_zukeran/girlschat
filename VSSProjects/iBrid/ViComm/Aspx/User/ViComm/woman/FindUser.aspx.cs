/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 検索条件指定
--	Progaram ID		: FindUser
--
--  Creation Date	: 2009.07.16
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public partial class ViComm_woman_FindUser:MobileWomanPageBase {
	private const string ONLINE_STATUS = "FindUserOnlineStatus";
	private const string NEWMAN_FLG = "FindUserNewManFlg";
	private const string HANDLE_NAME = "FindUserHandleNm";
	private const string BIRTHDAY_FROM = "FindUserBirthdayFrom";
	private const string BIRTHDAY_TO = "FindUserBirthdayTo";
	private const string MAN_ITEM = "FindUserManItem";
	private const string AGE = "FindUserAge";
	private const string MAIL_SEND_COUNT = "FindUserMailSendCount";

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		int iIdx = 0;

		if (!IsPostBack) {

			using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
				DataSet dsAttrType = oUserManAttrType.GetList(sessionWoman.site.siteCd);
				SelectionList lstUserManItem;
				iBMobileLabel lblUserManItem;
				iBMobileTextBox txtUserManItem;
				Dictionary<string,string> manAttrList = (Dictionary<string,string>)Session[MAN_ITEM];

				foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
					if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",iIdx + 1)) as iBMobileLabel;
						lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iIdx + 1)) as SelectionList;
						txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iIdx + 1)) as iBMobileTextBox;

						lblUserManItem.Text = drAttrType["MAN_ATTR_TYPE_FIND_NM"].ToString();

						if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
							lstUserManItem.Visible = false;

							// 入力値の復元を行います。
							if (manAttrList != null) {
								if (manAttrList.ContainsKey(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString())) {
									string val = manAttrList[drAttrType["MAN_ATTR_TYPE_SEQ"].ToString()];
									txtUserManItem.Text = val;
								}
							}
						} else {
							txtUserManItem.Visible = false;

							//lstUserManItem.Items.Add(new MobileListItem("指定なし","*"));
							if (string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {

								using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
									DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionWoman.site.siteCd,drAttrType["MAN_ATTR_TYPE_SEQ"].ToString());
									foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
										if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
											lstUserManItem.Items.Add(new MobileListItem(drAttrTypeValue["MAN_ATTR_NM"].ToString(),drAttrTypeValue["MAN_ATTR_SEQ"].ToString()));
										}
									}
								}
							} else {
								using (CodeDtl oCodeDtl = new CodeDtl()) {
									DataSet dsGroup = oCodeDtl.GetList(drAttrType["GROUPING_CATEGORY_CD"].ToString());
									foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
										lstUserManItem.Items.Add(new MobileListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
									}
								}
							}
							if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_RADIO)) {
								lstUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
								if (manAttrList == null) {
									lstUserManItem.SelectedIndex = 0;
								}
							}

							// 入力値の復元を行います。
							if (manAttrList != null) {
								if (manAttrList.ContainsKey(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString())) {
									string val = manAttrList[drAttrType["MAN_ATTR_TYPE_SEQ"].ToString()];
									foreach (MobileListItem item in lstUserManItem.Items) {
										if (item.Value == val) {
											item.Selected = true;
										}
									}
								}
							}
						}
						iIdx++;
					}
				}
			}
			for (int i = iIdx;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i + 1)) as Panel;
				pnlUserManItem.Visible = false;
			}

			int iOnlineFlag;
			int.TryParse(iBridUtil.GetStringValue(Session[ONLINE_STATUS]),out iOnlineFlag);

			foreach (MobileListItem item in rdoStatus.Items) {
				if (iOnlineFlag == ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING) {
					if (item.Value.Equals("2")) {
						item.Selected = true;
					}
				} else if (iOnlineFlag == ViCommConst.SeekOnlineStatus.WAITING) {
					if (item.Value.Equals("1")) {
						item.Selected = true;
					}
				} else {
					if (item.Value.Equals("0")) {
						item.Selected = true;
					}
				}
			}

			if (iBridUtil.GetStringValue(Session[NEWMAN_FLG]) == "1") {
				chkNewCast.Items[0].Selected = true;
			}
			txtHandelNm.Text = iBridUtil.GetStringValue(Session[HANDLE_NAME]);

			if (Session[MAIL_SEND_COUNT] != null) {
				string sMailSendCount = iBridUtil.GetStringValue(Session[MAIL_SEND_COUNT]);

				foreach (MobileListItem oItem in chkMailSendCount.Items) {
					oItem.Selected = false;
				}
				foreach (MobileListItem oItem in chkMailSendCount.Items) {
					if (oItem.Value.Equals(sMailSendCount)) {
						oItem.Selected = true;
					}
				}
			}

			string sBirthdayFrom = iBridUtil.GetStringValue(Session[BIRTHDAY_FROM]);
			string sBirthdayTo = iBridUtil.GetStringValue(Session[BIRTHDAY_TO]);
			string sAge = iBridUtil.GetStringValue(Session[AGE]);

			SysPrograms.SetupFromToDay(null,lstBirthdayFromMM,lstBirthdayFromDD,null,lstBirthdayToMM,lstBirthdayToDD,true);
			if (!sBirthdayFrom.Equals(string.Empty)) {
				string[] sBirthday = sBirthdayFrom.Split('/');
				foreach (MobileListItem item in lstBirthdayFromMM.Items) {
					if (item.Value == sBirthday[0]) {
						item.Selected = true;
					}
				}
				foreach (MobileListItem item in lstBirthdayFromDD.Items) {
					if (item.Value == sBirthday[1]) {
						item.Selected = true;
					}
				}
			}
			if (!sBirthdayTo.Equals(string.Empty)) {
				string[] sBirthday = sBirthdayTo.Split('/');
				foreach (MobileListItem item in lstBirthdayToMM.Items) {
					if (item.Value == sBirthday[0]) {
						item.Selected = true;
					}
				}
				foreach (MobileListItem item in lstBirthdayToDD.Items) {
					if (item.Value == sBirthday[1]) {
						item.Selected = true;
					}
				}
			}
			if (!sAge.Equals(string.Empty)) {
				foreach (MobileListItem item in lstAge.Items) {
					if (item.Value == sAge) {
						item.Selected = true;
					}
				}				
			}
			this.rdoStatus.Items[0].Text = DisplayWordUtil.Replace(this.rdoStatus.Items[0].Text);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = string.Empty;
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sHandleNm = HttpUtility.UrlEncode(txtHandelNm.Text,enc);
		string sAgeFrom = HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[0]);
		string sAgeTo = HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[1]);
		string sBirthdayFrom = string.Empty;
		string sBirthdayTo = string.Empty;
		string sUrl = string.Empty;
		string sMailSendCount = "";
		int i;
		int iOnlineFlag = 0;
		int iNewManFlag = 0;

		if (chkMailSendCount.SelectedIndex != -1) {
			sMailSendCount = chkMailSendCount.Items[chkMailSendCount.SelectedIndex].Value;
		}

		if (rdoStatus.Items[rdoStatus.SelectedIndex].Value.Equals("2")) {
			iOnlineFlag = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
		} else if (rdoStatus.Items[rdoStatus.SelectedIndex].Value.Equals("1")) {
			iOnlineFlag = ViCommConst.SeekOnlineStatus.WAITING;
		}

		if (chkNewCast.SelectedIndex != -1) {
			iNewManFlag = 1;
		}

		if (!lstBirthdayFromMM.Selection.Value.Equals("")) {
			sBirthdayFrom = lstBirthdayFromMM.Selection.Value + "/";
			if (!lstBirthdayFromDD.Selection.Value.Equals("")) {
				sBirthdayFrom += lstBirthdayFromDD.Selection.Value;
			} else {
				sBirthdayFrom += "01";
			}
		}
		if (!lstBirthdayToMM.Selection.Value.Equals("")) {
			sBirthdayTo = lstBirthdayToMM.Selection.Value + "/";
			if (!lstBirthdayToDD.Selection.Value.Equals("")) {
				sBirthdayTo += lstBirthdayToDD.Selection.Value;
			} else {
				sBirthdayTo += "31";
			}
		}

		sUrl = string.Format(
						"ListFindResult.aspx?category={0}&online={1}&handle={2}&play={3}&site={4}&agefrom={5}&ageto={6}&birthfrom={7}&birthto={8}&newman={9}&charno={10}",
						sessionWoman.currentCategoryIndex,
						iOnlineFlag,
						sHandleNm,
						"0",
						sessionWoman.site.siteCd,
						sAgeFrom,
						sAgeTo,
						sBirthdayFrom,
						sBirthdayTo,
						iNewManFlag,
						sessionWoman.userWoman.curCharNo
						);

		if (!string.IsNullOrEmpty(sMailSendCount)) {
			if (sMailSendCount.Equals("0")) {
				sUrl += "&mailsend=0";
			} else {
				sUrl += string.Format("&mailsendcnt={0}",sMailSendCount);
			}
		}

		// 選択した値をセッションにいれておきます。 
		Session[ONLINE_STATUS] = iOnlineFlag;
		Session[NEWMAN_FLG] = iNewManFlag;
		Session[HANDLE_NAME] = txtHandelNm.Text;
		Session[BIRTHDAY_FROM] = sBirthdayFrom;
		Session[BIRTHDAY_TO] = sBirthdayTo;
		Session[AGE] = lstAge.Selection.Value;
		Session[MAIL_SEND_COUNT] = sMailSendCount;

		SelectionList lstUserManItem;
		iBMobileTextBox txtUserManItem;
		Panel pnlUserManItem;

		i = 1;
		string sGroupingFlag;
		Dictionary<string,string> manAttrList = new Dictionary<string,string>();

		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet dsAttrType = oUserManAttrType.GetList(sessionWoman.site.siteCd);
			foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
				if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
					pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i)) as Panel;

					if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i)) as iBMobileTextBox;
						if ((pnlUserManItem != null) && (txtUserManItem != null) && (pnlUserManItem.Visible)) {
							sUrl = sUrl + string.Format(
												"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}",
												i,
												HttpUtility.UrlEncode(txtUserManItem.Text,enc),
												drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),
												"0",
												"1");
							// 入力値復元用の値保存 
							manAttrList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),txtUserManItem.Text);

						}
					} else {
						lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i)) as SelectionList;
						sGroupingFlag = "";
						if (!string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
							sGroupingFlag = "1";
						}
						if ((pnlUserManItem != null) && (lstUserManItem != null) && (pnlUserManItem.Visible)) {
							sUrl = sUrl + string.Format(
												"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}",
												i,
												lstUserManItem.Items[lstUserManItem.SelectedIndex].Value,
												drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),
												sGroupingFlag,
												"0");
							// 入力値復元用の値保存 
							manAttrList.Add(drAttrType["MAN_ATTR_TYPE_SEQ"].ToString(),lstUserManItem.Items[lstUserManItem.SelectedIndex].Value);

						}
					}
					i++;
				}

			}
		}
		Session[MAN_ITEM] = manAttrList;
		this.AppendSearchCondtion(ref sUrl,i);

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,sUrl + "&list=1"));
	}

	protected virtual void AppendSearchCondtion(ref string pUrl,int pIndex) {
		// 何もしない。aspxで処理を追記する場合に使用 
	}
}
