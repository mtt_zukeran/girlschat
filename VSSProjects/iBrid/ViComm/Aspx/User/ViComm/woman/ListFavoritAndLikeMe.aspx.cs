/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: スキスキ同士一覧
--	Progaram ID		: ListFavoritAndLikeMe
--
--  Creation Date	: 2011.04.20
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListFavoritAndLikeMe:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_FAVORIT_AND_LIKE_ME_LIST,
					ActiveForm);
		}
	}
}
