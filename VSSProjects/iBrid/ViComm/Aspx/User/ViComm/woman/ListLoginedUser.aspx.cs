/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ログイン中一覧
--	Progaram ID		: ListLoginedUser
--
--  Creation Date	: 2010.09.16
--  Creater			: i-Brid(K.Itoh)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_woman_ListLoginedUser : MobileWomanPageBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);
		if (!IsPostBack) {
			sessionWoman.ControlList(Request,ViCommConst.INQUIRY_MAN_LOGINED,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		UrlBuilder oUrlBuilder = new UrlBuilder(SessionObjs.Current.GetNavigateUrl(ViCommPrograms.GetCurrentAspx()));
		
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["mailsend"]))) {
			oUrlBuilder.AddParameter("mailsend",iBridUtil.GetStringValue(this.Request.QueryString["mailsend"]));
		}
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Form["notxmaildays"]))) {
			oUrlBuilder.AddParameter("notxmaildays",iBridUtil.GetStringValue(this.Request.Form["notxmaildays"]));
		}
		RedirectToMobilePage(oUrlBuilder.ToString());
	}
}
