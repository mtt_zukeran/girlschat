/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳コメント編集
--	Progaram ID		: ModifyYakyukenComment
--
--  Creation Date	: 2013.04.30
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ModifyYakyukenComment:MobileWomanPageBase {
	private string sYakyukenCommentSeq;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		sYakyukenCommentSeq = iBridUtil.GetStringValue(Request.QueryString["ycommentseq"]);

		if (string.IsNullOrEmpty(sYakyukenCommentSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			if (sessionWoman.logined == false) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			}

			DataSet dsYakyukenComment = GetYakyukenComment(sYakyukenCommentSeq);

			if (dsYakyukenComment.Tables[0].Rows.Count > 0) {
				txtCommentText.Text = iBridUtil.GetStringValue(dsYakyukenComment.Tables[0].Rows[0]["COMMENT_TEXT"]);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private DataSet GetYakyukenComment(string pYakyukenCommentSeq) {
		DataSet ds;
		YakyukenCommentSeekCondition oCondition = new YakyukenCommentSeekCondition();
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.CastUserSeq = sessionWoman.userWoman.userSeq;
		oCondition.CastCharNo = sessionWoman.userWoman.curCharNo;
		oCondition.YakyukenCommentSeq = pYakyukenCommentSeq;

		using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
			ds = oYakyukenComment.GetPageCollection(oCondition,1,1);
		}

		return ds;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (CheckInputValue()) {
			string sResult = string.Empty;

			using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
				oYakyukenComment.ModifyYakyukenComment(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					sYakyukenCommentSeq,
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtCommentText.Text)),
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListYakyukenComment.aspx"));
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}

	protected bool CheckInputValue() {
		bool bOk = true;
		string sNGWord;
		lblErrorMessage.Text = string.Empty;

		if (sessionWoman.ngWord == null) {
			sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
		}

		if (txtCommentText.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄを入力して下さい。";
		} else if (txtCommentText.Text.Length < 10) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は10文字以上です。";
		} else if (txtCommentText.Text.Length > 50) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄの入力は50文字以内です。";
		} else if (sessionWoman.ngWord.VaidateDoc(txtCommentText.Text,out sNGWord) == false) {
			bOk = false;
			lblErrorMessage.Text = "ｺﾒﾝﾄに禁止語句が含まれています。";
		}

		return bOk;
	}
}
