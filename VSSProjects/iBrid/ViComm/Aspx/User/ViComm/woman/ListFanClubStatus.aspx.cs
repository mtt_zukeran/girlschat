/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ファンクラブ会員一覧
--	Progaram ID		: ListFanClubStatus
--
--  Creation Date	: 2013.02.04
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListFanClubStatus:MobileWomanPageBase {
	private string sAreaAttrTypeSeq;
	private string sAreaValue;
	private string sMemberRank;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sAreaValue = iBridUtil.GetStringValue(Request.QueryString["item01"]);
		sMemberRank = iBridUtil.GetStringValue(Request.QueryString["memberrank"]);

		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet dsAttrType = oUserManAttrType.GetList(sessionWoman.site.siteCd);

			foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
				if (drAttrType["MAN_ATTR_TYPE_NM"].ToString().Equals("地域")) {
					sAreaAttrTypeSeq = drAttrType["MAN_ATTR_TYPE_SEQ"].ToString();
				}
			}
		}

		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sAreaAttrTypeSeq)) {
				using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
					DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionWoman.site.siteCd,sAreaAttrTypeSeq);

					using (CodeDtl oCodeDtl = new CodeDtl()) {
						DataSet dsCodeDtl = oCodeDtl.GetList("81");
						foreach (DataRow drCodeDtl in dsCodeDtl.Tables[0].Rows) {
							string sExp = string.Format("GROUPING_CD={0}",drCodeDtl["CODE"]);

							string sValues = string.Empty;
							foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Select(sExp)) {
								if (!string.IsNullOrEmpty(sValues))
									sValues += ",";
								sValues += drAttrTypeValue["MAN_ATTR_SEQ"];
							}

							lstArea.Items.Add(new MobileListItem(drCodeDtl["CODE_NM"].ToString(),sValues));
						}
					}

					foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
						if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
							lstArea.Items.Add(new MobileListItem(drAttrTypeValue["MAN_ATTR_NM"].ToString(),drAttrTypeValue["MAN_ATTR_SEQ"].ToString()));
						}
					}
				}
			}

			if (!string.IsNullOrEmpty(sMemberRank)) {
				foreach (MobileListItem item in lstMemberRank.Items) {
					if (item.Value == sMemberRank) {
						item.Selected = true;
						break;
					}
				}
			}

			if (!string.IsNullOrEmpty(sAreaValue)) {
				foreach (MobileListItem item in lstArea.Items) {
					if (item.Value == sAreaValue) {
						item.Selected = true;
						break;
					}
				}
			}

			sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_FANCLUB_STATUS,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		UrlBuilder oUrlBuilder = new UrlBuilder("ListFanClubStatus.aspx");

		if (lstMemberRank.Items[lstMemberRank.SelectedIndex].Value != "") {
			oUrlBuilder.AddParameter("memberrank",lstMemberRank.Items[lstMemberRank.SelectedIndex].Value);
		}
		
		if (lstArea.Items[lstArea.SelectedIndex].Value != "*") {
			oUrlBuilder.AddParameter("item01",lstArea.Items[lstArea.SelectedIndex].Value);
			oUrlBuilder.AddParameter("typeseq01",sAreaAttrTypeSeq);
		}

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,oUrlBuilder.ToString()));
	}
}
