/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール添付動画管理
--	Progaram ID		: ViewMailMovieConf
--
--  Creation Date	: 2009.08.12
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewMailMovieConf:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;


		if (!IsPostBack) {
			if (sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_CAST_MAIL_MOVIE_STOCK,ActiveForm)) {
				ViewState["MOVIE_SEQ"] = sessionWoman.GetMailMoiveValue("MOVIE_SEQ");

				if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
					using (CastMovie oMovie = new CastMovie()) {
						string sMovieFileNm =  sessionWoman.GetMailMoiveValue("MOVIE_TITLE");
						oMovie.UpdateProfileMovie(
							sessionWoman.site.siteCd,
							sessionWoman.userWoman.userSeq,
							sessionWoman.userWoman.curCharNo,
							ViewState["MOVIE_SEQ"].ToString(),
							sMovieFileNm,
							null,
							1,
							ViCommConst.ATTACHED_MAIL,
							ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString(),
							ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString()
						);
					}
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_MAIL_MOVIE) + "&site=" + sessionWoman.site.siteCd);
				} else if (!sMovieSeq.Equals("") && sPlayMovie.Equals("1")) {
					bDownload = Download(sMovieSeq);
				};

			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListMailMovieConf.aspx") + "?site=" + sessionWoman.site.siteCd);
			}
		}
		
		if (bDownload == false) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
			if (IsPostBack) {
				if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender,e);
				}
			}
		}
	}

	private bool Download(string pMovieSeq) {
		ParseHTML oParseHTML = sessionWoman.parseContainer;
		
		string sFileNm, sFullPath, sLoginId;
		string sErrorCode = null;
		
		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if(lSize==ViCommConst.GET_MOVIE_ERROR_VALUE){
			return false;
		}
		

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);

				return true;
			} else {
				return false;
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult = string.Empty;
		using (CastMailMovieStock oCastMailMovieStock = new CastMailMovieStock()) {
			oCastMailMovieStock.UpdateMailStockPic(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				iBridUtil.GetStringValue(ViewState["MOVIE_SEQ"]),
				out sResult
			);
		}
		
		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListMailMovieConf.aspx?movietype=3") + "&site=" + sessionWoman.site.siteCd);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}
