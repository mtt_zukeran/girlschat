/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 画像レビューコメント削除
--	Progaram ID		: DeleteObjReviewCommentPic
--
--  Creation Date	: 2013.09.25
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteObjReviewCommentPic:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY_PIC,this.ActiveForm)) {
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.errorMessage = string.Empty;
		string sObjReviewHistorySeq = iBridUtil.GetStringValue(Request.QueryString["history_seq"]);
		string sResult;

		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			sResult = oObjReviewHistory.DeleteObjReviewCommentPic(
				sessionWoman.site.siteCd,
				sObjReviewHistorySeq,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo
			);
		}

		if (sResult.Equals(PwViCommConst.DeleteObjReviewCommentResult.RESULT_OK)) {
			IDictionary<string,string> oParam = new Dictionary<string,string>();

			oParam.Add("objseq",iBridUtil.GetStringValue(Request.QueryString["objseq"]));
			oParam.Add("reviewpageno",iBridUtil.GetStringValue(Request.QueryString["reviewpageno"]));
			oParam.Add("attrtypeseq",iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]));
			oParam.Add("attrseq",iBridUtil.GetStringValue(Request.QueryString["attrseq"]));
			oParam.Add("listtype",iBridUtil.GetStringValue(Request.QueryString["listtype"]));
			oParam.Add("listpageno",iBridUtil.GetStringValue(Request.QueryString["listpageno"]));

			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_DELETE_OBJ_REVIEW_COMMENT_COMPLETE,oParam);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		string sUrl = string.Format(
			"ListObjReviewHistoryPic.aspx?objseq={0}&pageno={1}&attrtypeseq={2}&attrseq={3}&listtype={4}&listpageno={5}",
			iBridUtil.GetStringValue(Request.QueryString["objseq"]),
			iBridUtil.GetStringValue(Request.QueryString["reviewpageno"]),
			iBridUtil.GetStringValue(Request.QueryString["attrtypeseq"]),
			iBridUtil.GetStringValue(Request.QueryString["attrseq"]),
			iBridUtil.GetStringValue(Request.QueryString["listtype"]),
			iBridUtil.GetStringValue(Request.QueryString["listpageno"])
		);

		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,sUrl));
	}
}
