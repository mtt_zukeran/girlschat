/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 招待メール送信
--	Progaram ID		: TxInviteMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_TxInviteMail:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			if (IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL, 2)) {
				lblTitle.Visible = false;
				txtTitle.Visible = false;
				txtTitle.Text = string.Empty;
			}
			sessionWoman.userWoman.UpdateCharacterInfo(sessionWoman.site.siteCd);
			if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
				if (sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].inviteMailCount >= sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].inviteMailLimit) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_INVITE_MAIL_LIMIT_OVER));
				}
			}

			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
			sessionWoman.SetManDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),iRecNo);

			using (MailTemplate oTemplate = new MailTemplate()) {
				DataSet ds = oTemplate.GetCastInvaiteMailList(sessionWoman.site.siteCd);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstTemplate.Items.Add(new MobileListItem(dr["TEMPLATE_NM"].ToString(),dr["MAIL_TEMPLATE_NO"].ToString()));
				}
			}
			if (lstTemplate.Items.Count == 1) {
				lstTemplate.Items[0].Selected = true;
				lstTemplate.Visible = false;
				lblTemplate.Visible = false;
			}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds;
				ds = oCodeDtl.GetList(ViCommConst.CODE_INVITE_TALK_SERVICE_POINT);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstServicePoint.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
				}
			}

			// 相手からの拒否チェック
			CheckRefuse(sessionWoman.GetManValue("USER_SEQ"),ViCommConst.MAIN_CHAR_NO);

			// 自分が拒否しているチェック
			using (Refuse oRefuse = new Refuse()) {
				if (oRefuse.GetOne(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sessionWoman.GetManValue("USER_SEQ"),ViCommConst.MAIN_CHAR_NO)) {
					this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_MAN);
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		//sessionWoman.errorMessage = "";
		lblErrorMessage.Text = "";

		if ((txtTitle.Text.Equals("")) && (txtTitle.Visible == true)) {
			bOk = false;
			//タイトルを入力して下さい。
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("")) {
			bOk = false;
			//本文を入力して下さい。
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		string sUserSeq = sessionWoman.GetManValue("USER_SEQ");
		string sNGWord;
		if (bOk) {
			if (sessionWoman.ngWord == null) {
				sessionWoman.ngWord = new NGWord(sessionWoman.site.siteCd);
			}
			if (sessionWoman.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				//「{0}」の単語が不正です。
				lblErrorMessage.Text += string.Format(
											GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD)
											,sNGWord);
			}
		}

		if (bOk) {
			string sExisMsg = "";

			using (MailLog oMailLog = new MailLog()) {
				bOk = oMailLog.CheckTxMail(
						sessionWoman.site.siteCd,
						sessionWoman.userWoman.userSeq,
						sessionWoman.userWoman.curCharNo,
						sUserSeq,
						lstTemplate.Items[lstTemplate.SelectedIndex].Value,
						out sExisMsg);
				//sessionWoman.errorMessage = sExisMsg;
				if (!sExisMsg.Equals("")) {
					lblErrorMessage.Text += string.Format("{0}<br />",sExisMsg);
				}
			}
		}

		if (bOk == false) {
			return;
		} else {
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtTitle.Text));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionWoman.carrier,txtDoc.Text));

			if (sDoc.Length > 1000) {
				sDoc = SysPrograms.Substring(sDoc,1000);
			}

			using (MailBox oMailBox = new MailBox()) {
				int iServicePoint;
				string sObjTempId = string.Empty;
				int.TryParse(lstServicePoint.Items[lstServicePoint.SelectedIndex].Value,out iServicePoint);
				oMailBox.TxCastToManMail(
					sessionWoman.site.siteCd,
					lstTemplate.Items[lstTemplate.SelectedIndex].Value,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					new string[] { sUserSeq },
					iServicePoint,
					sTitle,
					sDoc,
					null,
					null,
					null,
					ViCommConst.FLAG_OFF,
					null,
					null,
					out sObjTempId
				);
			}
			sessionWoman.userWoman.UpdateInviteMailCount(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,1);

			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_MAIL_COMPLITE_CAST + "&" + Request.QueryString));
		}
	}
}
