/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・画像を保護した男性
--	Progaram ID		: ListGameCharacterTreasureMosaicErase
--
--  Creation Date	: 2011.10.10
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameCharacterTreasureMosaicErase:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sCastGamePicSeq = iBridUtil.GetStringValue(Request.QueryString["cast_game_pic_seq"]);

		if (sCastGamePicSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_TREASURE_MOSAIC_ERASE,ActiveForm);
	}
}
