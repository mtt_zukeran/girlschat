/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフィール表示
--	Progaram ID		: Profile
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_Profile:MobileMailWomanPage {

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
		MailTemplate = lstTemplate;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter, this, Request, ViewState, IsPostBack);

		ulong ulSeekMode;

		if (iBridUtil.GetStringValue(Request.QueryString["direct"]).ToString().Equals("1")) {
			UserWomanCharacter oWomanCharacter = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey];
			DataSet dsDataSet = oWomanCharacter.manCharacter.GetOneByLoginId(sessionWoman.site.siteCd,iBridUtil.GetStringValue(Request.QueryString["loginid"]),1);

			if (dsDataSet.Tables[0].Rows.Count > 0) {
				ulSeekMode = ViCommConst.INQUIRY_DIRECT;
			} else {
				if (sessionWoman.logined) {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"UserTop.aspx") + "?site=" + sessionWoman.site.siteCd);
				} else {
					RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,sessionWoman.sessionId,"NonUserTop.aspx"));
				}
				return;
			}
		} else {
			ulSeekMode = ulong.Parse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]));
		}

		if (!IsPostBack) {
			pnlTxMail.Visible = false;
			pnlTemplate.Visible = false;
			
			bool bFind = sessionWoman.ControlList(
							Request,
							ulSeekMode,
							ActiveForm);
			if (!bFind) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"ListOnlineUser.aspx"));
			} else {
				ParseHTML oParseMan = sessionWoman.parseContainer;
				string sUserSeq = sessionWoman.GetManValue("USER_SEQ");
				string sUserCharNo = sessionWoman.GetManValue("USER_CHAR_NO");

				string sUserStatus = sessionWoman.GetManValue("USER_STATUS");
				if (sUserStatus.Equals(ViCommConst.USER_MAN_HOLD) || sUserStatus.Equals(ViCommConst.USER_MAN_RESIGNED) || sUserStatus.Equals(ViCommConst.USER_MAN_STOP) || sUserStatus.Equals(ViCommConst.USER_MAN_BLACK)) {
					RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_MAN);
				}

				if (sessionWoman.logined) {
					//相手の拒否リストに載っている場合、リダイレクトさせる。
					using (Refuse oRefuse = new Refuse()) {
						if (oRefuse.GetOne(
								sessionWoman.site.siteCd,
								sUserSeq,
								sUserCharNo,
								sessionWoman.userWoman.userSeq,
								sessionWoman.userWoman.curCharNo)) {
							RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_REFUSE_WOMAN));
						}
					}
					using (Favorit oFavorit = new Favorit()) {
						//相手から見た時にオフライン、または会話中の場合、足あとをつけない
						int iCharacterOnlineStatus = ViCommConst.USER_OFFLINE;
						if (sessionWoman.userWoman.characterList.ContainsKey(sessionWoman.userWoman.curKey)) {
							iCharacterOnlineStatus = sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterOnlineStatus;
						}
						if (oFavorit.GetIsSeenOnlineStatus(
									sessionWoman.site.siteCd,
									sessionWoman.userWoman.userSeq,
									sessionWoman.userWoman.curCharNo,sUserSeq,
									sUserCharNo,
									iCharacterOnlineStatus) != ViCommConst.USER_OFFLINE &&
							iCharacterOnlineStatus != ViCommConst.USER_TALKING &&
							iCharacterOnlineStatus != ViCommConst.USER_DUMMY_TALKING) {

							using (Marking oMarking = new Marking()) {
								oMarking.MarkingMainte(
									sessionWoman.site.siteCd,
									sessionWoman.userWoman.userSeq,
									sessionWoman.userWoman.curCharNo,
									sUserSeq,
									sUserCharNo,
									ViCommConst.MAN,
									0
								);
							}
						}
					}

					//メールアタック(新人)対象チェック
					sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag = ViCommConst.FLAG_OFF;
					using (MailAttackNew oMailAttackNew = new MailAttackNew()) {
						string sResult = oMailAttackNew.RegistMailAttackNewTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,sUserCharNo,string.Empty,ViCommConst.FLAG_ON);
						if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
							sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag = ViCommConst.FLAG_ON;
						}
					}

					//メールアタック(ログイン)対象チェック
					sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag = ViCommConst.FLAG_OFF;
					using (MailAttackLogin oMailAttackLogin = new MailAttackLogin()) {
						string sResult = oMailAttackLogin.RegistMailAttackLoginTx(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo,sUserSeq,sUserCharNo,string.Empty,ViCommConst.FLAG_ON);
						if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
							sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag = ViCommConst.FLAG_ON;
						}
					}

					// 3分以内メール返信ボーナス用の値を取得
					MailResBonus2 oMailResBonus2 = new MailResBonus2();
					sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus = oMailResBonus2.SetEventData(ref sessionWoman.userWoman);
					// メール返信ボーナス対象チェック
					sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag = ViCommConst.FLAG_OFF_STR;
					sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag = ViCommConst.FLAG_OFF_STR;
					if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusStatus)) {
						MailResBonusLog2 oMailResBonusLog2 = new MailResBonusLog2();
						string pMainFlag;
						string pSubFlag;
						oMailResBonusLog2.CheckUser(
							sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusSeq,
							sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec1,
							sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusLimitSec2,
							sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusMaxNum,
							sessionWoman.userWoman.characterList[sessionWoman.userWoman.curKey].characterEx.mailResBonusUnreceivedDays,
							sessionWoman.site.siteCd,
							sUserSeq,
							sessionWoman.userWoman.userSeq,
							out pMainFlag,
							out pSubFlag
						);
						if (pMainFlag.Equals(ViCommConst.FLAG_ON_STR)) {
							sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag = ViCommConst.FLAG_ON_STR;
						}
						if (pSubFlag.Equals(ViCommConst.FLAG_ON_STR)) {
							sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag = ViCommConst.FLAG_ON_STR;
						}
					}

					if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["scrid"]))) {
						if (sessionWoman.carrier.Equals(ViCommConst.ANDROID) || sessionWoman.carrier.Equals(ViCommConst.IPHONE)) {
							using (ManageCompany oManageCompany = new ManageCompany()) {
								if (IsAvailableService(ViCommConst.RELEASE_TX_MAIL_FROM_PROFILE_CAST,2)) {
									pnlTxMail.Visible = true;
									bool bIsAvailable = oManageCompany.IsAvailableService(ViCommConst.RELEASE_TX_MAIL_DISABLE_TEMPLATE);
									if (bIsAvailable) {
										this.SelectNomalMail();
									}
									this.pnlTemplate.Visible = !bIsAvailable;
									
									if (IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2)) {
										lblTitle.Visible = false;
										txtTitle.Visible = false;
									}

									SetMailTemplate(false,string.Empty);

									string sMailDataSeq = iBridUtil.GetStringValue(sessionWoman.userWoman.NextMailDataSeq());
									sessionWoman.userWoman.mailData.Add(sMailDataSeq,new MailInfo());

									sessionWoman.userWoman.mailData[sMailDataSeq].rxUserSeq = sUserSeq;
									sessionWoman.userWoman.mailData[sMailDataSeq].rxUserCharNo = sUserCharNo;
									sessionWoman.userWoman.mailData[sMailDataSeq].rxUserNm = sessionWoman.GetManValue("HANDLE_NM");
									sessionWoman.userWoman.mailData[sMailDataSeq].rxLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
									sessionWoman.userWoman.mailData[sMailDataSeq].layOutType = string.Empty;
									
									sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackNewFlag = sessionWoman.userWoman.CurCharacter.displayMailAttackNewFlag;
									sessionWoman.userWoman.mailData[sMailDataSeq].mailAttackLoginFlag = sessionWoman.userWoman.CurCharacter.displayMailAttackLoginFlag;

									ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;
									sessionWoman.userWoman.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_OFF;

									// メール返信ボーナス対象チェック結果設定
									if (ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.CurCharacter.mailResBonus2MainFlag)
										|| ViCommConst.FLAG_ON_STR.Equals(sessionWoman.userWoman.CurCharacter.mailResBonus2SubFlag)
									) {
										sessionWoman.userWoman.mailData[sMailDataSeq].mailResBonus2Flag = ViCommConst.FLAG_ON_STR;
									} else {
										sessionWoman.userWoman.mailData[sMailDataSeq].mailResBonus2Flag = ViCommConst.FLAG_OFF_STR;
									}
								}
							}
						}
					}
				}
			}
		} else {
			if (Request.Params["cmdTxMail"] != null) {
				cmdTxMail_Click(sender,e);
			}
		}
	}

	protected void cmdTxMail_Click(object sender,EventArgs e) {
		Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false,false,ViCommConst.SCR_TX_MAIL_COMPLITE_CAST);
	}

	private void SelectNomalMail() {
		foreach (MobileListItem oMobileListItem in this.lstTemplate.Items) {
			if (ViCommConst.MailTemplateType.NORMAL.Equals(oMobileListItem.Value)) {
				this.lstTemplate.SelectedIndex = oMobileListItem.Index;
				break;
			}
		}
	}

}
