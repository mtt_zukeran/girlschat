﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 報酬表示
--	Progaram ID		: ViewPerformance
--
--  Creation Date	: 2010.05.12
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewPerformance:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		
		if (!IsPostBack) {
			SysPrograms.SetupFromToDay(lstFromYear,lstFromMonth,lstFromDay,lstToYear,lstToMonth,lstToDay,false);
			lstFromYear.SelectedIndex = 0;
			lstFromMonth.SelectedIndex = int.Parse(DateTime.Now.ToString("MM")) - 1;
			lstFromDay.SelectedIndex = 0;
			lstToYear.SelectedIndex = 0;
			lstToMonth.SelectedIndex = int.Parse(DateTime.Now.ToString("MM")) - 1;
			lstToDay.SelectedIndex = DateTime.DaysInMonth(int.Parse(lstToYear.Selection.Value),int.Parse(lstToMonth.Selection.Value)) - 1;
			chkNotPaymentOnlyFlag.Items[0].Selected = true;

			string sReportYear = iBridUtil.GetStringValue(this.Request.QueryString["year"]);
			string sReportMonth = iBridUtil.GetStringValue(this.Request.QueryString["month"]);
			string sReportDay = iBridUtil.GetStringValue(this.Request.QueryString["day"]);

			if (!string.IsNullOrEmpty(sReportYear) && !string.IsNullOrEmpty(sReportMonth) && !string.IsNullOrEmpty(sReportDay)) {
				DateTime dtReportDate;
				
				if (DateTime.TryParse(string.Format("{0}/{1}/{2}",sReportYear,sReportMonth,sReportDay),out dtReportDate)) {
					lstFromYear.Selection.Selected = false;
					foreach (MobileListItem item in lstFromYear.Items) {
						if (item.Value == dtReportDate.ToString("yyyy")) {
							item.Selected = true;
							break;
						}
					}

					lstToYear.Selection.Selected = false;
					foreach (MobileListItem item in lstToYear.Items) {
						if (item.Value == dtReportDate.ToString("yyyy")) {
							item.Selected = true;
							break;
						}
					}

					lstFromMonth.Selection.Selected = false;
					foreach (MobileListItem item in lstFromMonth.Items) {
						if (item.Value == dtReportDate.ToString("MM")) {
							item.Selected = true;
							break;
						}
					}

					lstToMonth.Selection.Selected = false;
					foreach (MobileListItem item in lstToMonth.Items) {
						if (item.Value == dtReportDate.ToString("MM")) {
							item.Selected = true;
							break;
						}
					}

					lstFromDay.Selection.Selected = false;
					foreach (MobileListItem item in lstFromDay.Items) {
						if (item.Value == dtReportDate.ToString("dd")) {
							item.Selected = true;
							break;
						}
					}

					lstToDay.Selection.Selected = false;
					foreach (MobileListItem item in lstToDay.Items) {
						if (item.Value == dtReportDate.ToString("dd")) {
							item.Selected = true;
							break;
						}
					}
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				}
			}
			
			GetPerformance();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		GetPerformance();
	}

	protected void GetPerformance() {
		using (TimeOperation oTimeOperation = new TimeOperation()) {
			bool bNotPaymentOnlyFlag;
			string sSiteCd = "";
			string sUserCharNo = "";
			if (chkNotPaymentOnlyFlag.Items[0].Selected) {
				bNotPaymentOnlyFlag = true;
			} else {
				bNotPaymentOnlyFlag = false;
			}
			//Z001で開いた時は、全サイト、全サブキャラクターの合計報酬を表示する
			if (sessionWoman.site.siteCd.Equals("Z001")) {
				sSiteCd = "";
				sUserCharNo = "";
			} else {
				sSiteCd = sessionWoman.site.siteCd;
				sUserCharNo = sessionWoman.userWoman.curCharNo;
			}

			// 選択されている年月の末日より大きい日にちが選択されている場合は末日に変更する
			int iDay = DateTime.DaysInMonth(int.Parse(lstFromYear.Selection.Value),int.Parse(lstFromMonth.Selection.Value));
			if (int.Parse(lstFromDay.Selection.Value) > iDay) {
				lstFromDay.SelectedIndex = iDay - 1;
			}
			iDay = DateTime.DaysInMonth(int.Parse(lstToYear.Selection.Value),int.Parse(lstToMonth.Selection.Value));
			if (int.Parse(lstToDay.Selection.Value) > iDay) {
				lstToDay.SelectedIndex = iDay - 1;
			}

			DataSet dsDataSet = oTimeOperation.GetPerformance(sSiteCd,sessionWoman.userWoman.userSeq,sUserCharNo,lstFromYear.Selection.Value,lstFromMonth.Selection.Value,lstFromDay.Selection.Value,lstToYear.Selection.Value,lstToMonth.Selection.Value,lstToDay.Selection.Value,bNotPaymentOnlyFlag);
			int iIdx = ViCommConst.DATASET_CAST_PAY;
			ParseHTML oParseWoman = sessionWoman.parseContainer;

			if (dsDataSet.Tables[0].Rows.Count > 0) {
				oParseWoman.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
				oParseWoman.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
				oParseWoman.parseUser.SetDataRow(iIdx,dsDataSet.Tables[0].Rows[0]);
				sessionWoman.userWoman.performanceFindMonth = lstFromMonth.Selection.Value;
				sessionWoman.userWoman.performanceFindFrom = string.Format("{0}/{1}/{2}",lstFromYear.Selection.Value,lstFromMonth.Selection.Value,lstFromDay.Selection.Value);
				sessionWoman.userWoman.performanceFindTo = string.Format("{0}/{1}/{2}",lstToYear.Selection.Value,lstToMonth.Selection.Value,lstToDay.Selection.Value);
			} else {
				oParseWoman.currentRecPos[iIdx] = -1;
				oParseWoman.parseUser.SetDataRow(iIdx,null);
			}
		}
	}
}
