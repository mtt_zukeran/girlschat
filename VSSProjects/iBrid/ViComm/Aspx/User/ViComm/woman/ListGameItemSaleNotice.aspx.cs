/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹް� ���Ѽ���� ��Ѿ�ٗ\�� ���шꗗ
--	Progaram ID		: ListGameItemSaleNotice
--
--  Creation Date	: 2011.09.21
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ListGameItemSaleNotice:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_SALE_NOTICE_LIST,ActiveForm);
		}
	}
}
