<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WriteBbsThread.aspx.cs" Inherits="ViComm_woman_WriteBbsThread" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
	    $PGM_HTML02;
	    <cc1:iBMobileTextBox ID="txtBbsThreadHandleNm" runat="server" Size="12" MaxLength="40"></cc1:iBMobileTextBox>
	    $PGM_HTML03;
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="255" Size="24"></cc1:iBMobileTextBox>
		$PGM_HTML04;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24">
		</tx:TextArea>
		$PGM_HTML06;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
