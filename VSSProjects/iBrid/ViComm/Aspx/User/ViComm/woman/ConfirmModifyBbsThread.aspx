<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfirmModifyBbsThread.aspx.cs" Inherits="ViComm_woman_ConfirmModifyBbsThread" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
	    $PGM_HTML02;
	    <cc1:iBMobileLabel ID="lblThreadDoc" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
	    $PGM_HTML03;
		$PGM_HTML06;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
