<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyBlogArticle.aspx.cs" Inherits="ViComm_woman_ModifyBlogArticle" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile"	%>
<%@ Register TagPrefix="ibrid"	Namespace="MobileLib"						Assembly="MobileLib"			%>
<%@ Register TagPrefix="ibrid"	TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>    
		$PGM_HTML02;
		<ibrid:iBMobileTextBox ID="txtBlogArticleTitle" runat="server"  BreakAfter="true" Size="25" MaxLength="50" EmojiPaletteEnabled="true" ></ibrid:iBMobileTextBox>
		$PGM_HTML03;
		<ibrid:TextArea ID="txtBlogArticleDoc" runat="server" BreakBefore="false" BrerakAfter="true" Columns="20" Rows="6" EmojiPaletteEnabled="true" >
		</ibrid:TextArea>		
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList"		runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail"	runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;		
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
