/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����ٹްїpFlash(����߶���)
--	Progaram ID		: ViewGameFlashLotteryNotComp
--
--  Creation Date	: 2012.06.16
--  Creater			: M&TT A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_woman_ViewGameFlashLotteryNotComp:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sLotteryPattern = iBridUtil.GetStringValue(this.Request.QueryString["lottery_pattern"]);

		string sFileNm = null;
		string sNextPageNm = string.Empty;
		string sGetParamStr = string.Empty;
		
		if (sLotteryPattern == "0") {
			sFileNm = "lottery.swf";
			sNextPageNm = "ViewGameLotteryGetItemResultNotComp.aspx";
		} else {
			sFileNm = "lottery_set.swf";
			sNextPageNm = "ListGameLotteryGetItemResultNotComp.aspx";

		}

		if (this.Request.QueryString.Count > 0) {
			string[] keys = this.Request.QueryString.AllKeys;
			string value = string.Empty;
			List<string> oGetParamList = new List<string>();

			foreach (string key in keys) {
				oGetParamList.Add(key + "=" + iBridUtil.GetStringValue(this.Request.QueryString[key]));
			}

			sGetParamStr = string.Join("&",oGetParamList.ToArray());
		}

		NameValueCollection oSwfParam = new NameValueCollection();
		GameFlashLiteHelper.ResponseFlashSocialGame(sFileNm,sNextPageNm,sGetParamStr,oSwfParam);
	}
}