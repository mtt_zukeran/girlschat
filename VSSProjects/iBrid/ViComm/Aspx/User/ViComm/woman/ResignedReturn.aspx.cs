/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �މ�A
--	Progaram ID		: ResignedReturn
--
--  Creation Date	: 2010.09.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ResignedReturn:MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = "";
		string sResult = string.Empty;

		if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
			sessionWoman.userWoman.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_TEL,txtLoginId.Text,txtPassword.Text,out sResult);
		} else {
			sessionWoman.userWoman.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_LOGINID,txtLoginId.Text,txtPassword.Text,out sResult);
		}

		if (sResult.Equals("0")) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_RETURN_RESIGNED_COMPLITE));
		} else {
			lblErrorMessage.Text = GetErrorMessage(sResult);
		}
	}

}
