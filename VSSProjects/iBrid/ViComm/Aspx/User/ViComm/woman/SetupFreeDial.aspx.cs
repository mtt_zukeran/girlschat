/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: フリーダイアル利用設定
--	Progaram ID		: SetupFreeDial
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_SetupFreeDial:MobileWomanPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			string sSetupFlag = iBridUtil.GetStringValue(Request.QueryString["setup"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));

			if (!string.IsNullOrEmpty(sSetupFlag)) {
				this.SetupFreeDial(sSetupFlag.Equals(ViCommConst.FLAG_ON_STR),sBackUrl);
			} else {
				if (sessionWoman.userWoman.useFreeDialFlag != 0) {
					chkUseFreeDiall.SelectedIndex = 0;
				}
				if (Request.Params["usefreedial"] != null) {
					this.SetupFreeDial(ViCommConst.FLAG_ON_STR.Equals(Request.Params["usefreedial"]),"");
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		this.SetupFreeDial(chkUseFreeDiall.SelectedIndex == 0,"");
	}

	private void SetupFreeDial(bool pSetupFlag,string pBackUrl) {
		sessionWoman.userWoman.SetupFreeDial(
			sessionWoman.userWoman.userSeq,
			pSetupFlag
		);

		if (!string.IsNullOrEmpty(pBackUrl)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(pBackUrl));
			return;
		} else {
			if (pSetupFlag) {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_USE_FREEDIAL_COMPLITE));
			} else {
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_NON_USE_FREEDIAL_COMPLITE));
			}
		}
	}
}
