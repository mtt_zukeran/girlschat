/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			:  ����ٹްсEʸތ���
--	Progaram ID		: ViewGameCharacterHugResult
--
--  Creation Date	: 2011.09.30
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ViewGameCharacterHugResult:MobileSocialGameWomanBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sResult = iBridUtil.GetStringValue(Request.QueryString["result"]);
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		if (sResult.Equals(string.Empty) || sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
		}

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		sessionWoman.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
	}
}