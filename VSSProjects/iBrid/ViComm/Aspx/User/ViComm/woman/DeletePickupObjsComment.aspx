<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeletePickupObjsComment.aspx.cs" Inherits="ViComm_woman_DeletePickupObjsComment" %>
<%@ Register TagPrefix="ibrid"		Namespace="MobileLib"						Assembly="MobileLib"%>
<%@ Register TagPrefix="mobile"		Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLiteralText ID="tagDeleteConfirm" runat="server" Text="$PGM_HTML02;" />
		<ibrid:iBMobileLiteralText ID="tagDeleteComplete" runat="server" Text="$PGM_HTML03;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>