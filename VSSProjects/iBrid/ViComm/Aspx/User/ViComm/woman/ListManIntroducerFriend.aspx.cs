﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 友達紹介一覧(男性)
--	Progaram ID		: ListManIntroducerFriend
--
--  Creation Date	: 2011.03.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ListManIntroducerFriend : MobileWomanPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		sessionWoman.ClearCategory();
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		if (!IsPostBack) {
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["pageno"])) || iBridUtil.GetStringValue(Request.QueryString["pageno"]).Equals("1")) {
				using (IntroCheck oIntroCheck = new IntroCheck()) {
					oIntroCheck.UpdateUserIntroCountCheck(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq,sessionWoman.userWoman.curCharNo);
				}
			}
			
			sessionWoman.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_INTRODUCER_FRIEND,
					ActiveForm);
		}

	}
}
