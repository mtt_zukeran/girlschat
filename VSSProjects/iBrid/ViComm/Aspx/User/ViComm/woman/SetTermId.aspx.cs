/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 固体識別設定
--	Progaram ID		: SetTermId
--
--  Creation Date	: 2010.02.09
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
//<a href="SetTermId.aspx?guid=on" utn="">
//  簡単ﾛｸﾞｲﾝ設定
//</a>

using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_SetTermId:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
			if (!iBridUtil.GetStringValue(Request.QueryString["release"]).Equals("1")) {
				ParseHTML oParseWoman = sessionWoman.parseContainer;
				oParseWoman.parseUser.postAction = ViCommConst.POST_ACT_GUID;
				if (sessionWoman.carrier.Equals(ViCommConst.DOCOMO) && sessionWoman.getTermIdFlag) {
					oParseWoman.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
				}
			} else {
				ReleaseTermId();
			}
			cmdSubmit_Click(sender,e);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string siModeId = "",sTermId = "";
		bool bNotExist = false;

		siModeId = Mobile.GetiModeId(sessionWoman.carrier,Request);
		sTermId = Mobile.GetUtn(sessionWoman.carrier,Request);

		ParseHTML oParseWoman = sessionWoman.parseContainer;
		oParseWoman.parseUser.postAction = 0;
		sessionWoman.errorMessage = string.Empty;

		if (sessionWoman.getTermIdFlag) {
			if (sTermId.Equals(string.Empty)) {
				sessionWoman.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_SET_TERM_ID_WOMAN));
			}
		} else {
			if (siModeId.Equals(string.Empty)) {
				sessionWoman.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
				RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_SET_TERM_ID_WOMAN));
			}
		}

		using (Cast oCast = new Cast()) {
			bNotExist = oCast.EixstTermId(sTermId,siModeId,sessionWoman.userWoman.userSeq);
		}

		if (bNotExist == false) {
			sessionWoman.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST_BY_SET);
			RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_SET_TERM_ID_WOMAN));
		}

		sessionWoman.userWoman.SetupTermId(sessionWoman.userWoman.userSeq,sTermId,siModeId);
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SET_TERM_ID_COMPLITE_WOMAN));
	}

	protected void ReleaseTermId() {
		sessionWoman.userWoman.SetupTermId(sessionWoman.userWoman.userSeq,"","");
		RedirectToMobilePage(sessionWoman.GetNavigateUrl(sessionWoman.root + sessionWoman.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_RELEASE_TERM_ID_COMPLITE_WOMAN));
	}
}
