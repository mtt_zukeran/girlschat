<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistTalkApp.aspx.cs" Inherits="ViComm_woman_RegistTalkApp" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="lblErrorMessage" runat="server" Text="" />		
		$PGM_HTML02;
		$PGM_HTML08;
		$PGM_HTML09;
		<cc1:iBMobileLiteralText ID="lblScript" runat="server" Text="" />	
	</mobile:Form>
</body>
</html>
