/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャラクタータイプ設定
--	Progaram ID		: GameCharacterTypeRegist.aspx
--
--  Creation Date	: 2011.07.21
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_GameCharacterTypeRegist:MobileSocialGameWomanBase {

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}

			if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {

		string sCharNo;
		sCharNo = iBridUtil.GetStringValue(Request.QueryString["charno"]);

		sessionWoman.logined = true;
		sessionWoman.userWoman.SetLastActionDate(sessionWoman.userWoman.userSeq);
		string sListCharacterIndex = string.Format("{0}{1}",sessionWoman.site.siteCd,sCharNo);

		sessionWoman.userWoman.characterList[sListCharacterIndex].gameCharacter.GetCurrentInfo(sessionWoman.site.siteCd,sessionWoman.userWoman.userSeq);
		
		if (!string.IsNullOrEmpty(this.sessionWoman.userWoman.characterList[sListCharacterIndex].gameCharacter.gameCharacterType)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_COMPLETE);
		}
		
		using (GameCharacter oGameCharacter = new GameCharacter()) {
			string sResult = oGameCharacter.SetupGameCharacterType(sessionWoman.site.siteCd,
																   sessionWoman.userWoman.userSeq,
																   sessionWoman.userWoman.curCharNo,
																   this.Request.QueryString["game_character_type"]);

			if (sResult == PwViCommConst.SetupGameCharacterType.RESULT_OK) {
				oGameCharacter.SetupTutorialStatus(
					sessionWoman.site.siteCd,
					sessionWoman.userWoman.userSeq,
					sessionWoman.userWoman.curCharNo,
					"1"
				);
			}

			if (sResult == PwViCommConst.SetupGameCharacterType.RESULT_OK) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_COMPLETE);
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_WOMAN_GAME_ERROR);
			}
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		RedirectToGameDisplayDoc(PwViCommConst.SCR_CAST_GAME_CHAR_TYPE_SELECT);
	}
}
