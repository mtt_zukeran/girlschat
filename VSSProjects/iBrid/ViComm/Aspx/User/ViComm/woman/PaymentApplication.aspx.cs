﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 精算申請
--	Progaram ID		: PaymentApplication
--
--  Creation Date	: 2010.09.27
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_woman_ModifyCastBank:MobileWomanPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdTopPage"] != null) {
				cmdTopPage_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionWoman.userWoman.PaymentApplication(
			sessionWoman.userWoman.userSeq
		);

		RedirectToDisplayDoc(ViCommConst.SCR_PAYMENT_APPLICATION_COMPLITE);
	}

	protected void cmdTopPage_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionWoman.GetNavigateUrl("UserTop.aspx"));
	}
}
