/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画コメント削除
--	Progaram ID		: DeleteCastMovieComment
--  Creation Date	: 2013.12.26
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_DeleteCastMovieComment:MobileWomanPageBase {
	private string sCastMovieCommentSeq;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		if (sessionWoman.logined == false) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		sCastMovieCommentSeq = iBridUtil.GetStringValue(Request.QueryString["commentseq"]);

		if (string.IsNullOrEmpty(sCastMovieCommentSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}

		if (!IsPostBack) {
			if (!sessionWoman.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_CAST_MOVIE_COMMENT,this.ActiveForm)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			pnlConfirm.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}			
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult = string.Empty;

		using (CastMovieComment oCastMovieComment = new CastMovieComment()) {
			oCastMovieComment.DeleteCastMovieComment(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				sCastMovieCommentSeq,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.DeleteCastMovieCommentResult.RESULT_OK)) {
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			return;
		}
	}
}
