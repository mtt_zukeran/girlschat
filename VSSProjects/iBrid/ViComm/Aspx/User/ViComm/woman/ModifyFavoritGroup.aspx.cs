/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入りグループ編集
--	Progaram ID		: ModifyFavoritGroup
--
--  Creation Date	: 2012.10.05
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_woman_ModifyFavoritGroup:MobileWomanPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionWoman.InitScreen(Response.Filter,this,Request,ViewState,IsPostBack);

		string sFavoritGroupSeq = iBridUtil.GetStringValue(Request.QueryString["grpseq"]);

		if (string.IsNullOrEmpty(sFavoritGroupSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}

		if (!IsPostBack) {
			DataSet oDataSet = GetFavoritGroup();

			if (oDataSet.Tables[0].Rows.Count != 0) {
				txtFavoritGroupNm.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["FAVORIT_GROUP_NM"]);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sFavoritGroupSeq);
			}
		}
	}

	private DataSet GetFavoritGroup() {
		DataSet oDataSet;
		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition(Request.QueryString);
		oCondition.SiteCd = sessionWoman.site.siteCd;
		oCondition.UserSeq = sessionWoman.userWoman.userSeq;
		oCondition.UserCharNo = sessionWoman.userWoman.curCharNo;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oDataSet = oFavoritGroup.GetPageCollection(oCondition,1,1);
		}

		return oDataSet;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pFavoritGroupSeq) {
		string sResult = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (txtFavoritGroupNm.Text.Equals(string.Empty)) {
			lblErrorMessage.Text = "ｸﾞﾙｰﾌﾟ名を入力して下さい";
			return;
		} else if (txtFavoritGroupNm.Text.Length > 20) {
			lblErrorMessage.Text = "ｸﾞﾙｰﾌﾟ名の入力は20文字までです";
			return;
		}

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			sResult = oFavoritGroup.ModifyFavoritGroup(
				sessionWoman.site.siteCd,
				sessionWoman.userWoman.userSeq,
				sessionWoman.userWoman.curCharNo,
				pFavoritGroupSeq,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,txtFavoritGroupNm.Text))
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			RedirectToMobilePage(sessionWoman.GetNavigateUrl("ListFavoritGroup.aspx"));
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
		}
	}
}
