/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE��ھ��ďڍ�
--	Progaram ID		: ViewGamePresentHistory
--
--  Creation Date	: 2011.09.16
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGamePresentHistory:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPresentHistorySeq = iBridUtil.GetStringValue(Request.QueryString["present_history_seq"]);

		if (!sPresentHistorySeq.Equals(string.Empty)) {
			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY,ActiveForm);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}
}