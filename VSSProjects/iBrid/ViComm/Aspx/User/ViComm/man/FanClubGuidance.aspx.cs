/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ファンクラブ手引
--	Progaram ID		: FanClubGuidance.aspx
--  Creation Date	: 2013.11.16
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_FanClubGuidance:MobileManPageBase {
	private string sCastUserSeq;
	private string sCastCharNo;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (string.IsNullOrEmpty(sCastUserSeq) || string.IsNullOrEmpty(sCastCharNo)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			if (!sessionMan.logined) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sResult = string.Empty;

			using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
				sResult = oFanClubStatus.GetFanClubAdmissionEnable(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sCastUserSeq,
					sCastCharNo
				);
			}

			if (sResult.Equals(PwViCommConst.FanClubAdmissionEnableResult.RESULT_OK)) {
				pnlAchieved.Visible = true;
			} else if (sResult.Equals(PwViCommConst.FanClubAdmissionEnableResult.RESULT_NG)) {
				pnlNotAchieved.Visible = true;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			if (!sessionMan.SetCastDataSetByUserSeq(sCastUserSeq,sCastCharNo,1)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
