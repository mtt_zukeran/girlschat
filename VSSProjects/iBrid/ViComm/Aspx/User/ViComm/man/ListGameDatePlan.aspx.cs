/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�ް����݈ꗗ
--	Progaram ID		: ListGameTown
--
--  Creation Date	: 2011.07.27
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameDatePlan:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_GAME_DATE_PLAN,this.ActiveForm);
		} else {
			string sDatePlanSeq = string.Empty;
			string sPartnerUserSeq = null;
			string sPartnerUserCharNo = null;
			string sSetFriendlyPoint = null;

			
			for (int i = 1;i <= 10;i++) {
				if (Request.Form[ViCommConst.BUTTON_GOTO_LINK + "_" + i] != null) {
					sDatePlanSeq = iBridUtil.GetStringValue(Request.Params["date_plan_seq" + i.ToString()]);
					sSetFriendlyPoint = iBridUtil.GetStringValue(Request.Params["set_friendly_point" + i.ToString()]);
				}
			}

			if (!sDatePlanSeq.Equals(string.Empty)) {
				string sResult = string.Empty;
				string sFriendlyPoint = null;
				string sPreFriendlyPoint = null;
				int iPreFriendlyPoint = 0;

				using (DatePlan oDatePlan = new DatePlan()) {
					sResult = oDatePlan.GetDate(
						this.sessionMan.site.siteCd,
						this.sessionMan.userMan.userSeq,
						this.sessionMan.userMan.userCharNo,
						ref sPartnerUserSeq,
						ref sPartnerUserCharNo,
						sDatePlanSeq
					);
				}

				FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
				oCondition.SiteCd = this.sessionMan.site.siteCd;
				oCondition.UserSeq = this.sessionMan.userMan.userSeq;
				oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
				oCondition.PartnerUserSeq = sPartnerUserSeq;
				oCondition.PartnerUserCharNo = sPartnerUserCharNo;

				using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
					sFriendlyPoint = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
				}
				
				iPreFriendlyPoint = int.Parse(sFriendlyPoint) - int.Parse(sSetFriendlyPoint);
				
				sPreFriendlyPoint = iPreFriendlyPoint.ToString();
				
				RedirectToMobilePage(sessionMan.GetNavigateUrl(
					sessionMan.root + sessionMan.sysType,
					sessionMan.sessionId,
					string.Format("ViewGameCharacterDateResult.aspx?result={0}&partner_user_seq={1}&partner_user_char_no={2}&date_plan_seq={3}&pre_friendly_point={4}",sResult,sPartnerUserSeq,sPartnerUserCharNo,sDatePlanSeq,sPreFriendlyPoint)
				));
			}
		}
	}
}