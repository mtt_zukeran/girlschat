﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り削除
--	Progaram ID		: ReleaseProdThemeFavorit
--
--  Creation Date	: 2011.07.01
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ReleaseProdThemeFavorite : MobileProdManPage {
	virtual protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);

		if (!IsPostBack) {

			string sProductId = iBridUtil.GetStringValue(this.Request.Params["product_id"]);
			using (Product oProduct = new Product()) {

				string sProductSeq = oProduct.ConvertProductId2Seq(sProductId);
				DataSet ds = oProduct.GetOneBySeq(sProductSeq);
				DataRow dr = ds.Tables[0].Rows[0];

				string sProductAgentId = iBridUtil.GetStringValue(dr["PRODUCT_AGENT_CD"]);
				using (FavoriteProduct oFavPro = new FavoriteProduct()) {
					oFavPro.FavoriteProductMainte(
						sessionMan.site.siteCd,
						sProductAgentId,
						int.Parse(sProductSeq.ToString()),
						int.Parse(sessionMan.GetUserSeq().ToString()),
						sessionMan.userMan.userCharNo,
						"",
						1
					);
				}
			}
		}
	}
}
