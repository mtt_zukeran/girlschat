﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾋﾞﾝｺﾞ ｶｰﾄﾞ詳細
--	Progaram ID		: ViewBingoCard
--
--  Creation Date	: 2011.07.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewBingoCard : MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		string sResult = this.sessionObj.bingoCard.GetBingoCard(
			this.sessionMan.site.siteCd,
			this.sessionMan.userMan.userSeq,
			this.sessionMan.userMan.userCharNo,
			ViCommConst.MAN);

		if (ViCommConst.GetBingoCardResult.NOT_HAVE_CARD.Equals(sResult)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"EntryBingo.aspx"));
		} 

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["bingo"])) {
				this.RedirectResultPage();
			}

			switch (this.sessionObj.bingoCard.bingoStatus) {
				case ViCommConst.BingoStatus.BINGO:
					this.tagBingo.Visible = true;
					this.tagBingoDenied.Visible = false;
					break;
				case ViCommConst.BingoStatus.BINGO_DENIED:
					this.tagBingo.Visible = false;
					this.tagBingoDenied.Visible = true;
					break;
				case ViCommConst.BingoStatus.BINGO_APPLIED:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"EntryBingo.aspx"));
					break;
				default:
					this.tagBingo.Visible = false;
					this.tagBingoDenied.Visible = false;
					break;
			}
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER,
					ActiveForm);
		}
	}

	private void RedirectResultPage() {
		string sBingoStatus;
		string sBingoElectionPoint;
		string sBingoElectionAmt;
		string sBingoCardNo;
		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			oMailDeBingo.DealJackpot(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				ViCommConst.MAN,
				out sBingoStatus,
				out sBingoElectionAmt,
				out sBingoElectionPoint,
				out sBingoCardNo);
		}

		switch (sBingoStatus) {
			case ViCommConst.BingoStatus.BINGO_DENIED: {
					// 先にビンゴした人がいる
					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("bingo",ViCommConst.FLAG_OFF_STR);
					oParameters.Add("bingocardno",sBingoCardNo);
					RedirectToDisplayDoc(ViCommConst.SCR_MAN_BINGO_APPLICATION,oParameters);
				}
				break;
			case ViCommConst.BingoStatus.BINGO_COMPLETE: {
					// 申請完了
					string sToMailAddress = this.sessionMan.site.infoEmailAddr;
					string sFromMailAddress = this.sessionMan.userMan.emailAddr;
					string sTitle = "【男性】メールdeビンゴ賞金獲得";
					string sDoc = string.Format("会員ID：{0}\r\nﾊﾝﾄﾞﾙﾈｰﾑ：{1}\r\n付与pt：{2}\r\n会員ﾒｰﾙｱﾄﾞﾚｽ：{3}\r\nｶｰﾄﾞ枚数：{4}枚目\r\n",
						this.sessionMan.userMan.loginId,this.sessionMan.userMan.handleNm,sBingoElectionPoint,this.sessionMan.userMan.emailAddr,sBingoCardNo);
					//メールアドレスの形式をチェック
					try {
						new System.Net.Mail.MailAddress(sFromMailAddress);
					}
					catch (FormatException) {
						sFromMailAddress = sToMailAddress;
					}

					this.SendMail(sFromMailAddress,sToMailAddress,sTitle,sDoc);

					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("bingo",ViCommConst.FLAG_ON_STR);
					oParameters.Add("bingocardno",sBingoCardNo);
					RedirectToDisplayDoc(ViCommConst.SCR_MAN_BINGO_APPLICATION,oParameters);
				}
				break;
			case ViCommConst.BingoStatus.BINGO_APPLIED:
				// 申請済み
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"EntryBingo.aspx"));
				break;
			case ViCommConst.BingoStatus.BINGO:
				// ビンゴ達成（ポイント付与失敗）
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				break;
			default:
				break;
		}
	}
}
