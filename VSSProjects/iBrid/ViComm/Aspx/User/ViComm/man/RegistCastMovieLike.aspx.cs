/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画イイネ登録
--	Progaram ID		: RegistCastMovieLike
--  Creation Date	: 2013.12.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistCastMovieLike:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));
			string sResult = string.Empty;

			if (string.IsNullOrEmpty(sMovieSeq) || string.IsNullOrEmpty(sBackUrl)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			using (CastMovieLike oCastMovieLike = new CastMovieLike()) {
				oCastMovieLike.RegistCastMovieLike(sessionMan.site.siteCd,sMovieSeq,sessionMan.userMan.userSeq,out sResult);
			}

			if (sResult.Equals(PwViCommConst.RegistCastMovieLikeResult.RESULT_OK)) {
				if (IsAvailableService(ViCommConst.RELEASE_CAST_LIKE_NOTICE,2)) {
					using (CastMovie oCastMovie = new CastMovie())
					using (CastLikeNotice oCastLikeNotice = new CastLikeNotice()) {
						DataSet oCastDataSet = oCastMovie.GetOneByMovieSeq(sessionMan.site.siteCd,sMovieSeq);

						if (oCastDataSet.Tables[0].Rows.Count > 0) {
							string sCastSeq = iBridUtil.GetStringValue(oCastDataSet.Tables[0].Rows[0]["USER_SEQ"]);
							string sCastCharNo = iBridUtil.GetStringValue(oCastDataSet.Tables[0].Rows[0]["USER_CHAR_NO"]);
							oCastLikeNotice.CastLikeNoticeMainte(
								sessionMan.site.siteCd,
								sCastSeq,
								sCastCharNo,
								PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_MOVIE,
								sMovieSeq,
								string.Empty,
								string.Empty,
								sessionMan.userMan.userSeq
							);
						}
					}
				}
				
				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sBackUrl));
					return;
				} else {
					pnlComplete.Visible = true;
				}
			} else if (sResult.Equals(PwViCommConst.RegistCastMovieLikeResult.RESULT_NG_VIEW)) {
				sessionMan.errorMessage = "先に動画を閲覧してください";
				pnlError.Visible = true;
			} else if (sResult.Equals(PwViCommConst.RegistCastMovieLikeResult.RESULT_NG_LIKED)) {
				sessionMan.errorMessage = "既にｲｲﾈ済みです";
				pnlError.Visible = true;
			} else if (sResult.Equals(PwViCommConst.RegistCastMovieLikeResult.RESULT_NG_CAST_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.ERR_REFUSE_MAN);
				return;
			} else if (sResult.Equals(PwViCommConst.RegistCastMovieLikeResult.RESULT_NG_SELF_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
				return;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
