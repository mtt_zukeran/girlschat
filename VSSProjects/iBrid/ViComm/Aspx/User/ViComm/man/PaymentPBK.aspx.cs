/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �߲�ı��(ײ���)�x��
--	Progaram ID		: PaymentPBK
--
--  Creation Date	: 2010.02.08
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentPBK:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {

		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sSid = "";
			string sBid = iBridUtil.GetStringValue(Request.QueryString["bid"]);
			string sTid = iBridUtil.GetStringValue(Request.QueryString["tid"]);

			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_RAIO,
					ViCommConst.SETTLE_POINT_AFFILIATE,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					0,
					0,
					"",
					"",
					out sSid);
			}
			string sSettleUrl = "";

			using (SiteSettle oSiteSettle = new SiteSettle()) {
				if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_RAIO,ViCommConst.SETTLE_POINT_AFFILIATE)) {
					Encoding enc = Encoding.GetEncoding("Shift_JIS");
					sSettleUrl = string.Format(oSiteSettle.settleUrl,sBid,sTid,sSid + ":" + sessionMan.userMan.userSeq);
				}
			}
			Response.Redirect(sSettleUrl);
		}
	}
}
