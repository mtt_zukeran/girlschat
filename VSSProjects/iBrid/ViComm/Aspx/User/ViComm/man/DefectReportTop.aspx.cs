/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: sορTOP
--	Progaram ID		: DefectReportTop
--
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiyia
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_DefectReportTop:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_DEFECT_REPORT,this.ActiveForm);
		}
	}
}