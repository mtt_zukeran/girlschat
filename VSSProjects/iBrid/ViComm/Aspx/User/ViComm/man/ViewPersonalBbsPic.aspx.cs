/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�f���ʐ^�ڍ�
--	Progaram ID		: ViewPersonalBbsPic
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewPersonalBbsPic:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		string sCastSeq,sCastCharNo,sPicSeq;
		string sLoginId;
		int iChargePoint;
		int iPaymentAmt;
		bool bOk = false;
		bool bDupUsed = false;


		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			} else {
				int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
				ulong uSeekMode;
				bool bControlList;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
					bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
				} else {
					bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_PIC,ActiveForm);
				}
				if (bControlList) {
					sCastSeq = sessionMan.GetBbsPicValue("USER_SEQ");
					sCastCharNo = sessionMan.GetBbsPicValue("USER_CHAR_NO");
					sPicSeq = sessionMan.GetBbsPicValue("PIC_SEQ");

					sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);

					if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo)) {
						sessionMan.errorMessage = "";

						using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
							bDupUsed = oObjUsedHistory.GetOne(
												sessionMan.userMan.siteCd,
												sessionMan.userMan.userSeq,
												ViCommConst.ATTACH_PIC_INDEX.ToString(),
												sPicSeq);
						}
						if (bDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
							bOk = sessionMan.CheckBbsPicBalance(sCastSeq,sCastCharNo,sPicSeq,out iChargePoint,out iPaymentAmt);
							if (bOk == false) {
								RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_BBS_PIC));
							} else {
								using (WebUsedLog oLog = new WebUsedLog()) {
									oLog.CreateWebUsedReport(
											sessionMan.userMan.siteCd,
											sessionMan.userMan.userSeq,
											ViCommConst.CHARGE_BBS_PIC,
											iChargePoint,
											sCastSeq,
											sCastCharNo,
											"",
											iPaymentAmt);
								}
								using (Access oAccess = new Access()) {
									oAccess.AccessPicPage(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sPicSeq);
								}
								using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
									oObjUsedHistory.AccessBbsObj(
											sessionMan.userMan.siteCd,
											sessionMan.userMan.userSeq,
											ViCommConst.ATTACH_PIC_INDEX.ToString(),
											sPicSeq);
								}

								int iGetBbsBingoNoCompleteFlag = ViCommConst.FLAG_OFF;
								string sGetBbsBingoNoResult = string.Empty;

								using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
									oBbsBingoTerm.GetBbsBingoNo(
										sessionMan.site.siteCd,
										sessionMan.userMan.userSeq,
										sPicSeq,
										ViCommConst.FLAG_OFF,
										out iGetBbsBingoNoCompleteFlag,
										out sGetBbsBingoNoResult
									);
								}

								if (sGetBbsBingoNoResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_OK) && iGetBbsBingoNoCompleteFlag.Equals(ViCommConst.FLAG_ON)) {
									UrlBuilder oNextUrl = new UrlBuilder(ViCommPrograms.GetCurrentAspx(),sessionMan.requestQuery.QueryString);
									string sToUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(oNextUrl.ToString(),System.Text.Encoding.GetEncoding(932)));
									RedirectToMobilePage(sessionMan.GetNavigateUrl(sToUrl));
									return;
								}
							}
						}
					}
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListBbsPic.aspx"));
				}
			}
		}
	}
}
