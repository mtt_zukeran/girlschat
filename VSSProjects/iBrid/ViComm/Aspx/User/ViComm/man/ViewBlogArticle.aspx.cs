/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �u���O���e �ڍ�
--	Progaram ID		: ViewBlogArticle
--
--  Creation Date	: 2011.04.11
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewBlogArticle:MobileBlogManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["blog_article_seq"]));
		if (!rgxMatch.Success) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			if (!sessionObj.IsImpersonated) {
				using (BlogArticle oBlogArticle = new BlogArticle()) {
					oBlogArticle.UpdateReadingCount(
						sessionMan.site.siteCd,
						null,
						null,
						null,
						Request.QueryString["blog_article_seq"]
					);
				}
			}
			string sMailSeq = iBridUtil.GetStringValue(Request.QueryString["mailseq"]);
			if (!sMailSeq.Equals(string.Empty)) {
				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailReadFlag(sMailSeq);
				}
			}
		}

		ulong lSeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE;
		if (!ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out lSeekMode)) {
			lSeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE;
		} else if (lSeekMode != ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE_PICKUP) {
			lSeekMode = ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE;
		}
		sessionMan.ControlList(Request,lSeekMode,ActiveForm);
	}
}
