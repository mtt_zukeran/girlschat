/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 投票順位一覧(エントリー制)
--	Progaram ID		: ListElectionRank
--  Creation Date	: 2013.12.12
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListElectionRank:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_ELECTION_ENTRY_CURRENT,this.ActiveForm);
		}
	}
}