/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@�����v���ꗗ
--	Progaram ID		: ListGameSupportRequest
--
--  Creation Date	: 2011.09.05
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameSupportRequest:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if(String.IsNullOrEmpty(this.Request.QueryString["battle_log_seq"])) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(this.Request.QueryString["battle_log_seq"]);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_SUPPORT_REQUEST,ActiveForm);
	}
}
