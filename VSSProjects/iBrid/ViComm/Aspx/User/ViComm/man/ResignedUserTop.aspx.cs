/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 退会者トップ
--	Progaram ID		: ResignedUserTop.aspx
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ResignedUserTop:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (iBridUtil.GetStringValue(Request.QueryString["resignedreturn"]).Equals(ViCommConst.FLAG_ON_STR)) {
				ResignedReturnUser();
			}
			sessionMan.ControlList(
				Request,
				ViCommConst.INQUIRY_RANDOM_RECOMMEND,
				ActiveForm);
		}
	}

	private void ResignedReturnUser() {
		string sResult = string.Empty;
		using (User oUser = new User()) {
			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCookie"]).Equals(ViCommConst.FLAG_ON_STR)) {
					if (Request.Cookies["maqia"] != null) {
						oUser.GetOne(iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiauid"]), iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiasex"]));
					}
				}
			} else {
				oUser.GetUserSexByIModeId(sessionMan.currentIModeId, sessionMan.site.siteCd);
			}
			if (oUser.userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)) {
				sessionMan.userMan.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_LOGINID,oUser.loginId,oUser.loginPassword,out sResult);
			}
		}
		if (sResult.Equals("0")) {
			Response.Redirect(sessionMan.site.url);
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.site.nonUserTopId));
		}
	}
}
