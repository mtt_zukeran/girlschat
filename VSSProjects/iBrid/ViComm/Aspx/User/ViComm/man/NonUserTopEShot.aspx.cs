/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 登録前トップ(e-Shot)
--	Progaram ID		: NonUserTopEShot
--
--  Creation Date	: 2010.02.12
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_NonUserTopEShot:MobileManPageBase {

	private string userSeq;
	private string userStatus;
	private string loginResult;

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
			sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_TEL,"",txtLoginId.Text,txtPassword.Text,Session.SessionID,out userSeq,out userStatus,out loginResult);
		} else {
			sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_LOGINID,"",txtLoginId.Text,txtPassword.Text,Session.SessionID,out userSeq,out userStatus,out loginResult);
		}
		LoginResult(false);
	}

	private void LoginResult(bool pUnt) {
		if (loginResult.Equals("0")) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.site.userTopId));
		} else {
			lblErrorMessage.Text = string.Empty;

			//ブラックユーザーと退会ユーザーは別画面に遷移させる
			if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_BLACK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_BLACK));
			} else if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_RESIGNED)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_RESIGNED));
			}

			if (pUnt == false) {
				lblErrorMessage.Text += GetErrorMessage(loginResult);
			}
		}
	}
}
