<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GameLoginUser.aspx.cs" Inherits="ViComm_man_GameLoginUser" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
	    <ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		$PGM_HTML05;
		<ibrid:iBMobileTextBox ID="txtLoginId" runat="server" MaxLength="11" Size="16" Numeric="true"></ibrid:iBMobileTextBox>
		$PGM_HTML06;
		<ibrid:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="10" Numeric="true"></ibrid:iBMobileTextBox>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
