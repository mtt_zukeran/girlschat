﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品着せ替えﾂｰﾙ一覧
--	Progaram ID		: ListProdMov
--
--  Creation Date	: 2011.06.30
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdTheme : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

		sessionMan.ControlList(
				this.Request,
				ViCommConst.INQUIRY_PRODUCT_THEME,
				this.ActiveForm);

	}

}
