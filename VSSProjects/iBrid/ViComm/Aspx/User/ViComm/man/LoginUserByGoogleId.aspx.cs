/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員ログイン・登録振り分け(Googleアカウント利用)
--	Progaram ID		: LoginUserByGoogleId
--  Creation Date	: 2015.01.08
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using System.Collections;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public partial class ViComm_man_LoginUserByGoogleId:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			string sCode = iBridUtil.GetStringValue(Request.QueryString["code"]);
			
			if (string.IsNullOrEmpty(sCode)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}

			// Cookie
			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				if (Request.Cookies["maqiaad"] != null) {
					sessionMan.adCd = iBridUtil.GetStringValue(Request.Cookies["maqiaad"]["adcd"]);
					sessionMan.affiliateCompany = iBridUtil.GetStringValue(Request.Cookies["maqiaad"]["company"]);
					sessionMan.affiliaterCd = iBridUtil.GetStringValue(Request.Cookies["maqiaad"]["affiliatercd"]);
					sessionMan.introducerFriendCd = iBridUtil.GetStringValue(Request.Cookies["maqiaad"]["intro"]);

					if (!sessionMan.adCd.Equals("")) {
						using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
							string sGroupCd;
							if (oSiteAdGroup.GetAdGroupCd(sessionMan.site.siteCd,sessionMan.adCd,out sGroupCd,out sessionMan.groupType)) {
								sessionMan.adGroup = new AdGroup(sessionMan.site.siteCd,sGroupCd);
							}
						}
					}
				} else if (Request.Cookies["maqiaadp"] != null) {
					string sMaqiaAdStr = iBridUtil.GetStringValue(Request.Cookies["maqiaadp"].Value);
					sMaqiaAdStr = HttpUtility.UrlDecode(sMaqiaAdStr);
					string[] sMaqiaAdArr = sMaqiaAdStr.Split('&');
					
					if (sMaqiaAdArr.Length > 0) {
						string[] sParamArr;
						string sKey;
						string sValue;
						
						for (int i = 0;i < sMaqiaAdArr.Length;i++) {
							sParamArr = sMaqiaAdArr[i].Split('=');
							
							if (sParamArr.Length == 2) {
								sKey = sParamArr[0];
								sValue = sParamArr[1];
								
								switch (sKey) {
									case "adcd":
										sessionMan.adCd = sValue;
										break;
									case "company":
										sessionMan.affiliateCompany = sValue;
										break;
									case "affiliatercd":
										sessionMan.affiliaterCd = sValue;
										break;
									case "intro":
										sessionMan.introducerFriendCd = sValue;
										break;
								}
							}
						}
					}

					if (!sessionMan.adCd.Equals("")) {
						using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
							string sGroupCd;
							if (oSiteAdGroup.GetAdGroupCd(sessionMan.site.siteCd,sessionMan.adCd,out sGroupCd,out sessionMan.groupType)) {
								sessionMan.adGroup = new AdGroup(sessionMan.site.siteCd,sGroupCd);
							}
						}
					}
					
				} else if (Request.Cookies["maqiaadcd"] != null) {
					sessionMan.adCd = iBridUtil.GetStringValue(Request.Cookies["maqiaadcd"].Value);

					if (Request.Cookies["maqiacompany"] != null) {
						sessionMan.affiliateCompany = iBridUtil.GetStringValue(Request.Cookies["maqiacompany"].Value);
					}

					if (Request.Cookies["maqiaaffiliatercd"] != null) {
						sessionMan.affiliaterCd = iBridUtil.GetStringValue(Request.Cookies["maqiaaffiliatercd"].Value);
					}

					if (Request.Cookies["maqiaintro"] != null) {
						sessionMan.introducerFriendCd = iBridUtil.GetStringValue(Request.Cookies["maqiaintro"].Value);
					}

					if (!sessionMan.adCd.Equals("")) {
						using (SiteAdGroup oSiteAdGroup = new SiteAdGroup()) {
							string sGroupCd;
							if (oSiteAdGroup.GetAdGroupCd(sessionMan.site.siteCd,sessionMan.adCd,out sGroupCd,out sessionMan.groupType)) {
								sessionMan.adGroup = new AdGroup(sessionMan.site.siteCd,sGroupCd);
							}
						}
					}
				}
			}
			
			string sRequestTokenUrl = string.Empty;
			string sConsumerKey = string.Empty;
			string sConsumerSecret = string.Empty;
			string sCallbackUrl = string.Format("http://{0}/user/start.aspx?googlelogin=1",sessionMan.site.hostNm);
			//string sCallbackUrl = string.Format("http://localhost:81/user/start.aspx?googlelogin=1",sessionMan.site.hostNm);
			string sGraphUrl = string.Empty;

			using (SnsOAuth oSnsOAuth = new SnsOAuth()) {
				DataSet oDataSet = oSnsOAuth.GetOneBySnsType(sessionMan.site.siteCd,PwViCommConst.SnsType.GOOGLE);

				if (oDataSet.Tables[0].Rows.Count == 0) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}

				sRequestTokenUrl = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REQUEST_TOKEN_URL"]);
				sConsumerKey = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CONSUMER_KEY"]);
				sConsumerSecret = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CONSUMER_SECRET"]);
				sGraphUrl = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["GRAPH_URL"]);
			}

			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

			// POSTデータの作成
			Encoding enc = Encoding.UTF8;
			string param = "";
			Hashtable ht = new Hashtable();
			ht["code"] = sCode;
			ht["client_id"] = sConsumerKey;
			ht["client_secret"] = sConsumerSecret;
			ht["redirect_uri"] = sCallbackUrl;
			ht["grant_type"] = "authorization_code";
			
			foreach (string k in ht.Keys) {
				param += String.Format("{0}={1}&",k,ht[k]);
			}
			byte[] data = Encoding.ASCII.GetBytes(param);

			// リクエストの作成
			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sRequestTokenUrl);
			req.Method = "POST";
			req.ContentType = "application/x-www-form-urlencoded";
			req.ContentLength = data.Length;

			// POSTデータの書き込み
			Stream reqStream = req.GetRequestStream();
			reqStream.Write(data,0,data.Length);
			reqStream.Close();

			// レスポンスの取得と読み込み
			WebResponse res = req.GetResponse();
			Stream resStream = res.GetResponseStream();
			StreamReader sr = new StreamReader(resStream,enc);
			string html = sr.ReadToEnd();
			sr.Close();
			resStream.Close();
			
			object obj = JsonConvert.DeserializeObject(html);
			
			string sAccessToken = ((Newtonsoft.Json.Linq.JObject)(obj))["access_token"].ToString();
			
			if (string.IsNullOrEmpty(sAccessToken)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}

			req = (HttpWebRequest)WebRequest.Create(string.Format("{0}?access_token={1}",sGraphUrl,sAccessToken));

			req.Timeout = 60000;
			res = req.GetResponse();

			res = req.GetResponse();
			resStream = res.GetResponseStream();
			sr = new StreamReader(resStream,enc);
			html = sr.ReadToEnd();
			sr.Close();
			resStream.Close();

			obj = JsonConvert.DeserializeObject(html);

			string sGoogleId = ((Newtonsoft.Json.Linq.JObject)(obj))["id"].ToString();
			string sGoogleEmailAddr = ((Newtonsoft.Json.Linq.JObject)(obj))["email"].ToString();

			bool bExistFlag = false;
			string sSexCd = string.Empty;

			using (User oUser = new User()) {
				DataSet oDataSet;
				oDataSet = oUser.GetOneBySnsAccountId(sGoogleId,PwViCommConst.SnsType.GOOGLE);

				if (oDataSet.Tables[0].Rows.Count > 0) {
					bExistFlag = true;

					sSexCd = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["SEX_CD"]);
				}
			}

			sessionObj.snsType = PwViCommConst.SnsType.GOOGLE;
			sessionObj.snsId = sGoogleId;

			if (!bExistFlag) {
				using (User oUser = new User()) {
					string sResult = oUser.CheckEmailAddrDuplication(sessionMan.site.siteCd,sGoogleEmailAddr);
					if (sResult.Equals(PwViCommConst.CheckEmailAddrDuplication.RESULT_OK)) {
						sessionObj.snsEmailAddr = sGoogleEmailAddr;
						RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByOpenId.aspx"));
					} else if (sResult.Equals(PwViCommConst.CheckEmailAddrDuplication.RESULT_NG)) {
						RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
					} else {
						IDictionary<string,string> oParameters = new Dictionary<string,string>();
						oParameters.Add("result",sResult);
						RedirectToDisplayDoc(PwViCommConst.SCR_MAN_EMAIL_ADDR_DUPLICATION,oParameters);
					}
				}
			} else if (sSexCd.Equals(ViCommConst.MAN)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("LoginUser.aspx"));
			} else {
				RedirectToMobilePage(string.Format("http://{0}{1}{2}/(S({3}))/LoginUser.aspx",sessionMan.site.jobOfferSiteHostNm,sessionMan.root,sessionMan.site.jobOfferSiteType,sessionMan.sessionId));
			}
		}
	}

	private bool OnRemoteCertificateValidationCallback(
	  Object sender,
	  X509Certificate certificate,
	  X509Chain chain,
	  SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}
	
}