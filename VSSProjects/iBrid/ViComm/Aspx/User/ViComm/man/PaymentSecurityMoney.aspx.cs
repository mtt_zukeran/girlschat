/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: セキュリティマネー支払
--	Progaram ID		: PaymentSecurityMoney
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentSecurityMoney:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);

			string sSettleType = iBridUtil.GetStringValue(Request.QueryString["type"]);
			string sPageKey;
			switch (sSettleType) {
				case ViCommConst.SETTLE_ECO_CHIP:
					sPageKey = "PaymentEcoChip.aspx";
					ViewState["SETTLE_TYPE"] = sSettleType;
					break;
				default:
					sPageKey = "PaymentSecurityMoney.aspx";
					ViewState["SETTLE_TYPE"] = ViCommConst.SETTLE_S_MONEY;
					break;
			}

			DataSet ds;
			ds = GetPackDataSet(ViewState["SETTLE_TYPE"].ToString(),0);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
					lstPack.SelectedIndex = lstPack.Items.Count - 1;
					lstPack.Visible = false;
					sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
					sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
					sessionMan.userMan.settleServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
					break;
				}
			}
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,sPageKey,ViewState);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstPack.Items[lstPack.SelectedIndex].Value;
		string sSid = "";
		string sCC = "";
		string sCompany;

		string sSettleType = iBridUtil.GetStringValue(ViewState["SETTLE_TYPE"]);
		switch (sSettleType) {
			case ViCommConst.SETTLE_ECO_CHIP:
				sCompany = ViCommConst.SETTLE_CORP_EWALLET;
				break;
			default:
				sCompany = ViCommConst.SETTLE_CORP_GIS;
				break;
		}

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,sSettleType,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sCompany,
					sSettleType,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					string.Empty,
					string.Empty,
					out sSid);
			}
			sCC = oPack.commoditiesCd;
		}
		string sSettleUrl = "";
		string sBackUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,sCompany,sSettleType)) {
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sCC,sSid,sSalesAmt,sBackUrl,sessionMan.userMan.userSeq);
			}
		}
		Response.Redirect(sSettleUrl);
	}
}
