/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエストエントリー
--	Progaram ID		: RegistGameQuestEntry
--
--  Creation Date	: 2012.07.12
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_man_RegistGameQuestEntry:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["quest_seq"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sQuestSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		string sQuestType = iBridUtil.GetStringValue(this.Request.QueryString["quest_type"]);
		string sScrid = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);

		rgxMatch = rgx.Match(sQuestType);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		if(sQuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
			rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["level_quest_seq"]));
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

			rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["little_quest_seq"]));
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else if(sQuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
			rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["level_quest_seq"]));
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else if(sQuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
			rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["other_quest_seq"]));
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}

		this.CheckQuestClear();

		if (this.CheckExistQuestClearReward(sQuestSeq)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,string.Format("ListGameQuestRewardGet.aspx?quest_seq={0}&scrid={1}",sQuestSeq,sScrid)));
		}

		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_GAME_REGIST_QUEST_ENTRY,this.ActiveForm);
		}
	}

	private bool CheckExistQuestClearReward(string sQuestSeq) {
		QuestEntryLogSeekCondition oCondition = new QuestEntryLogSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.UserSeq = this.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
		oCondition.SexCd = this.sessionMan.sexCd;
		oCondition.QuestSeq = sQuestSeq;
		oCondition.GetRewardFlag = ViCommConst.FLAG_OFF_STR;

		DataSet ds = null;

		using (QuestEntryLog oQuestEntryLog = new QuestEntryLog()) {
			ds = oQuestEntryLog.GetPageCollection(oCondition,1,1);
		}

		if (ds.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.FRIENDLY,
				PwViCommConst.GameQuestTrialCategoryDetail.STEDY,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}
