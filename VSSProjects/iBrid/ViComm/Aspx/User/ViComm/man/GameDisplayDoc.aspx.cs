/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ゲームサイト文章表示
--	Progaram ID		: GameDisplayDoc
--
--  Creation Date	: 2011.07.21
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Text;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Configuration;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GameDisplayDoc:MobileSocialGameManBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (IsAvailableService(ViCommConst.RELEASE_UPDATE_SESSION)) {
			if (sessionMan.logined) {
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			}
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			using (SiteHtmlDoc oDoc = new SiteHtmlDoc()) {
				System.Collections.Generic.List<string> oSexCdList = new System.Collections.Generic.List<string>();
				if (ViCommConst.FLAG_ON == sessionObj.site.siteHtmlDocSexCheckFlag) {
					oSexCdList.Add("3");
					if(sessionObj.logined){
						oSexCdList.Add(sessionObj.sexCd);
					}
				}
				string sHtml;
				if (iBridUtil.GetStringValue(Request.QueryString["sc"]) == "") {
					oDoc.GetOne(sessionMan.site.siteCd,iBridUtil.GetStringValue(Request.QueryString["doc"]),sessionMan.currentPageAgentType,ViCommConst.DEFUALT_PUB_DAY,oSexCdList.ToArray(),out sHtml);
				} else {
					oDoc.GetOne(iBridUtil.GetStringValue(Request.QueryString["sc"]),iBridUtil.GetStringValue(Request.QueryString["doc"]),sessionMan.currentPageAgentType,ViCommConst.DEFUALT_PUB_DAY,oSexCdList.ToArray(),out sHtml);
				}

				tag01.Text = sHtml;
			}
		} 
	}
}
