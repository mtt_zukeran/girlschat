<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewDefectReport.aspx.cs" Inherits="ViComm_man_ViewDefectReport" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
