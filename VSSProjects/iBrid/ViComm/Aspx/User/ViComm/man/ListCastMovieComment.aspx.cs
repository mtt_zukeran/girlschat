/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画コメント一覧
--	Progaram ID		: ListCastMovieComment
--  Creation Date	: 2013.12.26
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListCastMovieComment:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!sessionMan.logined && !sessionMan.IsImpersonated) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);

			if (string.IsNullOrEmpty(sMovieSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_MOVIE_COMMENT,this.ActiveForm);
		}
	}
}
