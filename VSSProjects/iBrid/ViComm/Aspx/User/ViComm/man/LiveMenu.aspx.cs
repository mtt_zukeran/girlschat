/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ライブサイトトップメニュー
--	Progaram ID		: LiveMenu
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class TypeP_Site_Premium_LiveMenu:MobileManPageBase {
	private bool publicFlag;
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = new HtmlFilter(Response.Filter,sessionMan.carrier,SessionObjs.Current.parseContainer);

		if (!IsPostBack) {
			frmLiveMenu.BackColor = Color.FromName(sessionMan.site.colorBack);
			frmLiveMenu.ForeColor = Color.FromName(sessionMan.site.colorChar);

			string sLiveScreenID = iBridUtil.GetStringValue(Request.QueryString["livescreenid"]);
			if (sLiveScreenID.Equals("")) {
				sLiveScreenID = "01";
			}
			ViewState["LIVE_SCREEN_ID"] = sLiveScreenID;

			publicFlag = false;
			lblNoDataError.Visible = false;
			lstLiveCloseHeader.Visible = false;
			lstLiveCloseFooter.Visible = false;

			using (LiveBroadcast objLiveBroadcast = new LiveBroadcast()) {
				DataSet dsLiveBroadcast = objLiveBroadcast.GetLiveInfo(sessionMan.site.siteCd,ViewState["LIVE_SCREEN_ID"].ToString());
				lstLiveOpenHeader.DataSource = dsLiveBroadcast;
				lstLiveOpenFooter.DataSource = dsLiveBroadcast;
				lstLiveCloseHeader.DataSource = dsLiveBroadcast;
				lstLiveCloseFooter.DataSource = dsLiveBroadcast;

				if (dsLiveBroadcast.Tables[0].Rows.Count > 0) {
					DataRow dr = dsLiveBroadcast.Tables[0].Rows[0];
					ViewState["CAST_USER_SEQ"] = dr["CAST_USER_SEQ"].ToString();
					ViewState["LIVE_SEQ"] = dr["LIVE_SEQ"].ToString();

					if (int.Parse(dr["PUBLISH_FLAG"].ToString()) != 0) {
						using (LiveCamera objCamera = new LiveCamera()) {
							publicFlag = objCamera.IsMainCameraActive(ViewState["LIVE_SEQ"].ToString());
						}
					}
				} else {
					lstLiveOpenHeader.Visible = false;
					lstLiveOpenFooter.Visible = false;
				}
			}

			if (publicFlag) {
				lblNoDataError.Visible = false;
				using (LiveCamera objCamera = new LiveCamera()) {
					DataSet dsCamera = objCamera.GetList(sessionMan.site.siteCd,ViewState["LIVE_SEQ"].ToString(),true);
					if (dsCamera.Tables[0].Rows.Count > 0) {
						lstCamera.DataSource = dsCamera;
						lstCamera.Visible = true;
					} else {
						lstCamera.Visible = false;
					}
				}

				using (Cast oCast = new Cast()) {
					if (oCast.GetCastInfo(sessionMan.site.siteCd,ViewState["CAST_USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO)) {
						ViewState["CHARACTER_ONLINE_STATUS"] = oCast.characterOnlineStatus;
					} else {
						ViewState["CHARACTER_ONLINE_STATUS"] = ViCommConst.USER_OFFLINE;
					}
				}
			} else {
				lstLiveOpenHeader.Visible = false;
				lstLiveOpenFooter.Visible = false;
				lstCamera.Visible = false;
				lstLiveCloseHeader.Visible = true;
				lstLiveCloseFooter.Visible = true;
				ViewState["CHARACTER_ONLINE_STATUS"] = ViCommConst.USER_OFFLINE;
			}

			using (OtherWebSite oOtherWebSite = new OtherWebSite()) {
				if (oOtherWebSite.GetOne(sessionMan.userMan.webLocCd)) {
					lnkTopMenu.NavigateUrl = string.Format(oOtherWebSite.loginUrl,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
					lnkMyPage.NavigateUrl = string.Format(oOtherWebSite.myPageUrl,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
					lnkTvSys1.NavigateUrl = string.Format(oOtherWebSite.vicommLoginUrl,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
					lnkTvSys2.NavigateUrl = string.Format(oOtherWebSite.vicommLoginUrl,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
					lnkTvSys1.Text = sessionMan.site.GetSupplement("01");
					lnkTvSys2.Text = sessionMan.site.GetSupplement("02");
					lnkTopMenu.Text = sessionMan.site.GetSupplement("03");
					lnkMyPage.Text = sessionMan.site.GetSupplement("04");
				} else {
					lnkTopMenu.Text = "";
					lnkTopMenu.NavigateUrl = "";
					lnkMyPage.Text = "";
					lnkMyPage.NavigateUrl = "";
					lnkTvSys1.Text = "";
					lnkTvSys1.NavigateUrl = "";
					lnkTvSys2.Text = "";
					lnkTvSys2.NavigateUrl = "";
				}
			}

			lnkRefresh.NavigateUrl = sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("LiveMenu.aspx?livescreenid={0}",ViewState["LIVE_SCREEN_ID"].ToString()));
			DataBind();
		}

	}

	protected string GetMenuDoc(Object pHtmlDoc1,Object pHtmlDoc2) {
		string sStaffInfo = pHtmlDoc1.ToString() + pHtmlDoc2.ToString();
		return sStaffInfo;
	}

	protected string GetLiveLink(Object pCameraID) {
		return sessionMan.GetNavigateUrl(
			sessionMan.root + sessionMan.sysType,
			Session.SessionID,
			string.Format("RequestIVP.aspx?castseq={0}&request={1}&liveseq={2}&camera={3}&chgtype={4}",
					ViewState["CAST_USER_SEQ"].ToString(),
					ViCommConst.REQUEST_LIVE_MONITOR,
					ViewState["LIVE_SEQ"].ToString(),
					pCameraID.ToString(),
					ViCommConst.CHARGE_VIEW_LIVE
				)
			);
	}


	protected bool IsVisibleTalkLink() {
		return ((publicFlag) && (ViewState["CHARACTER_ONLINE_STATUS"].ToString().Equals(ViCommConst.USER_WAITING.ToString())));
	}

	protected string GetTalkLinkUrl() {
		string sLinkUrl = "";
		if ((publicFlag) && (ViewState["CHARACTER_ONLINE_STATUS"].ToString().Equals(ViCommConst.USER_WAITING.ToString()))) {
			sLinkUrl = sessionMan.GetNavigateUrl(
							sessionMan.root + sessionMan.sysType,
							Session.SessionID,
							string.Format("RequestIVP.aspx?castseq={0}&request={1}&menuid={2}&chgtype={3}",
										ViewState["CAST_USER_SEQ"].ToString(),
										ViCommConst.REQUEST_TALK,
										ViCommConst.MENU_VIDEO_WTALK,
										ViCommConst.CHARGE_TALK_WSHOT)
							);
		}
		return sLinkUrl;
	}
}
