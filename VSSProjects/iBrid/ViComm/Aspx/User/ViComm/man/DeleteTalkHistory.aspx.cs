/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ��b�����폜
--	Progaram ID		: DeleteTalkHistory
--
--  Creation Date	: 2010.02.19
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_DeleteTalkHistory:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sAll = iBridUtil.GetStringValue(Request.QueryString["all"]);
			string sRedirect;
			using (TalkHistory oTalkHistory = new TalkHistory()){
				if (sAll.Equals("1")) {
					oTalkHistory.DeleteTalkAllHistory(iBridUtil.GetStringValue(Request.QueryString["talkseq"]),sessionMan.userMan.userSeq);
					sRedirect = ViCommConst.SCR_DELETED_TALK_HISTORY_ALL;
				} else {
					oTalkHistory.DeleteTalkHistory(iBridUtil.GetStringValue(Request.QueryString["talkseq"]),sessionMan.userMan.userSeq);
					sRedirect = ViCommConst.SCR_DELETED_TALK_HISTORY;
				}
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + sRedirect));
		}
	}
}
