﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メッセージ書込詳細
--	Progaram ID		: ViewMessage
--
--  Creation Date	: 2010.07.05
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewMessage:MobileManPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.ControlList(Request,0,ActiveForm)) {
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListMessage.aspx"));
			}
		}
	}
}
