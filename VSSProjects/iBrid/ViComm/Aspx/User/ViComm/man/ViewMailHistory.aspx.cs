/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール履歴詳細表示
--	Progaram ID		: ViewMailHistory
--
--  Creation Date	: 2010.09.06
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.IO;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewMailHistory:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		bool bDownload = false;

		if (!IsPostBack) {
			bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);

			if (!sMovieSeq.Equals("") && (sPlayMovie.Equals("1") || bIsCheckOnly)) {
				bDownload = Download(sMovieSeq);
			}

			if (bDownload == false) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

				if (sessionMan.ControlList(Request,ViCommConst.INQUIRY_MAIL_HISTORY,ActiveForm)) {
					if (sessionMan.GetMailValue("TXRX_TYPE").Equals(ViCommConst.RX)) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailReadFlag(sessionMan.GetMailValue("MAIL_SEQ"));
						}
					}
				} else {
					NameValueCollection oQeury = new NameValueCollection(Request.QueryString);
					oQeury.Remove("pageno");
					oQeury.Remove("mailseq");
					oQeury.Remove("seekmode");
					UrlBuilder oUrlBuilder = new UrlBuilder(sessionMan.GetNavigateUrl("ListMailHistory.aspx"));

					foreach (string sKey in oQeury.Keys) {
						if (sKey != null) {
							oUrlBuilder.AddParameter(sKey,HttpUtility.UrlEncodeUnicode(oQeury[sKey]));
						}
					}
					RedirectToMobilePage(oUrlBuilder.ToString());
				}
			}
		} else {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdProtect"] != null) {
				cmdProtect_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sCastSeq,sCastCharNo;
		int iChargePoint = 0;
		bool bOk = true;

		sessionMan.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm);
		sessionMan.errorMessage = "";

		sCastSeq = sessionMan.GetMailValue("TX_USER_SEQ");
		sCastCharNo = sessionMan.GetMailValue("TX_USER_CHAR_NO");
		string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
		string sOpenedFlag = sessionMan.GetMailValue("ATTACHED_OBJ_OPEN_FLAG");
		string sObjType = sessionMan.GetMailValue("ATTACHED_OBJ_TYPE");
		string sTxRxType = sessionMan.GetMailValue("TXRX_TYPE");
		//送信ﾒｰﾙだったら課金処理はいらない
		if (sTxRxType.Equals(ViCommConst.TX)) {
			return;
		}

		if ((!sOpenedFlag.Equals("1") || sessionMan.site.attachedMailDupChargeFlag == 1) && sObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
			if (sessionMan.CheckPicMailBalance(sCastSeq,sCastCharNo,out iChargePoint) == false) {
				bOk = false;
			} else {
				using (WebUsedLog oLog = new WebUsedLog()) {
					oLog.CreateWebUsedReport(
							sessionMan.userMan.siteCd,
							sessionMan.userMan.userSeq,
							ViCommConst.CHARGE_MAIL_WITH_PIC,
							iChargePoint,
							sCastSeq,
							sCastCharNo,
							"");
				}
				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailAttaChedOpenFlag(sMailSeq);
					sessionMan.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","1");
				}
			}
		}

		if (bOk == false) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_ATTACHED_PIC));
		}
	}

	protected void cmdProtect_Click(object sender,EventArgs e) {
		sessionMan.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm);
		int iProtectFlag = int.Parse(sessionMan.GetMailValue("DEL_PROTECT_FLAG")) == 1 ? 0 : 1;
		string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sMailSeq,iProtectFlag);
			sessionMan.SetMailValue("DEL_PROTECT_FLAG",iProtectFlag.ToString());
		}
	}

	private bool Download(string pMovieSeq) {
		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;
		string sTxRxType = sessionMan.GetMailValue("TXRX_TYPE");
		string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

		long lSize;

		if (sTxRxType.Equals(ViCommConst.TX)) {
			lSize = ((ParseViComm)sessionMan.parseContainer.parseUser).GetMovie(ViCommConst.MAN,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		} else {
			lSize = ((ParseViComm)sessionMan.parseContainer.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		}

		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);

				string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
				string sOpenedFlag = sessionMan.GetMailValue("ATTACHED_OBJ_OPEN_FLAG");
				string sObjType = sessionMan.GetMailValue("ATTACHED_OBJ_TYPE");
				
				if(!MovieHelper.IsRangeRequest()){
					//受信時のみ課金処理を行う
					if (sTxRxType.Equals(ViCommConst.RX)) {
						if (!sOpenedFlag.Equals("1") || sessionMan.site.attachedMailDupChargeFlag == 1) {
							int iChargePoint;
							if (sessionMan.CheckMovieMailBalance(sCastSeq, sCastCharNo, out iChargePoint) == false) {
								sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
								RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_ATTACHED_MOVIE));
								return false;
							}
							if (bIsCheckOnly) return false;

							using (WebUsedLog oLog = new WebUsedLog()) {
								oLog.CreateWebUsedReport(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.CHARGE_MAIL_WITH_MOVIE,
										iChargePoint,
										sCastSeq,
										sCastCharNo,
										"");
							}
							using (MailBox oMailBox = new MailBox()) {
								oMailBox.UpdateMailAttaChedOpenFlag(sMailSeq);
							}
							sessionMan.SetMailValue("ATTACHED_OBJ_OPEN_FLAG", "1");
						}
					}
				}
				
				if (sTxRxType.Equals(ViCommConst.TX)) {
					MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.MAN);
				}else{
					MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				}
				return true;
			} else {
				return false;
			}
		}
	}
}
