/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい削除/復活
--	Progaram ID		: DeleteRequest
--
--  Creation Date	: 2012.06.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_DeleteRequest:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sRequestSeq = iBridUtil.GetStringValue(Request.QueryString["req_seq"]);

		if (string.IsNullOrEmpty(sRequestSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!this.checkAccess()) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			pnlConfirm.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sRequestSeq);
			}
		}
	}

	private bool checkAccess() {
		if (sessionMan.IsValidMask(sessionMan.userMan.userDefineMask,PwViCommConst.ManUserDefFlag.REQUEST_ADMIN)) {
			return true;
		}

		return false;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pRequestSeq) {
		string sResult = string.Empty;

		using (PwRequest oPwRequest = new PwRequest()) {
			sResult = oPwRequest.DeleteRequest(
				sessionMan.site.siteCd,
				pRequestSeq,
				ViCommConst.FLAG_ON_STR
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}
