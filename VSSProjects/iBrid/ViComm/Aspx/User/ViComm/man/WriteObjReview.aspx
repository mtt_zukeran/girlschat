<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WriteObjReview.aspx.cs" Inherits="ViComm_man_WriteObjReview" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlWrite" Runat="server">
			$PGM_HTML02;
			<tx:TextArea ID="txtCommentDoc" runat="server" Visible="true" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
			</tx:TextArea>
			$PGM_HTML03;
		</mobile:Panel>
		<mobile:Panel ID="pnlConfirm" Runat="server">
			$PGM_HTML04;
			<ibrid:iBMobileLabel ID="lblCommentDoc" runat="server" BreakAfter="true" ></ibrid:iBMobileLabel>
			$PGM_HTML05;
		</mobile:Panel>
		<mobile:Panel ID="pnlCompleteNoPt" Runat="server">
			$PGM_HTML07;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>