﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品着せ替えﾂｰﾙ購入履歴 一覧
--	Progaram ID		: ListProdThemeBuyHist
--
--  Creation Date	: 2011.07.04
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdThemeBuyHist : MobileProdManPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

        if (!IsPostBack)
        {
            sessionMan.ControlList(
                    this.Request,
                    ViCommConst.INQUIRY_PRODUCT_THEME_BUYING_HIST,
                    this.ActiveForm);

        }

    }
}
