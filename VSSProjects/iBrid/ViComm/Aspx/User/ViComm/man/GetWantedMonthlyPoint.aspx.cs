/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せ 月間山分けﾎﾟｲﾝﾄ付与
--	Progaram ID		: GetWantedMonthlyPoint
--
--  Creation Date	: 2013.06.13
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.IO;
using System.Drawing;
using System.Data;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_man_GetWantedMonthlyPoint:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sResult = string.Empty;
			string sSiteCd = sessionMan.site.siteCd;
			string sUserSeq = sessionMan.userMan.userSeq;
			string sUserCharNo = sessionMan.userMan.userCharNo;
			
			using (WantedCompSheetCnt oWantedCompSheetCnt = new WantedCompSheetCnt()) {
				oWantedCompSheetCnt.WantedMonthlyPointAcquired(sSiteCd,sUserSeq,sUserCharNo,out sResult);
			}

			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("result",sResult);
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_WANTED_MONTHLY_POINT_ACQUIRED_RESULT,oParam);
		}
	}
}