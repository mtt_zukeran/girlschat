/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 販売動画購入確認
--	Progaram ID		: ConfirmSaleMovieEx
--
--  Creation Date	: 2010.07.2
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ConfirmSaleMovieEx:MobileManPageBase {
	private const string ACTION_PURCHASE = "purchase";
	

	private string castSeq = null;
	private string movieSeq = null;
	private string action = null;
	private string filenm = null;
	private string usetype = null;


	protected void Page_Load(object sender,EventArgs e) {
		

		action = iBridUtil.GetStringValue(Request.QueryString["action"]);
		filenm = iBridUtil.GetStringValue(Request.QueryString["filenm"]);
		castSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
		movieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		usetype = iBridUtil.GetStringValue(Request.QueryString["usetype"]);

		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

		if (!this.IsPostBack) {
			bool bDupUsed = false;
			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				bDupUsed = oObjUsedHistory.GetOne(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.ATTACHED_MOVIE.ToString(),
									movieSeq);
			}

			// 購入済の場合は購入処理が不要なので、ﾀﾞｳﾝﾛｰﾄﾞ画面へ遷移する
			if (bDupUsed)
				this.RedirectToViewSaleMovieEx();

			if (this.action.Equals(ACTION_PURCHASE)) {
				PurchaseAction(this.castSeq,sCastCharNo,this.movieSeq);
				this.RedirectToViewSaleMovieEx();
			} else {
			}

			this.Response.Filter = this.sessionMan.InitScreen(Response.Filter,this.frmMain,this.Request,this.ViewState);
			sessionMan.ControlList(
				Request,
				ViCommConst.INQUIRY_PERSONAL_SALE_MOVIE,
				ActiveForm);
		}
	}

	private void PurchaseAction(string pCastSeq,string pCastCharNo,string pMovieSeq) {
		int iChargePoint = 0;
		// ポイント残のチェック。残ポイントが必要ポイントを下回る場合は警告画面へ遷移する
		if (!sessionMan.CheckDownloadMovieBalance(castSeq,pCastCharNo,movieSeq,out iChargePoint)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_DOWNLOAD_MOVIE));
			return;
		}

		using (WebUsedLog oLog = new WebUsedLog()) {
			oLog.CreateWebUsedReport(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.CHARGE_DOWNLOAD_MOVIE,
					iChargePoint,
					pCastSeq,
					pCastCharNo,
					"");
		}
		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			oObjUsedHistory.AccessBbsObj(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.ATTACHED_MOVIE.ToString(),
					pMovieSeq);
		}

		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
	}

	private void RedirectToViewSaleMovieEx() {
		System.Text.StringBuilder oUrlBuilder = new System.Text.StringBuilder();

		oUrlBuilder.Append("ViewSaleMovieEx.aspx?");
		oUrlBuilder.Append("action=").Append("&");
		oUrlBuilder.AppendFormat("filenm={0}",this.filenm).Append("&");
		oUrlBuilder.AppendFormat("castseq={0}",this.castSeq).Append("&");
		oUrlBuilder.AppendFormat("movieseq={0}",this.movieSeq).Append("&");
		oUrlBuilder.AppendFormat("usetype={0}",this.usetype);

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,oUrlBuilder.ToString()));
	}


}
