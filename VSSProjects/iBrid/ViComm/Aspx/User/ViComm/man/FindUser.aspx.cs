/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 検索条件指定
--	Progaram ID		: FindUser
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;

public partial class ViComm_man_FindUser:MobileManPageBase {
	private const string ONLINE_STATUS = "FindUserOnlineStatus";
	private const string NEWCAST_FLG = "FindUserNewCastFlg";
	private const string HANDLE_NAME = "FindUserHandleNm";
	private const string PLAY_MASK = "FindUserPlayMsk";
	private const string CAST_ITEM = "FindUserCastItem";
	private const string COMMENT = "FindUserComment";
	private const string AGE = "FindUserAge";

	private const string SEEKMODE_OKLIST_NONE = "1";
	private const string SEEKMODE_OKLIST_ONLY = "2";

	protected string SeekMode {
		get{return (string)this.ViewState["SeekMode"];}
		set{ this.ViewState["SeekMode"] = value;}
	}

	protected string sAreaAttrTypeSeq;
	protected string sAgeAttrTypeSeq;

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		int iIdx = 0;
		this.SeekMode = iBridUtil.GetStringValue(Request.QueryString["seekmode"]);
		
		using (ManageCompany oManageCompany = new ManageCompany()) {
			pnlAge.Visible = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY);
		}

		if (!IsPostBack) {
			#region ---------- Find ----------
			if (this.SeekMode != SEEKMODE_OKLIST_ONLY) {
				using (CastAttrType oCastAttrType = new CastAttrType()) {
					DataSet dsAttrType = oCastAttrType.GetList(sessionMan.site.siteCd);
					SelectionList lstCastItem;
					SelectionList lstCastItemMax;
					iBMobileLabel lblCastItem;
					iBMobileTextBox txtCastItem;
					Dictionary<string,string> castAttrList = (Dictionary<string,string>)Session[CAST_ITEM];

					foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
						if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
							lblCastItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblCastItem{0}",iIdx + 1)) as iBMobileLabel;
							lstCastItem = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}",iIdx + 1)) as SelectionList;
							txtCastItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtCastItem{0}",iIdx + 1)) as iBMobileTextBox;

							if (drAttrType["CAST_ATTR_TYPE_FIND_NM"].ToString().Equals("ﾊﾞｽﾄｻｲｽﾞ")) {
								lstCastItemMax = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}Max",iIdx + 1)) as SelectionList;
							} else {
								lstCastItemMax = null;
							}

							lblCastItem.Text = drAttrType["CAST_ATTR_TYPE_FIND_NM"].ToString();
							
							if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
								lstCastItem.Visible = false;

								// 入力値の復元を行います。
								if (castAttrList != null) {
									if (castAttrList.ContainsKey(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString())) {
										string val = castAttrList[drAttrType["CAST_ATTR_TYPE_SEQ"].ToString()];
										txtCastItem.Text = val;
									}
								}
							} else {
								txtCastItem.Visible = false;
								if (string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
									using (CastAttrTypeValue oAttrTypeValue = new CastAttrTypeValue()) {
										DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionMan.site.siteCd,drAttrType["CAST_ATTR_TYPE_SEQ"].ToString());
										foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
											if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
												if (drAttrType["CAST_ATTR_TYPE_FIND_NM"].ToString().Equals("ﾊﾞｽﾄｻｲｽﾞ")) {
													if (!drAttrTypeValue["CAST_ATTR_NM"].ToString().Equals("不明/その他")) {
														lstCastItem.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),drAttrTypeValue["CAST_ATTR_SEQ"].ToString()));
														lstCastItemMax.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),drAttrTypeValue["CAST_ATTR_SEQ"].ToString()));
													}
												} else {
													lstCastItem.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),drAttrTypeValue["CAST_ATTR_SEQ"].ToString()));
												}
											}
										}
									}
								} else {
									using (CodeDtl oCodeDtl = new CodeDtl()) {
										DataSet dsGroup = oCodeDtl.GetList(drAttrType["GROUPING_CATEGORY_CD"].ToString());
										foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
											lstCastItem.Items.Add(new MobileListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
										}
									}
								}
								if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_RADIO)) {
									lstCastItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
									if (castAttrList == null) {
										lstCastItem.SelectedIndex = 0;
									}
								}

								// 入力値の復元を行います。
								if (castAttrList != null) {
									if (castAttrList.ContainsKey(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString())) {
										string val = castAttrList[drAttrType["CAST_ATTR_TYPE_SEQ"].ToString()];
										if (val.IndexOf(",") >= 0) {
											string[] sValues = val.Split(',');
											foreach (MobileListItem item in lstCastItem.Items) {
												if (item.Value == sValues[0]) {
													item.Selected = true;
												}
											}
											foreach (MobileListItem itemMax in lstCastItemMax.Items) {
												if (itemMax.Value == sValues[1]) {
													itemMax.Selected = true;
												}
											}
										} else {
											foreach (MobileListItem item in lstCastItem.Items) {
												if (item.Value == val) {
													item.Selected = true;
												}
											}
										}
									}
								}
							}
							iIdx++;
						}
					}
				}
				for (int i = iIdx;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
					Panel pnlCastItem = (Panel)frmMain.FindControl(string.Format("pnlCastItem{0}",i + 1)) as Panel;
					pnlCastItem.Visible = false;
				}

				int iOnlineStatus;
				int.TryParse(iBridUtil.GetStringValue(Session[ONLINE_STATUS]),out iOnlineStatus);

				if (iOnlineStatus == ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING) {
					// ﾛｸﾞｲﾝ中
					rdoStatus.Items[0].Selected = false;
					rdoStatus.Items[1].Selected = true;
				} else if (iOnlineStatus == ViCommConst.SeekOnlineStatus.WAITING) {
					// 待機中
					rdoStatus.Items[0].Selected = false;
					rdoStatus.Items[2].Selected = true;
				} else {
					// 全て
					rdoStatus.Items[0].Selected = true;
				}

				if (iBridUtil.GetStringValue(Session[NEWCAST_FLG]) == "1") {
					// 新人フラグ
					chkNewCast.Items[0].Selected = true;
				}
				txtHandelNm.Text = iBridUtil.GetStringValue(Session[HANDLE_NAME]);
				txtComment.Text = iBridUtil.GetStringValue(Session[COMMENT]);

				// 年齢
				using (ManageCompany oManageCompany = new ManageCompany()) {
					if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
						foreach (MobileListItem item in lstAge.Items) {
							if (item.Value == iBridUtil.GetStringValue(Session[AGE])) {
								item.Selected = true;
								break;
							}
						}
					} else {
						pnlAge.Visible = false;
					}
				}

				this.rdoStatus.Items[0].Text = DisplayWordUtil.Replace(this.rdoStatus.Items[0].Text);
				this.lblComment.Text = DisplayWordUtil.ReplaceWordFindUserCommentHeader(this.lblComment.Text);
				this.lblComment2.Text = DisplayWordUtil.ReplaceWordFindUserCommentFooter(this.lblComment2.Text);

			} else {
				//OKﾘｽﾄ以外を非表示
				pnlOnline.Visible = false;
				pnlComment.Visible = false;
				for (int i = 0;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
					Panel pnlCastItem = (Panel)frmMain.FindControl(string.Format("pnlCastItem{0}",i + 1)) as Panel;
					pnlCastItem.Visible = false;
				}
			}

			if (this.SeekMode != SEEKMODE_OKLIST_NONE) {
				Int64 iPlayMask = 0;
				Int64.TryParse(iBridUtil.GetStringValue(Session[PLAY_MASK]),out iPlayMask);

				using (OkPlayList oPlayList = new OkPlayList()) {
					DataSet ds = oPlayList.GetList(sessionMan.site.siteCd);
					int iCnt = 0;
					if (ds.Tables[0].Rows.Count > 0) {
						foreach (DataRow dr in ds.Tables[0].Rows) {
							ViewState["OK" + iCnt.ToString()] = dr["OK_PLAY"].ToString();
							SelectionList chkOk = (SelectionList)pnlOkList.FindControl(string.Format("chkOK{0}",iCnt)) as SelectionList;
							chkOk.Items.Add(new MobileListItem(dr["OK_PLAY_NM"].ToString(),dr["OK_PLAY"].ToString()));
							chkOk.Visible = true;

							// 入力値を復元します。
							if ((iPlayMask & Int64.Parse(dr["OK_PLAY"].ToString())) > 0) {
								chkOk.Items[0].Selected = true;
							}


							iCnt++;
						}
						for (int i = iCnt;i < ViCommConst.MAX_PLAY_COUNT;i++) {
							SelectionList chkOk = (SelectionList)pnlOkList.FindControl(string.Format("chkOK{0}",i)) as SelectionList;
							chkOk.Visible = false;
						}
					} else {
						pnlOkList.Visible = false;
					}
				}
				this.lblOkList.Text = DisplayWordUtil.ReplaceOkList(this.lblOkList.Text);

			} else {
				//OKﾘｽﾄを非表示
				pnlOkList.Visible = false;
			}

			// 表示するピックアップキャストを取得する
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			switch (sessionMan.site.topPageSeekMode) {
				case ViCommConst.CAST_TOP_PICKUP:
					sessionMan.ControlList(
							Request,
							ViCommConst.INQUIRY_CAST_PICKUP,
							ActiveForm);
					break;
				case ViCommConst.CAST_TOP_TALK_MOVIE_PICKUP:
					GetPickupMovie();
					break;
				default:
					break;
			}
			#endregion
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void GetPickupMovie() {
		using (TalkMovie oTalkMovie = new TalkMovie()) {
			string sUserSeq,sUserCharNo;
			string sRecNum;
			if (oTalkMovie.GetRandomMovie(sessionMan.site.siteCd,out sUserSeq,out sUserCharNo,out sRecNum) == 0) {
				return;
			}
			if (sessionMan.SetCastDataSetByUserSeq(sUserSeq,sUserCharNo,int.Parse(sRecNum))) {
				sessionMan.seekMode = ViCommConst.INQUIRY_TALK_MOVIE_PICKUP.ToString();
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sHandleNm = string.Empty;
		string sComment = string.Empty;
		string sAgeFrom = string.Empty;
		string sAgeTo = string.Empty;
		string sUrl = string.Empty;
		int i;

		int iOnlineStatus = 0;
		int iNewCastFlag = 0;

		if (pnlOnline.Visible) {
			sHandleNm = System.Web.HttpUtility.UrlEncode(txtHandelNm.Text,enc);
		}
		if (pnlComment.Visible) {
			sComment = System.Web.HttpUtility.UrlEncode(txtComment.Text,enc);
		}
		if (pnlAge.Visible) {
			sAgeFrom = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[0]);
			sAgeTo = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[1]);
		}

		if (pnlOnline.Visible) {
			if (rdoStatus.Items[1].Selected) {
				iOnlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
			} else if (rdoStatus.Items[2].Selected) {
				iOnlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
			}
			if (chkNewCast.SelectedIndex != -1) {
				iNewCastFlag = 1;
			}
		}

		SelectionList chkOk;
		Int64 iPlayMask = 0;

		if (pnlOkList.Visible) {
			for (i = 0;i < ViCommConst.MAX_PLAY_COUNT;i++) {
				chkOk = (SelectionList)pnlOkList.FindControl(string.Format("chkOK{0}",i)) as SelectionList;
				if ((chkOk.Visible) && (chkOk.SelectedIndex != -1)) {
					iPlayMask += Int64.Parse(chkOk.Items[chkOk.SelectedIndex].Value);
				}
			}
		}

		sUrl = string.Format(
						"ListFindResult.aspx?category={0}&online={1}&handle={2}&play={3}&comment={4}&newcast={5}&agefrom={6}&ageto={7}",
						sessionMan.currentCategoryIndex,
						iOnlineStatus,
						sHandleNm,
						iPlayMask.ToString(),
						sComment,
						iNewCastFlag,
						sAgeFrom,
						sAgeTo
						);

		// 選択した値をセッションにいれておきます。 
		if (pnlOnline.Visible) {
			Session[ONLINE_STATUS] = iOnlineStatus;
			Session[NEWCAST_FLG] = iNewCastFlag;
			Session[HANDLE_NAME] = txtHandelNm.Text;
		}
		if (pnlAge.Visible) {
			Session[AGE] = lstAge.Selection.Value;
		}
		if (pnlOkList.Visible) {
			Session[PLAY_MASK] = iPlayMask;
		}
		if (pnlComment.Visible) {
			Session[COMMENT] = txtComment.Text;
		}

		SelectionList lstCastItem;
		SelectionList lstCastItemMax;
		iBMobileTextBox txtCastItem;
		Panel pnlCastItem;
		string sItem;

		i = 1;
		string sGroupingFlag;
		Dictionary<string,string> castAttrList = new Dictionary<string,string>();
		using (CastAttrType oCastAttrType = new CastAttrType()) {

			DataSet dsAttrType = oCastAttrType.GetList(sessionMan.site.siteCd);

			foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
				if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
					pnlCastItem = (Panel)frmMain.FindControl(string.Format("pnlCastItem{0}",i)) as Panel;

					string sNotEq = iBridUtil.GetStringValue(this.Request.Params["noteq{0:D2}"]);

					if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtCastItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtCastItem{0}",i)) as iBMobileTextBox;

						if ((pnlCastItem != null) && (txtCastItem != null) && (pnlCastItem.Visible)) {
							sUrl = sUrl + string.Format(
												"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}",
												i,
												System.Web.HttpUtility.UrlEncode(txtCastItem.Text,enc),
												drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),
												"0",
												"1",
												sNotEq);
							// 入力値復元用の値保存 
							castAttrList.Add(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),txtCastItem.Text);
						}
					} else {
						lstCastItem = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}",i)) as SelectionList;
						
						if (drAttrType["CAST_ATTR_TYPE_FIND_NM"].ToString().Equals("ﾊﾞｽﾄｻｲｽﾞ")) {
							lstCastItemMax = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}Max",i)) as SelectionList;
						} else {
							lstCastItemMax = null;
						}
						
						sGroupingFlag = "";
						if (!string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
							sGroupingFlag = "1";
						}
						if ((pnlCastItem != null) && (lstCastItem != null) && (pnlCastItem.Visible)) {
							if (iBridUtil.GetStringValue(this.Request.Form[string.Format("itemSeqRangeSearchFlag{0}",i)]).Equals(ViCommConst.FLAG_ON_STR)) {
								sItem = string.Empty;
								if (lstCastItem.Items[lstCastItem.SelectedIndex].Value.Equals("*") && lstCastItemMax.Items[lstCastItemMax.SelectedIndex].Value.Equals("*")) {
									sItem = "*";
								} else {
									sItem = string.Format("{0},{1}",lstCastItem.Items[lstCastItem.SelectedIndex].Value,lstCastItemMax.Items[lstCastItemMax.SelectedIndex].Value).Replace("*",string.Empty);
								}
								
								sUrl = sUrl + string.Format(
													"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}&range{0:D2}={6}&seqrange{0:D2}={7}",
													i,
													sItem,
													drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),
													sGroupingFlag,
													"0",
													sNotEq,
													"0",
													"1");
								
								// 入力値復元用の値保存 
								castAttrList.Add(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),string.Format("{0},{1}",lstCastItem.Items[lstCastItem.SelectedIndex].Value,lstCastItemMax.Items[lstCastItemMax.SelectedIndex].Value));
							} else {
								sUrl = sUrl + string.Format(
													"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}",
													i,
													lstCastItem.Items[lstCastItem.SelectedIndex].Value,
													drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),
													sGroupingFlag,
													"0",
													sNotEq);

								// 入力値復元用の値保存 
								castAttrList.Add(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),lstCastItem.Items[lstCastItem.SelectedIndex].Value);
							}
						}
					}
					i++;
				}
			}
		}
		Session[CAST_ITEM] = castAttrList;
		this.AppendSearchCondtion(ref sUrl,i);
			
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
	}

	protected virtual void AppendSearchCondtion(ref string pUrl,int pIndex) {
		// 何もしない。aspxで処理を追記する場合に使用 
	}

}
