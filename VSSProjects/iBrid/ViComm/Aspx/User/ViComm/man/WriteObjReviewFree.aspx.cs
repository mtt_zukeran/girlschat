/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 無料画像・動画レビュー書込
--	Progaram ID		: WriteObjReviewFree
--
--  Creation Date	: 2013.09.23
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteObjReviewFree:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sObjSeq = iBridUtil.GetStringValue(Request.Params["objseq"]);
		string sUsedDate = string.Empty;

		if (string.IsNullOrEmpty(sObjSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("objseq",sObjSeq);
		oParam.Add("listpageno",iBridUtil.GetStringValue(Request.Params["listpageno"]));
		oParam.Add("attrtypeseq",iBridUtil.GetStringValue(Request.Params["attrtypeseq"]));
		oParam.Add("attrseq",iBridUtil.GetStringValue(Request.Params["attrseq"]));
		oParam.Add("ranktype",iBridUtil.GetStringValue(Request.Params["ranktype"]));
		oParam.Add("listloginid",iBridUtil.GetStringValue(Request.Params["listloginid"]));
		oParam.Add("listtype",iBridUtil.GetStringValue(Request.Params["listtype"]));

		//有料チェック
		using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
			if (oCastBbsObj.CheckExistsByObjSeq(sObjSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		}

		//レビュー済チェック
		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			if (oObjReviewHistory.CheckExistsByObjSeq(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sObjSeq
				)
			) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_WRITE_OBJ_REVIEW_ERROR02,oParam);
			}
		}

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e,sObjSeq,sUsedDate);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		int iGoodStar;
		bool bGoodStar = int.TryParse(iBridUtil.GetStringValue(Request.Form["good_star"]),out iGoodStar);

		if (bGoodStar == false) {
			sessionMan.errorMessage = "評価を選択してください";
			return;
		}

		if (!txtCommentDoc.Text.Equals(string.Empty)) {
			string sNGWord = string.Empty;

			if (txtCommentDoc.Text.Length > 500) {
				sessionMan.errorMessage = "ｺﾒﾝﾄが長すぎます";
				return;
			}

			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}

			if (!sessionMan.ngWord.VaidateDoc(txtCommentDoc.Text,out sNGWord)) {
				sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		ViewState["GOOD_STAR"] = iGoodStar.ToString();
		ViewState["COMMENT_DOC"] = txtCommentDoc.Text;

		lblCommentDoc.Text = HttpUtility.HtmlEncode(txtCommentDoc.Text).Replace(System.Environment.NewLine,"<br>");
		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
		pnlComplete.Visible = false;
	}

	protected void cmdTx_Click(object sender,EventArgs e,string sObjSeq,string sUsedDate) {
		sessionMan.errorMessage = string.Empty;
		int iGoodStar;
		bool bGoodStar = int.TryParse(iBridUtil.GetStringValue(ViewState["GOOD_STAR"]),out iGoodStar);
		string sCommentDoc = iBridUtil.GetStringValue(ViewState["COMMENT_DOC"]);
		string sResult = string.Empty;

		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			sResult = oObjReviewHistory.WriteObjReview(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sObjSeq,
				iGoodStar,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sCommentDoc)),
				0
			);
		}

		if (sResult.Equals(PwViCommConst.WriteObjReviewResult.RESULT_OK)) {
			pnlWrite.Visible = false;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
		pnlComplete.Visible = false;
	}

	protected virtual void RestrictDocLength(ref string doc) {
		if (doc.Length > 1000) {
			doc = SysPrograms.Substring(doc,1000);
		}
	}
}
