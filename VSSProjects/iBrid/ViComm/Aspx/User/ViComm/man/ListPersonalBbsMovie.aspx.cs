/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ʌf������ꗗ
--	Progaram ID		: ListPersonalBbsMovie
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;

public partial class ViComm_man_ListPersonalBbsMovie:MobileObjManPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			bool bOnlyUsed = iBridUtil.GetStringValue(Request.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
			string sScreenId = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);
			string sAttrSeq = iBridUtil.GetStringValue(this.Request.QueryString["attrseq"]);
			
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo)) {
				sessionMan.ControlList(
					Request,
					ulong.Parse(iBridUtil.GetStringValue(Request.QueryString["seekmode"])),
					ActiveForm);
			}

			if (sessionMan.site.bbsMovieAttrFlag != ViCommConst.FLAG_ON || IsAvailableService(ViCommConst.RELEASE_DISABLE_PERSONAL_ATTR_COMBO_BOX,2)) {
				pnlSearchList.Visible = false;
			} else {
				CreateObjAttrComboBox(sessionMan.GetCastValue("USER_SEQ"),sCastCharNo,string.Empty,ViCommConst.BbsObjType.MOVIE,ViCommConst.ATTACHED_BBS.ToString(),true,false,false,bOnlyUsed);
			}

			if (!string.IsNullOrEmpty(sScreenId) && !string.IsNullOrEmpty(sAttrSeq)) {
				this.pnlSearchList.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;

		string[] selectedValueList = selectedValue.Split(',');

		string sAddQuery = string.Format("&attrtypeseq={0}&attrseq={1}",selectedValueList[0],selectedValueList[1]);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
												   "ListPersonalBbsMovie.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&attrtypeseq=[0-9]*|&attrseq=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}