/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プリペード登録前トップ
--	Progaram ID		: PrepaidUserTop
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Net;
using System.IO;
using System.Threading;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PrepaidUserTop:MobileManPageBase {

	private string userSeq;
	private string userStatus;
	private string loginResult;
	private string idPwError = "";
	private string curPoint = "";
	private string money = "";
	private string authCd = "";
	private string cardType = "";
	private string ipapUrl = "";
	private string clientId = "";
	private string utn = "";
	private int callFromRegistProcessFlag = 0;

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		ParseHTML oParseMan = sessionMan.parseContainer;

		string sId = iBridUtil.GetStringValue(Request.QueryString["id"]);
		string sPassword = iBridUtil.GetStringValue(Request.QueryString["pw"]);
		string sUtn = iBridUtil.GetStringValue(Request.QueryString["utn"]);

		if ((sId.Equals("")) && (sPassword.Equals("")) && (sUtn.Equals(""))) {
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_UTN;
			callFromRegistProcessFlag = 0;
		}

		if (!IsPostBack) {
			// ﾌﾟﾘﾍﾟｰﾄﾞ新規登録時に登録ﾒｰﾙを受信するとMailParse内のSPより実行される。
			if ((!sId.Equals("")) && (!sPassword.Equals("")) && (!sUtn.Equals(""))) {
				txtLoginId.Text = sId;
				txtPassword.Text = sPassword;
				utn = sUtn;
				callFromRegistProcessFlag = 1;
				cmdSubmit_Click(sender,e);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (callFromRegistProcessFlag == 0) {
			utn = Mobile.GetUtn(sessionMan.carrier,Request);
		}
		bool bOk = false;

		lblErrorMessage.Text = string.Empty;
		sessionMan.userMan.prepaidId = string.Empty;
		sessionMan.userMan.prepaidPw = string.Empty;

		bOk = CheckPrepaid("09000000000","dummy0@marii.tv","00000000");

		if (!bOk) {
			if (idPwError.Equals("1")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PREPAID_LOGIN);
			} else {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PREPAID_NO_BALANCE);
			}
		} else {
			sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_UTN,utn,"","",Session.SessionID,out userSeq,out userStatus,out loginResult);
			if (loginResult.Equals("0")) {
				if (cardType.Equals("1") || (callFromRegistProcessFlag != 0)) {
					SettlePrepaid();
				} else {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_SAMPLE_PREPAID_NG);
				}
			} else {
				if (utn.Equals("") == false) {
					sessionMan.userMan.prepaidId = txtLoginId.Text;
					sessionMan.userMan.prepaidPw = txtPassword.Text;
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"RegistUserRequestByTermId.aspx?utn=" + utn));
				} else {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PREPAID_NG_TERM_ID);
				}
			}
		}
	}

	private bool CheckPrepaid(string pTel,string pEmailAddr,string pLoginId) {
		bool bOk = false;
		string sCheckUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_SITE01,ViCommConst.SETTLE_PREPAID)) {
				ipapUrl = oSiteSettle.settleUrl;
				clientId = oSiteSettle.cpIdNo;
				sCheckUrl = string.Format("{0}/AccountCheck.aspx?serviceid=100&clientip={1}&telno={2}&email={3}&sendid={4}&card_id={5}&card_pass={6}",
											ipapUrl,
											clientId,
											pTel,
											pEmailAddr,
											pLoginId,
											txtLoginId.Text,
											txtPassword.Text);

			}
		}

		money = "";
		curPoint = "";
		authCd = "";
		cardType = "";

		if (sCheckUrl.Equals("") == false) {
			string sResult;
			bOk = TransPrepaid(sCheckUrl,out sResult);
			if (bOk) {
				string[] sItem = sResult.Split('\n');
				for (int i = 0;i < sItem.Length;i++) {

					if (sItem[i].StartsWith("money")) {
						string[] sItemMoney = sItem[i].Split('=');
						money = sItemMoney[1];

					} else if (sItem[i].StartsWith("point")) {
						string[] sItemPoint = sItem[i].Split('=');
						curPoint = sItemPoint[1];

					} else if (sItem[i].StartsWith("card_type")) {
						string[] sCardType = sItem[i].Split('=');
						cardType = sCardType[1];

					} else if (sItem[i].StartsWith("auth_code")) {
						string[] sItemAuth = sItem[i].Split('=');
						authCd = sItemAuth[1];
					}
				}
			} else {
				string[] sItem = sResult.Split(':');
				if (sItem[0].Equals("552") || sItem[0].Equals("553")) {
					idPwError = "1";
				}
			}
		}
		return bOk;
	}

	private void SettlePrepaid() {
		bool bOk = false;
		string sResult;

		string sSid = "";

		using (SettleLog oLog = new SettleLog()) {
			oLog.LogSettleRequest(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				ViCommConst.SETTLE_CORP_SITE01,
				ViCommConst.SETTLE_PREPAID,
				ViCommConst.SETTLE_STAT_SETTLE_NOW,
				0,
				0,
				txtLoginId.Text,
				"",
				out sSid);
		}

		string sTel = sessionMan.userMan.tel;
		if (sTel.Equals(string.Empty)) {
			sTel = "09000000000";
		}
		if (CheckPrepaid(sTel,sessionMan.userMan.emailAddr,sSid)) {
			if ((!money.Equals("")) && (!curPoint.Equals("")) && (!authCd.Equals(""))) {

				string sSettleUrl = string.Format("{0}/SettleCheck.aspx?auth_code={1}&money={2}&point={3}&resultcode=200",ipapUrl,authCd,money,curPoint);
				bOk = TransPrepaid(sSettleUrl,out sResult);
			} else {
				bOk = false;
			}

			if (bOk) {
				bool bNewSignal;
				using (NamedAutoResetEvent oSignal = new NamedAutoResetEvent(false,"signal-" + sSid,out bNewSignal)) {
					string sCardSettleUrl = string.Format("{0}/CardSettle.aspx?auth_code={1}",ipapUrl,authCd);
					TransPrepaid(sCardSettleUrl,out sResult);
					oSignal.WaitOne(10000,false);
				}
			}
			if (callFromRegistProcessFlag == 0) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.site.userTopId));
			} else {
				Response.ContentType = "text/plain";
				Response.Charset = "shift_jis";
				Response.Write("result=0");
				Response.End();
			}
		} else {
			if (idPwError.Equals("1")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PREPAID_LOGIN);
			} else {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PREPAID_NO_BALANCE);
			}
		}
	}

	private bool TransPrepaid(string pUrl,out string pRes) {
		pRes = "";
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 20000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				pRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				pRes = pRes.Replace("\r","");
				return pRes.StartsWith("200");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransPrepaid",pUrl);
			return false;
		}
	}
}
