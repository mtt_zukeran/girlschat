/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールアドレス状態設定
--	Progaram ID		: SetupRxMailStatus
--
--  Creation Date	: 2013.06.11
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_SetupRxMailStatus:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			} else if (Request.Params["cmdSubmitEx"] != null) {
				cmdSubmitEx_Click(sender,e);
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {	
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_NG_ADDRESS_RESET_MAIL");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sessionMan.site.siteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sessionMan.userMan.userSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sessionMan.userMan.userCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected void cmdSubmitEx_Click(object sender,EventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_NG_ADDRESS_RESET_MAIL_EX");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sessionMan.site.siteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sessionMan.userMan.userSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sessionMan.userMan.userCharNo);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
