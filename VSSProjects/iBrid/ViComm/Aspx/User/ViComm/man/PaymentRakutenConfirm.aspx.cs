/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 楽天あんしん支払い確認
--	Progaram ID		: PaymentRakutenConfirm
--
--  Creation Date	: 2010.08.24
--  Creater			: K.Itoh
--
**************************************************************************/
// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain
  yyyy/mm/dd	XXXXXXXXX
-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_PaymentRakutenConfirm : MobileManPageBase {

	private ViComm.Rakuten.Checkout rakutenCheckout = new ViComm.Rakuten.Checkout();

	protected decimal SalesAmt {
		get { return (decimal)this.ViewState["SalesAmt"]; }
		set { this.ViewState["SalesAmt"] = value; }
	}

	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		
		if(!this.IsPostBack){
			// Queryの解析
			this.ParseQuery();

			using (Pack oPack = new Pack())
			using (SettleLog oSettleLog = new SettleLog()) 
			using (SiteSettle oSiteSettle = new SiteSettle()) {

				// パック情報の取得
				if(!oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_RAKUTEN,this.SalesAmt.ToString(),sessionMan.userMan.userSeq)){
					string sErrorMessage = string.Format("T_PACKから該当するデータが取得できませんでした。PathAndQuery:{0}",this.Request.Url.PathAndQuery);
					throw new ApplicationException(sErrorMessage);
				}
				// パック情報→送信用商品情報の変換
				ViComm.Rakuten.OrderItemEntity oOrderItem = this.Convert(oPack);
				oOrderItem.ItemSubId = sessionMan.userMan.userSeq;
				this.lblSalesAmt.Text = oPack.remarks;


				string sCartId = null;
				oSettleLog.LogSettleRequest(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.SETTLE_CORP_RAKUTEN,
						ViCommConst.SETTLE_RAKUTEN,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						oPack.salesAmt,
						oPack.exPoint,
						string.Empty,
						string.Empty,
						out sCartId);
				
				// SITE_SETTLEの取得
				if (!oSiteSettle.GetOne(sessionMan.site.siteCd, ViCommConst.SETTLE_CORP_RAKUTEN, ViCommConst.SETTLE_RAKUTEN)) {
					string sErrorMessage = string.Format("T_SITE_SETTLEから該当するデータが取得できませんでした。PathAndQuery:{0}", this.Request.Url.PathAndQuery);
					throw new ApplicationException(sErrorMessage);
				}
				
				// BackUrl生成
				string sBackUrl = string.Format(oSiteSettle.backUrl, sessionMan.site.url, sessionMan.userMan.loginId, sessionMan.userMan.loginPassword);

				// 商品情報送信用のHTMLフォームを作成
				this.ctrlCheckoutForm.Text = this.rakutenCheckout.GenerateCheckoutHtmlFormItem(oSiteSettle.cpIdNo,oSiteSettle.cpPassword,sCartId,oOrderItem,sBackUrl,string.Empty);
				this.frmMain.Action = oSiteSettle.settleUrl; 

			}
		}
	}
	
	/// <summary>
	/// パック情報を楽天決済 送信用商品情報に変換する
	/// </summary>
	/// <param name="pPack">パック情報</param>
	/// <returns>楽天決済 送信用商品情報</returns>
	private ViComm.Rakuten.OrderItemEntity Convert(Pack pPack){
		ViComm.Rakuten.OrderItemEntity oOrderItem = new ViComm.Rakuten.OrderItemEntity();
		
		oOrderItem.ItemId = ViComm.Rakuten.RakutenUtil.Convert2ItemId(pPack.siteCd,pPack.settleType,pPack.salesAmt);
		oOrderItem.ItemName = pPack.remarks;
		oOrderItem.ItemFee = pPack.salesAmt;
		oOrderItem.ItemNumbers = 1;// 固定
		
		return oOrderItem;
	}
	
	/// <summary>
	/// GETパラメータをパースする。
	/// </summary>
	private void ParseQuery(){
		decimal dTmpSalesAmt;
		if (!decimal.TryParse(this.Request.QueryString["sales_amt"], out dTmpSalesAmt)) {
			string sErrorMessage = string.Format("パラメータの値が不正です。PathAndQuery:{0}", this.Request.Url.PathAndQuery);
			throw new ApplicationException(sErrorMessage);
		}
		this.SalesAmt = dTmpSalesAmt;
	}
}
