/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ү���ވꗗ
--	Progaram ID		: ListWrittenMessage 
--
--  Creation Date	: 2010.06.24
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListWrittenMessage : MobileManPageBase {
	

	protected void Page_Load(object sender, EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);

		if (!IsPostBack) {
			if (!sessionMan.logined) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			} else {
				sessionMan.ControlList(Request, 0, ActiveForm);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> oMessageSeqList = new List<string>();

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.StartsWith("checkbox")) {
				oMessageSeqList.Add(Request.Form[sKey]);
			}
		}
		if (oMessageSeqList.Count > 0) {
			using (Message oMessage = new Message()) {
				oMessage.Delete(oMessageSeqList.ToArray());
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   "DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_MAN_MESSAGE + "&msg_delete_count=" + oMessageSeqList.Count));
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   "ListWrittenMessage.aspx?" + Request.QueryString));
		}
	}
}
