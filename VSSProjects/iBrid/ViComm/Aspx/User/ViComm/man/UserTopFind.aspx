<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserTopFind.aspx.cs" Inherits="ViComm_man_UserTopFind" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:SelectionList ID="lstWaitType" Runat="server"></mobile:SelectionList>
		<mobile:Panel ID="pnlCastItem1" Runat="server">
			<mobile:SelectionList ID="lstCastItem1" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem2" Runat="server">
			<mobile:SelectionList ID="lstCastItem2" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem3" Runat="server">
			<mobile:SelectionList ID="lstCastItem3" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem4" Runat="server">
			<mobile:SelectionList ID="lstCastItem4" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem5" Runat="server">
			<mobile:SelectionList ID="lstCastItem5" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem6" Runat="server">
			<mobile:SelectionList ID="lstCastItem6" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem7" Runat="server">
			<mobile:SelectionList ID="lstCastItem7" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem8" Runat="server">
			<mobile:SelectionList ID="lstCastItem8" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem9" Runat="server">
			<mobile:SelectionList ID="lstCastItem9" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem10" Runat="server">
			<mobile:SelectionList ID="lstCastItem10" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem11" Runat="server">
			<mobile:SelectionList ID="lstCastItem11" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem12" Runat="server">
			<mobile:SelectionList ID="lstCastItem12" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem13" Runat="server">
			<mobile:SelectionList ID="lstCastItem13" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem14" Runat="server">
			<mobile:SelectionList ID="lstCastItem14" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem15" Runat="server">
			<mobile:SelectionList ID="lstCastItem15" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem16" Runat="server">
			<mobile:SelectionList ID="lstCastItem16" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem17" Runat="server">
			<mobile:SelectionList ID="lstCastItem17" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem18" Runat="server">
			<mobile:SelectionList ID="lstCastItem18" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem19" Runat="server">
			<mobile:SelectionList ID="lstCastItem19" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem20" Runat="server">
			<mobile:SelectionList ID="lstCastItem20" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:SelectionList ID="chkMovie" Runat="server" SelectType="CheckBox" Alignment="Left">
			<Item Text="$PGM_HTML02;" Value="1" Selected="false" />
		</mobile:SelectionList>
		<mobile:SelectionList ID="chkNewCast" Runat="server" SelectType="CheckBox" Alignment="Left">
			<Item Text="$PGM_HTML03;" Value="1" Selected="false" />
		</mobile:SelectionList>
		$PGM_HTML06;
		$DATASET_LOOP_START0;
		$PGM_HTML07;
		$DATASET_LOOP_END0;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
