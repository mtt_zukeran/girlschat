/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 未送信メール編集リダイレクト
--	Progaram ID		: RedirectDraftMail.aspx
--
--  Creation Date	: 2013.09.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RedirectDraftMail:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		string sDraftMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["draftmailseq"]);
		
		if (string.IsNullOrEmpty(sDraftMailSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
		
		DataSet oDataSet = null;
		
		string sSiteCd = sessionMan.site.siteCd;
		string sTxUserSeq = sessionMan.userMan.userSeq;
		string sTxUserCharNo = sessionMan.userMan.userCharNo;
		
		using (CastDraftMail oCastDraftMail = new CastDraftMail(sessionObj)) {
			oDataSet = oCastDraftMail.GetOne(sSiteCd,sTxUserSeq,sTxUserCharNo,sDraftMailSeq);
		}
		
		if (oDataSet.Tables[0].Rows.Count == 0) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		string sMailDataSeq = iBridUtil.GetStringValue(sessionMan.userMan.NextMailDataSeq());
		sessionMan.userMan.mailData.Add(sMailDataSeq,new UserManMailInfo());
		
		bool bReturnMailFlag = oDataSet.Tables[0].Rows[0]["RETURN_MAIL_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR) ? true : false;
		
		sessionMan.userMan.mailData[sMailDataSeq].draftMailSeq = sDraftMailSeq;
		sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
		sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		sessionMan.userMan.mailData[sMailDataSeq].rxUserNm = oDataSet.Tables[0].Rows[0]["HANDLE_NM"].ToString();
		sessionMan.userMan.mailData[sMailDataSeq].mailTitle = oDataSet.Tables[0].Rows[0]["MAIL_TITLE"].ToString();
		sessionMan.userMan.mailData[sMailDataSeq].mailDoc = GetDraftMailDoc(oDataSet);
		sessionMan.userMan.mailData[sMailDataSeq].mailAttachType = oDataSet.Tables[0].Rows[0]["ATTACHED_OBJ_TYPE"].ToString();
		sessionMan.userMan.mailData[sMailDataSeq].isReturnMail = bReturnMailFlag;
		sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq = oDataSet.Tables[0].Rows[0]["RETURN_ORG_MAIL_SEQ"].ToString();
		sessionMan.userMan.mailData[sMailDataSeq].rxLoginId = oDataSet.Tables[0].Rows[0]["LOGIN_ID"].ToString();

		string sRedirectUrl = string.Empty;
		if (bReturnMailFlag) {
			sRedirectUrl = string.Format("ReturnMail.aspx?data={0}&direct=1&mailseq={1}",sMailDataSeq,sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq);
			if (iBridUtil.GetStringValue(this.Request.QueryString["chat"]).Equals(ViCommConst.FLAG_ON_STR)) {
				sRedirectUrl = sRedirectUrl + "&scrid=01&stay=1#btm";
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sRedirectUrl));
		} else {
			sRedirectUrl = string.Format("TxMail.aspx?data={0}",sMailDataSeq);
			if (iBridUtil.GetStringValue(this.Request.QueryString["chat"]).Equals(ViCommConst.FLAG_ON_STR)) {
				sRedirectUrl = sRedirectUrl + string.Format("&loginid={0}&userecno=1&scrid=01&stay=1#btm",sessionMan.userMan.mailData[sMailDataSeq].rxLoginId);
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sRedirectUrl));
		}
	}

	private string GetDraftMailDoc(DataSet pDS) {
		string sValue = "";

		sValue = pDS.Tables[0].Rows[0]["MAIL_DOC1"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC2"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC3"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC4"].ToString() + pDS.Tables[0].Rows[0]["MAIL_DOC5"].ToString();

		return sValue;
	}
}
