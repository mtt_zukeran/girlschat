/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ バトルTOP コンプ状況によるページ切替
--	Progaram ID		: DivideGameBattleTop
--
--  Creation Date	: 2012.09.04
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_DivideGameBattleTop:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sDisplayDay = string.Empty;
		string sRedirectUrl = string.Empty;
		
		sDisplayDay = this.GetAimDisplayDay(sSiteCd,sUserSeq,sUserCharNo);
		
		sDisplayDay = HttpUtility.UrlEncode(sDisplayDay);
		
		if(!string.IsNullOrEmpty(sDisplayDay)) {
			sRedirectUrl = string.Format("ListGameTreasureAttr.aspx?scrid=01&display_day={0}",sDisplayDay);
		} else {
			sRedirectUrl = "ListGameCharacterFavoriteBattle.aspx?treasure_pic=1&sort=5&drows=3";
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
	}
	
	public string GetAimDisplayDay(string sSiteCd,string sUserSeq,string sUserCharNo) {
		string sDisplayDay = string.Empty;
		string sResult = string.Empty;
		
		using (ManTreasure oManTreasure = new ManTreasure()) {
			oManTreasure.GetAimDisplayDay(sSiteCd,sUserSeq,sUserCharNo,out sDisplayDay,out sResult);
		}
		
		return sDisplayDay;
	}
}
