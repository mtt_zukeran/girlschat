/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入りグループ一覧
--	Progaram ID		: ListFavoritGroup
--
--  Creation Date	: 2012.10.02
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListFavoritGroup:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_FAVORIT_GROUP,this.ActiveForm);
		}
	}
}
