/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板画像一覧
--	Progaram ID		: ListBbsPic
--
--  Creation Date	: 2009.08.25
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2010/05/25  Nakano     画像の属性による検索機能を追加しました。

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using ViComm;
using iBridCommLib;

public partial class ViComm_man_ListBbsPic:MobileObjManPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_PIC,ActiveForm);
			bool bOnlyUsed = iBridUtil.GetStringValue(Request.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
			string sScreenId = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);
			string sAttrSeq = iBridUtil.GetStringValue(this.Request.QueryString["attrseq"]);

			if (sessionMan.site.bbsPicAttrFlag != ViCommConst.FLAG_ON) {
				pnlSearchList.Visible = false;
			} else if (IsAvailableService(ViCommConst.RELEASE_DISABLE_ATTR_COMBO_BOX_CNT,2)) {
				CreateCastPicAttrComboBox();
			} else {
				// カテゴリSEQ取得
				int iCurrentCategoryIndex;
				string sCategorySeq = string.Empty;
				int.TryParse(iBridUtil.GetStringValue(Request.QueryString["category"]),out iCurrentCategoryIndex);
				if (iCurrentCategoryIndex > 0) {
					sCategorySeq = ((ActCategory)sessionMan.categoryList[iCurrentCategoryIndex]).actCategorySeq;
				}
				CreateObjAttrComboBox(string.Empty,string.Empty,sCategorySeq,ViCommConst.BbsObjType.PIC,ViCommConst.ATTACHED_BBS.ToString(),false,false,true,bOnlyUsed);

			}

			if (!string.IsNullOrEmpty(sScreenId) && !string.IsNullOrEmpty(sAttrSeq)) {
				this.pnlSearchList.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
		string[] selectedValueList = selectedValue.Split(',');
		string sAddQuery = string.Format("&attrtypeseq={0}&attrseq={1}",selectedValueList[0],selectedValueList[1]);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
												   "ListBbsPic.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&attrtypeseq=[0-9]*|&attrseq=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}