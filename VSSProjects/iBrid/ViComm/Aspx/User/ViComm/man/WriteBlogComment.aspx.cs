/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログコメント投稿
--	Progaram ID		: WriteBlogComment
--
--  Creation Date	: 2012.12.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_WriteBlogComment:MobileBlogManBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["blog_article_seq"]))) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		DataSet dsBlogArticle;

		if (!sessionMan.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE,ActiveForm,out dsBlogArticle)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			if (sessionMan.logined == false) {		
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {	
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {	
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}	
				return;	
			}

			bool bBlogNgUser = false;
			string sCastUserSeq = iBridUtil.GetStringValue(dsBlogArticle.Tables[0].Rows[0]["USER_SEQ"]);
			string sCastCharNo = iBridUtil.GetStringValue(dsBlogArticle.Tables[0].Rows[0]["USER_CHAR_NO"]);

			using (BlogNgUser oBlogNgUser = new BlogNgUser()) {
				bBlogNgUser = oBlogNgUser.CheckExistBlogNgUser(
					sessionMan.site.siteCd,
					sCastUserSeq,
					sCastCharNo,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo
				);
			}

			if (bBlogNgUser) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_COMMENT_NG);
				return;
			}

			tagCommentWriteHeader.Visible = true;
			txtComment.Visible = true;
			tagCommentWriteFooter.Visible = true;
			tagCommentConfirmHeader.Visible = false;
			lblCommentDoc.Visible = false;
			tagCommentConfirmFooter.Visible = false;
			tagCommentComplete.Visible = false;
		} else {
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				this.cmdSubmit_Click(sender,e);
			} else if (IsPostAction(PwViCommConst.BUTTON_PUBLISH)) {
				this.cmdPublish_Click(sender,e);
			} else if (IsPostAction(ViCommConst.BUTTON_RETURN)) {
				tagCommentWriteHeader.Visible = true;
				txtComment.Visible = true;
				tagCommentWriteFooter.Visible = true;
				tagCommentConfirmHeader.Visible = false;
				lblCommentDoc.Visible = false;
				tagCommentConfirmFooter.Visible = false;
				tagCommentComplete.Visible = false;
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e) {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.Form["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.Form["partner_user_char_no"]);

		//自分が拒否している場合
		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sPartnerUserSeq,sPartnerUserCharNo)) {
				this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
			}
		}
		
		using (BlogComment oBlogComment = new BlogComment()) {
			if (oBlogComment.CheckBlogCommentLimitTime(sSiteCd,sUserSeq,sUserCharNo,sPartnerUserSeq,sPartnerUserCharNo)) {
				sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_BLOG_LIMIT_TIME);
				return;
			}
		}

		if (string.IsNullOrEmpty(txtComment.Text)) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_BLOG_COMMENT);
			return;
		}

		if (txtComment.Text.Length > 50) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_BLOG_COMMENT_OVER_50);
			return;
		}
		
		string sNGWord;
		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		if (!sessionMan.ngWord.VaidateDoc(txtComment.Text,out sNGWord)) {
			if (sessionMan.ngWord.VaidateDoc(txtComment.Text,out sNGWord) == false) {
				sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		ViewState["COMMENT_DOC"] = txtComment.Text;
		lblCommentDoc.Text = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtComment.Text)).Replace(System.Environment.NewLine,"<br>");
		tagCommentWriteHeader.Visible = false;
		txtComment.Visible = false;
		tagCommentWriteFooter.Visible = false;
		tagCommentConfirmHeader.Visible = true;
		lblCommentDoc.Visible = true;
		tagCommentConfirmFooter.Visible = true;
		tagCommentComplete.Visible = false;
	}

	private void cmdPublish_Click(object sender,EventArgs e) {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sBlogArticleSeq = iBridUtil.GetStringValue(this.Request.Form["blog_article_seq"]);
		
		string sCommentDoc = ViewState["COMMENT_DOC"].ToString();
		sCommentDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,sCommentDoc));
		using (BlogComment oBlogComment = new BlogComment()) {
			oBlogComment.WriteBlogComment(sSiteCd,sUserSeq,sUserCharNo,sBlogArticleSeq,sCommentDoc);
		}
		tagCommentWriteHeader.Visible = false;
		txtComment.Visible = false;
		tagCommentWriteFooter.Visible = false;
		tagCommentConfirmHeader.Visible = false;
		lblCommentDoc.Visible = false;
		tagCommentConfirmFooter.Visible = false;
		tagCommentComplete.Visible = true;
	}

	private bool IsBlogNgUser() {
		bool bNgUser = false;
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sBlogArticleSeq = iBridUtil.GetStringValue(this.Request.QueryString["blog_article_seq"]);

		using (BlogNgUser oBlogNgUser = new BlogNgUser()) {
			bNgUser = oBlogNgUser.CheckExistBlogNgUserByBlogArticleSeq(sSiteCd,sBlogArticleSeq,sUserSeq,sUserCharNo);
		}

		return bNgUser;
	}
}