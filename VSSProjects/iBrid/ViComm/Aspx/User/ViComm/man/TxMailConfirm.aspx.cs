/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信確認
--	Progaram ID		: TxMailConfirm
--
--  Creation Date	: 2010.09.09
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_TxMailConfirm:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			ViewState["MAIL_DATA_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["data"]);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdBack"] != null) {
				cmdBack_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		string sRxUserSeq = sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq;
		string sRxUserCharNo = sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo;
		string sMailTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(sessionMan.userMan.mailData[sMailDataSeq].mailTitle) : sessionMan.userMan.mailData[sMailDataSeq].mailTitle));
		string sMailDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,IsAvailableService(ViCommConst.RELEASE_SEND_MAIL_NG_WORD_MASK,2) ? sessionMan.ngWord.ReplaceMask(sessionMan.userMan.mailData[sMailDataSeq].mailDoc) : sessionMan.userMan.mailData[sMailDataSeq].mailDoc));
		string sQuereyString = sessionMan.userMan.mailData[sMailDataSeq].queryString;
		string sReturnMailSeq = sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq;
		bool bIsReturnMail = sessionMan.userMan.mailData[sMailDataSeq].isReturnMail;
		string sMailType = iBridUtil.GetStringValue(Request.QueryString["mailtype"]);

		sessionMan.errorMessage = string.Empty;
		lblErrorMessage.Text = string.Empty;

		ParseHTML oParseMan = sessionMan.parseContainer;
		int iChargePoint;

		if (sMailType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
			if (!sessionMan.CheckTxMailMovieBalance(sRxUserSeq,sRxUserCharNo,out iChargePoint)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
			}
		} else if (sMailType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
			if (!sessionMan.CheckTxMailPicBalance(sRxUserSeq,sRxUserCharNo,out iChargePoint)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
			}
		} else {
			if (!sessionMan.CheckMailBalance(sRxUserSeq,sRxUserCharNo,out iChargePoint)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
			}
		}

		if (bOk == false) {
			return;
		} else {
			string sPicSeq = string.Empty;
			string sMovieSeq = string.Empty;
			string sChargeType = string.Empty;

			if (sMailType.Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
				sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
				sChargeType = ViCommConst.CHARGE_TX_MAIL_WITH_MOVIE;
			} else if (sMailType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
				sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
				sChargeType = ViCommConst.CHARGE_TX_MAIL_WITH_PIC;
			} else {
				sMailType = string.Empty;
				sChargeType = ViCommConst.CHARGE_TX_MAIL;
			}
			using (WebUsedLog oLog = new WebUsedLog()) {
				oLog.CreateWebUsedReport(
						sessionMan.userMan.siteCd,
						sessionMan.userMan.userSeq,
						sChargeType,
						iChargePoint,
						sRxUserSeq,
						sRxUserCharNo,
						string.Empty);
			}
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,sMailTitle));
			string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,sMailDoc));
			string sMailSeq = string.Empty;
			using (MailBox oMailBox = new MailBox()) {
				if (bIsReturnMail) {
					sMailSeq = oMailBox.TxManToCastReturnMail(
						sessionMan.userMan.siteCd,
						sessionMan.userMan.userSeq,
						sRxUserSeq,
						sRxUserCharNo,
						sTitle,
						sDoc,
						sMailType,
						sPicSeq,
						sMovieSeq,
						sReturnMailSeq,
						string.Empty,
						sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag
					);
				} else {
					sMailSeq = oMailBox.TxManToCastMail(
						sessionMan.userMan.siteCd,
						sessionMan.userMan.userSeq,
						sRxUserSeq,
						sRxUserCharNo,
						sTitle,
						sDoc,
						sMailType,
						sPicSeq,
						sMovieSeq,
						sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag,
						string.Empty
					);
				}
			}

			string sBingoQuery = string.Empty;
			int iCompliateFlag = ViCommConst.FLAG_OFF;
			int iGetNewBallFlag;
			int iGetOldBallFlag;
			int iGetCardBallFlag;
			int iGetBallNo;
			int iBingoBallFlag;
			string sResult = sessionMan.bingoCard.GetBingoBall(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN,sMailSeq,string.Empty,out iGetNewBallFlag,out iGetOldBallFlag,out iGetCardBallFlag,out iGetBallNo,out iCompliateFlag,out iBingoBallFlag);
			sBingoQuery = string.Format("&mailseq={0}&ballnew={1}&ballold={2}&cardball={3}&ballno={4}&comp={5}&result={6}&bingoball={7}",sMailSeq,iGetNewBallFlag,iGetOldBallFlag,iGetCardBallFlag,iGetBallNo,iCompliateFlag,sResult,iBingoBallFlag);

			string sNextUrl = string.Empty;

			if (bIsReturnMail) {
				sNextUrl = "DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_REMAIL_COMPLITE + sBingoQuery + "&" + sQuereyString;
			} else {
				sNextUrl = "DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_MAIL_COMPLITE_MAN + sBingoQuery + "&" + sQuereyString;
			}

			if (iCompliateFlag.Equals(ViCommConst.FLAG_ON)) {
				sNextUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)));
			}

			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
		}
	}

	protected void cmdBack_Click(object sender,EventArgs e) {
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		if (sessionMan.userMan.mailData[sMailDataSeq].isReturnMail) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ReturnMail.aspx?data=" + sMailDataSeq));
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"TxMail.aspx?data=" + sMailDataSeq));
		}
	}
}
