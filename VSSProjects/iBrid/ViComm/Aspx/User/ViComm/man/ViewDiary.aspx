<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewDiary.aspx.cs" Inherits="ViComm_man_ViewDiary" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagComplete" runat="server" Text="$PGM_HTML02;" />
		<cc1:iBMobileLiteralText ID="tagLiked" runat="server" Text="$PGM_HTML03;" />
		$DATASET_LOOP_START3;
			<cc1:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END3;
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
