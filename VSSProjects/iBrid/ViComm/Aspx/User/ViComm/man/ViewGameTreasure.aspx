<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewGameTreasure.aspx.cs" Inherits="ViComm_man_ViewGameTreasure" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="ibrid" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMsg" runat="server" Text="" ForeColor="Red" />
		<ibrid:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
