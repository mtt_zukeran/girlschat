/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �싅���摜�ڍ�
--	Progaram ID		: ViewYakyukenPic
--
--  Creation Date	: 2013.05.03
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewYakyukenPic:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sYakyukenGameSeq = iBridUtil.GetStringValue(Request.QueryString["ygameseq"]);

		if (string.IsNullOrEmpty(sYakyukenGameSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_YAKYUKEN_PIC_VIEW,this.ActiveForm)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		}
	}
}
