/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女性の欲しいものﾘｽﾄ
--	Progaram ID		: ListGameWantedItemPartner
--
--  Creation Date	: 2011.11.15
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameWantedItemPartner:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.QueryString["partner_user_char_no"]);
		
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sPartnerUserSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		rgx = new Regex("^[0-9]+$");
		rgxMatch = rgx.Match(sPartnerUserCharNo);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_WANTED_ITEM_PARTNER,ActiveForm);
		}
	}
}
