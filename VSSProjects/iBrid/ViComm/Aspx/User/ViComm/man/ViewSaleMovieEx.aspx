<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewSaleMovieEx.aspx.cs" Inherits="ViComm_man_ViewSaleMovieEx" %>
<%@ Register TagPrefix="ibrid" Assembly="MobileLib" Namespace="MobileLib"  %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Import Namespace="ViComm" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Label ID="lblMovieInfo" Runat="server"></mobile:Label>
		<mobile:Panel Runat="server" Alignment="left">
			<mobile:ObjectList ID="lstFileNmList" Runat="server" BreakAfter="false" >
				<DeviceSpecific>
					<Choice>
						<ItemTemplate>
							<ibrid:iBMobilePlaceHolder runat="server" Text="<%# GetDownloadTag((MobileListItem)Container) %>"></ibrid:iBMobilePlaceHolder>						
							<mobile:Label Runat="server" BreakAfter="<%# ((((MobileListItem)Container).Index + 1)%4)==0?true:false %>"></mobile:Label>
						</ItemTemplate>
					</Choice>
				</DeviceSpecific>
			</mobile:ObjectList>
		</mobile:Panel>
		<br /><br />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
