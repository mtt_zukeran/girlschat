﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: Edy支払(Sophi)
--	Progaram ID		: PaymentEdy
--
--  Creation Date	: 2010.05.14
--  Creater			: innetwork
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Net;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentEdySophia:System.Web.UI.MobileControls.MobilePage {
	private SessionMan sessionMan;

	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan = (SessionMan)Session["objs"];
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			using (Pack oPack = new Pack()) {
				DataSet ds;
				ds = oPack.GetList(sessionMan.site.siteCd,ViCommConst.SETTLE_EDY,0,sessionMan.userMan.userSeq);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstEdyPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				}
			}
		}

		//最初にアクセスした時に表示するテンプレート設定
		tagEdySettleTop.Visible = true;
		tagEdyconfirmTop.Visible = false;
		pnlEdySettle.Visible = true;
		pnlEdyConfirm.Visible = false;
		tagEdySettleFooter.Visible = true;
		tagEdyConfirmFooter.Visible = false;
	}


	//申し込みボタンを押した時に実行される
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstEdyPack.Items[lstEdyPack.SelectedIndex].Value;
		string sSid = "";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_EDY,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_SOPHIA,
					ViCommConst.SETTLE_EDY,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					"",
					"",
					out sSid);
			}
		}

		//エンコーディング設定（EUC-JP）
		Encoding enc = Encoding.GetEncoding("EUC-JP");


		//変数宣言
		string resText = "";
		string sPostData = "";


		//Edyから送られる決済完了メールに表示されるサイトのURLを作成「▼サイト名マイページ　改行コード　URL」
		string sSiteName = "▼" + sessionMan.site.siteNm + "マイページ";
		string sBaseUrl = sessionMan.site.url + "/user/start.aspx?loginid%3d{0}%26amp%3bpassword%3d{1}";
		string sSetIdUrl = string.Format(sBaseUrl,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
		string sSettlementAddInfo = System.Web.HttpUtility.UrlEncode(sSiteName,enc) + "\r\n" + sSetIdUrl;


		//Edyに決済を申し込む際に必要な引数
		// "spid"				Edyから発行されるID
		// "orderid"			システム側で発行される決済ID
		// "amount"  			購入金額
		// "usermail"			会員のメールアドレス
		// "settlementaddinfo"	決済完了メールに記載される文章(EUC-JP)
		string sBasePost = "spid={0}&member=regist&orderid={1}&amount={2}&usermail={3}&settlementaddinfo={4}";


		//Edyの決済サイトにpost送信する
		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_SOPHIA,ViCommConst.SETTLE_EDY)) {

				sPostData = string.Format(sBasePost,oSiteSettle.cpIdNo,sSid,sSalesAmt,sessionMan.userMan.emailAddr,sSettlementAddInfo);
				byte[] oPostDataBytes = enc.GetBytes(sPostData);

				WebClient wc = new WebClient();
				wc.Headers.Add("Content-Type","application/x-www-form-urlencoded");
				byte[] resData = wc.UploadData(oSiteSettle.settleUrl,oPostDataBytes);

				wc.Dispose();

				//post戻り値を格納
				resText = enc.GetString(resData);

			}
		}

		//戻り値を「|」で区切り配列に格納
		string[] sInfo = resText.Split('|');

		//格納された配列の0番目の値が「1」ならば決済の申し込み完了それ以外はNG
		if (sInfo[0].Equals("1")) {
			//決済申し込み完了
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_EDY_SETTLE_OK + "&" + Request.QueryString));
		} else {
			//決済申し込みNG
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_EDY_SETTLE_NG + "&" + Request.QueryString));
		}
	}


	//確認ボタンを押された時に実行される
	protected void cmdSubmitConf_Check(object sender,EventArgs e) {
		lblSettlePayment.Text = lstEdyPack.Items[lstEdyPack.SelectedIndex].Value + "円";

		//確認ボタンを押された時に表示するテンプレート設定
		tagEdySettleTop.Visible = false;
		tagEdyconfirmTop.Visible = true;
		pnlEdySettle.Visible = false;
		pnlEdyConfirm.Visible = true;
		tagEdySettleFooter.Visible = false;
		tagEdyConfirmFooter.Visible = true;
	}
}
