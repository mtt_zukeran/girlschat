﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ通知メール 詳細
--	Progaram ID		: ViewBlogMail
--
--  Creation Date	: 2011.04.14
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewBlogMail : MobileBlogManBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
		if (sessionMan.ControlList(Request, ViCommConst.INQUIRY_RX_BLOG_MAIL_BOX, ActiveForm)) {
			if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailDel(sessionMan.GetMailValue("MAIL_SEQ"), ViCommConst.RX);
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_BLOG_MAIL + "&mail_delete_count=1"));
				}
			} else {
				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailReadFlag(sessionMan.GetMailValue("MAIL_SEQ"));
				}
			}
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType, Session.SessionID, "ListBlogMailBox.aspx"));
		}
	}
}
