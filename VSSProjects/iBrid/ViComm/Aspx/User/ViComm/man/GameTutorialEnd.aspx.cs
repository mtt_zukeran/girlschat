/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE����ري���
--	Progaram ID		: GameTutorialEnd
--
--  Creation Date	: 2012.02.04
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GameTutorialEnd:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		//����رُ�ԍX�V
		using (GameCharacter oGameCharacter = new GameCharacter()) {
			oGameCharacter.SetupTutorialStatus(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				null
			);
		}

		sessionMan.userMan.gameCharacter.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

		string sResult = string.Empty;
		
		using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
			sResult = oGameIntroLog.LogGameIntro(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				ViCommConst.MAN
			);
		}

		if (sResult == PwViCommConst.LogGameIntroResult.RESULT_OK) {
			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}
}