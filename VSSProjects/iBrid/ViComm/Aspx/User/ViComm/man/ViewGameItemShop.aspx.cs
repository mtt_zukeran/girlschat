/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���Ѽ���� ���эw���m�F
--	Progaram ID		: ViewGameItemShop
--
--  Creation Date	: 2011.07.25
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_man_ViewGameItemShop:MobileSocialGameManBase {	
	protected void Page_Load(object sender,EventArgs e) {
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["item_seq"]));
		if(!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["buy_num"]));
		if(!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if(!IsPostBack) {
			if(!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_SHOP_VIEW,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if(this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.BuyGameItem();
			}
		}
	}
	
	private void BuyGameItem() {
		string sSiteCode = sessionMan.userMan.siteCd;
		string sUserSeq = sessionMan.userMan.userSeq;
		string sUserCharNo = sessionMan.userMan.userCharNo;
		string[] sValues = this.Request.Params["NEXTLINK"].Split('_');
		string sItemSeq = sValues[0];
		string sBuyNum = sValues[1];
		string sGetCd = sValues[2];
		string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.Params["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.Params["partner_user_char_no"]);
		string sCastGamePicSeq = iBridUtil.GetStringValue(this.Request.Params["cast_game_pic_seq"]);
		string sItemCategory = iBridUtil.GetStringValue(this.Request.Params["item_category"]);
		string sPageId = iBridUtil.GetStringValue(Session["PageId"]);
		string sStageSeq = iBridUtil.GetStringValue(Session["StageSeq"]);
		string sBattleLogSeq = iBridUtil.GetStringValue(Session["BattleLogSeq"]);
		string sSaleFlag = iBridUtil.GetStringValue(this.Request.Params["sale"]);
		
		string sPreMoney = this.sessionMan.userMan.gameCharacter.gamePoint.ToString();
		string sPreBalPoint = this.sessionMan.userMan.balPoint.ToString();
		string sPreAttackPower = this.GetAttackPower();
		string sPreDefencePower = this.GetDefencePower();
	
		string result = this.BuyGameItemExeCute(sSiteCode,sUserSeq,sUserCharNo,sItemSeq,sBuyNum);

		if(result.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
			if (sGetCd.Equals(PwViCommConst.GameItemGetCd.CHARGE)) {
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			}

			IDictionary<string,string> oParameters = new Dictionary<string,string>();

			if (sPageId == PwViCommConst.PageId.TRAP) {
				oParameters.Add("buy_num",sBuyNum);
				oParameters.Add("partner_user_seq",sPartnerUserSeq);
				oParameters.Add("partner_user_char_no",sPartnerUserCharNo);
				oParameters.Add("cast_game_pic_seq",sCastGamePicSeq);
				oParameters.Add("item_category",sItemCategory);
				oParameters.Add("page_id",sPageId);
			} else if (sPageId == PwViCommConst.PageId.BOSS_BATTLE) {
				oParameters.Add("buy_num",sBuyNum);
				oParameters.Add("stage_seq",sStageSeq);
				oParameters.Add("page_id",sPageId);
			} else if (sPageId == PwViCommConst.PageId.SUPPORT_BATTLE) {
				oParameters.Add("buy_num",sBuyNum);
				oParameters.Add("battle_log_seq",sBattleLogSeq);
				oParameters.Add("page_id",sPageId);
			} else {
				oParameters.Add("buy_num",sBuyNum);
			}
			
			if(sGetCd.Equals(PwViCommConst.GameItemGetCd.CHARGE)) {
				oParameters.Add("pre_point",sPreBalPoint);
			} else {
				oParameters.Add("pre_money",sPreMoney);
			}
			oParameters.Add("pre_attack_power",sPreAttackPower);
			oParameters.Add("pre_defence_power",sPreDefencePower);
			oParameters.Add("sale",sSaleFlag);
			
			Session["PageId"] = null;
			Session["BattleLogSeq"] = null;
			
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ITEM_BUY_COMPLETE,oParameters);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}
	
	private string BuyGameItemExeCute(string sSiteCode,string sUserSeq,string sUserCharNo,string sItemSeq,string sBuyNum) {
		string sResult = "";
		GameItem oGameItem = new GameItem();
		oGameItem.BuyGameItem(sSiteCode,sUserSeq,sUserCharNo,sItemSeq,sBuyNum,out sResult);
		return sResult;
	}

	private string GetAttackPower() {
		string sValue = "0";

		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sForceCount = this.sessionMan.userMan.gameCharacter.attackMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameAttackPowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}

	private string GetDefencePower() {
		string sValue = "0";

		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sForceCount = this.sessionMan.userMan.gameCharacter.defenceMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameDefencePowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}
}
