/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフィールMovie一覧
--	Progaram ID		: ListProfileMovie
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProfileMovie:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			bool bDownload = false;
			int iChargePoint = 0;
			string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

			if (!sMovieSeq.Equals("")) {
				sessionMan.errorMessage = "";
//				if (sessionMan.CheckPFDownloadBalance(sCastSeq,sCastCharNo,sMovieSeq,out iChargePoint) == false) {
//					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_DOWNLOAD_PF_MOVIE));
//				} else {
					bDownload = Download(sCastSeq,sCastCharNo,sMovieSeq,iChargePoint);
//				}
			};

			if (bDownload == false) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				sessionMan.ControlList(
						Request,
						ViCommConst.INQUIRY_CAST_MOVIE,
						ActiveForm);
			}
		}
	}


	private bool Download(string pCastSeq,string pCastCharNo,string pMovieSeq,int pChargePoint) {
		ParseHTML oParseHTML = sessionMan.parseContainer;
		
		string sFileNm, sFullPath, sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if(lSize==ViCommConst.GET_MOVIE_ERROR_VALUE){
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (lSize > 0) {
				if (sessionMan.logined) {
					if(!MovieHelper.IsRangeRequest()){
/*
						using (WebUsedLog oLog = new WebUsedLog()) {
							oLog.CreateWebUsedReport(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.CHARGE_DOWNLOAD_PF_MOVIE,
									pChargePoint,
									pCastSeq,
									pCastCharNo,
									"");
						}
*/

						using (CastMovieView oCastMovieView = new CastMovieView()) {
							oCastMovieView.RegistCastMovieView(
								sessionMan.site.siteCd,
								sessionMan.userMan.userSeq,
								pMovieSeq
							);
						}
					}
				}

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);

				return true;
			} else {
				return false;
			}
		}
	}

}
