<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetupVoiceApp.aspx.cs" Inherits="ViComm_man_SetupVoiceApp" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlInput" Runat="server">
			$PGM_HTML02;
			<mobile:SelectionList ID="chkUseVoiceapp" Runat="server" SelectType="CheckBox">
				<Item Text="音声通話ｱﾌﾟﾘを利用する" Value="1" />
			</mobile:SelectionList>
			$PGM_HTML03;
		</mobile:Panel>
		<mobile:Panel ID="pnlError" Runat="server">
			$PGM_HTML04;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
