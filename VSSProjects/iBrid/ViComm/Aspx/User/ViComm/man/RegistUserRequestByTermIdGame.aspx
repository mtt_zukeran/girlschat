<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistUserRequestByTermIdGame.aspx.cs" Inherits="ViComm_man_RegistUserRequestByTermIdGame" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML02;
	    $PGM_HTML03;
		<cc1:iBMobileTextBox ID="txtHandleNm" runat="server" MaxLength="20" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
