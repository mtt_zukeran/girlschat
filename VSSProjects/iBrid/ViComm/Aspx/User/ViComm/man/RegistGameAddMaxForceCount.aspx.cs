/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@�����z�u
--	Progaram ID		: RegistGameAddMaxForceCount
--
--  Creation Date	: 2011.09.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;

public partial class ViComm_man_RegistGameAddMaxForceCount:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			string sSiteCd = this.sessionMan.site.siteCd;
			string sUserSeq = this.sessionMan.userMan.userSeq;
			string sUserCharNo = this.sessionMan.userMan.userCharNo;
			int iAddCount = 0;
			int iMskForce = 0;
			string sResult = string.Empty;
			string sPreAttackForce = string.Empty;
			string sPreDefenceForce = string.Empty;
			string sPreMissionForce = string.Empty;
			string sPreAttackPower = string.Empty;
			string sPreDefencePower = string.Empty;

			if (!string.IsNullOrEmpty(this.Request.Params["cmdGoto_attack"])) {
				iAddCount = int.Parse(this.Request.Params["attack"].ToString());
				iMskForce = PwViCommConst.GameMskForce.MSK_ATTACK_FORCE;
				sPreAttackForce = this.sessionMan.userMan.gameCharacter.attackMaxForceCount.ToString();
				sPreAttackPower = this.GetAttackPower();
			} else if (!string.IsNullOrEmpty(this.Request.Params["cmdGoto_defence"])) {
				iAddCount = int.Parse(this.Request.Params["defence"].ToString());
				iMskForce = PwViCommConst.GameMskForce.MSK_DEFENCE_FORCE;
				sPreDefenceForce = this.sessionMan.userMan.gameCharacter.defenceMaxForceCount.ToString();
				sPreDefencePower = this.GetDefencePower();
			} else if (!string.IsNullOrEmpty(this.Request.Params["cmdGoto_mission"])) {
				iAddCount = int.Parse(this.Request.Params["mission"].ToString());
				iMskForce = PwViCommConst.GameMskForce.MSK_MISSION_FORCE;
				sPreMissionForce = this.sessionMan.userMan.gameCharacter.missionMaxForceCount.ToString();
			}
			
			using (AddMaxForceCount oAddMaxForceCount = new AddMaxForceCount()) {
				oAddMaxForceCount.ExecuteAddMaxForceCount(sSiteCd,sUserSeq,sUserCharNo,iAddCount,iMskForce,out sResult);
			}
			
			if(sResult.Equals(PwViCommConst.GameAddMaxForceResult.RESULT_OK)) {
				string sRedirectUrl = string.Format("RegistGameAddMaxForceCount.aspx?add_count={0}&msk_force={1}" +
				"&pre_attack_force={2}&pre_defence_force={3}&pre_mission_force={4}&pre_attack_power={5}&pre_defence_power={6}"
				,iAddCount.ToString(),iMskForce.ToString(),sPreAttackForce,sPreDefenceForce,sPreMissionForce,sPreAttackPower,sPreDefencePower);
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}

	private string GetAttackPower() {
		string sValue = "0";

		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sForceCount = this.sessionMan.userMan.gameCharacter.attackMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameAttackPowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}

	private string GetDefencePower() {
		string sValue = "0";

		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sForceCount = this.sessionMan.userMan.gameCharacter.defenceMaxForceCount.ToString();

		sValue = GamePowerExpressionHelper.GameDefencePowerExpression(sSiteCd,sUserSeq,sUserCharNo,sForceCount);

		return sValue;
	}
}
