<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CCheckComplite.aspx.cs" Inherits="ViComm_man_CCheckComplite" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" ><cc1:iBMobileLiteralText ID="tag01" runat="server" />
	$PGM_HTML01;
	$PGM_HTML02;
	<cc1:iBMobileTextBox ID="txtLoginId" runat="server" MaxLength="11" Size="16" Numeric="true" Visible="False"></cc1:iBMobileTextBox>
	<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="4" Size="6" Numeric="true" Visible="False"></cc1:iBMobileTextBox>
	$PGM_HTML03;
	$PGM_HTML04;
	$PGM_HTML09;
	</mobile:Form>
</body>
</html>
