﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者掲示板詳細
--	Progaram ID		: ViewCastBbs
--
--  Creation Date	: 2010.04.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewCastBbs:MobileManPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {

		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			ulong uSeekMode;
			bool bControlList;
			if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
				bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
			} else {
				bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_DOC,ActiveForm);
			}

			if (bControlList) {
				// キャストのクエリーパラメーターを作成するためにデータセットを作成します。
				string sCastSeq = sessionMan.GetBbsDocValue("USER_SEQ");
				string sCastCharNo = sessionMan.GetBbsDocValue("USER_CHAR_NO");
				string sLoginId = sessionMan.GetBbsDocValue("LOGIN_ID");
				string sBbsSeq = sessionMan.GetBbsDocValue("BBS_SEQ");

				int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
				sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo);

				// 閲覧数を更新します。
				using (CastBbs oCastBbs = new CastBbs()) {
					oCastBbs.UpdateReadingCount(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sBbsSeq);
				}
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListCastBbs.aspx"));
			}
		}
	}
}
