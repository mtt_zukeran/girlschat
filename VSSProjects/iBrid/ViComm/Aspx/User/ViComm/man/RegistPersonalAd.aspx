<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistPersonalAd.aspx.cs" Inherits="ViComm_man_RegistPersonalAd" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML02;
		<cc1:iBMobileTextBox ID="txtPersonalAdCd" runat="server" MaxLength="6" Size="8" Numeric="true"></cc1:iBMobileTextBox>
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
