/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �f���ʐ^����ڍ�
--	Progaram ID		: ViewBbsMovie
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_ViewBbsMovie:MobileManPageBase {
	private string castSeq;
	private string movieSeq;
	private string castCharNo;
	private int chargePoint;
	private int paymentAmt;
	private bool dupUsed;

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			} else {
				bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);
				castSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
				movieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
				castCharNo = ViCommConst.MAIN_CHAR_NO;

				bool bDownload = false;
				dupUsed = false;

				if (!movieSeq.Equals("")) {
					CheckMovieBalance();
					if (!bIsCheckOnly) {
						bDownload = Download();
					}
				}

				if (bDownload == false) {
					ulong uSeekMode;
					bool bControlList;
					Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

					if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
						bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
					} else {
						bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_MOVIE,ActiveForm);
					}
					if (bControlList) {
						int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
						castSeq = sessionMan.GetBbsMovieValue("USER_SEQ");
						castCharNo = sessionMan.GetBbsMovieValue("USER_CHAR_NO");
						movieSeq = sessionMan.GetBbsMovieValue("MOVIE_SEQ");
						CheckMovieBalance();
						sessionMan.SetCastDataSetByLoginId(sessionMan.GetBbsMovieValue("LOGIN_ID"),castCharNo,iRecNo);
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListBbsMovie.aspx"));
					}
				}
			}
		}
	}


	private void CheckMovieBalance() {
		bool bDupUsed;
		chargePoint = 0;
		if (!MovieHelper.IsRangeRequest()) {
			sessionMan.errorMessage = "";
			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				bDupUsed = oObjUsedHistory.GetOne(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
									movieSeq);

				dupUsed = bDupUsed;
			}
			if (bDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
				if (sessionMan.CheckBbsMovieBalance(castSeq,castCharNo,movieSeq,out chargePoint,out paymentAmt) == false) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_BBS_MOVIE));
				}
			}
		}
	}

	private bool Download() {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		CheckMovieBalance();

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,movieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				if (sessionMan.logined) {

					if (!MovieHelper.IsRangeRequest()) {
						if (dupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
							using (Access oAccess = new Access()) {
								oAccess.AccessMoviePage(sessionMan.site.siteCd,castSeq,castCharNo,movieSeq);
							}
							using (WebUsedLog oLog = new WebUsedLog()) {
								oLog.CreateWebUsedReport(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.CHARGE_BBS_MOVIE,
										chargePoint,
										castSeq,
										castCharNo,
										"",
										paymentAmt);
							}
							using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {

								oObjUsedHistory.AccessBbsObj(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
										movieSeq);
							}
						}
					}
				}

				MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.OPERATOR);

				return true;
			} else {
				return false;
			}
		}
	}
}
