/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｸﾚｼﾞｯﾄQUICK支払(ZEROｸﾚｼﾞｯﾄ)自動決済
--	Progaram ID		: PaymentCreditZeroQuickAuto
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Security.Cryptography.X509Certificates;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_PaymentCreditZeroQuickAuto:MobileManPageBase {
	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sCreditAutoSettleSeq = iBridUtil.GetStringValue(Request.QueryString["settleseq"]);
			DataSet ds;
			using (CreditAutoSettle oCreditAutoSettle = new CreditAutoSettle()) {
				ds = oCreditAutoSettle.GetOne(sessionMan.site.siteCd,sCreditAutoSettleSeq);
			}
			if (ds.Tables[0].Rows.Count > 0) {
				sessionMan.userMan.settleRequestAmt = int.Parse(ds.Tables[0].Rows[0]["SETTLE_AMT"].ToString());
				sessionMan.userMan.settleRequestPoint = int.Parse(ds.Tables[0].Rows[0]["SETTLE_POINT"].ToString());
				int iServicePoint = 0;
				using (Pack oPack = new Pack()) {
					iServicePoint = oPack.CalcServicePointCreditAutoSettle(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.settleRequestAmt);
				}
				sessionMan.userMan.settleServicePoint = iServicePoint;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdCancel"] != null) {
				cmdCancel_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSettleUrl = "";
		int iSettleAmt = 0;
		string sSid = string.Empty;
		string sResult = string.Empty;
		string sCreditAutoSettleSeq = iBridUtil.GetStringValue(this.Request.QueryString["settleseq"]);

		if (!iBridUtil.GetStringValue(this.Request.Form["agreement"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_CREDIT_AUTO_SETTLE_NO_AGREEMENT);
			return;
		}

		using (CreditAutoSettleStatus oCreditAutoSettleStatus = new CreditAutoSettleStatus()) {
			oCreditAutoSettleStatus.RegistCreditAutoSettle(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCreditAutoSettleSeq,
				"30",
				out iSettleAmt,
				out sSid,
				out sResult
			);
		}
		
		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_NG)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
				
		string sSalesAmt = iBridUtil.GetStringValue(iSettleAmt);

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_CREDIT_PACK)) {
				sSettleUrl = string.Format(oSiteSettle.continueSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sessionMan.userMan.tel);
			}
		}

		if (TransQuick(sSettleUrl)) {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"0");
			}
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + PwViCommConst.SCR_CREDIT_QUICK_AUTO_OK));
		} else {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"9");
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + PwViCommConst.SCR_CREDIT_QUICK_AUTO_NG));
		}
	}


	public bool TransQuick(string pUrl) {
		try {
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sRes.Equals("Success_order");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransQuick",pUrl);
			return false;
		}
	}

	private bool OnRemoteCertificateValidationCallback(
	  Object sender,
	  X509Certificate certificate,
	  X509Chain chain,
	  SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}

	protected void cmdCancel_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionMan.GetNavigateUrl("ListCreditAutoSettle.aspx?scrid=01"));
	}
}
