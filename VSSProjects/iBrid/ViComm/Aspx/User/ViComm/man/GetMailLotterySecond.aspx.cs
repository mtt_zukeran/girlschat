/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ(二次)実行
--	Progaram ID		: GetMailLotterySecond
--
--  Creation Date	: 2015.03.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GetMailLotterySecond:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMailLotteryRateSeq = string.Empty;
		string sMailLotteryRewardPoint = string.Empty;
		string sLoseFlag = string.Empty;
		string sResult = string.Empty;

		using (MailLottery oMailLottery = new MailLottery()) {
			oMailLottery.GetMailLotterySecond(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sessionMan.sexCd,
				ViCommConst.FLAG_OFF,
				out sMailLotteryRateSeq,
				out sMailLotteryRewardPoint,
				out sResult
			);
		}

		if (!sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_NG)) {
			string sNextUrl = string.Format("ViewMailLotteryTicketCount.aspx?result={0}",sResult);

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				sNextUrl = string.Format("{0}&lottery={1}&reward={2}",sNextUrl,sMailLotteryRateSeq,sMailLotteryRewardPoint);

				if (!sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
					if (IsAvailableService(ViCommConst.RELEASE_MAIL_LOTTERY_MOVIE,2)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ViewFlashMailLotterySecond.aspx?next_url={0}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)))));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
					}
				} else {
					if (IsAvailableService(ViCommConst.RELEASE_MAIL_LOTTERY_MOVIE_IPHONE,2)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ViewFlashMailLotterySecond.aspx?next_url={0}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)))));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
					}
				}

			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
			}

		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}
