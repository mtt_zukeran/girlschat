﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 通話アプリ利用設定
--	Progaram ID		: SetupTalkApp
--
--  Creation Date	: 2011.12.14
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_SetupTalkApp:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sSetupFlag = iBridUtil.GetStringValue(Request.QueryString["setup"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));

			if (!string.IsNullOrEmpty(sSetupFlag)) {
				this.SetupCrosmile(sSetupFlag.Equals(ViCommConst.FLAG_ON_STR),sBackUrl);
			} else {
				if (sessionMan.userMan.useCrosmileFlag != 0) {
					chkUseCrosmile.SelectedIndex = 0;
				}
				if (Request.Params["usecrosmile"] != null) {
					this.SetupCrosmile(ViCommConst.FLAG_ON_STR.Equals(Request.Params["usecrosmile"]),"");
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		this.SetupCrosmile(chkUseCrosmile.SelectedIndex == 0,"");
	}

	private void SetupCrosmile(bool pSetupFlag,string pBackUrl) {
		if (pSetupFlag) {
			if (!sessionMan.IsRegistedCrosmile(sessionMan.userMan.crosmileUri)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_TALKAPP_UNREGISTERED);
				return;
			}
		}

		sessionMan.userMan.SetupCrosmile(
			sessionMan.userMan.userSeq,
			pSetupFlag
		);

		if (!string.IsNullOrEmpty(pBackUrl)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(pBackUrl));
			return;
		} else {
			if (pSetupFlag) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_USE_CROSMILE_COMPLITE));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_NON_USE_CROSMILE_COMPLITE));
			}
		}
	}
}
