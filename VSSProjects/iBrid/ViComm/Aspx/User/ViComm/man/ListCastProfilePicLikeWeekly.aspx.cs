/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者無料画像イイネ週間ランキング
--	Progaram ID		: ListCastProfilePicLikeWeekly
--  Creation Date	: 2013.12.23
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListCastProfilePicLikeWeekly:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_PROFILE_PIC_LIKE_WEEKLY,this.ActiveForm);
		}
	}
}
