<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LiveMenu.aspx.cs" Inherits="TypeP_Site_Premium_LiveMenu" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmLiveMenu" Runat="server">
		<cc1:iBMobileLabel ID="lblNoDataError" Runat="server" StyleReference="error">ご指定のＬＩＶＥメニューが見つかりません。</cc1:iBMobileLabel>
		<mobile:List ID="lstLiveOpenHeader" Runat="server">
			<DeviceSpecific>
				<Choice>
					<ItemTemplate>
						<cc1:iBMobileLiteralText ID="tagLiveOpenHeader" runat="server" Text='<%# GetMenuDoc(Eval( "HTML_DOC1"),Eval( "HTML_DOC2")) %>' />
					</ItemTemplate>
				</Choice>
			</DeviceSpecific>
		</mobile:List>
		<mobile:List ID="lstLiveCloseHeader" Runat="server">
			<DeviceSpecific>
				<Choice>
					<ItemTemplate>
						<cc1:iBMobileLiteralText ID="tagLiveCloseHeader" runat="server" Text='<%# GetMenuDoc(Eval( "NA_HTML_DOC1"),Eval( "NA_HTML_DOC2")) %>' />
					</ItemTemplate>
				</Choice>
			</DeviceSpecific>
		</mobile:List>
		<br />
		<cc1:iBMobileLiteralText ID="tagDIV01s" runat="server" Text='<div align="center">' />
		<cc1:iBMobileLink ID="lnkTalk" runat="server" Visible='<%#IsVisibleTalkLink() %>' NavigateUrl='<%# GetTalkLinkUrl()%>' Text="お話しする" />
		<mobile:List ID="lstCamera" Runat="server">
			<DeviceSpecific>
				<Choice>
					<ItemTemplate>
						<cc1:iBMobileLink ID="lnkCamera" runat="server" NavigateUrl='<%# GetLiveLink(Eval("CAMERA_ID")) %>' Text='<%# Eval("LIVE_CAMERA_NM") %>' />
					</ItemTemplate>
				</Choice>
			</DeviceSpecific>
		</mobile:List>
		<cc1:iBMobileLink ID="lnkRefresh" runat="server" Text="更新" />
		<br />
		<cc1:iBMobileLiteralText ID="tagDIV01e" runat="server" Text='</div>' />
		<mobile:List ID="lstLiveOpenFooter" Runat="server">
			<DeviceSpecific>
				<Choice>
					<ItemTemplate>
						<cc1:iBMobileLiteralText ID="tagLiveOpenFooter" runat="server" Text='<%#GetMenuDoc(Eval("HTML_DOC3"),Eval("HTML_DOC4")) %>' />
					</ItemTemplate>
				</Choice>
			</DeviceSpecific>
		</mobile:List>
		<mobile:List ID="lstLiveCloseFooter" Runat="server">
			<DeviceSpecific>
				<Choice>
					<ItemTemplate>
						<cc1:iBMobileLiteralText ID="tagLiveCloseFooter" runat="server" Text='<%# GetMenuDoc(Eval("NA_HTML_DOC3"),Eval("NA_HTML_DOC4")) %>' />
					</ItemTemplate>
				</Choice>
			</DeviceSpecific>
		</mobile:List>
		<br />
		$DIV_FLAG_ON($OTHER_SYS_INFO;);
		<cc1:iBMobileLink ID="lnkTvSys1" runat="server" Text="素人女性と話す" />
		<cc1:iBMobileLink ID="lnkTvSys2" runat="server" Text="TV電話" BreakAfter="false" />
		<cc1:iBMobileLink ID="lnkTopMenu" runat="server" Text="ﾄｯﾌﾟ" BreakAfter="false" />
		<cc1:iBMobileLink ID="lnkMyPage" runat="server" Text="ﾏｲﾍﾟｰｼﾞ" />
		<br />
		$DIV_END;
		$FOOTER;
	</mobile:Form>
</body>
</html>
