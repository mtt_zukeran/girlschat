/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 投票
--	Progaram ID		: RegistVote
--  Creation Date	: 2013.10.18
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistVote:MobileManPageBase {
	private string sCastUserSeq;
	private string sCastCharNo;

	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (string.IsNullOrEmpty(sCastUserSeq) || string.IsNullOrEmpty(sCastCharNo)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			int iTicketCount = 0;

			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_VOTE_RANK_CURRENT,this.ActiveForm)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("ListVoteRank.aspx"));
				return;
			}

			using (VoteTerm oVoteTerm = new VoteTerm()) {
				oVoteTerm.GetVoteTicketCount(sessionMan.site.siteCd,sessionMan.userMan.userSeq,out iTicketCount);
			}

			if (iTicketCount > 0) {
				for (int i = 1;i <= iTicketCount;i++) {
					lstVoteCount.Items.Add(new MobileListItem(string.Format("{0}票",i.ToString()),i.ToString()));
				}
			} else {
				lstVoteCount.Visible = false;
			}

			pnlInput.Visible = true;
			pnlError.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iVoteCount = 1;
		string sResult;

		if (lstVoteCount.Items.Count > 0) {
			iVoteCount = int.Parse(lstVoteCount.Items[lstVoteCount.SelectedIndex].Value);
		}

		using (VoteRank oVoteRank = new VoteRank()) {
			oVoteRank.RegistVote(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCastUserSeq,
				sCastCharNo,
				iVoteCount,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_OK)) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_VOTE_RANK_CURRENT,this.ActiveForm);
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_REGIST_VOTE_COMPLETE);
			return;
		} else if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_NG_TERM)) {
			sessionMan.errorMessage = "投票は終了しました";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_NG_TICKET)) {
			sessionMan.errorMessage = "投票券が足りません";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_NG_RANK)) {
			sessionMan.errorMessage = "このｱｲﾄﾞﾙは投票対象外です";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("RegistVote.aspx?castuserseq={0}&castcharno={1}",sCastUserSeq,sCastCharNo)));
		return;
	}
}