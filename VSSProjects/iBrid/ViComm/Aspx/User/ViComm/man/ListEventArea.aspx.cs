/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: イベント情報一覧（地域検索）
--	Progaram ID		: ListEventArea
--
--  Creation Date	: 2013.07.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListEventArea:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sPrefectureCD = iBridUtil.GetStringValue(this.Request.QueryString["prefecture"]);
			string sEventAreaCd = iBridUtil.GetStringValue(this.Request.QueryString["area"]);

			this.CreateList();
			
			if (!string.IsNullOrEmpty(sPrefectureCD)) {
				lstPrefecture.Visible = false;
				lstEventArea.Visible = true;
			} else {
				lstEventArea.Visible = false;
				lstEventArea.SelectedIndex = 0;
			}
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_EVENT,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sNotEndFlag = iBridUtil.GetStringValue(this.Request.QueryString["notend"]);
		string sEventCategory = iBridUtil.GetStringValue(this.Request.QueryString["eventcategory"]);
		string sUrl = "ListEventArea.aspx" + "?eventcategory=" + sEventCategory + "&prefecture=" + lstPrefecture.Selection.Value + "&area=" + lstEventArea.Selection.Value + "&notend=" + sNotEndFlag;
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sUrl));
	}

	virtual protected void CreateList() {
		SelectionList lstPrefecture = (SelectionList)frmMain.FindControl("lstPrefecture") as SelectionList;
		
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			DataSet ds = oCodeDtl.GetListOrderByCodeAsNumber(ViCommConst.CODE_TYPE_AREA);

			if (ds.Tables[0].Rows.Count > 0) {
				int i = 0;
				lstPrefecture.Items.Add(new MobileListItem("未選択",string.Empty));
				foreach (DataRow dr in ds.Tables[0].Rows) {
					i++;
					if (!dr["CODE_NM"].ToString().Equals("海外")) {
						lstPrefecture.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),string.Format("{0:D2}",i)));
					}
				}
			}
		}
		
		string sAttrTypeNm = string.Empty;
		
		if (!sessionMan.IsImpersonated) {
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				sAttrTypeNm = sessionMan.userMan.attrList[i].attrTypeNm;
				if (SysPrograms.Expression(@"地域",sAttrTypeNm)) {

					CreateListUserManItem(lstPrefecture,i);
				}
			}
		} else {
			for (int i = 0;i < sessionMan.originalWomanObj.userWoman.characterList[sessionMan.originalWomanObj.userWoman.curKey].attrList.Count;i++) {
				sAttrTypeNm = sessionMan.originalWomanObj.userWoman.characterList[sessionMan.originalWomanObj.userWoman.curKey].attrList[i].attrTypeNm;
				if (SysPrograms.Expression(@"(?:地域|出身地)",sAttrTypeNm)) {

					CreateListUserWomanItem(lstPrefecture,i);
				}
			}
		}
		
		string sPrefectureCd = iBridUtil.GetStringValue(this.Request.QueryString["prefecture"]);
		if (!string.IsNullOrEmpty(sPrefectureCd)) {
			using (EventArea oEventArea = new EventArea()) {
				DataSet ds = oEventArea.GetList(sPrefectureCd);
				
				if (ds.Tables[0].Rows.Count > 0) {
					int iIdx = 0;
					foreach (DataRow dr in ds.Tables[0].Rows) {
						iIdx++;
						lstEventArea.Items.Add(new MobileListItem(dr["EVENT_AREA_NM"].ToString(),dr["EVENT_AREA_CD"].ToString()));

						if (iBridUtil.GetStringValue(this.Request.QueryString["area"]).Equals(dr["EVENT_AREA_CD"].ToString())) {
							lstEventArea.SelectedIndex = iIdx;
						}
					}
				}
			}
		}
	}

	protected void CreateListUserManItem(SelectionList pListPrefecture,int pAttrIndex) {
		foreach (MobileListItem item in pListPrefecture.Items) {
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["prefecture"]))) {
				if (item.Value.Equals(iBridUtil.GetStringValue(this.Request.QueryString["prefecture"]))) {
					item.Selected = true;
				}
			} else {
				Regex rgx = new Regex(sessionMan.userMan.attrList[pAttrIndex].attrNm);
				Match rgxMatch = rgx.Match(item.Text);

				if (rgxMatch.Success) {
					item.Selected = true;
				}
			}
		}
	}
	
	protected void CreateListUserWomanItem(SelectionList pListPrefecture,int pAttrIndex) {
		foreach (MobileListItem item in pListPrefecture.Items) {
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["prefecture"]))) {
				if (item.Value.Equals(iBridUtil.GetStringValue(this.Request.QueryString["prefecture"]))) {
					item.Selected = true;
				}
			} else {
				Regex rgx = new Regex(sessionMan.originalWomanObj.userWoman.characterList[sessionMan.originalWomanObj.userWoman.curKey].attrList[pAttrIndex].attrNm);
				Match rgxMatch = rgx.Match(item.Text);

				if (rgxMatch.Success) {
					item.Selected = true;
				}
			}
		}
	}
}