/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���i������ ���D���� �ꗗ
--	Progaram ID		: ListProdAuctionBidLog
--
--  Creation Date	: 2011.06.21
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdAuctionBidLog : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		sessionMan.ControlList(
				this.Request,
				ViCommConst.INQUIRY_PRODUCT_AUCTION_BID_LOG,
				this.ActiveForm);
	}
}
