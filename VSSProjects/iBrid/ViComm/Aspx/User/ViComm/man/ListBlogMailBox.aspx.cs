/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ通知メール 一覧
--	Progaram ID		: ListBlogMailBox
--
--  Creation Date	: 2011.04.13
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListBlogMailBox : MobileManPageBase {
	protected void Page_Load(object sender, EventArgs e) {
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request, ViCommConst.INQUIRY_RX_BLOG_MAIL_BOX, ActiveForm);
		} 
	}
}
