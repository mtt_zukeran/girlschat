/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o�^��񏉉�ύX
--	Progaram ID		: ModifyUserBasicInfo
--
--  Creation Date	: 2011.05.24
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Configuration;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ModifyUserBasicInfo:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			txtPassword.Text = string.Empty;
			txtHandelNm.Text = HttpUtility.HtmlDecode(sessionMan.userMan.handleNm);

			bool bDupliFlag = false;
			if (IsAvailableService(ViCommConst.RELEASE_CHECK_HANDLE_NM_DUPLI)) {
				using (UserMan oUserMan = new UserMan()) {
					bDupliFlag = oUserMan.CheckManHandleNmDupli(sessionMan.site.siteCd,sessionMan.userMan.userSeq,Mobile.EmojiToCommTag(sessionMan.carrier,sessionMan.userMan.handleNm));
				}
			}

			string sFlag = string.Empty;
			using (ManageCompany oCompany = new ManageCompany()) {
				oCompany.GetValue("AUTO_GET_TEL_INFO_FLAG",out sFlag);
			}

			if (sFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				pnlTel.Visible = false;
			} else {
				pnlTel.Visible = true;
			}
			CreateList();

			int i = 0;

			sessionMan.userMan.GetAttrType(sessionMan.site.siteCd);

			int iCtrl = 0;
			for (i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				SelectionList lstUserManItem;
				iBMobileTextBox txtUserManItem;
				iBMobileLabel lblUserManItem;
				TextArea txtAreaUserManItem;

				if (sessionMan.userMan.attrList[i].registInputFlag) {
					lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iCtrl + 1)) as SelectionList;
					txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iCtrl + 1)) as iBMobileTextBox;
					lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",iCtrl + 1)) as iBMobileLabel;
					txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",iCtrl + 1)) as TextArea;

					lblUserManItem.Text = sessionMan.userMan.attrList[i].attrTypeNm;
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							txtAreaUserManItem.Visible = true;
							txtUserManItem.Visible = false;
							txtAreaUserManItem.Rows = int.Parse(sessionMan.userMan.attrList[i].rowCount);
						} else {
							txtAreaUserManItem.Visible = false;
							txtUserManItem.Visible = true;
						}
						lstUserManItem.Visible = false;
					} else {
						txtAreaUserManItem.Visible = false;
						txtUserManItem.Visible = false;
						lstUserManItem.Visible = true;
						CreateListUserManItem(lstUserManItem,i);
					}
					iCtrl++;
				}
			}
			for (;iCtrl < ViComm.ViCommConst.MAX_ATTR_COUNT;iCtrl++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",iCtrl + 1)) as Panel;
				pnlUserManItem.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		string sNGWord = string.Empty;

		if (txtPassword.Visible) {
			if (txtPassword.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD);
			} else {
				if (!SysPrograms.Expression(@"^[a-zA-Z0-9]{4,8}$",txtPassword.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD_NG);
				}
			}
		}

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			} else if (!SysPrograms.IsWithinByteCount(HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)),60)) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"�����Ȱ�");
			}

			if (sessionMan.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (txtTel.Visible) {
			if (txtTel.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
			} else {
				if (!sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
					}
				}
			}
		}

		if (lstYear.Visible) {
			if ((lstYear.Selection.Value.Equals("")) || (lstMonth.Selection.Value.Equals("")) || (lstDay.Selection.Value.Equals(""))) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY);
			}

			if (bOk) {
				string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
				int iAge = ViCommPrograms.Age(sDate);
				int iOkAge;
				int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
				if (iOkAge == 0) {
					iOkAge = 18;
				}

				if (iAge > 99 || iAge == 0) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
				} else if (iAge < iOkAge) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18);
				}
			}
		}

		if (bOk == false) {
			return;
		} else {
			string sResult;
			string sBirthday = "";

			if (lstYear.Visible) {
				DateTime dtBiathday = new DateTime(int.Parse(lstYear.Selection.Value),int.Parse(lstMonth.Selection.Value),int.Parse(lstDay.Selection.Value));
				sBirthday = dtBiathday.ToString("yyyy/MM/dd");
			}

			string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
			int iCtrl = 0;

			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				if (sessionMan.userMan.attrList[i].registInputFlag) {
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",iCtrl + 1)) as TextArea;
							pAttrInputValue[iCtrl] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtAreaUserManItem.Text));
							if (pAttrInputValue[iCtrl].Length > 300) {
								pAttrInputValue[iCtrl] = SysPrograms.Substring(pAttrInputValue[iCtrl],300);
							}
						} else {
							iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iCtrl + 1)) as iBMobileTextBox;
							pAttrInputValue[iCtrl] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtUserManItem.Text));
						}
						if (pAttrInputValue[iCtrl].Equals(string.Empty) && sessionMan.userMan.attrList[i].profileReqItemFlag) {
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
							bOk = false;
						}
						if (sessionMan.ngWord.VaidateDoc(pAttrInputValue[iCtrl],out sNGWord) == false) {
							bOk = false;
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
						}
					} else {
						SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iCtrl + 1)) as SelectionList;
						if (lstUserManItem.Selection != null) {
							if (sessionMan.userMan.attrList[i].profileReqItemFlag && lstUserManItem.Selection.Value.Equals(string.Empty)) {
								lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
								bOk = false;
							} else {
								pAttrSeq[iCtrl] = lstUserManItem.Selection.Value;
							}
						}
					}
					pAttrTypeSeq[iCtrl] = sessionMan.userMan.attrList[i].attrTypeSeq;
					iCtrl++;
				}
			}

			if (bOk == false) {
				return;
			}

			sessionMan.userMan.ModifyUser(
				sessionMan.userMan.userSeq,
				txtPassword.Text,
				txtTel.Text,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)),
				sBirthday,
				out sResult,
				pAttrTypeSeq,
				pAttrSeq,
				pAttrInputValue,
				iCtrl,
				ViCommConst.FLAG_ON
			);

			if (sResult.Equals("0")) {
				using (TempRegist oTemp = new TempRegist()) {
					if (oTemp.GetOneByUserSeq(sessionMan.userMan.userSeq)) {
						if (!oTemp.mobileCarrierCd.Equals(ViCommConst.ANDROID) && !oTemp.mobileCarrierCd.Equals(ViCommConst.IPHONE)) {
							oTemp.TrackingReport(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAN_AFFILIATE_REPORT_PROFILE,ViCommConst.MAN);
						}
					}
				}

				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.site.userTopId));
			} else {
				switch (int.Parse(sResult)) {
					case ViCommConst.REG_USER_RST_TEL_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MODIFIED_TEL_NO_ALREADY_EXIST);
						break;
					case ViCommConst.REG_USER_RST_HANDLE_NM_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
						break;
					default:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
						break;
				}
			}
		}
	}

	private void CreateList() {
		int iYear = DateTime.Today.Year - 18;

		for (int i = 0;i < 85;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear -= 1;
		}
		lstYear.SelectedIndex = 7;
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}

	virtual protected void CreateListUserManItem(SelectionList pListUserManItem,int pAttrIndex) {
		if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_LIST) {
			pListUserManItem.Items.Add(new MobileListItem("���I��",string.Empty));
		}
		using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
			DataSet ds = oAttrTypeValue.GetList(sessionMan.site.siteCd,sessionMan.userMan.attrList[pAttrIndex].attrTypeSeq);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				pListUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
			}
		}
		if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_RADIO) {
			pListUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
			pListUserManItem.SelectedIndex = 0;
		}
	}
}
