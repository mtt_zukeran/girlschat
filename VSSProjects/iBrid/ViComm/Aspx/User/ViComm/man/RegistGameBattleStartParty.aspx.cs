/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ作戦設定(ﾊﾟｰﾃｨｰﾊﾞﾄﾙ)
--	Progaram ID		: RegistGameBattleStartParty
--
--  Creation Date	: 2011.08.26
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_man_RegistGameBattleStartParty:MobileSocialGameManBase {
	private NameValueCollection query = new NameValueCollection();
	
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

			if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

			if(!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_START_PARTY,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if (!string.IsNullOrEmpty(this.Request.Params["cmdNext"])) {

				string[] textArr = this.Request.Params["NEXTLINK"].ToString().Split('_');

				string sPartnerUserSeq = textArr[0];
				string sPartnerUserCharNo = textArr[1];
				string sTreasureSeq = textArr[2];
				string sCastUserSeq = null;
				string sCastUserCharNo = null;
				
				int iAttackForceCount;

				iAttackForceCount = this.sessionMan.userMan.gameCharacter.attackMaxForceCount;
				
				DataSet oDataSetManTreasure;
				using (ManTreasure oManTreasure = new ManTreasure()) {
					oDataSetManTreasure = oManTreasure.GetCastSeqData(this.sessionMan.site.siteCd,sTreasureSeq);
				}

				foreach (DataRow drManTreasure in oDataSetManTreasure.Tables[0].Rows) {
					sCastUserSeq = drManTreasure["USER_SEQ"].ToString();
					sCastUserCharNo = drManTreasure["USER_CHAR_NO"].ToString();
				}

				string sFriendlyPoint = null;

				FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
				oCondition.SiteCd = this.sessionMan.site.siteCd;
				oCondition.UserSeq = this.sessionMan.userMan.userSeq;
				oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
				oCondition.PartnerUserSeq = sCastUserSeq;
				oCondition.PartnerUserCharNo = sCastUserCharNo;

				using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
					sFriendlyPoint = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
				}

				this.BattleStart(sPartnerUserSeq,sPartnerUserCharNo,sTreasureSeq,iAttackForceCount,sFriendlyPoint);
				
			}
		}
	}

	private void BattleStart(string sPartnerUserSeq,string sPartnerUserCharNo,string sTreasureSeq,int iAttackForceCount,string sFriendlyPoint) {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sSupportUserSeq = string.Empty;
		string sSupportUserCharNo = string.Empty;
		string sSupportBattleLogSeq = string.Empty;
		string sSupportRequestSubSeq = string.Empty;
		string sBattleType = PwViCommConst.GameBattleType.PARTY;
		int iSurelyWinFlag = 0;
		int iLevelUpFlag;
		int iAddForceCount;
		int iTreasureCompleteFlag;
		int iBonusGetFlag;
		int iGetCompCountItemFlag;				//お宝規定回数コンプ時獲得アイテム取得フラグ
		string[] sGetCompCountItemSeq = null;	//お宝規定回数コンプ時獲得アイテムSEQ
		string[] sGetCompCountItemCount = null;	//お宝規定回数コンプ時獲得アイテム付与数
		int iWinFlag;
		int iComePoliceFlag;
		int iTrapUsedFlag;
		string sBattleLogSeq;
		string[] BreakGameItemSeq;
		string sResult;

		string sPreExp = this.sessionMan.userMan.gameCharacter.exp.ToString();

		Battle oBattle = new Battle();
		oBattle.BattleStart(
			sSiteCd,
			sUserSeq,
			sUserCharNo,
			sPartnerUserSeq,
			sPartnerUserCharNo,
			sSupportUserSeq,
			sSupportUserCharNo,
			sSupportBattleLogSeq,
			sSupportRequestSubSeq,
			sBattleType,
			sTreasureSeq,
			iSurelyWinFlag,
			iAttackForceCount,
			out iLevelUpFlag,
			out iAddForceCount,
			out iTreasureCompleteFlag,
			out iBonusGetFlag,
			out iWinFlag,
			out iComePoliceFlag,
			out iTrapUsedFlag,
			out sBattleLogSeq,
			out BreakGameItemSeq,
			out iGetCompCountItemFlag,
			out sGetCompCountItemSeq,
			out sGetCompCountItemCount,
			out sResult
		);

		string sRedirectUrl = string.Empty;

		if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_OK)) {
			this.CheckQuestClear(sUserSeq,sUserCharNo);
			this.CheckQuestClear(sPartnerUserSeq,sPartnerUserCharNo);
		
			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
				string sPartnerGameCharacterType = this.GetGameCharacterType(sPartnerUserSeq,sPartnerUserCharNo);
				string sPartnerGamePartyMsk = this.GetPartyMemberMsk(sPartnerUserSeq,sPartnerUserCharNo);
				
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				
				if (iWinFlag == ViCommConst.FLAG_ON) {
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("treasure_complete_flag",iTreasureCompleteFlag.ToString());
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("pre_friendly_point",sFriendlyPoint);
					oParameters.Add("get_comp_count_item_seq",string.Join("_",sGetCompCountItemSeq));
					oParameters.Add("get_comp_count_item_count",string.Join("_",sGetCompCountItemCount));
					oParameters.Add("get_comp_count_item_rec_count",sGetCompCountItemSeq.Length.ToString());
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
					oParameters.Add("partner_party_msk",sPartnerGamePartyMsk);
				} else {
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("come_police_flag",iComePoliceFlag.ToString());
					oParameters.Add("trap_used_flag",iTrapUsedFlag.ToString());
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
					oParameters.Add("partner_party_msk",sPartnerGamePartyMsk);
				}
				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_PARTY_BATTLE_SP,oParameters);
			} else {
				if (iWinFlag == ViCommConst.FLAG_ON) {
					sRedirectUrl = String.Format(
										"ViewGameFlashPartyBattle.aspx?battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&treasure_complete_flag={3}&pre_exp={4}&pre_friendly_point={5}&get_comp_count_item_seq={6}&get_comp_count_item_count={7}&get_comp_count_item_rec_count={8}&battle_type={9}",
										sBattleLogSeq,
										iLevelUpFlag.ToString(),
										iAddForceCount.ToString(),
										iTreasureCompleteFlag.ToString(),
										sPreExp,
										sFriendlyPoint,
										string.Join("_",sGetCompCountItemSeq),
										string.Join("_",sGetCompCountItemCount),
										sGetCompCountItemSeq.Length.ToString(),
										PwViCommConst.GameBattleType.PARTY
									);
				} else {
					sRedirectUrl = String.Format(
										"ViewGameFlashPartyBattle.aspx?battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&come_police_flag={3}&trap_used_flag={4}&pre_exp={5}&battle_type={6}",
										sBattleLogSeq,
										iLevelUpFlag.ToString(),
										iAddForceCount.ToString(),
										iComePoliceFlag.ToString(),
										iTrapUsedFlag.ToString(),
										sPreExp,
										PwViCommConst.GameBattleType.PARTY
									);
				}

				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
			}
		} else if(sResult.Equals(PwViCommConst.GameBattleResult.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		else {
			this.query["result"] = sResult;
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_BATTLE_NO_FINISH,this.query);
		}
	}

	private string GetGameCharacterType(string sPartnerUserSeq,string sPartnerUserCharNo) {
		string sGameCharacterType = string.Empty;

		GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.PartnerUserSeq = sPartnerUserSeq;
		oCondition.PartnerUserCharNo = sPartnerUserCharNo;

		using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
			DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);

			if (ds.Tables[0].Rows.Count > 0) {
				sGameCharacterType = ds.Tables[0].Rows[0]["GAME_CHARACTER_TYPE"].ToString();
			}
		}

		return sGameCharacterType;
	}

	private string GetPartyMemberMsk(string sUserSeq,string sUserCharNo) {
		string sPartyMemberMsk = string.Empty;

		GamePartyBattleMemberSeekCondition oCondition = new GamePartyBattleMemberSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.UserSeq = sUserSeq;
		oCondition.UserCharNo = sUserCharNo;

		using (GamePartyBattleMember oGamePartyBattleMember = new GamePartyBattleMember()) {
			sPartyMemberMsk = oGamePartyBattleMember.GetPartyMemberMask(oCondition);
		}
		
		if(sPartyMemberMsk.Equals("0")) {
			sPartyMemberMsk = "8";
		}

		return sPartyMemberMsk;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}
