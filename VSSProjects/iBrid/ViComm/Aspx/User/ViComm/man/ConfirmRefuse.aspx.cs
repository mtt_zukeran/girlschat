﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 拒否登録確認
--	Progaram ID		: ConfirmRefuse
--
--  Creation Date	: 2011.01.05
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ConfirmRefuse : MobileManPageBase {
	virtual protected void Page_Load(object sender, EventArgs e) {

		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
		if (this.sessionMan.parseContainer.parseUser.currentTableIdx != ViCommConst.DATASET_CAST) {
			DataRow dr;
			sessionMan.SetCastDataSetByLoginId(sLoginId,ViCommConst.MAIN_CHAR_NO,1,out dr);
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
	}
}
