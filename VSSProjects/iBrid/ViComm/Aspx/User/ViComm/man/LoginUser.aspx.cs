/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーログイン
--	Progaram ID		: LoginUser
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  2010/08/16  K.Itoh	ｱｸｾｽ集計ﾕﾆｰｸ数対応

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_LoginUser:MobileManPageBase {
	private string userSeq;
	private string userStatus;
	private string loginResult;
	private string utn;
	private string id;
	private string bookmark;
	private string snsType;
	private string snsId;
	private bool isPreviousLogin = false;
	private bool bFirstFlag = false;

	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			// 偽装状態の場合、偽造終了画面へ転送
			if (sessionMan.IsImpersonated) {
				sessionMan.parseContainer.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + "/ViComm/man",Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_RETURN_TO_ORIGINAL_MENU));
			}


			ParseHTML oParseMan = sessionMan.parseContainer;
			string sGoto = this.GetStringValue(Request.QueryString["goto"]);
			utn = this.GetStringValue(Request.QueryString["utn"]);
			id = this.GetStringValue(Request.QueryString["id"]);
			bookmark = this.GetStringValue(Request.QueryString["bookmark"]);
			bFirstFlag = this.GetStringValue(Request.QueryString["first"]).Equals(ViCommConst.FLAG_ON_STR);
			snsType = sessionObj.snsType;
			snsId = sessionObj.snsId;

			if (utn.Equals("") && id.Equals("") && snsType.Equals("") && snsId.Equals("")) {
				txtLoginId.Text = this.GetStringValue(Request.QueryString["uid"]);
				txtPassword.Text = this.GetStringValue(Request.QueryString["pw"]);
				if ((!txtLoginId.Text.Equals("")) && (!txtPassword.Text.Equals("")) && (bookmark.Equals(""))) {
					cmdSubmit_Click(sender,e);
					return;
				}

				txtLoginId.Text = this.GetStringValue(Request.QueryString["loginid"]);
				txtPassword.Text = this.GetStringValue(Request.QueryString["password"]);
				txtPassword.Text = txtPassword.Text.Replace("?spare1=","");	// GIGA-POINTの戻りにゴミがつく
				if ((!txtLoginId.Text.Equals("")) && (!txtPassword.Text.Equals("")) && (bookmark.Equals(""))) {
					cmdSubmit_Click(sender,e);
				}
			} else if (!string.IsNullOrEmpty(snsType) && !string.IsNullOrEmpty(snsId)) {
				if (snsType.Equals(PwViCommConst.SnsType.TWITTER)) {
					string sPreviousFlag = string.Empty;
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_TWITTER_ID,snsId,"","",Session.SessionID,out userSeq,out userStatus,out sPreviousFlag,out loginResult);
					LoginResult(true);
				} else {
					string sPreviousFlag = string.Empty;
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_SNS_ACCOUNT_ID,"","","",Session.SessionID,out userSeq,out userStatus,out sPreviousFlag,out loginResult);
					LoginResult(true);
				}
			} else {
				if (this.GetStringValue(Request.QueryString["useutn"]).Equals("1")) {
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_UTN,utn,"","",Session.SessionID,out userSeq,out userStatus,out loginResult);
					LoginResult(true);
				} else {
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_IMODE_ID,id,"","",Session.SessionID,out userSeq,out userStatus,out loginResult);
					LoginResult(true);
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (sessionObj.isCrawler) {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				Session.Abandon();
				return;
			} else {
				return;
			}
		}
		if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
			using (User oUser = new User()) {
				if (oUser.IsAdminUserByTel(sessionMan.site.siteCd, txtLoginId.Text, txtPassword.Text)) {
					sessionMan.currentIModeId = string.Empty;
				}
			}
			// sessionMan.currentIModeIdは旧Ver対応(LOGIN_USER_MANでIMODE_IDがNULLの場合更新される)
			sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_TEL,sessionMan.currentIModeId,txtLoginId.Text,txtPassword.Text,Session.SessionID,out userSeq,out userStatus,out loginResult);
		} else {
			using (User oUser = new User()) {
				if (oUser.IsAdminUserByLoginID(sessionMan.site.siteCd, txtLoginId.Text, txtPassword.Text)) {
					sessionMan.currentIModeId = string.Empty;
				}
			}
			// sessionMan.currentIModeIdは旧Ver対応(LOGIN_USER_MANでIMODE_IDがNULLの場合更新される)
			string sPreviousFlag = string.Empty;
			sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_LOGINID,sessionMan.currentIModeId,txtLoginId.Text,txtPassword.Text,Session.SessionID,out userSeq,out userStatus,out sPreviousFlag,out loginResult);
			isPreviousLogin = ViCommConst.FLAG_ON_STR.Equals(sPreviousFlag);
		}
		LoginResult(false);
	}

	private void LoginResult(bool pUnt) {
		if (loginResult.Equals("0")) {
			string sGoto = isPreviousLogin ? string.Empty : this.GetStringValue(Request.QueryString["goto"]);
			string sInviate = this.GetStringValue(Request.QueryString["invite"]);
			string sInfoMail = this.GetStringValue(Request.QueryString["if"]);
			sessionMan.snsId = string.Empty;
			sessionMan.snsType = string.Empty;

			sessionMan.alreadyWrittenLoginLog = false;

			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || (sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionMan.adminFlg)) {
				HttpCookie cookie = new HttpCookie("maqia");
				cookie.Values["maqiauid"] = sessionMan.userMan.userSeq;
				cookie.Values["maqiapw"] = sessionMan.userMan.loginPassword;
				cookie.Values["maqiasex"] = ViCommConst.MAN;
				cookie.Expires = DateTime.Now.AddDays(90);
				cookie.Domain = sessionMan.site.hostNm;
				Response.Cookies.Add(cookie);
			}

			// Loginの確定により女性偽装分を破棄
			sessionMan.originalWomanObj = null;

			if (!sInfoMail.Equals("")) {
				if (!IsValidServicePoint(sInfoMail)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SERVICE_POINT_MAIL_INVALID));
					return;
				}
			}

			string sCastCharNo = "";
			if (sessionMan.site.charNoItemNm.Equals("") == false) {
				sCastCharNo = string.Format("&{0}={1}",sessionMan.site.charNoItemNm,this.GetStringValue(Request.QueryString[sessionMan.site.charNoItemNm]));
			}
			
			if (sessionMan.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS);
			}

			// Check Profile Regist Complite
			if (!sessionMan.site.siteCd.Substring(1,3).Equals("000")) {
				if (sessionMan.userMan.registIncompleteFlag == 2) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserBasicInfo.aspx"));
				} else if (sessionMan.userMan.registIncompleteFlag != 0) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserProfile.aspx"));
				}
			}

			bool bMailAppAccess = false;
			if (sessionMan.adminFlg == false) {
				using (ManNews oManNews = new ManNews()) {
					oManNews.LogManNews(sessionMan.site.siteCd,sessionMan.userMan.userSeq,PwViCommConst.ManNewsType.LOGIN);
				}

				if (!string.IsNullOrEmpty(sessionMan.deviceToken)) {
					using (User oUser = new User()) {
						oUser.UpdateDeviceToken(sessionMan.userMan.userSeq,sessionMan.deviceToken);
					}

					bMailAppAccess = true;

					sessionMan.deviceToken = string.Empty;
				}

				bool bCertifyNotificationApp = false;

				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) && IsAvailableService(ViCommConst.RELEASE_CERTIFY_APP_ANDROID,2)) {
					bCertifyNotificationApp = true;
				} else if (sessionMan.carrier.Equals(ViCommConst.IPHONE) && IsAvailableService(ViCommConst.RELEASE_CERTIFY_APP_IPHONE,2)) {
					bCertifyNotificationApp = true;
				}

				if (bCertifyNotificationApp) {
					if (!string.IsNullOrEmpty(sessionMan.deviceUuid)) {
						if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
							using (DeviceUuid oDeviceUuid = new DeviceUuid()) {
								oDeviceUuid.DeviceUuidMainte(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.deviceId,sessionMan.androidId,sessionMan.macAddress,sessionMan.deviceSerialNo,ref sessionMan.deviceUuid);
							}
						}
						using (User oUser = new User()) {
							oUser.CertifyNotificationApp(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.deviceUuid);
						}
						
						sessionMan.deviceUuid = string.Empty;
						sessionMan.deviceId = string.Empty;
						sessionMan.androidId = string.Empty;
						sessionMan.macAddress = string.Empty;
						sessionMan.deviceSerialNo = string.Empty;
					}
					if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
						if (!string.IsNullOrEmpty(sessionMan.deviceTel)) {
							if (SysPrograms.Expression(@"0(7|8|9)0\d{8}",sessionMan.deviceTel)) {
								using (User oUser = new User()) {
									oUser.UpdateSmartPhoneTel(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.deviceTel);
								}
							}
							sessionMan.deviceTel = string.Empty;
							
						}
					}
				} else {
					sessionMan.deviceUuid = string.Empty;
					sessionMan.deviceId = string.Empty;
					sessionMan.androidId = string.Empty;
					sessionMan.macAddress = string.Empty;
					sessionMan.deviceSerialNo = string.Empty;
					sessionMan.deviceTel = string.Empty;
				}
				
				if (!string.IsNullOrEmpty(sessionMan.accessMailTemplateNo) && !string.IsNullOrEmpty(sessionMan.accessTxMailDay)) {
					if (IsAvailableService(ViCommConst.RELEASE_ACCESS_MAIL_COUNT,2)) {
						using (AccessMailLog oAccessMailLog = new AccessMailLog()) {
							oAccessMailLog.RegistAccessMailLog(
								sessionMan.site.siteCd,
								sessionMan.userMan.userSeq,
								sessionMan.userMan.userCharNo,
								sessionMan.accessMailTemplateNo,
								ViCommConst.MAN,
								sessionMan.accessTxMailDay
							);
						}
					}
				}
				sessionMan.accessMailTemplateNo = string.Empty;
				sessionMan.accessTxMailDay = string.Empty;
			}

			switch (sGoto) {
				case "Profile.aspx":
					if (sInviate.Equals("")) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("Profile.aspx?direct=1&loginid={0}{1}&beforesystemid={2}",this.GetStringValue(Request.QueryString["castid"]),sCastCharNo,this.GetStringValue(Request.QueryString["beforesystemid"]))));
					} else {
						if (IsValidInvite(sInviate)) {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("Profile.aspx?direct=1&loginid={0}{1}&beforesystemid={2}",this.GetStringValue(Request.QueryString["castid"]),sCastCharNo,this.GetStringValue(Request.QueryString["beforesystemid"]))));
						} else {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TALK_INVITE_MAIL_INVALID));
						}
					}
					break;

				case "ListPersonalProfileMovie.aspx":
					string sDirectMovie = this.GetStringValue(Request.QueryString["directmovie"]);

					if (sDirectMovie.Equals(string.Empty)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListPersonalProfileMovie.aspx?loginid={0}&seekmode={1}&userrecno={2}{3}",this.GetStringValue(Request.QueryString["castid"]),ViCommConst.INQUIRY_CAST_MOVIE,1,sCastCharNo)));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListPersonalProfileMovie.aspx?direct={0}&loginid={1}&directmovie={2}{3}",ViCommConst.FLAG_ON_STR,this.GetStringValue(Request.QueryString["castid"]),sDirectMovie,sCastCharNo)));
					}
					break;

				case "SetupRxMail.aspx":
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"SetupRxMail.aspx"));
					break;

				case "ViewUserMail.aspx":
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ViewUserMail.aspx?direct=1&mailseq=" + this.GetStringValue(Request.QueryString["mailseq"])));
					break;

				case "ReturnMail.aspx":{
						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("mailseq",this.GetStringValue(Request.QueryString["mailseq"]));
						oParams.Add("direct",ViCommConst.FLAG_ON_STR);
						string sLinkName = string.Empty;

						if (IsAvailableService(ViCommConst.RELEASE_SP_LOGIN_RETURN_MAIL_CHAT,2)) {
							if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
								oParams.Add("scrid","01");
								oParams.Add("stay",ViCommConst.FLAG_ON_STR);
								sLinkName = "#btm";
							}
						}
						
						if (bMailAppAccess) {
							if (IsAvailableService(ViCommConst.RELEASE_MAIL_APP_ACCESS_BONUS,2)) {
								string sResult = string.Empty;
								using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
									oUserDefPoint.AddBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,"ACCESS_MAIL_APP",out sResult);
								}
								
								if (sResult.Equals(ViCommConst.UserDefPointResult.OK)) {
									oParams.Add("appbonus",ViCommConst.FLAG_ON_STR);
								}
							}
						}
						
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString() + sLinkName));
					}
					break;

				case "RequestIVP.aspx":
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"RequestIVP.aspx?" + Request.QueryString.ToString()));
					break;

				case "DisplayDoc.aspx":
					using (SiteHtmlDoc oDoc = new SiteHtmlDoc()) {
						List<string> oSexCdList = new List<string>();
						if (ViCommConst.FLAG_ON == sessionObj.site.siteHtmlDocSexCheckFlag) {
							oSexCdList.Add("3");
							if (sessionObj.logined) {
								oSexCdList.Add(sessionObj.sexCd);
							}
						}
						oDoc.GetOne(sessionMan.site.siteCd,loginResult,sessionMan.currentPageAgentType,ViCommConst.DEFUALT_PUB_DAY,oSexCdList.ToArray(),out sessionMan.errorMessage);
					}
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + this.GetStringValue(Request.QueryString["doc"])));
					break;

				case "TxMail.aspx":
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"TxMail.aspx?loginid=" + this.GetStringValue(Request.QueryString["castid"]) + "&userrecno=" + this.GetStringValue(Request.QueryString["userrecno"]) + sCastCharNo));
					break;

				case "ViewGame.aspx": {
						string sGameType = this.GetStringValue(Request.QueryString["game_type"]);
						string sScore = this.GetStringValue(Request.QueryString["score"]);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ViewGame.aspx?game_type={0}&score={1}",sGameType,sScore)));
					}
					break;
				case "ViewBlogArticle.aspx": {
						string sBlogArticleSeq = this.GetStringValue(Request.QueryString["blog_article_seq"]);
						string sMailSeq = this.GetStringValue(Request.QueryString["mailseq"]);

						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("blog_article_seq",sBlogArticleSeq);
						oParams.Add("mailseq",sMailSeq);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "ViewBlog.aspx": {
						string sLoginId = this.GetStringValue(Request.QueryString["castid"]);

						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("loginid",sLoginId);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "ViewBlogMail.aspx": {
						string sMailSeq = this.GetStringValue(Request.QueryString["mailseq"]);

						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("mailseq",sMailSeq);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "ViewProdAuction.aspx": {
						string sProdSeq = this.GetStringValue(Request.QueryString["product_seq"]);

						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("product_seq",sProdSeq);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "ListManTweetComment.aspx": {
						string sTweetSeq = this.GetStringValue(Request.QueryString["tweetseq"]);
						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("tweetseq",sTweetSeq);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "ListBbsObjFree.aspx": {
						string sUnusedFlag = this.GetStringValue(Request.QueryString["unused"]);
						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("unused",sUnusedFlag);
						oParams.Add("drows",this.GetStringValue(Request.QueryString["drows"]));
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "ListPickup.aspx": {
						Dictionary<string,string> oParams = new Dictionary<string,string>();
						oParams.Add("pickupid",this.GetStringValue(Request.QueryString["pickupid"]));
						if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["ranktype"]))) {
							oParams.Add("ranktype",this.GetStringValue(Request.QueryString["ranktype"]));
						}
						if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["orderasc"]))) {
							oParams.Add("orderasc",this.GetStringValue(Request.QueryString["orderasc"]));
						}
						if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["orderdesc"]))) {
							oParams.Add("orderdesc",this.GetStringValue(Request.QueryString["orderdesc"]));
						}
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "RequestTop.aspx": {
						Dictionary<string,string> oParams = new Dictionary<string,string>();
						if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["prg_seq"]))) {
							oParams.Add("prg_seq",this.GetStringValue(Request.QueryString["prg_seq"]));
						}
						if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["ctg_seq"]))) {
							oParams.Add("ctg_seq",this.GetStringValue(Request.QueryString["ctg_seq"]));
						}
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				case "DefectReportTop.aspx": {
						Dictionary<string,string> oParams = new Dictionary<string,string>();
						if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["drows"]))) {
							oParams.Add("drows",this.GetStringValue(Request.QueryString["drows"]));
						}
						RedirectToMobilePage(sessionMan.GetNavigateUrl(new UrlBuilder(sGoto,oParams).ToString()));
					}
					break;
				default:
					if (string.IsNullOrEmpty(sGoto) || sGoto.Equals("LoginUser.aspx")) {		// Goto=LoginUser.aspxでID,PWが設定されていない場合
						if (isPreviousLogin) {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.site.userTopId + "?oldloginid=1"));
						}

						string sParamFirst = string.Empty;

						if (bFirstFlag) {
							sParamFirst = "?first=1";
						}

						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.site.userTopId + sParamFirst));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sGoto));
					}
					break;
			}
		} else {
			lblErrorMessage.Text = string.Empty;

			//ブラックユーザーと退会ユーザーは別画面に遷移させる 
			if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_BLACK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_BLACK));
			} else if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_STOP)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_STOP));
			} else if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_RESIGNED)) {
				if (!userStatus.Equals(ViCommConst.USER_MAN_HOLD)) {
					//退会会員でﾛｸﾞｲﾝした場合、自動で復活させる
					string sResult = string.Empty;
					if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
						sessionMan.userMan.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_TEL,txtLoginId.Text,txtPassword.Text,out sResult);
					} else {
						sessionMan.userMan.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_LOGINID,txtLoginId.Text,txtPassword.Text,out sResult);
					}
					if (sResult.Equals("0")) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("LoginUser.aspx?loginid={0}&password={1}",txtLoginId.Text,txtPassword.Text)));
					}
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_RESIGNED));

				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"NonUserTop.aspx"));
				}
			} else if (loginResult.Equals(ViCommConst.ERR_FRAME_DUPLICATE_GUID)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_DUPLICATE_GUID));
			}

			if (pUnt == false) {
				lblErrorMessage.Text += GetErrorMessage(loginResult);
			}
		}
	}

	private bool IsValidInvite(string pKey) {
		string sManUserSeq,sCastUserSeq,sCastCharNo;
		int iServicePoint,iTransfered;
		DateTime dEffectTime;

		using (MailLog oMailLog = new MailLog()) {
			if (oMailLog.IsValidInvaiteMail(pKey,out sManUserSeq,out sCastUserSeq,out sCastCharNo,out iServicePoint,out dEffectTime,out iTransfered)) {
				if (sManUserSeq.Equals(sessionMan.userMan.userSeq)) {
					sessionMan.userMan.inviteTalkKey = pKey;
					sessionMan.userMan.inviteCastSeq = sCastUserSeq;
					sessionMan.userMan.inviteCastCharNo = sCastCharNo;
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

	private bool IsValidServicePoint(string pKey) {
		string sManUserSeq,sCastUserSeq,sCastCharNo;
		int iServicePoint,iTransfered;
		DateTime dEffectTime;

		using (MailLog oMailLog = new MailLog()) {
			bool bRet = oMailLog.IsValidInvaiteMail(pKey,out sManUserSeq,out sCastUserSeq,out sCastCharNo,out iServicePoint,out dEffectTime,out iTransfered);
			if (iTransfered != 0) {
				return true;
			}
			if (bRet == true) {
				if (sManUserSeq.Equals(sessionMan.userMan.userSeq)) {
					return sessionMan.userMan.TrasnServicePoint(pKey);
				} else {
					return true;
				}
			} else {
				return false;
			}
		}
	}

	//マルチスレッド問題回避
	private string GetStringValue(object value) {
		if (value == null) {
			return "";
		} else {
			return value.ToString();
		}
	}
}
