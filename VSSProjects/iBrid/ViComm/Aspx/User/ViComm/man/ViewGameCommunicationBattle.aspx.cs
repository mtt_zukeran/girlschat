/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE���ًL�^
--	Progaram ID		: ViewGameCommunicationBattle.aspx
--
--  Creation Date	: 2011.07.26
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_man_ViewGameCommunicationBattle:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sPartnerUserSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		rgx = new Regex("^[0-9]+$");
		rgxMatch = rgx.Match(sPartnerUserCharNo);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		if(!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_COMMUNICATION_VIEW,ActiveForm)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}
}
