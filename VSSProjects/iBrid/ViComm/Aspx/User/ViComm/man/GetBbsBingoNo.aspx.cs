/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝deビンゴNo獲得
--	Progaram ID		: GetBbsBingoNo
--  Creation Date	: 2013.11.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GetBbsBingoNo:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sObjSeq = iBridUtil.GetStringValue(Request.QueryString["objseq"]);
			int iCompleteFlag;
			string sResult = string.Empty;

			if (string.IsNullOrEmpty(sObjSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
				oBbsBingoTerm.GetBbsBingoNo(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sObjSeq,
					ViCommConst.FLAG_OFF,
					out iCompleteFlag,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_OK)) {
				string sNextUrl = string.Format("ViewBbsBingoLog.aspx?objseq={0}",sObjSeq);

				if (iCompleteFlag.Equals(ViCommConst.FLAG_ON)) {
					string sToUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)));
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sToUrl));
					return;
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sNextUrl));
					return;
				}
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_TERM)) {
				sessionMan.errorMessage = "お宝deﾋﾞﾝｺﾞは終了しました";
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_ENTRY)) {
				sessionMan.errorMessage = "ｴﾝﾄﾘｰしていません";
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_COMPLETE)) {
				sessionMan.errorMessage = "既にﾋﾞﾝｺﾞ完成しています。ｲﾍﾞﾝﾄﾍﾟｰｼﾞでBINGOﾎﾞﾀﾝを押して下さい";
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_NOT_VIEW)) {
				sessionMan.errorMessage = "先にお宝動画を閲覧して下さい";
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_FREE_VIEW)) {
				sessionMan.errorMessage = "無料で閲覧したものは対象外です";
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_OUT_TERM)) {
				sessionMan.errorMessage = "ｲﾍﾞﾝﾄ開始前に閲覧したものは対象外です";
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_OUT_ENTRY)) {
				sessionMan.errorMessage = "ｴﾝﾄﾘｰ前に閲覧したものは対象外です";
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_NG_EXIST_LOG)) {
				sessionMan.errorMessage = "既にBINGOﾅﾝﾊﾞｰ獲得済みです";
			}
		}
	}
}
