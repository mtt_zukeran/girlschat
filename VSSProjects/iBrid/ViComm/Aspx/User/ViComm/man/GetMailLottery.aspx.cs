/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ(一次)実行
--	Progaram ID		: GetMailLottery
--
--  Creation Date	: 2015.03.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GetMailLottery:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["mailseq"]);

		string sMailDataSeq = iBridUtil.GetStringValue(this.Request.QueryString["data"]);
		string sAttach = iBridUtil.GetStringValue(this.Request.QueryString["attach"]);

		if (string.IsNullOrEmpty(sMailSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		string sMailLotteryRateSeq = string.Empty;
		string sLoseFlag = string.Empty;
		string sResult = string.Empty;

		using (MailLottery oMailLottery = new MailLottery()) {
			oMailLottery.GetMailLottery(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sessionMan.sexCd,
				sMailSeq,
				string.Empty,
				ViCommConst.FLAG_OFF,
				out sMailLotteryRateSeq,
				out sLoseFlag,
				out sResult
			);
		}

		if (!sResult.Equals(PwViCommConst.GetMailLotteryResult.RESULT_NG)) {
			string sNextUrl = string.Format("DisplayDoc.aspx?doc={0}&result={1}",PwViCommConst.SCR_MAN_MAIL_LOTTERY_COMPLETE,sResult);

			if (sResult.Equals(PwViCommConst.GetMailLotteryResult.RESULT_OK)) {
				sNextUrl = string.Format("{0}&lottery={1}&lose={2}",sNextUrl,sMailLotteryRateSeq,sLoseFlag);

				if (!sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
					if (IsAvailableService(ViCommConst.RELEASE_MAIL_LOTTERY_MOVIE,2)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ViewFlashMailLottery.aspx?next_url={0}&lose={1}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)),sLoseFlag)));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
					}
				} else {
					if (IsAvailableService(ViCommConst.RELEASE_MAIL_LOTTERY_MOVIE_IPHONE,2)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ViewFlashMailLottery.aspx?next_url={0}&lose={1}",HttpUtility.UrlEncode(sNextUrl,System.Text.Encoding.GetEncoding(932)),sLoseFlag)));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
					}
				}
			} else if (sResult.Equals(PwViCommConst.GetMailLotteryResult.RESULT_NO_ATTACH)) {

				string sDoc = string.Empty;
				if (sAttach.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
					sDoc = ViCommConst.SCR_MAN_PIC_TX_MAILER_COMPLITE;
					
					if (!string.IsNullOrEmpty(sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq)) {
						sDoc = ViCommConst.SCR_MAN_PIC_RETURN_MAILER_COMPLITE;
					}
					
					RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&lotteryreturn=1",sDoc,sMailDataSeq,sMailSeq)));
				} else {
					sDoc = ViCommConst.SCR_MAN_MOVIE_TX_MAILER_COMPLITE;

					if (!string.IsNullOrEmpty(sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq)) {
						sDoc = ViCommConst.SCR_MAN_MOVIE_RETURN_MAILER_COMPLITE;
					}

					RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("DisplayDoc.aspx?doc={0}&data={1}&mailseq={2}&lotteryreturn=1",sDoc,sMailDataSeq,sMailSeq)));
				}
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
			}

		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}
