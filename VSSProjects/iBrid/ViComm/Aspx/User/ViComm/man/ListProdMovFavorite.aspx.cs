﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品写真一覧
--	Progaram ID		: ListProdPicFavorit
--
--  Creation Date	: 2010.12.20
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;
public partial class ViComm_man_ListProdMovFavorite : MobileProdManPage {
	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					this.Request,
					ViCommConst.INQUIRY_PRODUCT_MOVIE_FAVORITE,
					this.ActiveForm);

		}
	}
}
