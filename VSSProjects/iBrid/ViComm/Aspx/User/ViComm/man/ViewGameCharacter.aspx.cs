/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女の子詳細
--	Progaram ID		: ViewGameCharacter.aspx
--
--  Creation Date	: 2011.08.09
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameCharacter:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		
		if (!IsPostBack) {
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
			
			if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

			string sNaFlag = this.GetNaFlag(sPartnerUserSeq,sPartnerUserCharNo);
			if (!sNaFlag.Equals(ViCommConst.NaFlag.OK.ToString()) && !sNaFlag.Equals(ViCommConst.NaFlag.NO_AUTH.ToString())) {
				RedirectToGameDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
			}

			this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

			if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["favorite_ins"])) {
				GameFavoriteMainte(sPartnerUserSeq,sPartnerUserCharNo,0);
			} else if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["favorite_del"])) {
				GameFavoriteMainte(sPartnerUserSeq,sPartnerUserCharNo,1);
			} else {
				if(!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}
	}

	private void GameFavoriteMainte(string pPartnerUserSeq,string pPartnerUserCharNo,int pDelFlag) {
		using (GameFavorite oGameFavorite = new GameFavorite()) {
			oGameFavorite.GameFavoriteMainte(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				pDelFlag
			);
		}
		
		IDictionary<string,string> oParameters = new Dictionary<string,string>();
		oParameters.Add("del_flag",pDelFlag.ToString());
		oParameters.Add("partner_user_seq",pPartnerUserSeq);
		oParameters.Add("partner_user_char_no",pPartnerUserCharNo);
		RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_FAVORITE_COMPLETE,oParameters);
	}

	private string GetNaFlag(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sNaFlag = string.Empty;
		DataSet oDataSet;
		using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
			oDataSet = oCastGameCharacter.GetOne(sessionMan.userMan.siteCd,pPartnerUserSeq,pPartnerUserCharNo);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			sNaFlag = oDataSet.Tables[0].Rows[0]["NA_FLAG"].ToString();
		}
		return sNaFlag;
	}
}