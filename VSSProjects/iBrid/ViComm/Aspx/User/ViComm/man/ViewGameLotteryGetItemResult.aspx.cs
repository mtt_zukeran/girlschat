/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@��������
--	Progaram ID		: ViewGameLotteryGetItemResult
--
--  Creation Date	: 2011.09.09
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public partial class ViComm_man_ViewGameLotteryGetItemResult:MobileSocialGameManBase {
	private NameValueCollection query = new NameValueCollection();

	protected void Page_Load(object sender,EventArgs e) {
		string sLotterySeq = iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sLotterySeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		if(iBridUtil.GetStringValue(this.Request.QueryString["result"]).Equals(PwViCommConst.GameLotteryResult.RESULT_OK)) {
			rgx = new Regex("^[0-9]+$");
			rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["get_item_seq"]));
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}

		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_RESULT,ActiveForm);
		} else {
			if(!string.IsNullOrEmpty(this.Request.Params["cmdNext"])) {
				string sSiteCd = this.sessionMan.site.siteCd;
				string sUserSeq = this.sessionMan.userMan.userSeq;
				string sUserCharNo = this.sessionMan.userMan.userCharNo;
				string sSexCd = this.sessionMan.sexCd;
				string sGetGameItemSeq = string.Empty;
				string sCompleteGetItemSeq = string.Empty;
				int iLotteryCompleteFlag;
				int iLotteryCompLeftCount;
				string sResult = string.Empty;
				string sPrePoint = sessionMan.userMan.balPoint.ToString();
				string sScrid = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);
				string sPreMaxAttackPower = sessionMan.userMan.gameCharacter.maxAttackPower.ToString();
				string sPreMaxDefencePower = sessionMan.userMan.gameCharacter.maxDefencePower.ToString();
				
				using(Lottery oLottery = new Lottery()) {
					oLottery.GetLottery(
						sSiteCd,
						sUserSeq,
						sUserCharNo,
						sSexCd,
						sLotterySeq,
						out sGetGameItemSeq,
						out sCompleteGetItemSeq,
						out iLotteryCompleteFlag,
						out iLotteryCompLeftCount,
						out sResult
					);
				}

				if (!sResult.Equals(PwViCommConst.GameLotteryResult.RESULT_NG)) {
					StringBuilder oUrl = new StringBuilder();

					if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
						if(sResult.Equals(PwViCommConst.GameLotteryResult.RESULT_OK)) {
							IDictionary<string,string> oParameters = new Dictionary<string,string>();

							oParameters.Add("lottery_seq",sLotterySeq);
							oParameters.Add("get_item_seq",sGetGameItemSeq);
							oParameters.Add("comp_get_seq",sCompleteGetItemSeq);
							oParameters.Add("comp_flag",iLotteryCompleteFlag.ToString());
							oParameters.Add("result",sResult);
							oParameters.Add("pre_point",sPrePoint);
							oParameters.Add("pre_max_attack_power",sPreMaxAttackPower);
							oParameters.Add("pre_max_defence_power",sPreMaxDefencePower);

							RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_TICKET_LOTTERY_SP,oParameters);
						} else {
							oUrl.Append("ViewGameLotteryGetItemResult.aspx");
							oUrl.AppendFormat("?lottery_seq={0}",sLotterySeq);
							oUrl.AppendFormat("&get_item_seq={0}",sGetGameItemSeq);
							oUrl.AppendFormat("&comp_get_seq={0}",sCompleteGetItemSeq);
							oUrl.AppendFormat("&comp_flag={0}",iLotteryCompleteFlag.ToString());
							oUrl.AppendFormat("&result={0}",sResult);
							oUrl.AppendFormat("&pre_point={0}",sPrePoint);
							oUrl.AppendFormat("&pre_max_attack_power={0}",sPreMaxAttackPower);
							oUrl.AppendFormat("&pre_max_defence_power={0}",sPreMaxDefencePower);

							if (!string.IsNullOrEmpty(sScrid)) {
								oUrl.AppendFormat("&scrid={0}",sScrid);
							}
							
							string sRedirectUrl = oUrl.ToString();

							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
						}
					} else {
						if (sResult.Equals(PwViCommConst.GameLotteryResult.RESULT_OK)) {
							oUrl.Append("ViewGameFlashLottery.aspx");
						} else {
							oUrl.Append("ViewGameLotteryGetItemResult.aspx");
						}
						oUrl.AppendFormat("?lottery_seq={0}",sLotterySeq);
						oUrl.AppendFormat("&get_item_seq={0}",sGetGameItemSeq);
						oUrl.AppendFormat("&comp_get_seq={0}",sCompleteGetItemSeq);
						oUrl.AppendFormat("&comp_flag={0}",iLotteryCompleteFlag.ToString());
						oUrl.AppendFormat("&result={0}",sResult);
						oUrl.AppendFormat("&pre_point={0}",sPrePoint);
						oUrl.AppendFormat("&pre_max_attack_power={0}",sPreMaxAttackPower);
						oUrl.AppendFormat("&pre_max_defence_power={0}",sPreMaxDefencePower);

						if (!string.IsNullOrEmpty(sScrid)) {
							oUrl.AppendFormat("&scrid={0}",sScrid);
						}
						
						string sRedirectUrl = oUrl.ToString();

						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
					}
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}
	}
}
