/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ相手一覧(写真保持)
--	Progaram ID		: ListGameCharacterBattleTreasure
--
--  Creation Date	: 2011.08.02
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameCharacterBattleTreasure:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sCastGamePicSeq = iBridUtil.GetStringValue(this.Request.QueryString["cast_game_pic_seq"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sCastGamePicSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		if(!this.CheckExistsManTreasure(sCastGamePicSeq)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_TREASURE_LIST,ActiveForm);
		}
	}
	
	private bool CheckExistsManTreasure(string sCastGamePicSeq) {
		ManTreasureSeekCondition oCondition = new ManTreasureSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.CastGamePicSeq = sCastGamePicSeq;
		oCondition.UserSeq = this.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_TREASURE_LIST;
		
		DataSet ds = null;
		
		using(ManTreasure oManTreasure = new ManTreasure()) {
			ds = oManTreasure.GetPageCollection(oCondition,1,1);
		}
		
		if(ds.Tables[0].Rows.Count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
