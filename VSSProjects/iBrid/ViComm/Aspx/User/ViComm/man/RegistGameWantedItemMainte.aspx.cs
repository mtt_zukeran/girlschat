/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�~������ؽēo�^�A����
--	Progaram ID		: RegistGameWantedItemMainte.aspx
--
--  Creation Date	: 2011.08.20
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameWantedItemMainte:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);

		if (!IsPostBack) {
			string sGameItemSeq = iBridUtil.GetStringValue(Request.QueryString["game_item_seq"]);
			string sWantedItemSeq = iBridUtil.GetStringValue(Request.QueryString["wanted_item_seq"]);
			string sDelFlag = iBridUtil.GetStringValue(Request.QueryString["del_flag"]);

			if ((!sGameItemSeq.Equals(string.Empty) && sDelFlag.Equals(ViCommConst.FLAG_OFF_STR)) || (!sWantedItemSeq.Equals(string.Empty) && sDelFlag.Equals(ViCommConst.FLAG_ON_STR))) {
				if (sDelFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sGameItemSeq = GetGameItemSeq(sWantedItemSeq);
				}
				string sResult = WantedItemMainte(sWantedItemSeq,sGameItemSeq,sDelFlag);
				if (!sResult.Equals(PwViCommConst.WantedItemManteResult.RESULT_NG)) {
					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("result",sResult);
					oParameters.Add("game_item_seq",sGameItemSeq);
					oParameters.Add("del_flag",sDelFlag);
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_WANTED_ITEM_MAINTE_RESULT,oParameters);
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}

	private string GetGameItemSeq(string pWantedItemSeq) {
		string sGameItemSeq = string.Empty;
		using (WantedItem oWantedItem = new WantedItem()) {
			sGameItemSeq = oWantedItem.GetGameItemSeq(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,pWantedItemSeq);
		}
		return sGameItemSeq;
	}

	private string WantedItemMainte(string pWantedItemSeq,string pGameItemSeq,string pDelFlag) {
		string sResult = string.Empty;

		using (WantedItem oWantedItem = new WantedItem()) {
			sResult = oWantedItem.WantedItemMainte(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				ViCommConst.MAN,
				pWantedItemSeq,
				pGameItemSeq,
				pDelFlag
			);
		}

		return sResult;
	}
}
