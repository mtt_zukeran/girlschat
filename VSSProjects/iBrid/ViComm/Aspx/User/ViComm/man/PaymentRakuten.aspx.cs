/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 楽天あんしん支払い
--	Progaram ID		: PaymentRakuten
--
--  Creation Date	: 2010.08.24
--  Creater			: K.Itoh
--
**************************************************************************/
// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain
  yyyy/mm/dd	XXXXXXXXX
-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentRakuten : MobileManPageBase {
	
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		if(!this.IsPostBack){
		
			// 購入ポイントDropDownの設定
			using (DataSet ds = GetPackDataSet(ViCommConst.SETTLE_RAKUTEN,0)) {
					foreach (DataRow dr in ds.Tables[0].Rows) {
						lstCreditPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(), dr["SALES_AMT"].ToString()));
					}
				}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if(this.lstCreditPack.SelectedIndex < 0)return;

		string sSalesAmt = this.lstCreditPack.Items[this.lstCreditPack.SelectedIndex].Value;

		UrlBuilder oUrlBuilder = new UrlBuilder(sessionMan.GetNavigateUrl("PaymentRakutenConfirm.aspx"));
		oUrlBuilder.Parameters.Add("sales_amt",sSalesAmt);
		RedirectToMobilePage(oUrlBuilder.ToString());
	}

}
