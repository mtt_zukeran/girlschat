/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: フリーダイアル利用設定
--	Progaram ID		: SetupFreeDial
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_SetupFreeDial:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sSetupFlag = iBridUtil.GetStringValue(Request.QueryString["setup"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));

			if (!string.IsNullOrEmpty(sSetupFlag)) {
				this.SetupFreeDial(sSetupFlag.Equals(ViCommConst.FLAG_ON_STR),sBackUrl);
			} else {
				if (sessionMan.userMan.useFreeDialFlag != 0) {
					chkUseFreeDiall.SelectedIndex = 0;
				}
				if (Request.Params["usefreedial"] != null) {
					this.SetupFreeDial(ViCommConst.FLAG_ON_STR.Equals(Request.Params["usefreedial"]),"");
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		this.SetupFreeDial(chkUseFreeDiall.SelectedIndex == 0,"");
	}

	private void SetupFreeDial(bool pSetupFlag,string pBackUrl) {
		sessionMan.userMan.SetupFreeDial(
			sessionMan.userMan.userSeq,
			pSetupFlag
		);

		if (!string.IsNullOrEmpty(pBackUrl)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(pBackUrl));
			return;
		} else {
			if (pSetupFlag) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.SCR_USE_FREEDIAL_COMPLITE));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + ViCommConst.SCR_NON_USE_FREEDIAL_COMPLITE));
			}
		}
	}
}
