/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: サイト文章表示
--	Progaram ID		: DisplayDoc
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Text;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Configuration;

public partial class ViComm_man_DisplayDoc:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		if (IsAvailableService(ViCommConst.RELEASE_UPDATE_SESSION)) {
			if (sessionMan.logined) {
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			}
		}
		if (iBridUtil.GetStringValue(Request.Params[ViCommConst.ACTION_REDIRECT]).Equals(ViCommConst.FLAG_ON_STR)) {
			cmdNextLink_Click(sender,e);
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {


			using (SiteHtmlDoc oDoc = new SiteHtmlDoc()) {
				System.Collections.Generic.List<string> oSexCdList = new System.Collections.Generic.List<string>();
				if (ViCommConst.FLAG_ON == sessionObj.site.siteHtmlDocSexCheckFlag) {
					oSexCdList.Add("3");
					if(sessionObj.logined){
						oSexCdList.Add(sessionObj.sexCd);
					}
				}
				string sHtml;
				if (iBridUtil.GetStringValue(Request.QueryString["sc"]) == "") {
					oDoc.GetOne(sessionMan.site.siteCd,iBridUtil.GetStringValue(Request.QueryString["doc"]),sessionMan.currentPageAgentType,ViCommConst.DEFUALT_PUB_DAY,oSexCdList.ToArray(),out sHtml);
				} else {
					oDoc.GetOne(iBridUtil.GetStringValue(Request.QueryString["sc"]),iBridUtil.GetStringValue(Request.QueryString["doc"]),sessionMan.currentPageAgentType,ViCommConst.DEFUALT_PUB_DAY,oSexCdList.ToArray(),out sHtml);
				}
				if (string.IsNullOrEmpty(sHtml)) {
					sHtml = iBridUtil.GetStringValue(Request.QueryString["doc"]);
				}
				tag01.Text = sHtml;
			}
			string sAction = iBridUtil.GetStringValue(this.Request.QueryString["frmaction"]);
			if (!string.IsNullOrEmpty(sAction)) {
				this.frmMain.Action = sAction;
				this.EnableViewState = false;
			}
			string sMethod = iBridUtil.GetStringValue(this.Request.QueryString["frmmethod"]);
			if (sMethod.ToLower().Equals("get")) {
				this.frmMain.Method = System.Web.UI.MobileControls.FormMethod.Get;
			}

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				cmdNextLink_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_FINDUSER] != null) {
				this.FindUser();
			} else if (Request.Params[ViCommConst.BUTTON_RESET_MAIL_NG] != null) {
				this.ResetMailNg();
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		StringBuilder oRequest = new StringBuilder();
		string sKeyInput = string.Empty;

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.StartsWith("*")) {
				sKeyInput = sKey.Replace("*",string.Empty);
				oRequest.Append(string.Format("&{0}={1}&{2}={3}",sKey,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKey]),enc),sKeyInput,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKeyInput]),enc)));
			} else if (sKey.StartsWith("?")) {
				sKeyInput = sKey.Replace("?",string.Empty);
				oRequest.Append(string.Format("&{0}={1}&{2}={3}",sKey,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKey]),enc),sKeyInput,HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKeyInput]),enc)));
			}
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("MailFormConfirm.aspx?doc={0}&error={1}&success={2}&send={3}&title={4}{5}",
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["doc"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["error"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["success"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["send"]),enc),
																																HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form["title"]),enc),
																																oRequest.ToString())));

	}

	protected void cmdNextLink_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sNextUrl = iBridUtil.GetStringValue(Request.Params["NEXTLINK"]);
		string sAddPointId = iBridUtil.GetStringValue(Request.Params["ADD_POINT_ID"]);

		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableRedirectAddBonusPoint"]).Equals(ViCommConst.FLAG_ON_STR)) {
			if (sessionObj.logined && !string.IsNullOrEmpty(sAddPointId)) {
				using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
					string sResult = string.Empty;
					oUserDefPoint.AddBonusPoint(sessionObj.site.siteCd,sessionObj.GetUserSeq(),sAddPointId,out sResult);
				}
			}
		}
		string sKeyInput = string.Empty;
		StringBuilder oRequest = new StringBuilder();

		foreach (string sKey in Request.Params.AllKeys) {
			if (sKey.StartsWith("*")) {
				oRequest.Append(string.Format("&{0}={1}",sKey.Replace("*",string.Empty),HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.Form[sKey]),enc)));
			}
		}

		string sSep = sNextUrl.Contains("?") ? string.Empty : "?";

		if (sNextUrl.StartsWith("http://") || sNextUrl.StartsWith("https://")) {
			RedirectToMobilePage(sNextUrl + sSep + oRequest.ToString());
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl + sSep + oRequest.ToString()));
		}
	}

	private void FindUser() {
		UrlBuilder oUrlBuilder = new UrlBuilder("ListFindResult.aspx");
		using (CastAttrType oCastAttrType = new CastAttrType())
		using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue()) {

			foreach (string sKey in this.Request.Params.AllKeys) {
				if (sKey == null)
					continue;

				if (!sKey.StartsWith("item"))
					continue;

				string sItemNo = sKey.Replace("item",string.Empty);
				string sValue = HttpUtility.UrlDecode(this.Request.Params[sKey]);
				if (string.IsNullOrEmpty(sValue))
					continue;

				if (sValue.Equals(":"))
					continue;

				string[] sAttrItemNoArry = sValue.Split(':');

				string sAttrTypeSeq = oCastAttrType.GetSeqByItemNo(sessionMan.site.siteCd,sItemNo);
				if (string.IsNullOrEmpty(sAttrTypeSeq))
					continue;

				System.Text.StringBuilder oSb = new System.Text.StringBuilder();
				for (int i = 0;i < sAttrItemNoArry.Length;i++) {
					if (i != 0)
						oSb.Append(",");
					oSb.Append(oCastAttrTypeValue.GetSeqByItemNo(sessionMan.site.siteCd,sAttrTypeSeq,sAttrItemNoArry[i]));
				}
				oUrlBuilder.Parameters.Add("item" + sItemNo,oSb.ToString());
				oUrlBuilder.Parameters.Add("typeseq" + sItemNo,sAttrTypeSeq);
			}
		}
		RedirectToMobilePage(SessionObjs.Current.GetNavigateUrl(oUrlBuilder.ToString()));
	}

	private void ResetMailNg() {
		using (User oUser = new User()) {
			oUser.ResetMailNg(sessionMan.userMan.userSeq);
		}
		RedirectToDisplayDoc(ViCommConst.SCR_MAN_RESET_MAIL_NG_COMPLETE);
	}
}
