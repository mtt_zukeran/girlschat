/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����۸޲��ްŽSP
--	Progaram ID		: RegistGameGetLoginBonus
--
--  Creation Date	: 2011.08.09
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_RegistGameGetLoginBonus:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!sessionMan.logined) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByTermIdGame.aspx"));
			return;
		}

		this.GetLoginBonusExecute();
	}

	private void GetLoginBonusExecute() {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		int iDailyBonusFlag;
		int iMonthlyBonusFlag;
		string sResult;

		using (GameLoginBonusMonthly oGameLoginBonusMontly = new GameLoginBonusMonthly()) {
			oGameLoginBonusMontly.GetLoginBonus(
				sSiteCd							,
				sUserSeq						,
				sUserCharNo						,
				out iDailyBonusFlag				,
				out iMonthlyBonusFlag			,
				out sResult						
			);
		}

		if (sResult.Equals(PwViCommConst.GameGetLoginBonusResult.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		} else {
			string query = string.Empty;

			if (sResult.Equals(PwViCommConst.GameGetLoginBonusResult.RESULT_OK)) {
				query = string.Format("?daily_flag={0}&monthly_flag={1}",iDailyBonusFlag.ToString(),iMonthlyBonusFlag.ToString());
			}

			string sRedirectUrl = "ListGameLoginBonusMonthly.aspx" + query;

			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sRedirectUrl));
		}
	}
}