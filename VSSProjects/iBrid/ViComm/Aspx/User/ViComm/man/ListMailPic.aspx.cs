/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �Y�t�ʐ^�I���ꗗ
--	Progaram ID		: ListMailPic
--
--  Creation Date	: 2009.08.19
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListMailPic:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
				Request,
				ViCommConst.INQUIRY_NONE_DISP_PROFILE,
				ActiveForm);
		}
	}
}