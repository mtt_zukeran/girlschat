<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentEdySophia.aspx.cs" Inherits="ViComm_man_PaymentEdySophia" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagEdySettleTop" runat="server" Text="$PGM_HTML02;" />
		<cc1:iBMobileLiteralText ID="tagEdyconfirmTop" runat="server" Text="$PGM_HTML03;" />
		<mobile:Panel ID="pnlEdySettle" Runat="server">
			<cc1:iBMobileLiteralText ID="DivS01" runat="server" Text="<DIV align=center>"/>
			<mobile:SelectionList ID="lstEdyPack" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="DivE01" runat="server" Text="</DIV>"/>
			$PGM_HTML06;
			<mobile:Command ID="cmdSubmitConf" Runat="server" Alignment="Center" OnClick="cmdSubmitConf_Check">確認</mobile:Command>
		</mobile:Panel>
		<mobile:Panel ID="pnlEdyConfirm" Runat="server">
			<cc1:iBMobileLabel ID="lblSettlePayment" runat="server" Alignment="Center"/>
			<mobile:Command ID="cmdSubmit" Runat="server" Alignment="Center" OnClick="cmdSubmit_Click">申込み</mobile:Command>
		</mobile:Panel>
		<cc1:iBMobileLiteralText ID="tagEdySettleFooter" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLiteralText ID="tagEdyConfirmFooter" runat="server" Text="$PGM_HTML05;" />
		$PGM_HTML09;
		<cc1:iBMobileLabel ID="lblMblyzerTag" runat="server"></cc1:iBMobileLabel>
	</mobile:Form>
</body>
</html>
