<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WriteManTweet.aspx.cs" Inherits="ViComm_man_WriteManTweet" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagTweetWrite" runat="server" Text="$PGM_HTML02;" />
		<tx:TextArea ID="txtTweet" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		<cc1:iBMobileLiteralText ID="tagTweetWriteSub" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLiteralText ID="tagTweetComplete" runat="server" Text="$PGM_HTML04;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>