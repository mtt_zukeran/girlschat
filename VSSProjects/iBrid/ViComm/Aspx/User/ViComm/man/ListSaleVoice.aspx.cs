/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 販売着ﾎﾞｲｽ一覧
--	Progaram ID		: ListSaleMovie
--
--  Creation Date	: 2010.07.22
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

/// <summary>
/// 「販売着ボイス一覧」画面
/// </summary>
public partial class ViComm_man_ListSaleVoice : MobileManPageBase {
	
	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_SALE_VOICE,
					ActiveForm);
		}
	}
}
