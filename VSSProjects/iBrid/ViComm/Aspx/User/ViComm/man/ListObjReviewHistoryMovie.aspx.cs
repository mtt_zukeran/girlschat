/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 動画レビュー一覧
--	Progaram ID		: ListObjReviewHistoryMovie
--
--  Creation Date	: 2013.09.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListObjReviewHistoryMovie:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.IsImpersonated == false) {
				if (sessionMan.logined == false) {
					if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
					}
					return;
				}
			}

			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY_MOVIE,this.ActiveForm);
		}
	}
}
