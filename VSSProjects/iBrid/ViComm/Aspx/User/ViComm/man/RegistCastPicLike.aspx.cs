/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o���҉摜�C�C�l�o�^
--	Progaram ID		: RegistCastPicLike
--  Creation Date	: 2013.12.23
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistCastPicLike:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);


		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));
			string sResult = string.Empty;

			if (string.IsNullOrEmpty(sPicSeq) || string.IsNullOrEmpty(sBackUrl)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			using (CastPicLike oCastPicLike = new CastPicLike()) {
				oCastPicLike.RegistCastPicLike(sessionMan.site.siteCd,sPicSeq,sessionMan.userMan.userSeq,out sResult);
			}

			if (sResult.Equals(PwViCommConst.RegistCastPicLikeResult.RESULT_OK)) {
				if (IsAvailableService(ViCommConst.RELEASE_CAST_LIKE_NOTICE,2)) {
					using (CastPic oCastPic = new CastPic())
					using (CastLikeNotice oCastLikeNotice = new CastLikeNotice()) {
						DataSet oCastDataSet = oCastPic.GetOneByPicSeq(sessionMan.site.siteCd,sPicSeq);
						
						if (oCastDataSet.Tables[0].Rows.Count > 0) {
							string sCastSeq = iBridUtil.GetStringValue(oCastDataSet.Tables[0].Rows[0]["USER_SEQ"]);
							string sCastCharNo = iBridUtil.GetStringValue(oCastDataSet.Tables[0].Rows[0]["USER_CHAR_NO"]);
							oCastLikeNotice.CastLikeNoticeMainte(
								sessionMan.site.siteCd,
								sCastSeq,
								sCastCharNo,
								PwViCommConst.CastLikeNotice.CONTENTS_PROFILE_PIC,
								sPicSeq,
								string.Empty,
								string.Empty,
								sessionMan.userMan.userSeq
							);
						}
					}
				}
				
				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sBackUrl));
					return;
				} else {
					pnlComplete.Visible = true;
				}
			} else if (sResult.Equals(PwViCommConst.RegistCastPicLikeResult.RESULT_NG_LIKED)) {
				sessionMan.errorMessage = "���ɲ�ȍς݂ł�";
				pnlError.Visible = true;
			} else if (sResult.Equals(PwViCommConst.RegistCastPicLikeResult.RESULT_NG_CAST_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.ERR_REFUSE_MAN);
				return;
			} else if (sResult.Equals(PwViCommConst.RegistCastPicLikeResult.RESULT_NG_SELF_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
				return;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
