/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザーメール詳細表示
--	Progaram ID		: ViewUserMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewUserMail:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		bool bDownload = false;

		if (!IsPostBack) {
			string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
			bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

			if (!sMovieSeq.Equals("") && (sPlayMovie.Equals("1") || bIsCheckOnly)) {
				bDownload = Download(sCastSeq, sCastCharNo, sMovieSeq);
			}
			if (bDownload == false) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				ulong uSeekMode;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode) == false) {
					uSeekMode = ViCommConst.INQUIRY_RX_USER_MAIL_BOX;
				}

				if (sessionMan.ControlList(Request,uSeekMode,ActiveForm)) {

					string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
					string sObjType = sessionMan.GetMailValue("ATTACHED_OBJ_TYPE");

					if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailDel(sMailSeq,ViCommConst.RX);
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_CAST_MAIL + "&mail_delete_count=1"));
						}
					} else {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailReadFlag(sMailSeq);
						}
					}
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListUserMailBox.aspx"));
				}

				if (sessionMan.site.attachedMailDupChargeFlag == 1) {
					sessionMan.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","0");
				}
			}
		} else {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdProtect"] != null) {
				cmdProtect_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sCastSeq,sCastCharNo;
		int iChargePoint = 0;
		bool bOk = true;

		sessionMan.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm);
		sessionMan.errorMessage = "";

		sCastSeq = sessionMan.GetMailValue("TX_USER_SEQ");
		sCastCharNo = sessionMan.GetMailValue("TX_USER_CHAR_NO");
		string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
		string sOpenedFlag = sessionMan.GetMailValue("ATTACHED_OBJ_OPEN_FLAG");
		string sObjType = sessionMan.GetMailValue("ATTACHED_OBJ_TYPE");

		if ((!sOpenedFlag.Equals("1") || sessionMan.site.attachedMailDupChargeFlag == 1) && sObjType.Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
			if (sessionMan.CheckPicMailBalance(sCastSeq,sCastCharNo,out iChargePoint) == false) {
				bOk = false;
			} else {
				using (WebUsedLog oLog = new WebUsedLog()) {
					oLog.CreateWebUsedReport(
							sessionMan.userMan.siteCd,
							sessionMan.userMan.userSeq,
							ViCommConst.CHARGE_MAIL_WITH_PIC,
							iChargePoint,
							sCastSeq,
							sCastCharNo,
							"");
				}
				using (MailBox oMailBox = new MailBox()) {
					oMailBox.UpdateMailAttaChedOpenFlag(sMailSeq);
					sessionMan.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","1");
				}
			}
		}

		if (bOk == false) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_ATTACHED_PIC));
		}
	}

	protected void cmdProtect_Click(object sender,EventArgs e) {
		sessionMan.ControlList(Request,ViCommConst.INQUIRY_RX_USER_MAIL_BOX,ActiveForm);
		int iProtectFlag = int.Parse(sessionMan.GetMailValue("DEL_PROTECT_FLAG")) == 1 ? 0 : 1;
		string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sMailSeq,iProtectFlag);
			sessionMan.SetMailValue("DEL_PROTECT_FLAG",iProtectFlag.ToString());
		}
	}

	private bool Download(string pCastSeq,string pCastCharNo,string pMovieSeq) {
		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)sessionMan.parseContainer.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);

				string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
				string sOpenedFlag = sessionMan.GetMailValue("ATTACHED_OBJ_OPEN_FLAG");
				string sObjType = sessionMan.GetMailValue("ATTACHED_OBJ_TYPE");

				if(!MovieHelper.IsRangeRequest()){
					if (!sOpenedFlag.Equals("1") || sessionMan.site.attachedMailDupChargeFlag == 1) {
						int iChargePoint;
						if (sessionMan.CheckMovieMailBalance(pCastSeq,pCastCharNo,out iChargePoint) == false) {
							sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_ATTACHED_MOVIE));
							return false;
						}

						if (bIsCheckOnly) return false;

						using (WebUsedLog oLog = new WebUsedLog()) {
							oLog.CreateWebUsedReport(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.CHARGE_MAIL_WITH_MOVIE,
									iChargePoint,
									pCastSeq,
									pCastCharNo,
									"");
						}
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailAttaChedOpenFlag(sMailSeq);
						}
						sessionMan.SetMailValue("ATTACHED_OBJ_OPEN_FLAG","1");
					}
				}

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}
}
