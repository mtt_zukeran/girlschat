/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@�����E�ڸ��ݼ��
--	Progaram ID		: ListGameLotteryGetItem
--
--  Creation Date	: 2011.09.12
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameLotteryGetItem:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["lottery_seq"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_LOTTERY_GET_ITEM_LIST,ActiveForm);
		}
	}
}
