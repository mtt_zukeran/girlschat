/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　他人の持ち物一覧
--	Progaram ID		: ListGameItemPossessionPartner
--
--  Creation Date	: 2011.08.04
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameItemPossessionPartner:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["partner_user_seq"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		rgx = new Regex("^[0-9]+$");
		rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["partner_user_char_no"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		if (sessionMan.userMan.userSeq.Equals(this.Request.QueryString["partner_user_seq"]) &&
			sessionMan.userMan.userCharNo.Equals(this.Request.QueryString["partner_user_char_no"])
		) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_POSSESSION_PARTNER_LIST,ActiveForm);
		}
	}
}