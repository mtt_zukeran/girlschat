/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: BITCASH支払
--	Progaram ID		: PaymentBitCash
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Configuration;
using CookComputing.XmlRpc;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public partial class ViComm_man_PaymentBitCash:MobileManPageBase {

	private Iparm apiProxy;
	private XmlRpcClientProtocol clientProtocol;

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);

			DataSet ds;
			ds = GetPackDataSet(ViCommConst.SETTLE_BITCASH,0);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
					lstPack.SelectedIndex = lstPack.Items.Count - 1;
					lstPack.Visible = false;
					sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
					sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
					sessionMan.userMan.settleServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
					break;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}

	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;
		sessionMan.errorMessage = "";
		string sCardNo = txtCol1.Text + txtCol2.Text + txtCol3.Text + txtCol4.Text;
		string sSalesAmt = lstPack.Items[lstPack.SelectedIndex].Value;
		string sSid = "";

		if (sCardNo.Equals("")) {
			bOk = false;
			sessionMan.errorMessage = sessionMan.errorMessage + "カードIDを入力して下さい。<br />";
		} else {
			if (!SysPrograms.Expression(@"^[あ-を]+$",sCardNo)) {
				bOk = false;
				sessionMan.errorMessage = sessionMan.errorMessage + "ひらがなを入力して下さい。<br />";
			}
			if (bOk & sCardNo.Length > 16) {
				bOk = false;
				sessionMan.errorMessage = sessionMan.errorMessage + "カードIDは16文字以内で入力してください。<br />";
			}
		}

		if (bOk) {
			using (Pack oPack = new Pack())
			using (SettleLog oLog = new SettleLog()) {
				if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_BITCASH,sSalesAmt,sessionMan.userMan.userSeq)) {
					oLog.LogSettleRequest(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.SETTLE_CORP_BITCASH,
						ViCommConst.SETTLE_BITCASH,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						int.Parse(sSalesAmt),
						oPack.exPoint,
						sCardNo,
						"",
						out sSid);
				}
			}

			// SSL証明書のチェック
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

			Hashtable oValidateResult = new Hashtable();
			oValidateResult = (Hashtable)ValidateCard(sCardNo,sSid,int.Parse(sSalesAmt));

			string sValidateResult = (string)oValidateResult["STATUS"];

			if (sValidateResult.Equals("OK")) {

				string sValidateBcsId = (string)oValidateResult["BCS_ID"];
				int iValidateLogId = (int)oValidateResult["LOG_ID"];
				int iValidateBalance = (int)oValidateResult["BALANCE"];

				Hashtable oSettlementResult = new Hashtable();

				//決済実行
				oSettlementResult = (Hashtable)do_settlement(sValidateBcsId,sSid);
				string sSettlementResult = (string)oSettlementResult["STATUS"];

				if (!sSettlementResult.Equals("OK")) {
					System.Array settelement_error = (System.Array)oSettlementResult["ERROR"];
					string sMsg = (string)settelement_error.GetValue(0);
					if (sMsg.StartsWith("206")) {
						sessionMan.errorMessage = "残高が不足しています。";
					} else {
						sessionMan.errorMessage = "ｶｰﾄﾞIDを正しく入力して下さい。";
					}
					bOk = false;
				}

			} else {
				using (SettleLog oLog = new SettleLog()) {
					oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"1");
				}
				System.Array validate_error = (System.Array)oValidateResult["ERROR"];
				sessionMan.errorMessage = "ｶｰﾄﾞIDを正しく入力して下さい。";
				bOk = false;
			}
		}

		if (bOk) {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"0");
			}
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_BITCASH_OK));
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_BITCASH_NG));
		}

	}


	private XmlRpcStruct ValidateCard(string pCardNo,string pSid,int pSalesAmt) {
		int iBitCashType = 99;
		string sBitCashType = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["BitCashType"]);
		if (!sBitCashType.Equals("")) {
			iBitCashType = int.Parse(sBitCashType);
		}

		XmlRpcStruct hashTable = new XmlRpcStruct();
		XmlRpcStruct ret = new XmlRpcStruct();

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_BITCASH,ViCommConst.SETTLE_BITCASH);
			hashTable.Add("SHOP_ID",oSiteSettle.cpIdNo);
			hashTable.Add("SHOP_PASSWD",oSiteSettle.cpPassword);
			hashTable.Add("ORDER_ID",pSid);
			hashTable.Add("CARD_NUMBER",pCardNo);
			hashTable.Add("RATING",iBitCashType);
			hashTable.Add("PRICE",pSalesAmt);

			apiProxy = (Iparm)XmlRpcProxyGen.Create(typeof(Iparm));
			clientProtocol = (XmlRpcClientProtocol)apiProxy;
			clientProtocol.Url = oSiteSettle.settleUrl;
		}

		try {
			//実処理
			ret = apiProxy.validate_card(hashTable);

		} catch (Exception ex) {
			HandleException(ex);
		}

		return ret;

	}

	//決済実行
	private XmlRpcStruct do_settlement(string pBcsid,string pSid) {
		XmlRpcStruct hashTable = new XmlRpcStruct();
		XmlRpcStruct ret = new XmlRpcStruct();

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_BITCASH,ViCommConst.SETTLE_BITCASH);
			hashTable.Add("SHOP_ID",oSiteSettle.cpIdNo);
			hashTable.Add("SHOP_PASSWD",oSiteSettle.cpPassword);
			hashTable.Add("BCS_ID",pBcsid);
			hashTable.Add("ORDER_ID",pSid);
			apiProxy = (Iparm)XmlRpcProxyGen.Create(typeof(Iparm));
			clientProtocol = (XmlRpcClientProtocol)apiProxy;
			clientProtocol.Url = oSiteSettle.settleUrl;
		}

		try {
			//実処理
			ret = apiProxy.do_settlement(hashTable);

		} catch (Exception ex) {
			HandleException(ex);
		}

		return ret;

	}

	//インタフェース定義
	public interface Iparm:IXmlRpcProxy {
		//カードバリデーション
		[XmlRpcMethod("Settlement.validate_card")]
		XmlRpcStruct validate_card(XmlRpcStruct a);

		//決済実行
		[XmlRpcMethod("Settlement.do_settlement")]
		XmlRpcStruct do_settlement(XmlRpcStruct b);

		//決済確認
		[XmlRpcMethod("Settlement.confirm_settlement")]
		XmlRpcStruct confirm_settlement(XmlRpcStruct c);
	}

	private void HandleException(Exception ex) {
		try {
			throw ex;
		} catch (XmlRpcFaultException fex) {
			ViCommInterface.WriteIFError(ex,"PaymentBitCash","Fault Response: " + fex.FaultString);
		} catch (XmlRpcServerException xmlRpcEx) {
			ViCommInterface.WriteIFError(ex,"PaymentBitCash","XmlRpcServerException: " + xmlRpcEx.Message);
		} catch (Exception defEx) {
			ViCommInterface.WriteIFError(ex,"PaymentBitCash","Exception: " + defEx.Message);
		}
	}

	/// <summary>
	/// SSL証明書のチェック (チェックせずに通す)
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="certificate"></param>
	/// <param name="chain"></param>
	/// <param name="sslPolicyErrors"></param>
	/// <returns></returns>
	private bool OnRemoteCertificateValidationCallback(
		Object sender
		,X509Certificate certificate
		,X509Chain chain
		,SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}

}
