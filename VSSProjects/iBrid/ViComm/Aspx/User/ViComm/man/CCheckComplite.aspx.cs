/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: C�|CHECK���ϊ���
--	Progaram ID		: CCheckComplite
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_CCheckComplite:MobileManPageBase {
	
	private string userSeq;
	private string userStatus;
	private string loginResult;

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sFuka = iBridUtil.GetStringValue(Request.QueryString["FUKA"]);
			string[] sItem = sFuka.Split(':');
			if (sFuka.Length >= 3) {
				sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_LOGINID,"",sItem[1],sItem[2],Session.SessionID,out userSeq,out userStatus,out loginResult);
			}
		}
	}
}
