<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FindGameCharacter.aspx.cs" Inherits="ViComm_man_FindGameCharacter" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<ibrid:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		
		<mobile:Panel ID="pnlOnline" Runat="server">
			<ibrid:iBMobileLabel ID="lblOnline" runat="server" BreakAfter="true">
			</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="rdoStatus" Runat="server" SelectType="Radio" Alignment="Left">
				<Item Text="全ての女性" Value="0" Selected="true" />
				<Item Text="ﾛｸﾞｲﾝ中の子のみ" Value="1" Selected="false" />
				<Item Text="待機中の子のみ" Value="2" Selected="false" />
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesOnline" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>

		<mobile:Panel ID="pnlNewCast" Runat="server">
			<mobile:SelectionList ID="chkNewCast" Runat="server" SelectType="CheckBox" Alignment="Left">
				<Item Text="新人さんのみ" Value="1" Selected="false" />
			</mobile:SelectionList>
		</mobile:Panel>

		<mobile:Panel ID="pnlTreasure" Runat="server">
			<mobile:SelectionList ID="chkTreasurePic" Runat="server" SelectType="CheckBox" Alignment="Left">
				<Item Text="お宝写真ｱﾘのみ" Value="1" Selected="false" />
			</mobile:SelectionList>
			<mobile:SelectionList ID="chkTreasureMovie" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="true">
				<Item Text="限定動画ｱﾘのみ" Value="1" Selected="false" />
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesTreasure" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlRelation" Runat="server">	
			<mobile:SelectionList ID="lstRelation" Runat="server" SelectType="DropDown" BreakAfter="False">
				<Item Value="" Text="--関係--"></Item>
				<Item Value="" Text="他人"></Item>
				<Item Value="200" Text="味方"></Item>
				<Item Value="100" Text="相棒"></Item>
				<Item Value="25" Text="愛人"></Item>
				<Item Value="1" Text="恋人"></Item>
			</mobile:SelectionList>以上<br />
			<ibrid:iBMobileLiteralText ID="tagLinesRelation" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlLevel" Runat="server">	
			<ibrid:iBMobileLabel ID="lblLevel" runat="server" BreakAfter="False">ﾚﾍﾞﾙ</ibrid:iBMobileLabel>
			<ibrid:iBMobileTextBox ID="txtStartLevel" runat="server" MaxLength="4" Size="10" Numeric= BreakAfter="False"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLabel ID="IBMobileLabel2" runat="server" BreakAfter="False">〜</ibrid:iBMobileLabel>
			<ibrid:iBMobileTextBox ID="txtEndLevel" runat="server" MaxLength="4" Size="10" Numeric =true></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLinesLevel" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>

		<mobile:Panel ID="pnlGameType" Runat="server">
			<mobile:SelectionList ID="lstGameType" Runat="server" SelectType="DropDown">
				<Item Value="" Text="--ｹﾞｰﾑﾀｲﾌﾟ--"></Item>
				<Item Value="2" Text="癒し系"></Item>
				<Item Value="1" Text="小悪魔"></Item>
				<Item Value="3" Text="お姉系"></Item>
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesGameType" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlAge" Runat="server">
			<mobile:SelectionList ID="lstAge" Runat="server" SelectType="DropDown">
				<Item Value=":" Text="--年齢--"></Item>
				<Item Value="18:20" Text="18〜20歳"></Item>
				<Item Value="21:24" Text="21〜24歳"></Item>
				<Item Value="25:29" Text="25〜29歳"></Item>
				<Item Value="30:34" Text="30〜34歳"></Item>
				<Item Value="35:40" Text="35〜40歳"></Item>
				<Item Value="40:" Text="40歳以上"></Item>
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesAge" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem1" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem1" runat="server" BreakAfter="true" Visible="false">castItemm01</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem1" Runat="server"><Item Text="--地域-" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem1" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem9" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem9" runat="server" BreakAfter="true" Visible="false">castItemm09</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem9" Runat="server"><Item Text="--ｴｯﾁ度--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem9" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines9" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItemEx1" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItemEx1" runat="server" BreakAfter="true">ｴｯﾁ度</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItemEx1" Runat="server" BreakAfter="False"><Item Text="--ｴｯﾁ度--" Value="*" /></mobile:SelectionList>以上<br />
			<ibrid:iBMobileLiteralText ID="tagLinesEx1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem10" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem10" runat="server" BreakAfter="true" Visible="false">castItemm10</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem10" Runat="server"><Item Text="--SM度--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem10" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem3" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem3" runat="server" BreakAfter="true" Visible="false">castItemm03</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem3" Runat="server"><Item Text="--ｽﾀｲﾙ--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem3" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines3" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem2" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem2" runat="server" BreakAfter="true" Visible="false">castItemm02</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem2" Runat="server"><Item Text="--ﾀｲﾌﾟ--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem2" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines2" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem4" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem4" runat="server" BreakAfter="true" Visible="false">castItemm04</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem4" Runat="server"><Item Text="--職業--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem4" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines4" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem8" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem8" runat="server" BreakAfter="true" Visible="false">castItemm08</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem8" Runat="server"><Item Text="--出没時間--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem8" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines8" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem11" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem11" runat="server" BreakAfter="true" Visible="false">castItemm11</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem11" Runat="server"><Item Text="--TEL待ち--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem11" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem12" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem12" runat="server" BreakAfter="true" Visible="false">castItemm12</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem12" Runat="server"><Item Text="--顔だし--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem12" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>

		<mobile:Panel ID="pnlName" Runat="server">
			<ibrid:iBMobileLiteralText ID="tagEmojiHandleNm" runat="server" Text="<font color=orange>$xE6FA;</font>" />
			<ibrid:iBMobileLabel ID="lblHandleNm" runat="server" BreakAfter="true">名前に</ibrid:iBMobileLabel>
			<ibrid:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12" BreakAfter="False"></ibrid:iBMobileTextBox>を含む<br />
			<ibrid:iBMobileLiteralText ID="tagLinesHandelNm" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlWithoutNonAdult" Runat="server" BreakAfter="false">
			<ibrid:iBMobileLiteralText ID="tagEmojiWithoutNonAdult" runat="server" Text="<font color=red>$xE72F;</font>ｱﾀﾞﾙﾄNGの女性を表示<br />" />
			<mobile:SelectionList ID="rdoWithoutNonAdult" Runat="server" SelectType="Radio" BreakAfter="False">
				<Item Text="問わない" Value="1" Selected="True" />
				<Item Text="しない" Value="0" />
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesWithoutNonAdult" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem6" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem6" runat="server" BreakAfter="true" Visible="false">castItemm06</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem6" Runat="server"><Item Text="--身長--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem6" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines6" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem5" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem5" runat="server" BreakAfter="true" Visible="false">castItemm05</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem5" Runat="server"><Item Text="--ﾊﾞｽﾄｻｲｽﾞ--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem5" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines5" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItemEx2" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItemEx2" runat="server" BreakAfter="true">ﾊﾞｽﾄｻｲｽﾞ</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItemEx2" Runat="server" BreakAfter="False"><Item Text="--ﾊﾞｽﾄｻｲｽﾞ--" Value="*" /></mobile:SelectionList>以上<br />
			<ibrid:iBMobileLiteralText ID="tagLinesEx2" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem7" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem7" runat="server" BreakAfter="true" Visible="false">castItemm07</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem7" Runat="server"><Item Text="--血液型--" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem7" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines7" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlComment" Runat="server">
			<ibrid:iBMobileLabel ID="lblComment" runat="server" BreakAfter="true">$xE68F;ﾌﾘｰﾜｰﾄﾞ検索</ibrid:iBMobileLabel>
			<ibrid:iBMobileTextBox ID="txtComment" runat="server" MaxLength="20" Size="12" BreakAfter="false"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLabel ID="lblComment2" runat="server" BreakAfter="true">を含む</ibrid:iBMobileLabel>
			<ibrid:iBMobileLabel ID="lblComment3" runat="server" BreakAfter="true">↑女性のﾌﾟﾛﾌｺﾒﾝﾄより検索</ibrid:iBMobileLabel>
		</mobile:Panel>
		
		<mobile:Panel ID="pnlCastItem13" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem13" runat="server" BreakAfter="true">castItemm13</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem13" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem13" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem14" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem14" runat="server" BreakAfter="true">castItemm14</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem14" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem14" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem15" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem15" runat="server" BreakAfter="true">castItemm15</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem15" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem15" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem16" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem16" runat="server" BreakAfter="true">castItemm16</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem16" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem16" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem17" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem17" runat="server" BreakAfter="true">castItemm17</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem17" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem17" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem18" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem18" runat="server" BreakAfter="true">castItemm18</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem18" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem18" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem19" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem19" runat="server" BreakAfter="true">castItemm19</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem19" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem19" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem20" Runat="server">
			<ibrid:iBMobileLabel ID="lblCastItem20" runat="server" BreakAfter="true">castItemm20</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem20" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<ibrid:iBMobileTextBox ID="txtCastItem20" runat="server" MaxLength="300" Size="12"></ibrid:iBMobileTextBox>
			<ibrid:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		$PGM_HTML06;
		$DATASET_LOOP_START0;
		$PGM_HTML07;
		$DATASET_LOOP_END0;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
