/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 送信済みメール詳細表示
--	Progaram ID		: ViewUserMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewTxMail:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		bool bDownload = false;

		if (!IsPostBack) {
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);

			if (!sMovieSeq.Equals("")) {
				bDownload = Download(sMovieSeq);
			}
			if (bDownload == false) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				ulong uSeekMode;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode) == false) {
					uSeekMode = ViCommConst.INQUIRY_TX_USER_MAIL_BOX;
				}
				if (sessionMan.ControlList(Request,uSeekMode,ActiveForm)) {
					if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
						using (MailBox oMailBox = new MailBox()) {
							oMailBox.UpdateMailDel(sessionMan.GetMailValue("MAIL_SEQ"),ViCommConst.TX);
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_MAN_TX_MAIL + "&mail_delete_count=1"));
						}
					}
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListTxMailBox.aspx"));
				}
			}
		} else {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		}
	}
	private bool Download(string pMovieSeq) {
		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)sessionMan.parseContainer.parseUser).GetMovie(ViCommConst.MAN,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				string sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.MAN);
				return true;
			} else {
				return false;
			}
		}
	}
}
