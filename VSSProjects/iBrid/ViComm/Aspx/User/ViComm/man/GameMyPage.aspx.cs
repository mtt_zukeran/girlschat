/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсEϲ�߰��
--	Progaram ID		: GameMyPage
--
--  Creation Date	: 2011.08.02
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
public partial class ViComm_man_GameMyPage:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!sessionMan.logined) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByTermIdGame.aspx"));
			return;
		}

		if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || (sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionMan.adminFlg)) {
			HttpCookie cookie = new HttpCookie("maqia");
			cookie.Values["maqiauid"] = sessionMan.userMan.userSeq;
			cookie.Values["maqiapw"] = sessionMan.userMan.loginPassword;
			cookie.Values["maqiasex"] = ViCommConst.MAN;
			cookie.Expires = DateTime.Now.AddDays(90);
			cookie.Domain = sessionMan.site.hostNm;
			Response.Cookies.Add(cookie);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
	}
}
