/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフ登録完了時ポイント付与
--	Progaram ID		: GetRegistProfilePoint
--
--  Creation Date	: 2015.09.15
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_GetRegistProfilePoint:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.errorMessage = string.Empty;
		sessionMan.userMan.profilePicPointType = 0;
		sessionMan.userMan.profilePointResult = string.Empty;
		
		int iProfilePicPointType;
		string sCheckResult;
		int iNoProfFlag;
		int iModifyProfFlag;
		
		if (!IsPostBack) {
			if (sessionMan.userMan.registServicePointFlag == ViCommConst.FLAG_OFF) {
				tagCertify.Visible = true;
				tagNoProf.Visible = false;
				tagModifyProf.Visible = false;
				tagGetPoint.Visible = false;
				tagAfterGetPoint.Visible = false;
				tagCondition.Visible = true;
			} else {
				tagCertify.Visible = false;
				
				this.CheckAddRegistProfilePoint(out iProfilePicPointType,out sCheckResult);
				
				if (sCheckResult.Equals(PwViCommConst.AddRegistProfilePointResult.RESULT_OK)) {
					CheckAttr(out iNoProfFlag,out iModifyProfFlag);

					if (iNoProfFlag == ViCommConst.FLAG_ON) {
						tagNoProf.Visible = true;
						tagModifyProf.Visible = false;
						tagGetPoint.Visible = false;
						tagAfterGetPoint.Visible = false;
						tagCondition.Visible = true;
					} else if (iModifyProfFlag == ViCommConst.FLAG_ON) {
						tagNoProf.Visible = false;
						tagModifyProf.Visible = true;
						tagGetPoint.Visible = false;
						tagAfterGetPoint.Visible = false;
						tagCondition.Visible = true;
					} else {
						tagNoProf.Visible = false;
						tagModifyProf.Visible = false;
						tagGetPoint.Visible = true;
						tagAfterGetPoint.Visible = false;
						tagCondition.Visible = true;
					}
				} else if (sCheckResult.Equals(PwViCommConst.AddRegistProfilePointResult.RESULT_ADDED)) {
					tagNoProf.Visible = false;
					tagModifyProf.Visible = false;
					tagGetPoint.Visible = false;
					tagAfterGetPoint.Visible = true;
					tagCondition.Visible = false;
					
					sessionMan.userMan.profilePicPointType = iProfilePicPointType;
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iNoProfFlag;
		int iModifyProfFlag;
		
		this.CheckAttr(out iNoProfFlag,out iModifyProfFlag);

		if (iNoProfFlag == ViCommConst.FLAG_ON || iModifyProfFlag == ViCommConst.FLAG_ON) {
			tagCertify.Visible = false;
			tagNoProf.Visible = false;
			tagModifyProf.Visible = true;
			tagGetPoint.Visible = false;
			tagAfterGetPoint.Visible = false;
			tagCondition.Visible = true;
		} else {
			int iProfilePicPointType;
			string sResult;
			using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
				string sSiteCd = sessionMan.site.siteCd;
				string sUserSeq = sessionMan.userMan.userSeq;
				oUserDefPoint.AddRegistProfilePoint(sSiteCd,sUserSeq,ViCommConst.FLAG_OFF,out iProfilePicPointType,out sResult);
			}

			if (sResult.Equals(PwViCommConst.AddRegistProfilePointResult.RESULT_NG)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			} else {
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
				
				tagNoProf.Visible = false;
				tagModifyProf.Visible = false;
				tagGetPoint.Visible = false;
				tagAfterGetPoint.Visible = true;
				tagCondition.Visible = false;

				sessionMan.userMan.profilePicPointType = iProfilePicPointType;
				sessionMan.userMan.profilePointResult = sResult;
			}
		}
	}

	private void CheckAddRegistProfilePoint(out int pProfilePicPointType,out string pResult) {
		using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
			string sSiteCd = sessionMan.site.siteCd;
			string sUserSeq = sessionMan.userMan.userSeq;
			oUserDefPoint.AddRegistProfilePoint(sSiteCd,sUserSeq,ViCommConst.FLAG_ON,out pProfilePicPointType,out pResult);
		}
	}
	
	private void CheckAttr(out int pNoProfFlag,out int pModifyProfFlag) {
		sessionMan.errorMessage = string.Empty;
		
		pNoProfFlag = ViCommConst.FLAG_OFF;
		pModifyProfFlag = ViCommConst.FLAG_OFF;
		
		int iExistDataCount = 0;
		int iProfileSecretCount = 0;
		
		int i = 0;
		
		Regex regex = new Regex("内緒",RegexOptions.Compiled);
		
		for (i = 0;i < sessionMan.userMan.attrList.Count;i++) {
			if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
				if (string.IsNullOrEmpty(sessionMan.userMan.attrList[i].attrImputValue)) {
					sessionMan.errorMessage = sessionMan.errorMessage + string.Format("{0}が未入力です。\r\n",sessionMan.userMan.attrList[i].attrTypeNm);
				} else if (sessionMan.userMan.attrList[i].attrImputValue.Length < 30) {
					iExistDataCount = iExistDataCount + 1;
					sessionMan.errorMessage = sessionMan.errorMessage + string.Format("{0}を30文字以上入力してください。\r\n",sessionMan.userMan.attrList[i].attrTypeNm);
				} else {
					iExistDataCount = iExistDataCount + 1;
				}
			} else {
				if (sessionMan.userMan.attrList[i].profileReqItemFlag == false) {
					if (string.IsNullOrEmpty(sessionMan.userMan.attrList[i].attrSeq)) {
						sessionMan.errorMessage = sessionMan.errorMessage + string.Format("{0}が未選択です。\r\n",sessionMan.userMan.attrList[i].attrTypeNm);
					} else if (regex.Match(sessionMan.userMan.attrList[i].attrNm).Success) {
						iProfileSecretCount = iProfileSecretCount + 1;
						iExistDataCount = iExistDataCount + 1;
					} else {
						iExistDataCount = iExistDataCount + 1;
					}
				}
			}
		}

		if (iExistDataCount == 0) {
			pNoProfFlag = ViCommConst.FLAG_ON;
			sessionMan.errorMessage = string.Empty;
		} else {
			if (iProfileSecretCount > 3) {
				sessionMan.errorMessage = sessionMan.errorMessage + "【内緒】は3つまでです。";
			}

			if (!string.IsNullOrEmpty(sessionMan.errorMessage)) {
				pModifyProfFlag = ViCommConst.FLAG_ON;

				sessionMan.errorMessage = sessionMan.errorMessage.Trim();
				sessionMan.errorMessage = sessionMan.errorMessage.Replace("\r\n","<br />");
			}
		}
	}
}