/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい投稿
--	Progaram ID		: WriteRequest
--
--  Creation Date	: 2012.06.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteRequest:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;

			this.setLstCategory();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	private void setLstCategory() {
		using (PwRequest oPwRequest = new PwRequest()) {
			DataSet dsCategory = oPwRequest.GetRequestCategory(sessionMan.site.siteCd,ViCommConst.MAN,null,null);

			foreach (DataRow drCategory in dsCategory.Tables[0].Rows) {
				lstCategory.Items.Add(new MobileListItem(drCategory["REQUEST_CATEGORY_NM"].ToString(),drCategory["REQUEST_CATEGORY_SEQ"].ToString()));
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;

		if (txtTitle.Text.Equals(string.Empty)) {
			sessionMan.errorMessage = "ﾀｲﾄﾙを入力して下さい";
			return;
		} else {
			if (txtTitle.Text.Length > 42) {
				sessionMan.errorMessage = "ﾀｲﾄﾙが長すぎます";
				return;
			}
		}

		if (lstCategory.Items[lstCategory.SelectedIndex].Value.Equals(string.Empty)) {
			sessionMan.errorMessage = "ｶﾃｺﾞﾘを選択して下さい";
			return;
		}

		if (txtText.Text.Equals(string.Empty)) {
			sessionMan.errorMessage = "提案内容を入力して下さい";
			return;
		} else {
			if (txtText.Text.Length > 1000) {
				sessionMan.errorMessage = "提案内容が長すぎます";
				return;
			}

			string sNGWord = string.Empty;

			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}

			if (!sessionMan.ngWord.VaidateDoc(txtText.Text,out sNGWord)) {
				sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		ViewState["TITLE"] = txtTitle.Text;
		ViewState["REQUEST_CATEGORY_SEQ"] = lstCategory.Items[lstCategory.SelectedIndex].Value;
		ViewState["TEXT"] = txtText.Text;

		lblTitle.Text = HttpUtility.HtmlEncode(txtTitle.Text);
		lblCategory.Text = lstCategory.Items[lstCategory.SelectedIndex].Text;
		lblText.Text = HttpUtility.HtmlEncode(txtText.Text).Replace(System.Environment.NewLine,"<br>");

		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
	}

	protected void cmdTx_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		string sTitle = iBridUtil.GetStringValue(ViewState["TITLE"]);
		string sRequestCategorySeq = iBridUtil.GetStringValue(ViewState["REQUEST_CATEGORY_SEQ"]);
		string sText = iBridUtil.GetStringValue(ViewState["TEXT"]);
		string sRequestSeq;
		string sResult = string.Empty;

		using (PwRequest oPwRequest = new PwRequest()) {
			sResult = oPwRequest.WriteRequest(
				sessionMan.site.siteCd,
				ViCommConst.MAN,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sTitle)),
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sText)),
				sRequestCategorySeq,
				out sRequestSeq
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("req_seq",sRequestSeq);
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_WRITE_REQUEST_COMPLETE,oParam);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
	}
}
