/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入られ一覧
--	Progaram ID		: ListLikeMe
--
--  Creation Date	: 2010.02.04
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListLikeMe:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_LIKE_ME,
					ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sAddQuery = string.Format("&sortex={0}",this.Request.Form["sortex"]);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
												   "ListLikeMe.aspx?" + RemoveSortEx(Request.QueryString.ToString()) + sAddQuery));
	}

	private string RemoveSortEx(string pQueryString) {
		Regex regex = new Regex("(&|\\?)sortex=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(pQueryString,string.Empty);
	}
}
