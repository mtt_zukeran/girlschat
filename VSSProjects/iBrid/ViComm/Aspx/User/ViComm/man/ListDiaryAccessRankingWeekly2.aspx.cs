﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 日記被閲覧ランキング(月～日)
--	Progaram ID		: ListDiaryAccessRankingWeekly2
--
--  Creation Date	: 2011.08.31
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListDiaryAccessRankingWeekly2 : MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_CAST_DIARY_ACCESS_RANKING_WEEKLY2,
					ActiveForm);
		}
	}
}
