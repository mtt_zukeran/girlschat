/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �߯����ߵ�޼ު ���[
--	Progaram ID		: RegistPickupObjsVote.aspx
--
--  Creation Date	: 2013.05.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistPickupObjsVote:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sPickupId = iBridUtil.GetStringValue(this.Request.QueryString["pickupid"]);
			string sObjSeq = iBridUtil.GetStringValue(this.Request.QueryString["objseq"]);
			string sResult = string.Empty;
			
			using (PickupObjsVote oPickupObjsVote = new PickupObjsVote()) {
				oPickupObjsVote.VotePickupObj(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sPickupId,sObjSeq,out sResult);
			}
			
			switch (sResult) {
				case PwViCommConst.VotePickupObjResult.RESULT_OK:
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,string.Format("WritePickupObjsComment.aspx?pickupid={0}&objseq={1}",sPickupId,sObjSeq)));
					break;
				case PwViCommConst.VotePickupObjResult.RESULT_VOTED:
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_PICKUP_OBJS_VOTE_ERROR);
					break;
				default:
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
					break;
			}
		}
	}
}
