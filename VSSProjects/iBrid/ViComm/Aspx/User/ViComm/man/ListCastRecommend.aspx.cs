﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップ一覧
--	Progaram ID		: ListCastRecomend
--
--  Creation Date	: 2010.08.13
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using ViComm;

public partial class ViComm_man_ListPickupCast:MobileManPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_RECOMMEND,ActiveForm);
		}
	}
}
