/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールフォーム送信確認
--	Progaram ID		: MailFormConfirm
--
--  Creation Date	: 2010.09.13
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;

public partial class ViComm_man_MailFormConfirm:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			ErrorCheck();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdBack"] != null) {
				cmdBack_Click(sender,e);
			}
		}
	}

	private string CreateDocument() {
		string sParamsInput = string.Empty;

		StringBuilder sb = new StringBuilder();

		string callfrom = iBridUtil.GetStringValue(Request.QueryString["callfrom"]);
		if (callfrom.Equals("ModifyUserHandleNm.aspx")) {
			string oldHandleNm = sessionMan.userMan.handleNm;
			string newHandleNm = iBridUtil.GetStringValue(Request.QueryString["handlenm"]);
			sb.AppendFormat("旧ハンドルネーム：{0}\r\nから\r\n新ハンドルネーム：{1}\r\nへの変更依頼\r\nログインID:{2}",oldHandleNm,newHandleNm,sessionMan.userMan.loginId);
		} else {
			string sAfterDefaultBody = iBridUtil.GetStringValue(Request.Params["afterdefaultbody"]);
			if (!sAfterDefaultBody.Equals(ViCommConst.FLAG_ON_STR)) {
				AppendDefaultBody(false,ref sb);
			}
			foreach (string sParams in Request.QueryString.AllKeys) {
				if (sParams.StartsWith("*")) {
					sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("*",string.Empty)]);
					sb.AppendFormat("{0}:{1}\r\n",iBridUtil.GetStringValue(Request.QueryString[sParams]),sParamsInput);
				} else if (sParams.StartsWith("?")) {
					sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("?",string.Empty)]);
					sb.AppendFormat("{0}:{1}\r\n",iBridUtil.GetStringValue(Request.QueryString[sParams]),sParamsInput);
				}
			}
			if (sAfterDefaultBody.Equals(ViCommConst.FLAG_ON_STR)) {
				AppendDefaultBody(true,ref sb);
			}
		}

		return sb.ToString();
	}

	private void ErrorCheck() {
		string sParamsInput = string.Empty;
		string sParamsName = string.Empty;
		bool bOk = true;
		StringBuilder sb = new StringBuilder();

		foreach (string sParams in Request.QueryString.AllKeys) {
			if (sParams.StartsWith("*")) {
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("*",string.Empty)]);
				sParamsName = iBridUtil.GetStringValue(Request.QueryString[sParams]);

				if (!sParamsInput.Equals("")) {
					lblConfirm.Text += string.Format("{0}:{1}<BR />",sParamsName.Replace("\r\n","<BR>"),sParamsInput.Replace("\r\n","<BR>"));
				} else {
					bOk = false;
					sb.AppendFormat("{0}が入力されていません<BR />",sParamsName);
				}
			} else if (sParams.StartsWith("?")) {
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sParams.Replace("?",string.Empty)]);
				sParamsName = iBridUtil.GetStringValue(Request.QueryString[sParams]);

				if (!sParamsInput.Equals("")) {
					lblConfirm.Text += string.Format("{0}:{1}<BR />",sParamsName.Replace("\r\n","<BR>"),sParamsInput.Replace("\r\n","<BR>"));
				}
			}
		}
		if (bOk == false) {
			sessionMan.errorMessage = sb.ToString();
			IDictionary<string, string> oParam = new Dictionary<string, string>();

			Encoding enc = Encoding.GetEncoding(932);
			string sMail = iBridUtil.GetStringValue(Request.QueryString["mail"]);
			if (!string.IsNullOrEmpty(sMail)) {
				oParam.Add("mail", HttpUtility.UrlEncode(sMail, enc));
			}
			string sName = iBridUtil.GetStringValue(Request.QueryString["name"]);
			if (!string.IsNullOrEmpty(sName)) {
				oParam.Add("name", HttpUtility.UrlEncode(sName, enc));
			}
			string sBody = iBridUtil.GetStringValue(Request.QueryString["body"]);
			if (!string.IsNullOrEmpty(sBody)) {
				oParam.Add("body", HttpUtility.UrlEncode(sBody, enc));
			}
			RedirectToDisplayDoc(iBridUtil.GetStringValue(Request.QueryString["error"]), oParam);
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sToMailAddress = iBridUtil.GetStringValue(Request.QueryString["send"]);
		string sFromMailAddress = iBridUtil.GetStringValue(Request.QueryString["from"]);
		//fromがない場合ｾｯｼｮﾝのﾒｰﾙｱﾄﾞﾚｽを利用する。
		if (sFromMailAddress.Equals(string.Empty)) {
			sFromMailAddress = sessionMan.userMan.emailAddr;
		}

		string sTitle = iBridUtil.GetStringValue(Request.QueryString["title"]);
		string sDoc = CreateDocument();
		//メールアドレスの形式をチェック
		try {
			new System.Net.Mail.MailAddress(sFromMailAddress);
		} catch (FormatException) {
			sFromMailAddress = sToMailAddress;
		}

		bool bUtf8Flag = false;
		if (iBridUtil.GetStringValue(Request.QueryString["callfrom"]).Equals("ModifyUserHandleNm.aspx")) {
			if (iBridUtil.GetStringValue(System.Configuration.ConfigurationManager.AppSettings["HandleNmMailUtf8Flag"]).Equals(ViCommConst.FLAG_ON_STR)) {
				bUtf8Flag = true;
			}
		}
		SendMail(sFromMailAddress,sToMailAddress,sTitle,sDoc,bUtf8Flag);

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("DisplayDoc.aspx?doc={0}",iBridUtil.GetStringValue(Request.QueryString["success"]))));
	}

	protected void cmdBack_Click(object sender,EventArgs e) {
		string sKey,sParamsInput;
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		StringBuilder sb = new StringBuilder();

		foreach (string sParams in Request.QueryString.AllKeys) {
			if (sParams.StartsWith("*")) {
				sKey = sParams.Replace("*",string.Empty);
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sKey]);
				sb.AppendFormat("&{0}={1}",sKey,HttpUtility.UrlEncode(sParamsInput,enc));
			} else if (sParams.StartsWith("?")) {
				sKey = sParams.Replace("?",string.Empty);
				sParamsInput = iBridUtil.GetStringValue(Request.QueryString[sKey]);
				sb.AppendFormat("&{0}={1}",sKey,HttpUtility.UrlEncode(sParamsInput,enc));
			}
		}

		string doc = iBridUtil.GetStringValue(Request.QueryString["doc"]);
		if (!string.IsNullOrEmpty(doc)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("DisplayDoc.aspx?doc={0}{1}",doc,sb.ToString())));
		} else {
			string callfrom = iBridUtil.GetStringValue(Request.QueryString["callfrom"]);
			if (callfrom.Equals("ModifyUserHandleNm.aspx")) {
				sb.AppendFormat("&{0}={1}","sendmail",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["sendmail"]),enc));
				sb.AppendFormat("&{0}={1}","send",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["send"]),enc));
				sb.AppendFormat("&{0}={1}","title",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["title"]),enc));
				sb.AppendFormat("&{0}={1}","success",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["success"]),enc));
				sb.AppendFormat("&{0}={1}","handlenm",HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["handlenm"]),enc));
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("{0}?{1}",callfrom,sb.ToString())));
		}
	}

	private void AppendDefaultBody(bool pAfter,ref StringBuilder pBody) {
		if (pAfter) {
			pBody.Append("\r\n");
		}
		pBody.AppendFormat("ﾛｸﾞｲﾝID:{0}\r\n",sessionMan.userMan.loginId);
		pBody.AppendFormat("登録ﾒｰﾙｱﾄﾞﾚｽ:{0}",sessionMan.userMan.emailAddr);
		if (!pAfter) {
			pBody.Append("\r\n\r\n");
		}
	}
}
