/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 販売着ボイス詳細
--	Progaram ID		: ViewSaleSave
--
--  Creation Date	: 2010.07.29
--  Creater			: i-Brid(Kazuaki.Itoh)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewSaleVoice:MobileManPageBase {
	private const string ACTION_DOWNLOAD = "download";

	private string action = string.Empty;
	private string voiceSeq = string.Empty;
	private VoiceHelper voiceHelper = new VoiceHelper();

	protected void Page_Load(object sender,EventArgs e) {
		action = iBridUtil.GetStringValue(Request.QueryString["action"]);
		voiceSeq = iBridUtil.GetStringValue(Request.QueryString["voiceseq"]);
	
		if(!IsPostBack) {
			bool bIsDownload = false;

			if(action.Equals(ACTION_DOWNLOAD)) {
				bIsDownload = this.DownloadAction(voiceSeq);
			}
			
			if(!bIsDownload){
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				ulong uSeekMode;
				bool bControlList;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
					bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
				} else {
					bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_SALE_VOICE,ActiveForm);
				}
				if (!bControlList) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListSaleVoice.aspx"));
				}
			}

		}
	}
	
	/// <summary>
	/// 着ﾎﾞｲｽのﾀﾞｳﾝﾛｰﾄﾞ処理を行う
	/// </summary>
	/// <returns>ﾀﾞｳﾝﾛｰﾄﾞ処理を行う場合はtrue。それ以外はfalse。</returns>
	private bool DownloadAction(string pVoiceSeq){
		if(string.IsNullOrEmpty(pVoiceSeq)) return false;

		if(!this.voiceHelper.IsAlreadyUsed(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.ATTACHED_VOICE.ToString(),voiceSeq)) {
			RedirectToConfirmSaleVoice();
			return false;
		}
			
		using(CastVoice oCastVoice = new CastVoice()){
			using(DataSet oCastVoiceDs = oCastVoice.GetPageCollectionBySeq(pVoiceSeq,false,false,ViCommConst.ATTACHED_VOICE.ToString())){
				if(oCastVoiceDs.Tables.Count==0 || oCastVoiceDs.Tables[0].Rows.Count==0){
					return false;
				}
				
				DataRow oCastVoiceDr = oCastVoiceDs.Tables[0].Rows[0];

				string sSiteCd = oCastVoiceDr["SITE_CD"].ToString().Trim();
				string sLoginId = oCastVoiceDr["LOGIN_ID"].ToString().Trim();
				string sChastSeq = oCastVoiceDr["USER_SEQ"].ToString().Trim();
				string sChastCharNo = oCastVoiceDr["USER_CHAR_NO"].ToString().Trim();

				using (Access oAccess = new Access()) {
					oAccess.AccessVoicePage(sessionMan.site.siteCd, sChastSeq, sChastCharNo, pVoiceSeq);
				}
				
				voiceHelper.Download(sSiteCd,sLoginId,pVoiceSeq);
			}
		}
		

		return true;
	}

	/// <summary>
	/// 購入確認画面へ遷移する
	/// </summary>
	private void RedirectToConfirmSaleVoice() {
		UrlBuilder oUrlBuilder = new UrlBuilder("ConfirmSaleVoice.aspx");
		oUrlBuilder.Parameters.Add("voiceseq",voiceSeq);
		RedirectToMobilePage(GenerateFullUrl(oUrlBuilder.ToString()));
	}
}
