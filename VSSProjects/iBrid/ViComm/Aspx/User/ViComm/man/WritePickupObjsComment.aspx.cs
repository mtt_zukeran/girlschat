/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップオブジェ コメント書き込み/完了
--	Progaram ID		: WritePickupObjsComment
--
--  Creation Date	: 2013.05.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WritePickupObjsComment:MobileManPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.errorMessage = string.Empty;
			tagCommentWrite.Visible = true;
			txtComment.Visible = true;
			tagCommentWriteSub.Visible = true;
			tagWriteComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);
		
		string sPickupId = iBridUtil.GetStringValue(Request.Params["pickupid"]);
		string sObjSeq = iBridUtil.GetStringValue(Request.Params["objseq"]);

		bool bOk = true;

		string sSiteCd = sessionMan.site.siteCd;
		string sUserSeq = sessionMan.userMan.userSeq;
		string sUserCharNo = sessionMan.userMan.userCharNo;

		using (Site oSite = new Site())
		using (PickupObjsComment oPickupObjsComment = new PickupObjsComment()) {

			string sLastCommentDay = oPickupObjsComment.GetLastCommentDay(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sPickupId,sObjSeq);

			if ((!string.IsNullOrEmpty(sLastCommentDay))
				&& (sLastCommentDay.Equals(DateTime.Now.ToString("yyyy/MM/dd")))) {
				bOk = false;
				sessionMan.errorMessage += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_PICKUP_OBJS_COMMENT_LIMIT);

				return;
			}
		}

		if (txtComment.Text.Equals("")) {
			bOk = false;
			//本文を入力して下さい。 
			sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}
		if (iMaxLength != 0 && txtComment.Text.Length > iMaxLength) {
			bOk = false;
			sessionMan.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"ｺﾒﾝﾄ");
		}

		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			string sNGWord;
			if (sessionMan.ngWord.VaidateDoc(txtComment.Text,out sNGWord) == false) {
				bOk = false;
				sessionMan.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		string sCommentText = txtComment.Text;
		string sCommentSeq = string.Empty;
		string sResult = string.Empty;

		if (bOk == false) {
			return;
		} else {
			using (PickupObjsComment oPickupObjsComment = new PickupObjsComment()) {
				sCommentText = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtComment.Text));
				oPickupObjsComment.WritePickupObjsComment(sSiteCd,sUserSeq,sUserCharNo,sPickupId,sObjSeq,sCommentText,out sResult);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				tagCommentWrite.Visible = false;
				txtComment.Visible = false;
				tagCommentWriteSub.Visible = false;
				tagWriteComplete.Visible = true;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		}
	}
}