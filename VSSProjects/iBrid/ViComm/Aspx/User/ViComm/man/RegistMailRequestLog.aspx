﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistMailRequestLog.aspx.cs" Inherits="ViComm_man_RegistMailRequestLog" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="lblForm" runat="server" Text="$PGM_HTML02;" />
		<cc1:iBMobileLiteralText ID="lblDone" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLiteralText ID="lblError" runat="server" Text="$PGM_HTML04;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
