﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 新着画像詳細
--	Progaram ID		: ViewGallery
--
--  Creation Date	: 2010.05.06
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using iBridCommLib;
using ViComm;

public partial class ViComm_man_ViewGallery:MobileManPageBase {

	
	protected void Page_Load(object sender,EventArgs e) {
		string sCastCharNo;
		string sLoginId;

		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			ulong uSeekMode;
			bool bControlList;
			if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
				bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
			} else {
				bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_GALLERY,ActiveForm);
			}

			if (bControlList) {
				sCastCharNo = sessionMan.GetProfilePicValue("USER_CHAR_NO");
				sLoginId = sessionMan.GetProfilePicValue("LOGIN_ID");
				sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo);
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListGallery.aspx"));
			}
		}
	}
}
