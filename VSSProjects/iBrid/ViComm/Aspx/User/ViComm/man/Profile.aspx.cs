/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフィール表示
--	Progaram ID		: Profile
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_Profile:MobileMailManPage {

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		ulong ulSeekMode;

		string sScrId = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);

		if (iBridUtil.GetStringValue(Request.QueryString["direct"]).ToString().Equals("1")) {
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sBeforeSystemId = iBridUtil.GetStringValue(Request.QueryString["beforesystemid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			if (!sBeforeSystemId.Equals(string.Empty)) {
				using (User oUser = new User()) {
					sLoginId = oUser.GetLoginIdByBeforeSystemId(sBeforeSystemId,ViCommConst.OPERATOR);
				}
			}
			DataSet dsDataSet = sessionMan.userMan.castCharacter.GetOneByLoginId(sessionMan.site.siteCd,sLoginId,sCastCharNo,1);

			if (dsDataSet.Tables[0].Rows.Count > 0) {
				ViewState["CATEGORY_INDEX"] = dsDataSet.Tables[0].Rows[0]["ACT_CATEGORY_IDX"].ToString();
				ulSeekMode = ViCommConst.INQUIRY_DIRECT;
			} else {
				if (sessionMan.logined) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.nonUserTopId));
				}
				return;
			}
		} else {
			ulSeekMode = ulong.Parse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]));
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			pnlTxMail.Visible = false;
			pnlTxMailPointLack.Visible = false;
			
			bool bFind = sessionMan.ControlList(
							Request,
							ulSeekMode,
							ActiveForm);
			if (!bFind) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			} else {

				string sUserSeq = sessionMan.GetCastValue("USER_SEQ");
				string sUserCharNo = sessionMan.GetCastValue("USER_CHAR_NO");
				if (!sessionMan.GetCastValue("USER_STATUS").Equals(ViCommConst.USER_WOMAN_NORMAL) ||
						!sessionMan.GetCastValue("NA_FLAG").Equals(ViCommConst.NaFlag.OK.ToString())) {
						RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
				}

				if (sessionMan.logined) {
					//相手の拒否リストに載っている場合、リダイレクトさせる。 
					CheckRefuse(sUserSeq,sUserCharNo);

					if (string.IsNullOrEmpty(sScrId) || sScrId.Equals("04")) {
						if (sessionMan.userMan.characterOnlineStatus != ViCommConst.USER_TALKING &&
							sessionMan.userMan.characterOnlineStatus != ViCommConst.USER_DUMMY_TALKING) {

							string sRefererNm = this.GetRefererNm();

							using (Marking oMarking = new Marking()) {
								oMarking.MarkingMainte(
										sessionMan.site.siteCd,
										sessionMan.userMan.userSeq,
										sessionMan.userMan.userCharNo,
										sUserSeq,
										sUserCharNo,
										ViCommConst.OPERATOR,
										0,
										sRefererNm
								);
							}

							if (IsAvailableService(ViCommConst.RELEASE_PROF_ACCESS_COUNT,2)) {
								if (!string.IsNullOrEmpty(sRefererNm) && sRefererNm.Length <= 50) {
									using (ProfAccessLog oProfAccessLog = new ProfAccessLog()) {
										oProfAccessLog.RegistProfAccessLog(
											sessionMan.site.siteCd,
											sessionMan.userMan.userSeq,
											sUserSeq,
											sUserCharNo,
											sRefererNm
										);
									}
								}
							}
						}
					}
				}
				
				if (string.IsNullOrEmpty(sScrId) || sScrId.Equals("04")) {
					using (Access oAccess = new Access()) {
						oAccess.AccessCastPage(
							sessionMan.site.siteCd,
							sUserSeq,
							sUserCharNo,
							sessionMan.userMan.userSeq
						);
					}
				}
				
				if (iBridUtil.GetStringValue(Request.QueryString["target_flag"]).ToString().Equals("1")) {
					string sCompleteFlag;
					string sResult;
					string sErrorMask;
					using (InvestigateEntrant oInvestigateEntrant = new InvestigateEntrant()) {
						InvestigateEntrantSeekCondition oCondition = new InvestigateEntrantSeekCondition();
						oCondition.SiteCd = sessionMan.site.siteCd;
						oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
						oCondition.UserSeq = sessionMan.userMan.userSeq;
						oCondition.UserCharNo = sessionMan.userMan.userCharNo;
						oCondition.CastUserSeq = sUserSeq;
						oCondition.CastCharNo = sUserCharNo;
						DataSet dsInvestigateEntrant = oInvestigateEntrant.GetOne(oCondition);

						if (dsInvestigateEntrant.Tables[0].Rows.Count > 0) {
							if (dsInvestigateEntrant.Tables[0].Rows[0]["CAUGHT_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF_STR)) {
								oInvestigateEntrant.InvestigateEntrantCaught(oCondition,out sCompleteFlag,out sErrorMask,out sResult);

								if (sResult.Equals(PwViCommConst.WantedEntrantResult.RESULT_OK)) {
									RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,string.Format("ViewFlashWantedStamp.aspx?completeflag={0}",sCompleteFlag)));
								} else if (sResult.Equals(PwViCommConst.WantedEntrantResult.RESULT_NG)) {
									RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
								} else {
									RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,string.Format("ViewInvestigateEntryError.aspx?err={0}&subtype=2",sErrorMask)));
								}
							}
						}
					}
				}
				
				if (sessionMan.logined && string.IsNullOrEmpty(sScrId)) {
					if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
						
						if (IsAvailableService(ViCommConst.RELEASE_TX_MAIL_FROM_PROFILE,2)) {
							if (IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2)) {
								lblTitle.Visible = false;
								txtTitle.Visible = false;
							}

							string sMailDataSeq = iBridUtil.GetStringValue(sessionMan.userMan.NextMailDataSeq());
							sessionMan.userMan.mailData.Add(sMailDataSeq,new UserManMailInfo());

							string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);

							sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq = sUserSeq;
							sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo = sUserCharNo;
							sessionMan.userMan.mailData[sMailDataSeq].rxUserNm = sessionMan.GetCastValue("HANDLE_NM");
							sessionMan.userMan.mailData[sMailDataSeq].rxLoginId = sLoginId;
							int.TryParse(iBridUtil.GetStringValue(Request.QueryString["quickrequest"]),out sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag);

							if (sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag != 1) {
								using (Cast oCast = new Cast()) {
									if (oCast.IsQuickResponseEntry(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sUserSeq,sUserCharNo)) {
										sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag = ViCommConst.FLAG_ON;
									} else {
										sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag = ViCommConst.FLAG_OFF;
									}
								}
							}
							
							if (sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag == 1) {
								lblTitle.Visible = false;
								txtTitle.Visible = false;
								txtTitle.Text = GetErrorMessage(ViCommConst.INFO_STATUS_QUICK_RES_TITLE,false);
							}

							sessionMan.userMan.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_OFF;

							ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;

							sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
							int iChargePoint;
							if (!CheckMailBalance(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo,out iChargePoint)) {
								pnlTxMailPointLack.Visible = true;
								pnlTxMail.Visible = false;
							} else {
								pnlTxMail.Visible = true;
								pnlTxMailPointLack.Visible = false;
							}
						}
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(null,new EventArgs());
			} else if (Request.Params["cmdTxMail"] != null) {
				cmdTxMail_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iValue;
		if (int.TryParse(iBridUtil.GetStringValue(Request.Params["rdoNotRx"]),out iValue)) {
			string sUserSeq = sessionMan.GetCastValue("USER_SEQ");
			string sUserCharNo = sessionMan.GetCastValue("USER_CHAR_NO");

			using (Favorit oFavorite = new Favorit()) {
				oFavorite.LogingMailNotRxMainte(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sUserSeq,sUserCharNo,iValue);
			}

			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_SETTING_FAVORIT_LOGIN_MAIL_RX));
		}
	}

	private string GetRefererNm() {
		string sRefererNm;

		if (Request.UrlReferrer != null) {
			string sUrl = Request.UrlReferrer.OriginalString;
			string sQuery = Request.UrlReferrer.Query;
			string sFileNm = Path.GetFileName(sUrl);

			if (!string.IsNullOrEmpty(sQuery)) {
				sFileNm = sFileNm.Replace(sQuery,"");
			}

			UrlBuilder oUrlBuilder = new UrlBuilder(sFileNm);

			if (!string.IsNullOrEmpty(sQuery)) {
				Regex oRegex = new Regex("(?:&|\\?)(doc|scrid|pickupid)(?:=)([0-9]+)");

				foreach (Match oMatch in oRegex.Matches(sQuery)) {
					oUrlBuilder.Parameters.Add(oMatch.Groups[1].ToString(),oMatch.Groups[2].ToString());
				}
			}

			sRefererNm = oUrlBuilder.ToString();
		} else if (sessionMan.markingReferer.Equals("none")) {
			sRefererNm = "none";
		} else {
			sRefererNm = string.Empty;
		}

		return sRefererNm;
	}

	protected void cmdTxMail_Click(object sender,EventArgs e) {
		if (iBridUtil.GetStringValue(Request.Params["chkpresent"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_ON;
			PresentSelect(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false);
		} else {
			sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_OFF;
			Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false,ViCommConst.SCR_TX_MAIL_COMPLITE_MAN);
		}
	}
}
