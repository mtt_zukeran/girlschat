/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�l���ʐ^�ꗗ
--	Progaram ID		: ListGamePossessionManTreasure.aspx
--
--  Creation Date	: 2012.07.23
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;


public partial class ViComm_man_ListGamePossessionManTreasure:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		
		if (!IsPostBack) {

			using (ManTreasureAttr oManTreasureAttr = new ManTreasureAttr()) {
				DataSet dsManTreasureAttr = oManTreasureAttr.GetManTreasureAttr();

				foreach (DataRow drManTreasureAttr in dsManTreasureAttr.Tables[0].Rows) {
					lstCastGamePicAttr.Items.Add(new MobileListItem(drManTreasureAttr["CAST_GAME_PIC_ATTR_NM"].ToString(),drManTreasureAttr["CAST_GAME_PIC_ATTR_SEQ"].ToString()));
				}

				foreach (MobileListItem item in lstCastGamePicAttr.Items) {
					if (item.Value == iBridUtil.GetStringValue(Request.Params["cast_game_pic_attr_seq"])) {
						item.Selected = true;
						break;
					}
				}
				
				this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
				sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_POSSESSION_MAN_TREASURE,ActiveForm);
			}
			
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {

				string sUrl = string.Empty;
				
				sUrl = string.Format(
						"ListGamePossessionManTreasure.aspx?sort={0}&cast_game_pic_attr_seq={1}",
						iBridUtil.GetStringValue(Request.Params["sort"]),
						lstCastGamePicAttr.Selection.Value
						);
				
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
			}
		}
	}
}
