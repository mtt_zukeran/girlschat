/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女性詳細検索
--	Progaram ID		: FindGameCharacter.aspx
--
--  Creation Date	: 2011.08.10
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public partial class ViComm_man_FindGameCharacter:MobileSocialGameManBase {
	private const string ONLINE_STATUS = "FindGameCharacterOnlineStatus";
	private const string NEWCAST_FLG = "FindGameCharacterNewCastFlg";
	private const string HANDLE_NAME = "FindGameCharacterHandleNm";
	private const string CAST_ITEM = "FindGameCharacterCastItem";
	private const string CAST_ITEM_EX1 = "FindGameCharacterCastItemEx1";
	private const string CAST_ITEM_EX2 = "FindGameCharacterCastItemEx2";
	private const string COMMENT = "FindGameCharacterComment";
	private const string AGE = "FindGameCharacterAge";
	private const string NONADULT = "FindGameCharacterNonAdult";
	private const string RELATION = "FindGameCharacterRelation";
	private const string GAME_TYPE = "FindGameCharacterGameType";
	
	protected void Page_Load(object sender,EventArgs e) {
		pnlRelation.Visible = true;
		pnlLevel.Visible = true;
		pnlGameType.Visible = true;
		pnlTreasure.Visible = true;
		pnlName.Visible = true;
		pnlCastItemEx1.Visible = false;	//ｴｯﾁ度(以上)
		pnlCastItemEx2.Visible = false;	//ﾊﾞｽﾄｻｲｽﾞ(以上)

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		int iIdx = 0;

		using (ManageCompany oManageCompany = new ManageCompany()) {
			pnlAge.Visible = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY);
		}

		if (!IsPostBack) {
			GetQueryParm();

			// 地域
			using (CastAttrTypeValue oAttrTypeValue = new CastAttrTypeValue()) {
				DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionMan.site.siteCd,"405");

				using (CodeDtl oCodeDtl = new CodeDtl()) {
					DataSet dsCodeDtl = oCodeDtl.GetList("81");

					foreach (DataRow drCodeDtl in dsCodeDtl.Tables[0].Rows) {
						string sExp = string.Format("GROUPING_CD={0}",drCodeDtl["CODE"]);
						string sValues = string.Empty;

						foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Select(sExp)) {
							if (!string.IsNullOrEmpty(sValues))
								sValues += ",";
							sValues += drAttrTypeValue["CAST_ATTR_SEQ"];
						}

						lstCastItem1.Items.Add(new MobileListItem(drCodeDtl["CODE_NM"].ToString(),sValues));
					}
				}

				foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
					if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lstCastItem1.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),drAttrTypeValue["CAST_ATTR_SEQ"].ToString()));
					}
				}

				// 男性の登録地域を選択状態
				if (sessionMan.userMan.attrList.Count > 0) {
					foreach (MobileListItem oItem in this.lstCastItem1.Items) {
						if (oItem.Text.Equals(sessionMan.userMan.attrList[0].attrNm)) {
							this.lstCastItem1.SelectedIndex = oItem.Index;
						}
					}
				}
			}

			// ｴｯﾁ度(以上)
			if (pnlCastItemEx1.Visible) {
				using (CastAttrTypeValue oAttrTypeValue = new CastAttrTypeValue()) {
					DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionMan.site.siteCd,"409");
					string sValues = string.Empty;

					foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
						string sExp = string.Format("CAST_ATTR_SEQ >= {0} AND CAST_ATTR_SEQ <> 10549",drAttrTypeValue["CAST_ATTR_SEQ"]);

						if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
							sValues = null;

							foreach (DataRow drAttrTypeValueSub in dsAttrTypeValue.Tables[0].Select(sExp)) {
								if (!string.IsNullOrEmpty(sValues)) {
									sValues += ",";
								}

								sValues += drAttrTypeValueSub["CAST_ATTR_SEQ"];
							}

							if (string.IsNullOrEmpty(sValues)) {
								sValues = drAttrTypeValue["CAST_ATTR_SEQ"].ToString();
							}

							lstCastItemEx1.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),sValues));
						}
					}
				}
			}

			// ﾊﾞｽﾄｻｲｽﾞ(以上)
			if (pnlCastItemEx2.Visible) {
				using (CastAttrTypeValue oAttrTypeValue = new CastAttrTypeValue()) {
					DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionMan.site.siteCd,"411");
					string sValues = string.Empty;

					foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
						string sExp = string.Format("CAST_ATTR_SEQ >= {0} ",drAttrTypeValue["CAST_ATTR_SEQ"]);

						if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
							sValues = null;

							foreach (DataRow drAttrTypeValueSub in dsAttrTypeValue.Tables[0].Select(sExp)) {
								if (!string.IsNullOrEmpty(sValues)) {
									sValues += ",";
								}

								sValues += drAttrTypeValueSub["CAST_ATTR_SEQ"];
							}

							if (string.IsNullOrEmpty(sValues)) {
								sValues = drAttrTypeValue["CAST_ATTR_SEQ"].ToString();
							}

							lstCastItemEx2.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),sValues));
						}
					}
				}
			}

			#region ---------- Find ----------

			using (CastAttrType oCastAttrType = new CastAttrType()) {
				DataSet dsAttrType = oCastAttrType.GetList(sessionMan.site.siteCd);
				SelectionList lstCastItem;
				iBMobileLabel lblCastItem;
				iBMobileTextBox txtCastItem;
				Dictionary<string,string> castAttrList = (Dictionary<string,string>)Session[CAST_ITEM];

				foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
					if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lblCastItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblCastItem{0}",iIdx + 1)) as iBMobileLabel;
						lstCastItem = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}",iIdx + 1)) as SelectionList;
						txtCastItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtCastItem{0}",iIdx + 1)) as iBMobileTextBox;

						lblCastItem.Text = drAttrType["CAST_ATTR_TYPE_FIND_NM"].ToString();

						if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
							lstCastItem.Visible = false;

							// 入力値の復元を行います。
							if (castAttrList != null) {
								if (castAttrList.ContainsKey(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString())) {
									string val = castAttrList[drAttrType["CAST_ATTR_TYPE_SEQ"].ToString()];
									txtCastItem.Text = val;
								}
							}
						} else {
							txtCastItem.Visible = false;
							if (string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
								using (CastAttrTypeValue oAttrTypeValue = new CastAttrTypeValue()) {
									DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionMan.site.siteCd,drAttrType["CAST_ATTR_TYPE_SEQ"].ToString());
									foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
										if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
											lstCastItem.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),drAttrTypeValue["CAST_ATTR_SEQ"].ToString()));
										}
									}
								}
							} else {
								using (CodeDtl oCodeDtl = new CodeDtl()) {
									DataSet dsGroup = oCodeDtl.GetList(drAttrType["GROUPING_CATEGORY_CD"].ToString());
									foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
										lstCastItem.Items.Add(new MobileListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
									}
								}
							}
							if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_RADIO)) {
								lstCastItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
								if (castAttrList == null) {
									lstCastItem.SelectedIndex = 0;
								}
							}

							// 入力値の復元を行います。
							if (castAttrList != null) {
								if (castAttrList.ContainsKey(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString())) {
									string val = castAttrList[drAttrType["CAST_ATTR_TYPE_SEQ"].ToString()];
									foreach (MobileListItem item in lstCastItem.Items) {
										if (item.Value == val) {
											item.Selected = true;
										}
									}
								}
							}
						}
						iIdx++;
					}
				}
			}

			for (int i = iIdx;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
				Panel pnlCastItem = (Panel)frmMain.FindControl(string.Format("pnlCastItem{0}",i + 1)) as Panel;
				pnlCastItem.Visible = false;
			}

			int iOnlineStatus;
			int.TryParse(iBridUtil.GetStringValue(Session[ONLINE_STATUS]),out iOnlineStatus);

			if (iOnlineStatus == ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING) {
				// ﾛｸﾞｲﾝ中
				rdoStatus.Items[0].Selected = false;
				rdoStatus.Items[1].Selected = true;
			} else if (iOnlineStatus == ViCommConst.SeekOnlineStatus.WAITING) {
				// 待機中
				rdoStatus.Items[0].Selected = false;
				rdoStatus.Items[2].Selected = true;
			} else {
				// 全て
				rdoStatus.Items[0].Selected = true;
			}

			// 新人フラグ
			if (pnlNewCast.Visible) {
				if (iBridUtil.GetStringValue(Session[NEWCAST_FLG]) == "1") {
					chkNewCast.Items[0].Selected = true;
				}
			}

			txtHandelNm.Text = iBridUtil.GetStringValue(Session[HANDLE_NAME]);
			txtComment.Text = iBridUtil.GetStringValue(Session[COMMENT]);
			
			// 年齢
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
					foreach (MobileListItem item in lstAge.Items) {
						if (item.Value == iBridUtil.GetStringValue(Session[AGE])) {
							item.Selected = true;
							break;
						}
					}
				} else {
					pnlAge.Visible = false;
				}
			}

			this.rdoStatus.Items[0].Text = DisplayWordUtil.Replace(this.rdoStatus.Items[0].Text);
			this.lblComment.Text = DisplayWordUtil.ReplaceWordFindUserCommentHeader(this.lblComment.Text);
			this.lblComment2.Text = DisplayWordUtil.ReplaceWordFindUserCommentFooter(this.lblComment2.Text);

			// ｴｯﾁ度(以上)
			if (pnlCastItemEx1.Visible) {
				foreach (MobileListItem item in lstCastItemEx1.Items) {
					if (item.Value == iBridUtil.GetStringValue(Session[CAST_ITEM_EX1])) {
						item.Selected = true;
						break;
					}
				}
			}

			// ﾊﾞｽﾄｻｲｽﾞ(以上)
			if (pnlCastItemEx2.Visible) {
				foreach (MobileListItem item in lstCastItemEx2.Items) {
					if (item.Value == iBridUtil.GetStringValue(Session[CAST_ITEM_EX2])) {
						item.Selected = true;
						break;
					}
				}
			}

			// ｱﾀﾞﾙﾄNG
			int? iWithoutNonAdult = Session[NONADULT] as int?;
			if (iWithoutNonAdult != null) {
				this.rdoWithoutNonAdult.SelectedIndex = iWithoutNonAdult.Value;
			}

			// 関係
			if (pnlRelation.Visible) {
				foreach (MobileListItem item in lstRelation.Items) {
					if (item.Value == iBridUtil.GetStringValue(Session[RELATION])) {
						item.Selected = true;
						break;
					}
				}
			}

			// ｹﾞｰﾑﾀｲﾌﾟ
			if (pnlGameType.Visible) {
				foreach (MobileListItem item in lstGameType.Items) {
					if (item.Value == iBridUtil.GetStringValue(Session[GAME_TYPE])) {
						item.Selected = true;
						break;
					}
				}
			}

			#endregion
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sHandleNm = string.Empty;
		string sComment = string.Empty;
		string sAgeFrom = string.Empty;
		string sAgeTo = string.Empty;
		string sUrl = string.Empty;
		string sTreasurePic = string.Empty;
		string sTreasureMovie = string.Empty;
		string sRelation = string.Empty;
		string sStartLevel = string.Empty;
		string sEndLevel = string.Empty;
		string sGameType = string.Empty;

		bool bOk = true;

		int i;

		int iOnlineStatus = 0;
		int iNewCastFlag = 0;

		if (pnlComment.Visible) {
			sComment = HttpUtility.UrlEncode(HttpUtility.HtmlEncode(txtComment.Text),enc);
			txtComment.Text = HttpUtility.HtmlEncode(txtComment.Text);
		}

		if (chkTreasurePic.SelectedIndex >= 0) {
			sTreasurePic = chkTreasurePic.Selection.Value;
		}

		if (chkTreasureMovie.SelectedIndex >= 0) {
			sTreasureMovie = chkTreasureMovie.Selection.Value;
		}

		sStartLevel = HttpUtility.HtmlEncode(txtStartLevel.Text);
		txtStartLevel.Text = HttpUtility.HtmlEncode(txtStartLevel.Text);

		sEndLevel = HttpUtility.HtmlEncode(txtEndLevel.Text);
		txtEndLevel.Text = HttpUtility.HtmlEncode(txtEndLevel.Text);

		if (!txtStartLevel.Text.Equals("")) {
			if (!SysPrograms.Expression(@"^\d+$",txtStartLevel.Text)) {
				lblErrorMessage.Text = string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_VALIDATE_NUMERIC_FIELD),"開始ﾚﾍﾞﾙ");
				bOk = false;
			}
		}

		if (!txtEndLevel.Text.Equals("")) {
			if (!SysPrograms.Expression(@"^\d+$",txtEndLevel.Text)) {
				lblErrorMessage.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_VALIDATE_NUMERIC_FIELD),"終了ﾚﾍﾞﾙ");
				bOk = false;
			}
		}

		if (pnlName.Visible) {
			sHandleNm = HttpUtility.UrlEncode(HttpUtility.HtmlEncode(txtHandelNm.Text),enc);
			txtHandelNm.Text = HttpUtility.HtmlEncode(txtHandelNm.Text);
		}

		if (bOk == false) {
			return;
		}

		if (pnlAge.Visible) {
			sAgeFrom = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[0]);
			sAgeTo = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[1]);
		}

		if (pnlOnline.Visible) {
			if (rdoStatus.Items[1].Selected) {
				iOnlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
			} else if (rdoStatus.Items[2].Selected) {
				iOnlineStatus = ViCommConst.SeekOnlineStatus.WAITING;
			}
		}

		if (pnlNewCast.Visible) {
			if (chkNewCast.SelectedIndex != -1) {
				iNewCastFlag = 1;
			}
		}

		if (pnlRelation.Visible) {
			sRelation = lstRelation.Selection.Value;
		}

		if (pnlGameType.Visible) {
			sGameType = lstGameType.Selection.Value;
		}

		sUrl = string.Format(
						"ListFindGameCharacter.aspx?online={0}&handle={1}&comment={2}&newcast={3}&treasure_pic={4}&treasure_movie={5}&relation={6}"
						+ "&start_level={7}&end_level={8}&agefrom={9}&ageto={10}&game_type={11}",
						iOnlineStatus,
						sHandleNm,
						sComment,
						iNewCastFlag,
						sTreasurePic,
						sTreasureMovie,
						sRelation,
						sStartLevel,
						sEndLevel,
						sAgeFrom,
						sAgeTo,
						sGameType
						);

		// 選択した値をセッションにいれておきます。 
		if (pnlOnline.Visible) {
			Session[ONLINE_STATUS] = iOnlineStatus;
			Session[NEWCAST_FLG] = iNewCastFlag;
			Session[HANDLE_NAME] = txtHandelNm.Text;
		}

		if (pnlAge.Visible) {
			Session[AGE] = lstAge.Selection.Value;
		}

		if (pnlCastItemEx1.Visible) {
			Session[CAST_ITEM_EX1] = lstCastItemEx1.Selection.Value;
		}

		if (pnlCastItemEx2.Visible) {
			Session[CAST_ITEM_EX2] = lstCastItemEx2.Selection.Value;
		}

		if (pnlComment.Visible) {
			Session[COMMENT] = txtComment.Text;
		}

		Session[NONADULT] = this.rdoWithoutNonAdult.SelectedIndex;

		if (pnlRelation.Visible) {
			Session[RELATION] = lstRelation.Selection.Value;
		}

		if (pnlGameType.Visible) {
			Session[GAME_TYPE] = lstGameType.Selection.Value;
		}
		
		SelectionList lstCastItem;
		iBMobileTextBox txtCastItem;
		Panel pnlCastItem;

		i = 1;
		string sGroupingFlag;
		Dictionary<string,string> castAttrList = new Dictionary<string,string>();

		using (CastAttrType oCastAttrType = new CastAttrType()) {
			DataSet dsAttrType = oCastAttrType.GetList(sessionMan.site.siteCd);

			foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
				if (drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
					pnlCastItem = (Panel)frmMain.FindControl(string.Format("pnlCastItem{0}",i)) as Panel;

					string sNotEq = iBridUtil.GetStringValue(this.Request.Params["noteq{0:D2}"]);

					if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtCastItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtCastItem{0}",i)) as iBMobileTextBox;

						if ((pnlCastItem != null) && (txtCastItem != null) && (pnlCastItem.Visible)) {
							sUrl = sUrl + string.Format(
												"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}",
												i,
												System.Web.HttpUtility.UrlEncode(txtCastItem.Text,enc),
												drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),
												"0",
												"1",
												sNotEq);
							// 入力値復元用の値保存 
							castAttrList.Add(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),txtCastItem.Text);
						}
					} else {
						lstCastItem = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}",i)) as SelectionList;
						sGroupingFlag = "";
						if (!string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
							//地域の場合はグルーピングを使わない
							if (!drAttrType["CAST_ATTR_TYPE_SEQ"].ToString().Equals("405")) {
								sGroupingFlag = "1";
							}
						}
						if ((pnlCastItem != null) && (lstCastItem != null) && (pnlCastItem.Visible)) {
							sUrl = sUrl + string.Format(
												"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}",
												i,
												lstCastItem.Items[lstCastItem.SelectedIndex].Value,
												drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),
												sGroupingFlag,
												"0",
												sNotEq);
							// 入力値復元用の値保存 
							castAttrList.Add(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),lstCastItem.Items[lstCastItem.SelectedIndex].Value);
						}

						//ｴｯﾁ度(以上)のパラメータを設定
						if (pnlCastItemEx1.Visible) {
							if (drAttrType["CAST_ATTR_TYPE_SEQ"].ToString().Equals("409") && (lstCastItemEx1 != null)) {
								sUrl = sUrl + string.Format(
													"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}",
													i,
													lstCastItemEx1.Items[lstCastItemEx1.SelectedIndex].Value,
													drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),
													sGroupingFlag,
													"0",
													sNotEq);
							}
						}

						//ﾊﾞｽﾄｻｲｽﾞ(以上)のパラメータを設定
						if (pnlCastItemEx2.Visible) {
							if (drAttrType["CAST_ATTR_TYPE_SEQ"].ToString().Equals("411") && (lstCastItemEx2 != null)) {
								sUrl = sUrl + string.Format(
													"&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}&like{0:D2}={4}&noteq{0:D2}={5}",
													i,
													lstCastItemEx2.Items[lstCastItemEx2.SelectedIndex].Value,
													drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),
													sGroupingFlag,
													"0",
													sNotEq);
							}
						}
					}
					i++;
				}
			}
		}

		Session[CAST_ITEM] = castAttrList;

		if (this.rdoWithoutNonAdult.SelectedIndex == 1) {
			sUrl += string.Format("&item{0:D2}=10549&typeseq{0:D2}=409&group{0:D2}=&like{0:D2}=0&noteq{0:D2}=1",i);
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
	}

	private void GetQueryParm() {
		if (!String.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["treasure_pic"]))) {
			chkTreasurePic.SelectedIndex = 0;
			chkTreasurePic.Selection.Value = iBridUtil.GetStringValue(this.Request.QueryString["treasure_pic"]);
		}

		if (!String.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["treasure_movie"]))) {
			chkTreasureMovie.SelectedIndex = 0;
			chkTreasureMovie.Selection.Value = iBridUtil.GetStringValue(this.Request.QueryString["treasure_movie"]);
		}

		txtStartLevel.Text = iBridUtil.GetStringValue(this.Request.QueryString["start_level"]);
		txtEndLevel.Text = iBridUtil.GetStringValue(this.Request.QueryString["end_level"]);
	}
}
