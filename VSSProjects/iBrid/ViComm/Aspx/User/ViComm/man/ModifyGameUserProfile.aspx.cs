/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�o�^���ύX
--	Progaram ID		: ModifyGameUserProfile
--
--  Creation Date	: 2012.02.06
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Configuration;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ModifyGameUserProfile:MobileSocialGameManBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		if (!sessionMan.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"UserTop.aspx"));
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			/*�R���p*/
			if (IsAvailableService(ViCommConst.RELEASE_DISABLE_MODIFY_HANDLE_AND_PASS)) {
				pnlHandleNm.Visible = false;
				pnlPassword.Visible = false;
			}

			pnlBirthDay.Visible = false;

			txtHandelNm.Text = sessionMan.userMan.gameCharacter.gameHandleNm;
			txtPassword.Text = sessionMan.userMan.loginPassword;

			bool bDupliFlag = false;
			if (IsAvailableService(ViCommConst.RELEASE_CHECK_HANDLE_NM_DUPLI)) {
				using (UserMan oUserMan = new UserMan()) {
					bDupliFlag = oUserMan.CheckManHandleNmDupli(sessionMan.site.siteCd,sessionMan.userMan.userSeq,Mobile.EmojiToCommTag(sessionMan.carrier,sessionMan.userMan.handleNm));
				}
			}
			if (txtHandelNm.Text.Equals(string.Empty) || bDupliFlag) {
				pnlHandleNm.Visible = true;
			}

			lblTel.Visible = false;
			txtTel.Visible = false;

			int j = 0;

			for (;j < ViComm.ViCommConst.MAX_ATTR_COUNT;j++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",j + 1)) as Panel;
				pnlUserManItem.Visible = false;
			}

			if (iBridUtil.GetStringValue(Request.QueryString["delpic"]).Equals("1")) {
				sessionMan.userMan.RemoveProfilePic(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo);
			}

			txtHandelNm.Text = sessionMan.userMan.gameCharacter.gameHandleNm;

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		string sNGWord = string.Empty;

		if (bOk == false) {
			return;
		} else {
			string sResult;
			string sBirthday = "";

			if (lstYear.Visible) {
				DateTime dtBiathday = new DateTime(int.Parse(lstYear.Selection.Value),int.Parse(lstMonth.Selection.Value),int.Parse(lstDay.Selection.Value));
				sBirthday = dtBiathday.ToString("yyyy/MM/dd");
			} else {
				sBirthday = DateTime.Now.AddYears(-25).ToString("yyyy/01/01");
			}

			string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];

			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				if (sessionMan.userMan.attrList[i].profileReqItemFlag) {
					using (GameManAttrTypeValue oGameManAttrTypeValue = new GameManAttrTypeValue()) {
						pAttrSeq[i] = oGameManAttrTypeValue.GetMaxManAttrSeqByManAttrTypeSeq(sessionMan.site.siteCd,sessionMan.userMan.attrList[i].attrTypeSeq);
					}
				}

				pAttrTypeSeq[i] = sessionMan.userMan.attrList[i].attrTypeSeq;

			}

			if ((bOk & this.CheckOther()) == false) {
				return;
			}

			sessionMan.userMan.ModifyUser(
				sessionMan.userMan.userSeq,
				txtPassword.Text,
				sessionMan.userMan.tel,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)),
				sBirthday,
				out sResult,
				pAttrTypeSeq,
				pAttrSeq,
				pAttrInputValue,
				sessionMan.userMan.attrList.Count,
				ViCommConst.FLAG_OFF
			);

			if (sResult.Equals("0")) {
				using (TempRegist oTemp = new TempRegist()) {
					if (oTemp.GetOneByUserSeq(sessionMan.userMan.userSeq)) {
						if (!oTemp.mobileCarrierCd.Equals(ViCommConst.ANDROID) && !oTemp.mobileCarrierCd.Equals(ViCommConst.IPHONE)) {
							oTemp.TrackingReport(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAN_AFFILIATE_REPORT_PROFILE,ViCommConst.MAN);
						}
					}
				}

				string sDomain = System.Text.RegularExpressions.Regex.Replace(sessionMan.requestQuery.Url.DnsSafeHost,"(game.|bijo.)",string.Empty);

				RedirectToMobilePage(sessionMan.requestQuery.Url.Scheme + "://" + sDomain + sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"UserTop.aspx"));
			} else {
				switch (int.Parse(sResult)) {
					case ViCommConst.REG_USER_RST_TEL_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MODIFIED_TEL_NO_ALREADY_EXIST);
						break;
					case ViCommConst.REG_USER_RST_HANDLE_NM_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
						break;
					case ViCommConst.REG_USER_RST_UNT_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
						break;
					case ViCommConst.REG_USER_RST_SUB_SCR_NO_IS_NULL:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						break;
					default:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
						break;
				}
			}
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
