/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE��׸�����وꗗ
--	Progaram ID		: ListGameCharacterBattle.aspx
--
--  Creation Date	: 2011.07.25
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameCharacterBattle:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_BATTLE_LIST,ActiveForm);
	}
}
