<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewBbsBingoTerm.aspx.cs" Inherits="ViComm_man_ViewBbsBingoTerm" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlEntry" runat="server" Visible="false">
			$PGM_HTML02;
		</mobile:Panel>
		<mobile:Panel ID="pnlReEntry" runat="server" Visible="false">
			$PGM_HTML03;
		</mobile:Panel>
		<mobile:Panel ID="pnlTry" runat="server" Visible="false">
			$PGM_HTML04;
		</mobile:Panel>
		<mobile:Panel ID="pnlComplete" runat="server" Visible="false">
			$PGM_HTML05;
		</mobile:Panel>
		<mobile:Panel ID="pnlBingoBall" runat="server" Visible="false">
			$PGM_HTML06;
		</mobile:Panel>
		<mobile:Panel ID="pnlTermEnd" runat="server" Visible="false">
			$PGM_HTML07;
		</mobile:Panel>
		<mobile:Panel ID="pnlError" runat="server" Visible="false">
			$PGM_HTML08;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
