﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ相手詳細(応援ﾊﾞﾄﾙ)
--	Progaram ID		: ViewGameCharacterBattleSupport
--
--  Creation Date	: 2011.11.24
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;

public partial class ViComm_man_ViewGameCharacterBattleSupport:MobileSocialGameManBase {
	private NameValueCollection query = new NameValueCollection();

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["battle_log_seq"]);

		if (string.IsNullOrEmpty(sBattleLogSeq)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		using (GameCharacterBattleSupport oGameCharacterBattleSupport = new GameCharacterBattleSupport()) {
			if (!oGameCharacterBattleSupport.CheckExistsPartner(sSiteCd,sUserSeq,sUserCharNo,sBattleLogSeq)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

			if (!oGameCharacterBattleSupport.CheckExistsSupport(sSiteCd,sUserSeq,sUserCharNo,sBattleLogSeq)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

		}

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_START_SUPPORT,ActiveForm);
		}
	}
}
