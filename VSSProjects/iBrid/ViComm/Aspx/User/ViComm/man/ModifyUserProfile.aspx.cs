/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o�^���ύX
--	Progaram ID		: ModifyUserProfile
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/14  i-Brid(R.Suzuki)

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Configuration;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ModifyUserProfile:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			/*�R���p*/
			bool bInvisible = false;
			if (!sessionMan.site.ManRegistFuncLimitAdCd.Equals(string.Empty)){
				if (sessionMan.adCd.IndexOf(sessionMan.site.ManRegistFuncLimitAdCd) >= 0) {
					bInvisible = true;
				}
			}

			if (IsAvailableService(ViCommConst.RELEASE_DISABLE_MODIFY_HANDLE_AND_PASS)) {
				pnlHandleNm.Visible = false;
				pnlPassword.Visible = false;
			}

			txtPassword.Text = sessionMan.userMan.loginPassword;
			txtHandelNm.Text = HttpUtility.HtmlDecode(sessionMan.userMan.handleNm);

			bool bDupliFlag = false;
			if (IsAvailableService(ViCommConst.RELEASE_CHECK_HANDLE_NM_DUPLI)) {
				using (UserMan oUserMan = new UserMan()) {
					bDupliFlag = oUserMan.CheckManHandleNmDupli(sessionMan.site.siteCd,sessionMan.userMan.userSeq,Mobile.EmojiToCommTag(sessionMan.carrier,sessionMan.userMan.handleNm));
				}
			}
			if (txtHandelNm.Text.Equals(string.Empty) || bDupliFlag) {
				pnlHandleNm.Visible = true;
			}

			if (!sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				lblTel.Text = "�d�b�ԍ�";
				txtTel.Text = sessionMan.userMan.tel;
				txtTel.MaxLength = 11;

				string sFlag;
				using (ManageCompany oCompany = new ManageCompany()) {
					oCompany.GetValue("AUTO_GET_TEL_INFO_FLAG",out sFlag);
				}

				if (sessionMan.userMan.telAttestedFlag != 0) {
					lblTel.Visible = false;
					txtTel.Visible = false;
				} else if (sFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					lblTel.Visible = false;
					txtTel.Visible = false;
				}
			} else {
				lblTel.Text = "URI";
				txtTel.Text = sessionMan.userMan.tel;
				txtTel.MaxLength = 64;
			}
			CreateList();

			int iIdx = 0;
			int i = 0;

			for (i = 0;i < sessionMan.userMan.attrList.Count;i++) {

				if (!sessionMan.userMan.attrList[i].registInputFlag && bInvisible) {
					Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i + 1)) as Panel;
					pnlUserManItem.Visible = false;
					continue;
				}
				SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i + 1)) as SelectionList;
				iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i + 1)) as iBMobileTextBox;
				iBMobileLabel lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",i + 1)) as iBMobileLabel;
				TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",i + 1)) as TextArea;

				lblUserManItem.Text = sessionMan.userMan.attrList[i].attrTypeNm;
				if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
					if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
						txtAreaUserManItem.Visible = true;
						txtUserManItem.Visible = false;
						txtAreaUserManItem.Rows = int.Parse(sessionMan.userMan.attrList[i].rowCount);
						txtAreaUserManItem.Text = sessionMan.userMan.attrList[i].attrImputValue;
						txtAreaUserManItem.EmojiPaletteEnabled = true;
					} else {
						txtAreaUserManItem.Visible = false;
						txtUserManItem.Visible = true;
						txtUserManItem.Text = sessionMan.userMan.attrList[i].attrImputValue;
						txtUserManItem.EmojiPaletteEnabled = true;
					}
					lstUserManItem.Visible = false;
				} else {
					txtAreaUserManItem.Visible = false;
					txtUserManItem.Visible = false;
					lstUserManItem.Visible = true;
					iIdx = 0;
					using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
						DataSet ds = oAttrTypeValue.GetList(sessionMan.userMan.siteCd,sessionMan.userMan.attrList[i].attrTypeSeq);
						if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_LIST) {
							lstUserManItem.Items.Add(new MobileListItem("���I��",string.Empty));
						}
						foreach (DataRow dr in ds.Tables[0].Rows) {
							lstUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
							if (sessionMan.userMan.attrList[i].attrSeq.Equals(dr["MAN_ATTR_SEQ"].ToString())) {
								lstUserManItem.Items.Remove(new MobileListItem("���I��",string.Empty));
								lstUserManItem.SelectedIndex = iIdx;
							}
							iIdx++;
						}
					}
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_RADIO) {
						lstUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
						if (lstUserManItem.SelectedIndex == -1) {
							lstUserManItem.SelectedIndex = 0;
						}
					}
				}
			}
			for (;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i + 1)) as Panel;
				pnlUserManItem.Visible = false;
			}

			if (iBridUtil.GetStringValue(Request.QueryString["delpic"]).Equals("1")) {
				sessionMan.userMan.RemoveProfilePic(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo);
			}

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		string sNGWord = string.Empty;

		if (txtPassword.Visible) {
			if (txtPassword.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD);
			} else {
				if (!SysPrograms.Expression(@"^[a-zA-Z0-9]{4,8}$",txtPassword.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD_NG);
				}
			}
		}

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			} else if (!SysPrograms.IsWithinByteCount(HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)),60)) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"�����Ȱ�");
			}
			if (sessionMan.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (txtTel.Visible) {
			if (txtTel.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
			} else {
				if (!sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
					}
				}
			}
		}

		if (lstYear.Visible) {
			if ((lstYear.Selection.Value.Equals("")) || (lstMonth.Selection.Value.Equals("")) || (lstDay.Selection.Value.Equals(""))) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY);
			}

			if (bOk) {
				string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
				int iAge = ViCommPrograms.Age(sDate);
				int iOkAge;
				int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
				if (iOkAge == 0) {
					iOkAge = 18;
				}

				if (iAge > 99 || iAge == 0) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
				} else if (iAge < iOkAge) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18);
				}
			}
		}

		if (bOk == false) {
			return;
		} else {
			string sResult;
			string sBirthday = "";

			if (lstYear.Visible) {
				DateTime dtBiathday = new DateTime(int.Parse(lstYear.Selection.Value),int.Parse(lstMonth.Selection.Value),int.Parse(lstDay.Selection.Value));
				sBirthday = dtBiathday.ToString("yyyy/MM/dd");
			}

			string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];

			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i + 1)) as Panel;
				if (pnlUserManItem.Visible) {
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",i + 1)) as TextArea;
							pAttrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtAreaUserManItem.Text));
							if (pAttrInputValue[i].Length > 1000) {
								pAttrInputValue[i] = SysPrograms.Substring(pAttrInputValue[i],1000);
							}

							if (txtAreaUserManItem.Text.IndexOf("$") != -1) {
								txtAreaUserManItem.Text = txtAreaUserManItem.Text.Replace("$","");
								lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_INPUT_DOLLAR_MARK);
								bOk = false;
							}
						} else {
							iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i + 1)) as iBMobileTextBox;
							pAttrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtUserManItem.Text));
						}
						if (sessionMan.userMan.attrList[i].profileReqItemFlag && pAttrInputValue[i].Equals(string.Empty)) {
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
							bOk = false;
						}
						if (sessionMan.ngWord.VaidateDoc(pAttrInputValue[i],out sNGWord) == false) {
							bOk = false;
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
						}
					} else {
						SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i + 1)) as SelectionList;
						if (lstUserManItem.Selection != null) {
							if (sessionMan.userMan.attrList[i].profileReqItemFlag && lstUserManItem.Selection.Value.Equals(string.Empty)) {
								lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
								bOk = false;
							} else {
								pAttrSeq[i] = lstUserManItem.Selection.Value;
							}
						} else {
							if (sessionMan.userMan.attrList[i].profileReqItemFlag) {
								bOk = false;
								lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
							}
						}
					}
				}
				pAttrTypeSeq[i] = sessionMan.userMan.attrList[i].attrTypeSeq;

			}

			if ((bOk & this.CheckOther()) == false) {
				return;
			}

			sessionMan.userMan.ModifyUser(
				sessionMan.userMan.userSeq,
				txtPassword.Text,
				txtTel.Text,
				txtHandelNm.Visible ? HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)) : string.Empty,
				sBirthday,
				out sResult,
				pAttrTypeSeq,
				pAttrSeq,
				pAttrInputValue,
				sessionMan.userMan.attrList.Count,
				ViCommConst.FLAG_OFF
			);

			if (sResult.Equals("0")) {

				using (TempRegist oTemp = new TempRegist()) {
					if (oTemp.GetOneByUserSeq(sessionMan.userMan.userSeq)) {
						if (!oTemp.mobileCarrierCd.Equals(ViCommConst.ANDROID) && !oTemp.mobileCarrierCd.Equals(ViCommConst.IPHONE)) {
							oTemp.TrackingReport(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAN_AFFILIATE_REPORT_PROFILE,ViCommConst.MAN);
						}
					}
				}

				using (ManNews oManNews = new ManNews()) {
					oManNews.LogManNews(sessionMan.site.siteCd,sessionMan.userMan.userSeq,PwViCommConst.ManNewsType.UPDATE_PROFILE);
				}

				if (iBridUtil.GetStringValue(Request.QueryString["getpoint"]).Equals(ViCommConst.FLAG_ON_STR)) {
					string sBonusPointResult = string.Empty;
					using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
						oUserDefPoint.CheckBonusPoint(sessionMan.site.siteCd,sessionMan.userMan.userSeq,"PROFILE_PIC_FACE",out sBonusPointResult);
					}

					if (sBonusPointResult.Equals("1")) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"GetRegistProfilePoint.aspx"));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_MAN_PROFILE_COMPLITE));
					}
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_MAN_PROFILE_COMPLITE));
				} 
			} else {
				switch (int.Parse(sResult)) {
					case ViCommConst.REG_USER_RST_TEL_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MODIFIED_TEL_NO_ALREADY_EXIST);
						break;
					case ViCommConst.REG_USER_RST_HANDLE_NM_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
						break;
					case ViCommConst.REG_USER_RST_UNT_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
						break;
					case ViCommConst.REG_USER_RST_SUB_SCR_NO_IS_NULL:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						break;
					default:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
						break;
				}
			}
		}
	}

	private void CreateList() {
		int iYear = DateTime.Today.Year - 18;
		string[] sBirthday = sessionMan.userMan.birthday.Split('/');

		for (int i = 0;i < 85;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			if ((sBirthday.Length == 3) && (sBirthday[0].Equals(iYear.ToString("d4")))) {
				lstYear.SelectedIndex = i;
			}
			iYear -= 1;
		}
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
			if ((sBirthday.Length == 3) && (sBirthday[1].Equals(i.ToString("d2")))) {
				lstMonth.SelectedIndex = i - 1;
			}
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
			if ((sBirthday.Length == 3) && (sBirthday[2].Equals(i.ToString("d2")))) {
				lstDay.SelectedIndex = i - 1;
			}
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
