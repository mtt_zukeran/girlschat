/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性用所持お宝図鑑詳細
--	Progaram ID		: ViewGameTreasure
--
--  Creation Date	: 2011.07.28
--  Creater			: i-Brid
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameTreasure : MobileSocialGameManBase {
	virtual protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.Params["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.Params["partner_user_char_no"]);
		string sCastGamePicSeq = iBridUtil.GetStringValue(this.Request.QueryString["cast_game_pic_seq"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty) || sCastGamePicSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			if(!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE_VIEW,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				if (!string.IsNullOrEmpty(sessionMan.userMan.gameCharacter.tutorialStatus)) {
					this.cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sCastGamePicSeq);
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			} else if (Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.cmdNextLink_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo);
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE_VIEW,ActiveForm);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pCastUserSeq,string pCastUserCharNo,string pCastGamePicSeq) {
		string sResult = string.Empty;
		int iGameItemGetChargeFlg = ViCommConst.FLAG_OFF;
		//ﾁｭｰﾄﾘｱﾙ中ﾓｻﾞ消しﾂｰﾙ付与
		if (!string.IsNullOrEmpty(sessionMan.userMan.gameCharacter.tutorialStatus)) {
			string sGameItemSeq;
			using (GameItem oGameItem = new GameItem()) {
				sGameItemSeq = oGameItem.GetGameItem(
					sessionMan.userMan.siteCd,
					sessionMan.sexCd,
					PwViCommConst.GameItemCategory.ITEM_CATEGOAY_TOOL,
					"GAME_ITEM_SEQ"
				);
			}

			using (GameItem oGameItem = new GameItem()) {
				oGameItem.GamePossessionItemMainte(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sGameItemSeq
				);
			}			
		}
		
		using(GameItem oGameItem = new GameItem()) {
			oGameItem.UseMosaicErase(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				pCastUserSeq,
				pCastUserCharNo,
				pCastGamePicSeq,
				iGameItemGetChargeFlg,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.GameUseMosaicErase.RESULT_OK)) {
			this.CheckQuestClear();
			
			int iNextTutorialStatus = int.Parse(sessionMan.userMan.gameCharacter.tutorialStatus) + 1;
			//ﾁｭｰﾄﾘｱﾙ状態更新
			using (GameCharacter oGameCharacter = new GameCharacter()) {
				oGameCharacter.SetupTutorialStatus(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					iNextTutorialStatus.ToString()
				);
			}
			
			string sRedirectUrl = string.Format(
				"ViewGameTreasure.aspx?mosaic_erase=1&partner_user_seq={0}&partner_user_char_no={1}&cast_game_pic_seq={2}&stage_seq={3}&town_seq={4}",
				pCastUserSeq,
				pCastUserCharNo,
				pCastGamePicSeq,
				iBridUtil.GetStringValue(this.Request.QueryString["stage_seq"]),
				iBridUtil.GetStringValue(this.Request.QueryString["town_seq"])
			);

			this.RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}

	protected void cmdNextLink_Click(object sender,EventArgs e,string pCastUserSeq,string pCastUserCharNo) {
		int iDoubleTrapFlag = 0;
		int iUsedTrapCount = 0;
		string sTreasureSeq = this.Request.QueryString["cast_game_pic_seq"].ToString();
		int.TryParse(this.Request.Form["double_trap_flag"].ToString(), out iDoubleTrapFlag);
		int.TryParse(this.Request.Params["possessionItemNum"], out iUsedTrapCount);
		string sResult = string.Empty;

		using (GameItem oGameItem = new GameItem()) {
			oGameItem.UseTrap(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				this.sessionMan.sexCd,
				sTreasureSeq,
				iUsedTrapCount,
				iDoubleTrapFlag,
				out sResult
			);

			if (sResult.Equals(PwViCommConst.GameUseTrapResult.RESULT_OK)) {
				this.RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("{0}?partner_user_seq={1}&partner_user_char_no={2}&cast_game_pic_seq={3}", 
																	this.Request.Params["NEXTLINK"].ToString(),
																	pCastUserSeq,
																	pCastUserCharNo,
																	sTreasureSeq)));
			} else {
				string sErrerCd = string.Empty;
				string sErrerMsg = string.Empty;
				switch (sResult) {
					case PwViCommConst.GameUseTrapResult.RESULT_NOT_HAVE_ITEM:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_NOT_HAVE_OTHER;
						sErrerMsg = "所持しているﾜﾅ";
						break;
					case PwViCommConst.GameUseTrapResult.RESULT_NOT_HAVE_TREASURE:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_NOT_HAVE_OTHER;
						sErrerMsg = "所持しているお宝";
						break;
					case PwViCommConst.GameUseTrapResult.RESULT_OVER_USE_TRAP:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_LIMIT_OVER;
						sErrerMsg = "仕掛けられるﾜﾅの合計は50回";
						break;
					default:
						sErrerCd = PwViCommConst.PwErrerCode.GAME_NOT_HAVE_OTHER;
						sErrerMsg = "ﾜﾅ設定";
						break;
				}

				if (!string.IsNullOrEmpty(sErrerCd)) {
					lblErrorMsg.Text = string.Format(this.GetErrorMessage(sErrerCd),sErrerMsg);
				}
			}
		}
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.USE_MOSAIC_ERASE,
				PwViCommConst.GameQuestTrialCategoryDetail.USE_MOSAIC_ERACE,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}
