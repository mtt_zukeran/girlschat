/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: GIGA POINT Post���M
--	Progaram ID		: PostGigaPoint
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PostGigaPoint:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		//string sRequestBackUrl = string.Format("http://{0}:8080/settle-if/ConfirmGigaPoint.aspx",sessionMan.site.subHostNm);
		//string sSettleBackUrl = string.Format("http://{0}:8080/settle-if/SettleGigaPoint.aspx", sessionMan.site.subHostNm);

		if (!IsPostBack) {
			int iAmt,iPoint;
			int.TryParse(Request.QueryString["settle_amount"],out iAmt);
			int.TryParse(Request.QueryString["point"],out iPoint);
			
			sessionMan.userMan.settleRequestAmt = iAmt;
			sessionMan.userMan.settleRequestPoint = iPoint;
			
			string sPmg = iBridUtil.GetStringValue(Request.QueryString["pmg"]);
			
			tagSiteCode.Text = "<input type=\"hidden\" name=\"site_code\" value=\"" + Request.QueryString["site_code"] + "\"></input>";
			tagMallOrderNo.Text = "<input type=\"hidden\" name=\"mall_order_no\" Value=\"" + Request.QueryString["mall_order_no"] + "\"></input>";
			tagMemberId.Text = "<input type=\"hidden\" name=\"member_id\" Value=\"" + Request.QueryString["member_id"] + "\"></input>";
			tagMemberPw.Text = "<input type=\"hidden\" name=\"member_pw\" Value=\"" + Request.QueryString["member_pw"] + "\"></input>";
			tagMailAddr.Text = "<input type=\"hidden\" name=\"mail_addr\" Value=\"" + Request.QueryString["mail_addr"] + "\"></input>";
			tagSettleAmt.Text = "<input type=\"hidden\" name=\"settle_amount\" Value=\"" + Request.QueryString["settle_amount"] + "\"></input>";
			tagPoint.Text = "<input type=\"hidden\" name=\"point\" Value=\"" + Request.QueryString["point"] + "\"></input>";
			tagReturnUrl.Text = "<input type=\"hidden\" name=\"return_url\" Value=\"" + Request.QueryString["return_url"] + "\"></input>";
			//tagRequestBackUrl.Text = "<input type=\"hidden\" name=\"request_back_url\" Value=\"" + sRequestBackUrl + "\"></input>";
			//tagSettlementBackUrl.Text = "<input type=\"hidden\" name=\"settlement_back_url\" Value=\"" + sSettleBackUrl + "\"></input>";
			if(!string.IsNullOrEmpty(sPmg)){
				tagPmg.Text = "<input type=\"hidden\" name=\"pmg\" Value=\"" + sPmg + "\"></input>";
			}
			
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
	}
}
