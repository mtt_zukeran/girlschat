<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetupVoiceAppComplete.aspx.cs" Inherits="ViComm_man_SetupVoiceAppComplete" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlVoiceAppOn" Runat="server">
			$PGM_HTML02;
		</mobile:Panel>
		<mobile:Panel ID="pnlVoiceAppOff" Runat="server">
			$PGM_HTML03;
		</mobile:Panel>
		$PGM_HTML04;
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
