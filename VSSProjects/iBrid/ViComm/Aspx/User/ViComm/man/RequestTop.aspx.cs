/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがいTOP
--	Progaram ID		: RequestTop
--
--  Creation Date	: 2012.06.29
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RequestTop:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		this.setLstProgress();
		this.setLstCategory();

		if (!IsPostBack) {
			txtKeyword.Text = HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.QueryString["kwd"]));

			foreach (MobileListItem item in lstProgress.Items) {
				if (item.Value == iBridUtil.GetStringValue(this.Request.QueryString["prg_seq"])) {
					item.Selected = true;
				}
			}

			foreach (MobileListItem item in lstCategory.Items) {
				if (item.Value == iBridUtil.GetStringValue(this.Request.QueryString["ctg_seq"])) {
					item.Selected = true;
				}
			}

			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_REQUEST,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void setLstProgress() {
		using (PwRequest oPwRequest = new PwRequest()) {
			DataSet dsProgress = oPwRequest.GetRequestProgress(sessionMan.site.siteCd);

			foreach (DataRow drProgress in dsProgress.Tables[0].Rows) {
				lstProgress.Items.Add(new MobileListItem(drProgress["REQUEST_PROGRESS_NM"].ToString(),drProgress["REQUEST_PROGRESS_SEQ"].ToString()));
			}
		}
	}

	private void setLstCategory() {
		using (PwRequest oPwRequest = new PwRequest()) {
			DataSet dsCategory = oPwRequest.GetRequestCategory(sessionMan.site.siteCd,ViCommConst.MAN,null,null);

			foreach (DataRow drCategory in dsCategory.Tables[0].Rows) {
				lstCategory.Items.Add(new MobileListItem(drCategory["REQUEST_CATEGORY_NM"].ToString(),drCategory["REQUEST_CATEGORY_SEQ"].ToString()));
			}

		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		UrlBuilder oUrlBuilder = new UrlBuilder("ListRequest.aspx");

		if (!txtKeyword.Text.Equals(string.Empty)) {
			if (txtKeyword.Text.Length > 30) {
				sessionMan.errorMessage = "ｷｰﾜｰﾄﾞが長すぎます";
				return;
			}

			Encoding enc = Encoding.GetEncoding("Shift_JIS");
			oUrlBuilder.Parameters.Add("kwd",HttpUtility.UrlEncode(txtKeyword.Text,enc));
		}

		if (!lstProgress.Items[lstProgress.SelectedIndex].Value.Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("prg_seq",lstProgress.Items[lstProgress.SelectedIndex].Value);
		}

		if (!lstCategory.Items[lstCategory.SelectedIndex].Value.Equals(string.Empty)) {
			oUrlBuilder.Parameters.Add("ctg_seq",lstCategory.Items[lstCategory.SelectedIndex].Value);
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
	}
}