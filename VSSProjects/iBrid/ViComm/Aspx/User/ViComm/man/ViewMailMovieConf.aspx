<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewMailMovieConf.aspx.cs" Inherits="ViComm_man_ViewMailMovieConf" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="60" Size="20"></cc1:iBMobileTextBox>
		$PGM_HTML02;
		$PGM_HTML03;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
