/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����ٹްїpFlash(�������ذ�)
--	Progaram ID		: ViewGameFlashComplete
--
--  Creation Date	: 2011.11.11
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_man_ViewGameFlashComplete:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["battle_log_seq"]);
		string sLevelUpFlag = iBridUtil.GetStringValue(this.Request.QueryString["level_up_flag"]);
		string sTownFirstClearFlag = iBridUtil.GetStringValue(this.Request.QueryString["town_first_clear_flag"]);

		string sFileNm = "m_complete.swf";
		string sNextPageNm = string.Empty;
		string sGetParamStr = string.Empty;

		if (!string.IsNullOrEmpty(sBattleLogSeq)) {
			string sAttackWinFlag = string.Empty;
			string sDefenceWinFlag = string.Empty;
			string sBattleType = string.Empty;

			this.GetBattleLogData(
				sBattleLogSeq,
				out sAttackWinFlag,
				out sDefenceWinFlag,
				out sBattleType
			);

			if(sLevelUpFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sNextPageNm = "ViewGameFlashLevelup.aspx";
			} else if (sAttackWinFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				if (sBattleType.Equals(PwViCommConst.GameBattleType.SINGLE)) {
					sNextPageNm = "ViewGameBattleResultWin.aspx";
				} else if (sBattleType.Equals(PwViCommConst.GameBattleType.PARTY)) {
					sNextPageNm = "ViewGameBattleResultWinParty.aspx";
				} else if (sBattleType.Equals(PwViCommConst.GameBattleType.SUPPORT)) {
					sNextPageNm = "ViewGameBattleResultWinSupport.aspx";
				}
			} else {
				if (sBattleType.Equals(PwViCommConst.GameBattleType.SINGLE)) {
					sNextPageNm = "ViewGameBattleResultLose.aspx";
				} else if (sBattleType.Equals(PwViCommConst.GameBattleType.PARTY)) {
					sNextPageNm = "ViewGameBattleResultLoseParty.aspx";
				} else if (sBattleType.Equals(PwViCommConst.GameBattleType.SUPPORT)) {
					sNextPageNm = "ViewGameBattleResultLoseSupport.aspx";
				}
			}
		} else {
			sNextPageNm = "ViewGameTownClearMissionOK.aspx";
		}

		if (this.Request.QueryString.Count > 0) {
			string[] keys = this.Request.QueryString.AllKeys;
			string value = string.Empty;
			List<string> oGetParamList = new List<string>();

			foreach (string key in keys) {
				oGetParamList.Add(key + "=" + iBridUtil.GetStringValue(this.Request.QueryString[key]));
			}

			sGetParamStr = string.Join("&",oGetParamList.ToArray());
		}

		NameValueCollection oSwfParam = new NameValueCollection();
		GameFlashLiteHelper.ResponseFlashSocialGame(sFileNm,sNextPageNm,sGetParamStr,oSwfParam);
	}

	private void GetBattleLogData(
		string sBattleLogSeq,
		out string sAttackWinFlag,
		out string sDefenceWinFlag,
		out string sBattleType
	) {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.UserSeq = sessionMan.userMan.userSeq;
		oCondition.UserCharNo = sessionMan.userMan.userCharNo;
		oCondition.BattleLogSeq = sBattleLogSeq;

		DataSet pDS = null;
		using (BattleLog oBattleLog = new BattleLog()) {
			pDS = oBattleLog.GetOneSimpleData(oCondition);
		}

		sAttackWinFlag = pDS.Tables[0].Rows[0]["ATTACK_WIN_FLAG"].ToString();
		sDefenceWinFlag = pDS.Tables[0].Rows[0]["DEFENCE_WIN_FLAG"].ToString();
		sBattleType = pDS.Tables[0].Rows[0]["BATTLE_TYPE"].ToString();
	}
}