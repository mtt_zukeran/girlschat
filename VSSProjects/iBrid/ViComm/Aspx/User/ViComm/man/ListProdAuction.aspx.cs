/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���i������ �ꗗ
--	Progaram ID		: ListProdAuction
--
--  Creation Date	: 2011.06.20
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdAuction : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		sessionMan.ControlList(
				this.Request,
				ViCommConst.INQUIRY_PRODUCT_AUCTION,
				this.ActiveForm);

	}
}
