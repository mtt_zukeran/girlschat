/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入りグループ追加
--	Progaram ID		: RegistFavoritGroup
--
--  Creation Date	: 2012.10.02
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistFavoritGroup:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (txtFavoritGroupNm.Text.Equals(string.Empty)) {
			lblErrorMessage.Text = "ｸﾞﾙｰﾌﾟ名を入力して下さい";
			return;
		} else if (txtFavoritGroupNm.Text.Length > 20) {
			lblErrorMessage.Text = "ｸﾞﾙｰﾌﾟ名の入力は20文字までです";
			return;
		}

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			sResult = oFavoritGroup.RegistFavoritGroup(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,txtFavoritGroupNm.Text))
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl("ListFavoritGroup.aspx"));
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}
