<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListEventArea.aspx.cs" Inherits="ViComm_man_ListEventArea" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		$PGM_HTML02;
		<mobile:SelectionList ID="lstPrefecture" Runat="server" BreakAfter="false"></mobile:SelectionList>
		$PGM_HTML03;
		<mobile:SelectionList ID="lstEventArea" Runat="server" BreakAfter="false"><Item Text="�w��Ȃ�" Value="" /></mobile:SelectionList>
		$PGM_HTML04;
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList"		runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail"	runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />		
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
