/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ZERO EDY Post���M
--	Progaram ID		: PostEdyZero
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PostEdyZero:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			clientip.Text = string.Format("<input type=\"hidden\" name=\"clientip\" value=\"{0}\"></input>",Request.QueryString["clientip"]);
			act.Text = string.Format("<input type=\"hidden\" name=\"act\" value=\"{0}\"></input>","mobile_order");
			money.Text = string.Format("<input type=\"hidden\" name=\"money\" value=\"{0}\"></input>",Request.QueryString["money"]);
			telno.Text = string.Format("<input type=\"hidden\" name=\"telno\" value=\"{0}\"></input>",Request.QueryString["telno"]);
			email.Text = string.Format("<input type=\"hidden\" name=\"email\" value=\"{0}\"></input>",Request.QueryString["email"]);
			sendid.Text = string.Format("<input type=\"hidden\" name=\"sendid\" value=\"{0}\"></input>",Request.QueryString["sendid"]);
			sendpoint.Text = string.Format("<input type=\"hidden\" name=\"sendpoint\" value=\"{0}\"></input>",Request.QueryString["sendpoint"]);
			success_url.Text = string.Format("<input type=\"hidden\" name=\"success_url\" value=\"{0}\"></input>",Request.QueryString["success_url"]);
			success_str.Text = string.Format("<input type=\"hidden\" name=\"success_str\" value=\"{0}\"></input>",Request.QueryString["success_str"]);
			failure_url.Text = string.Format("<input type=\"hidden\" name=\"failure_url\" value=\"{0}\"></input>",Request.QueryString["failure_url"]);
			failure_str.Text = string.Format("<input type=\"hidden\" name=\"failure_str\" value=\"{0}\"></input>",Request.QueryString["failure_str"]);

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
	}
}
