/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: クレジット音声ガイダンス決済認証
--	Progaram ID		: PaymentCreditZeroVoiceAuth
--
--  Creation Date	: 2015.07.09
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_PaymentCreditZeroVoiceAuth:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		string sAuthTelNo = iBridUtil.GetStringValue(this.Request.QueryString["authtelno"]);
		
		if (sAuthTelNo.Length < 10) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		} else if (!SysPrograms.Expression(@"03\d{8}",sAuthTelNo)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		lnkAuthTelNo.NavigateUrl = string.Format("tel:186{0}",iBridUtil.GetStringValue(sAuthTelNo));
		
		string sTel1 = sAuthTelNo.Substring(0,2);
		string sTel2 = sAuthTelNo.Substring(2,4);
		string sTel3 = sAuthTelNo.Substring(6,4);

		lnkAuthTelNo.Text = string.Format("186-{0}-{1}-{2}",sTel1,sTel2,sTel3);
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
	}
}