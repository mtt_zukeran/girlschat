/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: クレジット自動決済状態一覧
--	Progaram ID		: ListCreditAutoSettleStatus
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListCreditAutoSettleStatus:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CREDIT_AUTO_SETTLE_STATUS,this.ActiveForm);
	}
}