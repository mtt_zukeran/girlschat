/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログランキング 一覧
--	Progaram ID		: ListBlogRanking
--
--  Creation Date	: 2014.03.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListBlogRanking:MobileBlogManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_BLOG_RANKING,ActiveForm);
	}
}
