/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 個人別掲示板写真・動画詳細
--	Progaram ID		: ViewPersonalBbsObj
--
--  Creation Date	: 2010.09.10
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewPersonalBbsObj:MobileManPageBase {

	bool bDupUsed;
	string sMovieSeq;
	string sCastSeq;
	string sCastCharNo;
	int iChargePoint;
	int iPaymentAmt;

	virtual protected void Page_Load(object sender,EventArgs e) {
		bool bDownload = false;
		string sAllBbs = iBridUtil.GetStringValue(Request.QueryString["allbbs"]);

		if (!IsPostBack) {

			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				}
				else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}
			
			sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
			sCastCharNo = ViCommConst.MAIN_CHAR_NO;

			if (!sMovieSeq.Equals(string.Empty)) {
				bDownload = Download(sCastSeq,sCastCharNo,sMovieSeq);
			}
			if (bDownload == false) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				ulong uSeekMode;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode) == false) {
					uSeekMode = ViCommConst.INQUIRY_BBS_OBJ;
				}

				if (sessionMan.ControlList(Request,uSeekMode,ActiveForm)) {

					string sObjType = sessionMan.GetBbsObjValue("OBJ_KIND");
					string sLoginId;

					switch (sObjType) {
						case ViCommConst.BbsObjType.PIC:

							sCastSeq = sessionMan.GetBbsObjValue("USER_SEQ");
							sCastCharNo = sessionMan.GetBbsObjValue("USER_CHAR_NO");
							sLoginId = sessionMan.GetBbsObjValue("LOGIN_ID");
							string sPicSeq = sessionMan.GetBbsObjValue("PIC_SEQ");

							if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo)) {
								sessionMan.errorMessage = "";

								using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
									bDupUsed = oObjUsedHistory.GetOne(
														sessionMan.userMan.siteCd,
														sessionMan.userMan.userSeq,
														ViCommConst.ATTACH_PIC_INDEX.ToString(),
														sPicSeq);
								}

								if (bDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
									bool bOk = sessionMan.CheckBbsPicBalance(sCastSeq,sCastCharNo,sPicSeq,out iChargePoint,out iPaymentAmt);
									if (bOk == false) {
										RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_BBS_PIC));
									} else {
										using (WebUsedLog oLog = new WebUsedLog()) {
											oLog.CreateWebUsedReport(
													sessionMan.userMan.siteCd,
													sessionMan.userMan.userSeq,
													ViCommConst.CHARGE_BBS_PIC,
													iChargePoint,
													sCastSeq,
													sCastCharNo,
													"",
													iPaymentAmt);
										}
										using (Access oAccess = new Access()) {
											oAccess.AccessPicPage(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sPicSeq);
										}

										using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
											oObjUsedHistory.AccessBbsObj(
													sessionMan.userMan.siteCd,
													sessionMan.userMan.userSeq,
													ViCommConst.ATTACH_PIC_INDEX.ToString(),
													sPicSeq);
										}

										int iGetBbsBingoNoCompleteFlag = ViCommConst.FLAG_OFF;
										string sGetBbsBingoNoResult = string.Empty;

										using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
											oBbsBingoTerm.GetBbsBingoNo(
												sessionMan.site.siteCd,
												sessionMan.userMan.userSeq,
												sPicSeq,
												ViCommConst.FLAG_OFF,
												out iGetBbsBingoNoCompleteFlag,
												out sGetBbsBingoNoResult
											);
										}

										if (iGetBbsBingoNoCompleteFlag.Equals(ViCommConst.FLAG_ON)) {
											UrlBuilder oNextUrl = new UrlBuilder(ViCommPrograms.GetCurrentAspx(),sessionMan.requestQuery.QueryString);
											string sToUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(oNextUrl.ToString(),System.Text.Encoding.GetEncoding(932)));
											RedirectToMobilePage(sessionMan.GetNavigateUrl(sToUrl));
											return;
										}
									}
								}
							}
							break;

						case ViCommConst.BbsObjType.MOVIE:
							sessionMan.SetCastDataSetByLoginId(sessionMan.GetBbsObjValue("LOGIN_ID"),sCastCharNo,iRecNo);
							break;

						case ViCommConst.BbsObjType.BBSDOC:
							sessionMan.SetCastDataSetByLoginId(sessionMan.GetBbsObjValue("LOGIN_ID"),sCastCharNo,iRecNo);
							// 閲覧数を更新
							using (CastBbs oCastBbs = new CastBbs()) {
								oCastBbs.UpdateReadingCount(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sessionMan.GetBbsObjValue("BBS_SEQ"));
							}
							break;

						default:
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListPersonalBbsObj.aspx?allbbs={0}",sAllBbs)));
							break;
					}

				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListPersonalBbsObj.aspx?allbbs={0}",sAllBbs)));
				}
			}
		}
	}

	private bool Download(string pCastSeq,string pCastCharNo,string pMovieSeq) {
		ParseHTML oParseHTML = sessionMan.parseContainer;
		bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		sessionMan.errorMessage = "";
		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			bDupUsed = oObjUsedHistory.GetOne(
								sessionMan.userMan.siteCd,
								sessionMan.userMan.userSeq,
								ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
								sMovieSeq);
		}
		
		if (!MovieHelper.IsRangeRequest()) {
			if (bDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
				if (sessionMan.CheckBbsMovieBalance(sCastSeq,sCastCharNo,sMovieSeq,out iChargePoint,out iPaymentAmt) == false) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_BBS_MOVIE));
					return false;
				}
			}
		}

		if (bIsCheckOnly) return false;
		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				if (sessionMan.logined) {
					if (!MovieHelper.IsRangeRequest()) {
						if (bDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
							using (Access oAccess = new Access()) {
								oAccess.AccessMoviePage(sessionMan.site.siteCd, pCastSeq, pCastCharNo, pMovieSeq);
							}
							using (WebUsedLog oLog = new WebUsedLog()) {
								oLog.CreateWebUsedReport(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.CHARGE_BBS_MOVIE,
										iChargePoint,
										pCastSeq,
										pCastCharNo,
										"",
										iPaymentAmt);
							}
							using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
								oObjUsedHistory.AccessBbsObj(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
										pMovieSeq);
							}
						}
					}
				}

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}
}
