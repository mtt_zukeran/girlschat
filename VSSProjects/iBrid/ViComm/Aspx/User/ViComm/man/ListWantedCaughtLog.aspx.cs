/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せ スタンプ確認ページ
--	Progaram ID		: ListWantedCaughtLog
--
--  Creation Date	: 2013.06.10
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListWantedCaughtLog:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_WANTED_CAUGHT_LOG,this.ActiveForm);
	}
}