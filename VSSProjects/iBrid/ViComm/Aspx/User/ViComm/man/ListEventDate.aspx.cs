/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: イベント情報一覧（日付検索）
--	Progaram ID		: ListEventDate
--
--  Creation Date	: 2013.07.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListEventDate:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			CreateList();
			string sEventDate = iBridUtil.GetStringValue(this.Request.QueryString["eventdate"]);
			string sYear = string.Empty;
			string sMonth = string.Empty;
			string sDay = string.Empty;
			DateTime dt = new DateTime();
			if (!string.IsNullOrEmpty(sEventDate)) {
				if (!DateTime.TryParse(sEventDate,out dt)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
			} else {
				sEventDate = DateTime.Now.ToString("yyyy/MM/dd");
			}

			string[] sEventDateArr = sEventDate.Split('/');
			sYear = sEventDateArr[0];
			sMonth = sEventDateArr[1];
			sDay = sEventDateArr[2];

			foreach (MobileListItem item in lstYear.Items) {
				if (item.Value == sYear) {
					item.Selected = true;
				}
			}
			
			foreach (MobileListItem item in lstMonth.Items) {
				if (item.Value == sMonth) {
					item.Selected = true;
				}
			}
			
			foreach (MobileListItem item in lstDay.Items) {
				if (item.Value == sDay) {
					item.Selected = true;
				}
			}
		
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_EVENT,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sNotEndFlag = iBridUtil.GetStringValue(this.Request.QueryString["notend"]);
		string sEventCategory = iBridUtil.GetStringValue(this.Request.QueryString["eventcategory"]);
		string sUrl = "ListEventDate.aspx" + "?eventcategory=" + sEventCategory +"&eventdate=" + HttpUtility.UrlEncode(string.Format("{0}/{1}/{2}",lstYear.Selection.Value,lstMonth.Selection.Value,lstDay.Selection.Value)) + "&notend=" + sNotEndFlag;
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sUrl));
	}

	private void CreateList() {
		int iYear = DateTime.Today.Year;

		for (int i = 0;i < 2;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear += 1;
		}
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}
}