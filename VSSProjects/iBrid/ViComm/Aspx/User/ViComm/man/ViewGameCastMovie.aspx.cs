/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・動画詳細
--	Progaram ID		: ViewGameCastMovie
--
--  Creation Date	: 2011.09.05
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameCastMovie:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sCastUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		this.CheckGameRefusedByPartner(sCastUserSeq,sCastUserCharNo);
		
		if (!IsPostBack) {
			if (sessionMan.logined) {
				string sUserRecNo = this.Request.Params["userrecno"];
				string sLoginId = this.Request.Params["loginid"];
				string sCategory = this.Request.Params["category"];
				string sNocrypt = this.Request.Params["nocrypt"];
				string sSeekmode = this.Request.Params["seekmode"];
				string sObjseq = this.Request.Params["objseq"];
				string sPartnerUserSeq = this.Request.Params["partner_user_seq"];
				string sPartnerUserCharNo = this.Request.Params["partner_user_char_no"];
				string sCastUserMovieSeq = this.Request.Params["cast_user_movie_seq"];

				if (
					string.IsNullOrEmpty(sPartnerUserSeq) || 
					string.IsNullOrEmpty(sPartnerUserCharNo) || 
					string.IsNullOrEmpty(sCastUserMovieSeq) || 
					string.IsNullOrEmpty(sUserRecNo) || 
					string.IsNullOrEmpty(sLoginId) || 
					string.IsNullOrEmpty(sCategory) || 
					string.IsNullOrEmpty(sNocrypt) || 
					string.IsNullOrEmpty(sSeekmode)	|| 
					string.IsNullOrEmpty(sObjseq)
				) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}

				RedirectViewGameMoviePlay();
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				if (!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CAST_MOVIE_VIEW,ActiveForm)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByTermIdGame.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUrl = string.Empty;
		string sUserRecNo = this.Request.Params["userrecno"];
		string sLoginId = this.Request.Params["loginid"];
		string sCategory = this.Request.Params["category"];
		string sNocrypt = this.Request.Params["nocrypt"];
		string sSeekmode = this.Request.Params["seekmode"];
		string sObjseq = this.Request.Params["objseq"];
		string sPartnerUserSeq = this.Request.Params["partner_user_seq"];
		string sPartnerUserCharNo = this.Request.Params["partner_user_char_no"];
		string sCastUserMovieSeq = this.Request.Params["cast_user_movie_seq"];

		sUrl = string.Format(
						"ViewGameMoviePlay.aspx?userrecno={0}&loginid={1}&category={2}&nocrypt={3}&seekmode={4}&objseq={5}&partner_user_seq={6}"
						+ "&partner_user_char_no={7}&cast_user_movie_seq={8}",
						sUserRecNo,
						sLoginId,
						sCategory,
						sNocrypt,
						sSeekmode,
						sObjseq,
						sPartnerUserSeq,
						sPartnerUserCharNo,
						sCastUserMovieSeq
						);

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
	}

	protected void RedirectViewGameMoviePlay() {
		string sUrl = string.Empty;
		string sUserRecNo = this.Request.Params["userrecno"];
		string sLoginId = this.Request.Params["loginid"];
		string sCategory = this.Request.Params["category"];
		string sNocrypt = this.Request.Params["nocrypt"];
		string sSeekmode = this.Request.Params["seekmode"];
		string sObjseq = this.Request.Params["objseq"];
		string sPartnerUserSeq = this.Request.Params["partner_user_seq"];
		string sPartnerUserCharNo = this.Request.Params["partner_user_char_no"];
		string sCastUserMovieSeq = this.Request.Params["cast_user_movie_seq"];
		bool bDupUsed = false;
		int iValue = 0;

		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			bDupUsed = oObjUsedHistory.GetOne(
								sessionMan.userMan.siteCd,
								sessionMan.userMan.userSeq,
								ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
								sCastUserMovieSeq);
		}

		if (bDupUsed) {
			using (FriendlyPoint oCastGameMovie = new FriendlyPoint()) {
				iValue = oCastGameMovie.GetFriendlyRank(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									sessionMan.userMan.userCharNo,
									sPartnerUserSeq,
									sPartnerUserCharNo);
			}
			
			if (iValue <= PwViCommConst.GameFriendlyRank.BEST_FRIEND && iValue != ViCommConst.FLAG_OFF) {
				sUrl = string.Format(
								"ViewGameMoviePlay.aspx?userrecno={0}&loginid={1}&category={2}&nocrypt={3}&seekmode={4}&objseq={5}&partner_user_seq={6}"
								+ "&partner_user_char_no={7}&cast_user_movie_seq={8}",
								sUserRecNo,
								sLoginId,
								sCategory,
								sNocrypt,
								sSeekmode,
								sObjseq,
								sPartnerUserSeq,
								sPartnerUserCharNo,
								sCastUserMovieSeq
								);

				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
			}
		}
	}
}