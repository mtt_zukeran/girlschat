<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GameHandleNmRegist.aspx.cs" Inherits="ViComm_man_GameHandleNmRegist" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		<ibrid:iBMobileTextBox ID="txtHandleNm" runat="server" MaxLength="20" Size="12" BreakAfter="true" EmojiPaletteEnabled="true"></ibrid:iBMobileTextBox>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>