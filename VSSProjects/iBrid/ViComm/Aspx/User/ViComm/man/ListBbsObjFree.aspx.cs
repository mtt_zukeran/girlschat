/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝無料開放一覧
--	Progaram ID		: ListBbsObjFree
--
--  Creation Date	: 2013.07.29
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListBbsObjFree:MobileObjManPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		using (Site oSite = new Site()) {
			string[] sValue = null;
			oSite.GetValues(sessionMan.site.siteCd,"BBS_OBJ_FREE_VIEW_START_DATE,BBS_OBJ_FREE_VIEW_END_DATE",ref sValue);
			DateTime dtStartDate = DateTime.Parse(sValue[0]);
			DateTime dtEndDate = DateTime.Parse(sValue[1]);
			if (DateTime.Compare(dtStartDate,DateTime.Now) > 0 || DateTime.Compare(DateTime.Now,dtEndDate) > 0) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_BBS_OBJ_SPECIAL_FREE_END);
			}
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_OBJ,ActiveForm);

			if (sessionMan.site.bbsMovieAttrFlag != ViCommConst.FLAG_ON) {
				pnlSearchList.Visible = false;
			} else if (IsAvailableService(ViCommConst.RELEASE_DISABLE_ATTR_COMBO_BOX_CNT,2)) {
				CreateCastPicAttrComboBox();
			} else {
				// カテゴリSEQ取得
				int iCurrentCategoryIndex;
				string sCategorySeq = "";
				int.TryParse(iBridUtil.GetStringValue(Request.QueryString["category"]),out iCurrentCategoryIndex);
				if (iCurrentCategoryIndex > 0) {
					sCategorySeq = ((ActCategory)sessionMan.categoryList[iCurrentCategoryIndex]).actCategorySeq;
				}

				CreateObjAttrComboBox(string.Empty,string.Empty,sCategorySeq,iBridUtil.GetStringValue(Request.QueryString["viewmode"]),ViCommConst.ATTACHED_BBS.ToString(),true,true,true,false);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 選択された値をセッションに設定して検索します。
		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;

		string[] selectedValueList = selectedValue.Split(',');

		string sAddQuery = string.Format("&attrtypeseq={0}&attrseq={1}",selectedValueList[0],selectedValueList[1]);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
												   "ListBbsObjFree.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&attrtypeseq=[0-9]*|&attrseq=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}