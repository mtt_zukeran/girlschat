/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 個人別プロフィール動画一覧
--	Progaram ID		: ListPersonalProfileMovie
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListPersonalProfileMovie:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			int iChargePoint = 0;
			string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			bool bDownload = false;

			if (!sMovieSeq.Equals("")) {
				sessionMan.errorMessage = "";
//				if (sessionMan.CheckPFDownloadBalance(sCastSeq,sCastCharNo,sMovieSeq,out iChargePoint) == false) {
//					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_DOWNLOAD_PF_MOVIE));
//				} else {
					bDownload = Download(sCastSeq,sCastCharNo,sMovieSeq,iChargePoint);
//				}
			};
			if (bDownload == false) {
				DataRow dr;
				ulong ulSeekMode = ViCommConst.INQUIRY_DIRECT;
				bool bSetManData;

				if (iBridUtil.GetStringValue(Request.QueryString["direct"]).ToString().Equals("1")) {
					bSetManData = sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),sCastCharNo,1,out dr);
					if (bSetManData) {
						ViewState["CATEGORY_INDEX"] = dr["ACT_CATEGORY_IDX"].ToString();
					} else {
						if (sessionMan.logined) {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
						} else {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.nonUserTopId));
						}
					}
				} else {
					int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
					ulSeekMode = ulong.Parse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]));
					bSetManData = sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),sCastCharNo,iRecNo);
				}

				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

				if (bSetManData) {
					sessionMan.ControlList(
						Request,
						ulSeekMode,
						ActiveForm);
				}
			}
		}
	}

	private bool Download(string pCastSeq,string pCastCharNo,string pMovieSeq,int pChargePoint) {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (lSize > 0) {
				if (sessionMan.logined) {
					if(!MovieHelper.IsRangeRequest()){
/*
						using (WebUsedLog oLog = new WebUsedLog()) {
							oLog.CreateWebUsedReport(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.CHARGE_DOWNLOAD_PF_MOVIE,
									pChargePoint,
									pCastSeq,
									pCastCharNo,
									"");
						}
*/

						using (CastMovieView oCastMovieView = new CastMovieView()) {
							oCastMovieView.RegistCastMovieView(
								sessionMan.site.siteCd,
								sessionMan.userMan.userSeq,
								pMovieSeq
							);
						}
					}
				}

				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}
}
