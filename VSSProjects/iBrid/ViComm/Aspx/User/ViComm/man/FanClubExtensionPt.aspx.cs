/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: FC継続（ポイント消費）
--	Progaram ID		: FanClubExtensionPt
--
--  Creation Date	: 2013.08.06
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_FanClubExtensionPt:MobileManPageBase {
	private string sCastUserSeq;
	private string sCastCharNo;

	protected void Page_Load(object sender,EventArgs e) {
		sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (sCastUserSeq.Equals(string.Empty) || sCastCharNo.Equals(string.Empty)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!sessionMan.logined) {
			if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
			}
			return;
		}

		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			string sResult = oFanClubStatus.GetFanClubAdmissionEnable(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCastUserSeq,
				sCastCharNo
			);

			if (sResult.Equals(PwViCommConst.FanClubAdmissionEnableResult.RESULT_NG)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			pnlConfirm.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult = string.Empty;

		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			oFanClubStatus.FanClubExtensionPt(
				sessionMan.site.siteCd,
				sCastUserSeq,
				sCastCharNo,
				sessionMan.userMan.userSeq,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.FanClubExtensionPtResult.RESULT_OK)) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else if (sResult.Equals(PwViCommConst.FanClubExtensionPtResult.RESULT_NG_POINT_LACK)) {
			RedirectToDisplayDoc(ViCommConst.SCR_POINT_LACK_MAIL);
			return;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}
	}
}
