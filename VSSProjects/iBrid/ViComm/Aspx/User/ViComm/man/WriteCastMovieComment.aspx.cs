/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者動画コメント投稿
--	Progaram ID		: WriteCastMovieComment
--  Creation Date	: 2013.12.25
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteCastMovieComment:MobileManPageBase {
	private string sMovieSeq;

	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (sessionMan.logined == false) {
			if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
			}
			return;
		}

		sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);

		if (string.IsNullOrEmpty(sMovieSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			DataSet dsCastMovie;

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_MOVIE_VIEW,this.ActiveForm,out dsCastMovie)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			bool bBlogNgUser = false;
			string sCastUserSeq = iBridUtil.GetStringValue(dsCastMovie.Tables[0].Rows[0]["USER_SEQ"]);
			string sCastCharNo = iBridUtil.GetStringValue(dsCastMovie.Tables[0].Rows[0]["USER_CHAR_NO"]);

			using (BlogNgUser oBlogNgUser = new BlogNgUser()) {
				bBlogNgUser = oBlogNgUser.CheckExistBlogNgUser(
					sessionMan.site.siteCd,
					sCastUserSeq,
					sCastCharNo,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo
				);
			}

			if (bBlogNgUser) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_COMMENT_NG);
				return;
			}

			pnlWrite.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				this.cmdSubmit_Click(sender,e);
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult = string.Empty;
			string sCommentDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtCommentDoc.Text));

			using (CastMovieComment oCastMovieComment = new CastMovieComment()) {
				oCastMovieComment.WriteCastMovieComment(
					sessionMan.site.siteCd,
					sMovieSeq,
					sessionMan.userMan.userSeq,
					sCommentDoc,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.WriteCastMovieCommentResult.RESULT_OK)) {
				pnlWrite.Visible = false;
				pnlComplete.Visible = true;
			} else if (sResult.Equals(PwViCommConst.WriteCastMovieCommentResult.RESULT_NG_VIEW)) {
				sessionMan.errorMessage = "先に動画を閲覧してください";
			} else if (sResult.Equals(PwViCommConst.WriteCastMovieCommentResult.RESULT_NG_COMMENTED)) {
				sessionMan.errorMessage = "既にｺﾒﾝﾄ済みです";
			} else if (sResult.Equals(PwViCommConst.WriteCastMovieCommentResult.RESULT_NG_COMMENT_REFUSE)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_COMMENT_NG);
				return;
			} else if (sResult.Equals(PwViCommConst.WriteCastMovieCommentResult.RESULT_NG_CAST_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.ERR_REFUSE_MAN);
				return;
			} else if (sResult.Equals(PwViCommConst.WriteCastMovieCommentResult.RESULT_NG_SELF_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
				return;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}

	private bool CheckInput() {
		int iMaxLength = 50;
		string sNGWord;

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.Form["maxlength"]))) {
			int.TryParse(iBridUtil.GetStringValue(Request.Form["maxlength"]),out iMaxLength);
		}

		if (string.IsNullOrEmpty(txtCommentDoc.Text)) {
			sessionMan.errorMessage = "ｺﾒﾝﾄを入力してください";
			return false;
		}

		if (txtCommentDoc.Text.Length > iMaxLength) {
			sessionMan.errorMessage = "ｺﾒﾝﾄが長すぎます";
			return false;
		}

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}

		if (sessionMan.ngWord.VaidateDoc(txtCommentDoc.Text,out sNGWord) == false) {
			sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			return false;
		}

		return true;
	}
}
