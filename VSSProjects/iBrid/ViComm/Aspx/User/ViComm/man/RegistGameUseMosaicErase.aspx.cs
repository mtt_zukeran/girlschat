/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾓｻﾞ消しﾂｰﾙ使用
--	Progaram ID		: RegistGameUseMosaicErase.aspx
--
--  Creation Date	: 2011.09.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameUseMosaicErase:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.Params["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.Params["partner_user_char_no"]);
		string sCastGamePicSeq = iBridUtil.GetStringValue(this.Request.Params["cast_game_pic_seq"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty) || sCastGamePicSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE_VIEW,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				string sBuyFlg = iBridUtil.GetStringValue(this.Request.Params["NEXTLINK"]);
				cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sCastGamePicSeq,sBuyFlg);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pCastUserSeq,string pCastUserCharNo,string pCastGamePicSeq,string pBuyFlg) {
		string sResult = string.Empty;
		int iGameItemGetChargeFlg = ViCommConst.FLAG_OFF;

		this.CheckCanUseMosaicErase(pCastGamePicSeq);

		if (pBuyFlg.Equals(ViCommConst.FLAG_ON_STR)) {

			if (this.BuyGameItemMosaic()) {
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

			iGameItemGetChargeFlg = ViCommConst.FLAG_ON; 
		}
		
		using(GameItem oGameItem = new GameItem()) {
			oGameItem.UseMosaicErase(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				pCastUserSeq,
				pCastUserCharNo,
				pCastGamePicSeq,
				iGameItemGetChargeFlg,
				out sResult
			);

			if (sResult.Equals(PwViCommConst.GameUseMosaicErase.RESULT_OK)) {
				this.CheckQuestClear();

				using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
					oObjUsedHistory.AccessBbsObj(
						this.sessionMan.site.siteCd,
						this.sessionMan.userMan.userSeq,
						ViCommConst.ATTACH_PIC_INDEX.ToString(),
						pCastGamePicSeq
					);
				}
				
				string sRedirectUrl = string.Format(
					"ViewGameTreasure.aspx?mosaic_erase=1&partner_user_seq={0}&partner_user_char_no={1}&cast_game_pic_seq={2}",
					pCastUserSeq,
					pCastUserCharNo,
					pCastGamePicSeq
				);

				this.RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}

	private bool BuyGameItemMosaic() {
		string sGameItemSeq = string.Empty;
		string sResult = string.Empty;

		using (GameItem oGameItem = new GameItem()) {
			sGameItemSeq = oGameItem.GetGameItem(
				sessionMan.userMan.siteCd,
				sessionMan.sexCd,
				PwViCommConst.GameItemCategory.ITEM_CATEGOAY_TOOL,
				"GAME_ITEM_SEQ"
			);

			if (!sGameItemSeq.Equals(string.Empty)) {
				oGameItem.BuyGameItem(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sGameItemSeq,
					"1",
					out sResult
				);
			}
		}

		if (sResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
			return true;
		} else {
			return false;
		}
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.USE_MOSAIC_ERASE,
				PwViCommConst.GameQuestTrialCategoryDetail.USE_MOSAIC_ERACE,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
	
	private void CheckCanUseMosaicErase(string pCastGamePicSeq) {
		bool bOK = false;
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		
		using (PossessionManTreasure oPossessionManTreasure = new PossessionManTreasure()) {
			bOK = oPossessionManTreasure.CheckCanUseMosaicErase(sSiteCd,sUserSeq,sUserCharNo,pCastGamePicSeq);
		}
		
		if (!bOK) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}
}
