﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 違反報告メール送信
--	Progaram ID		: TxViolationReport
--
--  Creation Date	: 2010.05.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
  yyyy/MM/dd  xxxxxxxxx
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using ViComm;

public partial class ViComm_man_TxViolationReport:MobileManPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				}
				else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			// キャストのIDをクエリーから取得 
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			// キャストの属性データセットを取得
			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo) == false) {
				if (sessionMan.logined) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.nonUserTopId));
				}
			}

			// 男性側の情報を取得する 
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

			if (sessionMan.userMan.handleNm.Equals("")) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ModifyUserHandleNm.aspx"));
				return;
			}

			// 違反報告内容のコンボボックス作成
            lstViolationContents.Items.Add(new MobileListItem(this.GetViolationDefaultMessage(), ""));
			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_VIOLATION_TYPE);
				foreach (DataRow row in ds.Tables[0].Rows) {
					lstViolationContents.Items.Add(new MobileListItem(row["CODE_NM"].ToString(),row["CODE_NM"].ToString()));
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (lstViolationContents.SelectedIndex == -1) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_VIOLATION_NOT_SELECTED);
		} else if (string.IsNullOrEmpty(lstViolationContents.Items[lstViolationContents.SelectedIndex].Value)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_VIOLATION_NOT_SELECTED);
		}
		if (txtDoc.Text.Equals("")) {
			if (!IsAvailableService(ViCommConst.RELEASE_PERMIT_NO_BODY_REPORT)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
			}
		}

		if (bOk == false) {
			return;
		}

		// 絵文字変換処理 
		string sTitle = lstViolationContents.Items[lstViolationContents.SelectedIndex].Value;
		string sDoc = CreateDocument(txtDoc.Text);

		// 報告先のメールアドレス取得

		string sMailAddress;
		using (Site oSite = new Site()) {
			oSite.GetOne(sessionMan.site.siteCd);
			sMailAddress = oSite.reportEmailAddr;
		}

		// 違反送信先にメールを送る
		SendMail(sMailAddress,sMailAddress,sTitle,sDoc);

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TX_REPORT_MAIL_COMPLETED + "&" + Request.QueryString));
	}

	private string CreateDocument(string doc) {
		StringBuilder sb = new StringBuilder();
		sb.AppendFormat("・ID（コード）：{0}-{1}-{2}\r\n",sessionMan.GetCastValue("SITE_CD"),sessionMan.GetCastValue("LOGIN_ID"),sessionMan.GetCastValue("USER_CHAR_NO"));
		sb.AppendFormat("・ハンドル名：{0}\r\n",sessionMan.GetCastValue("HANDLE_NM"));
		sb.AppendFormat("・送信者：{0}({1}-{2})\r\n",sessionMan.userMan.handleNm,sessionMan.userMan.siteCd,sessionMan.userMan.loginId);
		sb.AppendFormat("・{0}頃送信\r\n",DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
		sb.Append("通報内容\r\n");
		sb.Append(doc);

		return sb.ToString();
	}

    protected virtual string GetViolationDefaultMessage() {
        return "違反内容を選択してください。";
    }
}
