/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE���ߐ���
--	Progaram ID		: ViewGameTownClearMissionOK
--
--  Creation Date	: 2011.09.07
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_man_ViewGameTownClearMissionOK:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sStageSeq = iBridUtil.GetStringValue(this.Request.QueryString["stage_seq"]);
		string sTownSeq = iBridUtil.GetStringValue(this.Request.QueryString["town_seq"]);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sStageSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		rgxMatch = rgx.Match(sTownSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		
		if (!IsPostBack) {
			string sGetTreasureLogSeq = iBridUtil.GetStringValue(this.Request.Params["get_treasure_log_seq"]);

			if (!sGetTreasureLogSeq.Equals("")) {
				if (!this.CheckRecentGetManTreasureLog(sGetTreasureLogSeq)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
			
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_TOWN,ActiveForm);
		}
	}

	private bool CheckRecentGetManTreasureLog(string pGetTreasureLogSeq) {
		bool bResult = false;

		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			if (oGetManTreasureLog.CheckRecent(this.sessionMan.site.siteCd,this.sessionMan.userMan.userSeq,this.sessionMan.userMan.userCharNo,pGetTreasureLogSeq)) {
				bResult = true;
			}
		}

		return bResult;
	}
}