/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せ スタンプFlash表示
--	Progaram ID		: ViewFlashWantedStamp
--
--  Creation Date	: 2013.06.10
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.IO;
using System.Drawing;
using System.Data;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_man_ViewFlashWantedStamp:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else if (sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else {
			string sNextUrl = "ListWantedCaughtLog.aspx";
			string sFileNm = string.Empty;
			string sCompleteFlag = iBridUtil.GetStringValue(this.Request.QueryString["completeflag"]);

			if (sCompleteFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sFileNm = "stamp_complete.swf";
			} else {
				sFileNm = "stamp.swf";
			}

			string sFileDir = Path.Combine(sessionMan.site.webPhisicalDir,PwViCommConst.FLASH_DIR);
			string sSwfFilePath = Path.Combine(sFileDir,sFileNm);
			byte[] bufTempSwf;

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!File.Exists(sSwfFilePath)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}

				bufTempSwf = FileHelper.GetFileBuffer(sSwfFilePath);
			}

			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("next_url",sNextUrl);
			byte[] oSwfBuffer = GameFlashLiteHelper.RewriteSwfBuffer(bufTempSwf,oParam);
			GameFlashLiteHelper.OutputSwf(oSwfBuffer);
		}
	}
}