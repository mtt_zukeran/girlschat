/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャラクタータイプ設定
--	Progaram ID		: GameCharacterTypeRegist.aspx
--
--  Creation Date	: 2011.07.21
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Web;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GameCharacterTypeRegist:MobileSocialGameManBase {
	
	protected void Page_Load(object sender,EventArgs e) {
		
		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {

		bool bExist = this.sessionMan.userMan.gameCharacter.GetCurrentInfo(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq);

		if (bExist) {
			int iTutorialStatus;
			int.TryParse(this.sessionMan.userMan.gameCharacter.tutorialStatus,out iTutorialStatus);

			if (!string.IsNullOrEmpty(this.sessionMan.userMan.gameCharacter.gameCharacterType)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_COMPLETE);
			}
		} 
	
		using (GameCharacter oGameCharacter = new GameCharacter()) {
			string sResult = oGameCharacter.SetupGameCharacterType(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				this.Request.QueryString["game_character_type"]
			);

			if (sResult == PwViCommConst.SetupGameCharacterType.RESULT_OK) {
				oGameCharacter.SetupTutorialStatus(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					"1"
				);
			}

			if (sResult == PwViCommConst.SetupGameCharacterType.RESULT_OK) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_COMPLETE);
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}	
		}
	}
	
	protected void cmdReturn_Click(object sender,EventArgs e) {
		RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_SELECT);
	}
	
}
