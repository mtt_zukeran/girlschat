﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListGameCompCalendar.aspx.cs" Inherits="ViComm_man_ListGameCompCalendar" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="ibrid" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<mobile:SelectionList ID="lstYear" Runat="server" SelectType="DropDown" BreakAfter="False">
		</mobile:SelectionList>年&nbsp;
		<mobile:SelectionList ID="lstMonth" Runat="server" SelectType="DropDown" BreakAfter="False">
		</mobile:SelectionList>月&nbsp;
		$PGM_HTML02;
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
