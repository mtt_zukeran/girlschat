﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ゲーム友達紹介記録一覧
--	Progaram ID		: ListGameIntroLog
--
--  Creation Date	: 2012.02.07
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameIntroLog:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sResult;
		
		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_GAME_INTRO_LOG,this.ActiveForm);
		} else if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
			using (GameIntroItem oGameIntroItem = new GameIntroItem()) {
				sResult = oGameIntroItem.GetGameIntroItem(
					this.sessionMan.site.siteCd,
					this.sessionMan.userMan.userSeq,
					this.sessionMan.userMan.userCharNo,
					null
				);
			}

			if (sResult == PwViCommConst.GetGameIntroItemResult.RESULT_OK) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(
					sessionMan.root + sessionMan.sysType,
					sessionMan.sessionId,
					"ListGameIntroLog.aspx?get_flag=1"
				));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			string sGameIntroLogSeq = string.Empty;

			for (int i = 1;i <= 10;i++) {
				if (Request.Form[ViCommConst.BUTTON_GOTO_LINK + "_" + i] != null) {
					sGameIntroLogSeq = iBridUtil.GetStringValue(Request.Params["game_intro_log_seq" + i.ToString()]);
				}
			}

			if (!sGameIntroLogSeq.Equals(string.Empty)) {
				using (GameIntroItem oGameIntroItem = new GameIntroItem()) {
					sResult = oGameIntroItem.GetGameIntroItem(
						this.sessionMan.site.siteCd,
						this.sessionMan.userMan.userSeq,
						this.sessionMan.userMan.userCharNo,
						sGameIntroLogSeq
					);
				}

				if (sResult == PwViCommConst.GetGameIntroItemResult.RESULT_OK) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(
						sessionMan.root + sessionMan.sysType,
						sessionMan.sessionId,
						"ListGameIntroLog.aspx?get_flag=1"
					));
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}
	}
}
