/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 登録申請(固体識別事前通知型・キャスト指名あり)
--	Progaram ID		: RegistUserRequestByTermIdCast
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Collections;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_RegistUserRequestByTermIdCast:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {


		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		ParseHTML oParseMan = sessionMan.parseContainer;
		if (sessionMan.getTermIdFlag) {
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
		} else {
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_GUID;
		}

		if (!IsPostBack) {
			// 偽装状態の場合、偽造終了画面へ転送
			if (sessionMan.IsImpersonated) {
				oParseMan.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + "/ViComm/man",Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_RETURN_TO_ORIGINAL_MENU));
			}

			if ((sessionMan.adCd.Equals("")) && (sessionMan.site.NonAdRegistReqAgeCertFlag != 0)) {
				oParseMan.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"RegistUserRequest.aspx"));
			}

			if (sessionMan.GetCastCount() <= 0) {
				oParseMan.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"RegistUserRequestByTermId.aspx"));
			}

			if (sessionMan.carrier.Equals(ViCommConst.DOCOMO) && iBridUtil.GetStringValue(Request.QueryString["guid"]).Equals(string.Empty)) {
				UrlBuilder oUrlBuilder = new UrlBuilder("RegistUserRequestByTermIdCast.aspx",Request.QueryString);
				oUrlBuilder.Parameters.Add("guid","on");
				oParseMan.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
			}

			CheckOtherSiteUser();

			string sFlag;
			using (ManageCompany oCompany = new ManageCompany()) {
				oCompany.GetValue("AUTO_GET_TEL_INFO_FLAG",out sFlag);
			}

			if (!IsAvailableService(ViCommConst.RELEASE_SP_TEL_NOT_ABSOLUTE,2)) {
				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
					sFlag = "0";
				}
			}

			if (sFlag.Equals("0")) {
				lblTel.Visible = true;
				txtTel.Visible = true;
				tagTel.Visible = true;
			} else {
				lblTel.Visible = false;
				txtTel.Visible = false;
				tagTel.Visible = false;
			}
			if (sessionMan.site.registHandleNmInputFlag == 1) {
				lblHandleNm.Visible = true;
				txtHandleNm.Visible = true;
				tagHandleNm.Visible = true;
			} else {
				lblHandleNm.Visible = false;
				txtHandleNm.Visible = false;
				tagHandleNm.Visible = false;
			}
			if (sessionMan.site.registBirthdayInputFlag == 1) {
				CreateList();
				lblBirthday.Visible = true;
				lblYear.Visible = true;
				lstYear.Visible = true;
				lblMonth.Visible = true;
				lstMonth.Visible = true;
				lblDay.Visible = true;
				lstDay.Visible = true;
				tagBirthday.Visible = true;
			} else {
				lblBirthday.Visible = false;
				lblYear.Visible = false;
				lstYear.Visible = false;
				lblMonth.Visible = false;
				lstMonth.Visible = false;
				lblDay.Visible = false;
				lstDay.Visible = false;
				tagBirthday.Visible = false;
			}

			sessionMan.userMan.GetAttrType(sessionMan.site.siteCd);

			int iCtrl = 0;
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				SelectionList lstUserManItem;
				iBMobileTextBox txtUserManItem;
				iBMobileLabel lblUserManItem;
				TextArea txtAreaUserManItem;

				if (sessionMan.userMan.attrList[i].registInputFlag) {
					lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iCtrl + 1)) as SelectionList;
					txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iCtrl + 1)) as iBMobileTextBox;
					lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",iCtrl + 1)) as iBMobileLabel;
					txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",iCtrl + 1)) as TextArea;

					lblUserManItem.Text = sessionMan.userMan.attrList[i].attrTypeNm;
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							txtAreaUserManItem.Visible = true;
							txtUserManItem.Visible = false;
							txtAreaUserManItem.Rows = int.Parse(sessionMan.userMan.attrList[i].rowCount);
						} else {
							txtAreaUserManItem.Visible = false;
							txtUserManItem.Visible = true;
						}
						lstUserManItem.Visible = false;
					} else {
						txtAreaUserManItem.Visible = false;
						txtUserManItem.Visible = false;
						lstUserManItem.Visible = true;
						CreateListUserManItem(lstUserManItem,i);
					}
					iCtrl++;
				}
			}
			for (;iCtrl < ViComm.ViCommConst.MAX_ATTR_COUNT;iCtrl++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",iCtrl + 1)) as Panel;
				pnlUserManItem.Visible = false;
			}
		}

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		txtPassword.Text = HttpUtility.HtmlEncode(txtPassword.Text);
		txtTel.Text = HttpUtility.HtmlEncode(txtTel.Text);
		txtHandleNm.Text = HttpUtility.HtmlEncode(txtHandleNm.Text);

		string sUtn = iBridUtil.GetStringValue(ViewState["UTN"]);
		ParseHTML oParseMan = sessionMan.parseContainer;
		oParseMan.parseUser.postAction = 0;

		if (sUtn.Equals("")) {
			sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
			CheckOtherSiteUser();
		}
		string siModeId = Mobile.GetiModeId(sessionMan.carrier,Request);

		string sResult;
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		string sNGWord = string.Empty;

		using (Man oMan = new Man()) {
			if (oMan.IsBlackUser(ViCommConst.NO_TYPE_TERMINAL_UNIQUE_ID,sUtn)) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
				bOk = false;
				return;
			}
			if (!siModeId.Equals(string.Empty) && oMan.IsBlackUser(ViCommConst.NO_TYPE_TERMINAL_UNIQUE_ID,siModeId)) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
				bOk = false;
				return;
			}
			if (oMan.IsBlackUser(ViCommConst.NO_TYPE_TEL,txtTel.Text)) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
				bOk = false;
				return;
			}
		}

		if (txtPassword.Visible) {
			if (txtPassword.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD);
				bOk = false;
			} else {
				if (!SysPrograms.Expression(@"^[a-zA-Z0-9]{4,8}$",txtPassword.Text)) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD_NG);
					bOk = false;
				}
			}
		} else {
			Random rnd = new Random();
			txtPassword.Text = string.Format("{0:D4}",rnd.Next(10000));
		}


		if (txtTel.Visible) {
			if (txtTel.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
				bOk = false;
			} else {
				if (!sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
						bOk = false;
					} else {
						using (Man oMan = new Man()) {
							string sSiteCd = sessionMan.site.siteCd;
							if (IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE)) {
								sSiteCd = string.Empty;
							}
							if (oMan.IsTelephoneRegistered(sSiteCd,txtTel.Text)) {
								lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
								bOk = false;
							}
						}
					}
				}
			}
		}

		if (sessionMan.site.registHandleNmInputFlag == 1) {
			if (txtHandleNm.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
				bOk = false;
			}
			if (sessionMan.ngWord.VaidateDoc(txtHandleNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		if (sessionMan.site.registBirthdayInputFlag == 1) {
			if ((lstYear.Selection.Value.Equals("")) || (lstMonth.Selection.Value.Equals("")) || (lstDay.Selection.Value.Equals(""))) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY);
				bOk = false;
			} else if (lstYear.Selection.Value.Length != 4) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_YYYY_NG);
				bOk = false;
			} else {
				if (bOk) {
					string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
					int iAge = ViCommPrograms.Age(sDate);
					int iOkAge;
					int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
					if (iOkAge == 0) {
						iOkAge = 18;
					}

					if (iAge > 99 || iAge == 0) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
					} else if (iAge < iOkAge) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18);
					}
				}
			}
		}



		string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
		int iCtrl = 0;

		if (bOk) {
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				if (sessionMan.userMan.attrList[i].registInputFlag) {
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",iCtrl + 1)) as TextArea;
							pAttrInputValue[iCtrl] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtAreaUserManItem.Text));
							if (pAttrInputValue[iCtrl].Length > 300) {
								pAttrInputValue[iCtrl] = SysPrograms.Substring(pAttrInputValue[iCtrl],300);
							}
						} else {
							iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iCtrl + 1)) as iBMobileTextBox;
							pAttrInputValue[iCtrl] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtUserManItem.Text));
						}
						if (pAttrInputValue[iCtrl].Equals(string.Empty)) {
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
							bOk = false;
						}
						if (sessionMan.ngWord.VaidateDoc(pAttrInputValue[iCtrl],out sNGWord) == false) {
							bOk = false;
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
						}
					} else {
						SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iCtrl + 1)) as SelectionList;
						if (lstUserManItem.Selection != null) {
							if (sessionMan.userMan.attrList[i].profileReqItemFlag && lstUserManItem.Selection.Value.Equals(string.Empty)) {
								lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
								bOk = false;
							} else {
								pAttrSeq[iCtrl] = lstUserManItem.Selection.Value;
							}
						}
					}
					pAttrTypeSeq[iCtrl] = sessionMan.userMan.attrList[i].attrTypeSeq;
					iCtrl++;
				}
			}
		}

		if (bOk) {
			if (!sessionMan.carrier.Equals(ViCommConst.ANDROID) && !sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
				if (sessionMan.getTermIdFlag) {
					if (sUtn.Equals("") || siModeId.Equals("")) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
					}
				} else {
					if (siModeId.Equals(string.Empty)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
					}
				}
			}
		}

		ArrayList alSiteCd = new ArrayList();

		if (IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE)) {
			for (int i = 0;i <= 10;i++) {
				if (Request.Form["registSiteCd" + i] != null) {
					alSiteCd.Add(iBridUtil.GetStringValue(Request.Form["registSiteCd" + i]));
				}
			}
			//ﾎﾟｰﾀﾙｻｲﾄからの登録でいずれのｻｲﾄにもﾁｪｯｸを入れていない場合
			if (sessionMan.site.siteCd.Substring(1,3).Equals("000") && alSiteCd.Count == 0) {
				lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_NOT_SELECT_SITE_CD);
				bOk = false;
			}
		}

		if (IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE)) {
			string sUserSeq;
			if (!sUtn.Equals(string.Empty) || !siModeId.Equals(string.Empty)) {
				if (IsRegistSite(sUtn,siModeId,out sUserSeq)) {
					string sHandleNm;
					string sBirthday;
					using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
						//ｷｬﾗｸﾀｰ登録
						oUserManCharacter.GetCharacterByPrioritySite(sUserSeq,out sHandleNm,out sBirthday);
						foreach (string sValue in alSiteCd) {
							oUserManCharacter.CreateManCharacter(sValue,sUserSeq,sHandleNm,sBirthday,sessionMan.adCd);
						}
					}
					RedirectToDisplayDoc(ViCommConst.SCR_MAN_CHARACTER_CREATE_COMPLITE);
				}
			}
		}

		if (bOk == false) {
			return;
		}

		using (TempRegist oRegist = new TempRegist()) {
			string sCastLoginId = "",sCastCharNo = "";
			string sBirthday = "";
			if (sessionMan.site.registBirthdayInputFlag == 1) {
				DateTime dtBiathday = new DateTime(int.Parse(lstYear.Selection.Value),int.Parse(lstMonth.Selection.Value),int.Parse(lstDay.Selection.Value));
				sBirthday = dtBiathday.ToString("yyyy/MM/dd");
			}
			sCastLoginId = sessionMan.GetCastValue("LOGIN_ID");
			sCastCharNo = sessionMan.GetCastValue("USER_CHAR_NO");

			oRegist.RegistUserManTemp(
				sUtn,
				siModeId,
				Request.UserHostAddress,
				txtTel.Text,
				txtPassword.Text,
				txtHandleNm.Text,
				sBirthday,
				sCastLoginId,
				sCastCharNo,
				out sessionMan.userMan.tempRegistId,
				out sResult,
				pAttrTypeSeq,
				pAttrSeq,
				pAttrInputValue,
				iCtrl,
				(string[])alSiteCd.ToArray(typeof(string)),
				ViCommConst.FLAG_OFF,
				string.Empty
			);

			switch (int.Parse(sResult)) {
				case ViCommConst.REG_USER_RST_UNT_EXIST:
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
					break;
			}
		}

		if (sResult.Equals("0")) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("RegistAddOnInfo.aspx?utn={0}&id={1}",sUtn,siModeId)));
		} else {
			return;
		}
	}

	//MAQIA内のいずれかのｻｲﾄに登録しているか
	private bool IsRegistSite(string pUtn,string pImodeId,out string pUserSeq) {
		bool bExist = false;
		pUserSeq = string.Empty;

		if (!pUtn.Equals(string.Empty)) {
			using (User oUser = new User()) {
				bExist = oUser.RegistSiteByUtn(string.Empty,pUtn,ViCommConst.MAN,out pUserSeq);
			}
		}
		if (bExist) {
			return bExist;
		}
		if (!pImodeId.Equals(string.Empty)) {
			using (User oUser = new User()) {
				bExist = oUser.RegistSiteByImodeId(string.Empty,pImodeId,ViCommConst.MAN,out pUserSeq);
			}
		}
		return bExist;
	}
	private void CreateList() {
		int iYear = DateTime.Today.Year - 18;

		for (int i = 0;i < 85;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear -= 1;
		}
		lstYear.SelectedIndex = 7;
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}


	private void CheckOtherSiteUser() {
		//ﾎﾟｰﾀﾙｻｲﾄを利用する
		if (IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE)) {
			string sUserSeq;
			//登録済(通常はiModeIdでUserTopが表示されるが、移行ﾃﾞｰﾀに固体識別のみ設定されている会員が存在する)
			if (IsRegistSite(Mobile.GetUtn(sessionMan.carrier,Request),Mobile.GetiModeId(sessionMan.carrier,Request),out sUserSeq)) {
				string sLoginId = "",sLoginPw = "";
				using (User oUser = new User()) {
					if (oUser.GetOne(sUserSeq,ViCommConst.MAN)) {
						sLoginId = oUser.loginId;
						sLoginPw = oUser.loginPassword;
					}
				}
				if (!sessionMan.site.siteCd.Substring(1,3).Equals("000")) {
					// 利用希望ｻｲﾄとActivationを行う。
					RedirectToMobilePage(string.Format("http://{0}/user/start.aspx?loginid={1}&password={2}&guid=on",sessionMan.site.hostNm,sLoginId,sLoginPw));
				} else {
					//現在ﾎﾟｰﾀﾙｻｲﾄに居る場合、ﾘﾀﾞｲﾚｸﾄ先を選択させる
					sessionMan.userMan.loginId = sLoginId;
					sessionMan.userMan.loginPassword = sLoginPw;
					pnlPortal.Visible = false;
				}
			}
		}
	}

	virtual protected void CreateListUserManItem(SelectionList pListUserManItem,int pAttrIndex) {
		if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_LIST) {
			pListUserManItem.Items.Add(new MobileListItem("未選択",string.Empty));
		}
		using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
			DataSet ds = oAttrTypeValue.GetList(sessionMan.site.siteCd,sessionMan.userMan.attrList[pAttrIndex].attrTypeSeq);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				pListUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
			}
		}
		if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_RADIO) {
			pListUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
			pListUserManItem.SelectedIndex = 0;
		}
	}
}
