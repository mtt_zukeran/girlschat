/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���ۃ��X�g�ǉ�
--	Progaram ID		: RegistRefuse
--
--  Creation Date	: 2010.05.10
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_RegistRefuse:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				}
				else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			DataRow dr;
			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo,out dr)) {
			
				if (!iBridUtil.GetStringValue(dr["USER_STATUS"]).Equals(ViCommConst.USER_WOMAN_NORMAL) ||
					!iBridUtil.GetStringValue(dr["NA_FLAG"]).Equals(ViCommConst.NaFlag.OK.ToString())) {
					RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
				}
			
				string sUserSeq = dr["USER_SEQ"].ToString();
				string sUserCharNo = dr["USER_CHAR_NO"].ToString();
				using (Refuse oRefuse = new Refuse()) {
					string sRefuseComment = "";
					if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,sUserSeq,sUserCharNo)) {
						sRefuseComment = oRefuse.refuseComment;
					}
					oRefuse.RefuseMainte(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.MAN,
						dr["USER_SEQ"].ToString(),
						dr["USER_CHAR_NO"].ToString(),
						"9",
						sRefuseComment,
						0);
				}
			}
		}
	}

}
