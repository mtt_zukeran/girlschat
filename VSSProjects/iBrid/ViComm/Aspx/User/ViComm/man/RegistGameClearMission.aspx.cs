/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾅﾝﾊﾟ
--	Progaram ID		: RegistGameClearMission.aspx
--
--  Creation Date	: 2011.09.01
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameClearMission:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sStageSeq = iBridUtil.GetStringValue(Request.QueryString["stage_seq"]);
			string sTownSeq = iBridUtil.GetStringValue(Request.QueryString["town_seq"]);
			string sRecoveryFlag = iBridUtil.GetStringValue(Request.QueryString["recovery_flag"]);
			string sNoRecoveryItemFlag = iBridUtil.GetStringValue(Request.QueryString["no_recovery_item_flag"]);

			if (!sStageSeq.Equals(string.Empty) && !sTownSeq.Equals(string.Empty)) {
				int iTutorialFlag = ViCommConst.FLAG_OFF;
				int? iItemAbsoluteGetFlag = null;
				int? iTreasureAbsoluteGetFlag = null;
				int iLevelUpFLag;					//レベルアップしたら1
				int iTownFirstClearFlag;			//初めてステージクリアしたら1
				int iAddForceCount;					//レベルアップや、街クリアで増えた部隊数
				string sGetTreasureLogSeq;			//獲得したお宝履歴SEQ なしの場合NULL
				string[] sGetGameItemSeq;			//獲得したアイテム
				string[] sGetGameItemCount;			//獲得したアイテム数
				int iGetRecordCount;				//獲得したアイテム種類数
				string[] sBreakGameItemSeq;			//壊れた所持アイテム
				int iBreakRecordCount;				//壊れた所持アイテム種類数
				string sResult;
				string sPreExp = this.sessionMan.userMan.gameCharacter.exp.ToString();
				string sPreGamePoint = this.sessionMan.userMan.gameCharacter.gamePoint.ToString();
				string sPreMissionForceCount = this.sessionMan.userMan.gameCharacter.missionForceCount.ToString();
				string sCastUserSeq = null;
				string sCastUserCharNo = null;

				if (sRecoveryFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.RecoveryForce(sNoRecoveryItemFlag);					
				}

				if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("1")) {
					iItemAbsoluteGetFlag = 0;
					iTreasureAbsoluteGetFlag = 0;
					iTutorialFlag = ViCommConst.FLAG_ON;
				} else if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("2")) {
					iItemAbsoluteGetFlag = 0;
					iTreasureAbsoluteGetFlag = 0;
					iTutorialFlag = ViCommConst.FLAG_ON;
				} else if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("3")) {
					iItemAbsoluteGetFlag = 0;
					iTreasureAbsoluteGetFlag = 0;
					iTutorialFlag = ViCommConst.FLAG_ON;
				}
				
				using (Town oTown = new Town()) {
					oTown.ClearMission(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						sessionMan.userMan.userCharNo,
						sStageSeq,
						sTownSeq,
						iItemAbsoluteGetFlag,
						iTreasureAbsoluteGetFlag,
						out iLevelUpFLag,
						out iTownFirstClearFlag,
						out iAddForceCount,
						out sGetTreasureLogSeq,
						out sGetGameItemSeq,
						out sGetGameItemCount,
						out iGetRecordCount,
						out sBreakGameItemSeq,
						out iBreakRecordCount,
						out sResult
					);
				}

				string sGetGameItemSeqList = string.Join("_",sGetGameItemSeq);
				string sGetGameItemCountList = string.Join("_",sGetGameItemCount);
				string sBreakGameItemSeqList = string.Join("_",sBreakGameItemSeq);

				StringBuilder oStrBuilder = new StringBuilder();

				if (sResult.Equals(PwViCommConst.GameClearMissionResult.RESULT_NG)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				} else if (sResult.Equals(PwViCommConst.GameClearMissionResult.RESULT_OK)) {
					
					this.CheckQuestClear();
					
					if (iTutorialFlag.Equals(ViCommConst.FLAG_ON)) {
						string sCastGamePicSeq = null;
						
						if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("3")) {
							DataSet oDataSetManTreasure;
							using (ManTreasure oManTreasure = new ManTreasure()) {
								oDataSetManTreasure = oManTreasure.GetManTreasureSeqData(this.sessionMan.site.siteCd);
							}

							foreach (DataRow drManTreasure in oDataSetManTreasure.Tables[0].Rows) {
								sCastUserSeq = drManTreasure["USER_SEQ"].ToString();
								sCastUserCharNo = drManTreasure["USER_CHAR_NO"].ToString();
								sCastGamePicSeq = drManTreasure["CAST_GAME_PIC_SEQ"].ToString();
							}
							
							sGetTreasureLogSeq = this.GetManTreasureLogCreate(sCastUserSeq,sCastUserCharNo,sCastGamePicSeq);
						} else {
							DataSet oDataSetTutorialMission;
							using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
								oDataSetTutorialMission = oCastGameCharacter.GetOneForTutorialMission(this.sessionMan.site.siteCd);
							}
							
							sCastUserSeq = oDataSetTutorialMission.Tables[0].Rows[0]["USER_SEQ"].ToString();
							sCastUserCharNo = oDataSetTutorialMission.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
						}
						
						int iNextTutorialStatus = int.Parse(sessionMan.userMan.gameCharacter.tutorialStatus) + 1;
						
						using (GameCharacter oGameCharacter = new GameCharacter()) {
							oGameCharacter.SetupTutorialStatus(
								sessionMan.userMan.siteCd,
								sessionMan.userMan.userSeq,
								sessionMan.userMan.userCharNo,
								iNextTutorialStatus.ToString()
							);
						}
					}

					string sPartnerUserSeq = string.Empty;
					string sPartnerUserCharNo = string.Empty;
					
					string sNextPageNm = string.Empty;
					
					if (!string.IsNullOrEmpty(sGetTreasureLogSeq)) {
						this.GetPartnerByGetTreasureLogSeq(sGetTreasureLogSeq,out sPartnerUserSeq,out sPartnerUserCharNo);
					} else {
						if (iTutorialFlag.Equals(ViCommConst.FLAG_ON)) {
							sPartnerUserSeq = sCastUserSeq;
							sPartnerUserCharNo = sCastUserCharNo;
						}else {
							string sAllFlag = iBridUtil.GetStringValue(this.Request.QueryString["all_flag"]);
							using (CastGameCharacter oCastGameCharacter = new CastGameCharacter()) {
								oCastGameCharacter.GetSeqForMission(this.sessionMan.site.siteCd,this.sessionMan.userMan.userSeq,this.sessionMan.userMan.userCharNo,sAllFlag,out sPartnerUserSeq,out sPartnerUserCharNo);
							}
						}
					}

					oStrBuilder.AppendFormat("?result={0}",sResult);
					oStrBuilder.AppendFormat("&stage_seq={0}",sStageSeq);
					oStrBuilder.AppendFormat("&town_seq={0}",sTownSeq);
					oStrBuilder.AppendFormat("&partner_user_seq={0}",sPartnerUserSeq);
					oStrBuilder.AppendFormat("&partner_user_char_no={0}",sPartnerUserCharNo);
					oStrBuilder.AppendFormat("&level_up_flag={0}",iLevelUpFLag.ToString());
					oStrBuilder.AppendFormat("&town_first_clear_flag={0}",iTownFirstClearFlag.ToString());
					oStrBuilder.AppendFormat("&add_force_count={0}",iAddForceCount.ToString());
					oStrBuilder.AppendFormat("&get_game_item_seq={0}",sGetGameItemSeqList);
					oStrBuilder.AppendFormat("&get_game_item_count={0}",sGetGameItemCountList);
					oStrBuilder.AppendFormat("&get_record_count={0}",iGetRecordCount.ToString());
					oStrBuilder.AppendFormat("&break_game_item_seq={0}",sBreakGameItemSeqList);
					oStrBuilder.AppendFormat("&break_record_count={0}",iBreakRecordCount.ToString());
					oStrBuilder.AppendFormat("&get_treasure_log_seq={0}",sGetTreasureLogSeq);
					oStrBuilder.AppendFormat("&pre_exp={0}",sPreExp);
					oStrBuilder.AppendFormat("&game_point={0}",sPreGamePoint);
					oStrBuilder.AppendFormat("&pre_mission_force_count={0}",sPreMissionForceCount);

					if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
						IDictionary<string,string> oParameters = new Dictionary<string,string>();

						oParameters.Add("result",sResult);
						oParameters.Add("stage_seq",sStageSeq);
						oParameters.Add("town_seq",sTownSeq);
						oParameters.Add("partner_user_seq",sPartnerUserSeq);
						oParameters.Add("partner_user_char_no",sPartnerUserCharNo);
						oParameters.Add("level_up_flag",iLevelUpFLag.ToString());
						oParameters.Add("town_first_clear_flag",iTownFirstClearFlag.ToString());
						oParameters.Add("add_force_count",iAddForceCount.ToString());
						oParameters.Add("get_game_item_seq",sGetGameItemSeqList);
						oParameters.Add("get_game_item_count",sGetGameItemCountList);
						oParameters.Add("get_record_count",iGetRecordCount.ToString());
						oParameters.Add("break_game_item_seq",sBreakGameItemSeqList);
						oParameters.Add("break_record_count",iBreakRecordCount.ToString());
						oParameters.Add("get_treasure_log_seq",sGetTreasureLogSeq);
						oParameters.Add("pre_exp",sPreExp);
						oParameters.Add("game_point",sPreGamePoint);
						oParameters.Add("pre_mission_force_count",sPreMissionForceCount);
						
						if (iTownFirstClearFlag == ViCommConst.FLAG_ON) {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_MOKUHYOU_SP,oParameters);
						} else if (iLevelUpFLag == ViCommConst.FLAG_ON) {
							RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_LEVELUP_SP,oParameters);
						} else {
							sNextPageNm = "ViewGameTownClearMissionOK.aspx";
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sNextPageNm + oStrBuilder.ToString()));
						}
					} else {
						if(iTownFirstClearFlag == ViCommConst.FLAG_ON) {
							sNextPageNm = "ViewGameFlashMokuhyo.aspx";
						} else if(iLevelUpFLag == ViCommConst.FLAG_ON) {
							sNextPageNm = "ViewGameFlashLevelup.aspx";
						} else {
							sNextPageNm = "ViewGameTownClearMissionOK.aspx";
						}

						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sNextPageNm + oStrBuilder.ToString()));
					}
				} else if (sResult.Equals(PwViCommConst.GameClearMissionResult.RESULT_NO_ITEM)) {

					oStrBuilder.Append("ListGameClearTownUsedItemShortage.aspx");
					oStrBuilder.AppendFormat("?stage_seq={0}",sStageSeq);
					oStrBuilder.AppendFormat("&town_seq={0}",sTownSeq);
					oStrBuilder.AppendFormat("&mission_result={0}",sResult);
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,oStrBuilder.ToString()));
				} else {
					oStrBuilder.Append("ViewGameTownClearMissionNG.aspx");
					oStrBuilder.AppendFormat("?result={0}",sResult);
					oStrBuilder.AppendFormat("&stage_seq={0}",sStageSeq);
					oStrBuilder.AppendFormat("&town_seq={0}",sTownSeq);

					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,oStrBuilder.ToString()));
				}
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}

	private void RecoveryForce(string sNoRecoveryItemFlag) {
		string sGameItemSeq;
		string sRecoveryForceResult;

		using (GameItem oGameItem = new GameItem()) {
			sGameItemSeq = oGameItem.GetGameItem(
				this.sessionMan.userMan.siteCd,
				this.sessionMan.sexCd,
				PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE,
				"GAME_ITEM_SEQ"
			);

			if (sNoRecoveryItemFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				string sBuyGameItemResult;
				
				oGameItem.BuyGameItem(
					this.sessionMan.site.siteCd,
					this.sessionMan.userMan.userSeq,
					this.sessionMan.userMan.userCharNo,
					sGameItemSeq,
					"1",
					out sBuyGameItemResult
				);

				if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
					sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
				} else if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_NO_MONEY)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_NO_POINT);
				} else if (sBuyGameItemResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_NG)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}

		using(RecoveryForce oRecoveryForce = new RecoveryForce()) {
			oRecoveryForce.RecoveryForceFull(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				0,
				int.Parse(sGameItemSeq),
				PwViCommConst.GameMskForce.MSK_ALL_FORCE,
				out sRecoveryForceResult
			);
		}

		if (!sRecoveryForceResult.Equals(PwViCommConst.GameRecoveryForceFullStatus.RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}

	private string GetManTreasureLogCreate(string pCastUserSeq,string pCastUserCharNo,string pCastGamePicSeq) {
		string sGetTreasureLogSeq;
		string sStatus;
		
		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oGetManTreasureLog.GetManTreasureLogCreate(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				pCastUserSeq,
				pCastUserCharNo,
				pCastGamePicSeq,
				out sGetTreasureLogSeq,
				out sStatus
			);
		}
		
		return sGetTreasureLogSeq;
	}

	private void GetPartnerByGetTreasureLogSeq(string pGetTreasureLogSeq,out string pPartnerUserSeq,out string pPartnerUserCharNo) {
		pPartnerUserSeq = string.Empty;
		pPartnerUserCharNo = string.Empty;
		DataSet oDataSet;

		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oDataSet = oGetManTreasureLog.GetOne(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				pGetTreasureLogSeq
			);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			pPartnerUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
			pPartnerUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		}
	}

	private string GetGameTutorialTownSeq() {
		string sValue = string.Empty;

		TownSeekCondition oCondition = new TownSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.SexCd = this.sessionMan.sexCd;

		using (Town oTown = new Town()) {
			sValue = oTown.GetGameTutorialTownSeq(oCondition);
		}

		return sValue;
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.MISSION,
				PwViCommConst.GameQuestTrialCategoryDetail.MISSION,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}
