<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewBbsBingoLog.aspx.cs" Inherits="ViComm_man_ViewBbsBingoLog" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlResult" runat="server" Visible="false">
			$PGM_HTML02;
		</mobile:Panel>
		<mobile:Panel ID="pnlError" runat="server" Visible="false">
			$PGM_HTML03;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
