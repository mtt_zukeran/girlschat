/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �r���S�{�[���l��(�Y�t)
--	Progaram ID		: GetAttachedBingoBall
--
--  Creation Date	: 2011.07.08
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_GetAttachedBingoBall:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sMailSeq = iBridUtil.GetStringValue(Request.QueryString["mailseq"]);
			if (!sMailSeq.Equals(string.Empty)) {
				int iGetNewBallFlag;
				int iGetOldBallFlag;
				int iGetCardBallFlag;
				int iGetBallNo;
				int iCompliateFlag;
				int iBingoBallFlag;
				string sResult = sessionMan.bingoCard.GetBingoBall(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN,sMailSeq,string.Empty,out iGetNewBallFlag,out iGetOldBallFlag,out iGetCardBallFlag,out iGetBallNo,out iCompliateFlag,out iBingoBallFlag);
				string sBingoQuery = string.Format("ballnew={0}&ballold={1}&cardball={2}&ballno={3}&comp={4}&result={5}&objtype={6}&bingoball={7}",iGetNewBallFlag,iGetOldBallFlag,iGetCardBallFlag,iGetBallNo,iCompliateFlag,sResult,iBridUtil.GetStringValue(Request.QueryString["objtype"]),iBingoBallFlag);
				string sNextUrl = "GetAttachedBingoBall.aspx?" + sBingoQuery;

				if (iCompliateFlag.Equals(ViCommConst.FLAG_ON)) {
					sNextUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(sNextUrl,Encoding.GetEncoding(932)));
				}

				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sNextUrl));
			}
		}
	}
}
