/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せターゲット詳細
--	Progaram ID		: ViewInvestigateTarget
--
--  Creation Date	: 2015.03.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewInvestigateTarget:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_INVESTIGATE_TARGET,ActiveForm);
	}
}
