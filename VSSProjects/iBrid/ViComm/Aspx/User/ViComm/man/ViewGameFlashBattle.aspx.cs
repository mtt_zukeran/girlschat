/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@����ٹްїpFlash(�ʏ�����)
--	Progaram ID		: ViewGameFlashBattle
--
--  Creation Date	: 2011.11.10
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_man_ViewGameFlashBattle:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sBattleLogSeq = this.Request.QueryString["battle_log_seq"];

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(sBattleLogSeq));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		string sLevelUpFlag = Request.QueryString["level_up_flag"];
		string sAddForceCount = Request.QueryString["add_force_count"];
		string sBreakItemSeq = Request.QueryString["break_item_seq"];
		string sPreExp = Request.QueryString["pre_exp"];
		string sTreasureCompleteFlag = iBridUtil.GetStringValue(Request.QueryString["treasure_complete_flag"]);
		string sTutorialFlag = Request.QueryString["tutorial_flag"];
		string sComePoliceFlag = iBridUtil.GetStringValue(Request.QueryString["come_police_flag"]);
		string sTrapUsedFlag = iBridUtil.GetStringValue(Request.QueryString["trap_used_flag"]);
		string sPartnerUserSeq = string.Empty;
		string sPartnerUserCharNo = string.Empty;
		string sAttackWinFlag = string.Empty;
		string sDefenceWinFlag = string.Empty;
		string sPartnerGameCharacterType = string.Empty;
		string sPreFriendlyPoint = iBridUtil.GetStringValue(Request.QueryString["pre_friendly_point"]);

		this.GetBattleLogData(
			sBattleLogSeq,
			out sPartnerUserSeq,
			out sPartnerUserCharNo,
			out sAttackWinFlag,
			out sDefenceWinFlag,
			out sPartnerGameCharacterType
		);

		if (string.IsNullOrEmpty(sPartnerGameCharacterType)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		string sFileNm = string.Empty;
		string sNextPageNm;
		string sGetParamStr = string.Empty;
		
		if(sTreasureCompleteFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sNextPageNm = "ViewGameFlashComplete.aspx";
		} else {
			if(sLevelUpFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				sNextPageNm = "ViewGameFlashLevelup.aspx";
			} else {
				if(sAttackWinFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					sNextPageNm = "ViewGameBattleResultWin.aspx";
				} else {
					sNextPageNm = "ViewGameBattleResultLose.aspx";
				}
			}
		}

		
		if(sAttackWinFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sFileNm = "battle_win.swf";
		} else {
			sFileNm = "battle_lose.swf";
		}

		if (this.Request.QueryString.Count > 0) {
			string[] keys = this.Request.QueryString.AllKeys;
			string value = string.Empty;
			List<string> oGetParamList = new List<string>();

			foreach (string key in keys) {
				oGetParamList.Add(key + "=" + iBridUtil.GetStringValue(this.Request.QueryString[key]));
			}

			sGetParamStr = string.Join("&",oGetParamList.ToArray());
		}
		
		NameValueCollection oSwfParam = new NameValueCollection();
		oSwfParam.Add("char1",sessionMan.userMan.gameCharacter.gameCharacterType);
		oSwfParam.Add("char2",sPartnerGameCharacterType);
		oSwfParam.Add("sex1",ViCommConst.MAN);
		oSwfParam.Add("sex2",ViCommConst.MAN);
		GameFlashLiteHelper.ResponseFlashSocialGame(sFileNm,sNextPageNm,sGetParamStr,oSwfParam);
	}
	
	private void GetBattleLogData(
		string sBattleLogSeq,
		out string sPartnerUserSeq,
		out string sPartnerUserCharNo,
		out string sAttackWinFlag,
		out string sDefenceWinFlag,
		out string sPartnerGameCharacterType
	) {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.UserSeq = sessionMan.userMan.userSeq;
		oCondition.UserCharNo = sessionMan.userMan.userCharNo;
		oCondition.BattleLogSeq = sBattleLogSeq;
		
		DataSet pDS = null;
		using(BattleLog oBattleLog = new BattleLog()) {
			pDS = oBattleLog.GetOneSimpleData(oCondition);
		}
		
		sPartnerUserSeq = pDS.Tables[0].Rows[0]["PARTNER_USER_SEQ"].ToString();
		sPartnerUserCharNo = pDS.Tables[0].Rows[0]["PARTNER_USER_CHAR_NO"].ToString();
		sAttackWinFlag = pDS.Tables[0].Rows[0]["ATTACK_WIN_FLAG"].ToString();
		sDefenceWinFlag = pDS.Tables[0].Rows[0]["DEFENCE_WIN_FLAG"].ToString();
		sPartnerGameCharacterType = pDS.Tables[0].Rows[0]["PARTNER_GAME_CHARACTER_TYPE"].ToString();
	}
}
