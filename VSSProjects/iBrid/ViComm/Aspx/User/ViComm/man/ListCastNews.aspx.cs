/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者ニュース一覧
--	Progaram ID		: ListCastNews
--
--  Creation Date	: 2013.02.02
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListCastNews:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_NEWS,this.ActiveForm);
	}
}