/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝ブックマーク削除
--	Progaram ID		: DeleteObjBookmark
--
--  Creation Date	: 2012.07.31
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_DeleteObjBookmark:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sObjSeq = iBridUtil.GetStringValue(Request.QueryString["objseq"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));

			if (string.IsNullOrEmpty(sObjSeq)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			string sResult = string.Empty;

			using (ObjBookmark oObjBookmark = new ObjBookmark()) {
				sResult = oObjBookmark.DeleteObjBookmark(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sObjSeq
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				if (!string.IsNullOrEmpty(sBackUrl)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sBackUrl));
					return;
				} else {
					Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				}
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
