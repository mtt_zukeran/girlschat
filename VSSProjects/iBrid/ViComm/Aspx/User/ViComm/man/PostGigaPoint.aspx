<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PostGigaPoint.aspx.cs" Inherits="ViComm_man_PostGigaPoint" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" Action="https://www.aflopigs.jp/giga_b/point.php" Method="post">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
		<cc1:iBMobileLiteralText ID="tagSiteCode" runat="server" />
		<cc1:iBMobileLiteralText ID="tagMallOrderNo" runat="server" />
		<cc1:iBMobileLiteralText ID="tagMemberId" runat="server" />
		<cc1:iBMobileLiteralText ID="tagMemberPw" runat="server" />
		<cc1:iBMobileLiteralText ID="tagMailAddr" runat="server" />
		<cc1:iBMobileLiteralText ID="tagSettleAmt" runat="server" />
		<cc1:iBMobileLiteralText ID="tagPoint" runat="server" />
		<cc1:iBMobileLiteralText ID="tagReturnUrl" runat="server" />
		<cc1:iBMobileLiteralText ID="tagRequestBackUrl" runat="server" />
		<cc1:iBMobileLiteralText ID="tagSettlementBackUrl" runat="server" />
		<cc1:iBMobileLiteralText ID="tagPmg" runat="server" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
