/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: FanClub�ڼޯ�QUICK�x��
--	Progaram ID		: FanClubCreditQuick
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_FanClubCreditQuick:MobileManPageBase {
	private string sid;
	private string sCastUserSeq;
	private string sCastCharNo;

	virtual protected void Page_Load(object sender,EventArgs e) {

		this.sid = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		this.sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		this.sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (!sessionMan.logined) {
			if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
			}
			return;
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSettleUrl = "";
		string sSalesAmt = "350";
	
		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_CREDIT_PACK)) {
				sSettleUrl = string.Format(oSiteSettle.continueSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,this.sid,sSalesAmt,sessionMan.userMan.tel);
			}
		}

		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("castuserseq",sCastUserSeq);
		oParam.Add("castcharno",sCastCharNo);

		if (TransQuick(sSettleUrl)) {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(this.sid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"0");
			}
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_FC_QUICK_OK,oParam);
		} else {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(this.sid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"9");
			}
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_FC_QUICK_NG,oParam);
		}
	}


	public bool TransQuick(string pUrl) {
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sRes.Equals("Success_order");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransQuick",pUrl);
			return false;
		}
	}
}
