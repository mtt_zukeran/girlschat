/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: IVP申請
--	Progaram ID		: RequestIVP
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Web;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_RequestIVP:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {


		if (!IsPostBack) {
			NameValueCollection oQuery = new NameValueCollection(Request.QueryString);

			int iRecNo;
			int.TryParse(iBridUtil.GetStringValue(oQuery["userrecno"]),out iRecNo);
			if (iRecNo == 0) {
				iRecNo = 1;
			}

			string sMeetingKey = iBridUtil.GetStringValue(Request.QueryString["meeting"]);

			if (!sMeetingKey.Equals(string.Empty)) {
				using (Meeting oMeeting = new Meeting()) {
					if (oMeeting.GetOne(sMeetingKey)) {
						oQuery.Add("request",oMeeting.requestId);
						oQuery.Add("menuid",oMeeting.menuId);
						oQuery.Add("castseq",oMeeting.userSeq);
						oQuery.Add("chgtype",oMeeting.chargeType);
						oQuery.Add("refresh","1");
					}
				}
			}

			string sRequestCastSeq = iBridUtil.GetStringValue(oQuery["castseq"]);
			string sRefresh = iBridUtil.GetStringValue(oQuery["refresh"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

			if (sRequestCastSeq.Equals("") == false) {
				sessionMan.userMan.inviteServicePoint = 0;
				sessionMan.userMan.inviteServiceReuqest = false;
				if (sessionMan.SetCastDataSetByUserSeq(sRequestCastSeq,sCastCharNo,iRecNo)) {
					using (Refuse oRefuse = new Refuse()) {
						DataRow oDataRow = sessionMan.parseContainer.parseUser.dataTable[ViCommConst.DATASET_CAST].Rows[0];

						// 拒否登録している
						if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]))) {
							RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
						}

						// 拒否登録されている
						this.CheckRefuse(iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]));
					}
				}
			}


			if (sRefresh.Equals("1")) {
				string sCurSeekMode;
				if (iBridUtil.GetStringValue(sessionMan.seekMode).Equals(string.Empty)) {
					sCurSeekMode = ViCommConst.INQUIRY_DIRECT.ToString();
				} else {
					sCurSeekMode = sessionMan.seekMode;
				}

				bool bFind = sessionMan.ControlList(
						Request,
						ViCommConst.INQUIRY_DIRECT,
						ActiveForm);

				if (bFind == false) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
					return;
				}
				sessionMan.seekMode = sCurSeekMode;
			}


			if ((sessionMan.userMan.inviteTalkKey.Equals("") == false) && (sessionMan.userMan.inviteCastSeq.Equals(sRequestCastSeq)) && (sessionMan.userMan.inviteCastCharNo.Equals(sCastCharNo))) {
				if (IsValidInvite(sessionMan.userMan.inviteTalkKey,sRequestCastSeq) == false) {
					sessionMan.userMan.inviteTalkKey = "";
					sessionMan.userMan.inviteCastSeq = "";
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_TALK_INVITE_MAIL_INVALID));
					return;
				}
			}

			bool bLogin;
			sessionMan.ContorIVP(this,oQuery,out bLogin);
			if (bLogin == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			if ((sessionMan.site.useOtherSysInfoFlag == 0) && (sessionMan.userMan.handleNm.Equals(""))) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ModifyUserHandleNm.aspx"));
				return;
			}

			bool bTelRedirect = false;

			if (string.IsNullOrEmpty(sessionMan.userMan.tel)) {
				if (sessionMan.ivpRequest.useTwilioFlag) {
					if (!sessionMan.ivpRequest.useVoiceapp) {
						bTelRedirect = true;
					}
				} else if (!sessionMan.ivpRequest.useCrosmile && IsAvailableService(ViCommConst.RELEASE_USER_TEL_REDIRECT,2)) {
					bTelRedirect = true;
				}
			}

			if (bTelRedirect) {
				UrlBuilder oUrlBuilder = new UrlBuilder("RequestIVP.aspx");

				foreach (string sKey in this.Request.QueryString) {
					oUrlBuilder.AddParameter(sKey,this.Request.QueryString[sKey]);
				}

				string sBackUrl = HttpUtility.UrlEncode(oUrlBuilder.ToString(),Encoding.GetEncoding("Shift_JIS"));

				RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("ModifyUserTel.aspx?backurl={0}",sBackUrl)));
				return;
			}

			string sPageKey = "";
			string sChargeType = iBridUtil.GetStringValue(oQuery["chgtype"]);
			switch (sChargeType) {
				case ViCommConst.CHARGE_TALK_WSHOT:
					sPageKey = "RequestWShotTalk.aspx";
					if (!sMeetingKey.Equals(string.Empty)) {
						sPageKey = "RequestMeetingTvTalk.aspx";
					}
					break;
				case ViCommConst.CHARGE_TALK_PUBLIC:
					sPageKey = "RequestPublicTalk.aspx";
					break;

				case ViCommConst.CHARGE_TALK_VOICE_WSHOT:
					sPageKey = "RequestVoiceTalk.aspx";
					if (!sMeetingKey.Equals(string.Empty)) {
						sPageKey = "RequestMeetingVoiceTalk.aspx";
					}
					break;

				case ViCommConst.CHARGE_TALK_VOICE_PUBLIC:
					sPageKey = "RequestVoiceTalk.aspx";
					break;
				case ViCommConst.CHARGE_VIEW_TALK:
					sPageKey = "RequestTalkMonitor.aspx";
					break;
				case ViCommConst.CHARGE_VIEW_ONLINE:
					sPageKey = "RequestRoomMonitor.aspx";
					break;
				case ViCommConst.CHARGE_PLAY_MOVIE:
					sPageKey = "RequestPlayMovie.aspx";
					break;
				case ViCommConst.CHARGE_VIEW_LIVE:
					sPageKey = "RequestViewLive.aspx";
					break;
				case ViCommConst.CHARGE_PLAY_PF_VOICE:
					sPageKey = "RequestVoicePlayProfile.aspx";
					break;
				case ViCommConst.CHARGE_REC_PF_VOICE:
					sPageKey = "RequestVoiceRecProfile.aspx";
					break;
				case ViCommConst.CHARGE_WIRETAPPING:
					sPageKey = "RequestVoiceWireTapping.aspx";
					break;
				case ViCommConst.CHARGE_PLAY_PV_MSG:
					sPageKey = "RequestVoicePlayPrivateMsg.aspx";
					break;
				default:
					switch (iBridUtil.GetStringValue(oQuery["request"])) {
						case ViCommConst.REQUEST_TEL_AUTH_LIGHT:
							sPageKey = "RequestSmartPhoneCertify.aspx";
							break;
					}
					break;
			}
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,sPageKey,ViewState);
		}
	}


	private bool IsValidInvite(string pKey,string pRequestCastSeq) {
		string sManUserSeq,sCastUserSeq,sCastCharNo;
		int iServicePoint,iTransfered;
		DateTime dEffectTime;
		bool bOk = false;

		using (MailLog oMailLog = new MailLog()) {
			if (oMailLog.IsValidInvaiteMail(
						pKey,
						out sManUserSeq,
						out sCastUserSeq,
						out sCastCharNo,
						out iServicePoint,
						out dEffectTime,
						out iTransfered)) {
				if ((sManUserSeq.Equals(sessionMan.userMan.userSeq)) &&
					(sCastUserSeq.Equals(sessionMan.userMan.inviteCastSeq)) &&
					(sCastCharNo.Equals(sessionMan.userMan.inviteCastCharNo))) {
					sessionMan.userMan.inviteServicePoint = iServicePoint;
					sessionMan.userMan.inviteServiceReuqest = true;
					bOk = true;
				}
			}
		}
		return bOk;
	}
}
