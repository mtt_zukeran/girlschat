/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: FanClub����
--	Progaram ID		: FanClubAdmission
--
--  Creation Date	: 2013.01.31
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_FanClubAdmission:MobileManPageBase {
	private string sCastUserSeq;
	private string sCastCharNo;

	protected void Page_Load(object sender,EventArgs e) {
		sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (!sessionMan.logined) {
			if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
			}
			return;
		}

		if (sCastUserSeq.Equals(string.Empty) || sCastCharNo.Equals(string.Empty)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			string sResult = oFanClubStatus.GetFanClubAdmissionEnable(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCastUserSeq,
				sCastCharNo
			);

			if (sResult.Equals(PwViCommConst.FanClubAdmissionEnableResult.RESULT_NG)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}

		if (!IsPostBack) {
			if (iBridUtil.GetStringValue(Request.QueryString["scrid"]).Equals("01")) {
				chkForceNonQuick.Visible = false;
				chkForceNonQuick.Items[0].Selected = true;
			} else {
				if (sessionMan.userMan.existCreditDealFlag.Equals(ViCommConst.FLAG_ON)) {
					chkForceNonQuick.Visible = true;
				} else {
					chkForceNonQuick.Visible = false;
				}
			}

			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sForceNonQuick;
		string sResult = string.Empty;
		string sSid = string.Empty;

		if (chkForceNonQuick.Items[0].Selected == true) {
			sForceNonQuick = ViCommConst.FLAG_ON_STR;
		} else {
			sForceNonQuick = ViCommConst.FLAG_OFF_STR;
		}

		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			oFanClubStatus.FanClubAdmission(
				sessionMan.site.siteCd,
				sCastUserSeq,
				sCastCharNo,
				sessionMan.userMan.userSeq,
				sForceNonQuick,
				out sResult,
				out sSid
			);
		}

		if (sResult.Equals(PwViCommConst.FanClubAdmissionResult.RESULT_OK)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("FanClubCredit.aspx?sid={0}",sSid)));
			return;
		} else if (sResult.Equals(PwViCommConst.FanClubAdmissionResult.RESULT_OK_WITH_QUICK)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("FanClubCreditQuick.aspx?sid={0}&castuserseq={1}&castcharno={2}",sSid,sCastUserSeq,sCastCharNo)));
			return;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}
	}
}
