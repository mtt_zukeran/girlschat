/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���ٔs�k(����)
--	Progaram ID		: ViewGameBattleResultLoseSupport
--
--  Creation Date	: 2011.08.30
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_man_ViewGameBattleResultLoseSupport:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.checkRecentBattleLog();

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE_SUPPORT,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}

	private void checkRecentBattleLog() {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition(this.Request.QueryString);
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.UserSeq = this.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
		oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

		using (BattleLog oBattleLog = new BattleLog()) {
			if (!oBattleLog.CheckRecentBattleLog(oCondition)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}
}
