/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: コンビニダウンロード支払
--	Progaram ID		: PaymentCVSDL
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.IO;
using System.Text;
using System.Net;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCVSDL:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {


		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			//using (Pack oPack = new Pack()) {
				DataSet ds;
			//	ds = oPack.GetList(sessionMan.site.siteCd,ViCommConst.SETTLE_CVSDL,0);
				ds = GetPackDataSet(ViCommConst.SETTLE_CVSDL,0);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				}
			//}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
			//	DataSet ds;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_CVSDL);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstCVS.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
				}
			}
			txtTel.Text = sessionMan.userMan.tel;
			txtFamilyNm.Text = sessionMan.userMan.familyNm;
			txtGivenNm.Text = sessionMan.userMan.givenNm;
			txtTel.Text = sessionMan.userMan.tel;
			lblEMailAddr.Text = sessionMan.userMan.emailAddr;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstPack.Items[lstPack.SelectedIndex].Value;
		string sSid = "";
		string sAddr = "東京都";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CVSDL,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_BIGSUN,
					ViCommConst.SETTLE_CVSDL,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					string.Empty,
					string.Empty,
					out sSid);
			}
		}
		string sSettleUrl = "";
		string sFamilyNm = "";
		string sGivenNm = "";
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (!txtFamilyNm.Text.Equals("")) {
			sFamilyNm = System.Web.HttpUtility.UrlEncode(txtFamilyNm.Text,enc);
		} else {
			sFamilyNm = "*";
		}
		if (!txtGivenNm.Text.Equals("")) {
			sGivenNm = System.Web.HttpUtility.UrlEncode(txtGivenNm.Text,enc);
		} else {
			sGivenNm = "*";
		}

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_BIGSUN,ViCommConst.SETTLE_CVSDL)) {
				sSettleUrl = string.Format(oSiteSettle.settleUrl,
					oSiteSettle.cpIdNo,
					sSid,
					sFamilyNm,
					sGivenNm,
					txtTel.Text,
					sAddr,
					lblEMailAddr.Text,
					sSalesAmt,
					lstCVS.Items[lstCVS.SelectedIndex].Value,
					sessionMan.userMan.loginId,
					sessionMan.userMan.userSeq);
			}
		}
		if (TransCVS(sSettleUrl)) {
			sessionMan.userMan.ModifyUserName(txtFamilyNm.Text,txtGivenNm.Text);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=087"));
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=088"));
		}
	}


	public bool TransCVS(string pUrl) {
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 20000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				string[] sItem = sRes.Split('\n');
				sr.Close();
				st.Close();
				if (sItem.Length > 0) {
					sessionMan.userMan.csvdlname = lstCVS.Items[lstCVS.SelectedIndex].Text;

					if (sItem.Length >= 8 && sItem[7].IndexOf("receipt_no:") >= 0) {
						string[] sCD = sItem[7].Split('\t');
						for (int i = 0;i < sCD.Length;i++) {
							if (sCD[i].IndexOf("receipt_no:") == 0) {
								sessionMan.userMan.csvdlreceiptno = sCD[i].Substring("receipt_no:".Length);
							}
						}
					}

					return sItem[0].Equals("OK");
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransCVS",pUrl);
			return false;
		}
	}
}
