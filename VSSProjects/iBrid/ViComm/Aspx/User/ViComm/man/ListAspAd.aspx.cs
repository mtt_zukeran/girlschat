﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ASP広告一覧
--	Progaram ID		: ListAspAd
--
--  Creation Date	: 2010.09.27
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using ViComm;
using iBridCommLib;

public partial class ViComm_man_ListAspAd:MobileManPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_ASP_AD,ActiveForm);
			sessionMan.userMan.aspAdTitle = iBridUtil.GetStringValue(Request.QueryString["aspadtitle"]);
		}
	}
}
