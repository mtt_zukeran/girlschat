﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品着せ替えﾂｰﾙお気に入り一覧
--	Progaram ID		: ListProdThemeFavorit
--
--  Creation Date	: 2011.07.01
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;
public partial class ViComm_man_ListProdThemeFavorite : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					this.Request,
					ViCommConst.INQUIRY_PRODUCT_THEME_FAVORITE,
					this.ActiveForm);

		}
	}
}
