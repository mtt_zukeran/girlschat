/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｸﾚｼﾞｯﾄQUICK支払(ZEROｸﾚｼﾞｯﾄ)
--	Progaram ID		: PaymentCreditZeroQuick
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Security.Cryptography.X509Certificates;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCreditZeroQuick:MobileManPageBase {
	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sAmt = iBridUtil.GetStringValue(Request.QueryString["money"]);
			DataSet ds;
			ds = GetPackDataSet(ViCommConst.SETTLE_CREDIT_PACK,0);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
					sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
					sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
					sessionMan.userMan.settleServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
					break;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSettleUrl = "";
		string sSid = iBridUtil.GetStringValue(Request.QueryString["sid"]);
		string sSalesAmt = iBridUtil.GetStringValue(Request.QueryString["money"]);

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_CREDIT_PACK)) {
				sSettleUrl = string.Format(oSiteSettle.continueSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sessionMan.userMan.tel);
			}
		}

		if (TransQuick(sSettleUrl)) {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"0");
			}
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_CREDIT_QUICK_OK));
		} else {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"9");
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_CREDIT_QUICK_NG));
		}
	}


	public bool TransQuick(string pUrl) {
		try {
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sRes.Equals("Success_order");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransQuick",pUrl);
			return false;
		}
	}

	private bool OnRemoteCertificateValidationCallback(
	  Object sender,
	  X509Certificate certificate,
	  X509Chain chain,
	  SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}
}
