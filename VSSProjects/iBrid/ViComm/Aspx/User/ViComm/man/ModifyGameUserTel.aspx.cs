/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 電話番号変更
--	Progaram ID		: ModifyGameUserTel
--
--  Creation Date	: 2012.03.20
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ModifyGameUserTel:MobileSocialGameManBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);


		if (!IsPostBack) {
			txtTel.Text = string.Empty;

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (txtTel.Visible) {
			if (txtTel.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
				bOk = false;
			} else {
				if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
					bOk = false;
				} else {
					using (Man oMan = new Man()) {
						if (oMan.IsTelephoneRegistered(sessionMan.site.siteCd,txtTel.Text)) {
							lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
							bOk = false;
						}
					}
				}
			}
		}

		if (bOk == false) {
			txtTel.Text = HttpUtility.HtmlEncode(txtTel.Text);
			return;
		} else {
			string sResult;

			sessionMan.userMan.ModifyUserTel(
				sessionMan.userMan.userSeq,
				txtTel.Text,
				sessionMan.site.siteCd,
				out sResult
			);

			if (sResult.Equals("0")) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_MODIFY_TEL_COMPLETE);
			} else {
				txtTel.Text = HttpUtility.HtmlEncode(txtTel.Text);
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
			}
		}
	}
}
