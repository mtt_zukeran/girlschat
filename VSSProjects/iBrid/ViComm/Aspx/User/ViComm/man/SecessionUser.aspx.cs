/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 退会
--	Progaram ID		: SecessionUser
--
--  Creation Date	: 2010.10.14
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Collections;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_SecessionUser:MobileManPageBase {
	/// <summary>手動退会処理対象となる最低課金額(info宛にメール送信)</summary>
	private const int SEND_MAIL_MIN_RECEIPT_AMT = 2980;

	private string withdrawalSeq;

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		// 退会済み会員をリダイレクトさせる
		RedirectWithdrawalUser();

		if (!IsPostBack) {
			// 退会済み以外＆退会申請中以外の場合、退会申請レコードを生成する
			// (選択された理由の登録)
			if (!sessionMan.userMan.userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)
				&& !sessionMan.userMan.existAcceptWithdrawalFlag.Equals(ViCommConst.FLAG_ON_STR)
			) {
				withdrawalSeq = string.Empty;
				sessionMan.userMan.WithdrawalMainte(ref withdrawalSeq,iBridUtil.GetStringValue(Request.QueryString["reason"]));
				ViewState["WITHDRAWAL_SEQ"] = withdrawalSeq;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				// 退会済み以外＆退会申請中以外の場合、退会申請処理を行う
				if (!sessionMan.userMan.userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)
					&& !sessionMan.userMan.existAcceptWithdrawalFlag.Equals(ViCommConst.FLAG_ON_STR)
				) {
					cmdSubmit_Click(sender,e);
				}
				// 上記以外の場合、退会申請完了画面へ遷移させる
				else {
					RedirectToDisplayDoc(ViCommConst.SCR_MAN_ACCEPT_WITHDRAWAL_COMPLETE);
				}
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = string.Empty;
		string sResult = string.Empty;
		string url = string.Empty;
		string sSendMailAddr = string.Empty;
		string sDrawalDoc = string.Empty;

		ArrayList list = new ArrayList();

		foreach (string sKey in Request.Form.AllKeys) {
			if (sKey.Equals("*mail")) {
				sSendMailAddr = Request.Form[sKey];
			} else if (sKey.StartsWith("*withdrawaldoc")) {
				list.Add(Request.Form[sKey]);
				sDrawalDoc = Request.Form[sKey];
				if (!this.CheckDrawalDoc(sDrawalDoc)) {
					return;
				}
			} else if (sKey.StartsWith("*")) {
				list.Add(Request.Form[sKey]);
			}
		}

		if (iBridUtil.GetStringValue(Request.QueryString["onlyaccept"]).Equals(ViCommConst.FLAG_ON_STR)) {
			withdrawalSeq = iBridUtil.GetStringValue(ViewState["WITHDRAWAL_SEQ"]);
			sessionMan.userMan.WithdrawalMainte(ref withdrawalSeq,sDrawalDoc);
			sResult = "0";
		} else {
			sessionMan.userMan.SecessionUser(out sResult);
		}

		sResult = "0";

		if (sResult.Equals("0")) {
			// 退会申請中フラグをたてる
			sessionMan.userMan.existAcceptWithdrawalFlag = ViCommConst.FLAG_ON_STR;

			// 退会時ポイント付与日時（最新1件）を取得
			UserDefPointHistory oDefPointHistory = new UserDefPointHistory();
			string pointDate = oDefPointHistory.GetWithdrawalAddPointDate(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			if (!string.IsNullOrEmpty(pointDate)) {
				// 退会時ポイント付与日時以降の課金額を取得
				Receipt oReceipt = new Receipt();
				int receiptAmt = oReceipt.GetReceiptAmtFromCreateDate(sessionMan.site.siteCd,sessionMan.userMan.userSeq,pointDate);

				// 退会時ポイント付与日時以降の課金額が一定以上の場合
				if (receiptAmt >= SEND_MAIL_MIN_RECEIPT_AMT) {
					// 手動対応フラグをたてる
					sessionMan.userMan.UpdateManualWithdrawal(withdrawalSeq);

					// 送信先のメールアドレスがあればメールを送信する
					if (!sSendMailAddr.Equals(string.Empty)) {
						string sDoc = string.Empty;
						sDoc += string.Format("退会者ログインID:{0}\n",sessionMan.userMan.loginId);
						sDoc += string.Format("退会者メールアドレス：{0}\n",sessionMan.userMan.emailAddr);
						sDoc += string.Format("退会者ハンドルネーム：{0}\n\n",sessionMan.userMan.handleNm);
						string[] sDocList = (string[])list.ToArray(typeof(string));
						sDoc += string.Join("\n\n",sDocList);

						string sManNm = string.Empty;
						sManNm = "会員";

						string sSubject;
						if (iBridUtil.GetStringValue(Request.QueryString["onlyaccept"]).Equals(ViCommConst.FLAG_ON_STR)) {
							sSubject = string.Format("{0}退会申請",sManNm);
						} else {
							sSubject = string.Format("{0}が退会しました",sManNm);
						}
						SendMail(sSendMailAddr,sSendMailAddr,sSubject,sDoc);
					}
				}
			}

			if (!iBridUtil.GetStringValue(Request.QueryString["onlyaccept"]).Equals(ViCommConst.FLAG_ON_STR)) {
				url = sessionMan.GetNavigateUrlNoSession("Start.aspx?goto=DisplayDoc.aspx&doc=" + ViCommConst.SCR_MAN_SECESSION_COMPLITE +
						"&tran=secession&sc=" + sessionMan.site.siteCd +
						"&host=" + sessionMan.site.hostNm);
				Session.Abandon();
				RedirectToMobilePage(url);
			} else {
				RedirectToDisplayDoc(ViCommConst.SCR_MAN_ACCEPT_WITHDRAWAL_COMPLETE);
			}
		} else {
			lblErrorMessage.Text += GetErrorMessage(sResult);
		}
	}

	protected virtual bool CheckDrawalDoc(string pDrawalDoc) {
		if (pDrawalDoc.Equals(string.Empty)) {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_WITHDRAWAL_REASON_DOC_EMPTY);
			return false;
		}

		return true;
	}

	/// <summary>
	/// 退会済みの会員の場合のリダイレクト処理
	/// </summary>
	private void RedirectWithdrawalUser() {
		string url = string.Empty;
		string userStatus = string.Empty;
		string existsAccept = string.Empty;
		User oUser = new User();
		DataSet ds = oUser.GetWithdrawalStatus(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo);
		DataRow dr = null;
		if (ds.Tables[0].Rows.Count <= 0) {
			// ユーザ情報が取得できなかった場合
			sessionMan.userMan.existAcceptWithdrawalFlag = string.Empty;
			return;
		}
		dr = ds.Tables[0].Rows[0];
		userStatus = dr["USER_STATUS"].ToString();
		existsAccept = dr["EXISTS_ACCEPT"].ToString();

		// セッションに保存（パース処理での判定時に利用）
		sessionMan.userMan.userStatus = userStatus;
		sessionMan.userMan.existAcceptWithdrawalFlag = existsAccept;

		// 会員状態が退会の場合
		if (userStatus.Equals(ViCommConst.USER_MAN_RESIGNED)) {
			url = sessionMan.GetNavigateUrlNoSession("Start.aspx");
			Session.Abandon();
			RedirectToMobilePage(url);
		}
	}
}
