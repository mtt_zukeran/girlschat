/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・お気に入り一覧
--	Progaram ID		: ListGameCharacterFavorite.aspx
--
--  Creation Date	: 2011.08.05
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameCharacterFavorite:MobileSocialGameManBase {
	private const string ONLINE_STATUS_FAVORITE = "FindGameCharacterOnlineStatusFavorite";
	private const string HANDLE_NAME_FAVORITE = "FindGameCharacterHandleNmFavorite";
	private const string AGE_FAVORITE = "FindGameCharacterAgeFavorite";
	
	protected void Page_Load(object sender,EventArgs e) {
		pnlTreasure.Visible = true;
		pnlName.Visible = true;
		
		if(!IsPostBack) {
			GetQueryParm();

			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);

			int iOnlineStatus;
			int.TryParse(iBridUtil.GetStringValue(Session[ONLINE_STATUS_FAVORITE]),out iOnlineStatus);

			if (iOnlineStatus == ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING) {
				// ﾛｸﾞｲﾝ中
				rdoStatus.Items[0].Selected = false;
				rdoStatus.Items[1].Selected = true;
			} else {
				// 全て
				rdoStatus.Items[0].Selected = true;
			}

			txtHandelNm.Text = iBridUtil.GetStringValue(Session[HANDLE_NAME_FAVORITE]);

			// 年齢
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY)) {
					foreach (MobileListItem item in lstAge.Items) {
						if (item.Value == iBridUtil.GetStringValue(Session[AGE_FAVORITE])) {
							item.Selected = true;
							break;
						}
					}
				} else {
					pnlAge.Visible = false;
				}
			}
			
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_FAVORITE_LIST,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		Encoding enc = Encoding.GetEncoding("Shift_JIS");
		string sHandleNm = string.Empty;
		string sAgeFrom = string.Empty;
		string sAgeTo = string.Empty;
		string sUrl = string.Empty;
		string sTreasurePic = string.Empty;
		string sTreasureMovie = string.Empty;

		string sSort = HttpUtility.UrlDecode(iBridUtil.GetStringValue(this.Request.QueryString["sort"]));

		bool bOk = true;

		int iOnlineStatus = 0;

		if (chkTreasurePic.SelectedIndex >= 0) {
			sTreasurePic = chkTreasurePic.Selection.Value;
		}

		if (chkTreasureMovie.SelectedIndex >= 0) {
			sTreasureMovie = chkTreasureMovie.Selection.Value;
		}

		if (pnlName.Visible) {
			sHandleNm = HttpUtility.UrlEncode(HttpUtility.HtmlEncode(txtHandelNm.Text),enc);
			txtHandelNm.Text = HttpUtility.HtmlEncode(txtHandelNm.Text);
		}

		if (bOk == false) {
			return;
		}

		if (pnlAge.Visible) {
			sAgeFrom = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[0]);
			sAgeTo = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[1]);
		}

		if (pnlOnline.Visible) {
			if (rdoStatus.Items[1].Selected) {
				iOnlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING;
			}
		}

		sUrl = string.Format(
						"ListGameCharacterFavorite.aspx?online={0}&handle={1}&treasure_pic={2}&treasure_movie={3}"
						+ "&agefrom={4}&ageto={5}&sort={6}",
						iOnlineStatus,
						sHandleNm,
						sTreasurePic,
						sTreasureMovie,
						sAgeFrom,
						sAgeTo,
						sSort
						);

		// 選択した値をセッションにいれておきます。
		Session[ONLINE_STATUS_FAVORITE] = iOnlineStatus;
		Session[HANDLE_NAME_FAVORITE] = txtHandelNm.Text;
		if (pnlAge.Visible) {
			Session[AGE_FAVORITE] = lstAge.Selection.Value;
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
	}

	private void GetQueryParm() {
		if (!String.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["treasure_pic"]))) {
			chkTreasurePic.SelectedIndex = 0;
			chkTreasurePic.Selection.Value = iBridUtil.GetStringValue(this.Request.QueryString["treasure_pic"]);
		}

		if (!String.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["treasure_movie"]))) {
			chkTreasureMovie.SelectedIndex = 0;
			chkTreasureMovie.Selection.Value = iBridUtil.GetStringValue(this.Request.QueryString["treasure_movie"]);
		}
	}
}
