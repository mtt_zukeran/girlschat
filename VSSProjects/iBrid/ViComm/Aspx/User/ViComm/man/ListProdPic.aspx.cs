/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���i�ʐ^�ꗗ
--	Progaram ID		: ListProdMov
--
--  Creation Date	: 2010.12.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdPic : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

		sessionMan.ControlList(
				this.Request,
				ViCommConst.INQUIRY_PRODUCT_PIC,
				this.ActiveForm);

	}
	
}
