/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ID/PW問い合わせ
--	Progaram ID		: LoginReminder
--
--  Creation Date	: 2010.10.26
--  Creater			: K.Itoh
--
**************************************************************************/
// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater   Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_LoginReminder : MobileManPageBase {


	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		this.lblErrorMessage.Text = string.Empty;
		
		if (!this.IsPostBack) {
			
		}else{
			if(!string.IsNullOrEmpty(this.Request.Params[ViCommConst.BUTTON_SUBMIT])){
				this.cmdSubmit_Click(this,EventArgs.Empty);
			}
		}
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (string.IsNullOrEmpty(this.txtMailAddr.Text)) {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAIL_ADDR);
			bOk = false;
		}
		if (bOk == false) return;


		string sEmailAddr = this.txtMailAddr.Text;


		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_REMINDER_MAIL");
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sessionObj.site.siteCd);
			db.ProcedureInParm("pEMAIL_ADDR", DbSession.DbType.VARCHAR2, sEmailAddr);
			db.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, ViCommConst.MAN);
			db.ProcedureOutParm("pRESULT", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			string sResult = db.GetStringValue("PRESULT");
			if(sResult.Equals("-1")){
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NOT_EXISTS_MAIL_ADDR);
				return;
			}
		}
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("mail",System.Web.HttpUtility.UrlEncode(sEmailAddr));
		RedirectToDisplayDoc(ViCommConst.SCR_MAN_REMINDER_OK,oParam);


	}	

}
