/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 返信メール送信
--	Progaram ID		: ReturnMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Text.RegularExpressions;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ReturnMail:MobileMailManPage {
	private static readonly Regex oReg = new Regex("<(\"[^\"]*\"|'[^']*'|[^'\">])*(>)",RegexOptions.IgnoreCase | RegexOptions.Compiled);

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (iBridUtil.GetStringValue(this.Request.QueryString["protect"]).Equals(ViCommConst.FLAG_ON_STR)) {
				this.ProtectRxMail();
			} else {
				pnlForm.Visible = true;
				pnlPointLack.Visible = false;
				
				if (IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL, 2)) {
					lblTitle.Visible = false;
					txtTitle.Visible = false;
				}
				txtTitle.Text = GetMailTitle();
				if (IsAvailableService(ViCommConst.RELEASE_RETURN_MAIL_USE_QUOTE)) {
					txtTitle.Text += sessionMan.GetMailValue("MAIL_TITLE");
					string sMailDoc = sessionMan.GetMailValue("MAIL_DOC1") + sessionMan.GetMailValue("MAIL_DOC2") + sessionMan.GetMailValue("MAIL_DOC3") + sessionMan.GetMailValue("MAIL_DOC4") + sessionMan.GetMailValue("MAIL_DOC5");

					sMailDoc = oReg.Replace(sMailDoc,string.Empty);

					txtDoc.Text += "\r\n\r\n>" + sMailDoc.Replace("\r\n","\r\n>");
				}
				string sMailDataSeq = iBridUtil.GetStringValue(Request.QueryString["data"]);
				if (sMailDataSeq.Equals(string.Empty)) {
					sMailDataSeq = iBridUtil.GetStringValue(sessionMan.userMan.NextMailDataSeq());

					sessionMan.userMan.mailData.Add(sMailDataSeq,new UserManMailInfo());

					string sDirect = iBridUtil.GetStringValue(Request.QueryString["direct"]);
					string sMailSeq = string.Empty;
					if (sDirect.Equals("1") == false) {
						if (sessionMan.GetMailCount() == 0) {
							if (sessionMan.logined) {
								RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
							} else {
								RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.nonUserTopId));
							}
						}
						string sUserSeq = sessionMan.GetMailValue("TX_USER_SEQ");
						string sUserCharNo = sessionMan.GetMailValue("TX_USER_CHAR_NO");
						string sHandleNM = sessionMan.GetMailValue("HANDLE_NM");
						sMailSeq = sessionMan.GetMailValue("MAIL_SEQ");
						
						sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq = sMailSeq;
						sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq = sUserSeq;
						sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo = sUserCharNo;
						sessionMan.userMan.mailData[sMailDataSeq].rxUserNm = sHandleNM;
					} else {
						sessionMan.seekMode = ViCommConst.INQUIRY_RX_USER_MAIL_BOX.ToString();
						// ﾒｰﾙからのﾀﾞｲﾚｸﾄ返信
						sMailSeq = iBridUtil.GetStringValue(Request.QueryString["mailseq"]);

						if (sessionMan.SetMailDataSetByMailSeq(sMailSeq) == false) {

							if (sessionMan.logined) {
								RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
							} else {
								RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.nonUserTopId));
							}
						}

						string sUserSeq = sessionMan.GetMailValue("TX_USER_SEQ");
						string sUserCharNo = sessionMan.GetMailValue("TX_USER_CHAR_NO");
						
						string sReadFlag = sessionMan.GetMailValue("READ_FLAG");
						if (sReadFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
							using (MailBox oMailBox = new MailBox()) {
								oMailBox.UpdateMailReadFlag(sMailSeq);
							}
						}
						sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq = sMailSeq;
						sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq = sUserSeq;
						sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo = sUserCharNo;
						sessionMan.userMan.mailData[sMailDataSeq].rxUserNm = sessionMan.GetMailValue("HANDLE_NM");
						
						if (iBridUtil.GetStringValue(this.Request.QueryString["scrid"]).Equals("01")) {
							sessionMan.userMan.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_ON;
						} else {
							sessionMan.userMan.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_OFF;
						}
						
						using (Cast oCast = new Cast()) {
							if (oCast.IsQuickResponseEntry(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sUserSeq,sUserCharNo)) {
								sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag = ViCommConst.FLAG_ON;
							} else {
								sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag = ViCommConst.FLAG_OFF;
							}
						}
					}
				} else {
					txtTitle.Text = sessionMan.userMan.mailData[sMailDataSeq].mailTitle;
					txtDoc.Text = sessionMan.userMan.mailData[sMailDataSeq].mailDoc;

					sessionMan.SetMailDataSetByMailSeq(sessionMan.userMan.mailData[sMailDataSeq].returnMailSeq);
				}

				//相手が通常会員以外だったら、リダイレクトさせる。 
				CheckNormalUser(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo);
				//相手の拒否リストに載っている場合、リダイレクトさせる。 
				CheckRefuse(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo);

				using (Refuse oRefuse = new Refuse()) {
					if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo)) {
						RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
					}
				}
				
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
				if (sessionMan.userMan.handleNm.Equals("")) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ModifyUserHandleNm.aspx"));
					return;
				}

				int iChargePoint;
				if (!CheckMailBalance(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo,out iChargePoint)) {
					if (IsAvailableService(ViCommConst.RELEASE_DISPLAY_RETURN_MAIL_POINT_LACK,2)) {
						pnlForm.Visible = false;
						pnlPointLack.Visible = true;
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_POINT_LACK_MAIL));
					}
				}

				ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;
			}
		} else {
			if (iBridUtil.GetStringValue(Request.QueryString["direct"]).Equals("1")) {
				sessionMan.SetMailDataSetByMailSeq(iBridUtil.GetStringValue(Request.QueryString["mailseq"]));
			}
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_CONFIRM] != null) {
				cmdConfirm_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_REGIST] != null) {
				cmdRegist_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_PRESENT] != null) {
				cmdPresent_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (iBridUtil.GetStringValue(Request.Params["chkpresent"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_ON;
			PresentSelect(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true);
		} else {
			sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_OFF;
			Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true,ViCommConst.SCR_TX_REMAIL_COMPLITE);
		}
	}

	protected void cmdPresent_Click(object sender,EventArgs e) {
		sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_ON;
		PresentSelect(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true);
	}

	private void CheckNormalUser(string pUserSeq,string pUserCharNo) {
		using (Cast oCast = new Cast()) {
			using (DataSet ds = oCast.GetOne(sessionMan.site.siteCd,pUserSeq,pUserCharNo,1)) {
				if (ds.Tables[0].Rows.Count != 0) {
					if (!ds.Tables[0].Rows[0]["USER_STATUS"].Equals(ViCommConst.USER_WOMAN_NORMAL) ||
						!iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["NA_FLAG"]).Equals(ViCommConst.NaFlag.OK.ToString())) {
						RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
					}
				}
			}
		}
	}

	virtual protected string GetMailTitle() {
		return "Re:";
	}

	protected void cmdConfirm_Click(object sender,EventArgs e) {
		sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = iBridUtil.GetStringValue(Request.Params["chkpresent"]).Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		Confirm(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true);
	}

	protected void cmdRegist_Click(object sender,EventArgs e) {
		MainteDraftMail(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),true);
	}

	private void cmdDelete_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDelTxRx(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,mailSeqList.ToArray());
			}
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("mail_delete_count",mailSeqList.Count.ToString());
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_DELETED_MAIL_HISTORY,oParam);
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   "ReturnMail.aspx?" + Request.QueryString));
		}
	}

	private void ProtectRxMail() {
		int iProtectFlag;
		int.TryParse(iBridUtil.GetStringValue(this.Request.QueryString["protectflag"]),out iProtectFlag);
		iProtectFlag = iProtectFlag == 1 ? 0 : 1;
		
		string sMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["mailseq"]);
		string sListNo = iBridUtil.GetStringValue(this.Request.QueryString["listno"]);
		string sProtectMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["protectmailseq"]);
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sProtectMailSeq,iProtectFlag);
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   string.Format("ReturnMail.aspx?scrid=01&stay=1&mailseq={0}&listno={1}#{2}",sMailSeq,sListNo,sProtectMailSeq)));
	}
}
