/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者つぶやき個人別一覧
--	Progaram ID		: ListPersonalCastDiary
--  Creation Date	: 2015.05.04
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListPersonalCastDiary:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["userseq"]))) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_DIARY_EX,this.ActiveForm)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
