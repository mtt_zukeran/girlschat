/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �f���ʐ^����ڍ�
--	Progaram ID		: ViewPersonalBbsMovie
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_ViewPersonalBbsMovie:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			} else {
				bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);

				int iChargePoint = 0;
				int iPaymentAmt = 0;
				string sLoginId;
				string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
				string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
				string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
				bool bDownload = false;
				bool bDupUsed = false;

				if (!sMovieSeq.Equals("")) {
					sessionMan.errorMessage = "";
					if (!MovieHelper.IsRangeRequest()) {
						using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
							bDupUsed = oObjUsedHistory.GetOne(
												sessionMan.userMan.siteCd,
												sessionMan.userMan.userSeq,
												ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
												sMovieSeq);
						}

						if (bDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
							if (sessionMan.CheckBbsMovieBalance(sCastSeq,sCastCharNo,sMovieSeq,out iChargePoint,out iPaymentAmt) == false) {
								RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_BBS_MOVIE));
							}
							if (!bIsCheckOnly)
								bDownload = Download(sCastSeq,sCastCharNo,sMovieSeq,iChargePoint,iPaymentAmt,bDupUsed);
						} else {
							if (!bIsCheckOnly)
								bDownload = Download(sCastSeq,sCastCharNo,sMovieSeq,iChargePoint,iPaymentAmt,bDupUsed);
						}
					} else {
						if (!bIsCheckOnly)
							bDownload = Download(sCastSeq,sCastCharNo,sMovieSeq,iChargePoint,iPaymentAmt,bDupUsed);
					}
				};

				if (bDownload == false) {
					ulong uSeekMode;
					bool bControlList;
					Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
					if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
						bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
					} else {
						bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_MOVIE,ActiveForm);
					}

					if (bControlList) {
						int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
						sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
						sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo);
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListBbsMovie.aspx"));
					}
				}
			}
		}
	}

	private bool Download(string pCastSeq,string pCastCharNo,string pMovieSeq,int pChargePoint,int pPaymentAmt,bool pDupUsed) {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				if (sessionMan.logined) {
					if (!MovieHelper.IsRangeRequest()) {
						if (pDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
							using (Access oAccess = new Access()) {
								oAccess.AccessMoviePage(sessionMan.site.siteCd,pCastSeq,pCastCharNo,pMovieSeq);
							}
							using (WebUsedLog oLog = new WebUsedLog()) {
								oLog.CreateWebUsedReport(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.CHARGE_BBS_MOVIE,
										pChargePoint,
										pCastSeq,
										pCastCharNo,
										"",
										pPaymentAmt);
							}
							using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
								oObjUsedHistory.AccessBbsObj(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
										pMovieSeq);
							}
						}
					}
				}
				MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.OPERATOR);

				return true;
			} else {
				return false;
			}
		}
	}
}
