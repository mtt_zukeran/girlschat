/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールdeガチャ(一次)Flash表示
--	Progaram ID		: ViewFlashMailLottery
--
--  Creation Date	: 2015.03.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_man_ViewFlashMailLottery:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else if (sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else {
			string sNextUrl = iBridUtil.GetStringValue(this.Request.QueryString["next_url"]);
			string sLose = iBridUtil.GetStringValue(this.Request.QueryString["lose"]);

			string sFlashFileNm = string.Empty;
			sFlashFileNm = "mail_lottery.swf";

			if (string.IsNullOrEmpty(sNextUrl)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}

			string sFlashDir = Path.Combine(sessionMan.site.webPhisicalDir,PwViCommConst.MAIL_LOTTERY_DIR);
			string sSwfFilePath = Path.Combine(sFlashDir,sFlashFileNm);
			byte[] bufTempSwf;

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!File.Exists(sSwfFilePath)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}

				bufTempSwf = FileHelper.GetFileBuffer(sSwfFilePath);
			}

			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("next_url",sNextUrl);
			oParam.Add("lose",sLose);
			byte[] oSwfBuffer = GameFlashLiteHelper.RewriteSwfBuffer(bufTempSwf,oParam);
			GameFlashLiteHelper.OutputSwf(oSwfBuffer);
		}
	}
}
