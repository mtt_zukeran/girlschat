/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品動画詳細
--	Progaram ID		: ViewProdMov
--
--  Creation Date	: 2010.12.14
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewProdMov : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		
		ulong lSeekMode = ulong.Parse(this.Request.QueryString["seekmode"]);
		if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			lSeekMode = ViCommConst.INQUIRY_PRODUCT_MOVIE;
		}
			
		if (!IsPostBack) {
			if(!sessionMan.ControlList(this.Request,lSeekMode,this.ActiveForm)){
				throw new ApplicationException();
			}
		}

	}
}
