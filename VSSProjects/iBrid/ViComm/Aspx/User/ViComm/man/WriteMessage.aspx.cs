/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾒｯｾｰｼﾞ書き込み
--	Progaram ID		: WriteMessage
--
--  Creation Date	: 2010.06.22
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;

using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_WriteMessage:MobileManPageBase {


	private string loginid = null;
	private int recNo;

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		loginid = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
		recNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

		ParseHTML oParseMan = sessionMan.parseContainer;


		if (!this.IsPostBack) {
			if (!sessionMan.logined) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				}
				else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			DataRow dr;
			if (!sessionMan.SetCastDataSetByLoginId(loginid,sCastCharNo,recNo,out dr)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 入力チェック
		if (!this.ValidateForSubmit())
			return;

		string sMessageDoc = this.txtMessageDoc.Text.Trim();
		sMessageDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,sMessageDoc));
		sMessageDoc = MobileLib.Mobile.GetInputValue(sMessageDoc,4000,System.Text.Encoding.UTF8);

		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

		DataRow dr;
		if (sessionMan.SetCastDataSetByLoginId(this.loginid,sCastCharNo,this.recNo,out dr)) {
			// 登録処理
			using (Message oMessage = new Message()) {
				oMessage.WriteMessage(
					sessionMan.site.siteCd,
					ViCommConst.MAN,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					iBridUtil.GetStringValue(dr["USER_SEQ"]),
					iBridUtil.GetStringValue(dr["USER_CHAR_NO"]),
					sMessageDoc);
			}
		}

		// 完了画面へリダイレクト
		RedirectToMobilePage(
			sessionMan.GetNavigateUrl(
				sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WRITE_MESSAGE_COMPLITE + "&" + Request.QueryString));
	}

	/// <summary>
	/// ﾒｯｾｰｼﾞ登録前の入力検証を行う。
	/// </summary>
	/// <returns>すべての検証に成功した場合はtrue。それ以外はfalse。</returns>
	private bool ValidateForSubmit() {
		bool bIsValid = true;
		string sNGWord;
		lblErrorMessage.Text = string.Empty;

		string sMessageDoc = this.txtMessageDoc.Text.Trim();
		if (string.IsNullOrEmpty(sMessageDoc)) {
			bIsValid = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}

		if (sMessageDoc.Length > 300) {
			bIsValid = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MESSAGE_OVER_LEN);
		}

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}

		if (sessionMan.ngWord.VaidateDoc(sMessageDoc,out sNGWord) == false) {
			bIsValid = false;
			lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
		}

		return bIsValid;
	}
}
