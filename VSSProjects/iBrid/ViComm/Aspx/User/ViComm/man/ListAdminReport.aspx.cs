﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 管理者連絡一覧
--	Progaram ID		: ListAdminReport
--
--  Creation Date	: 2010.05.06
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using ViComm;

public partial class ViComm_man_ListAdminReport:MobileManPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_ADMIN_REPORT,ActiveForm);
		}
	}
}
