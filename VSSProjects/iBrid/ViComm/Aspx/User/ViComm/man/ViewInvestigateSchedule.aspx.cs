/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せスケジュール詳細
--	Progaram ID		: ViewInvestigateSchedule
--
--  Creation Date	: 2015.03.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewInvestigateSchedule:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				string sResult = PwViCommConst.WantedEntrantResult.RESULT_OK;
				string sErrorMask = "0";
				using (InvestigateEntrant oInvestigateEntrant = new InvestigateEntrant()) {
					InvestigateEntrantSeekCondition oCondition = new InvestigateEntrantSeekCondition();
					oCondition.SiteCd = sessionMan.site.siteCd;
					oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
					oCondition.UserSeq = sessionMan.userMan.userSeq;
					oCondition.UserCharNo = sessionMan.userMan.userCharNo;
					oInvestigateEntrant.InvestigateEntrantEnter(oCondition,out sErrorMask,out sResult);

					if (sResult.Equals(PwViCommConst.WantedEntrantResult.RESULT_NG)) {
						RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
					} else if (sResult.Equals(PwViCommConst.WantedEntrantResult.RESULT_OK)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"ViewInvestigateSchedule.aspx"));
					}
					 else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,string.Format("ViewInvestigateEntryError.aspx?err={0}&subtype=1",sErrorMask)));
					}
				}
			}

			if (this.IsInvestigateEnter()) {
				tagEntryForm.Visible = false;
				tagEntered.Visible = true;
			} else {
				tagEntryForm.Visible = true;
				tagEntered.Visible = false;
			}
			
			sessionMan.ControlList(
					Request,
					PwViCommConst.INQUIRY_INVESTIGATE_SCHEDULE,
					ActiveForm);
		}
	}

	private bool IsInvestigateEnter() {
		bool bValue = false;
		using (InvestigateEntrant oInvestigateEntrant = new InvestigateEntrant()) {
			InvestigateEntrantSeekCondition oCondition = new InvestigateEntrantSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.UserSeq = sessionMan.userMan.userSeq;
			oCondition.UserCharNo = sessionMan.userMan.userCharNo;
			oCondition.ExecutionDay = DateTime.Now.ToString("yyyy/MM/dd");
			DataSet ds = oInvestigateEntrant.GetOne(oCondition);
			bValue = (ds.Tables[0].Rows.Count > 0);
		}

		return bValue;
	}
}
