/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ΒlΚ¨σK`TOP
--	Progaram ID		: GetPersonalCastPicGacha
--
--  Creation Date	: 2013.01.13
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web.UI.MobileControls;

public partial class ViComm_man_GetPersonalCastPicGacha:MobileObjManPage {
	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		string sUnusedFlag = iBridUtil.GetStringValue(this.Request.QueryString["unused"]);

		if (string.IsNullOrEmpty(sUnusedFlag)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.currentAspx + sessionMan.requestQuery.Url.Query + "&unused=1"));
		}

		if (!IsPostBack) {
			sessionMan.errorMessage = string.Empty;
			string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["user_seq"]);
			string sUserCharNo = iBridUtil.GetStringValue(Request.QueryString["user_char_no"]);
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			sessionMan.SetCastDataSetByUserSeq(sUserSeq,sUserCharNo,iRecNo);

			if (sessionMan.site.bbsPicAttrFlag != ViCommConst.FLAG_ON) {
				pnlSearchList.Visible = false;
			} else {
				CreateObjAttrComboBox(sUserSeq,sUserCharNo,string.Empty,ViCommConst.BbsObjType.PIC,ViCommConst.ATTACHED_BBS.ToString(),true,false,false,false);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		string sLoginId = iBridUtil.GetStringValue(this.Request.Form["loginid"]);
		string sCastSeq;
		string sCastCharNo = iBridUtil.GetStringValue(Request.Form["user_char_no"]);
		int iChargePoint;
		int iPaymentAmt;
		bool bOk = false;
		bool bDupUsed = false;
		string sPicSeq;

		DataSet oDataSet = null;
		SeekCondition oCondition = new SeekCondition();
		string sSiteCd = this.sessionMan.site.siteCd;
		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;
		string[] selectedValueList = selectedValue.Split(',');
		string sAttrTypeSeq = selectedValueList[0];
		string sAttrSeq = selectedValueList[1];
		oCondition.unusedFlag = ViCommConst.FLAG_ON_STR;
		oCondition.sortType = ViCommConst.SortType.RANDOM;

		using (CastPic oCastPic = new CastPic()) {
			oDataSet = oCastPic.GetPageCollectionBase("",sSiteCd,sLoginId,sCastCharNo,ViCommConst.FLAG_OFF_STR,false,ViCommConst.ATTACHED_BBS.ToString(),"",sAttrTypeSeq,sAttrSeq,oCondition,false,false,"",1,1);
		}

		if (oDataSet.Tables[0].Rows.Count == 0) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_CAST_PIC_NOT_FOUND_UNUSED);
			return;
		}

		sCastSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
		sCastCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		sPicSeq = oDataSet.Tables[0].Rows[0]["PIC_SEQ"].ToString();

		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			bDupUsed = oObjUsedHistory.GetOne(
								sessionMan.userMan.siteCd,
								sessionMan.userMan.userSeq,
								ViCommConst.ATTACH_PIC_INDEX.ToString(),
								sPicSeq);
		}

		if (bDupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
			bOk = sessionMan.CheckBbsPicBalance(sCastSeq,sCastCharNo,sPicSeq,out iChargePoint,out iPaymentAmt);
			if (bOk == false) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_POINT_LACK_MAIL));
			} else {
				using (WebUsedLog oLog = new WebUsedLog()) {
					oLog.CreateWebUsedReport(
							sessionMan.userMan.siteCd,
							sessionMan.userMan.userSeq,
							ViCommConst.CHARGE_BBS_PIC,
							iChargePoint,
							sCastSeq,
							sCastCharNo,
							"",
							iPaymentAmt);
				}
				using (Access oAccess = new Access()) {
					oAccess.AccessPicPage(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sPicSeq);
				}

				using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
					oObjUsedHistory.AccessBbsObj(
							sessionMan.userMan.siteCd,
							sessionMan.userMan.userSeq,
							ViCommConst.ATTACH_PIC_INDEX.ToString(),
							sPicSeq);
				}

				string sWinFlag = string.Empty;
				using (CastPicGacha oCastPicGacha = new CastPicGacha()) {
					oCastPicGacha.GetCastPicGacha(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,out sWinFlag);
				}

				string sNextUrl = string.Format("ViewPersonalBbsPic.aspx?userrecno={0}&loginid={1}&category={2}&{3}={4}&objseq={5}&win_flag={6}",
					oDataSet.Tables[0].Rows[0]["RNUM"].ToString(),
					oDataSet.Tables[0].Rows[0]["LOGIN_ID"].ToString(),
					sessionMan.currentCategoryIndex,
					sessionMan.site.charNoItemNm,
					oDataSet.Tables[0].Rows[0]["CRYPT_VALUE"].ToString(),
					sPicSeq,
					sWinFlag
				);

				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ViewFlashCastPicGacha.aspx?next_url={0}&pic_seq={1}&win_flag={2}",HttpUtility.UrlEncode(sNextUrl),sPicSeq,sWinFlag)));
			}
		}
	}
}