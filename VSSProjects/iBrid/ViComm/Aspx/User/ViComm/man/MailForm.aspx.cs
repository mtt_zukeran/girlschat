/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールフォーム
--	Progaram ID		: MailForm
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Text;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class ViComm_man_MailForm:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.errorMessage = string.Empty;
			tagMailWrite.Visible = true;
			tagMailConfirm.Visible = false;
			tagMailComplete.Visible = false;
			ViewState["SUBJECT"] = string.Empty;
			ViewState["BODY"] = string.Empty;
			ViewState["FROM_ADDR"] = string.Empty;
			ViewState["TO_ADDR"] = string.Empty;
			string sDirectSend = iBridUtil.GetStringValue(Request.QueryString["quickResViolation"]);
			if (sDirectSend.Equals(ViCommConst.FLAG_ON_STR)) {
				QuickResViolation();
			}
			string sManTweetCommentReport = iBridUtil.GetStringValue(Request.QueryString["manTweetCommentReport"]);
			if (sManTweetCommentReport.Equals(ViCommConst.FLAG_ON_STR)) {
				SendManTweetCommentReport();
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sFromAddr = iBridUtil.GetStringValue(Request.Form["from"]);
		if (sFromAddr.Equals(string.Empty)) {
			sessionMan.errorMessage = "ﾒｰﾙｱﾄﾞﾚｽを入力してください<BR />";
			return;
		} else {
			Regex rgx = new Regex(@"[0-9A-Za-z\._-]+@[a-zA-Z0-9_-]+\.[a-zA-Z0-9\._-]+");
			Match rgxMatch = rgx.Match(sFromAddr);
			if (!rgxMatch.Success) {
				sessionMan.errorMessage = "ﾒｰﾙｱﾄﾞﾚｽを正しい形式で入力してください<BR />";
				return;
			}
		}

		string sParamsInput = string.Empty;
		string sParamsName = string.Empty;
		StringBuilder sb = new StringBuilder();
		foreach (string sParams in Request.Form.AllKeys) {
			if (sParams.StartsWith("*")) {
				sParamsInput = iBridUtil.GetStringValue(Request.Form[sParams.Replace("*",string.Empty)]);
				sParamsName = iBridUtil.GetStringValue(Request.Form[sParams]);

				if (!sParamsInput.Equals("")) {
					sb.Append(string.Format("{0}:{1}",sParamsName,sParamsInput)).AppendLine();
				} else {
					sessionMan.errorMessage = string.Format("{0}を入力してください<BR />",sParamsName);
					return;
				}
			}
		}

		AppendDefaultBody(ref sb);
		ViewState["SUBJECT"] = iBridUtil.GetStringValue(Request.Form["subject"]);
		ViewState["BODY"] = sb.ToString();
		ViewState["FROM_ADDR"] = sFromAddr;
		//送信先は自己責任
		ViewState["TO_ADDR"] = iBridUtil.GetStringValue(Request.Form["to"]);
		tagMailWrite.Visible = false;
		tagMailConfirm.Visible = true;
		tagMailComplete.Visible = false;
	}

	protected void cmdTx_Click(object sender,EventArgs e) {
		string sToMailAddress = iBridUtil.GetStringValue(ViewState["TO_ADDR"]);
		string sFromMailAddress = iBridUtil.GetStringValue(ViewState["FROM_ADDR"]);
		string sTitle = iBridUtil.GetStringValue(ViewState["SUBJECT"]);
		string sDoc = iBridUtil.GetStringValue(ViewState["BODY"]);
		//メールアドレスの形式をチェック
		try {
			new Agiletech.Net.Mail.MailAddress(sFromMailAddress);
		} catch (FormatException) {
			sFromMailAddress = sToMailAddress;
		}

		SendMail(sFromMailAddress,sToMailAddress,sTitle,sDoc);
		tagMailWrite.Visible = false;
		tagMailConfirm.Visible = false;
		tagMailComplete.Visible = true;
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		tagMailWrite.Visible = true;
		tagMailConfirm.Visible = false;
		tagMailComplete.Visible = false;
	}

	private void AppendDefaultBody(ref StringBuilder pBody) {
		pBody.Append("\r\n------------------------------").AppendLine();
		pBody.Append("送信者情報").AppendLine();
		if (sessionMan.logined) {
			pBody.AppendFormat("ﾛｸﾞｲﾝID:{0}",sessionMan.userMan.loginId).AppendLine();
			pBody.AppendFormat("登録ﾒｰﾙｱﾄﾞﾚｽ:{0}",sessionMan.userMan.emailAddr).AppendLine();
		} else {
			pBody.Append("未ﾛｸﾞｲﾝ").AppendLine();
		}
		pBody.AppendFormat("USER-AGENT:{0}",sessionMan.mobileUserAgent).AppendLine();
		if (!sessionMan.currentIModeId.Equals(string.Empty)) {
			pBody.AppendFormat("GUID:{0}",sessionMan.currentIModeId).AppendLine();
		}
	}

	private void QuickResViolation() {
		string sCastLoginId = iBridUtil.GetStringValue(Request.QueryString["castid"]);
		string sCastHandleNm = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["casthandlenm"]));
		string sSendDate = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["senddate"]));
		string sToMailAddress = sessionMan.site.supportEmailAddr;
		string sFromMailAddress = sessionMan.userMan.emailAddr;
		
		//メールアドレスの形式をチェック
		try {
			new System.Net.Mail.MailAddress(sFromMailAddress);
		} catch (FormatException) {
			sFromMailAddress = sToMailAddress;
		}
		string sSubject = "10分以内返信違反";
		StringBuilder sBody = new StringBuilder();
		sBody.AppendFormat("男性会員:[{0}] {1}",sessionMan.userMan.loginId,sessionMan.userMan.handleNm).AppendLine();
		sBody.AppendFormat("送信日時:{0}",sSendDate).AppendLine();
		sBody.AppendFormat("出演者:[{0}] {1}",sCastLoginId,sCastHandleNm).AppendLine();

		SendMail(sFromMailAddress,sToMailAddress,sSubject,sBody.ToString());
		tagMailWrite.Visible = false;
		tagMailConfirm.Visible = false;
		tagMailComplete.Visible = true;		
	}

	private void SendManTweetCommentReport() {
		string sCastLoginId = iBridUtil.GetStringValue(Request.QueryString["castloginid"]);
		string sCastHandleNm = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["casthandlenm"]));
		string sManTweetSeq = iBridUtil.GetStringValue(Request.QueryString["mantweetseq"]);
		string sToMailAddress = sessionMan.site.supportEmailAddr;
		string sFromMailAddress = sessionMan.userMan.emailAddr;

		try {
			new System.Net.Mail.MailAddress(sFromMailAddress);
		} catch (FormatException) {
			sFromMailAddress = sToMailAddress;
		}

		string sSubject = "会員つぶやきコメント通報";
		StringBuilder sBody = new StringBuilder();
		sBody.AppendFormat("男性会員:[{0}] {1}",sessionMan.userMan.loginId,sessionMan.userMan.handleNm).AppendLine();
		sBody.AppendFormat("つぶやきSEQ:{0}",sManTweetSeq).AppendLine();
		sBody.AppendFormat("出演者:[{0}] {1}",sCastLoginId,sCastHandleNm).AppendLine();

		SendMail(sFromMailAddress,sToMailAddress,sSubject,sBody.ToString());

		tagMailWrite.Visible = false;
		tagMailConfirm.Visible = false;
		tagMailComplete.Visible = true;
	}
}
