<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PostEdyZero.aspx.cs" Inherits="ViComm_man_PostEdyZero" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" Action="https://secure.credix-web.co.jp/cgi-bin/edy.cgi" Method="post">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
		<cc1:iBMobileLiteralText ID="clientip" runat="server" />
		<cc1:iBMobileLiteralText ID="act" runat="server" />
		<cc1:iBMobileLiteralText ID="money" runat="server" />
		<cc1:iBMobileLiteralText ID="telno" runat="server" />
		<cc1:iBMobileLiteralText ID="email" runat="server" />
		<cc1:iBMobileLiteralText ID="sendid" runat="server" />
		<cc1:iBMobileLiteralText ID="sendpoint" runat="server" />
		<cc1:iBMobileLiteralText ID="success_url" runat="server" />
		<cc1:iBMobileLiteralText ID="success_str" runat="server" />
		<cc1:iBMobileLiteralText ID="failure_url" runat="server" />
		<cc1:iBMobileLiteralText ID="failure_str" runat="server" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
