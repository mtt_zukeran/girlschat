/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑﾛｸﾞｲﾝ
--	Progaram ID		: GameLoginUser
--
--  Creation Date	: 2011.07.22
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GameLoginUser:MobileSocialGameManBase {
	private string userSeq;
	private string userStatus;
	private string loginResult;
	private string utn;
	private string id;

	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			ParseHTML oParseMan = sessionMan.parseContainer;
			utn = this.GetStringValue(Request.QueryString["utn"]);
			id = this.GetStringValue(Request.QueryString["id"]);

			if (utn.Equals("") && id.Equals("")) {
				txtLoginId.Text = this.GetStringValue(Request.QueryString["uid"]);
				txtPassword.Text = this.GetStringValue(Request.QueryString["pw"]);
				if ((!txtLoginId.Text.Equals("")) && (!txtPassword.Text.Equals("")) ) {
					cmdSubmit_Click(sender,e);
					return;
				}

				txtLoginId.Text = this.GetStringValue(Request.QueryString["loginid"]);
				txtPassword.Text = this.GetStringValue(Request.QueryString["password"]);
				txtPassword.Text = txtPassword.Text.Replace("?spare1=","");	// GIGA-POINTの戻りにゴミがつく
				if ((!txtLoginId.Text.Equals("")) && (!txtPassword.Text.Equals("")) ) {
					cmdSubmit_Click(sender,e);
				}
			} else {
				if (this.GetStringValue(Request.QueryString["useutn"]).Equals("1")) {
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_UTN,utn,"","",Session.SessionID,out userSeq,out userStatus,out loginResult);
					LoginResult(true);
				} else {
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_IMODE_ID,id,"","",Session.SessionID,out userSeq,out userStatus,out loginResult);
					LoginResult(true);
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
			// sessionMan.currentIModeIdは旧Ver対応(LOGIN_USER_MANでIMODE_IDがNULLの場合更新される)
			sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_TEL,sessionMan.currentIModeId,txtLoginId.Text,txtPassword.Text,Session.SessionID,out userSeq,out userStatus,out loginResult);
		} else {
			// sessionMan.currentIModeIdは旧Ver対応(LOGIN_USER_MANでIMODE_IDがNULLの場合更新される)
			sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_LOGINID,sessionMan.currentIModeId,txtLoginId.Text,txtPassword.Text,Session.SessionID,out userSeq,out userStatus,out loginResult);
		}
		LoginResult(false);
	}

	private void LoginResult(bool pUnt) {
		if (loginResult.Equals("0")) {
			
			sessionMan.alreadyWrittenLoginLog = false;
			string sGoto = this.GetStringValue(Request.QueryString["goto"]);

			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || (sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionMan.adminFlg)) {
				HttpCookie cookie = new HttpCookie("maqia");
				cookie.Values["maqiauid"] = sessionMan.userMan.userSeq;
				cookie.Values["maqiapw"] = sessionMan.userMan.loginPassword;
				cookie.Values["maqiasex"] = ViCommConst.MAN;
				cookie.Expires = DateTime.Now.AddDays(90);
				cookie.Domain = sessionMan.site.hostNm;
				Response.Cookies.Add(cookie);
			}

			// Loginの確定により女性偽装分を破棄
			sessionMan.originalWomanObj = null;

			bool bExist = this.sessionMan.userMan.gameCharacter.GetCurrentInfo(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq);
			string sRedirectUrl = string.Empty;
			
			if (bExist) {

				if (sGoto.Equals(string.Format("GameDisplayDoc.aspx?doc={0}",PwViCommConst.SCR_MAN_LIVE_CHAT_NO_APPLY_LIVE_CHAT_ACCESS))) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sGoto));
				} else if (sGoto.Equals("GameDisplayDoc.aspx")) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"GameDisplayDoc.aspx?doc=" + this.GetStringValue(Request.QueryString["doc"])));
				} else if (sGoto.Equals("DisplayDoc.aspx")) {
					sRedirectUrl = sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + this.GetStringValue(Request.QueryString["doc"]));
					if (!string.IsNullOrEmpty(this.GetStringValue(Request.QueryString["pay_game_flag"]))) {
						sRedirectUrl = sRedirectUrl + string.Format("&pay_game_flag={0}",this.GetStringValue(Request.QueryString["pay_game_flag"]));
					}
					RedirectToMobilePage(sRedirectUrl);
				} else if(!string.IsNullOrEmpty(sGoto)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sGoto));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("GameUserTop.aspx"));
				}
			} else {
				if (sessionMan.userMan.registIncompleteFlag == 2 || string.IsNullOrEmpty(sessionMan.userMan.handleNm) || string.IsNullOrEmpty(sessionMan.userMan.birthday)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserBasicInfo.aspx"));
				} else if (sessionMan.userMan.registIncompleteFlag != 0) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserProfile.aspx"));
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_NON_USER_TOP_FOR_CHAT_USER);
				}
			}
			
		} else {
			lblErrorMessage.Text = string.Empty;

			//ブラックユーザーは別画面に遷移させる 
			if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_BLACK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_BLACK));
			} else if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_STOP)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_STOP));
			} else if (loginResult.Equals(ViCommConst.ERR_FRAME_USER_RESIGNED)) {
				if (!userStatus.Equals(ViCommConst.USER_MAN_HOLD)) {
					//退会会員でﾛｸﾞｲﾝした場合、自動で復活させる
					string sResult = string.Empty;
					if (SysPrograms.Expression(@"0(7|8|9)0?\d{4}?\d{4}",txtLoginId.Text)) {
						sessionMan.userMan.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_TEL,txtLoginId.Text,txtPassword.Text,out sResult);
					} else {
						sessionMan.userMan.ResignedReturn(ViCommConst.RESIGNED_RETURN_BY_LOGINID,txtLoginId.Text,txtPassword.Text,out sResult);
					}
					if (sResult.Equals("0")) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("GameLoginUser.aspx?loginid={0}&password={1}",txtLoginId.Text,txtPassword.Text)));
					}
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_USER_RESIGNED));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("GameLoginUser.aspx?loginid={0}&password={1}",sessionMan.userMan.loginId,sessionMan.userMan.loginPassword)));
				}
			} else if (loginResult.Equals(ViCommConst.ERR_FRAME_DUPLICATE_GUID)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_DUPLICATE_GUID));
			}
			
			txtLoginId.Text = HttpUtility.HtmlEncode(txtLoginId.Text);
			txtPassword.Text = HttpUtility.HtmlEncode(txtPassword.Text);

			if (pUnt == false) {
				lblErrorMessage.Text += GetErrorMessage(loginResult);
			}
		}
	}
	
	private string GetGameTutorialTownSeq() {
		string sValue = string.Empty;

		TownSeekCondition oCondition = new TownSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.SexCd = this.sessionMan.sexCd;

		using (Town oTown = new Town()) {
			sValue = oTown.GetGameTutorialTownSeq(oCondition);
		}

		return sValue;
	}

	//マルチスレッド問題回避
	private string GetStringValue(object value) {
		if (value == null) {
			return "";
		} else {
			return value.ToString();
		}
	}
}
