/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ¸Ú¼Þ¯Äx¥
--	Progaram ID		: PaymentCredit
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCredit:MobileManPageBase {

	private string sBillAmt;

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sBillAmt = iBridUtil.GetStringValue(Request.QueryString["bill"]);

		if (!IsPostBack) {
			if (sBillAmt.Equals("")) {
				DataSet ds;
				ds = GetPackDataSet(ViCommConst.SETTLE_CREDIT_PACK,500);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstCreditPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				}
				lblCreditPack.Visible = true;
				tagNormalHeader01.Visible = true;
				tagNormalHeader02.Visible = true;
				tagNormalBody01.Visible = true;
				tagNormalFooter01.Visible = true;
				tagChargeHeader01.Visible = false;
				tagChargeFooter01.Visible = false;
			} else {
				lblCreditPack.Visible = false;
				tagNormalHeader01.Visible = false;
				tagNormalHeader02.Visible = false;
				tagNormalBody01.Visible = false;
				tagNormalFooter01.Visible = false;
				tagChargeHeader01.Visible = true;
				tagChargeFooter01.Visible = true;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = "";
		if (sBillAmt.Equals("")) {
			sSalesAmt = lstCreditPack.Items[lstCreditPack.SelectedIndex].Value;
		} else if (sessionMan.userMan.ggFlag == 1) {
			//GG_FLAGªONÌlÍ]ÊÏÌ\¿Í³¹È¢
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_CREDIT_CHARGE_GG_FLAG));
		} else {
			sSalesAmt = sBillAmt;
		}
		string sSid = "";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (sBillAmt.Equals("")) {
				if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,sSalesAmt,sessionMan.userMan.userSeq)) {
					oLog.LogSettleRequest(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.SETTLE_CORP_BIGSUN,
						ViCommConst.SETTLE_CREDIT_PACK,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						int.Parse(sSalesAmt),
						oPack.exPoint,
						"",
						"",
						out sSid);
				}
			} else {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_BIGSUN,
					ViCommConst.SETTLE_CREDIT,
					ViCommConst.SETTLE_STAT_CHARGE,
					int.Parse(sSalesAmt),
					(int)Math.Floor(double.Parse(sSalesAmt) / sessionMan.site.pointPrice),
				"",
						"",
					out sSid);
			}
		}
		string sSettleUrl = "";
		string sBackUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_BIGSUN,ViCommConst.SETTLE_CREDIT_PACK)) {
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				if (sBillAmt.Equals("")) {
					sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sBackUrl);
				} else {
					sSettleUrl = string.Format(oSiteSettle.settleUrl,"48380",sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sBackUrl);
				}
			}
		}
		Response.Redirect(sSettleUrl);
	}
}
