/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女性検索結果
--	Progaram ID		: ListFindGameCharacter.aspx
--
--  Creation Date	: 2011.08.10
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListFindGameCharacter:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if(!this.IsPostBack){
			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_FIND_LIST,ActiveForm);
		}
	}
}
