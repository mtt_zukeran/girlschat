/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: クレジット自動決済解除
--	Progaram ID		: CancelCreditAutoSettle
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_CancelCreditAutoSettle:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		string sCreditAutoSettleSeq = iBridUtil.GetStringValue(Request.QueryString["settleseq"]);

		if (sCreditAutoSettleSeq.Equals(string.Empty)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			tagTop.Visible = true;
			tagComplete.Visible = false;
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CREDIT_AUTO_SETTLE_STATUS,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdCancel"] != null) {
				cmdCancel_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult;
		string sCreditAutoSettleSeq = iBridUtil.GetStringValue(this.Request.QueryString["settleseq"]);

		using (CreditAutoSettleStatus oCreditAutoSettleStatus = new CreditAutoSettleStatus()) {
			oCreditAutoSettleStatus.CancelCreditAutoSettle(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCreditAutoSettleSeq,
				"解約",
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.FanClubResignResult.RESULT_OK)) {
			tagTop.Visible = false;
			tagComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}
	}
	
	protected void cmdCancel_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionMan.GetNavigateUrl("ListCreditAutoSettleStatus.aspx"));
	}
}
