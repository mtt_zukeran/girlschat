/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ 会員つぶやき削除
--	Progaram ID		: DeleteManTweet
--
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_DeleteManTweet:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sManTweetSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetseq"]);

		if (string.IsNullOrEmpty(sManTweetSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_SELF_MAN_TWEET,this.ActiveForm);
		} else {
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				string sSiteCd = sessionMan.site.siteCd;
				string sUserSeq = sessionMan.userMan.userSeq;
				string sUserCharNo = sessionMan.userMan.userCharNo;
				string sResult = string.Empty;

				using (ManTweet oManTweet = new ManTweet()) {
					oManTweet.DeleteManTweet(sSiteCd,sUserSeq,sUserCharNo,sManTweetSeq,out sResult);
				}

				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_DELETE_TWEET_COMPLETE);
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
			} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("ListSelfManTweet.aspx"));
			}
		}
	}
}