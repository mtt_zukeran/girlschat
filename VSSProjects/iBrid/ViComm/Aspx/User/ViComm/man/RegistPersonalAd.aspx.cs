/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�L���o�^
--	Progaram ID		: RegistPersonalAd
--
--  Creation Date	: 2010.12.11
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_RegistPersonalAd:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			lblErrorMessage.Text = string.Empty;

			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_REGIST_CAST] != null) {
				cmdRegistCast_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sFriendIntroCd = string.Empty;
		if (CheckPersonalAdCd(out sFriendIntroCd) == false) {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_NOT_FOUND_PERSONAL_AD_CD);
			return;
		}
		
		RedirectToMobilePage(string.Format("http://{0}?intm={1}&goto=RegistUserRequestByTermId.aspx&guid=on",iBridUtil.GetStringValue(Request.Params["manRegistUrl"]),sFriendIntroCd));
	}

	protected void cmdRegistCast_Click(object sender,EventArgs e) {
		string sFriendIntroCd = string.Empty;
		if (CheckPersonalAdCd(out sFriendIntroCd) == false) {
			lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_NOT_FOUND_PERSONAL_AD_CD);
			return;
		}

		RedirectToMobilePage(string.Format("http://{0}?intw={1}&goto=RegistUserRequestByTermId.aspx&guid=on",iBridUtil.GetStringValue(Request.Params["castRegistUrl"]),sFriendIntroCd));
	}

	private bool CheckPersonalAdCd(out string pIntroducerFriendCd) {
		pIntroducerFriendCd = string.Empty;

		if (txtPersonalAdCd.Text.Equals(string.Empty)) {
			return true;
		}
		using (PersonalAd oPersonalAd = new PersonalAd()) {
			pIntroducerFriendCd = oPersonalAd.GetFriendIntroCd(txtPersonalAdCd.Text);
		}
		if (pIntroducerFriendCd.Equals(string.Empty)) {
			return false;
		} else {
			return true;
		}
	}
}
