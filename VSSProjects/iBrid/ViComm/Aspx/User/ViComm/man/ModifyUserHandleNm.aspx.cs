/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ハンドルネーム変更
--	Progaram ID		: ModifyUserHandleNm
--
--  Creation Date	: 2009.08.04
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ModifyUserHandleNm:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.userMan.bulkImpNonUseFlag == ViCommConst.FLAG_OFF) {
				if (!iBridUtil.GetStringValue(Request.QueryString["handlenm"]).Equals(string.Empty)) {
					txtHandelNm.Text = iBridUtil.GetStringValue(Request.QueryString["handlenm"]);
				} else {
					txtHandelNm.Text = sessionMan.userMan.handleNm;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			} else if (!SysPrograms.IsWithinByteCount(HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)),60)) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"ﾊﾝﾄﾞﾙﾈｰﾑ");
			}
		}


		string sNGWord;
		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			if (sessionMan.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (bOk == false) {
			return;
		} else {
			if (!iBridUtil.GetStringValue(Request.QueryString["sendmail"]).Equals("1")) {
				string sResult;
				sessionMan.userMan.ModifyUserHandleNm(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)),
					out sResult
				);

				if (sResult.Equals("0")) {
					using (ManNews oManNews = new ManNews()) {
						oManNews.LogManNews(sessionMan.site.siteCd,sessionMan.userMan.userSeq,PwViCommConst.ManNewsType.UPDATE_PROFILE);
					}

					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_MAN_PROFILE_COMPLITE));
				} else if (sResult.Equals(ViCommConst.REG_USER_RST_HANDLE_NM_EXIST.ToString())) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
				} else if (sResult.Equals(ViCommConst.REG_USER_RST_UNT_EXIST.ToString())) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
				} else if (sResult.Equals(ViCommConst.REG_USER_RST_SUB_SCR_NO_IS_NULL.ToString())) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
				} else {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
				}
			} else {
				bool bDupliFlag = false;
				if (IsAvailableService(ViCommConst.RELEASE_CHECK_HANDLE_NM_DUPLI)) {
					using (UserMan oUserMan = new UserMan()) {
						bDupliFlag = oUserMan.CheckManHandleNmDupli(sessionMan.site.siteCd,sessionMan.userMan.userSeq,Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text));
					}
				}

				if (bDupliFlag == false) {
					System.Text.Encoding enc = System.Text.Encoding.GetEncoding("Shift_JIS");
					string send = HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["send"]),enc);
					string title = HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["title"]),enc);
					string success = HttpUtility.UrlEncode(iBridUtil.GetStringValue(Request.QueryString["success"]),enc);
					string handleNm = HttpUtility.UrlEncode(txtHandelNm.Text,enc);
					string url = string.Format("MailFormConfirm.aspx?callfrom=ModifyUserHandleNm.aspx&sendmail=1&send={0}&title={1}&success={2}&handlenm={3}",send,title,success,handleNm);
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,url));
				} else {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
					return;
				}

			}
		}
	}

}
