/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �̔�����ꗗ
--	Progaram ID		: ListSaleMovie
--
--  Creation Date	: 2010.07.22
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListSaleMovie : MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		

		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_SALE_MOVIE,
					ActiveForm);
		}
	}
}
