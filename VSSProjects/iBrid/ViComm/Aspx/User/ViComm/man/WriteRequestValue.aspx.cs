/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい評価投稿
--	Progaram ID		: WriteRequestValue
--
--  Creation Date	: 2012.06.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteRequestValue:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sRequestSeq = iBridUtil.GetStringValue(Request.QueryString["req_seq"]);

		if (string.IsNullOrEmpty(sRequestSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e,sRequestSeq);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;

		if (rdoValue.Items[rdoValue.SelectedIndex].Value.Equals(string.Empty)) {
			sessionMan.errorMessage = "評価を選択して下さい";
			return;
		}

		if (!txtComment.Text.Equals(string.Empty)) {
			if (txtComment.Text.Length > 100) {
				sessionMan.errorMessage = "ｺﾒﾝﾄが長すぎます";
				return;
			}

			string sNGWord = string.Empty;

			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}

			if (!sessionMan.ngWord.VaidateDoc(txtComment.Text,out sNGWord)) {
				sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		ViewState["VALUE_NO"] = rdoValue.Items[rdoValue.SelectedIndex].Value;
		ViewState["COMMENT_TEXT"] = txtComment.Text;

		lblValue.Text = rdoValue.Items[rdoValue.SelectedIndex].Text;
		lblComment.Text = HttpUtility.HtmlEncode(txtComment.Text).Replace(System.Environment.NewLine,"<br>");

		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
		pnlComplete.Visible = false;

		if (string.IsNullOrEmpty(txtComment.Text)) {
			pnlConfirmNoComment.Visible = true;
			pnlConfirmComment.Visible = false;
		} else {
			pnlConfirmNoComment.Visible = false;
			pnlConfirmComment.Visible = true;
		}
	}

	protected void cmdTx_Click(object sender,EventArgs e,string pRequestSeq) {
		sessionMan.errorMessage = string.Empty;
		string sValueNo = iBridUtil.GetStringValue(ViewState["VALUE_NO"]);
		string sCommentText = iBridUtil.GetStringValue(ViewState["COMMENT_TEXT"]);
		string sResult = string.Empty;

		using (RequestValue oRequestValue = new RequestValue()) {
			sResult = oRequestValue.WriteRequestValue(
				sessionMan.site.siteCd,
				ViCommConst.MAN,
				pRequestSeq,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sValueNo,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sCommentText))
			);
		}

		if (sResult.Equals(PwViCommConst.WriteRequestValueResult.RESULT_OK)) {
			pnlWrite.Visible = false;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;

			if (string.IsNullOrEmpty(sCommentText)) {
				pnlCompleteNoComment.Visible = true;
				pnlCompleteComment.Visible = false;
			} else {
				pnlCompleteNoComment.Visible = false;
				pnlCompleteComment.Visible = true;
			}
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
		pnlComplete.Visible = false;
	}
}
