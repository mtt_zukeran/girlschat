<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PostCashZero.aspx.cs" Inherits="ViComm_man_PostCashZero" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" Action="https://credit.zeroweb.ne.jp/cgi-bin/ebank.cgi" Method="post">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
		<cc1:iBMobileLiteralText ID="clientip" runat="server" />
		<cc1:iBMobileLiteralText ID="act" runat="server" />
		<cc1:iBMobileLiteralText ID="money" runat="server" />
		<cc1:iBMobileLiteralText ID="telno" runat="server" />
		<cc1:iBMobileLiteralText ID="email" runat="server" />
		<cc1:iBMobileLiteralText ID="sendid" runat="server" />
		<cc1:iBMobileLiteralText ID="sendpoint" runat="server" />
		<cc1:iBMobileLiteralText ID="success_url" runat="server" />
		<cc1:iBMobileLiteralText ID="success_str" runat="server" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
