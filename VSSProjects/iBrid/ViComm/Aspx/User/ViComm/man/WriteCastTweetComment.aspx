<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WriteCastTweetComment.aspx.cs" Inherits="ViComm_man_WriteCastTweetComment" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		$PGM_HTML02;
		<tx:TextArea ID="txtComment" runat="server" Row="10" BreakBefore="false" BrerakAfter="false" Rows="2" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		$PGM_HTML03;
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
