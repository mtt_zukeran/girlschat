/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品写真ランキング(週間)一覧
--	Progaram ID		: ListProdPicRankingWeekly
--
--  Creation Date	: 2011.02.11
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdPicRankingWeekly : MobileManPageBase {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					this.Request,
					ViCommConst.INQUIRY_PRODUCT_PIC_WEELKY_RANKING,
					this.ActiveForm);

		}
	}
}
