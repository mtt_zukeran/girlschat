/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ相手の保持写真一覧
--	Progaram ID		: ListGameTreasureBattle
--
--  Creation Date	: 2011.08.05
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameTreasureBattle:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sUserSeq = iBridUtil.GetStringValue(Request.QueryString["user_seq"]);
			string sUserCharNo = iBridUtil.GetStringValue(Request.QueryString["user_char_no"]);

			if (sUserSeq.Equals(string.Empty) || sUserCharNo.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE_BATTLE_LIST,ActiveForm);
		}
	}
}
