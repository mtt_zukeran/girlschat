/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�ʖ싅���Q�[���ꗗ
--	Progaram ID		: ListPersonalYakyukenGame
--
--  Creation Date	: 2013.05.03
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListPersonalYakyukenGame:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		string sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (string.IsNullOrEmpty(sCastUserSeq) || string.IsNullOrEmpty(sCastCharNo)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_YAKYUKEN_GAME,this.ActiveForm);
		}
	}
}
