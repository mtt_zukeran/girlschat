/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@�{�X��
--	Progaram ID		: ViewGameStageBossBattle
--
--  Creation Date	: 2011.08.01
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public partial class ViComm_man_ViewGameStageBossBattle:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			Regex rgx = new Regex("^[0-9]+$");
			Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["stage_seq"]));
			if (!rgxMatch.Success) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		
			using (Town oTown = new Town()) {
				string sSiteCd = this.sessionMan.site.siteCd;
				string sUserSeq = this.sessionMan.userMan.userSeq;
				string sUserCharNo = this.sessionMan.userMan.userCharNo;
				string sSexCd = this.sessionMan.sexCd;
				string sStageSeq = this.Request.QueryString["stage_seq"];

				if (!oTown.CheckTownCrear(sSiteCd,sUserSeq,sUserCharNo,sSexCd,sStageSeq)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
			
			if(!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_STAGE_BOSS_BATTLE,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.StageClear();
			}
		}
	}

	private void StageClear() {
		string sResult;
		string sStageSeq = this.Request.Params["NEXTLINK"];
		string sPreMoney = this.sessionMan.userMan.gameCharacter.gamePoint.ToString();
		string sAreaBossType = string.Empty;
		string sBossLevel = string.Empty;
		int iLevelUpFlag = 0;
		int iAddForceCount = 0;
		int iStageClearPointFlag = 0;

		sResult = this.StageClearExecute(sStageSeq,out iLevelUpFlag,out iAddForceCount,out iStageClearPointFlag);
		
		if(sResult.Equals(PwViCommConst.GameStageClear.RESULT_OK)) {
			this.CheckQuestClear();
		}

		IDictionary<string,string> oParameters = new Dictionary<string,string>();

		if(sResult.Equals(PwViCommConst.GameStageClear.RESULT_OK) || sResult.Equals(PwViCommConst.GameStageClear.RESULT_LOSS)) {

			sAreaBossType = this.GetAreaBossType(sStageSeq);
			sBossLevel = this.GetBossLevel(sStageSeq);
			
			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
				
				oParameters.Add("stage_seq",sStageSeq);
				oParameters.Add("result",sResult);
				oParameters.Add("pre_money",sPreMoney);
				oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
				oParameters.Add("add_force_count",iAddForceCount.ToString());
				oParameters.Add("stage_clear_point_flag",iStageClearPointFlag.ToString());
				oParameters.Add("boss_level",sBossLevel);
				oParameters.Add("area_boss_type",sAreaBossType);
				oParameters.Add("boss_battle_flag",ViCommConst.FLAG_ON_STR);

				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_BOSS_BATTLE_SP,oParameters);
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,String.Format("ViewGameFlashBossBattle.aspx?stage_seq={0}&result={1}&pre_money={2}&level_up_flag={3}&add_force_count={4}&stage_clear_point_flag={5}&boss_level={6}&area_boss_type={7}&boss_battle_flag={8}",sStageSeq,sResult,sPreMoney,iLevelUpFlag.ToString(),iAddForceCount.ToString(),iStageClearPointFlag.ToString(),sBossLevel,sAreaBossType,ViCommConst.FLAG_ON_STR)));
			}
		} else if (sResult.Equals(PwViCommConst.GameStageClear.RESULT_CLEARED)) {
			sAreaBossType = this.GetAreaBossType(sStageSeq);
			sBossLevel = this.GetBossLevel(sStageSeq);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,String.Format("ViewGameStageBossBattleResult.aspx?stage_seq={0}&result={1}&boss_level={2}&area_boss_type={3}",sStageSeq,"0",sBossLevel,sAreaBossType)));
		} else if (sResult.Equals(PwViCommConst.GameStageClear.RESULT_NOT_CLEAR_TOWN)) {
			oParameters.Add("result",sResult);
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_BOSS_BATTLE_NO_FINISH,oParameters);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}

	private string StageClearExecute(string sStageSeq,out int iLevelUpFlag,out int iAddForceCount,out int iStageClearPointFlag) {
		string sResult = string.Empty;
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		Stage oStage = new Stage();

		oStage.ClearStage(sSiteCd,sUserSeq,sUserCharNo,sStageSeq,out iLevelUpFlag,out iAddForceCount,out iStageClearPointFlag,out sResult);

		return sResult;
	}

	private string GetBossLevel(string sStageSeq) {
		string sBossLevel = string.Empty;

		string sSiteCd = sessionMan.site.siteCd;
		string sSexCd = sessionMan.sexCd;

		using (Stage oStage = new Stage()) {
			sBossLevel = oStage.GetStageLevelInStageTypeByStageSeq(sSiteCd,sStageSeq,sSexCd);
		}

		return sBossLevel;
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.STAGE_CLEAR,
				PwViCommConst.GameQuestTrialCategoryDetail.STAGE_CLEAR,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
	
	private string GetAreaBossType(string pStageSeq) {
		int iValue = 0;
		string sStageGroupNo = string.Empty;
		
		using (Stage oStage = new Stage()) {
			sStageGroupNo = oStage.GetStageGroupNo(
				this.sessionMan.site.siteCd,
				this.sessionMan.sexCd,
				pStageSeq
			);
		}

		iValue = (int.Parse(sStageGroupNo) % 3);

		if (iValue == 0) {
			iValue = 3;
		}
		
		return iValue.ToString();
	}
}
