/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者つぶやき詳細
--	Progaram ID		: ViewCastDiary
--  Creation Date	: 2015.05.04
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewCastDiary:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["userseq"])) || string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["reportday"])) || string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["subseq"]))) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			tagComplete.Visible = false;
			tagLiked.Visible = false;
			DataSet ds;

			if (sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_DIARY_EX,this.ActiveForm,out ds)) {
				string sUserSeq = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["USER_SEQ"]);
				string sUserCharNo = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["USER_CHAR_NO"]);
				string sReportDay = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["REPORT_DAY"]);
				string sSubSeq = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["CAST_DIARY_SUB_SEQ"]);

				using (Access oAccess = new Access()) {
					oAccess.AccessDiaryPage(
						sessionMan.site.siteCd,
						sUserSeq,
						sUserCharNo,
						sessionMan.userMan.userSeq
					);
				}

				using (CastDiary oCastDiary = new CastDiary()) {
					oCastDiary.UpdateReadingCount(
						sessionMan.site.siteCd,
						sUserSeq,
						sUserCharNo,
						sReportDay,
						sSubSeq
					);

					if (iBridUtil.GetStringValue(Request.QueryString["like"]).Equals(ViCommConst.FLAG_ON_STR)) {

						using (Refuse oRefuse = new Refuse()) {
							if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sUserSeq,sUserCharNo)) {
								RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
							}
						}
						
						string sResult;

						oCastDiary.UpdateLikeCount(
							sessionMan.site.siteCd,
							sUserSeq,
							sUserCharNo,
							sReportDay,
							sSubSeq,
							sessionMan.userMan.userSeq,
							sessionMan.userMan.userCharNo,
							out sResult
						);

						if (sResult.Equals(PwViCommConst.RegistDiaryLikeResult.RESULT_OK)) {
							tagComplete.Visible = true;
						} else if (sResult.Equals(PwViCommConst.RegistDiaryLikeResult.RESULT_LIKED)) {
							tagLiked.Visible = true;
						} else {
							RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
							return;
						}
					}
				}
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
