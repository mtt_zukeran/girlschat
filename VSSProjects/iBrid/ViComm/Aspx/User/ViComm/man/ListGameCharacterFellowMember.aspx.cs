/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE���Ԉꗗ(���ް�I��)
--	Progaram ID		: ListGameCharacterFellowMember.aspx
--
--  Creation Date	: 2011.08.06
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameCharacterFellowMember:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sTeamExecut = iBridUtil.GetStringValue(Request.QueryString["team_execut"]);
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		string sStageSeq = iBridUtil.GetStringValue(Request.QueryString["stage_seq"]);
		string sType = iBridUtil.GetStringValue(Request.QueryString["type"]);
		string sPage = iBridUtil.GetStringValue(Request.QueryString["page_id"]);
	
		if (!sTeamExecut.Equals(string.Empty)) {

			if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
			
			if (sTeamExecut == ViCommConst.FLAG_ON_STR) {
				SetPartyMember(sPartnerUserSeq,sPartnerUserCharNo,ViCommConst.FLAG_ON,sStageSeq,sType);
			}else if (sTeamExecut == ViCommConst.FLAG_OFF_STR) {
				SetPartyMember(sPartnerUserSeq,sPartnerUserCharNo,ViCommConst.FLAG_OFF,sStageSeq,sType);
			}
		}else {
			Session["BattlePartnerUserSeq"] = iBridUtil.GetStringValue(Request.QueryString["battle_partner_user_seq"]);
			Session["BattlePartnerUserCharNo"] = iBridUtil.GetStringValue(Request.QueryString["battle_partner_user_char_no"]);
			if (sType.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
		
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_MEMBER_LIST,ActiveForm);
	}

	private void SetPartyMember(string pPartnerUserSeq,string pPartnerUserCharNo,int TeamFlag,string pStageSeq,string sType) {
		
		string sResult = null;
		int iSetPartyMemberFlag = 0;
		string sReleasePartnerUserSeq = null;
		string sReleasePartnerUserCharNo = null;
		
		if (TeamFlag == ViCommConst.FLAG_ON) {

			DataSet oDataSet;

			using (CastGameCharacterFellowMember oCastGameCharacterFellowMember = new CastGameCharacterFellowMember()) {
				oDataSet = oCastGameCharacterFellowMember.GetTeamMemberSeqData(
					this.sessionMan.site.siteCd,
					this.sessionMan.userMan.userSeq,
					this.sessionMan.userMan.userCharNo,
					PwViCommConst.GameCharFellowApplicationCd.FELLOW,
					sType,
					ViCommConst.FLAG_ON_STR
				);
			}

			if (oDataSet.Tables[0].Rows.Count > 0) {
				sReleasePartnerUserSeq = oDataSet.Tables[0].Rows[0]["PARTNER_USER_SEQ"].ToString();
				sReleasePartnerUserCharNo = oDataSet.Tables[0].Rows[0]["PARTNER_USER_CHAR_NO"].ToString();
				iSetPartyMemberFlag = ViCommConst.FLAG_ON;
			}

			if (iSetPartyMemberFlag == ViCommConst.FLAG_ON) {
				using (GameFellow oGameFellow = new GameFellow()) {
					sResult = oGameFellow.SetPartyMember(
								this.sessionMan.site.siteCd,
								this.sessionMan.userMan.userSeq,
								this.sessionMan.userMan.userCharNo,
								sReleasePartnerUserSeq,
								sReleasePartnerUserCharNo,
								ViCommConst.FLAG_OFF
							);
				}
				
				if (sResult != "0") {
					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("result",sResult);
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_SET_PARTY_MEMBER_ERR,oParameters);
				}
						
			}
		}
		
		using (GameFellow oGameFellow = new GameFellow()) {
			sResult = oGameFellow.SetPartyMember(
						this.sessionMan.site.siteCd,
						this.sessionMan.userMan.userSeq,
						this.sessionMan.userMan.userCharNo,
						pPartnerUserSeq,
						pPartnerUserCharNo,
						TeamFlag
					);
		}

		if (sResult == "0") {
			if (!pStageSeq.Equals(string.Empty)) {	
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ViewGameStageBossBattle.aspx?stage_seq=" + pStageSeq));		
			} else {
				if (!iBridUtil.GetStringValue(Session["BattlePartnerUserSeq"]).Equals(string.Empty) && !iBridUtil.GetStringValue(Session["BattlePartnerUserCharNo"]).Equals(string.Empty)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"RegistGameBattleStartParty.aspx?partner_user_seq="
					+ iBridUtil.GetStringValue(Session["BattlePartnerUserSeq"]) + "&partner_user_char_no=" + iBridUtil.GetStringValue(Session["BattlePartnerUserCharNo"])));		
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DivideGameBattleTop.aspx"));
				}
			}
		} else {
			IDictionary<string,string> oParameters = new Dictionary<string,string>();
			oParameters.Add("result",sResult);
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_SET_PARTY_MEMBER_ERR,oParameters);
		}
	}
}
