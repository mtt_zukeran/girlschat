/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾌﾟﾚｾﾞﾝﾄされた一覧
--	Progaram ID		: ListGamePresentHistory
--
--  Creation Date	: 2011.09.16
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGamePresentHistory:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			using (GameCharacter oGameCharacter = new GameCharacter()) {
				oGameCharacter.UpdateLastPresentLogCheck(
					this.sessionMan.site.siteCd,
					this.sessionMan.userMan.userSeq,
					this.sessionMan.userMan.userCharNo
				);
			}
			
			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CAST_PRESENT_HISTORY,ActiveForm);
		} else {
			string sPresentHistorySeq = string.Empty;
			
			for (int i = 1;i <= 10;i++) {
				if (Request.Form[ViCommConst.BUTTON_GOTO_LINK + "_" + i] != null) {
					sPresentHistorySeq = iBridUtil.GetStringValue(Request.Params["present_history_seq" + i.ToString()]);
				}
			}

			if (!sPresentHistorySeq.Equals(string.Empty)) {
				string sResult = string.Empty;

				using (GameCharacterPresentHistory oGameCharacterPresentHistory = new GameCharacterPresentHistory()) {
					sResult = oGameCharacterPresentHistory.GetPresentGameItem(
						this.sessionMan.site.siteCd,
						sPresentHistorySeq
					);
				}

				if (sResult == PwViCommConst.GetPresentGameItemResult.RESULT_OK) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(
						sessionMan.root + sessionMan.sysType,
						sessionMan.sessionId,
						string.Format("ViewGamePresentHistory.aspx?get_flag=1&present_history_seq={0}",sPresentHistorySeq)
					));
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}
	}
}
