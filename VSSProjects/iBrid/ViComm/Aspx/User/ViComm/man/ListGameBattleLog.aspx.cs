/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹް� ���ًL�^�ꗗ
--	Progaram ID		: ListGameBattleLog
--
--  Creation Date	: 2011.09.16
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameBattleLog:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		string sBattleType = iBridUtil.GetStringValue(Request.QueryString["battle_type"]);
		if (sBattleType.Equals(PwViCommConst.GameBattleType.SUPPORT)) {
			this.UpdateSupportRequestCheck();
		}

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_LOG,ActiveForm);
		}
	}
	
	private void UpdateSupportRequestCheck() {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sResult = string.Empty;
		
		using (SupportRequest oSupportRequest = new SupportRequest()) {
			oSupportRequest.UpdateSupportRequestCheck(sSiteCd,sUserSeq,sUserCharNo,string.Empty,out sResult);
		}
	}
}
