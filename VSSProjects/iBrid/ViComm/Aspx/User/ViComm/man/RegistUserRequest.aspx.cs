/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �V�K�o�^
--	Progaram ID		: RegistUserRequest
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_RegistUserRequest:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			txtDomain.Text = sessionMan.site.mailHost;
		}
	}
}
