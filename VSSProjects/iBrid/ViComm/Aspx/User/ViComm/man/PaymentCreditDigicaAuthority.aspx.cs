/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ڼޯĎx��(DIGICA�ڼޯ�)Auth����
--	Progaram ID		: PaymentCreditDigicaAuthority
--
--  Creation Date	: 2010.06.03
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCreditDigicaAuthority:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		lblErrorMessage.Text = string.Empty;
		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

		if (sessionMan.userMan.zeroSettleOkFlag != 0) {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_EXIST_CREDIT_DEAL);
			sessionMan.valid = false;
		} else {
			sessionMan.valid = true;
		}

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = "0";
		string sSid = "";
		int iZeroSettleServicePoint = 0;

		if (sessionMan.valid == false) {
			return;
		}

		using (Site oSite = new Site()) {
			if (oSite.GetOne(sessionMan.site.siteCd)) {
				iZeroSettleServicePoint = oSite.zeroSettleServicePoint;
			}
		}

		using (SettleLog oLog = new SettleLog()) {
			oLog.LogSettleRequest(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				ViCommConst.SETTLE_CORP_DIGICA,
				ViCommConst.SETTLE_CREDIT_AUTH,
				ViCommConst.SETTLE_STAT_SETTLE_NOW,
				int.Parse(sSalesAmt),
				iZeroSettleServicePoint,
				string.Empty,
				string.Empty,
				out sSid);
		}
		string sSettleUrl = "";
		string sBackUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_DIGICA,ViCommConst.SETTLE_CREDIT_AUTH)) {
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sBackUrl);
			}
		}
		Response.Redirect(sSettleUrl);
	}
}
