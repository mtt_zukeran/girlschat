/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ｿｰｼｬﾙｹﾞｰﾑ用Flash(ﾅﾝﾊﾟ時お宝GET男性用)
--	Progaram ID		: ViewGameFlashBattle
--
--  Creation Date	: 2012.01.06
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_man_ViewGameFlashGetManTreasure:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else if (sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else {
			string sGetTreasureLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["get_treasure_log_seq"]);
			string sWebPhisicalDir = sessionMan.site.webPhisicalDir;
			string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.GAME_FLASH_DIR);
			string sXmlPath = Path.Combine(sFlashDir,"get_man_treasure.xml");
			string sPhotoImgPath = string.Empty;
			string sObjPhotoImgPath = string.Empty;

			if (string.IsNullOrEmpty(sGetTreasureLogSeq)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}

			using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
				DataSet oDataSet = oGetManTreasureLog.GetOne(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sGetTreasureLogSeq
				);

				if (oDataSet.Tables[0].Rows.Count != 0) {
					sPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["SMALL_PHOTO_IMG_PATH"]));
					sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]));
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
			}

			if (string.IsNullOrEmpty(sPhotoImgPath) || string.IsNullOrEmpty(sObjPhotoImgPath)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}

			string sXmlData = string.Empty;
			string sPhotoImgBase64 = string.Empty;
			string sObjPhotoImgBase64 = string.Empty;

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!File.Exists(sPhotoImgPath)) {
					sPhotoImgPath = Path.Combine(sWebPhisicalDir,PwViCommConst.NoPicJpgPath);
				}

				if (!File.Exists(sObjPhotoImgPath)) {
					sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,PwViCommConst.NoPicJpgPath);
				}

				//XMLファイル読み込み
				using (StreamReader srXml = new StreamReader(sXmlPath)) {
					sXmlData = srXml.ReadToEnd();
				}

				//プロフ画像読み込み
				byte[] bufPhotoImg = FileHelper.GetFileBuffer(sPhotoImgPath);
				sPhotoImgBase64 = Convert.ToBase64String(bufPhotoImg);

				//お宝画像読み込み
				byte[] bufObjPhotoImg = FileHelper.GetFileBuffer(sObjPhotoImgPath);
				sObjPhotoImgBase64 = Convert.ToBase64String(bufObjPhotoImg);

				sXmlData = sXmlData.Replace("%IMAGE1%",sPhotoImgBase64);
				sXmlData = sXmlData.Replace("%IMAGE2%",sObjPhotoImgBase64);
			}

			string sTempSwf = Path.GetTempFileName();

			using (Process prExe = new Process()) {
				prExe.StartInfo.FileName = "D:\\swfmill\\swfmill.exe";
				prExe.StartInfo.Arguments = string.Format("xml2swf stdin {0}",sTempSwf);
				prExe.StartInfo.CreateNoWindow = true;//コンソールウィンドウを開かない
				prExe.StartInfo.UseShellExecute = false;//シェル機能を使用しない
				prExe.StartInfo.RedirectStandardInput = true;//標準入力を使用する
				prExe.Start();

				using (StreamWriter sw = prExe.StandardInput) {
					sw.Write(sXmlData); // 子プロセスへの書き込み
				}

				prExe.WaitForExit(3000);
			}

			byte[] bufTempSwf = FileHelper.GetFileBuffer(sTempSwf);
			File.Delete(sTempSwf);

			IDictionary<string,string> oParam = this.CreateParam(this.Request.QueryString);

			byte[] oSwfBuffer = GameFlashLiteHelper.RewriteSwfBuffer(bufTempSwf,oParam);
			GameFlashLiteHelper.OutputSwf(oSwfBuffer);
		}
	}

	private IDictionary<string,string> CreateParam(NameValueCollection pQueryString) {
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		string sTreasureCompleteFlag = iBridUtil.GetStringValue(pQueryString["treasure_complete_flag"]);
		string sNextPageNm = string.Empty;

		if (sTreasureCompleteFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sNextPageNm = "ViewGameFlashComplete.aspx";
		} else {
			sNextPageNm = "ViewGameTownClearMissionOK.aspx";
		}

		UrlBuilder oNextUrl = new UrlBuilder(sNextPageNm);

		foreach (string key in pQueryString.AllKeys) {
			oNextUrl.AddParameter(key,iBridUtil.GetStringValue(pQueryString[key]));
		}

		oParam.Add("nextUrl",oNextUrl.ToString());

		return oParam;
	}
}