/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・女性の動画一覧
--	Progaram ID		: ListGameCastMovie.aspx
--
--  Creation Date	: 2011.08.26
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameCastMovie:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);
		
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CAST_MOVIE_LIST,ActiveForm);
	}
}
