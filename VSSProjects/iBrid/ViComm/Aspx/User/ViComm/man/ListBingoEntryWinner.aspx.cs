﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾋﾞﾝｺﾞ Winner一覧
--	Progaram ID		: ListBingoEntryWinner
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListBingoEntryWinner : MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_BINGO_ENTRY_WINNER,
					ActiveForm);
		}
	}

}
