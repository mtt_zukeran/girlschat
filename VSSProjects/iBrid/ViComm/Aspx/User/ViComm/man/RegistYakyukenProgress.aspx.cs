/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 野球拳ジャンケン実行
--	Progaram ID		: RegistYakyukenProgress
--
--  Creation Date	: 2013.05.03
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistYakyukenProgress:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sYakyukenGameSeq = iBridUtil.GetStringValue(Request.QueryString["ygameseq"]);
		string sManJyankenType = iBridUtil.GetStringValue(Request.QueryString["jtype"]);

		if (string.IsNullOrEmpty(sYakyukenGameSeq) || string.IsNullOrEmpty(sManJyankenType)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sCastJyankenType;
			string sWinStatus;
			string sYakyukenType;
			string sResult;

			using (YakyukenProgress oYakyukenProgress = new YakyukenProgress()) {
				oYakyukenProgress.RegistYakyukenProgress(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sYakyukenGameSeq,
					sManJyankenType,
					out sCastJyankenType,
					out sWinStatus,
					out sYakyukenType,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

				UrlBuilder oUrlBuilder = new UrlBuilder("ViewYakyukenGame.aspx");
				oUrlBuilder.AddParameter("scrid","01");
				oUrlBuilder.AddParameter("ygameseq",sYakyukenGameSeq);
				oUrlBuilder.AddParameter("ytype",sYakyukenType);
				oUrlBuilder.AddParameter("winstatus",sWinStatus);
				oUrlBuilder.AddParameter("manjtype",sManJyankenType);
				oUrlBuilder.AddParameter("castjtype",sCastJyankenType);

				RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		}
	}
}
