/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板写真・動画一覧
--	Progaram ID		: ListBbsObj
--
--  Creation Date	: 2010.09.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using ViComm;
using iBridCommLib;

public partial class ViComm_man_ListBbsObj:MobileObjManPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_OBJ,ActiveForm);
			bool bOnlyUsed = iBridUtil.GetStringValue(Request.QueryString["used"]).Equals(ViCommConst.FLAG_ON_STR);
			string sScreenId = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);
			string sAttrSeq = iBridUtil.GetStringValue(this.Request.QueryString["attrseq"]);

			if (sessionMan.site.bbsMovieAttrFlag != ViCommConst.FLAG_ON) {
				pnlSearchList.Visible = false;
			} else if (IsAvailableService(ViCommConst.RELEASE_DISABLE_ATTR_COMBO_BOX_CNT,2)) {
				CreateCastPicAttrComboBox();
			} else {
				// カテゴリSEQ取得
				int iCurrentCategoryIndex;
				string sCategorySeq = "";
				int.TryParse(iBridUtil.GetStringValue(Request.QueryString["category"]),out iCurrentCategoryIndex);
				if (iCurrentCategoryIndex > 0) {
					sCategorySeq = ((ActCategory)sessionMan.categoryList[iCurrentCategoryIndex]).actCategorySeq;
				}

				CreateObjAttrComboBox(string.Empty,string.Empty,sCategorySeq,string.Empty,ViCommConst.ATTACHED_BBS.ToString(),false,false,true,bOnlyUsed);
			}

			if (!string.IsNullOrEmpty(sScreenId) && !string.IsNullOrEmpty(sAttrSeq)) {
				this.pnlSearchList.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 選択された値をセッションに設定して検索します。
		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;

		string[] selectedValueList = selectedValue.Split(',');

		string sAddQuery = string.Format("&attrtypeseq={0}&attrseq={1}",selectedValueList[0],selectedValueList[1]);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
												   "ListBbsObj.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&attrtypeseq=[0-9]*|&attrseq=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}
}