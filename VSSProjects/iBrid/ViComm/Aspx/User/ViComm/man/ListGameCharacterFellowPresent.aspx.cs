/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・贈り先仲間一覧
--	Progaram ID		: ListGameCharacterFellowPresent
--
--  Creation Date	: 2011.09.07
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameCharacterFellowPresent:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {

		string sItemSeq = iBridUtil.GetStringValue(Request.QueryString["item_seq"]);

		if (sItemSeq.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_PRESENT_LIST,ActiveForm);
	}
}
