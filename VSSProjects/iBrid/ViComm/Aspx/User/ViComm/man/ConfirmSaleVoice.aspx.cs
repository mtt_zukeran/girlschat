/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 販売着ボイス購入確認
--	Progaram ID		: ConfirmSaleVoice
--
--  Creation Date	: 2010.07.29
--  Creater			: i-Brid(Kazuaki.Itoh)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ConfirmSaleVoice:MobileManPageBase {
	private const string ACTION_PURCHASE = "purchase";

	private string action = string.Empty;
	private string voiceSeq = string.Empty;
	protected void Page_Load(object sender,EventArgs e) {
		if(!IsPostBack){
			action = iBridUtil.GetStringValue(Request.QueryString["action"]);
			voiceSeq = iBridUtil.GetStringValue(Request.QueryString["voiceseq"]);
			
			if(this.action.Equals(ACTION_PURCHASE)) {
				PurchaseAction(this.voiceSeq);
				this.RedirectToViewSaleVoice();
			}
				
			this.Response.Filter = this.sessionMan.InitScreen(Response.Filter,this.frmMain,this.Request,this.ViewState);
			sessionMan.ControlList(
				Request,
				ViCommConst.INQUIRY_SALE_VOICE,
				ActiveForm);
		}
	}
	private void PurchaseAction(string pVoiceSeq) {
		
		// 購入済の場合はそのまま着ﾎﾞｲｽ詳細画面へ遷移する
		if(this.AlreadySaleVoiceUsed(pVoiceSeq)){
			this.RedirectToViewSaleVoice();
		}


		decimal sCastSeq;
		string sCastCharNo = null;

		using(CastVoice oCastVoice = new CastVoice()) {
			DataSet oCastVoiceDs = oCastVoice.GetPageCollectionBySeq(voiceSeq,false,false,ViCommConst.ATTACHED_VOICE.ToString());
			if(oCastVoiceDs.Tables[0].Rows.Count == 0) {
				string sErrorMessage = string.Format("着ﾎﾞｲｽ情報が取得できませんでした。VOICE_SEQ:{0}",voiceSeq);
				throw new ApplicationException(sErrorMessage);
			}
			DataRow oCastVoiceDr = oCastVoiceDs.Tables[0].Rows[0];

			sCastSeq = (long)oCastVoiceDr["USER_SEQ"];
			sCastCharNo = (string)oCastVoiceDr["USER_CHAR_NO"];
		}


		int iChargePoint = 0;
		// ポイント残のチェック。残ポイントが必要ポイントを下回る場合は警告画面へ遷移する
		if(!sessionMan.CheckDownloadVoiceBalance(sCastSeq.ToString(),sCastCharNo,pVoiceSeq,out iChargePoint)) {
			RedirectToMobilePage(GenerateFullUrl("DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_DOWNLOAD_VOICE));
			return;
		}

		using(WebUsedLog oLog = new WebUsedLog()) {
			oLog.CreateWebUsedReport(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.CHARGE_DOWNLOAD_VOICE,
					iChargePoint,
					sCastSeq.ToString(),
					sCastCharNo,
					string.Empty);
		}
		using(ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			oObjUsedHistory.AccessBbsObj(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.ATTACHED_VOICE.ToString(),
					pVoiceSeq);
		}

		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
	}
	
	/// <summary>
	/// 既に購入済かどうかを示す値を取得する。
	/// </summary>
	/// <param name="pVoiceSeq">着ﾎﾞｲｽSEQ</param>
	/// <returns>既に購入済の場合はtrue。それ以外はfalse。</returns>
	private bool AlreadySaleVoiceUsed(string pVoiceSeq) {
		using(ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			return oObjUsedHistory.GetOne(
								sessionMan.userMan.siteCd,
								sessionMan.userMan.userSeq,
								ViCommConst.ATTACHED_VOICE.ToString(),
								pVoiceSeq);
		}
	}

	private void RedirectToViewSaleVoice() {
	
		UrlBuilder oUrlBuilder = new UrlBuilder("ViewSaleVoice.aspx");
		oUrlBuilder.Parameters.Add("voiceseq",voiceSeq);

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,oUrlBuilder.ToString()));
	}

}
