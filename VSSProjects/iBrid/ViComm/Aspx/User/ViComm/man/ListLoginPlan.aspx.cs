/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者ログインスケジュール一覧
--	Progaram ID		: ListLoginPlan
--
--  Creation Date	: 2014.12.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListLoginPlan:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			this.CreateList();
			string sFromTime = iBridUtil.GetStringValue(this.Request.QueryString["loginstarttime"]);
			string sFromDate = string.Empty;
			string sFromHourMint = string.Empty;
			DateTime dt = new DateTime();
			if (!string.IsNullOrEmpty(sFromTime)) {
				if (!DateTime.TryParse(sFromTime,out dt)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
			} else {
				string sMin = string.Empty;
				if (DateTime.Now.Minute < 30) {
					sMin = "00";
				} else {
					sMin = "30";
				}
				sFromTime = string.Format("{0}:{1}",DateTime.Now.ToString("yyyy/MM/dd HH"),sMin);
				dt = DateTime.Parse(sFromTime);
			}

			sFromDate = dt.ToString("yyyy/MM/dd");
			sFromHourMint = dt.ToString("HH:mm");

			foreach (MobileListItem item in lstFromDate.Items) {
				if (item.Value == sFromDate) {
					item.Selected = true;
				}
			}

			foreach (MobileListItem item in lstFromTime.Items) {
				if (item.Value == sFromHourMint) {
					item.Selected = true;
				}
			}

			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_LOGIN_PLAN,this.ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void CreateList() {
		string[] sWeekArr = { "日","月","火","水","木","金","土" };
		string sMaxLoginPlanDaysSelectCount = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MaxWaitScheduleDaysSelectCount"]);
		int iMaxLoginPlanDaysSelectCount;
		if (!int.TryParse(sMaxLoginPlanDaysSelectCount,out iMaxLoginPlanDaysSelectCount)) {
			iMaxLoginPlanDaysSelectCount = 15;
		}

		DateTime dt = DateTime.Now;

		for (int i = 0;i < iMaxLoginPlanDaysSelectCount;i++) {
			lstFromDate.Items.Add(new MobileListItem(string.Format("{0} ({1})",dt.ToString("MM/dd"),sWeekArr[int.Parse(dt.DayOfWeek.ToString("d"))]),dt.ToString("yyyy/MM/dd")));
			dt = dt.AddDays(1);
		}

		for (int i = 0;i <= 23;i++) {
			lstFromTime.Items.Add(new MobileListItem(string.Format("{0}:00",i.ToString("d2")),string.Format("{0}:00",i.ToString("d2"))));
			lstFromTime.Items.Add(new MobileListItem(string.Format("{0}:30",i.ToString("d2")),string.Format("{0}:30",i.ToString("d2"))));
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		DateTime dt;

		string sFromTime = string.Empty;
		if (iBridUtil.GetStringValue(Request.Form["not_use_time_select_flag"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sFromTime = "00:00";
		} else {
			sFromTime = iBridUtil.GetStringValue(lstFromTime.Selection.Value);
		}

		if (DateTime.TryParse(string.Format("{0} {1}",lstFromDate.Selection.Value,sFromTime),out dt)) {
			string sUrl = "ListLoginPlan.aspx" + "?loginstarttime=" + HttpUtility.UrlEncode(string.Format("{0} {1}",lstFromDate.Selection.Value,sFromTime));
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sUrl));
		} else {
			sessionMan.errorMessage = "日時を正しく入力してください。";
		}
	}
}