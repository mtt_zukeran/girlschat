/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсEʸގ��s
--	Progaram ID		: RegistGameHugCharacter.aspx
--
--  Creation Date	: 2011.08.09
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameHugCharacter:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		
		if (!IsPostBack) {
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
			string sResult = string.Empty;

			this.CheckGameRefusePartner(sPartnerUserSeq,sPartnerUserCharNo);
			this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

			if (!sPartnerUserSeq.Equals(string.Empty) && !sPartnerUserCharNo.Equals(string.Empty)) {
				string sPreCooperationPoint = sessionMan.userMan.gameCharacter.cooperationPoint.ToString();
				string sPreFriendryPoint = this.GetFriendryPoint(sPartnerUserSeq,sPartnerUserCharNo);
				sResult = HugGameCharacter(sPartnerUserSeq,sPartnerUserCharNo);
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,string.Format("ViewGameCharacterHugResult.aspx?result={0}&partner_user_seq={1}&partner_user_char_no={2}&pre_cooperation_point={3}&pre_friendly_point={4}",sResult,sPartnerUserSeq,sPartnerUserCharNo,sPreCooperationPoint,sPreFriendryPoint)));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}

	private string HugGameCharacter(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult = string.Empty;

		using (GameCommunication oGameCommunication = new GameCommunication()) {
			sResult = oGameCommunication.HugGameCharacter(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo
			);
		}
		
		if(sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			this.CheckQuestClear(this.sessionMan.userMan.userSeq,this.sessionMan.userMan.userCharNo,PwViCommConst.GameQuestTrialCategoryDetail.HUG,ViCommConst.MAN);
			this.CheckQuestClear(pPartnerUserSeq,pPartnerUserCharNo,PwViCommConst.GameQuestTrialCategoryDetail.PARTNER_HUG,ViCommConst.OPERATOR);
		}

		return sResult;
	}
	
	private string GetFriendryPoint(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sFriendryPoint = string.Empty;
		
		FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.UserSeq = this.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
		oCondition.PartnerUserSeq = pPartnerUserSeq;
		oCondition.PartnerUserCharNo = pPartnerUserCharNo;
		using(FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
			sFriendryPoint = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
		}
		
		return sFriendryPoint;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo,string pTrialCategoryDetail,string pSexCd) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				pUserSeq,
				pUserCharNo,
				PwViCommConst.GameQuestTrialCategory.HUG,
				pTrialCategoryDetail,
				out sQuestClearFlag,
				out sResult,
				pSexCd
			);
		}
	}
}
