/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: Sakazuki�x��(�ꗗ)
--	Progaram ID		: PaymentSakazukiList
--
--  Creation Date	: 2010.04.02
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentSakazukiList:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {

		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sSid = "";
			string sMId = iBridUtil.GetStringValue(Request.QueryString["m_id"]);
			string sTmpId = iBridUtil.GetStringValue(Request.QueryString["tmp_id"]);
			string sTId = iBridUtil.GetStringValue(Request.QueryString["t_id"]);
			string sCtId = iBridUtil.GetStringValue(Request.QueryString["ct_id"]);
			string sPtMin = iBridUtil.GetStringValue(Request.QueryString["pt_min"]);
			string sPtMax = iBridUtil.GetStringValue(Request.QueryString["pt_max"]);
			string sOrder = iBridUtil.GetStringValue(Request.QueryString["order"]);
			string sLimit = iBridUtil.GetStringValue(Request.QueryString["limit"]);
			string sPage = iBridUtil.GetStringValue(Request.QueryString["page"]);
			string sCheck = iBridUtil.GetStringValue(Request.QueryString["check"]);
			string sCostMax = iBridUtil.GetStringValue(Request.QueryString["cost_max"]);
			string sCostMin = iBridUtil.GetStringValue(Request.QueryString["cost_min"]);
			
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_GACHIRI_SEL_LIST,
					ViCommConst.SETTLE_POINT_AFFILIATE,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					0,
					0,
					"",
					"",
					out sSid);
			}
			string sSettleUrl = "";

			using (SiteSettle oSiteSettle = new SiteSettle()) {
				if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_GACHIRI_SEL_LIST,ViCommConst.SETTLE_POINT_AFFILIATE)) {
					Encoding enc = Encoding.GetEncoding("Shift_JIS");
					sSettleUrl = string.Format(oSiteSettle.settleUrl,sMId,sTmpId,sSid + ":" + sessionMan.userMan.userSeq,sTId,sCtId,sPtMin,sPtMax,sOrder,sLimit,sPage,sCheck,sCostMax,sCostMin);
				}
			}
			Response.Redirect(sSettleUrl);
		}
	}
}
