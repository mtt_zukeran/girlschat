/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール添付動画管理
--	Progaram ID		: ViewMailMovieConf
--
--  Creation Date	: 2009.08.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewMailMovieConf:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		string sPlayMovie = iBridUtil.GetStringValue(Request.QueryString["playmovie"]);
		bool bDownload = false;


		if (!IsPostBack) {
			if (sessionMan.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {

				txtTitle.Text = sessionMan.GetManMovieValue("MOVIE_TITLE");
				ViewState["MOVIE_SEQ"] = sessionMan.GetManMovieValue("MOVIE_SEQ");

				if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
					using (UserManMovie oUserManMovie = new UserManMovie()) {
						string sMovieTitle = txtTitle.Text;
						oUserManMovie.UpdateProfileMovie(
							sessionMan.site.siteCd,
							sessionMan.userMan.userSeq,
							ViewState["MOVIE_SEQ"].ToString(),
							sMovieTitle,
							1,
							ViCommConst.ATTACHED_MAIL);
					}
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_USER_MAN_MAIL_MOVIE));
				} else if (!sMovieSeq.Equals("") && sPlayMovie.Equals("1")) {
					bDownload = Download(sMovieSeq);
				};

			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListMailMovieConf.aspx?movietype=3"));
			}
		}

		if (bDownload == false) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

			if (!IsPostBack) {
			} else {
				if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
					cmdSubmit_Click(sender,e);
				}
			}
		}
	}

	private bool Download(string pMovieSeq) {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.MAN,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.MAN);
				return true;
			} else {
				return false;
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		sessionMan.errorMessage = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		string sNGWord = string.Empty;

		if (txtTitle.Text.Equals(string.Empty)) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		} else {
			if (sessionMan.ngWord.VaidateDoc(txtTitle.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (bOk == false) {
			return;
		} else {
			string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtTitle.Text));
			using (UserManMovie oUserManMovie = new UserManMovie()) {
				oUserManMovie.UpdateProfileMovie(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					iBridUtil.GetStringValue(ViewState["MOVIE_SEQ"]),
					txtTitle.Text,
					0,
					ViCommConst.ATTACHED_MAIL);
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListMailMovieConf.aspx"));
		}
	}
}
