/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ　ﾊﾞﾄﾙ作戦設定
--	Progaram ID		: RegistGameBattleStart
--
--  Creation Date	: 2011.08.25
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_man_RegistGameBattleStart:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		int iTutorialFlag = ViCommConst.FLAG_OFF;

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

			if (!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_START,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				string sTreasureSeq = iBridUtil.GetStringValue(this.Request.Params["NEXTLINK"]);
				string sCastUserSeq = null;
				string sCastUserCharNo = null;
				
				if (!string.IsNullOrEmpty(this.sessionMan.userMan.gameCharacter.tutorialStatus)) {
					iTutorialFlag = ViCommConst.FLAG_ON;
				}
				
				int iAttackForceCount;
				int.TryParse(iBridUtil.GetStringValue(this.Request.Params["attack_force_count"]),out iAttackForceCount);
				
				if (iAttackForceCount > 0) {

					DataSet oDataSetManTreasure;
					using (ManTreasure oManTreasure = new ManTreasure()) {
						oDataSetManTreasure = oManTreasure.GetCastSeqData(this.sessionMan.site.siteCd,sTreasureSeq);
					}

					foreach (DataRow drManTreasure in oDataSetManTreasure.Tables[0].Rows) {
						sCastUserSeq = drManTreasure["USER_SEQ"].ToString();
						sCastUserCharNo = drManTreasure["USER_CHAR_NO"].ToString();
					}
					
					string sFriendlyPoint = null;
					
					FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
					oCondition.SiteCd = this.sessionMan.site.siteCd;
					oCondition.UserSeq = this.sessionMan.userMan.userSeq;
					oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
					oCondition.PartnerUserSeq = sCastUserSeq;
					oCondition.PartnerUserCharNo = sCastUserCharNo;

					using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
						sFriendlyPoint = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
					}

					this.BattleStart(sPartnerUserSeq,sPartnerUserCharNo,sTreasureSeq,iAttackForceCount,iTutorialFlag,sFriendlyPoint);
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}
	}

	private void BattleStart(string sPartnerUserSeq,string sPartnerUserCharNo,string sTreasureSeq,int iAttackForceCount,int iTutorialFlag,string sFriendlyPoint) {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sSupportUserSeq = string.Empty;
		string sSupportUserCharNo = string.Empty;
		string sSupportBattleLogSeq = string.Empty;
		string sSupportRequestSubSeq = string.Empty;
		string sBattleType = PwViCommConst.GameBattleType.SINGLE;
		int iSurelyWinFlag = (iTutorialFlag.Equals(ViCommConst.FLAG_ON)) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		int iLevelUpFlag;
		int iAddForceCount;
		int iTreasureCompleteFlag;
		int iBonusGetFlag;
		int iGetCompCountItemFlag;				//お宝規定回数コンプ時獲得アイテム取得フラグ
		string[] sGetCompCountItemSeq = null;	//お宝規定回数コンプ時獲得アイテムSEQ
		string[] sGetCompCountItemCount = null;	//お宝規定回数コンプ時獲得アイテム付与数
		int iWinFlag;
		int iComePoliceFlag;
		int iTrapUsedFlag;
		string sBattleLogSeq;
		string[] BreakGameItemSeq;
		string sResult;
		string sPreExp = this.sessionMan.userMan.gameCharacter.exp.ToString();

		Battle oBattle = new Battle();
		oBattle.BattleStart(
			sSiteCd,
			sUserSeq,
			sUserCharNo,
			sPartnerUserSeq,
			sPartnerUserCharNo,
			sSupportUserSeq,
			sSupportUserCharNo,
			sSupportBattleLogSeq,
			sSupportRequestSubSeq,
			sBattleType,
			sTreasureSeq,
			iSurelyWinFlag,
			iAttackForceCount,
			out iLevelUpFlag,
			out iAddForceCount,
			out iTreasureCompleteFlag,
			out iBonusGetFlag,
			out iWinFlag,
			out iComePoliceFlag,
			out iTrapUsedFlag,
			out sBattleLogSeq,
			out BreakGameItemSeq,
			out iGetCompCountItemFlag,
			out sGetCompCountItemSeq,
			out sGetCompCountItemCount,
			out sResult
		);

		string sRedirectUrl = string.Empty;

		if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_OK)) {
			this.CheckQuestClear(sUserSeq,sUserCharNo);
			this.CheckQuestClear(sPartnerUserSeq,sPartnerUserCharNo);
			
			if (iTutorialFlag.Equals(ViCommConst.FLAG_ON)) {
				
				string sGameItemSeq;
				
				int iNextTutorialStatus = int.Parse(sessionMan.userMan.gameCharacter.tutorialStatus) + 1;
				//ﾁｭｰﾄﾘｱﾙ状態更新
				using (GameCharacter oGameCharacter = new GameCharacter()) {
					oGameCharacter.SetupTutorialStatus(
						sessionMan.userMan.siteCd,
						sessionMan.userMan.userSeq,
						sessionMan.userMan.userCharNo,
						"7"
					);
				}
			
				//ﾊﾞｲｱｹﾞﾗを付与
				using (GameItem oGameItem = new GameItem()) {
					sGameItemSeq = oGameItem.GetGameItem(
						sessionMan.userMan.siteCd,
						sessionMan.sexCd,
						PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE,
						"GAME_ITEM_SEQ"
					);
				}

				using (GameItem oGameItem = new GameItem()) {
					oGameItem.GamePossessionItemMainte(
						sessionMan.userMan.siteCd,
						sessionMan.userMan.userSeq,
						sessionMan.userMan.userCharNo,
						sGameItemSeq
					);
				}
			}

			string sBreakItemSeqList = string.Join("_",BreakGameItemSeq);
			
			if(sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
				string sPartnerGameCharacterType = this.GetGameCharacterType(sPartnerUserSeq,sPartnerUserCharNo);

				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				if (iWinFlag.Equals(ViCommConst.FLAG_ON)) {

					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("treasure_complete_flag",iTreasureCompleteFlag.ToString());
					oParameters.Add("break_item_seq",sBreakItemSeqList);
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("tutorial_flag",iTutorialFlag.ToString());
					oParameters.Add("pre_friendly_point",sFriendlyPoint);
					oParameters.Add("get_comp_count_item_seq",string.Join("_",sGetCompCountItemSeq));
					oParameters.Add("get_comp_count_item_count",string.Join("_",sGetCompCountItemCount));
					oParameters.Add("get_comp_count_item_rec_count",sGetCompCountItemSeq.Length.ToString());
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
				} else {
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("come_police_flag",iComePoliceFlag.ToString());
					oParameters.Add("trap_used_flag",iTrapUsedFlag.ToString());
					oParameters.Add("break_item_seq",sBreakItemSeqList);
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
				}
				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_BATTLE_SP,oParameters);
			} else {
				if (iWinFlag.Equals(ViCommConst.FLAG_ON)) {
					
					sRedirectUrl = String.Format(
						"ViewGameFlashBattle.aspx?battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&treasure_complete_flag={3}&break_item_seq={4}&pre_exp={5}&tutorial_flag={6}&pre_friendly_point={7}&get_comp_count_item_seq={8}&get_comp_count_item_count={9}&get_comp_count_item_rec_count={10}&battle_type={11}",
						sBattleLogSeq,
						iLevelUpFlag.ToString(),
						iAddForceCount.ToString(),
						iTreasureCompleteFlag.ToString(),
						sBreakItemSeqList,
						sPreExp,
						iTutorialFlag,
						sFriendlyPoint,
						string.Join("_",sGetCompCountItemSeq),
						string.Join("_",sGetCompCountItemCount),
						sGetCompCountItemSeq.Length.ToString(),
						PwViCommConst.GameBattleType.SINGLE
					);
				} else {
					sRedirectUrl = String.Format(
						"ViewGameFlashBattle.aspx?battle_log_seq={0}&level_up_flag={1}&add_force_count={2}&come_police_flag={3}&trap_used_flag={4}&break_item_seq={5}&pre_exp={6}&battle_type={7}",
						sBattleLogSeq,
						iLevelUpFlag.ToString(),
						iAddForceCount.ToString(),
						iComePoliceFlag.ToString(),
						iTrapUsedFlag.ToString(),
						sBreakItemSeqList,
						sPreExp,
						PwViCommConst.GameBattleType.SINGLE
					);
				}

				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
			}
		} else if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		} else {
			NameValueCollection query = new NameValueCollection();
			query["result"] = sResult;
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_BATTLE_NO_FINISH,query);
		}
	}

	private string GetManTreasureLogCreate(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sGetTreasureLogSeq = null;
		string sStatus = null;
		string sTutorialMissionManTreasureUserSeq = null;
		string sTutorialMissionManTreasureUserCharNo = null;
		string sCastGamePicSeq = null;

		DataSet oDataSetManTreasure;
		using (ManTreasure oManTreasure = new ManTreasure()) {
			oDataSetManTreasure = oManTreasure.GetManTreasureSeqData(this.sessionMan.site.siteCd);
		}

		foreach (DataRow drManTreasure in oDataSetManTreasure.Tables[0].Rows) {
			sTutorialMissionManTreasureUserSeq = drManTreasure["USER_SEQ"].ToString();
			sTutorialMissionManTreasureUserCharNo = drManTreasure["USER_CHAR_NO"].ToString();
			sCastGamePicSeq = drManTreasure["CAST_GAME_PIC_SEQ"].ToString();
		}
		
		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oGetManTreasureLog.GetManTreasureLogCreate(
				sessionMan.site.siteCd,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				sTutorialMissionManTreasureUserSeq,
				sTutorialMissionManTreasureUserCharNo,
				sCastGamePicSeq,
				out sGetTreasureLogSeq,
				out sStatus
			);
		}

		return sGetTreasureLogSeq;
	}

	private string GetGameCharacterType(string sPartnerUserSeq,string sPartnerUserCharNo) {
		string sGameCharacterType = string.Empty;

		GameCharacterBattleSeekCondition oCondition = new GameCharacterBattleSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.PartnerUserSeq = sPartnerUserSeq;
		oCondition.PartnerUserCharNo = sPartnerUserCharNo;

		using (GameCharacterBattle oGameCharacterBattle = new GameCharacterBattle()) {
			DataSet ds = oGameCharacterBattle.GetOneByUserSeq(oCondition);

			if (ds.Tables[0].Rows.Count > 0) {
				sGameCharacterType = ds.Tables[0].Rows[0]["GAME_CHARACTER_TYPE"].ToString();
			}
		}

		return sGameCharacterType;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}
