/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 女性の写真一覧
--	Progaram ID		: ListGameTreasurePartner
--
--  Creation Date	: 2011.07.28
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameTreasurePartner : MobileSocialGameManBase {	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					PwViCommConst.INQUIRY_GAME_TREASURE_PARTNER,
					ActiveForm);
		}
		/**/
	}

}
