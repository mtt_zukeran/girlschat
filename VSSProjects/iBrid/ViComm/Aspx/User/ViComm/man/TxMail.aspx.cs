/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メール送信
--	Progaram ID		: TxMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_TxMail:MobileMailManPage {

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (iBridUtil.GetStringValue(this.Request.QueryString["protect"]).Equals(ViCommConst.FLAG_ON_STR)) {
				this.ProtectRxMail();
			}
			
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			} else {
				DataRow dr;
				string sMailDataSeq = iBridUtil.GetStringValue(Request.QueryString["data"]);
				
				string sUserStatus = string.Empty;
				string sNaFrag = string.Empty;

				if (IsAvailableService(ViCommConst.RELEASE_NON_TITLE_BY_USER_MAIL,2)) {
					lblTitle.Visible = false;
					txtTitle.Visible = false;
				}

				if (sMailDataSeq.Equals(string.Empty)) {
					sMailDataSeq = iBridUtil.GetStringValue(sessionMan.userMan.NextMailDataSeq());
					sessionMan.userMan.mailData.Add(sMailDataSeq,new UserManMailInfo());

					string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
					string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
					int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

					if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo,out dr)) {
						sUserStatus = iBridUtil.GetStringValue(dr["USER_STATUS"]);
						sNaFrag = iBridUtil.GetStringValue(dr["NA_FLAG"]);
					}

					if (dr == null) {
						if (sessionMan.logined) {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
						} else {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.nonUserTopId));
						}
					}

					sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq = iBridUtil.GetStringValue(dr["USER_SEQ"]);
					sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo = iBridUtil.GetStringValue(dr["USER_CHAR_NO"]);
					sessionMan.userMan.mailData[sMailDataSeq].rxUserNm = iBridUtil.GetStringValue(dr["HANDLE_NM"]);
					sessionMan.userMan.mailData[sMailDataSeq].rxLoginId = sLoginId;
					int.TryParse(iBridUtil.GetStringValue(Request.QueryString["quickrequest"]),out sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag);

					if (sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag != 1) {
						using (Cast oCast = new Cast()) {
							if (oCast.IsQuickResponseEntry(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,iBridUtil.GetStringValue(dr["USER_SEQ"]),iBridUtil.GetStringValue(dr["USER_CHAR_NO"]))) {
								sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag = ViCommConst.FLAG_ON;
							} else {
								sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag = ViCommConst.FLAG_OFF;
							}
						}
					}
					
					if (sessionMan.userMan.mailData[sMailDataSeq].quickResMailFlag == 1) {
						lblTitle.Visible = false;
						txtTitle.Visible = false;
						txtTitle.Text = GetErrorMessage(ViCommConst.INFO_STATUS_QUICK_RES_TITLE,false);
					}

					sessionMan.userMan.mailData[sMailDataSeq].chatMailFlag = ViCommConst.FLAG_OFF;

				} else {
					txtTitle.Text = sessionMan.userMan.mailData[sMailDataSeq].mailTitle;
					txtDoc.Text = sessionMan.userMan.mailData[sMailDataSeq].mailDoc;

					sessionMan.SetCastDataSetByUserSeq(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo,1);
					DataRow oDataRow = sessionMan.parseContainer.parseUser.dataTable[ViCommConst.DATASET_CAST].Rows[0];

					sUserStatus = iBridUtil.GetStringValue(oDataRow["USER_STATUS"]);
					sNaFrag = iBridUtil.GetStringValue(oDataRow["NA_FLAG"]);
				}

				if (!sUserStatus.Equals(ViCommConst.USER_WOMAN_NORMAL) ||
							!sNaFrag.Equals(ViCommConst.NaFlag.OK.ToString())) {
					RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
				}

				CheckRefuse(iBridUtil.GetStringValue(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq),iBridUtil.GetStringValue(sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo));

				using (Refuse oRefuse = new Refuse()) {
					if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,iBridUtil.GetStringValue(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq),iBridUtil.GetStringValue(sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo))) {
						RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
					}
				}

				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
				if (sessionMan.userMan.handleNm.Equals("")) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ModifyUserHandleNm.aspx"));
					return;
				}

				int iChargePoint;
				if (!CheckMailBalance(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo,out iChargePoint)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_POINT_LACK_MAIL));
				}

				ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_CONFIRM] != null) {
				cmdConfirm_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_REGIST] != null) {
				cmdRegist_Click(sender,e);
			} else if (Request.Params[PwViCommConst.BUTTON_PRESENT] != null) {
				cmdPresent_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (iBridUtil.GetStringValue(Request.Params["chkpresent"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_ON;
			PresentSelect(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false);
		} else {
			sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_OFF;
			Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false,ViCommConst.SCR_TX_MAIL_COMPLITE_MAN);
		}
	}

	protected void cmdPresent_Click(object sender,EventArgs e) {
		sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = ViCommConst.FLAG_ON;
		PresentSelect(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false);
	}

	protected void cmdConfirm_Click(object sender,EventArgs e) {
		sessionMan.userMan.mailData[iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"])].presentMailFlag = iBridUtil.GetStringValue(Request.Params["chkpresent"]).Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		Confirm(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false);
	}

	protected void cmdRegist_Click(object sender,EventArgs e) {
		MainteDraftMail(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),false);
	}

	private void cmdDelete_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDelTxRx(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,mailSeqList.ToArray());
			}
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("mail_delete_count",mailSeqList.Count.ToString());
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_DELETED_MAIL_HISTORY,oParam);
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   "TxMail.aspx?" + Request.QueryString));
		}
	}

	private void ProtectRxMail() {
		int iProtectFlag;
		int.TryParse(iBridUtil.GetStringValue(this.Request.QueryString["protectflag"]),out iProtectFlag);
		iProtectFlag = iProtectFlag == 1 ? 0 : 1;

		string sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);
		string sListNo = iBridUtil.GetStringValue(this.Request.QueryString["listno"]);
		string sProtectMailSeq = iBridUtil.GetStringValue(this.Request.QueryString["protectmailseq"]);
		using (MailLog oMailLog = new MailLog()) {
			oMailLog.UpdateMailDelProtectFlag(sProtectMailSeq,iProtectFlag);
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   string.Format("TxMail.aspx?scrid=01&stay=1&loginid={0}&userrecno=1&listno={1}#{2}",sLoginId,sListNo,sProtectMailSeq)));
	}
}
