/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 投票券追加(エントリー制)
--	Progaram ID		: RegistAddElectionVoteTicket
--  Creation Date	: 2013.12.12
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistAddElectionVoteTicket:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_ELECTOIN_PERIOD_VOTABLE,this.ActiveForm)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("ListElectionRank.aspx"));
				return;
			}

			pnlInput.Visible = true;
			pnlError.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iAddCount;
		string sAddCount = iBridUtil.GetStringValue(Request.Form["add_count"]);
		string sResult;

		if (!int.TryParse(sAddCount,out iAddCount)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
			oElectionPeriod.AddElectionVoteTicketByPt(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				iAddCount,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.AddVoteTicketByPtResult.RESULT_OK)) {
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("add_count",sAddCount);
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ADD_ELECTION_VOTE_TICKET_COMPLETE,oParam);
			return;
		} else if (sResult.Equals(PwViCommConst.AddVoteTicketByPtResult.RESULT_NG_TERM)) {
			sessionMan.errorMessage = "投票は終了しました";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.AddVoteTicketByPtResult.RESULT_NG_PT_LACK)) {
			sessionMan.errorMessage = "ﾎﾟｲﾝﾄが足りません";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.AddVoteTicketByPtResult.RESULT_NG_PT_MINUS)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}
	}

	protected void cmdTx_Click(object sender,EventArgs e) {
		string sCodeStr = iBridUtil.GetStringValue(Request.Form["code_str"]);
		int iAddCount;
		string sResult;

		if (string.IsNullOrEmpty(sCodeStr)) {
			sessionMan.errorMessage = "投票券ｺｰﾄﾞを入力して下さい";
			pnlInput.Visible = false;
			pnlError.Visible = true;
			return;
		}

		using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
			oElectionPeriod.AddElectionVoteTicketByCd(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCodeStr,
				out iAddCount,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.AddVoteTicketByCodeResult.RESULT_OK)) {
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("add_count",iAddCount.ToString());
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ADD_ELECTION_VOTE_TICKET_COMPLETE,oParam);
			return;
		} else if (sResult.Equals(PwViCommConst.AddVoteTicketByCodeResult.RESULT_NG_TERM)) {
			sessionMan.errorMessage = "投票は終了しました";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.AddVoteTicketByCodeResult.RESULT_NG_CODE)) {
			sessionMan.errorMessage = "投票券ｺｰﾄﾞが正しくありません";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.AddVoteTicketByCodeResult.RESULT_NG_USED)) {
			sessionMan.errorMessage = "このｺｰﾄﾞは既に使用済です";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistAddElectionVoteTicket.aspx"));
		return;
	}
}