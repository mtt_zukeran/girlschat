/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �����X�V
--	Progaram ID		: MemoMainte
--
--  Creation Date	: 2010.12.03
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_MemoMainte:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!this.IsPostBack) {
			if (!sessionMan.logined) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByTermIdCast.aspx"));
				}
			}

			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			DataRow dr;
			if (!sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),sCastCharNo,1,out dr)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.site.userTopId));
			}

			txtMemoDoc.Text = sessionMan.GetCastValue("MEMO_DOC");
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_DELETE] != null) {
				cmdDelete_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sReviews = string.Empty;
		bool bOk = true;
		this.lblErrorMessage.Text = string.Empty;
		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
		DataRow dr;
		if (!sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),sCastCharNo,1,out dr)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.site.userTopId));
		}

		if (this.Request.Form["reviews"] != null) {
			sReviews = iBridUtil.GetStringValue(this.Request.Form["reviews"]);
			if (sReviews.Equals(string.Empty)) {
				bOk = false;
				lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_REVIEWS);
			}
		}

		if (bOk & CheckOther()) {
			string sMemoDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtMemoDoc.Text));
			sMemoDoc = Mobile.GetInputValue(sMemoDoc,4000,System.Text.Encoding.UTF8);

			using (Memo oMemo = new Memo()) {
				oMemo.MemoMainte(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sessionMan.GetCastValue("USER_SEQ"),
					sessionMan.GetCastValue("USER_CHAR_NO"),
					sReviews,
					sMemoDoc,
					ViCommConst.FLAG_OFF);
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_MODIFY_MEMO));
		}
	}

	protected void cmdDelete_Click(object sender,EventArgs e) {
		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
		DataRow dr;
		if (!sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),sCastCharNo,1,out dr)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.site.userTopId));
		}
		using (Memo oMemo = new Memo()) {
			oMemo.MemoMainte(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sessionMan.GetCastValue("USER_SEQ"),
				sessionMan.GetCastValue("USER_CHAR_NO"),
				string.Empty,
				string.Empty,
				ViCommConst.FLAG_ON);
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl("DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_DELETE_MEMO));
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
