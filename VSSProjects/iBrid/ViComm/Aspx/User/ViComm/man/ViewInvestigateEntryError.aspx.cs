/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: この娘を探せメール受信設定エラー表示
--	Progaram ID		: ViewInvestigateEntryError
--
--  Creation Date	: 2015.03.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewInvestigateEntryError:MobileManPageBase {
	private const int MASK_ERROR_MAIL_ADDR_NG		= 1;
	private const int MASK_ERROR_MAIL_RX_TIME		= 2;
	private const int MASK_ERROR_INFO_MAIL_RX_TYPE	= 4;
	private const int MASK_ERROR_CAST_MAIL_RX_TYPE	= 8;
	
	private const string SUB_TYPE_ENTRY		= "1";
	private const string SUB_TYPE_CAUGHT	= "2";
	private const string SUB_TYPE_ACQUIRED	= "3";

	virtual protected void Page_Load(object sender,EventArgs e) {
		int iMask = 0;

		if (int.TryParse(iBridUtil.GetStringValue(this.Request.QueryString["err"]),out iMask)) {
			DisplayError(iMask);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		if (!IsPostBack) {
		}
	}
	
	private void DisplayError(int iMask) {
		sessionMan.errorMessage = string.Empty;
		tagErrMailAddrNg.Visible = false;
		tagErrMailAddrOk.Visible = false;
		tagErrRxMailTime.Visible = false;
		tagErrInfoMailRxType.Visible = false;
		tagErrCastMailRxType.Visible = false;
		
		if ((MASK_ERROR_MAIL_ADDR_NG & iMask) > 0) {
			tagErrMailAddrNg.Visible = true;
		} else {
			tagErrMailAddrOk.Visible = true;
		}

		if ((MASK_ERROR_MAIL_RX_TIME & iMask) > 0) {
			tagErrRxMailTime.Visible = true;
		}

		if ((MASK_ERROR_INFO_MAIL_RX_TYPE & iMask) > 0) {
			tagErrInfoMailRxType.Visible = true;
		}

		if ((MASK_ERROR_CAST_MAIL_RX_TYPE & iMask) > 0) {
			tagErrCastMailRxType.Visible = true;
		}
		
		switch (iBridUtil.GetStringValue(this.Request.QueryString["subtype"])) {
			case SUB_TYPE_ENTRY:
				sessionMan.errorMessage = "ｴﾝﾄﾘｰできません";
				break;
			case SUB_TYPE_CAUGHT:
				sessionMan.errorMessage = "発見ｽﾀﾝﾌﾟを押せません。";
				break;
			case SUB_TYPE_ACQUIRED:
				sessionMan.errorMessage = "ﾎﾟｲﾝﾄを受け取れません。";
				break;
			default:
				break;
		}
	}
}