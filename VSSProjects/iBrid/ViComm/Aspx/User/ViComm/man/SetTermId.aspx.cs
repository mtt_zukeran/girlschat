/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 固体識別設定
--	Progaram ID		: SetTermId
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
//<a href="SetTermId.aspx?guid=on" utn="">
//  簡単ﾛｸﾞｲﾝ設定
//</a>

using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_SetTermId:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!iBridUtil.GetStringValue(Request.QueryString["release"]).Equals("1")) {
				ParseHTML oParseMan = sessionMan.parseContainer;
				oParseMan.parseUser.postAction = ViCommConst.POST_ACT_GUID;
				if (sessionMan.carrier.Equals(ViCommConst.DOCOMO) && sessionMan.getTermIdFlag) {
					oParseMan.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
				}
			} else {
				ReleaseTermId();
			}
			cmdSubmit_Click(sender,e);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string siModeId = "",sTermId = "";
		bool bNotExist = false;

		siModeId = Mobile.GetiModeId(sessionMan.carrier,Request);
		sTermId = Mobile.GetUtn(sessionMan.carrier,Request);

		ParseHTML oParseMan = sessionMan.parseContainer;
		oParseMan.parseUser.postAction = 0;
		sessionMan.errorMessage = string.Empty;

		if (sessionMan.getTermIdFlag) {
			if (sTermId.Equals(string.Empty)) {
				sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_SET_TERM_ID_MAN));
			}
		} else {
			if (siModeId.Equals(string.Empty)) {
				sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_SET_TERM_ID_MAN));
			}
		}

		using (Man oMan = new Man()) {
			bNotExist = oMan.IsNotEixstTermId(sTermId,siModeId,sessionMan.userMan.siteCd,sessionMan.userMan.userSeq);
		}
		if (bNotExist == false) {
			sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST_BY_SET);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_SET_TERM_ID_MAN));
		}
		sessionMan.userMan.SetupTermId(sessionMan.userMan.userSeq,sTermId,siModeId);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SET_TERM_ID_COMPLITE_MAN));
	}

	protected void ReleaseTermId() {
		sessionMan.userMan.SetupTermId(sessionMan.userMan.userSeq,"","");
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SET_TERM_ID_COMPLITE_MAN));
	}
}
