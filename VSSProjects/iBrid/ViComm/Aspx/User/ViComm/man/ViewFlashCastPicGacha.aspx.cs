/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ お宝ガチャ用Flash表示
--	Progaram ID		: ViewFlashFlashCastPicGacha
--
--  Creation Date	: 2013.01.14
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.IO;
using System.Drawing;
using System.Data;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;

public partial class ViComm_man_ViewFlashCastPicGacha:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else if (sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		} else {
			string sNextUrl = iBridUtil.GetStringValue(this.Request.QueryString["next_url"]);
			string sPicSeq = iBridUtil.GetStringValue(this.Request.QueryString["pic_seq"]);
			string sWinFlag = iBridUtil.GetStringValue(this.Request.QueryString["win_flag"]);
			string sWebPhisicalDir = sessionMan.site.webPhisicalDir;
			string sFlashDir = Path.Combine(sWebPhisicalDir,PwViCommConst.FLASH_DIR);
			string sXmlPath = Path.Combine(sFlashDir,"get_photo.xml");
			string sObjPhotoImgPath = string.Empty;
			string sObjPicDoc = string.Empty;

			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				if (!oObjUsedHistory.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.ATTACH_PIC_INDEX.ToString(),sPicSeq)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
			}

			using (CastPic oCastPic = new CastPic()) {
				DataSet oDataSet = oCastPic.GetPageCollectionBySeq(sPicSeq);

				if (oDataSet.Tables[0].Rows.Count > 0) {
					sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"]));
					sObjPicDoc = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PIC_TITLE"]);
				}
			}

			string sXmlData = string.Empty;
			string sPhotoImgBase64 = string.Empty;
			string sObjPhotoImgBase64 = string.Empty;

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!File.Exists(sObjPhotoImgPath)) {
					sObjPhotoImgPath = Path.Combine(sWebPhisicalDir,PwViCommConst.NoPicJpgPath);
				}

				//XMLファイル読み込み
				using (StreamReader srXml = new StreamReader(sXmlPath)) {
					sXmlData = srXml.ReadToEnd();
				}
				
				//お宝写真読み込み
				byte[] bufObjPhotoImg = FileHelper.GetFileBuffer(sObjPhotoImgPath);
				sObjPhotoImgBase64 = Convert.ToBase64String(bufObjPhotoImg);
				
				//コメントの改行コード変換
				sObjPicDoc = sObjPicDoc.Replace("\r\n","&#13;");

				sXmlData = sXmlData.Replace("%IMAGE%",sObjPhotoImgBase64);
				sXmlData = sXmlData.Replace("%COMMENT%",sObjPicDoc);
			}

			string sTempXml = Path.GetTempFileName();
			string sTempSwf = Path.GetTempFileName();

			StreamWriter sw = new StreamWriter(sTempXml,false,Encoding.UTF8);
			sw.Write(sXmlData);
			sw.Close();

			using (Process prExe = new Process()) {
				prExe.StartInfo.FileName = "D:\\swfmill\\swfmill.exe";
				prExe.StartInfo.Arguments = string.Format("-e cp932 xml2swf {0} {1}",sTempXml,sTempSwf);
				prExe.StartInfo.CreateNoWindow = true;//コンソールウィンドウを開かない
				prExe.StartInfo.UseShellExecute = false;//シェル機能を使用しない
				prExe.Start();
				prExe.WaitForExit(3000);
			}

			byte[] bufTempSwf = FileHelper.GetFileBuffer(sTempSwf);
			File.Delete(sTempXml);
			File.Delete(sTempSwf);

			IDictionary<string,string> oParam = this.CreateParam(this.Request.QueryString);

			byte[] oSwfBuffer = GameFlashLiteHelper.RewriteSwfBuffer(bufTempSwf,oParam);
			GameFlashLiteHelper.OutputSwf(oSwfBuffer);
		}
	}

	private IDictionary<string,string> CreateParam(NameValueCollection pQueryString) {
		IDictionary<string,string> oParam = new Dictionary<string,string>();
		string sNextUrl = iBridUtil.GetStringValue(pQueryString["next_url"]);

		oParam.Add("nextUrl",sNextUrl);

		return oParam;
	}
}