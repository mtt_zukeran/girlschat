/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�{�����l�I��
--	Progaram ID		: RegistGameMostSweetHeart.aspx
--
--  Creation Date	: 2012.10.03
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameMostSweetHeart:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sSiteCd = this.sessionMan.site.siteCd;
			string sUserSeq = this.sessionMan.userMan.userSeq;
			string sUserCharNo = this.sessionMan.userMan.userCharNo;
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
			string sOutFlag = iBridUtil.GetStringValue(Request.QueryString["out_flag"]);
			string sResult = string.Empty;
			
			if(string.IsNullOrEmpty(sOutFlag)) {
				sOutFlag = ViCommConst.FLAG_OFF_STR;
			}
			
			using (MostSweetHeart oMostSweetHeart = new MostSweetHeart()) {
				oMostSweetHeart.RegistMostSweetHeart(sSiteCd,sUserSeq,sUserCharNo,sPartnerUserSeq,sPartnerUserCharNo,sOutFlag,out sResult);
			}
			
			if (sResult.Equals(PwViCommConst.RegistMostSweetHeartResult.RESULT_OK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"ListFindGameCharacter.aspx?scrid=01&relation=1&sort=3"));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}
}
