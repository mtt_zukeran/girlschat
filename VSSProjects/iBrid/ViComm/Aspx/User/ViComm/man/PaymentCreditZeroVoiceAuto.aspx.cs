/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｸﾚｼﾞｯﾄ音声ガイダンス支払(ZEROｸﾚｼﾞｯﾄ)自動決済
--	Progaram ID		: PaymentCreditZeroVoiceAuto
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_PaymentCreditZeroVoiceAuto:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sessionMan.userMan.tel)) {
				txtTelNo.Text = sessionMan.userMan.tel;
				pnlInputTelNo.Visible = false;
			}

			string sCreditAutoSettleSeq = iBridUtil.GetStringValue(Request.QueryString["settleseq"]);
			DataSet ds;
			using (CreditAutoSettle oCreditAutoSettle = new CreditAutoSettle()) {
				ds = oCreditAutoSettle.GetOne(sessionMan.site.siteCd,sCreditAutoSettleSeq);
			}
			if (ds.Tables[0].Rows.Count > 0) {
				sessionMan.userMan.settleRequestAmt = int.Parse(ds.Tables[0].Rows[0]["SETTLE_AMT"].ToString());
				sessionMan.userMan.settleRequestPoint = int.Parse(ds.Tables[0].Rows[0]["SETTLE_POINT"].ToString());
				int iServicePoint = 0;
				using (Pack oPack = new Pack()) {
					iServicePoint = oPack.CalcServicePointCreditAutoSettle(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.settleRequestAmt);
				}
				sessionMan.userMan.settleServicePoint = iServicePoint;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdCancel"] != null) {
				cmdCancel_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iSettleAmt = 0;
		string sSid = "";
		string sResult = string.Empty;
		string sCreditAutoSettleSeq = iBridUtil.GetStringValue(this.Request.QueryString["settleseq"]);
		string sSalesAmt;

		if (!iBridUtil.GetStringValue(this.Request.Form["agreement"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_CREDIT_AUTO_SETTLE_NO_AGREEMENT);
			return;
		}

		sessionMan.errorMessage = string.Empty;

		if (pnlInputTelNo.Visible) {
			if (string.IsNullOrEmpty(txtTelNo.Text)) {
				sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
				return;
			} else {
				if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTelNo.Text)) {
					sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
					return;
				} else {
					using (Man oMan = new Man()) {
						if (oMan.IsTelephoneRegistered(sessionMan.site.siteCd,txtTelNo.Text)) {
							sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
							return;
						}
					}
				}
			}
		}

		using (CreditAutoSettleStatus oCreditAutoSettleStatus = new CreditAutoSettleStatus()) {
			oCreditAutoSettleStatus.RegistCreditAutoSettle(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCreditAutoSettleSeq,
				"30",
				out iSettleAmt,
				out sSid,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_NG)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		string sSettleUrl = "";
		sSalesAmt = iSettleAmt.ToString();

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			string sSettleType = sessionMan.userMan.billAmt == 0 ? ViCommConst.SETTLE_CREDIT_PACK : ViCommConst.SETTLE_CREDIT;

			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,sSettleType)) {
				sSettleUrl = string.Format(oSiteSettle.subSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,txtTelNo.Text);
			}
		}

		string sAuthTelNo;

		if (TransVoice(sSettleUrl,out sAuthTelNo)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"PaymentCreditZeroVoiceAuth.aspx?authtelno=" + sAuthTelNo));
		} else {
			sessionMan.errorMessage = "通信に失敗しました。";
			return;
		}

	}

	public bool TransVoice(string pUrl,out string pAuthTelNo) {
		pAuthTelNo = string.Empty;
		try {
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();

				Match oMatch = Regex.Match(sRes,@"03\d{8}");
				pAuthTelNo = oMatch.Value;

				return Regex.IsMatch(sRes,"(s|S)uccessOK");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransVoice",pUrl);
			return false;
		}
	}

	private bool OnRemoteCertificateValidationCallback(
	  Object sender,
	  X509Certificate certificate,
	  X509Chain chain,
	  SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}

	protected void cmdCancel_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionMan.GetNavigateUrl("ListCreditAutoSettle.aspx"));
	}
}
