/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・仲間申請中一覧
--	Progaram ID		: ListGameCharacterFellowApplication.aspx
--
--  Creation Date	: 2011.08.08
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameCharacterFellowApplication:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_FELLOW_APPLICATION_LIST,ActiveForm);
	}
}
