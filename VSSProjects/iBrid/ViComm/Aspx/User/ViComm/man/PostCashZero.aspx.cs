/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ZERO Cash Post���M
--	Progaram ID		: PostCashZero
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PostCashZero:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			clientip.Text = string.Format("<input type=\"hidden\" name=\"clientip\" value=\"{0}\"></input>",Request.QueryString["clientip"]);
			act.Text = string.Format("<input type=\"hidden\" name=\"act\" value=\"{0}\"></input>","mobile_order");
			money.Text = string.Format("<input type=\"hidden\" name=\"money\" value=\"{0}\"></input>",Request.QueryString["money"]);
			telno.Text = string.Format("<input type=\"hidden\" name=\"telno\" value=\"{0}\"></input>",Request.QueryString["telno"]);
			email.Text = string.Format("<input type=\"hidden\" name=\"email\" value=\"{0}\"></input>",Request.QueryString["email"]);
			sendid.Text = string.Format("<input type=\"hidden\" name=\"sendid\" value=\"{0}\"></input>",Request.QueryString["sendid"]);
			sendpoint.Text = string.Format("<input type=\"hidden\" name=\"sendpoint\" value=\"{0}\"></input>",Request.QueryString["sendpoint"]);
			success_url.Text = string.Format("<input type=\"hidden\" name=\"siteurl\" value=\"{0}\"></input>",Request.QueryString["siteurl"]);
			success_str.Text = string.Format("<input type=\"hidden\" name=\"sitestr\" value=\"{0}\"></input>",Request.QueryString["sitestr"]);

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
	}
}
