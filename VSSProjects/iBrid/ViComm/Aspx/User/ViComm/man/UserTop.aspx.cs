/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: トップメニュー
--	Progaram ID		: UserTop
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_UserTop:MobileManPageBase {

	protected override bool NeedAccessLog {
		get {
			return true;
		}
	}

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!sessionMan.logined) {
			if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
			}
			return;
		}

		// ｷｬｽﾄ用のランダムカーソルをずらす
		sessionObj.RefreshRandomNum4Cast();

		using (User oUser = new User()) {
			oUser.UpdateMobileInfo(sessionMan.userMan.userSeq,sessionMan.carrier,sessionMan.mobileUserAgent);
		}

		if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || (sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionMan.adminFlg)) {
			HttpCookie cookie = new HttpCookie("maqia");
			cookie.Values["maqiauid"] = sessionMan.userMan.userSeq;
			cookie.Values["maqiapw"] = sessionMan.userMan.loginPassword;
			cookie.Values["maqiasex"] = ViCommConst.MAN;
			cookie.Expires = DateTime.Now.AddDays(90);
			cookie.Domain = sessionMan.site.hostNm;
			Response.Cookies.Add(cookie);
		}

		if (!IsPostBack) {
			// ﾛｸﾞｲﾝﾛｸﾞを書き込む
			if (!sessionMan.alreadyWrittenLoginLog) {
				this.AccessPage(ViCommConst.ProgramAction.LOGIN);
				sessionMan.alreadyWrittenLoginLog = true;
			}

			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			CheckRegistIncomplete();
			CheckBulkImpUser();
			switch (sessionMan.site.topPageSeekMode) {
				case ViCommConst.CAST_TOP_PICKUP:
					string sTopPagePickupId = string.Empty;
					using (Pickup oPickup = new Pickup()) {
						sTopPagePickupId = oPickup.GetTopPagePickupId(sessionMan.site.siteCd);
					}
					ulong uMode;
					if (sTopPagePickupId.Equals(string.Empty)) {
						uMode = ViCommConst.INQUIRY_CAST_PICKUP;
					} else {
						uMode = ViCommConst.INQUIRY_RECOMMEND;
					}
					sessionMan.ControlList(
							Request,
							uMode,
							ActiveForm);

					break;
				case ViCommConst.CAST_TOP_TALK_MOVIE_PICKUP:
					GetPickupMovie();
					break;
			}

			// 足あとの未確認件数を初期化
			sessionMan.userMan.characterEx.unconfirmMarkingCount = string.Empty;
		}
	}

	private void GetPickupMovie() {
		using (TalkMovie oTalkMovie = new TalkMovie()) {
			string sUserSeq,sUserCharNo;
			string sRecNum;
			if (oTalkMovie.GetRandomMovie(sessionMan.site.siteCd,out sUserSeq,out sUserCharNo,out sRecNum) == 0) {
				return;
			}

			if (sessionMan.SetCastDataSetByUserSeq(sUserSeq,sUserCharNo,int.Parse(sRecNum))) {
				sessionMan.seekMode = ViCommConst.INQUIRY_TALK_MOVIE_PICKUP.ToString();
			}
		}
	}

	private void CheckRegistIncomplete() {
		if (sessionMan.userMan.registIncompleteFlag == 1 && !sessionMan.site.siteCd.Substring(1,3).Equals("000")) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserProfile.aspx"));
		}
	}

	private void CheckBulkImpUser() {
		if (sessionMan.userMan.bulkImpNonUseFlag == 1) {
			if (sessionMan.site.impManFirstLoginAct.Equals(ViCommConst.ImpFirstLoginActType.USER_TOP)) {
				sessionMan.ActivateImpUser(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sessionMan.carrier,sessionMan.mobileUserAgent,sessionMan.currentIModeId);
			} else if (sessionMan.site.impManFirstLoginAct.Equals(ViCommConst.ImpFirstLoginActType.MODIFY_HANDLE_NM)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserHandleNm.aspx"));
			} else if (sessionMan.site.impManFirstLoginAct.Equals(ViCommConst.ImpFirstLoginActType.MODIFY_PROFILE)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserProfile.aspx"));
			}
		}
	}
}
