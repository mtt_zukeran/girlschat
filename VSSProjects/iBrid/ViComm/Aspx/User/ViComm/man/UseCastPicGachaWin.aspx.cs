/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝ガチャ当たり使用
--	Progaram ID		: UseCastPicGachaWin
--
--  Creation Date	: 2013.02.07
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_UseCastPicGachaWin:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		string sNextPage = iBridUtil.GetStringValue(this.Request.QueryString["nextpage"]);
		string sResult = string.Empty;
		
		using (CastPicGachaWin oCastPicGachaWin = new CastPicGachaWin()) {
			oCastPicGachaWin.UseCastPicGachaWin(sessionMan.site.siteCd,sessionMan.userMan.userSeq,out sResult);
		}
		
		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sNextPage));
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}