<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentCVSDirect.aspx.cs" Inherits="ViComm_man_PaymentCVSDirect" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<mobile:SelectionList ID="lstPack" Runat="server"></mobile:SelectionList>
		$PGM_HTML02;
		<mobile:SelectionList ID="lstCVS" Runat="server"></mobile:SelectionList>
		$PGM_HTML03;
		<cc1:iBMobileTextBox ID="txtFamilyNm" runat="server" MaxLength="10" Size="8"></cc1:iBMobileTextBox>
		$PGM_HTML04;
		<cc1:iBMobileTextBox ID="txtGivenNm" runat="server" MaxLength="10" Size="8"></cc1:iBMobileTextBox>
		$PGM_HTML05;
		<cc1:iBMobileTextBox ID="txtAddr" runat="server" MaxLength="50" Size="20"></cc1:iBMobileTextBox>
		$PGM_HTML06;
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true"></cc1:iBMobileTextBox>
		$PGM_HTML07;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
