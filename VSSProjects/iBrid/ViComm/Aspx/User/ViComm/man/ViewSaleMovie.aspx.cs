/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �̔�����ڍ�
--	Progaram ID		: ViewSaleMovie
--
--  Creation Date	: 2010.07.23
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewSaleMovie:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			int iChargePoint = 0;
			string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			string sMovieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			bool bDownload = false;

			if (!sMovieSeq.Equals("")) {
				bDownload = Download(sCastSeq,sCastCharNo,sMovieSeq,iChargePoint);
			}

			if (bDownload == false) {

				ParseHTML oParseMan = sessionMan.parseContainer;

				int iRecNo;
				int.TryParse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]),out iRecNo);
				if (iRecNo == 0) {
					iRecNo = 1;
				}

				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

				ulong uSeekMode;
				bool bControlList;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
					bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
				} else {
					bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_SALE_MOVIE,ActiveForm);
				}
				if (bControlList) {
					sessionMan.SetCastDataSetByLoginId(sessionMan.GetSalesMovieValue("LOGIN_ID"),sCastCharNo,iRecNo);
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListSaleMovie.aspx"));
				}
			}
		}
	}

	private bool Download(string pCastSeq,string pCastCharNo,string pMovieSeq,int pChargePoint) {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,true,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				MovieHelper.ResponseMovie(sLoginId, sFileNm, sFullPath, lSize, ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}
}
