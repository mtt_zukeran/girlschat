﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザー掲示板書込詳細
--	Progaram ID		: ViewUserBbsConf
--
--  Creation Date	: 2010.04.23
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewUserBbsConf:MobileManPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				return;
			} else {
				if (sessionMan.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListUserBbsConf.aspx"));
				}
			}
			if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
				cmdSubmit_Click(sender,e);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("ListUserBbsConf.aspx"));
			}
		}
	}

	virtual protected void cmdSubmit_Click(object sender,EventArgs e) {
		using (UserManBbs oUserManBbs = new UserManBbs()) {
			oUserManBbs.DeleteBbs(sessionMan.site.siteCd,
				sessionMan.GetManBbsDocValue("USER_SEQ"),
				sessionMan.GetManBbsDocValue("USER_CHAR_NO"),
				sessionMan.GetManBbsDocValue("BBS_SEQ")
			);
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETE_USER_BBS_COMPLETE) + "&site=" + sessionMan.site.siteCd);
	}
}
