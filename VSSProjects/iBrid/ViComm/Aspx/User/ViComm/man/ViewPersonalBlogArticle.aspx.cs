/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�ʃu���O���e �ڍ�
--	Progaram ID		: ViewPersonalBlogArticle
--
--  Creation Date	: 2011.04.11
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
public partial class ViComm_man_ViewPersonalBlogArticle : MobileBlogManBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["blog_article_seq"]));
		if (!rgxMatch.Success) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!sessionObj.IsImpersonated) {
			using (BlogArticle oBlogArticle = new BlogArticle()) {
				oBlogArticle.UpdateReadingCount(
					sessionMan.site.siteCd,
					null,
					null,
					null,
					Request.QueryString["blog_article_seq"]
				);
			}
		}

		sessionMan.ControlList(Request,ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE,ActiveForm);
	}
}
