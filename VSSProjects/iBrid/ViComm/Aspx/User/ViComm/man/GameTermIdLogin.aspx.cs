/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・固体識別番号ログイン
--	Progaram ID		: GameTermIdLogin
--
--  Creation Date	: 2011.11.25
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_GameTermIdLogin:MobileManPageBase {
	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
				string sLoginId = string.Empty;
				string sLoginPassword = string.Empty;
				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCookie"]).Equals(ViCommConst.FLAG_ON_STR)) {
					if (Request.Cookies["maqia"] != null) {
						using (User oUser = new User()) {
							if (oUser.GetOne(iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiauid"]),iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiasex"]))) {
								sLoginId = oUser.loginId;
								sLoginPassword = oUser.loginPassword;
							}
						}
					}
					if (string.IsNullOrEmpty(sLoginId) || string.IsNullOrEmpty(sLoginPassword)) {
						if (IsAvailableService(ViCommConst.RELEASE_ENABLE_EASYLOGIN_ERROR,2)) {
							RedirectToDisplayDoc(ViCommConst.SCR_MAN_SMART_PHONE_EASYLOGIN_NG);
						}
					}
				}
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("GameLoginUser.aspx?loginid={0}&password={1}",sLoginId,sLoginPassword)));
			} else {
				string sId = Mobile.GetiModeId(sessionMan.carrier,Request);
				string sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("GameLoginUser.aspx?utn={0}&id={1}",sUtn,sId)));
			}
		}
	}
}
