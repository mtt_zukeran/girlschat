/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログコメント 一覧
--	Progaram ID		: ListBlogComment
--
--  Creation Date	: 2012.12.27
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListBlogComment:MobileBlogManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.IsImpersonated == false) {
				if (sessionMan.logined == false) {
					if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
					}
					return;
				}
			}

			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_BLOG_COMMENT,ActiveForm);
		}
	}
}
