/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品動画 一覧
--	Progaram ID		: ListProdMov
--
--  Creation Date	: 2010.12.14
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdMov : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		sessionMan.ControlList(
				this.Request,
				ViCommConst.INQUIRY_PRODUCT_MOVIE,
				this.ActiveForm);
	}
	

}
