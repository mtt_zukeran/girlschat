/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品写真購入確認
--	Progaram ID		: ConfirmProdPic
--
--  Creation Date	: 2010.12.19
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_ConfirmProdPic : MobileProdManPage {

	protected void Page_Load(object sender, EventArgs e) {
		string sAction = iBridUtil.GetStringValue(this.Request.Params["action"]);
		string sProductId = iBridUtil.GetStringValue(this.Request.Params["product_id"]);
		string sAdultFlag = iBridUtil.GetStringValue(this.Request.Params["adult"]);
		string sProductType = ProductHelper.GetMovieProductType(sAdultFlag);
		ulong lSeekMode = ulong.Parse(this.Request.QueryString["seekmode"]);
		if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			lSeekMode = ViCommConst.INQUIRY_PRODUCT_PIC;
		}

		string sProductSeq = null;
		using (Product oProduct = new Product()) {
			sProductSeq = oProduct.ConvertProductId2Seq(sProductId);
		}

		if (ProductHelper.IsAlreadyUsed(sProductSeq, sProductType)) {
			this.Redirect2DownloadProdPic();
		}

		if (sAction.Equals(ViCommConst.BUTTON_ACTION_PURHASE)) {
			if (!ProductHelper.Purchase(sProductSeq, sProductType)) {
				// 残ﾎﾟｲﾝﾄ不足の場合はエラー画面へ
				base.RedirectToDisplayDoc(ViCommConst.ERR_FRAME_PURCHASE_PRODUCT_PIC,this.Request.QueryString);
			}
			this.Redirect2DownloadProdPic();
		} else {
			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
			if (!this.IsPostBack) {
				sessionMan.ControlList(this.Request, lSeekMode, this.ActiveForm);
			}
		}
	}

	/// <summary>
	/// ﾀﾞｳﾝﾛｰﾄﾞ画面へ遷移
	/// </summary>
	private void Redirect2DownloadProdPic() {
		UrlBuilder oUrlBuilder = new UrlBuilder("DownloadProdPic.aspx", this.Request.QueryString);
		if (oUrlBuilder.Parameters.ContainsKey(ViCommConst.ACTION)) {
			oUrlBuilder.Parameters.Remove(ViCommConst.ACTION);
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
	}
}
