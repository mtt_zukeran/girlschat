/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性待機開始
--	Progaram ID		: StartWaiting
--
--  Creation Date	: 2010.06.03
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_StartWaiting:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		bool bWaitEnd = false;

		if (!IsPostBack) {

			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			} else {
				//所持ポイントを会話できるほどもっていない場合、エラーページへリダイレクト
				if (CheckCanWaiting() == false) {
					RedirectToDisplayDoc(ViCommConst.SCR_NO_BALANCE_WAITING_MAN);
					return;
				}

				bool bNoTel = false;

				if (string.IsNullOrEmpty(sessionMan.userMan.tel)) {
					if (sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
						if (!IsAvailableService(ViCommConst.RELEASE_VOICEAPP,2)) {
							bNoTel = true;
						}
					} else {
						bNoTel = true;
					}
				}

				if (bNoTel) {
					RedirectToDisplayDoc(ViCommConst.ERR_FRAME_NO_TEL);
					return;
				}

				using (Recording oRecording = new Recording()) {
					txtWaitComment.Text = oRecording.GetLastComment(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,"","","");
					if (oRecording.WaitingNow(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,"","","")) {
						lstWaitTime.Items.Add(new MobileListItem("待機終了","-1"));
						bWaitEnd = true;
					}
				}
				if (!bWaitEnd) {
					using (CodeDtl oCodeDtl = new CodeDtl()) {
						DataSet ds;
						ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_MAN_WAIT_TIME);
						foreach (DataRow dr in ds.Tables[0].Rows) {
							lstWaitTime.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
						}

						ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_WAIT_TYPE);
						int iIdx = 0;
						bool bOk;

						foreach (DataRow dr in ds.Tables[0].Rows) {
							bOk = true;
							if (sessionMan.carrier.Equals(ViCommConst.KDDI) || sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
								if (sessionMan.userMan.useCrosmileFlag == ViCommConst.FLAG_OFF) {
									if (dr["CODE"].Equals(ViCommConst.WAIT_TYPE_BOTH) || dr["CODE"].Equals(ViCommConst.WAIT_TYPE_VIDEO)) {
										bOk = false;
									}
								}
							}
							if (bOk) {
								lstConnectType.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
							}
							iIdx++;
						}
					}
				} else {
					//待機終了 
					sessionMan.userMan.StartWaiting(DateTime.Now,"","",ViCommConst.FLAG_ON,Session.SessionID);
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_WAITING_END));
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	//待機開始 or 終了 
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;
		lblErrorMessage.Text = string.Empty;

		//ﾊﾝﾄﾞﾙ名未設定の場合待機不可
		if (sessionMan.userMan.handleNm.Equals("")) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ModifyUserHandleNm.aspx"));
			return;
		}
		//所持ポイントを会話できるほどもっていない場合、エラーページへリダイレクト
		if (CheckCanWaiting() == false) {
			RedirectToDisplayDoc(ViCommConst.SCR_NO_BALANCE_WAITING_MAN);
			return;
		}
		
		//ｺﾒﾝﾄに半角の$ﾏｰｸがある場合ｴﾗｰﾒｯｾｰｼﾞ表示
		if (txtWaitComment.Text.IndexOf("$") != -1) {
			bOk = false;
			txtWaitComment.Text = txtWaitComment.Text.Replace("$","");
			lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_INPUT_DOLLAR_MARK);
		}

		//ｺﾒﾝﾄにNGﾜｰﾄﾞがある場合ｴﾗｰﾒｯｾｰｼﾞ表示
		string sNGWord;
		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			if (sessionMan.ngWord.VaidateDoc(txtWaitComment.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		if ((bOk & this.CheckOther()) == false) {
			return;
		}

		int iWaitMinutes;

		if (!ModifyProfile()) {
			return;
		}

		if (lstWaitTime.Items[lstWaitTime.SelectedIndex].Value.Equals("-1")) {
			sessionMan.userMan.StartWaiting(DateTime.Now,"","",ViCommConst.FLAG_ON,Session.SessionID);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_WAITING_END));
		} else {
			if (sessionMan.carrier.Equals(ViCommConst.DOCOMO) || sessionMan.carrier.Equals(ViCommConst.KDDI) || sessionMan.carrier.Equals(ViCommConst.SOFTBANK)) {
				if (sessionMan.userMan.useCrosmileFlag.Equals(ViCommConst.FLAG_ON)) {
					sessionMan.userMan.SetupCrosmile(sessionMan.userMan.userSeq,false);
				}

				if (sessionMan.userMan.useVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
					sessionMan.userMan.SetupVoiceapp(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.FLAG_OFF);
				}
			}

			bool bNoTel = false;

			if (lstConnectType.Items[lstConnectType.SelectedIndex].Value.Equals(ViCommConst.WAIT_TYPE_VOICE) || lstConnectType.Items[lstConnectType.SelectedIndex].Value.Equals(ViCommConst.WAIT_TYPE_BOTH)) {
				if (string.IsNullOrEmpty(sessionMan.userMan.tel)) {
					if (sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
						if (IsAvailableService(ViCommConst.RELEASE_VOICEAPP,2) && !IsAvailableService(ViCommConst.RELEASE_VOICEAPP_ONLY_DIAL,2)) {
							if (sessionMan.userMan.useVoiceappFlag.Equals(ViCommConst.FLAG_OFF)) {
								bNoTel = true;
							}
						} else {
							bNoTel = true;
						}
					} else {
						bNoTel = true;
					}
				}
			}

			if (bNoTel) {
				RedirectToDisplayDoc(ViCommConst.ERR_FRAME_NO_TEL);
				return;
			}

			int.TryParse(lstWaitTime.Items[lstWaitTime.SelectedIndex].Value,out iWaitMinutes);

			sessionMan.userMan.StartWaiting(
				DateTime.Now.Add(TimeSpan.FromMinutes(iWaitMinutes)),
				 HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtWaitComment.Text)),
				lstConnectType.Items[lstConnectType.SelectedIndex].Value,
				ViCommConst.FLAG_OFF,
				Session.SessionID
			);
			if (sessionMan.userMan.useCrosmileFlag == ViCommConst.FLAG_ON) {
				string sLastVer = string.Empty;
				sessionMan.GetCrosmileSipUriByRegistKey(sessionMan.userMan.crosmileRegistKey,out sessionMan.userMan.crosmileUri,out sLastVer);
				sessionMan.ModifyUserCrosmileUri(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAN,sessionMan.userMan.crosmileUri,string.Empty,sLastVer);
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_WAITING_START));
		}
	}

	private bool ModifyProfile() {
		List<string> attrTypeSeqList = new List<string>();
		List<string> attrSeqList = new List<string>();
		List<string> attrInputValue = new List<string>();
		string sResult = "";

		foreach (UserManAttr attr in sessionMan.userMan.attrList) {
			attrTypeSeqList.Add(attr.attrTypeSeq);
			attrSeqList.Add(attr.attrSeq);
			attrInputValue.Add(attr.attrImputValue);
		}

		string sAttrTypeSeq;
		string sAttrSeq;
		string sInputValue;
		int iIdx;
		foreach (string key in Request.Form.AllKeys) {
			iIdx = 0;
			sAttrTypeSeq = "";
			sAttrSeq = "";
			sInputValue = "";

			if (key.StartsWith("txtAreaUserManItem")) {
				sAttrTypeSeq = key.Replace("txtAreaUserManItem","");
				sAttrSeq = "";
				sInputValue = Request.Form[key];

				iIdx = attrTypeSeqList.IndexOf(sAttrTypeSeq);
				if (iIdx != -1) {
					attrTypeSeqList[iIdx] = sAttrTypeSeq;
					attrSeqList[iIdx] = sAttrSeq;
					attrInputValue[iIdx] = sInputValue;
				} else {
					attrTypeSeqList.Add(sAttrTypeSeq);
					attrSeqList.Add(sAttrSeq);
					attrInputValue.Add(sInputValue);
				}
				
				if (sInputValue.IndexOf("$") != -1) {
					lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_INPUT_DOLLAR_MARK);
					return false;
				}
				
				string sNGWord;
				if (sessionMan.ngWord.VaidateDoc(sInputValue,out sNGWord) == false) {
					lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
					return false;
				}
			} else if (key.StartsWith("rdoUserManItem")) {
				sAttrTypeSeq = key.Replace("rdoUserManItem","");
				sAttrSeq = Request.Form[key];
				sInputValue = "";

				iIdx = attrTypeSeqList.IndexOf(sAttrTypeSeq);
				if (iIdx != -1) {
					attrTypeSeqList[iIdx] = sAttrTypeSeq;
					attrSeqList[iIdx] = sAttrSeq;
					attrInputValue[iIdx] = sInputValue;
				} else {
					attrTypeSeqList.Add(sAttrTypeSeq);
					attrSeqList.Add(sAttrSeq);
					attrInputValue.Add(sInputValue);
				}
			} else if (key.StartsWith("txtUserManItem")) {
				sAttrTypeSeq = key.Replace("txtUserManItem","");
				sAttrSeq = "";
				sInputValue = Request.Form[key];

				iIdx = attrTypeSeqList.IndexOf(sAttrTypeSeq);
				if (iIdx != -1) {
					attrTypeSeqList[iIdx] = sAttrTypeSeq;
					attrSeqList[iIdx] = sAttrSeq;
					attrInputValue[iIdx] = sInputValue;
				} else {
					attrTypeSeqList.Add(sAttrTypeSeq);
					attrSeqList.Add(sAttrSeq);
					attrInputValue.Add(sInputValue);
				}
			} else if (key.StartsWith("lstUserManItem")) {
				sAttrTypeSeq = key.Replace("lstUserManItem","");
				sAttrSeq = Request.Form[key];
				sInputValue = "";

				iIdx = attrTypeSeqList.IndexOf(sAttrTypeSeq);
				if (iIdx != -1) {
					attrTypeSeqList[iIdx] = sAttrTypeSeq;
					attrSeqList[iIdx] = sAttrSeq;
					attrInputValue[iIdx] = sInputValue;
				} else {
					attrTypeSeqList.Add(sAttrTypeSeq);
					attrSeqList.Add(sAttrSeq);
					attrInputValue.Add(sInputValue);
				}
			}
		}

		if (attrTypeSeqList.Count != 0) {
			sessionMan.userMan.ModifyUser(
			sessionMan.userMan.userSeq,
			sessionMan.userMan.loginPassword,
			sessionMan.userMan.tel,
			sessionMan.userMan.handleNm,
			sessionMan.userMan.birthday,
			out sResult,
			attrTypeSeqList.ToArray(),
			attrSeqList.ToArray(),
			attrInputValue.ToArray(),
			attrTypeSeqList.Count,
			ViCommConst.FLAG_OFF
			);
		}

		return true;
	}

	private bool CheckCanWaiting() {
		int iUnitSec1,iUnitSec2,iUnitSec3,iUnitSec4;
		int iPoint1,iPoint2,iPoint3,iPoint4;

		sessionMan.userMan.GetCornerCharge(sessionMan.site.siteCd,string.Empty,ViCommConst.CHARGE_CAST_TALK_PUBLIC,sessionMan.userMan.castCharacter.actCategorySeq,out iUnitSec1,out iPoint1);
		sessionMan.userMan.GetCornerCharge(sessionMan.site.siteCd,string.Empty,ViCommConst.CHARGE_CAST_TALK_WSHOT,sessionMan.userMan.castCharacter.actCategorySeq,out iUnitSec2,out iPoint2);
		sessionMan.userMan.GetCornerCharge(sessionMan.site.siteCd,string.Empty,ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT,sessionMan.userMan.castCharacter.actCategorySeq,out iUnitSec3,out iPoint3);
		sessionMan.userMan.GetCornerCharge(sessionMan.site.siteCd,string.Empty,ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC,sessionMan.userMan.castCharacter.actCategorySeq,out iUnitSec4,out iPoint4);

		if (sessionMan.userMan.balPoint < iPoint1 || sessionMan.userMan.balPoint < iPoint2 || sessionMan.userMan.balPoint < iPoint3 || sessionMan.userMan.balPoint < iPoint4) {
			return false;
		} else {
			return true;
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
