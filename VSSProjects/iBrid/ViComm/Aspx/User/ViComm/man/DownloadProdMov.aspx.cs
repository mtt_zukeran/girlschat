/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 販売動画ﾀﾞｳﾝﾛｰﾄﾞ
--	Progaram ID		: DownloadProdMov
--
--  Creation Date	: 2010.12.17
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_DownloadProdMov : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
	
		string sAction = iBridUtil.GetStringValue(this.Request.Params["action"]);
		string sProductId = iBridUtil.GetStringValue(this.Request.Params["product_id"]);
		string sAdultFlag = iBridUtil.GetStringValue(this.Request.Params["adult"]);
		string sProductType = ProductHelper.GetMovieProductType(sAdultFlag);
		string sObjSeq = iBridUtil.GetStringValue(this.Request.Params["obj_seq"]);
		string sFileNm = iBridUtil.GetStringValue(this.Request.Params["file_nm"]);
		string sUseType = iBridUtil.GetStringValue(this.Request.Params["use_type"]);

		string sProductSeq = null;
		using (Product oProduct = new Product()) {
			sProductSeq = oProduct.ConvertProductId2Seq(sProductId);
		}

		// 未購入の場合は購入確認画面へ遷移
		if(!ProductHelper.IsAlreadyUsedMovie(sProductSeq,sProductType,sObjSeq)){
			this.Redirect2ConfirmProdMov();
		}

		// ======================
		// 動画のﾀﾞｳﾝﾛｰﾄﾞ処理
		// ======================
		if (sAction.Equals(ViCommConst.BUTTON_ACTION_DOWNLOAD)) {
			if (this.DownloadAction(sProductId, sObjSeq, sFileNm, sUseType)) return;
		}

		ulong lSeekMode = ulong.Parse(this.Request.QueryString["seekmode"]);
		if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			lSeekMode = ViCommConst.INQUIRY_PRODUCT_MOVIE;
		}

		// ======================
		//  画面表示処理
		// ======================
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		if(!this.IsPostBack){
			if(!sessionMan.ControlList(this.Request, lSeekMode, this.ActiveForm)){
				throw new ApplicationException();
			}
		}
	}
	
	private bool DownloadAction(string pProductId,string pObjSeq,string pFileNm,string pUseType){

		string sProductDir = ViCommPrograms.GetProductPhysicalDir(sessionMan.site.webPhisicalDir, sessionMan.site.siteCd, pProductId);
		string sFilePath = Path.Combine(sProductDir,pFileNm);

		// 動画ﾌｧｲﾙを格納するフォルダへアクセス可能なユーザーに偽装。
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, "", ViCommConst.FILE_UPLOAD_PASSWORD)) {

			FileInfo oFileInfo = new FileInfo(sFilePath);

			if (!oFileInfo.Exists || oFileInfo.Length == 0)
				return false;

			if (!ViCommConst.SOFTBANK.Equals(sessionMan.carrier)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(
					string.Format("productRes/{0}/{1}/{2}", sessionMan.site.siteCd, pProductId, oFileInfo.Name)));			
				return true;
			}

			Response.ContentType = "video/3gpp";
			Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", oFileInfo.Name));
			Response.AddHeader("Cache-Control", "no-store");
			Response.AddHeader("Content-Length", oFileInfo.Length.ToString());
			Response.Clear();
			Response.WriteFile(oFileInfo.FullName);
			Response.Flush();
			Response.Close();
			Response.End();
			return true;
		}	
	}

	private void Redirect2ConfirmProdMov() {
		UrlBuilder oUrlBuilder = new UrlBuilder("ConfirmProdMov.aspx", this.Request.QueryString);
		if(oUrlBuilder.Parameters.ContainsKey(ViCommConst.ACTION)){
			oUrlBuilder.Parameters.Remove(ViCommConst.ACTION);
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
	}

}
