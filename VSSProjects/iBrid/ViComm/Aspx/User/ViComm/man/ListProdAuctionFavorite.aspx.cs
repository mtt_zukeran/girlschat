﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: オークションお気に入り一覧
--	Progaram ID		: ListProdAuctionFavorite
--
--  Creation Date	: 2011.06.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;
public partial class ViComm_man_ListProdAuctionFavorite : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					this.Request,
					ViCommConst.INQUIRY_PRODUCT_AUCTION_FAVORITE,
					this.ActiveForm);

		}
	}
}
