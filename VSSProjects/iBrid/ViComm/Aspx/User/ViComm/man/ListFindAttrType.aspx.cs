/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���������w��i�����I�����j
--	Progaram ID		: ListFindAttrType
--
--  Creation Date	: 2010.01.25
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListFindAttrType:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_NONE_DISP_PROFILE,
					ActiveForm);
		}
	}
}
