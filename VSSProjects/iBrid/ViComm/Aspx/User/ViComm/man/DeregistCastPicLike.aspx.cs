﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者画像イイネ解除
--	Progaram ID		: DeregistCastPicLike
--  Creation Date	: 2016.10.05
--  Creater			: M&TT Zukeran
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_DeregistCastPicLike:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			// 未ログイン（登録画面へ遷移）
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));
			string sResult = string.Empty;

			// パラメータ不足
			if (string.IsNullOrEmpty(sPicSeq) || string.IsNullOrEmpty(sBackUrl)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			// いいね解除
			using (CastPicLike oCastPicLike = new CastPicLike()) {
				oCastPicLike.DeregistCastPicLike(sessionMan.site.siteCd,sPicSeq,sessionMan.userMan.userSeq,out sResult);
			}

			// 正常終了
			if (sResult.Equals(PwViCommConst.DeregistCastPicLikeResult.RESULT_OK)) {
				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sBackUrl));
					return;
				} else {
					pnlComplete.Visible = true;
				}
			}
			// いいねしてない
			else if (sResult.Equals(PwViCommConst.DeregistCastPicLikeResult.RESULT_NG_NOT_LIKED)) {
				sessionMan.errorMessage = "既にｲｲﾈ解除済みです";
				pnlError.Visible = true;
			}
			// 拒否されてる
			else if (sResult.Equals(PwViCommConst.DeregistCastPicLikeResult.RESULT_NG_CAST_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.ERR_REFUSE_MAN);
				return;
			}
			// 拒否している
			else if (sResult.Equals(PwViCommConst.DeregistCastPicLikeResult.RESULT_NG_SELF_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
				return;
			}
			// その他エラー
			else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
