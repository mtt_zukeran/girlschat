/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްщ��TOP
--	Progaram ID		: GameUserTop
--
--  Creation Date	: 2011.07.21
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_GameUserTop:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!sessionMan.logined) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByTermIdGame.aspx"));
			return;
		}

		if (sessionMan.userMan.characterEx.siteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_ONLY)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_NON_USER_TOP_FOR_CHAT_USER);
			return;
		}

		if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || (sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS) && !sessionMan.adminFlg)) {
			HttpCookie cookie = new HttpCookie("maqia");
			cookie.Values["maqiauid"] = sessionMan.userMan.userSeq;
			cookie.Values["maqiapw"] = sessionMan.userMan.loginPassword;
			cookie.Values["maqiasex"] = ViCommConst.MAN;
			cookie.Expires = DateTime.Now.AddDays(90);
			cookie.Domain = sessionMan.site.hostNm;
			Response.Cookies.Add(cookie);
		}

		using (User oUser = new User()) {
			oUser.UpdateMobileInfo(sessionMan.userMan.userSeq,sessionMan.carrier,sessionMan.mobileUserAgent);
		}
	}
}
