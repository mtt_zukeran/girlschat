﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールおねだり登録
--	Progaram ID		: RegistMailRequestLog
--
--  Creation Date	: 2014.12.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistMailRequestLog:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			lblForm.Visible = true;
			lblDone.Visible = false;
			lblError.Visible = false;
			
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);

			DataRow dr;
			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,1,out dr)) {

				if (!iBridUtil.GetStringValue(dr["USER_STATUS"]).Equals(ViCommConst.USER_WOMAN_NORMAL) ||
					!iBridUtil.GetStringValue(dr["NA_FLAG"]).Equals(ViCommConst.NaFlag.OK.ToString())) {
					RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
				}

				ViewState["CAST_USER_SEQ"] = dr["USER_SEQ"].ToString();
				ViewState["CAST_CHAR_NO"] = dr["USER_CHAR_NO"].ToString();
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
			
			if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
				this.RegistMailRequestLog();
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		this.RegistMailRequestLog();
	}

	protected void RegistMailRequestLog() {
		sessionMan.errorMessage = string.Empty;
		string sResult = string.Empty;
		int iRequestLimitCount;

		string sCastUserSeq = iBridUtil.GetStringValue(ViewState["CAST_USER_SEQ"]);
		string sCastCharNo = iBridUtil.GetStringValue(ViewState["CAST_CHAR_NO"]);
		using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
			oMailRequestLog.RegistMailRequestLog(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sCastUserSeq,sCastCharNo,ViCommConst.FLAG_OFF,out iRequestLimitCount,out sResult);
		}
		
		if (sResult.Equals(PwViCommConst.RegistMailRequestLogResult.RESULT_OK)) {
			lblForm.Visible = false;
			lblDone.Visible = true;
			lblError.Visible = false;
		} else if (sResult.Equals(PwViCommConst.RegistMailRequestLogResult.RESULT_COUNT_OVER)) {
			sessionMan.errorMessage = string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_REQUEST_NG_COUNT_OVER),iRequestLimitCount.ToString());
			lblForm.Visible = false;
			lblDone.Visible = false;
			lblError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.RegistMailRequestLogResult.RESULT_RECEIVED_MAIL)) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_REQUEST_NG_RECEIVED);
			lblForm.Visible = false;
			lblDone.Visible = false;
			lblError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.RegistMailRequestLogResult.RESULT_REFUSE)) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_REQUEST_NG_REFUSE);
			lblForm.Visible = false;
			lblDone.Visible = false;
			lblError.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}

}
