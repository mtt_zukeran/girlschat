<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewFavorit.aspx.cs" Inherits="ViComm_man_ViewFavorit" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:SelectionList ID="lstFavoritGroup" Runat="server" BreakAfter="true"></mobile:SelectionList>
		$PGM_HTML02;
		<ibrid:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
