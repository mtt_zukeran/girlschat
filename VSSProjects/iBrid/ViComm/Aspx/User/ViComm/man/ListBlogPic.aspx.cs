/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�ʎʐ^ �ꗗ
--	Progaram ID		: ListBlogPic
--
--  Creation Date	: 2011.04.25
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListBlogPic : MobileBlogManBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
		sessionMan.ControlList(Request, ViCommConst.INQUIRY_EXTENSION_BLOG_OBJ, ActiveForm);
	}
}
