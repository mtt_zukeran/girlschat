/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ڼޯĊȒP����
--	Progaram ID		: PaymentCreditEasy
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCreditEasy:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			if (sessionMan.userMan.existCreditDealFlag != 0){
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSid = "";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,"500",sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_BIGSUN,
					ViCommConst.SETTLE_CREDIT_PACK,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					500,
					oPack.exPoint,
					"",
					"",
					out sSid);
			}
		}
		string sSettleUrl = "";
		string sBackUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_BIGSUN,ViCommConst.SETTLE_CREDIT_PACK)) {
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,500,sBackUrl);
			}
		}
		Response.Redirect(sSettleUrl);
	}
}
