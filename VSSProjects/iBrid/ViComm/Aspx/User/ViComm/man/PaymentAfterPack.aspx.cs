/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 後払パック
--	Progaram ID		: PaymentAfterPack
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentAfterPack:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.valid = true;

		// 戻るﾎﾞﾀﾝによる連続購入をチェックするため,POSTBACK時もチェックを行う。 
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		lblErrorMessage.Text = string.Empty;
		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

		if (sessionMan.userMan.telAttestedFlag == 0) {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PAY_AFTER_UNAUTH_TEL);
			sessionMan.valid = false;
		} else {
			if (sessionMan.userMan.totalReceiptAmt == 0) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PAY_AFTER_RECEIPT_ZERO);
			}
		}

		if (sessionMan.valid) {
			int iCreditBal = (sessionMan.userMan.limitPoint * sessionMan.site.pointPrice) - sessionMan.userMan.billAmt;
			DataSet ds;
			ds = GetAfterPackDataSet(iCreditBal);
			if (!IsPostBack) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				}
			}
			if (ds.Tables[0].Rows.Count == 0) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PAY_AFTER_OVER_LIMIT);
				sessionMan.valid = false;
			}
		}

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}

	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstPack.Items[lstPack.SelectedIndex].Value;
		string sSid = "";
		bool bResult = false;
		lblErrorMessage.Text = "";

		int iCreditBal = (sessionMan.userMan.limitPoint * sessionMan.site.pointPrice) - sessionMan.userMan.billAmt - int.Parse(sSalesAmt);
		if (iCreditBal < 0) {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PAY_AFTER_OVER_LIMIT);
			return;
		}

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_PAYMENT_AFTER_PACK,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_SITE01,
					ViCommConst.SETTLE_PAYMENT_AFTER_PACK,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					"",
					"",
					out sSid);

				bResult = oLog.LogSettleResult(
					sSid,
					sessionMan.userMan.userSeq,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					"0");
			}
		}
		if (bResult) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_PAY_AFTER_OK));
		}
	}
}
