/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: メールアドレス変更
--	Progaram ID		: ModifyMailAddr
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ModifyMailAddr:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			ParseHTML oParseMan = sessionMan.parseContainer;
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_MAIL;
		}
	}
}
