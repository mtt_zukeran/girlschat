<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentCreditZeroVoiceAuto.aspx.cs" Inherits="ViComm_man_PaymentCreditZeroVoiceAuto" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="ibrid" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		$PGM_HTML02;
		$PGM_HTML03;
		<mobile:Panel ID="pnlInputTelNo" Runat="server">
			$PGM_HTML04;
			<ibrid:iBMobileTextBox ID="txtTelNo" MaxLength="11" Runat="server"></ibrid:iBMobileTextBox>
		</mobile:Panel>
		$PGM_HTML06;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
