/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 足あと一覧
--	Progaram ID		: ListMarking
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListMarking:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.userMan.profileOkFlag == 0) {
				tagNoProfile.Text = "$PGM_HTML05;";
			} else {
				tagNoProfile.Text = "";
			}

			// 足あとリストの確認日時を更新
			using (Marking oMarking = new Marking()) {
				oMarking.UpdateConfirmMarkingDate(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN);
			}

			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_MARKING,
					ActiveForm);
		}
	}

}
