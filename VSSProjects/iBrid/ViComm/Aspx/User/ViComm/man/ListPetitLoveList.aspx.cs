/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プチラブリスト一覧
--	Progaram ID		: ListPetitLoveList
--
--  Creation Date	: 2011.04.07
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListPetitLoveList:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_PETIT_LOVE_LIST,
					ActiveForm);
		}
	}

}
