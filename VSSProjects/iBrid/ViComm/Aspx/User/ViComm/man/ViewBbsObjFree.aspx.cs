/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���󖳗��J���ڍ�
--	Progaram ID		: ViewBbsObjFree
--
--  Creation Date	: 2013.07.29
--  Creater			: M&TT Y.Ikemiya
--	Remarks			: 
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ViewBbsObjFree:MobileManPageBase {

	bool dupUsed;
	string movieSeq;
	string castSeq;
	string castCharNo;
	const int chargePoint = 0;

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (sessionMan.logined == false) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			return;
		}
		
		using (Site oSite = new Site()) {
			string[] sValue = null;
			oSite.GetValues(sessionMan.site.siteCd,"BBS_OBJ_FREE_VIEW_START_DATE,BBS_OBJ_FREE_VIEW_END_DATE",ref sValue);
			DateTime dtStartDate = DateTime.Parse(sValue[0]);
			DateTime dtEndDate = DateTime.Parse(sValue[1]);
			if (DateTime.Compare(dtStartDate,DateTime.Now) > 0 || DateTime.Compare(DateTime.Now,dtEndDate) > 0) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_BBS_OBJ_SPECIAL_FREE_END);
			}
		}

		using (CastBbsObj oCastBbsObj = new CastBbsObj()) {
			string sFreeOkFlag = string.Empty;
			string sSiteCd = sessionMan.site.siteCd;
			string sUserSeq = sessionMan.userMan.userSeq;
			string sUserCharNo = sessionMan.userMan.userCharNo;
			string sObjSeq = string.Empty;
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["movieseq"]))) {
				sObjSeq = iBridUtil.GetStringValue(this.Request.QueryString["movieseq"]);
			} else {
				sObjSeq = iBridUtil.GetStringValue(this.Request.QueryString["objseq"]);
			}
			oCastBbsObj.CheckSpecialFreeBbsObj(sSiteCd,sUserSeq,sUserCharNo,sObjSeq,out sFreeOkFlag);

			if (!sFreeOkFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		}

		bool bDownload = false;
		string sAllBbs = iBridUtil.GetStringValue(Request.QueryString["allbbs"]);

		if (!IsPostBack) {
			castSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			movieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
			castCharNo = ViCommConst.MAIN_CHAR_NO;

			if (!movieSeq.Equals(string.Empty)) {
				bDownload = Download();
			}
			if (bDownload == false) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				ulong uSeekMode;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode) == false) {
					uSeekMode = ViCommConst.INQUIRY_BBS_OBJ;
				}

				if (sessionMan.ControlList(Request,uSeekMode,ActiveForm)) {

					string sObjType = sessionMan.GetBbsObjValue("OBJ_KIND");
					string sLoginId;

					switch (sObjType) {
						case ViCommConst.BbsObjType.PIC:

							castSeq = sessionMan.GetBbsObjValue("USER_SEQ");
							castCharNo = sessionMan.GetBbsObjValue("USER_CHAR_NO");
							sLoginId = sessionMan.GetBbsObjValue("LOGIN_ID");
							string sPicSeq = sessionMan.GetBbsObjValue("PIC_SEQ");

							if (sessionMan.SetCastDataSetByLoginId(sLoginId,castCharNo,iRecNo)) {
								sessionMan.errorMessage = "";

								using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
									dupUsed = oObjUsedHistory.GetOne(
														sessionMan.userMan.siteCd,
														sessionMan.userMan.userSeq,
														ViCommConst.ATTACH_PIC_INDEX.ToString(),
														sPicSeq);
								}

								if (dupUsed == false) {

									using (WebUsedLog oLog = new WebUsedLog()) {
										oLog.CreateWebUsedReport(
												sessionMan.userMan.siteCd,
												sessionMan.userMan.userSeq,
												ViCommConst.CHARGE_BBS_PIC,
												chargePoint,
												castSeq,
												castCharNo,
												"");
									}
									using (Access oAccess = new Access()) {
										oAccess.AccessPicPage(sessionMan.site.siteCd,castSeq,castCharNo,sPicSeq);
									}

									using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
										oObjUsedHistory.AccessBbsObj(
												sessionMan.userMan.siteCd,
												sessionMan.userMan.userSeq,
												ViCommConst.ATTACH_PIC_INDEX.ToString(),
												sPicSeq,
												ViCommConst.FLAG_OFF,
												ViCommConst.FLAG_ON);
									}
								}
							}
							break;

						case ViCommConst.BbsObjType.MOVIE:
							castSeq = sessionMan.GetBbsObjValue("USER_SEQ");
							castCharNo = sessionMan.GetBbsObjValue("USER_CHAR_NO");
							movieSeq = sessionMan.GetBbsObjValue("OBJ_SEQ");
							sLoginId = sessionMan.GetBbsObjValue("LOGIN_ID");
							CheckMovieBalance();
							sessionMan.SetCastDataSetByLoginId(sessionMan.GetBbsObjValue("LOGIN_ID"),castCharNo,iRecNo);
							break;

						default:
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListBbsObj.aspx?allbbs={0}",sAllBbs)));
							break;
					}

				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListBbsObj.aspx?allbbs={0}",sAllBbs)));
				}
			}
		}
	}

	private bool Download() {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);
		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		CheckMovieBalance();

		if (bIsCheckOnly)
			return false;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,movieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				if (sessionMan.logined) {
					if (!MovieHelper.IsRangeRequest()) {
						if (dupUsed == false) {
							using (Access oAccess = new Access()) {
								oAccess.AccessMoviePage(sessionMan.site.siteCd,castSeq,castCharNo,movieSeq);
							}
							using (WebUsedLog oLog = new WebUsedLog()) {
								oLog.CreateWebUsedReport(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.CHARGE_BBS_MOVIE,
										chargePoint,
										castSeq,
										castCharNo,
										"");
							}
							using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
								oObjUsedHistory.AccessBbsObj(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
										movieSeq,
										ViCommConst.FLAG_OFF,
										ViCommConst.FLAG_ON);
							}
						}
					}
				}

				MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.OPERATOR);

				return true;
			} else {
				return false;
			}
		}
	}

	private void CheckMovieBalance() {
		if (!MovieHelper.IsRangeRequest()) {
			sessionMan.errorMessage = "";
			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				dupUsed = oObjUsedHistory.GetOne(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
									movieSeq);
			}
		}
	}
}
