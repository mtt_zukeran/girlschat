/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���i�����ݏڍ�
--	Progaram ID		: ViewProdAuction
--
--  Creation Date	: 2011.06.20
--  Creater			: K.Itoh
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewProdAuction : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

		ulong lSeekMode = ViCommConst.INQUIRY_PRODUCT_AUCTION;
		if (!iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			string sSeekMode = iBridUtil.GetStringValue(this.Request.QueryString["seekmode"]);
			if(!ulong.TryParse(sSeekMode,out lSeekMode)){
				lSeekMode = ViCommConst.INQUIRY_PRODUCT_AUCTION;
			}
		}

		if (!IsPostBack) {
			if (!sessionMan.ControlList(this.Request, lSeekMode, this.ActiveForm)) {
				throw new ApplicationException();
			}
		}else{
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender, e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender, EventArgs e) {
		ulong lSeekMode = ViCommConst.INQUIRY_PRODUCT_AUCTION;
		string sSeekMode = iBridUtil.GetStringValue(this.Request.Params["seekmode"]);
		if (!ulong.TryParse(sSeekMode,out lSeekMode)) {
			lSeekMode = ViCommConst.INQUIRY_PRODUCT_AUCTION;
		}
		UrlBuilder oUrlBuilder = new UrlBuilder("BidProdAuction.aspx");
		oUrlBuilder.AddParameter("seekmode",lSeekMode.ToString());
		oUrlBuilder.AddParameter("adult", this.Request.Params["adult"]);
		oUrlBuilder.AddParameter("product_id",this.Request.Params["product_id"]);
		oUrlBuilder.AddParameter("product_seq",this.Request.Params["product_seq"]);
		RedirectToMobilePage(sessionObj.GetNavigateUrl(oUrlBuilder.ToString()));
	}

}
