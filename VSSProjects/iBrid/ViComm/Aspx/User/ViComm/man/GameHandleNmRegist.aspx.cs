/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ްٽ����ēo�^�҂̹ްі��o�^�y�[�W
--	Progaram ID		: GameHandleNmRegist
--
--  Creation Date	: 2011.07.21
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_man_GameHandleNmRegist:MobileSocialGameManBase {

	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		if(sessionMan.userMan.gameCharacter.gameLogined == true) {
			if(string.IsNullOrEmpty(sessionMan.userMan.gameCharacter.gameCharacterType)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_SELECT);
			} else if(sessionMan.userMan.gameCharacter.tutorialStatus.Equals("1")) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"ListGameItemShop.aspx"));
			} else if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("2") || sessionMan.userMan.gameCharacter.tutorialStatus.Equals("5") || sessionMan.userMan.gameCharacter.tutorialStatus.Equals("6")) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"ViewGameStage.aspx"));
			} else if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("3") || sessionMan.userMan.gameCharacter.tutorialStatus.Equals("4")) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,string.Format("ViewGameTownClearMissionOK.aspx?stage_seq={0}&town_seq={1}",this.sessionMan.userMan.gameCharacter.stageSeq,this.GetGameTutorialTownSeq())));
			} else if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("7")) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"ListGameCharacterBattle.aspx"));
			} else if (sessionMan.userMan.gameCharacter.tutorialStatus.Equals("8")) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"GameTutorialEnd.aspx"));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"GameUserTop.aspx"));
			}
		}

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdGame.aspx"));
				return;
			}

			cmdSubmit_Click(sender,e);
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e) {

		string sSiteCd = sessionMan.site.siteCd;
		string sUserSeq = sessionMan.userMan.userSeq;
		string sUserCharNo = sessionMan.userMan.userCharNo;
		string sSexCd = sessionMan.sexCd;

		string sName = this.sessionMan.userMan.handleNm;

		string sResult;
		sResult = sessionMan.userMan.gameCharacter.RegistNm(sSiteCd, sUserSeq, sUserCharNo, sSexCd, sName);
		if (sResult.Equals(PwViCommConst.GameCharCreate.GAME_CHAR_CREATE_RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

		// ����������ʂɑJ�ڂ��܂��B 
		RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CHAR_TYPE_SELECT);

	}

	private string GetGameTutorialTownSeq() {
		string sValue = string.Empty;

		TownSeekCondition oCondition = new TownSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.SexCd = this.sessionMan.sexCd;

		using (Town oTown = new Town()) {
			sValue = oTown.GetGameTutorialTownSeq(oCondition);
		}

		return sValue;
	}
}
