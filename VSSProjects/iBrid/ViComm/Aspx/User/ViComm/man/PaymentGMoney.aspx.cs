/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: G-MONEY�x��
--	Progaram ID		: PaymentGMoney
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/
// �p������URL���X�}�z�pURL��
// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentGMoney:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);

			DataSet ds;
			ds = GetPackDataSet(ViCommConst.SETTLE_G_MONEY,0);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
					lstPack.SelectedIndex = lstPack.Items.Count - 1;
					lstPack.Visible = false;
					sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
					sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
					sessionMan.userMan.settleServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
					break;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstPack.Items[lstPack.SelectedIndex].Value;
		string sSid = "";
		string sCC = "";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_G_MONEY,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_GIS,
					ViCommConst.SETTLE_G_MONEY,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					string.Empty,
					string.Empty,
					out sSid);
			}
			sCC = oPack.commoditiesCd;
		}
		string sSettleUrl = "";
		string sBackUrl = "";


		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_GIS,ViCommConst.SETTLE_G_MONEY)) {
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				string sUrl = string.Empty;
				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					sUrl = oSiteSettle.continueSettleUrl;
				} else {
					sUrl = oSiteSettle.settleUrl;
				}
				sSettleUrl = string.Format(sUrl,oSiteSettle.cpIdNo,sCC,DateTime.Now.ToString("yyyyMMddHHmmss"),sSalesAmt,sBackUrl,sessionMan.userMan.userSeq,sSid);
			}
		}
		Response.Redirect(sSettleUrl);
	}
}
