/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ アイドルのつぶやきイイネ登録
--	Progaram ID		: RegistCastTweetLike
--
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_RegistCastTweetLike:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_TWEET,this.ActiveForm)) {
			DataRow oDataRow = sessionMan.parseContainer.parseUser.dataTable[PwViCommConst.DATASET_CAST_TWEET].Rows[0];

			//自分が拒否している場合
			using (Refuse oRefuse = new Refuse()) {
				if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]))) {
					this.RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
				}
			}
			
			using (CastTweet oCastTweet = new CastTweet()) {
				string sSiteCd = sessionMan.site.siteCd;
				string sUserSeq = sessionMan.userMan.userSeq;
				string sUserCharNo = sessionMan.userMan.userCharNo;
				string sCastTweetSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetseq"]);
				string sResult = string.Empty;
				oCastTweet.RegistCastTweetLike(sSiteCd,sUserSeq,sUserCharNo,sCastTweetSeq,out sResult);
			}
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}