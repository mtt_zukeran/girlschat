<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewInvestigateEntryError.aspx.cs" Inherits="ViComm_man_ViewInvestigateEntryError" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagErrInfoMailRxType" runat="server" Text="$PGM_HTML02;" />
		<cc1:iBMobileLiteralText ID="tagErrCastMailRxType" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLiteralText ID="tagErrRxMailTime" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLiteralText ID="tagErrMailAddrNg" runat="server" Text="$PGM_HTML05;" />
		<cc1:iBMobileLiteralText ID="tagErrMailAddrOk" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>