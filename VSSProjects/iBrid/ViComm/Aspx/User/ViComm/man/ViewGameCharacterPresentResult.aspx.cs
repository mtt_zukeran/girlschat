/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE��ھ��Ċ���
--	Progaram ID		: ViewGameCharacterPresentResult
--
--  Creation Date	: 2011.08.12
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameCharacterPresentResult:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		string sItemSeq = iBridUtil.GetStringValue(Request.QueryString["item_seq"]);
		string sPurchaseFlg = iBridUtil.GetStringValue(Request.QueryString["purchase_flg"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty) || sItemSeq.Equals(string.Empty) || sPurchaseFlg.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
	}
}