﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・仲間申請取り下げ
--	Progaram ID		: RegistGameCancelApplicationFellow.aspx
--
--  Creation Date	: 2011.08.17
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameCancelApplicationFellow:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo) {
		using (GameFellow oGameFellow = new GameFellow()) {
			string sResult = oGameFellow.CancelApplicationFellow(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo
			);

			IDictionary<string,string> oParameters = new Dictionary<string,string>();

			oParameters.Add("result",sResult.ToString());
			oParameters.Add("partner_user_seq",pPartnerUserSeq);
			oParameters.Add("partner_user_char_no",pPartnerUserCharNo);

			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_CANCEL_APPLY_FELLOW_RESULT,oParameters);
		}
	}
}