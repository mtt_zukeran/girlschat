/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 個人別日記一覧
--	Progaram ID		: ListPersonalDiary
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListPersonalDiary:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo)) {
				sessionMan.ControlList(
					Request,
					ulong.Parse(iBridUtil.GetStringValue(Request.QueryString["seekmode"])),
					ActiveForm);
			}
		}
	}

}
