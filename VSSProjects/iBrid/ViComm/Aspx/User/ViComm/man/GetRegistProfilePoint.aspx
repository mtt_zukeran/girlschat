<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetRegistProfilePoint.aspx.cs" Inherits="ViComm_man_GetRegistProfilePoint" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagCertify" runat="server" Text="$PGM_HTML02;" />
		<cc1:iBMobileLiteralText ID="tagNoProf" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLiteralText ID="tagModifyProf" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLiteralText ID="tagGetPoint" runat="server" Text="$PGM_HTML05;" />
		<cc1:iBMobileLiteralText ID="tagAfterGetPoint" runat="server" Text="$PGM_HTML06;" />
		<cc1:iBMobileLiteralText ID="tagCondition" runat="server" Text="$PGM_HTML07;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
