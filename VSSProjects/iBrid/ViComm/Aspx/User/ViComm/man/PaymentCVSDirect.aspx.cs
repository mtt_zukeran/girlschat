/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: コンビニダダイレクト支払
--	Progaram ID		: PaymentCVSDirect
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.IO;
using System.Text;
using System.Net;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCVSDirect:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.errorMessage = "";

		if (!IsPostBack) {
			string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);
			DataSet ds;
			ds = GetPackDataSet(ViCommConst.SETTLE_CVS_DIRECT,0);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
					lstPack.SelectedIndex = lstPack.Items.Count - 1;
					lstPack.Visible = false;
					sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
					sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
					sessionMan.userMan.settleServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
					break;
				}
			}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_CVSDL);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstCVS.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString().Replace("@",string.Empty)));
				}
			}

			if (IsAvailableService(ViCommConst.RELEASE_CVS_DIRECT_FORM_OMIT)) {
				//住所
				string sAddrItemNo = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["cvsAddrItemNo"]);
				txtAddr.Text = GetUserManAttrValue(sAddrItemNo);
				if (txtAddr.Text.Equals(string.Empty)) {
					txtAddr.Text = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["cvsAddr"]);
				}

				//電話番号
				txtTel.Text = sessionMan.userMan.tel;
				if (txtTel.Text.Equals(string.Empty)) {
					txtTel.Text = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["cvsTel"]);
				}

				//お名前
				txtFamilyNm.Text = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["cvsFamilyNm"]);
				txtGivenNm.Text = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["cvsGivenNm"]);

				if (!txtTel.Text.Equals(string.Empty)) {
					txtTel.Visible = false;
				}
				if (!txtFamilyNm.Text.Equals(string.Empty)) {
					txtFamilyNm.Visible = false;
				}
				if (!txtGivenNm.Text.Equals(string.Empty)) {
					txtGivenNm.Visible = false;
				}
				if (!txtAddr.Text.Equals(string.Empty)) {
					txtAddr.Visible = false;
				}
			} else {
				txtTel.Text = sessionMan.userMan.tel;
				txtFamilyNm.Text = sessionMan.userMan.familyNm;
				txtGivenNm.Text = sessionMan.userMan.givenNm;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {

		string sSalesAmt = lstPack.Items[lstPack.SelectedIndex].Value;
		string sSid = "";

		bool bOk = true;
		sessionMan.errorMessage = "";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CVS_DIRECT,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_DIGITAL_CHECK,
					ViCommConst.SETTLE_CVS_DIRECT,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					string.Empty,
					string.Empty,
					out sSid);
			}
		}
		string sSettleUrl = "";
		string sFamilyNm = "";
		string sGivenNm = "";
		string sAddr = "";
		Encoding enc = Encoding.GetEncoding("Shift_JIS");

		if (!txtFamilyNm.Text.Equals("")) {
			sFamilyNm = System.Web.HttpUtility.UrlEncode(txtFamilyNm.Text,enc);
		} else {
			bOk = false;
			sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_FAMILY_NM);
		}
		if (!txtGivenNm.Text.Equals("")) {
			sGivenNm = System.Web.HttpUtility.UrlEncode(txtGivenNm.Text,enc);
		} else {
			bOk = false;
			sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_GIVEN_NM);
		}
		if (!txtAddr.Text.Equals("")) {
			sAddr = System.Web.HttpUtility.UrlEncode(txtAddr.Text,enc);
		} else {
			bOk = false;
			sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_ADDR);
		}

		using (Pack oPack = new Pack())
		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CVS_DIRECT,sSalesAmt,sessionMan.userMan.userSeq)) {
				if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_DIGITAL_CHECK,ViCommConst.SETTLE_CVS_DIRECT)) {
					sSettleUrl = string.Format(oSiteSettle.settleUrl,
						oSiteSettle.cpIdNo,
						sSid,
						sFamilyNm,
						sGivenNm,
						txtTel.Text,
						sAddr,
						sessionMan.userMan.emailAddr,
						sSalesAmt,
						lstCVS.Items[lstCVS.SelectedIndex].Value,
						oPack.commoditiesCd,
						DateTime.Now.AddDays(14).ToString("yyyyMMdd"),
						string.Format("{0}:{1}:{2}",sessionMan.userMan.userSeq,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword));
				}
			}
		}

		sessionMan.userMan.settleRequestAmt = int.Parse(sSalesAmt);
		sessionMan.userMan.csvdlname = lstCVS.Items[lstCVS.SelectedIndex].Text;
		sessionMan.userMan.csvdlreceiptLimitDate = DateTime.Now.AddDays(14);

		if (bOk) {
			string sPaymentUrl,sCommission;
			if (TransCVS(sSettleUrl,out sPaymentUrl,out sCommission)) {
				sessionMan.userMan.ModifyUserName(txtFamilyNm.Text,txtGivenNm.Text);
				using (MailLog oMailLog = new MailLog()) {
					oMailLog.TxSettleAcceptMail(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.SETTLE_CORP_DIGITAL_CHECK,
						ViCommConst.SETTLE_CVS_DIRECT,
						sessionMan.userMan.settleRequestAmt,
						0,
						sSid,
						sessionMan.userMan.csvdlname,
						sessionMan.userMan.csvdlreceiptno,
						sPaymentUrl,
						sCommission
					);
				}
				sessionMan.userMan.csvdlreceiptUrl = sPaymentUrl;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_PAYMENT_CVS_DIRECT_COMPLITE));

			} else {
				sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_SETTLE_SERVER);
			}
		}
	}


	public bool TransCVS(string pUrl,out string pPaymentUrl,out string pCommission) {
		pPaymentUrl = string.Empty;
		pCommission = string.Empty;
		try {
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);
			
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 20000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				string[] sItem = Regex.Split(sRes,"\r\n");
				sr.Close();
				st.Close();
				if (sItem.Length > 0) {
					if (sItem.Length >= 7 && sItem[0].IndexOf("OK") >= 0) {
						sessionMan.userMan.csvdlreceiptno = sItem[3];
						pPaymentUrl = sItem[6];
						pCommission = sItem[7];
					}
					return sItem[0].Equals("OK");
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransCVS",pUrl);
			return false;
		}
	}

	private string GetUserManAttrValue(string itemNo) {
		string ret = string.Empty;
		foreach (UserManAttr attr in this.sessionMan.userMan.attrList) {
			if (itemNo == attr.itemNo) {
				if (attr.inputType == ViCommConst.INPUT_TYPE_TEXT) {
					ret = attr.attrImputValue;
				} else {
					ret = attr.attrNm;
				}
				break;
			}
		}
		return ret;
	}

	private bool OnRemoteCertificateValidationCallback(
	  Object sender,
	  X509Certificate certificate,
	  X509Chain chain,
	  SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}
}
