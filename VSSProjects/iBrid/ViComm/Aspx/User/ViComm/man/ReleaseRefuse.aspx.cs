/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���ۃ��X�g����
--	Progaram ID		: ReleaseRefuse
--
--  Creation Date	: 2010.05.10
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ReleaseRefuse:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			DataRow dr;
			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo,out dr)) {
			
				if (!iBridUtil.GetStringValue(dr["USER_STATUS"]).Equals(ViCommConst.USER_WOMAN_NORMAL) ||
					!iBridUtil.GetStringValue(dr["NA_FLAG"]).Equals(ViCommConst.NaFlag.OK.ToString())) {
					RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
				}
			
				using (Refuse oRefuse = new Refuse()) {
					oRefuse.RefuseMainte(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.MAN,
						dr["USER_SEQ"].ToString(),
						dr["USER_CHAR_NO"].ToString(),
						"9",
						"",
						1);
				}
			}
		}
	}

}
