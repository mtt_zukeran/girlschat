/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・クエスト報酬受取一覧
--	Progaram ID		: ListGameQuestRewardGet
--
--  Creation Date	: 2012.07.17
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListGameQuestRewardGet:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {

		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["quest_seq"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_GAME_QUEST_REWARD_GET,this.ActiveForm);
		}
	}
}
