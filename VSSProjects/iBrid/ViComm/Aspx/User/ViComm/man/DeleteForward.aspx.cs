/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �]���ݒ�폜
--	Progaram ID		: DeleteForward
--
--  Creation Date	: 2010.02.23
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_DeleteForward:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		string sPartnerSeq = iBridUtil.GetStringValue(Request.QueryString["puserseq"]);
		string sPartnerCharNo = ViCommConst.MAIN_CHAR_NO;

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			using (Recording oRecording = new Recording()) {
				oRecording.RecordingMainte(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sPartnerSeq,sPartnerCharNo,ViCommConst.REC_TYPE_PROFILE,"",ViCommConst.FLAG_ON);
			}

			string sUrl,sRequestUrl;
			string acceptSeq = "";
			string dialNo = "";
			string videoPrefix = "";
			string audioPrefix = "";
			string interfaceResult = "";
			string sPartnerId = "";

			using (Cast oCast = new Cast()) {
				if (oCast.GetCastInfo(sessionMan.site.siteCd,sPartnerSeq,sPartnerCharNo)) {
					sPartnerId = oCast.loginId;
				}
			}

			using (Sys oSys = new Sys()) {
				oSys.GetValue("VCS_REQUEST_URL",out sRequestUrl);

				sUrl = string.Format(
								"{0}?request=10&loc={1}&menu={2}&userid={3}&puserid={4}&pcharno={5}",
								sRequestUrl,
								sessionMan.site.VcsWeLocCd,
								ViCommConst.VCS_MENU_RELEASE_PF,
								sessionMan.userMan.loginId,
								sPartnerId,
								sPartnerCharNo
							);
			}
			
			ViCommInterface.TransIVPUserRequest(sUrl,ref acceptSeq,ref dialNo,ref videoPrefix,ref audioPrefix,ref interfaceResult);
		}
	}
}
