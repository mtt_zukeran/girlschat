/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE���Ԑ\������
--	Progaram ID		: RegistGameAcceptApplicationFellow.aspx
--
--  Creation Date	: 2011.08.17
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameAcceptApplicationFellow:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {

		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		string sRefuseFlag = iBridUtil.GetStringValue(this.Request.QueryString["refuse_flag"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty) || sRefuseFlag.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		if (!IsPostBack) {
			this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sRefuseFlag);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo,string sRefuseFlag) {
		using (GameFellow oGameFellow = new GameFellow()) {
			string sResult = oGameFellow.AcceptApplicationFellow(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				int.Parse(sRefuseFlag)
			);

			if(sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				this.CheckQuestClear(sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN);
				this.CheckQuestClear(pPartnerUserSeq,pPartnerUserCharNo,ViCommConst.OPERATOR);
			}

			IDictionary<string,string> oParameters = new Dictionary<string,string>();

			oParameters.Add("result",sResult.ToString());
			oParameters.Add("partner_user_seq",pPartnerUserSeq);
			oParameters.Add("partner_user_char_no",pPartnerUserCharNo);
			oParameters.Add("refuse_flag",sRefuseFlag);

			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ACCEPT_APPLICATION_FELLOW_RESULT,oParameters);
		}
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo,string pSexCd) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				pSexCd
			);
		}
	}
}