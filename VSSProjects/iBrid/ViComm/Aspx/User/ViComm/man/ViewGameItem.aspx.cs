/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���Ѽ���� ���яڍ�
--	Progaram ID		: ViewGameItem
--
--  Creation Date	: 2011.07.29
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_man_ViewGameItem:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(iBridUtil.GetStringValue(this.Request.QueryString["item_seq"]));
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_VIEW,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				string sItemSeq = iBridUtil.GetStringValue(this.Request.Params["NEXTLINK"]);
				string sBuyNum = iBridUtil.GetStringValue(this.Request.Params["buyItemNum"]);				
				string sPartnerUserSeq = iBridUtil.GetStringValue(this.Request.Params["partner_user_seq"]);
				string sPartnerUserCharNo = iBridUtil.GetStringValue(this.Request.Params["partner_user_char_no"]);
				string sCastGamePicSeq = iBridUtil.GetStringValue(this.Request.Params["cast_game_pic_seq"]);
				string sItemCategory = iBridUtil.GetStringValue(this.Request.Params["item_category"]);
				string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.Params["battle_log_seq"]);
				string sPageId = iBridUtil.GetStringValue(this.Request.Params["page_id"]);
				string sSaleFlag = iBridUtil.GetStringValue(this.Request.Params["sale"]);

				if (iBridUtil.GetStringValue(Session["PageId"]) != PwViCommConst.PageId.BOSS_BATTLE) {
					Session["PageId"] = sPageId;
					Session["BattleLogSeq"] = sBattleLogSeq;
				}
				
				if (sPageId == PwViCommConst.PageId.TRAP) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(
					sessionMan.root +
					sessionMan.sysType,
					sessionMan.sessionId,
					String.Format("ViewGameItemShop.aspx?item_seq={0}&buy_num={1}&partner_user_seq={2}&partner_user_char_no={3}&cast_game_pic_seq={4}&item_category={5}&page_id={6}&sale={7}",
					sItemSeq,
					sBuyNum,
					sPartnerUserSeq,
					sPartnerUserCharNo,
					sCastGamePicSeq,
					sItemCategory,
					sPageId,
					sSaleFlag)));
				}else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,String.Format("ViewGameItemShop.aspx?item_seq={0}&buy_num={1}&sale={2}",sItemSeq,sBuyNum,sSaleFlag)));
				}
			}
		}
	}
}
