/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �j���p��������}�ӈꗗ
--	Progaram ID		: ListGameTreasure
--
--  Creation Date	: 2011.07.28
--  Creater			: i-Brid
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameTreasure : MobileSocialGameManBase {
	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_TREASURE,ActiveForm);
		}
	}
}
