﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 美人時計一覧
--	Progaram ID		: ListBeautyClock
--
--  Creation Date	: 2011.03.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListBeautyClock:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_BEAUTY_CLOCK,
					ActiveForm);
		}

	}
}
