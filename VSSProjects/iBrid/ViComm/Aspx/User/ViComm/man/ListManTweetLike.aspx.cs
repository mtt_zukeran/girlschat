/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ 会員つぶやきイイネ一覧
--	Progaram ID		: ListSelfManTweetLike
--
--  Creation Date	: 2013.04.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListManTweetLike:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_TWEET_LIKE,this.ActiveForm);
	}
}