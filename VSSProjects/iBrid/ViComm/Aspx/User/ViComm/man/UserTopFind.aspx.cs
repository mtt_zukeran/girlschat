/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: トップメニュー(検索画面付)
--	Progaram ID		: UserTopFind
--
--  Creation Date	: 2010.01.26
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using System.Collections.Generic;

public partial class ViComm_man_UserTopFind:MobileManPageBase {
	private const string MOVIE_FLAG = "UserTopFindMovieFlg";
	private const string NEW_CAST_FLAG = "UserTopFindNewCastFlg";
	private const string WAIT_TYPE = "UserTopFindWaitType";
	private const string CAST_ITEM = "UserTopFindCastItem";
	
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		int iIdx = 0;

		if (!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			CheckRegistIncomplete();
			switch (sessionMan.site.topPageSeekMode) {
				case ViCommConst.CAST_TOP_PICKUP:
					string sTopPagePickupId = string.Empty;
					using (Pickup oPickup = new Pickup()) {
						sTopPagePickupId = oPickup.GetTopPagePickupId(sessionMan.site.siteCd);
					}
					ulong uMode;
					if (sTopPagePickupId.Equals(string.Empty)) {
						uMode = ViCommConst.INQUIRY_CAST_PICKUP;
					} else {
						uMode = ViCommConst.INQUIRY_RECOMMEND;
					}
					sessionMan.ControlList(
							Request,
							uMode,
							ActiveForm);

					break;
				case ViCommConst.CAST_TOP_TALK_MOVIE_PICKUP:
					GetPickupMovie();
					break;
			}

			using (CastAttrType oCastAttrType = new CastAttrType()) {
				DataSet dsAttrType = oCastAttrType.GetList(sessionMan.site.siteCd);
				SelectionList lstCastItem;

				lstWaitType.Items.Add(new MobileListItem("全ての女性","*"));
				
				using (CodeDtl oCodeDtl = new CodeDtl()) {
					DataSet dsGroup = oCodeDtl.GetList("88");
					foreach(DataRow drGroup in dsGroup.Tables[0].Rows) {
						lstWaitType.Items.Add(new MobileListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
					}
				}

				// 入力値の復元を行います。
				string waitType = iBridUtil.GetStringValue(Session[WAIT_TYPE]);
				if(!string.IsNullOrEmpty(waitType)){
					foreach (MobileListItem item in lstWaitType.Items) {
						if (item.Value == waitType) {
							item.Selected = true;
						}
					}
				}				

				Dictionary<string,string> castAttrList = (Dictionary<string,string>)Session[CAST_ITEM];
				
				foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
					if (!drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT) && drAttrType["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lstCastItem = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}",iIdx + 1)) as SelectionList;
						lstCastItem.Items.Add(new MobileListItem(drAttrType["CAST_ATTR_TYPE_FIND_NM"].ToString() + "指定ﾅｼ","*"));

						if (string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
							using (CastAttrTypeValue oAttrTypeValue = new CastAttrTypeValue()) {
								DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionMan.site.siteCd,drAttrType["CAST_ATTR_TYPE_SEQ"].ToString());
								foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
									if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
										lstCastItem.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(),drAttrTypeValue["CAST_ATTR_SEQ"].ToString()));
									}
								}
							}
						} else {
							using (CodeDtl oCodeDtl = new CodeDtl()) {
								DataSet dsGroup = oCodeDtl.GetList(drAttrType["GROUPING_CATEGORY_CD"].ToString());
								foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
									lstCastItem.Items.Add(new MobileListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
								}
							}
						}
						if (drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_RADIO)) {
							lstCastItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
							if (castAttrList == null) {
								lstCastItem.SelectedIndex = 0;
							}
						}

						// 入力値の復元を行います。
						if (castAttrList != null) {
							if (castAttrList.ContainsKey(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString())) {
								string val = castAttrList[drAttrType["CAST_ATTR_TYPE_SEQ"].ToString()];
								foreach (MobileListItem item in lstCastItem.Items) {
									if (item.Value == val) {
										item.Selected = true;
									}
								}
							}
						}
						iIdx++;
					}
				}
			}
			for (int i = iIdx;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
				Panel pnlCastItem = (Panel)frmMain.FindControl(string.Format("pnlCastItem{0}",i + 1)) as Panel;
				pnlCastItem.Visible = false;
			}

			// セッションに値がある場合、入力値を復元します。
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session[MOVIE_FLAG]))) {
				if (iBridUtil.GetStringValue(Session[MOVIE_FLAG]) == "1") {
					// オンラインフラグ
					chkMovie.Items[0].Selected = true;
				}
				if (iBridUtil.GetStringValue(Session[NEW_CAST_FLAG]) == "1") {
					// オンラインフラグ
					chkNewCast.Items[0].Selected = true;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	private void GetPickupMovie() {
		using (TalkMovie oTalkMovie = new TalkMovie()) {
			string sUserSeq,sUserCharNo;
			string sRecNum;
			if (oTalkMovie.GetRandomMovie(sessionMan.site.siteCd,out sUserSeq,out sUserCharNo,out sRecNum) == 0) {
				return;
			}
			if (sessionMan.SetCastDataSetByUserSeq(sUserSeq,sUserCharNo,int.Parse(sRecNum))) {
				sessionMan.seekMode = ViCommConst.INQUIRY_TALK_MOVIE_PICKUP.ToString();
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUrl = "";
		int i;

		int iMovieFlag = 0;
		int iNewCastFlag = 0;
		
		if (chkMovie.SelectedIndex != -1) {
			iMovieFlag = 1;
		}
		if (chkNewCast.SelectedIndex != -1) {
			iNewCastFlag = 1;
		}

		sUrl = string.Format("ListFindResult.aspx?category={0}&online={1}&movie={2}&newcast={3}&waittype={4}",sessionMan.currentCategoryIndex,1,iMovieFlag,iNewCastFlag,lstWaitType.Items[lstWaitType.SelectedIndex].Value);

		// 選択した値をセッションにいれておきます。
		Session[MOVIE_FLAG] = iMovieFlag;
		Session[NEW_CAST_FLAG] = iNewCastFlag;
		Session[WAIT_TYPE] = lstWaitType.Items[lstWaitType.SelectedIndex].Value;
		
		SelectionList lstCastItem;
		Panel pnlCastItem;

		i = 1;
		string sGroupingFlag;
		Dictionary<string,string> castAttrList = new Dictionary<string,string>();
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			DataSet dsAttrType = oCastAttrType.GetList(sessionMan.site.siteCd);
			foreach (DataRow drAttrType in dsAttrType.Tables[0].Rows) {
				if (!drAttrType["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
					pnlCastItem = (Panel)frmMain.FindControl(string.Format("pnlCastItem{0}",i)) as Panel;
					lstCastItem = (SelectionList)frmMain.FindControl(string.Format("lstCastItem{0}",i)) as SelectionList;
					sGroupingFlag = "";
					if (!string.IsNullOrEmpty(drAttrType["GROUPING_CATEGORY_CD"].ToString())) {
						sGroupingFlag = "1";
					}
					if ((pnlCastItem != null) && (lstCastItem != null) && (pnlCastItem.Visible)) {
						sUrl = sUrl + string.Format("&item{0:D2}={1}&typeseq{0:D2}={2}&group{0:D2}={3}",i,lstCastItem.Items[lstCastItem.SelectedIndex].Value,drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),sGroupingFlag);
						// 入力値復元用の値保存
						castAttrList.Add(drAttrType["CAST_ATTR_TYPE_SEQ"].ToString(),lstCastItem.Items[lstCastItem.SelectedIndex].Value);
					}
					i++;
				}
			}
		}
		Session[CAST_ITEM] = castAttrList;

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
	}

	private void CheckRegistIncomplete() {
		if (sessionMan.userMan.registIncompleteFlag == 1) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl("ModifyUserProfile.aspx"));
		}
	}
}
