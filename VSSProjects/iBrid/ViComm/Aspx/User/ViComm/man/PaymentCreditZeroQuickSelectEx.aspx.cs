/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ڼޯ�QUICK�\����(ZERO�ڼޯ�)(����)
--	Progaram ID		: PaymentCreditZeroQuickSelectEx
--  Creation Date	: 2014.10.08
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_PaymentCreditZeroQuickSelectEx:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);
		int iSalesAmt;
		string sSid = "";

		if (string.IsNullOrEmpty(sSalesAmt)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		} else if (!int.TryParse(sSalesAmt,out iSalesAmt)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_ZERO,
					ViCommConst.SETTLE_CREDIT_PACK,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					string.Empty,
					string.Empty,
					out sSid);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
		string sSettleUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_CREDIT_PACK)) {
				sSettleUrl = string.Format(oSiteSettle.continueSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sessionMan.userMan.tel);
			}
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
			string.Format("PaymentCreditZeroQuick.aspx?sid={0}&money={1}",sSid,sSalesAmt)));
	}
}
