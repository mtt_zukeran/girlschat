/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: FanClubステータス確認
--	Progaram ID		: ViewFanClubStatus
--
--  Creation Date	: 2013.02.02
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewFanClubStatus:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_FANCLUB_STATUS,this.ActiveForm);
		}
	}
}
