/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プレゼントメールアイテム一覧
--	Progaram ID		: ListPresentMailItem
--
--  Creation Date	: 2014.12.02
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListPresentMailItem:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			this.CreateList();

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_PRESENT_MAIL_ITEM,this.ActiveForm)) {
				lstPresentMailItem.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sMailDataSeq = iBridUtil.GetStringValue(Request.QueryString["data"]);
		string sPresentMailItemSeq = lstPresentMailItem.Selection.Value;
		string sUrl = string.Format("TxPresentMailConfirm.aspx?data={0}&item={1}",sMailDataSeq,sPresentMailItemSeq);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sUrl));
	}

	virtual protected void CreateList() {
		SelectionList lstPresentMailItem = (SelectionList)frmMain.FindControl("lstPresentMailItem") as SelectionList;
		
		DataSet oDataSet = null;
		
		using (PresentMailItem oPresentMailItem = new PresentMailItem()) {
			oDataSet = oPresentMailItem.GetCurrnetPresentMailItemList(sessionMan.site.siteCd,iBridUtil.GetStringValue(Request.QueryString["sort"]));
		}
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow dr in oDataSet.Tables[0].Rows) {
				lstPresentMailItem.Items.Add(new MobileListItem(string.Format("{0}: +{1}pt",dr["PRESENT_MAIL_ITEM_NM"].ToString(),dr["CHARGE_POINT"].ToString()),dr["PRESENT_MAIL_ITEM_SEQ"].ToString()));
			}
		}
	}
}