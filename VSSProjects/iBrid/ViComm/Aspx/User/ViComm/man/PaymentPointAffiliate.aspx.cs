/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: |CgAtBGCgx¥(ASPL)
--	Progaram ID		: PaymentPointAffiliate
--
--  Creation Date	: 2010.09.27
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentPointAffiliate:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sSid = "";
		string sSettleUrl = "";
		string sSettleCompanyCd = iBridUtil.GetStringValue(Request.QueryString["aspsettlecom"]);
		string sAspAdCd = iBridUtil.GetStringValue(Request.QueryString["aspadcd"]);

		using (SiteSettle oSiteSettle = new SiteSettle())
		using (SettleLog oLog = new SettleLog()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,sSettleCompanyCd,ViCommConst.SETTLE_POINT_AFFILIATE)) {
				if (oSiteSettle.notCreateSettleReqLogFlag == 0) {
					oLog.LogSettleRequest(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						sSettleCompanyCd,
						ViCommConst.SETTLE_POINT_AFFILIATE,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						0,
						0,
						string.Empty,
						sAspAdCd,
						out sSid);
				}

				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				string sId = string.Empty;

				if (sSettleCompanyCd.Equals(ViCommConst.SETTLE_CORP_ACT_SYSTEM)) {
					sId = sessionMan.userMan.beforeSystemId;
				} else {
					sId = sessionMan.userMan.loginId;
				}
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sAspAdCd,sId,sSid);
			}
		}
		Response.Redirect(sSettleUrl);
	}
}
