﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ 詳細
--	Progaram ID		: ViewBlog
--
--  Creation Date	: 2011.04.11
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewBlog : MobileBlogManBase{
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
		sessionMan.ControlList(Request, ViCommConst.INQUIRY_EXTENSION_BLOG, ActiveForm);
	}
}
