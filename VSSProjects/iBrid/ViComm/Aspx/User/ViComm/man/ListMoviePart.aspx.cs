/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ムービー選択
--	Progaram ID		: ListMoviePart
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListMoviePart:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			if (sessionMan.logined) {
				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			}

			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_TALK_MOVIE_PART,
					ActiveForm);

			using (TalkMovie oTalkMovie = new TalkMovie()) {
				DataSet dsDataSet = oTalkMovie.GetPartList(sessionMan.site.siteCd,iBridUtil.GetStringValue(Request.QueryString["movieseq"]));
				int iIdx = ViCommConst.DATASET_TALK_MOVIE;

				ParseHTML oParseMan = sessionMan.parseContainer;
				if (dsDataSet.Tables[0].Rows.Count > 0) {
					oParseMan.loopCount[iIdx] = dsDataSet.Tables[0].Rows.Count;
					oParseMan.parseUser.dataTable[iIdx] = dsDataSet.Tables[0];
				} else {
					oParseMan.currentRecPos[iIdx] = -1;
					oParseMan.parseUser.SetDataRow(iIdx, null);
				}
			}
		}
	}

}
