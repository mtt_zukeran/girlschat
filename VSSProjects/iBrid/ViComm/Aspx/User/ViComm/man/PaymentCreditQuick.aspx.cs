/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ڼޯĎx��(Quick)
--	Progaram ID		: PaymentCreditQuick
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Net;
using System.IO;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCreditQuick:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			DataSet ds;
			ds = GetPackDataSet(ViCommConst.SETTLE_CREDIT_PACK,500);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstCreditPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstCreditPack.Items[lstCreditPack.SelectedIndex].Value;
		string sSid = "";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_BIGSUN,
					ViCommConst.SETTLE_CREDIT_PACK,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					string.Empty,
					string.Empty,
					out sSid);
			}
		}
		string sSettleUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_BIGSUN,ViCommConst.SETTLE_CREDIT_PACK)) {
				sSettleUrl = string.Format(oSiteSettle.continueSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt);
			}
		}

		if (TransQuick(sSettleUrl)) {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"0");
			}
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_CREDIT_QUICK_OK));
		} else {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,sessionMan.userMan.userSeq,int.Parse(sSalesAmt),0,"9");
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_CREDIT_QUICK_NG));
		}
	}


	public bool TransQuick(string pUrl) {
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sRes.Equals("Success_order");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransQuick",pUrl);
			return false;
		}
	}
}
