/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���[���Y�t�摜�Ǘ�
--	Progaram ID		: ViewMailPicConf
--
--  Creation Date	: 2009.08.19
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewMailPicConf:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.ControlList(Request,ViCommConst.INQUIRY_NONE_DISP_PROFILE,ActiveForm)) {
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListMailPicConf.aspx"));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sFileSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
		DeletePicture(sFileSeq);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_USER_MAN_MAIL_PIC));
	}

	private void UpdateProfilePicture(string pPicSeq,string pDelFlag,string pProfilePicFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_MAN_PIC_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sessionMan.site.siteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,sessionMan.userMan.userSeq);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private void DeletePicture(string pFileSeq) {
		UpdateProfilePicture(pFileSeq,"1","0");
	}
}