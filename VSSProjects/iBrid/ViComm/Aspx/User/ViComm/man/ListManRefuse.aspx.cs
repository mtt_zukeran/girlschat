/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���ۃ��X�g�ꗗ
--	Progaram ID		: ListManRefuse
--
--  Creation Date	: 2010.05.10
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListManRefuse:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_REFUSE,
					ActiveForm);
		}
	}

}
