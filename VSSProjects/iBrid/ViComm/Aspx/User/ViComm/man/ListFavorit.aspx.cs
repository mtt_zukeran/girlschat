/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り一覧
--	Progaram ID		: ListFavorit
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListFavorit:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sGrpSeq = iBridUtil.GetStringValue(Request.QueryString["grpseq"]);
			SetSelectBox(sGrpSeq);
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_FAVORIT,
					ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_SEARCH] != null) {
				cmdSearch_Click(sender,e);
			}
		}
	}

	private void SetSelectBox(string pGrpSeq) {
		int iAllUserCount = 0;
		int iNullUserCount = 0;
		string sListText = string.Empty;

		using (Favorit oFavorit = new Favorit()) {
			iAllUserCount = oFavorit.GetSelfFavoritCount(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				ViCommConst.MAN
			);
		}

		iNullUserCount = iAllUserCount;

		DataSet oDataSet = GetFavoritGroup();
		Regex regex2 = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			iNullUserCount = iNullUserCount - int.Parse(dr["USER_COUNT"].ToString());
			sListText = string.Format("{0}({1})",regex2.Replace(iBridUtil.GetStringValue(dr["FAVORIT_GROUP_NM"]),string.Empty),iBridUtil.GetStringValue(dr["USER_COUNT"]));
			lstFavoritGroup.Items.Add(new MobileListItem(sListText,iBridUtil.GetStringValue(dr["FAVORIT_GROUP_SEQ"])));
		}

		lstFavoritGroup.Items.Insert(0,new MobileListItem(string.Format("すべて({0})",iAllUserCount.ToString()),string.Empty));
		lstFavoritGroup.Items.Insert(1,new MobileListItem(string.Format("ｸﾞﾙｰﾌﾟ未設定({0})",iNullUserCount.ToString()),"null"));

		foreach (MobileListItem item in lstFavoritGroup.Items) {
			if (item.Value.Equals(pGrpSeq)) {
				item.Selected = true;
			}
		}
	}

	private DataSet GetFavoritGroup() {
		DataSet oDataSet;
		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.UserSeq = sessionMan.userMan.userSeq;
		oCondition.UserCharNo = sessionMan.userMan.userCharNo;
		oCondition.SexCd = ViCommConst.MAN;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oDataSet = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,1,99);
		}

		return oDataSet;
	}

	protected void cmdSearch_Click(object sender,EventArgs e) {
		string sGoto = iBridUtil.GetStringValue(Request.Form["goto"]);
		string sFavoritGroupSeq = lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value;

		if (!string.IsNullOrEmpty(sFavoritGroupSeq) && sGoto.Contains("ListFavorit.aspx")) {
			if (sGoto.Contains("&")) {
				sGoto += "&grpseq=" + sFavoritGroupSeq;
			} else {
				sGoto += "?grpseq=" + sFavoritGroupSeq;
			}
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sGoto));
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sAddQuery = string.Format("&sortex={0}",this.Request.Form["sortex"]);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
												   "ListFavorit.aspx?" + RemoveSortEx(Request.QueryString.ToString()) + sAddQuery));
	}

	private string RemoveSortEx(string pQueryString) {
		Regex regex = new Regex("(&|\\?)sortex=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(pQueryString,string.Empty);
	}
}
