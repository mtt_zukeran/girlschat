/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: [ðBOX
--	Progaram ID		: ListMailHistory
--
--  Creation Date	: 2010.09.06
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using ViComm;
using iBridCommLib;
using MobileLib;
using ViComm.Extension.Pwild;
//using ViComm;

public partial class ViComm_man_ListMailHistory:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_MAIL_HISTORY,ActiveForm);

			ParseHTML oParseMan = sessionMan.parseContainer;
			if (oParseMan.currentRecPos[ViCommConst.DATASET_MAIL] == -1) {
				string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
				string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
				sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,1);
				oParseMan.parseUser.currentTableIdx = ViCommConst.DATASET_CAST;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}

		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDelTxRx(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,mailSeqList.ToArray());
			}
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("mail_delete_count",mailSeqList.Count.ToString());
			oParam.Add("loginid",iBridUtil.GetStringValue(this.Request.QueryString["loginid"]));
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_DELETED_MAIL_HISTORY,oParam);
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   "ListMailHistory.aspx?" + Request.QueryString));
		}
	}
}
