﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性用お宝コンプカレンダー一覧
--	Progaram ID		: ListGameCompCalendar
--
--  Creation Date	: 2011.07.28
--  Creater			: i-Brid
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web.UI.MobileControls;
using System.Collections.Specialized;
using System.Web;

public partial class ViComm_man_ListGameCompCalendar : MobileSocialGameManBase {
	virtual protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);

		if (!IsPostBack) {

			string sSearch_flg = iBridUtil.GetStringValue(Request.QueryString["search_flg"]);
			
			if (sSearch_flg.Equals(string.Empty)) {

				string sStartDisplayDay = iBridUtil.GetStringValue(Request.QueryString["start_display_day"]);
				string sEndDisplayDay = iBridUtil.GetStringValue(Request.QueryString["end_display_day"]);

				if (string.IsNullOrEmpty(sStartDisplayDay) && string.IsNullOrEmpty(sEndDisplayDay)) {
					Session["year"] = null;
					Session["month"] = null;
				}
			}
			
			SetSelectionListItem();

			if (!sSearch_flg.Equals(string.Empty)) {
				cmdSubmit_Click(sender,e);
			}
			
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_COMP_CALENDAR,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUrl = string.Empty;
		string sYear = string.Empty;
		string sMonth = string.Empty;
		string sStartDisplayDay = string.Empty;
		string sEndDisplayDay = string.Empty;
		string sPageNo = string.Empty;
		int iToday = 0;
		System.Text.Encoding enc = System.Text.Encoding.GetEncoding("Shift_JIS");
		
		if (lstYear.SelectedIndex >= 0) {
			sYear = lstYear.Selection.Value;
			Session["year"] = lstYear.Selection.Text;
		}

		if (lstMonth.SelectedIndex >= 0) {
			sMonth = lstMonth.Selection.Value;
			Session["month"] = lstMonth.Selection.Text;
		}

		if (!string.IsNullOrEmpty(sYear) && !string.IsNullOrEmpty(sMonth)) {
			int iLastday = DateTime.DaysInMonth(int.Parse(sYear),int.Parse(sMonth));
			sStartDisplayDay = sYear + "/" + sMonth + "/01";
			sEndDisplayDay = sYear + "/" + sMonth + "/" + iLastday.ToString();
		}

		if (iBridUtil.GetStringValue(this.Request.QueryString["search_flg"]).Equals(ViCommConst.FLAG_ON_STR)) {
			iToday = int.Parse(DateTime.Now.Day.ToString("d"));

			decimal dCount = (iToday - 1) / PwViCommConst.GameCompCalendarRowCountOfPage;
			int iPageNo = (int)dCount + 1;

			if (iPageNo > 0) {
				sPageNo = iPageNo.ToString();
			}
		}

		sUrl = string.Format(
			"ListGameCompCalendar.aspx?start_display_day={0}&end_display_day={1}&pageno={2}",
			HttpUtility.UrlEncode(sStartDisplayDay,enc),
			HttpUtility.UrlEncode(sEndDisplayDay,enc),
			sPageNo
		);

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
	}
	
	private void SetSelectionListItem() {
		int iGameStartYear = this.GetGameStartYear(this.sessionMan.site.siteCd);

		for (int i = iGameStartYear;i <= DateTime.Now.Year;i++) {
			lstYear.Items.Add(new MobileListItem(i.ToString(),i.ToString()));
		}

		foreach (MobileListItem oItem in this.lstYear.Items) {
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["year"]))) {
				if (oItem.Text.Equals(DateTime.Now.ToString("yyyy"))) {
					this.lstYear.SelectedIndex = oItem.Index;
				}
			} else {
				if (oItem.Text.Equals(iBridUtil.GetStringValue(Session["year"]))) {
					this.lstYear.SelectedIndex = oItem.Index;
				}
			}
		}
		
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString(),i.ToString("00")));
		}

		foreach (MobileListItem oItem in this.lstMonth.Items) {
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["month"]))) {
				if (oItem.Text.Equals(DateTime.Now.ToString("%M"))) {
					this.lstMonth.SelectedIndex = oItem.Index;
				}
			}else {
				if (oItem.Text.Equals(iBridUtil.GetStringValue(Session["month"]))) {
					this.lstMonth.SelectedIndex = oItem.Index;
				}
			}
		}
	}

	private int GetGameStartYear(string pSiteCd) {
		DateTime dtGameStartDate;

		using (SocialGame oSocialGame = new SocialGame()) {
			dtGameStartDate = oSocialGame.GetGameStartDate(pSiteCd);
		}

		return dtGameStartDate.Year;
	}
}