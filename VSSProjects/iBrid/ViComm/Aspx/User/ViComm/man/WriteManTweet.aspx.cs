/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員つぶやき書き込み/完了
--	Progaram ID		: WriteManTweet
--
--  Creation Date	: 2013.01.23
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

using System.Web;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteManTweet:MobileManPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.errorMessage = string.Empty;
			tagTweetWrite.Visible = true;
			txtTweet.Visible = true;
			tagTweetWriteSub.Visible = true;
			tagTweetComplete.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);

		bool bOk = true;

		string sSiteCd = sessionMan.site.siteCd;
		string sUserSeq = sessionMan.userMan.userSeq;
		string sUserCharNo = sessionMan.userMan.userCharNo;
		
		bool bUseLimit = true;

		// クレジット自動決済
		using (CreditAutoSettleStatus oCreditAutoSettleStatus = new CreditAutoSettleStatus()) {
			bUseLimit = !oCreditAutoSettleStatus.IsExistAvailable(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
		}

		// 無制限イベント判定
		// (イベント開催状況＆初回入金日からの経過日数)
		if (bUseLimit) {
			UserMan oUserMan = new UserMan();
			bUseLimit = !oUserMan.IsTargetTweetUnlimited(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
		}

		if (bUseLimit) {
			using (Site oSite = new Site())
			using (ManTweet oManTweet = new ManTweet()) {
				oSite.GetOne(sSiteCd);
				
				int iManWriteBbsLimit = 0;
				int iManWriteIntervalMin = 0;

				using (RichinoRank oRichinoRank = new RichinoRank()) {
					DataSet oDataSet = oRichinoRank.GetUserRichinoRank(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
					
					if (oDataSet.Tables[0].Rows.Count > 0) {
						iManWriteBbsLimit = int.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["MAN_TWEET_DAILY_COUNT"]));
						iManWriteIntervalMin = int.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["MAN_TWEET_INTERVAL_MIN"]));
					}
				}
				
				if (iManWriteBbsLimit == 0) {
					if (sessionMan.userMan.totalReceiptAmt > 0) {
						iManWriteBbsLimit = oSite.payManWriteBbsLimit;
					} else {
						iManWriteBbsLimit = oSite.manWriteBbsLimit;
					}
				}

				if (iManWriteIntervalMin == 0) {
					if (sessionMan.userMan.totalReceiptAmt > 0) {
						iManWriteIntervalMin = oSite.payManWriteIntervalMin;
					} else {
						iManWriteIntervalMin = oSite.manWriteIntervalMin;
					}
				}

				// 書き込み件数のチェック
				if (iManWriteBbsLimit >= 0) {
					int iTweetCnt = oManTweet.GetTweetCntEachDay(sSiteCd,sUserSeq,DateTime.Now.Date);
					if (iTweetCnt >= iManWriteBbsLimit) {
						bOk = false;
						sessionMan.errorMessage += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAN_TWEET_LIMIT);
						
						return;
					}
				}
				
				DateTime? oLastTweetDate = oManTweet.GetLastTweetDate(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

				if ((oLastTweetDate != null)
					&& (oLastTweetDate.Value.AddMinutes(iManWriteIntervalMin) >= DateTime.Now)) {
					bOk = false;
					sessionMan.errorMessage += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAN_TWEET_INTERVAL_SEC);
					
					return;
				}
			}
		}

		if (txtTweet.Text.Equals("")) {
			bOk = false;
			//本文を入力して下さい。 
			sessionMan.errorMessage += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}
		if (iMaxLength != 0 && txtTweet.Text.Length > iMaxLength) {
			bOk = false;
			int iOverLength = txtTweet.Text.Length - iMaxLength;
			sessionMan.errorMessage += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iOverLength.ToString(),iMaxLength.ToString());
		}

		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			string sNGWord;
			if (sessionMan.ngWord.VaidateDoc(txtTweet.Text,out sNGWord) == false) {
				bOk = false;
				sessionMan.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		string sTweetText = txtTweet.Text;
		string sManTweetSeq = string.Empty;
		string sResult = string.Empty;

		if (bOk == false) {
			return;
		} else {
			using (ManTweet oManTweet = new ManTweet()) {
				sTweetText = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtTweet.Text));
				oManTweet.WriteManTweet(sSiteCd,sUserSeq,sUserCharNo,sTweetText,out sManTweetSeq,out sResult);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				using (ManNews oManNews = new ManNews()) {
					oManNews.LogManNews(sessionMan.site.siteCd,sessionMan.userMan.userSeq,PwViCommConst.ManNewsType.WRITE_MAN_TWEET);
				}

				tagTweetWrite.Visible = false;
				txtTweet.Visible = false;
				tagTweetWriteSub.Visible = false;
				tagTweetComplete.Visible = true;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
			}
		}
	}
}