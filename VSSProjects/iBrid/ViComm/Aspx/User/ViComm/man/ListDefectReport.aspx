<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListDefectReport.aspx.cs" Inherits="ViComm_man_ListDefectReport" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileTextBox ID="txtKeyword" runat="server" MaxLength="30" Size="20" BreakAfter="true"></ibrid:iBMobileTextBox>
		<mobile:SelectionList ID="lstCategory" Runat="server" SelectType="DropDown" BreakAfter="true">
			<Item Text="--�ú��--" Value="" />
		</mobile:SelectionList>
		$PGM_HTML02;
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
