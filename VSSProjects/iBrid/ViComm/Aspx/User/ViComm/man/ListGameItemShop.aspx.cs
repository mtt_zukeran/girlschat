/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���Ѽ���� ���шꗗ
--	Progaram ID		: ListGameItem
--
--  Creation Date	: 2011.07.22
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_man_ListGameItemShop:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sPageId = iBridUtil.GetStringValue(this.Request.Params["page_id"]);
		string sStageSeq = iBridUtil.GetStringValue(this.Request.Params["stage_seq"]);

		if (!sPageId.Equals(string.Empty)) {
			Session["PageId"] = sPageId;
		}

		if (!sStageSeq.Equals(string.Empty)) {
			Session["StageSeq"] = sStageSeq;
		}
				
		if(!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_SHOP_LIST,ActiveForm);
		} else {
			string RNum;
			if(this.getRNum(out RNum)) {
				string sItemSeq = this.Request.Params["GOTOLINK_" + RNum];
				string sBuyNum = this.Request.Params["buyItemNum_" + RNum];
				
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,String.Format("ViewGameItemShop.aspx?item_seq={0}&buy_num={1}",sItemSeq,sBuyNum)));
			}
		}
	}
	
	private bool getRNum(out string RNum) {
		string index;
		
		for(int i = 1; i <= 10; i++) {
			index = i.ToString();
			if(this.Request.Params[ViCommConst.BUTTON_GOTO_LINK + "_" + index] != null) {
				RNum = index;
				return true;
			}
		}

		RNum = null;
		return false;
	}
}
