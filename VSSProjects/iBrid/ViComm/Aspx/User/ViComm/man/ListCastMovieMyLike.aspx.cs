﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: いいねした動画一覧
--	Progaram ID		: ListCastMovieMyLike
--  Creation Date	: 2016.10.06
--  Creater			: M&TT Zukeran
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListCastMovieMyLike:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_MOVIE_MY_LIKE,this.ActiveForm);
		}
	}
}
