/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: SSAD�x��
--	Progaram ID		: PaymentSSAD
--
--  Creation Date	: 2008.03.02
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentSSAD:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSid = "";

		using (SettleLog oLog = new SettleLog()) {
			oLog.LogSettleRequest(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				ViCommConst.SETTLE_CORP_SEASCAPE,
				ViCommConst.SETTLE_POINT_AFFILIATE,
				ViCommConst.SETTLE_STAT_SETTLE_NOW,
				0,
				0,
				"",
				"",
				out sSid);
		}
		string sSettleUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_SEASCAPE,ViCommConst.SETTLE_POINT_AFFILIATE)) {
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sSid,sessionMan.userMan.userSeq);
			}
		}
		Response.Redirect(sSettleUrl);
	}
}
