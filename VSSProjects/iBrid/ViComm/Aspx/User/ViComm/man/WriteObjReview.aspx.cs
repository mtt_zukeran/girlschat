/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝レビュー書込
--	Progaram ID		: WriteObjReview
--
--  Creation Date	: 2012.04.21
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteObjReview:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sObjSeq = iBridUtil.GetStringValue(Request.Params["objseq"]);

		if (string.IsNullOrEmpty(sObjSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		IDictionary<string,string> oParam = new Dictionary<string,string>();
		oParam.Add("objseq",sObjSeq);
		oParam.Add("listpageno",iBridUtil.GetStringValue(Request.Params["listpageno"]));
		oParam.Add("attrtypeseq",iBridUtil.GetStringValue(Request.Params["attrtypeseq"]));
		oParam.Add("attrseq",iBridUtil.GetStringValue(Request.Params["attrseq"]));
		oParam.Add("used",iBridUtil.GetStringValue(Request.Params["used"]));
		oParam.Add("unused",iBridUtil.GetStringValue(Request.Params["unused"]));
		oParam.Add("ranktype",iBridUtil.GetStringValue(Request.Params["ranktype"]));
		oParam.Add("bkm",iBridUtil.GetStringValue(Request.Params["bkm"]));
		oParam.Add("listloginid",iBridUtil.GetStringValue(Request.Params["listloginid"]));
		oParam.Add("listtype",iBridUtil.GetStringValue(Request.Params["listtype"]));

		//購入済チェック
		using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
			DataSet dsHistory = oObjUsedHistory.GetOneDataSet(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sObjSeq,
				string.Empty
			);

			if (dsHistory.Tables[0].Rows.Count == 0) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_WRITE_OBJ_REVIEW_ERROR01,oParam);
			}
		}

		//レビュー済チェック
		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			int iReviewCount = oObjReviewHistory.GetRecCount(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sObjSeq
			);

			if (iReviewCount > 0) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_WRITE_OBJ_REVIEW_ERROR02,oParam);
			}
		}

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;
			pnlCompleteNoPt.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e,sObjSeq);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		int iGoodStar;
		bool bGoodStar = int.TryParse(iBridUtil.GetStringValue(Request.Form["good_star"]),out iGoodStar);

		if (bGoodStar == false) {
			sessionMan.errorMessage = "評価を選択してください";
			return;
		}

		if (!txtCommentDoc.Text.Equals(string.Empty)) {
			string sNGWord = string.Empty;

			if (txtCommentDoc.Text.Length > 500) {
				sessionMan.errorMessage = "ｺﾒﾝﾄが長すぎます";
				return;
			}

			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}

			if (!sessionMan.ngWord.VaidateDoc(txtCommentDoc.Text,out sNGWord)) {
				sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		ViewState["GOOD_STAR"] = iGoodStar.ToString();
		ViewState["COMMENT_DOC"] = txtCommentDoc.Text;

		lblCommentDoc.Text = HttpUtility.HtmlEncode(txtCommentDoc.Text).Replace(System.Environment.NewLine,"<br>");
		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
		pnlCompleteNoPt.Visible = false;
	}

	protected void cmdTx_Click(object sender,EventArgs e,string sObjSeq) {
		sessionMan.errorMessage = string.Empty;
		int iGoodStar;
		bool bGoodStar = int.TryParse(iBridUtil.GetStringValue(ViewState["GOOD_STAR"]),out iGoodStar);
		string sCommentDoc = iBridUtil.GetStringValue(ViewState["COMMENT_DOC"]);
		string sResult = string.Empty;

		using (ObjReviewHistory oObjReviewHistory = new ObjReviewHistory()) {
			sResult = oObjReviewHistory.WriteObjReview(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sObjSeq,
				iGoodStar,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sCommentDoc)),
				0
			);
		}

		if (sResult.Equals(PwViCommConst.WriteObjReviewResult.RESULT_OK)) {
			pnlWrite.Visible = false;
			pnlConfirm.Visible = false;
			pnlCompleteNoPt.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
	}

	protected virtual void RestrictDocLength(ref string doc) {
		if (doc.Length > 1000) {
			doc = SysPrograms.Substring(doc,1000);
		}
	}
}
