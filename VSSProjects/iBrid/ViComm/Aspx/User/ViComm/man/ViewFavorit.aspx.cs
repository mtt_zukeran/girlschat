/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り詳細
--	Progaram ID		: ViewFavorit
--
--  Creation Date	: 2012.10.04
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewFavorit:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		DataSet oDataSet;
		string sPartnerUserSeq = string.Empty;
		string sPartnerUserCharNo = string.Empty;
		string sFavoritGroupSeq = string.Empty;

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(this.Request,ViCommConst.INQUIRY_FAVORIT,this.ActiveForm,out oDataSet);

		if (oDataSet.Tables[0].Rows.Count != 0) {
			sPartnerUserSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["USER_SEQ"]);
			sPartnerUserCharNo = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"]);
			sFavoritGroupSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["FAVORIT_GROUP_SEQ"]);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			SetSelectBox(sFavoritGroupSeq);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo);
			}
		}
	}

	private void SetSelectBox(string sFavoritGroupSeq) {
		int iNullUserCount = 0;
		string sListText = string.Empty;

		lstFavoritGroup.Items.Clear();

		using (Favorit oFavorit = new Favorit()) {
			iNullUserCount = oFavorit.GetSelfFavoritCount(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				ViCommConst.MAN
			);
		}

		DataSet oDataSet = GetFavoritGroup();
		Regex regex2 = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			iNullUserCount = iNullUserCount - int.Parse(dr["USER_COUNT"].ToString());
			sListText = string.Format("{0}({1})",regex2.Replace(iBridUtil.GetStringValue(dr["FAVORIT_GROUP_NM"]),string.Empty),iBridUtil.GetStringValue(dr["USER_COUNT"]));
			lstFavoritGroup.Items.Add(new MobileListItem(sListText,iBridUtil.GetStringValue(dr["FAVORIT_GROUP_SEQ"])));
		}

		lstFavoritGroup.Items.Insert(0,new MobileListItem(string.Format("ｸﾞﾙｰﾌﾟ未設定({0})",iNullUserCount.ToString()),string.Empty));

		foreach (MobileListItem item in lstFavoritGroup.Items) {
			if (item.Value.Equals(sFavoritGroupSeq)) {
				item.Selected = true;
			}
		}
	}

	private DataSet GetFavoritGroup() {
		DataSet oDataSet;
		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.UserSeq = sessionMan.userMan.userSeq;
		oCondition.UserCharNo = sessionMan.userMan.userCharNo;
		oCondition.SexCd = ViCommConst.MAN;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oDataSet = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,1,99);
		}

		return oDataSet;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult;
		string[] sPartnerUserSeq = new string[] { pPartnerUserSeq };
		string[] sPartnerUserCharNo = new string[] { pPartnerUserCharNo };

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			sResult = oFavoritGroup.SetFavoritGroup(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sPartnerUserSeq,
				sPartnerUserCharNo,
				lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			string sFavoritGroupNm = lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Text;
			sFavoritGroupNm = System.Text.RegularExpressions.Regex.Replace(sFavoritGroupNm,"[(][0-9]+[)]","");
			sFavoritGroupNm = HttpUtility.UrlEncode(sFavoritGroupNm,Encoding.GetEncoding(932));

			UrlBuilder oUrlBuilder = new UrlBuilder("ViewFavorit.aspx");

			foreach (string sKey in Request.QueryString.AllKeys) {
				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString[sKey])) && !sKey.Equals("edit") && !sKey.Equals("grpnm")) {
					oUrlBuilder.AddParameter(sKey,iBridUtil.GetStringValue(Request.QueryString[sKey]));
				}
			}

			oUrlBuilder.AddParameter("edit","1");
			oUrlBuilder.AddParameter("grpnm",sFavoritGroupNm);
			RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}
