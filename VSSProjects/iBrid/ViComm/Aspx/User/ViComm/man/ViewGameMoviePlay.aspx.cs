/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｹﾞｰﾑ動画再生
--	Progaram ID		: ViewGameMoviePlay
--
--  Creation Date	: 2011.09.05
--  Creater			: PW A.Taba
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameMoviePlay:MobileSocialGameManBase {
	protected const string PARAM_NM_OBJ_MOVIE = "movieseq";
	protected const string PARAM_NM_PLAY_MOVIE = "playmovie";

	virtual protected void Page_Load(object sender,EventArgs e) {

		string sMovieSeq = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_OBJ_MOVIE]);
		string sPlayMovie = iBridUtil.GetStringValue(this.Request.Params[PARAM_NM_PLAY_MOVIE]);
		string sCastSeq = iBridUtil.GetStringValue(this.Request.Params["partner_user_seq"]);
		string sCastCharNo = iBridUtil.GetStringValue(this.Request.Params["partner_user_char_no"]);
		string sCastUserMovieSeq = iBridUtil.GetStringValue(this.Request.Params["cast_user_movie_seq"]);
		string sUserRecNo = iBridUtil.GetStringValue(this.Request.Params["userrecno"]);
		string sLoginId = iBridUtil.GetStringValue(this.Request.Params["loginid"]);
		string sCategory = iBridUtil.GetStringValue(this.Request.Params["category"]);
		string sNocrypt = iBridUtil.GetStringValue(this.Request.Params["nocrypt"]);
		string sSeekmode = iBridUtil.GetStringValue(this.Request.Params["seekmode"]);
		string sObjseq = iBridUtil.GetStringValue(this.Request.Params["objseq"]);

		this.CheckGameRefusedByPartner(sCastSeq,sCastCharNo);
		
		if (!IsPostBack) {
			if (sessionMan.logined) {

				bool bDownload = false;
				
				if (!string.IsNullOrEmpty(sMovieSeq) && sPlayMovie.Equals(ViCommConst.FLAG_ON_STR)) {
					bDownload = Download(sMovieSeq);
				} else {

					if (
					string.IsNullOrEmpty(sCastSeq) ||
					string.IsNullOrEmpty(sCastCharNo) ||
					string.IsNullOrEmpty(sCastUserMovieSeq) ||
					string.IsNullOrEmpty(sUserRecNo) ||
					string.IsNullOrEmpty(sLoginId) ||
					string.IsNullOrEmpty(sCategory) ||
					string.IsNullOrEmpty(sNocrypt) ||
					string.IsNullOrEmpty(sSeekmode) ||
					string.IsNullOrEmpty(sObjseq)
					) {
						RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
					}
					
					RedirectViewGameCastMovie();
				}

				if (!bDownload) {
					Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
					sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CAST_MOVIE_PLAY,ActiveForm);
				}
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("RegistUserRequestByTermIdGame.aspx"));
			}
		}
	}

	//動画をダウンロードする処理です。
	private bool Download(string pMovieSeq) {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;
		string sCastSeq,sCastCharNo;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,pMovieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode,out sCastSeq,out sCastCharNo);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}

		CheckMovieBalance(sCastSeq,sCastCharNo,pMovieSeq);

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (lSize > 0) {
				MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.OPERATOR);
				return true;
			} else {
				return false;
			}
		}
	}

	//動画が見れる状態かチェックし、必要なアイテムを消費させます。見れない場合別ページへリダイレクトします。
	private void CheckMovieBalance(string sCastSeq,string sCastCharNo,string sMovieSeq) {
		bool bDupUsed = false;
		int iGameItemGetChargeFlg = ViCommConst.FLAG_OFF;

		if (!MovieHelper.IsRangeRequest()) {
			sessionMan.errorMessage = "";
			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				bDupUsed = oObjUsedHistory.GetOne(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
					sMovieSeq
				);
			}

			//一度でも見た動画だった場合、アイテム消費をスキップします。
			if (bDupUsed == false) {
				int iTicketPossessionCount = this.GetTicketPossessionCount();

				if (iTicketPossessionCount == 0) {
					if (this.BuyGameItemTicket()) {
						sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
					} else {
						RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
					}
					iGameItemGetChargeFlg = ViCommConst.FLAG_ON; 	
				}

				if (!this.UseMovieTicket(sCastSeq,sCastCharNo,sMovieSeq,iGameItemGetChargeFlg)) {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}

				using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
					oObjUsedHistory.AccessBbsObj(
						sessionMan.userMan.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
						sMovieSeq
					);
				}

				using (Access oAccess = new Access()) {
					oAccess.AccessMoviePage(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sMovieSeq);
				}
			}
		}
	}

	private int GetTicketPossessionCount() {
		int iTicketPossessionCount = 0;
		DataSet oDataSet;

		using (PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			oDataSet = oPossessionGameItem.GetOneByCategoryType(
				sessionMan.site.siteCd,
				sessionMan.sexCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				PwViCommConst.GameItemCategory.ITEM_CATEGORY_TICKET
			);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			iTicketPossessionCount = int.Parse(oDataSet.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString());
		}

		return iTicketPossessionCount;
	}

	private bool BuyGameItemTicket() {
		string sGameItemSeq = string.Empty;
		string sResult = string.Empty;

		using (GameItem oGameItem = new GameItem()) {
			sGameItemSeq = oGameItem.GetGameItem(
				sessionMan.userMan.siteCd,
				sessionMan.sexCd,
				PwViCommConst.GameItemCategory.ITEM_CATEGORY_TICKET,
				"GAME_ITEM_SEQ"
			);

			if (!sGameItemSeq.Equals(string.Empty)) {
				oGameItem.BuyGameItem(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sGameItemSeq,
					"1",
					out sResult
				);
			}
		}

		if (sResult.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
			return true;
		} else {
			return false;
		}
	}

	private bool UseMovieTicket(string pCastSeq,string pCastCharNo,string pMovieSeq,int pGameItemGetChargeFlg) {
		string sResult;

		using (GameItem oGameItem = new GameItem()) {
			oGameItem.UseMovieTicket(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				pCastSeq,
				pCastCharNo,
				pMovieSeq,
				pGameItemGetChargeFlg,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.GameUseMovieTicket.RESULT_OK)) {
			this.CheckQuestClear();
			return true;
		} else {
			return false;
		}
	}

	private void RedirectViewGameCastMovie() {
		string sUrl = string.Empty;
		string sUserRecNo = this.Request.Params["userrecno"];
		string sLoginId = this.Request.Params["loginid"];
		string sCategory = this.Request.Params["category"];
		string sNocrypt = this.Request.Params["nocrypt"];
		string sSeekmode = this.Request.Params["seekmode"];
		string sObjseq = this.Request.Params["objseq"];
		string sPartnerUserSeq = this.Request.Params["partner_user_seq"];
		string sPartnerUserCharNo = this.Request.Params["partner_user_char_no"];
		string sCastUserMovieSeq = this.Request.Params["cast_user_movie_seq"];
		int iValue = 0;

		using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
			iValue = oFriendlyPoint.GetFriendlyRank(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									sessionMan.userMan.userCharNo,
									sPartnerUserSeq,
									sPartnerUserCharNo);
		}

		if (iValue > PwViCommConst.GameFriendlyRank.BEST_FRIEND || iValue == ViCommConst.FLAG_OFF) {
			sUrl = string.Format(
							"ViewGameCastMovie.aspx?userrecno={0}&loginid={1}&category={2}&nocrypt={3}&seekmode={4}&objseq={5}&partner_user_seq={6}"
							+ "&partner_user_char_no={7}&cast_user_movie_seq={8}",
							sUserRecNo,
							sLoginId,
							sCategory,
							sNocrypt,
							sSeekmode,
							sObjseq,
							sPartnerUserSeq,
							sPartnerUserCharNo,
							sCastUserMovieSeq
							);

			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
		}
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.USE_MOVIE_TICKET,
				PwViCommConst.GameQuestTrialCategoryDetail.USE_MOVIE_TICKET,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}
