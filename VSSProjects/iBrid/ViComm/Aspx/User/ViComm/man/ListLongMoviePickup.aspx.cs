/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会話ムービー一覧（お勧め）
--	Progaram ID		: ListLongMoviePickup
--
--  Creation Date	: 2009.10.27
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListLongMoviePickup:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_TALK_MOVIE_PICKUP,
					ActiveForm);
		}
	}

}
