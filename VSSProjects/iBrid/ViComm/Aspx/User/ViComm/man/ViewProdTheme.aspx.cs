﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品着せ替えﾂｰﾙ詳細
--	Progaram ID		: ViewProdTheme
--
--  Creation Date	: 2011.07.01
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_ViewProdTheme : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		ulong lSeekMode = ulong.Parse(this.Request.QueryString["seekmode"]);
		if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			lSeekMode = ViCommConst.INQUIRY_PRODUCT_THEME;
		}

		if (!IsPostBack) {
			if (!sessionMan.ControlList(this.Request, lSeekMode, this.ActiveForm)) {
				throw new ApplicationException();
			}
		}

	}
}
