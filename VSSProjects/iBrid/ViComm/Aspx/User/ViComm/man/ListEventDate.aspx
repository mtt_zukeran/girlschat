<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListEventDate.aspx.cs" Inherits="ViComm_man_ListEventDate" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:SelectionList ID="lstYear" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<ibrid:iBMobileLabel ID="IBMobileLabel1" runat="server" BreakAfter="false">�N</ibrid:iBMobileLabel>
		<mobile:SelectionList ID="lstMonth" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<ibrid:iBMobileLabel ID="IBMobileLabel2" runat="server" BreakAfter="false">��</ibrid:iBMobileLabel>
		<mobile:SelectionList ID="lstDay" Runat="server" SelectType="DropDown" BreakAfter="False"></mobile:SelectionList>
		<ibrid:iBMobileLabel ID="IBMobileLabel3" runat="server" BreakAfter="">���`</ibrid:iBMobileLabel>
		$PGM_HTML02;
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList"		runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail"	runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />		
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
