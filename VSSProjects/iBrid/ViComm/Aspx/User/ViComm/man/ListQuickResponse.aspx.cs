/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: クイックレスポンス一覧
--	Progaram ID		: ListQuickResponse
--
--  Creation Date	: 2011.06.02
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListQuickResponse:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		sessionMan.ClearCategory();
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_EXTENSION_CAST_QUICK_RESPONSE,
					ActiveForm);
		}
	}
}
