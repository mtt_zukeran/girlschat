/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 検索結果一覧
--	Progaram ID		: ListFindResult
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListFindResult:MobileManPageBase {

	protected void frmMain_Init(object sender,EventArgs e) {
		this.ClearCondition = false;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {
		string sScreenId = iBridUtil.GetStringValue(this.Request.QueryString["scrid"]);
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, string.Format("ListFindResult{0}.aspx", sScreenId), ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_CONDITION,
					ActiveForm);
		}
	}

}
