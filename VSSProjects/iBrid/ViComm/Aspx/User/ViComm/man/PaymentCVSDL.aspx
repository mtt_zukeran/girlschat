<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentCVSDL.aspx.cs" Inherits="ViComm_man_PaymentCVSDL" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
		<cc1:iBMobileLabel ID="lblFamilyNm" runat="server">��</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtFamilyNm" runat="server" MaxLength="10" Size="8"></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblGivenNm" runat="server">��</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtGivenNm" runat="server" MaxLength="10" Size="8"></cc1:iBMobileTextBox>
		<br />
		<cc1:iBMobileLabel ID="lblTel" runat="server">�d�b�ԍ�</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true"></cc1:iBMobileTextBox>
		<br />
		<cc1:iBMobileLabel ID="lblMark1" runat="server" ForeColor="blue" BreakAfter="false">��</cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblPack" runat="server">�w�����z</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstPack" Runat="server"></mobile:SelectionList>
		<br />
		<cc1:iBMobileLabel ID="lblMark2" runat="server" ForeColor="blue" BreakAfter="false">��</cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblCVS" runat="server">����ƑI��</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstCVS" Runat="server"></mobile:SelectionList>
		<br />
		<cc1:iBMobileLabel ID="lblMark3" runat="server" ForeColor="blue" BreakAfter="false">��</cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblMailTitle" runat="server">Ұٱ��ڽ</cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblEMailAddr" runat="server"></cc1:iBMobileLabel>
		$PGM_HTML06;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
