/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�ʔ̔����޲��ꗗ
--	Progaram ID		: ListProfileMovie
--
--  Creation Date	: 2010.07.29
--  Creater			: i-Brid(Kazuaki.Itoh)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListPersonalSaleVoice:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if(!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_PERSONAL_SALE_VOICE,
					ActiveForm);
		}
	}
}
