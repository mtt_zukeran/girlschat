/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �v���y�[�h�U��
--	Progaram ID		: PaymentPrepaid
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Net;
using System.IO;
using System.Threading;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentPrepaid:MobileManPageBase {
	
	private string curPoint = "";
	private string money = "";
	private string authCd = "";
	private string cardType = "";
	private string ipapUrl = "";
	private string clientId = "";

	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (SettlePrepaid()) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_PREPAID_TRANS_OK));
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_PREPAID_TRANS_NG));
		}
	}

	private bool SettlePrepaid() {
		bool bOk = false;
		string sResult;

		string sSid = "";

		using (SettleLog oLog = new SettleLog()) {
			oLog.LogSettleRequest(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				ViCommConst.SETTLE_CORP_SITE01,
				ViCommConst.SETTLE_PREPAID,
				ViCommConst.SETTLE_STAT_SETTLE_NOW,
				0,
				0,
				txtLoginId.Text,
				string.Empty,

			out sSid);
		}

		string sTel = sessionMan.userMan.tel;
		if (sTel.Equals(string.Empty)) {
			sTel = "09000000000";
		}
		if (CheckPrepaid(sTel,sessionMan.userMan.emailAddr,sSid)) {

			if ((!money.Equals("")) && (!curPoint.Equals("")) && (!authCd.Equals("")) && (cardType.Equals("1"))) {
				string sSettleUrl = string.Format("{0}/SettleCheck.aspx?auth_code={1}&money={2}&point={3}&resultcode=200",ipapUrl,authCd,money,curPoint);
				bOk = TransPrepaid(sSettleUrl,out sResult);
			} else {
				bOk = false;
			}

			if (bOk) {
				bool bNewSignal;
				using (NamedAutoResetEvent oSignal = new NamedAutoResetEvent(false,"signal-" + sSid,out bNewSignal)) {
					string sCardSettleUrl = string.Format("{0}/CardSettle.aspx?auth_code={1}",ipapUrl,authCd);
					TransPrepaid(sCardSettleUrl,out sResult);
					oSignal.WaitOne(10000,false);
				}
			}
		}
		return bOk;
	}

	private bool CheckPrepaid(string pTel,string pEmailAddr,string pLoginId) {
		bool bOk = false;
		string sCheckUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_SITE01,ViCommConst.SETTLE_PREPAID)) {
				ipapUrl = oSiteSettle.settleUrl;
				clientId = oSiteSettle.cpIdNo;
				sCheckUrl = string.Format("{0}/AccountCheck.aspx?serviceid=100&clientip={1}&telno={2}&email={3}&sendid={4}&card_id={5}&card_pass={6}",
											ipapUrl,
											clientId,
											pTel,
											pEmailAddr,
											pLoginId,
											txtLoginId.Text,
											txtPassword.Text);

			}
		}

		money = "";
		curPoint = "";
		authCd = "";
		cardType = "";

		if (sCheckUrl.Equals("") == false) {
			string sResult;
			bOk = TransPrepaid(sCheckUrl,out sResult);
			if (bOk) {
				string[] sItem = sResult.Split('\n');
				for (int i = 0;i < sItem.Length;i++) {

					if (sItem[i].StartsWith("money")) {
						string[] sItemMoney = sItem[i].Split('=');
						money = sItemMoney[1];

					} else if (sItem[i].StartsWith("point")) {
						string[] sItemPoint = sItem[i].Split('=');
						curPoint = sItemPoint[1];

					} else if (sItem[i].StartsWith("card_type")) {
						string[] sCardType = sItem[i].Split('=');
						cardType = sCardType[1];

					} else if (sItem[i].StartsWith("auth_code")) {
						string[] sItemAuth = sItem[i].Split('=');
						authCd = sItemAuth[1];
					}
				}
			}
		}
		return bOk;
	}

	private bool TransPrepaid(string pUrl,out string pRes) {
		pRes = "";
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 20000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				pRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				pRes = pRes.Replace("\r","");
				return pRes.StartsWith("200");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransPrepaid",pUrl);
			return false;
		}
	}
}
