/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ¨óderSðÚ×
--	Progaram ID		: ViewBbsBingoLog
--  Creation Date	: 2013.11.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewBbsBingoLog:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			DataSet dsBbsBingoLog;

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_BBS_BINGO_LOG,this.ActiveForm,out dsBbsBingoLog)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			string sResult = iBridUtil.GetStringValue(dsBbsBingoLog.Tables[0].Rows[0]["RESULT"]);

			if (sResult.Equals(PwViCommConst.GetBbsBingoLogResult.RESULT_OK)) {
				pnlResult.Visible = true;
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoLogResult.RESULT_NG_TERM)) {
				sessionMan.errorMessage = "¨ódeËÞÝºÞÍI¹µÜµ½";
				pnlError.Visible = true;
			} else if (sResult.Equals(PwViCommConst.GetBbsBingoLogResult.RESULT_NG_CHECKED)) {
				pnlResult.Visible = true;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}
}
