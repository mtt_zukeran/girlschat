/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プロフィール動画一覧
--	Progaram ID		: ListProfileMovieObj
--
--  Creation Date	: 2013.09.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProfileMovieObj:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_DIRECT,ActiveForm);
		}
	}
}
