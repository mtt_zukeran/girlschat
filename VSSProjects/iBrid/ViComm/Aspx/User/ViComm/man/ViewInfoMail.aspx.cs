/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お知らせメール詳細表示
--	Progaram ID		: ViewInfoMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ViewInfoMail:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.ControlList(Request,ViCommConst.INQUIRY_RX_INFO_MAIL_BOX,ActiveForm)) {
				if (iBridUtil.GetStringValue(Request.QueryString["del"]).Equals("1")) {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailDel(sessionMan.GetMailValue("MAIL_SEQ"),ViCommConst.RX);
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_INFO_MAIL + "&mail_delete_count=1"));
					}
				} else {
					using (MailBox oMailBox = new MailBox()) {
						oMailBox.UpdateMailReadFlag(sessionMan.GetMailValue("MAIL_SEQ"));
					}
				}
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListInfoMailBox.aspx"));
			}
		}
	}
}
