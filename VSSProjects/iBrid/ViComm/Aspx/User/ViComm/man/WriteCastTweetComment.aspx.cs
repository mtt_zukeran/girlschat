/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルのつぶやきコメント書込
--	Progaram ID		: WriteCastTweetComment.aspx
--  Creation Date	: 2013.11.16
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteCastTweetComment:MobileManPageBase {
	private string sTweetSeq;
	private int iMaxLength = 0;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.errorMessage = string.Empty;

		sTweetSeq = iBridUtil.GetStringValue(Request.QueryString["tweetseq"]);

		if (string.IsNullOrEmpty(sTweetSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			if (!sessionMan.logined) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_TWEET,this.ActiveForm)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int.TryParse(iBridUtil.GetStringValue(Request.Params["max_length"]),out iMaxLength);

		using (CastTweetComment oCastTweetComment = new CastTweetComment()) {
			if (oCastTweetComment.CheckCastTweetCommentLimitTime(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,this.sTweetSeq)) {
				sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_TWEET_LIMIT_TIME);
				return;
			}
		}

		if (CheckInput()) {
			string sResult = string.Empty;

			using (CastTweetComment oCastTweetComment = new CastTweetComment()) {
				string sCommentText = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtComment.Text));

				oCastTweetComment.WriteCastTweetComment(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					this.sTweetSeq,
					sCommentText,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("ListCastTweetComment.aspx?tweetseq={0}",this.sTweetSeq)));
				return;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}
		}
	}

	private bool CheckInput() {
		if (txtComment.Text.Equals("")) {
			sessionMan.errorMessage += GetErrorMessage(PwViCommConst.PwErrerCode.ERR_TWEET_COMMENT);
			return false;
		}

		if (this.iMaxLength != 0 && txtComment.Text.Length > this.iMaxLength) {
			sessionMan.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT),"ｺﾒﾝﾄ");
			return false;
		}

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}

		string sNGWord;

		if (sessionMan.ngWord.VaidateDoc(txtComment.Text,out sNGWord) == false) {
			sessionMan.errorMessage += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			return false;
		}

		return true;
	}
}
