﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ビンゴ参加
--	Progaram ID		: EntryBingo
--
--  Creation Date	: 2011.07.07
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_EntryBingo : MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {

			if (ViCommConst.FLAG_ON_STR.Equals(this.Request.QueryString["entry"])) {
				this.RedirectResultPage();
			}
		}
	}

	private void RedirectResultPage() {
		string sResult;
		using (BingoEntry oBingoEntry = new BingoEntry()) {
			sResult = oBingoEntry.EntryBingo(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,ViCommConst.MAN);
		}

		IDictionary<string, string> oParameters = new Dictionary<string, string>();
		oParameters.Add("result", sResult);
		RedirectToDisplayDoc(ViCommConst.SCR_MAN_BINGO_ENTRY_RESULT,oParameters);
	}

}
