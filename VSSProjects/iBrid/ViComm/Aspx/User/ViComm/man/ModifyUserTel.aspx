<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyUserTel.aspx.cs" Inherits="ViComm_man_ModifyUserTel" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tag01s" runat="server" Text="<font size=1>" />
		$PGM_HTML02;
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="15"></cc1:iBMobileTextBox>
		<br />
		<cc1:iBMobileLiteralText ID="tag01e" runat="server" Text="</font>" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
