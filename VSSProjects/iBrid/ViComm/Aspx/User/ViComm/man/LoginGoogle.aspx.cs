/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会員ログイン(Googleアカウント利用)
--	Progaram ID		: LoginGoogle
--  Creation Date	: 2016.01.08
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_LoginGoogle:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			sessionObj.snsType = string.Empty;
			sessionObj.snsId = string.Empty;

			string sConsumerKey = string.Empty;
			string sAuthrizeUrl = string.Empty;
			string sScopeUrl1 = string.Empty;
			string sScopeUrl2 = string.Empty;
			string sCallbackUrl = string.Format("http://{0}/user/start.aspx?googlelogin=1",sessionMan.site.hostNm);
			//string sCallbackUrl = string.Format("http://localhost:81/user/start.aspx?googlelogin=1",sessionMan.site.hostNm);

			using (SnsOAuth oSnsOAuth = new SnsOAuth()) {
				DataSet oDataSet = oSnsOAuth.GetOneBySnsType(sessionMan.site.siteCd,PwViCommConst.SnsType.GOOGLE);

				if (oDataSet.Tables[0].Rows.Count == 0) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}

				sConsumerKey = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CONSUMER_KEY"]);
				sAuthrizeUrl = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AUTHORIZE_URL"]);
				sScopeUrl1 = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["SCOPE_URL1"]);
				sScopeUrl2 = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["SCOPE_URL2"]);
			}

			// Cookieに広告コードを書き込む
			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE) || (sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS))) {
				HttpCookie cookie = new HttpCookie("maqiaad");
				cookie.Values["adcd"] = sessionMan.adCd;
				cookie.Values["company"] = sessionMan.affiliateCompany;
				cookie.Values["affiliatercd"] = sessionMan.affiliaterCd;
				cookie.Values["intro"] = sessionMan.introducerFriendCd;
				cookie.Expires = DateTime.Now.AddDays(3);
				cookie.Domain = sessionMan.site.hostNm;
				Response.Cookies.Add(cookie);
			}

			string sScope = HttpUtility.UrlEncode(string.Join(" ",new string[]{sScopeUrl1,sScopeUrl2}));
			RedirectToMobilePage(string.Format("{0}?scope={1}&redirect_uri={2}&response_type=code&client_id={3}",sAuthrizeUrl,sScope,sCallbackUrl,sConsumerKey));
		}
	}
}