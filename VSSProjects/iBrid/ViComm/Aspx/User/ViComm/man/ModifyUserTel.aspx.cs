/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 電話番号変更
--	Progaram ID		: ModifyUserTel
--
--  Creation Date	: 2010.10.28
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ModifyUserTel:MobileManPageBase {
	
	protected string NextDoc{
		get { return this.ViewState["nextdoc"] as string; }
		set{ this.ViewState["nextdoc"] =value;}
	}

	protected string BackUrl{
		get { return this.ViewState["backurl"] as string; }
		set { this.ViewState["backurl"] = value; }
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!string.IsNullOrEmpty(sessionMan.userMan.tel)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			txtTel.Text = string.Empty;
			this.NextDoc = this.Request.Params["nextdoc"];
			this.BackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (txtTel.Visible) {
			if (txtTel.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
				bOk = false;
			} else {
				if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
					bOk = false;
				} else {
					using (Man oMan = new Man()) {
						if (oMan.IsTelephoneRegistered(sessionMan.site.siteCd,txtTel.Text)) {
							lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
							bOk = false;
						}
					}
				}
			}
		}

		if (bOk == false) {
			return;
		} else {
			string sResult;

			sessionMan.userMan.ModifyUserTel(
				sessionMan.userMan.userSeq,
				txtTel.Text,
                sessionMan.site.siteCd,
				out sResult
			);
			
			string sDocNo = ViCommConst.SCR_MAN_MODIFY_TEL_COMPLETE;
			if(!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.Params["nextdoc"]))){
				sDocNo = this.Request.Params["nextdoc"];
			}
			
			if (sResult.Equals("0")) {
				if (!string.IsNullOrEmpty(this.BackUrl)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(this.BackUrl));
					return;
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType, Session.SessionID, "DisplayDoc.aspx?doc=" + sDocNo));
				}
			} else {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
			}
		}
	}

}
