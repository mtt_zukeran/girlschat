<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistInvestigateAcquiredPoint.aspx.cs" Inherits="ViComm_man_RegistInvestigateAcquiredPoint" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		$PGM_HTML02;
		<cc1:iBMobileLiteralText ID="tagEntry" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLiteralText ID="tagEntered" runat="server" Text="$PGM_HTML04;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>