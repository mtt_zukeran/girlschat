/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい編集
--	Progaram ID		: ModifyRequest
--
--  Creation Date	: 2012.06.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ModifyRequest:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sRequestSeq = iBridUtil.GetStringValue(Request.QueryString["req_seq"]);

		if (string.IsNullOrEmpty(sRequestSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!this.checkAccess()) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = false;

			this.setLstCategory();
			this.setLstProgress();

			DataSet dsRequest = getRequest(sRequestSeq);

			if (dsRequest.Tables[0].Rows.Count > 0) {
				DataRow drRequest = dsRequest.Tables[0].Rows[0];
				this.setFormValue(drRequest);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e,sRequestSeq);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	private bool checkAccess() {
		if (sessionMan.IsValidMask(sessionMan.userMan.userDefineMask,PwViCommConst.ManUserDefFlag.REQUEST_ADMIN)) {
			return true;
		}

		return false;
	}

	private void setLstCategory() {
		using (PwRequest oPwRequest = new PwRequest()) {
			DataSet dsCategory = oPwRequest.GetRequestCategory(sessionMan.site.siteCd,ViCommConst.MAN,null,null);

			foreach (DataRow drCategory in dsCategory.Tables[0].Rows) {
				lstCategory.Items.Add(new MobileListItem(drCategory["REQUEST_CATEGORY_NM"].ToString(),drCategory["REQUEST_CATEGORY_SEQ"].ToString()));
			}
		}
	}

	private void setLstProgress() {
		using (PwRequest oPwRequest = new PwRequest()) {
			DataSet dsProgress = oPwRequest.GetRequestProgress(sessionMan.site.siteCd);

			foreach (DataRow drProgress in dsProgress.Tables[0].Rows) {
				lstProgress.Items.Add(new MobileListItem(drProgress["REQUEST_PROGRESS_NM"].ToString(),drProgress["REQUEST_PROGRESS_SEQ"].ToString()));
			}
		}
	}

	private DataSet getRequest(string pRequestSeq) {
		DataSet dsRequest;
		PwRequestSeekCondition oCondition = new PwRequestSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.SexCd = ViCommConst.MAN;
		oCondition.RequestSeq = pRequestSeq;
		using (PwRequest oPwRequest = new PwRequest()) {
			dsRequest = oPwRequest.GetPageCollection(oCondition,1,1);
		}
		return dsRequest;
	}

	private void setFormValue(DataRow drRequest) {
		foreach (MobileListItem item in lstCategory.Items) {
			if (item.Value == iBridUtil.GetStringValue(drRequest["REQUEST_CATEGORY_SEQ"])) {
				item.Selected = true;
			}
		}

		foreach (MobileListItem item in lstProgress.Items) {
			if (item.Value == iBridUtil.GetStringValue(drRequest["REQUEST_PROGRESS_SEQ"])) {
				item.Selected = true;
			}
		}

		txtAdminComment.Text = iBridUtil.GetStringValue(drRequest["ADMIN_COMMENT"]);
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;

		if (lstCategory.Items[lstCategory.SelectedIndex].Value.Equals(string.Empty)) {
			sessionMan.errorMessage = "ｶﾃｺﾞﾘを選択して下さい";
			return;
		}

		if (lstProgress.Items[lstProgress.SelectedIndex].Value.Equals(string.Empty)) {
			sessionMan.errorMessage = "進捗状況を選択して下さい";
			return;
		}

		if (!txtAdminComment.Text.Equals(string.Empty)) {
			if (txtAdminComment.Text.Length > 1000) {
				sessionMan.errorMessage = "管理者ｺﾒﾝﾄが長すぎます";
				return;
			}
		}

		ViewState["REQUEST_CATEGORY_SEQ"] = lstCategory.Items[lstCategory.SelectedIndex].Value;
		ViewState["REQUEST_PROGRESS_SEQ"] = lstProgress.Items[lstProgress.SelectedIndex].Value;
		ViewState["ADMIN_COMMENT"] = txtAdminComment.Text;

		lblCategory.Text = lstCategory.Items[lstCategory.SelectedIndex].Text;
		lblProgress.Text = lstProgress.Items[lstProgress.SelectedIndex].Text;
		lblAdminComment.Text = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,txtAdminComment.Text)).Replace(System.Environment.NewLine,"<br>");

		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
		pnlComplete.Visible = false;
	}

	protected void cmdTx_Click(object sender,EventArgs e,string pRequestSeq) {
		sessionMan.errorMessage = string.Empty;
		string sRequestCategorySeq = iBridUtil.GetStringValue(ViewState["REQUEST_CATEGORY_SEQ"]);
		string sRequestProgressSeq = iBridUtil.GetStringValue(ViewState["REQUEST_PROGRESS_SEQ"]);
		string sAdminComment = iBridUtil.GetStringValue(ViewState["ADMIN_COMMENT"]);
		string sResult = string.Empty;

		using (PwRequest oPwRequest = new PwRequest()) {
			sResult = oPwRequest.ModifyRequest(
				sessionMan.site.siteCd,
				pRequestSeq,
				sRequestCategorySeq,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sAdminComment)),
				sRequestProgressSeq
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			pnlWrite.Visible = false;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
		pnlComplete.Visible = false;
	}
}
