﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾊｸﾞされた一覧
--	Progaram ID		: ListGameCommunicationPartnerHug.aspx
--
--  Creation Date	: 2011.08.12
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameCommunicationPartnerHug:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		using (GameCharacter oGameCharacter = new GameCharacter()) {
			oGameCharacter.UpdateLastHugLogCheck(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo
			);
		}

		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_COMMUNICATION_PARTNER_HUG,ActiveForm);
	}
}
