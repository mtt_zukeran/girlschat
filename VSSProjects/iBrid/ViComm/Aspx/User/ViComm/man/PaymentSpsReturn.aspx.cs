﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: SoftBank Payment Service
--	Progaram ID		: PaymentSpsRerturn
--
--  Creation Date	: 2013.06.26
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Security.Cryptography;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentSpsReturn:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
	}
}
