/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсEʸތ���
--	Progaram ID		: ViewGameCharacterHugResult
--
--  Creation Date	: 2011.08.23
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameCharacterHugResult:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		}
	}
}