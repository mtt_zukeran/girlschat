/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���i����w������ �ꗗ
--	Progaram ID		: ListProdMovBuyHist
--
--  Creation Date	: 2010.12.14
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListProdMovBuyHist : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					this.Request,
					ViCommConst.INQUIRY_PRODUCT_MOVIE_BUYING_HIST,
					this.ActiveForm);

		}
	}
}
