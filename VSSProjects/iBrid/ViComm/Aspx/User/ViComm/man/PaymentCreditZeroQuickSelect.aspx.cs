/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ڼޯ�QUICK�\����(ZERO�ڼޯ�)
--	Progaram ID		: PaymentCreditZeroQuickSelect
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCreditZeroQuickSelect:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			DataSet ds;
			ds = this.GetPackDataSet();
			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstCreditPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstCreditPack.Items[lstCreditPack.SelectedIndex].Value;
		string sSid = "";

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_ZERO,
					ViCommConst.SETTLE_CREDIT_PACK,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					string.Empty,
					string.Empty,
					out sSid);
			}
		}
		string sSettleUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_CREDIT_PACK)) {
				sSettleUrl = string.Format(oSiteSettle.continueSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sessionMan.userMan.tel);
			}
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
			string.Format("PaymentCreditZeroQuick.aspx?sid={0}&money={1}",sSid,sSalesAmt)));
	}

	private DataSet GetPackDataSet() {
		int iFirstSettleEnableDays = 0;
		DateTime dtRegistDate = DateTime.Parse(sessionMan.userMan.registDay).Date;
		DateTime dtToday = DateTime.Today;
		TimeSpan tsDaysFromRegist = dtToday - dtRegistDate;
		bool bFirstSettleEnable;

		using (Site oSite = new Site()) {
			string sFirstSettleEnableDays = string.Empty;
			oSite.GetValue(sessionMan.site.siteCd,"FIRST_SETTLE_ENABLE_DAYS",ref sFirstSettleEnableDays);

			if (!int.TryParse(sFirstSettleEnableDays,out iFirstSettleEnableDays)) {
				iFirstSettleEnableDays = 0;
			}
		}

		if (iFirstSettleEnableDays == 0) {
			bFirstSettleEnable = true;
		} else if (iFirstSettleEnableDays >= tsDaysFromRegist.Days) {
			bFirstSettleEnable = true;
		} else {
			bFirstSettleEnable = false;
		}

		DataSet ds;
		using (Pack oPack = new Pack()) {
			using (UserMan oUserMan = new UserMan()) {
				oUserMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
				if (oUserMan.totalReceiptAmt == 0 && bFirstSettleEnable) {
					ds = oPack.GetList(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,0,sessionMan.userMan.userSeq);
				} else {
					ds = oPack.GetNonFirstList(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,0,sessionMan.userMan.userSeq);
				}

			}
			oPack.CalcServicePoint(sessionMan.userMan.userSeq,ds);
		}

		return ds;
	}
}

