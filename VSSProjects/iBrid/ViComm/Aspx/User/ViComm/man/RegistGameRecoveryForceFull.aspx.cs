﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ 部隊回復
--	Progaram ID		: RegistGameRecoveryForceFull
--
--  Creation Date	: 2011.08.18
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_man_RegistGameRecoveryForceFull:MobileSocialGameManBase {
	private NameValueCollection query = new NameValueCollection();
	
	protected void Page_Load(object sender,EventArgs e) {
		string sItemSeq = iBridUtil.GetStringValue(this.Request.QueryString["item_seq"]);
		
		Regex rgx = new Regex("^[0-9]+$");
		Match rgxMatch = rgx.Match(sItemSeq);
		if (!rgxMatch.Success) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}

		if (!this.CheckPossessionRecoveryItem(sItemSeq)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_RECOVERY_FORCE_FULL,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.RecoveryForceFull();
			}
		}
	}
	
	private void RecoveryForceFull() {
		string[] sValues = this.Request.Params["NEXTLINK"].Split('_');
		string sGameItemSeq = sValues[0];
		string sGameItemPossessionCount = sValues[1];
		string sGameItemNm = sValues[2];
		string result = this.RecoveryForceFullExecute(sGameItemSeq);
		string sPageId = iBridUtil.GetStringValue(this.Request.QueryString["page_id"]);
		string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.Params["battle_log_seq"]);

		if(result.Equals(PwViCommConst.GameRecoveryForceFullStatus.RESULT_OK)) {
			this.query["item_seq"] = sGameItemSeq;
			int iItemCount = int.Parse(sGameItemPossessionCount) - 1;
			this.query["item_count"] = iItemCount.ToString();

			if (sPageId == PwViCommConst.PageId.SUPPORT_BATTLE) {
				this.query["page_id"] = sPageId;
				this.query["battle_log_seq"] = sBattleLogSeq;
			}

			Session["PageId"] = null;
			Session["BattleLogSeq"] = null;
			
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_RECOVERY_FORCE_FULL_COMPLETE,this.query);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}
	
	private string RecoveryForceFullExecute(string sGameItemSeq) {
		string sResult = "";
		int iGameItemSeq = int.Parse(sGameItemSeq);
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		int iUseCoopPointFlag = 0;
		int iMskForce = PwViCommConst.GameMskForce.MSK_ALL_FORCE;

		RecoveryForce oRecoveryForce = new RecoveryForce();
		oRecoveryForce.RecoveryForceFull(sSiteCd,sUserSeq,sUserCharNo,iUseCoopPointFlag,iGameItemSeq,iMskForce,out sResult);

		return sResult;
	}
	
	private bool CheckPossessionRecoveryItem(string sGameItemSeq) {
		PossessionGameItemSeekCondition oCondition = new PossessionGameItemSeekCondition();
		oCondition.SeekMode = PwViCommConst.INQUIRY_GAME_RECOVERY_FORCE_FULL;
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.UserSeq = this.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
		oCondition.ItemSeq = sGameItemSeq;
		oCondition.GameItemCategoryType = PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE;
		oCondition.PresentFlag = ViCommConst.FLAG_OFF_STR;
		oCondition.SexCd = this.sessionMan.sexCd;
		
		DataSet oDataSet = null;
		
		using(PossessionGameItem oPossessionGameItem = new PossessionGameItem()) {
			oDataSet = oPossessionGameItem.GetPossessionItem(oCondition);
		}
		
		int iPossessionCount = 0;
		
		int.TryParse(oDataSet.Tables[0].Rows[0]["POSSESSION_COUNT"].ToString(),out iPossessionCount);

		if (iPossessionCount > 0) {
			return true;
		} else {
			return false;
		}
	}
}