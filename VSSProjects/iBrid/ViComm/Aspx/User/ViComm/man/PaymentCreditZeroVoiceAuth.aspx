<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentCreditZeroVoiceAuth.aspx.cs" Inherits="ViComm_man_PaymentCreditZeroVoiceAuth" %>
<%@ Register TagPrefix="ibrid"		Namespace="MobileLib"						Assembly="MobileLib"%>
<%@ Register TagPrefix="mobile"		Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		$PGM_HTML02;
		<ibrid:iBMobileLink ID="lnkAuthTelNo" runat="server" />
		$PGM_HTML03;
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>