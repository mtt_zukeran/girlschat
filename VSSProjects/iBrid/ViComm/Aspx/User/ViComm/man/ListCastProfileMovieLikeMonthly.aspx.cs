/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者自己紹介動画イイネ月間ランキング
--	Progaram ID		: ListCastProfileMovieLikeMonthly
--  Creation Date	: 2013.12.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListCastProfileMovieLikeMonthly:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_PROFILE_MOVIE_LIKE_MONTHLY,this.ActiveForm);
		}
	}
}
