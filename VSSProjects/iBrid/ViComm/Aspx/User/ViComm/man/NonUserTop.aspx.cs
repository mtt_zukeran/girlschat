/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o�^�O�g�b�v
--	Progaram ID		: NonUserTop
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_NonUserTop:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			ParseHTML oParseMan = sessionMan.parseContainer;
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_GUID;

			// DOCOMO�ő̎��ʎ擾���� 
			if (sessionMan.carrier.Equals(ViCommConst.DOCOMO) && sessionMan.getTermIdFlag) {
				oParseMan.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
			}

			sessionMan.ControlList(
				Request,
				ViCommConst.INQUIRY_RANDOM_RECOMMEND,
				ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		if (!sessionMan.usedEasyLoginFlag) {
			ParseHTML oParseMan = sessionMan.parseContainer;
			string sId = Mobile.GetiModeId(sessionMan.carrier,Request);
			string sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
			oParseMan.parseUser.postAction = 0;
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("LoginUser.aspx?utn={0}&id={1}&useutn={2}",sUtn,sId,sessionMan.getTermIdFlag == true ? "1" : "0")));
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("LoginUser.aspx?id={0}",sessionMan.currentIModeId)));
		}
	}
}
