/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ブログ記事イイネ登録
--	Progaram ID		: RegistBlogArticleLike
--
--  Creation Date	: 2014.03.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_RegistBlogArticleLike:MobileBlogManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		tagComplete.Visible = false;
		tagError.Visible = false;
		
		string sResult = string.Empty;
		string sBlogArticleSeq = iBridUtil.GetStringValue(Request.QueryString["blogarticleseq"]);
		
		if (string.IsNullOrEmpty(sBlogArticleSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
		
		using (BlogArticleLike oBlogArticleLike = new BlogArticleLike()) {
			oBlogArticleLike.RegistBlogArticleLike(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sBlogArticleSeq,out sResult);
		}
		
		if (sResult.Equals(PwViCommConst.RegistBlogArticleLikeResult.RESULT_OK)) {
			tagComplete.Visible = true;
		} else if (sResult.Equals(PwViCommConst.RegistBlogArticleLikeResult.RESULT_EXISTS_LIKE)) {
			tagError.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}