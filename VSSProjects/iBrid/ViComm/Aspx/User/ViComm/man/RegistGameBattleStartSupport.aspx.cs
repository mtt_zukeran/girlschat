/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@���ٍ��ݒ�(��������)
--	Progaram ID		: RegistGameBattleStartSupport
--
--  Creation Date	: 2011.08.26
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Collections.Specialized;
using System.Collections.Generic;

public partial class ViComm_man_RegistGameBattleStartSupport:MobileSocialGameManBase {
	private NameValueCollection query = new NameValueCollection();

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sBattleLogSeq = iBridUtil.GetStringValue(this.Request.QueryString["battle_log_seq"]);
		
		if(string.IsNullOrEmpty(sBattleLogSeq)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		using(GameCharacterBattleSupport oGameCharacterBattleSupport = new GameCharacterBattleSupport()) {
			if(!oGameCharacterBattleSupport.CheckExistsPartner(sSiteCd,sUserSeq,sUserCharNo,sBattleLogSeq)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}

			if (!oGameCharacterBattleSupport.CheckExistsSupport(sSiteCd,sUserSeq,sUserCharNo,sBattleLogSeq)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		
		}

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_START_SUPPORT,ActiveForm);
		} else {
			if (!string.IsNullOrEmpty(this.Request.Params["cmdNext"])) {

				string sPartnerUserSeq = string.Empty;
				string sPartnerUserCharNo = string.Empty;
				string sPartnerGameCharacterType = string.Empty;
				
				string[] textArr = this.Request.Params["NEXTLINK"].ToString().Split('_');
				string sSupportUserSeq = textArr[0];
				string sSupportUserCharNo = textArr[1];
				string sSupportBattleLogSeq = textArr[2];
				string sSupportRequestSubSeq = textArr[3];
				string sTreasureSeq = textArr[4];
				int iAttackForceCount;
				int.TryParse(this.Request.Params["attack_force_count"],out iAttackForceCount);
				
				if (iAttackForceCount > 0) {

					string sFriendlyPoint = null;

					FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
					oCondition.SiteCd = this.sessionMan.site.siteCd;
					oCondition.UserSeq = this.sessionMan.userMan.userSeq;
					oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
					oCondition.PartnerUserSeq = sSupportUserSeq;
					oCondition.PartnerUserCharNo = sSupportUserCharNo;

					using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
						sFriendlyPoint = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
					}

					this.GetPartnerData(sSupportUserSeq,sSupportUserCharNo,sSupportBattleLogSeq,out sPartnerUserSeq,out sPartnerUserCharNo,out sPartnerGameCharacterType);

					this.BattleStart(sPartnerUserSeq,sPartnerUserCharNo,sSupportUserSeq,sSupportUserCharNo,sSupportBattleLogSeq,sSupportRequestSubSeq,sTreasureSeq,iAttackForceCount,sFriendlyPoint,sPartnerGameCharacterType);
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}
	}

	private void BattleStart(
		string sPartnerUserSeq,
		string sPartnerUserCharNo,
		string sSupportUserSeq,
		string sSupportUserCharNo,
		string sSupportBattleLogSeq,
		string sSupportRequestSubSeq,
		string sTreasureSeq,
		int iAttackForceCount,
		string sFriendlyPoint,
		string sPartnerGameCharacterType
	) {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sBattleType = PwViCommConst.GameBattleType.SUPPORT;
		int iSurelyWinFlag = 0;
		int iLevelUpFlag;
		int iAddForceCount;
		int iTreasureCompleteFlag;
		int iBonusGetFlag;
		int iWinFlag;
		int iComePoliceFlag;
		int iTrapUsedFlag;
		string sBattleLogSeq;
		string[] BreakGameItemSeq;
		string sResult;
		string sPreExp = this.sessionMan.userMan.gameCharacter.exp.ToString();
		string sPreCooperationPoint = this.sessionMan.userMan.gameCharacter.cooperationPoint.ToString();
		
		Battle oBattle = new Battle();
		oBattle.BattleStart(
			sSiteCd,
			sUserSeq,
			sUserCharNo,
			sPartnerUserSeq,
			sPartnerUserCharNo,
			sSupportUserSeq,
			sSupportUserCharNo,
			sSupportBattleLogSeq,
			sSupportRequestSubSeq,
			sBattleType,
			sTreasureSeq,
			iSurelyWinFlag,
			iAttackForceCount,
			out iLevelUpFlag,
			out iAddForceCount,
			out iTreasureCompleteFlag,
			out iBonusGetFlag,
			out iWinFlag,
			out iComePoliceFlag,
			out iTrapUsedFlag,
			out sBattleLogSeq,
			out BreakGameItemSeq,
			out sResult
		);

		string sRedirectUrl = string.Empty;

		if (sResult.Equals(PwViCommConst.GameBattleResult.RESULT_OK)) {
			this.CheckQuestClear(sUserSeq,sUserCharNo,ViCommConst.MAN);
			this.CheckQuestClear(sPartnerUserSeq,sPartnerUserCharNo,ViCommConst.OPERATOR);
			this.CheckQuestClear(sSupportUserSeq,sSupportUserCharNo,ViCommConst.OPERATOR);
			
			string sBreakItemSeqList = string.Join("_",BreakGameItemSeq);
			
			if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				if (iWinFlag == ViCommConst.FLAG_ON) {
					oParameters.Add("support_user_seq",sSupportUserSeq);
					oParameters.Add("support_user_char_no",sSupportUserCharNo);
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("break_item_seq",sBreakItemSeqList);
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("pre_friendly_point",sFriendlyPoint);
					oParameters.Add("pre_cooperation_point",sPreCooperationPoint);
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
				} else {
					oParameters.Add("support_user_seq",sSupportUserSeq);
					oParameters.Add("support_user_char_no",sSupportUserCharNo);
					oParameters.Add("battle_log_seq",sBattleLogSeq);
					oParameters.Add("level_up_flag",iLevelUpFlag.ToString());
					oParameters.Add("add_force_count",iAddForceCount.ToString());
					oParameters.Add("come_police_flag",iComePoliceFlag.ToString());
					oParameters.Add("trap_used_flag",iTrapUsedFlag.ToString());
					oParameters.Add("break_item_seq",sBreakItemSeqList);
					oParameters.Add("pre_exp",sPreExp);
					oParameters.Add("pre_friendly_point",sFriendlyPoint);
					oParameters.Add("pre_cooperation_point",sPreCooperationPoint);
					oParameters.Add("win_flag",iWinFlag.ToString());
					oParameters.Add("battle_type",sBattleType);
					oParameters.Add("partner_char_type",sPartnerGameCharacterType);
				}
				RedirectToGameDisplayDoc(PwViCommConst.SCR_GAME_FLASH_SUPPORT_BATTLE_SP,oParameters);
			} else {
				if (iWinFlag == 1) {
					sRedirectUrl = String.Format(
										"ViewGameFlashSupportBattle.aspx?support_user_seq={0}&support_user_char_no={1}&battle_log_seq={2}&level_up_flag={3}&add_force_count={4}&break_item_seq={5}&pre_exp={6}&pre_friendly_point={7}&pre_cooperation_point={8}&battle_type={9}",
										sSupportUserSeq,
										sSupportUserCharNo,
										sBattleLogSeq,
										iLevelUpFlag.ToString(),
										iAddForceCount.ToString(),
										sBreakItemSeqList,
										sPreExp,
										sFriendlyPoint,
										sPreCooperationPoint,
										PwViCommConst.GameBattleType.SUPPORT
									);
				} else {
					sRedirectUrl = String.Format(
										"ViewGameFlashSupportBattle.aspx?support_user_seq={0}&support_user_char_no={1}&battle_log_seq={2}&level_up_flag={3}&add_force_count={4}&come_police_flag={5}&trap_used_flag={6}&break_item_seq={7}&pre_exp={8}&pre_friendly_point={9}&pre_cooperation_point={10}&battle_type={11}",
										sSupportUserSeq,
										sSupportUserCharNo,
										sBattleLogSeq,
										iLevelUpFlag.ToString(),
										iAddForceCount.ToString(),
										iComePoliceFlag.ToString(),
										iTrapUsedFlag.ToString(),
										sBreakItemSeqList,
										sPreExp,
										sFriendlyPoint,
										sPreCooperationPoint,
										PwViCommConst.GameBattleType.SUPPORT
									);
				}

				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
			}
		} else if(sResult.Equals(PwViCommConst.GameBattleResult.RESULT_NG)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		} else {
			this.query["result"] = sResult;
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_BATTLE_NO_FINISH,this.query);
		}
	}

	private void GetPartnerData(string sSupportUserSeq,string sSupportUserCharNo,string sSupportBattleLogSeq,out string sPartnerUserSeq,out string sPartnerUserCharNo,out string sPartnerGameCharcterType) {
		sPartnerUserSeq = string.Empty;
		sPartnerUserCharNo = string.Empty;
		sPartnerGameCharcterType = string.Empty;

		BattleLogSeekCondition oBattleLogSeekCondition = new BattleLogSeekCondition();
		oBattleLogSeekCondition.SiteCd = sessionMan.site.siteCd;
		oBattleLogSeekCondition.UserSeq = sSupportUserSeq;
		oBattleLogSeekCondition.UserCharNo = sSupportUserCharNo;
		oBattleLogSeekCondition.BattleLogSeq = sSupportBattleLogSeq;

		using (BattleLog oBattleLog = new BattleLog()) {
			DataSet ds = oBattleLog.GetOneSimpleData(oBattleLogSeekCondition);

			if (ds.Tables[0].Rows.Count > 0) {
				sPartnerUserSeq = ds.Tables[0].Rows[0]["PARTNER_USER_SEQ"].ToString();
				sPartnerUserCharNo = ds.Tables[0].Rows[0]["PARTNER_USER_CHAR_NO"].ToString();
				sPartnerGameCharcterType = ds.Tables[0].Rows[0]["PARTNER_GAME_CHARACTER_TYPE"].ToString();
			}
		}
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo,string pSexCd) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				pSexCd
			);
		}
	}
}
