/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹް� ������
--	Progaram ID		: RegistGameRecoveryMissionForceFull
--
--  Creation Date	: 2011.09.15
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

public partial class ViComm_man_RegistGameRecoveryMissionForceFull:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
		} else {
			if (this.Request.Params[ViCommConst.BUTTON_NEXT_LINK] != null) {
				this.RecoveryMissionForceFull();
			}
		}
	}

	private void RecoveryMissionForceFull() {
		string result = this.RecoveryMissionForceFullExecute();
		
		if(result.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			this.CheckQuestClear();
		}

		if (result.Equals(PwViCommConst.GameRecoveryForceFullStatus.RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_RECOVERY_MISSION_FORCE_FULL_COMPLETE);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}

	private string RecoveryMissionForceFullExecute() {
		string sResult = string.Empty;
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		int iUseCoopPointFlag = ViCommConst.FLAG_ON;
		int iMskForce = PwViCommConst.GameMskForce.MSK_MISSION_FORCE;

		RecoveryForce oRecoveryForce = new RecoveryForce();
		oRecoveryForce.RecoveryForceFull(sSiteCd,sUserSeq,sUserCharNo,iUseCoopPointFlag,null,iMskForce,out sResult);

		return sResult;
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.USE_COOP_POINT,
				PwViCommConst.GameQuestTrialCategoryDetail.USE_COOP_POINT,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}