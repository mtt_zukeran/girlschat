/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o�^�t�H�[��(�ő̎��ʒʒm�o�^��)
--	Progaram ID		: RegistAddOnInfo
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Net;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistAddOnInfo:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			ViewState["UTN"] = iBridUtil.GetStringValue(Request.QueryString["utn"]);
			ViewState["GUID"] = iBridUtil.GetStringValue(Request.QueryString["id"]);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		lblErrorMessage.Text = string.Empty;

		string sUserSeq = string.Empty;
		string sUserStatus = string.Empty;
		string sResult = string.Empty;

		if (sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS) ||
			sessionMan.carrier.Equals(ViCommConst.IPHONE) ||
			sessionMan.carrier.Equals(ViCommConst.ANDROID)) {

			using (TempRegist oTempRegist = new TempRegist()) {
				string sLoginId,sLoginPassword;
				if (oTempRegist.GetLoginIdPass(sessionMan.userMan.tempRegistId,out sLoginId,out sLoginPassword)) {
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_LOGINID,string.Empty,sLoginId,sLoginPassword,Session.SessionID,out sUserSeq,out sUserStatus,out sResult);
				}
			}
		} else {
			if (sessionMan.getTermIdFlag) {
				sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_UTN,ViewState["UTN"].ToString(),"","",Session.SessionID,out sUserSeq,out sUserStatus,out sResult);
			} else {
				sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_IMODE_ID,ViewState["GUID"].ToString(),"","",Session.SessionID,out sUserSeq,out sUserStatus,out sResult);
			}
		}

		if (sResult.Equals("0")) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.site.userTopId + "?first=1"));
		} else {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_MAN_REJECT_ICLOUD_MAIL, 2)) {
					using (ManRejectEmailAddr oManRejectEmailAddr = new ManRejectEmailAddr()) {
						if (oManRejectEmailAddr.IsExist(sessionMan.userMan.tempRegistId)) {
							lblErrorMessage.Text = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAN_REJECT_ICLOUD_MAIL);
						}
					}
				}
			}

			if (string.IsNullOrEmpty(lblErrorMessage.Text)) {
				lblErrorMessage.Text = GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_LOGIN);
			}
		}
	}
}
