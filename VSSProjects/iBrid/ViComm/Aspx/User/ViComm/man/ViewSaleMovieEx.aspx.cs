/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 販売動画詳細
--	Progaram ID		: ViewSaleMovieEx
--
--  Creation Date	: 2010.06.18
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_ViewSaleMovieEx:MobileManPageBase {
	private const string ACTION_DOWNLOAD = "download";

	private string castSeq = null;
	private string movieSeq = null;
	private string action = null;
	private string filenm = null;
	private string usetype = null;

	private string[] fileNmArray = null;
	private string loginId = null;
	private string fileFormat = null;
	private string sizeType = null;

	virtual protected void Page_Load(object sender,EventArgs e) {
		action = iBridUtil.GetStringValue(Request.QueryString["action"]);
		filenm = iBridUtil.GetStringValue(Request.QueryString["filenm"]);
		castSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
		movieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		usetype = iBridUtil.GetStringValue(Request.QueryString["usetype"]);

		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

		// 動画の情報を取得
		using (CastMovieFile oCastMovieFile = new CastMovieFile()) {
			oCastMovieFile.GetCastMovieFile(
				sessionMan.site.siteCd,
				castSeq,
				sCastCharNo,
				movieSeq,
				sessionMan.carrier,
				iBridUtil.GetStringValue(sessionMan.modelName),
				usetype,
				out fileNmArray,
				out loginId,
				out sizeType,
				out fileFormat);
		}

		if (!IsPostBack) {
			bool bDownload = false;
			int iChargePoint = 0;


			bool bDupUsed = false;
			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				bDupUsed = oObjUsedHistory.GetOne(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.ATTACHED_MOVIE.ToString(),
									movieSeq);
			}
			if (!bDupUsed) {
				this.RedirectToConfirmSaleMovieEx();
			}

			if (action.Equals(ACTION_DOWNLOAD)) {
				// =============================
				// 　ﾀﾞｳﾝﾛｰﾄﾞ処理
				// =============================	
				if (!movieSeq.Equals(string.Empty)) {
					sessionMan.errorMessage = string.Empty;
					bDownload = Download(castSeq,sCastCharNo,movieSeq,filenm,loginId,usetype,iChargePoint);
				};
			}

			if (!bDownload) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				ulong uSeekMode;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
					sessionMan.ControlList(Request,uSeekMode,ActiveForm);
				} else {
					sessionMan.ControlList(Request,ViCommConst.INQUIRY_SALE_MOVIE,ActiveForm);
				}
				// =============================
				// 　ﾌｧｲﾙﾘｽﾄ表示
				// =============================
				if (fileNmArray.Length > 0) {
					this.lstFileNmList.DataSource = fileNmArray;
					this.lstFileNmList.DataBind();
				}

				if (!string.IsNullOrEmpty(sizeType) && !string.IsNullOrEmpty(fileFormat)) {
					this.lblMovieInfo.Text = string.Format("■{0}({1})",sizeType.ToUpper(),fileFormat.ToUpper());
				}
			}
		}
	}

	protected string GetDownloadTag(MobileListItem pMobileListItem) {
		string sFileNm = (string)pMobileListItem.DataItem;
		string sObjTag = string.Empty;
		string sNum = string.Format("{0:000}",pMobileListItem.Index + 1);
		string sUrl = GenerateLinkUrl(pMobileListItem);

		if (ViCommConst.DOCOMO.Equals(sessionMan.carrier)) {
			if (!usetype.Equals(ViCommConst.TARGET_USE_TYPE_STREAMING)) {
				sObjTag = string.Format("&nbsp;<OBJECT declare id=\"obj{0}\" data=\"{1}\" type=\"video/3gpp\">",sNum,sUrl) +
								string.Format("</OBJECT><A href=\"#obj{0}\">{1}</A>",sNum,sNum);
			} else {
				sObjTag = string.Format("&nbsp;<OBJECT declare id=\"obj{0}\" data=\"{1}\" type=\"video/3gpp\">",sNum,sUrl) +
											"<PARAM name=\"stream-type\" value=\"10\" valuetype=\"data\">" +
								string.Format("</OBJECT><A href=\"#obj{0}\">{1}</A>",sNum,sNum);
			}

		} else if (ViCommConst.SOFTBANK.Equals(sessionMan.carrier)) {
			sObjTag = string.Format("&nbsp;<A href=\"{0}\">{1}</A>",sUrl,sNum);

		} else if (ViCommConst.KDDI.Equals(sessionMan.carrier)) {
			if (!usetype.Equals(ViCommConst.TARGET_USE_TYPE_STREAMING)) {
				sObjTag = string.Format("<object data=\"{0}&audummy=1\" type=\"video/3gpp2\" copyright=\"no\" standby=\"{1}\" >", sUrl, sNum) +
										"<param name=\"disposition\" value=\"devmpzz\" valuetype=\"data\"/>" +
										string.Format("<param name=\"title\" value=\"MOVIE{0}\" valuetype=\"data\"/></object>", filenm);										
			} else {
				sObjTag = string.Format("<object data=\"{0}&audummy=1\" type=\"video/3gpp2\" copyright=\"yes\" standby=\"{1}\" >", sUrl, sNum) +
										"<param name=\"disposition\" value=\"devmpzz\" valuetype=\"data\"/>" +
										string.Format("<param name=\"title\" value=\"MOVIE{0}\" valuetype=\"data\"/></object>", sNum);
			}
		}
		return sObjTag;
	}


	private bool Download(string pCastSeq,string pCastCharNo,string pMovieSeq,string pFileNm,string pLoginId,string useType,int pChargePoint) {

		ParseHTML oParseHTML = sessionMan.parseContainer;

		FileInfo oFileInfo = null;

		string sFilePath = string.Format(@"{0}{1}\{2}\{3}\{4}\{5}",sessionMan.site.webPhisicalDir,ViCommConst.MOVIE_DIRECTRY,sessionMan.site.siteCd,"operator",pLoginId,pFileNm);

		// 動画ﾌｧｲﾙを格納するフォルダへアクセス可能なユーザーに偽装。
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			using (Access oAccess = new Access()) {
				oAccess.AccessMoviePage(sessionMan.site.siteCd,pCastSeq,pCastCharNo,pMovieSeq);
			}

			// ﾌｧｲﾙｻｲｽﾞ取得
			if (File.Exists(sFilePath))
				oFileInfo = new FileInfo(sFilePath);

			if (oFileInfo == null || oFileInfo.Length == 0)
				return false;

			if (oFileInfo.Extension.ToLower().Equals(ViCommConst.MOVIE_FOODER2.ToLower())) {
				Response.ContentType = "video/3gpp2";
			} else if (oFileInfo.Extension.ToLower().Equals(ViCommConst.MOVIE_FOODER3.ToLower())) {
				Response.ContentType = "video/mp4";
			} else {
				Response.ContentType = "video/3gpp";
			}

			if (ViCommConst.KDDI.Equals(sessionMan.carrier)) {
				if (useType.Equals(ViCommConst.TARGET_USE_TYPE_STREAMING)) {
					Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", oFileInfo.Name));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(
						sessionMan.root + sessionMan.sysType,
						sessionMan.sessionId,
						string.Format("movie/{0}/operator/{1}/{2}", sessionMan.site.siteCd, pLoginId, oFileInfo.Name)));
				}
			} else {
				Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", oFileInfo.Name));
			}

			if (ViCommConst.SOFTBANK.Equals(sessionMan.carrier)) {
				Response.AddHeader("Cache-Control","no-store");
			}
			Response.AddHeader("Content-Length",oFileInfo.Length.ToString());
			Response.Clear();
			Response.WriteFile(oFileInfo.FullName);
			Response.Flush();
			Response.Close();
			Response.End();
			return true;
		}
	}

	private void RedirectToConfirmSaleMovieEx() {
		System.Text.StringBuilder oUrlBuilder = new System.Text.StringBuilder();

		oUrlBuilder.Append("ConfirmSaleMovieEx.aspx?");
		oUrlBuilder.Append("direct=1").Append("&");
		oUrlBuilder.Append("action=").Append("&");
		oUrlBuilder.AppendFormat("filenm={0}",this.filenm).Append("&");
		oUrlBuilder.AppendFormat("castseq={0}",this.castSeq).Append("&");
		oUrlBuilder.AppendFormat("movieseq={0}",this.movieSeq).Append("&");
		oUrlBuilder.AppendFormat("usetype={0}",this.usetype).Append("&");
		oUrlBuilder.AppendFormat("pageno={0}",Request.QueryString["pageno"]).Append("&");
		oUrlBuilder.AppendFormat("userrecno={0}",Request.QueryString["userrecno"]).Append("&");
		oUrlBuilder.AppendFormat("loginid={0}",Request.QueryString["loginid"]).Append("&");
		oUrlBuilder.AppendFormat("category={0}",Request.QueryString["category"]).Append("&");
		oUrlBuilder.AppendFormat("{0}={1}",sessionMan.site.charNoItemNm,Request.QueryString[sessionMan.site.charNoItemNm]).Append("&");
		oUrlBuilder.AppendFormat("movieseries={0}",Request.QueryString["movieseries"]).Append("&");
		oUrlBuilder.AppendFormat("sort={0}",Request.QueryString["sort"]);

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,oUrlBuilder.ToString()));
	}

	protected string GenerateLinkUrl(MobileListItem pMobileListItem) {
		string sFileNm = (string)pMobileListItem.DataItem;
		return string.Format("{0}?action={1}&filenm={2}&castseq={3}&movieseq={4}&usetype={5}",sessionMan.currentAspx,ACTION_DOWNLOAD,sFileNm,castSeq,movieSeq,usetype);
	}
}
