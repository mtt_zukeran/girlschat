﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentSps.aspx.cs" Inherits="ViComm_man_PaymentSps" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="iBrid" Assembly="MobileLib" Namespace="MobileLib" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server" Method="Post">
		$PGM_HTML01;
		$PGM_HTML02;
		<mobile:SelectionList ID="lstPack" Runat="server"></mobile:SelectionList>
		<iBrid:iBMobileLiteralText ID="ctrlCheckoutForm" runat="server"/>
		$PGM_HTML06;
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
