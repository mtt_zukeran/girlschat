/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: EDY�x��
--	Progaram ID		: PaymentEdyIPS
--
--  Creation Date	: 2011.02.04
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentEdyIPS:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);


		if (!IsPostBack) {
			string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);

			DataSet ds;
			ds = GetPackDataSet(ViCommConst.SETTLE_EDY,0);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				lstPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
				if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
					lstPack.SelectedIndex = lstPack.Items.Count - 1;
					lstPack.Visible = false;
					sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
					sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
					break;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}

	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSalesAmt = lstPack.Items[lstPack.SelectedIndex].Value;
		string sSid = string.Empty;
		int iExPoint = 0;

		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_EDY,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_IPS,
					ViCommConst.SETTLE_EDY,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					"",
					"",
					out sSid);
				iExPoint = oPack.exPoint;
			}
		}
		string sSettleUrl = string.Empty;

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_IPS,ViCommConst.SETTLE_EDY)) {
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sSettleUrl = string.Format(	oSiteSettle.settleUrl,
											oSiteSettle.cpIdNo,
											"MOBILE",
											sSalesAmt,
											0,
											0,
											sSid,
											sessionMan.userMan.emailAddr,
											sessionMan.userMan.tel,
											"ja");
			}
		}
		RedirectToMobilePage(sSettleUrl);
	}
}
