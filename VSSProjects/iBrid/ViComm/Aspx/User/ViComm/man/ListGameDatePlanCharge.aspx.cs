/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�ް����݈ꗗ(�L��)
--	Progaram ID		: ListGameTownCharge
--
--  Creation Date	: 2011.08.24
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameDatePlanCharge:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_GAME_DATE_PLAN_CHARGE,this.ActiveForm);
		} else {
			string sDatePlanSeq = string.Empty;
			string sPartnerUserSeq = iBridUtil.GetStringValue(Request.Params["partner_user_seq"]);
			string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.Params["partner_user_char_no"]);

			for (int i = 1;i <= 10;i++) {
				if (Request.Form[ViCommConst.BUTTON_GOTO_LINK + "_" + i] != null) {
					sDatePlanSeq = iBridUtil.GetStringValue(Request.Params["date_plan_seq" + i.ToString()]);
				}
			}

			if (!sDatePlanSeq.Equals(string.Empty) && !sPartnerUserSeq.Equals(string.Empty) && !sPartnerUserCharNo.Equals(string.Empty)) {
				string sResult = string.Empty;
				string sPreBalPoint = this.sessionMan.userMan.balPoint.ToString();

				string sPreFriendlyPoint = null;

				FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
				oCondition.SiteCd = this.sessionMan.site.siteCd;
				oCondition.UserSeq = this.sessionMan.userMan.userSeq;
				oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
				oCondition.PartnerUserSeq = sPartnerUserSeq;
				oCondition.PartnerUserCharNo = sPartnerUserCharNo;

				using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
					sPreFriendlyPoint = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
				}
							
				using (DatePlan oDatePlan = new DatePlan()) {
					sResult = oDatePlan.GetDate(
						this.sessionMan.site.siteCd,
						this.sessionMan.userMan.userSeq,
						this.sessionMan.userMan.userCharNo,
						ref sPartnerUserSeq,
						ref sPartnerUserCharNo,
						sDatePlanSeq
					);
				}

				sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

				RedirectToMobilePage(sessionMan.GetNavigateUrl(
					sessionMan.root + sessionMan.sysType,
					sessionMan.sessionId,
					string.Format("ViewGameCharacterDateResult.aspx?result={0}&partner_user_seq={1}&partner_user_char_no={2}&date_plan_seq={3}&pre_bal_point={4}&pre_friendly_point={5}",sResult,sPartnerUserSeq,sPartnerUserCharNo,sDatePlanSeq,sPreBalPoint,sPreFriendlyPoint)
				));
			}
		}
	}
}