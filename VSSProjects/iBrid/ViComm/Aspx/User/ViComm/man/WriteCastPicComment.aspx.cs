/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 出演者画像コメント投稿
--	Progaram ID		: WriteCastPicComment
--  Creation Date	: 2013.12.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_WriteCastPicComment:MobileManPageBase {
	private string sPicSeq;

	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (sessionMan.logined == false) {
			if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
			}
			return;
		}

		sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);

		if (string.IsNullOrEmpty(sPicSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			DataSet dsCastPic;

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_CAST_PIC_VIEW,this.ActiveForm,out dsCastPic)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}

			bool bBlogNgUser = false;
			string sCastUserSeq = iBridUtil.GetStringValue(dsCastPic.Tables[0].Rows[0]["USER_SEQ"]);
			string sCastCharNo = iBridUtil.GetStringValue(dsCastPic.Tables[0].Rows[0]["USER_CHAR_NO"]);

			using (BlogNgUser oBlogNgUser = new BlogNgUser()) {
				bBlogNgUser = oBlogNgUser.CheckExistBlogNgUser(
					sessionMan.site.siteCd,
					sCastUserSeq,
					sCastCharNo,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo
				);
			}

			if (bBlogNgUser) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_COMMENT_NG);
				return;
			}

			pnlWrite.Visible = true;
			pnlComplete.Visible = false;
		} else {
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				this.cmdSubmit_Click(sender,e);
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult = string.Empty;
			string sCommentDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtCommentDoc.Text));

			using (CastPicComment oCastPicComment = new CastPicComment()) {
				oCastPicComment.WriteCastPicComment(
					sessionMan.site.siteCd,
					sPicSeq,
					sessionMan.userMan.userSeq,
					sCommentDoc,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.WriteCastPicCommentResult.RESULT_OK)) {
				pnlWrite.Visible = false;
				pnlComplete.Visible = true;
			} else if (sResult.Equals(PwViCommConst.WriteCastPicCommentResult.RESULT_NG_COMMENTED)) {
				sessionMan.errorMessage = "既にｺﾒﾝﾄ済みです";
			} else if (sResult.Equals(PwViCommConst.WriteCastPicCommentResult.RESULT_NG_COMMENT_REFUSE)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_COMMENT_NG);
				return;
			} else if (sResult.Equals(PwViCommConst.WriteCastPicCommentResult.RESULT_NG_CAST_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.ERR_REFUSE_MAN);
				return;
			} else if (sResult.Equals(PwViCommConst.WriteCastPicCommentResult.RESULT_NG_SELF_REFUSE)) {
				RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
				return;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}

	private bool CheckInput() {
		int iMaxLength = 50;
		string sNGWord;

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.Form["maxlength"]))) {
			int.TryParse(iBridUtil.GetStringValue(Request.Form["maxlength"]),out iMaxLength);
		}

		if (string.IsNullOrEmpty(txtCommentDoc.Text)) {
			sessionMan.errorMessage = "ｺﾒﾝﾄを入力してください";
			return false;
		}

		if (txtCommentDoc.Text.Length > iMaxLength) {
			sessionMan.errorMessage = "ｺﾒﾝﾄが長すぎます";
			return false;
		}

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}

		if (sessionMan.ngWord.VaidateDoc(txtCommentDoc.Text,out sNGWord) == false) {
			sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			return false;
		}

		return true;
	}
}
