/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 商品ｵｰｸｼｮﾝ入札
--	Progaram ID		: BidProdAuction
--
--  Creation Date	: 2011.06.20
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_BidProdAuction : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		ulong lSeekMode = ulong.Parse(this.Request.QueryString["seekmode"]);
		if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			lSeekMode = ViCommConst.INQUIRY_PRODUCT_AUCTION;
		}
		
		if (!sessionMan.ControlList(this.Request, lSeekMode, this.ActiveForm)) {
			throw new ApplicationException();
		}

		if (!IsPostBack) {
			DateTime oBlindEndDate = DateTime.Parse(sessionMan.GetProductAuctionValue("BLIND_END_DATE"));
			
			int iBidAmt = 0;
			if(DateTime.Compare(DateTime.Now,oBlindEndDate)<0){
				iBidAmt = int.Parse(sessionMan.GetProductAuctionValue("RESERVE_AMT"));
			}else{
				iBidAmt = int.Parse(sessionMan.GetProductAuctionValue("MAX_BID_AMT"));
				iBidAmt += int.Parse(sessionMan.GetProductAuctionValue("MINIMUM_BID_AMT"));
				
				iBidAmt = Math.Max(iBidAmt,int.Parse(sessionMan.GetProductAuctionValue("RESERVE_AMT")));
			}
			this.txtBidAmt.Text = iBidAmt.ToString();
			
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender, e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender, EventArgs e) {
		if(!this.ValidateForSubmit())return;
		
		using(ProductAuction oProductAuction = new ProductAuction()){
			string sSiteCd = sessionMan.site.siteCd;
			string sUserSeq = sessionMan.userMan.userSeq;
			string sUserCharNo = sessionMan.userMan.userCharNo;
			string sProductAgentCd = sessionMan.GetProductAuctionValue("PRODUCT_AGENT_CD");
			string sProductSeq = sessionMan.GetProductAuctionValue("PRODUCT_SEQ");

			string sResult = oProductAuction.Bid(sSiteCd, sUserSeq, sUserCharNo, sProductAgentCd, sProductSeq, this.txtBidAmt.Text.Trim());
			
			if(sResult.Equals(ViCommConst.AuctionBidStatus.OK)){
				RedirectToDisplayDoc(ViCommConst.SCR_AUCTION_BID_OK);
			}else{
				IDictionary<string,string> oParam = new Dictionary<string,string>();
				oParam.Add("result",sResult);
				RedirectToDisplayDoc(ViCommConst.SCR_AUCTION_BID_ERR,oParam);
			}	
		}
		
	}

	/// <summary>
	/// ﾒｯｾｰｼﾞ登録前の入力検証を行う。
	/// </summary>
	/// <returns>すべての検証に成功した場合はtrue。それ以外はfalse。</returns>
	private bool ValidateForSubmit() {
		bool bIsValid = true;

		this.lblErrorMessage.Text = string.Empty;

		string sBidAmt = this.txtBidAmt.Text.Trim();
		if (string.IsNullOrEmpty(sBidAmt)) {
			bIsValid = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BID_AMT_EMPTY);
		}

		return bIsValid;
	}
}
