<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FindUser.aspx.cs" Inherits="ViComm_man_FindUser" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:Panel ID="pnlOnline" Runat="server">
			<cc1:iBMobileLabel ID="lblOnline" runat="server" BreakAfter="true">
			</cc1:iBMobileLabel>
			<mobile:SelectionList ID="rdoStatus" Runat="server" SelectType="Radio" Alignment="Left">
				<Item Text="全ての女性" Value="0" Selected="true" />
				<Item Text="$PGM_HTML02;" Value="1" Selected="false" />
				<Item Text="$PGM_HTML04;" Value="2" Selected="false" />
			</mobile:SelectionList>
			<mobile:SelectionList ID="chkNewCast" Runat="server" SelectType="CheckBox" Alignment="Left">
				<Item Text="$PGM_HTML05;" Value="1" Selected="false" />
			</mobile:SelectionList>
			<mobile:Panel ID="pnlAge" Runat="server">
				<cc1:iBMobileLabel ID="lblAge" runat="server" BreakAfter="true">年齢</cc1:iBMobileLabel>
				<mobile:SelectionList ID="lstAge" Runat="server" SelectType="DropDown">
					<Item Value=":" Text="指定なし"></Item>
					<Item Value="18:20" Text="18〜20歳"></Item>
					<Item Value="21:24" Text="21〜24歳"></Item>
					<Item Value="25:29" Text="25〜29歳"></Item>
					<Item Value="30:34" Text="30〜34歳"></Item>
					<Item Value="35:39" Text="35〜39歳"></Item>
					<Item Value="40:" Text="40歳以上"></Item>
				</mobile:SelectionList>
			</mobile:Panel>
			<cc1:iBMobileLiteralText ID="tagLinesOnline" runat="server" Text="$PGM_HTML03;" />
			<cc1:iBMobileLabel ID="lblHandelNm" runat="server" BreakAfter="true">名前</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLinesHandelNm" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem1" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem1" runat="server" BreakAfter="true">castItemm01</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem1" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem1" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem2" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem2" runat="server" BreakAfter="true">castItemm02</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem2" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem2" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines2" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem3" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem3" runat="server" BreakAfter="true">castItemm03</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem3" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem3" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines3" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem4" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem4" runat="server" BreakAfter="true">castItemm04</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem4" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem4" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines4" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem5" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem5" runat="server" BreakAfter="true">castItemm05</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem5" Runat="server" BreakAfter="false"><Item Text="指定なし" Value="*" /></mobile:SelectionList>〜
			<mobile:SelectionList ID="lstCastItem5Max" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem5" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines5" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem6" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem6" runat="server" BreakAfter="true">castItemm06</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem6" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem6" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines6" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem7" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem7" runat="server" BreakAfter="true">castItemm07</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem7" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem7" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines7" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem8" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem8" runat="server" BreakAfter="true">castItemm08</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem8" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem8" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines8" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem9" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem9" runat="server" BreakAfter="true">castItemm09</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem9" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem9" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines9" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem10" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem10" runat="server" BreakAfter="true">castItemm10</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem10" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem10" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem11" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem11" runat="server" BreakAfter="true">castItemm11</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem11" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem11" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem12" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem12" runat="server" BreakAfter="true">castItemm12</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem12" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem12" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem13" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem13" runat="server" BreakAfter="true">castItemm13</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem13" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem13" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem14" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem14" runat="server" BreakAfter="true">castItemm14</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem14" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem14" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem15" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem15" runat="server" BreakAfter="true">castItemm15</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem15" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem15" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem16" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem16" runat="server" BreakAfter="true">castItemm16</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem16" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem16" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem17" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem17" runat="server" BreakAfter="true">castItemm17</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem17" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem17" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem18" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem18" runat="server" BreakAfter="true">castItemm18</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem18" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem18" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem19" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem19" runat="server" BreakAfter="true">castItemm19</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem19" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem19" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem20" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem20" runat="server" BreakAfter="true">castItemm20</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem20" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem20" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<br />
		<mobile:Panel ID="pnlOkList" Runat="server">
			<cc1:iBMobileLabel ID="lblOkList" runat="server" BreakAfter="true">OKﾘｽﾄ</cc1:iBMobileLabel>
			<mobile:SelectionList ID="chkOK0" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK1" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK2" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK3" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK4" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK5" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK6" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK7" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK8" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK9" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK10" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK11" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK12" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK13" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK14" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK15" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK16" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK17" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK18" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK19" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK20" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK21" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK22" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK23" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK24" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK25" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK26" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK27" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK28" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK29" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK30" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK31" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK32" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK33" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK34" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK35" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK36" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK37" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK38" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK39" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines21" runat="server" Text="$PGM_HTML03;" />
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlComment" Runat="server">
			<cc1:iBMobileLabel ID="lblComment" runat="server" BreakAfter="true">コメントに</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtComment" runat="server" MaxLength="20" Size="12" BreakAfter="false"></cc1:iBMobileTextBox>
			<cc1:iBMobileLabel ID="lblComment2" runat="server" BreakAfter="true">を含む</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagLinesComment" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<br />
		<br />
		$PGM_HTML06;
		$DATASET_LOOP_START0;
		$PGM_HTML07;
		$DATASET_LOOP_END0;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
