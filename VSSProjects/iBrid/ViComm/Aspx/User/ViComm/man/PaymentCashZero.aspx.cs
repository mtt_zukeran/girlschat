/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ��������(ZERO)
--	Progaram ID		: PaymentCashZero
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Net;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCashZero:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.userMan.billAmt == 0) {
				string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);
				DataSet ds;
				ds = GetPackDataSet(ViCommConst.SETTLE_AUTO_RECEIPT,0);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstCreditPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
					if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
						lstCreditPack.SelectedIndex = lstCreditPack.Items.Count - 1;
						lstCreditPack.Visible = false;
						sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
						sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
						break;
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSid = "";
		string sSalesAmt;

		sSalesAmt = lstCreditPack.Items[lstCreditPack.SelectedIndex].Value;
		using (Pack oPack = new Pack())
		using (SettleLog oLog = new SettleLog()) {
			if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_AUTO_RECEIPT,sSalesAmt,sessionMan.userMan.userSeq)) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_ZERO,
					ViCommConst.SETTLE_AUTO_RECEIPT,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					int.Parse(sSalesAmt),
					oPack.exPoint,
					"",
					"",
					out sSid);
				sessionMan.userMan.settleRequestAmt = int.Parse(sSalesAmt);
				sessionMan.userMan.settleRequestPoint = oPack.exPoint;
			}
		}

		string sSettleUrl = "";
		string sBackUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {

			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_AUTO_RECEIPT)) {
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sessionMan.userMan.tel,sBackUrl);
				string sRedirect = sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"PostCashZero.aspx" + "?" + sSettleUrl);
				RedirectToMobilePage(sRedirect);
			}
		}
	}
}
