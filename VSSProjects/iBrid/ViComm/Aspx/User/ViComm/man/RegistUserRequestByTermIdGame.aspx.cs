/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: Ήή°Ρo^\Ώ(ΕΜ―ΚOΚm^)
--	Progaram ID		: RegistUserRequestByTermIdGame
--
--  Creation Date	: 2011.07.21
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Collections;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistUserRequestByTermIdGame:MobileSocialGameManBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		ParseHTML oParseMan = sessionMan.parseContainer;
		if (sessionMan.getTermIdFlag) {
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
		} else {
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_GUID;
		}

		if (!IsPostBack) {

			// UσΤΜκAU’IΉζΚΦ]
			if (sessionMan.IsImpersonated) {
				oParseMan.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + "/ViComm/man",Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_RETURN_TO_ORIGINAL_MENU));
			}

			if (sessionMan.carrier.Equals(ViCommConst.DOCOMO) && iBridUtil.GetStringValue(Request.QueryString["guid"]).Equals(string.Empty)) {
				UrlBuilder oUrlBuilder = new UrlBuilder("RegistUserRequestByTermIdGame.aspx",Request.QueryString);
				oUrlBuilder.Parameters.Add("guid","on");
				oParseMan.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
			}

		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUtn = string.Empty;
		ParseHTML oParseMan = sessionMan.parseContainer;
		oParseMan.parseUser.postAction = 0;

		if (sUtn.Equals("")) {
			sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
		}
		string siModeId = Mobile.GetiModeId(sessionMan.carrier,Request);

		sessionMan.errorMessage = string.Empty;
		lblErrorMessage.Text = string.Empty;

		string sResult;
		bool bOk = true;

		using (Man oMan = new Man()) {
			if (!sUtn.Equals(string.Empty) && oMan.IsBlackUser(ViCommConst.NO_TYPE_TERMINAL_UNIQUE_ID,sUtn)) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
				bOk = false;
				return;
			}
			if (!siModeId.Equals(string.Empty) && oMan.IsBlackUser(ViCommConst.NO_TYPE_TERMINAL_UNIQUE_ID,siModeId)) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
				bOk = false;
				return;
			}
		}

		Random rnd = new Random();
		string sPassWord = string.Format("{0:D4}",rnd.Next(10000));

		string sAge = iBridUtil.GetStringValue(this.Request.Params["checkAge"]);

		if (string.IsNullOrEmpty(sAge)) {
			lblErrorMessage.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.GAME_CAN_NOT_REGISTER),"18Ξ’ΜϋΝ");
			bOk = false;
		}

		string sHandleNm = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier, txtHandleNm.Text));
		txtHandleNm.Text = HttpUtility.HtmlEncode(txtHandleNm.Text);
		if (sHandleNm.Equals("")) {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			bOk = false;
		} else if (!SysPrograms.IsWithinByteCount(HttpUtility.HtmlEncode(sHandleNm), 60)) {
			bOk = false;
			lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_OVER_BYTE_COUNT), "ΚέΔήΩΘ°Ρ");
		}

		if (bOk) {
			if (!sessionMan.carrier.Equals(ViCommConst.ANDROID) && !sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
				if (sessionMan.getTermIdFlag) {
					if (sUtn.Equals("") || siModeId.Equals("")) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
					}
				} else {
					if (siModeId.Equals(string.Empty)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
						bOk = false;
					}
				}
			}
		}

		if ((bOk & this.CheckOther()) == false) {
			return;
		}

		string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];

		using (TempRegist oRegist = new TempRegist()) {
			oRegist.RegistUserManTemp(
				sUtn,
				siModeId,
				Request.UserHostAddress,
				string.Empty,
				sPassWord,
				sHandleNm,
				string.Empty,
				string.Empty,
				string.Empty,
				out sessionMan.userMan.tempRegistId,
				out sResult,
				new string[ViCommConst.MAX_ATTR_COUNT],
				new string[ViCommConst.MAX_ATTR_COUNT],
				new string[ViCommConst.MAX_ATTR_COUNT],
				0,
				new string[0],
				ViCommConst.FLAG_ON,
				string.Empty
			);

			switch (int.Parse(sResult)) {
				case ViCommConst.REG_USER_RST_UNT_EXIST:
					lblErrorMessage.Text += GetErrorMessage(PwViCommConst.PwErrerCode.GAME_STATUS_TERM_ID_ALREADY_EXIST);
					break;
				case ViCommConst.REG_USER_RST_HANDLE_NM_EXIST:
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM_DUPLI);
					break;
			}

		}
		if (sResult.Equals("0")) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("RegistAddOnInfoByGame.aspx?utn={0}&id={1}",sUtn,siModeId)));
		} else {
			return;
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
