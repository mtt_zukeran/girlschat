/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���L�ڍ�
--	Progaram ID		: ViewDiary
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewDiary:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}
			
			tagComplete.Visible = false;
			tagLiked.Visible = false;

			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			DataRow dr;
			bool bControlList = false;
			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo,out dr)) {
				ulong uSeekMode;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode)) {
					bControlList = sessionMan.ControlList(Request,uSeekMode,ActiveForm);
				} else {
					bControlList = sessionMan.ControlList(Request,ViCommConst.INQUIRY_CAST_DIARY,ActiveForm);
				}
			}
			if (bControlList) {
				bool bDiarySwitchScreen = false;
				using (ManageCompany oManageCompany = new ManageCompany()) {
					bDiarySwitchScreen = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DIARY_SWITCH_SCREEN,2);
				}

				if (!bDiarySwitchScreen || string.IsNullOrEmpty(Request.QueryString["scrid"])) {
					using (Access oAccess = new Access()) {
						oAccess.AccessDiaryPage(
							sessionMan.site.siteCd,
							dr["USER_SEQ"].ToString(),
							dr["USER_CHAR_NO"].ToString(),
							sessionMan.userMan.userSeq
						);
					}
				}

				string sCastSeq = sessionMan.GetDiaryValue("USER_SEQ");
				string sReportDay = sessionMan.GetDiaryValue("REPORT_DAY");
				string sSubSeq = sessionMan.GetDiaryValue("CAST_DIARY_SUB_SEQ");

				using (CastDiary oCastDiary = new CastDiary()) {
					oCastDiary.UpdateReadingCount(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sReportDay,sSubSeq);
					if (iBridUtil.GetStringValue(Request.QueryString["like"]).Equals(ViCommConst.FLAG_ON_STR)) {

						using (Refuse oRefuse = new Refuse()) {
							if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sCastSeq,sCastCharNo)) {
								RedirectToDisplayDoc(ViCommConst.SCR_REFUSED_CAST);
							}
						}
						
						string sResult;
						oCastDiary.UpdateLikeCount(sessionMan.site.siteCd,sCastSeq,sCastCharNo,sReportDay,sSubSeq,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,out sResult);
						if (sResult.Equals(PwViCommConst.RegistDiaryLikeResult.RESULT_OK)) {
							tagComplete.Visible = true;
						} else if (sResult.Equals(PwViCommConst.RegistDiaryLikeResult.RESULT_LIKED)) {
							tagLiked.Visible = true;
						} else {
							RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
						}
					}
				}
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListDiary.aspx"));
			}
		}
	}

}
