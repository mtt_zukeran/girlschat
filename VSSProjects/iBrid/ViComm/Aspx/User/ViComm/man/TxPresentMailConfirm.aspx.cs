/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: プレゼントメール送信プレビュー
--	Progaram ID		: TxPresentMailConfirm
--
--  Creation Date	: 2014.12.02
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  XXXX/XX/XX  XXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_TxPresentMailConfirm:MobileMailManPage {

	protected override void SetControl() {
		MailTitle = txtTitle;
		MailDoc = txtDoc.Text;
		ErrorMsg = lblErrorMessage;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sMailDataSeq = iBridUtil.GetStringValue(Request.QueryString["data"]);
			ViewState["MAIL_DATA_SEQ"] = sMailDataSeq;
			
			string sPresentMailItemSeq = iBridUtil.GetStringValue(Request.QueryString["item"]);
			ViewState["PRESENT_MAIL_ITEM_SEQ"] = sPresentMailItemSeq;

			txtTitle.Text = sessionMan.userMan.mailData[sMailDataSeq].mailTitle;
			txtDoc.Text = sessionMan.userMan.mailData[sMailDataSeq].mailDoc;

			sessionMan.SetCastDataSetByUserSeq(sessionMan.userMan.mailData[sMailDataSeq].rxUserSeq,sessionMan.userMan.mailData[sMailDataSeq].rxUserCharNo,1);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdBack"] != null) {
				cmdBack_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		sessionMan.userMan.mailData[sMailDataSeq].presentMailItemSeq = iBridUtil.GetStringValue(ViewState["PRESENT_MAIL_ITEM_SEQ"]);
		bool isReturnMail = sessionMan.userMan.mailData[sMailDataSeq].isReturnMail;
		Submit(iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]),isReturnMail,ViCommConst.SCR_TX_MAIL_COMPLITE_MAN);
	}

	protected void cmdBack_Click(object sender,EventArgs e) {
		string sMailDataSeq = iBridUtil.GetStringValue(ViewState["MAIL_DATA_SEQ"]);
		string sRedirectUrl = string.Empty;
		if (sessionMan.userMan.mailData[sMailDataSeq].isReturnMail) {
			sRedirectUrl = string.Format("ReturnMail.aspx?data={0}",sMailDataSeq);
			if (sessionMan.userMan.mailData[sMailDataSeq].chatMailFlag == ViCommConst.FLAG_ON) {
				sRedirectUrl = sRedirectUrl + "&scrid=01&stay=1#btm";
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sRedirectUrl));
		} else {
			sRedirectUrl = string.Format("TxMail.aspx?data={0}",sMailDataSeq);
			if (sessionMan.userMan.mailData[sMailDataSeq].chatMailFlag == ViCommConst.FLAG_ON) {
				sRedirectUrl = sRedirectUrl + string.Format("&loginid={0}&userrecno=1&scrid=01&stay=1#btm",sessionMan.userMan.mailData[sMailDataSeq].rxLoginId);
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sRedirectUrl));
		}
	}
}
