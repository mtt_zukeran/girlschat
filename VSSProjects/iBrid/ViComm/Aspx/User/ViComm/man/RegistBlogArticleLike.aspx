<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistBlogArticleLike.aspx.cs" Inherits="ViComm_man_RegistBlogArticleLike" %>
<%@ Register TagPrefix="ibrid"		Namespace="MobileLib"						Assembly="MobileLib"%>
<%@ Register TagPrefix="mobile"		Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLiteralText ID="tagComplete"	runat="server" Text="$PGM_HTML02;" />
		<ibrid:iBMobileLiteralText ID="tagError" runat="server" Text="$PGM_HTML03;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>