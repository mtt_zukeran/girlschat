<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentBitCash.aspx.cs" Inherits="ViComm_man_PaymentBitCash" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		<mobile:SelectionList ID="lstPack" Runat="server"></mobile:SelectionList>
		$PGM_HTML02;
		<cc1:iBMobileTextBox ID="txtCol1" runat="server" MaxLength="8" Size="8"></cc1:iBMobileTextBox>
		$PGM_HTML03;
		<cc1:iBMobileTextBox ID="txtCol2" runat="server" MaxLength="8" Size="8"></cc1:iBMobileTextBox>
		$PGM_HTML04;
		<cc1:iBMobileTextBox ID="txtCol3" runat="server" MaxLength="8" Size="8"></cc1:iBMobileTextBox>
		$PGM_HTML05;
		<cc1:iBMobileTextBox ID="txtCol4" runat="server" MaxLength="8" Size="8"></cc1:iBMobileTextBox>
		$PGM_HTML06;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
