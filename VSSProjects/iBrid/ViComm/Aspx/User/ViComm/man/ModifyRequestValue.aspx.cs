/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: おねがい評価編集
--	Progaram ID		: ModifyRequestValue
--
--  Creation Date	: 2012.06.26
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ModifyRequestValue:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sRequestValueSeq = iBridUtil.GetStringValue(Request.QueryString["val_seq"]);

		if (string.IsNullOrEmpty(sRequestValueSeq)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			pnlWrite.Visible = true;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = false;

			DataSet dsRequestValue = getRequestValue(sRequestValueSeq);

			if (dsRequestValue.Tables[0].Rows.Count > 0) {
				DataRow drRequestValue = dsRequestValue.Tables[0].Rows[0];
				
				if (!this.checkAccess(drRequestValue)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
				
				this.setFormValue(drRequestValue);
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_TX] != null) {
				cmdTx_Click(sender,e,sRequestValueSeq);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	private DataSet getRequestValue(string pRequestValueSeq) {
		DataSet dsRequestValue;
		RequestValueSeekCondition oCondition = new RequestValueSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.SexCd = ViCommConst.MAN;
		oCondition.RequestValueSeq = pRequestValueSeq;
		using (RequestValue oRequestValue = new RequestValue()) {
			dsRequestValue = oRequestValue.GetPageCollection(oCondition,1,1);
		}
		return dsRequestValue;
	}

	private bool checkAccess(DataRow drRequestValue) {
		if (!sessionMan.userMan.userSeq.Equals(iBridUtil.GetStringValue(drRequestValue["USER_SEQ"])) || !sessionMan.userMan.userCharNo.Equals(iBridUtil.GetStringValue(drRequestValue["USER_CHAR_NO"]))) {
			return false;
		}

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drRequestValue["COMMENT_TEXT"])) && iBridUtil.GetStringValue(drRequestValue["COMMENT_PUBLIC_STATUS"]).Equals(PwViCommConst.RequestPublicStatus.APPLY)) {
			return false;
		}

		return true;
	}

	private void setFormValue(DataRow drRequestValue) {
		foreach (MobileListItem item in rdoValue.Items) {
			if (item.Value == iBridUtil.GetStringValue(drRequestValue["VALUE_NO"])) {
				item.Selected = true;
			}
		}

		txtComment.Text = iBridUtil.GetStringValue(drRequestValue["COMMENT_TEXT"]);
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;

		if (rdoValue.Items[rdoValue.SelectedIndex].Value.Equals(string.Empty)) {
			sessionMan.errorMessage = "評価を選択して下さい";
			return;
		}

		if (!txtComment.Text.Equals(string.Empty)) {
			if (txtComment.Text.Length > 100) {
				sessionMan.errorMessage = "ｺﾒﾝﾄが長すぎます";
				return;
			}

			string sNGWord = string.Empty;

			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}

			if (!sessionMan.ngWord.VaidateDoc(txtComment.Text,out sNGWord)) {
				sessionMan.errorMessage = string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
				return;
			}
		}

		ViewState["VALUE_NO"] = rdoValue.Items[rdoValue.SelectedIndex].Value;
		ViewState["COMMENT_TEXT"] = txtComment.Text;

		lblValue.Text = rdoValue.Items[rdoValue.SelectedIndex].Text;
		lblComment.Text = HttpUtility.HtmlEncode(txtComment.Text).Replace(System.Environment.NewLine,"<br>");

		pnlWrite.Visible = false;
		pnlConfirm.Visible = true;
		pnlComplete.Visible = false;

		if (string.IsNullOrEmpty(txtComment.Text)) {
			pnlConfirmNoComment.Visible = true;
			pnlConfirmComment.Visible = false;
		} else {
			pnlConfirmNoComment.Visible = false;
			pnlConfirmComment.Visible = true;
		}
	}

	protected void cmdTx_Click(object sender,EventArgs e,string pRequestValueSeq) {
		sessionMan.errorMessage = string.Empty;
		string sValueNo = iBridUtil.GetStringValue(ViewState["VALUE_NO"]);
		string sCommentText = iBridUtil.GetStringValue(ViewState["COMMENT_TEXT"]);
		string sCommentPublicStatus = string.Empty;
		string sResult = string.Empty;

		using (RequestValue oRequestValue = new RequestValue()) {
			sResult = oRequestValue.ModifyRequestValue(
				sessionMan.site.siteCd,
				pRequestValueSeq,
				sValueNo,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionObj.carrier,sCommentText)),
				out sCommentPublicStatus
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			pnlWrite.Visible = false;
			pnlConfirm.Visible = false;
			pnlComplete.Visible = true;

			if (sCommentPublicStatus.Equals(PwViCommConst.RequestValueCommentPublicStatus.APPLY)) {
				pnlCompleteNoComment.Visible = false;
				pnlCompleteComment.Visible = true;
			} else {
				pnlCompleteNoComment.Visible = true;
				pnlCompleteComment.Visible = false;
			}
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		pnlWrite.Visible = true;
		pnlConfirm.Visible = false;
		pnlComplete.Visible = false;
	}
}
