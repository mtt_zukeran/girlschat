/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o�^�\��(�ő̎��ʎ��O�ʒm�^)
--	Progaram ID		: RegistUserRequestByOpenId
--
--  Creation Date	: 2016.01.11
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistUserRequestByOpenId:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		ParseHTML oParseMan = sessionMan.parseContainer;
		if (sessionMan.getTermIdFlag) {
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
		} else {
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_GUID;
		}

		if (!IsPostBack) {
			// �X�}�[�g�t�H���̏ꍇ�̂ݓo�^��
			if (!sessionMan.carrier.Equals(ViCommConst.ANDROID) && !sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}

			// �U����Ԃ̏ꍇ�A�U���I����ʂ֓]��
			if (sessionMan.IsImpersonated) {
				oParseMan.parseUser.postAction = 0;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + "/ViComm/man",Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WOMAN_RETURN_TO_ORIGINAL_MENU));
			}

			string sFlag;

			using (ManageCompany oCompany = new ManageCompany()) {
				oCompany.GetValue("AUTO_GET_TEL_INFO_FLAG",out sFlag);
			}

			if (!IsAvailableService(ViCommConst.RELEASE_SP_TEL_NOT_ABSOLUTE,2)) {
				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
					sFlag = "0";
				}
			}

			if (sFlag.Equals("0")) {
				lblTel.Visible = true;
				txtTel.Visible = true;
				tagTel.Visible = true;
			} else {
				lblTel.Visible = false;
				txtTel.Visible = false;
				tagTel.Visible = false;
			}
			if (sessionMan.site.registHandleNmInputFlag == 1) {
				lblHandleNm.Visible = true;
				txtHandleNm.Visible = true;
				tagHandleNm.Visible = true;
			} else {
				lblHandleNm.Visible = false;
				txtHandleNm.Visible = false;
				tagHandleNm.Visible = false;
			}
			if (sessionMan.site.registBirthdayInputFlag == 1) {
				CreateList();
				lblBirthday.Visible = true;
				lblYear.Visible = true;
				lstYear.Visible = true;
				lblMonth.Visible = true;
				lstMonth.Visible = true;
				lblDay.Visible = true;
				lstDay.Visible = true;
				tagBirthday.Visible = true;
			} else {
				lblBirthday.Visible = false;
				lblYear.Visible = false;
				lstYear.Visible = false;
				lblMonth.Visible = false;
				lstMonth.Visible = false;
				lblDay.Visible = false;
				lstDay.Visible = false;
				tagBirthday.Visible = false;
			}

			sessionMan.userMan.GetAttrType(sessionMan.site.siteCd);

			int iCtrl = 0;
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				SelectionList lstUserManItem;
				iBMobileTextBox txtUserManItem;
				iBMobileLabel lblUserManItem;
				TextArea txtAreaUserManItem;

				if (sessionMan.userMan.attrList[i].registInputFlag) {
					lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iCtrl + 1)) as SelectionList;
					txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iCtrl + 1)) as iBMobileTextBox;
					lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",iCtrl + 1)) as iBMobileLabel;
					txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",iCtrl + 1)) as TextArea;

					lblUserManItem.Text = sessionMan.userMan.attrList[i].attrTypeNm;
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							txtAreaUserManItem.Visible = true;
							txtUserManItem.Visible = false;
							txtAreaUserManItem.Rows = int.Parse(sessionMan.userMan.attrList[i].rowCount);
						} else {
							txtAreaUserManItem.Visible = false;
							txtUserManItem.Visible = true;
						}
						lstUserManItem.Visible = false;
					} else {
						txtAreaUserManItem.Visible = false;
						txtUserManItem.Visible = false;
						lstUserManItem.Visible = true;
						CreateListUserManItem(lstUserManItem,i);
					}
					iCtrl++;
				}
			}
			for (;iCtrl < ViComm.ViCommConst.MAX_ATTR_COUNT;iCtrl++) {
				Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",iCtrl + 1)) as Panel;
				pnlUserManItem.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		using (User oUser = new User()) {
			string sAddrCheckResult = oUser.CheckEmailAddrDuplication(sessionMan.site.siteCd,sessionMan.snsEmailAddr);
			if (sAddrCheckResult.Equals(PwViCommConst.CheckEmailAddrDuplication.RESULT_NG)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			} else if (!sAddrCheckResult.Equals(PwViCommConst.CheckEmailAddrDuplication.RESULT_OK)) {
				IDictionary<string,string> oParameters = new Dictionary<string,string>();
				oParameters.Add("result",sAddrCheckResult);
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_EMAIL_ADDR_DUPLICATION,oParameters);
			}
		}
		
		txtPassword.Text = HttpUtility.HtmlEncode(txtPassword.Text);
		txtTel.Text = HttpUtility.HtmlEncode(txtTel.Text);
		txtHandleNm.Text = HttpUtility.HtmlEncode(txtHandleNm.Text);
		
		ParseHTML oParseMan = sessionMan.parseContainer;
		oParseMan.parseUser.postAction = 0;

		sessionMan.errorMessage = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		string sNGWord = string.Empty;

		string sResult;
		bool bOk = true;


		using (Man oMan = new Man()) {
			if (oMan.IsBlackUser(ViCommConst.NO_TYPE_TEL,txtTel.Text)) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BLACK_USER);
				bOk = false;
				return;
			}
		}

		if (txtPassword.Visible) {
			if (txtPassword.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD);
				bOk = false;
			} else {
				if (!SysPrograms.Expression(@"^[a-zA-Z0-9]{4,8}$",txtPassword.Text)) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD_NG);
					bOk = false;
				}
			}
		} else {
			Random rnd = new Random();
			txtPassword.Text = string.Format("{0:D4}",rnd.Next(10000));
		}

		if (txtTel.Visible) {
			if (txtTel.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
				bOk = false;
			} else {
				if (!sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					if (txtTel.Text.Length > 11) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
						bOk = false;
					} else if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
						bOk = false;
					} else {
						using (Man oMan = new Man()) {
							string sSiteCd = sessionMan.site.siteCd;
							if (IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE)) {
								sSiteCd = string.Empty;
							}
							if (oMan.IsTelephoneRegistered(sSiteCd,txtTel.Text)) {
								lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
								bOk = false;
							}
						}
					}
				}
			}
		}
		if (sessionMan.site.registHandleNmInputFlag == 1) {
			if (txtHandleNm.Text.Equals("")) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
				bOk = false;
			}
			if (sessionMan.ngWord.VaidateDoc(txtHandleNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}
		if (sessionMan.site.registBirthdayInputFlag == 1) {
			if ((lstYear.Selection.Value.Equals("")) || (lstMonth.Selection.Value.Equals("")) || (lstDay.Selection.Value.Equals(""))) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY);
				bOk = false;
			} else if (lstYear.Selection.Value.Length != 4) {
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_YYYY_NG);
				bOk = false;
			} else {
				if (bOk) {
					string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
					int iAge = ViCommPrograms.Age(sDate);
					int iOkAge;
					int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
					if (iOkAge == 0) {
						iOkAge = 18;
					}

					if (iAge > 99 || iAge == 0) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
					} else if (iAge < iOkAge) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18);
					}
				}
			}
		}

		string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
		string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
		int iCtrl = 0;

		if (bOk) {
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				if (sessionMan.userMan.attrList[i].registInputFlag) {
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",iCtrl + 1)) as TextArea;
							pAttrInputValue[iCtrl] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtAreaUserManItem.Text));
							if (pAttrInputValue[iCtrl].Length > 300) {
								pAttrInputValue[iCtrl] = SysPrograms.Substring(pAttrInputValue[iCtrl],300);
							}
						} else {
							iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",iCtrl + 1)) as iBMobileTextBox;
							pAttrInputValue[iCtrl] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtUserManItem.Text));
						}
						if (pAttrInputValue[iCtrl].Equals(string.Empty) && sessionMan.userMan.attrList[i].profileReqItemFlag) {
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
							bOk = false;
						}
						if (sessionMan.ngWord.VaidateDoc(pAttrInputValue[iCtrl],out sNGWord) == false) {
							bOk = false;
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
						}
					} else {
						SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",iCtrl + 1)) as SelectionList;
						if (lstUserManItem.Selection != null) {
							if (sessionMan.userMan.attrList[i].profileReqItemFlag && lstUserManItem.Selection.Value.Equals(string.Empty)) {
								lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
								bOk = false;
							} else {
								pAttrSeq[iCtrl] = lstUserManItem.Selection.Value;
							}
						}
					}
					pAttrTypeSeq[iCtrl] = sessionMan.userMan.attrList[i].attrTypeSeq;
					iCtrl++;
				}
			}
		}

		ArrayList alSiteCd = new ArrayList();

		if ((bOk & this.CheckOther()) == false) {
			return;
		}

		string sBirthday = "";
		if (sessionMan.site.registBirthdayInputFlag == 1) {
			DateTime dtBiathday = new DateTime(int.Parse(lstYear.Selection.Value),int.Parse(lstMonth.Selection.Value),int.Parse(lstDay.Selection.Value));
			sBirthday = dtBiathday.ToString("yyyy/MM/dd");
		}

		sessionMan.userMan.RegistUserManByOpenId(
			sessionMan.site.siteCd,
			Request.UserHostAddress,
			txtTel.Text,
			txtPassword.Text,
			txtHandleNm.Text,
			sBirthday,
			pAttrTypeSeq,
			pAttrSeq,
			pAttrInputValue,
			iCtrl,
			(string[])alSiteCd.ToArray(typeof(string)),
			out sessionMan.userMan.tempRegistId,
			out sResult
		);
		
		if (sResult.Equals("0")) {
			using (TempRegist oTempRegist = new TempRegist()) {
				string sLoginId;
				string sLoginPassword;
				string sUserSeq;
				string sUserStatus;
				string sLoginResult;
				if (oTempRegist.GetLoginIdPass(sessionMan.userMan.tempRegistId,out sLoginId,out sLoginPassword)) {
					sessionMan.userMan.LoginUser(ViCommConst.LOGIN_BY_LOGINID,string.Empty,sLoginId,sLoginPassword,Session.SessionID,out sUserSeq,out sUserStatus,out sLoginResult);
				}
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sessionMan.site.userTopId + "?first=1"));
		} else {
			return;
		}
	}

	private void CreateList() {
		int iYear = DateTime.Today.Year - 18;

		for (int i = 0;i < 85;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear -= 1;
		}
		lstYear.SelectedIndex = 7;
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}

	virtual protected void CreateListUserManItem(SelectionList pListUserManItem,int pAttrIndex) {
		if (!sessionMan.userMan.attrList[pAttrIndex].attrTypeNm.Equals("�n��")) {
			if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_LIST) {
				pListUserManItem.Items.Add(new MobileListItem("���I��",string.Empty));
			}
			using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
				DataSet ds = oAttrTypeValue.GetList(sessionMan.site.siteCd,sessionMan.userMan.attrList[pAttrIndex].attrTypeSeq);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					pListUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
				}
			}
			if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_RADIO) {
				pListUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
				pListUserManItem.SelectedIndex = 0;
			}
		} else {
			using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
				System.Data.DataSet ds = oAttrTypeValue.GetList(sessionMan.site.siteCd,sessionMan.userMan.attrList[pAttrIndex].attrTypeSeq);
				foreach (System.Data.DataRow dr in ds.Tables[0].Rows) {
					pListUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
				}
			}
			if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_LIST) {
				int iIndex = 13;
				pListUserManItem.Items.Insert(iIndex,new MobileListItem("���I��",string.Empty));
				pListUserManItem.SelectedIndex = iIndex;
			} else if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_RADIO) {
				pListUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
				pListUserManItem.SelectedIndex = 0;
			}
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
