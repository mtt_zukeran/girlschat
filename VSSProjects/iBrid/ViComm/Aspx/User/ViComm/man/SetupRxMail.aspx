<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetupRxMail.aspx.cs" Inherits="ViComm_man_SetupRxMail" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<cc1:iBMobileLabel ID="lblCastMailRxType" runat="server">女性からのﾒｰﾙを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstCastMailRxType" Runat="server"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblInfoMailRxType" runat="server">お知らせﾒｰﾙを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstInfoMailRxType" Runat="server"></mobile:SelectionList>
		<mobile:Panel ID="pnlTalkEndMailRxFlag" Runat="server">
			<cc1:iBMobileLabel ID="lblTalkEndMailRxFlag" Runat="server">通話切断理由ﾒｰﾙを</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstTalkEndMailRxFlag" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlBlogMailRxFlag" Runat="server">
            <cc1:iBMobileLabel ID="lblBlogMailRxFlag" runat="server">ブログ更新ﾒｰﾙを</cc1:iBMobileLabel>
            <mobile:SelectionList ID="lstBlogMailRxFlag" Runat="server">
            </mobile:SelectionList>
        </mobile:Panel>
		$PGM_HTML02;
		<mobile:SelectionList ID="chkLoginMailRxFlag1" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML03;" Value="1" />
		</mobile:SelectionList>		
		<mobile:SelectionList ID="chkLoginMailRxFlag2" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML04;" Value="1" />
		</mobile:SelectionList>
		<mobile:SelectionList ID="chkWaitMailRxFlag" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML06;" Value="1" />
		</mobile:SelectionList>
		$PGM_HTML05;
		<mobile:SelectionList ID="lstMobileMailRxStartTime" Runat="server" BreakAfter="false"></mobile:SelectionList>〜
		<mobile:SelectionList ID="lstMobileMailRxEndTime" Runat="server"></mobile:SelectionList>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
