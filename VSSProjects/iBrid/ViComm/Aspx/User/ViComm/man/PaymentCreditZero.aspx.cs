/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ڼޯĎx��(ZERO�ڼޯ�)
--	Progaram ID		: PaymentCreditZero
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCreditZero:MobileManPageBase {
	private string MinAndroidVersion = "4.1";
	private string MinIOsVersion = "4.3";

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (IsAvailableService(ViCommConst.RELEASE_PAYMENT_CREDIT_VOICE,2)) {
			this.CheckSmartPhoneVersion();
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.userMan.billAmt == 0) {
				string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);
				DataSet ds;
				ds = GetPackDataSet(ViCommConst.SETTLE_CREDIT_PACK,0);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstCreditPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
					if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
						lstCreditPack.SelectedIndex = lstCreditPack.Items.Count - 1;
						lstCreditPack.Visible = false;
						sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
						sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
						sessionMan.userMan.settleServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
						break;
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSid = "";
		string sSalesAmt;

		if (sessionMan.userMan.billAmt == 0) {
			sSalesAmt = lstCreditPack.Items[lstCreditPack.SelectedIndex].Value;
			using (Pack oPack = new Pack())
			using (SettleLog oLog = new SettleLog()) {
				if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,sSalesAmt,sessionMan.userMan.userSeq)) {
					oLog.LogSettleRequest(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.SETTLE_CORP_ZERO,
						ViCommConst.SETTLE_CREDIT_PACK,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						int.Parse(sSalesAmt),
						oPack.exPoint,
						"",
						"",
						out sSid);
				}
			}
		} else {
			sSalesAmt = sessionMan.userMan.billAmt.ToString();
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_ZERO,
					ViCommConst.SETTLE_CREDIT,
					ViCommConst.SETTLE_STAT_CHARGE,
					sessionMan.userMan.billAmt,
					(int)Math.Floor((double)(sessionMan.userMan.billAmt / sessionMan.site.pointPrice / sessionMan.site.pointTax)),
					"",
					"",
					out sSid);
			}
		}

		string sSettleUrl = "";
		string sBackUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			string sSettleType = sessionMan.userMan.billAmt == 0 ? ViCommConst.SETTLE_CREDIT_PACK : ViCommConst.SETTLE_CREDIT;

			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,sSettleType)) {
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sessionMan.userMan.tel,sBackUrl);
			}
		}
		Response.Redirect(sSettleUrl);
	}
	
	private void CheckSmartPhoneVersion() {
		bool bOK = true;
		string sSelfVersion;
		if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
			sSelfVersion = this.GetAndroidVersion();
			bOK = this.CompareSmartPhoneVersion(sSelfVersion,MinAndroidVersion);
		} else if (sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
			sSelfVersion = this.GetIOsVersion();
			bOK = this.CompareSmartPhoneVersion(sSelfVersion,MinIOsVersion);
		}
		
		if (!bOK) {
			UrlBuilder oUrlBuilder = new UrlBuilder("PaymentCreditZeroVoice.aspx");
			oUrlBuilder.Parameters.Add("packamt",iBridUtil.GetStringValue(this.Request.QueryString["packamt"]));
			RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
		}
	}
	
	private string GetAndroidVersion() {
		string sValue = string.Empty;
		string sUserAgent = sessionMan.requestQuery.UserAgent;
		Match oMatch = Regex.Match(sUserAgent,@"Android\s(?<ver>[\d.]*)");
		sValue = oMatch.Groups["ver"].Value;
		
		return sValue;
	}
	
	private string GetIOsVersion() {
		string sValue = string.Empty;

		string sUserAgent = sessionMan.requestQuery.UserAgent;
		Match oMatch = Regex.Match(sUserAgent,@"OS\s(?<ver>[\d_]*)");
		sValue = oMatch.Groups["ver"].Value;

		return sValue;
	}
	
	private bool CompareSmartPhoneVersion(string pSelfVersion,string pMinVersion) {
		bool bOK = true;
		
		if (string.IsNullOrEmpty(pMinVersion) || string.IsNullOrEmpty(pSelfVersion)) {
			return true;
		}
		
		char[] cSep = new char[]{'.','_'};

		string[] sMinVersion = pMinVersion.Split(cSep);
		string[] sSelfVersion = pSelfVersion.Split(cSep);
		
		int iMinMajorVer = 0;
		int iMinMinorVer = 0;
		int iSelfMajorVer = 0;
		int iSelfMinorVer = 0;
		
		int.TryParse(sMinVersion[0],out iMinMajorVer);
		if (sMinVersion.Length > 1) {
			int.TryParse(sMinVersion[1],out iMinMinorVer);
		}
		
		int.TryParse(sSelfVersion[0],out iSelfMajorVer);
		
		if (sSelfVersion.Length > 1) {
			int.TryParse(sSelfVersion[1],out iSelfMinorVer);
		}
		
		if (iMinMajorVer > iSelfMajorVer) {
			bOK = false;
		} else if (iMinMajorVer == iSelfMajorVer && iMinMinorVer > iSelfMinorVer) {
			bOK = false;
		}
		
		return bOK;
	}
}
