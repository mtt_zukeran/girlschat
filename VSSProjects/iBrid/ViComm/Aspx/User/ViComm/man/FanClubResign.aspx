<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FanClubResign.aspx.cs" Inherits="ViComm_man_FanClubResign" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="cc1" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagTop" runat="server" Text="$PGM_HTML02;" />
		<cc1:iBMobileLiteralText ID="tagConfirm" runat="server" Text="$PGM_HTML03;" />
		<cc1:iBMobileLiteralText ID="tagComplete" runat="server" Text="$PGM_HTML04;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
