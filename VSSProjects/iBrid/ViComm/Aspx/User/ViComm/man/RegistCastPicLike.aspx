<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistCastPicLike.aspx.cs" Inherits="ViComm_man_RegistCastPicLike" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlComplete" Runat="server" Visible="false">
			$PGM_HTML02;
		</mobile:Panel>
		<mobile:Panel ID="pnlError" Runat="server" Visible="false">
			$PGM_HTML03;
		</mobile:Panel>
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
