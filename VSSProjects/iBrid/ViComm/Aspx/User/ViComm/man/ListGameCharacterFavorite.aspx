<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListGameCharacterFavorite.aspx.cs" Inherits="ViComm_man_ListGameCharacterFavorite" %>
<%@ Register TagPrefix="mobile"		Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid"		Namespace="MobileLib"						Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		$DATASET_LOOP_START0;
			<ibrid:iBMobileLiteralText ID="tagList"		runat="server" Text="$PGM_HTML07;" />
			<ibrid:iBMobileLiteralText ID="tagDetail"	runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;
		<ibrid:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		<ibrid:iBMobileLiteralText ID="tagPaging"	runat="server" Text="$PGM_HTML05;" />
		<mobile:Panel ID="pnlOnline" Runat="server">
			<ibrid:iBMobileLabel ID="lblOnline" runat="server" BreakAfter="true">
			</ibrid:iBMobileLabel>
			<mobile:SelectionList ID="rdoStatus" Runat="server" SelectType="Radio" Alignment="Left">
				<Item Text="�S�Ă̏���" Value="0" Selected="true" />
				<Item Text="۸޲ݒ��̎q�̂�" Value="1" Selected="false" />
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesOnline" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlTreasure" Runat="server">
			<mobile:SelectionList ID="chkTreasurePic" Runat="server" SelectType="CheckBox" Alignment="Left">
				<Item Text="����ʐ^�؂̂�" Value="1" Selected="false" />
			</mobile:SelectionList>
			<mobile:SelectionList ID="chkTreasureMovie" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="true">
				<Item Text="���蓮��؂̂�" Value="1" Selected="false" />
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesTreasure" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlAge" Runat="server">
			<mobile:SelectionList ID="lstAge" Runat="server" SelectType="DropDown">
				<Item Value=":" Text="--$xE6F8;�N��--"></Item>
				<Item Value="18:20" Text="18�`20��"></Item>
				<Item Value="21:24" Text="21�`24��"></Item>
				<Item Value="25:29" Text="25�`29��"></Item>
				<Item Value="30:34" Text="30�`34��"></Item>
				<Item Value="35:40" Text="35�`40��"></Item>
				<Item Value="40:" Text="40�Έȏ�"></Item>
			</mobile:SelectionList>
			<ibrid:iBMobileLiteralText ID="tagLinesAge" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		
		<mobile:Panel ID="pnlName" Runat="server">
			<ibrid:iBMobileLiteralText ID="tagEmojiHandleNm" runat="server" Text="<font color=orange>$xE6FA;</font>" />
			<ibrid:iBMobileLabel ID="lblHandleNm" runat="server" BreakAfter="true">���O��</ibrid:iBMobileLabel>
			<ibrid:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12" BreakAfter="False"></ibrid:iBMobileTextBox>���܂�<br />
			<ibrid:iBMobileLiteralText ID="tagLinesHandelNm" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>