<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ListGallery.aspx.cs" Inherits="ViComm_man_ListGallery" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlSearchList" Runat="server">
			<mobile:SelectionList ID="lstGenreSelect" Runat="server" SelectType="DropDown" BreakAfter="False" Visible="false"></mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML04;
		$DATASET_LOOP_START14;
			<cc1:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END14;
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
