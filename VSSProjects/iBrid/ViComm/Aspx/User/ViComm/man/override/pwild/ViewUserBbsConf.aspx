<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ViewUserBbsConf.aspx.cs" Inherits="ViComm_man_ViewUserBbsConf" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Import Namespace="ViComm" %>

<script runat="server">
    protected override void cmdSubmit_Click(object sender,EventArgs e) {
		using (UserManBbs oUserManBbs = new UserManBbs()) {
			oUserManBbs.DeleteBbs(sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sessionMan.GetManBbsDocValue("BBS_SEQ")
			);
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETE_USER_BBS_COMPLETE) + "&site=" + sessionMan.site.siteCd);
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML07;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>