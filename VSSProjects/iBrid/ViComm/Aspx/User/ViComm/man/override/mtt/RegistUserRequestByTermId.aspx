<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../RegistUserRequestByTermId.aspx.cs" Inherits="ViComm_man_RegistUserRequestByTermId" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Register Assembly="ViCommLib" Namespace="ViComm" TagPrefix="ViCommConst"%>

<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		base.Page_Load(sender,e);

		if (!IsPostBack) {
			if (sessionMan.site.registBirthdayInputFlag == 1) {
				lstYear.Items.Clear();
				int iYear = DateTime.Today.Year - 6;

				for (int i = 0;i < 86;i++) {
					lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
					iYear -= 1;
				}
				lstYear.SelectedIndex = 19;
			}
		}
	}

    protected override void CreateListUserManItem(SelectionList pListUserManItem, int pAttrIndex) {
        if (!sessionMan.userMan.attrList[pAttrIndex].attrTypeNm.Equals("地域")) {
            base.CreateListUserManItem(pListUserManItem, pAttrIndex);
            return;
        }
        
        using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
            System.Data.DataSet ds = oAttrTypeValue.GetList(sessionMan.site.siteCd, sessionMan.userMan.attrList[pAttrIndex].attrTypeSeq);
            foreach (System.Data.DataRow dr in ds.Tables[0].Rows) {
                pListUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(), dr["MAN_ATTR_SEQ"].ToString()));
            }
        }
        if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_LIST) {
            int iIndex = 13;
            pListUserManItem.Items.Insert(iIndex, new MobileListItem("未選択", string.Empty));
            pListUserManItem.SelectedIndex = iIndex;
        } else if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViCommConst.INPUT_TYPE_RADIO) {
            pListUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
            pListUserManItem.SelectedIndex = 0;
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		<mobile:Panel ID="pnlPortal" Runat="server">
			<cc1:iBMobileLiteralText ID="tagPassWord" runat="server" Text="$PGM_HTML03;" />
			<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="12" Numeric="true" Visible="false"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagDivS01" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
			<cc1:iBMobileLiteralText ID="tagDivS02" runat="server" BreakAfter="false" Text="</div>" />
			<cc1:iBMobileLabel ID="lblTel" runat="server" Visible="false" BreakAfter="true">$PGM_HTML04;電話番号</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true" Visible="false" BreakAfter="true"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagTel" runat="server" Text="<br/>" Visible="false" />
			<cc1:iBMobileLiteralText ID="tagDivS03" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
			<cc1:iBMobileLiteralText ID="tagDivS04" runat="server" BreakAfter="false" Text="</div>" />
			<cc1:iBMobileLabel ID="lblHandleNm" runat="server" BreakAfter="true">$PGM_HTML04;ﾊﾝﾄﾞﾙ名</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtHandleNm" runat="server" MaxLength="20" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagHandleNm" runat="server" Text="<br/>" />
			<cc1:iBMobileLiteralText ID="tagDivS05" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
			<cc1:iBMobileLiteralText ID="tagDivS06" runat="server" BreakAfter="false" Text="</div>" />
			<cc1:iBMobileLabel ID="lblBirthday" runat="server" BreakAfter="true">$PGM_HTML04;生年月日</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">年</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">月</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblDay" runat="server">日</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagBirthday" runat="server" Text="<br/>" Visible="false" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem1" Runat="server">
			$PGM_HTML04;<cc1:iBMobileLabel ID="lblUserManItem1" runat="server">userManItem01</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem1" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem1" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem1" Runat="server" BreakAfter="true"></mobile:SelectionList>
			
		    <cc1:iBMobileLiteralText ID="tagDivS07" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
			<br />
    		<cc1:iBMobileLiteralText ID="tagDivS08" runat="server" BreakAfter="false" Text="</div>" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem2" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem2" runat="server">userManItem02</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem2" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem2" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem2" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem3" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem3" runat="server">userManItem03</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem3" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem3" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem3" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem4" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem4" runat="server">userManItem04</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem4" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem4" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem4" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem5" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem5" runat="server">userManItem05</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem5" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem5" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem5" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem6" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem6" runat="server">userManItem06</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem6" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem6" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem6" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem7" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem7" runat="server">userManItem07</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem7" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem7" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem7" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem8" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem8" runat="server">userManItem08</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem8" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem8" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem8" Runat="server"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem9" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem9" runat="server">userManItem09</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem9" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem9" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem9" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem10" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem10" runat="server">userManItem10</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem10" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem10" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem10" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem11" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem11" runat="server">userManItem11</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem11" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem11" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem11" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem12" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem12" runat="server">userManItem12</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem12" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem12" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem12" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem13" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem13" runat="server">userManItem13</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem13" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem13" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem13" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem14" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem14" runat="server">userManItem14</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem14" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem14" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem14" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem15" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem15" runat="server">userManItem15</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem15" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem15" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem15" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem16" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem16" runat="server">userManItem16</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem16" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem16" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem16" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem17" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem17" runat="server">userManItem17</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem17" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem17" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem17" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem18" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem18" runat="server">userManItem18</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem18" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem18" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem18" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem19" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem19" runat="server">userManItem19</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem19" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem19" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem19" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem20" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem20" runat="server">userManItem20</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem20" runat="server" MaxLength="300" Size="12" BreakAfter="true"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem20" runat="server" Row="10" BreakBefore="false" BreakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem20" Runat="server" BreakAfter="true"></mobile:SelectionList>
			<br />
		</mobile:Panel>
		<cc1:iBMobileLiteralText ID="tagDivS09" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		<br />
		<cc1:iBMobileLiteralText ID="tagDivS10" runat="server" BreakAfter="false" Text="</div>" />
		$PGM_HTML02;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>