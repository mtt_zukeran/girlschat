<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../LoginUser.aspx.cs" Inherits="ViComm_man_LoginUser" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>

		<cc1:iBMobileLiteralText ID="tagStartTableTag" runat="server" Text="<table><tr><td><font size=2>"/>
		$PGM_HTML05;
		<br />
		<cc1:iBMobileTextBox ID="txtLoginId" runat="server" MaxLength="11" Size="16" Numeric="true" DetailType="true"></cc1:iBMobileTextBox>
		$PGM_HTML06;
		<br />
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="10" Numeric="true" Password="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagEndTdTag" runat="server" Text="</font></td><td><font size=2>" />
		$PGM_HTML03;
		<cc1:iBMobileLiteralText ID="tagEndTableTag" runat="server" Text="</font></td></tr></table>"/>
		$PGM_HTML04;
	</mobile:Form>
</body>
</html>
