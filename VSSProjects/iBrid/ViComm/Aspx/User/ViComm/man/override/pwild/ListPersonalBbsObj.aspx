<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ListPersonalBbsObj.aspx.cs" Inherits="ViComm_man_ListPersonalBbsObj" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		<mobile:Panel ID="pnlSearchList" Runat="server">
			<mobile:SelectionList ID="lstGenreSelect" Runat="server" SelectType="DropDown" BreakAfter="false"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagDivS03" runat="server" BreakAfter="false" Text="<div id=$DIV_IS_FEATURE_PHONE;>" />
		        <br />
			<cc1:iBMobileLiteralText ID="tagDivS04" runat="server" BreakAfter="false" Text="</div>" />
		</mobile:Panel>
		$PGM_HTML04;
		$DATASET_LOOP_START25;
			<cc1:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END25;
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
