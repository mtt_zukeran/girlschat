<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ModifyFavoritGroup.aspx.cs" Inherits="ViComm_man_ModifyFavoritGroup" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="ibrid" Namespace="MobileLib" Assembly="MobileLib"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
	    <ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
        $PGM_HTML02;
		<ibrid:iBMobileTextBox ID="txtFavoritGroupNm" runat="server" MaxLength="40" Size="30" EmojiPaletteEnabled="false"></ibrid:iBMobileTextBox>
 		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
