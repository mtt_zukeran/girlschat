<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ModifyUserBasicInfo.aspx.cs" Inherits="ViComm_man_ModifyUserBasicInfo" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>

<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		base.Page_Load(sender,e);

		string sFlag;
		using (ManageCompany oCompany = new ManageCompany()) {
			oCompany.GetValue("AUTO_GET_TEL_INFO_FLAG",out sFlag);
		}

		if (!IsAvailableService(ViComm.ViCommConst.RELEASE_SP_TEL_NOT_ABSOLUTE,2)) {
			if (sessionMan.carrier.Equals(ViComm.ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViComm.ViCommConst.IPHONE)) {
				sFlag = "0";
			}
		}

		if (sFlag.Equals(ViComm.ViCommConst.FLAG_ON_STR)) {
			pnlTel.Visible = false;
		} else {
			pnlTel.Visible = true;
		}
	}

    protected override void CreateListUserManItem(SelectionList pListUserManItem, int pAttrIndex) {
        if (!sessionMan.userMan.attrList[pAttrIndex].attrTypeNm.Equals("地域")) {
            base.CreateListUserManItem(pListUserManItem, pAttrIndex);
            return;
        }
        
        using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
            System.Data.DataSet ds = oAttrTypeValue.GetList(sessionMan.site.siteCd, sessionMan.userMan.attrList[pAttrIndex].attrTypeSeq);
            foreach (System.Data.DataRow dr in ds.Tables[0].Rows) {
                pListUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(), dr["MAN_ATTR_SEQ"].ToString()));
            }
        }
        if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViComm.ViCommConst.INPUT_TYPE_LIST) {
            int iIndex = 13;
            pListUserManItem.Items.Insert(iIndex, new MobileListItem("未選択", string.Empty));
            pListUserManItem.SelectedIndex = iIndex;
        } else if (sessionMan.userMan.attrList[pAttrIndex].inputType == ViComm.ViCommConst.INPUT_TYPE_RADIO) {
            pListUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
            pListUserManItem.SelectedIndex = 0;
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tag01s" runat="server" Text="<font size=1>" />
		<cc1:iBMobileLabel ID="lblHandelNm" runat="server">ﾊﾝﾄﾞﾙ名</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines01" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLabel ID="lblPassword" runat="server" Visible="false">ﾊﾟｽﾜｰﾄﾞ</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="10" Numeric="true" Visible="false"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines02" runat="server" Text="$PGM_HTML04;" />
		<mobile:Panel ID="pnlTel" Runat="server">
			<cc1:iBMobileLabel ID="lblTel" runat="server" Visible="true">電話番号</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines03" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<cc1:iBMobileLabel ID="lblBirthDay" runat="server" Visible="true">$xE686;生年月日</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False">年</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False">月</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
			<cc1:iBMobileLabel ID="lblDay" runat="server">日</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagLines04" runat="server" Text="$PGM_HTML04;" />
		<br />
		<mobile:Panel ID="pnlUserManItem1" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem1" runat="server" Visible="false">userManItem01</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem1" runat="server" Text="$xE664;地域<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem1" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem1" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem1" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines05" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem2" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem2" runat="server" Visible="false">userManItem02</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem2" runat="server" Text="$xE666;血液型<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem2" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem2" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem2" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines06" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem3" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem3" runat="server" Visible="false">userManItem03</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem3" runat="server" Text="$xE6F8;お仕事(業種)は<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem3" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem3" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem3" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines07" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem4" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem4" runat="server" Visible="false">userManItem04</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem4" runat="server" Text="$xE6F8;年収は<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem4" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem4" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem4" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines08" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem5" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem5" runat="server" Visible="false">userManItem05</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem5" runat="server" Text="$xE746;趣味は<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem5" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem5" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem5" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines09" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem6" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem6" runat="server" Visible="false">userManItem06</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem6" runat="server" Text="$xE743;好みの女性の年齢は<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem6" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem6" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem6" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem7" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem7" runat="server" Visible="false">userManItem07</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem7" runat="server" Text="$xE750;好きな女性のｽﾀｲﾙは<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem7" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem7" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem7" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem8" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem8" runat="server" Visible="false">userManItem08</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem8" runat="server" Text="$xE6EC;好きな女性のﾀｲﾌﾟは<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem8" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem8" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem8" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem9" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem9" runat="server" Visible="false">userManItem09</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem9" runat="server" Text="$xE734;女性としたいこと<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem9" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem9" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem9" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem10" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem10" runat="server" Visible="false">userManItem10</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem10" runat="server" Text="$xE734;彼女いない暦<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem10" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem10" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem10" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem11" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem11" runat="server" Visible="false">userManItem11</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem11" runat="server" Text="$xE734;sex経験<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem11" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem11" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem11" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem12" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem12" runat="server" Visible="false">userManItem12</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem12" runat="server" Text="$xE6BA;ｻｲﾄをよく利用する時間帯<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem12" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem12" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem12" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem13" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem13" runat="server" Visible="false">userManItem13</cc1:iBMobileLabel>
			<cc1:iBMobileLiteralText ID="tagUserManItem13" runat="server" Text="$xE691;一言ｺﾒﾝﾄ(150文字以内)<BR>$xE737;ｺﾒﾝﾄを記入すればもて度は確実にup！誹謗中傷は書込禁止。<br>" />
			<cc1:iBMobileTextBox ID="txtUserManItem13" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem13" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem13" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem14" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem14" runat="server">userManItem14</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem14" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem14" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem14" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem15" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem15" runat="server">userManItem15</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem15" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem15" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem15" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem16" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem16" runat="server">userManItem16</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem16" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem16" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem16" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem17" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem17" runat="server">userManItem17</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem17" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem17" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem17" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines21" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem18" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem18" runat="server">userManItem18</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem18" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem18" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem18" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines22" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem19" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem19" runat="server">userManItem19</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem19" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem19" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem19" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines23" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem20" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem20" runat="server">userManItem20</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem20" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem20" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem20" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines24" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<cc1:iBMobileLiteralText ID="tag01e" runat="server" Text="</font>" />
		$PGM_HTML02;
		$PGM_HTML03;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
