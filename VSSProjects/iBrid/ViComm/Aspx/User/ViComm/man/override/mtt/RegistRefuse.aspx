<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../RegistRefuse.aspx.cs" Inherits="ViComm_man_RegistRefuse" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		if (IsFanClubMember()) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_REFUSE_NG_FANCLUB_MEMBER);
		} else {
			base.Page_Load(sender,e);
		}
	}
	
	private bool IsFanClubMember() {
		string sLoginId = iBridUtil.GetStringValue(this.Request.QueryString["loginid"]);
		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
		int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
		decimal dRecCount = 0;
		DataRow dr = null;
		if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo,out dr)) {
			string sCastUserSeq = dr["USER_SEQ"].ToString();
			sCastCharNo = dr["USER_CHAR_NO"].ToString();
			FanClubStatusSeekCondition oCondition = new FanClubStatusSeekCondition();
			oCondition.SiteCd = sessionMan.site.siteCd;
			oCondition.ManUserSeq = sessionMan.userMan.userSeq;
			oCondition.CastUserSeq = sCastUserSeq;
			oCondition.CastCharNo = sCastCharNo;
			oCondition.EnableFlag = ViCommConst.FLAG_ON_STR;
			
			using (CastFanClubStatus oCastFanClubStatus = new CastFanClubStatus(sessionObj)) {
				oCastFanClubStatus.GetPageCount(oCondition,1,out dRecCount);
			}
		}

		if (dRecCount > 0) {
			return true;
		} else {
			return false;
		}
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
		$DATASET_LOOP_START0;
			<cc1:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END0;
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
