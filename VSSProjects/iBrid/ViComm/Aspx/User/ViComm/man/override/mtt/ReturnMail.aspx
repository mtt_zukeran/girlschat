﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../ReturnMail.aspx.cs"
    Inherits="ViComm_man_ReturnMail" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>

<script runat="server">
    protected override string GetMailTitle() {
        return string.Empty;
    }

    protected override bool CheckOther() {
        if (System.Text.Encoding.GetEncoding(932).GetByteCount(txtTitle.Text) > 30) {
            this.lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_LENGTH_OVER_TITLE);
            return false;
        }
        return true;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
		<mobile:Panel ID="pnlForm" Runat="server">
			<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblTitle" runat="server" Text="件名：<font color=red>※全角15文字まで</font>"></cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="30" Size="24" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
			<cc1:iBMobileLabel ID="lblDoc" runat="server">本文：</cc1:iBMobileLabel>
			<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true"
				Rows="6" Columns="24" EmojiPaletteEnabled="True"></tx:TextArea>
			$PGM_HTML04;
			$PGM_HTML06;
		</mobile:Panel>
		<mobile:Panel ID="pnlPointLack" Runat="server">
			$PGM_HTML07;
		</mobile:Panel>
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
