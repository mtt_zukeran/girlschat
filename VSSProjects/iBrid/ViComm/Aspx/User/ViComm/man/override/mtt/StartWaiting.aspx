﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../StartWaiting.aspx.cs" Inherits="ViComm_man_StartWaiting" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>

<script runat="server">
    protected override void OnLoad(EventArgs e) {
        base.OnLoad(e);

        if (!this.IsPostBack) {
            MobileListItem[] oItemArray = this.lstConnectType.Items.GetAll();
            this.lstConnectType.Items.Clear();
            foreach (MobileListItem oItem in oItemArray) {
                if (oItem.Value == "3") {
                    this.lstConnectType.Items.Insert(1, oItem);
                } else {
                    this.lstConnectType.Items.Add(oItem);
                }
            }
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:SelectionList ID="lstWaitTime" Runat="server"></mobile:SelectionList>
		$PGM_HTML02;
		<mobile:SelectionList ID="lstConnectType" Runat="server"></mobile:SelectionList>
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		$PGM_HTML03;
		<tx:TextArea ID="txtWaitComment" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		$PGM_HTML04;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
