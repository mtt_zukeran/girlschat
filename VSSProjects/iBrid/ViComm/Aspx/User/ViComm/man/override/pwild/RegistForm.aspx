<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../RegistForm.aspx.cs" Inherits="ViComm_man_RegistForm" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="iBridCommLib" %>

<script runat="server">
	protected override void Page_Load(object sender,EventArgs e) {
		string sTempRegistId = iBridUtil.GetStringValue(Request.QueryString["regid"]);

		if (sessionMan.carrier.Equals(ViComm.ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViComm.ViCommConst.IPHONE) || sessionMan.carrier.Equals(ViComm.ViCommConst.CARRIER_OTHERS)) {
			string sLoginId = string.Empty;
			string sLoginPassword = string.Empty;
			string sSexCd = string.Empty;
			string sUserSeq = string.Empty;

			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["EnableCookie"]).Equals(ViComm.ViCommConst.FLAG_ON_STR)) {
				if (Request.Cookies["maqia"] != null) {
					using (User oUser = new User()) {
						if (oUser.GetOne(iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiauid"]),iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiasex"]))) {
							sLoginId = oUser.loginId;
							sLoginPassword = oUser.loginPassword;
							sSexCd = oUser.sexCd;
							sUserSeq = iBridUtil.GetStringValue(Request.Cookies["maqia"]["maqiauid"]);
						}
					}
				}
			}

			if (!string.IsNullOrEmpty(sLoginId) && !string.IsNullOrEmpty(sLoginPassword)) {
				using (TempRegist oTemp = new TempRegist()) {
					if (oTemp.GetOne(sTempRegistId)) {
						ModifyEmailAddr(oTemp.siteCd,sUserSeq,oTemp.emailAddr);
					}
				}

				if (sSexCd.Equals(ViComm.ViCommConst.MAN)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("LoginUser.aspx?loginid={0}&password={1}",sLoginId,sLoginPassword)));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + "/ViComm/woman",Session.SessionID,string.Format("LoginUser.aspx?loginid={0}&password={1}",sLoginId,sLoginPassword)));
				}
			}
		} else {
			string sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
			string sIModeId = Mobile.GetiModeId(sessionMan.carrier,Request);
			string sUserSeq;
			string sSexCd;

			if (IsRegistSite(sUtn,sIModeId,out sUserSeq,out sSexCd)) {
				string sLoginId = string.Empty;
				string sLoginPassword = string.Empty;
				string sUserStatus = string.Empty;

				using (User oUser = new User()) {
					if (oUser.GetOne(sUserSeq,sSexCd)) {
						sLoginId = oUser.loginId;
						sLoginPassword = oUser.loginPassword;
						sUserStatus = oUser.userStatus;
					}
				}

				using (TempRegist oTemp = new TempRegist()) {
					if (oTemp.GetOne(sTempRegistId)) {
						ModifyEmailAddr(oTemp.siteCd,sUserSeq,oTemp.emailAddr);
					}
				}

				if (!string.IsNullOrEmpty(sLoginId) && !string.IsNullOrEmpty(sLoginPassword)) {
					if (sSexCd.Equals(ViComm.ViCommConst.MAN)) {
						if (sUserStatus.Equals(ViComm.ViCommConst.USER_MAN_RESIGNED)) {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("LoginUser.aspx?loginid={0}&password={1}&bookmark=1",sLoginId,sLoginPassword)));
						} else {
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("LoginUser.aspx?loginid={0}&password={1}",sLoginId,sLoginPassword)));
						}
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + "/ViComm/woman",Session.SessionID,string.Format("LoginUser.aspx?loginid={0}&password={1}",sLoginId,sLoginPassword)));
					}
				}
			}
		}

		txtYear.Visible = false;
		txtMonth.Visible = false;
		txtDay.Visible = false;
		lblPassword.Visible = false;
		txtPassword.Visible = false;
		tagLines02.Visible = false;

		if (txtPassword.Visible == false) {
			txtPassword.Text = RandomHelper.GeneratePassword(8);
		}

		base.Page_Load(sender,e);

		txtYear.Visible = false;

		if (txtTel.Visible == false) {
			tagLines03.Visible = false;
		}

		if (!IsPostBack) {
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i + 1)) as SelectionList;
				iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i + 1)) as iBMobileTextBox;
				iBMobileLabel lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",i + 1)) as iBMobileLabel;
				TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",i + 1)) as TextArea;

				if (sessionMan.userMan.attrList[i].attrTypeNm == "地域") {
					int iIndex = 13;
					lstUserManItem.Items.Insert(iIndex,new MobileListItem("未選択",string.Empty));
					lstUserManItem.SelectedIndex = iIndex;
				} else {
					lblUserManItem.Visible = false;

					if (sessionMan.userMan.attrList[i].inputType == ViComm.ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							txtAreaUserManItem.Visible = false;
							txtUserManItem.Visible = false;
						} else {
							txtAreaUserManItem.Visible = false;
							txtUserManItem.Visible = false;
						}
						lstUserManItem.Visible = false;
					} else {
						txtAreaUserManItem.Visible = false;
						txtUserManItem.Visible = false;
						lstUserManItem.Visible = false;
					}
				}
			}
		}
	}

	protected override void CreateList() {
		int iYear = DateTime.Today.Year - 18;

		for (int i = 0;i < 85;i++) {
			lstYear.Items.Add(new MobileListItem(iYear.ToString("d4"),iYear.ToString("d4")));
			iYear -= 1;
		}
		lstYear.SelectedIndex = 7;
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new MobileListItem(i.ToString("d2"),i.ToString("d2")));
		}
	}

	protected override bool CheckList() {
		bool bOk = true;

		if ((lstYear.Selection.Value.Equals("")) || (lstMonth.Selection.Value.Equals("")) || (lstDay.Selection.Value.Equals(""))) {
			lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_BIRTHDAY);
			bOk = false;
		} else if (lstYear.Selection.Value.Length != 4) {
			lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_BIRTHDAY_YYYY_NG);
			bOk = false;
		} else {
			if (bOk) {
				string sDate = lstYear.Selection.Value + "/" + lstMonth.Selection.Value + "/" + lstDay.Selection.Value;
				int iAge = ViComm.ViCommPrograms.Age(sDate);
				int iOkAge;
				int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
				if (iOkAge == 0) {
					iOkAge = 18;
				}

				if (iAge > 99 || iAge == 0) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_BIRTHDAY_NG);
				} else if (iAge < iOkAge) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_UNDER_18);
				}
			}
		}

		if (bOk) {
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				if (sessionMan.userMan.attrList[i].attrTypeNm == "地域") {
					SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i + 1)) as SelectionList;
					if (lstUserManItem.Selection != null) {
						if (sessionMan.userMan.attrList[i].profileReqItemFlag && lstUserManItem.Selection.Value.Equals(string.Empty)) {
							lblErrorMessage.Text += string.Format(GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_SELECT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
							bOk = false;
						}
					}
				}
			}
		}

		if (!sessionMan.carrier.Equals(ViComm.ViCommConst.ANDROID) && !sessionMan.carrier.Equals(ViComm.ViCommConst.IPHONE)) {
			string sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
			string sIModeId = Mobile.GetiModeId(sessionMan.carrier,Request);

			if (sessionMan.getTermIdFlag) {
				if (sUtn.Equals(string.Empty) || sIModeId.Equals(string.Empty)) {
					lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			} else {
				if (sIModeId.Equals(string.Empty)) {
					lblErrorMessage.Text += GetErrorMessage(ViComm.ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			}
		}

		if (lblErrorMessage.Text.Equals(string.Empty) && bOk == true) {
			txtYear.Visible = true;
		}

		return bOk;
	}

	private bool IsRegistSite(string pUtn,string pImodeId,out string pUserSeq,out string pSexCd) {
		pUserSeq = string.Empty;
		pSexCd = string.Empty;

		using (User oUser = new User()) {
			if (!pUtn.Equals(string.Empty)) {
				if (oUser.RegistSiteByUtn(string.Empty,pUtn,ViComm.ViCommConst.MAN,out pUserSeq)) {
					pSexCd = ViComm.ViCommConst.MAN;
					return true;
				}

				if (oUser.RegistSiteByUtn(string.Empty,pUtn,ViComm.ViCommConst.OPERATOR,out pUserSeq)) {
					pSexCd = ViComm.ViCommConst.OPERATOR;
					return true;
				}
			}
			if (!pImodeId.Equals(string.Empty)) {
				if (oUser.RegistSiteByImodeId(string.Empty,pImodeId,ViComm.ViCommConst.MAN,out pUserSeq)) {
					pSexCd = ViComm.ViCommConst.MAN;
					return true;
				}

				if (oUser.RegistSiteByImodeId(string.Empty,pImodeId,ViComm.ViCommConst.OPERATOR,out pUserSeq)) {
					pSexCd = ViComm.ViCommConst.OPERATOR;
					return true;
				}
			}
		}

		return false;
	}

	private void ModifyEmailAddr(string pSiteCd,string pUserSeq,string pEmailAddr) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_EMAIL_ADDR");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,pEmailAddr);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	protected override string GetBirthDay() {
		DateTime dtBiathday = new DateTime(int.Parse(lstYear.Selection.Value),int.Parse(lstMonth.Selection.Value),int.Parse(lstDay.Selection.Value));
		return dtBiathday.ToString("yyyy/MM/dd");
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tag01s" runat="server" Text="<font size=1>" />
		<cc1:iBMobileLabel ID="lblHandelNm" runat="server">ﾊﾝﾄﾞﾙ名</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines01" runat="server" Text="$PGM_HTML04;" />

		<cc1:iBMobileLabel ID="lblPassword" runat="server" Visible="true">ﾊﾟｽﾜｰﾄﾞ</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="10" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines02" runat="server" Text="$PGM_HTML04;" />

		<cc1:iBMobileLabel ID="lblTel" runat="server" Visible="true">電話番号</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines03" runat="server" Text="$PGM_HTML04;" />

		<cc1:iBMobileLabel ID="lblBirthDay" runat="server" Visible="true">生年月日</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtYear" runat="server" MaxLength="4" BreakAfter="false" Size="4" Numeric="true" Visible="true"> </cc1:iBMobileTextBox>
		<mobile:SelectionList ID="lstYear" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False" Visible="true">年</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtMonth" runat="server" MaxLength="2" BreakAfter="false" Size="2" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<mobile:SelectionList ID="lstMonth" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False" Visible="true">月</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtDay" runat="server" MaxLength="2" BreakAfter="false" Size="2" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<mobile:SelectionList ID="lstDay" Runat="server" BreakAfter="False"></mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblDay" runat="server" Visible="true">日</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagLines04" runat="server" Text="$PGM_HTML04;" />
		<mobile:Panel ID="pnlUserManItem1" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem1" runat="server">userManItem01</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem1" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem1" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem1" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines05" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem2" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem2" runat="server">userManItem02</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem2" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem2" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem2" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines06" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem3" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem3" runat="server">userManItem03</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem3" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem3" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem3" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines07" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem4" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem4" runat="server">userManItem04</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem4" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem4" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem4" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines08" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem5" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem5" runat="server">userManItem05</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem5" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem5" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem5" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines09" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem6" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem6" runat="server">userManItem06</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem6" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem6" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem6" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem7" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem7" runat="server">userManItem07</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem7" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem7" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem7" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem8" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem8" runat="server">userManItem08</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem8" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem8" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem8" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem9" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem9" runat="server">userManItem09</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem9" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem9" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem9" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem10" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem10" runat="server">userManItem10</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem10" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem10" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem10" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem11" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem11" runat="server">userManItem11</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem11" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem11" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem11" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem12" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem12" runat="server">userManItem12</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem12" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem12" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem12" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem13" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem13" runat="server">userManItem13</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem13" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem13" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem13" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem14" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem14" runat="server">userManItem14</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem14" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem14" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem14" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem15" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem15" runat="server">userManItem15</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem15" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem15" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem15" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem16" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem16" runat="server">userManItem16</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem16" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem16" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem16" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem17" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem17" runat="server">userManItem17</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem17" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem17" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem17" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines21" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem18" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem18" runat="server">userManItem18</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem18" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem18" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem18" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines22" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem19" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem19" runat="server">userManItem19</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem19" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem19" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem19" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines23" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem20" Runat="server" Visible="false">
			<cc1:iBMobileLabel ID="lblUserManItem20" runat="server">userManItem20</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem20" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem20" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem20" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines24" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<cc1:iBMobileLiteralText ID="tag01e" runat="server" Text="</font>" />
		$PGM_HTML02;
		<mobile:SelectionList ID="chkRlue" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false">
			<Item Text="$PGM_HTML05;" Value="1" Selected="True" />
		</mobile:SelectionList>		
		<mobile:SelectionList ID="chkMail" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false">
			<Item Text="$PGM_HTML06;" Value="1" Selected="True">
			</Item>
		</mobile:SelectionList>
		$PGM_HTML07;
		$PGM_HTML03;
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
