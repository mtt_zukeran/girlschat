<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../SetupRxMail.aspx.cs" Inherits="ViComm_man_SetupRxMail" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="../../TextArea.ascx" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>

<script runat="server">
	protected override void OnLoad(EventArgs e) {
		base.OnLoad(e);
		if (!this.IsPostBack) {
			if (sessionMan.userMan.tweetCommentMailRxFlag == ViCommConst.FLAG_ON) {
				chkTweetCommentMailRxFlag.SelectedIndex = 0;
			}
			if (sessionMan.userMan.mobileMailRxTimeUseFlag == ViCommConst.FLAG_ON) {
				chkMobileMailRxTimeUseFlag.SelectedIndex = 0;
			}
			if (sessionMan.userMan.mobileMailRxTimeUseFlag2 == ViCommConst.FLAG_ON) {
				chkMobileMailRxTimeUseFlag2.SelectedIndex = 0;
			}
			
			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds;
				int iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TIME_PART);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstMobileMailRxStartTime2.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));
					lstMobileMailRxEndTime2.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));

					if (sessionMan.userMan.mobileMailRxStartTime2.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxStartTime2.SelectedIndex = iIdx;
					}
					if (sessionMan.userMan.mobileMailRxEndTime2.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxEndTime2.SelectedIndex = iIdx;
					}
					iIdx++;
				}
			}
		}
	}

	protected override void cmdSubmit_Click(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		int iTalkEndMailRxFlag = 0;
		int.TryParse(lstTalkEndMailRxFlag.Items[lstTalkEndMailRxFlag.SelectedIndex].Value,out iTalkEndMailRxFlag);
		int iMobileMailRxTimeUseFlag;
		int iMobileMailRxTimeUseFlag2;
		string sMobileMailRxStartTime2;
		string sMobileMailRxEndTime2;
		
		string sMobileMailRxStartTime = lstMobileMailRxStartTime.Items[lstMobileMailRxStartTime.SelectedIndex].Value;
		string sMobileMailRxEndTime = lstMobileMailRxEndTime.Items[lstMobileMailRxEndTime.SelectedIndex].Value;

		if (!iBridUtil.GetStringValue(this.Request.Form["double"]).Equals(ViCommConst.FLAG_ON_STR)) {
			iMobileMailRxTimeUseFlag = ViCommConst.FLAG_ON;
			iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_OFF;
			sMobileMailRxStartTime2 = "00:00";
			sMobileMailRxEndTime2 = "00:00";
		} else {
			iMobileMailRxTimeUseFlag = ViCommConst.FLAG_OFF;
			if (chkMobileMailRxTimeUseFlag.Selection == null) {
				iMobileMailRxTimeUseFlag = ViCommConst.FLAG_OFF;
			} else {
				iMobileMailRxTimeUseFlag = ViCommConst.FLAG_ON;
			}
			iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_OFF;
			if (chkMobileMailRxTimeUseFlag2.Selection == null) {
				iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_OFF;
			} else {
				iMobileMailRxTimeUseFlag2 = ViCommConst.FLAG_ON;
			}
			sMobileMailRxStartTime2 = lstMobileMailRxStartTime2.Items[lstMobileMailRxStartTime2.SelectedIndex].Value;
			sMobileMailRxEndTime2 = lstMobileMailRxEndTime2.Items[lstMobileMailRxEndTime2.SelectedIndex].Value;
			
			bool bOk = false;
			
			if (iMobileMailRxTimeUseFlag == ViCommConst.FLAG_ON && iMobileMailRxTimeUseFlag2 == ViCommConst.FLAG_ON) {
				int iMobileMailRxStartTime;
				int iMobileMailRxEndTime;
				int iMobileMailRxStartTime2;
				int iMobileMailRxEndTime2;
				
				iMobileMailRxStartTime = int.Parse(sMobileMailRxStartTime.Replace(":",""));
				iMobileMailRxEndTime = int.Parse(sMobileMailRxEndTime.Replace(":",""));

				iMobileMailRxStartTime2 = int.Parse(sMobileMailRxStartTime2.Replace(":",""));
				iMobileMailRxEndTime2 = int.Parse(sMobileMailRxEndTime2.Replace(":",""));

				if (iMobileMailRxStartTime < iMobileMailRxEndTime && iMobileMailRxStartTime2 < iMobileMailRxEndTime2) {
					if (iMobileMailRxStartTime <= iMobileMailRxStartTime2 && iMobileMailRxEndTime <= iMobileMailRxStartTime2) {
						bOk = true;
					} else if (iMobileMailRxStartTime2 <= iMobileMailRxStartTime && iMobileMailRxEndTime2 <= iMobileMailRxStartTime) {
						bOk = true;
					}
				} else if (iMobileMailRxStartTime < iMobileMailRxEndTime && iMobileMailRxStartTime2 >= iMobileMailRxEndTime2) {
					if (iMobileMailRxStartTime >= iMobileMailRxEndTime2 && iMobileMailRxEndTime <= iMobileMailRxStartTime2) {
						bOk = true;
					}
				} else if (iMobileMailRxStartTime >= iMobileMailRxEndTime && iMobileMailRxStartTime2 < iMobileMailRxEndTime2) {
					if (iMobileMailRxStartTime2 >= iMobileMailRxEndTime && iMobileMailRxEndTime2 <= iMobileMailRxStartTime) {
						bOk = true;
					}
				} else {
					bOk = false;
				}
			} else {
				bOk = true;
			}
			
			if (bOk == false) {
				sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_RX_TIME);
				return;
			}
		}
		
		sessionMan.userMan.SetupRxMailDouble(
			sessionMan.site.siteCd,
			sessionMan.userMan.userSeq,
			lstCastMailRxType.Items[lstCastMailRxType.SelectedIndex].Value,
			lstInfoMailRxType.Items[lstInfoMailRxType.SelectedIndex].Value,
			iTalkEndMailRxFlag,
			(chkLoginMailRxFlag1.SelectedIndex == 0),
			(chkLoginMailRxFlag2.SelectedIndex == 0),
			iMobileMailRxTimeUseFlag,
			sMobileMailRxStartTime,
			sMobileMailRxEndTime,
			iMobileMailRxTimeUseFlag2,
			sMobileMailRxStartTime2,
			sMobileMailRxEndTime2,
			ViCommConst.FLAG_OFF
		);

		int iTweetCommentMailRxUseFlag;
		int.TryParse(iBridUtil.GetStringValue(this.Request.Form["tweet_comment"]),out iTweetCommentMailRxUseFlag);

		int iTweetCommentMailRxFlag;
		if (iTweetCommentMailRxUseFlag == ViCommConst.FLAG_ON) {
			iTweetCommentMailRxFlag = (chkTweetCommentMailRxFlag.SelectedIndex == 0) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		} else {
			iTweetCommentMailRxFlag = sessionMan.userMan.tweetCommentMailRxFlag;
		}

		using (UserManCharacterEx oUserManCharacterEx = new UserManCharacterEx()) {
			oUserManCharacterEx.SetupRxMailEx(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				lstBlogMailRxFlag.Items[lstBlogMailRxFlag.SelectedIndex].Value,
				(chkWaitMailRxFlag.SelectedIndex == 0) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF,
				iTweetCommentMailRxFlag
			);
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SETUP_MAIL_COMPLITE));
	}

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<cc1:iBMobileLiteralText runat="server" Text="<font color=#FF0099 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblInfoMailRxType" runat="server">ｻｲﾄからのお知らせﾒｰﾙを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstInfoMailRxType" Runat="server"></mobile:SelectionList>
		<cc1:iBMobileLiteralText runat="server" Text="<br /><font color=#FF0099 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblCastMailRxType" runat="server">女性からのﾒｰﾙを</cc1:iBMobileLabel>
		<mobile:SelectionList ID="lstCastMailRxType" Runat="server"></mobile:SelectionList>
		<mobile:Panel ID="pnlTalkEndMailRxFlag" Runat="server">
			<cc1:iBMobileLabel ID="lblTalkEndMailRxFlag" Runat="server">通話切断理由ﾒｰﾙを</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstTalkEndMailRxFlag" Runat="server"></mobile:SelectionList>
		</mobile:Panel>
		<mobile:Panel ID="pnlBlogMailRxFlag" Runat="server">
			<cc1:iBMobileLiteralText runat="server" Text="<br /><font color=#FF0099 size=2>▼</font>" />
			<cc1:iBMobileLabel ID="lblBlogMailRxFlag" runat="server">お気に入り登録した女性がブログ投稿した時のﾒｰﾙを</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstBlogMailRxFlag" Runat="server">
			</mobile:SelectionList>
		</mobile:Panel>
		$PGM_HTML02;
		<cc1:iBMobileLiteralText runat="server" Text="<br /><font color=#FF0099 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblLoginMailRxFlag1" runat="server">お気に入り登録した子がTEL待ちした時に</cc1:iBMobileLabel>
		<mobile:SelectionList ID="chkLoginMailRxFlag1" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML03;" Value="1" />
		</mobile:SelectionList>		
		<cc1:iBMobileLiteralText runat="server" Text="<br /><font color=#FF0099 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblLoginMailRxFlag2" runat="server">ｱﾅﾀをお気に入り登録している女性がTEL待ちした時に</cc1:iBMobileLabel>
		<mobile:SelectionList ID="chkLoginMailRxFlag2" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML04;" Value="1" />
		</mobile:SelectionList>
		<cc1:iBMobileLiteralText runat="server" Text="<br /><font color=#FF0099 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblWaitMailRxFlag" runat="server">今までに通話した事のある女性の方がTEL待ちした時に</cc1:iBMobileLabel>
		<mobile:SelectionList ID="chkWaitMailRxFlag" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML06;" Value="1" />
		</mobile:SelectionList>
		$PGM_HTML07;
		<cc1:iBMobileLiteralText runat="server" Text="<br /><font color=#FF0099 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblTweetCommentMailRxFlag" runat="server">つぶやきにｺﾒﾝﾄされた時に</cc1:iBMobileLabel>
		<mobile:SelectionList ID="chkTweetCommentMailRxFlag" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left">
			<Item Text="$PGM_HTML05;" Value="1" />
		</mobile:SelectionList>
		$PGM_HTML08;
		<cc1:iBMobileLiteralText runat="server" Text="<br /><font color=#FF0099 size=2>▼</font>" />
		<cc1:iBMobileLabel ID="lblMobileMailRxTime" runat="server">受信時間帯</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText runat="server" Text="<font color=#FF0099 size=2>※</font>設定時間以外は携帯へ転送されません<br />" />
		<mobile:SelectionList ID="chkMobileMailRxTimeUseFlag" Runat="server" SelectType="CheckBox" Font-Size="Small" BreakAfter="false" Visible="true"><Item Text="" Value="1" /></mobile:SelectionList>
		<mobile:SelectionList ID="lstMobileMailRxStartTime" Runat="server" BreakAfter="false"></mobile:SelectionList>〜
		<mobile:SelectionList ID="lstMobileMailRxEndTime" Runat="server"></mobile:SelectionList><br />
		<mobile:SelectionList ID="chkMobileMailRxTimeUseFlag2" Runat="server" SelectType="CheckBox" Font-Size="Small" BreakAfter="false" Visible="true"><Item Text="" Value="1" /></mobile:SelectionList>
		<mobile:SelectionList ID="lstMobileMailRxStartTime2" Runat="server" BreakAfter="false"></mobile:SelectionList>〜
		<mobile:SelectionList ID="lstMobileMailRxEndTime2" Runat="server" BreakAfter="false"></mobile:SelectionList>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
