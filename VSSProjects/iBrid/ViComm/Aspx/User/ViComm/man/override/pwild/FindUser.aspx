<%@ Page Language="C#" AutoEventWireup="true" CodeFile="../../FindUser.aspx.cs" Inherits="ViComm_man_FindUser" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="ViComm" %>

<script runat="server">
	protected override void OnLoad(EventArgs e) {
		
		if (!this.IsPostBack) {		
		
			// 地域
			using (CastAttrTypeValue oAttrTypeValue = new CastAttrTypeValue()) {		
				DataSet dsAttrTypeValue = oAttrTypeValue.GetList(sessionMan.site.siteCd, "405");

				using (CodeDtl oCodeDtl = new CodeDtl()) {
					DataSet dsCodeDtl = oCodeDtl.GetList("81");
					foreach (DataRow drCodeDtl in dsCodeDtl.Tables[0].Rows) {
						string sExp = string.Format("GROUPING_CD={0}", drCodeDtl["CODE"]);
							
						string sValues = string.Empty;
						foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Select(sExp)) {
							if(!string.IsNullOrEmpty(sValues)) sValues += ",";
							sValues += drAttrTypeValue["CAST_ATTR_SEQ"];
						}

						lstCastItemEx1.Items.Add(new MobileListItem(drCodeDtl["CODE_NM"].ToString(), sValues));

					}
				}

				foreach (DataRow drAttrTypeValue in dsAttrTypeValue.Tables[0].Rows) {
					if (drAttrTypeValue["OMIT_SEEK_CONTION_FLAG"].ToString().Equals("0")) {
						lstCastItemEx1.Items.Add(new MobileListItem(drAttrTypeValue["CAST_ATTR_NM"].ToString(), drAttrTypeValue["CAST_ATTR_SEQ"].ToString()));
					}
				}
			}
			// 検索条件の復帰
			int? iWithoutNonAdult = Session["ViComm_man_FindUser.rdoWithoutNonAdult"] as int?;
			if (iWithoutNonAdult != null) {
				this.rdoWithoutNonAdult.SelectedIndex = iWithoutNonAdult.Value;
			}
			int? iCastItemEx1 = Session["ViComm_man_FindUser.lstCastItemEx1"] as int?;
			if (iCastItemEx1 != null) {
				this.lstCastItemEx1.SelectedIndex = iCastItemEx1.Value;
            }
            
            if (!sessionMan.logined) {
				rdoStatus.Visible = false;
				chkNewCast.Visible = false;
            }
		 
		} else {
			Session["ViComm_man_FindUser.rdoWithoutNonAdult"] = this.rdoWithoutNonAdult.SelectedIndex;
			Session["ViComm_man_FindUser.lstCastItemEx1"] = lstCastItemEx1.SelectedIndex;
		}
	
		base.OnLoad(e);
	}
	
	protected override void AppendSearchCondtion(ref string pUrl,int pIndex) {
		
			// アダルト女性を表示する/しない
			if (this.rdoWithoutNonAdult.SelectedIndex == 1) {
				pUrl += string.Format("&item{0:D2}=10549&typeseq{0:D2}=409&group{0:D2}=&like{0:D2}=0&noteq{0:D2}=1",pIndex);
			}
			pIndex += 1;
			// 地域
			pUrl += string.Format("&item{0:D2}={1}&typeseq{0:D2}=405&group{0:D2}=&like{0:D2}=0&noteq{0:D2}=", pIndex, lstCastItemEx1.Items[lstCastItemEx1.SelectedIndex].Value);

			// ログインしている場合のみ、選択されている検索条件を保存
			if (sessionMan.logined && sessionMan.userMan.userSeq != null) {
				RegistFindCastHistory();
			}
	}

	/// <summary>
	/// 選択されている検索条件を保存
	/// </summary>
	private void RegistFindCastHistory() {
		FindCastCondition oFindCastCondition = new FindCastCondition();

		// ステータス
		if (pnlOnline.Visible) {
			if (rdoStatus.Items[1].Selected) {
				// ログイン中のみ
				oFindCastCondition.onlineStatus = ViCommConst.SeekOnlineStatus.LOGINED_AND_WAITING.ToString();
			} else if (rdoStatus.Items[2].Selected) {
				// 待機中のみ
				oFindCastCondition.onlineStatus = ViCommConst.SeekOnlineStatus.WAITING.ToString();
			} else {
				// 全ての女性
				oFindCastCondition.onlineStatus = ViCommConst.SeekOnlineStatus.IGNORE.ToString();
			}
		}

		// 年齢
		if (pnlAge.Visible) {
			oFindCastCondition.ageFrom = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[0]);
			oFindCastCondition.ageTo = System.Web.HttpUtility.UrlEncode(lstAge.Selection.Value.Split(':')[1]);
		}

		// 地域
		if (pnlCastItemEx.Visible) {
			if (lstCastItemEx1.SelectedIndex > 0) {
				oFindCastCondition.area = lstCastItemEx1.Items[lstCastItemEx1.SelectedIndex].Value;
			}

			// 男性の登録地域
			string sDefaultArea = Session[this.GetType().FullName + ".DefaultArea"] as string;
			if (!string.IsNullOrEmpty(sDefaultArea)) {
				oFindCastCondition.areaDefault = sDefaultArea;
			}
		}

		// エッチ度
		if (pnlCastItem9.Visible && lstCastItem9.SelectedIndex > 0) {
			oFindCastCondition.hDegree = lstCastItem9.Items[lstCastItem9.SelectedIndex].Value;
		}

		// SM度
		if (pnlCastItem10.Visible && lstCastItem10.SelectedIndex > 0) {
			oFindCastCondition.smDegree = lstCastItem10.Items[lstCastItem10.SelectedIndex].Value;
		}

		// スタイル
		if (pnlCastItem3.Visible && lstCastItem3.SelectedIndex > 0) {
			oFindCastCondition.style = lstCastItem3.Items[lstCastItem3.SelectedIndex].Value;
		}

		// タイプ
		if (pnlCastItem2.Visible && lstCastItem2.SelectedIndex > 0) {
			oFindCastCondition.likeType = lstCastItem2.Items[lstCastItem2.SelectedIndex].Value;
		}

		// 職業
		if (pnlCastItem4.Visible && lstCastItem4.SelectedIndex > 0) {
			oFindCastCondition.job = lstCastItem4.Items[lstCastItem4.SelectedIndex].Value;
		}

		// 出没時間
		if (pnlCastItem8.Visible && lstCastItem8.SelectedIndex > 0) {
			oFindCastCondition.loginTimeZone = lstCastItem8.Items[lstCastItem8.SelectedIndex].Value;
		}

		// TEL待ち
		if (pnlCastItem11.Visible && lstCastItem11.SelectedIndex > 0) {
			oFindCastCondition.waitTel = lstCastItem11.Items[lstCastItem11.SelectedIndex].Value;
		}

		// 顔出し
		if (pnlCastItem12.Visible && lstCastItem12.SelectedIndex > 0) {
			oFindCastCondition.faceDisplay = lstCastItem12.Items[lstCastItem12.SelectedIndex].Value;
		}

		// 名前
		if (pnlCastNm.Visible) {
			oFindCastCondition.handleNm = txtHandelNm.Text.ToString();
		}

		// アダルトNGの女性表示
		if (pnlWithoutNonAdult.Visible) {
			oFindCastCondition.withoutNonAdult = rdoWithoutNonAdult.SelectedIndex.ToString();
		}

		// 身長
		if (pnlCastItem6.Visible && lstCastItem6.SelectedIndex > 0) {
			oFindCastCondition.height = lstCastItem6.Items[lstCastItem6.SelectedIndex].Value;
		}

		// バスト
		if (pnlCastItem5.Visible) {
			if (lstCastItem5.SelectedIndex > 0) {
				oFindCastCondition.bustFrom = lstCastItem5.Items[lstCastItem5.SelectedIndex].Value;
			}
			if (lstCastItem5Max.SelectedIndex > 0) {
				oFindCastCondition.bustTo = lstCastItem5Max.Items[lstCastItem5Max.SelectedIndex].Value;
			}
		}

		// 血液型
		if (pnlCastItem7.Visible && lstCastItem7.SelectedIndex > 0) {
			oFindCastCondition.bloodType = lstCastItem7.Items[lstCastItem7.SelectedIndex].Value;
		}

		// フリーワード
		if (pnlComment.Visible) {
			oFindCastCondition.freeword = txtComment.Text.ToString();
		}

		// 検索条件を保存
		FindCastHistory oFindCastHistory = new FindCastHistory();
		oFindCastHistory.RegistFindCastHistory(sessionMan.site.siteCd,sessionMan.userMan.userSeq,oFindCastCondition);
	}

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:Panel ID="pnlOnline" Runat="server">
			<cc1:iBMobileLabel ID="lblOnline" runat="server" BreakAfter="true">
			</cc1:iBMobileLabel>
			<mobile:SelectionList ID="rdoStatus" Runat="server" SelectType="Radio" Alignment="Left">
				<Item Text="全ての女性" Value="0" Selected="true" />
				<Item Text="$PGM_HTML02;" Value="1" Selected="false" />
				<Item Text="$PGM_HTML04;" Value="2" Selected="false" />
			</mobile:SelectionList>
			<mobile:SelectionList ID="chkNewCast" Runat="server" SelectType="CheckBox" Alignment="Left" BreakAfter="False">
				<Item Text="$PGM_HTML05;" Value="1" Selected="false" />
			</mobile:SelectionList>
			<mobile:Panel ID="pnlAge" Runat="server">
			    <cc1:iBMobileLiteralText ID="tagLinesAge" runat="server" Text="$PGM_HTML03;" />
				<mobile:SelectionList ID="lstAge" Runat="server" SelectType="DropDown">
					<Item Value=":" Text="--年齢--"></Item>
					<Item Value="18:20" Text="18〜20歳"></Item>
					<Item Value="21:24" Text="21〜24歳"></Item>
					<Item Value="25:29" Text="25〜29歳"></Item>
					<Item Value="30:34" Text="30〜34歳"></Item>
					<Item Value="35:39" Text="35〜39歳"></Item>
					<Item Value="40:" Text="40歳以上"></Item>
				</mobile:SelectionList>
			</mobile:Panel>
			<cc1:iBMobileLiteralText ID="tagLinesHandelNm" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem1" Runat="server" Visible="False">
			<cc1:iBMobileLabel ID="lblCastItem1" runat="server" BreakAfter="true">castItemm01</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem1" Runat="server"><Item Text="castItemm01" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem1" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItemEx" Runat="server" Visible="true">
			<mobile:SelectionList ID="lstCastItemEx1" Runat="server">
				<Item Text="--地域--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLinesEx1" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>	
		<mobile:Panel ID="pnlCastItem9" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem9" runat="server" BreakAfter="true" Visible="false">castItemm09</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem9" Runat="server">
				<Item Text="--ｴｯﾁ度--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem9" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines9" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem10" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem10" runat="server" BreakAfter="true" Visible="false">castItemm10</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem10" Runat="server">
				<Item Text="--SM度--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem10" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem3" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem3" runat="server" BreakAfter="true" Visible="false">castItemm03</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem3" Runat="server">
				<Item Text="--ｽﾀｲﾙ--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem3" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines3" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem2" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem2" runat="server" BreakAfter="true" Visible="false">castItemm02</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem2" Runat="server">
				<Item Text="--ﾀｲﾌﾟ--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem2" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines2" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem4" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem4" runat="server" BreakAfter="true" Visible="false">castItemm04</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem4" Runat="server">
				<Item Text="--職業--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem4" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines4" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem8" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem8" runat="server" BreakAfter="true" Visible="false">castItemm08</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem8" Runat="server">
				<Item Text="--出没時間--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem8" runat="server" MaxLength="300" Size="12" ></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines8" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem11" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem11" runat="server" BreakAfter="true" Visible="false">castItemm11</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem11" Runat="server">
				<Item Text="--TEL待ち--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem11" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem12" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem12" runat="server" BreakAfter="true" Visible="false">castItemm12</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem12" Runat="server">
				<Item Text="--顔だし--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem12" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastNm" Runat="server">
			<cc1:iBMobileLiteralText ID="tagEmojiHandleNm" runat="server" Text="<font color=orange>$xE6FA;</font>" />
			<cc1:iBMobileLabel ID="lblHandleNm" runat="server" BreakAfter="true">名前に</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12" BreakAfter="false"></cc1:iBMobileTextBox>
			を含む
			<cc1:iBMobileLiteralText ID="tagLinesOnline" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlWithoutNonAdult" Runat="server" BreakAfter="false">
			<cc1:iBMobileLiteralText ID="tagEmojiWithoutNonAdult" runat="server" Text="<font color=red>$xE72F;</font>ｱﾀﾞﾙﾄNGの女性を表示<br />" />
			<mobile:SelectionList ID="rdoWithoutNonAdult" Runat="server" SelectType="Radio" BreakAfter="False">
				<Item Text="問わない" Value="1" Selected="True" />
				<Item Text="しない" Value="0" />
			</mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLinesWithoutNonAdult" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem6" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem6" runat="server" BreakAfter="true" Visible="false">castItemm06</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem6" Runat="server">
				<Item Text="--身長--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem6" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines6" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem5" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem5" runat="server" BreakAfter="true" Visible="false">castItemm05</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem5" Runat="server" BreakAfter="false">
				<Item Text="--ﾊﾞｽﾄ--" Value="*" />
			</mobile:SelectionList>
			$PGM_HTML07;
			〜
			<mobile:SelectionList ID="lstCastItem5Max" Runat="server">
				<Item Text="--ﾊﾞｽﾄ--" Value="*" />
			</mobile:SelectionList>
			$PGM_HTML08;
			<cc1:iBMobileTextBox ID="txtCastItem5" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines5" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem7" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem7" runat="server" BreakAfter="true" Visible="false">castItemm07</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem7" Runat="server">
				<Item Text="--血液型--" Value="*" />
			</mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem7" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines7" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem13" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem13" runat="server" BreakAfter="true">castItemm13</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem13" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem13" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem14" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem14" runat="server" BreakAfter="true">castItemm14</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem14" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem14" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem15" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem15" runat="server" BreakAfter="true">castItemm15</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem15" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem15" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem16" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem16" runat="server" BreakAfter="true">castItemm16</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem16" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem16" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem17" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem17" runat="server" BreakAfter="true">castItemm17</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem17" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem17" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem18" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem18" runat="server" BreakAfter="true">castItemm18</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem18" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem18" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem19" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem19" runat="server" BreakAfter="true">castItemm19</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem19" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem19" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlCastItem20" Runat="server">
			<cc1:iBMobileLabel ID="lblCastItem20" runat="server" BreakAfter="true">castItemm20</cc1:iBMobileLabel>
			<mobile:SelectionList ID="lstCastItem20" Runat="server"><Item Text="指定なし" Value="*" /></mobile:SelectionList>
			<cc1:iBMobileTextBox ID="txtCastItem20" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML03;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlOkList" Runat="server">
			<cc1:iBMobileLabel ID="lblOkList" runat="server" BreakAfter="true">OKﾘｽﾄ</cc1:iBMobileLabel>
			<mobile:SelectionList ID="chkOK0" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK1" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK2" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK3" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK4" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK5" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK6" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK7" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK8" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK9" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK10" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK11" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK12" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK13" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK14" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK15" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK16" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK17" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK18" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK19" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK20" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK21" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK22" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK23" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK24" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK25" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK26" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK27" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK28" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK29" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK30" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK31" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK32" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK33" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK34" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK35" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK36" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK37" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK38" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<mobile:SelectionList ID="chkOK39" Runat="server" SelectType="CheckBox" Alignment="Left" Visible="false"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines21" runat="server" Text="$PGM_HTML03;" />
			<br />
		</mobile:Panel>
		<mobile:Panel ID="pnlComment" Runat="server">
			<cc1:iBMobileLabel ID="lblComment" runat="server" BreakAfter="true">$xE68F;ﾌﾘｰﾜｰﾄﾞ検索</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtComment" runat="server" MaxLength="20" Size="12" BreakAfter="false"></cc1:iBMobileTextBox>
			<cc1:iBMobileLabel ID="lblComment2" runat="server" BreakAfter="true">を含む</cc1:iBMobileLabel>
			<cc1:iBMobileLabel ID="lblComment3" runat="server" BreakAfter="true">↑女性のﾌﾟﾛﾌｺﾒﾝﾄより検索</cc1:iBMobileLabel>
		</mobile:Panel>
		$PGM_HTML06;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
