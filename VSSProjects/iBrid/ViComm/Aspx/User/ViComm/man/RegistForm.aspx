<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistForm.aspx.cs" Inherits="ViComm_man_RegistForm" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<br />
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tag01s" runat="server" Text="<font size=1>" />
		<cc1:iBMobileLabel ID="lblHandelNm" runat="server">名前</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtHandelNm" runat="server" MaxLength="20" Size="12"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines01" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLabel ID="lblPassword" runat="server" Visible="true">ﾊﾟｽﾜｰﾄﾞ</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="10" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines02" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLabel ID="lblTel" runat="server" Visible="true">電話番号</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtTel" runat="server" MaxLength="11" Size="18" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLiteralText ID="tagLines03" runat="server" Text="$PGM_HTML04;" />
		<cc1:iBMobileLabel ID="lblBirthDay" runat="server" Visible="true">生年月日</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtYear" runat="server" MaxLength="4" BreakAfter="false" Size="4" Numeric="true" Visible="true"> </cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblYear" runat="server" BreakAfter="False" Visible="true">年</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtMonth" runat="server" MaxLength="2" BreakAfter="false" Size="2" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblMonth" runat="server" BreakAfter="False" Visible="true">月</cc1:iBMobileLabel>
		<cc1:iBMobileTextBox ID="txtDay" runat="server" MaxLength="2" BreakAfter="false" Size="2" Numeric="true" Visible="true"></cc1:iBMobileTextBox>
		<cc1:iBMobileLabel ID="lblDay" runat="server" Visible="true">日</cc1:iBMobileLabel>
		<cc1:iBMobileLiteralText ID="tagLines04" runat="server" Text="$PGM_HTML04;" />
		<mobile:Panel ID="pnlUserManItem1" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem1" runat="server">userManItem01</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem1" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem1" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem1" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines05" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem2" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem2" runat="server">userManItem02</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem2" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem2" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem2" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines06" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem3" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem3" runat="server">userManItem03</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem3" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem3" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem3" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines07" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem4" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem4" runat="server">userManItem04</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem4" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem4" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem4" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines08" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem5" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem5" runat="server">userManItem05</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem5" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem5" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem5" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines09" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem6" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem6" runat="server">userManItem06</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem6" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem6" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem6" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines10" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem7" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem7" runat="server">userManItem07</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem7" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem7" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem7" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines11" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem8" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem8" runat="server">userManItem08</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem8" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem8" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem8" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines12" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem9" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem9" runat="server">userManItem09</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem9" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem9" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem9" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines13" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem10" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem10" runat="server">userManItem10</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem10" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem10" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem10" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines14" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem11" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem11" runat="server">userManItem11</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem11" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem11" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem11" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines15" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem12" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem12" runat="server">userManItem12</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem12" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem12" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem12" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines16" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem13" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem13" runat="server">userManItem13</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem13" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem13" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem13" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines17" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem14" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem14" runat="server">userManItem14</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem14" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem14" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem14" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines18" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem15" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem15" runat="server">userManItem15</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem15" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem15" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem15" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines19" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem16" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem16" runat="server">userManItem16</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem16" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem16" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem16" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines20" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem17" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem17" runat="server">userManItem17</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem17" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem17" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem17" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines21" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem18" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem18" runat="server">userManItem18</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem18" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem18" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem18" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines22" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem19" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem19" runat="server">userManItem19</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem19" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem19" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem19" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines23" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<mobile:Panel ID="pnlUserManItem20" Runat="server">
			<cc1:iBMobileLabel ID="lblUserManItem20" runat="server">userManItem20</cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtUserManItem20" runat="server" MaxLength="300" Size="12"></cc1:iBMobileTextBox>
			<tx:TextArea ID="txtAreaUserManItem20" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="1" Columns="24" MaxLength="300">
			</tx:TextArea>
			<mobile:SelectionList ID="lstUserManItem20" Runat="server"></mobile:SelectionList>
			<cc1:iBMobileLiteralText ID="tagLines24" runat="server" Text="$PGM_HTML04;" />
		</mobile:Panel>
		<cc1:iBMobileLiteralText ID="tag01e" runat="server" Text="</font>" />
		$PGM_HTML02;
		<mobile:SelectionList ID="chkRlue" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false">
			<Item Text="$PGM_HTML05;" Value="1" Selected="True" />
		</mobile:SelectionList>		
		<mobile:SelectionList ID="chkMail" Runat="server" SelectType="CheckBox" Font-Size="Small" Alignment="Left" Visible="false">
			<Item Text="$PGM_HTML06;" Value="1" Selected="True">
			</Item>
		</mobile:SelectionList>
		$PGM_HTML07;
		$PGM_HTML03;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
