<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserTop.aspx.cs" Inherits="ViComm_man_UserTop" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <mobile:Form ID="frmMain" Runat="server">
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
		$PGM_HTML03;
		$PGM_HTML04;
		$PGM_HTML05;
		$PGM_HTML06;
		$DATASET_LOOP_START0;
		$PGM_HTML07;
		$DATASET_LOOP_END0;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
