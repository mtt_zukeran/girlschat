/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ΜΚ^ΐή³έΫ°Δή
--	Progaram ID		: DownloadProdPic
--
--  Creation Date	: 2010.12.17
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;

using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_DownloadProdPic : MobileProdManPage {
	protected void Page_Load(object sender, EventArgs e) {

		string sAction = iBridUtil.GetStringValue(this.Request.Params["action"]);
		string sProductId = iBridUtil.GetStringValue(this.Request.Params["product_id"]);
		string sAdultFlag = iBridUtil.GetStringValue(this.Request.Params["adult"]);
		string sProductType = ProductHelper.GetMovieProductType(sAdultFlag);
		string sObjSeq = iBridUtil.GetStringValue(this.Request.Params["obj_seq"]);
		string sFileNm = iBridUtil.GetStringValue(this.Request.Params["file_nm"]);
		string sUseType = iBridUtil.GetStringValue(this.Request.Params["use_type"]);

		string sProductSeq = null;
		using (Product oProduct = new Product()) {
			sProductSeq = oProduct.ConvertProductId2Seq(sProductId);
		}

		// ’wόΜκΝwόmFζΚΦJΪ
		if (!ProductHelper.IsAlreadyUsed(sProductSeq, sProductType)) {
			this.Redirect2ConfirmProdPic();
		}

		ulong lSeekMode = ulong.Parse(this.Request.QueryString["seekmode"]);
		if (iBridUtil.GetStringValue(this.Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
			lSeekMode = ViCommConst.INQUIRY_PRODUCT_PIC;
		}

		// ======================
		//  ζΚ\¦
		// ======================
		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter, this.frmMain, this.Request, this.ViewState);
		if (!this.IsPostBack) {
			if (!sessionMan.ControlList(this.Request, lSeekMode, this.ActiveForm)) {
				throw new ApplicationException();
			}
		}
	}

	private void Redirect2ConfirmProdPic() {
		UrlBuilder oUrlBuilder = new UrlBuilder("ConfirmProdPic.aspx", this.Request.QueryString);
		if (oUrlBuilder.Parameters.ContainsKey(ViCommConst.ACTION)) {
			oUrlBuilder.Parameters.Remove(ViCommConst.ACTION);
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
	}
}
