/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: PointGate�x��
--	Progaram ID		: PaymentPointGate
--
--  Creation Date	: 2008.03.02
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentPointGate:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {

		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sSid = "";

			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_SITE02,
					ViCommConst.SETTLE_POINT_AFFILIATE,
					ViCommConst.SETTLE_STAT_SETTLE_NOW,
					0,
					0,
					string.Empty,
					string.Empty,
					out sSid);
			}
			sessionMan.userMan.settleId = sSid;
		}
	}

}
