﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ユーザー掲示板書き込み
--	Progaram ID		: WriteUserBbs
--
--  Creation Date	: 2010.04.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_man_WriteUserBbs:MobileManPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);

		bool bOk = true;

		lblErrorMessage.Text = string.Empty;

		if (txtTitle.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TITLE);
		}
		if (txtDoc.Text.Equals("")) {
			bOk = false;
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_DOC);
		}
		if (iMaxLength != 0 && txtDoc.Text.Length > iMaxLength) {
			bOk = false;
			int iOverLength = txtDoc.Text.Length - iMaxLength;
			lblErrorMessage.Text += string.Format(GetErrorMessage(PwViCommConst.PwErrerCode.ERR_MAIL_DOC_LENGTH_OVER),iOverLength.ToString(),iMaxLength.ToString());
		}

		if (bOk) {
			if (sessionMan.ngWord == null) {
				sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
			}
			string sNGWord;
			if (sessionMan.ngWord.VaidateDoc(txtTitle.Text + txtDoc.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		string sSiteCd = sessionMan.site.siteCd;
		string sUserSeq = sessionMan.userMan.userSeq;
		string sUserCharNo = sessionMan.userMan.userCharNo;

		using (Site oSite = new Site())
		using (UserManBbs oUserManBbs = new UserManBbs()) {

			oSite.GetOne(sessionMan.site.siteCd);

			int iManWriteBbsLimit = 0;
			int iManWriteIntervalMin = 0;

			if (sessionMan.userMan.totalReceiptAmt > 0) {
				iManWriteBbsLimit = oSite.payManWriteBbsLimit;
				iManWriteIntervalMin = oSite.payManWriteIntervalMin;
			} else {
				iManWriteBbsLimit = oSite.manWriteBbsLimit;
				iManWriteIntervalMin = oSite.manWriteIntervalMin;
			}
			// 書き込み件数のチェック
			if (iManWriteBbsLimit >= 0) {
				int iWriteCnt = oUserManBbs.GetWriteCntEachDay(sSiteCd,sUserSeq,sUserCharNo,DateTime.Now.Date);
				if (iWriteCnt >= iManWriteBbsLimit) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAN_WRITE_BBS_LIMIT);
				}
			}

			// 書き込み間隔のチェック
			DateTime? oLastWriteDate = oUserManBbs.GetLastWriteDate(sSiteCd,sUserSeq,sUserCharNo);
			if ((oLastWriteDate != null)
				&& (oLastWriteDate.Value.AddMinutes(iManWriteIntervalMin) >= DateTime.Now)) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MAN_WRITE_BBS_INTERVAL_SEC);
			}
		}


		if (bOk == false) {
			return;
		}

		// 掲示板書込のポイント消費を行います。 
		/*int iChargePoint;
		bOk = sessionMan.CheckWriteBbsBalance(out iChargePoint);
		if (bOk == false) {
			lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_BALANCE);
		} else {
			using (WebUsedLog oLog = new WebUsedLog()) {
				oLog.CreateWebUsedReport(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.CHARGE_WRITE_BBS,
					iChargePoint,
					null,
					null,
					"");
			}
		}*/

		bOk = bOk & CheckOther();

		if (bOk == false) {
			return;
		}

		string sTitle = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtTitle.Text));
		string sDoc = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtDoc.Text));
		RestrictDocLength(ref sDoc);

		using (UserManBbs oUserManBbs = new UserManBbs()) {
			oUserManBbs.WriteBbs(sessionMan.userMan.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,sTitle,sDoc);
		}

		using (ManNews oManNews = new ManNews()) {
			oManNews.LogManNews(sessionMan.site.siteCd,sessionMan.userMan.userSeq,PwViCommConst.ManNewsType.WRITE_BBS);
		}

		// 書込完了画面に遷移します。 
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_WRITE_USER_BBS_COMPLETE + "&" + Request.QueryString));
	}

	protected virtual void RestrictDocLength(ref string doc) {
		if (doc.Length > 1000) {
			doc = SysPrograms.Substring(doc,1000);
		}
	}

	protected virtual bool CheckOther() {
		return true;
	}
}
