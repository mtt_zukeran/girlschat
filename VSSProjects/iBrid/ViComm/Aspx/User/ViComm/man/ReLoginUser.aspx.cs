/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 固体識別利用再ﾛｸﾞｲﾝ
--	Progaram ID		: ReLoginUser
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ReLoginUser:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			ParseHTML oParseMan = sessionMan.parseContainer;
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_UTN;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("LoginUser.aspx?utn={0}&useutn=1",sUtn)));
	}
}
