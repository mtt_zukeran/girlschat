/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 会話履歴メモ変更
--	Progaram ID		: ModifyTalkHistoryMemo
--
--  Creation Date	: 2009.08.04
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ModifyTalkHistoryMemo:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
						Request,
						ViCommConst.INQUIRY_ALL_TALK_HISTORY,
						ActiveForm);

			ParseHTML oParse = sessionMan.parseContainer;
			string sMemo = sessionMan.GetTalkHistoryValue("MEMO");
			string sTalkSeq = sessionMan.GetTalkHistoryValue("TALK_SEQ");

			txtMemo.Text = sMemo;
			ViewState["TALK_SEQ"] = sTalkSeq;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		using (TalkHistory oTalkHistory = new TalkHistory()) {
			oTalkHistory.ModifyTalkHistoryMemo(ViewState["TALK_SEQ"].ToString(), HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier, txtMemo.Text)));
		}
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MODIFY_TALK_HIS_MEMO_COMPLATE));
	}

}
