/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾌﾟﾚｾﾞﾝﾄ確認
--	Progaram ID		: ViewGameCharacterPresent
--
--  Creation Date	: 2011.09.12
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameCharacterPresent:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);
		string sItemSeq = iBridUtil.GetStringValue(Request.QueryString["item_seq"]);

		this.CheckGameRefusePartner(sPartnerUserSeq,sPartnerUserCharNo);
		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);

		if (!IsPostBack) {
			sessionMan.errorMessage = string.Empty;
			if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty) || sItemSeq.Equals(string.Empty)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
			
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_DEFAULT] != null) {
				cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sItemSeq,ViCommConst.FLAG_OFF_STR);
			} else if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo,sItemSeq,ViCommConst.FLAG_ON_STR);
			}
		}
	}

	private void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo,string pItemSeq,string pPurchaseFlg) {
		string sResult = string.Empty;
		string sUrl = string.Empty;
		sessionMan.errorMessage = string.Empty;

		string sPreCooperationPoint = sessionMan.userMan.gameCharacter.cooperationPoint.ToString();
		string sPreFriendlyPoint = this.GetFriendryPoint(pPartnerUserSeq,pPartnerUserCharNo);
		string sPrePoint = sessionMan.userMan.balPoint.ToString();

		//ﾌﾟﾚｾﾞﾝﾄ上限判定
		int iLimitUsePoint;
		if (!int.TryParse(iBridUtil.GetStringValue(this.Request.Form["limit_use_point"]),out iLimitUsePoint)) {
			iLimitUsePoint = 50000;
		}
		
		using (PresentHistory oPresentHistory = new PresentHistory()) {
			string sToday = DateTime.Now.ToString("yyyy/MM/dd");
			DateTime dtFrom	= DateTime.Parse(string.Format("{0} 00:00:00",sToday));
			DateTime dtTo = DateTime.Parse(string.Format("{0} 23:59:59",sToday));;
			int iTodayTotalPrice = oPresentHistory.GetTotalPriceDaily(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.userCharNo,dtFrom,dtTo);

			if (iTodayTotalPrice > iLimitUsePoint) {
				sessionMan.errorMessage = string.Format("今日ﾌﾟﾚｾﾞﾝﾄしたｱｲﾃﾑの合計価格が{0}ptを超えています。",iLimitUsePoint.ToString());
				return;
			}
		}

		if (pPurchaseFlg.Equals(ViCommConst.FLAG_ON_STR)) {
			BuyGameItem(pItemSeq);
		}

		sResult = this.PresentGameItem(pPartnerUserSeq,pPartnerUserCharNo,pItemSeq);
		
		if(sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			this.CheckQuestClear(this.sessionMan.userMan.userSeq,this.sessionMan.userMan.userCharNo,ViCommConst.MAN);
			this.CheckQuestClear(pPartnerUserSeq,pPartnerUserCharNo,ViCommConst.OPERATOR);
		}

		if (sResult == ViCommConst.FLAG_OFF_STR) {
			sUrl = string.Format(
				"ViewGameCharacterPresentResult.aspx?partner_user_seq={0}&partner_user_char_no={1}&item_seq={2}&purchase_flg={3}&pre_cooperation_point={4}&pre_friendly_point={5}&pre_point={6}",
				pPartnerUserSeq,
				pPartnerUserCharNo,
				pItemSeq,
				pPurchaseFlg,
				sPreCooperationPoint,
				sPreFriendlyPoint,
				sPrePoint
			);

			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,sUrl));
		} else if (sResult == ViCommConst.FLAG_ON_STR) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_PRESENT_GAME_ITEM_INJUSTICE);
		} else {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
	}

	private void BuyGameItem(string pItem_seq) {
		string sSiteCode = sessionMan.userMan.siteCd;
		string sUserSeq = sessionMan.userMan.userSeq;
		string sUserCharNo = sessionMan.userMan.userCharNo;
		string sBuyNum = ViCommConst.FLAG_ON_STR;
		string result = this.BuyGameItemExeCute(sSiteCode,sUserSeq,sUserCharNo,pItem_seq,sBuyNum);

		if (!result.Equals(PwViCommConst.GameItemBuy.BUY_GAME_ITEM_RESULT_OK)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
	}

	private string BuyGameItemExeCute(string sSiteCode,string sUserSeq,string sUserCharNo,string sItemSeq,string sBuyNum) {
		string sResult = "";

		GameItem oGameItem = new GameItem();
		oGameItem.BuyGameItem(sSiteCode,sUserSeq,sUserCharNo,sItemSeq,sBuyNum,out sResult);

		return sResult;
	}
	
	private string PresentGameItem(string pPartnerUserSeq,string pPartnerUserCharNo,string pItemSeq) {
		string sResult = string.Empty;
		
		using (GameItem oGameItem = new GameItem()) {
			sResult = oGameItem.PresentGameItem(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				pPartnerUserSeq,
				pPartnerUserCharNo,
				sessionMan.sexCd,
				pItemSeq
			);
		}
		
		return sResult;
	}
	
	private string GetFriendryPoint(string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sFriendryPoint = string.Empty;

		FriendlyPointSeekCondition oCondition = new FriendlyPointSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.UserSeq = this.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
		oCondition.PartnerUserSeq = pPartnerUserSeq;
		oCondition.PartnerUserCharNo = pPartnerUserCharNo;
		using (FriendlyPoint oFriendlyPoint = new FriendlyPoint()) {
			sFriendryPoint = oFriendlyPoint.GetTotalFriendlyPoint(oCondition);
		}

		return sFriendryPoint;
	}

	private void CheckQuestClear(string pUserSeq,string pUserCharNo,string pSexCd) {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				pUserSeq,
				pUserCharNo,
				string.Empty,
				string.Empty,
				out sQuestClearFlag,
				out sResult,
				pSexCd
			);
		}
	}
}