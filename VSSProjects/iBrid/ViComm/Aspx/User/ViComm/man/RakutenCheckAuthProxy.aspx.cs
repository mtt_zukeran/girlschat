﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 楽天認証Proxy
--	Progaram ID		: RakutenCheckAuthProxy.aspx
--
--  Creation Date	: 2010.08.23
--  Creater			: K.Itoh
--
**************************************************************************/
// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX
-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Rakuten;

public partial class ViComm_man_RakutenCheckAuthProxy : MobileManPageBase {
	private UserAuth rakutenUserAuth = new UserAuth();

	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.CheckAuthentication();
		}
	}

	/// <summary>
	/// [ViComm⇒楽天]
	/// 認証結果確認要求URLにアクセスし楽天ID認証結果が正しいかどうかチェックする。
	/// </summary>
	private void CheckAuthentication() {

		SessionObjs.Current.tempOpenId = null;
		SessionObjs.Current.tempOpenIdType = null;
		
		// ユーザー認証結果確認用のURLを生成する
		string sCheckAuthenticaionUrl = this.rakutenUserAuth.GenerateCheckAuthenticaionUrl(this.Request);
		
		// ユーザー認証結果確認処理
		if (this.rakutenUserAuth.CheckAuthenticaion(sCheckAuthenticaionUrl)) {
		
			// ﾕｰｻﾞｰ認証結果のリクエストからｵｰﾌﾟﾝID(及びユーザー情報)を抽出
			UserAuthResultEntity oRakutenUserAuthResult = this.rakutenUserAuth.ParseUserAuthResult(this.Request);
			
			SessionObjs.Current.tempOpenId = oRakutenUserAuthResult.OpenId;
			SessionObjs.Current.tempOpenIdType = ViCommConst.OpenIdTypes.RAKUTEN;
			
			this.RedirectToRegistUserRequestByTermId();

		} else {
			// 楽天認証の確認が取れない場合はシステムエラー扱い。
			StringBuilder oErrMsgBuilder = new StringBuilder();
			oErrMsgBuilder.Append("楽天ID認証で認証結果の確認が取れませんでした。").AppendLine();
			oErrMsgBuilder.AppendFormat("URL:{0}", sCheckAuthenticaionUrl).AppendLine();
			throw new Exception(oErrMsgBuilder.ToString());
		}
	}
	
	#region □■□ 画面遷移 □■□ ================================================================

	private void RedirectToRegistUserRequestByTermId() {
		string sId = Mobile.GetiModeId(SessionObjs.Current.carrier, Request);
		string sUtn = Mobile.GetUtn(SessionObjs.Current.carrier, Request);
		((ParseHTML)sessionMan.parseContainer).parseUser.postAction = 0;

		UrlBuilder oUrlBuilder = new UrlBuilder("RegistUserRequestByTermId.aspx");
		oUrlBuilder.Parameters.Add("utn", sUtn);
		oUrlBuilder.Parameters.Add("id", sId);
		RedirectToMobilePage(oUrlBuilder.ToString());
	}
	#endregion ====================================================================================
}
