/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ﾋﾟｯｸｱｯﾌﾟ一覧
--	Progaram ID		: ListPickup
--
--  Creation Date	: 2010.11.17
--  Creater			: K.Itoh
--
**************************************************************************/
// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

using ViComm;
using iBridCommLib;

public partial class ViComm_man_ListPickup : MobileManPageBase {
	protected void Page_Load(object sender, EventArgs e) {

		string sPickupId = iBridUtil.GetStringValue(this.Request.QueryString["pickupid"]);
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, string.Format("ListPickup{0}.aspx", sPickupId), ViewState);

		if (IsAvailableService(ViCommConst.RELEASE_PICKUP_MV_SEARCH_MAIL_TX_DATE,2)) {
			if (sPickupId.Equals("36") || sPickupId.Equals("37")) {
				if (string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["lasttxmaildays"]))) {
					NameValueCollection oQeuryList = new NameValueCollection(this.Request.QueryString);
					oQeuryList.Remove("lasttxmaildays");
					string sQuery = this.ConstructQueryString(oQeuryList);
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListPickup.aspx?{0}",sQuery + "&lasttxmaildays=2")));
				}
			}
		}
		
		if (!this.IsPostBack) {
			ulong pMode = 0;
			string sPickupType = string.Empty;
			
			using(Pickup oPickup = new Pickup()){
				sPickupType = oPickup.GetPickupType(sessionMan.site.siteCd,sPickupId);
			}
			
			switch(sPickupType){
				case ViCommConst.PicupTypes.BBS_MOVIE:
				case ViCommConst.PicupTypes.PROFILE_MOVIE:
					pMode = ViCommConst.INQUIRY_PICKUP_MOVIE;
					break;
				case ViCommConst.PicupTypes.BBS_PIC:
				case ViCommConst.PicupTypes.PROFILE_PIC:
					pMode = ViCommConst.INQUIRY_PICKUP_PIC;
					break;
				case ViCommConst.PicupTypes.CAST_CHARACTER:
					pMode = ViCommConst.INQUIRY_RECOMMEND;
					break;
				default:
					throw new NotSupportedException(string.Format("想定外のピックアップ区分です。{0}",sPickupType));
			}

			sessionMan.ControlList(Request, pMode, ActiveForm);
		}
	}

	private string ConstructQueryString(NameValueCollection parameters) {
		List<string> items = new List<String>();
		foreach (string name in parameters)
			items.Add(string.Concat(name,"=",parameters[name]));
		return string.Join("&",items.ToArray());
	}
}
