﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ホワイトプラン利用設定
--	Progaram ID		: SetupWhitePlan
--
--  Creation Date	: 2011.02.09
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_SetupWhitePlan : MobileManPageBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
		if (!IsPostBack) {
			string sSetupFlag = iBridUtil.GetStringValue(Request.QueryString["setup"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));

			if (!string.IsNullOrEmpty(sSetupFlag)) {
				this.SetupWhitePlan(sSetupFlag.Equals(ViCommConst.FLAG_ON_STR),sBackUrl);
			} else {
				if (sessionMan.userMan.useWhitePlanFlag != 0) {
					rdoUseWhitePlan.SelectedIndex = 0;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender, e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender, EventArgs e) {
		this.SetupWhitePlan(rdoUseWhitePlan.SelectedIndex == 0,"");
	}

	private void SetupWhitePlan(bool pSetupFlag,string pBackUrl) {
		sessionMan.userMan.SetupWhitePlan(
			sessionMan.userMan.userSeq,
			pSetupFlag
		);

		if (!string.IsNullOrEmpty(pBackUrl)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(pBackUrl));
			return;
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_MAN_USE_WHITE_PLAN));
		}
	}
}
