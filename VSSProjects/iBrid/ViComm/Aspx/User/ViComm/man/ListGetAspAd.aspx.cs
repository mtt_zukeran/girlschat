﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ASP広告自動取得一覧
--	Progaram ID		: ListGetAspAd
--
--  Creation Date	: 2010.11.03
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class ViComm_man_ListGetAspAd:MobileManPageBase {


	protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_GET_ASP_AD,ActiveForm);
		}
	}
}
