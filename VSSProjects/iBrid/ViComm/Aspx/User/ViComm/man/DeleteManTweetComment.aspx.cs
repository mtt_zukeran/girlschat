/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: アイドルテレビ 会員つぶやきコメント削除
--	Progaram ID		: DeleteManTweetComment
--
--  Creation Date	: 2013.01.22
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_DeleteManTweetComment:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_MAN_TWEET_COMMENT,this.ActiveForm);
		} else {
			string sManTweetSeq = iBridUtil.GetStringValue(this.Request.QueryString["tweetseq"]);
			if (IsPostAction(ViCommConst.BUTTON_SUBMIT)) {
				string sSiteCd = sessionMan.site.siteCd;
				string sUserSeq = sessionMan.userMan.userSeq;
				string sUserCharNo = sessionMan.userMan.userCharNo;
				string sManTweetCommentSeq = iBridUtil.GetStringValue(this.Request.QueryString["commentseq"]);
				string sResult = string.Empty;

				using (ManTweetComment oManTweetComment = new ManTweetComment()) {
					oManTweetComment.DeleteManTweetComment(sSiteCd,sUserSeq,sUserCharNo,sManTweetCommentSeq,out sResult);
				}

				if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
					IDictionary<string,string> oParameters = new Dictionary<string,string>();
					oParameters.Add("tweetseq",sManTweetSeq);
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_DELETE_TWEET_COMMENT_COMPLETE,oParameters);
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				}
			} else if (IsPostAction(PwViCommConst.BUTTON_BACK)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("ListManTweetComment.aspx?tweetseq={0}",sManTweetSeq)));
			}
		}
	}
}