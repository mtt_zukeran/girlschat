/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: FanClub�މ�
--	Progaram ID		: FanClubResign
--
--  Creation Date	: 2013.02.02
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_FanClubResign:MobileManPageBase {
	private string sCastUserSeq;
	private string sCastCharNo;

	protected void Page_Load(object sender,EventArgs e) {
		sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (sCastUserSeq.Equals(string.Empty) || sCastCharNo.Equals(string.Empty)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_FANCLUB_STATUS,this.ActiveForm);

		if (!IsPostBack) {
			tagTop.Visible = true;
			tagConfirm.Visible = false;
			tagComplete.Visible = false;
		} else {
			if (Request.Params["btnConfirm"] != null) {
				cmdConfirm_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdConfirm_Click(object sender,EventArgs e) {
		tagTop.Visible = false;
		tagConfirm.Visible = true;
		tagComplete.Visible = false;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sResult;

		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			sResult = oFanClubStatus.FanClubResign(
				sessionMan.site.siteCd,
				sCastUserSeq,
				sCastCharNo,
				sessionMan.userMan.userSeq,
				"�މ�"
			);
		}

		if (sResult.Equals(PwViCommConst.FanClubResignResult.RESULT_OK)) {
			tagTop.Visible = false;
			tagConfirm.Visible = false;
			tagComplete.Visible = true;
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}
	}
}
