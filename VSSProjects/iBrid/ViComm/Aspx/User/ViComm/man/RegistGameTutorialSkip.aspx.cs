/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: Legend of Night チュートリアルスキップ
--	Progaram ID		: RegistGameBattleStart
--
--  Creation Date	: 2012.12.13
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameTutorialSkip:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sessionMan.userMan.gameCharacter.tutorialStatus)) {
				string sGameItemSeq;

				//ﾊﾞｲｱｹﾞﾗを付与
				using (GameItem oGameItem = new GameItem()) {
					sGameItemSeq = oGameItem.GetGameItem(
						sessionMan.userMan.siteCd,
						sessionMan.sexCd,
						PwViCommConst.GameItemCategory.ITEM_CATEGORY_RESTORE,
						"GAME_ITEM_SEQ"
					);
				}

				using (GameItem oGameItem = new GameItem()) {
					oGameItem.GamePossessionItemMainte(
						sessionMan.userMan.siteCd,
						sessionMan.userMan.userSeq,
						sessionMan.userMan.userCharNo,
						sGameItemSeq
					);
				}
			}

			//ﾁｭｰﾄﾘｱﾙ状態更新
			using (GameCharacter oGameCharacter = new GameCharacter()) {
				oGameCharacter.SetupTutorialStatus(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					null
				);
			}

			sessionMan.userMan.gameCharacter.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);

			string sResult = string.Empty;

			using (GameIntroLog oGameIntroLog = new GameIntroLog()) {
				sResult = oGameIntroLog.LogGameIntro(
					sessionMan.userMan.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					ViCommConst.MAN
				);
			}

			if (sResult == PwViCommConst.LogGameIntroResult.RESULT_OK) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("GameMyPage.aspx"));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}
}
