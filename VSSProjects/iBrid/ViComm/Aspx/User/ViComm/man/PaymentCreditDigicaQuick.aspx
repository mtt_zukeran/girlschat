<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentCreditDigicaQuick.aspx.cs" Inherits="ViComm_man_PaymentCreditDigicaQuick" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
		<cc1:iBMobileLiteralText ID="DivS01" runat="server" Text="<DIV align=center>"/>
		<mobile:SelectionList ID="lstCreditPack" Runat="server"></mobile:SelectionList>
		<cc1:iBMobileLiteralText ID="DivE01" runat="server" Text="</DIV>"/>
		$PGM_HTML06;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
