<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewCastDiary.aspx.cs" Inherits="ViComm_man_ViewCastDiary" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="cc1" Namespace="MobileLib" Assembly="MobileLib" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<cc1:iBMobileLiteralText ID="tagComplete" runat="server" Text="$PGM_HTML02;" />
		<cc1:iBMobileLiteralText ID="tagLiked" runat="server" Text="$PGM_HTML03;" />
		$DATASET_LOOP_START3;
			<cc1:iBMobileLiteralText ID="tagList" runat="server" Text="$PGM_HTML07;" />
			<cc1:iBMobileLiteralText ID="tagDetail" runat="server" Text="$PGM_HTML08;" />
		$DATASET_LOOP_END3;
		<cc1:iBMobileLiteralText ID="tagNoDataFound" runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
