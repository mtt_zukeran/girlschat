/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�ʔ̔�����ꗗ
--	Progaram ID		: ListProfileMovie
--
--  Creation Date	: 2010.07.22
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;
public partial class ViComm_man_ListPersonalSaleMovie:MobileManPageBase {
	
	protected void Page_Load(object sender,EventArgs e) {
		

		if (!IsPostBack) {
			string sCastSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;

			DataRow dr;
			bool bSetManData;
			
			if (iBridUtil.GetStringValue(Request.QueryString["direct"]).ToString().Equals("1")) {
				bSetManData = sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),sCastCharNo,1,out dr);
				if (bSetManData) {
					ViewState["CATEGORY_INDEX"] = dr["ACT_CATEGORY_IDX"].ToString();
				} else {
					if (sessionMan.logined) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.userTopId));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sessionMan.site.nonUserTopId));
					}
				}
			} else {
				int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
				bSetManData = sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),sCastCharNo,iRecNo);
			}

			Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

			if (bSetManData) {
				sessionMan.ControlList(
					Request,
					ulong.Parse(iBridUtil.GetStringValue(Request.QueryString["seekmode"])),
					ActiveForm);
			}
		}
	}
}
