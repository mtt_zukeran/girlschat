/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 受信メール設定
--	Progaram ID		: SetupRxMail
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_SetupRxMail:MobileManPageBase {

	private const string PARAM_NM_DIRECT_SETUP = "direct";
	private const string PARAM_NM_CAST_MAIL_RX_TYPE = "castmail";
	private const string PARAM_NM_INFO_MAIL_RX_TYPE = "infomail";
	private const string PARAM_NM_CLEAR_MAIL_ADDR_NG = "clearng";
	private const string PARAM_NEXT_DOC_NO = "*nextdoc";

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		bool bDirectSetup = iBridUtil.GetStringValue(Request.Params[PARAM_NM_DIRECT_SETUP]).Equals(ViCommConst.FLAG_ON_STR);
		string sCastMailRxType = iBridUtil.GetStringValue(Request.Params[PARAM_NM_CAST_MAIL_RX_TYPE]);
		string sInfoMailRxType = iBridUtil.GetStringValue(Request.Params[PARAM_NM_INFO_MAIL_RX_TYPE]);
		string sNextDocNo = iBridUtil.GetStringValue(Request.Params[PARAM_NEXT_DOC_NO]);
		int iClearNg;
		if (!int.TryParse(iBridUtil.GetStringValue(Request.Params[PARAM_NM_CLEAR_MAIL_ADDR_NG]), out iClearNg)) {
			iClearNg = ViCommConst.FLAG_OFF;
		}


		if (!IsPostBack) {
			if (bDirectSetup) {
				this.DirectSetup(sCastMailRxType, sInfoMailRxType,iClearNg, sNextDocNo);
				return;
			}
			if (sessionMan.userMan.loginMailRxFlag1 == 1) {
				chkLoginMailRxFlag1.SelectedIndex = 0;
			}
			if (sessionMan.userMan.loginMailRxFlag2 == 1) {
				chkLoginMailRxFlag2.SelectedIndex = 0;
			}
			if (sessionMan.userMan.characterEx.waitMailRxTyp == 1) {
				chkWaitMailRxFlag.SelectedIndex = 0;
			} 
			
			if (sessionMan.site.loginMail2NotSendFlag == ViCommConst.FLAG_ON) {
				chkLoginMailRxFlag2.Visible = false;
			}
			if (sessionMan.site.manTalkEndSendMailFlag == ViCommConst.FLAG_OFF) {
				pnlTalkEndMailRxFlag.Visible = false;
			}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
				DataSet ds;
				int iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_MAIL_RX_TYPE);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstCastMailRxType.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));
					if (sessionMan.userMan.castMailRxType.Equals(dr["CODE"].ToString())) {
						lstCastMailRxType.SelectedIndex = iIdx;
					}
					iIdx++;
				}

				iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_INFO_MAIL_RX_TYPE);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstInfoMailRxType.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(), dr["CODE"].ToString()));
					if (sessionMan.userMan.InfoMailRxType.Equals(dr["CODE"].ToString())) {
						lstInfoMailRxType.SelectedIndex = iIdx;
					}
					iIdx++;
				}

				iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TIME_PART);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstMobileMailRxStartTime.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));
					lstMobileMailRxEndTime.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE_NM"].ToString()));

					if (sessionMan.userMan.mobileMailRxStartTime.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxStartTime.SelectedIndex = iIdx;
					}
					if (sessionMan.userMan.mobileMailRxEndTime.Equals(dr["CODE_NM"].ToString())) {
						lstMobileMailRxEndTime.SelectedIndex = iIdx;
					}
					iIdx++;
				}

				iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_TALK_END_MAIL_RX_TYPE);
				foreach (DataRow dr in ds.Tables[0].Rows) {

					lstTalkEndMailRxFlag.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(),dr["CODE"].ToString()));

					if (sessionMan.userMan.talkEndMailRxFlag.ToString().Equals(dr["CODE"].ToString())) {
						lstTalkEndMailRxFlag.SelectedIndex = iIdx;
					}
					iIdx++;
				}

				iIdx = 0;
				ds = oCodeDtl.GetList(ViCommConst.CODE_TYPE_BLOG_MAIL_RX_TYPE);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstBlogMailRxFlag.Items.Add(new MobileListItem(dr["CODE_NM"].ToString(), dr["CODE"].ToString()));
					if (sessionMan.userMan.characterEx.blogMailRxType.Equals(dr["CODE"].ToString())) {
						lstBlogMailRxFlag.SelectedIndex = iIdx;
					}
					iIdx++;
				}
			}

			this.lblCastMailRxType.Text = DisplayWordUtil.ReplaceSetupRxMailCastMailLabel(this.lblCastMailRxType.Text);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	virtual protected void DirectSetup(string pCastMailRxType, string pInfoMailRxType, int pClearMailAddrNgFlag, string pNextDoc) {
		if (string.IsNullOrEmpty(pCastMailRxType)) {
			pCastMailRxType = sessionMan.userMan.castMailRxType;
		}
		if (string.IsNullOrEmpty(pInfoMailRxType)) {
			pInfoMailRxType = sessionMan.userMan.InfoMailRxType;
		}
		if (string.IsNullOrEmpty(pNextDoc)) {
			pNextDoc = ViCommConst.SCR_MAN_SETUP_RX_MAIL_LIGHT_COMPLETE;
		}


		sessionMan.userMan.SetupRxMail(
			sessionMan.site.siteCd,
			sessionMan.userMan.userSeq,
			pCastMailRxType,
			pInfoMailRxType,
			sessionMan.userMan.talkEndMailRxFlag,
			sessionMan.userMan.loginMailRxFlag1 == ViCommConst.FLAG_ON,
			sessionMan.userMan.loginMailRxFlag2 == ViCommConst.FLAG_ON,
			sessionMan.userMan.mobileMailRxStartTime,
			sessionMan.userMan.mobileMailRxEndTime,
			pClearMailAddrNgFlag
		);

		RedirectToDisplayDoc(pNextDoc);
	}
	virtual protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iTalkEndMailRxFlag = 0;
		int.TryParse(lstTalkEndMailRxFlag.Items[lstTalkEndMailRxFlag.SelectedIndex].Value,out iTalkEndMailRxFlag);
		sessionMan.userMan.SetupRxMail(
			sessionMan.site.siteCd,
			sessionMan.userMan.userSeq,
			lstCastMailRxType.Items[lstCastMailRxType.SelectedIndex].Value,
			lstInfoMailRxType.Items[lstInfoMailRxType.SelectedIndex].Value,
			iTalkEndMailRxFlag,
			(chkLoginMailRxFlag1.SelectedIndex == 0),
			(chkLoginMailRxFlag2.SelectedIndex == 0),
			lstMobileMailRxStartTime.Items[lstMobileMailRxStartTime.SelectedIndex].Value,
			lstMobileMailRxEndTime.Items[lstMobileMailRxEndTime.SelectedIndex].Value,
			ViCommConst.FLAG_OFF
		);

		using (UserManCharacterEx oUserManCharacterEx = new UserManCharacterEx()) {
			oUserManCharacterEx.SetupRxMailEx(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				lstBlogMailRxFlag.Items[lstBlogMailRxFlag.SelectedIndex].Value,
				(chkWaitMailRxFlag.SelectedIndex == 0) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF,
				sessionMan.userMan.tweetCommentMailRxFlag
			);
		}

		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_SETUP_MAIL_COMPLITE));
	}
}
