/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �j�����PC�o�^��t
--	Progaram ID		: ApplyRegistUserPC
--
--  Creation Date	: 2014.09.25
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ApplyRegistUserPC:MobileManPageBase {
	private string sTempRegistId;
	private string sUtn;
	private string sIModeId;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sTempRegistId = iBridUtil.GetStringValue(Request.QueryString["trid"]);

			if (string.IsNullOrEmpty(sTempRegistId)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_CAST_ERROR);
				return;
			}

			sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
			sIModeId = Mobile.GetiModeId(sessionMan.carrier,Request);

			if (!CheckUtn()) {
				return;
			}

			string sResult;

			using (TempRegist oTempRegist = new TempRegist()) {
				oTempRegist.ApplyRegistUserPC(
					sTempRegistId,
					sessionMan.carrier,
					sUtn,
					sIModeId,
					Request.UserHostAddress,
					sessionMan.mobileUserAgent,
					out sResult
				);
			}

			if (sResult.Equals(PwViCommConst.ApplyRegistUserPCResult.RESULT_OK)) {
				sessionMan.userMan.tempRegistId = sTempRegistId;
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("RegistAddOnInfo.aspx?utn={0}&id={1}",sUtn,sIModeId)));
				return;
			} else if (sResult.Equals(PwViCommConst.ApplyRegistCastPCResult.RESULT_DONE)) {
				RedirectToDisplayDoc(PwViCommConst.SCR_REGIST_DONE_USER_BY_PC);
				return;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
				return;
			}
		}
	}

	private bool CheckUtn() {
		bool bOk = true;
		sessionMan.errorMessage = string.Empty;

		if (Mobile.IsFeaturePhone(sessionMan.carrier)) {
			if (sessionMan.getTermIdFlag) {
				if (sUtn.Equals(string.Empty)) {
					sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			} else {
				if (sIModeId.Equals(string.Empty)) {
					sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			}

			if (bOk) {
				int iUtnExistFlag;

				using (User oUser = new User()) {
					oUser.CheckCastUtnExist(sUtn,sIModeId,out iUtnExistFlag);
				}

				if (iUtnExistFlag.Equals(ViCommConst.FLAG_ON)) {
					sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST);
					bOk = false;
				}
			}
		}

		return bOk;
	}
}
