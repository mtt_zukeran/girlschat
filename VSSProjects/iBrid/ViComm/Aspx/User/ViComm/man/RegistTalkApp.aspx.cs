/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ʘb�A�v���o�^
--	Progaram ID		: RegistTalkApp
--
--  Creation Date	: 2011.12.14
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Configuration;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_RegistTalkApp:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sErrorMessage = string.Empty;
		string sOtherRegistKey = string.Empty;

		lblErrorMessage.Text = string.Empty;

		sOtherRegistKey = sessionMan.userMan.crosmileRegistKey;
		if (sessionMan.RegistCrosmileSipUri(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAN,sessionMan.userMan.tel,sessionMan.userMan.telAttestedFlag,ref sOtherRegistKey)) {

			String sUrl = sessionObj.GetNavigateUrl("UserTop.aspx");
			String sPort = this.Request.Url.Port == 80 ? string.Empty : string.Format(":{0}",this.Request.Url.Port);
			sUrl = string.Format("{0}://{1}{2}{3}?activated_talk_app=1",this.Request.Url.Scheme.ToString(),this.Request.Url.Host,sPort,sUrl);


			sessionMan.userMan.crosmileRegistKey = sOtherRegistKey;
			
			string url = string.Format("{0}/UserDirectRegister/?otherRegistKey={1}&goto={2}",iBridUtil.GetStringValue(ConfigurationManager.AppSettings["xsmIntent"]),sOtherRegistKey,Server.UrlEncode(sUrl));
			if(sessionMan.carrier.Equals(ViCommConst.ANDROID)){
				lblScript.Text="<script type=\"text/javascript\">location.href='" + url + "'; </script>";
			}else{
				Response.Redirect(url);
			}
		} else {
			lblErrorMessage.Text = sErrorMessage;
		}
	}
}
