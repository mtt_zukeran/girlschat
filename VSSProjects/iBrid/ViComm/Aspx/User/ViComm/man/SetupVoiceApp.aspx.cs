/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 音声通話アプリ利用設定
--	Progaram ID		: SetupVoiceApp
--  Creation Date	: 2015.03.24
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_SetupVoiceApp:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sSetupFlag = iBridUtil.GetStringValue(Request.QueryString["setup"]);
			string sBackUrl = HttpUtility.UrlDecode(iBridUtil.GetStringValue(Request.QueryString["backurl"]));

			if (!string.IsNullOrEmpty(sSetupFlag)) {
				this.SetupVoiceapp(sSetupFlag.Equals(ViCommConst.FLAG_ON_STR),sBackUrl);
			} else {
				pnlInput.Visible = true;
				pnlError.Visible = false;

				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
					chkUseVoiceapp.Items[0].Text = "音声通話アプリを利用する";
				}

				if (sessionMan.userMan.useVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
					chkUseVoiceapp.Items[0].Selected = true;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		this.SetupVoiceapp(chkUseVoiceapp.Items[0].Selected,"");
	}

	private void SetupVoiceapp(bool pSetupFlag,string pBackUrl) {
		if (!pSetupFlag) {
			if (sessionMan.userMan.characterOnlineStatus.Equals(ViCommConst.USER_WAITING)) {
				if (sessionMan.userMan.manPfWaitingType.Equals(ViCommConst.WAIT_TYPE_VOICE) || sessionMan.userMan.manPfWaitingType.Equals(ViCommConst.WAIT_TYPE_BOTH)) {
					if (string.IsNullOrEmpty(sessionMan.userMan.tel)) {
						RedirectToDisplayDoc(ViCommConst.ERR_FRAME_NO_TEL);
						return;
					}
				}
			}
		}

		string sResult = sessionMan.userMan.SetupVoiceapp(
			sessionMan.site.siteCd,
			sessionMan.userMan.userSeq,
			(pSetupFlag) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF
		);

		if (sResult.Equals("0")) {
			if (!string.IsNullOrEmpty(pBackUrl)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(pBackUrl));
				return;
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("SetupVoiceAppComplete.aspx"));
				return;
			}
		} else {
			pnlInput.Visible = false;
			pnlError.Visible = true;
		}
	}
}
