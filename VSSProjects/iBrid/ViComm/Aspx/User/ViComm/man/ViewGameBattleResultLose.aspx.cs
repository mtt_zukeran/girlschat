/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްс@�ʏ����ٔs�k
--	Progaram ID		: ViewGameBattleResultLose
--
--  Creation Date	: 2011.08.30
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Web;

public partial class ViComm_man_ViewGameBattleResultLose:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		this.checkRecentBattleLog();

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_BATTLE_RESULT_LOSE,ActiveForm)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		} else {
			if (!string.IsNullOrEmpty(this.Request.Params["cmdNext"])) {
				string sBattleLogSeq = this.Request.Params["NEXTLINK"].ToString();
				string sResult = string.Empty;

				this.SupportRequest(sBattleLogSeq,out sResult);

				if(sResult.Equals(PwViCommConst.GameSupportRequestResult.RESULT_OK)) {
					string sRedirectUrl = string.Format("ListGameSupportRequest.aspx?battle_log_seq={0}",sBattleLogSeq);
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,sRedirectUrl));
				} else {
					RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
				}
			}
		}
	}

	private void checkRecentBattleLog() {
		BattleLogSeekCondition oCondition = new BattleLogSeekCondition(this.Request.QueryString);
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.UserSeq = this.sessionMan.userMan.userSeq;
		oCondition.UserCharNo = this.sessionMan.userMan.userCharNo;
		oCondition.DefenceWinFlag = ViCommConst.FLAG_ON_STR;

		using (BattleLog oBattleLog = new BattleLog()) {
			if (!oBattleLog.CheckRecentBattleLog(oCondition)) {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}
	
	private void SupportRequest(string sBattleLogSeq,out string sResult) {
		string sSiteCd = this.sessionMan.site.siteCd;
		string sUserSeq = this.sessionMan.userMan.userSeq;
		string sUserCharNo = this.sessionMan.userMan.userCharNo;
		string sSexCd = this.sessionMan.sexCd;

		using(SupportRequest oSupportRequest = new SupportRequest()) {
			oSupportRequest.ExecuteSupportRequest(sSiteCd,sUserSeq,sUserCharNo,sSexCd,sBattleLogSeq,out sResult);
		}
	}
}
