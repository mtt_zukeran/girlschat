/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入り追加
--	Progaram ID		: RegistFavorit
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistFavorit:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		string sPartnerUserSeq = string.Empty;
		string sPartnerUserCharNo = string.Empty;
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
		string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
		int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

		DataRow dr;
		if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo,out dr)) {
			if (!iBridUtil.GetStringValue(dr["USER_STATUS"]).Equals(ViCommConst.USER_WOMAN_NORMAL) ||
				!iBridUtil.GetStringValue(dr["NA_FLAG"]).Equals(ViCommConst.NaFlag.OK.ToString())) {
				RedirectToDisplayDoc(ViCommConst.ERR_ACCESS_BANNED_CAST);
			}

			sPartnerUserSeq = dr["USER_SEQ"].ToString();
			sPartnerUserCharNo = dr["USER_CHAR_NO"].ToString();
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				}
				else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			using (Favorit oFavorit = new Favorit()) {
				oFavorit.FavoritMainte(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					sessionMan.userMan.userCharNo,
					sPartnerUserSeq,
					sPartnerUserCharNo,
					string.Empty,
					ViCommConst.FLAG_OFF,
					ViCommConst.FLAG_OFF);
			}

			SetSelectBox(string.Empty);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e,sPartnerUserSeq,sPartnerUserCharNo);
			}
		}
	}

	private void SetSelectBox(string sFavoritGroupSeq) {
		int iNullUserCount = 0;
		string sListText = string.Empty;

		lstFavoritGroup.Items.Clear();

		using (Favorit oFavorit = new Favorit()) {
			iNullUserCount = oFavorit.GetSelfFavoritCount(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				ViCommConst.MAN
			);
		}

		DataSet oDataSet = GetFavoritGroup();
		Regex regex2 = new Regex(@"(\$x.{4};|&#\d+;)",RegexOptions.Compiled);

		foreach (DataRow dr in oDataSet.Tables[0].Rows) {
			iNullUserCount = iNullUserCount - int.Parse(dr["USER_COUNT"].ToString());
			sListText = string.Format("{0}({1})",regex2.Replace(iBridUtil.GetStringValue(dr["FAVORIT_GROUP_NM"]),string.Empty),iBridUtil.GetStringValue(dr["USER_COUNT"]));
			lstFavoritGroup.Items.Add(new MobileListItem(sListText,iBridUtil.GetStringValue(dr["FAVORIT_GROUP_SEQ"])));
		}

		lstFavoritGroup.Items.Insert(0,new MobileListItem(string.Format("ｸﾞﾙｰﾌﾟ未設定({0})",iNullUserCount.ToString()),string.Empty));

		foreach (MobileListItem item in lstFavoritGroup.Items) {
			if (item.Value.Equals(sFavoritGroupSeq)) {
				item.Selected = true;
			}
		}
	}

	private DataSet GetFavoritGroup() {
		DataSet oDataSet;
		FavoritGroupSeekCondition oCondition = new FavoritGroupSeekCondition();
		oCondition.SiteCd = sessionMan.site.siteCd;
		oCondition.UserSeq = sessionMan.userMan.userSeq;
		oCondition.UserCharNo = sessionMan.userMan.userCharNo;
		oCondition.SexCd = ViCommConst.MAN;

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			oDataSet = oFavoritGroup.GetPageCollectionWithUserCount(oCondition,1,99);
		}

		return oDataSet;
	}

	protected void cmdSubmit_Click(object sender,EventArgs e,string pPartnerUserSeq,string pPartnerUserCharNo) {
		string sResult;
		string[] sPartnerUserSeq = new string[] { pPartnerUserSeq };
		string[] sPartnerUserCharNo = new string[] { pPartnerUserCharNo };

		using (FavoritGroup oFavoritGroup = new FavoritGroup()) {
			sResult = oFavoritGroup.SetFavoritGroup(
				sessionMan.userMan.siteCd,
				sessionMan.userMan.userSeq,
				sessionMan.userMan.userCharNo,
				sPartnerUserSeq,
				sPartnerUserCharNo,
				lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_OK)) {
			string sFavoritGroupNm = lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Text;
			sFavoritGroupNm = System.Text.RegularExpressions.Regex.Replace(sFavoritGroupNm,"[(][0-9]+[)]","");
			lblDone.Visible = true;
			lblDone.Text = string.Format("{0}に変更しました。",sFavoritGroupNm);
			SetSelectBox(lstFavoritGroup.Items[lstFavoritGroup.SelectedIndex].Value);
		} else {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}
	}
}
