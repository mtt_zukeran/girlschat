/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �o�^�t�H�[��
--	Progaram ID		: RegistForm
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Net;
using System.IO;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_RegistForm:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			ParseHTML oParseMan = sessionMan.parseContainer;
			oParseMan.parseUser.postAction = ViCommConst.POST_ACT_UTN;

			if (sessionMan.carrier.Equals(ViCommConst.DOCOMO) && sessionMan.getTermIdFlag) {
				oParseMan.parseUser.postAction = ViCommConst.POST_ACT_BOTH_ID;
			}

			sessionMan.userMan.tempRegistId = Request.QueryString["regid"];

			string sFlag;
			using (ManageCompany oCompany = new ManageCompany()) {
				oCompany.GetValue("AUTO_GET_TEL_INFO_FLAG",out sFlag);
			}

			if (!IsAvailableService(ViCommConst.RELEASE_SP_TEL_NOT_ABSOLUTE,2)) {
				if (sessionMan.carrier.Equals(ViCommConst.ANDROID) || sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
					sFlag = "0";
				}
			}

			if (sFlag.Equals("0")) {
				lblTel.Visible = true;
				txtTel.Visible = true;
			} else {
				lblTel.Visible = false;
				txtTel.Visible = false;
			}

			using (TempRegist oTemp = new TempRegist()) {
				if ((!oTemp.GetOne(sessionMan.userMan.tempRegistId)) || (!oTemp.siteCd.Equals(sessionMan.site.siteCd))) {
					RedirectToMobilePage(sessionMan.GetNavigateErrorUrl(Session.SessionID,"MobileError.aspx?mode=user&errtype=" + ViCommConst.USER_INFO_ILLIGAL_ACCESS));
					return;
				} else {
					sessionMan.userMan.registCastLoginId = oTemp.castLoginId;
					sessionMan.userMan.registCastHandleNm = oTemp.castHandleNm;
					sessionMan.userMan.emailAddr = oTemp.emailAddr;
					sessionMan.affiliateCompany = oTemp.affiliaterCd;
					sessionMan.affiliaterCd = oTemp.registAffiliateCd;
				}

				if (oTemp.mobileCarrierCd.Equals(ViCommConst.CARRIER_OTHERS)) {
					oTemp.UpdateCarrier(sessionMan.userMan.tempRegistId,sessionMan.carrier);
				}

				int i = 0;
				sessionMan.userMan.GetAttrType(sessionMan.site.siteCd);

				for (i = 0;i < sessionMan.userMan.attrList.Count;i++) {
					SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i + 1)) as SelectionList;
					iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i + 1)) as iBMobileTextBox;
					iBMobileLabel lblUserManItem = (iBMobileLabel)frmMain.FindControl(string.Format("lblUserManItem{0}",i + 1)) as iBMobileLabel;
					TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",i + 1)) as TextArea;

					lblUserManItem.Text = sessionMan.userMan.attrList[i].attrTypeNm;
					if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
						if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
							txtAreaUserManItem.Visible = true;
							txtUserManItem.Visible = false;
							txtAreaUserManItem.Rows = int.Parse(sessionMan.userMan.attrList[i].rowCount);
						} else {
							txtAreaUserManItem.Visible = false;
							txtUserManItem.Visible = true;
						}
						lstUserManItem.Visible = false;
					} else {
						txtAreaUserManItem.Visible = false;
						txtUserManItem.Visible = false;
						lstUserManItem.Visible = true;
						using (UserManAttrTypeValue oAttrTypeValue = new UserManAttrTypeValue()) {
							DataSet ds = oAttrTypeValue.GetList(sessionMan.site.siteCd,sessionMan.userMan.attrList[i].attrTypeSeq);
							foreach (DataRow dr in ds.Tables[0].Rows) {
								lstUserManItem.Items.Add(new MobileListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
							}
						}
						if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_RADIO) {
							lstUserManItem.SelectType = System.Web.UI.MobileControls.ListSelectType.Radio;
							if (lstUserManItem.SelectedIndex == -1) {
								lstUserManItem.SelectedIndex = 0;
							}
						}
					}
				}
				for (;i < ViComm.ViCommConst.MAX_ATTR_COUNT;i++) {
					Panel pnlUserManItem = (Panel)frmMain.FindControl(string.Format("pnlUserManItem{0}",i + 1)) as Panel;
					pnlUserManItem.Visible = false;
				}
			}

			CreateList();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}


	protected void cmdSubmit_Click(object sender,EventArgs e) {
		bool bOk = true;
		string sUtn = string.Empty,sIMode = string.Empty;
		string sUserSeq;
		string sLoginId;
		string sResult;
		string sBirthday = string.Empty;

		if (sessionMan.ngWord == null) {
			sessionMan.ngWord = new NGWord(sessionMan.site.siteCd);
		}
		string sNGWord = string.Empty;

		lblErrorMessage.Text = string.Empty;

		if (txtPassword.Visible) {
			if (txtPassword.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD);
			} else {
				if (!SysPrograms.Expression(@"^[a-zA-Z0-9]{4,8}$",txtPassword.Text)) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_PASSWORD_NG);
				}
			}
		}

		if (txtHandelNm.Visible) {
			if (txtHandelNm.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HANDLE_NM);
			}
			if (sessionMan.ngWord.VaidateDoc(txtHandelNm.Text,out sNGWord) == false) {
				bOk = false;
				lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
			}
		}

		if (txtTel.Visible) {
			if (txtTel.Text.Equals("")) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
			} else {
				if (!sessionMan.carrier.Equals(ViCommConst.CARRIER_OTHERS)) {
					if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTel.Text)) {
						bOk = false;
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
					}
				}
			}
		}

		if (txtYear.Visible) {
			if ((txtYear.Text.Equals("")) || (txtMonth.Text.Equals("")) || (txtDay.Text.Equals(""))) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY);
			}

			if (bOk) {
				string sDate = txtYear.Text + "/" + txtMonth.Text + "/" + txtDay.Text;
				int iAge = ViCommPrograms.Age(sDate);
				if (iAge < 18) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_UNDER_18);
				} else if (iAge > 99) {
					bOk = false;
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_BIRTHDAY_NG);
				}
			}

			if (chkRlue.SelectedIndex == -1) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HAVE_TO_CHECK_RULE);
			}

			if (chkMail.SelectedIndex == -1) {
				bOk = false;
				lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_HAVE_TO_CHECK_MAIL);
			}

			sUtn = Mobile.GetUtn(sessionMan.carrier,Request);
			sIMode = Mobile.GetiModeId(sessionMan.carrier,Request);

			if (sessionMan.getTermIdFlag) {
				if (sUtn.Equals(string.Empty) || sIMode.Equals(string.Empty)) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			} else {
				if (sIMode.Equals(string.Empty)) {
					lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NO_TERM_ID);
					bOk = false;
				}
			}
		}

		bOk = bOk & CheckList();

		if (bOk == false) {
			return;
		} else {

			if (txtYear.Visible) {
				sBirthday = GetBirthDay();
			}

			string[] pAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] pAttrRowId = new string[ViCommConst.MAX_ATTR_COUNT];

			bool bIsVisible;
			for (int i = 0;i < sessionMan.userMan.attrList.Count;i++) {
				bIsVisible = false;
				if (sessionMan.userMan.attrList[i].inputType == ViCommConst.INPUT_TYPE_TEXT) {
					if (int.Parse(sessionMan.userMan.attrList[i].rowCount) > 1) {
						TextArea txtAreaUserManItem = (TextArea)frmMain.FindControl(string.Format("txtAreaUserManItem{0}",i + 1)) as TextArea;
						bIsVisible = txtAreaUserManItem.Visible;
						pAttrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtAreaUserManItem.Text));
						if (pAttrInputValue[i].Length > 300) {
							pAttrInputValue[i] = SysPrograms.Substring(pAttrInputValue[i],300);
						}
					} else {
						iBMobileTextBox txtUserManItem = (iBMobileTextBox)frmMain.FindControl(string.Format("txtUserManItem{0}",i + 1)) as iBMobileTextBox;
						bIsVisible = txtUserManItem.Visible;
						pAttrInputValue[i] = HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtUserManItem.Text));
					}
					if (sessionMan.userMan.attrList[i].registInputFlag && pAttrInputValue[i].Equals(string.Empty)) {
						lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_INPUT_REQ_FILED),sessionMan.userMan.attrList[i].attrTypeNm);
						bOk = false;
					}
					if (sessionMan.ngWord.VaidateDoc(pAttrInputValue[i],out sNGWord) == false) {
						bOk = false;
						lblErrorMessage.Text += string.Format(GetErrorMessage(ViCommConst.ERR_STATUS_NG_WORD),sNGWord);
					}
				} else {
					SelectionList lstUserManItem = (SelectionList)frmMain.FindControl(string.Format("lstUserManItem{0}",i + 1)) as SelectionList;
					bIsVisible = lstUserManItem.Visible;
					if (lstUserManItem.Selection != null) {
						pAttrSeq[i] = lstUserManItem.Selection.Value;
					}
				}
				pAttrTypeSeq[i] = bIsVisible ? sessionMan.userMan.attrList[i].attrTypeSeq : string.Empty;
				pAttrRowId[i] = "";
			}

			if (bOk == false) {
				return;
			}

			sessionMan.userMan.RegistUser(
				sessionMan.userMan.tempRegistId,
				txtPassword.Text,
				txtTel.Text,
				HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtHandelNm.Text)),
				sBirthday,
				sessionMan.mobileUserAgent,
				sUtn,
				sIMode,
				pAttrTypeSeq,
				pAttrSeq,
				pAttrInputValue,
				pAttrRowId,
				sessionMan.userMan.attrList.Count,
				out sUserSeq,
				out sLoginId,
				out sResult
			);

			if (sResult.Equals("0")) {
				sessionMan.userMan.userSeq = sUserSeq;
				sessionMan.userMan.loginId = sLoginId;
				sessionMan.userMan.loginPassword = txtPassword.Text;
				if (sessionMan.userMan.registCastLoginId.Equals("")) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_REGIST_MAN_COMPLITE));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.SCR_REGIST_MAN_BY_CAST_COMPLITE));
				}
			} else {
				switch (int.Parse(sResult)) {
					case ViCommConst.REG_USER_RST_TEL_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_MODIFIED_TEL_NO_ALREADY_EXIST);
						break;

					default:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_REGIST_NOT_DEFINED);
						break;

					case ViCommConst.REG_USER_RST_NOT_FOUND_INFO:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_NOT_EXIST_REGIST_APPLY);
						break;

					case ViCommConst.REG_USER_RST_UNT_EXIST:
						lblErrorMessage.Text += GetErrorMessage(ViCommConst.ERR_STATUS_TERM_ID_ALREADY_EXIST_BY_SET);
						break;
				}
			}
		}
	}

	protected virtual void CreateList() { }

	protected virtual bool CheckList() {
		return true;
	}

	protected virtual string GetBirthDay() {
		DateTime dtBiathday = new DateTime(int.Parse(txtYear.Text),int.Parse(txtMonth.Text),int.Parse(txtDay.Text));
		return dtBiathday.ToString("yyyy/MM/dd");
	}
}
