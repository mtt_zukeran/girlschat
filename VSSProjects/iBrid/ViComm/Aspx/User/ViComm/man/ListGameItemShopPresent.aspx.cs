/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ｼｮｯﾌﾟ贈り物一覧
--	Progaram ID		: ListGameItemShopPresent
--
--  Creation Date	: 2011.09.15
--  Creater			: PW A.Taba
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListGameItemShopPresent:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		string sPartnerUserSeq = iBridUtil.GetStringValue(Request.QueryString["partner_user_seq"]);
		string sPartnerUserCharNo = iBridUtil.GetStringValue(Request.QueryString["partner_user_char_no"]);

		if (sPartnerUserSeq.Equals(string.Empty) || sPartnerUserCharNo.Equals(string.Empty)) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		this.CheckGameRefusedByPartner(sPartnerUserSeq,sPartnerUserCharNo);

		this.Response.Filter = sessionMan.InitScreen(this.Response.Filter,this.frmMain,this.Request,this.ViewState);
		sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_ITEM_SHOP_PRESENT,ActiveForm);
	}
}
