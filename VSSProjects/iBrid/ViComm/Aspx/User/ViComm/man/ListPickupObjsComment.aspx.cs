/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ピックアップオブジェ コメント一覧
--	Progaram ID		: ListPickupObjsComment
--
--  Creation Date	: 2013.05.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;


public partial class ViComm_man_ListPickupObjsComment:MobileManPageBase {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.errorMessage = string.Empty;
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_PICKUP_OBJS_COMMENT,this.ActiveForm);
		}
	}
}