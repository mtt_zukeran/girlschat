<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BidProdAuction.aspx.cs" Inherits="ViComm_man_BidProdAuction" %>
<%@ Register TagPrefix="ibrid"		Namespace="MobileLib"						Assembly="MobileLib"%>
<%@ Register TagPrefix="mobile"		Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		$PGM_HTML01;
	    <ibrid:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></ibrid:iBMobileLabel>
		$PGM_HTML02;
		<ibrid:iBMobileTextBox ID="txtBidAmt" runat="server" MaxLength="11" Size="15" BreakAfter="false"></ibrid:iBMobileTextBox>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>