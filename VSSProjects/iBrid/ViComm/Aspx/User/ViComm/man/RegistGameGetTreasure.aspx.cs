/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｿｰｼｬﾙｹﾞｰﾑ・ﾅﾝﾊﾟ成功時のお宝付与
--	Progaram ID		: RegistGameGetTreasure.aspx
--
--  Creation Date	: 2012.05.09
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistGameGetTreasure:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sStageSeq = iBridUtil.GetStringValue(Request.QueryString["stage_seq"]);
			string sTownSeq = iBridUtil.GetStringValue(Request.QueryString["town_seq"]);
			string sTownFirstClearFlag = iBridUtil.GetStringValue(Request.QueryString["town_first_clear_flag"]);
			string sGetTreasureLogSeq = iBridUtil.GetStringValue(Request.QueryString["get_treasure_log_seq"]);

			if(!string.IsNullOrEmpty(this.sessionMan.userMan.gameCharacter.tutorialStatus)) {
				sGetTreasureLogSeq = this.GetTutorialGetManTreasureLogSeq();
			}

			if (!string.IsNullOrEmpty(sStageSeq) && !string.IsNullOrEmpty(sTownSeq) && !string.IsNullOrEmpty(sGetTreasureLogSeq)) {
				int iGetCompCountItemFlag;			//男性用お宝規定回数コンプ時獲得アイテム取得フラグ
				string[] sGetCompCountItemSeq;		//男性用お宝規定回数コンプ時獲得アイテムSEQ
				string[] sGetCompCountItemCount;	//男性用お宝規定回数コンプ時獲得アイテム付与数
				int iTreasureCompleteFlag = ViCommConst.FLAG_OFF;	//お宝をコンプリートしたら1(既にコンプリート済みの場合は0)
				int iBonusGetFlag = ViCommConst.FLAG_OFF;
				string sGetTreasureResult = PwViCommConst.GameGetTreasureResult.RESULT_OK;

				using (Town oTown = new Town()) {
					oTown.GetTreasure(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						sessionMan.userMan.userCharNo,
						sGetTreasureLogSeq,
						out iTreasureCompleteFlag,
						out iBonusGetFlag,
						out iGetCompCountItemFlag,
						out sGetCompCountItemSeq,
						out sGetCompCountItemCount,
						out sGetTreasureResult
					);

					if (!sGetTreasureResult.Equals(PwViCommConst.GameGetTreasureResult.RESULT_OK)) {
						RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
					}
				}

				this.CheckQuestClear();

				if (!string.IsNullOrEmpty(sessionMan.userMan.gameCharacter.tutorialStatus)) {
					int iNextTutorialStatus = int.Parse(sessionMan.userMan.gameCharacter.tutorialStatus) + 1;

					using (GameCharacter oGameCharacter = new GameCharacter()) {
						oGameCharacter.SetupTutorialStatus(
							sessionMan.userMan.siteCd,
							sessionMan.userMan.userSeq,
							sessionMan.userMan.userCharNo,
							iNextTutorialStatus.ToString()
						);
					}
				}

				string sPartnerUserSeq = string.Empty;
				string sPartnerUserCharNo = string.Empty;

				this.GetPartnerByGetTreasureLogSeq(sGetTreasureLogSeq,out sPartnerUserSeq,out sPartnerUserCharNo);

				UrlBuilder oUrlBuilder = new UrlBuilder("ViewGameFlashGetManTreasure.aspx");
				oUrlBuilder.AddParameter("stage_seq",sStageSeq);
				oUrlBuilder.AddParameter("town_seq",sTownSeq);
				oUrlBuilder.AddParameter("partner_user_seq",sPartnerUserSeq);
				oUrlBuilder.AddParameter("partner_user_char_no",sPartnerUserCharNo);
				oUrlBuilder.AddParameter("town_first_clear_flag",sTownFirstClearFlag);
				oUrlBuilder.AddParameter("get_treasure_log_seq",sGetTreasureLogSeq);
				oUrlBuilder.AddParameter("treasure_complete_flag",iTreasureCompleteFlag.ToString());
				oUrlBuilder.AddParameter("get_treasure_result",sGetTreasureResult);

				if (iTreasureCompleteFlag == ViCommConst.FLAG_ON) {
					oUrlBuilder.AddParameter("get_comp_count_item_seq",string.Join("_",sGetCompCountItemSeq));
					oUrlBuilder.AddParameter("get_comp_count_item_count",string.Join("_",sGetCompCountItemCount));
					oUrlBuilder.AddParameter("get_comp_count_item_rec_count",sGetCompCountItemSeq.Length.ToString());
				}

				RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
			} else {
				RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
			}
		}
	}

	private void GetPartnerByGetTreasureLogSeq(string pGetTreasureLogSeq,out string pPartnerUserSeq,out string pPartnerUserCharNo) {
		pPartnerUserSeq = string.Empty;
		pPartnerUserCharNo = string.Empty;
		DataSet oDataSet;

		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oDataSet = oGetManTreasureLog.GetOne(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				pGetTreasureLogSeq
			);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			pPartnerUserSeq = oDataSet.Tables[0].Rows[0]["USER_SEQ"].ToString();
			pPartnerUserCharNo = oDataSet.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
		}
	}
	
	private string GetTutorialGetManTreasureLogSeq() {
		string sValue = string.Empty;
		DataSet oDataSet = null;
		using (GetManTreasureLog oGetManTreasureLog = new GetManTreasureLog()) {
			oDataSet = oGetManTreasureLog.GetOneForTutorial(this.sessionMan.userMan.siteCd,this.sessionMan.userMan.userSeq,this.sessionMan.userMan.userCharNo);
		}
		
		if(oDataSet.Tables[0].Rows.Count == 0) {
			RedirectToGameDisplayDoc(PwViCommConst.SCR_MAN_GAME_ERROR);
		}
		
		return oDataSet.Tables[0].Rows[0]["GET_TREASURE_LOG_SEQ"].ToString();
	}

	private string GetGameTutorialTownSeq() {
		string sValue = string.Empty;

		TownSeekCondition oCondition = new TownSeekCondition();
		oCondition.SiteCd = this.sessionMan.site.siteCd;
		oCondition.SexCd = this.sessionMan.sexCd;

		using (Town oTown = new Town()) {
			sValue = oTown.GetGameTutorialTownSeq(oCondition);
		}

		return sValue;
	}

	private void CheckQuestClear() {
		string sQuestClearFlag;
		string sResult;

		using (Quest oQuest = new Quest()) {
			oQuest.CheckQuestClear(
				this.sessionMan.site.siteCd,
				this.sessionMan.userMan.userSeq,
				this.sessionMan.userMan.userCharNo,
				PwViCommConst.GameQuestTrialCategory.MISSION,
				PwViCommConst.GameQuestTrialCategoryDetail.MISSION,
				out sQuestClearFlag,
				out sResult,
				this.sessionMan.sexCd
			);
		}
	}
}
