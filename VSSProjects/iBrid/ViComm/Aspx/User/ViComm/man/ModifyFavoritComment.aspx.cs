/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お気に入りメモ編集
--	Progaram ID		: ModifyFavoritComment
--
--  Creation Date	: 2010.05.06
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ModifyFavoritComment:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));

			DataRow dr;
			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo,out dr)) {
				ViewState["CAST_USER_SEQ"] = dr["USER_SEQ"].ToString();
				ViewState["CAST_USER_CHAR_NO"] = dr["USER_CHAR_NO"].ToString();
				using (Favorit oFavorit = new Favorit()) {
					txtAreaFavoritComment.Text = oFavorit.GetFavoritComment(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,ViewState["CAST_USER_SEQ"].ToString(),ViewState["CAST_USER_CHAR_NO"].ToString());
				}
				pnlFoundDate.Visible = true;
				pnlNotFoundDate.Visible = false;
			} else {
				pnlFoundDate.Visible = false;
				pnlNotFoundDate.Visible = true;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);
		if (iMaxLength < 1) {
			iMaxLength = 300;
		}
		if (txtAreaFavoritComment.Text.Length > iMaxLength) {
			lblModifyComplate.Text = GetErrorMessage(ViCommConst.ERR_STATUS_LENGTH_OVER_FAV_MEMO);
		} else {
			using (Favorit oFavorit = new Favorit()) {
				oFavorit.FavoritMainte(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.MAIN_CHAR_NO,
					ViewState["CAST_USER_SEQ"].ToString(),
					ViewState["CAST_USER_CHAR_NO"].ToString(),
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtAreaFavoritComment.Text)),
					ViCommConst.FLAG_OFF,
					ViCommConst.FLAG_ON);
			}
			lblModifyComplate.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MODIFY_COMMENT_COMPLATE);
		}
	}
}
