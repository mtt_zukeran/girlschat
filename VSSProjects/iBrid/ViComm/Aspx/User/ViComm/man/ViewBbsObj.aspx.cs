/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 掲示板写真・動画詳細
--	Progaram ID		: ViewBbsObj
--
--  Creation Date	: 2010.09.09
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewBbsObj:MobileManPageBase {

	bool dupUsed;
	string movieSeq;
	string castSeq;
	string castCharNo;
	int chargePoint;
	int paymentAmt;

	virtual protected void Page_Load(object sender,EventArgs e) {

		if (sessionMan.logined == false) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
			return;
		}

		bool bDownload = false;
		string sAllBbs = iBridUtil.GetStringValue(Request.QueryString["allbbs"]);

		if (!IsPostBack) {
			castSeq = iBridUtil.GetStringValue(Request.QueryString["castseq"]);
			movieSeq = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
			castCharNo = ViCommConst.MAIN_CHAR_NO;

			if (!movieSeq.Equals(string.Empty)) {
				bDownload = Download();
			}
			if (bDownload == false) {
				Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
				ulong uSeekMode;
				if (ulong.TryParse(iBridUtil.GetStringValue(Request.QueryString["seekmode"]),out uSeekMode) == false) {
					uSeekMode = ViCommConst.INQUIRY_BBS_OBJ;
				}

				if (sessionMan.ControlList(Request,uSeekMode,ActiveForm)) {

					string sObjType = sessionMan.GetBbsObjValue("OBJ_KIND");
					string sLoginId;

					switch (sObjType) {
						case ViCommConst.BbsObjType.PIC:

							castSeq = sessionMan.GetBbsObjValue("USER_SEQ");
							castCharNo = sessionMan.GetBbsObjValue("USER_CHAR_NO");
							sLoginId = sessionMan.GetBbsObjValue("LOGIN_ID");
							string sPicSeq = sessionMan.GetBbsObjValue("PIC_SEQ");

							if (sessionMan.SetCastDataSetByLoginId(sLoginId,castCharNo,iRecNo)) {
								sessionMan.errorMessage = "";

								using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
									dupUsed = oObjUsedHistory.GetOne(
														sessionMan.userMan.siteCd,
														sessionMan.userMan.userSeq,
														ViCommConst.ATTACH_PIC_INDEX.ToString(),
														sPicSeq);
								}

								if (dupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
									bool bOk = sessionMan.CheckBbsPicBalance(castSeq,castCharNo,sPicSeq,out chargePoint,out paymentAmt);
									if (bOk == false) {
										RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_BBS_PIC));
									} else {
										using (WebUsedLog oLog = new WebUsedLog()) {
											oLog.CreateWebUsedReport(
													sessionMan.userMan.siteCd,
													sessionMan.userMan.userSeq,
													ViCommConst.CHARGE_BBS_PIC,
													chargePoint,
													castSeq,
													castCharNo,
													"",
													paymentAmt);
										}
										using (Access oAccess = new Access()) {
											oAccess.AccessPicPage(sessionMan.site.siteCd,castSeq,castCharNo,sPicSeq);
										}

										using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
											oObjUsedHistory.AccessBbsObj(
													sessionMan.userMan.siteCd,
													sessionMan.userMan.userSeq,
													ViCommConst.ATTACH_PIC_INDEX.ToString(),
													sPicSeq);
										}

										int iGetBbsBingoNoCompleteFlag = ViCommConst.FLAG_OFF;
										string sGetBbsBingoNoResult = string.Empty;

										using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
											oBbsBingoTerm.GetBbsBingoNo(
												sessionMan.site.siteCd,
												sessionMan.userMan.userSeq,
												sPicSeq,
												ViCommConst.FLAG_OFF,
												out iGetBbsBingoNoCompleteFlag,
												out sGetBbsBingoNoResult
											);
										}

										if (sGetBbsBingoNoResult.Equals(PwViCommConst.GetBbsBingoNoResult.RESULT_OK) && iGetBbsBingoNoCompleteFlag.Equals(ViCommConst.FLAG_ON)) {
											UrlBuilder oNextUrl = new UrlBuilder(ViCommPrograms.GetCurrentAspx(),sessionMan.requestQuery.QueryString);
											string sToUrl = string.Format("ViewFlashBingoComplete.aspx?next_url={0}",HttpUtility.UrlEncode(oNextUrl.ToString(),System.Text.Encoding.GetEncoding(932)));
											RedirectToMobilePage(sessionMan.GetNavigateUrl(sToUrl));
											return;
										}
									}
								}
							}
							break;

						case ViCommConst.BbsObjType.MOVIE:
							castSeq = sessionMan.GetBbsObjValue("USER_SEQ");
							castCharNo = sessionMan.GetBbsObjValue("USER_CHAR_NO");
							movieSeq = sessionMan.GetBbsObjValue("OBJ_SEQ");
							sLoginId = sessionMan.GetBbsObjValue("LOGIN_ID");
							CheckMovieBalance();
							sessionMan.SetCastDataSetByLoginId(sessionMan.GetBbsObjValue("LOGIN_ID"),castCharNo,iRecNo);
							break;

						case ViCommConst.BbsObjType.BBSDOC:
							sessionMan.SetCastDataSetByLoginId(sessionMan.GetBbsObjValue("LOGIN_ID"),castCharNo,iRecNo);
							// 閲覧数を更新
							using (CastBbs oCastBbs = new CastBbs()) {
								oCastBbs.UpdateReadingCount(sessionMan.site.siteCd,castSeq,castCharNo,sessionMan.GetBbsObjValue("BBS_SEQ"));
							}
							break;

						default:
							RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListBbsObj.aspx?allbbs={0}",sAllBbs)));
							break;
					}

				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,string.Format("ListBbsObj.aspx?allbbs={0}",sAllBbs)));
				}
			}
		}
	}

	private bool Download() {
		ParseHTML oParseHTML = sessionMan.parseContainer;

		bool bIsCheckOnly = iBridUtil.GetStringValue(Request.QueryString["check"]).Equals(ViCommConst.FLAG_ON_STR);
		string sFileNm,sFullPath,sLoginId;
		string sErrorCode = null;

		CheckMovieBalance();

		if (bIsCheckOnly)
			return false;

		long lSize = ((ParseViComm)oParseHTML.parseUser).GetMovie(ViCommConst.OPERATOR,movieSeq,false,out sFileNm,out sFullPath,out sLoginId,out sErrorCode);
		if (lSize == ViCommConst.GET_MOVIE_ERROR_VALUE) {
			return false;
		}
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (lSize > 0) {
				if (sessionMan.logined) {
					if (!MovieHelper.IsRangeRequest()) {
						if (dupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
							using (Access oAccess = new Access()) {
								oAccess.AccessMoviePage(sessionMan.site.siteCd,castSeq,castCharNo,movieSeq);
							}
							using (WebUsedLog oLog = new WebUsedLog()) {
								oLog.CreateWebUsedReport(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.CHARGE_BBS_MOVIE,
										chargePoint,
										castSeq,
										castCharNo,
										"",
										paymentAmt);
							}
							using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
								oObjUsedHistory.AccessBbsObj(
										sessionMan.userMan.siteCd,
										sessionMan.userMan.userSeq,
										ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
										movieSeq);
							}
						}
					}
				}

				MovieHelper.ResponseMovie(sLoginId,sFileNm,sFullPath,lSize,ViCommConst.OPERATOR);

				return true;
			} else {
				return false;
			}
		}
	}

	private void CheckMovieBalance() {
		if (!MovieHelper.IsRangeRequest()) {
			sessionMan.errorMessage = "";
			using (ObjUsedHistory oObjUsedHistory = new ObjUsedHistory()) {
				dupUsed = oObjUsedHistory.GetOne(
									sessionMan.userMan.siteCd,
									sessionMan.userMan.userSeq,
									ViCommConst.ATTACH_MOVIE_INDEX.ToString(),
									movieSeq);
			}

			if (dupUsed == false || sessionMan.site.bbsDupChargeFlag.Equals(1)) {
				if (sessionMan.CheckBbsMovieBalance(castSeq,castCharNo,movieSeq,out chargePoint,out paymentAmt) == false) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"DisplayDoc.aspx?doc=" + ViCommConst.ERR_FRAME_BBS_MOVIE));
				}
			}
		}
	}
}
