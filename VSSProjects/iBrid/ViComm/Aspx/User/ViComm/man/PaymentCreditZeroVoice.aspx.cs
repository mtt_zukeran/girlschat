/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ｸﾚｼﾞｯﾄ音声ガイダンス支払(ZEROｸﾚｼﾞｯﾄ)
--	Progaram ID		: PaymentCreditZeroVoice
--
--  Creation Date	: 2015.07.09
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.IO;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using iBridCommLib;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using MobileLib;
using ViComm;

public partial class ViComm_man_PaymentCreditZeroVoice:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (!string.IsNullOrEmpty(sessionMan.userMan.tel)) {
				txtTelNo.Text = sessionMan.userMan.tel;
				pnlInputTelNo.Visible = false;
			}
			
			if (sessionMan.userMan.billAmt == 0) {
				string sAmt = iBridUtil.GetStringValue(Request.QueryString["packamt"]);
				DataSet ds;
				ds = GetPackDataSet(ViCommConst.SETTLE_CREDIT_PACK,0);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					lstCreditPack.Items.Add(new MobileListItem(dr["REMARKS"].ToString(),dr["SALES_AMT"].ToString()));
					if (dr["SALES_AMT"].ToString().Equals(sAmt)) {
						lstCreditPack.SelectedIndex = lstCreditPack.Items.Count - 1;
						lstCreditPack.Visible = false;
						sessionMan.userMan.settleRequestAmt = int.Parse(dr["SALES_AMT"].ToString());
						sessionMan.userMan.settleRequestPoint = int.Parse(dr["EX_POINT"].ToString());
						sessionMan.userMan.settleServicePoint = int.Parse(dr["SERVICE_POINT"].ToString());
						break;
					}
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sSid = "";
		string sSalesAmt;
		
		sessionMan.errorMessage = string.Empty;

		if (pnlInputTelNo.Visible) {
			if (string.IsNullOrEmpty(txtTelNo.Text)) {
				sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO);
				return;
			} else {
				if (!SysPrograms.Expression(@"0(7|8|9)0\d{8}",txtTelNo.Text)) {
					sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_NG);
					return;
				} else {
					using (Man oMan = new Man()) {
						if (oMan.IsTelephoneRegistered(sessionMan.site.siteCd,txtTelNo.Text)) {
							sessionMan.errorMessage = GetErrorMessage(ViCommConst.ERR_STATUS_TEL_NO_ALREADY_EXIST);
							return;
						}
					}
				}
			}
		}

		if (sessionMan.userMan.billAmt == 0) {
			sSalesAmt = lstCreditPack.Items[lstCreditPack.SelectedIndex].Value;
			using (Pack oPack = new Pack())
			using (SettleLog oLog = new SettleLog()) {
				if (oPack.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CREDIT_PACK,sSalesAmt,sessionMan.userMan.userSeq)) {
					oLog.LogSettleRequest(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.SETTLE_CORP_ZERO,
						ViCommConst.SETTLE_CREDIT_PACK,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						int.Parse(sSalesAmt),
						oPack.exPoint,
						"",
						"",
						out sSid);
				}
			}
		} else {
			sSalesAmt = sessionMan.userMan.billAmt.ToString();
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleRequest(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.SETTLE_CORP_ZERO,
					ViCommConst.SETTLE_CREDIT,
					ViCommConst.SETTLE_STAT_CHARGE,
					sessionMan.userMan.billAmt,
					(int)Math.Floor((double)(sessionMan.userMan.billAmt / sessionMan.site.pointPrice / sessionMan.site.pointTax)),
					"",
					"",
					out sSid);
			}
		}

		string sSettleUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			string sSettleType = sessionMan.userMan.billAmt == 0 ? ViCommConst.SETTLE_CREDIT_PACK : ViCommConst.SETTLE_CREDIT;

			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,sSettleType)) {
				sSettleUrl = string.Format(oSiteSettle.subSettleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,txtTelNo.Text);
			}
		}
		
		string sAuthTelNo;

		if (TransVoice(sSettleUrl,out sAuthTelNo)) {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"PaymentCreditZeroVoiceAuth.aspx?authtelno=" + sAuthTelNo));
		} else {
			sessionMan.errorMessage = "通信に失敗しました。";
			return;
		}

	}

	public bool TransVoice(string pUrl,out string pAuthTelNo) {
		pAuthTelNo = string.Empty;
		try {
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(OnRemoteCertificateValidationCallback);

			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 60000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				
				Match oMatch = Regex.Match(sRes,@"03\d{8}");
				pAuthTelNo = oMatch.Value;
				
				return Regex.IsMatch(sRes,"(s|S)uccessOK");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransVoice",pUrl);
			return false;
		}
	}

	private bool OnRemoteCertificateValidationCallback(
	  Object sender,
	  X509Certificate certificate,
	  X509Chain chain,
	  SslPolicyErrors sslPolicyErrors
	) {
		return true;  // 「SSL証明書の使用は問題なし」と示す
	}
}
