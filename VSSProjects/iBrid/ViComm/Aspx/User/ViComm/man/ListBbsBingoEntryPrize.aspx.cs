/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ¨óderSÜàl¾Òê
--	Progaram ID		: ListBbsBongoEntryPrize
--  Creation Date	: 2013.11.06
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListBbsBingoEntryPrize:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_BBS_BINGO_ENTRY_PRIZE,this.ActiveForm);
		}
	}
}
