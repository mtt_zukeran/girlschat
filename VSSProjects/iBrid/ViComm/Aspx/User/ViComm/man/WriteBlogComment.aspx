<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WriteBlogComment.aspx.cs" Inherits="ViComm_man_WriteBlogComment" %>
<%@ Register TagPrefix="ibrid"		Namespace="MobileLib"						Assembly="MobileLib"%>
<%@ Register TagPrefix="mobile"		Namespace="System.Web.UI.MobileControls"	Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMain" runat="server">
		$PGM_HTML01;
		<ibrid:iBMobileLiteralText ID="tagCommentWriteHeader"		runat="server" Text="$PGM_HTML02;" />
		<tx:TextArea ID="txtComment" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="2" Columns="24" EmojiPaletteEnabled="true">
		</tx:TextArea>
		<ibrid:iBMobileLiteralText ID="tagCommentWriteFooter"	runat="server" Text="$PGM_HTML03;" />
		<ibrid:iBMobileLiteralText ID="tagCommentConfirmHeader"	runat="server" Text="$PGM_HTML04;" />
		<ibrid:iBMobileLabel ID="lblCommentDoc" runat="server" BreakAfter="true" ></ibrid:iBMobileLabel>
		<ibrid:iBMobileLiteralText ID="tagCommentConfirmFooter"	runat="server" Text="$PGM_HTML05;" />
		<ibrid:iBMobileLiteralText ID="tagCommentComplete"		runat="server" Text="$PGM_HTML06;" />
		$PGM_HTML09;
    </mobile:Form>
</body>
</html>
