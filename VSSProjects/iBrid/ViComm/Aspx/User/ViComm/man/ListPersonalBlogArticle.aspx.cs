/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �l�ʃu���O���e �ꗗ
--	Progaram ID		: ListPersonalBlogArticle
--
--  Creation Date	: 2011.04.11
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;

using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListPersonalBlogArticle : MobileBlogManBase {
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
		sessionMan.ControlList(Request, ViCommConst.INQUIRY_EXTENSION_BLOG_ARTICLE, ActiveForm);
	}
}
