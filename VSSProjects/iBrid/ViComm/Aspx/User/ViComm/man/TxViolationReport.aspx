﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TxViolationReport.aspx.cs" Inherits="ViComm_man_TxViolationReport" %>
<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		$PGM_HTML02;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<cc1:iBMobileLabel ID="lblTitle" runat="server">通報内容</cc1:iBMobileLabel>
		$PGM_HTML03;
		<mobile:SelectionList ID="lstViolationContents" Runat="server" SelectType="DropDown">
		</mobile:SelectionList>
		<cc1:iBMobileLabel ID="lblDoc" runat="server">詳細内容</cc1:iBMobileLabel>
		$PGM_HTML04;
		<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="6" Columns="24">
		</tx:TextArea>
		<br />
		$PGM_HTML06;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
