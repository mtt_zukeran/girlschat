﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャスト掲示板一覧
--	Progaram ID		: ListCastBbs
--
--  Creation Date	: 2010.04.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListCastBbs:MobileManPageBase {
	

	protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_BBS_DOC,ActiveForm);
		}
	}
}
