<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="ViComm_man_Profile" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="tx" TagName="TextArea" Src="TextArea.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server" >
		<cc1:iBMobileLiteralText ID="tag01" runat="server" />
		$PGM_HTML01;
		<mobile:Panel ID="pnlTxMail" runat="server">
			$PGM_HTML02;
			<cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
			$PGM_HTML03;
			<cc1:iBMobileLabel ID="lblTitle" runat="server" Text="件名：<font color=red>※全角15文字まで</font>"></cc1:iBMobileLabel>
			<cc1:iBMobileTextBox ID="txtTitle" runat="server" MaxLength="30" Size="24" EmojiPaletteEnabled="true"></cc1:iBMobileTextBox>
			<cc1:iBMobileLabel ID="lblDoc" runat="server">本文：</cc1:iBMobileLabel>
			<tx:TextArea ID="txtDoc" runat="server" Row="10" BreakBefore="false" BrerakAfter="true" Rows="9" Columns="24" EmojiPaletteEnabled="true"></tx:TextArea>
			$PGM_HTML04;
		</mobile:Panel>
		<mobile:Panel ID="pnlTxMailPointLack" runat="server">
			$PGM_HTML05;
		</mobile:Panel>
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
