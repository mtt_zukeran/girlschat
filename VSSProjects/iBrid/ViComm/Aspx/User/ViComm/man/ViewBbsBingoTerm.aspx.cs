/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝deビンゴイベントTOP
--	Progaram ID		: ViewBbsBingoTerm
--  Creation Date	: 2013.10.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewBbsBingoTerm:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			if (iBridUtil.GetStringValue(this.Request.QueryString["entry"]).Equals(ViCommConst.FLAG_ON_STR)) {
				string sResult;

				using (BbsBingoEntry oBbsBingoEntry = new BbsBingoEntry()) {
					oBbsBingoEntry.EntryBbsBingo(sessionMan.site.siteCd,sessionMan.userMan.userSeq,out sResult);
				}

				if (sResult.Equals(PwViCommConst.EntryBbsBingoResult.RESULT_OK)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl("ViewBbsBingoTerm.aspx"));
					return;
				} else if (sResult.Equals(PwViCommConst.EntryBbsBingoResult.RESULT_NG_TERM)) {
					sessionMan.errorMessage = "お宝deﾋﾞﾝｺﾞは終了しました";
					pnlError.Visible = true;
				} else if (sResult.Equals(PwViCommConst.EntryBbsBingoResult.RESULT_NG_ENTRY)) {
					sessionMan.errorMessage = "既にｴﾝﾄﾘｰ済みです";
					pnlError.Visible = true;
				} else if (sResult.Equals(PwViCommConst.EntryBbsBingoResult.RESULT_NG_CARD)) {
					sessionMan.errorMessage = "次のﾋﾞﾝｺﾞｶｰﾄﾞはありません";
					pnlError.Visible = true;
				}
			} else if (iBridUtil.GetStringValue(this.Request.QueryString["bingo"]).Equals(ViCommConst.FLAG_ON_STR)) {
				string sResult;

				using (BbsBingoTerm oBbsBingoTerm = new BbsBingoTerm()) {
					oBbsBingoTerm.GetBbsBingoPrize(sessionMan.site.siteCd,sessionMan.userMan.userSeq,out sResult);
				}

				if (sResult.Equals(PwViCommConst.GetBbsBingoPrizeResult.RESULT_OK)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_GET_BBS_BINGO_PRIZE_COMPLETE);
					return;
				} else if (sResult.Equals(PwViCommConst.GetBbsBingoPrizeResult.RESULT_NG_TERM)) {
					sessionMan.errorMessage = "お宝deﾋﾞﾝｺﾞは終了しました";
					pnlError.Visible = true;
				} else if (sResult.Equals(PwViCommConst.GetBbsBingoPrizeResult.RESULT_NG_DENIED)) {
					sessionMan.errorMessage = "ｱﾅﾀより先にBINGOした方がいます。30秒間はBINGOﾎﾞﾀﾝを押す事ができません。前のﾍﾟｰｼﾞに戻ってJACKPOTの金額を確認後、BINGOﾎﾞﾀﾝを押して下さい";
					pnlError.Visible = true;
				} else {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
					return;
				}
			} else {
				DataSet dsBbsBingoTerm;
				DataSet dsBbsBingoEntry;

				if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_BBS_BINGO_TERM_LATEST,this.ActiveForm,out dsBbsBingoTerm)) {
					RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
					return;
				}

				string sTermSeq = iBridUtil.GetStringValue(dsBbsBingoTerm.Tables[0].Rows[0]["TERM_SEQ"]);
				DateTime dtEndDate = DateTime.Parse(iBridUtil.GetStringValue(dsBbsBingoTerm.Tables[0].Rows[0]["END_DATE"]));

				if (dtEndDate < DateTime.Now) {
					pnlTermEnd.Visible = true;
				} else {
					using (BbsBingoEntry oBbsBingoEntry = new BbsBingoEntry()) {
						dsBbsBingoEntry = oBbsBingoEntry.GetSelf(sessionMan.site.siteCd,sTermSeq,sessionMan.userMan.userSeq);
					}

					if (dsBbsBingoEntry.Tables[0].Rows.Count == 0) {
						pnlEntry.Visible = true;
					} else {
						string sSelfCompleteFlag = iBridUtil.GetStringValue(dsBbsBingoEntry.Tables[0].Rows[0]["COMPLETE_FLAG"]);
						string sSelfPrizeFlag = iBridUtil.GetStringValue(dsBbsBingoEntry.Tables[0].Rows[0]["PRIZE_FLAG"]);
						string sSelfBingoBallFlag = iBridUtil.GetStringValue(dsBbsBingoEntry.Tables[0].Rows[0]["BINGO_BALL_FLAG"]);

						if (sSelfCompleteFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
							pnlTry.Visible = true;
						} else {
							if (sSelfPrizeFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
								if (sSelfBingoBallFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
									pnlComplete.Visible = true;
								} else {
									pnlBingoBall.Visible = true;
								}
							} else {
								pnlReEntry.Visible = true;
							}
						}
					}
				}
			}
		}
	}
}
