/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 過去の投票順位一覧(エントリー制)
--	Progaram ID		: ListElectionRankPast
--  Creation Date	: 2013.12.12
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListElectionRankPast:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_ELECTION_ENTRY_PAST,this.ActiveForm);
			CreateListElectionPeriod();
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	
	private void CreateListElectionPeriod () {
		string sElectionPeriodSeq = iBridUtil.GetStringValue(this.Request.QueryString["electionperiodseq"]);
		using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
			DataSet oDataSet = null;
			oDataSet = oElectionPeriod.GetListPast(sessionMan.site.siteCd);
			if (oDataSet.Tables[0].Rows.Count > 0) {
				int iIdx = 0;
				foreach (DataRow dr in oDataSet.Tables[0].Rows) {
					lstElectionPeriod.Items.Add(new MobileListItem(dr["ELECTION_PERIOD_NM"].ToString(),dr["ELECTION_PERIOD_SEQ"].ToString()));

					if (iBridUtil.GetStringValue(this.Request.QueryString["electionperiodseq"]).Equals(dr["ELECTION_PERIOD_SEQ"].ToString())) {
						lstElectionPeriod.SelectedIndex = iIdx;
					}
					iIdx++;
				}
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		string sElectionPeriodSeq = lstElectionPeriod.Selection.Value;
		RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("ListElectionRankPast.aspx?electionperiodseq={0}",sElectionPeriodSeq)));
		return;
	}
}