/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 投票(エントリー制)
--	Progaram ID		: RegistElectionVote
--  Creation Date	: 2013.12.12
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_RegistElectionVote:MobileManPageBase {
	private string sCastUserSeq;
	private string sCastCharNo;
	private int[] iVote = {1,2,3,4,5,10,15,20,25,50,100};

	protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		sCastUserSeq = iBridUtil.GetStringValue(Request.QueryString["castuserseq"]);
		sCastCharNo = iBridUtil.GetStringValue(Request.QueryString["castcharno"]);

		if (string.IsNullOrEmpty(sCastUserSeq) || string.IsNullOrEmpty(sCastCharNo)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			return;
		}

		if (!IsPostBack) {
			int iTicketCount = 0;

			if (sessionMan.logined == false) {
				if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
				} else {
					RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
				}
				return;
			}

			if (!sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_ELECTION_ENTRY_CURRENT,this.ActiveForm)) {
				RedirectToMobilePage(sessionMan.GetNavigateUrl("ListElectionRank.aspx"));
				return;
			}

			using (ElectionPeriod oElectionPeriod = new ElectionPeriod()) {
				oElectionPeriod.GetElectionVoteTicketCount(sessionMan.site.siteCd,sessionMan.userMan.userSeq,out iTicketCount);
			}

			if (iTicketCount > 0) {
				for (int i = 0;i < iVote.Length;i++) {
					if (iTicketCount >= iVote[i]) {
						lstVoteCount.Items.Add(new MobileListItem(string.Format("{0}票",iVote[i].ToString()),iVote[i].ToString()));
					}
				}
			} else {
				lstVoteCount.Visible = false;
			}

			pnlInput.Visible = true;
			pnlError.Visible = false;
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params[ViCommConst.BUTTON_RETURN] != null) {
				cmdReturn_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iVoteCount = 1;
		string sResult;

		if (lstVoteCount.Items.Count > 0) {
			iVoteCount = int.Parse(lstVoteCount.Items[lstVoteCount.SelectedIndex].Value);
		}

		using (ElectionEntry oElectionEntry = new ElectionEntry()) {
			oElectionEntry.RegistElectionVote(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCastUserSeq,
				sCastCharNo,
				iVoteCount,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_OK)) {
			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_ELECTION_ENTRY_CURRENT,this.ActiveForm);
			IDictionary<string,string> oParam = new Dictionary<string,string>();
			oParam.Add("add_count",iVoteCount.ToString());
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_REGIST_ELECTION_VOTE_COMPLETE,oParam);
			return;
		} else if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_NG_TERM)) {
			sessionMan.errorMessage = "投票は終了しました";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_NG_TICKET)) {
			sessionMan.errorMessage = "投票券が足りません";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		} else if (sResult.Equals(PwViCommConst.RegistVoteResult.RESULT_NG_RANK)) {
			sessionMan.errorMessage = "このｱｲﾄﾞﾙは投票対象外です";
			pnlInput.Visible = false;
			pnlError.Visible = true;
		}
	}

	protected void cmdReturn_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionMan.GetNavigateUrl(string.Format("RegistElectionVote.aspx?castuserseq={0}&castcharno={1}",sCastUserSeq,sCastCharNo)));
		return;
	}
}