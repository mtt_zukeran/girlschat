﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: SoftBank Payment Service
--	Progaram ID		: PaymentSps
--
--  Creation Date	: 2013.06.17
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Security.Cryptography;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;


public partial class ViComm_man_PaymentSps:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		if(!sessionMan.logined){
			throw new ApplicationException("Not logined.");
		}
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		
		if(!this.IsPostBack){
			string sAmt = iBridUtil.GetStringValue(Request.Params["packamt"]);
			string sPaymethod = iBridUtil.GetStringValue(Request.Params["paymethod"]);
			int iAmt = 0;
			if(!int.TryParse(sAmt,out iAmt)){
				throw new ArgumentException(string.Format("packamt={0}",sAmt));
			}
	
			
			string sSid = string.Empty;
			DataRow oPackDr = null;
			using (DataSet oPackDs = this.GetPackDataSet(ViCommConst.SETTLE_SPS,0)) 
			{
				if (oPackDs.Tables[0].Rows.Count > 0) {
					foreach(DataRow dr in oPackDs.Tables[0].Rows){
						if(int.Parse(iBridUtil.GetStringValue(dr["SALES_AMT"])) == iAmt){
							oPackDr = dr;
						}
					}
				}
			}

			if (oPackDr != null) {
				using (SettleLog oSettleLog = new SettleLog()) {

					oSettleLog.LogSettleRequest(
						sessionMan.site.siteCd,
						sessionMan.userMan.userSeq,
						ViCommConst.SETTLE_CORP_SOFTBANK_PAYMENT_SERVICE,
						ViCommConst.SETTLE_SPS,
						ViCommConst.SETTLE_STAT_SETTLE_NOW,
						int.Parse(iBridUtil.GetStringValue(oPackDr["SALES_AMT"])),
						int.Parse(iBridUtil.GetStringValue(oPackDr["EX_POINT"])),
						string.Empty,
						string.Empty,
						out sSid);
				}
			} else {
				string sErrorMessage = string.Format("該当するパック設定が存在しません。{0},{1}",ViCommConst.SETTLE_SPS,0);
				throw new ApplicationException(sErrorMessage);
			}
			

			// ｻｲﾄ別決済設定取得
			using(SiteSettle oSiteSettle = new SiteSettle()){
				if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_SOFTBANK_PAYMENT_SERVICE,ViCommConst.SETTLE_SPS)) {
				
					// 決済システム連携用にFORMを調整
					this.frmMain.Action = oSiteSettle.settleUrl;
					this.ctrlCheckoutForm.Text = this.GeneratePaymentFormString(sPaymethod,iAmt,oSiteSettle,sSid);					
					
				}else{
					string sErrorMessage = string.Format("決済設定が存在しません。{0},{1},{2}",sessionMan.site.siteCd,ViCommConst.SETTLE_SPS,ViCommConst.SETTLE_SPS);
					throw new ApplicationException(sErrorMessage);
				}
			}


		} 
	}
	
	private string GeneratePaymentFormString(string pPayMethods,int pPackAmt,SiteSettle pSiteSettle,string pSid){

		string sUrlBase = string.Format("http://{0}",sessionMan.site.hostNm);

		IDictionary<string,string> orderParams = new Dictionary<string,string>();
		orderParams.Add("pay_method",pPayMethods);
		orderParams.Add("merchant_id",pSiteSettle.cpIdNo);
		orderParams.Add("service_id",pSiteSettle.cpPassword);
		orderParams.Add("cust_code",sessionMan.userMan.userSeq);
		orderParams.Add("order_id",pSid);
		orderParams.Add("item_id",pPackAmt.ToString());
		orderParams.Add("amount",pPackAmt.ToString());
		orderParams.Add("pay_type","0");// 固定。0:都度購入
		orderParams.Add("service_type","0");// 固定。0:売上(購入)
		orderParams.Add("terminal_type","1");// 固定。1:モバイル
		orderParams.Add("success_url",sUrlBase + sessionMan.GetNavigateUrl("PaymentSpsReturn.aspx?sps_status=0"));
		orderParams.Add("cancel_url",sUrlBase + sessionMan.GetNavigateUrl("PaymentSpsReturn.aspx?sps_status=5"));
		orderParams.Add("error_url",sUrlBase + sessionMan.GetNavigateUrl("PaymentSpsReturn.aspx?sps_status=9"));
		orderParams.Add("pagecon_url",pSiteSettle.backUrl);
		orderParams.Add("request_date",DateTime.Now.ToString("yyyyMMddHHmmss"));
		
		StringBuilder oValueString = new StringBuilder();
		foreach(KeyValuePair<string,string> keyValue in orderParams){
			if(!string.IsNullOrEmpty(keyValue.Value)){
				oValueString.Append(keyValue.Value.Trim());
			}
		}
		//oValueString.Append("a2d8648d0246a924bed081433930f868c8100224");
		oValueString.Append(pSiteSettle.continueSettleUrl); // 継続決済URL の項目にHASHキーを設定する仕様
		
		orderParams.Add("sps_hashcode",this.GenerateSignatureString(oValueString.ToString()));
		
		StringBuilder oBuilder = new StringBuilder();
		foreach(KeyValuePair<string,string> keyValue in orderParams){
			oBuilder.AppendFormat("<input type='hidden' name='{0}' value='{1}' />",keyValue.Key,keyValue.Value).AppendLine();	
		}
		
		return oBuilder.ToString();
	}
	public string GenerateSignatureString(string pOrderString) {
		HashAlgorithm oHashAlgorithm = new SHA1CryptoServiceProvider();
		byte[] oOrderByteArry = Encoding.UTF8.GetBytes(pOrderString);
		byte[] oHashedByteArry = oHashAlgorithm.ComputeHash(oOrderByteArry);
		return BitConverter.ToString(oHashedByteArry).Replace("-",string.Empty).ToLower();
	}

	
}

