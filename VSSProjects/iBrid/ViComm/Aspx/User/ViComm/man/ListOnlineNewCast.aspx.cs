/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 新人一覧（オンライン）
--	Progaram ID		: ListOnlineNewCast
--
--  Creation Date	: 2009.11.13
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListOnlineNewCast:MobileManPageBase {
	

	virtual protected void Page_Load(object sender,EventArgs e) {
		
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_ONLINE_CAST_NEW_FACE,
					ActiveForm);
		}
	}

}
