﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性用管理者連絡詳細表示
--	Progaram ID		: ViewAdminReport
--
--  Creation Date	: 2010.05.07
--  Creater			: i-Brid(Nakano)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using ViComm;

public partial class ViComm_man_ViewAdminReport:MobileManPageBase {
	
	protected void Page_Load(object sender,EventArgs e) {
		

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.ControlList(Request,ViCommConst.INQUIRY_ADMIN_REPORT,ActiveForm)) {
			} else {
				RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,"ListAdminReport.aspx"));
			}
		}
	}
}
