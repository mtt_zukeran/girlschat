﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: いいねした画像一覧
--	Progaram ID		: ListCastPicMyLike
--
--  Creation Date	: 2016.10.05
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;
using System.Text.RegularExpressions;

public partial class ViComm_man_ListCastPicMyLike:MobileObjManPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_CAST_PIC_MY_LIKE,ActiveForm);

			// カテゴリSEQ取得
			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["hide_box"]))) {
				if (IsAvailableService(ViCommConst.RELEASE_DISABLE_ATTR_COMBO_BOX_CNT,2)) {
					CreateCastPicAttrComboBox();
					lstGenreSelect.Visible = true;
				} else {
					int iCurrentCategoryIndex;
					string sCategorySeq = "";
					int.TryParse(iBridUtil.GetStringValue(Request.QueryString["category"]),out iCurrentCategoryIndex);
					if (iCurrentCategoryIndex > 0) {
						sCategorySeq = ((ActCategory)sessionMan.categoryList[iCurrentCategoryIndex]).actCategorySeq;
					}

					// カテゴリのコンボボックスを生成（対象の画像があるカテゴリのみ表示）
					CreateObjAttrComboBox(string.Empty,string.Empty,sCategorySeq,ViCommConst.BbsObjType.PIC,ViCommConst.ATTACHED_PROFILE.ToString(),true,false,true,false);
					lstGenreSelect.Visible = true;
				}
			} else {
				pnlSearchList.Visible = false;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 選択された値をセッションに設定して検索します。

		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;

		string[] selectedValueList = selectedValue.Split(',');

		UrlBuilder oUrlBuilder = new UrlBuilder(sessionMan.GetNavigateUrl("ListCastPicMyLike.aspx"),this.Request.QueryString);
		oUrlBuilder.RemoveAddParameter("attrtypeseq", selectedValueList[0]);
		oUrlBuilder.RemoveAddParameter("attrseq", selectedValueList[1]);
		oUrlBuilder.RemoveAddParameter("pageno", string.Empty);

		foreach (string sKey in this.Request.Params.Keys) {
			if (string.IsNullOrEmpty(sKey) || !sKey.StartsWith("*")) {
				continue;
			}
			oUrlBuilder.RemoveAddParameter(sKey.Substring(1, sKey.Length - 1), Request.Params[sKey]);
		}
		RedirectToMobilePage(oUrlBuilder.ToString());
	}
}
