/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 音声通話アプリ利用設定完了
--	Progaram ID		: SetupVoiceAppComplete
--  Creation Date	: 2015.03.24
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Web.UI.MobileControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_SetupVoiceAppComplete:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.userMan.useVoiceappFlag.Equals(ViCommConst.FLAG_ON)) {
				pnlVoiceAppOn.Visible = true;
				pnlVoiceAppOff.Visible = false;
			} else {
				pnlVoiceAppOn.Visible = false;
				pnlVoiceAppOff.Visible = true;
			}
		}
	}
}
