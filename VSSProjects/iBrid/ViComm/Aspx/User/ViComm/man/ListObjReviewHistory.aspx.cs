/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: お宝レビュー一覧
--	Progaram ID		: ListObjReviewHistory
--
--  Creation Date	: 2012.04.24
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ListObjReviewHistory:MobileManPageBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			if (sessionMan.IsImpersonated == false) {
				if (sessionMan.logined == false) {
					if (IsAvailableService(ViCommConst.RELEASE_NOT_REDIRECT_REGIST_CAST)) {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermId.aspx"));
					} else {
						RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,sessionMan.sessionId,"RegistUserRequestByTermIdCast.aspx"));
					}
					return;
				}
			}

			sessionMan.ControlList(this.Request,PwViCommConst.INQUIRY_OBJ_REVIEW_HISTORY,this.ActiveForm);
		}
	}
}
