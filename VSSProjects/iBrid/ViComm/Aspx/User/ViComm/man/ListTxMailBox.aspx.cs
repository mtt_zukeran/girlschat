/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 送信メールBOX
--	Progaram ID		: ListTxMailBox
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using ViComm;

public partial class ViComm_man_ListTxMailBox:MobileManPageBase {

	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.ClearCategory();

		if (!IsPostBack) {
			Response.Filter = sessionMan.InitScreen(Response.Filter, frmMain, Request, ViewState);
			sessionMan.ControlList(Request,ViCommConst.INQUIRY_TX_USER_MAIL_BOX,ActiveForm);
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender, e);
			} else if (Request.Params[ViCommConst.BUTTON_SEARCH] != null) {
				string sSearchKey = this.Request.Form["search_key"];

				string sBaseUrl = sessionMan.GetNavigateUrl(ViCommPrograms.GetCurrentAspx());
				UrlBuilder oUrlBuilder = new UrlBuilder(sBaseUrl, sessionMan.requestQuery.QueryString);
				oUrlBuilder.Parameters.Remove("pageno");			
				oUrlBuilder.Parameters.Remove("search_key");
				if (!string.IsNullOrEmpty(sSearchKey)) {
					oUrlBuilder.Parameters.Add("search_key", sSearchKey);
				}
				RedirectToMobilePage(oUrlBuilder.ToString(true));
			}			
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		List<string> mailSeqList = new List<string>();

		foreach (string key in Request.Form.AllKeys) {
			if (key.StartsWith("checkbox")) {
				mailSeqList.Add(Request.Form[key]);
			}
		}
		if (mailSeqList.Count > 0) {
			using (MailBox oMailBox = new MailBox()) {
				oMailBox.UpdateMailDel(mailSeqList.ToArray(),ViCommConst.TX);
			}
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   "DisplayDoc.aspx?doc=" + ViCommConst.SCR_DELETED_MAN_TX_MAIL + "&mail_delete_count=" + mailSeqList.Count));
		} else {
			RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
															   "ListTxMailBox.aspx?" + Request.QueryString));
		}
	}
}
