/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 個人別写真一覧
--	Progaram ID		: ListPersonalProfilePic
--
--  Creation Date	: 2010.09.06
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ListPersonalProfilePic:MobileObjManPage {

	protected override void SetControl() {
		ObjAttrCombo = lstGenreSelect;
	}

	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sBeforeSystemId = iBridUtil.GetStringValue(Request.QueryString["beforesystemid"]);
			string sCastCharNo = ViCommConst.MAIN_CHAR_NO;
			if (!sBeforeSystemId.Equals(string.Empty)) {
				using (User oUser = new User()) {
					sLoginId = oUser.GetLoginIdByBeforeSystemId(sBeforeSystemId,ViCommConst.OPERATOR);
				}
			}
			int iRecNo;
			int.TryParse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]),out iRecNo);

			if (sessionMan.SetCastDataSetByLoginId(sLoginId,sCastCharNo,iRecNo)) {
				sessionMan.ControlList(
					Request,
					ViCommConst.INQUIRY_GALLERY,
					ActiveForm);

				if (!IsAvailableService(ViCommConst.RELEASE_DISABLE_PERSONAL_ATTR_COMBO_BOX,2)) {
					// カテゴリSEQ取得
					int iCurrentCategoryIndex;
					string sCategorySeq = "";
					int.TryParse(iBridUtil.GetStringValue(Request.QueryString["category"]),out iCurrentCategoryIndex);
					if (iCurrentCategoryIndex > 0) {
						sCategorySeq = ((ActCategory)sessionMan.categoryList[iCurrentCategoryIndex]).actCategorySeq;
					}

					CreateObjAttrComboBox(sessionMan.GetCastValue("USER_SEQ"),sessionMan.GetCastValue("USER_CHAR_NO"),sCategorySeq,ViCommConst.BbsObjType.PIC,ViCommConst.ATTACHED_PROFILE.ToString(),true,false,true,false);
					pnlSearchList.Visible = true;
				} else {
					pnlSearchList.Visible = false;
				}
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}
	protected void cmdSubmit_Click(object sender,EventArgs e) {
		// 選択された値をセッションに設定して検索します。
		string selectedValue = lstGenreSelect.Items[lstGenreSelect.SelectedIndex].Value;

		string[] selectedValueList = selectedValue.Split(',');

		string sAddQuery = string.Format("&attrtypeseq={0}&attrseq={1}",selectedValueList[0],selectedValueList[1]);
		RedirectToMobilePage(sessionMan.GetNavigateUrl(sessionMan.root + sessionMan.sysType,Session.SessionID,
												   "ListPersonalProfilePic.aspx?" + initializePageNo(Request.QueryString.ToString()) + sAddQuery));
	}

	private string initializePageNo(string query) {
		Regex regex = new Regex("&pageno=[0-9]+|&attrtypeseq=[0-9]*|&attrseq=[0-9]*",RegexOptions.Compiled);
		return regex.Replace(query,"");
	}

}
