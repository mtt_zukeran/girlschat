/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ���ۃ����ҏW
--	Progaram ID		: ModifyRefuseComment
--
--  Creation Date	: 2010.05.26
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class ViComm_man_ModifyRefuseComment:MobileManPageBase {


	virtual protected void Page_Load(object sender,EventArgs e) {

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			int iRecNo = int.Parse(iBridUtil.GetStringValue(Request.QueryString["userrecno"]));
			DataRow dr;

			if (sessionMan.SetCastDataSetByLoginId(iBridUtil.GetStringValue(Request.QueryString["loginid"]),ViCommConst.MAIN_CHAR_NO,iRecNo,out dr)) {
				ViewState["CAST_USER_SEQ"] = dr["USER_SEQ"].ToString();
				ViewState["CAST_USER_CHAR_NO"] = dr["USER_CHAR_NO"].ToString();
				using (Refuse oRefuse = new Refuse()) {
					if (oRefuse.GetOne(sessionMan.site.siteCd,sessionMan.userMan.userSeq,ViCommConst.MAIN_CHAR_NO,ViewState["CAST_USER_SEQ"].ToString(),ViewState["CAST_USER_CHAR_NO"].ToString())) {
						txtAreaRefuseComment.Text = oRefuse.refuseComment;
					}
				}
				pnlFoundDate.Visible = true;
				pnlNotFoundDate.Visible = false;
			} else {
				pnlFoundDate.Visible = false;
				pnlNotFoundDate.Visible = true;
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iMaxLength = 0;
		string sMaxLength = iBridUtil.GetStringValue(Request.Params["maxLength"]);
		int.TryParse(sMaxLength,out iMaxLength);
		if (iMaxLength < 1) {
			iMaxLength = 300;
		}
		if (txtAreaRefuseComment.Text.Length > iMaxLength) {
			lblModifyComplate.Text = GetErrorMessage(ViCommConst.ERR_STATUS_LENGTH_OVER_REF_MEMO);
		} else {
			using (Refuse oRefuse = new Refuse()) {
				oRefuse.RefuseMainte(
					sessionMan.site.siteCd,
					sessionMan.userMan.userSeq,
					ViCommConst.MAN,
					ViewState["CAST_USER_SEQ"].ToString(),
					ViewState["CAST_USER_CHAR_NO"].ToString(),
					"9",
					HttpUtility.HtmlEncode(Mobile.EmojiToCommTag(sessionMan.carrier,txtAreaRefuseComment.Text)),
					0);
			}
			lblModifyComplate.Text = GetErrorMessage(ViCommConst.ERR_STATUS_MODIFY_COMMENT_COMPLATE);
		}
	}
}
