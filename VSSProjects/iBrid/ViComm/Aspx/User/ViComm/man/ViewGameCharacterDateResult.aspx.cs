/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: ����ٹްсE�ްČ���
--	Progaram ID		: ViewGameCharacterDateResult
--
--  Creation Date	: 2011.08.25
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Drawing;
using System.Data;
using System.Collections.Specialized;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_ViewGameCharacterDateResult:MobileSocialGameManBase {
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);
		if (!IsPostBack) {
			sessionMan.userMan.GetCurrentInfo(sessionMan.site.siteCd,sessionMan.userMan.userSeq);
			sessionMan.ControlList(Request,PwViCommConst.INQUIRY_GAME_CHARACTER_VIEW,ActiveForm);
		}
	}
}