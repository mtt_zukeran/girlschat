<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NonUserTopEShot.aspx.cs" Inherits="ViComm_man_NonUserTopEShot" %>

<%@ Register Assembly="MobileLib" Namespace="MobileLib" TagPrefix="cc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
	<mobile:Form ID="frmMain" Runat="server">
		$PGM_HTML01;
		$PGM_HMTL02;
		$PGM_HTML03;
	    <cc1:iBMobileLabel ID="lblErrorMessage" runat="server" BreakAfter="true" ForeColor="Red"></cc1:iBMobileLabel>
		<br />
		<cc1:iBMobileTextBox ID="txtLoginId" runat="server" MaxLength="11" Size="16" Numeric="true"></cc1:iBMobileTextBox>
		$PGM_HTML04;
		<cc1:iBMobileTextBox ID="txtPassword" runat="server" MaxLength="8" Size="10" Numeric="true"></cc1:iBMobileTextBox>
		$PGM_HTML05;
		$PGM_HTML08;
		$PGM_HTML09;
	</mobile:Form>
</body>
</html>
