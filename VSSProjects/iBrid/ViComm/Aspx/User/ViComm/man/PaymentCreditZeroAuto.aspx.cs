/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: �ڼޯĎx��(ZERO�ڼޯ�)�������ώ葱��
--	Progaram ID		: PaymentCreditZeroAuto
--
--  Creation Date	: 2016.06.08
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI.MobileControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class ViComm_man_PaymentCreditZeroAuto:MobileManPageBase {
	private string MinAndroidVersion = "4.1";
	private string MinIOsVersion = "4.3";

	virtual protected void Page_Load(object sender,EventArgs e) {
		sessionMan.errorMessage = string.Empty;
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["settleseq"]))) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		if (IsAvailableService(ViCommConst.RELEASE_PAYMENT_CREDIT_VOICE,2)) {
			this.CheckSmartPhoneVersion();
		}

		Response.Filter = sessionMan.InitScreen(Response.Filter,frmMain,Request,ViewState);

		if (!IsPostBack) {
			string sCreditAutoSettleSeq = iBridUtil.GetStringValue(Request.QueryString["settleseq"]);
			DataSet ds;
			using (CreditAutoSettle oCreditAutoSettle = new CreditAutoSettle()) {
				ds = oCreditAutoSettle.GetOne(sessionMan.site.siteCd,sCreditAutoSettleSeq);
			}
			if (ds.Tables[0].Rows.Count > 0) {
				sessionMan.userMan.settleRequestAmt = int.Parse(ds.Tables[0].Rows[0]["SETTLE_AMT"].ToString());
				sessionMan.userMan.settleRequestPoint = int.Parse(ds.Tables[0].Rows[0]["SETTLE_POINT"].ToString());
				int iServicePoint = 0;
				using (Pack oPack = new Pack()) {
					iServicePoint = oPack.CalcServicePointCreditAutoSettle(sessionMan.site.siteCd,sessionMan.userMan.userSeq,sessionMan.userMan.settleRequestAmt);
				}
				sessionMan.userMan.settleServicePoint = iServicePoint;
			} else {
				RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
			}
		} else {
			if (Request.Params[ViCommConst.BUTTON_SUBMIT] != null) {
				cmdSubmit_Click(sender,e);
			} else if (Request.Params["cmdCancel"] != null) {
				cmdCancel_Click(sender,e);
			}
		}
	}

	protected void cmdSubmit_Click(object sender,EventArgs e) {
		int iSettleAmt = 0;
		string sSid = string.Empty;
		string sResult = string.Empty;
		string sCreditAutoSettleSeq = iBridUtil.GetStringValue(this.Request.QueryString["settleseq"]);

		if (!iBridUtil.GetStringValue(this.Request.Form["agreement"]).Equals(ViCommConst.FLAG_ON_STR)) {
			sessionMan.errorMessage = GetErrorMessage(PwViCommConst.PwErrerCode.ERR_CREDIT_AUTO_SETTLE_NO_AGREEMENT);
			return;
		}

		using (CreditAutoSettleStatus oCreditAutoSettleStatus = new CreditAutoSettleStatus()) {
			oCreditAutoSettleStatus.RegistCreditAutoSettle(
				sessionMan.site.siteCd,
				sessionMan.userMan.userSeq,
				sCreditAutoSettleSeq,
				"30",
				out iSettleAmt,
				out sSid,
				out sResult
			);
		}

		if (sResult.Equals(PwViCommConst.SimpleSPResult.RESULT_NG)) {
			RedirectToDisplayDoc(PwViCommConst.SCR_MAN_ERROR);
		}

		string sSettleUrl = "";
		string sBackUrl = "";
		string sSalesAmt = iSettleAmt.ToString();

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(sessionMan.site.siteCd,ViCommConst.SETTLE_CORP_ZERO,ViCommConst.SETTLE_CREDIT_PACK)) {
				sBackUrl = string.Format(oSiteSettle.backUrl,sessionMan.site.url,sessionMan.userMan.loginId,sessionMan.userMan.loginPassword);
				Encoding enc = Encoding.GetEncoding("Shift_JIS");
				sBackUrl = System.Web.HttpUtility.UrlEncode(sBackUrl,enc);
				sSettleUrl = string.Format(oSiteSettle.settleUrl,oSiteSettle.cpIdNo,sessionMan.userMan.emailAddr,sessionMan.userMan.userSeq,sSid,sSalesAmt,sessionMan.userMan.tel,sBackUrl);
			}
		}
		Response.Redirect(sSettleUrl);
	}

	private void CheckSmartPhoneVersion() {
		bool bOK = true;
		string sSelfVersion;
		if (sessionMan.carrier.Equals(ViCommConst.ANDROID)) {
			sSelfVersion = this.GetAndroidVersion();
			bOK = this.CompareSmartPhoneVersion(sSelfVersion,MinAndroidVersion);
		} else if (sessionMan.carrier.Equals(ViCommConst.IPHONE)) {
			sSelfVersion = this.GetIOsVersion();
			bOK = this.CompareSmartPhoneVersion(sSelfVersion,MinIOsVersion);
		}

		if (!bOK) {
			UrlBuilder oUrlBuilder = new UrlBuilder("PaymentCreditZeroVoiceAuto.aspx");
			oUrlBuilder.Parameters.Add("settleseq",iBridUtil.GetStringValue(this.Request.QueryString["settleseq"]));
			RedirectToMobilePage(sessionMan.GetNavigateUrl(oUrlBuilder.ToString()));
		}
	}

	private string GetAndroidVersion() {
		string sValue = string.Empty;
		string sUserAgent = sessionMan.requestQuery.UserAgent;
		Match oMatch = Regex.Match(sUserAgent,@"Android\s(?<ver>[\d.]*)");
		sValue = oMatch.Groups["ver"].Value;

		return sValue;
	}

	private string GetIOsVersion() {
		string sValue = string.Empty;

		string sUserAgent = sessionMan.requestQuery.UserAgent;
		Match oMatch = Regex.Match(sUserAgent,@"OS\s(?<ver>[\d_]*)");
		sValue = oMatch.Groups["ver"].Value;

		return sValue;
	}

	private bool CompareSmartPhoneVersion(string pSelfVersion,string pMinVersion) {
		bool bOK = true;

		if (string.IsNullOrEmpty(pMinVersion) || string.IsNullOrEmpty(pSelfVersion)) {
			return true;
		}

		char[] cSep = new char[] { '.','_' };

		string[] sMinVersion = pMinVersion.Split(cSep);
		string[] sSelfVersion = pSelfVersion.Split(cSep);

		int iMinMajorVer = 0;
		int iMinMinorVer = 0;
		int iSelfMajorVer = 0;
		int iSelfMinorVer = 0;

		int.TryParse(sMinVersion[0],out iMinMajorVer);
		if (sMinVersion.Length > 1) {
			int.TryParse(sMinVersion[1],out iMinMinorVer);
		}

		int.TryParse(sSelfVersion[0],out iSelfMajorVer);

		if (sSelfVersion.Length > 1) {
			int.TryParse(sSelfVersion[1],out iSelfMinorVer);
		}

		if (iMinMajorVer > iSelfMajorVer) {
			bOK = false;
		} else if (iMinMajorVer == iSelfMajorVer && iMinMinorVer > iSelfMinorVer) {
			bOK = false;
		}

		return bOK;
	}

	protected void cmdCancel_Click(object sender,EventArgs e) {
		RedirectToMobilePage(sessionMan.GetNavigateUrl("ListCreditAutoSettle.aspx"));
	}
}
