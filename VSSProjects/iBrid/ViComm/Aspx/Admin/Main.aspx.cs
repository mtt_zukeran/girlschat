﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メイン画面
--	Progaram ID		: Main
--
--  Creation Date	: 2010.04.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Web.UI.WebControls;
using ViComm;

public partial class Main:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			titleNm.Text = iBridCommLib.iBridUtil.GetStringValue(ConfigurationManager.AppSettings["TitleNm"]);
			if (titleNm.Text.Equals(string.Empty)) {
				titleNm.Text = "Vicomm Site Management";
			}
		}
	}
}
