﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TreeMenu.aspx.cs" Inherits="TreeMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>無題のページ</title>
</head>
<body class="menu">
	<form id="form1" runat="server">
		<div id="mainmenu">
			<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" AutoPostBack="True" Width="80%"
				OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
			</asp:DropDownList>
			<hr />
			<asp:TreeView ID="trvMenu" runat="server" DataSourceID="SiteMapDataSource1" ImageSet="Msdn" NodeIndent="15" Font-Size="Small" ExpandDepth="0" Target="contents"
				OnTreeNodeDataBound="trvMenu_TreeNodeDataBound">
				<NodeStyle Font-Size="8pt" HorizontalPadding="1px" NodeSpacing="1px" VerticalPadding="1px" Font-Names="ＭＳ Ｐゴシック" ForeColor="Black" />
				<SelectedNodeStyle ForeColor="Crimson" Font-Bold="True" Font-Italic="False" />
				<DataBindings>
					<asp:TreeNodeBinding DataMember="SiteMapNode" NavigateUrlField="Url" TextField="Title" />
				</DataBindings>
			</asp:TreeView>
		</div>
	</form>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" ShowStartingNode="false" SiteMapProvider="" />
</body>
</html>
