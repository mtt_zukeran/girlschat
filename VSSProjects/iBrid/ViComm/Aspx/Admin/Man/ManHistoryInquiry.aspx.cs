﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員変更履歴検索
--	Progaram ID		: ManModifyInquiry
--
--  Creation Date	: 2010.04.15
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_ManHistoryInquiry:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsUserHistory_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		grdUserHistory.PageSize = 100;
		pnlInfo.Visible = false;
		ClearField();
		lstSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		txtLoginId.Text = "";
		txtModifyDayFrom.Text = "";
		txtModifyDayTo.Text = "";
		chkRegist.Checked = false;
		chkModify.Checked = false;
		chkDelete.Checked = false;
		chkReceipt.Checked = false;
		chkTransPoint.Checked = false;
		chkCancelReceipt.Checked = false;
		
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		grdUserHistory.PageIndex = 0;

		pnlInfo.Visible = true;
		grdUserHistory.DataSourceID = "dsUserHistory";
		grdUserHistory.DataBind();
		pnlCount.DataBind();		
	}

	protected void dsUserHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		int iModifyReason;
		SetupCondition(out iModifyReason);

		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = iModifyReason;
		e.InputParameters[2] = txtLoginId.Text;
		e.InputParameters[3] = txtModifyDayFrom.Text;
		e.InputParameters[4] = "";
		e.InputParameters[5] = txtModifyDayTo.Text;
		e.InputParameters[6] = "";
	}

	private void SetupCondition(out int pModifyReason) {
		pModifyReason = 0;
		
		if (chkRegist.Checked) {
			pModifyReason += ViCommConst.MASK_MODIFY_REASON_REGIST;
		}
		if (chkModify.Checked) {
			pModifyReason += ViCommConst.MASK_MODIFY_REASON_MODIFY;
		}
		if (chkDelete.Checked) {
			pModifyReason += ViCommConst.MASK_MODIFY_REASON_DELETE;
		}
		if (chkReceipt.Checked) {
			pModifyReason += ViCommConst.MASK_MODIFY_REASON_RECEIPT;
		}
		if (chkTransPoint.Checked) {
			pModifyReason += ViCommConst.MASK_MODIFY_REASON_TRANS_POINT;
		}
		if (chkCancelReceipt.Checked) {
			pModifyReason += ViCommConst.MASK_MODIFY_REASON_CANCEL_RECEIPT;
		}

		if ((!txtModifyDayFrom.Text.Equals("")) || (!txtModifyDayTo.Text.Equals(""))) {
			if (txtModifyDayFrom.Text.Equals("")) {
				txtModifyDayFrom.Text = txtModifyDayTo.Text;
			} else if (txtModifyDayTo.Text.Equals("")) {
				txtModifyDayTo.Text = txtModifyDayFrom.Text;
			}
		}
	}
}
