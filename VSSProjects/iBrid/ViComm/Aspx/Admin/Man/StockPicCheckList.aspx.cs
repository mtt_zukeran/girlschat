﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 認証待ちストック画像一覧
--	Progaram ID		: StockPicCheckList
--
--  Creation Date	: 2011.08.04
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_StockPicCheckList : System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			if (iBridUtil.GetStringValue(Request.QueryString["closed"]).ToString() == "1") {
				chkClosed.Checked = true;
			}
			GetList();
		}
	}

	protected void dsUserManPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdPic.PageSize = 100;
		grdPic.DataSourceID = string.Empty;
		DataBind();
		if (Session["SiteCd"].ToString().Equals(string.Empty)) {
			lstSiteCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
		//if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
		//    lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		//}
		lstSiteCd.DataSourceID = string.Empty;
	}

	private void InitPage() {
		grdPic.DataSourceID = string.Empty;
		DataBind();
		recCount = "0";
		chkClosed.Checked = false;
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		string[] sKey = e.CommandArgument.ToString().Split(':');
		string sNotPublishFlag = string.Empty;
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		Response.Redirect(string.Format("../Man/StockPicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&picseq={3}&loginid={4}&return={5}&closed={6}",sKey[0],sKey[1],sKey[2],sKey[3],sKey[4],"StockPicCheckList",sNotPublishFlag));
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}

	protected void btnUpdate_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		string sSiteCd = grdPic.DataKeys[iIndex][0].ToString();
		string sUserSeq = grdPic.DataKeys[iIndex][1].ToString();
		string sUserCharNo = grdPic.DataKeys[iIndex][2].ToString();
		string sPicSeq = grdPic.DataKeys[iIndex][3].ToString();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("USER_MAN_PIC_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
			oDbSession.ProcedureInParm("pPIC_SEQ",DbSession.DbType.VARCHAR2,sPicSeq);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureInParm("pPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureInParm("pPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureInParm("pOBJECT_MODE",DbSession.DbType.NUMBER,0);
			oDbSession.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ExecuteProcedure();
		}
		Server.Transfer(string.Format("StockPicCheckList.aspx?sitecd={0}&closed={1}",
			sSiteCd,this.chkClosed.Checked ? ViCommConst.FLAG_ON_STR : string.Empty));
	}

	private void GetList() {
		grdPic.DataSourceID = "dsUserManPic";
		grdPic.PageIndex = 0;
		grdPic.DataBind();
		pnlCount.DataBind();
	}

	protected void dsUserManPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sNotPublishFlag = string.Empty;
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "1";
		e.InputParameters[2] = sNotPublishFlag;
		e.InputParameters[3] = ViCommConst.ATTACHED_MAIL.ToString();
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[5] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayTo.Text;
	}

	protected void grdPic_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"OBJ_NOT_PUBLISH_FLAG").ToString() == "1") {
				e.Row.BackColor = System.Drawing.Color.LightYellow;
			}
		}
	}
}
