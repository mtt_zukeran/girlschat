<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManWaitingInquiry.aspx.cs" Inherits="Man_ManWaitingInquiry"
	Title="男性電話待機一覧" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性電話待機一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 300px" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle2" style="width: 100px">
							サイト
						</td>
						<td class="tdDataStyle" style="width: 200px">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<table border="0" style="margin-bottom:3px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2" width="200">
							待機ｺﾒﾝﾄ
						</td>
						<td class="tdDataStyle">
							$NO_TRANS_START;
							<asp:TextBox ID="txtWaitingComment" runat="server" MaxLength="4000" Width="800px" Height="200px" TextMode="multiLine"></asp:TextBox>
							$NO_TRANS_END;
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
				<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>
					<%= DisplayWordUtil.Replace("[男性電話待機一覧]") %>
				</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdRecording.PageIndex + 1%>
						of
						<%=grdRecording.PageCount%>
					</a>
					<br>
				</asp:Panel>
				<asp:GridView ID="grdRecording" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="" SkinID="GridViewColor" AllowSorting="True"
					Font-Size="Small" PageSize="100">
					<Columns>
						<asp:TemplateField HeaderText="ID" SortExpression="LOGIN_ID">
							<ItemTemplate>
								<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
									Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
						</asp:TemplateField>
						<asp:BoundField DataField="BAL_POINT" HeaderText="残ﾎﾟｲﾝﾄ" SortExpression="BAL_POINT">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Top" />
						</asp:BoundField>
						<asp:BoundField DataField="RECORDING_DATE" HeaderText="転送開始時間" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="RECORDING_DATE">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
						</asp:BoundField>
						<asp:BoundField DataField="FOWARD_TERM_DATE" HeaderText="転送終了時間" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="RECORDING_DATE">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
						</asp:BoundField>
						<asp:BoundField DataField="WAITING_TYPE_NM" HeaderText="待機種別" SortExpression="WAITING_TYPE_NM">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
						</asp:BoundField>
						<asp:BoundField DataField="CALL_COUNT" HeaderText="呼出回数" SortExpression="CALL_COUNT">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Top" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="待機ｺﾒﾝﾄ">
							<ItemTemplate>
								<asp:Label ID="lblPicDoc" runat="server" Text='<%# Eval("WAITING_COMMENT").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Left" Width="500px" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="操作">
							<ItemTemplate>
								<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("REC_SEQ") %>' OnCommand="lnkEdit_Command" Text="編集"></asp:LinkButton>
							</ItemTemplate>
							<ItemStyle VerticalAlign="Top" />
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsRecording" runat="server" SelectMethod="GetPageCollection" TypeName="Recording" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsRecording_Selected" OnSelecting="dsRecording_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pRecType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
