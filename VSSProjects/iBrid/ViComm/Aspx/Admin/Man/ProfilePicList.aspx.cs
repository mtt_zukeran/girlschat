﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プロフィール画像一覧
--	Progaram ID		: ProfilePicList
--
--  Creation Date	: 2010.05.17
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_ProfilePicList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			GetList();
		}
	}

	protected void dsManPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdPic.PageSize = 100;
		grdPic.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
		if (iBridUtil.GetStringValue(Request.QueryString["unauth"]).Equals("1")) {
			ViewState["UNAUTH"] = "1";
			lblPgmTitle.Text = "認証待ち会員プロフ画像";
			grdPic.Columns[4].Visible = true;
			grdPic.Columns[5].Visible = true;
		} else {
			ViewState["UNAUTH"] = "0";
			lblPgmTitle.Text = "会員プロフィール画像一覧";
			grdPic.Columns[4].Visible = false;
			grdPic.Columns[5].Visible = false;
		}
	}

	private void InitPage() {
		grdPic.DataSourceID = "";
		DataBind();
		recCount = "0";
		ClearField();
	}

	private void ClearField() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		txtLoginId.Text = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	/// <summary>
	/// CSV出力ボタンが押された時の処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_PROFILE_PIC;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		// 検索条件に必要な値を取得
		int iUnAuth;
		int.TryParse(iBridUtil.GetStringValue(ViewState["UNAUTH"]),out iUnAuth);

		using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
			string sUploadDayFrom = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
			string sUploadDayTo = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayTo.Text;
			DataSet ds = oUserManCharacter.GetProfilePicList(lstSiteCd.SelectedValue,iUnAuth,txtLoginId.Text.Trim(),sUploadDayFrom,sUploadDayTo);
			// CSVに出力する文字列にタイトル行を設定
			System.Text.StringBuilder oCsvBuilder = new System.Text.StringBuilder();
			oCsvBuilder.Append("ログインID,");
			oCsvBuilder.Append("アップロード日時");
			oCsvBuilder.Append("\r\n");
			// CSVに出力する文字列にレコード行を設定
			DataRow dr;
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				dr = ds.Tables[0].Rows[i];
				oCsvBuilder.Append(dr["LOGIN_ID"]);
				oCsvBuilder.Append(",");
				oCsvBuilder.Append(dr["UPLOAD_DATE"]);
				oCsvBuilder.Append("\r\n");
			}

			Response.BinaryWrite(encoding.GetBytes(oCsvBuilder.ToString()));
			Response.End();
		}
	}

	protected void btnDelete_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		DeletePicture(sSiteCd,sUserSeq,sPicSeq);
	}

	protected void btnAuth_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		string sLoginId = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][4]).ToString();
		AuthPicture(sSiteCd,sUserSeq,sLoginId,sPicSeq,ViCommConst.FLAG_OFF);
	}

	protected void btnAuthFace_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		string sLoginId = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][4]).ToString();
		AuthPicture(sSiteCd,sUserSeq,sLoginId,sPicSeq,ViCommConst.FLAG_ON);
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl + "?" + DateTime.Now.ToString("yyyyMMddHHmmss"),sRoot);
	}

	private void DeletePicture(string pSiteCd,string pUserSeq,string pPicSeq) {

		if (!pPicSeq.Equals("")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("USER_MAN_PIC_MAINTE");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
				db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
				db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
				db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
				db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ProcedureInParm("POBJECT_MODE",DbSession.DbType.NUMBER,int.Parse(iBridUtil.GetStringValue(ViewState["UNAUTH"])));
				db.ExecuteProcedure();
			}

			DeletePicObj(pSiteCd,pPicSeq);

		}
		DataBind();
	}

	private void DeletePicObj(string pSiteCd,string pPicSeq) {
		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}
		string sFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pPicSeq.PadLeft(15,'0') + ViCommConst.PIC_FOODER;
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
			sFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pPicSeq.PadLeft(15,'0') + ViCommConst.PIC_FOODER_SMALL;
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
	}

	private void RenamePicObj(string pSiteCd,string pLoginId,string pPicSeq) {
		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}
		string sSrcFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pPicSeq.PadLeft(15,'0') + ViCommConst.PIC_FOODER;
		string sDestFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pLoginId.PadLeft(15,'0') + ViCommConst.PIC_FOODER;

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sSrcFullPath)) {
				System.IO.File.Copy(sSrcFullPath,sDestFullPath,true);
			}
			sSrcFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pPicSeq.PadLeft(15,'0') + ViCommConst.PIC_FOODER_SMALL;
			sDestFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pLoginId.PadLeft(15,'0') + ViCommConst.PIC_FOODER_SMALL;

			if (System.IO.File.Exists(sSrcFullPath)) {
				System.IO.File.Copy(sSrcFullPath,sDestFullPath,true);
			}
		}
		DeletePicObj(pSiteCd,pPicSeq);
	}

	private void AuthPicture(string pSiteCd,string pUserSeq,string pLoginId,string pPicSeq,int pProfileFacePicFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAN_PF_PIC_APPROVE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("PPROFILE_PIC_FACE_FLAG",DbSession.DbType.VARCHAR2,pProfileFacePicFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		RenamePicObj(pSiteCd,pLoginId,pPicSeq);
		DataBind();
	}

	private void GetList() {
		grdPic.DataSourceID = "dsManPic";
		grdPic.PageIndex = 0;
		grdPic.DataBind();
		pnlCount.DataBind();
	}

	protected void dsManPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		int iUnAuth;
		int.TryParse(iBridUtil.GetStringValue(ViewState["UNAUTH"]),out iUnAuth);
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = iUnAuth;
		e.InputParameters[2] = txtLoginId.Text.Trim();
		e.InputParameters[3] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayTo.Text;
	}
}
