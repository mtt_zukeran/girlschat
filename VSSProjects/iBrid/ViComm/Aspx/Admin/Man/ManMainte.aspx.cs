﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員情報設定--	Progaram ID		: ManMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Man_ManMainte:System.Web.UI.Page {
	#region キャラクター拡張
	private string RevisionNoEX {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNoEX"]);
		}
		set {
			this.ViewState["RevisionNoEX"] = value;
		}
	}
	#endregion

	private string NonExistMailAddrFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["NonExistMailAddrFlag"]);
		}
		set {
			this.ViewState["NonExistMailAddrFlag"] = value;
		}
	}

	private string RecSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RecSeq"]);
		}
		set {
			this.ViewState["RecSeq"] = value;
		}
	}

	private int CompanyMask {
		get {
			return int.Parse(Session["CompanyMask"].ToString());
		}
	}

	private bool bCheckTmgt;
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
		using (ManageCompany oInstance = new ManageCompany()) {
			bCheckTmgt = oInstance.IsAvailableService(ViCommConst.RELEASE_CHECK_TMGT_DUPLI);
		}
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		plcGGFlag.Visible = false;
		vdrModifyUserNm.Enabled = false;
		vdrModifyUserNmByDelete.Enabled = false;
		plcCreditMeasured.Visible = false;

		using (ManageCompany oManageCompany = new ManageCompany()) {
			plcMonthlyReceipt.Visible = oManageCompany.IsAvailableService(ViCommConst.RELEASE_RICHINO);
		}
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		ViewState["LOGIN_ID"] = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
		ClearField();
		DataBind();

		if (!iBridUtil.GetStringValue(Request.QueryString["site"]).Equals(string.Empty)) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["site"]);
			lstSiteCd.Enabled = false;
		}

		this.plcManWaitingComment.Visible = false;
		this.txtManWaitingComment.Enabled = false;
		this.txtManWaitingComment.BackColor = System.Drawing.SystemColors.Control;

		GetData();
	}

	private void ClearField() {
		lblRegist.Visible = false;
		lstSiteCd.Enabled = true;
		lstSiteCd.SelectedIndex = 0;
		lblLoginId.Text = "";
		txtLoginPassword.Text = "";
		txtTel.Text = "";
		txtEmailAddr.Text = "";
		txtBalPoint.Text = "0";
		txtLimitPoint.Text = "0";
		txtReceiptLimitDay.Text = "";
		txtBillAmt.Text = "0";
		txtTotalReceiptCount.Text = "0";
		txtTotalReceiptAmt.Text = "0";
		txtMchaTotalReceiptCount.Text = "0";
		txtMchaTotalReceiptAmt.Text = "0";
		txtMonthlyReceiptCount.Text = "0";
		txtMonthlyReceiptAmt.Text = "0";
		txtUrgeLevel.Text = "0";
		txtUrgeExecCount.Text = "0";
		txtMaxUrgeCount.Text = "0";
		txtUrgeCountToday.Text = "0";
		txtUrgeEndDay.Text = "";
		txtUrgeRestartDay.Text = "";
		txtServicePoint.Text = "0";
		txtServicePointEffectiveDay.Text = "";
		txtServicePointEffectiveTime.Text = "";
		txtAdCd.Text = "";
		lblIntroducerFriendCd.Text = "";
		lblFriendIntroCd.Text = "";
		txtHandleNm.Text = "";
		hdnPrevHandleNm.Value = string.Empty;
		txtRemarks1.Text = "";
		txtRemarks2.Text = "";
		txtRemarks3.Text = "";
		txtModifyUserNm.Text = "";
		txtURI.Text = "";
		txtSkypeId.Text = "";
		txtTerminalUniqueId.Text = "";
		txtIModeId.Text = "";
		lblAdNm.Text = "";
		lstCastMailRxType.SelectedIndex = 0;
		lstInfoMailRxType.SelectedIndex = 0;
		lstUseTerminalType.SelectedIndex = 0;
		lstUserStatus.SelectedIndex = 0;
		lstOnLineStatus.SelectedIndex = 0;
		lstBlogMailRxType.SelectedIndex = 0;
		lstMobileMailRxEndTime.SelectedIndex = 0;
		lstMobileMailRxStartTime.SelectedIndex = 0;
		chkLoginMailRxFlag1.Checked = false;
		chkLoginMailRxFlag2.Checked = false;
		chkAttestedFlag.Checked = false;
		rdoNonExistMailAddrFlag.SelectedValue = "0";
		rdoTxMuller3NgMailAddrFlag.SelectedValue = "0";
		chkLimitPayAfterFixedFlag.Checked = false;
		chkGGFlag.Checked = false;
		chkUseFreeDialFlag.Checked = false;
		chkUseWhitePlanFlag.Checked = false;
		chkCreditMeasuredRateTarget.Checked = false;
		chkWaitMailRxFlag.Checked = false;
		txtTel.Visible = true;
		txtEmailAddr.Visible = true;
		plcPayAfter.Visible = false;
		lstSiteCd.Enabled = true;
		chkAdminFlag.Checked = false;
		txtManWaitingComment.Text = string.Empty;
		txtCrosmileUri.Text = string.Empty;
		lstNgCount.SelectedIndex = 0;
		lstNgSkullCount.SelectedIndex = 0;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("ManView.aspx?site={0}&manloginid={1}",lstSiteCd.SelectedValue,lblLoginId.Text));
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (Ad oAd = new Ad()) {
				string sAdNm = "";
				args.IsValid = oAd.IsExist(txtAdCd.Text,ref sAdNm);
				lblAdNm.Text = sAdNm;
			}
		}
	}

	protected void vdcTel_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (txtTel.Text.Equals("") == false) {
				using (UserManCharacter oCharacter = new UserManCharacter()) {
					args.IsValid = oCharacter.IsUniqueTel(lstSiteCd.SelectedValue,txtTel.Text,lblUserSeq.Text);
				}
				if (args.IsValid) {
					if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_MAN_HOLD)) {
						using (UserManCharacter oCharacter = new UserManCharacter()) {
							args.IsValid = oCharacter.IsTelDupli(lstSiteCd.SelectedValue,this.lblUserSeq.Text,this.txtTel.Text);
						}
					}
				}
			}
		}
	}
	protected void vdcHandleNm_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (string.IsNullOrEmpty(this.txtHandleNm.Text)) {
				return;
			} else if (!SysPrograms.IsWithinByteCount(this.txtHandleNm.Text,60)) {
				args.IsValid = false;
				this.vdcHandleNm.ErrorMessage = "ﾊﾝﾄﾞﾙ名が長すぎます。";
				return;
			}

			if (!lstUserStatus.SelectedValue.Equals(ViCommConst.USER_MAN_HOLD)) {
				using (UserManCharacter oCharacter = new UserManCharacter()) {
					args.IsValid = oCharacter.CheckHandleNmDupli(lstSiteCd.SelectedValue,this.lblUserSeq.Text,this.txtHandleNm.Text);
					this.vdcHandleNm.ErrorMessage = "既に利用されているﾊﾝﾄﾞﾙ名です。";
					return;
				}
			}
		}
	}
	protected void vdcTerminalUniqueId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_MAN_HOLD)) {
				if (string.IsNullOrEmpty(this.txtTerminalUniqueId.Text)) {
					return;
				}

				using (UserManCharacter oCharacter = new UserManCharacter()) {
					args.IsValid = oCharacter.IsTerminalUniqueIdDupli(lstSiteCd.SelectedValue,this.lblUserSeq.Text,this.txtTerminalUniqueId.Text);
				}
			}
		}
	}
	protected void vdcIModeId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_MAN_HOLD)) {
				if (string.IsNullOrEmpty(this.txtIModeId.Text)) {
					return;
				}

				using (UserManCharacter oCharacter = new UserManCharacter()) {
					args.IsValid = oCharacter.IsIModeIdDupli(lstSiteCd.SelectedValue,this.lblUserSeq.Text,this.txtIModeId.Text);
				}
			}
		}
	}
	protected void vdcEmailAddr_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_MAN_HOLD)) {
				if (string.IsNullOrEmpty(this.txtEmailAddr.Text)) {
					return;
				}

				using (UserManCharacter oCharacter = new UserManCharacter()) {
					args.IsValid = oCharacter.IsEmailDupli(lstSiteCd.SelectedValue,this.lblUserSeq.Text,this.txtEmailAddr.Text);
				}
			}
		}
	}

	protected void vdcServicePointEffectiveDay_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			args.IsValid = ((int.Parse(txtServicePoint.Text) == 0) || (!txtServicePointEffectiveDay.Text.Equals("")));
		}
	}
	protected void vdcServicePointEffectiveTime_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			args.IsValid = ((int.Parse(txtServicePoint.Text) == 0) || (!txtServicePointEffectiveTime.Text.Equals("")));
		}
	}

	private void GetData() {
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_MAN_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,lblUserSeq.Text);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PONLINE_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEL_ATTESTED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_FREE_DIAL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_WHITE_PLAN_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_CROSMILE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTALK_SMART_DIRECT_NOT_MONITOR",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNON_EXIST_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBAL_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSERVICE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSERVICE_POINT_EFFECTIVE_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSERVICE_POINT_EFFECTIVE_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLIMIT_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PFIRST_POINT_USED_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLAST_POINT_USED_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFIRST_RECEIPT_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLAST_RECEIPT_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRECEIPT_LIMIT_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBILL_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTOTAL_RECEIPT_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTOTAL_RECEIPT_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPOINT_AFFILIATE_TOTAL_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPOINT_AFFILIATE_TOTAL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMONTHLY_RECEIPT_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMONTHLY_RECEIPT_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PEXIST_CREDIT_DEAL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLIMIT_PAY_AFTER_FIXED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCREDIT_MEASURED_RATE_TARGET",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PGG_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBLACK_DAY_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBLACK_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLAST_BLACK_DAY_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_MAIL_RX_FLAG1",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGIN_MAIL_RX_FLAG2",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHANDLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBIRTHDAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PREMARKS4",DbSession.DbType.VARCHAR2,7);
			db.ProcedureOutParm("PUSE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PURI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSKYPE_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIMODE_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCROSMILE_URI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PURGE_LEVEL",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PURGE_EXEC_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAX_URGE_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PURGE_COUNT_TODAY",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PURGE_END_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PURGE_RESTART_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID_USER",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_USER",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MAN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MAN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MAN_CHARACTER",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MAN_CHARACTER",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_URGE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_URGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFRIEND_INTRO_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_DEFINE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNG_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNG_SKULL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSE_VOICEAPP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pTX_MULLER3_NG_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutArrayParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PMAN_ATTR_TYPE_NM",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PATTR_INPUT_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PMAN_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PMAN_ATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PROW_COUNT",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PMAN_ATTR_ROWID",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutParm("PMAN_ATTR_RECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO_USER"] = db.GetStringValue("PREVISION_NO_USER");
			ViewState["ROWID_USER"] = db.GetStringValue("PROWID_USER");

			ViewState["REVISION_NO_MAN"] = db.GetStringValue("PREVISION_NO_MAN");
			ViewState["ROWID_MAN"] = db.GetStringValue("PROWID_MAN");

			ViewState["REVISION_NO_MAN_CHARACTER"] = db.GetStringValue("PREVISION_NO_MAN_CHARACTER");
			ViewState["ROWID_MAN_CHARACTER"] = db.GetStringValue("PROWID_MAN_CHARACTER");

			ViewState["REVISION_NO_URGE"] = db.GetStringValue("PREVISION_NO_URGE");
			ViewState["ROWID_URGE"] = db.GetStringValue("PROWID_URGE");

			ViewState["MAN_ATTR_RECORD_COUNT"] = db.GetStringValue("PMAN_ATTR_RECORD_COUNT");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				lblLoginId.Text = db.GetStringValue("PLOGIN_ID");
				txtLoginPassword.Text = db.GetStringValue("PLOGIN_PASSWORD");
				txtTel.Text = db.GetStringValue("PTEL");
				txtEmailAddr.Text = db.GetStringValue("PEMAIL_ADDR");
				txtBalPoint.Text = db.GetStringValue("PBAL_POINT");
				txtLimitPoint.Text = db.GetStringValue("PLIMIT_POINT");
				txtReceiptLimitDay.Text = db.GetStringValue("PRECEIPT_LIMIT_DAY");
				txtBillAmt.Text = db.GetStringValue("PBILL_AMT");
				txtTotalReceiptCount.Text = db.GetStringValue("PTOTAL_RECEIPT_COUNT");
				txtTotalReceiptAmt.Text = db.GetStringValue("PTOTAL_RECEIPT_AMT");
				txtMchaTotalReceiptCount.Text = db.GetStringValue("PPOINT_AFFILIATE_TOTAL_COUNT");
				txtMchaTotalReceiptAmt.Text = db.GetStringValue("PPOINT_AFFILIATE_TOTAL_AMT");
				txtMonthlyReceiptCount.Text = db.GetStringValue("PMONTHLY_RECEIPT_COUNT");
				txtMonthlyReceiptAmt.Text = db.GetStringValue("PMONTHLY_RECEIPT_AMT");
				txtUrgeLevel.Text = db.GetStringValue("PURGE_LEVEL");
				txtUrgeExecCount.Text = db.GetStringValue("PURGE_EXEC_COUNT");
				txtMaxUrgeCount.Text = db.GetStringValue("PMAX_URGE_COUNT");
				txtUrgeCountToday.Text = db.GetStringValue("PURGE_COUNT_TODAY");
				txtUrgeEndDay.Text = db.GetStringValue("PURGE_END_DAY");
				txtUrgeRestartDay.Text = db.GetStringValue("PURGE_RESTART_DAY");
				txtServicePoint.Text = db.GetStringValue("PSERVICE_POINT");
				txtServicePointEffectiveDay.Text = db.GetStringValue("PSERVICE_POINT_EFFECTIVE_DAY");
				txtServicePointEffectiveTime.Text = db.GetStringValue("PSERVICE_POINT_EFFECTIVE_TIME");
				txtAdCd.Text = db.GetStringValue("PAD_CD");
				lblIntroducerFriendCd.Text = db.GetStringValue("PINTRODUCER_FRIEND_CD");
				lblFriendIntroCd.Text = db.GetStringValue("PFRIEND_INTRO_CD");
				txtHandleNm.Text = db.GetStringValue("PHANDLE_NM");
				hdnPrevHandleNm.Value = txtHandleNm.Text;
				txtBirthDay.Text = db.GetStringValue("PBIRTHDAY");
				txtRemarks1.Text = db.GetStringValue("PREMARKS1");
				txtRemarks2.Text = db.GetStringValue("PREMARKS2");
				txtRemarks3.Text = db.GetStringValue("PREMARKS3");
				for (int i = 0;i < 7;i++) {
					txtRemarks4.Text = txtRemarks4.Text + db.GetArryStringValue("PREMARKS4",i);
				}
				txtURI.Text = db.GetStringValue("PURI");
				txtSkypeId.Text = db.GetStringValue("PSKYPE_ID");
				txtTerminalUniqueId.Text = db.GetStringValue("PTERMINAL_UNIQUE_ID");
				txtIModeId.Text = db.GetStringValue("PIMODE_ID");
				txtCrosmileUri.Text = db.GetStringValue("pCROSMILE_URI");

				lblAdNm.Text = db.GetStringValue("PAD_NM");

				if (!db.GetStringValue("PCAST_MAIL_RX_TYPE").Equals("")) {
					lstCastMailRxType.SelectedValue = db.GetStringValue("PCAST_MAIL_RX_TYPE");
				}

				if (!db.GetStringValue("PINFO_MAIL_RX_TYPE").Equals("")) {
					lstInfoMailRxType.SelectedValue = db.GetStringValue("PINFO_MAIL_RX_TYPE");
				}

				if (!db.GetStringValue("PUSER_STATUS").Equals("")) {
					lstUserStatus.SelectedValue = db.GetStringValue("PUSER_STATUS");
				}

				if (!db.GetStringValue("PONLINE_STATUS").Equals("")) {
					lstOnLineStatus.SelectedValue = db.GetStringValue("PONLINE_STATUS");
				}

				if (!db.GetStringValue("PMOBILE_MAIL_RX_START_TIME").Equals("")) {
					lstMobileMailRxStartTime.SelectedValue = db.GetStringValue("PMOBILE_MAIL_RX_START_TIME");
				}
				if (!db.GetStringValue("PMOBILE_MAIL_RX_END_TIME").Equals("")) {
					lstMobileMailRxEndTime.SelectedValue = db.GetStringValue("PMOBILE_MAIL_RX_END_TIME");
				}
				if (!db.GetStringValue("PUSE_TERMINAL_TYPE").Equals("")) {
					lstUseTerminalType.SelectedValue = db.GetStringValue("PUSE_TERMINAL_TYPE");
				}

				chkLoginMailRxFlag1.Checked = (db.GetIntValue("PLOGIN_MAIL_RX_FLAG1") != 0);
				chkLoginMailRxFlag2.Checked = (db.GetIntValue("PLOGIN_MAIL_RX_FLAG2") != 0);
				chkAttestedFlag.Checked = (db.GetIntValue("PTEL_ATTESTED_FLAG") != 0);
				rdoNonExistMailAddrFlag.SelectedValue = db.GetIntValue("PNON_EXIST_MAIL_ADDR_FLAG").ToString();
				rdoTxMuller3NgMailAddrFlag.SelectedValue = db.GetIntValue("PTX_MULLER3_NG_MAIL_ADDR_FLAG").ToString();
				chkLimitPayAfterFixedFlag.Checked = (db.GetIntValue("PLIMIT_PAY_AFTER_FIXED_FLAG") != 0);
				chkGGFlag.Checked = (db.GetIntValue("PGG_FLAG") != 0);
				chkExistCreditDealFlag.Checked = (db.GetIntValue("PEXIST_CREDIT_DEAL_FLAG") != 0);
				chkUseFreeDialFlag.Checked = (db.GetIntValue("PUSE_FREE_DIAL_FLAG") != 0);
				chkUseWhitePlanFlag.Checked = (db.GetIntValue("PUSE_WHITE_PLAN_FLAG") != 0);
				chkUseCrosmileFlag.Checked = (db.GetIntValue("PUSE_CROSMILE_FLAG") != 0);
				chkTalkDirectSmartNotMonitor.Checked = (db.GetIntValue("pTALK_SMART_DIRECT_NOT_MONITOR") != 0);
				chkCreditMeasuredRateTarget.Checked = (db.GetIntValue("PCREDIT_MEASURED_RATE_TARGET") != 0);
				chkAdminFlag.Checked = (db.GetIntValue("PADMIN_FLAG") != 0);
				chkUseVoiceappFlag.Checked = (db.GetIntValue("pUSE_VOICEAPP_FLAG") != 0);

				this.NonExistMailAddrFlag = db.GetStringValue("PNON_EXIST_MAIL_ADDR_FLAG");

				if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
					txtTel.Visible = true;
					txtEmailAddr.Visible = true;
				} else {
					txtTel.Visible = false;
					txtEmailAddr.Visible = false;
				}
				lblRegist.Visible = false;

				int iUserDefineFlag = db.GetIntValue("PUSER_DEFINE_FLAG");

				foreach (ListItem oListItem in chkUserDefineFlag.Items) {
					if ((iUserDefineFlag & int.Parse(oListItem.Value)) > 0) {
						oListItem.Selected = true;
					} else {
						oListItem.Selected = false;
					}
				}

				lstNgCount.SelectedIndex = lstNgCount.Items.IndexOf(lstNgCount.Items.FindByValue(db.GetIntValue("PNG_COUNT").ToString()));
				lstNgSkullCount.SelectedIndex = lstNgSkullCount.Items.IndexOf(lstNgSkullCount.Items.FindByValue(db.GetIntValue("PNG_SKULL_COUNT").ToString()));

				using (Site oSite = new Site()) {
					plcPayAfter.Visible = oSite.IsAvailablePayAfterFlag(lstSiteCd.SelectedValue);
					if (plcPayAfter.Visible == false) {
						plcPayAfter.Visible = oSite.IsUsedChargeSettleFlag(lstSiteCd.SelectedValue);
					}
				}
				lstSiteCd.Enabled = false;

				if (this.plcManWaitingComment.Visible) {
					this.GetManWaitingComment();
				}

			} else {
				ClearField();
				lblLoginId.Text = db.GetStringValue("PLOGIN_ID");
				lblRegist.Visible = true;
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				}
			}
			db.conn.Close();
			using (UserManAttrTypeValue oManAttrTypeValue = new UserManAttrTypeValue()) {
				for (int i = 0;i < db.GetIntValue("PMAN_ATTR_RECORD_COUNT");i++) {
					Label lblManAttrNm = (Label)plcHolder.FindControl(string.Format("lblManAttrNm{0}",i)) as Label;
					lblManAttrNm.Text = db.GetArryStringValue("PMAN_ATTR_TYPE_NM",i);

					DropDownList lstManAttrSeq = (DropDownList)plcHolder.FindControl(string.Format("lstManAttrSeq{0}",i)) as DropDownList;
					TextBox txtInputValue = (TextBox)plcHolder.FindControl(string.Format("txtAttrInputValue{0}",i)) as TextBox;
					DataSet ds = oManAttrTypeValue.GetList(lstSiteCd.SelectedValue,db.GetArryStringValue("PMAN_ATTR_TYPE_SEQ",i));
					foreach (DataRow dr in ds.Tables[0].Rows) {
						lstManAttrSeq.Items.Add(new ListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
					}

					lstManAttrSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));

					if (db.GetArryStringValue("PATTR_INPUT_TYPE",i).Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtInputValue.Rows = int.Parse(db.GetArryStringValue("PROW_COUNT",i));
						if (txtInputValue.Rows > 1) {
							txtInputValue.TextMode = TextBoxMode.MultiLine;
						}
						txtInputValue.Text = db.GetArryStringValue("PMAN_ATTR_INPUT_VALUE",i);
						lstManAttrSeq.Visible = false;
					} else {
						if (db.GetArryStringValue("PMAN_ATTR_SEQ",i) != "") {
							lstManAttrSeq.SelectedValue = db.GetArryStringValue("PMAN_ATTR_SEQ",i);
						}
						txtInputValue.Visible = false;
					}
					ViewState["ATTR_INPUT_TYPE" + i.ToString()] = db.GetArryStringValue("PATTR_INPUT_TYPE",i);
					ViewState["ATTR_ROWID" + i.ToString()] = db.GetArryStringValue("PMAN_ATTR_ROWID",i);
					ViewState["MAN_ATTR_TYPE_SEQ" + i.ToString()] = db.GetArryStringValue("PMAN_ATTR_TYPE_SEQ",i);
				}

				for (int i = db.GetIntValue("PMAN_ATTR_RECORD_COUNT");i < ViCommConst.MAX_ATTR_COUNT;i++) {
					TableRow rowAttr = (TableRow)plcHolder.FindControl(string.Format("rowAttr{0}",i)) as TableRow;
					rowAttr.Visible = false;
				}
			}
		}
		pnlDtl.Visible = true;
		this.pnlCharacterEx.Visible = true;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("USER_MAN_CHARACTER_EX_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.lblUserSeq.Text);
			oDbSession.ProcedureOutParm("pBLOG_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pWAIT_MAIL_RX_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROW_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
			if (int.Parse(oDbSession.GetStringValue("pROW_COUNT")) > 0) {
				this.lstBlogMailRxType.SelectedValue = oDbSession.GetStringValue("pBLOG_MAIL_RX_TYPE");
				this.chkWaitMailRxFlag.Checked = oDbSession.GetStringValue("pWAIT_MAIL_RX_FLAG").Equals("1");
				this.RevisionNoEX = oDbSession.GetStringValue("pREVISION_NO");
			}
		}
	}

	private void GetManWaitingComment() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_MAN_WAITING_COMMENT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.lblUserSeq.Text);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureOutParm("pREC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pWAITING_COMMENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.RecSeq = db.GetStringValue("pREC_SEQ");
			this.txtManWaitingComment.Text = db.GetStringValue("pWAITING_COMMENT");
			if (string.IsNullOrEmpty(this.RecSeq)) {
				this.txtManWaitingComment.Enabled = false;
				this.txtManWaitingComment.BackColor = System.Drawing.SystemColors.Control;
			} else {
				this.txtManWaitingComment.Enabled = true;
				this.txtManWaitingComment.BackColor = System.Drawing.Color.Empty;
			}			
		}
	}

	private void UpdateData(int pDelFlag) {
		string sHandelNm = Mobile.EmojiToCommTag(ViCommConst.CARRIER_OTHERS,txtHandleNm.Text);

		int iUserDefineFlag = 0;

		foreach (ListItem oListItem in chkUserDefineFlag.Items) {
			if (!oListItem.Selected)
				continue;
			iUserDefineFlag += int.Parse(oListItem.Value);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_MAN_MAINTE");
			db.ProcedureBothParm("PUSER_SEQ",DbSession.DbType.NUMBER,lblUserSeq.Text);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,lblLoginId.Text);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,txtLoginPassword.Text);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,txtTel.Text);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,txtEmailAddr.Text.ToLower());
			db.ProcedureInParm("PUSER_STATUS",DbSession.DbType.VARCHAR2,lstUserStatus.SelectedValue);
			db.ProcedureInParm("PONLINE_STATUS",DbSession.DbType.VARCHAR2,lstOnLineStatus.SelectedValue);
			db.ProcedureInParm("PTEL_ATTESTED_FLAG",DbSession.DbType.NUMBER,chkAttestedFlag.Checked);
			db.ProcedureInParm("PUSE_FREE_DIAL_FLAG",DbSession.DbType.NUMBER,chkUseFreeDialFlag.Checked);
			db.ProcedureInParm("PUSE_WHITE_PLAN_FLAG",DbSession.DbType.NUMBER,chkUseWhitePlanFlag.Checked);
			db.ProcedureInParm("PUSE_CROSMILE_FLAG",DbSession.DbType.NUMBER,chkUseCrosmileFlag.Checked);
			db.ProcedureInParm("PTALK_SMART_DIRECT_NOT_MONITOR",DbSession.DbType.NUMBER,chkTalkDirectSmartNotMonitor.Checked);
			db.ProcedureInParm("PNON_EXIST_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER,int.Parse(rdoNonExistMailAddrFlag.SelectedValue));
			db.ProcedureInParm("PBAL_POINT",DbSession.DbType.NUMBER,int.Parse(txtBalPoint.Text));
			db.ProcedureInParm("PSERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtServicePoint.Text));
			db.ProcedureInParm("PSERVICE_POINT_EFFECTIVE_DAY",DbSession.DbType.VARCHAR2,txtServicePointEffectiveDay.Text);
			db.ProcedureInParm("PSERVICE_POINT_EFFECTIVE_TIME",DbSession.DbType.VARCHAR2,txtServicePointEffectiveTime.Text);
			db.ProcedureInParm("PLIMIT_POINT",DbSession.DbType.NUMBER,int.Parse(txtLimitPoint.Text));
			db.ProcedureInParm("PRECEIPT_LIMIT_DAY",DbSession.DbType.VARCHAR2,txtReceiptLimitDay.Text);
			db.ProcedureInParm("PBILL_AMT",DbSession.DbType.NUMBER,int.Parse(txtBillAmt.Text));
			db.ProcedureInParm("PTOTAL_RECEIPT_AMT",DbSession.DbType.NUMBER,int.Parse(txtTotalReceiptAmt.Text));
			db.ProcedureInParm("PTOTAL_RECEIPT_COUNT",DbSession.DbType.NUMBER,int.Parse(txtTotalReceiptCount.Text));
			db.ProcedureInParm("PPOINT_AFFILIATE_TOTAL_AMT",DbSession.DbType.NUMBER,int.Parse(txtMchaTotalReceiptAmt.Text));
			db.ProcedureInParm("PPOINT_AFFILIATE_TOTAL_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMchaTotalReceiptCount.Text));
			db.ProcedureInParm("PMONTHLY_RECEIPT_AMT",DbSession.DbType.NUMBER,int.Parse(txtMonthlyReceiptAmt.Text));
			db.ProcedureInParm("PMONTHLY_RECEIPT_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMonthlyReceiptCount.Text));
			db.ProcedureInParm("PCAST_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,lstCastMailRxType.SelectedValue);
			db.ProcedureInParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,lstInfoMailRxType.SelectedValue);
			db.ProcedureInParm("PLOGIN_MAIL_RX_FLAG1",DbSession.DbType.NUMBER,chkLoginMailRxFlag1.Checked);
			db.ProcedureInParm("PLOGIN_MAIL_RX_FLAG2",DbSession.DbType.NUMBER,chkLoginMailRxFlag2.Checked);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2,lstMobileMailRxStartTime.SelectedValue);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2,lstMobileMailRxEndTime.SelectedValue);
			db.ProcedureInParm("PEXIST_CREDIT_DEAL_FLAG",DbSession.DbType.VARCHAR2,chkExistCreditDealFlag.Checked);
			db.ProcedureInParm("PLIMIT_PAY_AFTER_FIXED_FLAG",DbSession.DbType.NUMBER,chkLimitPayAfterFixedFlag.Checked);
			db.ProcedureInParm("PCREDIT_MEASURED_RATE_TARGET",DbSession.DbType.NUMBER,chkCreditMeasuredRateTarget.Checked);
			db.ProcedureInParm("PGG_FLAG",DbSession.DbType.NUMBER,chkGGFlag.Checked);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sHandelNm);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,txtBirthDay.Text);
			db.ProcedureInParm("PREMARKS1",DbSession.DbType.VARCHAR2,txtRemarks1.Text);
			db.ProcedureInParm("PREMARKS2",DbSession.DbType.VARCHAR2,txtRemarks2.Text);
			db.ProcedureInParm("PREMARKS3",DbSession.DbType.VARCHAR2,txtRemarks3.Text);
			string[] sRemarks = SysPrograms.SplitBytes(Encoding.GetEncoding("UTF-8"),txtRemarks4.Text,3000);
			db.ProcedureInArrayParm("PREMARKS4",DbSession.DbType.VARCHAR2,sRemarks);
			db.ProcedureInParm("PMODIFY_USER_NM",DbSession.DbType.VARCHAR2,txtModifyUserNm.Text);
			db.ProcedureInParm("PUSE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2,lstUseTerminalType.SelectedValue);
			db.ProcedureInParm("PURI",DbSession.DbType.VARCHAR2,txtURI.Text);
			db.ProcedureInParm("PSKYPE_ID",DbSession.DbType.VARCHAR2,txtSkypeId.Text);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,txtTerminalUniqueId.Text);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,txtIModeId.Text);
			db.ProcedureInParm("pCROSMILE_URI",DbSession.DbType.VARCHAR2,txtCrosmileUri.Text.Trim());
			db.ProcedureInParm("PADMIN_FLAG",DbSession.DbType.NUMBER,chkAdminFlag.Checked);
			db.ProcedureInParm("PURGE_LEVEL",DbSession.DbType.NUMBER,int.Parse(txtUrgeLevel.Text));
			db.ProcedureInParm("PURGE_EXEC_COUNT",DbSession.DbType.NUMBER,int.Parse(txtUrgeExecCount.Text));
			db.ProcedureInParm("PMAX_URGE_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMaxUrgeCount.Text));
			db.ProcedureInParm("PURGE_COUNT_TODAY",DbSession.DbType.NUMBER,int.Parse(txtUrgeCountToday.Text));
			db.ProcedureInParm("PURGE_END_DAY",DbSession.DbType.VARCHAR2,txtUrgeEndDay.Text);
			db.ProcedureInParm("PURGE_RESTART_DAY",DbSession.DbType.VARCHAR2,txtUrgeRestartDay.Text);
			db.ProcedureInParm("PROWID_USER",DbSession.DbType.VARCHAR2,ViewState["ROWID_USER"].ToString());
			db.ProcedureInParm("PREVISION_NO_USER",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_USER"].ToString()));
			db.ProcedureInParm("PROWID_MAN",DbSession.DbType.VARCHAR2,ViewState["ROWID_MAN"].ToString());
			db.ProcedureInParm("PREVISION_NO_MAN",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_MAN"].ToString()));
			db.ProcedureInParm("PROWID_MAN_CHARACTER",DbSession.DbType.VARCHAR2,ViewState["ROWID_MAN_CHARACTER"].ToString());
			db.ProcedureInParm("PREVISION_NO_MAN_CHARACTER",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_MAN_CHARACTER"].ToString()));
			db.ProcedureInParm("PROWID_URGE",DbSession.DbType.VARCHAR2,ViewState["ROWID_URGE"].ToString());
			db.ProcedureInParm("PREVISION_NO_URGE",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_URGE"].ToString()));

			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,lblIntroducerFriendCd.Text);
			db.ProcedureInParm("PUSER_DEFINE_FLAG",DbSession.DbType.NUMBER,iUserDefineFlag);
			db.ProcedureInParm("PNG_COUNT",DbSession.DbType.NUMBER,int.Parse(lstNgCount.SelectedValue));
			db.ProcedureInParm("PNG_SKULL_COUNT",DbSession.DbType.NUMBER,int.Parse(lstNgSkullCount.SelectedValue));
			db.ProcedureInParm("pUSE_VOICEAPP_FLAG",DbSession.DbType.NUMBER,chkUseVoiceappFlag.Checked);
			db.ProcedureInParm("PTX_MULLER3_NG_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER,int.Parse(rdoTxMuller3NgMailAddrFlag.SelectedValue));
			int iRecCount = int.Parse(ViewState["MAN_ATTR_RECORD_COUNT"].ToString());

			string[] sManAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sManAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sAttrRowID = new string[ViCommConst.MAX_ATTR_COUNT];

			for (int i = 0;i < iRecCount;i++) {
				if (ViewState["ATTR_INPUT_TYPE" + i.ToString()].ToString() == ViCommConst.INPUT_TYPE_TEXT) {
					TextBox txtInputValue = (TextBox)plcHolder.FindControl(string.Format("txtAttrInputValue{0}",i)) as TextBox;
					sAttrInputValue[i] = txtInputValue.Text;
				} else {
					DropDownList lstManAttrSeq = (DropDownList)plcHolder.FindControl(string.Format("lstManAttrSeq{0}",i)) as DropDownList;
					sManAttrSeq[i] = lstManAttrSeq.SelectedValue;
				}
				sManAttrTypeSeq[i] = (string)ViewState["MAN_ATTR_TYPE_SEQ" + i.ToString()];
				sAttrRowID[i] = (string)ViewState["ATTR_ROWID" + i.ToString()];
			}

			db.ProcedureInArrayParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,iRecCount,sManAttrTypeSeq);
			db.ProcedureInArrayParm("PCAST_ATTR_SEQ",DbSession.DbType.VARCHAR2,iRecCount,sManAttrSeq);
			db.ProcedureInArrayParm("PATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,iRecCount,sAttrInputValue);
			db.ProcedureInArrayParm("PATTR_ROWID",DbSession.DbType.VARCHAR2,iRecCount,sAttrRowID);

			db.ProcedureInParm("PATTR_RECORD_COUNT",DbSession.DbType.NUMBER,iRecCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.lblUserSeq.Text = db.GetStringValue("pUSER_SEQ");

			if (this.plcManWaitingComment.Visible) {
				this.UpdateManWaitingComment();
			}
		}

		string sNonExistMailAddrFlag = this.rdoNonExistMailAddrFlag.SelectedValue;
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("USER_MAN_CHARACTER_EX_MAINTE");
			oDbSession.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,this.lblUserSeq.Text);
			oDbSession.ProcedureInParm("PCHANGED_NON_EXIST_MAIL_FLAG",DbSession.DbType.NUMBER,this.NonExistMailAddrFlag.Equals(sNonExistMailAddrFlag) ? ViCommConst.FLAG_OFF : ViCommConst.FLAG_ON);
			oDbSession.ProcedureInParm("PNON_EXIST_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER,sNonExistMailAddrFlag);
			oDbSession.ProcedureInParm("PBLOG_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,this.lstBlogMailRxType.SelectedValue);
			oDbSession.ProcedureInParm("PWAIT_MAIL_RX_FLAG",DbSession.DbType.NUMBER,this.chkWaitMailRxFlag.Checked);
			oDbSession.ProcedureInParm("OREVISION_NO",DbSession.DbType.VARCHAR2,this.RevisionNoEX);
			oDbSession.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		Server.Transfer(string.Format("ManView.aspx?site={0}&manloginid={1}",lstSiteCd.SelectedValue,lblLoginId.Text));
	}

	private void UpdateManWaitingComment() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAN_WAITING_COMMENT");
			db.ProcedureInParm("pREC_SEQ",DbSession.DbType.VARCHAR2,this.RecSeq);
			db.ProcedureInParm("pWAITING_COMMENT",DbSession.DbType.VARCHAR2,this.txtManWaitingComment.Text);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected void vdcBirthday_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.vddBirthDay.IsValid) {
			int iOkAge;
			int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
			if (iOkAge == 0) {
				iOkAge = 6;
			}
			args.IsValid = (ViCommPrograms.Age(txtBirthDay.Text) >= iOkAge);
		}
	}

	protected void vdcMaxBirthday_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.vddBirthDay.IsValid) {
			int iMaxAge = 999;
			args.IsValid = (iMaxAge >= ViCommPrograms.Age(txtBirthDay.Text));
		}
	}
}
