﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 登録不可番号メンテナンス
--	Progaram ID		: UnableRegistNoList
--
--  Creation Date	: 2010.04.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Man_UnableRegistNoList:System.Web.UI.Page {
	private string recCount = "";
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdUnableRegistNo.PageSize = 100;
		grdUnableRegistNo.DataSourceID ="";
		DataBind();

		lstSeekNoType.Items.Insert(0,new ListItem("",""));
		lstSeekNoType.DataSourceID = "";
		lstNoType.DataSourceID = "";
	}

	private void InitPage() {
		txtTermIdOrTel.Text = "";
		txtSeekTermIdOrTel.Text = "";
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		txtRemarks.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdUnableRegistNo.DataSourceID = "dsUnableRegistNo";
		grdUnableRegistNo.PageIndex = 0;
		grdUnableRegistNo.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkTermIdOrTel_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstNoType.SelectedValue = sKeys[0];
		txtTermIdOrTel.Text = sKeys[1];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		ExportCSV();
	}

	protected void dsUnableRegistNo_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsUnableRegistNo_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekNoType.SelectedValue;
		e.InputParameters[1] = txtSeekTermIdOrTel.Text;
	}


	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UNABLE_REGIST_NO_GET");
			db.ProcedureInParm("PNO_TYPE",DbSession.DbType.VARCHAR2,lstNoType.SelectedValue);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID_OR_TEL",DbSession.DbType.VARCHAR2,txtTermIdOrTel.Text);
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtRemarks.Text = db.GetStringValue("PREMARKS");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UNABLE_REGIST_NO_MAINTE");
			db.ProcedureInParm("PNO_TYPE",DbSession.DbType.VARCHAR2,lstNoType.SelectedValue);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID_OR_TEL",DbSession.DbType.VARCHAR2,txtTermIdOrTel.Text);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstNoType.SelectedIndex;
		InitPage();
		if (lstSeekNoType.SelectedIndex != 0) {
			iIdx++;
			lstSeekNoType.SelectedIndex = iIdx;
		}
		GetList();
	}

	private void ExportCSV() {
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition","attachment;filename=UnableRegistNo.csv");
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (UnableRegistNo oUnableRegistNo = new UnableRegistNo()) {
			DataSet ds = oUnableRegistNo.GetCsvData(lstSeekNoType.SelectedValue,txtSeekTermIdOrTel.Text);
			SetCsvData(ds);
			Response.End();
		}
	}

	private void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0}\t{1}\t{2}",
							dr["NO_TYPE"].ToString(),
							dr["TERMINAL_UNIQUE_ID_OR_TEL"].ToString(),
							dr["REMARKS"].ToString());
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
}
