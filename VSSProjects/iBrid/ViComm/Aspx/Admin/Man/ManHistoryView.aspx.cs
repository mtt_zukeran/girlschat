﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員変更履歴検索
--	Progaram ID		: ManHistoryView
--
--  Creation Date	: 2010.04.15
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_ManHistoryView:System.Web.UI.Page {
	private string recCount = "";
	private string userHisSeq = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
			txtLoginId.Text = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["site"]);
			if (!txtLoginId.Text.Equals("")) {
				GetList();
			}
		}
	}

	protected void dsUserHistory_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		grdUserHistory.PageSize = 15;
		pnlInfo.Visible = false;
		pnlUserMan.Visible = false;
		pnlUrge.Visible = false;
		pnlReceipt.Visible = false;

		ClearField();
		lstSiteCd.DataBind();
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		txtLoginId.Text = "";

		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnManView_Click(object sender,EventArgs e) {
		Response.Redirect(string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",lstSiteCd.SelectedValue,txtLoginId.Text));
	}

	protected void lnkReasonView_Command(object sender,CommandEventArgs e) {
		userHisSeq = e.CommandArgument.ToString();

		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());

		pnlUrge.Visible = true;
		pnlUrge.DataBind();
		pnlUserMan.Visible = true;
		pnlUserMan.DataBind();
		pnlReceipt.Visible = true;
		pnlReceipt.DataBind();
	}

	private void GetList() {
		pnlUserMan.Visible = false;
		pnlUrge.Visible = false;
		pnlReceipt.Visible = false;

		grdUserHistory.PageIndex = 0;

		pnlInfo.Visible = true;
		grdUserHistory.DataSourceID = "dsUserHistory";
		grdUserHistory.DataBind();
		pnlCount.DataBind();
	}

	protected void dsUserHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = 0;
		e.InputParameters[2] = txtLoginId.Text;
		e.InputParameters[3] = "";
		e.InputParameters[4] = "";
		e.InputParameters[5] = "";
		e.InputParameters[6] = "";
	}

	protected void dsUserHistoryByGetOne_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = userHisSeq;
	}

	protected void dsUserHistoryByGetOne_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet ds = (DataSet)e.ReturnValue;
		if (ds == null) {
			return;
		}
	}

}
