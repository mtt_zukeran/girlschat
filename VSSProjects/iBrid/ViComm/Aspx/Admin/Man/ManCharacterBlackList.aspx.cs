﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員ブラックリスト設定
--	Progaram ID		: ManCharacterBlackList
--
--  Creation Date	: 2011.05.18
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Windows.Forms;

public partial class Man_ManCharacterBlackList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["site"]);
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sBlackType = iBridUtil.GetStringValue(Request.QueryString["blacktype"]);
			if (!sSiteCd.Equals(string.Empty) && !sLoginId.Equals(string.Empty) && !sBlackType.Equals(string.Empty)) {
				lstSeekSiteCd.SelectedValue = sSiteCd;
				lstSeekBlackType.SelectedValue = sBlackType;
				DataBind();
				btnRegist_Click(sender,e);
				txtLoginId.Text = sLoginId;
				btnSeek_Click(sender,e);
			}
		}
	}

	private void FirstLoad() {
		grdUserManCharacterBlack.PageSize = 100;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		lstSiteCd.DataSourceID = string.Empty;
		lstBlackType.DataSourceID = string.Empty;
		lstSeekSiteCd.DataSourceID = string.Empty;
		lstSeekBlackType.DataSourceID = string.Empty;
		txtLoginId.Text = string.Empty;

	}
	private void InitPage() {
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		recCount = "0";
		txtRemarks.Text = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdUserManCharacterBlack.PageIndex = 0;
		grdUserManCharacterBlack.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		if (IsPostBack) {
			if (!IsValid) {
				return;
			}
		}

		using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
			DataSet ds;
			ds = oUserManCharacter.GetOne(lstSiteCd.SelectedValue,txtLoginId.Text.TrimEnd());

			if (ds.Tables[0].Rows.Count != 0) {
				ViewState["USER_SEQ"] = ds.Tables[0].Rows[0]["USER_SEQ"].ToString();
			} else {
				return;
			}
		}

		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("MAN_CHARACTER_BLACK_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"]);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("PBLACK_TYPE",DbSession.DbType.VARCHAR2,lstBlackType.SelectedValue);
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtRemarks.Text = db.GetStringValue("PREMARKS");
			}
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedValue = lstSiteCd.SelectedValue;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAN_CHARACTER_BLACK_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("PBLACK_TYPE",DbSession.DbType.VARCHAR2,lstBlackType.SelectedValue);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedValue = lstSiteCd.SelectedValue;
	}


	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
		lstSiteCd.SelectedValue = lstSeekSiteCd.SelectedValue;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.Enabled = false;
		lstBlackType.Enabled = false;
		lstSiteCd.SelectedValue = lstSeekSiteCd.SelectedValue;
		lstBlackType.SelectedValue = lstSeekBlackType.SelectedValue;
		txtLoginId.Text = string.Empty;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}
	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}


	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
			args.IsValid = oUserManCharacter.IsExistLoginId(lstSiteCd.SelectedValue,txtLoginId.Text.TrimEnd(),ViCommConst.MAIN_CHAR_NO);
		}
	}
	protected void dsUserManCharacterBlack_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsUserManCharacterBlack_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = lstSeekBlackType.SelectedValue;
		e.InputParameters[2] = txtSeekLoginId.Text.TrimEnd();
	}
}
