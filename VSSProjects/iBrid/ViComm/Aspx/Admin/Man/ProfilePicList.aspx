<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProfilePicList.aspx.cs" Inherits="Man_ProfilePicList" Title="プロフィール画像"
	ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="プロフィール画像"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							ログインID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="80px" MaxLength="11"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							アップロード日付
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            〜&nbsp;
                            <asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ErrorMessage="アップロード日Fromを正しく入力して下さい。"
                                ControlToValidate="txtUploadDayFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdeUploadDayTo" runat="server" ErrorMessage="アップロード日付Toを正しく入力して下さい。"
                                ControlToValidate="txtUploadDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[プロフィール画像一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
				<asp:GridView ID="grdPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsManPic" SkinID="GridViewColor" DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ,LOGIN_ID">
					<Columns>
						<asp:TemplateField HeaderText="写真" SortExpression="PIC_SEQ">
							<ItemTemplate>
								<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# string.Format("../{0}?{1}",Eval("SMALL_PHOTO_IMG_PATH"),DateTime.Now.ToString("yyyyMMddHHmmss")) %>' OnClientClick='<%# GetClientScript(Eval("PHOTO_IMG_PATH"))  %>'>
								</asp:ImageButton>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ログインID">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
									Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="認証">
							<ItemStyle HorizontalAlign="Center" Width="50px" />
							<ItemTemplate>
								<asp:Button ID="btnAuth" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnAuth_OnClick" Text="認証" OnClientClick="return confirm('認証を行いますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="顔認証">
							<ItemStyle HorizontalAlign="Center" Width="50px" />
							<ItemTemplate>
								<asp:Button ID="btnAuthFace" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnAuthFace_OnClick" Text="顔認証" OnClientClick="return confirm('顔写真画像として認証を行いますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="削除">
							<ItemStyle HorizontalAlign="Center" Width="50px" />
							<ItemTemplate>
								<asp:Button ID="btnDelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnDelete_OnClick" Text="削除" OnClientClick="return confirm('削除を行いますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdPic.PageIndex + 1%>
					of
					<%=grdPic.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsManPic" runat="server" SelectMethod="GetProfilePicPageCollection" TypeName="UserManCharacter" SelectCountMethod="GetProfilePicPageCount"
		EnablePaging="True" OnSelected="dsManPic_Selected" OnSelecting="dsManPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUnAuthFlag" Type="Int16" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pUploadDateFrom" Type="String" />
			<asp:Parameter Name="pUploadDateTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
        TargetControlID="vdeUploadDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="vdeUploadDayTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtUploadDayFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtUploadDayTo">
    </ajaxToolkit:MaskedEditExtender>
</asp:Content>
