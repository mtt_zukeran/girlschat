﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 逆プロフィール一覧
--	Progaram ID		: GProfileInquiry
--
--  Creation Date	: 2010.04.20
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_GProfileInquiry:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsRecording_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdRecording.PageSize = 100;
		pnlInfo.Visible = false;
		ClearField();
		lstSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_MANAGER);
		grdRecording.Columns[2].Visible = (iCompare > 0);
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		grdRecording.PageIndex = 0;

		pnlInfo.Visible = true;
		grdRecording.DataSourceID = "dsRecording";
		grdRecording.DataBind();
		pnlCount.DataBind();		
	}

	protected void dsRecording_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = ViCommConst.REC_TYPE_PROFILE;
	}
}
