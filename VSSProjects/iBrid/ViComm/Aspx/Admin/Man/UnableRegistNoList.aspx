<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UnableRegistNoList.aspx.cs" Inherits="Man_UnableRegistNoList"
	Title="登録不可番号設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="登録不可番号設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 700px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									番号種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstNoType" runat="server" DataSourceID="dsNoType" DataTextField="CODE_NM" DataValueField="CODE" Width="110px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									固体識別または電話番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTermIdOrTel" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrTermIdOrTel" runat="server" ErrorMessage="固体識別または電話番号を入力して下さい。" ControlToValidate="txtTermIdOrTel" ValidationGroup="Key">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[内容]</legend>
						<table border="0" style="width: 700px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									備考
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRemarks" runat="server" MaxLength="80" Width="500px"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[登録不可番号一覧]</legend>
			<table border="0" style="width: 700px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						番号種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekNoType" runat="server" DataSourceID="dsNoType" DataTextField="CODE_NM" DataValueField="CODE" Width="110px">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						固体識別または電話番号
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtSeekTermIdOrTel" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="追加・修正" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			<br />
			<br />
			<asp:GridView ID="grdUnableRegistNo" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUnableRegistNo" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:BoundField DataField="NO_TYPE_NM" HeaderText="番号種別" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkTermIdOrTel" runat="server" Text='<%# Eval("TERMINAL_UNIQUE_ID_OR_TEL") %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("NO_TYPE"),Eval("TERMINAL_UNIQUE_ID_OR_TEL"))  %>'
								OnCommand="lnkTermIdOrTel_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							登録不可番号
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="REMARKS" HeaderText="備考" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdUnableRegistNo.PageIndex + 1%>
					of
					<%=grdUnableRegistNo.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUnableRegistNo" runat="server" SelectMethod="GetPageCollection" TypeName="UnableRegistNo" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsUnableRegistNo_Selected" OnSelecting="dsUnableRegistNo_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pNoType" Type="String" />
			<asp:Parameter Name="pTermIdOrTel" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNoType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="99" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrTermIdOrTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
