﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ストック動画認証
--	Progaram ID		: StockMovieMainte
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_StockMovieMainte : System.Web.UI.Page {

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}
	private string UserSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["UserSeq"]); }
		set { this.ViewState["UserSeq"] = value; }
	}
	private string UserCharNo {
		get { return iBridUtil.GetStringValue(this.ViewState["UserCharNo"]); }
		set { this.ViewState["UserCharNo"] = value; }
	}
	private string LoginId {
		get { return iBridUtil.GetStringValue(this.ViewState["LoginId"]); }
		set { this.ViewState["LoginId"] = value; }
	}
	private string MovieSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["MovieSeq"]); }
		set { this.ViewState["MovieSeq"] = value; }
	}
	private string ObjNotPublishFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["ObjNotPublishFlag"]); }
		set { this.ViewState["ObjNotPublishFlag"] = value; }
	}
	private string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		set { this.ViewState["Rowid"] = value; }
	}
	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}
	private string ReturnPage {
		get { return iBridUtil.GetStringValue(this.ViewState["ReturnPage"]); }
		set { this.ViewState["ReturnPage"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		this.SiteCd = Request.QueryString["sitecd"];
		this.UserSeq = Request.QueryString["userseq"];
		this.UserCharNo = Request.QueryString["usercharno"];
		this.MovieSeq = Request.QueryString["movieseq"];
		this.LoginId = Request.QueryString["loginid"];
		this.ReturnPage = Request.QueryString["return"];
		this.ObjNotPublishFlag = Request.QueryString["closed"];

		lblSiteCd.Text = this.SiteCd;
		lblUserSeq.Text = this.UserSeq;
		lblUserCharNo.Text = this.UserCharNo;
		lblLoginId.Text = this.LoginId;

		using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
			if (oUserManCharacter.IsExistLoginId(this.SiteCd,this.LoginId,this.UserCharNo)) {
				lblHandleNm.Text = oUserManCharacter.handleNm;
			}
		}

		ClearField();
		DataBind();
		GetData();
	}

	private void ClearField() {
		txtMovieTitle.Text = string.Empty;
		lstAuthType.SelectedIndex = 0;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_MAN_MOVIE_GET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.UserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.UserCharNo);
			db.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.VARCHAR2,this.MovieSeq);
			db.ProcedureOutParm("pMOVIE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pMOVIE_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.RevisionNo = db.GetStringValue("PREVISION_NO");
			this.Rowid = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMovieTitle.Text = db.GetStringValue("PMOVIE_TITLE");
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
	}

	private void UpdateData() {
		int iNotApproveFlag = 0;
		int iNotPublishFlag = 0;

		switch (lstAuthType.SelectedValue) {
			case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
				iNotApproveFlag = 1;
				iNotPublishFlag = 0;
				break;

			case ViCommConst.MOVIE_APPLY_OK:	// 公開
				iNotApproveFlag = 0;
				iNotPublishFlag = 0;
				break;

			case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
				throw new ArgumentException("ストック動画の削除はできません。");

			case ViCommConst.MOVIE_APPLY_NG:	// 非公開
				iNotApproveFlag = 0;
				iNotPublishFlag = 1;
				break;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("USER_MAN_MOVIE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.UserSeq);
			oDbSession.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.VARCHAR2,this.MovieSeq);
			oDbSession.ProcedureInParm("pMOVIE_TITLE",DbSession.DbType.VARCHAR2,this.txtMovieTitle.Text);
			oDbSession.ProcedureInParm("pMOVIE_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
			oDbSession.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,this.Rowid);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			oDbSession.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			oDbSession.ExecuteProcedure();
		}

		ReturnToCall();
	}

	private string GetMovieTitle(string name) {
		byte[] byteStr = System.Text.Encoding.UTF8.GetBytes(name);
		string ret = name;
		if (byteStr.Length > 90) {
			ret = System.Text.Encoding.UTF8.GetString(byteStr,0,90);
		}
		if (System.Text.Encoding.UTF8.GetBytes(ret).Length > 90) {
			ret = ret.Substring(0,ret.Length - 1);
		}
		return ret;
	}

	private void ReturnToCall() {
		if (this.ReturnPage.Equals("StockMovieCheckList")) {
			Server.Transfer(string.Format("../Man/StockMovieCheckList.aspx?sitecd={0}&closed={1}",this.SiteCd,this.ObjNotPublishFlag));

		} else if (this.ReturnPage.Equals("StockMovieOpenList")) {
			Server.Transfer(string.Format("../Man/StockMovieOpenList.aspx?sitecd={0}",this.SiteCd));

		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}",lblLoginId.Text,this.SiteCd));
		}
	}

	protected void lstAuthType_DataBound(object sender,EventArgs e) {
		ListItem oItemDel = this.lstAuthType.Items.FindByValue(ViCommConst.MOVIE_APPLY_REMOVE);
		if (oItemDel != null) {
			this.lstAuthType.Items.Remove(oItemDel);
		}
	}
}
