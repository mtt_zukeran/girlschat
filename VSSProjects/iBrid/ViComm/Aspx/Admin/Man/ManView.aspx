<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManView.aspx.cs" Inherits="Man_ManView" Title="男性会員詳細" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="System" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性会員詳細"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlSeek">
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0">
					<tr>
						<td valign="top">
							<fieldset class="fieldset" style="padding: 0px 10px 4px 10px;">
								<legend>[検索条件]</legend>
								<table border="0" style="width: 600px" class="tableStyle">
									<tr>
										<td class="tdHeaderSmallStyle">
											サイトコード
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
											</asp:DropDownList>
										</td>
										<td class="tdHeaderSmallStyle">
											履歴日
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="66px"></asp:TextBox>&nbsp;〜
											<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="66px"></asp:TextBox>
											<asp:CheckBox ID="chkReportDayEnable" runat="server" Text="有効" />
											<asp:RangeValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="履歴日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
												MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
											<asp:RangeValidator ID="vdrReportDayTo" runat="server" ErrorMessage="履歴日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
												Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
											<asp:CompareValidator ID="vdcReportDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportDayFrom" ControlToValidate="txtReportDayTo"
												Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderSmallStyle">
											電話番号・ＩＤ
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtLoginId" runat="server" Width="80px" MaxLength="11"></asp:TextBox>
											<asp:Label ID="lblUserSeq" runat="server" Text="" Visible="false"></asp:Label>
										</td>
										<td class="tdHeaderSmallStyle2">
											ﾒｰﾙｱﾄﾞﾚｽ
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtEMailAddr" runat="server" Width="200px" MaxLength="64"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderSmallStyle2">
											ﾕｰｻﾞｰSEQ
										</td>
										<td class="tdDataStyle" colspan="3">
											<asp:TextBox ID="txtUserSeq" runat="server" Width="80px" MaxLength="15"></asp:TextBox>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
								<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
								<asp:Button ID="btnBack" runat="server" Text="戻る" CssClass="seekbutton" OnClientClick="history.back();return false;" />
								<asp:CustomValidator ID="vrdLoginId" runat="server" ErrorMessage="CustomValidator" OnServerValidate="vrdLoginId_ServerValidate" ValidationGroup="Key" Display="Dynamic">検索条件を入力して下さい。</asp:CustomValidator>
								<asp:Label ID="lblNotFound" runat="server" Text="指定された会員は存在しません。" ForeColor="Red" Font-Bold="true"></asp:Label>
								<asp:Label ID="lblManyUser" runat="server" Text="該当の会員は複数存在します。検索内容を変更して下さい。" ForeColor="Red" Font-Bold="true"></asp:Label>
							</fieldset>
						</td>
						<td valign="top">
							<fieldset class="fieldset" style="padding: 0px 5px 4px 5px; height: 90px">
								<legend>[稼動状況]</legend>
								<table style="line-height: 150%; text-align: left; vertical-align: top" cellspacing="0">
									<tr>
										<td>
											<asp:LinkButton runat="server" ID="lnkReceiptHis" Text="入金履歴" Width="100px" ValidationGroup="Key" OnClick="btnReceiptHis_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkTalkHis" Text="利用履歴" Width="100px" ValidationGroup="Key" OnClick="btnTalkHis_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkUserHistory" Text="変更履歴" Width="100px" ValidationGroup="Key" OnClick="btnUserHistory_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkMarking" Text="足あと" Width="100px" ValidationGroup="Key" OnClick="btnMarking_Click" />
										</td>
									</tr>
									<tr>
										<td>
											<asp:LinkButton runat="server" ID="lnkFavorit" Text="お気に入り" Width="100px" ValidationGroup="Key" OnClick="btnFavorit_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="Button1" Text="お気に入られ" Width="100px" ValidationGroup="Key" OnClick="btnLikeMe_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="Button2" Text="拒否している" Width="100px" ValidationGroup="Key" OnClick="btnRefuse_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="Button3" Text="拒否されている" Width="100px" ValidationGroup="Key" OnClick="btnRefuseMe_Click" />
										</td>
									</tr>
									<tr>
										<td>
											<asp:LinkButton runat="server" ID="lnkMailBox" Text="ﾒｰﾙBOX" Width="100px" ValidationGroup="Key" OnClick="btnMailBox_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkCreateMail" Text="ﾒｰﾙ作成" Width="100px" ValidationGroup="Key" OnClick="btnCreateMail_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkEmail" Text="変更履歴/送信NG" Width="120px" ValidationGroup="Key" OnClick="btnModifyMailHis_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkBbs" Text="掲示板履歴" Width="100px" ValidationGroup="Key" OnClick="btnBbs_Click" />
										</td>
									</tr>
									<tr>
										<td>
											<asp:LinkButton runat="server" ID="lnkFriends" Text="お友達紹介" Width="100px" ValidationGroup="Key" OnClick="btnFriends_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkUserDefPointHistory" Text="定義ﾎﾟｲﾝﾄ履歴" Width="100px" ValidationGroup="Key" OnClick="btnUserDefPointHistory_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="lnkProductBuy" Text="商品購入履歴" Width="100px" ValidationGroup="Key" OnClick="btnProductBuy_Click" />
											<asp:LinkButton runat="server" ID="lnkFanClubFee" Text="FC会費" Width="100px" ValidationGroup="Key" OnClick="btnFanClubFee_Click" />
										</td>
										<td>
											<asp:LinkButton runat="server" ID="LinkTweet" Text="会員つぶやき履歴" Width="120px" ValidationGroup="Key" OnClick="btnTweet_Click" />
										</td>
									</tr>
									<tr>
										<td>
											<asp:LinkButton runat="server" ID="lnkWithdrawalHis" Text="退会履歴" Width="100px" ValidationGroup="Key" OnClick="lnkWithdrawalHis_Click" />
										</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlWithdrawal" HorizontalAlign="Left" Visible="false" Width="400px">
				<table border="0">
					<tr>
						<td>
							<fieldset class="fieldset-inner">
								<legend>[退会申請]</legend>
								<table border="0" class="tableStyleAutoSize">
									<tr>
										<td class="tdHeaderStyle">
											申請状態
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblWithdrawalStatus" runat="server" Text="申請"></asp:Label><asp:Label ID="lblWithdrawalSeq" runat="server" Text="" Visible="false"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											申請日時
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblWithdrawalDate" runat="server" Text="yyyy/mm/dd hh:ss"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											理由区分
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblWithdrawalReasonCd" runat="server" Text="申請"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											詳細理由
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtWithdrawalReasonDoc" runat="server" Width="250px" TextMode="MultiLine" Height="100px" ReadOnly="true"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											備考
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtWithdrawalRemarks" runat="server" Width="250px" TextMode="MultiLine" Height="100px"></asp:TextBox>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnReturnWithdrawalList" Text="申請履歴に戻る" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnReturnWithdrawalList_Click" />
								<asp:Button runat="server" ID="btnUpdateWithdrawal" Text="備考更新" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnUpdateWithdrawal_Click" />
								<asp:Button runat="server" ID="btnDelWithdrawal" Text="申請削除" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnDelWithdrawal_Click" />
								<asp:Button runat="server" ID="btnAddPoint300" Text="300円" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnAddPoint300_Click" />
								<asp:Button runat="server" ID="btnAddPoint500" Text="500円" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnAddPoint500_Click" />
								<asp:Button runat="server" ID="btnAddPoint1000" Text="1000円" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnAddPoint1000_Click" />
								<asp:Button runat="server" ID="btnAcceptWithdrawal" Text="退会受理" CssClass="seekrightbutton" ValidationGroup="Withdrawal" OnClick="btnAcceptWithdrawal_Click" />
							</fieldset>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlWithdrawalHistoryDetail" Visible="false">
				<fieldset class="fieldset-inner">
					<legend>[退会履歴]</legend>
					<asp:Button ID="btnCloseWithdrawalHistoryDetail" runat="server" Text="閉じる" OnClick="btnCloseWithdrawalHistoryDetail_Click" />
					<table border="0" class="tableStyleAutoSize"">
						<tr>
							<td class="tdHeaderStyle">
								退会処理日
							</td>
							<td class="tdDataStyle">
                                <asp:DropDownList ID="lstWithdrawalYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstWithdrawalMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstWithdrawalDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstWithdrawalHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstWithdrawalMI" runat="server" Width="40px">
                                </asp:DropDownList>分
								<asp:Label ID="lblWithdrawalSS" runat="server" Visible="false"></asp:Label>
								<asp:Label ID="lblWithdrawalHistorySeq" runat="server" Visible="false"></asp:Label>
								<asp:CustomValidator ID="vrdWithdrawalHistoryDate" runat="server" ControlToValidate="lstWithdrawalMI" ErrorMessage="退会処理日を正しく入力して下さい。" 
									OnServerValidate="vrdWithdrawalHistoryDate_ServerValidate" ValidationGroup="WithdrawalHistory" Display="Dynamic" />
								<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vrdWithdrawalHistoryDate" HighlightCssClass="validatorCallout" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								備考
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtWithdrawalHistoryRemarks" runat="server" Width="500px" Height="48px" TextMode="MultiLine"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrWithdrawalHistoryRemarks" runat="server" ErrorMessage="備考を入力してください。" ControlToValidate="txtWithdrawalHistoryRemarks" ValidationGroup="WithdrawalHistory"
									Display="Dynamic">*</asp:RequiredFieldValidator>
								<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdrWithdrawalHistoryRemarks" HighlightCssClass="validatorCallout" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								退会時ポイント付与回数
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtWithdrawalHistoryAddPointCount" runat="server" Width="70px" MaxLength="4"></asp:TextBox>
								<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWithdrawalHistoryAddPointCount" />
								<asp:RegularExpressionValidator ID="vdeWithdrawalHistoryAddPointCount" runat="server" ErrorMessage="退会時ポイント付与回数は数値で入力してください。" ControlToValidate="txtWithdrawalHistoryAddPointCount" ValidationExpression="^\d+$"
									ValidationGroup="WithdrawalHistory" Display="Dynamic">*</asp:RegularExpressionValidator>
								<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdeWithdrawalHistoryAddPointCount" HighlightCssClass="validatorCallout" />
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnRegistWithdrawalHistory" Text="登録" CssClass="seekbutton" OnClick="btnRegWithdrawalHistory_Click" Visible="false" ValidationGroup="WithdrawalHistory" />
					<asp:Button runat="server" ID="btnUpdateWithdrawalHistory" Text="更新" CssClass="seekbutton" OnClick="btnUpWithdrawalHistory_Click" Visible="false" ValidationGroup="WithdrawalHistory" />
					<asp:Button runat="server" ID="btnDeleteWithdrawalHistory" Text="削除" CssClass="seekbutton" OnClick="btnDelWithdrawalHistory_Click" Visible="false" ValidationGroup="WithdrawalHistory" />
					<br />
				</fieldset>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlWithdrawalHistoryList" Visible="false">
				<fieldset class="fieldset-inner">
					<legend>[退会履歴一覧]</legend>
					<asp:Button ID="btnNewWithdrawalHistory" runat="server" Text="新規登録" OnClick="btnNewWithdrawalHistory_Click" />
					<asp:Button ID="btnCloseWithdrawalHistoryList" runat="server" Text="閉じる" OnClick="btnCloseWithdrawalHistoryList_Click" />
					<asp:GridView ID="grdWithdrawalHistoryList" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsWithdrawalHistoryList" AllowSorting="True" SkinID="GridViewColor"
						PageSize="30">
						<Columns>
							<asp:TemplateField HeaderText="退会処理日時">
								<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# DateTime.Parse(Eval("CREATE_DATE").ToString()).ToString("yyyy/MM/dd HH:mm:ss") %>'></asp:Label><br />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="備考">
								<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRemarks" runat="server" Text='<%# GetWithdrawalHistoryRemarks(Eval("REMARKS"),Eval("WITHDRAWAL_ADD_POINT_CNT"))%>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="">
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="false" />
								<ItemTemplate>
									<asp:LinkButton ID="lnkEditWithdrawalHistory" runat="server" CommandArgument='<%# Eval("WITHDRAWAL_HISTORY_SEQ") %>' Text="編集" OnCommand="lnkEditWithdrawalHistory_Command"></asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</fieldset>
			</asp:Panel>

			<asp:Panel runat="server" ID="pnlBingoEntry" HorizontalAlign="Left" Visible="false" Width="400px">
				<table border="0">
					<tr>
						<td>
							<fieldset class="fieldset-inner">
								<legend>[ﾒｰﾙdeﾋﾞﾝｺﾞ]</legend>
								<table border="0" class="tableStyleAutoSize">
									<tr>
										<td class="tdHeaderStyle">
											ﾋﾞﾝｺﾞｶｰﾄﾞ
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoCardNo" runat="server"></asp:Label>&nbsp;枚目
											<asp:Label ID="lblBingoEntrySeq" runat="server" Visible="false"></asp:Label>
											<asp:Label ID="lblBingoTermSeq" runat="server" Visible="false"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											挑戦回数
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoCharangeCount" runat="server"></asp:Label>&nbsp;回
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											付与前のｼﾞｬｯｸﾎﾟｯﾄ金額
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoCompliateJackpotBefore" runat="server"></asp:Label>&nbsp;円
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											付与PT
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoElectionPoint" runat="server"></asp:Label>&nbsp;PT
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											付与金額
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoElectionAmt" runat="server"></asp:Label>&nbsp;円
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											付与後のｼﾞｬｯｸﾎﾟｯﾄ金額
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoCompliateJackpotAfter" runat="server"></asp:Label>&nbsp;円
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ｴﾝﾄﾘｰ日時
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoEntryDate" runat="server" Text=""></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ﾋﾞﾝｺﾞ日時
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBingoApplicationDate" runat="server" Text=""></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											pt付与
										</td>
										<td class="tdDataStyle">
											<asp:RadioButton ID="rdoBingoNotPointPayment" runat="server" Text="未" GroupName="AdGrouping" ForeColor="red" Checked="true" />
											<asp:RadioButton ID="rdoBingoPointPayment" runat="server" Text="済" GroupName="AdGrouping" />
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnBingoEntryMainte" Text="更新" CssClass="seekrightbutton" OnClick="btnBingoEntryMainte_Click" />
								<asp:Button runat="server" ID="btnReturnBingoEntryList" Text="一覧に戻る" CssClass="seekbutton" OnClick="btnReturnBingoEntryList_Click" />
							</fieldset>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlExtInfo" HorizontalAlign="Left" Visible="false">
				<table border="0">
					<tr>
						<td>
							<asp:Panel runat="server" ID="pnlReceiptHis" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[入金履歴]</legend>
									<asp:Panel ID="pnlReceiptCancelMsg" runat="server" Visible="false">
										<font>※会員が通話中の場合、または取消しに必要なポイントが不足している場合は取消しできません(「取消」リンクが表示されません)。 </font>
									</asp:Panel>
									<asp:GridView ID="grdReceipt" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsReceipt" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30">
										<Columns>
											<asp:TemplateField HeaderText="サイト名" Visible="False">
												<ItemTemplate>
													<asp:Label ID="lblSiteNm" runat="server" Text='<%# Bind("SITE_NM") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>" ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="入金日">
												<ItemTemplate>
													<asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CREATE_DATE", "{0:yy/MM/dd HH:mm}") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>"
														ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="種別">
												<ItemTemplate>
													<asp:Label ID="lblSettleTypeNm" runat="server" Text='<%# Bind("SETTLE_TYPE_NM") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>" ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Left" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="金額">
												<ItemTemplate>
													<asp:Label ID="lblReceiptAmt" runat="server" Text='<%# Bind("RECEIPT_AMT") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>" ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Right" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="入金者">
												<ItemTemplate>
													<asp:Label ID="lblReceiptPerson" runat="server" Text='<%# Bind("RECEIPT_PERSON") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>" ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Left" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="入金Pt">
												<ItemTemplate>
													<asp:Label ID="lblExPoint" runat="server" Text='<%# Bind("EX_POINT") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>" ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Right" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="ｻｰﾋﾞｽPt">
												<ItemTemplate>
													<asp:Label ID="lblServicePoint" runat="server" Text='<%# Bind("SERVICE_POINT") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>" ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Right" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="入金前Pt">
												<ItemTemplate>
													<asp:Label ID="lblBeforeReceiptPoint" runat="server" Text='<%# Bind("BEFORE_RECEIPT_POINT") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>"
														ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Right" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="入金後Pt">
												<ItemTemplate>
													<asp:Label ID="lblAfterReceiptPoint" runat="server" Text='<%# Bind("AFTER_RECEIPT_POINT") %>' Font-Strikeout="<%#IsReceiptCanceled(Container.DataItem) %>"
														ForeColor="<%# GetReceiptForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Right" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="取消">
												<ItemTemplate>
													<asp:LinkButton ID="lnkCancelReceipt" runat="server" CausesValidation="False" CommandArgument='<%#Eval("RECEIPT_SEQ") %>' OnCommand="lnkCancelReceipt_Command"
														Text="取消" Visible='<%# !IsReceiptCanceled(Container.DataItem) && IsCancelReceiptVisible(Container.DataItem) %>' OnClientClick="return confirm('入金を取消しますか？');"></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="復帰">
												<ItemTemplate>
													<asp:LinkButton ID="lnkRevertReceipt" runat="server" CommandArgument='<%#Eval("RECEIPT_SEQ") %>' OnCommand="lnkRevertReceipt_Command" Text="復帰" Visible='<%# IsReceiptCanceled(Container.DataItem) %>'
														OnClientClick="return confirm('取消した入金履歴を復帰しますか？');"></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="btnCloseExt1" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlSettleHis" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[入金履歴]</legend>
									<asp:GridView ID="grdSettle" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSettle" AllowSorting="True" SkinID="GridViewColor">
										<Columns>
											<asp:BoundField DataField="SITE_NM" HeaderText="サイト名" Visible="False">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="CREATE_DATE" HeaderText="入金日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="SETTLE_TYPE_NM" HeaderText="種別">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:BoundField DataField="SETTLE_AMT" HeaderText="金額">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="RECEIPT_PERSON" HeaderText="入金者">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:BoundField DataField="EX_POINT" HeaderText="入金Pt">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="SERVICE_POINT" HeaderText="ｻｰﾋﾞｽPt">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="BEFORE_RECEIPT_POINT" HeaderText="入金前Pt">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="AFTER_RECEIPT_POINT" HeaderText="入金後Pt">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="SETTLE_STATUS_NM" HeaderText="決済ステータス">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="削除">
												<ItemTemplate>
													<asp:LinkButton ID="lnkCancelReceipt" runat="server" CausesValidation="False" CommandArgument='<%#Eval("RECEIPT_SEQ") %>' OnCommand="lnkCancelReceipt_Command"
														Text="削除" Visible='<%# IsCancelReceiptVisible(Container.DataItem) %>' OnClientClick="return confirm('入金を削除しますか？');"></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="btnCloseSettle" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlUsedHis" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[利用履歴]</legend>
									<table border="0" class="SmallTableStyle">
										<tr>
											<td class="tdHeaderSmallStyle">
												相手ＩＤ
											</td>
											<td class="tdDataStyle" align="center">
												<asp:TextBox ID="txtTalkPartnerId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
											</td>
											<td>
											</td>
											<td align="center">
											</td>
										</tr>
									</table>
									<table>
										<tr valign="top">
											<td>
												<asp:GridView ID="grdWebUsedLogSummary" runat="server" AutoGenerateColumns="False" DataSourceID="dsWebUsedLogSummary" SkinID="GridViewColor">
													<Columns>
														<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="種別" HtmlEncode="False">
															<ItemStyle HorizontalAlign="Left" />
														</asp:BoundField>
														<asp:BoundField DataField="SUMMARY" DataFormatString="{0:#,##0}" HeaderText="課金ポイント計(WEB)" HtmlEncode="False">
															<ItemStyle HorizontalAlign="Right" />
														</asp:BoundField>
													</Columns>
												</asp:GridView>
											</td>
											<td>
												&nbsp;</td>
											<td>
												<asp:GridView ID="grdTalkHistorySummary" runat="server" AutoGenerateColumns="False" DataSourceID="dsUsedLogTalkSummary" SkinID="GridViewColor">
													<Columns>
														<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="会話種別" HtmlEncode="False">
															<ItemStyle HorizontalAlign="Left" />
														</asp:BoundField>
														<asp:BoundField DataField="SUMMARY" DataFormatString="{0:#,##0}" HeaderText="課金ポイント計(会話)" HtmlEncode="False">
															<ItemStyle HorizontalAlign="Right" />
														</asp:BoundField>
														<asp:BoundField DataField="INVITE_SUMMARY" DataFormatString="{0:#,##0}" HeaderText="招待ポイント計" HtmlEncode="False">
															<ItemStyle HorizontalAlign="Right" />
														</asp:BoundField>
													</Columns>
												</asp:GridView>
												<asp:GridView ID="grdUsedLogMonitorSummary" runat="server" AutoGenerateColumns="False" DataSourceID="dsUsedLogMonitorSummary" SkinID="GridViewColor">
													<Columns>
														<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="モニタ種別" HtmlEncode="False">
															<ItemStyle HorizontalAlign="Left" />
														</asp:BoundField>
														<asp:BoundField DataField="SUMMARY" DataFormatString="{0:#,##0}" HeaderText="モニタポイント計" HtmlEncode="False">
															<ItemStyle HorizontalAlign="Right" />
														</asp:BoundField>
													</Columns>
												</asp:GridView>
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnSeekTalkHis" Text="検索" CssClass="seekbutton" OnClick="btnSeekTalkHis_Click" />
									<asp:Button runat="server" ID="btnCloseExt2" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
								</fieldset>
								<fieldset class="fieldset-inner">
									<legend>[履歴一覧]</legend>
									<table>
										<tr valign="top">
											<td>
												<asp:GridView ID="grdWebUsedLog" runat="server" AllowPaging="True" AutoGenerateColumns="False" Font-Size="x-Small" DataSourceID="dsWebUsedLog" AllowSorting="True"
													SkinID="GridViewColor" PageSize="30">
													<Columns>
														<asp:TemplateField HeaderText="利用日時">
															<ItemTemplate>
																<asp:Label ID="Label3" runat="server" Text='<%# Eval("CHARGE_START_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>&nbsp;
															</ItemTemplate>
															<ItemStyle HorizontalAlign="Center" />
														</asp:TemplateField>
														<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="種別">
															<ItemStyle HorizontalAlign="Left" />
														</asp:BoundField>
														<asp:TemplateField HeaderText="ハンドル名">
															<ItemStyle HorizontalAlign="Left" />
															<ItemTemplate>
																<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("CAST_LOGIN_ID"),"ManView.aspx",Eval("MAN_LOGIN_ID"),Eval("CAST_SITE_CD")) %>'
																	Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateField>
														<asp:TemplateField HeaderText="会員ID">
															<ItemTemplate>
																<asp:Label ID="lblCastLoginId" runat="server" Text='<%# Eval("CAST_LOGIN_ID") %>'></asp:Label>
															</ItemTemplate>
															<ItemStyle HorizontalAlign="Center" />
														</asp:TemplateField>
														<asp:BoundField DataField="CHARGE_POINT" HeaderText="Pt">
															<ItemStyle HorizontalAlign="Right" />
														</asp:BoundField>
													</Columns>
													<PagerSettings Mode="NumericFirstLast" />
												</asp:GridView>
											</td>
											<td>
												<asp:Panel runat="server" ID="pnlTalkHis">
													<asp:GridView ID="grdTalkHistory" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="dsUsedLogTalk" Font-Size="x-Small"
														PageSize="30" SkinID="GridViewColor">
														<Columns>
															<asp:TemplateField HeaderText="会話日時">
																<ItemTemplate>
																	<asp:Label ID="lblTalkStartDate" runat="server" Text='<%# Eval("CHARGE_START_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'>
																	</asp:Label>
																	&nbsp;
																	<asp:Label ID="lblTalkEndDate" runat="server" Text='<%# Eval("CHARGE_END_DATE", "{0:HH:mm:ss}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateField>
															<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="会話種別">
																<ItemStyle HorizontalAlign="Left" />
															</asp:BoundField>
															<asp:TemplateField HeaderText="ハンドル名">
																<ItemStyle HorizontalAlign="Left" />
																<ItemTemplate>
																	<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("CAST_LOGIN_ID"),"ManView.aspx",Eval("MAN_LOGIN_ID"),Eval("MAN_USER_SITE_CD")) %>'
																		Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
																</ItemTemplate>
															</asp:TemplateField>
															<asp:TemplateField HeaderText="出演者ID">
																<ItemTemplate>
																	<asp:Label ID="lblCastLoginId" runat="server" Text='<%# Eval("CAST_LOGIN_ID") %>'></asp:Label>
																</ItemTemplate>
																<ItemStyle HorizontalAlign="Center" />
															</asp:TemplateField>
															<asp:BoundField DataField="CHARGE_SEC" HeaderText="秒数">
																<ItemStyle HorizontalAlign="Right" />
															</asp:BoundField>
															<asp:BoundField DataField="CHARGE_POINT" HeaderText="Pt">
																<ItemStyle HorizontalAlign="Right" />
															</asp:BoundField>
															<asp:BoundField DataField="CHARGE_POINT_FREE_DIAL" HeaderText="Free">
																<ItemStyle HorizontalAlign="Right" />
															</asp:BoundField>
															<asp:BoundField DataField="INVITE_TALK_SERVICE_POINT" HeaderText="招待Pt">
																<ItemStyle HorizontalAlign="Right" />
															</asp:BoundField>
															<asp:BoundField DataField="CALL_RESULT_NM" HeaderText="通話終了理由" HtmlEncode="False" SortExpression="CALL_RESULT_NM">
																<ItemStyle HorizontalAlign="Left" />
																<FooterStyle HorizontalAlign="Center" />
															</asp:BoundField>
														</Columns>
														<PagerSettings Mode="NumericFirstLast" />
													</asp:GridView>
												</asp:Panel>
												<asp:Panel runat="server" ID="pnlUsedLogMonitor">
													<asp:GridView ID="grdUsedLogMonitor" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="dsUsedLogMonitor"
														Font-Size="x-Small" PageSize="10" SkinID="GridViewColor">
														<Columns>
															<asp:TemplateField HeaderText="モニタ日時">
																<ItemTemplate>
																	<asp:Label ID="lblTalkStartDate" runat="server" Text='<%# Eval("CHARGE_START_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'>
																	</asp:Label>
																	&nbsp;
																	<asp:Label ID="lblTalkEndDate" runat="server" Text='<%# Eval("CHARGE_END_DATE", "{0:HH:mm:ss}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateField>
															<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="モニタ種別">
																<ItemStyle HorizontalAlign="Left" />
															</asp:BoundField>
															<asp:TemplateField HeaderText="ハンドル名">
																<ItemStyle HorizontalAlign="Left" />
																<ItemTemplate>
																	<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("CAST_LOGIN_ID"),"ManView.aspx",Eval("MAN_LOGIN_ID"),Eval("MAN_USER_SITE_CD")) %>'
																		Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
																</ItemTemplate>
															</asp:TemplateField>
															<asp:TemplateField HeaderText="出演者ID">
																<ItemTemplate>
																	<asp:Label ID="lblCastLoginId" runat="server" Text='<%# Eval("CAST_LOGIN_ID") %>'></asp:Label>
																</ItemTemplate>
																<ItemStyle HorizontalAlign="Center" />
															</asp:TemplateField>
															<asp:BoundField DataField="CHARGE_SEC" HeaderText="秒数">
																<ItemStyle HorizontalAlign="Right" />
															</asp:BoundField>
															<asp:BoundField DataField="CHARGE_POINT" HeaderText="Pt">
																<ItemStyle HorizontalAlign="Right" />
															</asp:BoundField>
															<asp:BoundField DataField="CHARGE_POINT_FREE_DIAL" HeaderText="Free">
																<ItemStyle HorizontalAlign="Right" />
															</asp:BoundField>
														</Columns>
														<PagerSettings Mode="NumericFirstLast" />
													</asp:GridView>
												</asp:Panel>
											</td>
										</tr>
									</table>
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlFavorit" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[お気に入り]</legend>
									<asp:GridView ID="grdFavorit" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsFavorit" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30">
										<Columns>
											<asp:BoundField DataField="FAVORIT_REGIST_DATE" HeaderText="お気に入り登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
												<HeaderStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="ACT_CATEGORY_NM" HeaderText="カテゴリ">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("PARTNER_LOGIN_ID"),"ManView.aspx",Eval("LOGIN_ID"),Eval("SITE_CD")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="状態">
												<ItemTemplate>
													<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS"), Eval("DUMMY_TALK_FLAG"))%>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="FAVORIT_COMMENT" HeaderText="コメント">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="お気に入られ">
												<ItemTemplate>
													<asp:Label ID="lblLikeMe" runat="server" Text='<%# GetLikeMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="拒否されている">
												<ItemTemplate>
													<asp:Label ID="lblRefuseMe" runat="server" Text='<%# GetRefuseMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="btnCloseExt3" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
									<asp:Button runat="server" ID="btnSeekFavorit" Text="検索" CssClass="seekbutton" OnClick="btnSeekFavorit_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlLikeMe" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[お気に入られ]</legend>
									<asp:GridView ID="grdLikeMe" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsLikeMe" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30">
										<Columns>
											<asp:BoundField DataField="FAVORIT_REGIST_DATE" HeaderText="お気に入られ登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
												<HeaderStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="ACT_CATEGORY_NM" HeaderText="カテゴリ">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("LOGIN_ID"),"ManView.aspx",Eval("PARTNER_LOGIN_ID"),Eval("SITE_CD")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="状態">
												<ItemTemplate>
													<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS"), Eval("DUMMY_TALK_FLAG"))%>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="FAVORIT_COMMENT" HeaderText="コメント">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="Button4" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
									<asp:Button runat="server" ID="btnSeekLikeMe" Text="検索" CssClass="seekbutton" OnClick="btnSeekLikeMe_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlRefuse" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[拒否している]</legend>
									<asp:GridView ID="grdRefuse" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsRefuse" AllowSorting="True" SkinID="GridViewColor"
										PageSize="10">
										<Columns>
											<asp:BoundField DataField="FAVORIT_REGIST_DATE" HeaderText="拒否登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
												<HeaderStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="ACT_CATEGORY_NM" HeaderText="カテゴリ">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("PARTNER_LOGIN_ID"),"ManView.aspx",Eval("LOGIN_ID"),Eval("SITE_CD")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="状態">
												<ItemTemplate>
													<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS"), Eval("DUMMY_TALK_FLAG"))%>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="REFUSE_COMMENT" HeaderText="コメント">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="お気に入られ">
												<ItemTemplate>
													<asp:Label ID="lblLikeMe" runat="server" Text='<%# GetLikeMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="拒否されている">
												<ItemTemplate>
													<asp:Label ID="lblRefuseMe" runat="server" Text='<%# GetRefuseMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="Button5" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
									<asp:Button runat="server" ID="btnSeekRefuse" Text="検索" CssClass="seekbutton" OnClick="btnSeekRefuse_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlRefuseMe" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[拒否されている]</legend>
									<asp:GridView ID="grdRefuseMe" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsRefuseMe" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30">
										<Columns>
											<asp:BoundField DataField="FAVORIT_REGIST_DATE" HeaderText="拒否登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
												<HeaderStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="ACT_CATEGORY_NM" HeaderText="カテゴリ">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("LOGIN_ID"),"ManView.aspx",Eval("PARTNER_LOGIN_ID"),Eval("SITE_CD")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="状態">
												<ItemTemplate>
													<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS"), Eval("DUMMY_TALK_FLAG"))%>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="REFUSE_COMMENT" HeaderText="コメント">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="Button6" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
									<asp:Button runat="server" ID="btnSeekRefuseMe" Text="検索" CssClass="seekbutton" OnClick="btnSeekRefuseMe_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlMarking" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[足あと]</legend>
									<asp:GridView ID="grdMarking" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMarking" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30">
										<Columns>
											<asp:BoundField DataField="MARKING_DATE" HeaderText="足あと日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
												<HeaderStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="ACT_CATEGORY_NM" HeaderText="カテゴリ">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("LOGIN_ID"),"ManView.aspx",Eval("PARTNER_LOGIN_ID"),Eval("SITE_CD")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" />
											</asp:BoundField>
											<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="状態">
												<ItemTemplate>
													<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS"), Eval("DUMMY_TALK_FLAG"))%>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="btnCloseExt4" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
									<asp:Button runat="server" ID="btnSeekMarking" Text="検索" CssClass="seekbutton" OnClick="btnSeekMarking_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlMailBox" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[メールBOX]</legend>
									<table border="0" class="tableStyle"">
										<tr>
											<td class="tdHeaderStyle">
												履歴開始日
											</td>
											<td class="tdDataStyle">
												<asp:DropDownList ID="lstMailFromYYYY" runat="server" Width="60px">
												</asp:DropDownList>年
												<asp:DropDownList ID="lstMailFromMM" runat="server" Width="40px">
												</asp:DropDownList>月
												<asp:DropDownList ID="lstMailFromDD" runat="server" Width="40px">
												</asp:DropDownList>日
												<asp:DropDownList ID="lstMailFromHH" runat="server" Width="40px">
												</asp:DropDownList>時
											</td>
											<td class="tdHeaderStyle">
												履歴終了日
											</td>
											<td class="tdDataStyle">
												<asp:DropDownList ID="lstMailToYYYY" runat="server" Width="60px">
												</asp:DropDownList>年
												<asp:DropDownList ID="lstMailToMM" runat="server" Width="40px">
												</asp:DropDownList>月
												<asp:DropDownList ID="lstMailToDD" runat="server" Width="40px">
												</asp:DropDownList>日
												<asp:DropDownList ID="lstMailToHH" runat="server" Width="40px">
												</asp:DropDownList>時
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												出演者ID・ｷｬﾗｸﾀｰNo
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtMailPartnerLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
												<asp:TextBox ID="txtMailPartnerCharNo" runat="server" Width="20px" MaxLength="2" Visible="false"></asp:TextBox>
											</td>
											<td class="tdHeaderStyle">
												送受信
											</td>
											<td class="tdDataStyle">
												<asp:RadioButton ID="rdoTxRxType" runat="server" Text="送受信" GroupName="TxRxTypeGrouping" Checked="true" />
												<asp:RadioButton ID="rdoTxType" runat="server" Text="送信" GroupName="TxRxTypeGrouping" />
												<asp:RadioButton ID="rdoRxType" runat="server" Text="受信" GroupName="TxRxTypeGrouping" />
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle2">
												添付種別
											</td>
											<td class="tdDataStyle">
												<asp:CheckBoxList ID="chkAttachedType" runat="server" RepeatDirection="horizontal">
													<asp:ListItem Text="画像添付" Value="1"></asp:ListItem>
													<asp:ListItem Text="動画添付" Value="2"></asp:ListItem>
												</asp:CheckBoxList>
											</td>
											<td class="tdHeaderStyle2">
												一括送信
											</td>
											<td class="tdDataStyle">
												<asp:CheckBox ID="chkWithBatchMail" runat="server" Text="一括ﾒｰﾙ送信を表示する" />
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnSeekMailBox" Text="検索" CssClass="seekbutton" OnClick="btnSeekMailBox_Click" ValidationGroup="MailBox" />
									<asp:Button runat="server" ID="btnCloseExt5" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
									<asp:CustomValidator ID="vdcMailBox" runat="server" ErrorMessage="日時を正しく入力してください" OnServerValidate="vdcMailBox_ServerValidate" ValidationGroup="MailBox"></asp:CustomValidator>
									<br />
									<br />
									<asp:GridView ID="grdMailBox" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailBox" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30" OnRowDataBound="grdMailBox_RowDataBound">
										<Columns>
											<asp:TemplateField HeaderText="送受信">
												<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
												<HeaderStyle Wrap="false" />
												<ItemTemplate>
													<asp:Label ID="lblTxRxTypeNm" runat="server" Text='<%# GetMailTxRxNm(Eval("TXRX_TYPE"))%>'></asp:Label><br />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="作成日時">
												<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
												<ItemTemplate>
													<asp:Label ID="lblCreateDate" runat="server" Text='<%# DateTime.Parse(Eval("CREATE_DATE").ToString()).ToString("yy/MM/dd HH:mm:ss") %>'></asp:Label><br />
													<asp:LinkButton ID="lnkRxDel" runat="server" CommandArgument='<%# Eval("MAIL_SEQ") %>' Text="受信削除" OnCommand="lnkDelRxMail_Command"></asp:LinkButton>&nbsp;
													<asp:LinkButton ID="lnkTxDel" runat="server" CommandArgument='<%# Eval("MAIL_SEQ") %>' Text="送信削除" OnCommand="lnkDelTxMail_Command"></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="出演者情報">
												<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
												<HeaderStyle Wrap="false" />
												<ItemTemplate>
													<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}&manloginid={2}&mansite={3}",Eval("PARTNER_LOGIN_ID"),"ManView.aspx",Eval("LOGIN_ID"),Eval("SITE_CD")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
													<asp:Label ID="lblMailPartnerLoginId" runat="server" Text='<%# Eval("PARTNER_LOGIN_ID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="内容">
												<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
												<ItemTemplate>
													<asp:Label ID="lblMailTitle" runat="server" Text='<%# Eval("ORIGINAL_TITLE") %>' Font-Bold="true"></asp:Label><br />
													<asp:Label ID="lblMailDoc" runat="server" Text='<%# string.Format("{0}{1}{2}{3}{4}",Eval("ORIGINAL_DOC1"),Eval("ORIGINAL_DOC2"),Eval("ORIGINAL_DOC3"),Eval("ORIGINAL_DOC4"),Eval("ORIGINAL_DOC5")).Replace("\r\n","<br />") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="種別">
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="false" />
												<ItemTemplate>
													<asp:Panel ID="pnlOpenPicViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_PIC_INDEX.ToString()) %>'>
														<asp:HyperLink ID="lnkOpenPicViewer" runat="server" NavigateUrl="<%# GenerateOpenPicScript(Container.DataItem) %>" Text="画像添付"></asp:HyperLink>
													</asp:Panel>
													<asp:Panel ID="pnlOpenMovieViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString()) %>'>
														<asp:HyperLink ID="lnkOpenMovieViewer" runat="server" NavigateUrl="<%# GenerateOpenMovieScript(Container.DataItem) %>" Text="動画添付"></asp:HyperLink>
													</asp:Panel>
													<asp:Label ID="lblBatchMailMark" runat="server" Text='<%# GetBatchMark(Eval("BATCH_MAIL_FLAG")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="状態">
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="false" />
												<ItemTemplate>
													<asp:Label ID="lblDelMark" runat="server" Text='<%# GetDelMark(Eval("TX_DEL_FLAG"),Eval("RX_DEL_FLAG")) %>'></asp:Label><br />
													<asp:Label ID="lblReadFlag" runat="server" Text='<%# GetReadNm(Eval("READ_FLAG")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlFanClubFee" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[FC会費]</legend>
									<asp:GridView ID="grdFanClubFee" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsFanClubFee" AllowSorting="True" SkinID="GridViewColor" PageSize="30">
										<Columns>
											<asp:TemplateField HeaderText="決済日">
												<ItemTemplate>
													<asp:Label ID="lblAnswerDate" runat="server" Text='<%# Bind("CREATE_DATE", "{0:yy/MM/dd HH:mm}") %>' Font-Strikeout="<%#IsFanClubFeeCanceled(Container.DataItem) %>" ForeColor="<%# GetFanClubFeeForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="金額">
												<ItemTemplate>
													<asp:Label ID="lblSettleAmt" runat="server" Text='<%# Bind("SETTLE_AMT") %>' Font-Strikeout="<%#IsFanClubFeeCanceled(Container.DataItem) %>" ForeColor="<%# GetFanClubFeeForeColor(Container.DataItem) %>"></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Right" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="取消">
												<ItemTemplate>
													<asp:LinkButton ID="lnkCancelFanClubFee" runat="server" CausesValidation="False" CommandArgument='<%#Eval("SETTLE_SEQ") %>' OnCommand="lnkCancelFanClubFee_Command" Text="取消" Visible='<%# !IsFanClubFeeCanceled(Container.DataItem) %>' OnClientClick="return confirm('FC会費を取消しますか？');"></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="復帰">
												<ItemTemplate>
													<asp:LinkButton ID="lnkRevertFanClubFee" runat="server" CommandArgument='<%#Eval("SETTLE_SEQ") %>' OnCommand="lnkRevertFanClubFee_Command" Text="復帰" Visible='<%# IsFanClubFeeCanceled(Container.DataItem) %>' OnClientClick="return confirm('取消したFC会費を復帰しますか？');"></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="btnCloseExt6" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</table>
			</asp:Panel>
			<asp:Panel ID="pnlMail" runat="server">
				<fieldset class="fieldset-inner">
					<legend>[メール作成]</legend>
					<table border="0" style="width: 480px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ﾒｰﾙｻｰﾊﾞｰ
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoMailServer" runat="server" RepeatDirection="Horizontal">
									<asp:ListItem Text="BEAM" Value="1" Selected="True"></asp:ListItem>
									<asp:ListItem Text="MULLER3" Value="2"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ﾒｰﾙ種別
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstMailTemplate" runat="server" DataSourceID="dsDMMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px"
									AutoPostBack="True" OnSelectedIndexChanged="lstMailTemplate_SelectedIndexChanged">
								</asp:DropDownList>
								<asp:RequiredFieldValidator ID="vdrMailTemplate" runat="server" ErrorMessage="ﾒｰﾙ種別を選択して下さい。" ControlToValidate="lstMailTemplate" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderSmallStyle">
								ｻｰﾋﾞｽPt
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtServicePoint" runat="server" MaxLength="4" Width="30px"></asp:TextBox>Pt
								<asp:RequiredFieldValidator ID="vdrServicePoint" runat="server" ErrorMessage="サービスポイントを入力して下さい。" ControlToValidate="txtServicePoint" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ﾒｰﾙ有効時間
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMailAvaHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
								<asp:RequiredFieldValidator ID="vdrMailAvaHour" runat="server" ErrorMessage="メール有効時間を入力して下さい。" ControlToValidate="txtMailAvaHour" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								Pt振替後有効時間
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtPointTransferAvaHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
								<asp:RequiredFieldValidator ID="vdrPointTransferAvaHour" runat="server" ErrorMessage="ポイント振替後有効時間を入力して下さい。" ControlToValidate="txtPointTransferAvaHour"
									ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								ﾒｰﾙﾀｲﾄﾙ
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtMailTitle" runat="server" MaxLength="60" Width="180px"></asp:TextBox>
								$NO_TRANS_END;
								<asp:RequiredFieldValidator ID="vdrMailTitle" runat="server" ErrorMessage="メールタイトルを選択して下さい。" ControlToValidate="txtMailTitle" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdDataStyle" valign="top" colspan="2">
								$NO_TRANS_START;
								<pin:pinEdit ID="txtMailDocHtml" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR"
									SpellMode="Inline" ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic"
									BorderWidth="1px" Height="500px" Width="500px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/"
									RelativeImageRoot="/">
								</pin:pinEdit>
								<asp:TextBox ID="txtMailDocText" runat="server" TextMode="MultiLine" Width="500px" Height="500px">
								</asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
					</table>
					<asp:Label ID="lblAttention" runat="server" Text="メール有効時間/Ｐ振替後有効時間に０を設定すると無期限になります。" Font-Size="Small" ForeColor="Red"></asp:Label><br />
					<asp:Button runat="server" ID="btnCloseExt7" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
					<asp:Button runat="server" ID="btnTransMail" Text="メール送信" CssClass="seekbutton" OnClientClick='return confirm("メールを送信しますか？");' OnClick="btnTransMail_Click"
						ValidationGroup="TxMail" />
				</fieldset>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlProductBuy" Visible="false">
				<fieldset class="fieldset-inner">
					<legend>[商品購入履歴]</legend>
					<asp:GridView ID="grdProductBuy" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsProductBuy" AllowSorting="True" SkinID="GridViewColor"
						PageSize="30">
						<Columns>
							<asp:BoundField DataField="BUY_DATE" HeaderText="購入日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
								<HeaderStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="商品ID">
								<ItemStyle HorizontalAlign="Left" />
								<ItemTemplate>
									<asp:HyperLink ID="lblProductId" runat="server" NavigateUrl='<%# string.Format("~/Product/ProductView.aspx?product_id={0}",Eval("PRODUCT_ID")) %>' Text='<%# Eval("PRODUCT_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="PRODUCT_NM" HeaderText="商品名">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="POINT" HeaderText="消費ポイント" DataFormatString="{0:#,##0}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
					<asp:Button runat="server" ID="btnCloseProductBuy" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
					<asp:Button runat="server" ID="btnSeekProductBuy" Text="検索" CssClass="seekbutton" OnClick="btnSeekProductBuy_Click" />
				</fieldset>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlFriends" Visible="false">
				<asp:Panel runat="server" ID="pnlManFriends" Visible="false">
					<fieldset class="fieldset-inner">
						<legend>[男性会員紹介]</legend>
						<asp:GridView ID="grdManFriends" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsManFriends" AllowSorting="True" SkinID="GridViewColor"
							PageSize="30" HeaderStyle-HorizontalAlign="center">
							<Columns>
								<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
									<ItemStyle HorizontalAlign="Left" Width="80px" />
									<ItemTemplate>
										<asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
											Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField DataField="HANDLE_NM" HeaderText="ﾊﾝﾄﾞﾙ名" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Left" Width="120px" />
								</asp:BoundField>
								<asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Center" Width="110px" />
								</asp:BoundField>
								<asp:BoundField DataField="LAST_LOGIN_DATE" DataFormatString="{0:yy/MM/dd HH:mm}" HeaderText="最終ﾛｸﾞｲﾝ日" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Center" Width="110px" />
								</asp:BoundField>
								<asp:TemplateField HeaderText="ﾌﾟﾛﾌ入力済">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# GetValidMark(Eval("PROFILE_OK_FLAG")) %>' ID="lblProfileOk" />
									</ItemTemplate>
									<ItemStyle HorizontalAlign="Center" Width="70px" />
								</asp:TemplateField>
								<asp:BoundField DataField="LIMIT_POINT" HeaderText="残ﾎﾟｲﾝﾄ" DataFormatString="{0:#,##0}" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Right" Width="100px" />
								</asp:BoundField>
							</Columns>
							<PagerSettings Mode="NumericFirstLast" />
						</asp:GridView>
						<asp:Button runat="server" ID="btnCloseManFriends" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseManFriends_Click" />
						<asp:Button runat="server" ID="Button9" Text="検索" CssClass="seekbutton" OnClick="btnSeekManFriends_Click" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlCastFriends" Visible="false">
					<fieldset class="fieldset-inner">
						<legend>[出演者紹介]</legend>
						<asp:GridView ID="grdCastFriends" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastFriends" AllowSorting="True" SkinID="GridViewColor"
							PageSize="30" OnDataBound="grdCastFriends_DataBound" HeaderStyle-HorizontalAlign="center">
							<Columns>
								<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
									<ItemStyle HorizontalAlign="Left" Width="80px" />
									<ItemTemplate>
										<asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField DataField="HANDLE_NM" HeaderText="ﾊﾝﾄﾞﾙ名" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Left" Width="120px" />
								</asp:BoundField>
								<asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Center" Width="110px" />
								</asp:BoundField>
								<asp:BoundField DataField="LAST_LOGIN_DATE" DataFormatString="{0:yy/MM/dd HH:mm}" HeaderText="最終ﾛｸﾞｲﾝ日" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Center" Width="110px" />
								</asp:BoundField>
								<asp:TemplateField HeaderText="ﾌﾟﾛﾌ入力済">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# GetInValidMark(Eval("NA_FLAG")) %>' ID="lblProfileOk" />
									</ItemTemplate>
									<ItemStyle HorizontalAlign="Center" Width="70px" />
								</asp:TemplateField>
								<asp:BoundField DataField="TOTAL_PAYMENT_AMT" HeaderText="累計報酬金額" DataFormatString="{0:#,##0}" HtmlEncode="False">
									<ItemStyle HorizontalAlign="Right" Width="100px" />
								</asp:BoundField>
							</Columns>
							<PagerSettings Mode="NumericFirstLast" />
						</asp:GridView>
						<asp:Button runat="server" ID="btnCloseCastFriends" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseCastFriends_Click" />
						<asp:Button runat="server" ID="btnSeekCastFriends" Text="検索" CssClass="seekbutton" OnClick="btnSeekCastFriends_Click" />
					</fieldset>
				</asp:Panel>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlBbs" Visible="false">
				<fieldset class="fieldset-inner">
					<legend>[掲示板書込履歴]</legend>
					<asp:GridView ID="grdBbs" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsBbsLog" AllowSorting="True" SkinID="GridViewColor"
						PageSize="30" OnRowDataBound="grdBbs_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="書込日時">
								<ItemTemplate>
								    $NO_TRANS_START;
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE")%>'></asp:Label><br />
									<asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("BBS_SEQ"),Eval("SEX_CD"),1) %>' Text="削除" Visible='<%# Eval("DEL_FLAG","{0}").Equals(ViComm.ViCommConst.FLAG_OFF_STR)%>'
										OnCommand="lnkDelBbs_Command" OnClientClick="return confirm('削除を実行しますか？');"></asp:LinkButton>
									<asp:LinkButton ID="lnkRetrieve" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("BBS_SEQ"),Eval("SEX_CD"),0) %>' Text="復活" Visible='<%# Eval("DEL_FLAG","{0}").Equals(ViComm.ViCommConst.FLAG_ON_STR)%>'
										OnCommand="lnkDelBbs_Command"></asp:LinkButton><br />
								    $NO_TRANS_END;
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="内容">
								<ItemTemplate>
									<asp:Label ID="lblMailTitle" runat="server" Text='<%# Eval("BBS_TITLE") %>'></asp:Label><br />
									<asp:Label ID="lblDoc" runat="server" Text='<%# Eval("BBS_DOC") %>' Width="500px"></asp:Label>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<%# GetDelFlag(Eval("DEL_FLAG"),Eval("ADMIN_DEL_FLAG")) %>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
					<asp:Button runat="server" ID="btnCloseBbs" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
					<asp:Button runat="server" ID="btnSeekBbs" Text="検索" CssClass="seekbutton" OnClick="btnSeekBbs_Click" />
				</fieldset>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlModifyMailHis" Visible="false">
				<fieldset class="fieldset-inner">
					<legend>[ﾒｱﾄﾞ変更履歴/送信NG履歴/TEL変更履歴/ﾊﾟｽﾜｰﾄﾞ変更履歴]</legend>
					<table>
						<tr valign="top">
							<td>
								<asp:GridView ID="grdModifyMailHis" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsModifyMailHis" AllowSorting="True" SkinID="GridViewColor"
									PageSize="20" HeaderStyle-HorizontalAlign="center">
									<Columns>
										<asp:TemplateField HeaderText="変更日時">
											<ItemTemplate>
												<asp:Label ID="Label3" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="EMAIL_ADDR" HeaderText="ﾒｰﾙｱﾄﾞﾚｽ">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="変更者">
											<ItemTemplate>
												<asp:Label ID="lblModifyUserNm" runat="server" Text='<%# GetModifyUserNm(Eval("MODIFY_USER_CD")) %>'></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="変更ﾀｲﾐﾝｸﾞ">
											<ItemTemplate>
												<asp:Label ID="lblModifyBeforeAfterNm" runat="server" Text='<%# string.Format("変更{0}",Eval("MODIFY_BEFORE_AFTER_NM")) %>'></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" />
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</td>
							<td>
								<asp:GridView ID="grdNgMailHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsNgMailHistory" AllowSorting="True" SkinID="GridViewColor"
									PageSize="20" HeaderStyle-HorizontalAlign="center">
									<Columns>
										<asp:TemplateField HeaderText="NGﾒｰﾙ受信日">
											<ItemTemplate>
												<asp:Label ID="Label3" runat="server" Text='<%# Eval("NG_MAIL_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="EMAIL_ADDR" HeaderText="ﾒｰﾙｱﾄﾞﾚｽ">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ｴﾗｰ種別">
											<ItemTemplate>
												<asp:Label ID="lblErrorType" runat="server" Text='<%# GetErrorTypeText(Eval("ERROR_TYPE")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</td>
						</tr>
						<tr valign="top">
						</tr>
						<tr valign="top">
							<td>
								<asp:GridView ID="grdModifyTelHis" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsModifyTelHis" AllowSorting="True" SkinID="GridViewColor"
									PageSize="20" HeaderStyle-HorizontalAlign="center">
									<Columns>
										<asp:TemplateField HeaderText="変更日時">
											<ItemTemplate>
												<asp:Label ID="Label3" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="TEL" HeaderText="電話番号">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="変更者">
											<ItemTemplate>
												<asp:Label ID="lblModifyUserNm" runat="server" Text='<%# GetModifyUserNm(Eval("MODIFY_USER_CD")) %>'></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="変更ﾀｲﾐﾝｸﾞ">
											<ItemTemplate>
												<asp:Label ID="lblModifyBeforeAfterNm" runat="server" Text='<%# string.Format("変更{0}",Eval("MODIFY_BEFORE_AFTER_NM")) %>'></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" />
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</td>
							<td>
								<asp:GridView ID="grdModifyLoginPassword" runat="server" AllowPaging="false" AutoGenerateColumns="false" DataSourceID="dsModifyLoginPassword" AllowSorting="false" SkinID="GridViewColor" HeaderStyle-HorizontalAlign="center">
									<Columns>
										<asp:TemplateField HeaderText="変更日時">
											<ItemTemplate>
												<asp:Label ID="lblMLPCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="LOGIN_PASSWORD" HeaderText="ﾊﾟｽﾜｰﾄﾞ">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="変更者">
											<ItemTemplate>
												<asp:Label ID="lblMLPModifyUserNm" runat="server" Text='<%# GetModifyUserNm(Eval("MODIFY_USER_CD")) %>'></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="変更ﾀｲﾐﾝｸﾞ">
											<ItemTemplate>
												<asp:Label ID="lblMLPModifyBeforeAfterNm" runat="server" Text='<%# GetModifyBeforeAfterNm(Eval("MODIFY_BEFORE_AFTER_CD")) %>'></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" />
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnCloseModifyMailHis" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseModifyMailHis_Click" />
				</fieldset>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlUserDefPointHistory" Visible="false">
				<fieldset class="fieldset-inner">
					<legend>[定義ﾎﾟｲﾝﾄ履歴]</legend>
					<asp:GridView ID="grdUserDefPointHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserDefPointHistory" AllowSorting="True"
						SkinID="GridViewColor" PageSize="30">
						<Columns>
							<asp:TemplateField HeaderText="追加日時">
								<ItemTemplate>
									<asp:Label runat="server" Text='<%# Eval("ADD_POINT_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="ADD_POINT_ID" HeaderText="ﾎﾟｲﾝﾄ追加識別">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="ADD_POINT" HeaderText="増減ﾎﾟｲﾝﾄ" DataFormatString="{0:N0}">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="REMARKS" HeaderText="備考">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
					<asp:Button runat="server" ID="Button8" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
					<asp:Button runat="server" ID="Button10" Text="検索" CssClass="seekbutton" OnClick="btnSeekUserDefPointHis_Click" />
				</fieldset>
			</asp:Panel>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlDtl" HorizontalAlign="Left">
			<table border="0">
				<tr>
					<td valign="top">
						<asp:Panel runat="server" ID="pnlInfo">
							<fieldset class="fieldset-inner">
								<legend>
									<%= DisplayWordUtil.Replace("[男性会員詳細]") %>
								</legend>
								<asp:DetailsView ID="dvwUserManCharacter" runat="server" DataSourceID="dsUserManCharacter" AutoGenerateRows="False" SkinID="DetailsView" Width="368px"
									OnDataBound="dvwUserManCharacter_DataBound">
									<Fields>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="IＤ">
											<ItemTemplate>
												<asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>(<asp:Label ID="lblUserSeqDetail" runat="server" Text='<%# Eval("USER_SEQ") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>)
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="TEL">
											<ItemTemplate>
												<asp:Label ID="lblTel" runat="server" Text='<%# CheckAdminLevel(Eval("TEL")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label><asp:Label ID="lblAttested" runat="server" Text="(認証済み)" Visible='<%# Eval("TEL_ATTESTED_FLAG").ToString().Equals("1") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label><asp:Label ID="lblNoAttested" runat="server" Text="(未認証)" Visible='<%# Eval("TEL_ATTESTED_FLAG").ToString().Equals("0") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
												<asp:Label ID="lblUseFreeDial" runat="server" Text="FreeDial" Visible='<%# Eval("USE_FREE_DIAL_FLAG").ToString().Equals("1") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
												<asp:Label ID="lblNonUseFreeDial" runat="server" Text="一般回線" Visible='<%# Eval("USE_FREE_DIAL_FLAG").ToString().Equals("0") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="パスワード">
											<ItemTemplate><asp:Label ID="lblPassword" runat="server" Text='<%# Eval("LOGIN_PASSWORD") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="残ポイント">
											<ItemTemplate>
												<asp:Label ID="lblBalPoint" runat="server" Text='<%# string.Format("{0}Pt",Eval("BAL_POINT")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="サービスPt">
											<ItemTemplate>
												<asp:Label ID="lblServicePoint" runat="server" Text='<%# string.Format("{0}Pt 期限:{1}",Eval("SERVICE_POINT"),Eval("SERVICE_POINT_EFFECTIVE_DATE")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="後払限度Pt">
											<ItemTemplate>
												<asp:Label ID="lblLimitPoint" runat="server" Text='<%# string.Format("{0}Pt",Eval("LIMIT_POINT")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="入金期日">
											<ItemTemplate>
												<asp:Label ID="lblReceiptLimitDay" runat="server" Text='<%# Eval("RECEIPT_LIMIT_DAY") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="請求額">
											<ItemTemplate>
												<asp:Label ID="lblBillAmt" runat="server" Text='<%# string.Format("{0}円 ",Eval("BILL_AMT")) %>' Font-Bold="true" ForeColor="blue"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="会員状態">
											<ItemTemplate>
												<asp:Label ID="lblUserStatus" runat="server" Text='<%# Eval("USER_STATUS_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ランク">
											<ItemTemplate>
												<asp:Label ID="lblUserRank" runat="server" Text='<%# Eval("USER_RANK_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾒｰﾙｱﾄﾞﾚｽ">
											<ItemTemplate>
												<asp:Label ID="lblEmail" runat="server" Text='<%# CheckAdminLevel(Eval("EMAIL_ADDR")) %>' oreColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>' CssClass="Warp"
													Width="240px" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾒｰﾙｱﾄﾞﾚｽ状態">
											<ItemTemplate>
												<asp:Label ID="lblEmailNg" runat="server" Text='<%# CheckAddrNg(Eval("NON_EXIST_MAIL_ADDR_FLAG"))%>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="MULLER3送信">
											<ItemTemplate>
												<asp:Label ID="lblMuller3Ng" runat="server" Text='<%# CheckMuller3Ng(Eval("TX_MULLER3_NG_MAIL_ADDR_FLAG"))%>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="広告ｺｰﾄﾞ">
											<ItemTemplate>
												<asp:Label ID="lblAdCd" runat="server" Text='<%# Eval("AD_CD") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
												<asp:Label ID="lblAdNm" runat="server" Text='<%# Eval("AD_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="登録日">
											<ItemTemplate>
												<asp:Label ID="lblRegistDate" runat="server" Text='<%# Eval("REGIST_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="初回ﾛｸﾞｲﾝ日">
											<ItemTemplate>
												<asp:Label ID="lblFirstLoginDate" runat="server" Text='<%# Eval("FIRST_LOGIN_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="初回利用日">
											<ItemTemplate>
												<asp:Label ID="lblFirstPointUsedDate" runat="server" Text='<%# Eval("FIRST_POINT_USED_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="最終ﾛｸﾞｲﾝ日">
											<ItemTemplate>
												<asp:Label ID="lblLastLoginDate" runat="server" Text='<%# Eval("LAST_LOGIN_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="最終利用日">
											<ItemTemplate>
												<asp:Label ID="lblLastPointUsedDate" runat="server" Text='<%# Eval("LAST_POINT_USED_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾌﾞﾗｯｸ登録日">
											<ItemTemplate>
												<asp:Label ID="lblLastBlackDay" runat="server" Text='<%# Eval("LAST_BLACK_DAY") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="入金累計額">
											<ItemTemplate>
												<asp:Label ID="Label1" runat="server" Text='<%# string.Format("{0}({1})",Eval("TOTAL_RECEIPT_AMT"),Eval("TOTAL_RECEIPT_COUNT")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄｱﾌﾘ累計額">
											<ItemTemplate>
												<asp:Label ID="lblMchaTotalReceiptAmt" runat="server" Text='<%# string.Format("{0}({1})",Eval("POINT_AFFILIATE_TOTAL_AMT"),Eval("POINT_AFFILIATE_TOTAL_COUNT")) %>'
													ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="当月入金額">
											<ItemTemplate>
												<asp:Label ID="lblMonthlyReceiptAmt" runat="server" Text='<%# string.Format("{0}({1})",Eval("MONTHLY_RECEIPT_AMT"),Eval("MONTHLY_RECEIPT_COUNT")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="初回入金日">
											<ItemTemplate>
												<asp:Label ID="lblFirstReceiptDay" runat="server" Text='<%# Eval("FIRST_RECEIPT_DAY") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="最終入金日">
											<ItemTemplate>
												<asp:Label ID="lblLastReceiptDay" runat="server" Text='<%# GetDaysInDate(Eval("LAST_RECEIPT_DATE")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾘｯﾁｰﾉﾗﾝｸ">
											<ItemTemplate>
												<asp:Label ID="lblRichinoRankNm" runat="server" Text=""></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾘｯﾁｰﾉﾗﾝｸ更新日">
											<ItemTemplate>
												<asp:Label ID="lblLastRichinoRankUpdDate" runat="server" Text=""></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="登録端末">
											<ItemTemplate>
												<asp:Label ID="lblRegistCarrierNm" runat="server" Text='<%# Eval("REGIST_CARRIER_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
												<asp:Label ID="lblRegistTerminalNm" runat="server" Text='<%# Eval("REGIST_TERMINAL_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'
													CssClass="Warp" Width="240px"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="利用端末">
											<ItemTemplate>
												<asp:Label ID="lblCarrierNm" runat="server" Text='<%# Eval("MOBILE_CARRIER_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
												<asp:Label ID="lblTerminalNm" runat="server" Text='<%# Eval("MOBILE_TERMINAL_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>' CssClass="Warp"
													Width="240px"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="従量制登録日時">
											<ItemTemplate>
												<asp:Label ID="lblMeasuredRegistDate" runat="server" Text='<%# Eval("MEASURED_RATE_REGIST_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="更新日">
											<ItemTemplate>
												<asp:Label ID="lblUpdateDate" runat="server" Text='<%# Eval("UPDATE_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="会員情報更新">
											<ItemTemplate>
												<asp:HyperLink ID="lnkUpdate" runat="server" NavigateUrl='<%# string.Format("ManMainte.aspx?site={0}&userseq={1}&loginid={2}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("LOGIN_ID")) %>'
													Text="更新実行" Enabled="true"></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="直ﾛｸﾞｲﾝ">
											<ItemTemplate>
												<asp:LinkButton ID="lnkLogin" runat="server" CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("SITE_CD"),Eval("LOGIN_ID"),Eval("LOGIN_PASSWORD")) %>'
													OnCommand="lnkLogin_Command" Text="直ﾛｸﾞｲﾝ"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="生年月日">
											<ItemTemplate>
												<asp:Label ID="lblBirthday" runat="server" Text='<%# Eval("BIRTHDAY") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemTemplate><asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="固体識別番号">
											<ItemTemplate><asp:Label ID="lblIModeId" runat="server" Text='<%# Eval("IMODE_ID") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="固体識別">
											<ItemTemplate>
												<asp:Label ID="lblterminalUniqueId" runat="server" Text='<%# Eval("TERMINAL_UNIQUE_ID") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="紹介者情報">
											<ItemTemplate>
												<asp:HyperLink ID="lnkIntroducerData" runat="server" Text='<%# Eval("INTRODUCER_FRIEND_CD") %>' NavigateUrl='<%# GetIntroducerLink(Eval("INTRODUCER_FRIEND_CD")) %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="利用機種">
											<ItemTemplate><asp:Label ID="lblMobileNm" runat="server" Text='<%# GetModelName(Eval("MOBILE_CARRIER_CD").ToString(),Eval("MOBILE_TERMINAL_NM").ToString())%>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ｷｬｽﾄﾒｰﾙ<br>受信区分">
											<ItemTemplate>
												<asp:Label ID="lblCastMailRxTypeNm" runat="server" Text='<%# Eval("CAST_MAIL_RX_TYPE_NM")%>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="お知らせﾒｰﾙ<br>受信区分">
											<ItemTemplate>
												<asp:Label ID="lblInfoMailRxTypeNm" runat="server" Text='<%# Eval("INFO_MAIL_RX_TYPE_NM")%>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾕｰｻﾞｰ定義ﾌﾗｸﾞ">
											<ItemTemplate>
												<asp:Label ID="lblUserDefineMask" runat="server" Text='<%# GetEnabledUserDefineFlagMark(Eval("USER_DEFINE_MASK")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
                                        <asp:TemplateField HeaderText="CrosmileVer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCrosmileLsatUsedVersion" runat="server" Text='<%# Eval("CROSMILE_LAST_USED_VERSION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GCｱﾌﾟﾘVer">
                                            <ItemTemplate>
												<asp:Label ID="lblGcappLastLoginVersion" runat="server" Text='<%# Eval("GCAPP_LAST_LOGIN_VERSION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GCｱﾌﾟﾘﾛｸﾞｲﾝ日">
                                            <ItemTemplate>
												<asp:Label ID="lblGcappLastLoginDate" runat="server" Text='<%# Eval("GCAPP_LAST_LOGIN_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
										<asp:TemplateField HeaderText="旧ID">
											<ItemTemplate>
												<asp:Label ID="lblBeforeSystemId" runat="server" Text='<%# Eval("BEFORE_SYSTEM_ID") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾃﾞﾊﾞｲｽﾄｰｸﾝ">
											<ItemTemplate>
												<asp:Label ID="lblDeviceToken" runat="server" Text='<%# Eval("DEVICE_TOKEN") %>' CssClass="Warp" Width="240px"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾃﾞﾊﾞｲｽUUID">
											<ItemTemplate>
												<asp:Label ID="lblDeviceUuid" runat="server" Text='<%# Eval("DEVICE_UUID") %>' CssClass="Warp" Width="240px"></asp:Label>
												<asp:Button runat="server" ID="btnDeleteDeviceUuid" Text="ﾃﾞﾊﾞｲｽUUID削除" CssClass="seekbutton" OnClientClick="return confirm('デバイスUUIDを削除しますか？');" OnClick="btnDeleteDeviceUuid_Click" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="登録時ｻｰﾋﾞｽPt">
											<ItemTemplate>
												<asp:Label ID="lblRegistServicePointFlag" runat="server" Text='<%# GetRegistServicePointFlagText(Eval("REGIST_SERVICE_POINT_FLAG")) %>'></asp:Label>&nbsp;
												<asp:Label ID="lblRegistServicePointDate" runat="server" Text='<%# Eval("REGIST_SERVICE_POINT_DATE") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="会員ﾃｽﾄﾀｲﾌﾟ">
											<ItemTemplate>
												<asp:Label ID="lblUserTestType" runat="server" Text='<%# Eval("USER_TEST_TYPE") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
									</Fields>
									<HeaderStyle Wrap="False" />
								</asp:DetailsView>
								[詳細プロフィール]
								<asp:PlaceHolder ID="plcHolder" runat="server">
									<asp:Table ID="Table1" runat="server" CssClass="tableStyle tdDetailsViewStyle" Width="368px">
										<asp:TableRow ID="rowAttr0" runat="server">
											<asp:TableCell ID="celUserManAttrNm0" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm0" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq0" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq0" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr1" runat="server">
											<asp:TableCell ID="celUserManAttrNm1" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm1" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq1" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq1" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr2" runat="server">
											<asp:TableCell ID="celUserManAttrNm2" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm2" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq2" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq2" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr3" runat="server">
											<asp:TableCell ID="celUserManAttrNm3" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm3" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq3" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq3" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr4" runat="server">
											<asp:TableCell ID="celUserManAttrNm4" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm4" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq4" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq4" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr5" runat="server">
											<asp:TableCell ID="celUserManAttrNm5" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm5" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq5" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq5" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr6" runat="server">
											<asp:TableCell ID="celUserManAttrNm6" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm6" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq6" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq6" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr7" runat="server">
											<asp:TableCell ID="celUserManAttrNm7" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm7" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq7" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq7" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr8" runat="server">
											<asp:TableCell ID="celUserManAttrNm8" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm8" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq8" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq8" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr9" runat="server">
											<asp:TableCell ID="celUserManAttrNm9" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm9" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq9" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq9" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr10" runat="server">
											<asp:TableCell ID="celUserManAttrNm10" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm10" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq10" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq10" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr11" runat="server">
											<asp:TableCell ID="celUserManAttrNm11" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm11" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq11" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq11" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr12" runat="server">
											<asp:TableCell ID="celUserManAttrNm12" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm12" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq12" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq12" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr13" runat="server">
											<asp:TableCell ID="celUserManAttrNm13" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm13" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq13" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq13" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr14" runat="server">
											<asp:TableCell ID="celUserManAttrNm14" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm14" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq14" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq14" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr15" runat="server">
											<asp:TableCell ID="celUserManAttrNm15" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm15" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq15" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq15" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr16" runat="server">
											<asp:TableCell ID="celUserManAttrNm16" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm16" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq16" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq16" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr17" runat="server">
											<asp:TableCell ID="celUserManAttrNm17" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm17" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq17" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq17" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr18" runat="server">
											<asp:TableCell ID="celUserManAttrNm18" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm18" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq18" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq18" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowAttr19" runat="server">
											<asp:TableCell ID="celUserManAttrNm19" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblUserManAttrNm19" runat="server" Text='Label'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celUserManAttrSeq19" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblUserManAttrSeq19" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowManWaitingComment" runat="server">
											<asp:TableCell ID="celManWaitingCommentTitle" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblManWaitingCommentTitle" runat="server" Text='待機コメント'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celManWaitingComment" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Label ID="lblManWaitingComment" runat="server" Text="Label"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowManProfilePic" runat="server">
											<asp:TableCell ID="celManProfilePic" runat="server" CssClass="tdHeaderStyle tdDetailsViewFieldHeaderStyle">
												<asp:Label ID="lblManProfilePic" runat="server" Text='プロフィール写真'></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celManProfilePicImg" runat="server" CssClass="tdDataStyle tdDetailsViewFieldDataStyle">
												<asp:Image ID="imgManProfilePic" runat="server" Height="80px" /><br />
												<asp:Button runat="server" ID="btnDeleteManPic" Text="画像削除" CssClass="seekbutton" OnClick="btnDeleteManPic_Click" />
											</asp:TableCell>
										</asp:TableRow>
									</asp:Table>
								</asp:PlaceHolder>
								<asp:Panel ID="pnlGameCharacter" runat="server" Visible="false">
									[ゲームキャラクター]
									<asp:PlaceHolder ID="plcGameHolder" runat="server">
										<asp:Table ID="Table2" runat="server" CssClass="tableStyle" Width="368px">
											<asp:TableRow ID="rowGameCharacterHandleNm" runat="server">
												<asp:TableCell ID="celGameCharacterHandleNmHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterHandleNmHeader" runat="server" Text="ｹﾞｰﾑﾊﾝﾄﾞﾙﾈｰﾑ"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterHandleNm" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterHandleNm" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterTypeNm" runat="server">
												<asp:TableCell ID="celGameCharacterTypeNmHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterTypeNmHeader" runat="server" Text="ｹﾞｰﾑｷｬﾗｸﾀｰ種別"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterTypeNm" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterTypeNm" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterPoint" runat="server">
												<asp:TableCell ID="celGameCharacterPointHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterPointHeader" runat="server" Text="ｹﾞｰﾑ内ﾎﾟｲﾝﾄ"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterPoint" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterPoint" runat="server"></asp:Label>円
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterLevel" runat="server">
												<asp:TableCell ID="celGameCharacterLevelHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterLevelHeader" runat="server" Text="ﾚﾍﾞﾙ"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterLevel" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterLevel" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterExp" runat="server">
												<asp:TableCell ID="celGameCharacterExpHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterExpHeader" runat="server" Text="現在の経験値"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterExp" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterExp" runat="server"></asp:Label>
													次のﾚﾍﾞﾙまであと<asp:Label ID="lblGameCharacterRestExp" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterTotalExp" runat="server">
												<asp:TableCell ID="celGameCharacterTotalExpHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterTotalExpHeader" runat="server" Text="累計経験値"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterTotalExp" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterTotalExp" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterUnassignedFroce" runat="server">
												<asp:TableCell ID="celGameCharacterUnassignedFroceHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterUnassignedFroceHeader" runat="server" Text="未割当部隊数"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterUnassignedFroce" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterUnassignedFroce" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterMissionMaxForce" runat="server">
												<asp:TableCell ID="celGameCharacterMissionMaxForceHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterMissionMaxForceHeader" runat="server" Text="ﾐｯｼｮﾝ部隊数"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterMissionMaxForce" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterMissionMaxForce" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterAttackMaxForce" runat="server">
												<asp:TableCell ID="celGameCharacterAttackMaxForceHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterAttackMaxForceHeader" runat="server" Text="攻撃部隊数"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterAttackMaxForce" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterAttackMaxForce" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterDefenceMaxForce" runat="server">
												<asp:TableCell ID="celGameCharacterDefenceMaxForceHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterDefenceMaxForcepHeader" runat="server" Text="防御部隊数"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterDefenceMaxForce" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterDefenceMaxForce" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterCooperation" runat="server">
												<asp:TableCell ID="celGameCharacterCooperationHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterCooperationHeader" runat="server" Text="連携ﾎﾟｲﾝﾄ"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterCooperation" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterCooperation" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterStageNm" runat="server">
												<asp:TableCell ID="celGameCharacterStageNmHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterStageNmHeader" runat="server" Text="現在のｽﾃｰｼﾞ"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterStageNm" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterStageNm" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterRegistDate" runat="server">
												<asp:TableCell ID="celGameCharacterRegistDateHeader" runat="server" CssClass="tdHeaderStyle">
													<asp:Label ID="lblGameCharacterRegistDateHeader" runat="server" Text="登録日"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterRegistDate" runat="server" CssClass="tdDataStyle">
													<asp:Label ID="lblGameCharacterRegistDate" runat="server"></asp:Label>
												</asp:TableCell>
											</asp:TableRow>
											<asp:TableRow ID="rowGameCharacterMainteLink" runat="server">
												<asp:TableCell ID="celGameCharacterMainteLinkHeader" runat="server" CssClass="tdHeaderStyle2">
													<asp:Label ID="lblGameCharacterMainteLinkHeader" runat="server" Text="ｹﾞｰﾑｷｬﾗｸﾀｰ情報更新"></asp:Label>
												</asp:TableCell>
												<asp:TableCell ID="celGameCharacterMainteLink" runat="server" CssClass="tdDataStyle">
													<asp:HyperLink ID="lnkGameCharacterMainteLink" runat="server" NavigateUrl='<%# GetGameCharacterMainteLink() %>' Text="更新実行"></asp:HyperLink>
												</asp:TableCell>
											</asp:TableRow>
										</asp:Table>
									</asp:PlaceHolder>
								</asp:Panel>
							</fieldset>
						</asp:Panel>
					</td>
					<td valign="top">
						<asp:Panel runat="server" ID="pnlInputReceipt">
							<fieldset class="fieldset-inner">
								<legend>[入金]</legend>
								<table border="0" style="width: 656px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											入金額
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtReceiptAmt" runat="server" MaxLength="6" Width="48px"></asp:TextBox>
											<asp:RegularExpressionValidator ID="vdeReceiptAmt" runat="server" ErrorMessage="残ポイントは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtReceiptAmt"
												ValidationGroup="Receipt" Display="Dynamic">*</asp:RegularExpressionValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											備 考
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRemarks1" runat="server" MaxLength="1000" Width="500px" Height="48px" TextMode="MultiLine"></asp:TextBox><br />
											<asp:TextBox ID="txtRemarks2" runat="server" MaxLength="256" Width="360px"></asp:TextBox><br />
											<asp:TextBox ID="txtRemarks3" runat="server" MaxLength="256" Width="360px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											入金ﾀｲﾌﾟ
										</td>
										<td class="tdDataStyle">
											<asp:RadioButton ID="rdoCash" runat="server" Text="銀行振込" GroupName="CashTypeGroup" Checked="true" />
											<asp:RadioButton ID="rdoMobileCash" runat="server" Text="携帯送金" GroupName="CashTypeGroup" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											入金者
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtModifyUserNm" runat="server" MaxLength="30" Width="210px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrModifyUserNm" runat="server" ErrorMessage="更新者を入力して下さい。" ControlToValidate="txtModifyUserNm" ValidationGroup="Receipt">*</asp:RequiredFieldValidator>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnReceipt" Text="入金更新" CssClass="seekbutton" OnClick="btnReceipt_Click" ValidationGroup="Receipt" />
								<asp:Button runat="server" ID="btnSettleInstant" Text="即時決済" CssClass="seekbutton" OnClick="btnSettleInstant_Click" ValidationGroup="" />
								<asp:Label runat="server" ID="lblSettleResult"></asp:Label>
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlRemarks">
							<fieldset class="fieldset-inner">
								<legend>[備考]</legend>
								<table border="0" style="width: 656px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											備 考
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRemarks4" runat="server" MaxLength="1024" Width="500px" Height="280px" TextMode="MultiLine"></asp:TextBox><br />
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnRemarks" Text="備考更新" CssClass="seekbutton" OnClick="btnRemarks_Click" ValidationGroup="Remarks" />
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlAddPoint">
							<fieldset class="fieldset-inner">
								<legend>[ポイント追加]</legend>
								<table border="0" style="width: 656px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											追加ポイント
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAddPointAmt" runat="server" MaxLength="6" Width="46px"></asp:TextBox>
											<asp:RegularExpressionValidator ID="vdeAddPointAmt" runat="server" ErrorMessage="追加ポイントは数値で入力してください。" ControlToValidate="txtAddPointAmt" ValidationExpression="^[-]?\d+$"
												ValidationGroup="AddPoint" Display="Dynamic">*</asp:RegularExpressionValidator>
											<asp:CustomValidator ID="vdcAddPointAmt" runat="server" ErrorMessage="0以外の数値を入力してください" ValidationGroup="AddPoint"></asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											備考
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRemarks" runat="server" MaxLength="128" Width="500px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											ポイント追加者
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAddUserNm" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrAddUserNm" runat="server" ErrorMessage="ポイント追加者を入力して下さい。" ControlToValidate="txtAddUserNm" ValidationGroup="AddPoint">*</asp:RequiredFieldValidator>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnAddPoint" Text="ポイント追加" CssClass="seekbutton" ValidationGroup="AddPoint" OnClick="btnAddPoint_Click" />
								<asp:Button runat="server" ID="btnAddProfilePicFacePoint" Text="プロフ顔写真Pt" Width="80px" CssClass="seekrightbutton" OnClick="btnAddProfilePicFacePoint_Click" />
								<asp:Button runat="server" ID="btnAddProfilePicPoint" Text="プロフ写真Pt" Width="80px" CssClass="seekrightbutton" OnClick="btnAddProfilePicPoint_Click" />
								<asp:Button runat="server" ID="btnAddProfileFillDocPoint" Text="プロフ設定Pt" Width="80px" CssClass="seekrightbutton" OnClick="btnAddProfileFillDocPoint_Click" />
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlAddGamePoint">
							<fieldset class="fieldset-inner">
								<legend>[ゲーム用ポイント追加]</legend>
								<table border="0" style="width: 520px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											追加ポイント
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAddGamePoint" runat="server" MaxLength="6" Width="46px"></asp:TextBox>円
											<asp:RequiredFieldValidator ID="vdrAddGamePoint" runat="server" ErrorMessage="追加ポイントを入力してください。" ControlToValidate="txtAddGamePoint" ValidationGroup="AddGamePoint"
												Display="Dynamic">*</asp:RequiredFieldValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdrAddGamePoint" HighlightCssClass="validatorCallout" />
											<asp:RegularExpressionValidator ID="vdeAddGamePoint" runat="server" ErrorMessage="追加ポイントは数値で入力してください。" ControlToValidate="txtAddGamePoint" ValidationExpression="^[-]?\d+$"
												ValidationGroup="AddGamePoint" Display="Dynamic">*</asp:RegularExpressionValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeAddGamePoint" HighlightCssClass="validatorCallout" />
											<asp:CustomValidator ID="vdcAddGamePoint" runat="server" ErrorMessage="現在のポイントとの合計が0以上になるように入力してください。" ControlToValidate="txtAddGamePoint" OnServerValidate="vdcAddGamePoint_ServerValidate"
												ValidationGroup="AddGamePoint" Display="Dynamic">*</asp:CustomValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcAddGamePoint" HighlightCssClass="validatorCallout" />
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnAddGamePoint" Text="ポイント追加" CssClass="seekbutton" ValidationGroup="AddGamePoint" OnClick="btnAddGamePoint_Click" />
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlAddExp">
							<fieldset class="fieldset-inner">
								<legend>[経験値追加]</legend>
								<table border="0" style="width: 520px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											追加経験値
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAddExp" runat="server" MaxLength="6" Width="46px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrAddExp" runat="server" ErrorMessage="追加経験値を入力してください。" ControlToValidate="txtAddExp" ValidationGroup="AddExp" Display="Dynamic">*</asp:RequiredFieldValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdrAddExp" HighlightCssClass="validatorCallout" />
											<asp:RegularExpressionValidator ID="vdeAddExp" runat="server" ErrorMessage="追加経験値は1以上の数値で入力してください。" ControlToValidate="txtAddExp" ValidationExpression="[1-9]\d*"
												ValidationGroup="AddExp" Display="Dynamic">*</asp:RegularExpressionValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdeAddExp" HighlightCssClass="validatorCallout" />
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnAddExp" Text="追加する" CssClass="seekbutton" ValidationGroup="AddExp" OnClick="btnAddExp_Click" />
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlAddGameItem">
							<fieldset class="fieldset-inner">
								<legend>[ゲーム用アイテム追加]</legend>
								<table border="0" style="width: 650px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											アイテム
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory" OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
												DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE" Width="170px" AutoPostBack="True">
											</asp:DropDownList>
											<asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
												DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
											</asp:DropDownList>
											<asp:DropDownList ID="lstItemPresent" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
												<asp:ListItem Value=""></asp:ListItem>
												<asp:ListItem Value="0">通常</asp:ListItem>
												<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
											</asp:DropDownList>
											<asp:DropDownList ID="lstGameItem" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM" OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ"
												Width="170px">
											</asp:DropDownList>
											<asp:RequiredFieldValidator ID="vdrGameItem" runat="server" ErrorMessage="付与するアイテムを選んでください。" ControlToValidate="lstGameItem" ValidationGroup="AddGameItem"
												Display="Dynamic">*</asp:RequiredFieldValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrGameItem" HighlightCssClass="validatorCallout" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											個数
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtGameItemGiveCount" runat="server" MaxLength="10" Width="60px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrGameItemGiveCount" runat="server" ErrorMessage="個数を入力してください" ControlToValidate="txtGameItemGiveCount" ValidationGroup="AddGameItem"
												Display="Dynamic">*</asp:RequiredFieldValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdrGameItemGiveCount" HighlightCssClass="validatorCallout" />
											<asp:RegularExpressionValidator ID="vdeGameItemGiveCount" runat="server" ErrorMessage="個数は数値で入力してください" ControlToValidate="txtGameItemGiveCount" ValidationExpression="^[-]?\d+$"
												ValidationGroup="AddGameItem" Display="Dynamic">*</asp:RegularExpressionValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdeGameItemGiveCount" HighlightCssClass="validatorCallout" />
											<asp:RangeValidator ID="vdaGameItemGivCount" runat="server" ErrorMessage="数値は1以上で入力してください" ControlToValidate="txtGameItemGiveCount" MinimumValue="1" MaximumValue="9999999999"
												ValidationGroup="AddGameItem" Display="Dynamic">*</asp:RangeValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdaGameItemGivCount" HighlightCssClass="validatorCallout" />
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnAddGameItem" Text="追加する" CssClass="seekbutton" ValidationGroup="AddGameItem" OnClick="btnAddGameItem_Click" />
								<asp:LinkButton runat="server" ID="lnkPossessionGameItem" Text="所持アイテム一覧" CssClass="seekbutton" OnClick="lnkPossessionGameItem_Click"></asp:LinkButton>
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlPossessionGameItem" Visible="false">
							<fieldset class="fieldset-inner">
								<legend>[所持ゲームアイテム一覧]</legend>
								<asp:GridView ID="grdPossessionGameItem" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsPossessionGameItem" AllowSorting="True"
									SkinID="GridViewColor" PageSize="15">
									<Columns>
										<asp:BoundField DataField="GAME_ITEM_CATEGORY_NM" HeaderText="ｱｲﾃﾑｶﾃｺﾞﾘ名">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:BoundField DataField="GAME_ITEM_NM" HeaderText="ｱｲﾃﾑ名">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:BoundField DataField="POSSESSION_COUNT" DataFormatString="{0:N0}" HeaderText="所持数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="現在耐久力">
											<ItemTemplate>
												<asp:Label ID="lblEndurance" runat="server" Text='<%# Eval("NOW_ENDURANCE") + "/" + Eval("ENDURANCE") %>'></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
								<asp:Button runat="server" ID="Button7" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
								<asp:Button runat="server" ID="btnSeekPossessionGameItem" Text="検索" CssClass="seekbutton" OnClick="btnSeekPossessionGameItem_Click" />
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlActivated">
							<fieldset class="fieldset-inner">
								<legend>[アクティベート済みサイト一覧]</legend>
								<asp:GridView ID="grdActivated" AutoGenerateColumns="False" runat="server" DataSourceID="" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="SITE_NM" HeaderText="サイト名" Visible="true">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
									</Columns>
								</asp:GridView>
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlUrge">
							<fieldset class="fieldset-inner">
								<legend>[督促詳細]</legend>
								<div class="bottomSpace">
									<asp:DetailsView ID="dvwUrge" runat="server" DataSourceID="dsUrge" AutoGenerateRows="False" SkinID="DetailsView" Width="490px">
										<Fields>
											<asp:BoundField DataField="URGE_LEVEL" HeaderText="督促レベル" />
											<asp:TemplateField HeaderText="督促実行済み回数">
												<ItemTemplate>
													<asp:Label ID="lblUrgeExecCount" runat="server" Text='<%# string.Format("{0}回",Eval("URGE_EXEC_COUNT")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="督促最大回数">
												<ItemTemplate>
													<asp:Label ID="lblMaxUrgeCount" runat="server" Text='<%# string.Format("{0}回",Eval("MAX_URGE_COUNT")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="本日督促回数">
												<ItemTemplate>
													<asp:Label ID="lblUrgeCountToday" runat="server" Text='<%# string.Format("{0}回",Eval("URGE_COUNT_TODAY")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="URGE_END_DAY" HeaderText="督促終了予定日" />
											<asp:BoundField DataField="URGE_RESTART_DAY" HeaderText="督促再開予定日" />
											<asp:BoundField DataField="URGE_EXEC_LAST_DATE" HeaderText="督促最終実行日" />
											<asp:BoundField DataField="URGE_EXEC_LAST_STATUS_NM" HeaderText="回線状態" />
										</Fields>
										<HeaderStyle Wrap="False" />
									</asp:DetailsView>
								</div>
							</fieldset>
						</asp:Panel>
					</td>
				</tr>
			</table>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserManCharacter" runat="server" SelectMethod="GetOne" TypeName="UserManCharacter" OnSelected="dsUserManCharacter_Selected"
		OnSelecting="dsUserManCharacter_Selecting">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="txtLoginId" Name="pLoginID" PropertyName="Text" Type="String" />
			<asp:Parameter Name="pTelAttestedFlag"  Type="String" />
			<asp:ControlParameter ControlID="txtUserSeq" Name="pUserSeq" PropertyName="Text" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogMonitor" runat="server" EnablePaging="True" OnSelecting="dsUsedLogMonitor_Selecting" SelectCountMethod="GetMonitorPageCount"
		SelectMethod="GetMonitorPageCollection" TypeName="UsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogMonitorSummary" runat="server" OnSelecting="dsUsedLogMonitor_Selecting" SelectMethod="GetMonitorPointSummaryList" TypeName="UsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebUsedLog" runat="server" SelectMethod="GetPageCollection" TypeName="WebUsedLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsWebUsedLog_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerHandleNm" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebUsedLogSummary" runat="server" OnSelecting="dsWebUsedLog_Selecting" SelectMethod="GetPointSummaryList" TypeName="WebUsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerHandleNm" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsBbsLog" runat="server" SelectMethod="GetPageCollection" TypeName="BbsLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsBbsLog_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pTxLoginId" Type="String" />
			<asp:Parameter Name="pTxUserCharNo" Type="String" />
			<asp:Parameter Name="pKeyword" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsReceipt" runat="server" SelectMethod="GetPageCollection" TypeName="Receipt" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsReceipt_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pSettleType" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pReceiptCancelFlag" Type="String" DefaultValue="" />
			<asp:Parameter Name="pWithoutCredit0Yen" Type="Boolean" DefaultValue="False" />
			<asp:Parameter Name="pWithoutCredit300Yen" Type="Boolean" DefaultValue="False" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSettle" runat="server" SelectMethod="GetSettlePageCollection" TypeName="SettleLog" SelectCountMethod="GetSettlePageCount" EnablePaging="True"
		OnSelecting="dsSettle_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsActivated" runat="server" SelectMethod="GetActivatedSiteList" TypeName="UserManCharacter" OnSelecting="dsActivated_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogTalk" runat="server" SelectMethod="GetPageCollection" TypeName="UsedLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsUsedLogTalk_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerUserCharNo" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pChargeType" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pCallResult" Type="String" />
			<asp:Parameter Name="pTalkSubType" Type="int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogTalkSummary" runat="server" OnSelecting="dsUsedLogTalk_Selecting" SelectMethod="GetTalkPointSummaryList" TypeName="UsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerUserCharNo" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pChargeType" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pCallResult" Type="String" />
			<asp:Parameter Name="pTalkSubType" Type="int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsFavorit" runat="server" SelectMethod="GetPageCollection" TypeName="Favorit" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsFavorit_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="string" />
			<asp:Parameter DefaultValue="0" Name="pLikeMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLikeMe" runat="server" SelectMethod="GetPageCollection" TypeName="Favorit" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsLikeMe_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="string" />
			<asp:Parameter DefaultValue="1" Name="pLikeMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRefuse" runat="server" SelectMethod="GetManRefuseCastPageCollection" TypeName="Refuse" SelectCountMethod="GetManRefuseCastPageCount"
		EnablePaging="True" OnSelecting="dsRefuse_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter DefaultValue="0" Name="pRefuseMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRefuseMe" runat="server" SelectMethod="GetManRefuseCastPageCollection" TypeName="Refuse" SelectCountMethod="GetManRefuseCastPageCount"
		EnablePaging="True" OnSelecting="dsRefuse_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pRefuseMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMarking" runat="server" SelectMethod="GetPageCollection" TypeName="Marking" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsMarking_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="string" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailBox" runat="server" SelectMethod="GetPageCollection" TypeName="MailBox" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsMailBox_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerUserCharNo" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pReportDayTimeFrom" Type="String" />
			<asp:Parameter Name="pReportDayTimeTo" Type="String" />
			<asp:Parameter Name="pAttachedType" Type="String" />
			<asp:Parameter Name="pWithBatchMail" Type="boolean" />
			<asp:Parameter Name="pTxRxType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDMMail" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:Parameter DefaultValue="05" Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="55" Name="pMailTemplateType2" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUrge" runat="server" SelectMethod="GetOne" TypeName="Urge" OnSelected="dsUrge_Selected" OnSelecting="dsUrge_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProductBuy" runat="server" SelectMethod="GetPageCollectionByUser" SelectCountMethod="GetPageCountByUser" TypeName="ProductBuyHistory"
		EnablePaging="true" OnSelecting="dsProductBuy_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManFriends" runat="server" EnablePaging="true" OnSelecting="dsManFriends_Selecting" SelectCountMethod="GetManPageCount" SelectMethod="GetManPageCollection"
		TypeName="Friends">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pFriendIntroCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastFriends" runat="server" EnablePaging="true" OnSelecting="dsCastFriends_Selecting" SelectCountMethod="GetCastPageCount"
		SelectMethod="GetCastPageCollection" TypeName="Friends">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pFriendIntroCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserDefPointHistory" runat="server" EnablePaging="true" SelectMethod="GetPageCollection" TypeName="UserDefPointHistory" OnSelecting="dsUserDefPointHistory_Selecting"
		SelectCountMethod="GetPageCount">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsModifyMailHis" runat="server" EnablePaging="true" OnSelecting="dsModifyMailHis_Selecting" SelectCountMethod="GetModifyMailHistoryCount"
		SelectMethod="GetModifyMailHistory" TypeName="UserHistory">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNgMailHistory" runat="server" EnablePaging="true" OnSelecting="dsNgMailHistory_Selecting" SelectCountMethod="GetPageCount"
		SelectMethod="GetPageCollection" TypeName="NgMailHistory">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsModifyTelHis" runat="server" EnablePaging="true" OnSelecting="dsModifyTelHis_Selecting" SelectCountMethod="GetModifyTelHistoryCount"
		SelectMethod="GetModifyTelHistory" TypeName="UserHistory">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsModifyLoginPassword" runat="server" EnablePaging="false" OnSelecting="dsModifyLoginPassword_Selecting" SelectMethod="GetManModifyLoginPassword" TypeName="UserHistory">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList" TypeName="GameItemCategory" OnSelecting="dsGameItemCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType" TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" DefaultValue="" />
			<asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
			<asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPossessionGameItem" runat="server" EnablePaging="true" OnSelecting="dsPossessionGameItem_Selecting" SelectCountMethod="GetPageCount"
		SelectMethod="GetPageCollection" TypeName="PossessionGameItem">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsFanClubFee" runat="server" SelectMethod="GetPageCollection" TypeName="SettleLog" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsFanClubFee_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pSettleType" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pSettleStatus" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pSettleCompanyCd" Type="String" />
            <asp:Parameter Name="pAnswerDayFrom" Type="String" />
            <asp:Parameter Name="pAnswerDayTo" Type="String" />
            <asp:Parameter Name="pFanClubFeeFlag" Type="String" />
            <asp:Parameter Name="pSettleCancelFlag" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWithdrawalHistoryList" runat="server" SelectMethod="GetPageCollection" TypeName="WithdrawalHistory" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsWithdrawalHistoryList_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReceiptAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtServicePoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMailAvaHour" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPointTransferAvaHour" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUserSeq" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdrServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdrMailAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender37" TargetControlID="vdrPointTransferAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrMailTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrMailTemplate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrModifyUserNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender6" runat="server" TargetControlID="btnDelWithdrawal" ConfirmText="退会申請の削除を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender7" runat="server" TargetControlID="btnUpdateWithdrawal" ConfirmText="退会申請の備考を更新しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender8" runat="server" TargetControlID="btnAcceptWithdrawal" ConfirmText="退会申請を受理しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender19" runat="server" TargetControlID="btnRegistWithdrawalHistory" ConfirmText="退会履歴を登録しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender14" runat="server" TargetControlID="btnUpdateWithdrawalHistory" ConfirmText="退会履歴を更新しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender15" runat="server" TargetControlID="btnDeleteWithdrawalHistory" ConfirmText="退会履歴を削除しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender16" runat="server" TargetControlID="btnAddPoint300" ConfirmText="300円分ポイントを付与して継続ステータスに更新しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender17" runat="server" TargetControlID="btnAddPoint500" ConfirmText="500円分ポイントを付与して継続ステータスに更新しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender18" runat="server" TargetControlID="btnAddPoint1000" ConfirmText="1000円分ポイントを付与して継続ステータスに更新しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnReceipt" ConfirmText="入金更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnSettleInstant" ConfirmText="即時決済を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnAddProfileFillDocPoint" ConfirmText="プロフ設定ポイントを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnAddProfilePicPoint" ConfirmText="プロフ写真ポイントを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender5" runat="server" TargetControlID="btnAddProfilePicFacePoint" ConfirmText="プロフ顔写真ポイントを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender9" runat="server" TargetControlID="btnRemarks" ConfirmText="備考の更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender10" runat="server" TargetControlID="btnAddPoint" ConfirmText="ポイントを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender11" runat="server" TargetControlID="btnAddGamePoint" ConfirmText="ゲーム用ポイントを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender12" runat="server" TargetControlID="btnAddExp" ConfirmText="経験値を追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender13" runat="server" TargetControlID="btnAddGameItem" ConfirmText="アイテムを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="makReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" Enabled="true" FilterType="Custom, Numbers" TargetControlID="txtAddPointAmt"
		ValidChars="-" />
</asp:Content>
