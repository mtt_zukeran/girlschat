﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManRejectEmailAddrList.aspx.cs" Inherits="Man_ManRejectEmailAddrList" Title="会員登録拒否ﾒｰﾙｱﾄﾞﾚｽ一覧" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="会員登録拒否ﾒｰﾙｱﾄﾞﾚｽ一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px; margin-bottom:3px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							最終拒否日時
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLastRejectDateFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							～
							<asp:TextBox ID="txtLastRejectDateTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdrLastRejectDateFrom" runat="server" ErrorMessage="最終拒否日時(開始)を正しく入力して下さい。" ControlToValidate="txtLastRejectDateFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrLastRejectDateTo" runat="server" ErrorMessage="最終拒否日時(終了)を正しく入力して下さい。" ControlToValidate="txtLastRejectDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcLastRejectDateFromTo" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastRejectDateFrom" ControlToValidate="txtLastRejectDateTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							拒否ﾒｰﾙｱﾄﾞﾚｽ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtRejectEmailAddr" runat="server" Width="300px" MaxLength="120"></asp:TextBox>
							※部分一致
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnCsv" Text="CSV出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset class="fieldset">
			<legend>[一覧]</legend>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">
					Record Count <%# GetRecCount() %>
				</a>
				<br />
				<a class="reccount">
					Current viewing page <%=grdData.PageIndex + 1%> of <%=grdData.PageCount%>
				</a>
			</asp:Panel>
			&nbsp;
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
				<asp:GridView ID="grdData" runat="server" DataSourceID="" AllowSorting="false" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridView" PageSize="100">
				    <Columns>
						<asp:BoundField DataField="LAST_REJECT_DATE" HeaderText="最終拒否日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
						</asp:BoundField>
						<asp:BoundField DataField="REJECT_EMAIL_ADDR" HeaderText="拒否ﾒｰﾙｱﾄﾞﾚｽ" HtmlEncode="False">
						</asp:BoundField>
						<asp:BoundField DataField="REJECT_COUNT" HeaderText="拒否回数" DataFormatString="{0}回" HtmlEncode="False">
						</asp:BoundField>
						<asp:TemplateField HeaderText="登録状態">
							<ItemTemplate>
								<asp:Label ID="lblRegistStatus" runat="server" Text='<%# GetRegistStatusText(Eval("REGIST_STATUS")) %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="REGIST_COMPLITE_DATE" HeaderText="登録完了日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
						</asp:BoundField>
						<asp:BoundField DataField="REGIST_EMAIL_ADDR" HeaderText="登録ﾒｰﾙｱﾄﾞﾚｽ" HtmlEncode="False">
						</asp:BoundField>
						<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
							<ItemTemplate><asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("ManView.aspx?site=A001&manloginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink></ItemTemplate>
						</asp:TemplateField>
				    </Columns>
				    <PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsData" runat="server" TypeName="ManRejectEmailAddr" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsData_Selecting" OnSelected="dsData_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrLastRejectDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrLastRejectDateTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcLastRejectDateFromTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskLastRejectDateFrom" TargetControlID="txtLastRejectDateFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastRejectDateTo" TargetControlID="txtLastRejectDateTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>

