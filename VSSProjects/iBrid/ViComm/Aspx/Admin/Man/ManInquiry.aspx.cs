﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員検索
--	Progaram ID		: ManInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;
using System.Reflection;

public partial class Man_ManInquiry:System.Web.UI.Page {
	// 一覧表示項目選択チェックボックス　並び順はGridViewと合わせること。


	protected static readonly string[] ColumnsArray = new string[] { "ログインID","TEL","ﾊﾝﾄﾞﾙﾈｰﾑ","ランク","状態","広告ｺｰﾄﾞ","残Pt","ｻｰﾋﾞｽPt","請求額","入金累計額","ポイントアフリ計","メールアドレス","登録日","最終ﾛｸﾞｲﾝ日","最終課金日" };
	private static readonly string[] ColumnsSelectedArray = new string[] { "ログインID","TEL","ﾊﾝﾄﾞﾙﾈｰﾑ","ランク","状態","広告ｺｰﾄﾞ","残Pt","ｻｰﾋﾞｽPt","入金累計額","ポイントアフリ計","登録日","最終ﾛｸﾞｲﾝ日","最終課金日" };

	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}
	#endregion

	private string TxMailCastUserSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TxMailCastUserSeq"]);
		}
		set {
			this.ViewState["TxMailCastUserSeq"] = value;
		}
	}

	private string MailTemplateType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MailTemplateType"]);
		}
		set {
			this.ViewState["MailTemplateType"] = value;
		}
	}

	private string recCount = "";
	private string mailTemplateType = "";
	private bool bClaer = false;

	// お知らせメール予約送信編集用
	private PageCollectionParameters oReservationParamlist;
	private TxInfoMailHistory.MainteData oReservationMailParamlist;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			// 予約送信条件の初期化
			this.oReservationParamlist = null;
			this.oReservationMailParamlist = null;

			InitPage();
			this.SetupSelectColumns();
			if (!Request.QueryString.ToString().Equals("")) {
				GetList();
			}
		}

		//GridViewHelper.RegiserCreateGridHeader(1, this.grdUserManCharacter, this.pnlGrid);
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		pnlKey.Enabled = true;
		pnlInfo.Visible = false;
		btnInfoSetupMail.Visible = false;
		btnApproachSetupMail.Visible = false;
		btnCleaningMail.Visible = false;
		btnToResignedMail.Visible = false;
		lblCsvPassword.Visible = false;
		txtCsvPassword.Visible = false;
		btnOutputCsv.Visible = false;
		btnSetupBulkUpdate.Visible = false;
		pnlTxMail.Visible = false;
		pnlBulkUpdate.Visible = false;
		pnlPointMailOutcome.Visible = false;
		grdUserManCharacter.PageSize = 100;

		this.pnlSeekConditionMainte.Visible = false;
		this.InitSeekConditionList();


		bClaer = true;
		lstInfoMailRxType.DataBind();
		lstCastMailRxType.DataBind();

		ClearField();

		lstSiteCd.DataBind();
		//lstMailTemplateNo.DataBind();
		//lstUserStatus.DataBind();
		//rdoMailSendType.DataBind();

		SetupUserRank();
		SetupInqAdminManType();
		if (!iBridUtil.GetStringValue(Request.QueryString["sitecd"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			lstSiteCd_SelectedIndexChanged(lstSiteCd,null);
		} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			lstSiteCd_SelectedIndexChanged(lstSiteCd,null);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["registdayfrom"]).Equals("")) {
			txtRegistDayFrom.Text = iBridUtil.GetStringValue(Request.QueryString["registdayfrom"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["registdayto"]).Equals("")) {
			txtRegistDayTo.Text = iBridUtil.GetStringValue(Request.QueryString["registdayto"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["adcd"]).Equals("")) {
			txtAdCd.Text = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["adnm"]).Equals("")) {
			lblAdNm.Text = iBridUtil.GetStringValue(Request.QueryString["adnm"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["registtimefrom"]).Equals("")) {
			txtRegistTimeFrom.Text = iBridUtil.GetStringValue(Request.QueryString["registtimefrom"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["registtimeto"]).Equals("")) {
			txtRegistTimeTo.Text = iBridUtil.GetStringValue(Request.QueryString["registtimeto"]);
		}

		this.plcExtension.Visible = true;
		SetupUrgeSearch();

		DisplayWordUtil.ReplaceValidatorErrorMessage(this.Page.Validators);

		using (ManageCompany oCompany = new ManageCompany()) {
			if (oCompany.IsAvailableService(ViCommConst.RELEASE_SELECT_HTML_TEXT_VISIBLE)) {
				plcMailSendType.Visible = false;
			}

			pnlActivated.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE);

			rdoActivatedAll.Checked = !pnlActivated.Visible;
			rdoActivatedOnry.Checked = pnlActivated.Visible;
			rdoActivatedNG.Checked = false;

			this.lnkSelectColumns.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_INQUIRY_SELECT_COLUMNS,2);

			bool isEnableNegative = oCompany.IsAvailableService(ViCommConst.RELEASE_SEEK_CONDITION_NEGATIVE,2);
			this.chkLoginIdListNegativeFlag.Visible = isEnableNegative;
			this.chkLoginIdNegativeFlag.Visible = isEnableNegative;
			this.chkLastLoginDayNegativeFlag.Visible = isEnableNegative;
			this.chkLastPointUsedDayNegativeFlag.Visible = isEnableNegative;
			this.chkRegistDayNegativeFlag.Visible = isEnableNegative;
			this.chkUserDefineMaskNegatieFlag.Visible = isEnableNegative;
		}

		this.chkNaFlag.DataBind();
		this.chkUserDefineMask.DataBind();
		this.chkGameCharacterType.DataBind();
		this.chkSelectColumns.DataBind();

		this.SortExpression = string.Empty;
		this.SortDirect = string.Empty;
		this.SetupConditionLoginIdList(false);
		this.SetupConditionAdCdList(false);

		// お知らせメール送信履歴からの遷移
		// (予約送信条件の設定)
		this.GetReservationParamList();
		if (this.oReservationParamlist != null) {
			PageCollectionParameters oParamlist = this.oReservationParamlist;

			// 会員ランク
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_A) != 0) {
				chkRankA.Checked = true;
			}
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_B) != 0) {
				chkRankB.Checked = true;
			}
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_C) != 0) {
				chkRankC.Checked = true;
			}
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_D) != 0) {
				chkRankD.Checked = true;
			}
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_E) != 0) {
				chkRankE.Checked = true;
			}
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_F) != 0) {
				chkRankF.Checked = true;
			}
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_G) != 0) {
				chkRankG.Checked = true;
			}
			if ((oParamlist.iUserRankMask & ViCommConst.MASK_RANK_H) != 0) {
				chkRankH.Checked = true;
			}

			// 会員状態
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_NEW) != 0) {
				chkManNew.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_BLACK) != 0) {
				chkManBlack.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_NO_RECEIPT) != 0) {
				chkManNoReceipt.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_NO_USED) != 0) {
				chkManNonUsed.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_NORMAL) != 0) {
				chkManNormal.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_DELAY) != 0) {
				chkManDelay.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_HOLD) != 0) {
				chkManHold.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_RESIGNED) != 0) {
				chkManResigned.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_STOP) != 0) {
				chkManStop.Checked = true;
			}
			if ((oParamlist.iUserStatusRank & ViCommConst.MASK_MAN_TEL_NO_AUTH) != 0) {
				chkManTelNoAuth.Checked = true;
			}

			// オンライン状態
			if ((oParamlist.iUserOnlineStatus & ViCommConst.MASK_LOGINED) != 0) {
				chkOnline.Checked = true;
			}
			if ((oParamlist.iUserOnlineStatus & ViCommConst.MASK_OFFLINE) != 0) {
				chkOffline.Checked = true;
			}
			if ((oParamlist.iUserOnlineStatus & ViCommConst.MASK_TALKING) != 0) {
				chkTalking.Checked = true;
			}
			if ((oParamlist.iUserOnlineStatus & ViCommConst.MASK_WAITING) != 0) {
				chkWaiting.Checked = true;
			}

			// 利用キャリア
			if ((oParamlist.iCarrier & ViCommConst.MASK_DOCOMO) != 0) {
				chkDocomo.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_KDDI) != 0) {
				chkAu.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_SOFTBANK) != 0) {
				chkSoftbank.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_ANDROID) != 0) {
				chkAndroid.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_IPHONE) != 0) {
				chkIphone.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_OTHER) != 0) {
				chkOther.Checked = true;
			}

			// 登録キャリア
			if ((oParamlist.iRegistCarrierCd & ViCommConst.MASK_DOCOMO) != 0) {
				chkRegistDocomo.Checked = true;
			}
			if ((oParamlist.iRegistCarrierCd & ViCommConst.MASK_KDDI) != 0) {
				chkRegistAu.Checked = true;
			}
			if ((oParamlist.iRegistCarrierCd & ViCommConst.MASK_SOFTBANK) != 0) {
				chkRegistSoftbank.Checked = true;
			}
			if ((oParamlist.iRegistCarrierCd & ViCommConst.MASK_ANDROID) != 0) {
				chkRegistAndroid.Checked = true;
			}
			if ((oParamlist.iRegistCarrierCd & ViCommConst.MASK_IPHONE) != 0) {
				chkRegistIphone.Checked = true;
			}
			if ((oParamlist.iRegistCarrierCd & ViCommConst.MASK_OTHER) != 0) {
				chkRegistOther.Checked = true;
			}

			// メールアドレス状態
			if ((oParamlist.iNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_OK) != 0) {
				chkMailAddrOk.Checked = true;
			}
			if ((oParamlist.iNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_FILTERING_ERROR) != 0) {
				chkMailAddrFilteringError.Checked = true;
			}
			if ((oParamlist.iNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_NG) != 0) {
				chkMailAddrNg.Checked = true;
			}

			// アクティベート
			if (oParamlist.iWithUnRegistComplete == null) {
				rdoActivatedAll.Checked = true;
			} else if (oParamlist.iWithUnRegistComplete == ViCommConst.NA_CHAR_NONE) {
				rdoActivatedOnry.Checked = true;
			} else {
				rdoActivatedNG.Checked = true;
			}

			// サイト利用状況
			if (string.IsNullOrEmpty(oParamlist.sSiteUseStatus)) {
				// チェックなし
			} else {
				string[] aUseStatus = oParamlist.sSiteUseStatus.Split(',');
				foreach (string sUseStatus in aUseStatus) {
					if (sUseStatus.Equals("0")) {
						chkChat.Checked = true;
					} else if (sUseStatus.Equals("1")) {
						chkGame.Checked = true;
					} else if (sUseStatus.Equals("2")) {
						chkChatGamet.Checked = true;
					}
				}
			}

			// キャラクタ状態
			if (oParamlist.sNaFlag.Equals(ViCommConst.WITHOUT)) {
				// チェックなし
			} else {
				string[] aNaFlag = oParamlist.sNaFlag.Split(',');
				foreach (ListItem oListItem in chkNaFlag.Items) {
					foreach (string sNaFlag in aNaFlag) {
						if (sNaFlag.Equals(oListItem.Value)) {
							oListItem.Selected = true;
							break;
						}
					}
				}
			}

			// ユーザー定義フラグ
			if (string.IsNullOrEmpty(oParamlist.sUserDefineMask)) {
				// チェックなし
			} else {
				int iUserDefineMask = 0;
				if (int.TryParse(oParamlist.sUserDefineMask,out iUserDefineMask)) {
					foreach (ListItem oListItem in chkUserDefineMask.Items) {
						if ((iUserDefineMask & int.Parse(oListItem.Value)) != 0) {
							oListItem.Selected = true;
						}
					}
				}
			}

			// ゲームキャラクタータイプ
			if (string.IsNullOrEmpty(oParamlist.sGameCharacterType)) {
				// チェックなし
			} else {
				string[] aGameCharacterType = oParamlist.sGameCharacterType.Split(',');
				foreach (ListItem oListItem in chkGameCharacterType.Items) {
					foreach (string sGameCharaType in aGameCharacterType) {
						if (sGameCharaType.Equals(string.Format("'{0}'",oListItem.Value))) {
							oListItem.Selected = true;
							break;
						}
					}
				}
			}

			// ログインID
			if (Regex.IsMatch(oParamlist.sLoginID,",|\r?\n")) {
				// 複数入力
				txtLoginIdList.Text = oParamlist.sLoginID;
				this.SetupConditionLoginIdList(true);
			} else {
				txtLoginId.Text = oParamlist.sLoginID;
			}

			// 広告コード
			if (Regex.IsMatch(oParamlist.sAdCd,",|\r?\n")) {
				// 複数入力
				txtAdCdList.Text = oParamlist.sAdCd;
				this.SetupConditionAdCdList(true);
			} else {
				txtAdCd.Text = oParamlist.sAdCd;
			}

			this.TxMailCastUserSeq = oParamlist.sExceptRefusedByCastUserSeq;
			this.SortExpression = oParamlist.sSortExpression;
			this.SortDirect = oParamlist.sSortDirection;

			lstSiteCd.SelectedValue = oParamlist.sSiteCd;
			lstManAttrSeq0.SelectedValue = oParamlist.sManAttrValue1;
			lstManAttrSeq1.SelectedValue = oParamlist.sManAttrValue2;
			lstManAttrSeq2.SelectedValue = oParamlist.sManAttrValue3;
			lstManAttrSeq3.SelectedValue = oParamlist.sManAttrValue4;

			this.DropDownList_SetSelectedIndexByValue(lstUrgeLevel,oParamlist.sUrgeLevel);
			this.DropDownList_SetSelectedIndexByValue(lstCastMailRxType,oParamlist.sCastMailRxType);
			this.DropDownList_SetSelectedIndexByValue(lstInfoMailRxType,oParamlist.sInfoMailRxType);

			rdoTxMuller3NgMailAddrFlag.SelectedValue = oParamlist.sTxMuller3NgMailAddrFlag;

			this.RadioButtonList_SetSelectedIndexByValue(rdoUseCrosmileFlag,oParamlist.sUseCrosmileFlag);
			this.RadioButtonList_SetSelectedIndexByValue(rdoUseVoiceappFlag,oParamlist.sUseVoiceappFlag);
			this.RadioButtonList_SetSelectedIndexByValue(rdoPointUseFlag,oParamlist.sPointUseFlag);
			this.RadioButtonList_SetSelectedIndexByValue(rdoAuthFlag,oParamlist.sAuthFlag);
			this.RadioButtonList_SetSelectedIndexByValue(rdoUserTestType,oParamlist.sUserTestType);

			txtTel.Text = oParamlist.sTel;
			txtBeforeSystemId.Text = oParamlist.sBeforeSystemId;
			txtKeyword.Text = oParamlist.sKeyword;
			txtBalPointFrom.Text = oParamlist.sBalPointFrom;
			txtBalPointTo.Text = oParamlist.sBalPointTo;
			txtEMailAddr.Text = oParamlist.sEMailAddr;
			txtHandleNm.Text = oParamlist.sHandleNm;
			txtRemarks.Text = oParamlist.sRemarks;
			txtTotalReceiptAmtFrom.Text = oParamlist.sTotalReceiptAmtFrom;
			txtTotalReceiptAmtTo.Text = oParamlist.sTotalReceiptAmtTo;
			txtRegistDayFrom.Text = oParamlist.sRegistDayFrom;
			txtRegistDayTo.Text = oParamlist.sRegistDayTo;
			txtFirstLoginDayFrom.Text = oParamlist.sFirstLoginDayFrom;
			txtFirstLoginDayTo.Text = oParamlist.sFirstLoginDayTo;
			txtLastLoginDayFrom.Text = oParamlist.sLastLoginDayFrom;
			txtLastLoginDayTo.Text = oParamlist.sLastLoginDayTo;
			txtLastPointUsedDayFrom.Text = oParamlist.sLastPointUsedDayFrom;
			txtLastPointUsedDayTo.Text = oParamlist.sLastPointUsedDayTo;
			txtFirstReceiptDayFrom.Text = oParamlist.sFirstReceiptDayFrom;
			txtFirstReceiptDayTo.Text = oParamlist.sFirstReceiptDayTo;
			txtLastReceiptDayFrom.Text = oParamlist.sLastReceiptDayFrom;
			txtLastReceiptDayTo.Text = oParamlist.sLastReceiptDayTo;
			txtTotalReceiptCountFrom.Text = oParamlist.sTotalReceiptCountFrom;
			txtTotalReceiptCountTo.Text = oParamlist.sTotalReceiptCountTo;
			txtFirstLoginTimeFrom.Text = oParamlist.sFirstLoginTimeFrom;
			txtFirstLoginTimeTo.Text = oParamlist.sFirstLoginTimeTo;
			txtLastLoginTimeFrom.Text = oParamlist.sLastLoginTimeFrom;
			txtLastLoginTimeTo.Text = oParamlist.sLastLoginTimeTo;
			txtLastPointUsedTimeFrom.Text = oParamlist.sLastPointUsedTimeFrom;
			txtLastPointUsedTimeTo.Text = oParamlist.sLastPointUsedTimeTo;
			txtRegistTimeFrom.Text = oParamlist.sRegistTimeFrom;
			txtRegistTimeTo.Text = oParamlist.sRegistTimeTo;
			txtLastBlackDayFrom.Text = oParamlist.sLastBlackDayFrom;
			txtLastBlackDayTo.Text = oParamlist.sLastBlackDayTo;
			txtBillAmtFrom.Text = oParamlist.sBillAmtFrom;
			txtBillAmtTo.Text = oParamlist.sBillAmtTo;
			txtMchaTotalReceiptAmtFrom.Text = oParamlist.sMchaTotalReceiptAmtFrom;
			txtMchaTotalReceiptAmtTo.Text = oParamlist.sMchaTotalReceiptAmtTo;
			txtMchaTotalReceiptCountFrom.Text = oParamlist.sMchaTotalReceiptCountFrom;
			txtMchaTotalReceiptCountTo.Text = oParamlist.sMchaTotalReceiptCountTo;
			chkManCreditMeasured.Checked = oParamlist.bCreditMeasured;
			chkMchaUsedFlag.Checked = oParamlist.bMchaUsedFlag;
			chkCreditUsedFlag.Checked = oParamlist.bCreditUsedFlag;
			txtGuid.Text = oParamlist.sGuid;
			chkPointAffiFlag.Checked = oParamlist.bPointAffiFlag;
			chkIntroducerFriendAllFlag.Checked = oParamlist.bIntroducerFriendAllFlag;
			txtRemarks4.Text = oParamlist.sRemarks4;
			txtRegistCarrier.Text = oParamlist.sRegistCarrier;
			txtMobileCarrier.Text = oParamlist.sMobileCarrier;
			chkAdCdIsNull.Checked = oParamlist.bIsNullAdCd;
			txtReportAffiliateDateFrom.Text = oParamlist.sReportAffiliateDateFrom;
			txtReportAffiliateDateTo.Text = oParamlist.sReportAffiliateDateTo;
			txtChkExistAddrExeCountFrom.Text = oParamlist.sChkExistAddrExeCountFrom;
			txtChkExistAddrExeCountTo.Text = oParamlist.sChkExistAddrExeCountTo;
			txtServicePointGetCountFrom.Text = oParamlist.sServicePointGetCountFrom;
			txtServicePointGetCountTo.Text = oParamlist.sServicePointGetCountTo;
			txtGameHandleNm.Text = oParamlist.sGameHandleNm;
			txtGameRegistDateFrom.Text = oParamlist.sGameRegistDateFrom;
			txtGameRegistDateTo.Text = oParamlist.sGameRegistDateTo;
			txtGameCharacterLevelFrom.Text = oParamlist.sGameCharacterLevelFrom;
			txtGameCharacterLevelTo.Text = oParamlist.sGameCharacterLevelTo;
			txtGamePointFrom.Text = oParamlist.sGamePointFrom;
			txtGamePointTo.Text = oParamlist.sGamePointTo;
			chkHandleNmExactFlag.Checked = oParamlist.bHandleNmExactFlag;
			if (chkLoginIdListVisible.Checked) {
				chkLoginIdListNegativeFlag.Checked = oParamlist.bLoginIdNegativeFlag;
			} else {
				chkLoginIdNegativeFlag.Checked = oParamlist.bLoginIdNegativeFlag;
			}
			chkLastLoginDayNegativeFlag.Checked = oParamlist.bLastLoginDayNegativeFlag;
			chkLastPointUsedDayNegativeFlag.Checked = oParamlist.bLastPointUsedDayNegativeFlag;
			chkRegistDayNegativeFlag.Checked = oParamlist.bRegistDayNegativeFlag;
			chkUserDefineMaskNegatieFlag.Checked = oParamlist.bUserDefineMaskNegatieFlag;
			txtCrosmileLastUsedVersionFrom.Text = oParamlist.sCrosmileLastUsedVersionFrom;
			txtCrosmileLastUsedVersionTo.Text = oParamlist.sCrosmileLastUsedVersionTo;
			chkTalkSmartDirectNotMonitor.Checked = oParamlist.bTalkDirectSmartNotMonitorFlag;
			chkTelIsNull.Checked = oParamlist.bIsNullTel;
			txtUserSeq.Text = oParamlist.sUserSeq;
			txtGcappLastLoginVersionFrom.Text = oParamlist.sGcappLastLoginVersionFrom;
			txtGcappLastLoginVersionTo.Text = oParamlist.sGcappLastLoginVersionTo;
			chkNotExistFirstPointUseDate.Checked = oParamlist.bNotExistFirstPointUsedDate;
			chkExcludeEmailAddr.Checked = oParamlist.bExcludeEmailAddr;
			chkExcludeAdCd.Checked = oParamlist.bExcludeAdCd;
			txtMchaReceiptDateFrom.Text = oParamlist.sMchaReceiptDateFrom;
			if (!string.IsNullOrEmpty(oParamlist.sMchaReceiptDateTo)) {
				DateTime dtTo = System.DateTime.ParseExact(oParamlist.sMchaReceiptDateTo,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
				txtMchaReceiptDateTo.Text = dtTo.AddDays(-1).ToString("yyyy/MM/dd");
			}
		}// (予約送信条件の設定終了)
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		txtFirstLoginTimeFrom.Text = "";
		txtFirstLoginTimeTo.Text = "";
		txtLastLoginTimeFrom.Text = "";
		txtLastLoginTimeTo.Text = "";
		txtLastPointUsedTimeFrom.Text = "";
		txtLastPointUsedTimeTo.Text = "";
		txtRegistTimeFrom.Text = "";
		txtRegistTimeTo.Text = "";
		txtTel.Text = "";
		txtLoginId.Text = "";
		txtLoginIdList.Text = "";
		txtBalPointFrom.Text = "";
		txtBalPointTo.Text = "";
		txtEMailAddr.Text = "";
		txtHandleNm.Text = "";
		txtAdCd.Text = "";
		txtAdCdList.Text = "";
		lblAdNm.Text = "";
		txtRemarks.Text = "";
		txtCastLoginId.Text = "";
		txtTotalReceiptAmtFrom.Text = "";
		txtTotalReceiptAmtTo.Text = "";
		txtMchaTotalReceiptAmtFrom.Text = "";
		txtMchaTotalReceiptAmtTo.Text = "";
		txtMchaTotalReceiptCountFrom.Text = "";
		txtMchaTotalReceiptCountTo.Text = "";
		txtBillAmtFrom.Text = "";
		txtBillAmtTo.Text = "";
		txtRegistDayFrom.Text = "";
		txtRegistDayTo.Text = "";
		txtFirstLoginDayFrom.Text = "";
		txtFirstLoginDayTo.Text = "";
		txtLastLoginDayFrom.Text = "";
		txtLastLoginDayTo.Text = "";
		txtLastPointUsedDayFrom.Text = "";
		txtLastPointUsedDayTo.Text = "";
		txtFirstReceiptDayFrom.Text = "";
		txtFirstReceiptDayTo.Text = "";
		txtLastReceiptDayFrom.Text = "";
		txtLastReceiptDayTo.Text = "";
		txtTotalReceiptCountFrom.Text = "";
		txtTotalReceiptCountTo.Text = "";
		txtLastBlackDayFrom.Text = "";
		txtLastBlackDayTo.Text = "";
		txtGuid.Text = "";
		txtRemarks4.Text = "";
		txtBeforeSystemId.Text = string.Empty;
		txtKeyword.Text = string.Empty;
		chkRankA.Checked = false;
		chkRankB.Checked = false;
		chkRankC.Checked = false;
		chkRankD.Checked = false;
		chkRankE.Checked = false;
		chkRankF.Checked = false;
		chkRankG.Checked = false;
		chkRankH.Checked = false;
		chkNormal.Checked = false;
		chkManBlack.Checked = false;
		chkManNew.Checked = false;
		chkManNonUsed.Checked = false;
		chkManNoReceipt.Checked = false;
		chkManNormal.Checked = false;
		chkManHold.Checked = false;
		chkManResigned.Checked = false;
		chkManStop.Checked = false;
		chkManTelNoAuth.Checked = false;
		chkManDelay.Checked = false;
		chkManCreditMeasured.Checked = false;
		chkOnline.Checked = false;
		chkOffline.Checked = false;
		chkWaiting.Checked = false;
		chkTalking.Checked = false;
		chkMchaUsedFlag.Checked = false;
		chkCreditUsedFlag.Checked = false;
		//利用キャリア
		chkDocomo.Checked = false;
		chkAu.Checked = false;
		chkSoftbank.Checked = false;
		chkAndroid.Checked = false;
		chkIphone.Checked = false;
		chkOther.Checked = false;
		//登録キャリア
		chkRegistDocomo.Checked = false;
		chkRegistAu.Checked = false;
		chkRegistSoftbank.Checked = false;
		chkRegistAndroid.Checked = false;
		chkRegistIphone.Checked = false;
		chkRegistOther.Checked = false;
		//メールアドレス状態
		chkMailAddrOk.Checked = false;
		chkMailAddrFilteringError.Checked = false;
		chkMailAddrNg.Checked = false;
		
		chkPointAffiFlag.Checked = false;
		chkHandleNmExactFlag.Checked = false;
		recCount = "0";
		lstUrgeLevel.SelectedIndex = 0;
		lstCastMailRxType.SelectedIndex = 0;
		lstInfoMailRxType.SelectedIndex = 0;
		txtRegistCarrier.Text = string.Empty;
		txtMobileCarrier.Text = string.Empty;
		txtReportAffiliateDateFrom.Text = "";
		txtReportAffiliateDateTo.Text = "";
		chkNaFlag.SelectedIndex = -1;
		txtServicePointGetCountFrom.Text = string.Empty;
		txtServicePointGetCountTo.Text = string.Empty;
		txtChkExistAddrExeCountFrom.Text = string.Empty;
		txtChkExistAddrExeCountTo.Text = string.Empty;
		this.txtGameHandleNm.Text = string.Empty;
		this.txtGameRegistDateFrom.Text = string.Empty;
		this.txtGameRegistDateTo.Text = string.Empty;
		this.txtGameCharacterLevelFrom.Text = string.Empty;
		this.txtGameCharacterLevelTo.Text = string.Empty;
		this.txtGamePointFrom.Text = string.Empty;
		this.txtGamePointTo.Text = string.Empty;
		this.txtPointMailOutcomeRemarks.Text = string.Empty;
		this.chkLoginIdListNegativeFlag.Checked = false;
		this.chkLoginIdNegativeFlag.Checked = false;
		this.chkLastLoginDayNegativeFlag.Checked = false;
		this.chkLastPointUsedDayNegativeFlag.Checked = false;
		this.chkRegistDayNegativeFlag.Checked = false;
		this.chkUserDefineMaskNegatieFlag.Checked = false;
		this.chkIntroducerFriendAllFlag.Checked = false;
		this.txtCrosmileLastUsedVersionFrom.Text = string.Empty;
		this.txtCrosmileLastUsedVersionTo.Text = string.Empty;
		this.rdoUseCrosmileFlag.SelectedIndex = 0;
		txtUserSeq.Text = "";
		this.rdoUseVoiceappFlag.SelectedIndex = 0;
		
		this.rdoPointUseFlag.SelectedIndex = 0;
		this.rdoAuthFlag.SelectedIndex = 0;
		
		this.txtGcappLastLoginVersionFrom.Text = string.Empty;
		this.txtGcappLastLoginVersionTo.Text = string.Empty;
		this.rdoUserTestType.SelectedIndex = 0;
		this.chkExcludeEmailAddr.Checked = false;
		this.rdoTxMuller3NgMailAddrFlag.SelectedValue = string.Empty;

		// ﾎﾟｲﾝﾄｱﾌﾘ決済日
		txtMchaReceiptDateFrom.Text = string.Empty;
		txtMchaReceiptDateTo.Text = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			this.SortExpression = string.Empty;
			this.SortDirect = string.Empty;
			GetList();
		}
	}
	protected void btnSetupBulkUpdate_Click(object sender,EventArgs e) {
		this.pnlKey.Enabled = false;
		this.pnlBulkUpdate.Visible = true;
		this.btnBulkUpdUserStatus.Enabled = true;
		this.btnBulkUpdAdCd.Enabled = true;
		this.btnBulkUpdMailFlag.Enabled = true;
		this.btnBulkDelete.Enabled = true;
		this.btnBulkUpdTalkDirectSmartNotMonitor.Enabled = true;
		this.chkNotMonitor.Checked = false;

		this.lstUserStatus.DataBind();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
		this.SetupSelectColumns();
		this.SetupSelectColumnsVisible(true);
	}

	protected void btnInfoSetupMail_Click(object sender,EventArgs e) {
		setSetupMail(ViCommConst.MAIL_TP_INFO);
	}

	protected void btnApproachSetupMail_Click(object sender,EventArgs e) {
		setSetupMail(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER);
	}

	protected void btnCleaningMail_Click(object sender,EventArgs e) {
		setSetupMail(ViCommConst.MAIL_TP_CLEANING_ADDR);
	}

	protected void btnToResignedMail_Click(object sender,EventArgs e) {
		setSetupMail(ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED);
	}

	protected void btnOutputCsv_Click(object sender,EventArgs e) {
		bool bLimitFlag = false;
		
		if (grdUserManCharacter.Rows.Count == 0) {
			return;
		}

		string sCsvPassword = string.Empty;

		using (SysEx oSysEx = new SysEx()) {
			oSysEx.GetValue("CSV_PASSWORD",out sCsvPassword);
		}

		if (!sCsvPassword.Equals(txtCsvPassword.Text)) {
			bLimitFlag = true;
		}

		//データ取得 
		DataSet dsList = GetListDs(false);
		DataTable dtList = dsList.Tables[0];

		//ヘッダ作成 
		string sHeader =
				"サイト," +
				"ログインID," +
				"ログインパスワード," +
				"電話番号," +
				"電話番号認証済み," +
				"メールアドレス," +
				"メールアドレスNG," +
				"会員状態," +
				"登録日," +
				"最終利用日," +
				"残ポイント," +
				"入金累計回数," +
				"入金累計額," +
				"最終入金日," +
				"ポイントアフリ計," +
				"サービスポイント," +
				"サービスポイント有効日時," +
				DisplayWordUtil.Replace("キャストメール受信区分,") +
				"お知らせメール受信区分," +
				DisplayWordUtil.Replace("お気に入りのキャストがログインした場合に通知,") +
				DisplayWordUtil.Replace("会員をブックマークしているキャストがログインした場合に通知,") +
				"フリーダイアル利用," +
				"クレジット決済履歴," +
				"広告コード," +
				"広告名," +
				"紹介者コード," +
				"紹介元コード," +
				"ハンドル名," +
				"生年月日," +
				"前システムID," +
				"登録端末," +
				"最終ログイン日時," +
				"会員テストタイプ," +
				"認証," +
				"利用端末," +
				"最終利用日";

		string[] sAttrTableNo = new string[0];

		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet ds = oUserManAttrType.GetPageCollection(lstSiteCd.SelectedValue,0,20);
			DataTable dt = ds.Tables[0];

			if (dt.Rows.Count != 0) {
				sAttrTableNo = new string[dt.Rows.Count];
				sHeader = sHeader + ",";
				for (int i = 0;i < dt.Rows.Count;i++) {
					//CSVヘッダ文字列 
					sHeader = sHeader + dt.Rows[i]["MAN_ATTR_TYPE_NM"].ToString();
					if (i == dt.Rows.Count - 1) {
						sHeader = sHeader + "\r\n";
					} else {
						sHeader = sHeader + ",";
					}
					sAttrTableNo[i] = (i + 1).ToString();
				}
			} else {
				sHeader = sHeader + "\r\n";
			}
		}

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=UserList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader);
		Response.Write("$NO_TRANS_START;");
		StringBuilder sData = new StringBuilder();
		foreach (DataRow drList in dtList.Rows) {
			sData.Length = 0;
			if (bLimitFlag == false) {
				sData.Append(setCsvString(drList["SITE_CD"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LOGIN_ID"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LOGIN_PASSWORD"].ToString())).Append(",");
				sData.Append(setCsvString(drList["TEL"].ToString())).Append(",");
				sData.Append(setCsvString(drList["TEL_ATTESTED_FLAG"].ToString())).Append(",");
				sData.Append(setCsvString(drList["EMAIL_ADDR"].ToString())).Append(",");
				sData.Append(setCsvString(drList["NON_EXIST_MAIL_ADDR_FLAG"].ToString())).Append(",");
				sData.Append(setCsvString(drList["USER_STATUS_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["REGIST_DATE"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LAST_POINT_USED_DATE"].ToString())).Append(",");
				sData.Append(setCsvString(drList["BAL_POINT"].ToString())).Append(",");
				sData.Append(setCsvString(drList["TOTAL_RECEIPT_COUNT"].ToString())).Append(",");
				sData.Append(setCsvString(drList["TOTAL_RECEIPT_AMT"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LAST_RECEIPT_DATE"].ToString())).Append(",");
				sData.Append(setCsvString(drList["POINT_AFFILIATE_TOTAL_AMT"].ToString())).Append(",");
				sData.Append(setCsvString(drList["SERVICE_POINT"].ToString())).Append(",");
				sData.Append(setCsvString(drList["SERVICE_POINT_EFFECTIVE_DATE"].ToString())).Append(",");
				sData.Append(setCsvString(drList["CAST_MAIL_RX_TYPE_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["INFO_MAIL_RX_TYPE_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LOGIN_MAIL_RX_FLAG1"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LOGIN_MAIL_RX_FLAG2"].ToString())).Append(",");
				sData.Append(setCsvString(drList["USE_FREE_DIAL_FLAG"].ToString())).Append(",");
				sData.Append(setCsvString(drList["EXIST_CREDIT_DEAL_FLAG"].ToString())).Append(",");
				sData.Append(setCsvString(drList["AD_CD"].ToString())).Append(",");
				sData.Append(setCsvString(drList["AD_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["FRIEND_INTRO_CD"].ToString())).Append(","); //紹介者コード
				//INTRODUCER_FRIEND_CD(紹介元コード)->CAST_LOGIN_IDに変更
				sData.Append(setCsvString(drList["CAST_LOGIN_ID"].ToString())).Append(",");  //キャストログインID
				sData.Append(setCsvString(drList["HANDLE_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["BIRTHDAY"].ToString())).Append(",");
				sData.Append(setCsvString(drList["BEFORE_SYSTEM_ID"].ToString())).Append(",");
				sData.Append(setCsvString(drList["REGIST_TERMINAL_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LAST_LOGIN_DATE"].ToString())).Append(",");
				sData.Append(setCsvString(drList["USER_TEST_TYPE"].ToString())).Append(",");
				if (drList["REGIST_SERVICE_POINT_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
					sData.Append(setCsvString("済")).Append(",");
				} 
				else {
					sData.Append(setCsvString("")).Append(",");
				}

				sData.Append(setCsvString(drList["MOBILE_TERMINAL_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LAST_POINT_USED_DATE"].ToString()));
			} else {
				sData.Append(",");
				sData.Append(setCsvString(drList["LOGIN_ID"].ToString())).Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(setCsvString(drList["REGIST_DATE"].ToString())).Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(setCsvString(drList["AD_CD"].ToString())).Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(setCsvString(drList["HANDLE_NM"].ToString())).Append(",");
				sData.Append(",");
				sData.Append(",");
				sData.Append(setCsvString(drList["REGIST_TERMINAL_NM"].ToString())).Append(",");
				sData.Append(setCsvString(drList["LAST_LOGIN_DATE"].ToString())).Append(",");
				sData.Append(setCsvString(drList["USER_TEST_TYPE"].ToString())).Append(",");
				if (drList["REGIST_SERVICE_POINT_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
					sData.Append(setCsvString("済")).Append(",");
				} 
				else {
					sData.Append(setCsvString("")).Append(",");
				}
				sData.Append(setCsvString(drList["MOBILE_TERMINAL_NM"].ToString())).Append(",");
			}
			//属性
			if (sAttrTableNo.Length != 0) {
				sData.Append(",");
				for (int i = 0;i < sAttrTableNo.Length;i++) {
					if (drList["INPUT_TYPE" + sAttrTableNo[i]].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						if (bLimitFlag == false) {
							sData.Append(setCsvString(drList["MAN_ATTR_INPUT_VALUE" + sAttrTableNo[i]].ToString()));
						}
					} else {
						if (bLimitFlag == false) {
							sData.Append(setCsvString(drList["MAN_ATTR_NM" + sAttrTableNo[i]].ToString()));
						}
					}
					if (i == sAttrTableNo.Length - 1) {
						sData.Append("\r\n");
					} else {
						sData.Append(",");
					}
				}
			} else {
				sData.Append("\r\n");
			}
			Response.Write(sData.ToString());
		}
		Response.Write("$NO_TRANS_END;");
		Response.End();
	}

	private string setCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	private void setSetupMail(string pMailTemplateType) {
		this.MailTemplateType = pMailTemplateType;
		pnlKey.Enabled = false;
		pnlTxMail.Visible = true;
		btnTxMail.Enabled = true;
		lblAttention.Text = "メール有効時間/Pt振替後有効時間に０を設定すると無期限になります。";
		txtServicePoint.Text = "0";
		txtMailAvaHour.Text = "0";
		txtPointTransferAvaHour.Text = "0";
		txtCastLoginId.Text = "";
		lblCastHandleNm.Text = "";
		mailTemplateType = pMailTemplateType;
		lblAttentionCleaning.Visible = false;
		lblAttentionToResigned.Visible = false;
		if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_CLEANING_ADDR)) {
			lblAttentionCleaning.Visible = true;
		}
		if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED)) {
			lblAttentionToResigned.Visible = true;
		}
		tdHeaderMailSendType.Attributes["class"] = "tdHeaderStyle2";

		// 予約送信設定
		plcReservationMail.Visible = false;
		btnTxReservationMail.Visible = false;

		if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER)) {
			trCastLoginId.Visible = true;
			using (Site oSite = new Site()) {
				if (oSite.GetMultiCharFlag(lstSiteCd.SelectedValue)) {
					tdHeaderCastCharNo.Visible = true;
					tdDataCastCharNo.Visible = true;
					tdDataCastLoginId.ColSpan = 1;
				} else {
					tdHeaderCastCharNo.Visible = false;
					tdDataCastCharNo.Visible = false;
					txtCastCharNo.Text = ViCommConst.MAIN_CHAR_NO;
					tdDataCastLoginId.ColSpan = 7;
				}
			}
		} else {
			trCastLoginId.Visible = false;
			if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_INFO)) {
				plcReservationMail.Visible = true;
				btnTxReservationMail.Visible = true;
				tdHeaderMailSendType.Attributes["class"] = "tdHeaderStyle";
			}
		}
		lstMailTemplateNo.DataBind();
		rdoMailSendType.DataBind();
		rdoMailSendType.SelectedIndex = 0;

		this.lnkMailTemplate.Text = "[プレビュー表示]";
		this.pnlMailTemplate.Visible = false;
	}

	protected void btnCancelTx_Click(object sender,EventArgs e) {
		pnlKey.Enabled = true;
		pnlTxMail.Visible = false;
	}

	protected void btnCancelBulkUpdate_Click(object sender,EventArgs e) {
		pnlKey.Enabled = true;
		pnlBulkUpdate.Visible = false;
	}

	protected void btnExecuteBulkUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;

		List<string> oUserSeqList = new List<string>();
		using (DataSet oDs = this.GetListDs(false)) {
			if (oDs.Tables.Count > 0) {
				foreach (DataRow oDr in oDs.Tables[0].Rows) {
					string sUserSeq = iBridUtil.GetStringValue(oDr["USER_SEQ"]);
					if (!string.IsNullOrEmpty(sUserSeq)) {
						oUserSeqList.Add(sUserSeq);
					}
				}
			}
		}
		if (oUserSeqList.Count > 0) {

			string sModifyUserNm = "BULK_UPDATE";
			int sModifyType = -1;
			string sUserStatus = null;
			string sAdCd = null;
			string pMAIL_ADDR_FLAG = null;
			string pTALK_SMART_DIRECT_NOT_MONITOR = null;

			if (this.tabBulkUpdate.ActiveTabIndex == 0) {
				sModifyType = 16;
				pTALK_SMART_DIRECT_NOT_MONITOR = this.chkNotMonitor.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
			} else if (this.tabBulkUpdate.ActiveTabIndex == 1) {
				sModifyType = 2;
				sAdCd = this.txtAdCd4BulkUpd.Text;
			} else if (this.tabBulkUpdate.ActiveTabIndex == 2) {
				sModifyType = 4;
				pMAIL_ADDR_FLAG = ViCommConst.FLAG_OFF_STR;
			} else if (this.tabBulkUpdate.ActiveTabIndex == 3) {
				sModifyType = 8;
			} else if (this.tabBulkUpdate.ActiveTabIndex == 4) {
				sModifyType = 16;
				pTALK_SMART_DIRECT_NOT_MONITOR = this.chkNotMonitor.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
			} else {
				throw new InvalidOperationException();
			}

			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("USER_MAN_BULK_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
				oDbSession.ProcedureInArrayParm("pUSER_SEQ_ARR",DbSession.DbType.VARCHAR2,oUserSeqList.Count,oUserSeqList.ToArray());
				oDbSession.ProcedureInParm("pMODIFY_USER_NM",DbSession.DbType.VARCHAR2,sModifyUserNm);
				oDbSession.ProcedureInParm("pMODIFY_TYPE",DbSession.DbType.NUMBER,sModifyType);
				oDbSession.ProcedureInParm("pUSER_STATUS",DbSession.DbType.VARCHAR2,sUserStatus);
				oDbSession.ProcedureInParm("pAD_CD",DbSession.DbType.VARCHAR2,sAdCd);
				oDbSession.ProcedureInParm("pMAIL_ADDR_FLAG",DbSession.DbType.NUMBER,pMAIL_ADDR_FLAG);
				oDbSession.ProcedureInParm("pTALK_SMART_DIRECT_NOT_MONITOR",DbSession.DbType.NUMBER,pTALK_SMART_DIRECT_NOT_MONITOR);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}
		this.btnBulkUpdUserStatus.Enabled = false;
		this.btnBulkUpdAdCd.Enabled = false;
		this.btnBulkUpdMailFlag.Enabled = false;
		this.btnBulkDelete.Enabled = false;
		this.btnBulkUpdTalkDirectSmartNotMonitor.Enabled = false;

		this.GetList();
	}

	protected void lnkMailTemplate_Click(object sender,EventArgs e) {
		this.pnlMailTemplate.Visible = !this.pnlMailTemplate.Visible;
		if (this.pnlMailTemplate.Visible) {
			this.lnkMailTemplate.Text = "[プレビュー非表示]";
			this.grdMailTemplate.DataSourceID = "dsMailTemplate";
			this.grdMailTemplate.DataBind();
		} else {
			this.lnkMailTemplate.Text = "[プレビュー表示]";
			this.grdMailTemplate.DataSourceID = string.Empty;
		}

		this.SetSearchConditon(this.pnlMailTemplate.Visible);
	}

	protected void lnkPointMailOutcome_Click(object sender,EventArgs e) {
		this.pnlPointMailOutcome.Visible = !this.pnlPointMailOutcome.Visible;
		this.chkPointMailOutcome.Checked = this.pnlPointMailOutcome.Visible;
	}

	protected void lnkCondition_Click(object sender,EventArgs e) {
		SetSearchConditon(pnlKey.Visible);

		//GridViewHelper.RegiserCreateGridHeader(1, this.grdUserManCharacter, this.pnlGrid);
	}

	private void SetSearchConditon(bool pIsPanelVisible) {
		if (pIsPanelVisible) {
			pnlKey.Visible = false;
			pnlGrid.Height = 550;
			lnkCondition.Text = "[検索条件表示]";
		} else {
			pnlKey.Visible = true;
			pnlGrid.Height = 290;
			lnkCondition.Text = "[条件非表示]";
		}
	}

	protected void btnTxMail_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		int iPointMailOutcomeSeq = 0;
		if (this.chkPointMailOutcome.Checked && int.Parse(txtServicePoint.Text) > 0) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("CREATE_POINT_MAIL_OUTCOME_LOG");
				db.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtPointMailOutcomeRemarks.Text.Trim());
				db.ProcedureInParm("pSERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtServicePoint.Text));
				db.ProcedureOutParm("pPOINT_MAIL_OUTCOME_SEQ",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();

				int.TryParse(db.GetStringValue("pPOINT_MAIL_OUTCOME_SEQ"),out iPointMailOutcomeSeq);
			}
		}

		DataSet ds = GetListDs(true);

		DataRow dr = null;
		string[] sUserSeq = new string[ds.Tables[0].Rows.Count];
		for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
			dr = ds.Tables[0].Rows[i];
			sUserSeq[i] = dr["USER_SEQ"].ToString();
		}

		string[] sDoc;
		int iDocCount;
		string sMailSendType = this.rdoMailSendType.SelectedValue;
		if (plcMailSendType.Visible == false) {
			sMailSendType = string.Empty;
		}

		SysPrograms.SeparateHtml("",ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		AdminMailSender oAdminMailSender = new AdminToManMailSender(
												lstSiteCd.SelectedValue,
												this.lstMailTemplateNo.SelectedValue,
												sMailSendType,
												this.txtCastLoginId.Text,
												this.txtCastCharNo.Text,
												SessionObjs.AdminId,
												sUserSeq,
												int.Parse(txtMailAvaHour.Text),
												int.Parse(txtPointTransferAvaHour.Text),
												int.Parse(txtServicePoint.Text),
												string.Empty,
												sDoc,
												iDocCount,
												iPointMailOutcomeSeq,
												ViCommConst.FLAG_OFF,
												this.rdoMailServer.SelectedValue);
		if (!oAdminMailSender.Send()) {
			btnTxMail.Enabled = false;
			this.lblAttention.Text = "実行中のﾒｰﾙ送信処理が存在するため送信できません。";
			return;
		}

		// アプローチメールの場合、足あとをつける
		if (ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER.Equals(this.MailTemplateType)) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("BATCH_MAIL_MARKING");
				db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
				db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.TxMailCastUserSeq);
				db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.txtCastCharNo.Text);
				db.ProcedureInArrayParm("pMAN_USER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
				db.ProcedureInParm("pMAN_USER_COUNT",DbSession.DbType.NUMBER,sUserSeq.Length);
				db.ProcedureInParm("pMARKING_DATE",DbSession.DbType.VARCHAR2,string.Empty);
				db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}
		// メール送信情報を保存
		else if (ViCommConst.MAIL_TP_INFO.Equals(this.MailTemplateType)) {
			TxInfoMailHistory.MainteData oMailHisData = new TxInfoMailHistory.MainteData();
			SetTxInfoMailHistory(ref oMailHisData,ViCommConst.FLAG_OFF_STR);
		}

		btnTxMail.Enabled = false;
		lblAttention.Text = "メール送信が完了しました。";
	}



	private DataSet GetListDs(bool pExceptRefused) {
		DataSet ds = new DataSet();

		using (UserManCharacter oCharacter = new UserManCharacter()) {

			// 画面情報の取得
			PageCollectionParameters oParamlist = GetSearchParamList();

			ds = oCharacter.GetPageCollection(
							oParamlist.sSiteCd,
							oParamlist.iUserRankMask,
							oParamlist.iUserStatusRank,
							oParamlist.iUserOnlineStatus,
							oParamlist.iCarrier,
							oParamlist.sTel,
							oParamlist.sLoginID,
							oParamlist.sBalPointFrom,
							oParamlist.sBalPointTo,
							oParamlist.sEMailAddr,
							oParamlist.sHandleNm,
							oParamlist.sAdCd,
							oParamlist.sRemarks,
							oParamlist.sTotalReceiptAmtFrom,
							oParamlist.sTotalReceiptAmtTo,
							oParamlist.sRegistDayFrom,
							oParamlist.sRegistDayTo,
							oParamlist.sFirstLoginDayFrom,
							oParamlist.sFirstLoginDayTo,
							oParamlist.sLastLoginDayFrom,
							oParamlist.sLastLoginDayTo,
							oParamlist.sLastPointUsedDayFrom,
							oParamlist.sLastPointUsedDayTo,
							oParamlist.sFirstReceiptDayFrom,
							oParamlist.sFirstReceiptDayTo,
							oParamlist.sLastReceiptDayFrom,
							oParamlist.sLastReceiptDayTo,
							oParamlist.sTotalReceiptCountFrom,
							oParamlist.sTotalReceiptCountTo,
							oParamlist.iNonExistMailAddrFlagMask,
							oParamlist.sFirstLoginTimeFrom,
							oParamlist.sFirstLoginTimeTo,
							oParamlist.sLastLoginTimeFrom,
							oParamlist.sLastLoginTimeTo,
							oParamlist.sLastPointUsedTimeFrom,
							oParamlist.sLastPointUsedTimeTo,
							oParamlist.sRegistTimeFrom,
							oParamlist.sRegistTimeTo,
							oParamlist.sLastBlackDayFrom,
							oParamlist.sLastBlackDayTo,
							oParamlist.sBillAmtFrom,
							oParamlist.sBillAmtTo,
							oParamlist.sMchaTotalReceiptAmtFrom,
							oParamlist.sMchaTotalReceiptAmtTo,
							oParamlist.sMchaTotalReceiptCountFrom,
							oParamlist.sMchaTotalReceiptCountTo,
							oParamlist.sUrgeLevel,
							oParamlist.bCreditMeasured,
							oParamlist.bMchaUsedFlag,
							oParamlist.bCreditUsedFlag,
							oParamlist.sManAttrValue1,
							oParamlist.sManAttrValue2,
							oParamlist.sManAttrValue3,
							oParamlist.sManAttrValue4,
							oParamlist.iWithUnRegistComplete,
							oParamlist.sGuid,
							oParamlist.bPointAffiFlag,
							oParamlist.sCastMailRxType,
							oParamlist.sInfoMailRxType,
							oParamlist.sIntroducerFriendCd,
							oParamlist.sRemarks4,
							oParamlist.sRegistCarrier,
							oParamlist.sMobileCarrier,
							oParamlist.bIsNullAdCd,
							oParamlist.sNaFlag,
							oParamlist.sReportAffiliateDateFrom,
							oParamlist.sReportAffiliateDateTo,
							oParamlist.sServicePointGetCountFrom,
							oParamlist.sServicePointGetCountTo,
							oParamlist.sChkExistAddrExeCountFrom,
							oParamlist.sChkExistAddrExeCountTo,
							oParamlist.sSiteUseStatus,
							oParamlist.sUserDefineMask,
							pExceptRefused ? oParamlist.sExceptRefusedByCastUserSeq : string.Empty,
							oParamlist.sGameHandleNm,
							oParamlist.sGameCharacterType,
							oParamlist.sGameRegistDateFrom,
							oParamlist.sGameRegistDateTo,
							oParamlist.sGameCharacterLevelFrom,
							oParamlist.sGameCharacterLevelTo,
							oParamlist.sGamePointFrom,
							oParamlist.sGamePointTo,
							oParamlist.bHandleNmExactFlag,
							oParamlist.sBeforeSystemId,
							oParamlist.sKeyword,
							oParamlist.bLoginIdNegativeFlag,
							oParamlist.bLastLoginDayNegativeFlag,
							oParamlist.bLastPointUsedDayNegativeFlag,
							oParamlist.bRegistDayNegativeFlag,
							oParamlist.bUserDefineMaskNegatieFlag,
							oParamlist.bIntroducerFriendAllFlag,
							oParamlist.sCrosmileLastUsedVersionFrom,
							oParamlist.sCrosmileLastUsedVersionTo,
							oParamlist.bTalkDirectSmartNotMonitorFlag,
							oParamlist.sUseCrosmileFlag,
							string.Empty,
							string.Empty,
							0,
							SysConst.DB_MAX_ROWS,
							oParamlist.bIsNullTel,
							oParamlist.sUserSeq,
							oParamlist.sUseVoiceappFlag,
							oParamlist.sPointUseFlag,
							oParamlist.sAuthFlag,
							oParamlist.sGcappLastLoginVersionFrom,
							oParamlist.sGcappLastLoginVersionTo,
							oParamlist.sUserTestType,
							oParamlist.iRegistCarrierCd,
							oParamlist.bNotExistFirstPointUsedDate,
							oParamlist.bExcludeEmailAddr,
							oParamlist.sTxMuller3NgMailAddrFlag,
							oParamlist.sRichinoRankFrom,
							oParamlist.sRichinoRankTo,
							oParamlist.bExcludeAdCd,
							oParamlist.sMchaReceiptDateFrom,
							oParamlist.sMchaReceiptDateTo
			);
		}
		return ds;
	}

	private void GetList() {

		if (txtAdCd.Text.Equals(string.Empty)) {
			lblAdNm.Text = string.Empty;
		} else {
			lblAdNm.Text = "<br>" + lblAdNm.Text;
		}

		bClaer = false;
		pnlInfo.Visible = true;
		grdUserManCharacter.PageIndex = 0;
		grdUserManCharacter.DataSourceID = "dsUserManCharacter";
		grdUserManCharacter.DataBind();
		pnlCount.DataBind();
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnInfoSetupMail.Visible = (iCompare >= 0);
		btnApproachSetupMail.Visible = (iCompare >= 0);
		lblCsvPassword.Visible = (iCompare >= 0);
		txtCsvPassword.Visible = (iCompare >= 0);
		btnOutputCsv.Visible = (iCompare >= 0);

		string sAvaCheckExistAddrFlag = ViCommConst.FLAG_OFF_STR;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("AVA_CHECK_EXIST_ADDR_FLAG",out sAvaCheckExistAddrFlag);
		}

		if (sAvaCheckExistAddrFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			btnCleaningMail.Visible = (iCompare >= 0);
		}

		btnSetupBulkUpdate.Visible = (iCompare >= 0);
		btnToResignedMail.Visible = (iCompare >= 0);
		if (btnSetupBulkUpdate.Visible) {
			using (ManageCompany oCompany = new ManageCompany()) {
				bool bEnableBulkUpdate = oCompany.IsAvailableService(ViCommConst.RELEASE_BULK_UPDATE_MAN);
				this.btnSetupBulkUpdate.Visible = bEnableBulkUpdate;
				tpgBulkUpdateUserStatus.Visible = false;
				tpgBulkUpdateAdCd.Visible = false;
				tpgBulkUpdateMailFlag.Visible = false;
				TabPanel1.Visible = false;
			}
		}

		// お知らせメール送信履歴からの遷移
		if (!IsPostBack) {
			if (this.oReservationParamlist != null) {
				// お知らせメール送信欄の表示
				setSetupMail(ViCommConst.MAIL_TP_INFO);

				// お知らせメール送信欄の初期値設定
				txtServicePoint.Text = this.oReservationParamlist.iServicePoint.ToString();
				txtMailAvaHour.Text = this.oReservationParamlist.iMailAvaHour.ToString();
				txtPointTransferAvaHour.Text = this.oReservationParamlist.iPointTransferAvaHour.ToString();
				lstMailTemplateNo.SelectedValue = this.oReservationMailParamlist.MailTemplateNo;
				rdoMailServer.SelectedValue = this.oReservationMailParamlist.MailServer;
				rdoMailSendType.SelectedValue = this.oReservationMailParamlist.MailSendType;
				if (!string.IsNullOrEmpty(this.oReservationMailParamlist.ReservationSendDate)) {
					DateTime oDT = Convert.ToDateTime(this.oReservationMailParamlist.ReservationSendDate);
					txtReservationSendDay.Text = oDT.ToString("yyyy/MM/dd");
					txtReservationSendHour.Text = oDT.ToString("HH");
					txtReservationSendMinute.Text = oDT.ToString("mm");
				}
			}
		}
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (Ad oAd = new Ad()) {
				string sAdNm = "";
				args.IsValid = oAd.IsExist(txtAdCd.Text,ref sAdNm);
				lblAdNm.Text = sAdNm;
			}
		}
	}
	protected void vdcAdCdList_ServerValidate(object source, ServerValidateEventArgs args) {
		if (args.IsValid) {

			string[] sAdCdArray = Regex.Split(txtAdCdList.Text, ",|\r?\n");
			lblAdNmList.Text = string.Empty;
			for (int i = 0; i < sAdCdArray.Length; i++) {
				using (Ad oAd = new Ad()) {
					string sAdNm = "";
					args.IsValid = oAd.IsExist(sAdCdArray[i], ref sAdNm);
					lblAdNmList.Text = lblAdNmList.Text + "<br />" + sAdNm;
					if (!args.IsValid) {
						this.vdcAdCdList.ErrorMessage = string.Format("広告コード[{0}]が未登録です。", sAdCdArray[i]);
						break;
					}
				}
			}
		}
	}

	protected void dsUserManCharacter_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsUserManCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		PageCollectionParameters oParamlist = GetSearchParamList();

		e.InputParameters[0] = oParamlist.sSiteCd;
		e.InputParameters[1] = oParamlist.iUserRankMask;
		e.InputParameters[2] = oParamlist.iUserStatusRank;
		e.InputParameters[3] = oParamlist.iUserOnlineStatus;
		e.InputParameters[4] = oParamlist.iCarrier;
		e.InputParameters[5] = oParamlist.sTel;
		e.InputParameters[6] = oParamlist.sLoginID;
		e.InputParameters[7] = oParamlist.sBalPointFrom;
		e.InputParameters[8] = oParamlist.sBalPointTo;
		e.InputParameters[9] = oParamlist.sEMailAddr;
		e.InputParameters[10] = oParamlist.sHandleNm;
		e.InputParameters[11] = oParamlist.sAdCd;
		e.InputParameters[12] = oParamlist.sRemarks;
		e.InputParameters[13] = oParamlist.sTotalReceiptAmtFrom;
		e.InputParameters[14] = oParamlist.sTotalReceiptAmtTo;
		e.InputParameters[15] = oParamlist.sRegistDayFrom;
		e.InputParameters[16] = oParamlist.sRegistDayTo;
		e.InputParameters[17] = oParamlist.sFirstLoginDayFrom;
		e.InputParameters[18] = oParamlist.sFirstLoginDayTo;
		e.InputParameters[19] = oParamlist.sLastLoginDayFrom;
		e.InputParameters[20] = oParamlist.sLastLoginDayTo;
		e.InputParameters[21] = oParamlist.sLastPointUsedDayFrom;
		e.InputParameters[22] = oParamlist.sLastPointUsedDayTo;
		e.InputParameters[23] = oParamlist.sFirstReceiptDayFrom;
		e.InputParameters[24] = oParamlist.sFirstReceiptDayTo;
		e.InputParameters[25] = oParamlist.sLastReceiptDayFrom;
		e.InputParameters[26] = oParamlist.sLastReceiptDayTo;
		e.InputParameters[27] = oParamlist.sTotalReceiptCountFrom;
		e.InputParameters[28] = oParamlist.sTotalReceiptCountTo;
		e.InputParameters[29] = oParamlist.iNonExistMailAddrFlagMask;
		e.InputParameters[30] = oParamlist.sFirstLoginTimeFrom;
		e.InputParameters[31] = oParamlist.sFirstLoginTimeTo;
		e.InputParameters[32] = oParamlist.sLastLoginTimeFrom;
		e.InputParameters[33] = oParamlist.sLastLoginTimeTo;
		e.InputParameters[34] = oParamlist.sLastPointUsedTimeFrom;
		e.InputParameters[35] = oParamlist.sLastPointUsedTimeTo;
		e.InputParameters[36] = oParamlist.sRegistTimeFrom;
		e.InputParameters[37] = oParamlist.sRegistTimeTo;
		e.InputParameters[38] = oParamlist.sLastBlackDayFrom;
		e.InputParameters[39] = oParamlist.sLastBlackDayTo;
		e.InputParameters[40] = oParamlist.sBillAmtFrom;
		e.InputParameters[41] = oParamlist.sBillAmtTo;
		e.InputParameters[42] = oParamlist.sMchaTotalReceiptAmtFrom;
		e.InputParameters[43] = oParamlist.sMchaTotalReceiptAmtTo;
		e.InputParameters[44] = oParamlist.sMchaTotalReceiptCountFrom;
		e.InputParameters[45] = oParamlist.sMchaTotalReceiptCountTo;
		e.InputParameters[46] = oParamlist.sUrgeLevel;
		e.InputParameters[47] = oParamlist.bCreditMeasured;
		e.InputParameters[48] = oParamlist.bMchaUsedFlag;
		e.InputParameters[49] = oParamlist.bCreditUsedFlag;
		e.InputParameters[50] = oParamlist.sManAttrValue1;
		e.InputParameters[51] = oParamlist.sManAttrValue2;
		e.InputParameters[52] = oParamlist.sManAttrValue3;
		e.InputParameters[53] = oParamlist.sManAttrValue4;
		e.InputParameters[54] = oParamlist.iWithUnRegistComplete;
		e.InputParameters[55] = oParamlist.sGuid;
		e.InputParameters[56] = oParamlist.bPointAffiFlag;
		e.InputParameters[57] = oParamlist.sCastMailRxType;
		e.InputParameters[58] = oParamlist.sInfoMailRxType;
		e.InputParameters[59] = oParamlist.sIntroducerFriendCd;
		e.InputParameters[60] = oParamlist.sRemarks4;
		e.InputParameters[61] = oParamlist.sRegistCarrier;
		e.InputParameters[62] = oParamlist.sMobileCarrier;
		e.InputParameters[63] = oParamlist.bIsNullAdCd;
		e.InputParameters[64] = oParamlist.sNaFlag;
		e.InputParameters[65] = oParamlist.sReportAffiliateDateFrom;
		e.InputParameters[66] = oParamlist.sReportAffiliateDateTo;
		e.InputParameters[67] = oParamlist.sServicePointGetCountFrom;
		e.InputParameters[68] = oParamlist.sServicePointGetCountTo;
		e.InputParameters[69] = oParamlist.sChkExistAddrExeCountFrom;
		e.InputParameters[70] = oParamlist.sChkExistAddrExeCountTo;
		e.InputParameters[71] = oParamlist.sSiteUseStatus;
		e.InputParameters[72] = oParamlist.sUserDefineMask;
		e.InputParameters[73] = oParamlist.sExceptRefusedByCastUserSeq;
		e.InputParameters[74] = oParamlist.sGameHandleNm;
		e.InputParameters[75] = oParamlist.sGameCharacterType;
		e.InputParameters[76] = oParamlist.sGameRegistDateFrom;
		e.InputParameters[77] = oParamlist.sGameRegistDateTo;
		e.InputParameters[78] = oParamlist.sGameCharacterLevelFrom;
		e.InputParameters[79] = oParamlist.sGameCharacterLevelTo;
		e.InputParameters[80] = oParamlist.sGamePointFrom;
		e.InputParameters[81] = oParamlist.sGamePointTo;
		e.InputParameters[82] = oParamlist.bHandleNmExactFlag;
		e.InputParameters[83] = oParamlist.sBeforeSystemId;
		e.InputParameters[84] = oParamlist.sKeyword;
		e.InputParameters[85] = oParamlist.bLoginIdNegativeFlag;
		e.InputParameters[86] = oParamlist.bLastLoginDayNegativeFlag;
		e.InputParameters[87] = oParamlist.bLastPointUsedDayNegativeFlag;
		e.InputParameters[88] = oParamlist.bRegistDayNegativeFlag;
		e.InputParameters[89] = oParamlist.bUserDefineMaskNegatieFlag;
		e.InputParameters[90] = oParamlist.bIntroducerFriendAllFlag;
		e.InputParameters[91] = oParamlist.sCrosmileLastUsedVersionFrom;
		e.InputParameters[92] = oParamlist.sCrosmileLastUsedVersionTo;
		e.InputParameters[93] = oParamlist.bTalkDirectSmartNotMonitorFlag;
		e.InputParameters[94] = oParamlist.sUseCrosmileFlag;
		e.InputParameters[95] = oParamlist.sSortExpression;
		e.InputParameters[96] = oParamlist.sSortDirection;
		e.InputParameters[97] = oParamlist.bIsNullTel;
		e.InputParameters[98] = oParamlist.sUserSeq;
		e.InputParameters[99] = oParamlist.sUseVoiceappFlag;

		e.InputParameters[100] = oParamlist.sPointUseFlag;
		e.InputParameters[101] = oParamlist.sAuthFlag;
		
		e.InputParameters[102] = oParamlist.sGcappLastLoginVersionFrom;
		e.InputParameters[103] = oParamlist.sGcappLastLoginVersionTo;
		e.InputParameters[104] = oParamlist.sUserTestType;
		e.InputParameters[105] = oParamlist.iRegistCarrierCd;
		e.InputParameters[106] = oParamlist.bNotExistFirstPointUsedDate;
		e.InputParameters[107] = oParamlist.bExcludeEmailAddr;
		e.InputParameters[108] = oParamlist.sTxMuller3NgMailAddrFlag;
		e.InputParameters[109] = oParamlist.sRichinoRankFrom;
		e.InputParameters[110] = oParamlist.sRichinoRankTo;
		e.InputParameters[111] = oParamlist.bExcludeAdCd;
		// ﾎﾟｲﾝﾄｱﾌﾘ決済日
		e.InputParameters[112] = oParamlist.sMchaReceiptDateFrom;
		e.InputParameters[113] = oParamlist.sMchaReceiptDateTo;
	}

	protected string GetServicePoint(object pServicePoint,object pEffectDate) {
		if (!pEffectDate.ToString().Equals("")) {
			DateTime dtEffect = DateTime.Parse(pEffectDate.ToString());
			if (dtEffect > DateTime.Now) {
				return pServicePoint.ToString();
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	protected string CheckAdminLevel(object pText) {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			return pText.ToString();
		} else {
			return "***********";
		}
	}

	private void SetupUserRank() {
		DataSet ds;
		string sUserRank = "";
		string sUserRankNm = "";

		chkRankA.Checked = false;
		chkRankB.Checked = false;
		chkRankC.Checked = false;
		chkRankD.Checked = false;
		chkRankE.Checked = false;
		chkRankF.Checked = false;
		chkRankG.Checked = false;
		chkRankH.Checked = false;
		chkRankI.Checked = false;
		chkRankJ.Checked = false;
		chkRankK.Checked = false;
		chkRankL.Checked = false;
		chkRankM.Checked = false;
		chkRankN.Checked = false;
		chkRankO.Checked = false;
		chkRankP.Checked = false;
		chkRankQ.Checked = false;
		chkRankR.Checked = false;
		chkRankS.Checked = false;
		chkRankT.Checked = false;
		chkRankU.Checked = false;
		chkRankV.Checked = false;
		chkRankW.Checked = false;
		chkRankX.Checked = false;
		chkRankY.Checked = false;
		chkRankZ.Checked = false;

		chkRankA.Visible = false;
		chkRankB.Visible = false;
		chkRankC.Visible = false;
		chkRankD.Visible = false;
		chkRankE.Visible = false;
		chkRankF.Visible = false;
		chkRankG.Visible = false;
		chkRankH.Visible = false;
		chkRankI.Visible = false;
		chkRankJ.Visible = false;
		chkRankK.Visible = false;
		chkRankL.Visible = false;
		chkRankM.Visible = false;
		chkRankN.Visible = false;
		chkRankO.Visible = false;
		chkRankP.Visible = false;
		chkRankQ.Visible = false;
		chkRankR.Visible = false;
		chkRankS.Visible = false;
		chkRankT.Visible = false;
		chkRankU.Visible = false;
		chkRankV.Visible = false;
		chkRankW.Visible = false;
		chkRankX.Visible = false;
		chkRankY.Visible = false;
		chkRankZ.Visible = false;

		using (UserRank oUserRank = new UserRank()) {
			ds = oUserRank.GetListIncDefault(lstSiteCd.SelectedValue);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				sUserRank = iBridUtil.GetStringValue(dr["USER_RANK"]);
				sUserRankNm = iBridUtil.GetStringValue(dr["USER_RANK_NM"]);

				if (!sUserRank.Equals(ViCommConst.DEFAUL_USER_RANK)) {
					CheckBox chkOk = (CheckBox)plcRank.FindControl(string.Format("chkRank{0}",sUserRank)) as CheckBox;
					chkOk.Visible = true;
					chkOk.Text = sUserRankNm;
				}
			}
		}
	}

	private void SetupInqAdminManType() {
		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			if (!oCompany.IsAvailableService(ViCommConst.RELEASE_ATTR_INQUIRY)) {
				plcAttrType.Visible = false;
				return;
			}
		}
		using (UserManAttrTypeValue oManAttrTypeValue = new UserManAttrTypeValue())
		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet dsUserManAttrType = oUserManAttrType.GetInqAdminManAttrType(lstSiteCd.SelectedValue);
			bool bDisp = false;
			for (int i = 0;i < ViCommConst.MAX_ADMIN_MAN_INQUIRY_ITEMS;i++) {
				Label lblManAttrNm = (Label)plcAttrType.FindControl(string.Format("lblManAttrNm{0}",i)) as Label;
				lblManAttrNm.Text = dsUserManAttrType.Tables[0].Rows[0][string.Format("MAN_ATTR_TYPE_NM{0}",i + 1)].ToString();

				DropDownList lstManAttrSeq = (DropDownList)plcAttrType.FindControl(string.Format("lstManAttrSeq{0}",i)) as DropDownList;
				lstManAttrSeq.Items.Clear();

				HtmlTableCell CellAttrNm = (HtmlTableCell)plcAttrType.FindControl(string.Format("celManAttrNm{0}",i)) as HtmlTableCell;
				HtmlTableCell CellAttrSeq = (HtmlTableCell)plcAttrType.FindControl(string.Format("celManAttrSeq{0}",i)) as HtmlTableCell;

				if (lblManAttrNm.Text == "") {
					CellAttrNm.Visible = false;
					CellAttrSeq.Visible = false;
				} else {
					bDisp = true;
					DataSet dsManAttrTypeValue = oManAttrTypeValue.GetList(lstSiteCd.SelectedValue,dsUserManAttrType.Tables[0].Rows[0][string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1)].ToString());

					lstManAttrSeq.Items.Add(new ListItem("指定なし",""));
					if (string.IsNullOrEmpty(dsUserManAttrType.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString())) {
						foreach (DataRow dr in dsManAttrTypeValue.Tables[0].Rows) {
							lstManAttrSeq.Items.Add(new ListItem(dr["MAN_ATTR_NM"].ToString(),dr["MAN_ATTR_SEQ"].ToString()));
						}
					} else {
						using (CodeDtl oCodeDtl = new CodeDtl()) {
							using (Code oCode = new Code()) {
								if (oCode.GetOne(dsUserManAttrType.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString())) {
									lblManAttrNm.Text = oCode.codeTypeNm;
								}
							}
							DataSet dsGroup = oCodeDtl.GetList(dsUserManAttrType.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString());
							foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
								lstManAttrSeq.Items.Add(new ListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
							}
						}
					}
					CellAttrNm.Visible = true;
					CellAttrSeq.Visible = true;
				}
			}
			plcAttrType.Visible = bDisp;
		}
	}

	private void SetupCondition(out int pRank,out int pStatus,out int pOnlineStatus,out int pCarrier, out int pRegistCarrierCd, out int pNonExistMailAddrFlag) {
		pRank = 0;
		pStatus = 0;
		pOnlineStatus = 0;
		pCarrier = 0;
		pRegistCarrierCd = 0;
		pNonExistMailAddrFlag = 0;

		if (chkRankA.Checked) {
			pRank += ViCommConst.MASK_RANK_A;
		}
		if (chkRankB.Checked) {
			pRank += ViCommConst.MASK_RANK_B;
		}
		if (chkRankC.Checked) {
			pRank += ViCommConst.MASK_RANK_C;
		}
		if (chkRankD.Checked) {
			pRank += ViCommConst.MASK_RANK_D;
		}
		if (chkRankE.Checked) {
			pRank += ViCommConst.MASK_RANK_E;
		}
		if (chkRankF.Checked) {
			pRank += ViCommConst.MASK_RANK_F;
		}
		if (chkRankG.Checked) {
			pRank += ViCommConst.MASK_RANK_G;
		}
		if (chkRankH.Checked) {
			pRank += ViCommConst.MASK_RANK_H;
		}

		if (chkManTelNoAuth.Checked || chkNormal.Checked) {
			pStatus += ViCommConst.MASK_MAN_TEL_NO_AUTH;
		}
		if (chkManNew.Checked || chkNormal.Checked) {
			pStatus += ViCommConst.MASK_MAN_NEW;
		}
		if (chkManNoReceipt.Checked || chkNormal.Checked) {
			pStatus += ViCommConst.MASK_MAN_NO_RECEIPT;
		}
		if (chkManNonUsed.Checked || chkNormal.Checked) {
			pStatus += ViCommConst.MASK_MAN_NO_USED;
		}
		if (chkManDelay.Checked) {
			pStatus += ViCommConst.MASK_MAN_DELAY;
		}
		if (chkManHold.Checked) {
			pStatus += ViCommConst.MASK_MAN_HOLD;
		}
		if (chkManResigned.Checked) {
			pStatus += ViCommConst.MASK_MAN_RESIGNED;
		}
		if (chkManStop.Checked) {
			pStatus += ViCommConst.MASK_MAN_STOP;
		}
		if (chkManBlack.Checked) {
			pStatus += ViCommConst.MASK_MAN_BLACK;
		}
		if (chkManNormal.Checked || chkNormal.Checked) {
			pStatus += ViCommConst.MASK_MAN_NORMAL;
		}

		if (chkOnline.Checked) {
			pOnlineStatus += ViCommConst.MASK_LOGINED;
		}
		if (chkOffline.Checked) {
			pOnlineStatus += ViCommConst.MASK_OFFLINE;
		}
		if (chkWaiting.Checked) {
			pOnlineStatus += ViCommConst.MASK_WAITING;
		}
		if (chkTalking.Checked) {
			pOnlineStatus += ViCommConst.MASK_TALKING;
		}

		//利用キャリア
		if (chkDocomo.Checked) {
			pCarrier += ViCommConst.MASK_DOCOMO;
		}
		if (chkAu.Checked) {
			pCarrier += ViCommConst.MASK_KDDI;
		}
		if (chkSoftbank.Checked) {
			pCarrier += ViCommConst.MASK_SOFTBANK;
		}
		if (chkAndroid.Checked) {
			pCarrier += ViCommConst.MASK_ANDROID;
		}
		if (chkIphone.Checked) {
			pCarrier += ViCommConst.MASK_IPHONE;
		}
		if (chkOther.Checked) {
			pCarrier += ViCommConst.MASK_OTHER;
		}
		
		//登録キャリア
		if (chkRegistDocomo.Checked) {
			pRegistCarrierCd += ViCommConst.MASK_DOCOMO;
		}
		if (chkRegistAu.Checked) {
			pRegistCarrierCd += ViCommConst.MASK_KDDI;
		}
		if (chkRegistSoftbank.Checked) {
			pRegistCarrierCd += ViCommConst.MASK_SOFTBANK;
		}
		if (chkRegistAndroid.Checked) {
			pRegistCarrierCd += ViCommConst.MASK_ANDROID;
		}
		if (chkRegistIphone.Checked) {
			pRegistCarrierCd += ViCommConst.MASK_IPHONE;
		}
		if (chkRegistOther.Checked) {
			pRegistCarrierCd += ViCommConst.MASK_OTHER;
		}
		
		//メールアドレス状態
		if (chkMailAddrOk.Checked) {
			pNonExistMailAddrFlag += ViCommConst.MSK_MAIL_ADDR_OK;
		}
		if (chkMailAddrFilteringError.Checked) {
			pNonExistMailAddrFlag += ViCommConst.MSK_MAIL_ADDR_FILTERING_ERROR;
		}
		if (chkMailAddrNg.Checked) {
			pNonExistMailAddrFlag += ViCommConst.MSK_MAIL_ADDR_NG;
		}

		if ((!txtBalPointFrom.Text.Equals("")) || (!txtBalPointTo.Text.Equals(""))) {
			if (txtBalPointFrom.Text.Equals("")) {
				txtBalPointFrom.Text = txtBalPointTo.Text;
			} else if (txtBalPointTo.Text.Equals("")) {
				txtBalPointTo.Text = txtBalPointFrom.Text;
			}
		}

		if ((!txtTotalReceiptAmtFrom.Text.Equals("")) || (!txtTotalReceiptAmtTo.Text.Equals(""))) {
			if (txtTotalReceiptAmtFrom.Text.Equals("")) {
				txtTotalReceiptAmtFrom.Text = txtTotalReceiptAmtTo.Text;
			} else if (txtTotalReceiptAmtTo.Text.Equals("")) {
				txtTotalReceiptAmtTo.Text = txtTotalReceiptAmtFrom.Text;
			}
		}

		if ((!txtBillAmtFrom.Text.Equals("")) || (!txtBillAmtTo.Text.Equals(""))) {
			if (txtBillAmtFrom.Text.Equals("")) {
				txtBillAmtFrom.Text = txtBillAmtTo.Text;
			} else if (txtBillAmtTo.Text.Equals("")) {
				txtBillAmtTo.Text = txtBillAmtFrom.Text;
			}
		}

		if ((!txtTotalReceiptCountFrom.Text.Equals("")) || (!txtTotalReceiptCountTo.Text.Equals(""))) {
			if (txtTotalReceiptCountFrom.Text.Equals("")) {
				txtTotalReceiptCountFrom.Text = txtTotalReceiptCountTo.Text;
			} else if (txtTotalReceiptCountTo.Text.Equals("")) {
				txtTotalReceiptCountTo.Text = txtTotalReceiptCountFrom.Text;
			}
		}

		if ((!txtMchaTotalReceiptAmtFrom.Text.Equals("")) || (!txtMchaTotalReceiptAmtTo.Text.Equals(""))) {
			if (txtMchaTotalReceiptAmtFrom.Text.Equals("")) {
				txtMchaTotalReceiptAmtFrom.Text = txtMchaTotalReceiptAmtTo.Text;
			} else if (txtMchaTotalReceiptAmtTo.Text.Equals("")) {
				txtMchaTotalReceiptAmtTo.Text = txtMchaTotalReceiptAmtFrom.Text;
			}
		}

		if ((!txtMchaTotalReceiptCountFrom.Text.Equals("")) || (!txtMchaTotalReceiptCountTo.Text.Equals(""))) {
			if (txtMchaTotalReceiptCountFrom.Text.Equals("")) {
				txtMchaTotalReceiptCountFrom.Text = txtMchaTotalReceiptCountTo.Text;
			} else if (txtMchaTotalReceiptCountTo.Text.Equals("")) {
				txtMchaTotalReceiptCountTo.Text = txtMchaTotalReceiptCountFrom.Text;
			}
		}

		if ((!txtRegistDayFrom.Text.Equals("")) || (!txtRegistDayTo.Text.Equals(""))) {
			if (txtRegistDayFrom.Text.Equals("")) {
				txtRegistDayFrom.Text = txtRegistDayTo.Text;
			} else if (txtRegistDayTo.Text.Equals("")) {
				txtRegistDayTo.Text = txtRegistDayFrom.Text;
			}
		}

		if ((!txtFirstLoginDayFrom.Text.Equals("")) || (!txtFirstLoginDayTo.Text.Equals(""))) {
			if (txtFirstLoginDayFrom.Text.Equals("")) {
				txtFirstLoginDayFrom.Text = txtFirstLoginDayTo.Text;
			} else if (txtFirstLoginDayTo.Text.Equals("")) {
				txtFirstLoginDayTo.Text = txtFirstLoginDayFrom.Text;
			}
		}

		if ((!txtLastLoginDayFrom.Text.Equals("")) || (!txtLastLoginDayTo.Text.Equals(""))) {
			if (txtLastLoginDayFrom.Text.Equals("")) {
				txtLastLoginDayFrom.Text = txtLastLoginDayTo.Text;
			} else if (txtLastLoginDayTo.Text.Equals("")) {
				txtLastLoginDayTo.Text = txtLastLoginDayFrom.Text;
			}
		}

		if ((!txtLastPointUsedDayFrom.Text.Equals("")) || (!txtLastPointUsedDayTo.Text.Equals(""))) {
			if (txtLastPointUsedDayFrom.Text.Equals("")) {
				txtLastPointUsedDayFrom.Text = txtLastPointUsedDayTo.Text;
			} else if (txtLastPointUsedDayTo.Text.Equals("")) {
				txtLastPointUsedDayTo.Text = txtLastPointUsedDayFrom.Text;
			}
		}

		if ((!txtFirstReceiptDayFrom.Text.Equals("")) || (!txtFirstReceiptDayTo.Text.Equals(""))) {
			if (txtFirstReceiptDayFrom.Text.Equals("")) {
				txtFirstReceiptDayFrom.Text = txtFirstReceiptDayTo.Text;
			} else if (txtFirstReceiptDayTo.Text.Equals("")) {
				txtFirstReceiptDayTo.Text = txtFirstReceiptDayFrom.Text;
			}
		}

		if ((!txtLastReceiptDayFrom.Text.Equals("")) || (!txtLastReceiptDayTo.Text.Equals(""))) {
			if (txtLastReceiptDayFrom.Text.Equals("")) {
				txtLastReceiptDayFrom.Text = txtLastReceiptDayTo.Text;
			} else if (txtLastReceiptDayTo.Text.Equals("")) {
				txtLastReceiptDayTo.Text = txtLastReceiptDayFrom.Text;
			}
		}

		if ((!txtReportAffiliateDateFrom.Text.Equals("")) || (!txtReportAffiliateDateTo.Text.Equals(""))) {
			if (txtReportAffiliateDateFrom.Text.Equals("")) {
				txtReportAffiliateDateFrom.Text = txtReportAffiliateDateTo.Text;
			} else if (txtReportAffiliateDateTo.Text.Equals("")) {
				txtReportAffiliateDateTo.Text = txtReportAffiliateDateFrom.Text;
			}
		}

		if ((!txtLastBlackDayFrom.Text.Equals("")) || (!txtLastBlackDayTo.Text.Equals(""))) {
			if (txtLastBlackDayFrom.Text.Equals("")) {
				txtLastBlackDayFrom.Text = txtLastBlackDayTo.Text;
			} else if (txtLastBlackDayTo.Text.Equals("")) {
				txtLastBlackDayTo.Text = txtLastBlackDayFrom.Text;
			}
		}

		if ((!txtServicePointGetCountFrom.Text.Equals("")) || (!txtServicePointGetCountTo.Text.Equals(""))) {
			if (txtServicePointGetCountFrom.Text.Equals("")) {
				txtServicePointGetCountFrom.Text = txtServicePointGetCountTo.Text;
			} else if (txtServicePointGetCountTo.Text.Equals("")) {
				txtServicePointGetCountTo.Text = txtServicePointGetCountFrom.Text;
			}
		}

		if ((!txtChkExistAddrExeCountFrom.Text.Equals("")) || (!txtChkExistAddrExeCountTo.Text.Equals(""))) {
			if (txtChkExistAddrExeCountFrom.Text.Equals("")) {
				txtChkExistAddrExeCountFrom.Text = txtChkExistAddrExeCountTo.Text;
			} else if (txtChkExistAddrExeCountTo.Text.Equals("")) {
				txtChkExistAddrExeCountTo.Text = txtChkExistAddrExeCountFrom.Text;
			}
		}

		if ((!txtCrosmileLastUsedVersionFrom.Text.Equals("")) || (!txtCrosmileLastUsedVersionTo.Text.Equals(""))) {
			if (txtCrosmileLastUsedVersionFrom.Text.Equals("")) {
				txtCrosmileLastUsedVersionFrom.Text = txtCrosmileLastUsedVersionTo.Text;
			} else if (txtCrosmileLastUsedVersionTo.Text.Equals("")) {
				txtCrosmileLastUsedVersionTo.Text = txtCrosmileLastUsedVersionFrom.Text;
			}
		}

		if ((!txtGcappLastLoginVersionFrom.Text.Equals("")) || (!txtGcappLastLoginVersionTo.Text.Equals(""))) {
			if (txtGcappLastLoginVersionFrom.Text.Equals("")) {
				txtGcappLastLoginVersionFrom.Text = txtGcappLastLoginVersionTo.Text;
			} else if (txtGcappLastLoginVersionTo.Text.Equals("")) {
				txtGcappLastLoginVersionTo.Text = txtGcappLastLoginVersionFrom.Text;
			}
		}
	}

	protected void dsDMMail_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = mailTemplateType;
	}

	protected void vdcCastLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (MailTemplate oTemplate = new MailTemplate()) {
				if (oTemplate.GetOne(lstSiteCd.SelectedValue,lstMailTemplateNo.SelectedValue)) {
					if (oTemplate.mailTemplateType.Equals(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER) == false) {
						txtCastLoginId.Text = "";
						return;
					}
				}
			}

			using (CastCharacter oCastCharacter = new CastCharacter()) {
				args.IsValid = oCastCharacter.IsExistLoginId(this.lstSiteCd.SelectedValue,txtCastLoginId.Text,ViCommConst.MAIN_CHAR_NO);
				if (args.IsValid) {
					lblCastHandleNm.Text = oCastCharacter.handleNm;
					this.TxMailCastUserSeq = oCastCharacter.userSeq;
				} else {
					lblCastHandleNm.Text = "";
					this.TxMailCastUserSeq = string.Empty;
				}
			}
		}
	}

	protected void vdcCastCharNo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (CastCharacter oCastCharacter = new CastCharacter()) {
				args.IsValid = oCastCharacter.IsExistUserCharNoByLoginId(lstSiteCd.SelectedValue,txtCastLoginId.Text,txtCastCharNo.Text);
			}
		}
	}
	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		SetupUserRank();
		SetupInqAdminManType();
		SetupUrgeSearch();
	}

	protected void lstMailTemplateNo_SelectedIndexChanged(object sender,EventArgs e) {
		if (this.pnlMailTemplate.Visible) {
			this.grdMailTemplate.DataSourceID = "dsMailTemplate";
			this.grdMailTemplate.DataBind();
		}
	}

	public class PageCollectionParameters {
		public string sSiteCd;
		public int iUserRankMask;
		public int iUserStatusRank;
		public string sNaFlag;
		public int iUserOnlineStatus;
		public int iCarrier;
		public string sTel;
		public string sLoginID;
		public string sBalPointFrom;
		public string sBalPointTo;
		public string sEMailAddr;
		public string sHandleNm;
		public string sAdCd;
		public string sRemarks;
		public string sTotalReceiptAmtFrom;
		public string sTotalReceiptAmtTo;
		public string sRegistDayFrom;
		public string sRegistDayTo;
		public string sFirstLoginDayFrom;
		public string sFirstLoginDayTo;
		public string sLastLoginDayFrom;
		public string sLastLoginDayTo;
		public string sLastPointUsedDayFrom;
		public string sLastPointUsedDayTo;
		public string sFirstReceiptDayFrom;
		public string sFirstReceiptDayTo;
		public string sLastReceiptDayFrom;
		public string sLastReceiptDayTo;
		public string sTotalReceiptCountFrom;
		public string sTotalReceiptCountTo;
		public int iNonExistMailAddrFlagMask;
		public string sFirstLoginTimeFrom;
		public string sFirstLoginTimeTo;
		public string sLastLoginTimeFrom;
		public string sLastLoginTimeTo;
		public string sLastPointUsedTimeFrom;
		public string sLastPointUsedTimeTo;
		public string sRegistTimeFrom;
		public string sRegistTimeTo;
		public string sLastBlackDayFrom;
		public string sLastBlackDayTo;
		public string sBillAmtFrom;
		public string sBillAmtTo;
		public string sMchaTotalReceiptAmtFrom;
		public string sMchaTotalReceiptAmtTo;
		public string sMchaTotalReceiptCountFrom;
		public string sMchaTotalReceiptCountTo;
		public string sUrgeLevel;
		public bool bCreditMeasured;
		public bool bMchaUsedFlag;
		public bool bCreditUsedFlag;
		public string sManAttrValue1;
		public string sManAttrValue2;
		public string sManAttrValue3;
		public string sManAttrValue4;
		public int? iWithUnRegistComplete;
		public string sGuid;
		public bool bPointAffiFlag;
		public string sCastMailRxType;
		public string sInfoMailRxType;
		public string sIntroducerFriendCd;
		public string sRemarks4;
		public string sRegistCarrier;
		public string sMobileCarrier;
		public bool bIsNullAdCd;
		public string sReportAffiliateDateFrom;
		public string sReportAffiliateDateTo;
		public string sServicePointGetCountFrom;
		public string sServicePointGetCountTo;
		public string sChkExistAddrExeCountFrom;
		public string sChkExistAddrExeCountTo;
		public string sSiteUseStatus;
		public string sUserDefineMask;
		public string sExceptRefusedByCastUserSeq;
		public string sGameHandleNm;
		public string sGameCharacterType;
		public string sGameRegistDateFrom;
		public string sGameRegistDateTo;
		public string sGameCharacterLevelFrom;
		public string sGameCharacterLevelTo;
		public string sGamePointFrom;
		public string sGamePointTo;
		public string sBeforeSystemId;
		public string sKeyword;
		public bool bHandleNmExactFlag;
		public bool bLoginIdNegativeFlag;
		public bool bLastLoginDayNegativeFlag;
		public bool bLastPointUsedDayNegativeFlag;
		public bool bRegistDayNegativeFlag;
		public bool bUserDefineMaskNegatieFlag;
		public bool bIntroducerFriendAllFlag;
		public string sCrosmileLastUsedVersionFrom;
		public string sCrosmileLastUsedVersionTo;
		public bool bTalkDirectSmartNotMonitorFlag;
		public string sUseCrosmileFlag;
		public string sSortExpression;
		public string sSortDirection;
		public bool bIsNullTel;
		public string sUserSeq;
		public string sUseVoiceappFlag;

		public string sPointUseFlag;
		public string sAuthFlag;
		
		public string sGcappLastLoginVersionFrom;
		public string sGcappLastLoginVersionTo;
		public string sUserTestType;
		public int iRegistCarrierCd;
		public bool bNotExistFirstPointUsedDate;
		public bool bExcludeEmailAddr;
		public string sTxMuller3NgMailAddrFlag;
		public string sRichinoRankFrom;
		public string sRichinoRankTo;
		public bool bExcludeAdCd;
		// ﾎﾟｲﾝﾄｱﾌﾘ決済日
		public string sMchaReceiptDateFrom;
		public string sMchaReceiptDateTo;
		// ｻｰﾋﾞｽPt
		public int iServicePoint;
		// ﾒｰﾙ有効時間
		public int iMailAvaHour;
		// Pt振替後有効時間
		public int iPointTransferAvaHour;

		public PageCollectionParameters() {
			this.sSiteCd = string.Empty;
			this.iUserRankMask = 0;
			this.iUserStatusRank = 0;
			this.sNaFlag = string.Empty;
			this.iUserOnlineStatus = 0;
			this.iCarrier = 0;
			this.sTel = string.Empty;
			this.sLoginID = string.Empty;
			this.sBalPointFrom = string.Empty;
			this.sBalPointTo = string.Empty;
			this.sEMailAddr = string.Empty;
			this.sHandleNm = string.Empty;
			this.sAdCd = string.Empty;
			this.sRemarks = string.Empty;
			this.sTotalReceiptAmtFrom = string.Empty;
			this.sTotalReceiptAmtTo = string.Empty;
			this.sRegistDayFrom = string.Empty;
			this.sRegistDayTo = string.Empty;
			this.sFirstLoginDayFrom = string.Empty;
			this.sFirstLoginDayTo = string.Empty;
			this.sLastLoginDayFrom = string.Empty;
			this.sLastLoginDayTo = string.Empty;
			this.sLastPointUsedDayFrom = string.Empty;
			this.sLastPointUsedDayTo = string.Empty;
			this.sFirstReceiptDayFrom = string.Empty;
			this.sFirstReceiptDayTo = string.Empty;
			this.sLastReceiptDayFrom = string.Empty;
			this.sLastReceiptDayTo = string.Empty;
			this.sTotalReceiptCountFrom = string.Empty;
			this.sTotalReceiptCountTo = string.Empty;
			this.iNonExistMailAddrFlagMask = 0;
			this.sFirstLoginTimeFrom = string.Empty;
			this.sFirstLoginTimeTo = string.Empty;
			this.sLastLoginTimeFrom = string.Empty;
			this.sLastLoginTimeTo = string.Empty;
			this.sLastPointUsedTimeFrom = string.Empty;
			this.sLastPointUsedTimeTo = string.Empty;
			this.sRegistTimeFrom = string.Empty;
			this.sRegistTimeTo = string.Empty;
			this.sLastBlackDayFrom = string.Empty;
			this.sLastBlackDayTo = string.Empty;
			this.sBillAmtFrom = string.Empty;
			this.sBillAmtTo = string.Empty;
			this.sMchaTotalReceiptAmtFrom = string.Empty;
			this.sMchaTotalReceiptAmtTo = string.Empty;
			this.sMchaTotalReceiptCountFrom = string.Empty;
			this.sMchaTotalReceiptCountTo = string.Empty;
			this.sUrgeLevel = string.Empty;
			this.bCreditMeasured = false;
			this.bMchaUsedFlag = false;
			this.bCreditUsedFlag = false;
			this.sManAttrValue1 = string.Empty;
			this.sManAttrValue2 = string.Empty;
			this.sManAttrValue3 = string.Empty;
			this.sManAttrValue4 = string.Empty;
			iWithUnRegistComplete = null;
			this.sGuid = string.Empty;
			this.bPointAffiFlag = false;
			this.sCastMailRxType = string.Empty;
			this.sInfoMailRxType = string.Empty;
			this.sIntroducerFriendCd = string.Empty;
			this.sRemarks4 = string.Empty;
			sRegistCarrier = string.Empty;
			sMobileCarrier = string.Empty;
			bIsNullAdCd = false;
			this.sReportAffiliateDateFrom = string.Empty;
			this.sReportAffiliateDateTo = string.Empty;
			this.sServicePointGetCountFrom = string.Empty;
			this.sServicePointGetCountTo = string.Empty;
			this.sChkExistAddrExeCountFrom = string.Empty;
			this.sChkExistAddrExeCountTo = string.Empty;
			this.sSiteUseStatus = string.Empty;
			this.sUserDefineMask = string.Empty;
			this.sExceptRefusedByCastUserSeq = string.Empty;
			this.sGameHandleNm = string.Empty;
			this.sGameCharacterType = string.Empty;
			this.sGameRegistDateFrom = string.Empty;
			this.sGameRegistDateTo = string.Empty;
			this.sGameCharacterLevelFrom = string.Empty;
			this.sGameCharacterLevelTo = string.Empty;
			this.sGamePointFrom = string.Empty;
			this.sGamePointTo = string.Empty;
			this.bHandleNmExactFlag = false;
			this.sBeforeSystemId = string.Empty;
			this.sKeyword = string.Empty;
			this.bLoginIdNegativeFlag = false;
			this.bLastLoginDayNegativeFlag = false;
			this.bLastPointUsedDayNegativeFlag = false;
			this.bRegistDayNegativeFlag = false;
			this.bUserDefineMaskNegatieFlag = false;
			this.bIntroducerFriendAllFlag = false;
			this.sCrosmileLastUsedVersionFrom = string.Empty;
			this.sCrosmileLastUsedVersionTo = string.Empty;
			this.bTalkDirectSmartNotMonitorFlag = false;
			this.sUseCrosmileFlag = string.Empty;
			this.sSortExpression = string.Empty;
			this.sSortDirection = string.Empty;
			bIsNullTel = false;
			this.sUserSeq = string.Empty;
			this.sUseVoiceappFlag = string.Empty;

			this.sPointUseFlag = string.Empty;
			this.sAuthFlag = string.Empty;
			
			this.sGcappLastLoginVersionFrom = string.Empty;
			this.sGcappLastLoginVersionTo = string.Empty;
			this.sUserTestType = string.Empty;
			this.iRegistCarrierCd = 0;
			this.bNotExistFirstPointUsedDate = false;
			this.bExcludeEmailAddr = false;
			this.sTxMuller3NgMailAddrFlag = string.Empty;
			this.sRichinoRankFrom = string.Empty;
			this.sRichinoRankTo = string.Empty;
			this.bExcludeAdCd = false;
			this.sMchaReceiptDateFrom = string.Empty;
			this.sMchaReceiptDateTo = string.Empty;
			this.iServicePoint = 0;
			this.iMailAvaHour = 0;
			this.iPointTransferAvaHour = 0;
		}
	}

	protected PageCollectionParameters GetSearchParamList() {

		PageCollectionParameters oParamlist = new PageCollectionParameters();
		int iRank,iStatus,iOnlineStatus,iCarrier,iRegistCarrierCd,iNonExistMailAddrFlag;
		SetupCondition(out iRank,out iStatus,out iOnlineStatus,out iCarrier,out iRegistCarrierCd,out iNonExistMailAddrFlag);
		string sSiteUseStatus = null;

		int? iWithUnRegistComplete = null;
		if (rdoActivatedAll.Checked == true) {
			iWithUnRegistComplete = null;
		} else if (rdoActivatedOnry.Checked == true) {
			iWithUnRegistComplete = ViCommConst.NA_CHAR_NONE;
		} else {
			iWithUnRegistComplete = ViCommConst.NA_CHAR_PF_NOT_APPROVED;
		}

		string sNaFlags = string.Empty;
		foreach (ListItem oListItem in chkNaFlag.Items) {
			if (!oListItem.Selected)
				continue;

			if (!sNaFlags.Equals(string.Empty)) {
				sNaFlags += ",";
			}
			sNaFlags += oListItem.Value;
		}
		if (sNaFlags.Equals(string.Empty)) {
			sNaFlags = ViCommConst.WITHOUT;
		}
		
		if (this.chkChat.Checked == true) {
			sSiteUseStatus = "0";
		}
		if (string.IsNullOrEmpty(sSiteUseStatus)) {
			if (this.chkGame.Checked == true) {
				sSiteUseStatus = "1";
			}			
		} else {
			if (this.chkGame.Checked == true) {
				sSiteUseStatus = sSiteUseStatus + ",1";
			}	
		}
		if (string.IsNullOrEmpty(sSiteUseStatus)) {
			if (this.chkChatGamet.Checked == true) {
				sSiteUseStatus = "2";
			}
		} else {
			if (this.chkChatGamet.Checked == true) {
				sSiteUseStatus = sSiteUseStatus + ",2";
			}
		}
	
		oParamlist.sSiteCd = lstSiteCd.SelectedValue;
		oParamlist.iUserRankMask = iRank;
		oParamlist.iUserStatusRank = iStatus;
		oParamlist.iUserOnlineStatus = iOnlineStatus;
		oParamlist.sNaFlag = sNaFlags;
		oParamlist.iCarrier = iCarrier;
		oParamlist.sTel = TrimEnd(txtTel.Text);
		oParamlist.sLoginID = this.chkLoginIdListVisible.Checked ? TrimEnd(txtLoginIdList.Text) : TrimEnd(txtLoginId.Text);
		oParamlist.sBeforeSystemId = txtBeforeSystemId.Text.Trim();
		oParamlist.sKeyword = txtKeyword.Text.Trim();
		oParamlist.sBalPointFrom = txtBalPointFrom.Text;
		oParamlist.sBalPointTo = txtBalPointTo.Text;
		oParamlist.sEMailAddr = TrimEnd(txtEMailAddr.Text);
		oParamlist.sHandleNm = TrimEnd(txtHandleNm.Text);
		oParamlist.sAdCd = this.chkAdCdListVisible.Checked ? TrimEnd(txtAdCdList.Text) :TrimEnd(txtAdCd.Text);
		oParamlist.sRemarks = TrimEnd(txtRemarks.Text);
		oParamlist.sTotalReceiptAmtFrom = txtTotalReceiptAmtFrom.Text;
		oParamlist.sTotalReceiptAmtTo = txtTotalReceiptAmtTo.Text;
		if (bClaer) {
			oParamlist.sRegistDayFrom = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");
			oParamlist.sRegistDayTo = DateTime.Now.ToString("yyyy/MM/dd");
		} else {
			oParamlist.sRegistDayFrom = txtRegistDayFrom.Text;
			oParamlist.sRegistDayTo = txtRegistDayTo.Text;
		}
		oParamlist.sFirstLoginDayFrom = txtFirstLoginDayFrom.Text;
		oParamlist.sFirstLoginDayTo = txtFirstLoginDayTo.Text;
		oParamlist.sLastLoginDayFrom = txtLastLoginDayFrom.Text;
		oParamlist.sLastLoginDayTo = txtLastLoginDayTo.Text;
		oParamlist.sLastPointUsedDayFrom = txtLastPointUsedDayFrom.Text;
		oParamlist.sLastPointUsedDayTo = txtLastPointUsedDayTo.Text;
		oParamlist.sFirstReceiptDayFrom = txtFirstReceiptDayFrom.Text;
		oParamlist.sFirstReceiptDayTo = txtFirstReceiptDayTo.Text;
		oParamlist.sLastReceiptDayFrom = txtLastReceiptDayFrom.Text;
		oParamlist.sLastReceiptDayTo = txtLastReceiptDayTo.Text;
		oParamlist.sTotalReceiptCountFrom = txtTotalReceiptCountFrom.Text;
		oParamlist.sTotalReceiptCountTo = txtTotalReceiptCountTo.Text;
		oParamlist.iNonExistMailAddrFlagMask = iNonExistMailAddrFlag;
		oParamlist.sFirstLoginTimeFrom = txtFirstLoginTimeFrom.Text;
		oParamlist.sFirstLoginTimeTo = txtFirstLoginTimeTo.Text;
		oParamlist.sLastLoginTimeFrom = txtLastLoginTimeFrom.Text;
		oParamlist.sLastLoginTimeTo = txtLastLoginTimeTo.Text;
		oParamlist.sLastPointUsedTimeFrom = txtLastPointUsedTimeFrom.Text;
		oParamlist.sLastPointUsedTimeTo = txtLastPointUsedTimeTo.Text;
		oParamlist.sRegistTimeFrom = txtRegistTimeFrom.Text;
		oParamlist.sRegistTimeTo = txtRegistTimeTo.Text;
		oParamlist.sLastBlackDayFrom = txtLastBlackDayFrom.Text;
		oParamlist.sLastBlackDayTo = txtLastBlackDayTo.Text;
		oParamlist.sBillAmtFrom = txtBillAmtFrom.Text;
		oParamlist.sBillAmtTo = txtBillAmtTo.Text;
		oParamlist.sMchaTotalReceiptAmtFrom = txtMchaTotalReceiptAmtFrom.Text;
		oParamlist.sMchaTotalReceiptAmtTo = txtMchaTotalReceiptAmtTo.Text;
		oParamlist.sMchaTotalReceiptCountFrom = txtMchaTotalReceiptCountFrom.Text;
		oParamlist.sMchaTotalReceiptCountTo = txtMchaTotalReceiptCountTo.Text;
		oParamlist.sUrgeLevel = lstUrgeLevel.SelectedValue;
		oParamlist.bCreditMeasured = chkManCreditMeasured.Checked;
		oParamlist.bMchaUsedFlag = chkMchaUsedFlag.Checked;
		oParamlist.bCreditUsedFlag = chkCreditUsedFlag.Checked;
		oParamlist.sManAttrValue1 = lstManAttrSeq0.SelectedValue;
		oParamlist.sManAttrValue2 = lstManAttrSeq1.SelectedValue;
		oParamlist.sManAttrValue3 = lstManAttrSeq2.SelectedValue;
		oParamlist.sManAttrValue4 = lstManAttrSeq3.SelectedValue;
		oParamlist.iWithUnRegistComplete = iWithUnRegistComplete;
		oParamlist.sGuid = TrimEnd(txtGuid.Text);
		oParamlist.bPointAffiFlag = this.chkPointAffiFlag.Checked;
		oParamlist.bIntroducerFriendAllFlag = this.chkIntroducerFriendAllFlag.Checked;
		oParamlist.sCastMailRxType = this.lstCastMailRxType.SelectedValue;
		oParamlist.sInfoMailRxType = this.lstInfoMailRxType.SelectedValue;
		oParamlist.sIntroducerFriendCd = string.Empty;
		oParamlist.sRemarks4 = this.txtRemarks4.Text.TrimEnd();
		oParamlist.sRegistCarrier = this.txtRegistCarrier.Text.TrimEnd();
		oParamlist.sMobileCarrier = this.txtMobileCarrier.Text.TrimEnd();
		oParamlist.bIsNullAdCd = this.chkAdCdIsNull.Checked;
		oParamlist.sReportAffiliateDateFrom = txtReportAffiliateDateFrom.Text;
		oParamlist.sReportAffiliateDateTo = txtReportAffiliateDateTo.Text;
		oParamlist.sChkExistAddrExeCountFrom = txtChkExistAddrExeCountFrom.Text;
		oParamlist.sChkExistAddrExeCountTo = txtChkExistAddrExeCountTo.Text;
		oParamlist.sServicePointGetCountFrom = txtServicePointGetCountFrom.Text;
		oParamlist.sServicePointGetCountTo = txtServicePointGetCountTo.Text;
		oParamlist.sSiteUseStatus = sSiteUseStatus;
		int iUserDefineMask = 0;
		foreach (ListItem oListItem in this.chkUserDefineMask.Items) {
			if (oListItem.Selected) {
				iUserDefineMask += int.Parse(oListItem.Value);
			}
		}
		oParamlist.sUserDefineMask = iUserDefineMask > 0 ? iUserDefineMask.ToString() : string.Empty;
		oParamlist.sExceptRefusedByCastUserSeq = this.TxMailCastUserSeq;
		oParamlist.sGameHandleNm = this.txtGameHandleNm.Text;
		List<string> oCharType = new List<string>();
		foreach (ListItem oListItem in this.chkGameCharacterType.Items) {
			if (oListItem.Selected) {
				oCharType.Add(string.Format("'{0}'",oListItem.Value));
			}
		}
		oParamlist.sGameCharacterType = string.Join(",",oCharType.ToArray());
		oParamlist.sGameRegistDateFrom = this.txtGameRegistDateFrom.Text;
		oParamlist.sGameRegistDateTo = this.txtGameRegistDateTo.Text;
		oParamlist.sGameCharacterLevelFrom = this.txtGameCharacterLevelFrom.Text;
		oParamlist.sGameCharacterLevelTo = this.txtGameCharacterLevelTo.Text;
		oParamlist.sGamePointFrom = this.txtGamePointFrom.Text;
		oParamlist.sGamePointTo = this.txtGamePointTo.Text;
		oParamlist.bHandleNmExactFlag = this.chkHandleNmExactFlag.Checked;
		oParamlist.bLoginIdNegativeFlag = this.chkLoginIdListVisible.Checked ? this.chkLoginIdListNegativeFlag.Checked : this.chkLoginIdNegativeFlag.Checked;
		oParamlist.bLastLoginDayNegativeFlag = this.chkLastLoginDayNegativeFlag.Checked;
		oParamlist.bLastPointUsedDayNegativeFlag = this.chkLastPointUsedDayNegativeFlag.Checked;
		oParamlist.bRegistDayNegativeFlag = this.chkRegistDayNegativeFlag.Checked;
		oParamlist.bUserDefineMaskNegatieFlag = this.chkUserDefineMaskNegatieFlag.Checked;
		oParamlist.sCrosmileLastUsedVersionFrom = this.txtCrosmileLastUsedVersionFrom.Text.Trim();
		oParamlist.sCrosmileLastUsedVersionTo = this.txtCrosmileLastUsedVersionTo.Text.Trim();
		oParamlist.sUseCrosmileFlag = this.rdoUseCrosmileFlag.SelectedValue;
		oParamlist.bTalkDirectSmartNotMonitorFlag = this.chkTalkSmartDirectNotMonitor.Checked;

		oParamlist.sSortExpression = this.SortExpression;
		oParamlist.sSortDirection = this.SortDirect;
		oParamlist.bIsNullTel = this.chkTelIsNull.Checked;
		oParamlist.sUserSeq = txtUserSeq.Text.Trim();
		oParamlist.sUseVoiceappFlag = this.rdoUseVoiceappFlag.SelectedValue;
		
		oParamlist.sPointUseFlag = this.rdoPointUseFlag.SelectedValue;
		oParamlist.sAuthFlag = this.rdoAuthFlag.SelectedValue;
		
		oParamlist.sGcappLastLoginVersionFrom = this.txtGcappLastLoginVersionFrom.Text.Trim();
		oParamlist.sGcappLastLoginVersionTo = this.txtGcappLastLoginVersionTo.Text.Trim();
		oParamlist.sUserTestType = this.rdoUserTestType.SelectedValue;
		oParamlist.iRegistCarrierCd = iRegistCarrierCd;
		oParamlist.bNotExistFirstPointUsedDate = this.chkNotExistFirstPointUseDate.Checked;
		oParamlist.bExcludeEmailAddr = this.chkExcludeEmailAddr.Checked;
		oParamlist.sTxMuller3NgMailAddrFlag = this.rdoTxMuller3NgMailAddrFlag.SelectedValue;
		oParamlist.sRichinoRankFrom = this.lstRichinoRankFrom.SelectedValue;
		oParamlist.sRichinoRankTo = this.lstRichinoRankTo.SelectedValue;
		oParamlist.bExcludeAdCd = this.chkExcludeAdCd.Checked;
		// ﾎﾟｲﾝﾄｱﾌﾘ決済日
		oParamlist.sMchaReceiptDateFrom = txtMchaReceiptDateFrom.Text;
		if (!string.IsNullOrEmpty(txtMchaReceiptDateTo.Text)) {
			DateTime dtTo = System.DateTime.ParseExact(txtMchaReceiptDateTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			oParamlist.sMchaReceiptDateTo = dtTo.AddDays(1).ToString("yyyy/MM/dd");
		}

		return oParamlist;
	}

	protected string TrimEnd(string pTargetStr) {
		char[] trims = { ' ' };
		string sRetStr = pTargetStr.TrimEnd(trims);

		return sRetStr;
	}

	protected void vdcStatus_ServerValidate(object source,ServerValidateEventArgs args) {
		if (!this.IsValid) {
			return;
		}
		if (!args.IsValid) {
			return;
		}

		if (this.chkNormal.Checked) {
			if (this.chkManTelNoAuth.Checked || this.chkManNew.Checked || this.chkManNoReceipt.Checked || this.chkManNonUsed.Checked || this.chkManNormal.Checked) {
				args.IsValid = false;
			}
		}
	}

	protected void lstMailRxType_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList != null) {
			oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));

			// お知らせメール送信履歴からの遷移
			// (予約送信条件の設定)
			this.GetReservationParamList();
			if (this.oReservationParamlist != null) {
				if (oDropDownList.ID.Equals("lstCastMailRxType")) {
					oDropDownList.SelectedValue = this.oReservationParamlist.sCastMailRxType;
				} else if (oDropDownList.ID.Equals("lstInfoMailRxType")) {
					oDropDownList.SelectedValue = this.oReservationParamlist.sInfoMailRxType;
				}
			}
		}
	}

	/// <summary>
	/// セレクトボックス（リッチーノランク）のデータバインド後の処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lstRichinoRank_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList != null) {
			// 一番上の選択肢に通常会員を追加
			oDropDownList.Items.Insert(0,new ListItem("通常会員",string.Empty));

			// 初期選択の値を設定
			if (oDropDownList.ID.EndsWith("To")) {
				oDropDownList.SelectedIndex = oDropDownList.Items.Count - 1;
			} else {
				oDropDownList.SelectedIndex = 0;
			}

			// お知らせメール送信履歴からの遷移
			// (予約送信条件の設定)
			this.GetReservationParamList();
			if (this.oReservationParamlist != null) {
				if (oDropDownList.ID.Equals("lstRichinoRankFrom")) {
					oDropDownList.SelectedValue = this.oReservationParamlist.sRichinoRankFrom;
				} else if (oDropDownList.ID.Equals("lstRichinoRankTo")) {
					oDropDownList.SelectedValue = this.oReservationParamlist.sRichinoRankTo;
				}
			}
		}
	}


	#region === 検索条件保存 ================================================================================

	protected void lstSeekCondition_SelectedIndexChanged(object sender,EventArgs e) {
		this.ReSearchBySeekCondition();
	}

	protected void lnkEditSeekCondition_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		this.InitSeekConditionPanel();

		using (SeekCondition oSeekCondition = new SeekCondition()) {
			using (DataSet oSeekConditionDataSet = oSeekCondition.GetOne(this.lstSeekCondition.SelectedValue)) {
				if (oSeekConditionDataSet.Tables[0].Rows.Count == 0) {
					return;
				}

				DataRow oSeekConditionDataRow = oSeekConditionDataSet.Tables[0].Rows[0];

				this.hdnSeekConditionSeq.Value = iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION_SEQ"]);
				this.hdnSeekConditionRevisionNo.Value = iBridUtil.GetStringValue(oSeekConditionDataRow["REVISION_NO"]);
				this.txtSeekConditionNm.Text = iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION_NM"]);
				this.chkSeekConditionPublish.Checked = iBridUtil.GetStringValue(oSeekConditionDataRow["PUBLISH_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
				this.btnDeleteSeekCondition.Visible = true;
			}
		}
	}
	protected void lnkAppendSeekCondition_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		this.InitSeekConditionPanel();
	}

	protected void btnSaveSeekCondition_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		UpdateSeekCondition(false);
	}
	protected void btnDeleteSeekCondition_Click(object sender,EventArgs e) {
		UpdateSeekCondition(true);
	}
	protected void btnCancelSeekCondition_Click(object sender,EventArgs e) {
		this.pnlKey.Enabled = true;
		this.pnlSeekConditionMainte.Visible = false;
	}

	private void InitSeekConditionPanel() {
		this.pnlKey.Enabled = false;
		this.pnlSeekConditionMainte.Visible = true;

		this.hdnSeekConditionSeq.Value = string.Empty;
		this.hdnSeekConditionRevisionNo.Value = string.Empty;
		this.txtSeekConditionNm.Text = string.Empty;
		this.chkSeekConditionPublish.Checked = false;
		this.btnDeleteSeekCondition.Visible = false;
	}

	private void InitSeekConditionList() {
		SeekConditionHelper.SetupSeekConditionDropDownList(SessionObjs.AdminId,ViCommConst.SeekConditionType.UserManCharacter,this.lstSeekCondition);
		this.lnkEditSeekCondition.Enabled = false;
	}

	private void UpdateSeekCondition(bool pIsDelete) {
		string sSeekConditionSeq = this.hdnSeekConditionSeq.Value;
		int iPublishFlag = this.chkSeekConditionPublish.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		string sRevisionNo = this.hdnSeekConditionRevisionNo.Value;
		string sSeekConditionNm = this.txtSeekConditionNm.Text.Trim();

		if (pIsDelete) {
			SeekConditionHelper.Delete(SessionObjs.AdminId,sSeekConditionSeq,sRevisionNo);
		} else {
			sSeekConditionSeq = SeekConditionHelper.Save(SessionObjs.AdminId,sSeekConditionSeq,ViCommConst.SeekConditionType.UserManCharacter,sSeekConditionNm,iPublishFlag,sRevisionNo,this.pnlSeekCondition);
		}

		this.InitSeekConditionList();
		if (!pIsDelete) {
			this.lstSeekCondition.SelectedValue = sSeekConditionSeq;
		}
		this.ReSearchBySeekCondition();

		if (pIsDelete) {
			this.SetupSelectColumns();
		}
	}

	private void ReSearchBySeekCondition() {
		this.pnlKey.Enabled = true;
		this.pnlSeekConditionMainte.Visible = false;
		this.lnkEditSeekCondition.Enabled = false;

		string sSeekConditionSeq = this.lstSeekCondition.SelectedValue;

		this.InitPage();
		this.lstSeekCondition.SelectedValue = sSeekConditionSeq;

		if (!string.IsNullOrEmpty(sSeekConditionSeq) && !sSeekConditionSeq.StartsWith("*")) {

			DataSet oSeekConditionDataSet = SeekConditionHelper.Load(sSeekConditionSeq,this.pnlSeekCondition);
			this.SetupConditionLoginIdList(this.chkLoginIdListVisible.Checked);
			this.SetupConditionAdCdList(this.chkAdCdListVisible.Checked);
			this.SetupSelectColumnsVisible(!this.chkSelectColumnsVisible.Checked);
			this.SetupSelectColumns();
			if (oSeekConditionDataSet.Tables[0].Rows.Count > 0) {
				DataRow oSeekConditionDataRow = oSeekConditionDataSet.Tables[0].Rows[0];
				this.lnkEditSeekCondition.Enabled = iBridUtil.GetStringValue(oSeekConditionDataRow["ADMIN_ID"]).Equals(SessionObjs.AdminId);
			}

			ViewState["PICKUP"] = "0";
			GetList();
		}
	}
	#endregion ========================================================================================================

	protected string ReplaceForm(object pHtmlDoc) {
		string sUrl = "";
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lstSiteCd.SelectedValue)) {
				sUrl = oSite.url;
			}
		}

		string sDoc = Regex.Replace(pHtmlDoc.ToString(),"%%12%%",sUrl);
		sDoc = Regex.Replace(sDoc,"\r\n","<br />");
		return Regex.Replace(sDoc,@"<\/?[fF][oO][rR][mM].*?>",string.Empty,RegexOptions.Compiled);
	}

	protected void grdUserManCharacter_DataBound(object sender,EventArgs e) {
		for (int i = 0;i < this.chkSelectColumns.Items.Count;i++) {
			this.grdUserManCharacter.Columns[i].Visible = this.chkSelectColumns.Items[i].Selected;
		}
		//GridViewHelper.RegiserCreateGridHeader(1, this.grdUserManCharacter, this.pnlGrid);
	}

	protected void grdUserManCharacter_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected void lnkDisplayLoginIdList_Click(object sender,EventArgs e) {
		this.SetupConditionLoginIdList(true);
	}

	protected void lnkHideLoginIdList_Click(object sender,EventArgs e) {
		this.SetupConditionLoginIdList(false);
	}

	private void SetupConditionLoginIdList(bool pIsVisible) {
		this.lnkDisplayLoginIdList.Visible = !pIsVisible;
		this.chkLoginIdNegativeFlag.Enabled = !pIsVisible;
		this.txtLoginId.Enabled = !pIsVisible;
		this.txtLoginId.BackColor = pIsVisible ? System.Drawing.SystemColors.Control : System.Drawing.Color.White;
		this.pnlLoginIdList.Visible = pIsVisible;
		this.chkLoginIdListVisible.Checked = pIsVisible;
		this.tdHeaderInfoMailRxType.Attributes["class"] = pIsVisible ? "tdHeaderStyle2" : "tdHeaderStyle";
	}

	protected void lnkDisplayAdCdList_Click(object sender, EventArgs e) {
		this.SetupConditionAdCdList(true);
	}

	protected void lnkHideAdCdList_Click(object sender, EventArgs e) {
		this.SetupConditionAdCdList(false);
	}

	private void SetupConditionAdCdList(bool pIsVisible) {
		using (ManageCompany oCompany = new ManageCompany()) {
			bool bIsCompanyVisible = oCompany.IsAvailableService(ViCommConst.RELEASE_INQUIRY_MULTI_AD_CD, 2);
			if (bIsCompanyVisible) {
				this.lnkDisplayAdCdList.Visible = !pIsVisible;
				this.txtAdCd.Enabled = !pIsVisible;
				this.txtAdCd.BackColor = pIsVisible ? System.Drawing.SystemColors.Control : System.Drawing.Color.White;
				this.lblAdNmNotVisibleStartFont.Text = pIsVisible ? "<font color=\"#999999\">" : string.Empty;
				this.lblAdNmNotVisibleEndFont.Text = pIsVisible ? "</font>" : string.Empty;
				this.pnlAdCdList.Visible = pIsVisible;
				this.chkAdCdListVisible.Checked = pIsVisible;
				this.lblAdNmList.Text = string.Empty;
				this.tdOnlineState.Attributes["class"] = pIsVisible ? "tdHeaderStyle2" : "tdHeaderStyle";
				this.tdAdCd.Attributes["class"] = pIsVisible ? "tdHeaderStyle2" : "tdHeaderStyle";
			} else { 
				this.lnkDisplayAdCdList.Visible = false;
				this.txtAdCd.Enabled = true;
				this.txtAdCd.BackColor = System.Drawing.Color.White;
				this.lblAdNmNotVisibleStartFont.Text = string.Empty;
				this.lblAdNmNotVisibleEndFont.Text = string.Empty;
				this.pnlAdCdList.Visible = false;
				this.chkAdCdListVisible.Checked = false;
				this.lblAdNmList.Text = string.Empty;
				this.tdOnlineState.Attributes["class"] = "tdHeaderStyle";
				this.tdAdCd.Attributes["class"] = "tdHeaderStyle";
			}
		}

	}
	protected void lnkSelectColumns_Click(object sender,EventArgs e) {
		this.SetupSelectColumnsVisible(this.plcSelectColumns.Visible);
	}

	private void SetupSelectColumnsVisible(bool pIsVisible) {
		if (pIsVisible) {
			this.plcSelectColumns.Visible = false;
			this.lnkSelectColumns.Text = "[一覧項目選択表示]";
		} else {
			this.plcSelectColumns.Visible = true;
			this.lnkSelectColumns.Text = "[一覧項目選択非表示]";
		}
		this.chkSelectColumnsVisible.Checked = this.plcSelectColumns.Visible;
	}

	private void SetupSelectColumns() {
		foreach (string sColumnName in ColumnsSelectedArray) {
			ListItem oListItem = this.chkSelectColumns.Items.FindByText(sColumnName);
			if (oListItem != null) {
				oListItem.Selected = true;
			}
		}
	}

	private void  SetupUrgeSearch() {
		bool bExist = false;
		using (Site oSite = new Site()) {
			bExist = oSite.IsAvailablePayAfterFlag(lstSiteCd.SelectedValue);
			if (bExist == false) {
				bExist = oSite.IsUsedChargeSettleFlag(lstSiteCd.SelectedValue);
			}
		}
		this.plcBlackHeaders.Visible = bExist;
		this.plcBlackParams.Visible = bExist;
		this.txtLastBlackDayFrom.Text = string.Empty;
		this.txtLastBlackDayTo.Text = string.Empty;
		this.lstUrgeLevel.SelectedIndex = 0;

	}

	// 男性会員認証済み確認
	public string GetServicePtFlag(object ptFlg)
	{
		string sText = string.Empty;

		if (ptFlg.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			sText = "済";
		} else {
			sText = "";
		}

		return sText;
	}

	/// <summary>
	/// 予約送信ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnTxReservationMail_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		// メール送信情報を保存
		TxInfoMailHistory.MainteData oMailHisData = new TxInfoMailHistory.MainteData();
		SetTxInfoMailHistory(ref oMailHisData,ViCommConst.FLAG_ON_STR);

		// メール送信履歴に移動して登録・更新したデータを表示
		Response.Redirect("/ADMIN/Mail/TxInfoMailHistory.aspx?sdate=" + oMailHisData.ReservationSendDate + "&sexcd=" + ViCommConst.MAN);
	}

	/// <summary>
	/// メール送信情報を保存
	/// </summary>
	/// <param name="oMailHisData"></param>
	/// <param name="sReservationFlag"></param>
	private void SetTxInfoMailHistory(ref TxInfoMailHistory.MainteData oMailHisData,string sReservationFlag) {
		// 検索条件の取得
		PageCollectionParameters oParamlist = GetSearchParamList();
		int.TryParse(txtServicePoint.Text,out oParamlist.iServicePoint);
		int.TryParse(txtMailAvaHour.Text,out oParamlist.iMailAvaHour);
		int.TryParse(txtPointTransferAvaHour.Text,out oParamlist.iPointTransferAvaHour);

		// 送信予定(検索結果)件数の取得
		int iRecCount = 0;
		int.TryParse(lblRecCount.Text,out iRecCount);

		// オリジナル本文
		string[] sDoc;
		int iDocCount;
		string sMailSendType = this.rdoMailSendType.SelectedValue;
		if (plcMailSendType.Visible == false) {
			sMailSendType = string.Empty;
		}
		SysPrograms.SeparateHtml("",ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		// Json文字列へ変換
		//oParamStr.Remove(0,oParamStr.Length);
		//new JavaScriptSerializer().Serialize(sDoc,oParamStr);
		//string sDocJson = oParamStr.ToString();

		// 検索条件を配列に格納する
		PropertyInfo[] infoArray = oParamlist.GetType().GetProperties();
		FieldInfo[] fields = oParamlist.GetType().GetFields();
		int num = fields.Length;
		List<string> keys = new List<string>();
		List<string> vals = new List<string>();
		foreach (FieldInfo info in fields) {
			keys.Add(info.Name);
			if (info.GetValue(oParamlist) == null) {
				vals.Add(string.Empty);
			} else {
				vals.Add(info.GetValue(oParamlist).ToString());
			}
		}

		// 送信予定日時
		DateTime oSendDate;
		if (sReservationFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
			// 手動送信の場合、現在日時を設定する
			oSendDate = DateTime.Now;
		} else if (string.IsNullOrEmpty(txtReservationSendDay.Text)) {
			// 送信予定日が未入力の場合、現在日付を設定する
			oSendDate = DateTime.Today;
		} else if (!string.IsNullOrEmpty(txtReservationSendHour.Text)
			&& !string.IsNullOrEmpty(txtReservationSendMinute.Text)
		) {
			// 送信予定日時分が全て入力されてる場合、日時分が正しいかチェックして設定する
			string sDT = string.Format("{0} {1}:{2}:00",
				txtReservationSendDay.Text,
				txtReservationSendHour.Text,
				txtReservationSendMinute.Text
			);
			if (!DateTime.TryParse(sDT,out oSendDate)) {
				oSendDate = DateTime.Today;
			}
		} else if (!string.IsNullOrEmpty(txtReservationSendHour.Text)) {
			// 送信予定日時が入力されてる場合、日時が正しいかチェックして設定する
			string sDT = string.Format("{0} {1}:00:00",txtReservationSendDay.Text,txtReservationSendHour.Text);
			if (!DateTime.TryParse(sDT,out oSendDate)) {
				oSendDate = DateTime.Today;
			}
		} else {
			// 送信予定日のみ入力されてる場合、日付が正しいかチェックして設定する
			string sDT = string.Format("{0} 00:00:00",txtReservationSendDay.Text);
			if (!DateTime.TryParse(sDT,out oSendDate)) {
				oSendDate = DateTime.Today;
			}
		}

		oMailHisData.SiteCd = lstSiteCd.SelectedValue;
		oMailHisData.MailTemplateNo = lstMailTemplateNo.SelectedValue;
		oMailHisData.MailServer = rdoMailServer.SelectedValue;
		oMailHisData.MailSendType = rdoMailSendType.SelectedValue;
		oMailHisData.MailType = this.MailTemplateType;
		oMailHisData.RxSexCd = ViCommConst.MAN;
		//oMailHisData.Doc = sDocJson;
		oMailHisData.DocCount = sDoc.Length;
		oMailHisData.aConditionNames = keys.ToArray();
		oMailHisData.aConditionVals = vals.ToArray();
		oMailHisData.ConditionCount = oMailHisData.aConditionNames.Length;
		oMailHisData.ReservationSendDate = oSendDate.ToString("yyyy/MM/dd HH:mm:ss");
		oMailHisData.ReservationSendNum = iRecCount;
		oMailHisData.ReservationFlag = sReservationFlag;
		oMailHisData.AdminId = Session["AdminId"].ToString();

		string sSeq = null;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["rmhseq"]))) {
			sSeq = iBridUtil.GetStringValue(Request.QueryString["rmhseq"]);
		}
		string sRNo = null;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["revno"]))) {
			sRNo = iBridUtil.GetStringValue(Request.QueryString["revno"]);
		}

		// 登録
		TxInfoMailHistory oTxInfoMailHistory = new TxInfoMailHistory();
		oTxInfoMailHistory.Mainte(oMailHisData,sRNo,sSeq,ViCommConst.FLAG_OFF_STR);
	}

	/// <summary>
	/// 予約送信設定された条件を取得
	/// </summary>
	/// <returns></returns>
	private void GetReservationParamList() {
		// post通信時
		if (IsPostBack) {
			this.oReservationParamlist = null;
			this.oReservationMailParamlist = null;
			return;
		}

		// 取得済みの場合
		if (this.oReservationParamlist != null) {
			return;
		}

		// お知らせメール送信履歴からの遷移
		string sReservationSeq = iBridUtil.GetStringValue(Request.QueryString["rmhseq"]);
		if (string.IsNullOrEmpty(sReservationSeq)) {
			return;
		}

		this.oReservationMailParamlist = new TxInfoMailHistory.MainteData();
		this.oReservationParamlist = new PageCollectionParameters();

		// 予約送信条件の取得
		TxInfoMailHistory oTxInfoMailHistory = new TxInfoMailHistory();
		DataSet oDS = oTxInfoMailHistory.GetOne(sReservationSeq);
		if (oDS.Tables[0].Rows.Count > 0) {
			DataRow oDR = oDS.Tables[0].Rows[0];

			// メール送信設定欄の内容取得
			this.oReservationMailParamlist.MailTemplateNo = oDR["MAIL_TEMPLATE_NO"].ToString();
			this.oReservationMailParamlist.MailServer = oDR["MAIL_SERVER"].ToString();
			this.oReservationMailParamlist.MailSendType = oDR["MAIL_SEND_TYPE"].ToString();
			this.oReservationMailParamlist.MailType = oDR["MAIL_TYPE"].ToString();
			if (oDR["RESERVATION_SEND_DATE"] != DBNull.Value) {
				DateTime oDT = Convert.ToDateTime(oDR["RESERVATION_SEND_DATE"]);
				this.oReservationMailParamlist.ReservationSendDate = oDT.ToString("yyyy/MM/dd HH:mm:00");
			}
		}

		// 検索条件欄の内容取得
		DataSet oDSDtl = oTxInfoMailHistory.GetDetail(sReservationSeq);
		if (oDSDtl.Tables[0].Rows.Count > 0) {
			foreach (DataRow oDRDtl in oDSDtl.Tables[0].Rows) {
				FieldInfo oFInfo = this.oReservationParamlist.GetType().GetField(oDRDtl["NAME"].ToString());
				// それぞれプロパティの型にあわせて変換した後に設定する
				if (oDRDtl["VALUE"] == DBNull.Value) {
					// Nullの場合は何もしない
				} else if (oFInfo.FieldType == typeof(int) || oFInfo.FieldType == typeof(Int32)) {
					oFInfo.SetValue(this.oReservationParamlist,int.Parse(oDRDtl["VALUE"].ToString()));
				} else if (oFInfo.FieldType == typeof(bool)) {
					oFInfo.SetValue(this.oReservationParamlist,bool.Parse(oDRDtl["VALUE"].ToString()));
				} else {
					oFInfo.SetValue(this.oReservationParamlist,oDRDtl["VALUE"]);
				}
			}
		}
	}

	/// <summary>
	/// RadioButtonListのSelectedIndexを値から検索して設定する
	/// (SelectedIndexとSelectedValueにより同時に設定するとエラーが発生するため)
	/// </summary>
	/// <param name="oList"></param>
	/// <param name="sVal"></param>
	private void RadioButtonList_SetSelectedIndexByValue(RadioButtonList oList,string sVal) {
		oList.SelectedIndex = oList.Items.IndexOf(oList.Items.FindByValue(sVal));
	}

	/// <summary>
	/// DropDownListのSelectedIndexを値から検索して設定する
	/// (SelectedIndexとSelectedValueにより同時に設定するとエラーが発生するため)
	/// </summary>
	/// <param name="oList"></param>
	/// <param name="sVal"></param>
	private void DropDownList_SetSelectedIndexByValue(DropDownList oList,string sVal) {
		oList.SelectedIndex = oList.Items.IndexOf(oList.Items.FindByValue(sVal));
	}
}
