﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: Android端末UUID重複一覧
--	Progaram ID		: AndroidUuidOverlapList
--  Creation Date	: 2015.03.17
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using ViComm;
using iBridCommLib;

public partial class Man_AndroidUuidOverlapList:System.Web.UI.Page {
	private int recCount = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			grdData.DataSourceID = "dsData";
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		DataSet ds;

		using (AndroidUuidOverlap oAndroidUuidOverlap = new AndroidUuidOverlap()) {
			AndroidUuidOverlap.SearchCondition oSearchCondition = new AndroidUuidOverlap.SearchCondition();
			oSearchCondition.SiteCd = lstSiteCd.SelectedValue;

			ds = oAndroidUuidOverlap.GetPageCollection(
				oSearchCondition,
				0,
				Int32.MaxValue
			);
		}

		Response.ContentType = "application/download";
		Response.AddHeader("Content-Disposition","attachment;filename=androidUuidOverlapList.csv");
		Response.ContentEncoding = Encoding.GetEncoding("Shift_JIS");

		Response.Write("\"重複者ID\",\"被重複者ID\",\"端末UUID\",\"重複日\"\r\n");

		foreach (DataRow dr in ds.Tables[0].Rows) {
			string sData = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\"\r\n",
				iBridUtil.GetStringValue(dr["LOGIN_ID"]),
				iBridUtil.GetStringValue(dr["OVERLAP_LOGIN_ID"]),
				iBridUtil.GetStringValue(dr["DEVICE_UUID"]),
				iBridUtil.GetStringValue(dr["OVERLAP_DATE"])
			);

			Response.Write(sData);
		}

		Response.End();
	}

	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdData.Rows[iIndex];
		HiddenField oSiteCd = oRow.FindControl("hdnSiteCd") as HiddenField;
		HiddenField oUserSeq = oRow.FindControl("hdnUserSeq") as HiddenField;

		if (!string.IsNullOrEmpty(oSiteCd.Value) && !string.IsNullOrEmpty(oUserSeq.Value)) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("DELETE_ANDROID_UUID_OVERLAP");
				db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,oSiteCd.Value);
				db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,oUserSeq.Value);
				db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				db.cmd.BindByName = true;
				db.ExecuteProcedure();
			}
		}

		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected int GetRecCount() {
		return this.recCount;
	}

	protected void dsData_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		AndroidUuidOverlap.SearchCondition oSearchCondition = new AndroidUuidOverlap.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsData_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null && !(e.ReturnValue is DataSet)) {
			recCount = int.Parse(iBridUtil.GetStringValue(e.ReturnValue));
		}
	}

	protected string GetLoginIdUrl(object pSiteCd,object pLoginId) {
		string sUrl = string.Empty;

		if (!string.IsNullOrEmpty(pSiteCd.ToString()) && !string.IsNullOrEmpty(pLoginId.ToString())) {
			sUrl = string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoginId.ToString());
		}

		return sUrl;
	}
}
