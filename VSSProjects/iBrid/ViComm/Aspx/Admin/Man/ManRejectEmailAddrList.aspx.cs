﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員登録拒否ﾒｰﾙｱﾄﾞﾚｽ一覧--	Progaram ID		: ManRejectEmailAddrList
--  Creation Date	: 2015.05.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using iBridCommLib;
using ViComm;

public partial class Man_ManRejectEmailAddrList:Page {
	private int recCount = 0;

	protected void Page_Load(object sender,EventArgs e) {
		this.txtRejectEmailAddr.Style.Add("ime-mode","disabled");
		this.txtLastRejectDateFrom.Style.Add("ime-mode","disabled");
		this.txtLastRejectDateTo.Style.Add("ime-mode","disabled");

		if (!this.IsPostBack) {
			grdData.DataSourceID = "dsData";
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		DataSet ds;
		ManRejectEmailAddr.SearchCondition oSearchCondition = new ManRejectEmailAddr.SearchCondition();
		oSearchCondition.LastRejectDateFrom = this.txtLastRejectDateFrom.Text.Trim();
		oSearchCondition.LastRejectDateTo = this.txtLastRejectDateTo.Text.Trim();
		oSearchCondition.RejectEmailAddr = this.txtRejectEmailAddr.Text.Trim();

		using (ManRejectEmailAddr oManRejectEmailAddr = new ManRejectEmailAddr()) {
			ds = oManRejectEmailAddr.GetPageCollection(oSearchCondition,0,Int32.MaxValue);
		}

		StringBuilder oCsv = new StringBuilder();
		oCsv.AppendLine("最終拒否日時,拒否ﾒｰﾙｱﾄﾞﾚｽ,拒否回数,登録状態,登録完了日時,登録ﾒｰﾙｱﾄﾞﾚｽ,ﾛｸﾞｲﾝID");

		foreach (DataRow dr in ds.Tables[0].Rows) {
			oCsv.AppendFormat("{0},",iBridUtil.GetStringValue(dr["LAST_REJECT_DATE"]));
			oCsv.AppendFormat("{0},",iBridUtil.GetStringValue(dr["REJECT_EMAIL_ADDR"]));
			oCsv.AppendFormat("{0},",iBridUtil.GetStringValue(dr["REJECT_COUNT"]));
			oCsv.AppendFormat("{0},",this.GetRegistStatusText(dr["REGIST_STATUS"]));
			oCsv.AppendFormat("{0},",iBridUtil.GetStringValue(dr["REGIST_COMPLITE_DATE"]));
			oCsv.AppendFormat("{0},",iBridUtil.GetStringValue(dr["REGIST_EMAIL_ADDR"]));
			oCsv.AppendFormat("{0}",iBridUtil.GetStringValue(dr["LOGIN_ID"]));
			oCsv.AppendLine();
		}

		Response.AddHeader("Content-Disposition","attachment;filename=ManRejectEmailAddr.csv");
		Response.ContentType = "application/octet-stream-dummy";
		Response.BinaryWrite(Encoding.GetEncoding("Shift-JIS").GetBytes(oCsv.ToString()));
		Response.End();
	}

	protected int GetRecCount() {
		return this.recCount;
	}

	protected void dsData_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ManRejectEmailAddr.SearchCondition oSearchCondition = new ManRejectEmailAddr.SearchCondition();
		oSearchCondition.LastRejectDateFrom = this.txtLastRejectDateFrom.Text.Trim();
		oSearchCondition.LastRejectDateTo = this.txtLastRejectDateTo.Text.Trim();
		oSearchCondition.RejectEmailAddr = this.txtRejectEmailAddr.Text.Trim();
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsData_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null && !(e.ReturnValue is DataSet)) {
			recCount = int.Parse(iBridUtil.GetStringValue(e.ReturnValue));
		}
	}

	protected string GetRegistStatusText(object pRegistStatus) {
		string sText = string.Empty;

		switch (pRegistStatus.ToString()) {
			case "0":
				sText = "ﾒｰﾙ待ち状態";
				break;
			case "1":
				sText = "仮登録ﾒｰﾙ受信";
				break;
			case "2":
				sText = "登録完了";
				break;
			case "3":
				sText = "登録完了(成果未報告)";
				break;
		}

		return sText;
	}
}
