﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 公開ストック動画一覧
--	Progaram ID		: StockMovieOpenList
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_StockMovieOpenList : System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			if (iBridUtil.GetStringValue(this.SiteCd).Equals(string.Empty) == false) {
				lstSiteCd.SelectedValue = this.SiteCd;
			} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			GetList();
		}
	}

	protected void dsUserManMovie_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdMovie.PageSize = 100;
		grdMovie.DataSourceID = string.Empty;
		DataBind();
		if (Session["SiteCd"].ToString().Equals(string.Empty)) {
			lstSiteCd.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
		lstSiteCd.DataSourceID = string.Empty;
	}

	private void InitPage() {
		grdMovie.DataSourceID = string.Empty;
		DataBind();
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;
		GetList();
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void lnkQuickTime_Command(object sender, CommandEventArgs e) {
		string[] sKey = iBridUtil.GetStringValue(e.CommandArgument).Split(':');
		string sRoot = ConfigurationManager.AppSettings["Root"];
		string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + sKey[0] + string.Format("/man/{0}{1}",iBridUtil.addZero(sKey[1],ViCommConst.OBJECT_NM_LENGTH),ViCommConst.MOVIE_FOODER);
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	private void GetList() {
		grdMovie.DataSourceID = "dsUserManMovie";
		grdMovie.PageIndex = 0;
		grdMovie.DataBind();
		pnlCount.DataBind();
	}

	protected void dsUserManMovie_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "0";
		e.InputParameters[2] = "0";
		e.InputParameters[3] = ViCommConst.ATTACHED_MAIL.ToString();
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[5] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text;
		e.InputParameters[6] = this.txtLoginId.Text.TrimEnd();
	}
}
