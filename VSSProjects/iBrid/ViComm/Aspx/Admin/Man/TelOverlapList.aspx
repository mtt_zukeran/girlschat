﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TelOverlapList.aspx.cs" Inherits="Man_TelOverlapList" Title="電話番号重複一覧" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="電話番号重複一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[電話番号重複一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server">
				<asp:GridView ID="grdTelOverlap" runat="server" DataSourceID="dsTelOverlap" SkinID="GridViewColor" AutoGenerateColumns="False">
					<Columns>
						<asp:TemplateField HeaderText="ログインID">
							<ItemTemplate>
								<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("ManView.aspx?site=A001&manloginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'>
								</asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="TEL" HeaderText="電話番号">
						</asp:BoundField>
						<asp:BoundField DataField="REGIST_DATE" HeaderText="重複日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="">
							<ItemTemplate>
								<asp:LinkButton ID="lnkDelete" runat="server" Text="対応完了" OnClientClick="return confirm('対応完了にしてよろしいですか？');" OnCommand="lnkDelete_Command" CommandArgument='<%# Eval("USER_SEQ") %>'>
								</asp:LinkButton>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" />
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsTelOverlap" runat="server" TypeName="TelOverlap" SelectMethod="GetList">
	</asp:ObjectDataSource>
</asp:Content>

