﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StockMovieOpenList.aspx.cs" Inherits="Man_StockMovieOpenList"
	Title="公開中ストック動画" ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="公開中ストック動画"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							アップロード日付
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ControlToValidate="txtUploadDayFrom" ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							～&nbsp;
							<asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayTo" runat="server" ControlToValidate="txtUploadDayTo" ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ログインＩＤ
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[公開中ストック動画一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
				<asp:GridView ID="grdMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserManMovie" AllowSorting="True" SkinID="GridViewColor">
					<Columns>
						<asp:BoundField DataField="LOGIN_ID" HeaderText="ﾛｸﾞｲﾝID">
							<HeaderStyle Wrap="false" />
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="ハンドル名" SortExpression="HANDLE_NM">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?manloginid={0}&site={1}&return={2}",Eval("LOGIN_ID"),Eval("SITE_CD"),"StockMovieOpenList.aspx") %>'
									Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="動画確認">
							<HeaderStyle Wrap="false" />
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkQuickTime" runat="server" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("MOVIE_SEQ")) %>' OnCommand="lnkQuickTime_Command" Text="確認"></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:HyperLinkField HeaderText="動画設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ,LOGIN_ID" DataNavigateUrlFormatString="../Man/StockMovieMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&movieseq={3}&loginid={4}&return=StockMovieOpenList">
							<HeaderStyle Wrap="false" />
							<ItemStyle HorizontalAlign="Center" />
						</asp:HyperLinkField>
						<asp:BoundField DataField="MOVIE_TITLE" HeaderText="タイトル">
							<ItemStyle Width="150px" HorizontalAlign="Left" />
						</asp:BoundField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
						</asp:BoundField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdMovie.PageIndex + 1%>
					of
					<%=grdMovie.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUserManMovie" runat="server" SelectMethod="GetApproveManagePageCollection" TypeName="UserManMovie" SelectCountMethod="GetApproveManagePageCount"
		EnablePaging="True" OnSelected="dsUserManMovie_Selected" OnSelecting="dsUserManMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pNotApproveFlag" Type="String" />
			<asp:Parameter Name="pNotPublishFlag" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
			<asp:Parameter Name="pUploadDateFrom" Type="String" />
			<asp:Parameter Name="pUploadDateTo" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" TargetControlID="txtUploadDayFrom" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayFrom" runat="Server" TargetControlID="vdeUploadDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" TargetControlID="txtUploadDayTo" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayTo" runat="Server" TargetControlID="vdeUploadDayTo" HighlightCssClass="validatorCallout" />
</asp:Content>
