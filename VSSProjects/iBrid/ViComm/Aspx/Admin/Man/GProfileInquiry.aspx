<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GProfileInquiry.aspx.cs" Inherits="Man_GProfileInquiry" Title="逆プロフィール一覧"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="逆プロフィール一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[設定]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 300px" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle2" style="width: 100px">
							サイト
						</td>
						<td class="tdDataStyle" style="width: 200px">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[逆プロフィール一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdRecording.PageIndex + 1%>
						of
						<%=grdRecording.PageCount%>
					</a>
					<br />
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
					<asp:GridView ID="grdRecording" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsRecording" SkinID="GridViewColor" AllowSorting="True"
						Font-Size="Small" Style="width: 800px">
						<Columns>
							<asp:TemplateField HeaderText="ID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="70px" />
							</asp:TemplateField>
							<asp:BoundField DataField="BAL_POINT" HeaderText="残ﾎﾟｲﾝﾄ" SortExpression="BAL_POINT">
								<ItemStyle HorizontalAlign="Right" Width="60px" />
							</asp:BoundField>
							<asp:BoundField DataField="FOWARD_TEL_NO" HeaderText="転送先" SortExpression="FOWARD_TEL_NO">
								<ItemStyle HorizontalAlign="Center" Width="100px" />
							</asp:BoundField>
							<asp:BoundField DataField="RECORDING_DATE" HeaderText="転送開始時間" DataFormatString="{0:yy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="RECORDING_DATE">
								<ItemStyle HorizontalAlign="Center" Width="130px" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="相手ID-No." SortExpression="PARTNER_LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("PARTNER_LOGIN_ID"),"GProfileInquiry.aspx") %>'
										Text='<%# string.Format("{0}-{1}",Eval("PARTNER_LOGIN_ID"),Eval("PARTNER_USER_CHAR_NO")) %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="90px" />
							</asp:TemplateField>
							<asp:BoundField DataField="PLAY_COUNT" HeaderText="再生" SortExpression="PLAY_COUNT">
								<ItemStyle HorizontalAlign="Right" Width="60px" />
							</asp:BoundField>
							<asp:BoundField DataField="CALL_COUNT" HeaderText="指名" SortExpression="CALL_COUNT">
								<ItemStyle HorizontalAlign="Right" Width="60px" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsRecording" runat="server" SelectMethod="GetPageCollection" TypeName="Recording" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsRecording_Selected" OnSelecting="dsRecording_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pRecType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
