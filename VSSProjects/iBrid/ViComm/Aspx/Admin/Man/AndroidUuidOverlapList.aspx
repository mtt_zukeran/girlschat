﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AndroidUuidOverlapList.aspx.cs" Inherits="Man_AndroidUuidOverlapList" Title="Android端末UUID重複一覧" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="Android端末UUID重複一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset class="fieldset">
				<legend>[Android端末UUID重複者一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%=grdData.PageIndex + 1%> of <%=grdData.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
					<asp:GridView ID="grdData" runat="server" DataSourceID="" AllowSorting="false" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridView" PageSize="100">
					    <Columns>
							<asp:TemplateField HeaderText="重複者ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# GetLoginIdUrl(Eval("SITE_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="被重複者ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkOverlapLoginId" runat="server" NavigateUrl='<%# GetLoginIdUrl(Eval("SITE_CD"),Eval("OVERLAP_LOGIN_ID")) %>' Text='<%# Eval("OVERLAP_LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="DEVICE_UUID" HeaderText="端末UUID" HtmlEncode="False">
							</asp:BoundField>
							<asp:BoundField DataField="OVERLAP_DATE" HeaderText="重複日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							</asp:BoundField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
									<asp:LinkButton ID="lnkDelete" runat="server" Text="削除" OnCommand="lnkDelete_Command" CommandArgument='<%# Container.DataItemIndex %>' ></asp:LinkButton>
									<ajaxToolkit:ConfirmButtonExtender ID="cfmLnkDelete" runat="server" TargetControlID="lnkDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
									<asp:HiddenField ID="hdnSiteCd" runat="server" Value='<%# Eval("SITE_CD") %>' />
									<asp:HiddenField ID="hdnUserSeq" runat="server" Value='<%# Eval("USER_SEQ") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
					    </Columns>
					    <PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsData" runat="server" TypeName="AndroidUuidOverlap" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsData_Selecting" OnSelected="dsData_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

