<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManHistoryView.aspx.cs" Inherits="Man_ManHistoryView" Title="j«ļõĻXšõ"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="j«ļõĻXšõ"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[Żč]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 900px" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle2" style="width: 100px">
							TCg
						</td>
						<td class="tdDataStyle" style="width: 400px">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderSmallStyle2" style="width: 100px; height: 26px;">
							ŪøŽ²Żhc
						</td>
						<td class="tdDataStyle" style="width: 400px; height: 26px;">
							<asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="64px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrLoginId" runat="server" ErrorMessage="OChcšüĶµÄŗ³¢B" ControlToValidate="txtLoginId" ValidationGroup="Key" Display="Dynamic">*</asp:RequiredFieldValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="źõ" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="NA" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnManView" Text="ļõŚ×" CssClass="seekbutton" OnClick="btnManView_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[ļõź]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdUserHistory.PageIndex + 1%>
						of
						<%=grdUserHistory.PageCount%>
					</a>
				</asp:Panel>
				<br />
				<asp:GridView ID="grdUserHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserHistory" SkinID="GridViewColor" AllowSorting="True"
					Font-Size="Small">
					<Columns>
						<asp:TemplateField HeaderText="ĻXąe">
							<ItemTemplate>
								<asp:LinkButton ID="lnkReasonView" runat="server" Text='<%# string.Format("{0}{1}",Eval("MODIFY_REASON_NM"),Eval("MODIFY_BEFORE_AFTER_NM")) %>' CommandArgument='<%# Eval("USER_HIS_SEQ") %>'
									OnCommand="lnkReasonView_Command" CausesValidation="False">
								</asp:LinkButton>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Left" />
						</asp:TemplateField>
						<asp:BoundField DataField="CREATE_DATE" HeaderText="ĻXś" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="CREATE_DATE">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="LOGIN_PASSWORD" HeaderText="Źß½Ü°ÄŽ" SortExpression="LOGIN_PASSWORD">
							<ItemStyle HorizontalAlign="Left" />
						</asp:BoundField>
						<asp:BoundField DataField="TOTAL_RECEIPT_COUNT" HeaderText="üąń" SortExpression="TOTAL_RECEIPT_COUNT">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:BoundField DataField="BAL_POINT" HeaderText="cĪß²ŻÄ" SortExpression="BAL_POINT">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:BoundField DataField="LIMIT_POINT" HeaderText="§ĄĪß²ŻÄ" SortExpression="LIMIT_POINT">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:BoundField DataField="USER_STATUS_NM" HeaderText="ļõóŌ" SortExpression="USER_STATUS_NM">
							<ItemStyle HorizontalAlign="Left" />
						</asp:BoundField>
						<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="ÅIŪøŽ²Żś" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="LAST_LOGIN_DATE">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="MODIFY_USER_NM" HeaderText="ĻXŅ" SortExpression="MODIFY_USER_NM">
							<ItemStyle HorizontalAlign="Left" />
						</asp:BoundField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</fieldset>
		</asp:Panel>
		<table>
			<tr style="vertical-align: top;">
				<td>
					<asp:Panel runat="server" ID="pnlUserMan">
						<fieldset class="fieldset-inner">
							<legend>[ļõīń]</legend>
							<asp:DetailsView ID="DetailsView1" runat="server" DataSourceID="dsUserHistoryByGetOne" AutoGenerateRows="False" SkinID="DetailsView" Width="420px">
								<Fields>
									<asp:BoundField DataField="LOGIN_PASSWORD" HeaderText="Źß½Ü°ÄŽ">
										<ItemStyle HorizontalAlign="Left" />
									</asp:BoundField>
									<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="ÅIŪøŽ²Żś" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="LAST_LOGIN_DATE">
										<ItemStyle HorizontalAlign="Left" />
									</asp:BoundField>
									<asp:BoundField DataField="CREATE_DATE" HeaderText="ĻXś" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="CREATE_DATE">
										<ItemStyle HorizontalAlign="Left" />
									</asp:BoundField>
									<asp:TemplateField HeaderText="õl">
										<ItemTemplate>
											<asp:Label ID="lblRemarks1" runat="server" Text='<%# Eval("REMARKS1") %>' Width="280px"></asp:Label><br />
											<asp:Label ID="lblRemarks2" runat="server" Text='<%# Eval("REMARKS2") %>' Width="280px"></asp:Label><br />
											<asp:Label ID="lblRemarks3" runat="server" Text='<%# Eval("REMARKS3") %>' Width="280px"></asp:Label>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Left" />
									</asp:TemplateField>
								</Fields>
								<HeaderStyle Wrap="False" />
							</asp:DetailsView>
						</fieldset>
					</asp:Panel>
				</td>
				<td>
					<asp:Panel runat="server" ID="pnlReceipt">
						<fieldset class="fieldset-inner">
							<legend>[üąóµ]</legend>
							<asp:DetailsView ID="dvwReceipt" runat="server" DataSourceID="dsUserHistoryByGetOne" AutoGenerateRows="False" SkinID="DetailsView" Width="230px">
								<Fields>
									<asp:BoundField DataField="USER_STATUS_NM" HeaderText="óŌ">
										<ItemStyle HorizontalAlign="Left" />
									</asp:BoundField>
									<asp:TemplateField HeaderText="cĪß²ŻÄ">
										<ItemTemplate>
											<asp:Label ID="lblBalPoint" runat="server" Text='<%# string.Format("{0}P",Eval("BAL_POINT")) %>'></asp:Label>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Right" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="§ĄĪß²ŻÄ">
										<ItemTemplate>
											<asp:Label ID="lblLimitPoint" runat="server" Text='<%# string.Format("{0}P",Eval("LIMIT_POINT")) %>'></asp:Label>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Right" />
									</asp:TemplateField>
									<asp:BoundField DataField="RECEIPT_LIMIT_DAY" HeaderText="üąśś">
										<ItemStyle HorizontalAlign="Left" />
									</asp:BoundField>
									<asp:TemplateField HeaderText="æz">
										<ItemTemplate>
											<asp:Label ID="lblBillAmt" runat="server" Text='<%# string.Format("{0}~",Eval("BILL_AMT")) %>'></asp:Label>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Right" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="üąŻv">
										<ItemTemplate>
											<asp:Label ID="lblTotalReceiptAmt" runat="server" Text='<%# string.Format("{0}~",Eval("TOTAL_RECEIPT_AMT")) %>'></asp:Label>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Right" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="üąń">
										<ItemTemplate>
											<asp:Label ID="lblTotalReceiptCount" runat="server" Text='<%# string.Format("{0}ń",Eval("TOTAL_RECEIPT_COUNT")) %>'></asp:Label>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Right" />
									</asp:TemplateField>
								</Fields>
								<HeaderStyle Wrap="False" />
							</asp:DetailsView>
						</fieldset>
					</asp:Panel>
				</td>
				<td>
					<asp:Panel runat="server" ID="pnlUrge">
						<fieldset class="fieldset-inner">
							<legend>[Ā£óµ]</legend>
							<asp:DetailsView ID="dvwUrge" runat="server" DataSourceID="dsUserHistoryByGetOne" AutoGenerateRows="False" SkinID="DetailsView" Width="300px">
								<Fields>
									<asp:BoundField DataField="URGE_LEVEL" HeaderText="Ā£x" />
									<asp:TemplateField HeaderText="Ā£ĄsĻŻń">
										<ItemTemplate>
											<asp:Label ID="lblUrgeExecCount" runat="server" Text='<%# string.Format("{0}ń",Eval("URGE_EXEC_COUNT")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Ā£Ååń">
										<ItemTemplate>
											<asp:Label ID="lblMaxUrgeCount" runat="server" Text='<%# string.Format("{0}ń",Eval("MAX_URGE_COUNT")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="{śĀ£ń">
										<ItemTemplate>
											<asp:Label ID="lblUrgeCountToday" runat="server" Text='<%# string.Format("{0}ń",Eval("URGE_COUNT_TODAY")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="URGE_END_DAY" HeaderText="Ā£I¹\čś" />
									<asp:BoundField DataField="URGE_RESTART_DAY" HeaderText="Ā£ÄJ\čś" />
									<asp:BoundField DataField="URGE_EXEC_LAST_DATE" HeaderText="Ā£ÅIĄsś" />
									<asp:BoundField DataField="URGE_EXEC_LAST_STATUS_NM" HeaderText="ńüóŌ" />
								</Fields>
								<HeaderStyle Wrap="False" />
							</asp:DetailsView>
						</fieldset>
					</asp:Panel>
				</td>
			</tr>
		</table>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserHistory" runat="server" SelectMethod="GetPageCollection" TypeName="UserHistory" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUserHistory_Selected" OnSelecting="dsUserHistory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pModifyReasonMask" Type="Int16" />
			<asp:Parameter Name="pLoginID" Type="String" />
			<asp:Parameter Name="pModifyDayFrom" Type="String" />
			<asp:Parameter Name="pModifyTimeFrom" Type="String" />
			<asp:Parameter Name="pModifyDayTo" Type="String" />
			<asp:Parameter Name="pModifyTimeTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserHistoryByGetOne" runat="server" SelectMethod="GetOne" TypeName="UserHistory" OnSelected="dsUserHistoryByGetOne_Selected"
		OnSelecting="dsUserHistoryByGetOne_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pUserHisSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrLoginId" HighlightCssClass="validatorCallout" />
</asp:Content>
