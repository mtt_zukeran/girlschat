﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性電話待機一覧
--	Progaram ID		: ManWaitingInquiry
--
--  Creation Date	: 2010.04.20
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Man_ManWaitingInquiry:System.Web.UI.Page {
	private string recCount = "";

	private string RecSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RecSeq"]);
		}
		set {
			this.ViewState["RecSeq"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			lstSiteCd.DataSourceID = "dsSite";
			lstSiteCd.DataBind();
			lstSiteCd.SelectedIndex = 0;
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			InitPage();
			GetList();
		}
	}

	protected void dsRecording_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.Title	= DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.RecSeq = string.Empty;
		pnlKey.Enabled = true;
		pnlMainte.Visible = false;
		ClearField();
	}

	private void ClearField() {
		this.txtWaitingComment.Text = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	private void GetList() {
		grdRecording.PageIndex = 0;
		grdRecording.DataSourceID = "dsRecording";
		grdRecording.DataBind();
		pnlCount.DataBind();		
	}

	protected void dsRecording_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = ViCommConst.REC_TYPE_MAN_WAITING;
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		this.RecSeq = iBridUtil.GetStringValue(e.CommandArgument);
		ClearField();
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECORDING_WAITING_COMMENT_GET");
			db.ProcedureInParm("pREC_SEQ",DbSession.DbType.NUMBER,int.Parse(this.RecSeq));
			db.ProcedureOutParm("pWAITING_COMMENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.txtWaitingComment.Text = db.GetStringValue("pWAITING_COMMENT");
		}

		pnlMainte.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAN_WAITING_COMMENT");
			db.ProcedureInParm("pREC_SEQ",DbSession.DbType.VARCHAR2,this.RecSeq);
			db.ProcedureInParm("pWAITING_COMMENT",DbSession.DbType.VARCHAR2,this.txtWaitingComment.Text);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		InitPage();
		GetList();
	}
}
