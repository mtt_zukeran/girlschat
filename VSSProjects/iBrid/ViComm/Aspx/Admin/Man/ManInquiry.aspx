<%@ Import Namespace="ViComm" %>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManInquiry.aspx.cs" Inherits="Man_ManInquiry" Title="男性会員検索"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性会員検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 1200px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイト
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True"
									OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle" id="tdOnlineState" runat="server">
								オンライン状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkOnline" runat="server" Text="ﾛｸﾞｲﾝ中" />
								<asp:CheckBox ID="chkWaiting" runat="server" Text="待機中" />
								<asp:CheckBox ID="chkTalking" runat="server" Text="ﾄｰｸ中" />
								<asp:CheckBox ID="chkOffline" runat="server" Text="ｵﾌﾗｲﾝ" />
							</td>
							<td class="tdHeaderStyle" id="tdAdCd" runat="server">
								広告コード
								<asp:LinkButton ID="lnkDisplayAdCdList" runat="server" Text="複数入力" OnClick="lnkDisplayAdCdList_Click"></asp:LinkButton>
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="100px"></asp:TextBox>
								<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
									ValidationGroup="Key" Display="Dynamic">*</asp:RegularExpressionValidator>
								<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="txtAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Key"
									Display="Dynamic">*</asp:CustomValidator>
								<asp:Literal ID="lblAdNmNotVisibleStartFont" runat="server"></asp:Literal>
								<asp:Literal ID="lblAdNm" runat="server"></asp:Literal>
								<asp:Literal ID="lblAdNmNotVisibleEndFont" runat="server"></asp:Literal>
								<asp:CheckBox ID="chkAdCdIsNull" runat="server" Text="広告コード未登録" />
								<asp:CheckBox ID="chkExcludeAdCd" runat="server" Text="除外" />
							</td>
						</tr>
						<asp:Panel ID="pnlAdCdList" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderStyle">
									広告コード<br />
									（複数入力）
									<asp:LinkButton ID="lnkHideAdCdList" runat="server" Text="閉じる" OnClick="lnkHideAdCdList_Click"></asp:LinkButton>
									<asp:CheckBox ID="chkAdCdListVisible" runat="server" Visible="false" />
								</td>
								<td class="tdDataStyle" colspan="7">
									<asp:TextBox ID="txtAdCdList" runat="server" Width="500px" TextMode="multiline" Rows="3"></asp:TextBox>
                                    <asp:CustomValidator ID="vdcAdCdList" runat="server" ControlToValidate="txtAdCdList" ErrorMessage="" OnServerValidate="vdcAdCdList_ServerValidate" ValidationGroup="Key"
									    Display="Dynamic">*</asp:CustomValidator>
									<asp:Literal ID="lblAdNmList" runat="server"></asp:Literal>
								</td>
							</tr>
						</asp:Panel>
						<tr>
							<td class="tdHeaderStyle">
								電話番号・GUID
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="88px"></asp:TextBox>&nbsp;<asp:TextBox ID="txtGuid" runat="server" MaxLength="32" Width="70px"></asp:TextBox>
								<asp:CheckBox ID="chkTelIsNull" runat="server" Text="電話番号未登録" />
							</td>
							<td class="tdHeaderStyle">
								メールアドレス
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtEMailAddr" runat="server" Width="170px"></asp:TextBox>
								<asp:CheckBox ID="chkExcludeEmailAddr" runat="server" Text="除外" />
							</td>
							<td class="tdHeaderStyle">
								メールアドレス状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkMailAddrOk" runat="server" Text="正常" />
								<asp:CheckBox ID="chkMailAddrFilteringError" runat="server" Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" />
								<asp:CheckBox ID="chkMailAddrNg" runat="server" Text="不通ｴﾗｰ" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログインID・SEQ<asp:CheckBox ID="chkLoginIdNegativeFlag" runat="server" /><br />
								<asp:LinkButton ID="lnkDisplayLoginIdList" runat="server" Text="複数入力" OnClick="lnkDisplayLoginIdList_Click"></asp:LinkButton>
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="64px"></asp:TextBox>
								<asp:TextBox ID="txtUserSeq" runat="server" MaxLength="15" Width="64px"></asp:TextBox>
							</td>
							<td class="tdHeaderStyle2" id="trHandleNm" runat="server">
								ハンドル名
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtHandleNm" runat="server" MaxLength="120" Width="150px"></asp:TextBox>
							    <asp:CheckBox ID="chkHandleNmExactFlag" runat="server" Checked="false" Text="完全一致" />
							</td>
							<td class="tdHeaderStyle" id="tdHeaderInfoMailRxType" runat="server">
								お知らせﾒｰﾙ受信区分
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstInfoMailRxType" runat="server" DataSourceID="dsInfoMailRxType" DataTextField="CODE_NM" DataValueField="CODE" OnDataBound="lstMailRxType_DataBound"
									Width="200px" />
							</td>
							<asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="false">
								<td class="tdHeaderStyle">
									キャストメール受信区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastMailRxType" runat="server" DataSourceID="dsMailRxType" DataTextField="CODE_NM" DataValueField="CODE" OnDataBound="lstMailRxType_DataBound"
										Width="200px" />
								</td>
							</asp:PlaceHolder>
						</tr>
						<asp:Panel ID="pnlLoginIdList" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderStyle">
									ログインID<asp:CheckBox ID="chkLoginIdListNegativeFlag" runat="server" /><br />
									（複数入力）
									<asp:LinkButton ID="lnkHideLoginIdList" runat="server" Text="閉じる" OnClick="lnkHideLoginIdList_Click"></asp:LinkButton>
									<asp:CheckBox ID="chkLoginIdListVisible" runat="server" Visible="false" />
								</td>
								<td class="tdDataStyle" colspan="7">
									<asp:TextBox ID="txtLoginIdList" runat="server" Width="500px" TextMode="multiline" Rows="3"></asp:TextBox>
								</td>
							</tr>
						</asp:Panel>
						<tr>
							<td class="tdHeaderStyle" rowspan="2">
								会員状態
							</td>
							<td class="tdDataStyle" colspan="3" rowspan="2">
								<asp:CheckBox ID="chkNormal" runat="server" Text="正常" />
								<asp:CheckBox ID="chkManDelay" runat="server" Text="遅延" />
								<asp:CheckBox ID="chkManHold" runat="server" Text="保留" ForeColor="Blue" />
								<asp:CheckBox ID="chkManResigned" runat="server" Text="退会" ForeColor="Maroon" />
								<asp:CheckBox ID="chkManStop" runat="server" Text="禁止" ForeColor="Red" />
								<asp:CheckBox ID="chkManBlack" runat="server" Text="停止" ForeColor="Red" />
								<label id="Label1" runat="server">
									<br />
									&nbsp;&nbsp;&nbsp;└─</label>
								<asp:CheckBox ID="chkManTelNoAuth" runat="server" Text="TEL未認証" />
								<asp:CheckBox ID="chkManNew" runat="server" Text="新規" />
								<asp:CheckBox ID="chkManNoReceipt" runat="server" Text="未入金" />
								<asp:CheckBox ID="chkManNonUsed" runat="server" Text="未稼動" />
								<asp:CheckBox ID="chkManNormal" runat="server" Text="通常" />
								<asp:CheckBox ID="chkMchaUsedFlag" runat="server" Text="ﾎﾟｲﾝﾄｱﾌﾘ決済履歴あり" />
								<asp:CustomValidator ID="vdcStatus" runat="server" Display="none" ErrorMessage="会員状態「正常」と会員状態詳細を同時に検索条件として指定することはできません。" OnServerValidate="vdcStatus_ServerValidate"
									ValidationGroup="Key" />
							</td>
							<td class="tdHeaderStyle">
								会員備考
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRemarks4" runat="server" MaxLength="1024" Width="160px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								入金備考
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRemarks" runat="server" MaxLength="1024" Width="160px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								会員ランク
							</td>
							<asp:PlaceHolder ID="plcRank" runat="server">
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkRankA" runat="server" Text="" />
									<asp:CheckBox ID="chkRankB" runat="server" Text="" />
									<asp:CheckBox ID="chkRankC" runat="server" Text="" />
									<asp:CheckBox ID="chkRankD" runat="server" Text="" />
									<asp:CheckBox ID="chkRankE" runat="server" Text="" />
									<asp:CheckBox ID="chkRankF" runat="server" Text="" />
									<asp:CheckBox ID="chkRankG" runat="server" Text="" />
									<asp:CheckBox ID="chkRankH" runat="server" Text="" />
									<asp:CheckBox ID="chkRankI" runat="server" Text="" />
									<asp:CheckBox ID="chkRankJ" runat="server" Text="" />
									<asp:CheckBox ID="chkRankK" runat="server" Text="" />
									<asp:CheckBox ID="chkRankL" runat="server" Text="" />
									<asp:CheckBox ID="chkRankM" runat="server" Text="" />
									<asp:CheckBox ID="chkRankN" runat="server" Text="" />
									<asp:CheckBox ID="chkRankO" runat="server" Text="" />
									<asp:CheckBox ID="chkRankP" runat="server" Text="" />
									<asp:CheckBox ID="chkRankQ" runat="server" Text="" />
									<asp:CheckBox ID="chkRankR" runat="server" Text="" />
									<asp:CheckBox ID="chkRankS" runat="server" Text="" />
									<asp:CheckBox ID="chkRankT" runat="server" Text="" />
									<asp:CheckBox ID="chkRankU" runat="server" Text="" />
									<asp:CheckBox ID="chkRankV" runat="server" Text="" />
									<asp:CheckBox ID="chkRankW" runat="server" Text="" />
									<asp:CheckBox ID="chkRankX" runat="server" Text="" />
									<asp:CheckBox ID="chkRankY" runat="server" Text="" />
									<asp:CheckBox ID="chkRankZ" runat="server" Text="" />
								</td>
							</asp:PlaceHolder>
							<td class="tdHeaderStyle">
								登録キャリア
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkRegistDocomo" runat="server" Text="D" />
								<asp:CheckBox ID="chkRegistAu" runat="server" Text="A" />
								<asp:CheckBox ID="chkRegistSoftbank" runat="server" Text="S" />
								<asp:CheckBox ID="chkRegistAndroid" runat="server" Text="Android" />
								<asp:CheckBox ID="chkRegistIphone" runat="server" Text="iPhone" />
								<asp:CheckBox ID="chkRegistOther" runat="server" Text="その他" />
							</td>
							<td class="tdHeaderStyle">
								利用キャリア
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkDocomo" runat="server" Text="D" />
								<asp:CheckBox ID="chkAu" runat="server" Text="A" />
								<asp:CheckBox ID="chkSoftbank" runat="server" Text="S" />
								<asp:CheckBox ID="chkAndroid" runat="server" Text="Android" />
								<asp:CheckBox ID="chkIphone" runat="server" Text="iPhone" />
								<asp:CheckBox ID="chkOther" runat="server" Text="その他" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								登録端末
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRegistCarrier" runat="server" MaxLength="128" Width="140px"></asp:TextBox>
							</td>
							<td class="tdHeaderStyle">
								利用端末
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMobileCarrier" runat="server" MaxLength="128" Width="140px"></asp:TextBox>
							</td>
							<td class="tdHeaderStyle">
								ｷｬﾗｸﾀ状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBoxList ID="chkNaFlag" runat="server" DataSourceID="dsNaFlag" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal">
								</asp:CheckBoxList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								入金累計額
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTotalReceiptAmtFrom" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtTotalReceiptAmtTo" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
								<asp:CompareValidator ID="vdcTotalReceiptAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtTotalReceiptAmtFrom" ControlToValidate="txtTotalReceiptAmtTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
							</td>
							<td class="tdHeaderStyle">
								入金回数
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTotalReceiptCountFrom" runat="server" MaxLength="4" Width="44px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtTotalReceiptCountTo" runat="server" MaxLength="4" Width="44px"></asp:TextBox>
								<asp:CompareValidator ID="vdcTotalReceiptCountFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtTotalReceiptCountFrom" ControlToValidate="txtTotalReceiptCountTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
							</td>
							<td class="tdHeaderStyle">
								残ポイント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtBalPointFrom" runat="server" MaxLength="6" Width="48px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtBalPointTo" runat="server" MaxLength="6" Width="48px"></asp:TextBox>
								<asp:CompareValidator ID="vdcBalPointFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtBalPointFrom" ControlToValidate="txtBalPointTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
							</td>
						</tr>
						<asp:PlaceHolder ID="plcOnlyMashup" runat="server">
							<tr>
								<td class="tdHeaderStyle">
									ﾎﾟｲﾝﾄｱﾌﾘ累計額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMchaTotalReceiptAmtFrom" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;〜
									<asp:TextBox ID="txtMchaTotalReceiptAmtTo" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
									<asp:CompareValidator ID="vdcMchaTotalReceiptAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtMchaTotalReceiptAmtFrom" ControlToValidate="txtMchaTotalReceiptAmtTo"
										Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
								</td>
								<td class="tdHeaderStyle">
									ﾎﾟｲﾝﾄｱﾌﾘ回数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMchaTotalReceiptCountFrom" runat="server" MaxLength="4" Width="44px"></asp:TextBox>&nbsp;〜
									<asp:TextBox ID="txtMchaTotalReceiptCountTo" runat="server" MaxLength="4" Width="44px"></asp:TextBox>
									<asp:CompareValidator ID="vdcMchaTotalReceiptCountFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtMchaTotalReceiptCountFrom" ControlToValidate="txtMchaTotalReceiptCountTo"
										Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
								</td>
								<td class="tdHeaderStyle">
									会員ﾃｽﾄﾀｲﾌﾟ
								</td>
								<td class="tdDataStyle">
									<asp:RadioButtonList ID="rdoUserTestType" runat="server" RepeatDirection="horizontal">
										<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
										<asp:ListItem Text="A" Value="1"></asp:ListItem>
										<asp:ListItem Text="B" Value="0"></asp:ListItem>
									</asp:RadioButtonList>
								</td>
							</tr>
						</asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								登録日<asp:CheckBox ID="chkRegistDayNegativeFlag" runat="server" /><br />
								初回ログイン日<br />
								最終ログイン日<asp:CheckBox ID="chkLastLoginDayNegativeFlag" runat="server" /><br />
								最終課金日<asp:CheckBox ID="chkLastPointUsedDayNegativeFlag" runat="server" /><br />
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRegistDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtRegistTimeFrom" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtRegistDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtRegistTimeTo" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>
								<asp:RangeValidator ID="vdrRegistTimeFrom" runat="server" ErrorMessage="登録時Fromを正しく入力して下さい。" ControlToValidate="txtRegistTimeFrom" MaximumValue="23" MinimumValue="00"
									Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrRegistTimeTo" runat="server" ErrorMessage="登録時Toを正しく入力して下さい。" ControlToValidate="txtRegistTimeTo" MaximumValue="23" MinimumValue="00"
									Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrRegistDayFrom" runat="server" ErrorMessage="登録日Fromを正しく入力して下さい。" ControlToValidate="txtRegistDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrRegistDayTo" runat="server" ErrorMessage="登録日Toを正しく入力して下さい。" ControlToValidate="txtRegistDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
									Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcRegistDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtRegistDayFrom" ControlToValidate="txtRegistDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
								<br />
								<asp:TextBox ID="txtFirstLoginDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtFirstLoginTimeFrom" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtFirstLoginDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtFirstLoginTimeTo" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>
								<asp:RangeValidator ID="vdrFirstLoginTimeFrom" runat="server" ErrorMessage="初回ﾛｸﾞｲﾝ時Fromを正しく入力して下さい。" ControlToValidate="txtFirstLoginTimeFrom" MaximumValue="23"
									MinimumValue="00" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrFirstLoginTimeTo" runat="server" ErrorMessage="初回ﾛｸﾞｲﾝ時Toを正しく入力して下さい。" ControlToValidate="txtFirstLoginTimeTo" MaximumValue="23"
									MinimumValue="00" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrFirstLoginDayFrom" runat="server" ErrorMessage="初回ﾛｸﾞｲﾝ日Fromを正しく入力して下さい。" ControlToValidate="txtFirstLoginDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrFirstLoginDayTo" runat="server" ErrorMessage="初回ﾛｸﾞｲﾝ日Toを正しく入力して下さい。" ControlToValidate="txtFirstLoginDayTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcFirstLoginDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtFirstLoginDayFrom" ControlToValidate="txtFirstLoginDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
								<br />
								<asp:TextBox ID="txtLastLoginDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtLastLoginTimeFrom" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtLastLoginDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtLastLoginTimeTo" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>
								<asp:RangeValidator ID="vdrLastLoginTimeFrom" runat="server" ErrorMessage="最終ﾛｸﾞｲﾝ時Fromを正しく入力して下さい。" ControlToValidate="txtLastLoginTimeFrom" MaximumValue="23"
									MinimumValue="00" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastLoginTimeTo" runat="server" ErrorMessage="最終ﾛｸﾞｲﾝ時Toを正しく入力して下さい。" ControlToValidate="txtLastLoginTimeTo" MaximumValue="23"
									MinimumValue="00" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastLoginDayFrom" runat="server" ErrorMessage="最終ﾛｸﾞｲﾝ日Fromを正しく入力して下さい。" ControlToValidate="txtLastLoginDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastLoginDayTo" runat="server" ErrorMessage="最終ﾛｸﾞｲﾝ日Toを正しく入力して下さい。" ControlToValidate="txtLastLoginDayTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcLastLoginDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastLoginDayFrom" ControlToValidate="txtLastLoginDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
								<br />
								<asp:TextBox ID="txtLastPointUsedDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtLastPointUsedTimeFrom" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtLastPointUsedDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:TextBox ID="txtLastPointUsedTimeTo" runat="server" Width="15px" MaxLength="2">
								</asp:TextBox>
								<asp:RangeValidator ID="vdrLastPointUsedTimeFrom" runat="server" ErrorMessage="最終課金時Fromを正しく入力して下さい。" ControlToValidate="txtLastPointUsedTimeFrom" MaximumValue="23"
									MinimumValue="00" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastPointUsedTimeTo" runat="server" ErrorMessage="最終課金時Toを正しく入力して下さい。" ControlToValidate="txtLastPointUsedTimeTo" MaximumValue="23"
									MinimumValue="00" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastPointUsedDayFrom" runat="server" ErrorMessage="最終課金日Fromを正しく入力して下さい。" ControlToValidate="txtLastPointUsedDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastPointUsedDayTo" runat="server" ErrorMessage="最終課金日Toを正しく入力して下さい。" ControlToValidate="txtLastPointUsedDayTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcLastPointUsedDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastPointUsedDayFrom" ControlToValidate="txtLastPointUsedDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
							</td>
							<td class="tdHeaderStyle">
								初回入金日<br />
								最終入金日<br />
								ｱﾌｨﾘｴｲﾄ認証日<br />
							<asp:PlaceHolder ID="plcBlackHeaders" runat="server" Visible="true">
							<label id="lblLastBlackDay" runat="server">
								ﾌﾞﾗｯｸ登録日</label><br />
							</asp:PlaceHolder>
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtFirstReceiptDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtFirstReceiptDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:RangeValidator ID="vdrFirstReceiptDayFrom" runat="server" ErrorMessage="初回入金日Fromを正しく入力して下さい。" ControlToValidate="txtFirstReceiptDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrFirstReceiptDayTo" runat="server" ErrorMessage="初回入金日Toを正しく入力して下さい。" ControlToValidate="txtFirstReceiptDayTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcFirstReceiptDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtFirstReceiptDayFrom" ControlToValidate="txtFirstReceiptDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
								<br />
								<asp:TextBox ID="txtLastReceiptDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtLastReceiptDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:RangeValidator ID="vdrLastReceiptDayFrom" runat="server" ErrorMessage="最終入金日Fromを正しく入力して下さい。" ControlToValidate="txtLastReceiptDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastReceiptDayTo" runat="server" ErrorMessage="最終入金日Toを正しく入力して下さい。" ControlToValidate="txtReportAffiliateDateTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcLastReceiptDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportAffiliateDateFrom" ControlToValidate="txtLastReceiptDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
								<br />
								<asp:TextBox ID="txtReportAffiliateDateFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtReportAffiliateDateTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:RangeValidator ID="vdrReportAffiliateDateFrom" runat="server" ErrorMessage="ｱﾌｨﾘｴｲﾄ認証日Fromを正しく入力して下さい。" ControlToValidate="txtReportAffiliateDateFrom"
									MaximumValue="2099/12/31" MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrReportAffiliateDateTo" runat="server" ErrorMessage="ｱﾌｨﾘｴｲﾄ認証日Toを正しく入力して下さい。" ControlToValidate="txtReportAffiliateDateTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcReportAffiliateDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportAffiliateDateFrom" ControlToValidate="txtReportAffiliateDateTo"
									Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
								<asp:PlaceHolder ID="plcBlackParams" runat="server" Visible="true">
								        <br />
										<asp:TextBox ID="txtLastBlackDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;〜
										<asp:TextBox ID="txtLastBlackDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
										<label id="lblUrgeLevel">督促</label>
										<asp:DropDownList ID="lstUrgeLevel" runat="server" Width="40px">
										<asp:ListItem Text="" Value="" />
										<asp:ListItem Text="1" Value="1" />
										<asp:ListItem Text="2" Value="2" />
										<asp:ListItem Text="3" Value="3" />
										<asp:ListItem Text="4" Value="4" />
										</asp:DropDownList>
										<asp:RangeValidator ID="vdrLastBlackDayFrom" runat="server" ErrorMessage="ﾌﾞﾗｯｸ登録日Fromを正しく入力して下さい。" ControlToValidate="txtLastBlackDayFrom" MaximumValue="2099/12/31"
											MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
										<asp:RangeValidator ID="vdrLastBlackDayTo" runat="server" ErrorMessage="ﾌﾞﾗｯｸ登録日Toを正しく入力して下さい。" ControlToValidate="txtLastBlackDayTo" MaximumValue="2099/12/31"
											MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
										<asp:CompareValidator ID="vdcLastBlackDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastBlackDayFrom" ControlToValidate="txtLastBlackDayTo"
											Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
								</asp:PlaceHolder>
								<br />
							</td>
							<td class="tdHeaderStyle">
								請求額<br />
								ｸﾚｼﾞｯﾄ従量<br />
								ｸﾚｼﾞｯﾄ決済履歴<br />
								ﾎﾟｲﾝﾄｱﾌﾘのみ利用
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtBillAmtFrom" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtBillAmtTo" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
								<asp:CompareValidator ID="vdcBillAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtBillAmtFrom" ControlToValidate="txtBillAmtTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
								<br />
								<asp:CheckBox ID="chkManCreditMeasured" runat="server" Text="" Height="18px" />
								<br />
								<asp:CheckBox ID="chkCreditUsedFlag" runat="server" Text="" Height="18px" />
								<br />
								<asp:CheckBox ID="chkPointAffiFlag" runat="server" Text="" Height="18px" />
							</td>
						</tr>
						<tr>
							<td id="tdServicePointGetCount" runat="server" class="tdHeaderStyle">
								ｻｰﾋﾞｽﾎﾟｲﾝﾄ付与回数
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtServicePointGetCountFrom" runat="server" MaxLength="3" Width="44px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtServicePointGetCountTo" runat="server" MaxLength="3" Width="44px"></asp:TextBox>
								<asp:CompareValidator ID="vdcServicePointGetCount" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtServicePointGetCountFrom" ControlToValidate="txtServicePointGetCountTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
							</td>
							<asp:PlaceHolder ID="plcCleaning" runat="server" Visible="true">
								<td id="tdChkExistAddrExeCount" runat="server" class="tdHeaderStyle">
									ｱﾄﾞﾚｽｸﾘｰﾆﾝｸﾞ実行回数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtChkExistAddrExeCountFrom" runat="server" MaxLength="3" Width="44px"></asp:TextBox>&nbsp;〜
									<asp:TextBox ID="txtChkExistAddrExeCountTo" runat="server" MaxLength="3" Width="44px"></asp:TextBox>
									<asp:CompareValidator ID="vdcChkExistAddrExeCount" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtChkExistAddrExeCountFrom" ControlToValidate="txtChkExistAddrExeCountTo"
										Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
								</td>
							</asp:PlaceHolder>
                            <td class="tdHeaderStyle">
                                ｽﾏｰﾄﾀﾞｲﾚｸﾄ非ﾓﾆﾀﾘﾝｸﾞ
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkTalkSmartDirectNotMonitor" runat="server" Text="" Height="18px" />
                            </td>
						</tr>
                        <tr>
							<td class="tdHeaderStyle">
								Crosmile最終利用Ver.
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtCrosmileLastUsedVersionFrom" runat="server" MaxLength="12" Width="60px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtCrosmileLastUsedVersionTo" runat="server" MaxLength="12" Width="60px"></asp:TextBox>
								<asp:CompareValidator ID="vdcCrosmileLastUsedVersion" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtCrosmileLastUsedVersionFrom" ControlToValidate="txtCrosmileLastUsedVersionTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="String">*</asp:CompareValidator>
	                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdcCrosmileLastUsedVersion" HighlightCssClass="validatorCallout" />
							</td>
							<td class="tdHeaderStyle">
							    Crosmile利用
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoUseCrosmileFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="利用" Value="1"></asp:ListItem>
									<asp:ListItem Text="非利用" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
                            <td class="tdHeaderStyle">
                                音声通話ｱﾌﾟﾘ利用
                            </td>
                            <td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoUseVoiceappFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="利用" Value="1"></asp:ListItem>
									<asp:ListItem Text="非利用" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
							<td class="tdHeaderStyle">
							    ポイント利用
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoPointUseFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="利用" Value="1"></asp:ListItem>
									<asp:ListItem Text="未利用" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
                            <td class="tdHeaderStyle2">
                                承認有無
                            </td>
                            <td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoAuthFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="両方" Value=""></asp:ListItem>
									<asp:ListItem Text="有り" Value="1"></asp:ListItem>
									<asp:ListItem Text="無し" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
                            </td>
                            <td class="tdHeaderStyle">
                                MULLER3送信状態
                            </td>
                            <td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoTxMuller3NgMailAddrFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="正常" Value="0"></asp:ListItem>
									<asp:ListItem Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" Value="1"></asp:ListItem>
								</asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                キーワード(空白区切り)
                            </td>
                            <td class="tdDataStyle" colspan="3">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="500px"></asp:TextBox>&nbsp;ｺﾒﾝﾄを検索
                            </td>
                            <td class="tdHeaderStyle">
                                GCｱﾌﾟﾘVer
                            </td>
                            <td class="tdDataStyle">
								<asp:TextBox ID="txtGcappLastLoginVersionFrom" runat="server" MaxLength="12" Width="60px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtGcappLastLoginVersionTo" runat="server" MaxLength="12" Width="60px"></asp:TextBox>
								<asp:CompareValidator ID="vdcGcappLastLoginVersion" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtGcappLastLoginVersionFrom" ControlToValidate="txtGcappLastLoginVersionTo" Operator="GreaterThanEqual" ValidationGroup="Key" Type="String">*</asp:CompareValidator>
	                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdcGcappLastLoginVersion" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ﾎﾟｲﾝﾄ未使用
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkNotExistFirstPointUseDate" runat="server" />
                            </td>
                            <td class="tdHeaderStyle2">
                                ﾎﾟｲﾝﾄｱﾌﾘ決済日
                            </td>
                            <td class="tdDataStyle">
								<asp:TextBox ID="txtMchaReceiptDateFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								〜
								<asp:TextBox ID="txtMchaReceiptDateTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:RangeValidator ID="vdrMchaReceiptDateFrom" runat="server" ErrorMessage="ﾎﾟｲﾝﾄｱﾌﾘ決済日Fromを正しく入力して下さい。" ControlToValidate="txtMchaReceiptDateFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrMchaReceiptDateTo" runat="server" ErrorMessage="ﾎﾟｲﾝﾄｱﾌﾘ決済日Toを正しく入力して下さい。" ControlToValidate="txtMchaReceiptDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
									Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<ajaxToolkit:MaskedEditExtender ID="mskMchaReceiptDateFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
									TargetControlID="txtMchaReceiptDateFrom"></ajaxToolkit:MaskedEditExtender>
								<ajaxToolkit:MaskedEditExtender ID="mskMchaReceiptDateTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
									TargetControlID="txtMchaReceiptDateTo"></ajaxToolkit:MaskedEditExtender>
                            </td>
                            <td class="tdHeaderStyle2">
                                リッチーノランク
                            </td>
                            <td class="tdDataStyle">
								<asp:DropDownList ID="lstRichinoRankFrom" runat="server" Width="100px" DataSourceID="dsRichinoRank" DataTextField="RICHINO_RANK_NM" DataValueField="RICHINO_RANK" OnDataBound="lstRichinoRank_DataBound"></asp:DropDownList>
								&nbsp;〜&nbsp;
								<asp:DropDownList ID="lstRichinoRankTo" runat="server" Width="100px" DataSourceID="dsRichinoRank" DataTextField="RICHINO_RANK_NM" DataValueField="RICHINO_RANK" OnDataBound="lstRichinoRank_DataBound"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ユーザー定義フラグ<asp:CheckBox ID="chkUserDefineMaskNegatieFlag" runat="server" />
                            </td>
                            <td class="tdDataStyle" colspan="5">
                                <asp:CheckBoxList ID="chkUserDefineMask" runat="server" DataSourceID="dsUserDefineMask" RepeatDirection="horizontal"
                                    DataTextField="CODE_NM" DataValueField="CODE">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <asp:PlaceHolder ID="plcBeforeSystemId" runat="server" Visible="false">
                            <tr>
                                <td class="tdHeaderStyle2">
                                    旧ID
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtBeforeSystemId" runat="server"></asp:TextBox>
                                </td>
                                <td class="tdHeaderStyle2">
                                    紹介された会員
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkIntroducerFriendAllFlag" runat="server" Text="" Height="18px" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>						
					</table>
					<asp:PlaceHolder ID="plcExtension" runat="server" >
						<div style="padding-top: 5px; padding-bottom: 3px">
							<table border="0" class="tableStyle" style="width: 1100px;">
								<tr>
									<td class="tdHeaderStyle">
										ｻｲﾄ利用状況
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkChat" runat="server" Text="ﾗｲﾌﾞﾁｬｯﾄのみ" />
										<asp:CheckBox ID="chkGame" runat="server" Text="ｹﾞｰﾑのみ" />
										<asp:CheckBox ID="chkChatGamet" runat="server" Text="両方" />
									</td>
                                    <td class="tdHeaderStyle">
                                        ゲームハンドル名
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGameHandleNm" runat="server" MaxLength="120"></asp:TextBox>
                                    </td>
                                    <td class="tdHeaderStyle">
                                        ｹﾞｰﾑｷｬﾗｸﾀｰﾀｲﾌﾟ
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:CheckBoxList ID="chkGameCharacterType" runat="server" DataSourceID="dsGameCharacterType"
                                            RepeatDirection="horizontal" DataTextField="CODE_NM" DataValueField="CODE">
                                        </asp:CheckBoxList>
                                    </td>
								</tr>
								<tr>
                                    <td class="tdHeaderStyle2">
                                        ゲーム登録日
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGameRegistDateFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>〜
                                        <asp:TextBox ID="txtGameRegistDateTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
                                        <asp:RangeValidator ID="vdrGameRegistDateFrom" runat="server" ErrorMessage="ゲーム登録日Fromを正しく入力して下さい。"
                                            ControlToValidate="txtGameRegistDateFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                            Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:RangeValidator ID="vdrGameRegistDateTo" runat="server" ErrorMessage="ゲーム登録日Toを正しく入力して下さい。"
                                            ControlToValidate="txtGameRegistDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                            Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:CompareValidator ID="vdcGameRegistDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                            ControlToCompare="txtGameRegistDateFrom" ControlToValidate="txtGameRegistDateTo"
                                            Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                                    </td>
                                    <td class="tdHeaderStyle2">
                                        ｹﾞｰﾑｷｬﾗｸﾀｰﾚﾍﾞﾙ
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGameCharacterLevelFrom" runat="server" MaxLength="4" Width="35px"></asp:TextBox>〜
                                        <asp:TextBox ID="txtGameCharacterLevelTo" runat="server" MaxLength="4" Width="35px"></asp:TextBox>
                                        <asp:RangeValidator ID="vdrGameCharacterLevelFrom" runat="server" ErrorMessage="ｹﾞｰﾑｷｬﾗｸﾀｰﾚﾍﾞﾙFromを正しく入力して下さい。"
                                            ControlToValidate="txtGameCharacterLevelFrom" MaximumValue="9999" MinimumValue="0"
                                            Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:RangeValidator ID="vdrGameCharacterLevelTo" runat="server" ErrorMessage="ｹﾞｰﾑｷｬﾗｸﾀｰﾚﾍﾞﾙToを正しく入力して下さい。"
                                            ControlToValidate="txtGameCharacterLevelTo" MaximumValue="999" MinimumValue="0"
                                            Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:CompareValidator ID="vdcGameCharacterLevelFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                            ControlToCompare="txtGameCharacterLevelFrom" ControlToValidate="txtGameCharacterLevelTo"
                                            Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer" Display="Dynamic">*</asp:CompareValidator>
                                    </td>
                                    <td class="tdHeaderStyle2">
                                        お金
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGamePointFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>〜
                                        <asp:TextBox ID="txtGamePointTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                                        <asp:RangeValidator ID="vdrGamePointFrom" runat="server" ErrorMessage="お金の開始値を正しく入力して下さい。"
                                            ControlToValidate="txtGamePointFrom" MaximumValue="9999999999" MinimumValue="0" Type="Double"
                                            ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:RangeValidator ID="vdrGamePointTo" runat="server" ErrorMessage="お金の終了値を正しく入力して下さい。"
                                            ControlToValidate="txtGamePointTo" MaximumValue="9999999999" MinimumValue="0" Type="Double"
                                            ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:CompareValidator ID="vdcGamePointFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                            ControlToCompare="txtGamePointFrom" ControlToValidate="txtGamePointTo" Operator="GreaterThanEqual"
                                            ValidationGroup="Key" Type="Double" Display="Dynamic">*</asp:CompareValidator>
                                    </td>
								</tr>
							</table>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="plcAttrType" runat="server" Visible="false">
						<div style="padding-top: 5px; padding-bottom: 3px">
							<table border="0" class="tableStyle" style="width: 1000px;">
								<tr>
									<td class="tdHeaderStyle2" id="celManAttrNm0" runat="server" style="width: 115px">
										<asp:Label ID="lblManAttrNm0" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celManAttrSeq0" runat="server">
										<asp:DropDownList ID="lstManAttrSeq0" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdHeaderStyle2" id="celManAttrNm1" runat="server" style="width: 115px">
										<asp:Label ID="lblManAttrNm1" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celManAttrSeq1" runat="server">
										<asp:DropDownList ID="lstManAttrSeq1" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdHeaderStyle2" id="celManAttrNm2" runat="server" style="width: 115px">
										<asp:Label ID="lblManAttrNm2" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celManAttrSeq2" runat="server">
										<asp:DropDownList ID="lstManAttrSeq2" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdHeaderStyle2" id="celManAttrNm3" runat="server" style="width: 115px">
										<asp:Label ID="lblManAttrNm3" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celManAttrSeq3" runat="server">
										<asp:DropDownList ID="lstManAttrSeq3" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
								</tr>
							</table>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="pnlActivated" runat="server" Visible="true">
						<div style="padding-top: 5px; padding-bottom: 3px">
							<table border="0" class="tableStyle" style="width: 1000px;">
								<tr>
									<td class="tdHeaderStyle2" id="Td1" runat="server" style="width: 115px">
										<asp:Label ID="lblActivated" runat="server" Text="アクティベート"></asp:Label>
									</td>
									<td class="tdDataStyle" id="Td2" runat="server">
										<asp:RadioButton ID="rdoActivatedAll" runat="server" Checked="True" GroupName="ActivatedType" Text="全て">
										</asp:RadioButton>
										<asp:RadioButton ID="rdoActivatedOnry" runat="server" GroupName="ActivatedType" Text="アクティベート済 のみ">
										</asp:RadioButton>
										<asp:RadioButton ID="rdoActivatedNG" runat="server" GroupName="ActivatedType" Text="未アクティベート のみ">
										</asp:RadioButton>
									</td>
								</tr>
							</table>
						</div>
					</asp:PlaceHolder>
                    <asp:PlaceHolder ID="plcSelectColumns" runat="server" Visible="false">
                        <div style="padding-top: 5px; padding-bottom: 3px">
                            <table border="0" class="tableStyle" style="width: 1000px;">
                                <tr>
                                    <td class="tdHeaderStyle2">
                                        表示項目
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:CheckBoxList ID="chkSelectColumns" runat="server" DataSource='<%# ColumnsArray %>' />
                                        <asp:CheckBox ID="chkSelectColumnsVisible" runat="server" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:PlaceHolder>
				</asp:Panel>
				<div style="margin-left: 12px">
					<asp:ValidationSummary ID="vrdSummay" runat="server" Font-Size="X-Small" ValidationGroup="Key">
					</asp:ValidationSummary>
				</div>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" CausesValidation="True" />
							<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
							ﾌｨﾙﾀｰ：
							<asp:DropDownList runat="server" ID="lstSeekCondition" AutoPostBack="true" OnSelectedIndexChanged="lstSeekCondition_SelectedIndexChanged">
							</asp:DropDownList>
							<asp:LinkButton ID="lnkEditSeekCondition" runat="server" Text="編集" OnClick="lnkEditSeekCondition_Click" ValidationGroup="Key" CausesValidation="True"></asp:LinkButton>
							<asp:LinkButton ID="lnkAppendSeekCondition" runat="server" Text="追加" OnClick="lnkAppendSeekCondition_Click" ValidationGroup="Key" CausesValidation="True"></asp:LinkButton>
							&nbsp;
						</td>
						<td>
							<asp:Button runat="server" ID="btnInfoSetupMail" Text="お知らせメール送信設定" CssClass="seekbutton" OnClick="btnInfoSetupMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnApproachSetupMail" Text="アプローチメール送信設定" CssClass="seekbutton" OnClick="btnApproachSetupMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnCleaningMail" Text="クリーニングメール送信設定" CssClass="seekbutton" OnClick="btnCleaningMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnToResignedMail" Text="退会者メール送信設定" CssClass="seekbutton" OnClick="btnToResignedMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnSetupBulkUpdate" Text="一括更新" CssClass="seekbutton" OnClick="btnSetupBulkUpdate_Click" CausesValidation="False" />
							<asp:LinkButton ID="lnkSelectColumns" runat="server" Text="[一覧項目選択表示]" CssClass="seekbutton" OnClick="lnkSelectColumns_Click" CausesValidation ="false" />
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblCsvPassword" runat="server" Text="ﾊﾟｽﾜｰﾄﾞ:"></asp:Label>
							<asp:TextBox ID="txtCsvPassword" runat="server" MaxLength="20" Width="140px" TextMode="Password"></asp:TextBox>
							<asp:Button ID="btnOutputCsv" runat="server" Text="CSV出力" OnClick="btnOutputCsv_Click" CausesValidation="False" />
						</td>
						<td>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<asp:Panel ID="pnlSeekConditionMainte" runat="server">
			<fieldset>
				<legend>[検索条件保存]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle">
							ﾌｨﾙﾀｰ名
						</td>
						<td class="tdDataStyle">
							<asp:HiddenField ID="hdnSeekConditionSeq" runat="server">
							</asp:HiddenField>
							<asp:HiddenField ID="hdnSeekConditionRevisionNo" runat="server">
							</asp:HiddenField>
							<asp:TextBox ID="txtSeekConditionNm" runat="server" Width="300px" MaxLength="32"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrSeekConditionNm" runat="server" ErrorMessage="検索条件名を入力して下さい。" ControlToValidate="txtSeekConditionNm" ValidationGroup="SaveSeekCondition">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrSeekConditionNm" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle2">
							公開
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkSeekConditionPublish" runat="server" Text="他の管理者と共有する">
							</asp:CheckBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnSaveSeekCondition" Text="更新" CssClass="seekbutton" OnClick="btnSaveSeekCondition_Click" ValidationGroup="SaveSeekCondition" />
				<asp:Button runat="server" ID="btnDeleteSeekCondition" Text="削除" CssClass="seekbutton" CausesValidation="False" OnClick="btnDeleteSeekCondition_Click" />
				<asp:Button runat="server" ID="btnCancelSeekCondition" Text="中止" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancelSeekCondition_Click" />
				<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender6" runat="server" TargetControlID="btnDeleteSeekCondition" ConfirmText="検索条件設定の削除を実行しますか？"
					ConfirmOnFormSubmit="true" />
				<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender7" runat="server" TargetControlID="btnSaveSeekCondition" ConfirmText="検索条件設定の更新を実行しますか？" ConfirmOnFormSubmit="true" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlTxMail">
			<fieldset>
				<legend>[メール送信設定]</legend>
				<table border="0" class="tableStyleAutoSize">
					<tr>
						<td class="tdHeaderStyle">
							ﾒｰﾙ種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstMailTemplateNo" runat="server" Width="309px" DataSourceID="dsDMMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO"
								OnSelectedIndexChanged="lstMailTemplateNo_SelectedIndexChanged" AutoPostBack="true">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							ｻｰﾋﾞｽPt
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtServicePoint" runat="server" MaxLength="4" Width="30px"></asp:TextBox>Pt
							<asp:RequiredFieldValidator ID="vdrServicePoint" runat="server" ErrorMessage="サービスポイントを入力して下さい。" ControlToValidate="txtServicePoint" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle2">
							ﾒｰﾙ有効時間
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtMailAvaHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
							<asp:RequiredFieldValidator ID="vdrMailAvaHour" runat="server" ErrorMessage="メール有効時間を入力して下さい。" ControlToValidate="txtMailAvaHour" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle2">
							Pt振替後有効時間
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtPointTransferAvaHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
							<asp:RequiredFieldValidator ID="vdrPointTransferAvaHour" runat="server" ErrorMessage="ポイント振替後有効時間を入力して下さい。" ControlToValidate="txtPointTransferAvaHour"
								ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
						</td>
					</tr>
					<asp:PlaceHolder ID="plcMailSendType" runat="server" Visible="true">
						<tr>
							<td class="tdHeaderStyle2" ID="tdHeaderMailSendType" runat="server">
								ﾒｰﾙ送信種別
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoMailSendType" runat="server" DataSourceID="dsMailSendType" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal">
								</asp:RadioButtonList>
							</td>
							<td class="tdHeaderStyle2">
								ﾒｰﾙｻｰﾊﾞｰ
							</td>
							<td class="tdDataStyle" colspan="5">
								<asp:RadioButtonList ID="rdoMailServer" runat="server" RepeatDirection="Horizontal">
									<asp:ListItem Text="BEAM" Value="1" Selected="True"></asp:ListItem>
									<asp:ListItem Text="MULLER3" Value="2"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="plcReservationMail" runat="server" Visible="false">
						<tr>
							<td class="tdHeaderStyle2">
								送信予定日時
							</td>
							<td class="tdDataStyle" colspan="7">
								<asp:TextBox ID="txtReservationSendDay" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;
								<asp:TextBox ID="txtReservationSendHour" runat="server" Width="15px" MaxLength="2"></asp:TextBox>時
								<asp:TextBox ID="txtReservationSendMinute" runat="server" Width="15px" MaxLength="2"></asp:TextBox>分
								<asp:RangeValidator ID="vdrReservationSendHour" runat="server" ErrorMessage="送信予定時間を正しく入力して下さい。" ControlToValidate="txtReservationSendHour" MaximumValue="23" MinimumValue="00"
									Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrReservationSendMinute" runat="server" ErrorMessage="送信予定分を正しく入力して下さい。" ControlToValidate="txtReservationSendMinute" MaximumValue="59" MinimumValue="00"
									Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrReservationSendDay" runat="server" ErrorMessage="送信予定日を正しく入力して下さい。" ControlToValidate="txtReservationSendDay" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<ajaxToolkit:MaskedEditExtender ID="mskReservationSendDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
									TargetControlID="txtReservationSendDay"></ajaxToolkit:MaskedEditExtender>
								<ajaxToolkit:FilteredTextBoxExtender ID="fltReservationSendHour" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReservationSendHour" />
								<ajaxToolkit:FilteredTextBoxExtender ID="fltReservationSendMinute" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReservationSendMinute" />
							</td>
						</tr>
					</asp:PlaceHolder>
					<tr runat="server" id="trCastLoginId">
						<td class="tdHeaderSmallStyle2">
							<%= DisplayWordUtil.Replace("出演者ID") %>
						</td>
						<td class="tdDataStyle" id="tdDataCastLoginId" runat="server">
							<asp:TextBox ID="txtCastLoginId" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
							<asp:CustomValidator ID="vdcCastLoginId" runat="server" ControlToValidate="txtCastLoginId" ErrorMessage="出演者ログインＩＤが未登録です。" OnServerValidate="vdcCastLoginId_ServerValidate"
								ValidationGroup="TxMail" Display="Dynamic" ValidateEmptyText="True">*</asp:CustomValidator>
							<asp:Label ID="lblCastHandleNm" runat="server" Text=""></asp:Label>
						</td>
						<td runat="server" id="tdHeaderCastCharNo" class="tdHeaderStyle">
							ｷｬﾗｸﾀｰNo
						</td>
						<td runat="server" id="tdDataCastCharNo" class="tdDataStyle" colspan="5">
							<asp:TextBox ID="txtCastCharNo" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
							<asp:CustomValidator ID="vdcCastCharNo" runat="server" ControlToValidate="txtCastCharNo" ErrorMessage="出演者キャラクターＮｏが未登録です。" ValidationGroup="TxMail" Display="Dynamic"
								ValidateEmptyText="True" OnServerValidate="vdcCastCharNo_ServerValidate">*</asp:CustomValidator>
						</td>
					</tr>
				</table>
				<asp:Label ID="lblAttention" runat="server" Text="メール有効時間/Pt振替後有効時間に０を設定すると無期限になります。" Font-Size="Small" ForeColor="Red"></asp:Label><br />
				<asp:Label ID="lblAttentionCleaning" runat="server" Text="クリーニングメールはフィルタリングエラーの会員にのみ送信されます。" Font-Size="Small" ForeColor="Red" Visible="false"></asp:Label><br />
				<asp:Label ID="lblAttentionToResigned" runat="server" Text="検索結果に退会者以外が含まれる場合、退会者以外にも送信されます。" Font-Size="Small" ForeColor="Red" Visible="false"></asp:Label><br />
				<asp:Button runat="server" ID="btnTxMail" Text="メール送信" CssClass="seekbutton" OnClick="btnTxMail_Click" ValidationGroup="TxMail" />
				<asp:Button runat="server" ID="btnTxReservationMail" Text="予約送信" CssClass="seekbutton" OnClick="btnTxReservationMail_Click" ValidationGroup="TxMail" Visible="false" />
				<asp:Button runat="server" ID="btnCancelTx" Text="中止" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancelTx_Click" />
				<asp:LinkButton ID="lnkMailTemplate" runat="server" CssClass="seekbutton" OnClick="lnkMailTemplate_Click" Text="[プレビュー表示]" />
				<asp:LinkButton ID="lnkPointMailOutcome" runat="server" CssClass="seekbutton" OnClick="lnkPointMailOutcome_Click" Text="[ｻｰﾋﾞｽﾎﾟｲﾝﾄ記録]" />
				<br />
                <asp:Panel ID="pnlPointMailOutcome" runat="server" CssClass="collapsePanel">
                    <br />
                    <fieldset class="fieldset-inner">
                        <legend>[ｻｰﾋﾞｽﾎﾟｲﾝﾄ記録]</legend>
                        <table border="0" class="tableStyleAutoSize">
                            <tr>
                                <td class="tdHeaderSmallStyle">
                                    記録する
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkPointMailOutcome" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderSmallStyle2">
                                    備考
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtPointMailOutcomeRemarks" runat="server" MaxLength="1000" TextMode="MultiLine"
                                        Rows="4" Width="500px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
				<asp:Panel ID="pnlMailTemplate" runat="server" CssClass="collapsePanel">
					<br />
					<fieldset class="fieldset-inner">
						<legend>[メールプレビュー]</legend>
						<asp:GridView ID="grdMailTemplate" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="" SkinID="GridView" Width="230px" ShowHeader="False">
							<Columns>
								<asp:TemplateField>
									<ItemTemplate>
										<asp:Literal ID="lblHtmlDoc1" runat="server" Text='<%# ReplaceForm(string.Format("<table class=\"userColor\" ><tr><td width=\"220px\">{0}</tr></td></table>", ViComm.ViCommConst.FLAG_OFF_STR.Equals(Eval("TEXT_MAIL_FLAG").ToString()) ? string.Concat(Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4"), Eval("HTML_DOC5"), Eval("HTML_DOC6"),Eval("HTML_DOC7"), Eval("HTML_DOC8") ,Eval("HTML_DOC9"), Eval("HTML_DOC10")) : string.Concat(Eval("HTML_DOC11"), Eval("HTML_DOC12"), Eval("HTML_DOC13"), Eval("HTML_DOC14"), Eval("HTML_DOC15"), Eval("HTML_DOC16"),Eval("HTML_DOC17"), Eval("HTML_DOC18") ,Eval("HTML_DOC19"), Eval("HTML_DOC20")))) %>'></asp:Literal>
									</ItemTemplate>
									<ItemStyle HorizontalAlign="Left" />
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlBulkUpdate">
			<fieldset>
				<legend>[一括更新]</legend>
				<ajaxToolkit:TabContainer ID="tabBulkUpdate" runat="server" Width="650px">
					<ajaxToolkit:TabPanel ID="tpgBulkUpdateUserStatus" runat="server" HeaderText="会員状態">
						<ContentTemplate>
							<fieldset>
								<table border="0" style="width: 95%" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											会員状態
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstUserStatus" runat="server" Width="206px" DataSourceID="dsUserStatus" DataTextField="CODE_NM" DataValueField="CODE">
											</asp:DropDownList>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnBulkUpdUserStatus" Text="更新" CssClass="seekbutton" ValidationGroup="BULK_UPD_USER_STATUS" OnClick="btnExecuteBulkUpdate_Click" />
								<asp:Button runat="server" ID="btnCancelBulkUpdUserStatus" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnBulkUpdUserStatus" ConfirmText="会員状態の一括更新を行います。よろしいですか？"
									ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
					<ajaxToolkit:TabPanel ID="tpgBulkUpdateAdCd" runat="server" HeaderText="広告コード">
						<ContentTemplate>
							<fieldset>
								<table border="0" style="width: 95%" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											広告コード
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAdCd4BulkUpd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
											<asp:RegularExpressionValidator ID="vdeAdCd4BulkUpd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}"
												ControlToValidate="txtAdCd4BulkUpd" ValidationGroup="BULK_UPD_AD_CD" Display="Dynamic">*</asp:RegularExpressionValidator>
											<asp:CustomValidator ID="vdcAdCd4BulkUpd" runat="server" ControlToValidate="txtAdCd4BulkUpd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate"
												ValidationGroup="BULK_UPD_AD_CD" Display="Dynamic">*</asp:CustomValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdeAdCd4BulkUpd" HighlightCssClass="validatorCallout" />
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdcAdCd4BulkUpd" HighlightCssClass="validatorCallout" />
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnBulkUpdAdCd" Text="更新" CssClass="seekbutton" ValidationGroup="BULK_UPD_AD_CD" OnClick="btnExecuteBulkUpdate_Click" />
								<asp:Button runat="server" ID="btnCancelBulkUpdAdCd" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnBulkUpdAdCd" ConfirmText="広告コードの一括更新を行います。よろしいですか？" ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
					<ajaxToolkit:TabPanel ID="tpgBulkUpdateMailFlag" runat="server" HeaderText="メールアドレスＮＧ解除">
						<ContentTemplate>
							<fieldset>
								<font color="red">※メールアドレスNGを解除します。</font><br />
								<asp:Button runat="server" ID="btnBulkUpdMailFlag" Text="解除する" CssClass="seekbutton" ValidationGroup="BULK_UPD_MAIL_FLAG" OnClick="btnExecuteBulkUpdate_Click" />
								<asp:Button runat="server" ID="btnCancelBulkUpdMailFlag" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnBulkUpdMailFlag" ConfirmText="ﾒｰﾙｱﾄﾞﾚｽNGの一括解除を行います。よろしいですか？"
									ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
					<ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
						<HeaderTemplate>
							<font color="red">削除</font>
						</HeaderTemplate>
						<ContentTemplate>
							<fieldset>
								<font color="red"><b>注意）一括削除を行います。削除したデータは元に戻すことが出来ません。</b></font><br />
								<asp:Button runat="server" ID="btnBulkDelete" Text="削除する" CssClass="seekbutton" ValidationGroup="BULK_DELETE" OnClick="btnExecuteBulkUpdate_Click" BackColor="red" />
								<asp:Button runat="server" ID="btnCancelBulkDelete" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender5" runat="server" TargetControlID="btnBulkDelete" ConfirmText="条件に該当するデータの一括削除を行います。よろしいですか？"
									ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
					<ajaxToolkit:TabPanel ID="tpgTalkDirectSmartNotMonitor" runat="server" HeaderText="モニタリング">
						<ContentTemplate>
							<fieldset>
								<table border="0" style="width: 95%" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											ｽﾏｰﾄﾀﾞｲﾚｸﾄ非ﾓﾆﾀﾘﾝｸﾞ
										</td>
                                        <td class="tdDataStyle">
                                            <asp:CheckBox ID="chkNotMonitor" runat="server" />スマートダイレクト時、モニタリングしない。
                                        </td>
                                    </tr>
								</table>
								<asp:Button runat="server" ID="btnBulkUpdTalkDirectSmartNotMonitor" Text="更新" CssClass="seekbutton" ValidationGroup="BULK_UPD_TALK_SMART_DIRECT_NOT_MONITOR" OnClick="btnExecuteBulkUpdate_Click" />
								<asp:Button runat="server" ID="btnCancelTalkDirectSmartNotMonitor" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender8" runat="server" TargetControlID="btnBulkUpdTalkDirectSmartNotMonitor" ConfirmText="モニタフラグの一括更新を行います。よろしいですか？"
									ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
				</ajaxToolkit:TabContainer>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[会員一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<asp:Label ID="lblRecCount" runat="server" Text="<%# GetRecCount() %>" Visible="false" />
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdUserManCharacter.PageIndex + 1%>
						of
						<%=grdUserManCharacter.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:LinkButton ID="lnkCondition" runat="server" OnClick="lnkCondition_Click" Text="[条件非表示]" />
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="290px">
					<asp:GridView ID="grdUserManCharacter" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserManCharacter" SkinID="GridViewColor"
						OnDataBound="grdUserManCharacter_DataBound" AllowSorting="True" Font-Size="X-Small" OnSorting="grdUserManCharacter_Sorting" HeaderStyle-Wrap="false" RowStyle-Wrap="false">
						<Columns>
							<asp:TemplateField HeaderText="ID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙﾈｰﾑ" SortExpression="HANDLE_NM">
								<ItemTemplate>
									<asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(ViCommPrograms.DefHandleName(Eval("HANDLE_NM"))) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="left" />
							</asp:TemplateField>
							<asp:BoundField DataField="USER_RANK_NM" HeaderText="ランク" SortExpression="USER_RANK">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="USER_STATUS_NM" HeaderText="状態" SortExpression="USER_STATUS">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="AD_CD" HeaderText="広告ｺｰﾄﾞ" SortExpression="AD_CD">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							
							<asp:TemplateField HeaderText="認証">
								<ItemTemplate>
									<asp:Label ID="lblAuth" runat="server" Text='<%# GetServicePtFlag(Eval("REGIST_SERVICE_POINT_FLAG")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>

							<asp:TemplateField HeaderText="残Pt" SortExpression="BAL_POINT">
								<ItemTemplate>
									<asp:Label ID="lblBalPoint" runat="server" Text='<%# Eval("BAL_POINT") %>'></asp:Label><br />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｻｰﾋﾞｽPt">
								<ItemTemplate>
									<asp:Label ID="lblServiePoint" runat="server" Text='<%# GetServicePoint(Eval("SERVICE_POINT"),Eval("SERVICE_POINT_EFFECTIVE_DATE")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="請求額">
								<ItemTemplate>
									<asp:Label ID="lblBillAmount" runat="server" Text='<%# Eval("BILL_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="入金累計額">
								<ItemTemplate>
									<asp:Label ID="lblTotalReceiptAmount" runat="server" Text='<%# Eval("TOTAL_RECEIPT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ポイントアフリ計">
								<ItemTemplate>
									<asp:Label ID="lblMchaTotalReceiptAmount" runat="server" Text='<%# Eval("POINT_AFFILIATE_TOTAL_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
                            <asp:BoundField DataField="EMAIL_ADDR" HeaderText="メールアドレス" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left" Wrap="false"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="REGIST_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="LAST_LOGIN_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="LAST_POINT_USED_DATE" HeaderText="最終課金日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="LAST_POINT_USED_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsUserManCharacter" runat="server" SelectMethod="GetPageCollection" TypeName="UserManCharacter" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsUserManCharacter_Selected" OnSelecting="dsUserManCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserRankMask" Type="Int16" />
			<asp:Parameter Name="pUserStatusRank" Type="Int16" />
			<asp:Parameter Name="pUserOnlineStatus" Type="Int16" />
			<asp:Parameter Name="pCarrier" Type="Int16" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pLoginID" Type="String" />
			<asp:Parameter Name="pBalPointFrom" Type="String" />
			<asp:Parameter Name="pBalPointTo" Type="String" />
			<asp:Parameter Name="pEMailAddr" Type="String" />
			<asp:Parameter Name="pHandleNm" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pRemarks" Type="String" />
			<asp:Parameter Name="pTotalReceiptAmtFrom" Type="String" />
			<asp:Parameter Name="pTotalReceiptAmtTo" Type="String" />
			<asp:Parameter Name="pRegistDayFrom" Type="String" />
			<asp:Parameter Name="pRegistDayTo" Type="String" />
			<asp:Parameter Name="pFirstLoginDayFrom" Type="String" />
			<asp:Parameter Name="pFirstLoginDayTo" Type="String" />
			<asp:Parameter Name="pLastLoginDayFrom" Type="String" />
			<asp:Parameter Name="pLastLoginDayTo" Type="String" />
			<asp:Parameter Name="pLastPointUsedDayFrom" Type="String" />
			<asp:Parameter Name="pLastPointUsedDayTo" Type="String" />
			<asp:Parameter Name="pFirstReceiptDayFrom" Type="String" />
			<asp:Parameter Name="pFirstReceiptDayTo" Type="String" />
			<asp:Parameter Name="pLastReceiptDayFrom" Type="String" />
			<asp:Parameter Name="pLastReceiptDayTo" Type="String" />
			<asp:Parameter Name="pTotalReceiptCountFrom" Type="String" />
			<asp:Parameter Name="pTotalReceiptCountTo" Type="String" />
			<asp:Parameter Name="pNonExistMailAddrFlagMask" Type="Int16" />
			<asp:Parameter Name="pFirstLoginTimeFrom" Type="String" />
			<asp:Parameter Name="pFirstLoginTimeTo" Type="String" />
			<asp:Parameter Name="pLastLoginTimeFrom" Type="String" />
			<asp:Parameter Name="pLastLoginTimeTo" Type="String" />
			<asp:Parameter Name="pLastPointUsedTimeFrom" Type="String" />
			<asp:Parameter Name="pLastPointUsedTimeTo" Type="String" />
			<asp:Parameter Name="pRegistTimeFrom" Type="String" />
			<asp:Parameter Name="pRegistTimeTo" Type="String" />
			<asp:Parameter Name="pLastBlackDayFrom" Type="String" />
			<asp:Parameter Name="pLastBlackDayTo" Type="String" />
			<asp:Parameter Name="pBillAmtFrom" Type="String" />
			<asp:Parameter Name="pBillAmtTo" Type="String" />
			<asp:Parameter Name="pMchaTotalReceiptAmtFrom" Type="String" />
			<asp:Parameter Name="pMchaTotalReceiptAmtTo" Type="String" />
			<asp:Parameter Name="pMchaTotalReceiptCountFrom" Type="String" />
			<asp:Parameter Name="pMchaTotalReceiptCountTo" Type="String" />
			<asp:Parameter Name="pUrgeLevel" Type="String" />
			<asp:Parameter Name="pCreditMeasured" Type="Boolean" />
			<asp:Parameter Name="pMchaUsedFlag" Type="Boolean" />
			<asp:Parameter Name="pCreditUsedFlag" Type="Boolean" />
			<asp:Parameter Name="pManAttrValue1" Type="String" />
			<asp:Parameter Name="pManAttrValue2" Type="String" />
			<asp:Parameter Name="pManAttrValue3" Type="String" />
			<asp:Parameter Name="pManAttrValue4" Type="String" />
			<asp:Parameter Name="pWithUnRegistComplete" Type="Int16" />
			<asp:Parameter Name="pGuid" Type="String" />
			<asp:Parameter Name="pPointAffiFlag" Type="Boolean" />
			<asp:Parameter Name="pCastMailRxType" Type="String" />
			<asp:Parameter Name="pInfoMailRxType" Type="String" />
			<asp:Parameter Name="pIntroducerFriendCd" Type="String" />
			<asp:Parameter Name="pRemarks4" Type="String" />
			<asp:Parameter Name="pRegistCarrier" Type="String" />
			<asp:Parameter Name="pMobileCarrier" Type="String" />
			<asp:Parameter Name="pIsNullAdCd" Type="Boolean" />
			<asp:Parameter Name="pNaFlag" Type="String" />
			<asp:Parameter Name="pReportAffiliateDateFrom" Type="String" />
			<asp:Parameter Name="pReportAffiliateDateTo" Type="String" />
			<asp:Parameter Name="pServicePointGetCountFrom" Type="String" />
			<asp:Parameter Name="pServicePointGetCountTo" Type="String" />
			<asp:Parameter Name="pChkExistAddrExeCountFrom" Type="String" />
			<asp:Parameter Name="pChkExistAddrExeCountTo" Type="String" />
			<asp:Parameter Name="pSiteUseStatus" Type="String" />
            <asp:Parameter Name="pUserDefineMask" Type="String" />
            <asp:Parameter Name="pExceptRefusedByCastUserSeq" Type="String" />
            <asp:Parameter Name="pGameHandleNm" Type="String" />
            <asp:Parameter Name="pGameCharacterType" Type="String" />
            <asp:Parameter Name="pGameRegistDateFrom" Type="String" />
            <asp:Parameter Name="pGameRegistDateTo" Type="String" />
            <asp:Parameter Name="pGameCharacterLevelFrom" Type="String" />
            <asp:Parameter Name="pGameCharacterLevelTo" Type="String" />
            <asp:Parameter Name="pGamePointFrom" Type="String" />
            <asp:Parameter Name="pGamePointTo" Type="String" />
            <asp:Parameter Name="pHandleNmExactFlag" Type="boolean" />
            <asp:Parameter Name="pBeforeSystemId" Type="String" />
            <asp:Parameter Name="pKeyword" Type="String" DefaultValue="" />
            <asp:Parameter Name="pLoginIdNegativeFlag" Type="Boolean" />
            <asp:Parameter Name="pLastLoginDayNegativeFlag" Type="Boolean" />
            <asp:Parameter Name="pLastPointUsedDayNegativeFlag" Type="Boolean" />
            <asp:Parameter Name="pRegistDayNegativeFlag" Type="Boolean" />
            <asp:Parameter Name="pUserDefineMaskNegatieFlag" Type="Boolean" />
            <asp:Parameter Name="pIntroducerFriendAllFlag" Type="Boolean" />
            <asp:Parameter Name="pCrosmileLastUsedVersionFrom" Type="String" />
            <asp:Parameter Name="pCrosmileLastUsedVersionTo" Type="String" />
            <asp:Parameter Name="pTalkDirectSmartNotMonitorFlag" Type="Boolean" />
            <asp:Parameter Name="pUseCrosmileFlag" Type="String" />
            <asp:Parameter Name="pSortExpression" Type="String" />
			<asp:Parameter Name="pSortDirection" Type="String" />
			<asp:Parameter Name="pIsNullTel" Type="Boolean" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUseVoiceappFlag" Type="String" />
			<asp:Parameter Name="pPointUseFlag" Type="String" />
			<asp:Parameter Name="pAuthFlag" Type="String" />
			<asp:Parameter Name="pGcappLastLoginVersionFrom" Type="String" />
			<asp:Parameter Name="pGcappLastLoginVersionTo" Type="String" />
			<asp:Parameter Name="pUserTestType" Type="String" />
			<asp:Parameter Name="pRegistCarrierCd" Type="Int16" />
			<asp:Parameter Name="pNotExistFirstPointUsedDate" Type="Boolean" />
			<asp:Parameter Name="pExcludeEmailAddr" Type="boolean" />
			<asp:Parameter Name="pTxMuller3NgMailAddrFlag" Type="string" />
			<asp:Parameter Name="pRichinoRankFrom" Type="string" />
			<asp:Parameter Name="pRichinoRankTo" Type="string" />
			<asp:Parameter Name="pExcludeAdCd" Type="Boolean" />
			<asp:Parameter Name="pMchaReceiptDateFrom" Type="string" />
			<asp:Parameter Name="pMchaReceiptDateTo" Type="string" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDMMail" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate" OnSelecting="dsDMMail_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="50" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailSendType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="28" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="66" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsInfoMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="15" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetListByTemplateNo" TypeName="MailTemplate">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMailTemplateNo" Name="pMailTemplateNo" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNaFlag" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="98" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsUserDefineMask" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A0" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameCharacterType" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A3" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRichinoRank" runat="server" SelectMethod="GetList" TypeName="RichinoRank"></asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum1" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="txtBalPointFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum2" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="txtBalPointTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalReceiptAmtFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalReceiptAmtTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtFirstLoginTimeFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtFirstLoginTimeTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum7" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLastLoginTimeFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLastLoginTimeTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLastPointUsedTimeFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLastPointUsedTimeTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum11" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRegistTimeFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum12" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRegistTimeTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum13" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalReceiptCountFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum14" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalReceiptCountTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum15" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMchaTotalReceiptAmtFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMchaTotalReceiptAmtTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum17" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMchaTotalReceiptCountFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum18" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMchaTotalReceiptCountTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum19" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBillAmtFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum20" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBillAmtTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum21" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtServicePointGetCountFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum22" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtServicePointGetCountTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum23" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtChkExistAddrExeCountFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum24" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtChkExistAddrExeCountTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum25" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="txtBalPointFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum26" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="txtBalPointTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum27" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtServicePoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum28" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMailAvaHour" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum29" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPointTransferAvaHour" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGameCharacterLevelFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGameCharacterLevelTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGamePointFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGamePointTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUserSeq" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtGcappLastLoginVersionFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtGcappLastLoginVersionTo" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnTxMail" ConfirmText="メール送信を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskFirstLoginDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtFirstLoginDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskFirstLoginDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtFirstLoginDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastLoginDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastLoginDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastLoginDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastLoginDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastPointUsedDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastPointUsedDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastPointUsedDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastPointUsedDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskFirstReceiptDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtFirstReceiptDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskFirstReceiptDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtFirstReceiptDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastReceiptDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastReceiptDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastReceiptDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastReceiptDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportAffiliateDateFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportAffiliateDateFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportAffiliateDateTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportAffiliateDateTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastBlackDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastBlackDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="MskLastBlackDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastBlackDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskGameRegistDateFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtGameRegistDateFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskGameRegistDateTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtGameRegistDateTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrGameRegistDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrGameRegistDateTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcGameRegistDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrGamePointFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrGamePointTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcGamePointFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcAdCdList" HighlightCssClass="validatorCallout" />
</asp:Content>
