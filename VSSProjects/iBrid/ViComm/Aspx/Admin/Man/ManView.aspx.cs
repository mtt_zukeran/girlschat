﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員詳細
--	Progaram ID		: ManView
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Net;
using System.IO;
using System.Text;

public partial class Man_ManView:System.Web.UI.Page {
	private string siteCd = "";
	private string loginId = "";
	private string userSeq = "";
	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}
	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		txtMailDocHtml.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);

		if (!IsPostBack) {
			int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
			siteCd = iBridUtil.GetStringValue(Request.QueryString["site"]);
			loginId = iBridUtil.GetStringValue(Request.QueryString["manloginid"]);
			userSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
			ViewState["SiteCd"] = siteCd;
			InitPage();
			DataBind();
			lstSiteCd.DataSourceID = "";
			showSettleInstantButton();
			vdrModifyUserNm.Enabled = false;
			lnkUserHistory.Enabled = (string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_STAFF) > 0);
			this.rowManWaitingComment.Visible = false;
		}
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		lblNotFound.Visible = false;
		lblManyUser.Visible = false;
		if (IsValid) {
			pnlDtl.Visible = true;
			CloseExtInfo();
			dvwUserManCharacter.DataSourceID = "dsUserManCharacter";
			dvwUserManCharacter.DataBind();
			dvwUrge.DataBind();
			SetProfile();
			SetGameCharacter();
			this.lstGameItemCategory.DataBind();
			showSettleInstantButton();
		}
	}

	protected void btnReceiptHis_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlExtInfo.Visible = true;

				using (ManageCompany oCompany = new ManageCompany()) {
					if (oCompany.IsAvailableService(ViCommConst.RELEASE_SETTLEMENT_LOG)) {
						pnlSettleHis.Visible = true;
						grdSettle.DataSourceID = "dsSettle";
					} else {
						pnlReceiptHis.Visible = true;
						grdReceipt.DataSourceID = "dsReceipt";
					}
				}
			}
		}
	}

	/// <summary>
	/// 利用履歴リンクをクリック
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnTalkHis_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();

			// 利用履歴を表示
			if (this.IsExistManData()) {
				dispUsedLog();
			}
		}
	}

	/// <summary>
	/// 利用履歴を表示
	/// </summary>
	private void dispUsedLog() {
		pnlDtl.Visible = true;
		pnlUsedHis.Visible = true;
		pnlExtInfo.Visible = true;
		pnlUsedLogMonitor.Visible = true;
		pnlTalkHis.Visible = true;

		this.grdWebUsedLogSummary.DataSourceID = "dsWebUsedLogSummary";
		this.grdTalkHistorySummary.DataSourceID = "dsUsedLogTalkSummary";
		this.grdWebUsedLog.DataSourceID = "dsWebUsedLog";
		this.grdUsedLogMonitor.DataSourceID = "dsUsedLogMonitor";
		this.grdUsedLogMonitorSummary.DataSourceID = "dsUsedLogMonitorSummary";
		this.grdTalkHistory.DataSourceID = "dsUsedLogTalk";
	}

	protected void btnFavorit_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlFavorit.Visible = true;
				pnlExtInfo.Visible = true;
				grdFavorit.DataSourceID = "dsFavorit";
			}
		}
	}

	protected void btnBbs_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlBbs.Visible = true;
				pnlExtInfo.Visible = true;
				grdBbs.DataSourceID = "dsBbsLog";
			}
		}
	}

	protected void btnLikeMe_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlLikeMe.Visible = true;
				pnlExtInfo.Visible = true;
				grdLikeMe.DataSourceID = "dsLikeMe";
			}
		}
	}

	protected void btnRefuse_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlRefuse.Visible = true;
				pnlExtInfo.Visible = true;
				grdRefuse.DataSourceID = "dsRefuse";
			}
		}
	}

	protected void btnRefuseMe_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlRefuseMe.Visible = true;
				pnlExtInfo.Visible = true;
				grdRefuseMe.DataSourceID = "dsRefuseMe";
			}
		}
	}

	protected void btnMarking_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlMarking.Visible = true;
				pnlExtInfo.Visible = true;
				grdMarking.DataSourceID = "dsMarking";
			}
		}
	}

	protected void btnMailBox_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				SysPrograms.SetupFromToDayTime(lstMailFromYYYY,lstMailFromMM,lstMailFromDD,lstMailFromHH,lstMailToYYYY,lstMailToMM,lstMailToDD,lstMailToHH,false);
				DateTime dtFrom = DateTime.Now.AddDays(-7);
				DateTime dtTo = DateTime.Now;
				lstMailFromYYYY.SelectedValue = dtFrom.ToString("yyyy");
				lstMailToYYYY.SelectedValue = dtTo.ToString("yyyy");
				lstMailFromMM.SelectedValue = dtFrom.ToString("MM");
				lstMailToMM.SelectedValue = dtTo.ToString("MM");
				lstMailFromDD.SelectedValue = dtFrom.ToString("dd");
				lstMailToDD.SelectedValue = dtTo.ToString("dd");
				lstMailFromHH.SelectedValue = dtFrom.ToString("HH");
				lstMailToHH.SelectedValue = dtTo.ToString("HH");

				chkWithBatchMail.Checked = true;

				pnlDtl.Visible = true;
				pnlMailBox.Visible = true;
				pnlExtInfo.Visible = true;
				grdMailBox.PageIndex = 0;
				grdMailBox.DataSourceID = "dsMailBox";
			}
		}
	}

	protected void btnCreateMail_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlMail.Visible = true;
				pnlExtInfo.Visible = true;
				btnTransMail.Enabled = true;
				lstMailTemplate.DataSourceID = "dsDMMail";
				lstMailTemplate.DataBind();
				lstMailTemplate.Items.Insert(0,new ListItem("",""));
				lstMailTemplate.DataSourceID = "";
				lstMailTemplate.SelectedIndex = 0;
				txtMailTitle.Text = "";
				txtServicePoint.Text = "0";
				txtMailAvaHour.Text = "0";
				txtPointTransferAvaHour.Text = "0";
				lblAttention.Text = "メール有効時間/Ｐ振替後有効時間に０を設定すると無期限になります。";
				txtMailDocText.Visible = false;
				txtMailDocHtml.Visible = false;
			}
		}
	}

	protected void btnProductBuy_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlProductBuy.Visible = true;
				pnlExtInfo.Visible = true;
				grdProductBuy.DataSourceID = "dsProductBuy";
			}
		}
	}

	protected void btnFriends_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlFriends.Visible = true;
				pnlManFriends.Visible = true;
				pnlCastFriends.Visible = true;
				pnlExtInfo.Visible = true;
				grdManFriends.DataSourceID = "dsManFriends";
				grdCastFriends.DataSourceID = "dsCastFriends";
			}
		}
	}
	protected void btnUserDefPointHistory_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlUserDefPointHistory.Visible = true;
				pnlExtInfo.Visible = true;
				grdUserDefPointHistory.DataSourceID = "dsUserDefPointHistory";
			}
		}
	}

	protected void btnModifyMailHis_Click(object sender,EventArgs e) {
		if (IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlModifyMailHis.Visible = true;
				pnlExtInfo.Visible = true;
				grdModifyMailHis.DataSourceID = "dsModifyMailHis";
				grdNgMailHistory.DataSourceID = "dsNgMailHistory";
				grdModifyTelHis.DataSourceID = "dsModifyTelHis";
				grdModifyLoginPassword.DataSourceID = "dsModifyLoginPassword";
			}
		}
	}

	protected void btnFanClubFee_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			CloseExtInfo();
			GetManData();
			if (this.IsExistManData()) {
				pnlDtl.Visible = true;
				pnlExtInfo.Visible = true;
				pnlFanClubFee.Visible = true;
				grdFanClubFee.DataSourceID = "dsFanClubFee";
			}
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnClose_Ext_Click(object sender,EventArgs e) {
		CloseExtInfo();
	}

	protected void btnCloseManFriends_Click(object sender,EventArgs e) {
		grdManFriends.DataSourceID = string.Empty;
		pnlManFriends.Visible = false;
	}

	protected void btnCloseCastFriends_Click(object sender,EventArgs e) {
		grdCastFriends.DataSourceID = string.Empty;
		pnlCastFriends.Visible = false;
	}

	protected void btnCloseModifyMailHis_Click(object sender,EventArgs e) {
		grdModifyMailHis.DataSourceID = string.Empty;
		grdNgMailHistory.DataSourceID = string.Empty;
		grdModifyTelHis.DataSourceID = string.Empty;
		grdModifyLoginPassword.DataSourceID = string.Empty;
		pnlModifyMailHis.Visible = false;
	}

	protected void btnSeekTalkHis_Click(object sender,EventArgs e) {
		this.grdWebUsedLogSummary.DataBind();
		this.grdTalkHistorySummary.DataBind();
		this.grdWebUsedLog.DataBind();
		this.grdUsedLogMonitor.DataBind();
		this.grdUsedLogMonitorSummary.DataBind();
		this.grdTalkHistory.DataBind();
	}

	protected void btnSeekFavorit_Click(object sender,EventArgs e) {
		grdFavorit.DataBind();
	}

	protected void btnSeekBbs_Click(object sender,EventArgs e) {
		grdBbs.DataBind();
	}

	protected void btnSeekLikeMe_Click(object sender,EventArgs e) {
		grdLikeMe.DataBind();
	}

	protected void btnSeekRefuse_Click(object sender,EventArgs e) {
		grdRefuse.DataBind();
	}

	protected void btnSeekRefuseMe_Click(object sender,EventArgs e) {
		grdRefuseMe.DataBind();
	}

	protected void btnSeekMarking_Click(object sender,EventArgs e) {
		grdMarking.DataBind();
	}

	protected void btnSeekMailBox_Click(object sender,EventArgs e) {
		if (IsValid) {
			grdMailBox.PageIndex = 0;
			grdMailBox.DataBind();
		}
	}

	protected void btnSeekProductBuy_Click(object sender,EventArgs e) {
		grdProductBuy.DataBind();
	}

	protected void btnSeekManFriends_Click(object sender,EventArgs e) {
		this.grdManFriends.DataBind();
	}

	protected void btnSeekCastFriends_Click(object sender,EventArgs e) {
		this.grdCastFriends.DataBind();
	}

	protected void btnSeekUserDefPointHis_Click(object sender,EventArgs e) {
		grdUserDefPointHistory.DataBind();
	}

	protected void btnReceipt_Click(object sender,EventArgs e) {
		int iAmt = 0;
		int.TryParse(txtReceiptAmt.Text,out iAmt);

		string sSettleType = "";

		if (rdoMobileCash.Checked) {
			sSettleType = ViCommConst.SETTLE_MOBILE_CASH;
		} else {
			sSettleType = ViCommConst.SETTLE_CASH;
		}
		if (IsValid) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("RECEIPT_MONEY");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["SITE_CD"]));
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
				db.ProcedureInParm("PRECEIPT_AMT",DbSession.DbType.NUMBER,iAmt);
				db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,sSettleType);
				db.ProcedureInParm("PFAX_RECEIPT_FLAG",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PPERSONAL_ID",DbSession.DbType.VARCHAR2,this.txtModifyUserNm.Text.TrimEnd());
				db.ProcedureInParm("PREMARKS1",DbSession.DbType.VARCHAR2,txtRemarks1.Text);
				db.ProcedureInParm("PREMARKS2",DbSession.DbType.VARCHAR2,txtRemarks2.Text);
				db.ProcedureInParm("PREMARKS3",DbSession.DbType.VARCHAR2,txtRemarks3.Text);
				db.ProcedureInParm("PURGE_RESTART_DAY",DbSession.DbType.VARCHAR2,"");
				db.ProcedureInParm("PRECEIPT_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.SETTLE_CASH);
				db.ProcedureInParm("PUSER_REGIST_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.REGIST_BEFORE);
				db.ProcedureInParm("PMODIFY_USER_NM",DbSession.DbType.VARCHAR2,txtModifyUserNm.Text);
				db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(iBridUtil.GetStringValue(ViewState["REVISION_NO"])));
				db.ProcedureInParm("PREVISION_NO_MAN",DbSession.DbType.NUMBER,decimal.Parse(iBridUtil.GetStringValue(ViewState["REVISION_NO_MAN"])));
				db.ProcedureInParm("PREVISION_NO_URGE",DbSession.DbType.NUMBER,decimal.Parse(iBridUtil.GetStringValue(ViewState["REVISION_NO_URGE"])));
				db.ProcedureOutParm("PNEW_BAL_POINT",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PNEW_LIMIT_POINT",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PADDITION_POINT",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PRELEASE_BLACK_FLAG",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				loginId = txtLoginId.Text;
				siteCd = lstSiteCd.SelectedValue;
			}
			DataBind();
		}
	}

	/// <summary>
	/// 即時決済ボタンクリック
	/// </summary>
	/// <param name="sender">イベント発生元</param>
	/// <param name="e">イベント引数</param>
	protected void btnSettleInstant_Click(object sender,EventArgs e) {
		int billAmt = int.Parse(ViewState["BILL_AMT"].ToString());
		string sSid = "";
		string siteCd = ViewState["SITE_CD"].ToString();
		string userSeq = ViewState["USER_SEQ"].ToString();
		string emailAddr = ViewState["EMAIL_ADDR"].ToString();

		using (SettleLog oLog = new SettleLog()) {
			oLog.LogSettleRequest(
				siteCd,
				userSeq,
				ViCommConst.SETTLE_CORP_BIGSUN,
				ViCommConst.SETTLE_CREDIT,
				ViCommConst.SETTLE_STAT_SETTLE_NOW,
				billAmt,
				0,
				"",
				out sSid);
		}
		string sSettleUrl = "";

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			if (oSiteSettle.GetOne(siteCd,ViCommConst.SETTLE_CORP_BIGSUN,ViCommConst.SETTLE_CREDIT)) {
				sSettleUrl = string.Format(oSiteSettle.continueSettleUrl,oSiteSettle.cpIdNo,emailAddr,userSeq,sSid,billAmt);
			}
		}

		if (TransQuick(sSettleUrl)) {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,userSeq,billAmt,0,"0");
			}
			// 結果OKの表示
			lblSettleResult.Text = "決済OKです。";
		} else {
			using (SettleLog oLog = new SettleLog()) {
				oLog.LogSettleResult(sSid,userSeq,billAmt,0,"9");
			}
			// 結果NGの表示
			lblSettleResult.Text = "決済NGです。";
		}
	}

	protected void btnUserHistory_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			Label lblLoginId = dvwUserManCharacter.FindControl("lblLoginId") as Label;
			string sLoginId = string.Empty;
			if (lblLoginId != null) {
				sLoginId = lblLoginId.Text.TrimEnd();
			}
			Response.Redirect(string.Format("~/man/ManHistoryView.aspx?site={0}&loginid={1}",ViewState["SiteCd"].ToString(),sLoginId));
		}
	}

	protected void btnTweet_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			Label lblLoginId = dvwUserManCharacter.FindControl("lblLoginId") as Label;
			string sLoginId = string.Empty;
			if (lblLoginId != null) {
				sLoginId = lblLoginId.Text.TrimEnd();
			}
			Response.Redirect(string.Format("~/Extension/Pwild/ManTweetList.aspx?site={0}&loginid={1}",ViewState["SiteCd"].ToString(),sLoginId));
		}
	}

	private bool TransQuick(string pUrl) {
		try {
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 20000;
			WebResponse res = req.GetResponse();

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st)) {
				string sRes = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sRes.Equals("Success_order");
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransQuick",pUrl);
			return false;
		}
	}

	protected void btnDeleteManPic_Click(object sender,EventArgs e) {
		DeletePicture();
	}

	private void DeletePicture() {
		string sPicSeq = "";

		sPicSeq = iBridUtil.GetStringValue(ViewState["PIC_SEQ"]);

		if (!sPicSeq.Equals("")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("USER_MAN_PIC_MAINTE");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["SITE_CD"]));
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
				db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,sPicSeq);
				db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
				db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
				db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}

			string sWebPhisicalDir = "";
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			Label lblLoginId = ((Label)dvwUserManCharacter.FindControl("lblLoginId"));

			string sFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\man\\" + lblLoginId.Text.PadLeft(15,'0') + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (System.IO.File.Exists(sFullPath)) {
					System.IO.File.Delete(sFullPath);
				}


				sFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\man\\" + lblLoginId.Text.PadLeft(15,'0') + ViCommConst.PIC_FOODER_SMALL;

				if (System.IO.File.Exists(sFullPath)) {
					System.IO.File.Delete(sFullPath);
				}
			}
		}
		loginId = txtLoginId.Text;
		siteCd = lstSiteCd.SelectedValue;
		DataBind();
	}
	protected void dsUserManCharacter_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		ViewState["SITE_CD"] = "";
		ViewState["USER_SEQ"] = "";
		ViewState["USER_CHAR_NO"] = "";
		ViewState["LOGIN_ID"] = "";
		ViewState["REPORT_DAY_FROM"] = "";
		ViewState["REPORT_DAY_TO"] = "";
		ViewState["REVISION_NO"] = "";
		ViewState["REVISION_NO_MAN"] = "";
		ViewState["REVISION_NO_URGE"] = "";
		ViewState["BILL_AMT"] = "";
		ViewState["CREDIT_MEASURED_RATE_TARGET"] = "";
		ViewState["EMAIL_ADDR"] = "";

		lblUserSeq.Text = "";

		DataSet ds = (DataSet)e.ReturnValue;
		if (ds == null) {
			return;
		}
		if (ds.Tables[0].Rows.Count == 0) {
			lblNotFound.Visible = true;
			pnlDtl.Visible = false;
			grdActivated.DataSourceID = "";
		} else {
			lblNotFound.Visible = false;
			if (ds.Tables[0].Rows.Count > 1) {
				lblManyUser.Visible = true;
			}
			DataRow dr = ds.Tables[0].Rows[0];
			ViewState["SITE_CD"] = dr["SITE_CD"].ToString();
			ViewState["USER_SEQ"] = dr["USER_SEQ"].ToString();
			ViewState["USER_CHAR_NO"] = dr["USER_CHAR_NO"].ToString();
			ViewState["LOGIN_ID"] = dr["LOGIN_ID"].ToString();

			if ((txtReportDayFrom.Text.Equals("")) && (txtReportDayTo.Text.Equals(""))) {
				txtReportDayFrom.Text = DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd");
				txtReportDayTo.Text = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");
			}
			if ((!txtReportDayFrom.Text.Equals("")) || (!txtReportDayTo.Text.Equals(""))) {
				if (txtReportDayFrom.Text.Equals("")) {
					txtReportDayFrom.Text = txtReportDayTo.Text;
				} else if (txtReportDayTo.Text.Equals("")) {
					txtReportDayTo.Text = txtReportDayFrom.Text;
				}
			}
			if (chkReportDayEnable.Checked) {
				ViewState["REPORT_DAY_FROM"] = txtReportDayFrom.Text.ToString();
				ViewState["REPORT_DAY_TO"] = txtReportDayTo.Text.ToString();
			} else {
				ViewState["REPORT_DAY_FROM"] = "1900/01/01";
				ViewState["REPORT_DAY_TO"] = "2100/01/01";
			}

			lblUserSeq.Text = ViewState["USER_SEQ"].ToString();
			txtRemarks1.Text = dr["REMARKS1"].ToString();
			txtRemarks2.Text = dr["REMARKS2"].ToString();
			txtRemarks3.Text = dr["REMARKS3"].ToString();
			txtRemarks4.Text = dr["REMARKS4"].ToString() + dr["REMARKS5"].ToString() + dr["REMARKS6"].ToString() + dr["REMARKS7"].ToString() + dr["REMARKS8"].ToString() + dr["REMARKS9"].ToString() + dr["REMARKS10"].ToString();
			txtReceiptAmt.Text = "";
			txtModifyUserNm.Text = "";
			ViewState["REVISION_NO"] = dr["REVISION_NO"].ToString();
			ViewState["REVISION_NO_MAN"] = dr["REVISION_NO_MAN"].ToString();
			ViewState["REVISION_NO_URGE"] = dr["REVISION_NO_URGE"].ToString();

			using (ManageCompany oCompany = new ManageCompany()) {
				pnlActivated.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE);
			}
			if (pnlActivated.Visible) {
				grdActivated.DataSourceID = "dsActivated";
				grdActivated.DataBind();
			}

			ViewState["BILL_AMT"] = dr["BILL_AMT"];
			ViewState["CREDIT_MEASURED_RATE_TARGET"] = dr["CREDIT_MEASURED_RATE_TARGET"];
			ViewState["EMAIL_ADDR"] = dr["EMAIL_ADDR"];
			string sWithdrwalseq = iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]);
			string sCheckedSeq = iBridUtil.GetStringValue(Request.QueryString["checkedseq"]);

			// 退会申請履歴のユーザ名からの遷移（友達紹介による登録者のみ）
			if (!string.IsNullOrEmpty(sCheckedSeq)) {
				// 退会申請のチェック状態を変更
				string sStatus = string.Empty;
				Withdrawal oWithdrawal = new Withdrawal();
				oWithdrawal.AcceptWithdrawalCheck(
					iBridUtil.GetStringValue(ViewState["SITE_CD"]),
					iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
					sCheckedSeq,
					ViCommConst.FLAG_ON_STR,
					out sStatus
				);

				// 利用履歴を表示
				dispUsedLog();

				// 退会申請関連も表示
				sWithdrwalseq = sCheckedSeq;
			}

			// 退会申請履歴からの遷移
			if (!sWithdrwalseq.Equals(string.Empty)) {
				using (Withdrawal oWithdrawal = new Withdrawal()) {
					if (oWithdrawal.GetOne(sWithdrwalseq,ViCommConst.MAN)) {
						pnlWithdrawal.Visible = true;
						lblWithdrawalSeq.Text = sWithdrwalseq;
						lblWithdrawalStatus.Text = oWithdrawal.withdrawalStatusNm;
						lblWithdrawalDate.Text = oWithdrawal.updateDate;
						lblWithdrawalReasonCd.Text = oWithdrawal.withdrawalReasonNm;
						txtWithdrawalReasonDoc.Text = oWithdrawal.withdrawalReasonDoc;
						txtWithdrawalRemarks.Text = oWithdrawal.remarks;
						if (oWithdrawal.withdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE) || dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_RESIGNED)) {
							btnAcceptWithdrawal.Visible = false;
						}

					} else {
						pnlWithdrawal.Visible = false;
					}
				}

				// 退会履歴を表示
				pnlWithdrawalHistoryDetail.Visible = false;
				pnlWithdrawalHistoryList.Visible = true;
				grdWithdrawalHistoryList.DataSourceID = "dsWithdrawalHistoryList";
				grdWithdrawalHistoryList.DataBind();
			} else {
				pnlWithdrawal.Visible = false;

				// 退会履歴を非表示
				pnlWithdrawalHistoryDetail.Visible = false;
				pnlWithdrawalHistoryList.Visible = false;
				grdWithdrawalHistoryList.DataSourceID = string.Empty;
			}

			string sBingoEntrySeq = iBridUtil.GetStringValue(Request.QueryString["bingoentryseq"]);
			if (!sBingoEntrySeq.Equals(string.Empty)) {
				using (BingoEntry oBingoEntry = new BingoEntry()) {
					if (oBingoEntry.GetOne(sBingoEntrySeq)) {
						pnlBingoEntry.Visible = true;
						lblBingoTermSeq.Text = oBingoEntry.bingoTermSeq.ToString();
						lblBingoEntrySeq.Text = sBingoEntrySeq;
						lblBingoCardNo.Text = oBingoEntry.bingoCardNo.ToString();
						lblBingoCharangeCount.Text = oBingoEntry.bingoCharangeCount.ToString();
						lblBingoCompliateJackpotBefore.Text = oBingoEntry.bingoCompliateJackpot.ToString();
						lblBingoCompliateJackpotAfter.Text = (oBingoEntry.bingoCompliateJackpot - oBingoEntry.bingoElectionAmt).ToString();
						lblBingoElectionPoint.Text = oBingoEntry.bingoElectionPoint.ToString();
						lblBingoElectionAmt.Text = oBingoEntry.bingoElectionAmt.ToString();
						lblBingoEntryDate.Text = oBingoEntry.bingoEntryDate;
						lblBingoApplicationDate.Text = oBingoEntry.bingoApplicationDate;
						if (oBingoEntry.bingoPointPaymentFlag == ViCommConst.FLAG_ON) {
							rdoBingoPointPayment.Checked = true;
							rdoBingoNotPointPayment.Checked = false;
						} else {
							rdoBingoPointPayment.Checked = false;
							rdoBingoNotPointPayment.Checked = true;
						}

					} else {
						pnlBingoEntry.Visible = false;
					}
				}
			} else {
				pnlBingoEntry.Visible = false;
			}
		}
		bool bExist = false;
		using (Site oSite = new Site()) {
			bExist = oSite.IsAvailablePayAfterFlag(lstSiteCd.SelectedValue);
			if (bExist == false) {
				bExist = oSite.IsUsedChargeSettleFlag(lstSiteCd.SelectedValue);
			}
		}
		if (bExist) {
			dvwUserManCharacter.Fields[6].Visible = true;				//後払限度Pt. 
			dvwUserManCharacter.Fields[7].Visible = true;				//入金期日 
			dvwUserManCharacter.Fields[8].Visible = true;				//請求額 
			pnlUrge.Visible = true;
			dvwUserManCharacter.Fields[19].Visible = true;				//ﾌﾞﾗｯｸ登録日 
		} else {
			dvwUserManCharacter.Fields[6].Visible = false;				//後払限度Pt. 
			dvwUserManCharacter.Fields[7].Visible = false;				//入金期日 
			dvwUserManCharacter.Fields[8].Visible = false;				//請求額 
			pnlUrge.Visible = false;
			dvwUserManCharacter.Fields[19].Visible = false;				//ﾌﾞﾗｯｸ登録日 
		}
		bool bIsAvairableRichino = IsAvailableRichno();
		dvwUserManCharacter.Fields[25].Visible = bIsAvairableRichino;	//ﾘｯﾁｰﾉﾗﾝｸ 
		dvwUserManCharacter.Fields[26].Visible = bIsAvairableRichino;	//ﾘｯﾁｰﾉﾗﾝｸ更新日 
	}

	protected bool IsAvailableRichno() {
		bool bAvailable;

		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_RICHINO)) {
				bAvailable = true;
			} else {
				bAvailable = false;
			}
		}
		return bAvailable;
	}


	protected void dsUrge_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet ds = (DataSet)e.ReturnValue;
		if (ds == null) {
			return;
		}
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		grdWebUsedLog.PageSize = 30;
		grdTalkHistory.PageSize = 30;

		grdWebUsedLog.DataSourceID = "";
		grdReceipt.DataSourceID = "";
		grdSettle.DataSourceID = "";
		grdTalkHistory.DataSourceID = "";
		grdFavorit.DataSourceID = "";
		grdLikeMe.DataSourceID = "";
		grdRefuse.DataSourceID = "";
		grdRefuseMe.DataSourceID = "";
		grdMarking.DataSourceID = "";
		grdMailBox.DataSourceID = "";
		grdActivated.DataSourceID = "";
		grdProductBuy.DataSourceID = "";
		grdManFriends.DataSourceID = string.Empty;
		grdCastFriends.DataSourceID = string.Empty;
		grdModifyMailHis.DataSourceID = string.Empty;
		grdNgMailHistory.DataSourceID = string.Empty;
		grdModifyTelHis.DataSourceID = string.Empty;
		grdModifyLoginPassword.DataSourceID = string.Empty;
		grdUserDefPointHistory.DataSourceID = string.Empty;
		grdUsedLogMonitor.DataSourceID = string.Empty;
		grdFanClubFee.DataSourceID = string.Empty;
		
		CloseExtInfo();

		if (!loginId.Equals("")) {
			dvwUserManCharacter.DataSourceID = "dsUserManCharacter";
			lstSiteCd.SelectedValue = siteCd;
			txtLoginId.Text = loginId;
			txtUserSeq.Text = "";
			pnlDtl.Visible = true;
		} else if (!string.IsNullOrEmpty(userSeq)) {
			dvwUserManCharacter.DataSourceID = "dsUserManCharacter";
			lstSiteCd.SelectedValue = siteCd;
			txtLoginId.Text = "";
			txtUserSeq.Text = userSeq;
			pnlDtl.Visible = true;
		} else {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			txtLoginId.Text = "";
			txtUserSeq.Text = "";
			txtEMailAddr.Text = "";
			dvwUserManCharacter.DataSourceID = "";
			pnlDtl.Visible = false;
			pnlWithdrawal.Visible = false;
			pnlBingoEntry.Visible = false;
		}
		lblNotFound.Visible = false;
		lblManyUser.Visible = false;

		txtReportDayFrom.Text = "";
		txtReportDayTo.Text = "";


		using (ManageCompany oCompany = new ManageCompany()) {
			pnlActivated.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE);
			this.lnkProductBuy.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_PRODUCT_BUY);
			this.DisablePinEdit = oCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
		}
	}

	protected void Page_Prerender() {
		this.SetProfile();
		this.SetGameCharacter();
	}

	protected void vrdLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			args.IsValid = true;
			if (txtEMailAddr.Text.Equals(string.Empty) && txtLoginId.Text.Equals(string.Empty) && txtUserSeq.Text.Equals(string.Empty)) {
				args.IsValid = false;
			}
		}
	}

	private void SetProfile() {
		DataRowView drv = (DataRowView)(dvwUserManCharacter.DataItem);
		if (drv != null) {
			int count = int.Parse(drv.Row["COUNT"].ToString());
			for (int i = 0;i < count;i++) {
				Label lblUserManAttrNm = (Label)plcHolder.FindControl(string.Format("lblUserManAttrNm{0}",i)) as Label;
				lblUserManAttrNm.Text = drv.Row[string.Format("MAN_ATTR_TYPE_NM{0}",i)].ToString();
				Label lblUserManAttrValue = (Label)plcHolder.FindControl(string.Format("lblUserManAttrSeq{0}",i)) as Label;
				lblUserManAttrValue.Text = drv.Row[string.Format("DISPLAY_VALUE{0}",i)].ToString();
			}
			for (int i = count;i < ViCommConst.MAX_ATTR_COUNT;i++) {
				TableRow row;
				row = (TableRow)plcHolder.FindControl(string.Format("rowAttr{0}",i)) as TableRow;
				row.Visible = false;
			}

			if (this.rowManWaitingComment.Visible) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("GET_MAN_WAITING_COMMENT");
					db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
					db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
					db.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
					db.ProcedureOutParm("pREC_SEQ", DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("pWAITING_COMMENT", DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();

					this.lblManWaitingComment.Text = db.GetStringValue("pWAITING_COMMENT");
				}
			}
			imgManProfilePic.ImageUrl = ConfigurationManager.AppSettings["Root"] + "/" + drv.Row["PHOTO_IMG_PATH"].ToString() + "?" + DateTime.Now.ToString("yyyyMMddHHmmss");
			if (iBridUtil.GetStringValue(drv.Row["PIC_SEQ"]).Equals("")) {
				btnDeleteManPic.Enabled = false;
			} else {
				btnDeleteManPic.Enabled = true;
			}
			ViewState["PIC_SEQ"] = iBridUtil.GetStringValue(drv.Row["PIC_SEQ"]);
			ViewState["FRIEND_INTRO_CD"] = iBridUtil.GetStringValue(drv.Row["FRIEND_INTRO_CD"]);
		}

		AddUserDefPointBtnRefresh();
	}

	private bool IsExistManData() {
		bool ret = false;
		if (this.dvwUserManCharacter.DataItem != null) {
			ret = true;
		}
		return ret;
	}

	private void CloseExtInfo() {
		txtTalkPartnerId.Text = "";
		txtAddExp.Text = string.Empty;
		txtAddGamePoint.Text = string.Empty;
		txtGameItemGiveCount.Text = string.Empty;

		grdReceipt.DataSourceID = "";
		grdSettle.DataSourceID = "";
		grdTalkHistory.DataSourceID = "";
		grdFavorit.DataSourceID = "";
		grdLikeMe.DataSourceID = "";
		grdRefuse.DataSourceID = "";
		grdRefuseMe.DataSourceID = "";
		grdMarking.DataSourceID = "";
		grdMailBox.DataSourceID = "";
		grdProductBuy.DataSourceID = "";
		grdManFriends.DataSourceID = string.Empty;
		grdCastFriends.DataSourceID = string.Empty;
		grdBbs.DataSourceID = "";
		grdModifyMailHis.DataSourceID = string.Empty;
		grdNgMailHistory.DataSourceID = string.Empty;
		grdModifyTelHis.DataSourceID = string.Empty;
		grdModifyLoginPassword.DataSourceID = string.Empty;
		grdWebUsedLog.DataSourceID = string.Empty;
		grdUserDefPointHistory.DataSourceID = string.Empty;
		grdTalkHistorySummary.DataSourceID = string.Empty;
		grdWebUsedLogSummary.DataSourceID = string.Empty;
		grdUsedLogMonitorSummary.DataSourceID = string.Empty;
		grdPossessionGameItem.DataSourceID = string.Empty;
		grdFanClubFee.DataSourceID = string.Empty;

		pnlReceiptHis.Visible = false;
		pnlSettleHis.Visible = false;
		pnlUsedHis.Visible = false;
		pnlTalkHis.Visible = false;
		pnlUsedLogMonitor.Visible = false;
		pnlFavorit.Visible = false;
		pnlLikeMe.Visible = false;
		pnlRefuse.Visible = false;
		pnlRefuseMe.Visible = false;
		pnlMarking.Visible = false;
		pnlMailBox.Visible = false;
		pnlMail.Visible = false;
		pnlExtInfo.Visible = false;
		pnlProductBuy.Visible = false;
		pnlFriends.Visible = false;
		pnlManFriends.Visible = false;
		pnlCastFriends.Visible = false;
		pnlBbs.Visible = false;
		pnlModifyMailHis.Visible = false;
		pnlUserDefPointHistory.Visible = false;
		pnlPossessionGameItem.Visible = false;
		pnlFanClubFee.Visible = false;

		// 退会履歴を初期化
		ClearWithdrawalHistory();
	}

	private void GetManData() {
		pnlDtl.Visible = true;
		dvwUserManCharacter.DataSourceID = "dsUserManCharacter";
		DataBind();
	}

	protected string GetLikeMeMark(object pUserSeq,object pUserCharNo,object pPartnerUserSeq,object pPartnerUserCharNo) {
		string sValue = string.Empty;
		using (Favorit oFavorit = new Favorit()) {
			if (oFavorit.GetOne(iBridUtil.GetStringValue(ViewState["SiteCd"]),iBridUtil.GetStringValue(pPartnerUserSeq),iBridUtil.GetStringValue(pPartnerUserCharNo),iBridUtil.GetStringValue(pUserSeq),iBridUtil.GetStringValue(pUserCharNo))) {
				sValue = "お気に入られ";
			}
		}
		return sValue;
	}

	protected string GetRefuseMeMark(object pUserSeq,object pUserCharNo,object pPartnerUserSeq,object pPartnerUserCharNo) {
		string sValue = string.Empty;
		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(iBridUtil.GetStringValue(ViewState["SiteCd"]),iBridUtil.GetStringValue(pPartnerUserSeq),iBridUtil.GetStringValue(pPartnerUserCharNo),iBridUtil.GetStringValue(pUserSeq),iBridUtil.GetStringValue(pUserCharNo))) {
				sValue = "拒否されている";
			}
		}
		return sValue;
	}

	protected string GetSysImage(object pOnlienStatus) {
		return this.GetSysImage(pOnlienStatus,0);
	}

	protected string GetSysImage(object pOnlienStatus,object pDummyTalkFlag) {
		int iOnlineStatus = int.Parse(pOnlienStatus.ToString());

		switch (iOnlineStatus) {
			case ViCommConst.USER_LOGINED:
				return "../image/sys_logined.jpg";
			case ViCommConst.USER_WAITING:
				return "../image/sys_online.jpg";
			case ViCommConst.USER_TALKING:
				int iDummyTalkFlag = int.Parse(pDummyTalkFlag.ToString());
				if (iDummyTalkFlag == ViCommConst.FLAG_ON) {
					return "../image/sys_dummy_talking.jpg";
				}
				return "../image/sys_talking.jpg";
			case ViCommConst.USER_OFFLINE:
				return "../image/sys_offline.jpg";
		}
		return "";
	}

	protected string CheckAdminLevel(object pText) {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			return pText.ToString();
		} else {
			return "***********";
		}
	}

	protected bool IsCash(object pSettleType) {
		if (pSettleType.Equals(ViCommConst.SETTLE_CASH) || pSettleType.Equals(ViCommConst.SETTLE_MOBILE_CASH)) {
			return true;
		} else {
			return false;
		}
	}

	protected Color GetReceiptForeColor(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		Color oForeColor = Color.Empty;
		if (iBridUtil.GetStringValue(oDataRowView["RECEIPT_CANCEL_FLAG"]).Equals(ViCommConst.FLAG_ON_STR)) {
			oForeColor = Color.Red;
		}

		return oForeColor;

	}
	protected bool IsReceiptCanceled(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		bool IsReceiptCanceled = false;
		if (iBridUtil.GetStringValue(oDataRowView["RECEIPT_CANCEL_FLAG"]).Equals(ViCommConst.FLAG_ON_STR)) {
			IsReceiptCanceled = true;
		}

		return IsReceiptCanceled;

	}

	protected bool IsCancelReceiptVisible(object pDataItem) {
		bool bResult = IsCancelReceiptVisibleImpl(pDataItem);

		if (!bResult) {
			this.pnlReceiptCancelMsg.Visible = true;
		}
		return bResult;
	}

	private bool IsCancelReceiptVisibleImpl(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;


		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.IsAvailableService(ViCommConst.RELEASE_SETTLEMENT_LOG) && !this.IsCash(oDataRowView["SETTLE_TYPE"])) {
				return false;
			} else {
				if (iBridUtil.GetStringValue(oDataRowView["SETTLE_TYPE"]).Equals(ViCommConst.SETTLE_POINT_AFFILIATE)) {
					return false;
				}
			}
		}

		if (iBridUtil.GetStringValue(oDataRowView["RECEIPT_SEQ"]).Equals(string.Empty)) {
			return false;
		}
		if (ViCommConst.USER_TALKING == int.Parse(iBridUtil.GetStringValue(oDataRowView["CHARACTER_ONLINE_STATUS"]))) {
			return false;
		}

		if (int.Parse(iBridUtil.GetStringValue(oDataRowView["BAL_POINT"])) < int.Parse(iBridUtil.GetStringValue(oDataRowView["EX_POINT"]))) {
			return false;
		}

		DateTime dtPresentFirst = DateTime.ParseExact(string.Format("{0}01",DateTime.Today.ToString("yyyyMM")),"yyyyMMdd",null);
		DateTime dtPresentLast = dtPresentFirst.AddMonths(1).AddMilliseconds(-1);
		DateTime dtCreateDate;
		if (DateTime.TryParse(iBridUtil.GetStringValue(oDataRowView["CREATE_DATE"]),out dtCreateDate)) {
			if (dtPresentFirst <= dtCreateDate && dtCreateDate <= dtPresentLast) {
				if (int.Parse(iBridUtil.GetStringValue(oDataRowView["MONTHLY_RECEIPT_COUNT"])) < 1) {
					return false;
				}

				if (int.Parse(iBridUtil.GetStringValue(oDataRowView["MONTHLY_RECEIPT_AMT"])) < int.Parse(iBridUtil.GetStringValue(oDataRowView["EX_POINT"]))) {
					return false;
				}
			}
		} else {
			return false;
		}

		if (int.Parse(iBridUtil.GetStringValue(oDataRowView["TOTAL_RECEIPT_COUNT"])) < 1) {
			return false;
		}

		if (int.Parse(iBridUtil.GetStringValue(oDataRowView["TOTAL_RECEIPT_AMT"])) < int.Parse(iBridUtil.GetStringValue(oDataRowView["EX_POINT"]))) {
			return false;
		}



		return true;
	}

	protected Color GetFanClubFeeForeColor(object pDataItem) {
		Color oForeColor = Color.Empty;
		DataRowView oDataRowView = (DataRowView)pDataItem;

		if (iBridUtil.GetStringValue(oDataRowView["SETTLE_CANCEL_FLAG"]).Equals(ViCommConst.FLAG_ON_STR)) {
			oForeColor = Color.Red;
		}

		return oForeColor;
	}

	protected bool IsFanClubFeeCanceled(object pDataItem) {
		bool bResult = false;
		DataRowView oDataRowView = (DataRowView)pDataItem;

		if (iBridUtil.GetStringValue(oDataRowView["SETTLE_CANCEL_FLAG"]).Equals(ViCommConst.FLAG_ON_STR)) {
			bResult = true;
		}

		return bResult;
	}

	protected string CheckHaveTermUniqueIdFlag(object pFlag) {
		if (pFlag.Equals(ViCommConst.FLAG_ON.ToString())) {
			return "○";
		} else {
			return "×";
		}
	}

	protected void lnkDelRxMail_Command(object sender,CommandEventArgs e) {
		string sMailSeq = e.CommandArgument.ToString();
		using (MailBox oMailBox = new MailBox()) {
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.RX);
		}
		DataBind();
	}

	protected void lnkDelTxMail_Command(object sender,CommandEventArgs e) {
		string sMailSeq = e.CommandArgument.ToString();
		using (MailBox oMailBox = new MailBox()) {
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.TX);
		}
		DataBind();
	}

	protected void lnkCancelReceipt_Command(object sender,CommandEventArgs e) {
		string sReceiptSeq = e.CommandArgument.ToString();
		using (Receipt oReceipt = new Receipt()) {
			oReceipt.CancelReceipt(sReceiptSeq);
		}
		DataBind();
	}
	protected void lnkRevertReceipt_Command(object sender,CommandEventArgs e) {
		string sReceiptSeq = e.CommandArgument.ToString();
		using (Receipt oReceipt = new Receipt()) {
			oReceipt.RevertCancelReceipt(sReceiptSeq);
		}
		DataBind();
	}

	protected void lnkCancelFanClubFee_Command(object sender,CommandEventArgs e) {
		string sSettleSeq = e.CommandArgument.ToString();
		using (SettleLog oSettleLog = new SettleLog()) {
			oSettleLog.CancelSettleLog(sSettleSeq);
		}
		DataBind();
	}

	protected void lnkRevertFanClubFee_Command(object sender,CommandEventArgs e) {
		string sSettleSeq = e.CommandArgument.ToString();
		using (SettleLog oSettleLog = new SettleLog()) {
			oSettleLog.RevertCancelSettleLog(sSettleSeq);
		}
		DataBind();
	}

	protected void lnkLogin_Command(object sender,CommandEventArgs e) {
		using (Site oSite = new Site()) {
			string[] sKeys = e.CommandArgument.ToString().Split(':');
			if (oSite.GetOne(sKeys[0])) {
				string sURL = string.Format("{0}/User/Start.aspx?loginid={1}&password={2}&adminflg=1&nopc=1",oSite.url,sKeys[1],sKeys[2]);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=800,resizable=no,directories=no,scrollbars=yes' , false);</script>",sURL);
				ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
			}
		}
	}

	protected string GetIntroducerLink(object sIntroducerFriendCd) {
		DataSet ds;

		using (CastCharacter oCastCharacter = new CastCharacter()) {
			ds = oCastCharacter.GetOneByFriendIntroCd(sIntroducerFriendCd.ToString());
		}
		if (ds.Tables[0].Rows.Count != 0) {
			return string.Format("../Cast/CastView.aspx?loginid={0}&return=ManView.aspx",ds.Tables[0].Rows[0]["LOGIN_ID"].ToString());
		} else {
			using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
				ds = oUserManCharacter.GetOneByFriendIntroCd(sIntroducerFriendCd.ToString());
			}
			if (ds.Tables[0].Rows.Count != 0) {
				return string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",ds.Tables[0].Rows[0]["SITE_CD"].ToString(),ds.Tables[0].Rows[0]["LOGIN_ID"].ToString());
			}
		}
		return "";
	}

	protected void lstMailTemplate_SelectedIndexChanged(object sender,EventArgs e) {
		using (MailTemplate oTemplate = new MailTemplate()) {
			if (oTemplate.GetOne(ViewState["SITE_CD"].ToString(),lstMailTemplate.SelectedValue)) {
				txtMailTitle.Text = oTemplate.mailTitle;
				if (oTemplate.IsTextMail) {
					txtMailDocText.Visible = true;
					txtMailDocText.Text = oTemplate.textDoc;
					txtMailDocHtml.Visible = false;
					txtMailDocHtml.Text = string.Empty;
				} else if (this.DisablePinEdit) {
					txtMailDocText.Visible = true;
					txtMailDocText.Text = oTemplate.htmlDoc;
					txtMailDocHtml.Visible = false;
					txtMailDocHtml.Text = string.Empty;
				} else {
					txtMailDocText.Visible = false;
					txtMailDocText.Text = string.Empty;
					txtMailDocHtml.Visible = true;
					txtMailDocHtml.Text = oTemplate.htmlDoc;
				}
				ViewState["TEXT_MAIL_FLAG"] = oTemplate.IsTextMail;
			} else {
				txtMailDocText.Visible = false;
				txtMailDocText.Text = string.Empty;
				txtMailDocHtml.Visible = false;
				txtMailDocHtml.Text = string.Empty;
			}
		}

	}

	protected void btnTransMail_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		string[] sDoc;
		int iDocCount;

		string[] sUserSeq = new string[1];
		sUserSeq[0] = lblUserSeq.Text;

		if ((bool)ViewState["TEXT_MAIL_FLAG"]) {
			SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtMailDocText.Text.Replace(Environment.NewLine,"<br/>")),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
		} else if (this.DisablePinEdit) {
			SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtMailDocText.Text),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
		} else {
			SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtMailDocHtml.Text),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_ADMIN_TO_MAN_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplate.SelectedValue);
			db.ProcedureInParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInArrayParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,1,sUserSeq);
			db.ProcedureInParm("PMAN_USER_COUNT",DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("PMAIL_AVA_HOUR",DbSession.DbType.NUMBER,int.Parse(txtMailAvaHour.Text));
			db.ProcedureInParm("PPOINT_TRANSFER_AVA_HOUR",DbSession.DbType.NUMBER,int.Parse(txtPointTransferAvaHour.Text));
			db.ProcedureInParm("PSERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtServicePoint.Text));
			db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,txtMailTitle.Text);
			db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pTEST_SEND_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pMAIL_SEND_TYPE",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("pPOINT_MAIL_OUTCOME_SEQ",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pMAIL_SERVER",DbSession.DbType.VARCHAR2,rdoMailServer.SelectedValue);
			db.ExecuteProcedure();
		}
		btnTransMail.Enabled = false;
		lblAttention.Text = "メール送信が完了しました。";

	}

	protected void dsUserManCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = !txtLoginId.Text.TrimEnd().Equals(string.Empty) ? txtLoginId.Text.TrimEnd() : txtEMailAddr.Text.TrimEnd();
		e.InputParameters[2] = ViCommConst.FLAG_ON_STR;
		e.InputParameters[3] = txtUserSeq.Text.TrimEnd();

	}

	protected void dsUsedLogMonitor_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pSexCd"] = ViCommConst.MAN;
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters["pUserCharNo"] = ViCommConst.MAIN_CHAR_NO;
		e.InputParameters["pReportDayFrom"] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_FROM"]);
		e.InputParameters["pReportDayTo"] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_TO"]);
		e.InputParameters["pPartnerLoginId"] = txtTalkPartnerId.Text.TrimEnd();
	}

	protected void dsWebUsedLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters["pReportDayFrom"] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_FROM"]);
		e.InputParameters["pReportDayTo"] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_TO"]);
		e.InputParameters["pPartnerLoginId"] = txtTalkPartnerId.Text.TrimEnd();
		e.InputParameters["pPartnerHandleNm"] = string.Empty;
	}

	protected void dsReceipt_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		this.pnlReceiptCancelMsg.Visible = false;

		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_FROM"]);
		e.InputParameters[3] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_TO"]);
		e.InputParameters[4] = null;
		e.InputParameters[5] = "";
		e.InputParameters[6] = "";
		e.InputParameters[7] = "";

		//ポータルサイトの場合、サイト名を表示し、全ての入金履歴を表示する
		using (ManageCompany oCompany = new ManageCompany()) {
			bool bPortalFlag = oCompany.IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE);
			grdReceipt.Columns[0].Visible = bPortalFlag;
			if (bPortalFlag) {
				e.InputParameters[0] = "";
			}
		}

	}

	protected void dsSettle_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_FROM"]);
		e.InputParameters[3] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_TO"]);

		//ポータルサイトの場合、サイト名を表示し、全ての入金履歴を表示する
		using (ManageCompany oCompany = new ManageCompany()) {
			bool bPortalFlag = oCompany.IsAvailableService(ViCommConst.RELEASE_PORTAL_SITE);
			grdSettle.Columns[0].Visible = bPortalFlag;
			if (bPortalFlag) {
				e.InputParameters[0] = "";
			}
		}
	}

	protected void dsUsedLogTalk_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pSexCd"] = ViCommConst.MAN;
		e.InputParameters["pLoginId"] = iBridUtil.GetStringValue(ViewState["LOGIN_ID"]);
		e.InputParameters["pUserCharNo"] = ViCommConst.MAIN_CHAR_NO;
		e.InputParameters["pPartnerLoginId"] = txtTalkPartnerId.Text.Trim();
		e.InputParameters["pPartnerUserCharNo"] = string.Empty;
		e.InputParameters["pTel"] = "";
		e.InputParameters["pReportDayFrom"] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_FROM"]);
		e.InputParameters["pReportDayTo"] = iBridUtil.GetStringValue(ViewState["REPORT_DAY_TO"]);
		e.InputParameters["pChargeType"] = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
															ViCommConst.CHARGE_TALK_WSHOT,
															ViCommConst.CHARGE_TALK_PUBLIC,
															ViCommConst.CHARGE_TALK_VOICE_WSHOT,
															ViCommConst.CHARGE_TALK_VOICE_PUBLIC,
															ViCommConst.CHARGE_GPF_TALK_VOICE,
															ViCommConst.CHARGE_CAST_TALK_PUBLIC,
															ViCommConst.CHARGE_CAST_TALK_WSHOT,
															ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT,
															ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC);

		e.InputParameters["pReportTimeFrom"] = "";
		e.InputParameters["pReportTimeTo"] = "";
		e.InputParameters["pCallResult"] = "";
	}

	protected void dsFavorit_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = ViCommConst.MAIN_CHAR_NO;
	}

	protected void dsBbsLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = ViCommConst.MAN;
		e.InputParameters[2] = string.Empty;
		e.InputParameters[3] = string.Empty;
		e.InputParameters[4] = string.Empty;
		e.InputParameters[5] = string.Empty;
		e.InputParameters[6] = iBridUtil.GetStringValue(ViewState["LOGIN_ID"]);
		e.InputParameters[7] = ViCommConst.MAIN_CHAR_NO;
	}

	protected void dsLikeMe_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = ViCommConst.MAIN_CHAR_NO;
	}

	protected void dsRefuse_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = ViCommConst.MAIN_CHAR_NO;
	}

	protected void dsRefuseMe_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = ViCommConst.MAIN_CHAR_NO;
	}

	protected void dsMarking_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = ViCommConst.MAIN_CHAR_NO;
	}

	protected void dsMailBox_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sAttachedType = string.Empty;
		string sTxRxType = string.Empty;

		foreach (ListItem oItem in this.chkAttachedType.Items) {
			if (!oItem.Selected)
				continue;

			if (!sAttachedType.Equals(string.Empty))
				sAttachedType += ",";
			sAttachedType += oItem.Value;
		}

		if (rdoRxType.Checked) {
			sTxRxType = ViCommConst.RX;
		} else if (rdoTxType.Checked) {
			sTxRxType = ViCommConst.TX;
		} else {
			sTxRxType = ViCommConst.TXRX;
		}
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = ViCommConst.MAIN_CHAR_NO;
		e.InputParameters[3] = txtMailPartnerLoginId.Text.TrimEnd();
		e.InputParameters[4] = txtMailPartnerCharNo.Text.TrimEnd();
		e.InputParameters[5] = ViCommConst.MAN;
		e.InputParameters[6] = string.Format("{0}/{1}/{2} {3}",lstMailFromYYYY.SelectedValue,lstMailFromMM.SelectedValue,lstMailFromDD.SelectedValue,lstMailFromHH.SelectedValue);
		e.InputParameters[7] = string.Format("{0}/{1}/{2} {3}",lstMailToYYYY.SelectedValue,lstMailToMM.SelectedValue,lstMailToDD.SelectedValue,lstMailToHH.SelectedValue);
		e.InputParameters[8] = sAttachedType;
		e.InputParameters[9] = chkWithBatchMail.Checked;
		e.InputParameters[10] = sTxRxType;
	}

	protected void dsUrge_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
	}

	protected void dsActivated_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]);
	}

	protected void dsProductBuy_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters["pUserCharNo"] = iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]);
	}

	protected void dsCastFriends_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pFriendIntroCd"] = iBridUtil.GetStringValue(ViewState["FRIEND_INTRO_CD"]);
	}

	protected void dsManFriends_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pFriendIntroCd"] = iBridUtil.GetStringValue(ViewState["FRIEND_INTRO_CD"]);
	}

	protected void dsUserDefPointHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
	}

	protected void dsModifyMailHis_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
	}

	protected void dsNgMailHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
	}

	protected void dsModifyTelHis_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
	}

	protected void dsModifyLoginPassword_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters["pUserCharNo"] = iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]);
	}

	protected void dsFanClubFee_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pReportDayFrom"] = string.Empty;
		e.InputParameters["pReportDayTo"] = string.Empty;
		e.InputParameters["pSettleType"] = string.Empty;
		e.InputParameters["pLoginId"] = iBridUtil.GetStringValue(ViewState["LOGIN_ID"]);
		e.InputParameters["pTel"] = string.Empty;
		e.InputParameters["pSettleStatus"] = "0";
		e.InputParameters["pAdCd"] = string.Empty;
		e.InputParameters["pSettleCompanyCd"] = string.Empty;
        e.InputParameters["pAnswerDayFrom"] = string.Empty;
		e.InputParameters["pAnswerDayTo"] = string.Empty;
		e.InputParameters["pFanClubFeeFlag"] = ViCommConst.FLAG_ON_STR;
		e.InputParameters["pSettleCancelFlag"] = string.Empty;
	}

	private void showSettleInstantButton() {
		// 即時決済ボタンの表示可否を決定します。 
		if (!string.IsNullOrEmpty(txtLoginId.Text)) {
			int creditMeasured;
			int billAmt;
			if (int.TryParse(ViewState["CREDIT_MEASURED_RATE_TARGET"].ToString(),out creditMeasured) == false) {
				btnSettleInstant.Visible = false;
				lblSettleResult.Visible = false;
			}
			if (int.TryParse(ViewState["BILL_AMT"].ToString(),out billAmt) == false) {
				btnSettleInstant.Visible = false;
				lblSettleResult.Visible = false;
			}

			if (creditMeasured == 1 && billAmt > 0) {
				btnSettleInstant.Visible = true;
				lblSettleResult.Visible = true;
			} else {
				btnSettleInstant.Visible = false;
				lblSettleResult.Visible = false;
			}
		}
	}

	protected string GetModelName(string pCarrierCd,string pUserAgent) {
		string sModelName = string.Empty;
		using (ModelDiscriminant oModelDiscriminant = new ModelDiscriminant()) {
			sModelName = oModelDiscriminant.GetModelName(pCarrierCd,pUserAgent) ?? string.Empty;
		}
		return sModelName;
	}

	protected string GetMailTxRxNm(object pTxRxType) {
		string sTxRxType = pTxRxType.ToString();
		string sTxRxTypeNm = string.Empty;

		if (sTxRxType.Equals(ViCommConst.TX)) {
			sTxRxTypeNm = "送信";
		} else {
			sTxRxTypeNm = "受信";
		}
		return sTxRxTypeNm;
	}

	protected string GetDelMark(object pTxDelFlag,object pRxDelFlag) {
		string sTx = pTxDelFlag.ToString();
		string sRx = pRxDelFlag.ToString();

		if ((sTx.Equals(ViCommConst.FLAG_OFF_STR)) && (sRx.Equals(ViCommConst.FLAG_OFF_STR))) {
			return string.Empty;
		} else if ((sTx.Equals(ViCommConst.FLAG_ON_STR)) && (sRx.Equals(ViCommConst.FLAG_ON_STR))) {
			return "削除済";
		} else if ((sTx.Equals(ViCommConst.FLAG_OFF_STR)) && (sRx.Equals(ViCommConst.FLAG_ON_STR))) {
			return "受信側削除";
		} else {
			return "送信側削除";
		}
	}

	protected string GetReadNm(object pReadFlag) {
		if (pReadFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "既読";
		} else {
			return "未読";
		}
	}

	protected string GetBatchMark(object pBatchMailFlag) {
		string sBatch = pBatchMailFlag.ToString();

		if (sBatch.Equals(ViCommConst.FLAG_ON_STR)) {
			return "一括送信ﾒｰﾙ";
		} else {
			return string.Empty;
		}
	}

	protected string GenerateOpenPicScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("data/{0}/Man/{1}.jpg",sSiteCd,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("data/{0}/Operator/{1}/{2}.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openPicViewer('{0}');",sImgPath);
	}

	protected string GenerateOpenMovieScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sMovieSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("~/movie/{0}/Man/{1}.3gp",sSiteCd,iBridUtil.addZero(sMovieSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("~/movie/{0}/Operator/{1}/{2}.3gp",sSiteCd,sTxLoginId,iBridUtil.addZero(sMovieSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openMovieViewer('{0}');",this.ResolveUrl(sImgPath));
	}

	protected void vdcMailBox_ServerValidate(object source,ServerValidateEventArgs args) {
		DateTime dtFrom;
		DateTime dtTo;
		try {
			dtFrom = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:00:00",lstMailFromYYYY.SelectedValue,lstMailFromMM.SelectedValue,lstMailFromDD.SelectedValue,lstMailFromHH.SelectedValue));
			dtTo = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:00:00",lstMailToYYYY.SelectedValue,lstMailToMM.SelectedValue,lstMailToDD.SelectedValue,lstMailToHH.SelectedValue));
		} catch {
			vdcMailBox.ErrorMessage = "正しい日時を入力してください";
			args.IsValid = false;
			return;
		}
		if (dtFrom > dtTo) {
			vdcMailBox.ErrorMessage = "日時の大小関係を正しく入力してください";
			args.IsValid = false;
			return;
		}

		args.IsValid = true;
	}

	protected void grdMailBox_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"WAIT_TX_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR)) {
				if (DataBinder.Eval(e.Row.DataItem,"TXRX_TYPE").ToString().Equals(ViCommConst.TX)) {
					e.Row.BackColor = Color.Azure;
				} else {
					e.Row.BackColor = Color.LavenderBlush;
				}
			} else {
				e.Row.BackColor = Color.LightGray;
			}
		}
	}
	protected void btnAddPoint_Click(object sender,EventArgs e) {
		int iAmt = 0;
		int.TryParse(txtAddPointAmt.Text,out iAmt);

		if ((IsValid) && (txtAddPointAmt.Text != "")) {
			using (DbSession db = new DbSession()) {

				db.PrepareProcedure("USER_MAN_ADD_POINT");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,
									iBridUtil.GetStringValue(ViewState["SITE_CD"]));
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,
									iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
				db.ProcedureInParm("PADD_POINT",DbSession.DbType.NUMBER,iAmt);
				db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
				db.ProcedureInParm("PMODIFY_USER_NM",DbSession.DbType.VARCHAR2,txtAddUserNm.Text);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
			DataBind();
			txtAddPointAmt.Text = "";
			txtRemarks.Text = "";
			txtAddUserNm.Text = "";
		}
	}

	protected void btnAddProfileFillDocPoint_Click(object sender,EventArgs e) {
		AddUserDefPoint(ViCommConst.UserDefPointId.PROFILE_FILL_DOC);
	}

	protected void btnAddProfilePicPoint_Click(object sender,EventArgs e) {
		AddUserDefPoint(ViCommConst.UserDefPointId.PROFILE_PIC);
	}

	protected void btnAddProfilePicFacePoint_Click(object sender,EventArgs e) {
		AddUserDefPoint(ViCommConst.UserDefPointId.PROFILE_PIC_FACE);
	}

	private void AddUserDefPoint(string pAddPointId) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADD_USER_DEF_POINT");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["SITE_CD"]));
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
			oDbSession.ProcedureInParm("pADD_POINT_ID",DbSession.DbType.VARCHAR2,pAddPointId);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
		DataBind();
		AddUserDefPointBtnRefresh();
	}

	private void AddUserDefPointBtnRefresh() {
		string sSiteCd = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		string sUserSeq = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);

		using (UserDefPoint oUserDefPoint = new UserDefPoint()) {
			btnAddProfileFillDocPoint.Visible = oUserDefPoint.IsExistAddPointId(sSiteCd,ViCommConst.UserDefPointId.PROFILE_FILL_DOC);
			btnAddProfilePicPoint.Visible = oUserDefPoint.IsExistAddPointId(sSiteCd,ViCommConst.UserDefPointId.PROFILE_PIC);
			btnAddProfilePicFacePoint.Visible = oUserDefPoint.IsExistAddPointId(sSiteCd,ViCommConst.UserDefPointId.PROFILE_PIC_FACE);
		}
		using (UserDefPointHistory oUserDefPointHistory = new UserDefPointHistory()) {
			if (btnAddProfileFillDocPoint.Visible) {
				btnAddProfileFillDocPoint.Enabled = !oUserDefPointHistory.IsAddedPoint(sSiteCd,sUserSeq,ViCommConst.UserDefPointId.PROFILE_FILL_DOC);
			}
			if (btnAddProfilePicPoint.Visible) {
				btnAddProfilePicPoint.Enabled = !oUserDefPointHistory.IsAddedPoint(sSiteCd,sUserSeq,ViCommConst.UserDefPointId.PROFILE_PIC);
			}
			if (btnAddProfilePicFacePoint.Visible) {
				btnAddProfilePicFacePoint.Enabled = !oUserDefPointHistory.IsAddedPoint(sSiteCd,sUserSeq,ViCommConst.UserDefPointId.PROFILE_PIC_FACE);
			}
		}
	}

	protected string CheckAddrNg(object pNonExistMailAddrFlag) {
		string sText = string.Empty;

		switch (pNonExistMailAddrFlag.ToString()) {
			case "0":
				sText = "正常";
				break;
			case "1":
				sText = "フィルタリングエラー";
				break;
			case "2":
				sText = "不通エラー";
				break;
		}

		return sText;
	}
	
	protected string CheckMuller3Ng(object pTxMuller3NgMailAddrFlag) {
		string sText = string.Empty;

		switch (pTxMuller3NgMailAddrFlag.ToString()) {
			case "0":
				sText = "正常";
				break;
			case "1":
				sText = "フィルタリングエラー";
				break;
		}
		
		return sText;
	}

	protected void btnAddGamePoint_Click(object sender,EventArgs e) {

		if (IsValid) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("GAME_POINT_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["SITE_CD"]));
				oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
				oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]));
				oDbSession.ProcedureInParm("pADD_POINT",DbSession.DbType.VARCHAR2,txtAddGamePoint.Text);
				oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(ViewState["REVISION_NO_GAME"]));
				oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
			DataBind();
		}
	}

	protected void btnRemarks_Click(object sender,EventArgs e) {
		string[] sRemarks = SysPrograms.SplitBytes(Encoding.GetEncoding("UTF-8"),txtRemarks4.Text,3000);
		
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("USER_REMARKS4_UPDATE");
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
			oDbSession.ProcedureInArrayParm("pREMARKS",DbSession.DbType.VARCHAR2,sRemarks);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(ViewState["REVISION_NO"]));
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
		DataBind();
	}

	public static bool IsAvailableService(ulong pService) {
		using (ManageCompany oInstance = new ManageCompany()) {
			return oInstance.IsAvailableService(pService);
		}
	}
	protected Color GetUserStatusColor(object pUserStatus) {

		Color oRetColor = System.Drawing.Color.Black;
		switch (pUserStatus.ToString()) {

			case ViCommConst.USER_MAN_RESIGNED:
				oRetColor = System.Drawing.Color.Maroon;
				break;
			case ViCommConst.USER_MAN_STOP:
				oRetColor = System.Drawing.Color.Red;
				break;
			case ViCommConst.USER_MAN_BLACK:
				oRetColor = System.Drawing.Color.Red;
				break;
			case ViCommConst.USER_MAN_HOLD:
				oRetColor = System.Drawing.Color.Blue;
				break;
			default:
				break;
		}

		return oRetColor;
	}
	protected void grdBbs_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
		}
	}
	protected string GetDelFlag(object pDelFlag,object pAdminDelFlag) {
		if (pAdminDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "管理者削除";
		} else if (pDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "ﾕｰｻﾞｰ削除";
		} else {
			return string.Empty;
		}
	}
	protected void lnkDelBbs_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		string sBbsSeq = arguments[0];
		string sexCd = arguments[1];
		int iDelFlag;
		int.TryParse(arguments[2],out iDelFlag);
		using (BbsLog oBbsLog = new BbsLog()) {
			oBbsLog.DeleteBbs(sBbsSeq,sexCd,iDelFlag);
		}
		DataBind();
	}

	protected void grdCastFriends_DataBound(object sender,EventArgs e) {
		GridView oGridView = sender as GridView;
		if (oGridView == null) {
			return;
		}
		if (oGridView.Rows.Count == 0) {
			oGridView.Parent.Visible = false;
		}
	}

	protected string GetValidMark(object pValue) {
		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pValue))) {
			return "○";
		} else {
			return "×";
		}
	}

	protected string GetInValidMark(object pValue) {
		if (ViCommConst.NA_CHAR_PF_NOT_APPROVED.Equals(iBridUtil.GetStringValue(pValue)) ||
			ViCommConst.NA_CHAR_IMPORT.Equals(iBridUtil.GetStringValue(pValue))) {
			return "×";
		} else {
			return "○";
		}
	}

	protected string GetDaysInDate(object pValue) {
		string sDate = iBridUtil.GetStringValue(pValue);

		if (sDate.Equals(string.Empty)) {
			return string.Empty;
		} else {
			return sDate.Substring(0,10);
		}
	}

	protected void dvwUserManCharacter_DataBound(object sender,EventArgs e) {
		if (this.IsAvailableRichno()) {
			using (UserManCharacterEx oUserEx = new UserManCharacterEx()) {
				using (DataSet oDataSet = oUserEx.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]),iBridUtil.GetStringValue(ViewState["USER_SEQ"]))) {
					if (oDataSet.Tables[0].Rows.Count > 0) {
						DataRow oDataRow = oDataSet.Tables[0].Rows[0];
						Label lblUpdDate = dvwUserManCharacter.FindControl("lblLastRichinoRankUpdDate") as Label;
						Label lblRankNm = dvwUserManCharacter.FindControl("lblRichinoRankNm") as Label;
						lblUpdDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}",oDataRow["LAST_RICHINO_RANK_UPD_DATE"]);
						lblRankNm.Text = iBridUtil.GetStringValue(oDataRow["RICHINO_RANK_NM"]);
					}
				}
			}
		}
	}

	protected void btnDelWithdrawal_Click(object sender,EventArgs e) {
		UpdateWithdrawal(string.Empty,ViCommConst.FLAG_ON);
	}

	protected void btnUpdateWithdrawal_Click(object sender,EventArgs e) {
		UpdateWithdrawal(string.Empty,ViCommConst.FLAG_OFF);
	}

	protected void btnAcceptWithdrawal_Click(object sender,EventArgs e) {
		UpdateWithdrawal(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE,ViCommConst.FLAG_OFF);
	}

	protected void btnReturnWithdrawalList_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("../Extension/AcceptWithdrawalList.aspx?sexcd={0}&reload=1",ViCommConst.MAN));
	}

	private void UpdateWithdrawal(string pStatus,int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WITHDRAWAL_MAINTE");
			db.ProcedureBothParm("PWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,lblWithdrawalSeq.Text);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PWITHDRAWAL_STATUS",DbSession.DbType.VARCHAR2,pStatus);
			db.ProcedureInParm("PWITHDRAWAL_REASON_CD",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PWITHDRAWAL_REASON_DOC",DbSession.DbType.VARCHAR2,txtWithdrawalReasonDoc.Text);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtWithdrawalRemarks.Text);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.VARCHAR2,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			DataBind();
		}
	}
	protected void btnBingoEntryMainte_Click(object sender,EventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BINGO_ENTRY_MAINTE");
			db.ProcedureInParm("PBINGO_ENTRY_SEQ",DbSession.DbType.VARCHAR2,lblBingoEntrySeq.Text);
			db.ProcedureInParm("PBINGO_POINT_PAYMENT_FLAG",DbSession.DbType.VARCHAR2,rdoBingoPointPayment.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			DataBind();
		}
	}

	protected void btnReturnBingoEntryList_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("../Extension/MailDeBingoPrizeGetList.aspx?sexcd={0}&seq={1}&sitecd={2}",ViCommConst.MAN,lblBingoTermSeq.Text,iBridUtil.GetStringValue(ViewState["SITE_CD"])));
	}

	protected void SetGameCharacter() {

		this.pnlGameCharacter.Visible = false;
		this.pnlAddGamePoint.Visible = false;
		this.pnlAddGameItem.Visible = false;
		this.pnlAddExp.Visible = false;

		using (UserManCharacterEx oUserEx = new UserManCharacterEx()) {
			using (DataSet oDataSet = oUserEx.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]),iBridUtil.GetStringValue(ViewState["USER_SEQ"]))) {
				if (oDataSet.Tables[0].Rows.Count > 0) {
					string sSiteUseStatus = oDataSet.Tables[0].Rows[0]["SITE_USE_STATUS"].ToString();
					if (sSiteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY) ||
						sSiteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
						this.pnlGameCharacter.Visible = true;
						this.pnlAddGamePoint.Visible = true;
						this.pnlAddGameItem.Visible = true;
						this.pnlAddExp.Visible = true;

						using (GameCharacter oGameCharacter = new GameCharacter()) {
							using (DataSet oGameCharData = oGameCharacter.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]),iBridUtil.GetStringValue(ViewState["USER_SEQ"]),iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]),ViCommConst.MAN)) {
								foreach (DataRow oGameCharDr in oGameCharData.Tables[0].Rows) {

									Label oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterHandleNm") as Label;
									oTmpControl.Text = oGameCharDr["GAME_HANDLE_NM"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterTypeNm") as Label;
									oTmpControl.Text = oGameCharDr["GAME_CHAR_TYPE_NM"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterPoint") as Label;
									oTmpControl.Text = oGameCharDr["GAME_POINT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterLevel") as Label;
									oTmpControl.Text = oGameCharDr["GAME_CHARACTER_LEVEL"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterExp") as Label;
									oTmpControl.Text = oGameCharDr["EXP"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterRestExp") as Label;
									oTmpControl.Text = oGameCharDr["REST_EXP"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterTotalExp") as Label;
									oTmpControl.Text = oGameCharDr["TOTAL_EXP"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterUnassignedFroce") as Label;
									oTmpControl.Text = oGameCharDr["UNASSIGNED_FORCE_COUNT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterMissionMaxForce") as Label;
									oTmpControl.Text = oGameCharDr["MISSION_MAX_FORCE_COUNT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterAttackMaxForce") as Label;
									oTmpControl.Text = oGameCharDr["ATTACK_MAX_FORCE_COUNT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterDefenceMaxForce") as Label;
									oTmpControl.Text = oGameCharDr["DEFENCE_MAX_FORCE_COUNT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterCooperation") as Label;
									oTmpControl.Text = oGameCharDr["COOPERATION_POINT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterStageNm") as Label;
									oTmpControl.Text = oGameCharDr["STAGE_NM"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterRegistDate") as Label;
									oTmpControl.Text = oGameCharDr["GAME_REGIST_DATE"].ToString();

									ViewState["REVISION_NO_GAME"] = oGameCharDr["REVISION_NO"].ToString();
								}
							}
						}
					}
				}
			}
		}
	}

	protected void vdcAddGamePoint_ServerValidate(object sender,ServerValidateEventArgs args) {
		if (IsValid) {
			args.IsValid = true;
			Label lblGameCharacterPoint = this.plcGameHolder.FindControl("lblGameCharacterPoint") as Label;
			if (lblGameCharacterPoint.Text.Equals(string.Empty) ||
				txtAddGamePoint.Text.Equals(string.Empty) ||
				(int.Parse(lblGameCharacterPoint.Text) + int.Parse(txtAddGamePoint.Text)) < 0) {
				args.IsValid = false;
			}
		}

	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.ViewState["GAME_ITEM_CATEGORY_TYPE"] = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void dsGameItemCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.ViewState["SITE_CD"];
	}
	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.ViewState["SITE_CD"];
		e.InputParameters["pSexCd"] = ViCommConst.MAN;
		e.InputParameters["pGameItemCategoryType"] = this.ViewState["GAME_ITEM_CATEGORY_TYPE"];
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}
	protected void btnAddGameItem_Click(object sender,EventArgs e) {

		if (IsValid) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("GAME_POSSESSION_ITEM_MAINTE");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,this.ViewState["SITE_CD"]);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,this.ViewState["USER_SEQ"]);
				db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.ViewState["USER_CHAR_NO"]);
				db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.lstGameItem.SelectedValue);
				db.ProcedureInParm("pADD_COUNT",DbSession.DbType.NUMBER,int.Parse(txtGameItemGiveCount.Text));
				db.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,1);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				DataBind();
				this.txtGameItemGiveCount.Text = string.Empty;
				lstItemPresent.SelectedValue = null;
			}
		}
	}

	protected void btnSeekPossessionGameItem_Click(object sender,EventArgs e) {
		grdPossessionGameItem.DataBind();
	}

	protected void dsPossessionGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters["pUserCharNo"] = iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]);
	}

	protected void btnAddExp_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADD_EXP");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["SITE_CD"]));
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]));
			oDbSession.ProcedureInParm("pADD_EXP",DbSession.DbType.NUMBER,this.txtAddExp.Text);
			oDbSession.ProcedureOutParm("pADD_FORCE_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLEVEL_UP_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
			DataBind();
			this.txtAddExp.Text = string.Empty;
		}
	}

	protected void lnkPossessionGameItem_Click(object sender,EventArgs e) {
		this.pnlPossessionGameItem.Visible = true;
		this.grdPossessionGameItem.DataSourceID = "dsPossessionGameItem";
		this.grdPossessionGameItem.DataBind();
	}

	protected string GetGameCharacterMainteLink() {
		return string.Format("../Extension/GameCharacterMainte.aspx?site_cd={0}&user_seq={1}&user_char_no={2}&sex_cd={3}&login_id={4}",
			iBridUtil.GetStringValue(ViewState["SITE_CD"]),
			iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
			iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]),
			ViCommConst.MAN,
			txtLoginId.Text);
	}

	protected string GetEnabledUserDefineFlagMark(object pUserDefineMask) {
		int iUserDefineMask = 0;
		if (!int.TryParse(iBridUtil.GetStringValue(pUserDefineMask),out iUserDefineMask)) {
			return string.Empty;
		}
		StringBuilder oMark = new StringBuilder();
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			foreach (DataRow oRow in oCodeDtl.GetList(ViCommConst.CODE_TYPE_USER_DEFINE_FLAG_MAN).Tables[0].Rows) {
				if ((iUserDefineMask & int.Parse(oRow["CODE"].ToString())) > 0) {
					oMark.AppendFormat("{0}<br />",oRow["CODE_NM"]);
				}
			}
		}
		return oMark.ToString();
	}

	protected string GetModifyUserNm(object pModifyUserCd) {
		return (pModifyUserCd.ToString().Equals(ViCommConst.ModifyUserCd.Admin)) ? "管理者変更" : "ユーザー変更";
	}

	protected string GetModifyBeforeAfterNm(object pModifyBeforeAfterCd) {
		string sModifyBeforeAfterNm = string.Empty;

		switch (pModifyBeforeAfterCd.ToString()) {
			case "1":
				sModifyBeforeAfterNm = "変更前";
				break;
			case "2":
				sModifyBeforeAfterNm = "変更後";
				break;				
		}

		return sModifyBeforeAfterNm;
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}

	protected void btnDeleteDeviceUuid_Click(object sender,EventArgs e) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_DEVICE_UUID");
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
			DataBind();
		}
	}

	protected string GetErrorTypeText(object pErrorType) {
		string sText = string.Empty;

		switch (pErrorType.ToString()) {
			case "1":
				sText = "ﾌｨﾙﾀﾘﾝｸﾞ";
				break;
			case "2":
				sText = "不通";
				break;
		}

		return sText;
	}

	protected string GetRegistServicePointFlagText(object pRegistServicePointFlag) {
		string sText = string.Empty;

		if (pRegistServicePointFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			sText = "付与済";
		} else {
			sText = "未付与";
		}

		return sText;
	}

	/// <summary>
	/// 退会履歴リンク押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkWithdrawalHis_Click(object sender,EventArgs e) {
		// 初期表示状態に戻す
		CloseExtInfo();

		// 退会履歴一覧を表示する
		pnlWithdrawalHistoryList.Visible = true;
		grdWithdrawalHistoryList.DataSourceID = "dsWithdrawalHistoryList";
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// 退会履歴のバリデーション実行
	/// </summary>
	/// <returns></returns>
	private bool validateWithdrawalHistory() {
		vrdWithdrawalHistoryDate.Validate();
		vdrWithdrawalHistoryRemarks.Validate();
		vdeWithdrawalHistoryAddPointCount.Validate();
		if (!vrdWithdrawalHistoryDate.IsValid) {
			return false;
		}
		if (!vdrWithdrawalHistoryRemarks.IsValid) {
			return false;
		}
		if (!vdeWithdrawalHistoryAddPointCount.IsValid) {
			return false;
		}
		return true;
	}

	/// <summary>
	/// (退会履歴)登録ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnRegWithdrawalHistory_Click(object sender,EventArgs e) {
		string sStatus = string.Empty;
		string sCreateDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:{5}",
			lstWithdrawalYYYY.SelectedValue,
			lstWithdrawalMM.SelectedValue,
			lstWithdrawalDD.SelectedValue,
			lstWithdrawalHH.SelectedValue,
			lstWithdrawalMI.SelectedValue,
			lblWithdrawalSS.Text
		);
		string sWithdrawalHistorySeq = string.Empty;
		string sWithdrawalSeq = iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]);
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["checkedseq"]))) {
			sWithdrawalSeq = iBridUtil.GetStringValue(Request.QueryString["checkedseq"]);
		}

		// バリデーション実行
		if (!validateWithdrawalHistory()) {
			return;
		}

		// 退会履歴の追加・更新
		WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory();
		oWithdrawalHistory.Mainte(
			iBridUtil.GetStringValue(ViewState["SITE_CD"]),
			iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
			null,
			sWithdrawalSeq,
			ref sWithdrawalHistorySeq,
			txtWithdrawalHistoryAddPointCount.Text,
			txtWithdrawalHistoryRemarks.Text,
			sCreateDate,
			ViCommConst.FLAG_OFF_STR,
			out sStatus
		);
		// 入力欄を非表示
		pnlWithdrawalHistoryDetail.Visible = false;
		// 退会履歴一覧を再取得
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// (退会履歴)更新ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnUpWithdrawalHistory_Click(object sender,EventArgs e) {
		string sStatus = string.Empty;
		string sCreateDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:{5}",
			lstWithdrawalYYYY.SelectedValue,
			lstWithdrawalMM.SelectedValue,
			lstWithdrawalDD.SelectedValue,
			lstWithdrawalHH.SelectedValue,
			lstWithdrawalMI.SelectedValue,
			lblWithdrawalSS.Text
		);
		string sWithdrawalHistorySeq = lblWithdrawalHistorySeq.Text;
		string sWithdrawalSeq = iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]);
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["checkedseq"]))) {
			sWithdrawalSeq = iBridUtil.GetStringValue(Request.QueryString["checkedseq"]);
		}

		// バリデーション実行
		if (!validateWithdrawalHistory()) {
			return;
		}

		// 退会履歴の追加・更新
		WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory();
		oWithdrawalHistory.Mainte(
			iBridUtil.GetStringValue(ViewState["SITE_CD"]),
			iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
			null,
			sWithdrawalSeq,
			ref sWithdrawalHistorySeq,
			txtWithdrawalHistoryAddPointCount.Text,
			txtWithdrawalHistoryRemarks.Text,
			sCreateDate,
			ViCommConst.FLAG_OFF_STR,
			out sStatus
		);
		// 入力欄を非表示
		pnlWithdrawalHistoryDetail.Visible = false;
		// 退会履歴一覧を再取得
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// (退会履歴)削除ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnDelWithdrawalHistory_Click(object sender,EventArgs e) {
		string sStatus = string.Empty;
		string sWithdrawalHistorySeq = lblWithdrawalHistorySeq.Text;
		string sWithdrawalSeq = iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]);
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["checkedseq"]))) {
			sWithdrawalSeq = iBridUtil.GetStringValue(Request.QueryString["checkedseq"]);
		}

		// 退会履歴の削除
		WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory();
		oWithdrawalHistory.Mainte(
			iBridUtil.GetStringValue(ViewState["SITE_CD"]),
			iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
			null,
			sWithdrawalSeq,
			ref sWithdrawalHistorySeq,
			null,
			null,
			null,
			ViCommConst.FLAG_ON_STR,
			out sStatus
		);
		// 入力欄を非表示
		pnlWithdrawalHistoryDetail.Visible = false;
		// 退会履歴一覧を再取得
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// (退会履歴一覧)新規登録ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnNewWithdrawalHistory_Click(object sender,EventArgs e) {
		pnlWithdrawalHistoryDetail.Visible = true;

		lblWithdrawalHistorySeq.Text = string.Empty;
		txtWithdrawalHistoryRemarks.Text = string.Empty;
		txtWithdrawalHistoryAddPointCount.Text = string.Empty;

		lstWithdrawalYYYY.SelectedIndex = 0;
		lstWithdrawalMM.SelectedValue = DateTime.Now.ToString("MM");
		lstWithdrawalDD.SelectedValue = DateTime.Now.ToString("dd");
		lstWithdrawalHH.SelectedValue = DateTime.Now.ToString("HH");
		lstWithdrawalMI.SelectedValue = DateTime.Now.ToString("mm");
		lblWithdrawalSS.Text = DateTime.Now.ToString("ss");

		btnRegistWithdrawalHistory.Visible = true;
		btnUpdateWithdrawalHistory.Visible = false;
		btnDeleteWithdrawalHistory.Visible = false;
	}

	/// <summary>
	/// (退会履歴)閉じるボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCloseWithdrawalHistoryDetail_Click(object sender,EventArgs e) {
		pnlWithdrawalHistoryDetail.Visible = false;
	}

	/// <summary>
	/// (退会履歴一覧)閉じるボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCloseWithdrawalHistoryList_Click(object sender,EventArgs e) {
		grdWithdrawalHistoryList.DataSourceID = string.Empty;
		pnlWithdrawalHistoryDetail.Visible = false;
		pnlWithdrawalHistoryList.Visible = false;
	}

	/// <summary>
	/// 退会履歴取得用パラメータを設定
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsWithdrawalHistoryList_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
	}

	/// <summary>
	/// 退会履歴内容の取得（整形して返す）
	/// </summary>
	/// <param name="pRemarks"></param>
	/// <param name="pAddPointCnt"></param>
	/// <returns></returns>
	protected string GetWithdrawalHistoryRemarks(object pRemarks,object pAddPointCnt) {
		string sRemarks = iBridUtil.GetStringValue(pRemarks);
		string sAddPointCnt = iBridUtil.GetStringValue(pAddPointCnt);
		if (!string.IsNullOrEmpty(sAddPointCnt)) {
			return string.Format("{0}/{1}回目",sRemarks,sAddPointCnt);
		}
		return sRemarks;
	}

	/// <summary>
	/// (退会履歴一覧)編集リンク押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkEditWithdrawalHistory_Command(object sender,CommandEventArgs e) {
		string sWithdrawalHistorySeq = e.CommandArgument.ToString();
		using (WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory()) {
			DataSet ds = oWithdrawalHistory.GetOne(iBridUtil.GetStringValue(ViewState["SITE_CD"]),iBridUtil.GetStringValue(ViewState["USER_SEQ"]),sWithdrawalHistorySeq);
			if (ds.Tables[0].Rows.Count > 0) {
				// 退会履歴の入力欄を表示
				pnlWithdrawalHistoryDetail.Visible = true;

				// 登録ボタンを非表示、更新・削除ボタンを表示
				btnRegistWithdrawalHistory.Visible = false;
				btnUpdateWithdrawalHistory.Visible = true;
				btnDeleteWithdrawalHistory.Visible = true;

				// 退会履歴の内容を設定
				DataRow dr = ds.Tables[0].Rows[0];
				lblWithdrawalHistorySeq.Text = dr["WITHDRAWAL_HISTORY_SEQ"].ToString();
				txtWithdrawalHistoryRemarks.Text = dr["REMARKS"].ToString();
				txtWithdrawalHistoryAddPointCount.Text = dr["WITHDRAWAL_ADD_POINT_CNT"].ToString();

				DateTime dt = DateTime.Parse(dr["CREATE_DATE"].ToString());
				lstWithdrawalYYYY.SelectedValue = dt.ToString("yyyy");
				lstWithdrawalMM.SelectedValue = dt.ToString("MM");
				lstWithdrawalDD.SelectedValue = dt.ToString("dd");
				lstWithdrawalHH.SelectedValue = dt.ToString("HH");
				lstWithdrawalMI.SelectedValue = dt.ToString("mm");
				lblWithdrawalSS.Text = dt.ToString("ss");
			}
		}
	}

	/// <summary>
	/// 退会履歴関連を初期化
	/// </summary>
	private void ClearWithdrawalHistory() {
		grdWithdrawalHistoryList.DataSourceID = string.Empty;
		pnlWithdrawalHistoryDetail.Visible = false;
		pnlWithdrawalHistoryList.Visible = false;

		lblWithdrawalHistorySeq.Text = string.Empty;
		txtWithdrawalHistoryRemarks.Text = string.Empty;
		txtWithdrawalHistoryAddPointCount.Text = string.Empty;

		// 退会履歴の退会処理日の選択肢を設定
		SysPrograms.SetupDay(lstWithdrawalYYYY,lstWithdrawalMM,lstWithdrawalDD,false);
		SysPrograms.SetHourList(lstWithdrawalHH);
		SysPrograms.SetMinuteList(lstWithdrawalMI);
		lstWithdrawalYYYY.SelectedIndex = 0;
		lstWithdrawalMM.SelectedValue = DateTime.Now.ToString("MM");
		lstWithdrawalDD.SelectedValue = DateTime.Now.ToString("dd");
		lstWithdrawalHH.SelectedValue = DateTime.Now.ToString("HH");
		lstWithdrawalMI.SelectedValue = DateTime.Now.ToString("mm");
		lblWithdrawalSS.Text = DateTime.Now.ToString("ss");
	}

	/// <summary>
	/// 退会時ポイント付与300円分
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnAddPoint300_Click(object sender,EventArgs e) {
		string pWithdrawalSeq = lblWithdrawalSeq.Text;
		string pStatus = string.Empty;

		Withdrawal oWithdrawal = new Withdrawal();
		oWithdrawal.AddPointWithdrawalMan(
			iBridUtil.GetStringValue(ViewState["SITE_CD"]),
			iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
			ref pWithdrawalSeq,
			ViCommConst.UserDefPointId.WITHDRAWAL_300,
			out pStatus
		);
		lblWithdrawalSeq.Text = pWithdrawalSeq;
		DataBind();
	}

	/// <summary>
	/// 退会時ポイント付与500円分
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnAddPoint500_Click(object sender,EventArgs e) {
		string pWithdrawalSeq = lblWithdrawalSeq.Text;
		string pStatus = string.Empty;

		Withdrawal oWithdrawal = new Withdrawal();
		oWithdrawal.AddPointWithdrawalMan(
			iBridUtil.GetStringValue(ViewState["SITE_CD"]),
			iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
			ref pWithdrawalSeq,
			ViCommConst.UserDefPointId.WITHDRAWAL_500,
			out pStatus
		);
		lblWithdrawalSeq.Text = pWithdrawalSeq;
		DataBind();
	}

	/// <summary>
	/// 退会時ポイント付与1000円分
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnAddPoint1000_Click(object sender,EventArgs e) {
		string pWithdrawalSeq = lblWithdrawalSeq.Text;
		string pStatus = string.Empty;

		Withdrawal oWithdrawal = new Withdrawal();
		oWithdrawal.AddPointWithdrawalMan(
			iBridUtil.GetStringValue(ViewState["SITE_CD"]),
			iBridUtil.GetStringValue(ViewState["USER_SEQ"]),
			ref pWithdrawalSeq,
			ViCommConst.UserDefPointId.WITHDRAWAL_1000,
			out pStatus
		);
		lblWithdrawalSeq.Text = pWithdrawalSeq;
		DataBind();
	}

	/// <summary>
	/// 退会処理日のバリデーション
	/// </summary>
	/// <param name="source"></param>
	/// <param name="args"></param>
	protected void vrdWithdrawalHistoryDate_ServerValidate(object source,ServerValidateEventArgs args) {
		DateTime dt;
		string sWithdrawalDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:{5}",
			lstWithdrawalYYYY.SelectedValue,
			lstWithdrawalMM.SelectedValue,
			lstWithdrawalDD.SelectedValue,
			lstWithdrawalHH.SelectedValue,
			lstWithdrawalMI.SelectedValue,
			lblWithdrawalSS.Text
		);

		args.IsValid = true;
		if (!DateTime.TryParse(sWithdrawalDate,out dt)) {
			args.IsValid = false;
		}
	}
}
