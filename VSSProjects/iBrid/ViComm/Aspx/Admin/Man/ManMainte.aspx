<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManMainte.aspx.cs" Inherits="Man_ManMainte" Title="男性会員情報設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性会員情報設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			$NO_TRANS_START;
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset-inner">
					<legend>[会員情報]</legend>
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								SEQ.
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblUserSeq" runat="server" Text="Label"></asp:Label>
								<asp:Label ID="lblRegist" runat="server" Text="新規登録"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログインID
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblLoginId" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログインパスワード
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtLoginPassword" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrLoginPassword" runat="server" ErrorMessage="ログインパスワードを入力して下さい。" ControlToValidate="txtLoginPassword" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeLoginPassword" runat="server" ErrorMessage="ログインパスワードは4〜8桁の英数字で入力して下さい。" ValidationExpression="\w{4,8}" ControlToValidate="txtLoginPassword"
									ValidationGroup="Detail">*</asp:RegularExpressionValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								電話番号
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="100px"></asp:TextBox>
								<asp:RegularExpressionValidator ID="vdeTel" runat="server" ErrorMessage="電話番号を正しく入力して下さい。" ValidationExpression="(0\d{10})" ControlToValidate="txtTel"
									ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								<asp:CustomValidator ID="vdcTel" runat="server" ControlToValidate="txtTel" ErrorMessage="既に登録済みの電話番号です。" OnServerValidate="vdcTel_ServerValidate" ValidationGroup="Detail"
									Display="Dynamic">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								電話番号認証済み
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkAttestedFlag" runat="server" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								メールアドレス
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrEmailAddr" runat="server" ErrorMessage="メールアドレスを入力して下さい。" ControlToValidate="txtEmailAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:CustomValidator ID="vdcEmailAddr" runat="server" ControlToValidate="txtEmailAddr" ErrorMessage="既に利用されているﾒｰﾙｱﾄﾞﾚｽです。" OnServerValidate="vdcEmailAddr_ServerValidate"
									ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								メールアドレス状態
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoNonExistMailAddrFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Text="正常" Value="0"></asp:ListItem>
									<asp:ListItem Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" Value="1"></asp:ListItem>
									<asp:ListItem Text="不通ｴﾗｰ" Value="2"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								MULLER3送信
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoTxMuller3NgMailAddrFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Text="正常" Value="0"></asp:ListItem>
									<asp:ListItem Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" Value="1"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								会員状態
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstUserStatus" runat="server" Width="206px" DataSourceID="dsUserStatus" DataTextField="CODE_NM" DataValueField="CODE">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								オンラインステータス
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstOnLineStatus" runat="server" Width="206px" DataSourceID="dsOnlineStatus" DataTextField="CODE_NM" DataValueField="CODE">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								残ポイント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtBalPoint" runat="server" MaxLength="6" Width="48px"></asp:TextBox>Pt
								<asp:RequiredFieldValidator ID="vdrBalPoint" runat="server" ErrorMessage="残ポイントを入力して下さい。" ControlToValidate="txtBalPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeBalPoint" runat="server" ErrorMessage="残ポイントは1〜6桁の数字で入力して下さい。" ValidationExpression="^-?[0-9]{1,6}$" ControlToValidate="txtBalPoint"
									ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								入金累計回数
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTotalReceiptCount" runat="server" MaxLength="5" Width="70px"></asp:TextBox>回
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								入金累計額
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTotalReceiptAmt" runat="server" MaxLength="8" Width="70px"></asp:TextBox>円
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ﾎﾟｲﾝﾄｱﾌﾘ入金累計回数
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMchaTotalReceiptCount" runat="server" MaxLength="5" Width="70px"></asp:TextBox>回
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ﾎﾟｲﾝﾄｱﾌﾘ入金累計額
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMchaTotalReceiptAmt" runat="server" MaxLength="8" Width="70px"></asp:TextBox>円
							</td>
						</tr>
						<asp:PlaceHolder ID="plcMonthlyReceipt" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderStyle">
									当月入金回数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMonthlyReceiptCount" runat="server" MaxLength="5" Width="70px"></asp:TextBox>回
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									当月入金額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMonthlyReceiptAmt" runat="server" MaxLength="8" Width="70px"></asp:TextBox>円
								</td>
							</tr>
						</asp:PlaceHolder>
						<asp:PlaceHolder ID="plcPayAfter" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderStyle">
									制限ポイント
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLimitPoint" runat="server" MaxLength="6" Width="48px"></asp:TextBox>Pt
									<asp:RequiredFieldValidator ID="vdrLimitPoint" runat="server" ErrorMessage="制限ポイントを入力して下さい。" ControlToValidate="txtLimitPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeLimitPoint" runat="server" ErrorMessage="制限ポイントは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtLimitPoint"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									請求額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBillAmt" runat="server" MaxLength="8" Width="70px"></asp:TextBox>円
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金期日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReceiptLimitDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
									<asp:RangeValidator ID="vddReceiptLimitDay" runat="server" ErrorMessage="入金期日を正しく入力して下さい。" ControlToValidate="txtReceiptLimitDay" MaximumValue="2099/12/31"
										MinimumValue="2000/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
									<asp:RegularExpressionValidator ID="vdeReceiptLimitDay" runat="server" ErrorMessage="入金期日を正しく入力して下さい。" ControlToValidate="txtReceiptLimitDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									督促レベル
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUrgeLevel" runat="server" MaxLength="1" Width="45px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									督促実行済み回数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUrgeExecCount" runat="server" MaxLength="3" Width="45px"></asp:TextBox>回
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									督促最大回数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMaxUrgeCount" runat="server" MaxLength="3" Width="45px"></asp:TextBox>回
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									本日督促回数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUrgeCountToday" runat="server" MaxLength="2" Width="45px"></asp:TextBox>回
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									督促終了予定日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUrgeEndDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
									<asp:RangeValidator ID="vddUrgeEndDay" runat="server" ErrorMessage="督促終了予定日を正しく入力して下さい。" ControlToValidate="txtUrgeEndDay" MaximumValue="2099/12/31" MinimumValue="2000/01/01"
										Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
									<asp:RegularExpressionValidator ID="vdeUrgeEndDay" runat="server" ErrorMessage="督促終了予定日を正しく入力して下さい。" ControlToValidate="txtUrgeEndDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									督促再開予定日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUrgeRestartDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
									<asp:RangeValidator ID="vddUrgeRestartDay" runat="server" ErrorMessage="督促再開予定日を正しく入力して下さい。" ControlToValidate="txtUrgeRestartDay" MaximumValue="2099/12/31"
										MinimumValue="2000/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
									<asp:RegularExpressionValidator ID="vdeUrgeRestartDay" runat="server" ErrorMessage="督促再開予定日を正しく入力して下さい。" ControlToValidate="txtUrgeRestartDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									後払限度ポイント固定
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkLimitPayAfterFixedFlag" runat="server" />後払限度ポイントを固定する。
								</td>
							</tr>
							<asp:PlaceHolder ID="plcGGFlag" runat="server" Visible="false">
								<tr>
									<td class="tdHeaderStyle">
										GGフラグ
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkGGFlag" runat="server" />
									</td>
								</tr>
							</asp:PlaceHolder>
						</asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								サービスポイント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtServicePoint" runat="server" MaxLength="6" Width="48px"></asp:TextBox>Pt
								<asp:RequiredFieldValidator ID="vdrServicePoint" runat="server" ErrorMessage="サービスポイントを入力して下さい。" ControlToValidate="txtServicePoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeServicePoint" runat="server" ErrorMessage="サービスポイントは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtServicePoint"
									ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								サービスポイント有効日時
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtServicePointEffectiveDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
								<asp:RangeValidator ID="vddServicePointEffectiveDay" runat="server" ErrorMessage="サービスポイント有効日を正しく入力して下さい。" ControlToValidate="txtServicePointEffectiveDay"
									MaximumValue="2099/12/31" MinimumValue="1990/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
								<asp:CustomValidator ID="vdcServicePointEffectiveDay" runat="server" ControlToValidate="txtServicePoint" ErrorMessage='サービスポイントが０以外の場合はサービスポイント有効日を入力して下さい。'
									ValidationGroup="Detail" Display="Dynamic" OnServerValidate="vdcServicePointEffectiveDay_ServerValidate">*</asp:CustomValidator>
								<br />
								<asp:TextBox ID="txtServicePointEffectiveTime" runat="server" Width="80px" MaxLength="8"></asp:TextBox>
								<asp:RegularExpressionValidator ID="vdeServicePointEffectiveTime" runat="server" ErrorMessage="サービスポイント時刻は99:99:99の形式で入力して下さい。" ValidationExpression="^([01]+[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$"
									ControlToValidate="txtServicePointEffectiveTime" ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								<asp:CustomValidator ID="vdcServicePointEffectiveTime" runat="server" ControlToValidate="txtServicePoint" ErrorMessage='サービスポイントが０以外の場合はサービスポイント有効時刻を入力して下さい。'
									ValidationGroup="Detail" Display="Dynamic" OnServerValidate="vdcServicePointEffectiveTime_ServerValidate">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								<%= DisplayWordUtil.Replace("キャストメール受信区分") %>
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstCastMailRxType" runat="server" Width="206px" DataSourceID="dsMailRxType" DataTextField="CODE_NM" DataValueField="CODE">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								お知らせメール受信区分
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstInfoMailRxType" runat="server" Width="206px" DataSourceID="dsInfoMailRxType" DataTextField="CODE_NM" DataValueField="CODE">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログイン通知受信区分1
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkLoginMailRxFlag1" runat="server" /><%= DisplayWordUtil.Replace("お気に入りのキャストがログインした場合に通知する。") %>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログイン通知受信区分2
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkLoginMailRxFlag2" runat="server" /><%= DisplayWordUtil.Replace("会員をお気に入り登録しているキャストがログインした場合に通知する。")%>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								携帯ﾒｰﾙ受信時間帯
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstMobileMailRxStartTime" runat="server" Width="60px" DataSourceID="dsTime" DataTextField="CODE_NM" DataValueField="CODE_NM">
								</asp:DropDownList>〜
								<asp:DropDownList ID="lstMobileMailRxEndTime" runat="server" Width="60px" DataSourceID="dsTime" DataTextField="CODE_NM" DataValueField="CODE_NM">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								フリーダイアル利用
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkUseFreeDialFlag" runat="server" />フリーダイアルを利用する。
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ホワイトプラン利用
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkUseWhitePlanFlag" runat="server" />ホワイトプランを利用する。
							</td>
						</tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                CROSMILE利用
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkUseCrosmileFlag" runat="server" />CROSMILEを利用する。
                            </td>
                        </tr>
						<tr>
							<td class="tdHeaderStyle">
								CROSMILE SIPURI
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtCrosmileUri" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								音声通話ｱﾌﾟﾘ利用
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkUseVoiceappFlag" runat="server" />音声通話アプリを利用する。
							</td>
						</tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ｽﾏｰﾄﾀﾞｲﾚｸﾄ非ﾓﾆﾀﾘﾝｸﾞ
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkTalkDirectSmartNotMonitor" runat="server" />スマートダイレクト時、モニタリングしない。
                            </td>
                        </tr>
                        <tr>
							<td class="tdHeaderStyle">
								クレジット決済履歴
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkExistCreditDealFlag" runat="server" />クレジット決済履歴あり。
							</td>
						</tr>
						<asp:PlaceHolder ID="plcCreditMeasured" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderStyle">
									クレジット従量制
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCreditMeasuredRateTarget" runat="server" />クレジット従量制精算対象である。
								</td>
							</tr>
						</asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								広告コード
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
								<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
									ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="txtAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Detail"
									Display="Dynamic">*</asp:CustomValidator>
								<asp:Label ID="lblAdNm" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								紹介者コード
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblFriendIntroCd" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								紹介元コード
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblIntroducerFriendCd" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ハンドル名
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtHandleNm" runat="server" MaxLength="20" Width="200px"></asp:TextBox>
								<asp:HiddenField ID="hdnPrevHandleNm" runat="server">
								</asp:HiddenField>
								<asp:CustomValidator ID="vdcHandleNm" runat="server" ControlToValidate="txtHandleNm" ErrorMessage="" OnServerValidate="vdcHandleNm_ServerValidate" ValidationGroup="Detail"
									Display="Dynamic">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								生年月日
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtBirthDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
								<asp:RangeValidator ID="vddBirthDay" runat="server" ErrorMessage="生年月日を正しく入力して下さい。" ControlToValidate="txtBirthDay" MaximumValue="9999/12/31" MinimumValue="0001/01/01"
									Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
								<asp:CustomValidator ID="vdcMaxBirthday" runat="server" ControlToValidate="txtBirthDay" ErrorMessage="999才を超えています。" OnServerValidate="vdcMaxBirthday_ServerValidate"
									ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
								<asp:RegularExpressionValidator ID="vdeBirthDay" runat="server" ErrorMessage="生年月日を正しく入力して下さい。" ControlToValidate="txtBirthDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
									ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								<asp:CustomValidator ID="vdcBirthDay" runat="server" ControlToValidate="txtBirthDay" ErrorMessage="6歳未満です。" OnServerValidate="vdcBirthday_ServerValidate"
									ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ﾕｰｻﾞｰ定義ﾌﾗｸﾞ
							</td>
							<td class="tdDataStyle">
								<asp:CheckBoxList ID="chkUserDefineFlag" runat="server" DataSourceID="dsUserDefineFlag" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal"
									RepeatColumns="5">
								</asp:CheckBoxList>
							</td>
						</tr>
						<asp:PlaceHolder ID="plcHolder" runat="server">
						    <asp:TableRow ID="rowAttr0" runat="server">
							    <asp:TableCell ID="celManAttrNm0" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm0" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq0" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq0" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue0" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr1" runat="server">
							    <asp:TableCell ID="celManAttrNm1" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm1" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq1" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq1" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue1" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr2" runat="server">
							    <asp:TableCell ID="celManAttrNm2" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm2" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq2" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq2" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue2" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr3" runat="server">
							    <asp:TableCell ID="celManAttrNm3" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm3" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq3" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq3" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue3" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr4" runat="server">
							    <asp:TableCell ID="celManAttrNm4" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm4" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq4" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq4" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue4" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr5" runat="server">
							    <asp:TableCell ID="celManAttrNm5" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm5" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq5" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq5" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue5" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr6" runat="server">
							    <asp:TableCell ID="celManAttrNm6" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm6" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq6" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq6" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue6" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr7" runat="server">
							    <asp:TableCell ID="celManAttrNm7" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm7" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq7" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq7" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue7" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr8" runat="server">
							    <asp:TableCell ID="celManAttrNm8" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm8" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq8" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq8" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue8" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr9" runat="server">
							    <asp:TableCell ID="celManAttrNm9" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm9" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq9" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq9" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue9" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr10" runat="server">
							    <asp:TableCell ID="celManAttrNm10" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm10" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq10" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq10" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue10" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr11" runat="server">
							    <asp:TableCell ID="celManAttrNm11" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm11" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq11" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq11" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue11" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr12" runat="server">
							    <asp:TableCell ID="celManAttrNm12" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm12" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq12" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq12" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue12" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr13" runat="server">
							    <asp:TableCell ID="celManAttrNm13" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm13" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq13" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq13" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue13" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr14" runat="server">
							    <asp:TableCell ID="celManAttrNm14" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm14" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq14" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq14" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue14" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr15" runat="server">
							    <asp:TableCell ID="celManAttrNm15" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm15" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq15" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq15" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue15" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr16" runat="server">
							    <asp:TableCell ID="celManAttrNm16" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm16" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq16" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq16" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue16" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr17" runat="server">
							    <asp:TableCell ID="celManAttrNm17" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm17" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq17" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq17" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue17" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr18" runat="server">
							    <asp:TableCell ID="celManAttrNm18" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm18" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq18" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq18" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue18" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						    <asp:TableRow ID="rowAttr19" runat="server">
							    <asp:TableCell ID="celManAttrNm19" runat="server" CssClass="tdHeaderStyle">
								    <asp:Label ID="lblManAttrNm19" runat="server" Text="Label"></asp:Label>
							    </asp:TableCell>
							    <asp:TableCell ID="celManAttrSeq19" runat="server" CssClass="tdDataStyle">
								    <asp:DropDownList ID="lstManAttrSeq19" runat="server" Width="206px">
								    </asp:DropDownList>
								    <asp:TextBox ID="txtAttrInputValue19" runat="server" Width="200px" MaxLength="256">
								    </asp:TextBox>
							    </asp:TableCell>
						    </asp:TableRow>
						</asp:PlaceHolder>
                        <asp:PlaceHolder ID="plcManWaitingComment" runat="server">
                            <tr>
                                <td class="tdHeaderStyle">
                                    待機コメント
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtManWaitingComment" runat="server" MaxLength="1300" Width="360px"
                                        Height="48px" TextMode="multiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
						<tr>
							<td class="tdHeaderStyle">
								備 考
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRemarks1" runat="server" MaxLength="256" Width="360px " Height="48px" TextMode="MultiLine"></asp:TextBox><br />
								<asp:TextBox ID="txtRemarks2" runat="server" MaxLength="256" Width="360px"></asp:TextBox><br />
								<asp:TextBox ID="txtRemarks3" runat="server" MaxLength="256" Width="360px"></asp:TextBox><br />
								<asp:TextBox ID="txtRemarks4" runat="server" MaxLength="256" Width="360px" Height="140px" TextMode="MultiLine"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								変更者
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtModifyUserNm" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrModifyUserNm" runat="server" ErrorMessage="変更者を入力して下さい。" ControlToValidate="txtModifyUserNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:RequiredFieldValidator ID="vdrModifyUserNmByDelete" runat="server" ErrorMessage="削除者を入力して下さい。" ControlToValidate="txtModifyUserNm" ValidationGroup="Key">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								固体識別番号<br />
								(iMode ID等)
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtIModeId" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
								<asp:CustomValidator ID="vdcIModeId" runat="server" ControlToValidate="txtIModeId" ErrorMessage="既に利用されている固体識別番号です。" OnServerValidate="vdcIModeId_ServerValidate"
									ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								固体識別
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTerminalUniqueId" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
								<asp:CustomValidator ID="vdcTerminalUniqueId" runat="server" ControlToValidate="txtTerminalUniqueId" ErrorMessage="既に使われている固体識別です。" OnServerValidate="vdcTerminalUniqueId_ServerValidate"
									ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								利用端末種別
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstUseTerminalType" runat="server" Width="206px" DataSourceID="dsUseTerminalType" DataTextField="CODE_NM" DataValueField="CODE">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								テスト用URI
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtURI" runat="server" MaxLength="64" Width="150px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								テスト用SKYPE ID
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtSkypeId" runat="server" MaxLength="64" Width="150px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ﾃｽﾄﾕｰｻﾞｰﾌﾗｸﾞ
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkAdminFlag" runat="server" />サイトのテスト用ユーザーにする。
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								NG回数
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstNgCount" runat="server">
									<asp:ListItem Value="0">0回</asp:ListItem>
									<asp:ListItem Value="1">1回</asp:ListItem>
									<asp:ListItem Value="2">2回</asp:ListItem>
									<asp:ListItem Value="3">3回</asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								ドクロマーク
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstNgSkullCount" runat="server">
									<asp:ListItem Value="0">なし</asp:ListItem>
									<asp:ListItem Value="1">1つ</asp:ListItem>
									<asp:ListItem Value="2">2つ</asp:ListItem>
									<asp:ListItem Value="3">3つ</asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
					</table>
                    <asp:Panel ID="pnlCharacterEx" runat="server">
                        「キャラクター拡張」
                        <table border="0" style="width: 640px" class="tableStyle">
                            <asp:PlaceHolder ID="plcBlogMailRxType" runat="server">
                                <tr>
                                    <td class="tdHeaderStyle">
                                        ブログ更新メール受信区分
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:DropDownList ID="lstBlogMailRxType" runat="server" Width="206px" DataSourceID="dsBlogMailRxType"
                                            DataTextField="CODE_NM" DataValueField="CODE">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    待機通知受信区分
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkWaitMailRxFlag" runat="server" />通話履歴があるキャストが待機した場合に通知する。
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
					<table border="0" style="width: 640px">
						<tr>
							<td>
								<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
								<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
							</td>
							<td>
								<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekrightbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
							</td>
						</tr>
					</table>
				</fieldset>
			</asp:Panel>
			$NO_TRANS_END;
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetAbleToSiteList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetList" TypeName="Manager"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="50" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOnlineStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="52" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="66" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsInfoMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="15" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsBlogMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="11" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPrefecture" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="67" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTime" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="68" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUseTerminalType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="55" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserDefineFlag" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="A0" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrLoginPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeLoginPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdcTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdcEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrBalPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeBalPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrLimitPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeLimitPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vddReceiptLimitDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdeReceiptLimitDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vddUrgeEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdeUrgeEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vddUrgeRestartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdeUrgeRestartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdrServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdeServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vddServicePointEffectiveDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcServicePointEffectiveDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdeServicePointEffectiveTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdcServicePointEffectiveTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdeAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdcAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdrModifyUserNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdrModifyUserNmByDelete" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdcHandleNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vddBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdeBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdcBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdcMaxBirthday" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdcIModeId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdcTerminalUniqueId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLimitPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers, Custom" ValidChars="-" TargetControlID="txtBalPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBillAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalReceiptCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalReceiptAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUrgeLevel" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUrgeExecCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMaxUrgeCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUrgeCountToday" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtServicePoint" />
	<ajaxToolkit:MaskedEditExtender ID="mskServicePointEffectiveDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtServicePointEffectiveDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskBirthDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtBirthDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReceiptLimitDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReceiptLimitDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskUrgeEndDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtUrgeEndDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskUrgeRestartDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtUrgeRestartDay">
	</ajaxToolkit:MaskedEditExtender>
	<br />
</asp:Content>
