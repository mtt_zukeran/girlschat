<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManHistoryInquiry.aspx.cs" Inherits="Man_ManHistoryInquiry"
	Title="男性会員変更履歴検索" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性会員変更履歴検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[設定]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 900px" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 100px">
							サイト
						</td>
						<td class="tdDataStyle" style="width: 400px">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderSmallStyle" style="width: 100px">
							変更区分
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:CheckBox ID="chkRegist" runat="server" Text="登録" />
							<asp:CheckBox ID="chkModify" runat="server" Text="変更" />
							<asp:CheckBox ID="chkDelete" runat="server" Text="削除" />
							<asp:CheckBox ID="chkReceipt" runat="server" Text="入金" />
							<asp:CheckBox ID="chkTransPoint" runat="server" Text="ﾎﾟｲﾝﾄ振替" />
							<asp:CheckBox ID="chkCancelReceipt" runat="server" Text="入金取消" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle2" style="width: 100px; height: 26px;">
							ﾛｸﾞｲﾝＩＤ
						</td>
						<td class="tdDataStyle" style="width: 400px; height: 26px;">
							<asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="64px"></asp:TextBox>
						</td>
						<td class="tdHeaderSmallStyle2" style="height: 26px; width: 100px;">
							変更日
						</td>
						<td class="tdDataStyle" style="width: 400px; height: 26px;">
							<asp:TextBox ID="txtModifyDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							&nbsp;〜
							<asp:TextBox ID="txtModifyDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							<asp:RangeValidator ID="vdrModifyDayFrom" runat="server" ErrorMessage="変更日Fromを正しく入力して下さい。" ControlToValidate="txtModifyDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrModifyDayTo" runat="server" ErrorMessage="変更日Toを正しく入力して下さい。" ControlToValidate="txtModifyDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcModifyDayFromTo" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtModifyDayFrom" ControlToValidate="txtModifyDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[会員一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdUserHistory.PageIndex + 1%>
						of
						<%=grdUserHistory.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
					<asp:GridView ID="grdUserHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserHistory" SkinID="GridViewColor" AllowSorting="True"
						Font-Size="Small">
						<Columns>
							<asp:TemplateField HeaderText="変更内容">
								<ItemTemplate>
									<asp:Label ID="lblReason" runat="server" Text='<%# string.Format("{0}{1}",Eval("MODIFY_REASON_NM"),Eval("MODIFY_BEFORE_AFTER_NM")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:BoundField DataField="CREATE_DATE" HeaderText="変更日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="CREATE_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:BoundField DataField="LOGIN_PASSWORD" HeaderText="ﾊﾟｽﾜｰﾄﾞ" SortExpression="LOGIN_PASSWORD">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_RECEIPT_COUNT" HeaderText="入金回数" SortExpression="TOTAL_RECEIPT_COUNT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="BAL_POINT" HeaderText="残ﾎﾟｲﾝﾄ" SortExpression="BAL_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="LIMIT_POINT" HeaderText="制限ﾎﾟｲﾝﾄ" SortExpression="LIMIT_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="USER_STATUS_NM" HeaderText="会員状態" SortExpression="USER_STATUS_NM">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="MODIFY_USER_NM" HeaderText="変更者" SortExpression="MODIFY_USER_NM">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUserHistory" runat="server" SelectMethod="GetPageCollection" TypeName="UserHistory" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUserHistory_Selected" OnSelecting="dsUserHistory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pModifyReasonMask" Type="Int16" />
			<asp:Parameter Name="pLoginID" Type="String" />
			<asp:Parameter Name="pModifyDayFrom" Type="String" />
			<asp:Parameter Name="pModifyTimeFrom" Type="String" />
			<asp:Parameter Name="pModifyDayTo" Type="String" />
			<asp:Parameter Name="pModifyTimeTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrModifyDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrModifyDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcModifyDayFromTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskModifyDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtModifyDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskModifyDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtModifyDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
