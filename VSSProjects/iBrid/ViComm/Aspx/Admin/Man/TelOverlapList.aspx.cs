﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 電話番号重複一覧
--	Progaram ID		: TelOverlapList
--  Creation Date	: 2014.09.16
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Man_TelOverlapList:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {

	}

	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		string sUserSeq = e.CommandArgument.ToString();

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_TEL_OVERLAP");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,int.Parse(sUserSeq));
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		this.grdTelOverlap.DataBind();
	}
}
