﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者写真設定

--	Progaram ID		: CastPictureMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using iBridCommLib;
using ViComm;

public partial class Cast_CastPicMainte:System.Web.UI.Page {
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		ViewState["PIC_TYPE"] = iBridUtil.GetStringValue(Request.QueryString["pictype"]);
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;

		if (string.IsNullOrEmpty(ViewState["PIC_TYPE"] as string))
			ViewState["PIC_TYPE"] = ViCommConst.ATTACHED_PROFILE.ToString();

		if (GetMultiCharFlag(lblSiteCd.Text)) {
			lblHeader.Text = "ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.";
			lblUserCharNo.Text = " - " + lblUserCharNo.Text;
			lblUserCharNo.Visible = true;
		} else {
			lblHeader.Text = "ｻｲﾄ／SEQ.";
			lblUserCharNo.Visible = false;
		}

		using (Cast oCast = new Cast()) {
			string sName = string.Empty,sId = string.Empty;
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblCastNm.Text = sName;
			lblLoginId.Text = sId;
		}

		this.pnlList.Visible = false;
		this.trSaveType.Visible = false;
		this.rdoSaveTypeProfile.Checked = false;
		this.rdoSaveTypeMail.Checked = true;
		if (!ViewState["PIC_TYPE"].Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			this.pnlList.Visible = true;
			//this.trSaveType.Visible = true && ManageCompany.IsAvailableConvertImage();
			ClearField();
			DataBind();
		}

	}

	private void ClearField() {
	}

	protected void btnUp_Command(object sender,CommandEventArgs e) {
		SetPicPostion(decimal.Parse(e.CommandArgument.ToString()),-1);
	}

	protected void btnDown_Command(object sender,CommandEventArgs e) {
		SetPicPostion(decimal.Parse(e.CommandArgument.ToString()),1);
	}

	protected void btnUpload_Click(object sender,EventArgs e) {
		UploadPicture();
		ReturnToCall();
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}",ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString()));
	}

	protected void btnProfile_Command(object sender,CommandEventArgs e) {
		UpdateProfilePicture(iBridUtil.GetStringValue(e.CommandArgument),"0","1");
		FTPUpload(iBridUtil.GetStringValue(e.CommandArgument));
		ReturnToCall();
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}",ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString()));
	}

	protected void btnDelete_Command(object sender,CommandEventArgs e) {
		DeletePicture(e.CommandArgument.ToString());
		ReturnToCall();
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}",ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString()));
	}

	protected void btnCancel_Click(object sender,EventArgs e) {

		if (GetMultiCharFlag(ViewState["SITE_CD"].ToString())) {
			Server.Transfer(string.Format("CastView.aspx?sitecd={0}&loginid={1}&usercharno={2}&return={3}",ViewState["SITE_CD"].ToString(),lblLoginId.Text,ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
		} else {
			Server.Transfer(string.Format("CastView.aspx?loginid={0}&usercharno={1}&return={2}",lblLoginId.Text,ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
		}

	}

	private void UpdateProfilePicture(string pPicSeq,string pDelFlag,string pProfilePicFlag) {
		bool bOk = true;
		string sPicTitle = string.Empty;
		string sPicDoc = string.Empty;
		string sCastPicAttrTypeSeq = string.Empty;
		string sCastPicAttrSeq = string.Empty;

		using (CastPic oCastPic = new CastPic()) {
			bOk = oCastPic.GetOne(ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString(),pPicSeq);
			if (bOk) {
				sPicTitle = oCastPic.picTitle;
				sPicDoc = oCastPic.picDoc;
				sCastPicAttrTypeSeq = oCastPic.castPicAttrTypeSeq;
				sCastPicAttrSeq = oCastPic.castPicAttrSeq;
			}
		}

		if (bOk) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("CAST_PIC_PROFILE");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"]);
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,ViewState["USER_SEQ"].ToString());
				db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
				db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
				db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
				db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
				db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
				db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
				db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
				db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,sPicTitle);
				db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,sPicDoc);
				db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,sCastPicAttrTypeSeq);
				db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,sCastPicAttrSeq);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.cmd.BindByName = true;
				db.ExecuteProcedure();
			}
		}
	}

	private void SetPicPostion(Decimal pPicSeq,int pMove) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SET_PIC_POSITION");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"]);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
			db.ProcedureInParm("PMOVE_POS",DbSession.DbType.NUMBER,pMove);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Server.Transfer(string.Format("CastPicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}",ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString()));
	}

	protected Color GetBorderColor(object pProfileFlag) {
		if (pProfileFlag.ToString().Equals("1")) {
			return Color.Blue;
		} else {
			return Color.Empty;
		}
	}

	private void UploadPicture() {
		string sWebPhisicalDir = string.Empty;
		string sDomain = string.Empty;

		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"HOST_NM",ref sDomain);
		}

		using (CastPic objPic = new CastPic()) {
			decimal dNo = objPic.GetPicNo();

			string sFileNm = string.Empty,sPath = string.Empty,sFullPath = string.Empty;
			string sSiteCd = ViewState["SITE_CD"].ToString();

			if (uldCastPic.HasFile) {

				sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
				sPath = ViCommPrograms.GetCastPicDir(sWebPhisicalDir, ViewState["SITE_CD"].ToString(), lblLoginId.Text);								

				sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

				using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

					if (!System.IO.File.Exists(sFullPath)) {
						uldCastPic.SaveAs(sFullPath);

						if (ViewState["PIC_TYPE"].Equals(ViCommConst.ATTACHED_PROFILE.ToString()) && rdoSaveTypeProfile.Checked) {
							//Stream stStream = File.OpenRead(sFullPath);
							using (Bitmap bmSrc = new Bitmap(sFullPath)) {
								//	stStream.Close();

								int iWidth = bmSrc.Width / 2;
								int iHight = bmSrc.Height / 2;

								using (Bitmap bmDest = new Bitmap(bmSrc,iWidth,iHight)) {
									string sSmallPic = sWebPhisicalDir +
												ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
												sFileNm +
												ViCommConst.PIC_FOODER_SMALL;

									bmDest.Save(sSmallPic,ImageFormat.Jpeg);
								}
							}
							ImageHelper.SetCopyRight(sSiteCd,sPath,sFileNm + ViCommConst.PIC_FOODER);

						} else {
							// オリジナル画像をMailToolsディレクトリに配置
							string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
							uldCastPic.SaveAs(Path.Combine(sInputDir,sFileNm + ViCommConst.PIC_FOODER));

							ImageHelper.ConvertMobileImage(sSiteCd,sInputDir,sPath,sFileNm);
						}

					}
				}

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("CAST_PIC_UPLOAD");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"]);
					db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,ViewState["USER_SEQ"].ToString());
					db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
					db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,dNo);
					db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,int.Parse((string)ViewState["PIC_TYPE"]));
					db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,"no title");
					db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,string.Empty);
					db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
					db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
					db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}

			}
		}
	}


	private void DeletePicture(string pFileSeq) {
		string sFileNm = string.Empty;
		string sWebPhisicalDir = string.Empty;
		string sSiteCd = ViewState["SITE_CD"].ToString();
		using (Site oSite = new Site()) {
			oSite.GetValue(sSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}


		sFileNm = iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH);

		ViCommPrograms.DeleteFiles(ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sSiteCd,lblLoginId.Text),string.Format("{0}*",sFileNm));
		UpdateProfilePicture(pFileSeq,"1",string.Empty);
	}



	private void FTPUpload(string pFileSeq) {
		string sServerType;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("DECOMAIL_SERVER_TYPE",out sServerType);
		}
		if (sServerType.Equals(ViCommConst.DECO_SRV_COCOSPACE)) {
			return;
		}

		string sWebPhisicalDir = string.Empty;
		string sMailerIP = string.Empty;
		string sMailerFtpId = string.Empty;
		string sMailerFtpPw = string.Empty;
		string sFileNm;
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"TX_HI_MAILER_IP",ref sMailerIP);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"MAILER_FTP_ID",ref sMailerFtpId);
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"MAILER_FTP_PW",ref sMailerFtpPw);
		}

		sFileNm = iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH);

		string sFullPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
							sFileNm + ViCommConst.PIC_FOODER;

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			SysInterface.FTPUpload(sFullPath,sMailerIP,ViewState["SITE_CD"].ToString() + "_" + lblLoginId.Text + ViewState["USER_CHAR_NO"].ToString() + ".jpg",sMailerFtpId,sMailerFtpPw);
		}
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]);
		e.InputParameters[3] = ViCommConst.ATTACHED_PROFILE.ToString();
	}
	private bool GetMultiCharFlag(string pSiteCd) {
		bool bMultiCharFlag = false;
		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(pSiteCd)) {
				bMultiCharFlag = true;
			}
		}
		return bMultiCharFlag;
	}

	private void ReturnToCall() {
		if (ViewState["RETURN"].ToString().Equals("CastView")) {
			Response.Redirect(string.Format("../Cast/CastView.aspx?loginid={0}&moviesite={1}",lblLoginId.Text,ViewState["SITE_CD"].ToString()),true);
		}
	}

	protected string GetClosedMark(object pNotApproveFlag, object pNotPublishFlag, object pPicType) {
		if (ViCommConst.ATTACHED_HIDE.ToString().Equals(iBridUtil.GetStringValue(pPicType))) {
			return "非表示";
		} else if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNotApproveFlag)) || ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNotPublishFlag))) {
			return "非公開";
		}
		return "公開中";
	}

	protected Color GetClosedMarkColor(object pNotApproveFlag, object pNotPublishFlag, object pPicType) {
		if (ViCommConst.ATTACHED_HIDE.ToString().Equals(iBridUtil.GetStringValue(pPicType))) {
			return Color.Blue;
		} else if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNotApproveFlag)) || ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNotPublishFlag))) {
			return Color.Red;
		}
		return Color.Empty;
	}

	protected bool GetClosedVisible(object pNotApproveFlag,object pNotPublishFlag) {
		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNotApproveFlag)) || ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNotPublishFlag))) {
			return true;
		}
		return false;
	}

	protected void lnkClosed_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("CLOSE")) {
			return;
		}

		string[] sArgs = e.CommandArgument.ToString().Split(',');

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE_CLOSE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["SITE_CD"]));
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_CHAR_NO"]));
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,sArgs[0]);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,sArgs[1]);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		this.DataBind();
	}
	
	protected void lnkImgDownload_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("DOWNLOAD")) {
			return;
		}

		string sPicSeq = iBridUtil.GetStringValue(e.CommandArgument);
		string sWebPhisicalDir = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(iBridUtil.GetStringValue(ViewState["SITE_CD"]),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}
		string sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH);
		string sPath = sWebPhisicalDir +
						ViCommConst.PIC_DIRECTRY + "\\" + iBridUtil.GetStringValue(ViewState["SITE_CD"]) + "\\Operator\\" + lblLoginId.Text;

		string sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER_ORIGINAL;

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			Response.Filter = filter;
			Response.AddHeader("Content-Disposition","attachment;filename=" + sPicSeq + ViCommConst.PIC_FOODER_ORIGINAL);
			Response.ContentType = "image/jpeg";
			System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
			Response.Clear();
			Response.WriteFile(sFullPath);
			Response.Flush();
			Response.Close();
			Response.End();
		}
	}
}

