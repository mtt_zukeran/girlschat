﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VoiceUpload.aspx.cs" Inherits="Cast_VoiceUpload" Title="Untitled Page" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="着ボイス登録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend><%= DisplayWordUtil.Replace("[出演者着ボイスアップロード]") %></legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									<asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
									<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
									<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
									<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者名.") %>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									docomo用(3gp)
								</td>
								<td class="tdDataStyle">
									<asp:FileUpload ID="uldDocomo" runat="server" Width="400px" />
									<asp:RequiredFieldValidator ID="rfvDocomo" runat="server" ErrorMessage="docomo用アップロードファイルが入力されていません。" SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldDocomo">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeDocomo" runat="server" ErrorMessage="docomo用は3gpファイルを入力してください。" ValidationExpression=".*.(3)(g|G)(p|P)$" ControlToValidate="uldDocomo" ValidationGroup="Upload">*</asp:RegularExpressionValidator>
									
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									au用(mmf)
								</td>
								<td class="tdDataStyle">
									<asp:FileUpload ID="uldAu" runat="server" Width="400px" />
									<asp:RequiredFieldValidator ID="rfvAu" runat="server" ErrorMessage="au用アップロードファイルが入力されていません。" SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldAu">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAu" runat="server" ErrorMessage="au用はmmfファイルを入力してください。" ValidationExpression=".*.(m|M)(m|M)(f|F)$" ControlToValidate="uldAu" ValidationGroup="Upload">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									softbank用(mmf)
								</td>
								<td class="tdDataStyle">
									<asp:FileUpload ID="uldSoftbank" runat="server" Width="400px" />
									<asp:RequiredFieldValidator ID="rfvSoftbank" runat="server" ErrorMessage="softbank用アップロードファイルが入力されていません。" SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldSoftbank">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSoftbank" runat="server" ErrorMessage="softbank用はmmfファイルを入力してください。" ValidationExpression=".*.(m|M)(m|M)(f|F)$" ControlToValidate="uldSoftbank" ValidationGroup="Upload">*</asp:RegularExpressionValidator>									
								</td>
							</tr>
						</table>
						<asp:Button ID="btnUpload" runat="server" ValidationGroup="Upload" Text="実行" CssClass="seekbutton" OnClick="btnUpload_Click" />
						<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="rfvDocomo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="rfvAu" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="rfvSoftbank" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeDocomo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeAu" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeSoftbank" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpload" ConfirmText="アップロードを行ないますか？" ConfirmOnFormSubmit="true" />

</asp:Content>

