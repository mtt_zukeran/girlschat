﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CastDiaryInquiry.aspx.cs" Inherits="Status_CastDiaryInquiry" Title="日記記録" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="日記記録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 720px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            書込日
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:TextBox ID="txtCreateDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:TextBox ID="txtCreateTimeFrom" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                            ～&nbsp;
                            <asp:TextBox ID="txtCreateDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:TextBox ID="txtCreateTimeTo" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                            <asp:RangeValidator ID="vdrCreateTimeFrom" runat="server" ErrorMessage="書込時Fromを正しく入力して下さい。"
                                ControlToValidate="txtCreateTimeFrom" MaximumValue="23" MinimumValue="00" Type="Integer"
                                ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdrCreateTimeTo" runat="server" ErrorMessage="書込時Toを正しく入力して下さい。"
                                ControlToValidate="txtCreateTimeTo" MaximumValue="23" MinimumValue="00" Type="Integer"
                                ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="vdrCreateDayFrom" runat="server" ErrorMessage="送受信日Fromを入力して下さい。"
                                ControlToValidate="txtCreateDayFrom" ValidationGroup="Key" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="vdeCreateDayFrom" runat="server" ErrorMessage="書込日Fromを正しく入力して下さい。"
                                ControlToValidate="txtCreateDayFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdeCreateDayTo" runat="server" ErrorMessage="書込日Toを正しく入力して下さい。"
                                ControlToValidate="txtCreateDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:CompareValidator ID="vdcCreateDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                ControlToCompare="txtCreateDayFrom" ControlToValidate="txtCreateDayTo" Operator="GreaterThanEqual"
                                ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ログインID
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLoginId" runat="server" Width="70px"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle2">
                            キャラクターNo
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtUserCharNo" runat="server" Width="35px" MaxLength="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            キーワード(空白区切り)
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:TextBox ID="txtKeyword" runat="server" Width="500px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[掲示板記録]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdCastDiary.PageIndex + 1%>
                        of
                        <%=grdCastDiary.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Button runat="server" ID="btnAdminCheck" Text="選択したものを確認済にする" CssClass="seektopbutton" OnClick="btnAdminCheck_Click" />&nbsp;
				<asp:Button runat="server" ID="btnAdminCheckAll" Text="表示されているものを確認済にする" CssClass="seektopbutton" OnClick="btnAdminCheckAll_Click" />
				<br /><br />
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
                    <asp:GridView ID="grdCastDiary" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsCastDiary" AllowSorting="True" SkinID="GridViewColor" OnRowDataBound="grdCastDiary_RowDataBound">
                        <Columns>
							<asp:TemplateField HeaderText="確認">
								<ItemTemplate>
									<asp:Label ID="lblAdminChek" runat="server" Text="済" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR) ? true : false %>'></asp:Label>
									<asp:CheckBox ID="chkAdminCheck" runat="server" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR) ? true : false %>' />
	                                <asp:HiddenField ID="hdnUserSeq" runat="server" Value='<%# Eval("USER_SEQ") %>' />
	                                <asp:HiddenField ID="hdnUserCharNo" runat="server" Value='<%# Eval("USER_CHAR_NO") %>' />
	                                <asp:HiddenField ID="hdnReportDay" runat="server" Value='<%# Eval("REPORT_DAY") %>' />
	                                <asp:HiddenField ID="hdnSubSeq" runat="server" Value='<%# Eval("CAST_DIARY_SUB_SEQ") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
                            <asp:TemplateField HeaderText="書込日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE")%>'></asp:Label><br />
                                    <asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%#string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("REPORT_DAY"),Eval("CAST_DIARY_SUB_SEQ"),Eval("LOGIN_ID"),Eval("DEL_FLAG"),Eval("ADMIN_DEL_FLAG")) %>'
                                        OnClientClick="return confirm('削除を実行しますか？');" OnCommand="lnkDelCastDiary_Command"
                                        Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("ADMIN_DEL_FLAG","{0}")) ? "復活" : "削除" %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ログインID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return=CastDiaryInquiry.aspx",Eval("LOGIN_ID")) %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ハンドル名">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'
                                        ID="lblHandleNm" />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Label ID="lblMailTitle" runat="server" Text='<%# Eval("DIARY_TITLE") %>'></asp:Label><br />
                                    <asp:Label ID="lblDoc" runat="server" Text='<%# string.Concat(Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4")).Replace(System.Environment.NewLine, "<br />") %>' Width="500px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="画像">
                                <ItemTemplate>
                                    <%--<asp:Image ID="imgPic" runat="server" ImageUrl='<%# string.Format("../{0}",Eval("OBJ_SMALL_PHOTO_IMG_PATH").ToString()) %>'
                                        Visible='<%# GetPicVisible(Eval("PIC_SEQ")) %>'></asp:Image>--%>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# Eval("OBJ_SMALL_PHOTO_IMG_PATH", "../{0}") %>'
                                        OnClientClick='<%# GetClientScript(Eval("OBJ_PHOTO_IMG_PATH"))  %>' Visible='<%# GetPicVisible(Eval("PIC_SEQ")) %>' />
                                    <asp:LinkButton ID="lnkDelPic" runat="server" CommandArgument='<%#string.Format("{0},{1},{2},{3},{4}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("REPORT_DAY"),Eval("CAST_DIARY_SUB_SEQ")) %>'
                                        OnClientClick="return confirm('画像の削除を実行しますか？');" OnCommand="lnkDelCastDiaryPic_Command" Visible='<%# GetPicVisible(Eval("PIC_SEQ")) %>' Text="<br>画像削除"></asp:LinkButton>
                                </ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                            </asp:TemplateField>
 							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<%# GetDelFlagMark(Eval("DEL_FLAG"),Eval("ADMIN_DEL_FLAG")) %>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
                            <asp:TemplateField HeaderText="いいね">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLikeList" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastDiaryLikeList.aspx?sitecd={0}&userseq={1}&usercharno={2}&reportday={3}&subseq={4}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("REPORT_DAY"),Eval("CAST_DIARY_SUB_SEQ")) %>' Visible='<%# GetLikeLinkVisible(Eval("LIKE_COUNT")) %>'>一覧</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsCastDiary" runat="server" SelectMethod="GetPageCollection"
        TypeName="CastDiary" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsCastDiary_Selected"
        OnSelecting="dsCastDiary_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pCreateDayFrom" Type="String" />
            <asp:Parameter Name="pCreateTimeFrom" Type="String" />
            <asp:Parameter Name="pCreateDayTo" Type="String" />
            <asp:Parameter Name="pCreateTimeTo" Type="String" />
            <asp:Parameter Name="pLoginId" Type="String" />
            <asp:Parameter Name="pUserCharNo" Type="String" />
            <asp:Parameter Name="pKeyword" Type="String" DefaultValue="" />
            <asp:QueryStringParameter Name="pScreenId" Type="String" QueryStringField="scrid" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
        TargetControlID="vdrCreateDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
        TargetControlID="vdeCreateDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="vdeCreateDayTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
        TargetControlID="vdrCreateTimeFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
        TargetControlID="vdrCreateTimeTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
        TargetControlID="vdcCreateDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:MaskedEditExtender ID="mskCreateDayFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtCreateDayFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskCreateDayTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtCreateDayTo">
    </ajaxToolkit:MaskedEditExtender>
</asp:Content>
