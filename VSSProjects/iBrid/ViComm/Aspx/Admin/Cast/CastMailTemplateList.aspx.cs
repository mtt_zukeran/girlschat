﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャストメールテンプレートメンテナンス
--	Progaram ID		: CastMailTemplateList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_CastMailTemplateList:System.Web.UI.Page {
	private string recCount = "";

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);

		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdTemplate.PageSize = 999;
	}

	private void InitPage() {
	
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
	
	
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;
		lblUserCharNo.Text = " - " + lblUserCharNo.Text;

		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(lblSiteCd.Text)) {
				lblHeader.Text = "ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.";
				lblUserCharNo.Visible = true;
			} else {
				lblHeader.Text = "ｻｲﾄ／SEQ";
				lblUserCharNo.Visible = false;
			}
		}
		using (Cast oCast = new Cast()) {
			string sName = "",sId = "";
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblCastNm.Text = sName;
			lblLoginId.Text = sId;
		}
		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
			this.txtHtmlDoc.Visible = !this.DisablePinEdit;
			this.txtHtmlDocText.Visible = this.DisablePinEdit;
		}
		lblHtmlDocSeq.Text = "";
		ClearField();
		pnlSelect.Visible = false;
		pnlDtl.Visible = false;
		grdSiteHtmlDoc.DataSourceID = "";
	}

	private void ClearField() {
		txtHtmlDocTitle.Text = "";
		this.HtmlDoc = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdTemplate.PageIndex = 0;
		grdTemplate.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		EndEdit();
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnSeekCancel_Click(object sender,EventArgs e) {
		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(lblSiteCd.Text)) {
				Response.Redirect(string.Format("../Cast/CastView.aspx?sitecd={0}&loginid={1}&return={2}",lblSiteCd.Text,lblLoginId.Text,ViewState["RETURN"].ToString()));
			} else {
				Response.Redirect(string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",lblLoginId.Text,ViewState["RETURN"].ToString()));
			}
		}
	}

	private void GetData() {
		string sSubSeq = "";

		using (MailTemplate oTemplate = new MailTemplate()) {
			sSubSeq = oTemplate.GetModifiableSubSeq(lblSiteCd.Text,lstMailTemplateType.SelectedValue);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MAIL_TEMPLATE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,lblUserSeq.Text);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateType.SelectedValue);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.VARCHAR2,sSubSeq);
			db.ProcedureOutParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PROWID_VIEW",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO_VIEW"] = db.GetStringValue("PREVISION_NO_VIEW");
			ViewState["ROWID_VIEW"] = db.GetStringValue("PROWID_VIEW");

			ViewState["REVISION_NO_MANAGE"] = db.GetStringValue("PREVISION_NO_MANAGE");
			ViewState["ROWID_MANAGE"] = db.GetStringValue("PROWID_MANAGE");

			ViewState["REVISION_NO_DOC"] = db.GetStringValue("PREVISION_NO_DOC");
			ViewState["ROWID_DOC"] = db.GetStringValue("PROWID_DOC");

			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");

			this.HtmlDoc = "";
			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMailTitle.Text = db.GetStringValue("PMAIL_TITLE");
				txtHtmlDocTitle.Text = db.GetStringValue("PHTML_DOC_TITLE");
				for (int i = 0;i < SysConst.MAX_HTML_BLOCKS;i++) {
					this.HtmlDoc = this.HtmlDoc + db.GetArryStringValue("PHTML_DOC",i);
				}
			} else {
				ClearField();
			}
		}

		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
		pnlSelect.Visible = true;
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
	}

	private void UpdateData(int pDelFlag) {
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(this.HtmlDoc),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		string sSubSeq = "";

		using (MailTemplate oTemplate = new MailTemplate()) {
			sSubSeq = oTemplate.GetModifiableSubSeq(lblSiteCd.Text,lstMailTemplateType.SelectedValue);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MAIL_TEMPLATE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,lblUserSeq.Text);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateType.SelectedValue);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.VARCHAR2,sSubSeq);
			db.ProcedureInParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2,txtMailTitle.Text);
			db.ProcedureInParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2,txtHtmlDocTitle.Text);
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PROWID_VIEW",DbSession.DbType.VARCHAR2,ViewState["ROWID_VIEW"].ToString());
			db.ProcedureInParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER,ViewState["REVISION_NO_VIEW"].ToString());
			db.ProcedureInParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2,ViewState["ROWID_MANAGE"].ToString());
			db.ProcedureInParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER,ViewState["REVISION_NO_MANAGE"].ToString());
			db.ProcedureInParm("PROWID_DOC",DbSession.DbType.VARCHAR2,ViewState["ROWID_DOC"].ToString());
			db.ProcedureInParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER,ViewState["REVISION_NO_DOC"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");
		}
		EndEdit();
	}

	protected void lnkTemplate_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstMailTemplateType.SelectedValue = sKeys[0];
		lblHtmlDocSeq.Text = sKeys[1];
		pnlMainte.Visible = true;
		pnlSelect.Visible = true;
		pnlDtl.Visible = false;
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
		GetData();
	}

	protected void lnkDoc_Command(object sender,CommandEventArgs e) {
		GetData();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {

	}
	protected void dsSiteHtmlDoc_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblHtmlDocSeq.Text;
	}

	protected void dsMailTemplate_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsMailTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblSiteCd.Text;
		e.InputParameters[1] = lblUserSeq.Text;
		e.InputParameters[2] = ViewState["USER_CHAR_NO"].ToString();
	}

	protected void dsOrgTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblSiteCd.Text;
	}

	private void EndEdit() {
		ClearField();
		pnlDtl.Visible = false;
		pnlSelect.Visible = true;
		pnlSelect.Visible = false;
		pnlKey.Enabled = true;
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
		grdTemplate.DataBind();
	}
}
