﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 販売動画設定
--	Progaram ID		: SaleMovieMainte
--
--  Creation Date	: 2010.07.23
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  


-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Cast_SaleMovieMainte : System.Web.UI.Page {

	protected decimal? ThumbnailPicSeq{
		get { return ViewState["ThumbnailPicSeq"] as decimal?; }
		set { this.ViewState["ThumbnailPicSeq"] = value; }
	}

	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;
		ViewState["MOVIE_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		ViewState["OBJ_NOT_PUBLISH_FLAG"] = iBridUtil.GetStringValue(Request.QueryString["closed"]);
		string sMovieType = iBridUtil.GetStringValue(Request.QueryString["movietype"]);
		if (string.IsNullOrEmpty(sMovieType)) sMovieType = ViCommConst.ATTACHED_PROFILE.ToString();
		ViewState["MOVIE_TYPE"] = sMovieType;


		using (Cast oCast = new Cast()) {
			string sName = "", sId = "";
			oCast.GetValue(lblUserSeq.Text, "CAST_NM", ref sName);
			oCast.GetValue(lblUserSeq.Text, "LOGIN_ID", ref sId);
			lblUserNm.Text = sName;
			lblLoginId.Text = sId;
		}
		ClearField();
		DataBind();
		GetData();
	}

	private void ClearField() {
		txtChargePoint.Text = "";
		txtMovieTitle.Text = "";
		lstAuthType.SelectedIndex = 0;
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender, EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		ReturnToCall();
	}


	protected void lnkSampleMovie_Command(object sender, CommandEventArgs e) {
		using (CastMovie oMovie = new CastMovie()) {
			decimal dMovieSeq = decimal.Parse(ViewState["MOVIE_SEQ"].ToString());
			if (oMovie.GetOne(dMovieSeq)) {
				string sRoot = ConfigurationManager.AppSettings["Root"];
				string sSampleMovieFileNm = MovieConvertHelper.GetFileNm(decimal.Parse(e.CommandArgument.ToString()));
				string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + oMovie.siteCd + string.Format("/operator/{0}/{1}{2}", oMovie.loginId, sSampleMovieFileNm, ViCommConst.MOVIE_FOODER);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>", sURL);
				ClientScript.RegisterStartupScript(Page.GetType(), "OpenNewWindow", sScripts);
			}
		}
	}
	protected void btnThumbnailPic_Click(object sender, EventArgs e) {
		if (uldThumbnailPic.HasFile) {
			string sDomain = string.Empty;
			string sWebPhisicalDir = string.Empty;
			string sSiteCd = ViewState["SITE_CD"].ToString();
			string sLoginId = lblLoginId.Text;
			
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(), "WEB_PHISICAL_DIR", ref sWebPhisicalDir);
				oSite.GetValue(ViewState["SITE_CD"].ToString(), "HOST_NM", ref sDomain);
			}

			using (CastPic objPic = new CastPic()) {
				decimal dNo = objPic.GetPicNo();

				string sPath = ViCommPrograms.GetCastPicDir(sWebPhisicalDir, sSiteCd, sLoginId);
				
				string sFileNm = ViCommPrograms.GeneratePicFileNm(dNo);
				string sFullPath = this.GenerateImagePath(sPath,sFileNm);

				using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
					if (!System.IO.File.Exists(sFullPath)) {
						// 画像の保存
						uldThumbnailPic.SaveAs(sFullPath);
					}
					ImageHelper.SetCopyRight(sSiteCd, sPath, sFileNm + ViCommConst.PIC_FOODER);
				}
				this.ThumbnailPicSeq = dNo;
			}
			this.ShowThumbnailPic();
		}
	}
	protected void dsMovieAttrType_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}
	
	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_MOVIE_GET");
			db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, ViewState["SITE_CD"]);
			db.ProcedureInParm("PUSER_SEQ", DbSession.DbType.VARCHAR2, ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("PUSER_CHAR_NO", DbSession.DbType.VARCHAR2, ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("PMOVIE_SEQ", DbSession.DbType.VARCHAR2, ViewState["MOVIE_SEQ"].ToString());
			db.ProcedureOutParm("PMOVIE_TITLE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_DOC", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_POINT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_TYPE", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_TYPE_SEQ", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_SEQ", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_TIME", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_SERIES", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSAMPLE_MOVIE_SEQ", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTHUMBNAIL_PIC_SEQ", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMovieTitle.Text = db.GetStringValue("PMOVIE_TITLE");
				txtChargePoint.Text = db.GetStringValue("PCHARGE_POINT");
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}
				// 再生時間
				txtPlayTime.Text = db.GetStringValue("PPLAY_TIME");
				// サンプルムービー
				decimal dSampleMovieSeq = db.GetDecimalValue("PSAMPLE_MOVIE_SEQ");
				if(dSampleMovieSeq>decimal.Zero){
					lnkSampleMovie.Text = "確認する";
					lnkSampleMovie.CommandArgument = dSampleMovieSeq.ToString();
					lnkSampleMovie.Enabled = true;
				} else {
					lnkSampleMovie.Text = "なし";
					lnkSampleMovie.CommandArgument="";
					lnkSampleMovie.Enabled=false;
				}
				// サムネイル画像
				decimal dThumbnailPic = db.GetDecimalValue("PTHUMBNAIL_PIC_SEQ");
				if(dThumbnailPic != decimal.Zero) this.ThumbnailPicSeq = dThumbnailPic;
				this.ShowThumbnailPic();		
				// シリーズ
				string sMovieSeries = db.GetStringValue("PMOVIE_SERIES");
				if(!string.IsNullOrEmpty(sMovieSeries)){
					ListItem oListItem = this.lstMovieSeries.Items.FindByValue(sMovieSeries);
					if(oListItem != null) {
						oListItem.Selected=true;
					}
				}
				// 動画説明
				this.txtMovieDoc.Text = db.GetStringValue("pMOVIE_DOC");
				
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		int iNotApproveFlag = 0;
		int iDelFlag = 0;
		int iNotPublishFlag = 0;

		if (pDelFlag == 0) {
			switch (lstAuthType.SelectedValue) {
				case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
					iDelFlag = 0;
					iNotApproveFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_OK:	// 公開
					iDelFlag = 0;
					iNotApproveFlag = 0;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
					iNotApproveFlag = 0;
					iDelFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_NG:	// 非公開

					iNotApproveFlag = 0;
					iDelFlag = 0;
					iNotPublishFlag = 1;
					break;
			}
		} else {
			iDelFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MOVIE_MAINTE");
			db.ProcedureInParm("PMOVIE_SEQ", DbSession.DbType.VARCHAR2, ViewState["MOVIE_SEQ"].ToString());
			db.ProcedureInParm("PMOVIE_TITLE", DbSession.DbType.VARCHAR2, txtMovieTitle.Text);
			db.ProcedureInParm("PMOVIE_DOC", DbSession.DbType.VARCHAR2, this.txtMovieDoc.Text);
			db.ProcedureInParm("PCHARGE_POINT", DbSession.DbType.NUMBER, decimal.Parse(txtChargePoint.Text));
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG", DbSession.DbType.NUMBER, iNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("PMOVIE_TYPE", DbSession.DbType.NUMBER, int.Parse((string)ViewState["MOVIE_TYPE"]));
			db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ", DbSession.DbType.NUMBER, ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString());
			db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ", DbSession.DbType.NUMBER, ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString());
			db.ProcedureInParm("PROWID", DbSession.DbType.VARCHAR2, ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO", DbSession.DbType.NUMBER, decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG", DbSession.DbType.NUMBER, iDelFlag);
			db.ProcedureInParm("PPLAY_TIME", DbSession.DbType.VARCHAR2, this.txtPlayTime.Text);
			db.ProcedureInParm("PMOVIE_SERIES_SEQ", DbSession.DbType.NUMBER, this.lstMovieSeries.SelectedValue);
			db.ProcedureInParm("PTHUMBNAIL_PIC_SEQ", DbSession.DbType.NUMBER, this.ThumbnailPicSeq);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (iDelFlag == 1) {
			string sWebPhisicalDir = "";
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(), "WEB_PHISICAL_DIR", ref sWebPhisicalDir);
			}

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, "", ViCommConst.FILE_UPLOAD_PASSWORD)) {
				string sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(), ViCommConst.OBJECT_NM_LENGTH) +
									ViCommConst.MOVIE_FOODER;

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}

				sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(), ViCommConst.OBJECT_NM_LENGTH) +
									".3g2";

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}
			}
		}

		ReturnToCall();
	}

	private void ReturnToCall() {
		if (ViewState["RETURN"].ToString().Equals("ProfileMovieCheckList")) {
			Server.Transfer(string.Format("../CastAdmin/ProfileMovieCheckList.aspx?moviesite={0}&closed={1}",ViewState["SITE_CD"].ToString(),ViewState["OBJ_NOT_PUBLISH_FLAG"].ToString()));

		} else if (ViewState["RETURN"].ToString().Equals("ProfileMovieOpenList")) {
			Server.Transfer(string.Format("../CastAdmin/ProfileMovieOpenList.aspx?moviesite={0}", ViewState["SITE_CD"].ToString()));

			//} else if (ViewState["RETURN"].ToString().Equals("ProfileMovieClosedList")) {
			//    Server.Transfer(string.Format("../CastAdmin/ProfileMovieClosedList.aspx?moviesite={0}",ViewState["SITE_CD"].ToString()));

		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&moviesite={1}", lblLoginId.Text, ViewState["SITE_CD"].ToString()));
		}
	}
	
	private void ShowThumbnailPic(){
		bool bHasThumbnailPic = (this.ThumbnailPicSeq != null);

		if (bHasThumbnailPic) {
			this.imgThumbnailPic.ImageUrl = this.GenerateWebImagePath(this.ThumbnailPicSeq.Value);
		}
		this.imgThumbnailPic.Visible = bHasThumbnailPic;
		this.lblThumbnailPicNothingMessage.Visible = !bHasThumbnailPic;		
	}

	/// <summary>
	/// 画像を格納するディレクトリの物理パスを生成する
	/// </summary>
	/// <param name="pWebPhisicalDir"></param>
	/// <param name="pSiteCd"></param>
	/// <param name="pLoginId"></param>
	/// <returns></returns>
	private string GenerateImageDirectoryPath(string pWebPhisicalDir,string pSiteCd, string pLoginId) {
		return pWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\Operator\\" + pLoginId;
	}
	/// <summary>
	/// 画像のフルパスを生成する
	/// </summary>
	/// <param name="pImageDirectoryPath"></param>
	/// <param name="pFileNm"></param>
	/// <returns></returns>
	private string GenerateImagePath(string pImageDirectoryPath, string pFileNm) {
		return pImageDirectoryPath + "\\" + pFileNm + ViCommConst.PIC_FOODER;
	}
	private string GenerateWebImageDirectoryPath() {
		string sSiteCd = ViewState["SITE_CD"] as string;
		string sLoginId = this.lblLoginId.Text;
		return ConfigurationManager.AppSettings["Root"] + ViCommConst.WEB_PIC_PATH + "/" + sSiteCd + "/Operator/" + sLoginId + "/";
	}
	private string GenerateWebImagePath(decimal pPicSeq) {
		return this.GenerateWebImageDirectoryPath()+ ViCommPrograms.GeneratePicFileNm(pPicSeq) + ViCommConst.PIC_FOODER;
	}
	
}
