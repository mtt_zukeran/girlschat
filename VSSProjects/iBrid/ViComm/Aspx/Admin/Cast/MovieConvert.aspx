﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MovieConvert.aspx.cs" Inherits="Cast_MovieConvert" Title="Untitled Page" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="販売動画登録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend><%= DisplayWordUtil.Replace("[出演者]") %></legend>

				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
							<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
							<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
							<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者名.") %>
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							アスペクト比
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoAspectNone"   runat="server" GroupName="aspect" Checked="true"  Text="指定しない"></asp:RadioButton>
							<asp:RadioButton ID="rdoAspectNormal" runat="server" GroupName="aspect" Checked="false" Text="1.33:1（4:3）"></asp:RadioButton>
							<asp:RadioButton ID="rdoAspectWide"   runat="server" GroupName="aspect" Checked="false" Text="1.78:1（16:9）"></asp:RadioButton>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							サムネイル画像
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkCreateThumbnail" runat="server"  Checked="true"  Text="作成する"></asp:CheckBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnRefresh" Text="最新の情報に更新" CssClass="seekbutton" OnClick="btnRefresh_Click" CausesValidation="False" />						
				<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
			</fieldset>
			<fieldset>
				<legend>登録対象動画ファイル</legend>
				<br />
				<asp:GridView ID="grdTargetFile" runat="server" AllowPaging="False" AutoGenerateColumns="False" SkinID="GridView" Width="740px">
					<EmptyDataTemplate>
						<asp:Label ID="lblNotFound" runat="server" Text="登録対象のファイルが存在しません。" ForeColor="Red" Font-Bold="true"></asp:Label>
					</EmptyDataTemplate>
					<Columns>
						<asp:BoundField HeaderText="ファイル名" DataField="Name" />
						<asp:TemplateField HeaderText="サンプル動画" 
							ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right">
							<ItemTemplate>
								<%# HasSampleMovie((System.IO.FileInfo)Container.DataItem)?"あり":"なし"%>
							</ItemTemplate>
						</asp:TemplateField>						
						<asp:TemplateField HeaderText="サイズ(KB)" 
							ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right">
							<ItemTemplate>
								<%# string.Format("{0:#,##0}",Convert2KB(((System.IO.FileInfo)Container.DataItem).Length))%>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="更新日時" DataField="LastWriteTime" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" 
							ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" />
						<asp:TemplateField HeaderText="登　録" ItemStyle-Width="64px" ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<asp:LinkButton ID="btnConvert" runat="server" Text="登　録" OnClientClick="return confirm('登録を行いますか？');" OnCommand="btnConvert_Command" CommandArgument="<%# ((System.IO.FileInfo)Container.DataItem).FullName %>"/>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>

			</fieldset>			
		</asp:Panel>
	</div>
	
</asp:Content>

