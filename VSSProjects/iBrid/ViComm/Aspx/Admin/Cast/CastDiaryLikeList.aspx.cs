﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: いいね履歴

--	Progaram ID		: CastDiaryLikeList
--
--  Creation Date	: 2011.09.09
--  Creater			: iBrid
--
**************************************************************************/

// [ this.Update History ]
/*------------------------------------------------------------------------

  Date        this.Updater    this.Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Windows.Forms;


public partial class Cast_CastDiaryLikeList : System.Web.UI.Page
{
    private string recCount = "";

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            InitPage();
            FirstLoad();
			GetList();
        }
    }

    protected void InitPage() {
        this.ClearField();
    }
    private void FirstLoad() {
        this.DataBind();
        if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
            this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
        }
    }
    private void ClearField() {
        this.recCount = "0";
        this.pnlGrid.Visible = false;

		this.txtLoginId.Text = string.Empty;
    }
    private void GetList() {
        this.grdCastDiaryLikeList.PageIndex = 0;
        this.grdCastDiaryLikeList.DataSourceID = "dsCastDiaryLikeList";
        this.grdCastDiaryLikeList.DataBind();
        this.pnlCount.DataBind();
        this.pnlGrid.Visible = true;
    }

    protected string GetRecCount() {
        return this.recCount;
    }
    #region === Event Methods ===
    protected void btnListSeek_Click(object sender, EventArgs e) {
        this.GetList();
    }
	protected void btnClear_Click(object sender,EventArgs e) {
        this.ClearField();
    }
    protected void dsCastDiaryLikeList_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (e.ReturnValue != null) {
            this.recCount = e.ReturnValue.ToString();
        }
    }
    protected void dsCastDiaryLikeList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pLoginId"] = this.txtLoginId.Text.Trim();
    }
    protected void lnkLoginId_Command(object sender, CommandEventArgs e) {
        string[] sValues = e.CommandArgument.ToString().Split(',');
		Server.Transfer(string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",sValues[0],sValues[1]));
    }
    #endregion
}
